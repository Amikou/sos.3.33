package androidx.media3.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.ui.AspectRatioFrameLayout;
import androidx.media3.ui.PlayerControlView;
import com.github.mikephil.charting.utils.Utils;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* loaded from: classes.dex */
public class PlayerView extends FrameLayout {
    public CharSequence A0;
    public int B0;
    public boolean C0;
    public boolean D0;
    public boolean E0;
    public int F0;
    public final a a;
    public final AspectRatioFrameLayout f0;
    public final View g0;
    public final View h0;
    public final boolean i0;
    public final ImageView j0;
    public final SubtitleView k0;
    public final View l0;
    public final TextView m0;
    public final PlayerControlView n0;
    public final FrameLayout o0;
    public final FrameLayout p0;
    public q q0;
    public boolean r0;
    public b s0;
    public PlayerControlView.m t0;
    public c u0;
    public boolean v0;
    public Drawable w0;
    public int x0;
    public boolean y0;
    public bw0<? super PlaybackException> z0;

    /* loaded from: classes.dex */
    public final class a implements q.d, View.OnLayoutChangeListener, View.OnClickListener, PlayerControlView.m, PlayerControlView.d {
        public final u.b a = new u.b();
        public Object f0;

        public a() {
        }

        @Override // androidx.media3.ui.PlayerControlView.m
        public void A(int i) {
            PlayerView.this.M0();
            if (PlayerView.this.s0 != null) {
                PlayerView.this.s0.a(i);
            }
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void D(int i) {
            nr2.p(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void E(boolean z) {
            nr2.i(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void F(int i) {
            nr2.t(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void H(boolean z) {
            nr2.g(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void I() {
            nr2.x(this);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void J(q qVar, q.c cVar) {
            nr2.f(this, qVar, cVar);
        }

        @Override // androidx.media3.ui.PlayerControlView.d
        public void K(boolean z) {
            if (PlayerView.this.u0 != null) {
                PlayerView.this.u0.a(z);
            }
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void L(u uVar, int i) {
            nr2.B(this, uVar, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void N(boolean z) {
            nr2.y(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Q(int i, boolean z) {
            nr2.e(this, i, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void R(boolean z, int i) {
            nr2.s(this, z, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void T(n nVar) {
            nr2.k(this, nVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void V(x xVar) {
            nr2.C(this, xVar);
        }

        @Override // androidx.media3.common.q.d
        public void X() {
            if (PlayerView.this.g0 != null) {
                PlayerView.this.g0.setVisibility(4);
            }
        }

        @Override // androidx.media3.common.q.d
        public void Y(y yVar) {
            q qVar = (q) ii.e(PlayerView.this.q0);
            u S = qVar.S();
            if (S.q()) {
                this.f0 = null;
            } else if (!qVar.C().b()) {
                this.f0 = S.g(qVar.n(), this.a, true).f0;
            } else {
                Object obj = this.f0;
                if (obj != null) {
                    int b = S.b(obj);
                    if (b != -1) {
                        if (qVar.I() == S.f(b, this.a).g0) {
                            return;
                        }
                    }
                    this.f0 = null;
                }
            }
            PlayerView.this.P0(false);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Z(h hVar) {
            nr2.d(this, hVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void a0(m mVar, int i) {
            nr2.j(this, mVar, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void b(boolean z) {
            nr2.z(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void c0(PlaybackException playbackException) {
            nr2.r(this, playbackException);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void h0(PlaybackException playbackException) {
            nr2.q(this, playbackException);
        }

        @Override // androidx.media3.common.q.d
        public void i(int i) {
            PlayerView.this.L0();
            PlayerView.this.O0();
            PlayerView.this.N0();
        }

        @Override // androidx.media3.common.q.d
        public void k(z zVar) {
            PlayerView.this.K0();
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void k0(int i, int i2) {
            nr2.A(this, i, i2);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void l0(q.b bVar) {
            nr2.a(this, bVar);
        }

        @Override // androidx.media3.common.q.d
        public void m0(q.e eVar, q.e eVar2, int i) {
            if (PlayerView.this.A0() && PlayerView.this.D0) {
                PlayerView.this.y0();
            }
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void n(p pVar) {
            nr2.n(this, pVar);
        }

        @Override // androidx.media3.common.q.d
        public void o(nb0 nb0Var) {
            if (PlayerView.this.k0 != null) {
                PlayerView.this.k0.setCues(nb0Var.a);
            }
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PlayerView.this.J0();
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            PlayerView.s0((TextureView) view, PlayerView.this.F0);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void p0(boolean z) {
            nr2.h(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void s(int i) {
            nr2.w(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void t(Metadata metadata) {
            nr2.l(this, metadata);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void u(List list) {
            nr2.c(this, list);
        }

        @Override // androidx.media3.common.q.d
        public void y(boolean z, int i) {
            PlayerView.this.L0();
            PlayerView.this.N0();
        }
    }

    /* loaded from: classes.dex */
    public interface b {
        void a(int i);
    }

    /* loaded from: classes.dex */
    public interface c {
        void a(boolean z);
    }

    public PlayerView(Context context) {
        this(context, null);
    }

    public static void F0(AspectRatioFrameLayout aspectRatioFrameLayout, int i) {
        aspectRatioFrameLayout.setResizeMode(i);
    }

    public static void s0(TextureView textureView, int i) {
        Matrix matrix = new Matrix();
        float width = textureView.getWidth();
        float height = textureView.getHeight();
        if (width != Utils.FLOAT_EPSILON && height != Utils.FLOAT_EPSILON && i != 0) {
            float f = width / 2.0f;
            float f2 = height / 2.0f;
            matrix.postRotate(i, f, f2);
            RectF rectF = new RectF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, width, height);
            RectF rectF2 = new RectF();
            matrix.mapRect(rectF2, rectF);
            matrix.postScale(width / rectF2.width(), height / rectF2.height(), f, f2);
        }
        textureView.setTransform(matrix);
    }

    public static void u0(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(oz2.exo_edit_mode_logo));
        imageView.setBackgroundColor(resources.getColor(qy2.exo_edit_mode_background_color));
    }

    public static void v0(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(oz2.exo_edit_mode_logo, null));
        imageView.setBackgroundColor(resources.getColor(qy2.exo_edit_mode_background_color, null));
    }

    public final boolean A0() {
        q qVar = this.q0;
        return qVar != null && qVar.g() && this.q0.k();
    }

    public final void B0(boolean z) {
        if (!(A0() && this.D0) && R0()) {
            boolean z2 = this.n0.f0() && this.n0.getShowTimeoutMs() <= 0;
            boolean G0 = G0();
            if (z || z2 || G0) {
                I0(G0);
            }
        }
    }

    public void C0(AspectRatioFrameLayout aspectRatioFrameLayout, float f) {
        if (aspectRatioFrameLayout != null) {
            aspectRatioFrameLayout.setAspectRatio(f);
        }
    }

    public final boolean D0(n nVar) {
        byte[] bArr = nVar.n0;
        if (bArr == null) {
            return false;
        }
        return E0(new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(bArr, 0, bArr.length)));
    }

    public final boolean E0(Drawable drawable) {
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicWidth > 0 && intrinsicHeight > 0) {
                C0(this.f0, intrinsicWidth / intrinsicHeight);
                this.j0.setImageDrawable(drawable);
                this.j0.setVisibility(0);
                return true;
            }
        }
        return false;
    }

    public final boolean G0() {
        q qVar = this.q0;
        if (qVar == null) {
            return true;
        }
        int B = qVar.B();
        return this.C0 && !this.q0.S().q() && (B == 1 || B == 4 || !((q) ii.e(this.q0)).k());
    }

    public void H0() {
        I0(G0());
    }

    public final void I0(boolean z) {
        if (R0()) {
            this.n0.setShowTimeoutMs(z ? 0 : this.B0);
            this.n0.r0();
        }
    }

    public final void J0() {
        if (!R0() || this.q0 == null) {
            return;
        }
        if (!this.n0.f0()) {
            B0(true);
        } else if (this.E0) {
            this.n0.b0();
        }
    }

    public final void K0() {
        q qVar = this.q0;
        z p = qVar != null ? qVar.p() : z.i0;
        int i = p.a;
        int i2 = p.f0;
        int i3 = p.g0;
        float f = Utils.FLOAT_EPSILON;
        float f2 = (i2 == 0 || i == 0) ? 0.0f : (i * p.h0) / i2;
        View view = this.h0;
        if (view instanceof TextureView) {
            if (f2 > Utils.FLOAT_EPSILON && (i3 == 90 || i3 == 270)) {
                f2 = 1.0f / f2;
            }
            if (this.F0 != 0) {
                view.removeOnLayoutChangeListener(this.a);
            }
            this.F0 = i3;
            if (i3 != 0) {
                this.h0.addOnLayoutChangeListener(this.a);
            }
            s0((TextureView) this.h0, this.F0);
        }
        AspectRatioFrameLayout aspectRatioFrameLayout = this.f0;
        if (!this.i0) {
            f = f2;
        }
        C0(aspectRatioFrameLayout, f);
    }

    public final void L0() {
        int i;
        if (this.l0 != null) {
            q qVar = this.q0;
            boolean z = true;
            if (qVar == null || qVar.B() != 2 || ((i = this.x0) != 2 && (i != 1 || !this.q0.k()))) {
                z = false;
            }
            this.l0.setVisibility(z ? 0 : 8);
        }
    }

    public final void M0() {
        PlayerControlView playerControlView = this.n0;
        if (playerControlView != null && this.r0) {
            if (playerControlView.f0()) {
                setContentDescription(this.E0 ? getResources().getString(m13.exo_controls_hide) : null);
                return;
            } else {
                setContentDescription(getResources().getString(m13.exo_controls_show));
                return;
            }
        }
        setContentDescription(null);
    }

    public final void N0() {
        if (A0() && this.D0) {
            y0();
        } else {
            B0(false);
        }
    }

    public final void O0() {
        bw0<? super PlaybackException> bw0Var;
        TextView textView = this.m0;
        if (textView != null) {
            CharSequence charSequence = this.A0;
            if (charSequence != null) {
                textView.setText(charSequence);
                this.m0.setVisibility(0);
                return;
            }
            q qVar = this.q0;
            PlaybackException v = qVar != null ? qVar.v() : null;
            if (v != null && (bw0Var = this.z0) != null) {
                this.m0.setText((CharSequence) bw0Var.a(v).second);
                this.m0.setVisibility(0);
                return;
            }
            this.m0.setVisibility(8);
        }
    }

    public final void P0(boolean z) {
        q qVar = this.q0;
        if (qVar != null && !qVar.C().b()) {
            if (z && !this.y0) {
                t0();
            }
            if (qVar.C().c(2)) {
                x0();
                return;
            }
            t0();
            if (Q0() && (D0(qVar.b0()) || E0(this.w0))) {
                return;
            }
            x0();
        } else if (this.y0) {
        } else {
            x0();
            t0();
        }
    }

    public final boolean Q0() {
        if (this.v0) {
            ii.i(this.j0);
            return true;
        }
        return false;
    }

    public final boolean R0() {
        if (this.r0) {
            ii.i(this.n0);
            return true;
        }
        return false;
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        q qVar = this.q0;
        if (qVar != null && qVar.g()) {
            return super.dispatchKeyEvent(keyEvent);
        }
        boolean z0 = z0(keyEvent.getKeyCode());
        if (z0 && R0() && !this.n0.f0()) {
            B0(true);
        } else if (!w0(keyEvent) && !super.dispatchKeyEvent(keyEvent)) {
            if (z0 && R0()) {
                B0(true);
                return false;
            }
            return false;
        } else {
            B0(true);
        }
        return true;
    }

    public List<f8> getAdOverlayInfos() {
        ArrayList arrayList = new ArrayList();
        FrameLayout frameLayout = this.p0;
        if (frameLayout != null) {
            arrayList.add(new f8(frameLayout, 4, "Transparent overlay does not impact viewability"));
        }
        PlayerControlView playerControlView = this.n0;
        if (playerControlView != null) {
            arrayList.add(new f8(playerControlView, 1));
        }
        return ImmutableList.copyOf((Collection) arrayList);
    }

    public ViewGroup getAdViewGroup() {
        return (ViewGroup) ii.j(this.o0, "exo_ad_overlay must be present for ad playback");
    }

    public boolean getControllerAutoShow() {
        return this.C0;
    }

    public boolean getControllerHideOnTouch() {
        return this.E0;
    }

    public int getControllerShowTimeoutMs() {
        return this.B0;
    }

    public Drawable getDefaultArtwork() {
        return this.w0;
    }

    public FrameLayout getOverlayFrameLayout() {
        return this.p0;
    }

    public q getPlayer() {
        return this.q0;
    }

    public int getResizeMode() {
        ii.i(this.f0);
        return this.f0.getResizeMode();
    }

    public SubtitleView getSubtitleView() {
        return this.k0;
    }

    public boolean getUseArtwork() {
        return this.v0;
    }

    public boolean getUseController() {
        return this.r0;
    }

    public View getVideoSurfaceView() {
        return this.h0;
    }

    @Override // android.view.View
    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!R0() || this.q0 == null) {
            return false;
        }
        B0(true);
        return true;
    }

    @Override // android.view.View
    public boolean performClick() {
        J0();
        return super.performClick();
    }

    public void setAspectRatioListener(AspectRatioFrameLayout.b bVar) {
        ii.i(this.f0);
        this.f0.setAspectRatioListener(bVar);
    }

    public void setControllerAutoShow(boolean z) {
        this.C0 = z;
    }

    public void setControllerHideDuringAds(boolean z) {
        this.D0 = z;
    }

    public void setControllerHideOnTouch(boolean z) {
        ii.i(this.n0);
        this.E0 = z;
        M0();
    }

    @Deprecated
    public void setControllerOnFullScreenModeChangedListener(PlayerControlView.d dVar) {
        ii.i(this.n0);
        this.u0 = null;
        this.n0.setOnFullScreenModeChangedListener(dVar);
    }

    public void setControllerShowTimeoutMs(int i) {
        ii.i(this.n0);
        this.B0 = i;
        if (this.n0.f0()) {
            H0();
        }
    }

    public void setControllerVisibilityListener(b bVar) {
        this.s0 = bVar;
        setControllerVisibilityListener((PlayerControlView.m) null);
    }

    public void setCustomErrorMessage(CharSequence charSequence) {
        ii.g(this.m0 != null);
        this.A0 = charSequence;
        O0();
    }

    public void setDefaultArtwork(Drawable drawable) {
        if (this.w0 != drawable) {
            this.w0 = drawable;
            P0(false);
        }
    }

    public void setErrorMessageProvider(bw0<? super PlaybackException> bw0Var) {
        if (this.z0 != bw0Var) {
            this.z0 = bw0Var;
            O0();
        }
    }

    public void setExtraAdGroupMarkers(long[] jArr, boolean[] zArr) {
        ii.i(this.n0);
        this.n0.setExtraAdGroupMarkers(jArr, zArr);
    }

    public void setFullscreenButtonClickListener(c cVar) {
        ii.i(this.n0);
        this.u0 = cVar;
        this.n0.setOnFullScreenModeChangedListener(this.a);
    }

    public void setKeepContentOnPlayerReset(boolean z) {
        if (this.y0 != z) {
            this.y0 = z;
            P0(false);
        }
    }

    public void setPlayer(q qVar) {
        ii.g(Looper.myLooper() == Looper.getMainLooper());
        ii.a(qVar == null || qVar.T() == Looper.getMainLooper());
        q qVar2 = this.q0;
        if (qVar2 == qVar) {
            return;
        }
        if (qVar2 != null) {
            qVar2.G(this.a);
            View view = this.h0;
            if (view instanceof TextureView) {
                qVar2.o((TextureView) view);
            } else if (view instanceof SurfaceView) {
                qVar2.M((SurfaceView) view);
            }
        }
        SubtitleView subtitleView = this.k0;
        if (subtitleView != null) {
            subtitleView.setCues(null);
        }
        this.q0 = qVar;
        if (R0()) {
            this.n0.setPlayer(qVar);
        }
        L0();
        O0();
        P0(true);
        if (qVar != null) {
            if (qVar.J(27)) {
                View view2 = this.h0;
                if (view2 instanceof TextureView) {
                    qVar.Z((TextureView) view2);
                } else if (view2 instanceof SurfaceView) {
                    qVar.t((SurfaceView) view2);
                }
                K0();
            }
            if (this.k0 != null && qVar.J(28)) {
                this.k0.setCues(qVar.F().a);
            }
            qVar.O(this.a);
            B0(false);
            return;
        }
        y0();
    }

    public void setRepeatToggleModes(int i) {
        ii.i(this.n0);
        this.n0.setRepeatToggleModes(i);
    }

    public void setResizeMode(int i) {
        ii.i(this.f0);
        this.f0.setResizeMode(i);
    }

    public void setShowBuffering(int i) {
        if (this.x0 != i) {
            this.x0 = i;
            L0();
        }
    }

    public void setShowFastForwardButton(boolean z) {
        ii.i(this.n0);
        this.n0.setShowFastForwardButton(z);
    }

    public void setShowMultiWindowTimeBar(boolean z) {
        ii.i(this.n0);
        this.n0.setShowMultiWindowTimeBar(z);
    }

    public void setShowNextButton(boolean z) {
        ii.i(this.n0);
        this.n0.setShowNextButton(z);
    }

    public void setShowPreviousButton(boolean z) {
        ii.i(this.n0);
        this.n0.setShowPreviousButton(z);
    }

    public void setShowRewindButton(boolean z) {
        ii.i(this.n0);
        this.n0.setShowRewindButton(z);
    }

    public void setShowShuffleButton(boolean z) {
        ii.i(this.n0);
        this.n0.setShowShuffleButton(z);
    }

    public void setShowSubtitleButton(boolean z) {
        ii.i(this.n0);
        this.n0.setShowSubtitleButton(z);
    }

    public void setShowVrButton(boolean z) {
        ii.i(this.n0);
        this.n0.setShowVrButton(z);
    }

    public void setShutterBackgroundColor(int i) {
        View view = this.g0;
        if (view != null) {
            view.setBackgroundColor(i);
        }
    }

    public void setUseArtwork(boolean z) {
        ii.g((z && this.j0 == null) ? false : true);
        if (this.v0 != z) {
            this.v0 = z;
            P0(false);
        }
    }

    public void setUseController(boolean z) {
        boolean z2 = false;
        ii.g((z && this.n0 == null) ? false : true);
        if (z || hasOnClickListeners()) {
            z2 = true;
        }
        setClickable(z2);
        if (this.r0 == z) {
            return;
        }
        this.r0 = z;
        if (R0()) {
            this.n0.setPlayer(this.q0);
        } else {
            PlayerControlView playerControlView = this.n0;
            if (playerControlView != null) {
                playerControlView.b0();
                this.n0.setPlayer(null);
            }
        }
        M0();
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        View view = this.h0;
        if (view instanceof SurfaceView) {
            view.setVisibility(i);
        }
    }

    public final void t0() {
        View view = this.g0;
        if (view != null) {
            view.setVisibility(0);
        }
    }

    public boolean w0(KeyEvent keyEvent) {
        return R0() && this.n0.U(keyEvent);
    }

    public final void x0() {
        ImageView imageView = this.j0;
        if (imageView != null) {
            imageView.setImageResource(17170445);
            this.j0.setVisibility(4);
        }
    }

    public void y0() {
        PlayerControlView playerControlView = this.n0;
        if (playerControlView != null) {
            playerControlView.b0();
        }
    }

    @SuppressLint({"InlinedApi"})
    public final boolean z0(int i) {
        return i == 19 || i == 270 || i == 22 || i == 271 || i == 20 || i == 269 || i == 21 || i == 268 || i == 23;
    }

    public PlayerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public PlayerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2;
        boolean z;
        int i3;
        boolean z2;
        int i4;
        boolean z3;
        int i5;
        int i6;
        boolean z4;
        boolean z5;
        int i7;
        boolean z6;
        boolean z7;
        int i8;
        boolean z8;
        a aVar = new a();
        this.a = aVar;
        if (isInEditMode()) {
            this.f0 = null;
            this.g0 = null;
            this.h0 = null;
            this.i0 = false;
            this.j0 = null;
            this.k0 = null;
            this.l0 = null;
            this.m0 = null;
            this.n0 = null;
            this.o0 = null;
            this.p0 = null;
            ImageView imageView = new ImageView(context);
            if (androidx.media3.common.util.b.a >= 23) {
                v0(getResources(), imageView);
            } else {
                u0(getResources(), imageView);
            }
            addView(imageView);
            return;
        }
        int i9 = w03.exo_player_view;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, b33.PlayerView, i, 0);
            try {
                int i10 = b33.PlayerView_shutter_background_color;
                boolean hasValue = obtainStyledAttributes.hasValue(i10);
                int color = obtainStyledAttributes.getColor(i10, 0);
                int resourceId = obtainStyledAttributes.getResourceId(b33.PlayerView_player_layout_id, i9);
                boolean z9 = obtainStyledAttributes.getBoolean(b33.PlayerView_use_artwork, true);
                int resourceId2 = obtainStyledAttributes.getResourceId(b33.PlayerView_default_artwork, 0);
                boolean z10 = obtainStyledAttributes.getBoolean(b33.PlayerView_use_controller, true);
                int i11 = obtainStyledAttributes.getInt(b33.PlayerView_surface_type, 1);
                int i12 = obtainStyledAttributes.getInt(b33.PlayerView_resize_mode, 0);
                int i13 = obtainStyledAttributes.getInt(b33.PlayerView_show_timeout, 5000);
                boolean z11 = obtainStyledAttributes.getBoolean(b33.PlayerView_hide_on_touch, true);
                boolean z12 = obtainStyledAttributes.getBoolean(b33.PlayerView_auto_show, true);
                i4 = obtainStyledAttributes.getInteger(b33.PlayerView_show_buffering, 0);
                this.y0 = obtainStyledAttributes.getBoolean(b33.PlayerView_keep_content_on_player_reset, this.y0);
                boolean z13 = obtainStyledAttributes.getBoolean(b33.PlayerView_hide_during_ads, true);
                obtainStyledAttributes.recycle();
                z3 = z11;
                z = z12;
                i3 = i12;
                z6 = z10;
                i7 = resourceId2;
                z5 = z9;
                z4 = hasValue;
                i6 = color;
                i5 = i11;
                i9 = resourceId;
                i2 = i13;
                z2 = z13;
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        } else {
            i2 = 5000;
            z = true;
            i3 = 0;
            z2 = true;
            i4 = 0;
            z3 = true;
            i5 = 1;
            i6 = 0;
            z4 = false;
            z5 = true;
            i7 = 0;
            z6 = true;
        }
        LayoutInflater.from(context).inflate(i9, this);
        setDescendantFocusability(262144);
        AspectRatioFrameLayout aspectRatioFrameLayout = (AspectRatioFrameLayout) findViewById(n03.exo_content_frame);
        this.f0 = aspectRatioFrameLayout;
        if (aspectRatioFrameLayout != null) {
            F0(aspectRatioFrameLayout, i3);
        }
        View findViewById = findViewById(n03.exo_shutter);
        this.g0 = findViewById;
        if (findViewById != null && z4) {
            findViewById.setBackgroundColor(i6);
        }
        if (aspectRatioFrameLayout != null && i5 != 0) {
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            if (i5 == 2) {
                this.h0 = new TextureView(context);
            } else if (i5 == 3) {
                try {
                    this.h0 = (View) Class.forName("androidx.media3.exoplayer.video.spherical.SphericalGLSurfaceView").getConstructor(Context.class).newInstance(context);
                    z8 = true;
                    this.h0.setLayoutParams(layoutParams);
                    this.h0.setOnClickListener(aVar);
                    this.h0.setClickable(false);
                    aspectRatioFrameLayout.addView(this.h0, 0);
                    z7 = z8;
                } catch (Exception e) {
                    throw new IllegalStateException("spherical_gl_surface_view requires an ExoPlayer dependency", e);
                }
            } else if (i5 != 4) {
                this.h0 = new SurfaceView(context);
            } else {
                try {
                    this.h0 = (View) Class.forName("androidx.media3.exoplayer.video.VideoDecoderGLSurfaceView").getConstructor(Context.class).newInstance(context);
                } catch (Exception e2) {
                    throw new IllegalStateException("video_decoder_gl_surface_view requires an ExoPlayer dependency", e2);
                }
            }
            z8 = false;
            this.h0.setLayoutParams(layoutParams);
            this.h0.setOnClickListener(aVar);
            this.h0.setClickable(false);
            aspectRatioFrameLayout.addView(this.h0, 0);
            z7 = z8;
        } else {
            this.h0 = null;
            z7 = false;
        }
        this.i0 = z7;
        this.o0 = (FrameLayout) findViewById(n03.exo_ad_overlay);
        this.p0 = (FrameLayout) findViewById(n03.exo_overlay);
        ImageView imageView2 = (ImageView) findViewById(n03.exo_artwork);
        this.j0 = imageView2;
        this.v0 = z5 && imageView2 != null;
        if (i7 != 0) {
            this.w0 = m70.f(getContext(), i7);
        }
        SubtitleView subtitleView = (SubtitleView) findViewById(n03.exo_subtitles);
        this.k0 = subtitleView;
        if (subtitleView != null) {
            subtitleView.setUserDefaultStyle();
            subtitleView.setUserDefaultTextSize();
        }
        View findViewById2 = findViewById(n03.exo_buffering);
        this.l0 = findViewById2;
        if (findViewById2 != null) {
            findViewById2.setVisibility(8);
        }
        this.x0 = i4;
        TextView textView = (TextView) findViewById(n03.exo_error_message);
        this.m0 = textView;
        if (textView != null) {
            textView.setVisibility(8);
        }
        int i14 = n03.exo_controller;
        PlayerControlView playerControlView = (PlayerControlView) findViewById(i14);
        View findViewById3 = findViewById(n03.exo_controller_placeholder);
        if (playerControlView != null) {
            this.n0 = playerControlView;
            i8 = 0;
        } else if (findViewById3 != null) {
            i8 = 0;
            PlayerControlView playerControlView2 = new PlayerControlView(context, null, 0, attributeSet);
            this.n0 = playerControlView2;
            playerControlView2.setId(i14);
            playerControlView2.setLayoutParams(findViewById3.getLayoutParams());
            ViewGroup viewGroup = (ViewGroup) findViewById3.getParent();
            int indexOfChild = viewGroup.indexOfChild(findViewById3);
            viewGroup.removeView(findViewById3);
            viewGroup.addView(playerControlView2, indexOfChild);
        } else {
            i8 = 0;
            this.n0 = null;
        }
        PlayerControlView playerControlView3 = this.n0;
        this.B0 = playerControlView3 != null ? i2 : i8;
        this.E0 = z3;
        this.C0 = z;
        this.D0 = z2;
        this.r0 = (!z6 || playerControlView3 == null) ? i8 : 1;
        if (playerControlView3 != null) {
            playerControlView3.c0();
            this.n0.S(aVar);
        }
        if (z6) {
            setClickable(true);
        }
        M0();
    }

    @Deprecated
    public void setControllerVisibilityListener(PlayerControlView.m mVar) {
        ii.i(this.n0);
        PlayerControlView.m mVar2 = this.t0;
        if (mVar2 == mVar) {
            return;
        }
        if (mVar2 != null) {
            this.n0.m0(mVar2);
        }
        this.t0 = mVar;
        if (mVar != null) {
            this.n0.S(mVar);
        }
        setControllerVisibilityListener((b) null);
    }
}
