package androidx.media3.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import androidx.media3.ui.SubtitleView;
import com.github.mikephil.charting.utils.Utils;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
final class CanvasSubtitleOutput extends View implements SubtitleView.a {
    public final List<vv3> a;
    public List<kb0> f0;
    public int g0;
    public float h0;
    public zv i0;
    public float j0;

    public CanvasSubtitleOutput(Context context) {
        this(context, null);
    }

    public static kb0 b(kb0 kb0Var) {
        kb0.b p = kb0Var.b().k(-3.4028235E38f).l(Integer.MIN_VALUE).p(null);
        if (kb0Var.j0 == 0) {
            p.h(1.0f - kb0Var.i0, 0);
        } else {
            p.h((-kb0Var.i0) - 1.0f, 1);
        }
        int i = kb0Var.k0;
        if (i == 0) {
            p.i(2);
        } else if (i == 2) {
            p.i(0);
        }
        return p.a();
    }

    @Override // androidx.media3.ui.SubtitleView.a
    public void a(List<kb0> list, zv zvVar, float f, int i, float f2) {
        this.f0 = list;
        this.i0 = zvVar;
        this.h0 = f;
        this.g0 = i;
        this.j0 = f2;
        while (this.a.size() < list.size()) {
            this.a.add(new vv3(getContext()));
        }
        invalidate();
    }

    @Override // android.view.View
    public void dispatchDraw(Canvas canvas) {
        List<kb0> list = this.f0;
        if (list.isEmpty()) {
            return;
        }
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int width = getWidth() - getPaddingRight();
        int paddingBottom = height - getPaddingBottom();
        if (paddingBottom <= paddingTop || width <= paddingLeft) {
            return;
        }
        int i = paddingBottom - paddingTop;
        float h = yv3.h(this.g0, this.h0, height, i);
        if (h <= Utils.FLOAT_EPSILON) {
            return;
        }
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            kb0 kb0Var = list.get(i2);
            if (kb0Var.t0 != Integer.MIN_VALUE) {
                kb0Var = b(kb0Var);
            }
            kb0 kb0Var2 = kb0Var;
            int i3 = paddingBottom;
            this.a.get(i2).b(kb0Var2, this.i0, h, yv3.h(kb0Var2.r0, kb0Var2.s0, height, i), this.j0, canvas, paddingLeft, paddingTop, width, i3);
            i2++;
            size = size;
            i = i;
            paddingBottom = i3;
            width = width;
        }
    }

    public CanvasSubtitleOutput(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new ArrayList();
        this.f0 = Collections.emptyList();
        this.g0 = 0;
        this.h0 = 0.0533f;
        this.i0 = zv.g;
        this.j0 = 0.08f;
    }
}
