package androidx.media3.ui;

import android.content.Context;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;
import android.widget.FrameLayout;
import androidx.media3.ui.SubtitleView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
final class WebViewSubtitleOutput extends FrameLayout implements SubtitleView.a {
    public final CanvasSubtitleOutput a;
    public final WebView f0;
    public List<kb0> g0;
    public zv h0;
    public float i0;
    public int j0;
    public float k0;

    /* loaded from: classes.dex */
    public class a extends WebView {
        public a(WebViewSubtitleOutput webViewSubtitleOutput, Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        @Override // android.webkit.WebView, android.view.View
        public boolean onTouchEvent(MotionEvent motionEvent) {
            super.onTouchEvent(motionEvent);
            return false;
        }

        @Override // android.view.View
        public boolean performClick() {
            super.performClick();
            return false;
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Layout.Alignment.values().length];
            a = iArr;
            try {
                iArr[Layout.Alignment.ALIGN_NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Layout.Alignment.ALIGN_OPPOSITE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[Layout.Alignment.ALIGN_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public WebViewSubtitleOutput(Context context) {
        this(context, null);
    }

    public static int b(int i) {
        if (i != 1) {
            return i != 2 ? 0 : -100;
        }
        return -50;
    }

    public static String c(Layout.Alignment alignment) {
        if (alignment == null) {
            return "center";
        }
        int i = b.a[alignment.ordinal()];
        return i != 1 ? i != 2 ? "center" : "end" : "start";
    }

    public static String d(zv zvVar) {
        int i = zvVar.d;
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    return i != 4 ? "unset" : androidx.media3.common.util.b.A("-0.05em -0.05em 0.15em %s", fl1.b(zvVar.e));
                }
                return androidx.media3.common.util.b.A("0.06em 0.08em 0.15em %s", fl1.b(zvVar.e));
            }
            return androidx.media3.common.util.b.A("0.1em 0.12em 0.15em %s", fl1.b(zvVar.e));
        }
        return androidx.media3.common.util.b.A("1px 1px 0 %1$s, 1px -1px 0 %1$s, -1px 1px 0 %1$s, -1px -1px 0 %1$s", fl1.b(zvVar.e));
    }

    public static String f(int i) {
        return i != 1 ? i != 2 ? "horizontal-tb" : "vertical-lr" : "vertical-rl";
    }

    public static String h(kb0 kb0Var) {
        float f = kb0Var.u0;
        if (f != Utils.FLOAT_EPSILON) {
            int i = kb0Var.t0;
            return androidx.media3.common.util.b.A("%s(%.2fdeg)", (i == 2 || i == 1) ? "skewY" : "skewX", Float.valueOf(f));
        }
        return "";
    }

    @Override // androidx.media3.ui.SubtitleView.a
    public void a(List<kb0> list, zv zvVar, float f, int i, float f2) {
        this.h0 = zvVar;
        this.i0 = f;
        this.j0 = i;
        this.k0 = f2;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            kb0 kb0Var = list.get(i2);
            if (kb0Var.h0 != null) {
                arrayList.add(kb0Var);
            } else {
                arrayList2.add(kb0Var);
            }
        }
        if (!this.g0.isEmpty() || !arrayList2.isEmpty()) {
            this.g0 = arrayList2;
            i();
        }
        this.a.a(arrayList, zvVar, f, i, f2);
        invalidate();
    }

    public final String e(int i, float f) {
        float h = yv3.h(i, f, getHeight(), (getHeight() - getPaddingTop()) - getPaddingBottom());
        return h == -3.4028235E38f ? "unset" : androidx.media3.common.util.b.A("%.2fpx", Float.valueOf(h / getContext().getResources().getDisplayMetrics().density));
    }

    public void g() {
        this.f0.destroy();
    }

    /* JADX WARN: Code restructure failed: missing block: B:38:0x0144, code lost:
        if (r13 != 0) goto L55;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x0147, code lost:
        if (r13 != 0) goto L54;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x0149, code lost:
        r21 = "left";
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x014b, code lost:
        r22 = "top";
        r13 = 2;
        r23 = r21;
     */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00f8  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0107  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0121  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0124  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x013b  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0147  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x0183  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x021f  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x023b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void i() {
        /*
            Method dump skipped, instructions count: 699
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.ui.WebViewSubtitleOutput.i():void");
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (!z || this.g0.isEmpty()) {
            return;
        }
        i();
    }

    public WebViewSubtitleOutput(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g0 = Collections.emptyList();
        this.h0 = zv.g;
        this.i0 = 0.0533f;
        this.j0 = 0;
        this.k0 = 0.08f;
        CanvasSubtitleOutput canvasSubtitleOutput = new CanvasSubtitleOutput(context, attributeSet);
        this.a = canvasSubtitleOutput;
        a aVar = new a(this, context, attributeSet);
        this.f0 = aVar;
        aVar.setBackgroundColor(0);
        addView(canvasSubtitleOutput);
        addView(aVar);
    }
}
