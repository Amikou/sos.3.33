package androidx.media3.ui;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.media3.ui.DefaultTimeBar;
import androidx.media3.ui.b;
import com.github.mikephil.charting.utils.Utils;
import java.util.Collections;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArraySet;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public class DefaultTimeBar extends View implements b {
    public final Runnable A0;
    public final CopyOnWriteArraySet<b.a> B0;
    public final Point C0;
    public final float D0;
    public int E0;
    public long F0;
    public int G0;
    public Rect H0;
    public ValueAnimator I0;
    public float J0;
    public boolean K0;
    public boolean L0;
    public long M0;
    public long N0;
    public long O0;
    public long P0;
    public int Q0;
    public long[] R0;
    public boolean[] S0;
    public final Rect a;
    public final Rect f0;
    public final Rect g0;
    public final Rect h0;
    public final Paint i0;
    public final Paint j0;
    public final Paint k0;
    public final Paint l0;
    public final Paint m0;
    public final Paint n0;
    public final Drawable o0;
    public final int p0;
    public final int q0;
    public final int r0;
    public final int s0;
    public final int t0;
    public final int u0;
    public final int v0;
    public final int w0;
    public final int x0;
    public final StringBuilder y0;
    public final Formatter z0;

    public DefaultTimeBar(Context context) {
        this(context, null);
    }

    public static int d(float f, int i) {
        return (int) ((i * f) + 0.5f);
    }

    private long getPositionIncrement() {
        long j = this.F0;
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            long j2 = this.N0;
            if (j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return 0L;
            }
            return j2 / this.E0;
        }
        return j;
    }

    private String getProgressText() {
        return androidx.media3.common.util.b.d0(this.y0, this.z0, this.O0);
    }

    private long getScrubberPosition() {
        if (this.f0.width() <= 0 || this.N0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return 0L;
        }
        return (this.h0.width() * this.N0) / this.f0.width();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void j() {
        v(false);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void k(ValueAnimator valueAnimator) {
        this.J0 = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidate(this.a);
    }

    public static int m(float f, int i) {
        return (int) (i / f);
    }

    public static boolean q(Drawable drawable, int i) {
        return androidx.media3.common.util.b.a >= 23 && drawable.setLayoutDirection(i);
    }

    @Override // androidx.media3.ui.b
    public void a(b.a aVar) {
        ii.e(aVar);
        this.B0.add(aVar);
    }

    @Override // android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        x();
    }

    public final void e(Canvas canvas) {
        int i;
        if (this.N0 <= 0) {
            return;
        }
        Rect rect = this.h0;
        int q = androidx.media3.common.util.b.q(rect.right, rect.left, this.f0.right);
        int centerY = this.h0.centerY();
        Drawable drawable = this.o0;
        if (drawable == null) {
            if (!this.L0 && !isFocused()) {
                i = isEnabled() ? this.t0 : this.u0;
            } else {
                i = this.v0;
            }
            canvas.drawCircle(q, centerY, (int) ((i * this.J0) / 2.0f), this.n0);
            return;
        }
        int intrinsicWidth = ((int) (drawable.getIntrinsicWidth() * this.J0)) / 2;
        int intrinsicHeight = ((int) (this.o0.getIntrinsicHeight() * this.J0)) / 2;
        this.o0.setBounds(q - intrinsicWidth, centerY - intrinsicHeight, q + intrinsicWidth, centerY + intrinsicHeight);
        this.o0.draw(canvas);
    }

    public final void f(Canvas canvas) {
        int min;
        int height = this.f0.height();
        int centerY = this.f0.centerY() - (height / 2);
        int i = height + centerY;
        if (this.N0 <= 0) {
            Rect rect = this.f0;
            canvas.drawRect(rect.left, centerY, rect.right, i, this.k0);
            return;
        }
        Rect rect2 = this.g0;
        int i2 = rect2.left;
        int i3 = rect2.right;
        int max = Math.max(Math.max(this.f0.left, i3), this.h0.right);
        int i4 = this.f0.right;
        if (max < i4) {
            canvas.drawRect(max, centerY, i4, i, this.k0);
        }
        int max2 = Math.max(i2, this.h0.right);
        if (i3 > max2) {
            canvas.drawRect(max2, centerY, i3, i, this.j0);
        }
        if (this.h0.width() > 0) {
            Rect rect3 = this.h0;
            canvas.drawRect(rect3.left, centerY, rect3.right, i, this.i0);
        }
        if (this.Q0 == 0) {
            return;
        }
        long[] jArr = (long[]) ii.e(this.R0);
        boolean[] zArr = (boolean[]) ii.e(this.S0);
        int i5 = this.s0 / 2;
        for (int i6 = 0; i6 < this.Q0; i6++) {
            long r = androidx.media3.common.util.b.r(jArr[i6], 0L, this.N0);
            Rect rect4 = this.f0;
            canvas.drawRect(rect4.left + Math.min(rect4.width() - this.s0, Math.max(0, ((int) ((this.f0.width() * r) / this.N0)) - i5)), centerY, min + this.s0, i, zArr[i6] ? this.m0 : this.l0);
        }
    }

    public void g(long j) {
        if (this.I0.isStarted()) {
            this.I0.cancel();
        }
        this.I0.setFloatValues(this.J0, Utils.FLOAT_EPSILON);
        this.I0.setDuration(j);
        this.I0.start();
    }

    @Override // androidx.media3.ui.b
    public long getPreferredUpdateDelay() {
        int m = m(this.D0, this.f0.width());
        if (m != 0) {
            long j = this.N0;
            if (j != 0 && j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return j / m;
            }
        }
        return Long.MAX_VALUE;
    }

    public void h(boolean z) {
        if (this.I0.isStarted()) {
            this.I0.cancel();
        }
        this.K0 = z;
        this.J0 = Utils.FLOAT_EPSILON;
        invalidate(this.a);
    }

    public final boolean i(float f, float f2) {
        return this.a.contains((int) f, (int) f2);
    }

    @Override // android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.o0;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    public final void l(float f) {
        Rect rect = this.h0;
        Rect rect2 = this.f0;
        rect.right = androidx.media3.common.util.b.q((int) f, rect2.left, rect2.right);
    }

    public final Point n(MotionEvent motionEvent) {
        this.C0.set((int) motionEvent.getX(), (int) motionEvent.getY());
        return this.C0;
    }

    public final boolean o(long j) {
        long j2 = this.N0;
        if (j2 <= 0) {
            return false;
        }
        long j3 = this.L0 ? this.M0 : this.O0;
        long r = androidx.media3.common.util.b.r(j3 + j, 0L, j2);
        if (r == j3) {
            return false;
        }
        if (!this.L0) {
            u(r);
        } else {
            y(r);
        }
        w();
        return true;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        canvas.save();
        f(canvas);
        e(canvas);
        canvas.restore();
    }

    @Override // android.view.View
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        if (!this.L0 || z) {
            return;
        }
        v(false);
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (accessibilityEvent.getEventType() == 4) {
            accessibilityEvent.getText().add(getProgressText());
        }
        accessibilityEvent.setClassName("android.widget.SeekBar");
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName("android.widget.SeekBar");
        accessibilityNodeInfo.setContentDescription(getProgressText());
        if (this.N0 <= 0) {
            return;
        }
        if (androidx.media3.common.util.b.a >= 21) {
            accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
            accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
            return;
        }
        accessibilityNodeInfo.addAction(4096);
        accessibilityNodeInfo.addAction(8192);
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x001a  */
    @Override // android.view.View, android.view.KeyEvent.Callback
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onKeyDown(int r5, android.view.KeyEvent r6) {
        /*
            r4 = this;
            boolean r0 = r4.isEnabled()
            if (r0 == 0) goto L30
            long r0 = r4.getPositionIncrement()
            r2 = 66
            r3 = 1
            if (r5 == r2) goto L27
            switch(r5) {
                case 21: goto L13;
                case 22: goto L14;
                case 23: goto L27;
                default: goto L12;
            }
        L12:
            goto L30
        L13:
            long r0 = -r0
        L14:
            boolean r0 = r4.o(r0)
            if (r0 == 0) goto L30
            java.lang.Runnable r5 = r4.A0
            r4.removeCallbacks(r5)
            java.lang.Runnable r5 = r4.A0
            r0 = 1000(0x3e8, double:4.94E-321)
            r4.postDelayed(r5, r0)
            return r3
        L27:
            boolean r0 = r4.L0
            if (r0 == 0) goto L30
            r5 = 0
            r4.v(r5)
            return r3
        L30:
            boolean r5 = super.onKeyDown(r5, r6)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.ui.DefaultTimeBar.onKeyDown(int, android.view.KeyEvent):boolean");
    }

    @Override // android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7 = i3 - i;
        int i8 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingRight = i7 - getPaddingRight();
        int i9 = this.K0 ? 0 : this.w0;
        if (this.r0 == 1) {
            i5 = (i8 - getPaddingBottom()) - this.q0;
            int i10 = this.p0;
            i6 = ((i8 - getPaddingBottom()) - i10) - Math.max(i9 - (i10 / 2), 0);
        } else {
            i5 = (i8 - this.q0) / 2;
            i6 = (i8 - this.p0) / 2;
        }
        this.a.set(paddingLeft, i5, paddingRight, this.q0 + i5);
        Rect rect = this.f0;
        Rect rect2 = this.a;
        rect.set(rect2.left + i9, i6, rect2.right - i9, this.p0 + i6);
        if (androidx.media3.common.util.b.a >= 29) {
            r(i7, i8);
        }
        w();
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 0) {
            size = this.q0;
        } else if (mode != 1073741824) {
            size = Math.min(this.q0, size);
        }
        setMeasuredDimension(View.MeasureSpec.getSize(i), size);
        x();
    }

    @Override // android.view.View
    public void onRtlPropertiesChanged(int i) {
        Drawable drawable = this.o0;
        if (drawable == null || !q(drawable, i)) {
            return;
        }
        invalidate();
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x0025, code lost:
        if (r3 != 3) goto L32;
     */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            boolean r0 = r7.isEnabled()
            r1 = 0
            if (r0 == 0) goto L76
            long r2 = r7.N0
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 > 0) goto L10
            goto L76
        L10:
            android.graphics.Point r0 = r7.n(r8)
            int r2 = r0.x
            int r0 = r0.y
            int r3 = r8.getAction()
            r4 = 1
            if (r3 == 0) goto L5d
            r5 = 3
            if (r3 == r4) goto L4e
            r6 = 2
            if (r3 == r6) goto L28
            if (r3 == r5) goto L4e
            goto L76
        L28:
            boolean r8 = r7.L0
            if (r8 == 0) goto L76
            int r8 = r7.x0
            if (r0 >= r8) goto L3a
            int r8 = r7.G0
            int r2 = r2 - r8
            int r2 = r2 / r5
            int r8 = r8 + r2
            float r8 = (float) r8
            r7.l(r8)
            goto L40
        L3a:
            r7.G0 = r2
            float r8 = (float) r2
            r7.l(r8)
        L40:
            long r0 = r7.getScrubberPosition()
            r7.y(r0)
            r7.w()
            r7.invalidate()
            return r4
        L4e:
            boolean r0 = r7.L0
            if (r0 == 0) goto L76
            int r8 = r8.getAction()
            if (r8 != r5) goto L59
            r1 = r4
        L59:
            r7.v(r1)
            return r4
        L5d:
            float r8 = (float) r2
            float r0 = (float) r0
            boolean r0 = r7.i(r8, r0)
            if (r0 == 0) goto L76
            r7.l(r8)
            long r0 = r7.getScrubberPosition()
            r7.u(r0)
            r7.w()
            r7.invalidate()
            return r4
        L76:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.ui.DefaultTimeBar.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public final boolean p(Drawable drawable) {
        return androidx.media3.common.util.b.a >= 23 && q(drawable, getLayoutDirection());
    }

    @Override // android.view.View
    public boolean performAccessibilityAction(int i, Bundle bundle) {
        if (super.performAccessibilityAction(i, bundle)) {
            return true;
        }
        if (this.N0 <= 0) {
            return false;
        }
        if (i == 8192) {
            if (o(-getPositionIncrement())) {
                v(false);
            }
        } else if (i != 4096) {
            return false;
        } else {
            if (o(getPositionIncrement())) {
                v(false);
            }
        }
        sendAccessibilityEvent(4);
        return true;
    }

    public final void r(int i, int i2) {
        Rect rect = this.H0;
        if (rect != null && rect.width() == i && this.H0.height() == i2) {
            return;
        }
        Rect rect2 = new Rect(0, 0, i, i2);
        this.H0 = rect2;
        setSystemGestureExclusionRects(Collections.singletonList(rect2));
    }

    public void s() {
        if (this.I0.isStarted()) {
            this.I0.cancel();
        }
        this.K0 = false;
        this.J0 = 1.0f;
        invalidate(this.a);
    }

    @Override // androidx.media3.ui.b
    public void setAdGroupTimesMs(long[] jArr, boolean[] zArr, int i) {
        ii.a(i == 0 || !(jArr == null || zArr == null));
        this.Q0 = i;
        this.R0 = jArr;
        this.S0 = zArr;
        w();
    }

    public void setAdMarkerColor(int i) {
        this.l0.setColor(i);
        invalidate(this.a);
    }

    public void setBufferedColor(int i) {
        this.j0.setColor(i);
        invalidate(this.a);
    }

    @Override // androidx.media3.ui.b
    public void setBufferedPosition(long j) {
        if (this.P0 == j) {
            return;
        }
        this.P0 = j;
        w();
    }

    @Override // androidx.media3.ui.b
    public void setDuration(long j) {
        if (this.N0 == j) {
            return;
        }
        this.N0 = j;
        if (this.L0 && j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            v(true);
        }
        w();
    }

    @Override // android.view.View, androidx.media3.ui.b
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (!this.L0 || z) {
            return;
        }
        v(true);
    }

    public void setKeyCountIncrement(int i) {
        ii.a(i > 0);
        this.E0 = i;
        this.F0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public void setKeyTimeIncrement(long j) {
        ii.a(j > 0);
        this.E0 = -1;
        this.F0 = j;
    }

    public void setPlayedAdMarkerColor(int i) {
        this.m0.setColor(i);
        invalidate(this.a);
    }

    public void setPlayedColor(int i) {
        this.i0.setColor(i);
        invalidate(this.a);
    }

    @Override // androidx.media3.ui.b
    public void setPosition(long j) {
        if (this.O0 == j) {
            return;
        }
        this.O0 = j;
        setContentDescription(getProgressText());
        w();
    }

    public void setScrubberColor(int i) {
        this.n0.setColor(i);
        invalidate(this.a);
    }

    public void setUnplayedColor(int i) {
        this.k0.setColor(i);
        invalidate(this.a);
    }

    public void t(long j) {
        if (this.I0.isStarted()) {
            this.I0.cancel();
        }
        this.K0 = false;
        this.I0.setFloatValues(this.J0, 1.0f);
        this.I0.setDuration(j);
        this.I0.start();
    }

    public final void u(long j) {
        this.M0 = j;
        this.L0 = true;
        setPressed(true);
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        Iterator<b.a> it = this.B0.iterator();
        while (it.hasNext()) {
            it.next().A(this, j);
        }
    }

    public final void v(boolean z) {
        removeCallbacks(this.A0);
        this.L0 = false;
        setPressed(false);
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(false);
        }
        invalidate();
        Iterator<b.a> it = this.B0.iterator();
        while (it.hasNext()) {
            it.next().O(this, this.M0, z);
        }
    }

    public final void w() {
        this.g0.set(this.f0);
        this.h0.set(this.f0);
        long j = this.L0 ? this.M0 : this.O0;
        if (this.N0 > 0) {
            Rect rect = this.g0;
            Rect rect2 = this.f0;
            rect.right = Math.min(rect2.left + ((int) ((this.f0.width() * this.P0) / this.N0)), rect2.right);
            int width = (int) ((this.f0.width() * j) / this.N0);
            Rect rect3 = this.h0;
            Rect rect4 = this.f0;
            rect3.right = Math.min(rect4.left + width, rect4.right);
        } else {
            Rect rect5 = this.g0;
            int i = this.f0.left;
            rect5.right = i;
            this.h0.right = i;
        }
        invalidate(this.a);
    }

    public final void x() {
        Drawable drawable = this.o0;
        if (drawable != null && drawable.isStateful() && this.o0.setState(getDrawableState())) {
            invalidate();
        }
    }

    public final void y(long j) {
        if (this.M0 == j) {
            return;
        }
        this.M0 = j;
        Iterator<b.a> it = this.B0.iterator();
        while (it.hasNext()) {
            it.next().K(this, j);
        }
    }

    public DefaultTimeBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DefaultTimeBar(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, attributeSet);
    }

    public DefaultTimeBar(Context context, AttributeSet attributeSet, int i, AttributeSet attributeSet2) {
        this(context, attributeSet, i, attributeSet2, 0);
    }

    public DefaultTimeBar(Context context, AttributeSet attributeSet, int i, AttributeSet attributeSet2, int i2) {
        super(context, attributeSet, i);
        this.a = new Rect();
        this.f0 = new Rect();
        this.g0 = new Rect();
        this.h0 = new Rect();
        Paint paint = new Paint();
        this.i0 = paint;
        Paint paint2 = new Paint();
        this.j0 = paint2;
        Paint paint3 = new Paint();
        this.k0 = paint3;
        Paint paint4 = new Paint();
        this.l0 = paint4;
        Paint paint5 = new Paint();
        this.m0 = paint5;
        Paint paint6 = new Paint();
        this.n0 = paint6;
        paint6.setAntiAlias(true);
        this.B0 = new CopyOnWriteArraySet<>();
        this.C0 = new Point();
        float f = context.getResources().getDisplayMetrics().density;
        this.D0 = f;
        this.x0 = d(f, -50);
        int d = d(f, 4);
        int d2 = d(f, 26);
        int d3 = d(f, 4);
        int d4 = d(f, 12);
        int d5 = d(f, 0);
        int d6 = d(f, 16);
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, b33.DefaultTimeBar, i, i2);
            try {
                Drawable drawable = obtainStyledAttributes.getDrawable(b33.DefaultTimeBar_scrubber_drawable);
                this.o0 = drawable;
                if (drawable != null) {
                    p(drawable);
                    d2 = Math.max(drawable.getMinimumHeight(), d2);
                }
                this.p0 = obtainStyledAttributes.getDimensionPixelSize(b33.DefaultTimeBar_bar_height, d);
                this.q0 = obtainStyledAttributes.getDimensionPixelSize(b33.DefaultTimeBar_touch_target_height, d2);
                this.r0 = obtainStyledAttributes.getInt(b33.DefaultTimeBar_bar_gravity, 0);
                this.s0 = obtainStyledAttributes.getDimensionPixelSize(b33.DefaultTimeBar_ad_marker_width, d3);
                this.t0 = obtainStyledAttributes.getDimensionPixelSize(b33.DefaultTimeBar_scrubber_enabled_size, d4);
                this.u0 = obtainStyledAttributes.getDimensionPixelSize(b33.DefaultTimeBar_scrubber_disabled_size, d5);
                this.v0 = obtainStyledAttributes.getDimensionPixelSize(b33.DefaultTimeBar_scrubber_dragged_size, d6);
                int i3 = obtainStyledAttributes.getInt(b33.DefaultTimeBar_played_color, -1);
                int i4 = obtainStyledAttributes.getInt(b33.DefaultTimeBar_scrubber_color, -1);
                int i5 = obtainStyledAttributes.getInt(b33.DefaultTimeBar_buffered_color, -855638017);
                int i6 = obtainStyledAttributes.getInt(b33.DefaultTimeBar_unplayed_color, 872415231);
                int i7 = obtainStyledAttributes.getInt(b33.DefaultTimeBar_ad_marker_color, -1291845888);
                int i8 = obtainStyledAttributes.getInt(b33.DefaultTimeBar_played_ad_marker_color, 872414976);
                paint.setColor(i3);
                paint6.setColor(i4);
                paint2.setColor(i5);
                paint3.setColor(i6);
                paint4.setColor(i7);
                paint5.setColor(i8);
            } finally {
                obtainStyledAttributes.recycle();
            }
        } else {
            this.p0 = d;
            this.q0 = d2;
            this.r0 = 0;
            this.s0 = d3;
            this.t0 = d4;
            this.u0 = d5;
            this.v0 = d6;
            paint.setColor(-1);
            paint6.setColor(-1);
            paint2.setColor(-855638017);
            paint3.setColor(872415231);
            paint4.setColor(-1291845888);
            paint5.setColor(872414976);
            this.o0 = null;
        }
        StringBuilder sb = new StringBuilder();
        this.y0 = sb;
        this.z0 = new Formatter(sb, Locale.getDefault());
        this.A0 = new Runnable() { // from class: yk0
            @Override // java.lang.Runnable
            public final void run() {
                DefaultTimeBar.this.j();
            }
        };
        Drawable drawable2 = this.o0;
        if (drawable2 != null) {
            this.w0 = (drawable2.getMinimumWidth() + 1) / 2;
        } else {
            this.w0 = (Math.max(this.u0, Math.max(this.t0, this.v0)) + 1) / 2;
        }
        this.J0 = 1.0f;
        ValueAnimator valueAnimator = new ValueAnimator();
        this.I0 = valueAnimator;
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: xk0
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                DefaultTimeBar.this.k(valueAnimator2);
            }
        });
        this.N0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.F0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.E0 = 20;
        setFocusable(true);
        if (getImportantForAccessibility() == 0) {
            setImportantForAccessibility(1);
        }
    }
}
