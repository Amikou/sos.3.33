package androidx.media3.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.ui.LegacyPlayerControlView;
import androidx.media3.ui.b;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public class LegacyPlayerControlView extends FrameLayout {
    public final Drawable A0;
    public final String B0;
    public final String C0;
    public final String D0;
    public final Drawable E0;
    public final Drawable F0;
    public final float G0;
    public final float H0;
    public final String I0;
    public final String J0;
    public q K0;
    public d L0;
    public boolean M0;
    public boolean N0;
    public boolean O0;
    public boolean P0;
    public int Q0;
    public int R0;
    public int S0;
    public boolean T0;
    public boolean U0;
    public boolean V0;
    public boolean W0;
    public boolean X0;
    public long Y0;
    public long[] Z0;
    public final c a;
    public boolean[] a1;
    public long[] b1;
    public boolean[] c1;
    public long d1;
    public long e1;
    public final CopyOnWriteArrayList<e> f0;
    public long f1;
    public final View g0;
    public final View h0;
    public final View i0;
    public final View j0;
    public final View k0;
    public final View l0;
    public final ImageView m0;
    public final ImageView n0;
    public final View o0;
    public final TextView p0;
    public final TextView q0;
    public final androidx.media3.ui.b r0;
    public final StringBuilder s0;
    public final Formatter t0;
    public final u.b u0;
    public final u.c v0;
    public final Runnable w0;
    public final Runnable x0;
    public final Drawable y0;
    public final Drawable z0;

    /* loaded from: classes.dex */
    public static final class b {
        public static boolean a(View view) {
            return view.isAccessibilityFocused();
        }
    }

    /* loaded from: classes.dex */
    public final class c implements q.d, b.a, View.OnClickListener {
        public c() {
        }

        @Override // androidx.media3.ui.b.a
        public void A(androidx.media3.ui.b bVar, long j) {
            LegacyPlayerControlView.this.P0 = true;
            if (LegacyPlayerControlView.this.q0 != null) {
                LegacyPlayerControlView.this.q0.setText(androidx.media3.common.util.b.d0(LegacyPlayerControlView.this.s0, LegacyPlayerControlView.this.t0, j));
            }
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void D(int i) {
            nr2.p(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void E(boolean z) {
            nr2.i(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void F(int i) {
            nr2.t(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void H(boolean z) {
            nr2.g(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void I() {
            nr2.x(this);
        }

        @Override // androidx.media3.common.q.d
        public void J(q qVar, q.c cVar) {
            if (cVar.b(4, 5)) {
                LegacyPlayerControlView.this.Q();
            }
            if (cVar.b(4, 5, 7)) {
                LegacyPlayerControlView.this.R();
            }
            if (cVar.a(8)) {
                LegacyPlayerControlView.this.S();
            }
            if (cVar.a(9)) {
                LegacyPlayerControlView.this.T();
            }
            if (cVar.b(8, 9, 11, 0, 13)) {
                LegacyPlayerControlView.this.P();
            }
            if (cVar.b(11, 0)) {
                LegacyPlayerControlView.this.U();
            }
        }

        @Override // androidx.media3.ui.b.a
        public void K(androidx.media3.ui.b bVar, long j) {
            if (LegacyPlayerControlView.this.q0 != null) {
                LegacyPlayerControlView.this.q0.setText(androidx.media3.common.util.b.d0(LegacyPlayerControlView.this.s0, LegacyPlayerControlView.this.t0, j));
            }
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void L(u uVar, int i) {
            nr2.B(this, uVar, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void N(boolean z) {
            nr2.y(this, z);
        }

        @Override // androidx.media3.ui.b.a
        public void O(androidx.media3.ui.b bVar, long j, boolean z) {
            LegacyPlayerControlView.this.P0 = false;
            if (z || LegacyPlayerControlView.this.K0 == null) {
                return;
            }
            LegacyPlayerControlView legacyPlayerControlView = LegacyPlayerControlView.this;
            legacyPlayerControlView.L(legacyPlayerControlView.K0, j);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Q(int i, boolean z) {
            nr2.e(this, i, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void R(boolean z, int i) {
            nr2.s(this, z, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void T(n nVar) {
            nr2.k(this, nVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void V(x xVar) {
            nr2.C(this, xVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void X() {
            nr2.v(this);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Y(y yVar) {
            nr2.D(this, yVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Z(h hVar) {
            nr2.d(this, hVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void a0(m mVar, int i) {
            nr2.j(this, mVar, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void b(boolean z) {
            nr2.z(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void c0(PlaybackException playbackException) {
            nr2.r(this, playbackException);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void h0(PlaybackException playbackException) {
            nr2.q(this, playbackException);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void i(int i) {
            nr2.o(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void k(z zVar) {
            nr2.E(this, zVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void k0(int i, int i2) {
            nr2.A(this, i, i2);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void l0(q.b bVar) {
            nr2.a(this, bVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void m0(q.e eVar, q.e eVar2, int i) {
            nr2.u(this, eVar, eVar2, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void n(p pVar) {
            nr2.n(this, pVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void o(nb0 nb0Var) {
            nr2.b(this, nb0Var);
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            q qVar = LegacyPlayerControlView.this.K0;
            if (qVar == null) {
                return;
            }
            if (LegacyPlayerControlView.this.h0 != view) {
                if (LegacyPlayerControlView.this.g0 != view) {
                    if (LegacyPlayerControlView.this.k0 != view) {
                        if (LegacyPlayerControlView.this.l0 != view) {
                            if (LegacyPlayerControlView.this.i0 == view) {
                                LegacyPlayerControlView.this.B(qVar);
                                return;
                            } else if (LegacyPlayerControlView.this.j0 == view) {
                                LegacyPlayerControlView.this.A(qVar);
                                return;
                            } else if (LegacyPlayerControlView.this.m0 != view) {
                                if (LegacyPlayerControlView.this.n0 == view) {
                                    qVar.l(!qVar.U());
                                    return;
                                }
                                return;
                            } else {
                                qVar.K(y63.a(qVar.Q(), LegacyPlayerControlView.this.S0));
                                return;
                            }
                        }
                        qVar.a0();
                        return;
                    } else if (qVar.B() != 4) {
                        qVar.Y();
                        return;
                    } else {
                        return;
                    }
                }
                qVar.u();
                return;
            }
            qVar.X();
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void p0(boolean z) {
            nr2.h(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void s(int i) {
            nr2.w(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void t(Metadata metadata) {
            nr2.l(this, metadata);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void u(List list) {
            nr2.c(this, list);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void y(boolean z, int i) {
            nr2.m(this, z, i);
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a(long j, long j2);
    }

    /* loaded from: classes.dex */
    public interface e {
        void A(int i);
    }

    static {
        f62.a("media3.ui");
    }

    public LegacyPlayerControlView(Context context) {
        this(context, null);
    }

    public static int D(TypedArray typedArray, int i) {
        return typedArray.getInt(b33.LegacyPlayerControlView_repeat_toggle_modes, i);
    }

    @SuppressLint({"InlinedApi"})
    public static boolean G(int i) {
        return i == 90 || i == 89 || i == 85 || i == 79 || i == 126 || i == 127 || i == 87 || i == 88;
    }

    public static boolean y(u uVar, u.c cVar) {
        if (uVar.p() > 100) {
            return false;
        }
        int p = uVar.p();
        for (int i = 0; i < p; i++) {
            if (uVar.n(i, cVar).r0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return false;
            }
        }
        return true;
    }

    public final void A(q qVar) {
        qVar.c();
    }

    public final void B(q qVar) {
        int B = qVar.B();
        if (B == 1) {
            qVar.d();
        } else if (B == 4) {
            K(qVar, qVar.I(), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }
        qVar.f();
    }

    public final void C(q qVar) {
        int B = qVar.B();
        if (B != 1 && B != 4 && qVar.k()) {
            A(qVar);
        } else {
            B(qVar);
        }
    }

    public void E() {
        if (H()) {
            setVisibility(8);
            Iterator<e> it = this.f0.iterator();
            while (it.hasNext()) {
                it.next().A(getVisibility());
            }
            removeCallbacks(this.w0);
            removeCallbacks(this.x0);
            this.Y0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
    }

    public final void F() {
        removeCallbacks(this.x0);
        if (this.Q0 > 0) {
            long uptimeMillis = SystemClock.uptimeMillis();
            int i = this.Q0;
            this.Y0 = uptimeMillis + i;
            if (this.M0) {
                postDelayed(this.x0, i);
                return;
            }
            return;
        }
        this.Y0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public boolean H() {
        return getVisibility() == 0;
    }

    public final void I() {
        View view;
        View view2;
        boolean M = M();
        if (!M && (view2 = this.i0) != null) {
            view2.sendAccessibilityEvent(8);
        } else if (!M || (view = this.j0) == null) {
        } else {
            view.sendAccessibilityEvent(8);
        }
    }

    public final void J() {
        View view;
        View view2;
        boolean M = M();
        if (!M && (view2 = this.i0) != null) {
            view2.requestFocus();
        } else if (!M || (view = this.j0) == null) {
        } else {
            view.requestFocus();
        }
    }

    public final void K(q qVar, int i, long j) {
        qVar.i(i, j);
    }

    public final void L(q qVar, long j) {
        int I;
        u S = qVar.S();
        if (this.O0 && !S.q()) {
            int p = S.p();
            I = 0;
            while (true) {
                long g = S.n(I, this.v0).g();
                if (j < g) {
                    break;
                } else if (I == p - 1) {
                    j = g;
                    break;
                } else {
                    j -= g;
                    I++;
                }
            }
        } else {
            I = qVar.I();
        }
        K(qVar, I, j);
        R();
    }

    public final boolean M() {
        q qVar = this.K0;
        return (qVar == null || qVar.B() == 4 || this.K0.B() == 1 || !this.K0.k()) ? false : true;
    }

    public final void N() {
        Q();
        P();
        S();
        T();
        U();
    }

    public final void O(boolean z, boolean z2, View view) {
        if (view == null) {
            return;
        }
        view.setEnabled(z2);
        view.setAlpha(z2 ? this.G0 : this.H0);
        view.setVisibility(z ? 0 : 8);
    }

    public final void P() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (H() && this.M0) {
            q qVar = this.K0;
            boolean z5 = false;
            if (qVar != null) {
                boolean J = qVar.J(5);
                boolean J2 = qVar.J(7);
                z3 = qVar.J(11);
                z4 = qVar.J(12);
                z = qVar.J(9);
                z2 = J;
                z5 = J2;
            } else {
                z = false;
                z2 = false;
                z3 = false;
                z4 = false;
            }
            O(this.V0, z5, this.g0);
            O(this.T0, z3, this.l0);
            O(this.U0, z4, this.k0);
            O(this.W0, z, this.h0);
            androidx.media3.ui.b bVar = this.r0;
            if (bVar != null) {
                bVar.setEnabled(z2);
            }
        }
    }

    public final void Q() {
        boolean z;
        boolean z2;
        boolean z3;
        if (H() && this.M0) {
            boolean M = M();
            View view = this.i0;
            boolean z4 = true;
            if (view != null) {
                z = (M && view.isFocused()) | false;
                if (androidx.media3.common.util.b.a < 21) {
                    z3 = z;
                } else {
                    z3 = M && b.a(this.i0);
                }
                z2 = z3 | false;
                this.i0.setVisibility(M ? 8 : 0);
            } else {
                z = false;
                z2 = false;
            }
            View view2 = this.j0;
            if (view2 != null) {
                z |= !M && view2.isFocused();
                if (androidx.media3.common.util.b.a < 21) {
                    z4 = z;
                } else if (M || !b.a(this.j0)) {
                    z4 = false;
                }
                z2 |= z4;
                this.j0.setVisibility(M ? 0 : 8);
            }
            if (z) {
                J();
            }
            if (z2) {
                I();
            }
        }
    }

    public final void R() {
        long j;
        float f;
        if (H() && this.M0) {
            q qVar = this.K0;
            long j2 = 0;
            if (qVar != null) {
                j2 = this.d1 + qVar.y();
                j = this.d1 + qVar.W();
            } else {
                j = 0;
            }
            boolean z = j2 != this.e1;
            boolean z2 = j != this.f1;
            this.e1 = j2;
            this.f1 = j;
            TextView textView = this.q0;
            if (textView != null && !this.P0 && z) {
                textView.setText(androidx.media3.common.util.b.d0(this.s0, this.t0, j2));
            }
            androidx.media3.ui.b bVar = this.r0;
            if (bVar != null) {
                bVar.setPosition(j2);
                this.r0.setBufferedPosition(j);
            }
            d dVar = this.L0;
            if (dVar != null && (z || z2)) {
                dVar.a(j2, j);
            }
            removeCallbacks(this.w0);
            int B = qVar == null ? 1 : qVar.B();
            if (qVar == null || !qVar.E()) {
                if (B == 4 || B == 1) {
                    return;
                }
                postDelayed(this.w0, 1000L);
                return;
            }
            androidx.media3.ui.b bVar2 = this.r0;
            long min = Math.min(bVar2 != null ? bVar2.getPreferredUpdateDelay() : 1000L, 1000 - (j2 % 1000));
            postDelayed(this.w0, androidx.media3.common.util.b.r(qVar.e().a > Utils.FLOAT_EPSILON ? ((float) min) / f : 1000L, this.R0, 1000L));
        }
    }

    public final void S() {
        ImageView imageView;
        if (H() && this.M0 && (imageView = this.m0) != null) {
            if (this.S0 == 0) {
                O(false, false, imageView);
                return;
            }
            q qVar = this.K0;
            if (qVar == null) {
                O(true, false, imageView);
                this.m0.setImageDrawable(this.y0);
                this.m0.setContentDescription(this.B0);
                return;
            }
            O(true, true, imageView);
            int Q = qVar.Q();
            if (Q == 0) {
                this.m0.setImageDrawable(this.y0);
                this.m0.setContentDescription(this.B0);
            } else if (Q == 1) {
                this.m0.setImageDrawable(this.z0);
                this.m0.setContentDescription(this.C0);
            } else if (Q == 2) {
                this.m0.setImageDrawable(this.A0);
                this.m0.setContentDescription(this.D0);
            }
            this.m0.setVisibility(0);
        }
    }

    public final void T() {
        ImageView imageView;
        String str;
        if (H() && this.M0 && (imageView = this.n0) != null) {
            q qVar = this.K0;
            if (!this.X0) {
                O(false, false, imageView);
            } else if (qVar == null) {
                O(true, false, imageView);
                this.n0.setImageDrawable(this.F0);
                this.n0.setContentDescription(this.J0);
            } else {
                O(true, true, imageView);
                this.n0.setImageDrawable(qVar.U() ? this.E0 : this.F0);
                ImageView imageView2 = this.n0;
                if (qVar.U()) {
                    str = this.I0;
                } else {
                    str = this.J0;
                }
                imageView2.setContentDescription(str);
            }
        }
    }

    public final void U() {
        int i;
        u.c cVar;
        q qVar = this.K0;
        if (qVar == null) {
            return;
        }
        boolean z = true;
        this.O0 = this.N0 && y(qVar.S(), this.v0);
        long j = 0;
        this.d1 = 0L;
        u S = qVar.S();
        if (S.q()) {
            i = 0;
        } else {
            int I = qVar.I();
            boolean z2 = this.O0;
            int i2 = z2 ? 0 : I;
            int p = z2 ? S.p() - 1 : I;
            long j2 = 0;
            i = 0;
            while (true) {
                if (i2 > p) {
                    break;
                }
                if (i2 == I) {
                    this.d1 = androidx.media3.common.util.b.U0(j2);
                }
                S.n(i2, this.v0);
                u.c cVar2 = this.v0;
                if (cVar2.r0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    ii.g(this.O0 ^ z);
                    break;
                }
                int i3 = cVar2.s0;
                while (true) {
                    cVar = this.v0;
                    if (i3 <= cVar.t0) {
                        S.f(i3, this.u0);
                        int e2 = this.u0.e();
                        for (int q = this.u0.q(); q < e2; q++) {
                            long h = this.u0.h(q);
                            if (h == Long.MIN_VALUE) {
                                long j3 = this.u0.h0;
                                if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                                    h = j3;
                                }
                            }
                            long p2 = h + this.u0.p();
                            if (p2 >= 0) {
                                long[] jArr = this.Z0;
                                if (i == jArr.length) {
                                    int length = jArr.length == 0 ? 1 : jArr.length * 2;
                                    this.Z0 = Arrays.copyOf(jArr, length);
                                    this.a1 = Arrays.copyOf(this.a1, length);
                                }
                                this.Z0[i] = androidx.media3.common.util.b.U0(j2 + p2);
                                this.a1[i] = this.u0.r(q);
                                i++;
                            }
                        }
                        i3++;
                    }
                }
                j2 += cVar.r0;
                i2++;
                z = true;
            }
            j = j2;
        }
        long U0 = androidx.media3.common.util.b.U0(j);
        TextView textView = this.p0;
        if (textView != null) {
            textView.setText(androidx.media3.common.util.b.d0(this.s0, this.t0, U0));
        }
        androidx.media3.ui.b bVar = this.r0;
        if (bVar != null) {
            bVar.setDuration(U0);
            int length2 = this.b1.length;
            int i4 = i + length2;
            long[] jArr2 = this.Z0;
            if (i4 > jArr2.length) {
                this.Z0 = Arrays.copyOf(jArr2, i4);
                this.a1 = Arrays.copyOf(this.a1, i4);
            }
            System.arraycopy(this.b1, 0, this.Z0, i, length2);
            System.arraycopy(this.c1, 0, this.a1, i, length2);
            this.r0.setAdGroupTimesMs(this.Z0, this.a1, i4);
        }
        R();
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return z(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            removeCallbacks(this.x0);
        } else if (motionEvent.getAction() == 1) {
            F();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public q getPlayer() {
        return this.K0;
    }

    public int getRepeatToggleModes() {
        return this.S0;
    }

    public boolean getShowShuffleButton() {
        return this.X0;
    }

    public int getShowTimeoutMs() {
        return this.Q0;
    }

    public boolean getShowVrButton() {
        View view = this.o0;
        return view != null && view.getVisibility() == 0;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.M0 = true;
        long j = this.Y0;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            long uptimeMillis = j - SystemClock.uptimeMillis();
            if (uptimeMillis <= 0) {
                E();
            } else {
                postDelayed(this.x0, uptimeMillis);
            }
        } else if (H()) {
            F();
        }
        N();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.M0 = false;
        removeCallbacks(this.w0);
        removeCallbacks(this.x0);
    }

    public void setExtraAdGroupMarkers(long[] jArr, boolean[] zArr) {
        if (jArr == null) {
            this.b1 = new long[0];
            this.c1 = new boolean[0];
        } else {
            boolean[] zArr2 = (boolean[]) ii.e(zArr);
            ii.a(jArr.length == zArr2.length);
            this.b1 = jArr;
            this.c1 = zArr2;
        }
        U();
    }

    public void setPlayer(q qVar) {
        boolean z = true;
        ii.g(Looper.myLooper() == Looper.getMainLooper());
        if (qVar != null && qVar.T() != Looper.getMainLooper()) {
            z = false;
        }
        ii.a(z);
        q qVar2 = this.K0;
        if (qVar2 == qVar) {
            return;
        }
        if (qVar2 != null) {
            qVar2.G(this.a);
        }
        this.K0 = qVar;
        if (qVar != null) {
            qVar.O(this.a);
        }
        N();
    }

    public void setProgressUpdateListener(d dVar) {
        this.L0 = dVar;
    }

    public void setRepeatToggleModes(int i) {
        this.S0 = i;
        q qVar = this.K0;
        if (qVar != null) {
            int Q = qVar.Q();
            if (i == 0 && Q != 0) {
                this.K0.K(0);
            } else if (i == 1 && Q == 2) {
                this.K0.K(1);
            } else if (i == 2 && Q == 1) {
                this.K0.K(2);
            }
        }
        S();
    }

    public void setShowFastForwardButton(boolean z) {
        this.U0 = z;
        P();
    }

    public void setShowMultiWindowTimeBar(boolean z) {
        this.N0 = z;
        U();
    }

    public void setShowNextButton(boolean z) {
        this.W0 = z;
        P();
    }

    public void setShowPreviousButton(boolean z) {
        this.V0 = z;
        P();
    }

    public void setShowRewindButton(boolean z) {
        this.T0 = z;
        P();
    }

    public void setShowShuffleButton(boolean z) {
        this.X0 = z;
        T();
    }

    public void setShowTimeoutMs(int i) {
        this.Q0 = i;
        if (H()) {
            F();
        }
    }

    public void setShowVrButton(boolean z) {
        View view = this.o0;
        if (view != null) {
            view.setVisibility(z ? 0 : 8);
        }
    }

    public void setTimeBarMinUpdateInterval(int i) {
        this.R0 = androidx.media3.common.util.b.q(i, 16, 1000);
    }

    public void setVrButtonListener(View.OnClickListener onClickListener) {
        View view = this.o0;
        if (view != null) {
            view.setOnClickListener(onClickListener);
            O(getShowVrButton(), onClickListener != null, this.o0);
        }
    }

    public boolean z(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        q qVar = this.K0;
        if (qVar == null || !G(keyCode)) {
            return false;
        }
        if (keyEvent.getAction() == 0) {
            if (keyCode == 90) {
                if (qVar.B() != 4) {
                    qVar.Y();
                    return true;
                }
                return true;
            } else if (keyCode == 89) {
                qVar.a0();
                return true;
            } else if (keyEvent.getRepeatCount() == 0) {
                if (keyCode == 79 || keyCode == 85) {
                    C(qVar);
                    return true;
                } else if (keyCode == 87) {
                    qVar.X();
                    return true;
                } else if (keyCode == 88) {
                    qVar.u();
                    return true;
                } else if (keyCode == 126) {
                    B(qVar);
                    return true;
                } else if (keyCode != 127) {
                    return true;
                } else {
                    A(qVar);
                    return true;
                }
            } else {
                return true;
            }
        }
        return true;
    }

    public LegacyPlayerControlView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public LegacyPlayerControlView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, attributeSet);
    }

    public LegacyPlayerControlView(Context context, AttributeSet attributeSet, int i, AttributeSet attributeSet2) {
        super(context, attributeSet, i);
        int i2 = w03.exo_legacy_player_control_view;
        this.Q0 = 5000;
        this.S0 = 0;
        this.R0 = 200;
        this.Y0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.T0 = true;
        this.U0 = true;
        this.V0 = true;
        this.W0 = true;
        this.X0 = false;
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, b33.LegacyPlayerControlView, i, 0);
            try {
                this.Q0 = obtainStyledAttributes.getInt(b33.LegacyPlayerControlView_show_timeout, this.Q0);
                i2 = obtainStyledAttributes.getResourceId(b33.LegacyPlayerControlView_controller_layout_id, i2);
                this.S0 = D(obtainStyledAttributes, this.S0);
                this.T0 = obtainStyledAttributes.getBoolean(b33.LegacyPlayerControlView_show_rewind_button, this.T0);
                this.U0 = obtainStyledAttributes.getBoolean(b33.LegacyPlayerControlView_show_fastforward_button, this.U0);
                this.V0 = obtainStyledAttributes.getBoolean(b33.LegacyPlayerControlView_show_previous_button, this.V0);
                this.W0 = obtainStyledAttributes.getBoolean(b33.LegacyPlayerControlView_show_next_button, this.W0);
                this.X0 = obtainStyledAttributes.getBoolean(b33.LegacyPlayerControlView_show_shuffle_button, this.X0);
                setTimeBarMinUpdateInterval(obtainStyledAttributes.getInt(b33.LegacyPlayerControlView_time_bar_min_update_interval, this.R0));
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.f0 = new CopyOnWriteArrayList<>();
        this.u0 = new u.b();
        this.v0 = new u.c();
        StringBuilder sb = new StringBuilder();
        this.s0 = sb;
        this.t0 = new Formatter(sb, Locale.getDefault());
        this.Z0 = new long[0];
        this.a1 = new boolean[0];
        this.b1 = new long[0];
        this.c1 = new boolean[0];
        c cVar = new c();
        this.a = cVar;
        this.w0 = new Runnable() { // from class: fz1
            @Override // java.lang.Runnable
            public final void run() {
                LegacyPlayerControlView.this.R();
            }
        };
        this.x0 = new Runnable() { // from class: ez1
            @Override // java.lang.Runnable
            public final void run() {
                LegacyPlayerControlView.this.E();
            }
        };
        LayoutInflater.from(context).inflate(i2, this);
        setDescendantFocusability(262144);
        int i3 = n03.exo_progress;
        androidx.media3.ui.b bVar = (androidx.media3.ui.b) findViewById(i3);
        View findViewById = findViewById(n03.exo_progress_placeholder);
        if (bVar != null) {
            this.r0 = bVar;
        } else if (findViewById != null) {
            DefaultTimeBar defaultTimeBar = new DefaultTimeBar(context, null, 0, attributeSet2);
            defaultTimeBar.setId(i3);
            defaultTimeBar.setLayoutParams(findViewById.getLayoutParams());
            ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
            int indexOfChild = viewGroup.indexOfChild(findViewById);
            viewGroup.removeView(findViewById);
            viewGroup.addView(defaultTimeBar, indexOfChild);
            this.r0 = defaultTimeBar;
        } else {
            this.r0 = null;
        }
        this.p0 = (TextView) findViewById(n03.exo_duration);
        this.q0 = (TextView) findViewById(n03.exo_position);
        androidx.media3.ui.b bVar2 = this.r0;
        if (bVar2 != null) {
            bVar2.a(cVar);
        }
        View findViewById2 = findViewById(n03.exo_play);
        this.i0 = findViewById2;
        if (findViewById2 != null) {
            findViewById2.setOnClickListener(cVar);
        }
        View findViewById3 = findViewById(n03.exo_pause);
        this.j0 = findViewById3;
        if (findViewById3 != null) {
            findViewById3.setOnClickListener(cVar);
        }
        View findViewById4 = findViewById(n03.exo_prev);
        this.g0 = findViewById4;
        if (findViewById4 != null) {
            findViewById4.setOnClickListener(cVar);
        }
        View findViewById5 = findViewById(n03.exo_next);
        this.h0 = findViewById5;
        if (findViewById5 != null) {
            findViewById5.setOnClickListener(cVar);
        }
        View findViewById6 = findViewById(n03.exo_rew);
        this.l0 = findViewById6;
        if (findViewById6 != null) {
            findViewById6.setOnClickListener(cVar);
        }
        View findViewById7 = findViewById(n03.exo_ffwd);
        this.k0 = findViewById7;
        if (findViewById7 != null) {
            findViewById7.setOnClickListener(cVar);
        }
        ImageView imageView = (ImageView) findViewById(n03.exo_repeat_toggle);
        this.m0 = imageView;
        if (imageView != null) {
            imageView.setOnClickListener(cVar);
        }
        ImageView imageView2 = (ImageView) findViewById(n03.exo_shuffle);
        this.n0 = imageView2;
        if (imageView2 != null) {
            imageView2.setOnClickListener(cVar);
        }
        View findViewById8 = findViewById(n03.exo_vr);
        this.o0 = findViewById8;
        setShowVrButton(false);
        O(false, false, findViewById8);
        Resources resources = context.getResources();
        this.G0 = resources.getInteger(u03.exo_media_button_opacity_percentage_enabled) / 100.0f;
        this.H0 = resources.getInteger(u03.exo_media_button_opacity_percentage_disabled) / 100.0f;
        this.y0 = resources.getDrawable(oz2.exo_legacy_controls_repeat_off);
        this.z0 = resources.getDrawable(oz2.exo_legacy_controls_repeat_one);
        this.A0 = resources.getDrawable(oz2.exo_legacy_controls_repeat_all);
        this.E0 = resources.getDrawable(oz2.exo_legacy_controls_shuffle_on);
        this.F0 = resources.getDrawable(oz2.exo_legacy_controls_shuffle_off);
        this.B0 = resources.getString(m13.exo_controls_repeat_off_description);
        this.C0 = resources.getString(m13.exo_controls_repeat_one_description);
        this.D0 = resources.getString(m13.exo_controls_repeat_all_description);
        this.I0 = resources.getString(m13.exo_controls_shuffle_on_description);
        this.J0 = resources.getString(m13.exo_controls_shuffle_off_description);
        this.e1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.f1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
