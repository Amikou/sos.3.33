package androidx.media3.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.v;
import androidx.media3.common.w;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.ui.PlayerControlView;
import androidx.media3.ui.b;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public class PlayerControlView extends FrameLayout {
    public static final float[] z1;
    public final Drawable A0;
    public final String B0;
    public final String C0;
    public final String D0;
    public final Drawable E0;
    public final Drawable F0;
    public final float G0;
    public final float H0;
    public final String I0;
    public final String J0;
    public final Drawable K0;
    public final Drawable L0;
    public final String M0;
    public final String N0;
    public final Drawable O0;
    public final Drawable P0;
    public final String Q0;
    public final String R0;
    public q S0;
    public f T0;
    public d U0;
    public boolean V0;
    public boolean W0;
    public boolean X0;
    public boolean Y0;
    public boolean Z0;
    public final c a;
    public int a1;
    public int b1;
    public int c1;
    public long[] d1;
    public boolean[] e1;
    public final CopyOnWriteArrayList<m> f0;
    public long[] f1;
    public final View g0;
    public boolean[] g1;
    public final View h0;
    public long h1;
    public final View i0;
    public js2 i1;
    public final View j0;
    public Resources j1;
    public final View k0;
    public RecyclerView k1;
    public final TextView l0;
    public h l1;
    public final TextView m0;
    public e m1;
    public final ImageView n0;
    public PopupWindow n1;
    public final ImageView o0;
    public boolean o1;
    public final View p0;
    public int p1;
    public final TextView q0;
    public j q1;
    public final TextView r0;
    public b r1;
    public final androidx.media3.ui.b s0;
    public d84 s1;
    public final StringBuilder t0;
    public ImageView t1;
    public final Formatter u0;
    public ImageView u1;
    public final u.b v0;
    public ImageView v1;
    public final u.c w0;
    public View w1;
    public final Runnable x0;
    public View x1;
    public final Drawable y0;
    public View y1;
    public final Drawable z0;

    /* loaded from: classes.dex */
    public final class b extends l {
        public b() {
            super();
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void k(View view) {
            if (PlayerControlView.this.S0 == null) {
                return;
            }
            ((q) androidx.media3.common.util.b.j(PlayerControlView.this.S0)).L(PlayerControlView.this.S0.V().a().B(1).I(1, false).A());
            PlayerControlView.this.l1.c(1, PlayerControlView.this.getResources().getString(m13.exo_track_selection_auto));
            PlayerControlView.this.n1.dismiss();
        }

        @Override // androidx.media3.ui.PlayerControlView.l
        public void e(i iVar) {
            iVar.a.setText(m13.exo_track_selection_auto);
            iVar.b.setVisibility(i(((q) ii.e(PlayerControlView.this.S0)).V()) ? 4 : 0);
            iVar.itemView.setOnClickListener(new View.OnClickListener() { // from class: rr2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    PlayerControlView.b.this.k(view);
                }
            });
        }

        @Override // androidx.media3.ui.PlayerControlView.l
        public void g(String str) {
            PlayerControlView.this.l1.c(1, str);
        }

        public final boolean i(x xVar) {
            for (int i = 0; i < this.a.size(); i++) {
                if (xVar.C0.containsKey(this.a.get(i).a.b())) {
                    return true;
                }
            }
            return false;
        }

        public void j(List<k> list) {
            this.a = list;
            x V = ((q) ii.e(PlayerControlView.this.S0)).V();
            if (list.isEmpty()) {
                PlayerControlView.this.l1.c(1, PlayerControlView.this.getResources().getString(m13.exo_track_selection_none));
            } else if (!i(V)) {
                PlayerControlView.this.l1.c(1, PlayerControlView.this.getResources().getString(m13.exo_track_selection_auto));
            } else {
                for (int i = 0; i < list.size(); i++) {
                    k kVar = list.get(i);
                    if (kVar.a()) {
                        PlayerControlView.this.l1.c(1, kVar.c);
                        return;
                    }
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public final class c implements q.d, b.a, View.OnClickListener, PopupWindow.OnDismissListener {
        public c() {
        }

        @Override // androidx.media3.ui.b.a
        public void A(androidx.media3.ui.b bVar, long j) {
            PlayerControlView.this.Z0 = true;
            if (PlayerControlView.this.r0 != null) {
                PlayerControlView.this.r0.setText(androidx.media3.common.util.b.d0(PlayerControlView.this.t0, PlayerControlView.this.u0, j));
            }
            PlayerControlView.this.i1.V();
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void D(int i) {
            nr2.p(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void E(boolean z) {
            nr2.i(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void F(int i) {
            nr2.t(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void H(boolean z) {
            nr2.g(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void I() {
            nr2.x(this);
        }

        @Override // androidx.media3.common.q.d
        public void J(q qVar, q.c cVar) {
            if (cVar.b(4, 5)) {
                PlayerControlView.this.y0();
            }
            if (cVar.b(4, 5, 7)) {
                PlayerControlView.this.A0();
            }
            if (cVar.a(8)) {
                PlayerControlView.this.B0();
            }
            if (cVar.a(9)) {
                PlayerControlView.this.E0();
            }
            if (cVar.b(8, 9, 11, 0, 16, 17, 13)) {
                PlayerControlView.this.x0();
            }
            if (cVar.b(11, 0)) {
                PlayerControlView.this.F0();
            }
            if (cVar.a(12)) {
                PlayerControlView.this.z0();
            }
            if (cVar.a(2)) {
                PlayerControlView.this.G0();
            }
        }

        @Override // androidx.media3.ui.b.a
        public void K(androidx.media3.ui.b bVar, long j) {
            if (PlayerControlView.this.r0 != null) {
                PlayerControlView.this.r0.setText(androidx.media3.common.util.b.d0(PlayerControlView.this.t0, PlayerControlView.this.u0, j));
            }
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void L(u uVar, int i) {
            nr2.B(this, uVar, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void N(boolean z) {
            nr2.y(this, z);
        }

        @Override // androidx.media3.ui.b.a
        public void O(androidx.media3.ui.b bVar, long j, boolean z) {
            PlayerControlView.this.Z0 = false;
            if (!z && PlayerControlView.this.S0 != null) {
                PlayerControlView playerControlView = PlayerControlView.this;
                playerControlView.p0(playerControlView.S0, j);
            }
            PlayerControlView.this.i1.W();
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Q(int i, boolean z) {
            nr2.e(this, i, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void R(boolean z, int i) {
            nr2.s(this, z, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void T(n nVar) {
            nr2.k(this, nVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void V(x xVar) {
            nr2.C(this, xVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void X() {
            nr2.v(this);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Y(y yVar) {
            nr2.D(this, yVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void Z(androidx.media3.common.h hVar) {
            nr2.d(this, hVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void a0(androidx.media3.common.m mVar, int i) {
            nr2.j(this, mVar, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void b(boolean z) {
            nr2.z(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void c0(PlaybackException playbackException) {
            nr2.r(this, playbackException);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void h0(PlaybackException playbackException) {
            nr2.q(this, playbackException);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void i(int i) {
            nr2.o(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void k(z zVar) {
            nr2.E(this, zVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void k0(int i, int i2) {
            nr2.A(this, i, i2);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void l0(q.b bVar) {
            nr2.a(this, bVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void m0(q.e eVar, q.e eVar2, int i) {
            nr2.u(this, eVar, eVar2, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void n(p pVar) {
            nr2.n(this, pVar);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void o(nb0 nb0Var) {
            nr2.b(this, nb0Var);
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            q qVar = PlayerControlView.this.S0;
            if (qVar == null) {
                return;
            }
            PlayerControlView.this.i1.W();
            if (PlayerControlView.this.h0 != view) {
                if (PlayerControlView.this.g0 != view) {
                    if (PlayerControlView.this.j0 != view) {
                        if (PlayerControlView.this.k0 != view) {
                            if (PlayerControlView.this.i0 == view) {
                                PlayerControlView.this.X(qVar);
                                return;
                            } else if (PlayerControlView.this.n0 != view) {
                                if (PlayerControlView.this.o0 != view) {
                                    if (PlayerControlView.this.w1 == view) {
                                        PlayerControlView.this.i1.V();
                                        PlayerControlView playerControlView = PlayerControlView.this;
                                        playerControlView.Y(playerControlView.l1);
                                        return;
                                    } else if (PlayerControlView.this.x1 == view) {
                                        PlayerControlView.this.i1.V();
                                        PlayerControlView playerControlView2 = PlayerControlView.this;
                                        playerControlView2.Y(playerControlView2.m1);
                                        return;
                                    } else if (PlayerControlView.this.y1 == view) {
                                        PlayerControlView.this.i1.V();
                                        PlayerControlView playerControlView3 = PlayerControlView.this;
                                        playerControlView3.Y(playerControlView3.r1);
                                        return;
                                    } else if (PlayerControlView.this.t1 == view) {
                                        PlayerControlView.this.i1.V();
                                        PlayerControlView playerControlView4 = PlayerControlView.this;
                                        playerControlView4.Y(playerControlView4.q1);
                                        return;
                                    } else {
                                        return;
                                    }
                                }
                                qVar.l(!qVar.U());
                                return;
                            } else {
                                qVar.K(y63.a(qVar.Q(), PlayerControlView.this.c1));
                                return;
                            }
                        }
                        qVar.a0();
                        return;
                    } else if (qVar.B() != 4) {
                        qVar.Y();
                        return;
                    } else {
                        return;
                    }
                }
                qVar.u();
                return;
            }
            qVar.X();
        }

        @Override // android.widget.PopupWindow.OnDismissListener
        public void onDismiss() {
            if (PlayerControlView.this.o1) {
                PlayerControlView.this.i1.W();
            }
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void p0(boolean z) {
            nr2.h(this, z);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void s(int i) {
            nr2.w(this, i);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void t(Metadata metadata) {
            nr2.l(this, metadata);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void u(List list) {
            nr2.c(this, list);
        }

        @Override // androidx.media3.common.q.d
        public /* synthetic */ void y(boolean z, int i) {
            nr2.m(this, z, i);
        }
    }

    @Deprecated
    /* loaded from: classes.dex */
    public interface d {
        void K(boolean z);
    }

    /* loaded from: classes.dex */
    public final class e extends RecyclerView.Adapter<i> {
        public final String[] a;
        public final float[] b;
        public int c;

        public e(String[] strArr, float[] fArr) {
            this.a = strArr;
            this.b = fArr;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void c(int i, View view) {
            if (i != this.c) {
                PlayerControlView.this.setPlaybackSpeed(this.b[i]);
            }
            PlayerControlView.this.n1.dismiss();
        }

        public String b() {
            return this.a[this.c];
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: d */
        public void onBindViewHolder(i iVar, final int i) {
            String[] strArr = this.a;
            if (i < strArr.length) {
                iVar.a.setText(strArr[i]);
            }
            if (i == this.c) {
                iVar.itemView.setSelected(true);
                iVar.b.setVisibility(0);
            } else {
                iVar.itemView.setSelected(false);
                iVar.b.setVisibility(4);
            }
            iVar.itemView.setOnClickListener(new View.OnClickListener() { // from class: sr2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    PlayerControlView.e.this.c(i, view);
                }
            });
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: e */
        public i onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new i(LayoutInflater.from(PlayerControlView.this.getContext()).inflate(w03.exo_styled_sub_settings_list_item, viewGroup, false));
        }

        public void f(float f) {
            int i = 0;
            float f2 = Float.MAX_VALUE;
            int i2 = 0;
            while (true) {
                float[] fArr = this.b;
                if (i < fArr.length) {
                    float abs = Math.abs(f - fArr[i]);
                    if (abs < f2) {
                        i2 = i;
                        f2 = abs;
                    }
                    i++;
                } else {
                    this.c = i2;
                    return;
                }
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.a.length;
        }
    }

    /* loaded from: classes.dex */
    public interface f {
        void a(long j, long j2);
    }

    /* loaded from: classes.dex */
    public final class g extends RecyclerView.a0 {
        public final TextView a;
        public final TextView b;
        public final ImageView c;

        public g(View view) {
            super(view);
            if (androidx.media3.common.util.b.a < 26) {
                view.setFocusable(true);
            }
            this.a = (TextView) view.findViewById(n03.exo_main_text);
            this.b = (TextView) view.findViewById(n03.exo_sub_text);
            this.c = (ImageView) view.findViewById(n03.exo_icon);
            view.setOnClickListener(new View.OnClickListener() { // from class: tr2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    PlayerControlView.g.this.e(view2);
                }
            });
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void e(View view) {
            PlayerControlView.this.l0(getAdapterPosition());
        }
    }

    /* loaded from: classes.dex */
    public class h extends RecyclerView.Adapter<g> {
        public final String[] a;
        public final String[] b;
        public final Drawable[] c;

        public h(String[] strArr, Drawable[] drawableArr) {
            this.a = strArr;
            this.b = new String[strArr.length];
            this.c = drawableArr;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: a */
        public void onBindViewHolder(g gVar, int i) {
            gVar.a.setText(this.a[i]);
            if (this.b[i] == null) {
                gVar.b.setVisibility(8);
            } else {
                gVar.b.setText(this.b[i]);
            }
            if (this.c[i] == null) {
                gVar.c.setVisibility(8);
            } else {
                gVar.c.setImageDrawable(this.c[i]);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: b */
        public g onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new g(LayoutInflater.from(PlayerControlView.this.getContext()).inflate(w03.exo_styled_settings_list_item, viewGroup, false));
        }

        public void c(int i, String str) {
            this.b[i] = str;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.a.length;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public long getItemId(int i) {
            return i;
        }
    }

    /* loaded from: classes.dex */
    public static class i extends RecyclerView.a0 {
        public final TextView a;
        public final View b;

        public i(View view) {
            super(view);
            if (androidx.media3.common.util.b.a < 26) {
                view.setFocusable(true);
            }
            this.a = (TextView) view.findViewById(n03.exo_text);
            this.b = view.findViewById(n03.exo_check);
        }
    }

    /* loaded from: classes.dex */
    public final class j extends l {
        public j() {
            super();
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void j(View view) {
            if (PlayerControlView.this.S0 != null) {
                PlayerControlView.this.S0.L(PlayerControlView.this.S0.V().a().B(3).E(-3).A());
                PlayerControlView.this.n1.dismiss();
            }
        }

        @Override // androidx.media3.ui.PlayerControlView.l, androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: d */
        public void onBindViewHolder(i iVar, int i) {
            super.onBindViewHolder(iVar, i);
            if (i > 0) {
                iVar.b.setVisibility(this.a.get(i + (-1)).a() ? 0 : 4);
            }
        }

        @Override // androidx.media3.ui.PlayerControlView.l
        public void e(i iVar) {
            boolean z;
            iVar.a.setText(m13.exo_track_selection_none);
            int i = 0;
            while (true) {
                if (i >= this.a.size()) {
                    z = true;
                    break;
                } else if (this.a.get(i).a()) {
                    z = false;
                    break;
                } else {
                    i++;
                }
            }
            iVar.b.setVisibility(z ? 0 : 4);
            iVar.itemView.setOnClickListener(new View.OnClickListener() { // from class: ur2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    PlayerControlView.j.this.j(view);
                }
            });
        }

        @Override // androidx.media3.ui.PlayerControlView.l
        public void g(String str) {
        }

        public void i(List<k> list) {
            boolean z = false;
            int i = 0;
            while (true) {
                if (i >= list.size()) {
                    break;
                } else if (list.get(i).a()) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (PlayerControlView.this.t1 != null) {
                ImageView imageView = PlayerControlView.this.t1;
                PlayerControlView playerControlView = PlayerControlView.this;
                imageView.setImageDrawable(z ? playerControlView.K0 : playerControlView.L0);
                PlayerControlView.this.t1.setContentDescription(z ? PlayerControlView.this.M0 : PlayerControlView.this.N0);
            }
            this.a = list;
        }
    }

    /* loaded from: classes.dex */
    public static final class k {
        public final y.a a;
        public final int b;
        public final String c;

        public k(y yVar, int i, int i2, String str) {
            this.a = yVar.a().get(i);
            this.b = i2;
            this.c = str;
        }

        public boolean a() {
            return this.a.g(this.b);
        }
    }

    /* loaded from: classes.dex */
    public abstract class l extends RecyclerView.Adapter<i> {
        public List<k> a = new ArrayList();

        public l() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void c(q qVar, v vVar, k kVar, View view) {
            qVar.L(qVar.V().a().F(new w(vVar, ImmutableList.of(Integer.valueOf(kVar.b)))).I(kVar.a.d(), false).A());
            g(kVar.c);
            PlayerControlView.this.n1.dismiss();
        }

        public void b() {
            this.a = Collections.emptyList();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: d */
        public void onBindViewHolder(i iVar, int i) {
            final q qVar = PlayerControlView.this.S0;
            if (qVar == null) {
                return;
            }
            if (i == 0) {
                e(iVar);
                return;
            }
            boolean z = true;
            final k kVar = this.a.get(i - 1);
            final v b = kVar.a.b();
            if (qVar.V().C0.get(b) == null || !kVar.a()) {
                z = false;
            }
            iVar.a.setText(kVar.c);
            iVar.b.setVisibility(z ? 0 : 4);
            iVar.itemView.setOnClickListener(new View.OnClickListener() { // from class: vr2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    PlayerControlView.l.this.c(qVar, b, kVar, view);
                }
            });
        }

        public abstract void e(i iVar);

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: f */
        public i onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new i(LayoutInflater.from(PlayerControlView.this.getContext()).inflate(w03.exo_styled_sub_settings_list_item, viewGroup, false));
        }

        public abstract void g(String str);

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            if (this.a.isEmpty()) {
                return 0;
            }
            return this.a.size() + 1;
        }
    }

    @Deprecated
    /* loaded from: classes.dex */
    public interface m {
        void A(int i);
    }

    static {
        f62.a("media3.ui");
        z1 = new float[]{0.25f, 0.5f, 0.75f, 1.0f, 1.25f, 1.5f, 2.0f};
    }

    public PlayerControlView(Context context) {
        this(context, null);
    }

    public static boolean T(u uVar, u.c cVar) {
        if (uVar.p() > 100) {
            return false;
        }
        int p = uVar.p();
        for (int i2 = 0; i2 < p; i2++) {
            if (uVar.n(i2, cVar).r0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return false;
            }
        }
        return true;
    }

    public static int a0(TypedArray typedArray, int i2) {
        return typedArray.getInt(b33.PlayerControlView_repeat_toggle_modes, i2);
    }

    public static void e0(View view, View.OnClickListener onClickListener) {
        if (view == null) {
            return;
        }
        view.setVisibility(8);
        view.setOnClickListener(onClickListener);
    }

    @SuppressLint({"InlinedApi"})
    public static boolean g0(int i2) {
        return i2 == 90 || i2 == 89 || i2 == 85 || i2 == 79 || i2 == 126 || i2 == 127 || i2 == 87 || i2 == 88;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void setPlaybackSpeed(float f2) {
        q qVar = this.S0;
        if (qVar == null) {
            return;
        }
        qVar.b(qVar.e().c(f2));
    }

    public static void w0(View view, boolean z) {
        if (view == null) {
            return;
        }
        if (z) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
    }

    public final void A0() {
        long j2;
        float f2;
        if (h0() && this.W0) {
            q qVar = this.S0;
            long j3 = 0;
            if (qVar != null) {
                j3 = this.h1 + qVar.y();
                j2 = this.h1 + qVar.W();
            } else {
                j2 = 0;
            }
            TextView textView = this.r0;
            if (textView != null && !this.Z0) {
                textView.setText(androidx.media3.common.util.b.d0(this.t0, this.u0, j3));
            }
            androidx.media3.ui.b bVar = this.s0;
            if (bVar != null) {
                bVar.setPosition(j3);
                this.s0.setBufferedPosition(j2);
            }
            f fVar = this.T0;
            if (fVar != null) {
                fVar.a(j3, j2);
            }
            removeCallbacks(this.x0);
            int B = qVar == null ? 1 : qVar.B();
            if (qVar == null || !qVar.E()) {
                if (B == 4 || B == 1) {
                    return;
                }
                postDelayed(this.x0, 1000L);
                return;
            }
            androidx.media3.ui.b bVar2 = this.s0;
            long min = Math.min(bVar2 != null ? bVar2.getPreferredUpdateDelay() : 1000L, 1000 - (j3 % 1000));
            postDelayed(this.x0, androidx.media3.common.util.b.r(qVar.e().a > Utils.FLOAT_EPSILON ? ((float) min) / f2 : 1000L, this.b1, 1000L));
        }
    }

    public final void B0() {
        ImageView imageView;
        if (h0() && this.W0 && (imageView = this.n0) != null) {
            if (this.c1 == 0) {
                t0(false, imageView);
                return;
            }
            q qVar = this.S0;
            if (qVar == null) {
                t0(false, imageView);
                this.n0.setImageDrawable(this.y0);
                this.n0.setContentDescription(this.B0);
                return;
            }
            t0(true, imageView);
            int Q = qVar.Q();
            if (Q == 0) {
                this.n0.setImageDrawable(this.y0);
                this.n0.setContentDescription(this.B0);
            } else if (Q == 1) {
                this.n0.setImageDrawable(this.z0);
                this.n0.setContentDescription(this.C0);
            } else if (Q != 2) {
            } else {
                this.n0.setImageDrawable(this.A0);
                this.n0.setContentDescription(this.D0);
            }
        }
    }

    public final void C0() {
        q qVar = this.S0;
        int d0 = (int) ((qVar != null ? qVar.d0() : 5000L) / 1000);
        TextView textView = this.m0;
        if (textView != null) {
            textView.setText(String.valueOf(d0));
        }
        View view = this.k0;
        if (view != null) {
            view.setContentDescription(this.j1.getQuantityString(h13.exo_controls_rewind_by_amount_description, d0, Integer.valueOf(d0)));
        }
    }

    public final void D0() {
        this.k1.measure(0, 0);
        this.n1.setWidth(Math.min(this.k1.getMeasuredWidth(), getWidth() - (this.p1 * 2)));
        this.n1.setHeight(Math.min(getHeight() - (this.p1 * 2), this.k1.getMeasuredHeight()));
    }

    public final void E0() {
        ImageView imageView;
        String str;
        if (h0() && this.W0 && (imageView = this.o0) != null) {
            q qVar = this.S0;
            if (!this.i1.A(imageView)) {
                t0(false, this.o0);
            } else if (qVar == null) {
                t0(false, this.o0);
                this.o0.setImageDrawable(this.F0);
                this.o0.setContentDescription(this.J0);
            } else {
                t0(true, this.o0);
                this.o0.setImageDrawable(qVar.U() ? this.E0 : this.F0);
                ImageView imageView2 = this.o0;
                if (qVar.U()) {
                    str = this.I0;
                } else {
                    str = this.J0;
                }
                imageView2.setContentDescription(str);
            }
        }
    }

    public final void F0() {
        int i2;
        u.c cVar;
        q qVar = this.S0;
        if (qVar == null) {
            return;
        }
        boolean z = true;
        this.Y0 = this.X0 && T(qVar.S(), this.w0);
        long j2 = 0;
        this.h1 = 0L;
        u S = qVar.S();
        if (S.q()) {
            i2 = 0;
        } else {
            int I = qVar.I();
            boolean z2 = this.Y0;
            int i3 = z2 ? 0 : I;
            int p = z2 ? S.p() - 1 : I;
            long j3 = 0;
            i2 = 0;
            while (true) {
                if (i3 > p) {
                    break;
                }
                if (i3 == I) {
                    this.h1 = androidx.media3.common.util.b.U0(j3);
                }
                S.n(i3, this.w0);
                u.c cVar2 = this.w0;
                if (cVar2.r0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    ii.g(this.Y0 ^ z);
                    break;
                }
                int i4 = cVar2.s0;
                while (true) {
                    cVar = this.w0;
                    if (i4 <= cVar.t0) {
                        S.f(i4, this.v0);
                        int e2 = this.v0.e();
                        for (int q = this.v0.q(); q < e2; q++) {
                            long h2 = this.v0.h(q);
                            if (h2 == Long.MIN_VALUE) {
                                long j4 = this.v0.h0;
                                if (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                                    h2 = j4;
                                }
                            }
                            long p2 = h2 + this.v0.p();
                            if (p2 >= 0) {
                                long[] jArr = this.d1;
                                if (i2 == jArr.length) {
                                    int length = jArr.length == 0 ? 1 : jArr.length * 2;
                                    this.d1 = Arrays.copyOf(jArr, length);
                                    this.e1 = Arrays.copyOf(this.e1, length);
                                }
                                this.d1[i2] = androidx.media3.common.util.b.U0(j3 + p2);
                                this.e1[i2] = this.v0.r(q);
                                i2++;
                            }
                        }
                        i4++;
                    }
                }
                j3 += cVar.r0;
                i3++;
                z = true;
            }
            j2 = j3;
        }
        long U0 = androidx.media3.common.util.b.U0(j2);
        TextView textView = this.q0;
        if (textView != null) {
            textView.setText(androidx.media3.common.util.b.d0(this.t0, this.u0, U0));
        }
        androidx.media3.ui.b bVar = this.s0;
        if (bVar != null) {
            bVar.setDuration(U0);
            int length2 = this.f1.length;
            int i5 = i2 + length2;
            long[] jArr2 = this.d1;
            if (i5 > jArr2.length) {
                this.d1 = Arrays.copyOf(jArr2, i5);
                this.e1 = Arrays.copyOf(this.e1, i5);
            }
            System.arraycopy(this.f1, 0, this.d1, i2, length2);
            System.arraycopy(this.g1, 0, this.e1, i2, length2);
            this.s0.setAdGroupTimesMs(this.d1, this.e1, i5);
        }
        A0();
    }

    public final void G0() {
        d0();
        t0(this.q1.getItemCount() > 0, this.t1);
    }

    @Deprecated
    public void S(m mVar) {
        ii.e(mVar);
        this.f0.add(mVar);
    }

    public boolean U(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        q qVar = this.S0;
        if (qVar == null || !g0(keyCode)) {
            return false;
        }
        if (keyEvent.getAction() == 0) {
            if (keyCode == 90) {
                if (qVar.B() != 4) {
                    qVar.Y();
                    return true;
                }
                return true;
            } else if (keyCode == 89) {
                qVar.a0();
                return true;
            } else if (keyEvent.getRepeatCount() == 0) {
                if (keyCode == 79 || keyCode == 85) {
                    X(qVar);
                    return true;
                } else if (keyCode == 87) {
                    qVar.X();
                    return true;
                } else if (keyCode == 88) {
                    qVar.u();
                    return true;
                } else if (keyCode == 126) {
                    W(qVar);
                    return true;
                } else if (keyCode != 127) {
                    return true;
                } else {
                    V(qVar);
                    return true;
                }
            } else {
                return true;
            }
        }
        return true;
    }

    public final void V(q qVar) {
        qVar.c();
    }

    public final void W(q qVar) {
        int B = qVar.B();
        if (B == 1) {
            qVar.d();
        } else if (B == 4) {
            o0(qVar, qVar.I(), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }
        qVar.f();
    }

    public final void X(q qVar) {
        int B = qVar.B();
        if (B != 1 && B != 4 && qVar.k()) {
            V(qVar);
        } else {
            W(qVar);
        }
    }

    public final void Y(RecyclerView.Adapter<?> adapter) {
        this.k1.setAdapter(adapter);
        D0();
        this.o1 = false;
        this.n1.dismiss();
        this.o1 = true;
        this.n1.showAsDropDown(this, (getWidth() - this.n1.getWidth()) - this.p1, (-this.n1.getHeight()) - this.p1);
    }

    public final ImmutableList<k> Z(y yVar, int i2) {
        ImmutableList.a aVar = new ImmutableList.a();
        ImmutableList<y.a> a2 = yVar.a();
        for (int i3 = 0; i3 < a2.size(); i3++) {
            y.a aVar2 = a2.get(i3);
            if (aVar2.d() == i2) {
                for (int i4 = 0; i4 < aVar2.a; i4++) {
                    if (aVar2.h(i4)) {
                        androidx.media3.common.j c2 = aVar2.c(i4);
                        if ((c2.h0 & 2) == 0) {
                            aVar.a(new k(yVar, i3, i4, this.s1.a(c2)));
                        }
                    }
                }
            }
        }
        return aVar.l();
    }

    public void b0() {
        this.i1.C();
    }

    public void c0() {
        this.i1.F();
    }

    public final void d0() {
        this.q1.b();
        this.r1.b();
        q qVar = this.S0;
        if (qVar != null && qVar.J(30) && this.S0.J(29)) {
            y C = this.S0.C();
            this.r1.j(Z(C, 1));
            if (this.i1.A(this.t1)) {
                this.q1.i(Z(C, 3));
            } else {
                this.q1.i(ImmutableList.of());
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return U(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    public boolean f0() {
        return this.i1.I();
    }

    public q getPlayer() {
        return this.S0;
    }

    public int getRepeatToggleModes() {
        return this.c1;
    }

    public boolean getShowShuffleButton() {
        return this.i1.A(this.o0);
    }

    public boolean getShowSubtitleButton() {
        return this.i1.A(this.t1);
    }

    public int getShowTimeoutMs() {
        return this.a1;
    }

    public boolean getShowVrButton() {
        return this.i1.A(this.p0);
    }

    public boolean h0() {
        return getVisibility() == 0;
    }

    public void i0() {
        Iterator<m> it = this.f0.iterator();
        while (it.hasNext()) {
            it.next().A(getVisibility());
        }
    }

    public final void j0(View view) {
        if (this.U0 == null) {
            return;
        }
        boolean z = !this.V0;
        this.V0 = z;
        v0(this.u1, z);
        v0(this.v1, this.V0);
        d dVar = this.U0;
        if (dVar != null) {
            dVar.K(this.V0);
        }
    }

    public final void k0(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        int i10 = i5 - i3;
        int i11 = i9 - i7;
        if (!(i4 - i2 == i8 - i6 && i10 == i11) && this.n1.isShowing()) {
            D0();
            this.n1.update(view, (getWidth() - this.n1.getWidth()) - this.p1, (-this.n1.getHeight()) - this.p1, -1, -1);
        }
    }

    public final void l0(int i2) {
        if (i2 == 0) {
            Y(this.m1);
        } else if (i2 == 1) {
            Y(this.r1);
        } else {
            this.n1.dismiss();
        }
    }

    @Deprecated
    public void m0(m mVar) {
        this.f0.remove(mVar);
    }

    public void n0() {
        View view = this.i0;
        if (view != null) {
            view.requestFocus();
        }
    }

    public final void o0(q qVar, int i2, long j2) {
        qVar.i(i2, j2);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.i1.O();
        this.W0 = true;
        if (f0()) {
            this.i1.W();
        }
        s0();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.i1.P();
        this.W0 = false;
        removeCallbacks(this.x0);
        this.i1.V();
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        this.i1.Q(z, i2, i3, i4, i5);
    }

    public final void p0(q qVar, long j2) {
        int I;
        u S = qVar.S();
        if (this.Y0 && !S.q()) {
            int p = S.p();
            I = 0;
            while (true) {
                long g2 = S.n(I, this.w0).g();
                if (j2 < g2) {
                    break;
                } else if (I == p - 1) {
                    j2 = g2;
                    break;
                } else {
                    j2 -= g2;
                    I++;
                }
            }
        } else {
            I = qVar.I();
        }
        o0(qVar, I, j2);
        A0();
    }

    public final boolean q0() {
        q qVar = this.S0;
        return (qVar == null || qVar.B() == 4 || this.S0.B() == 1 || !this.S0.k()) ? false : true;
    }

    public void r0() {
        this.i1.b0();
    }

    public void s0() {
        y0();
        x0();
        B0();
        E0();
        G0();
        z0();
        F0();
    }

    public void setAnimationEnabled(boolean z) {
        this.i1.X(z);
    }

    public void setExtraAdGroupMarkers(long[] jArr, boolean[] zArr) {
        if (jArr == null) {
            this.f1 = new long[0];
            this.g1 = new boolean[0];
        } else {
            boolean[] zArr2 = (boolean[]) ii.e(zArr);
            ii.a(jArr.length == zArr2.length);
            this.f1 = jArr;
            this.g1 = zArr2;
        }
        F0();
    }

    @Deprecated
    public void setOnFullScreenModeChangedListener(d dVar) {
        this.U0 = dVar;
        w0(this.u1, dVar != null);
        w0(this.v1, dVar != null);
    }

    public void setPlayer(q qVar) {
        boolean z = true;
        ii.g(Looper.myLooper() == Looper.getMainLooper());
        if (qVar != null && qVar.T() != Looper.getMainLooper()) {
            z = false;
        }
        ii.a(z);
        q qVar2 = this.S0;
        if (qVar2 == qVar) {
            return;
        }
        if (qVar2 != null) {
            qVar2.G(this.a);
        }
        this.S0 = qVar;
        if (qVar != null) {
            qVar.O(this.a);
        }
        if (qVar instanceof androidx.media3.common.k) {
            ((androidx.media3.common.k) qVar).f0();
        }
        s0();
    }

    public void setProgressUpdateListener(f fVar) {
        this.T0 = fVar;
    }

    public void setRepeatToggleModes(int i2) {
        this.c1 = i2;
        q qVar = this.S0;
        if (qVar != null) {
            int Q = qVar.Q();
            if (i2 == 0 && Q != 0) {
                this.S0.K(0);
            } else if (i2 == 1 && Q == 2) {
                this.S0.K(1);
            } else if (i2 == 2 && Q == 1) {
                this.S0.K(2);
            }
        }
        this.i1.Y(this.n0, i2 != 0);
        B0();
    }

    public void setShowFastForwardButton(boolean z) {
        this.i1.Y(this.j0, z);
        x0();
    }

    public void setShowMultiWindowTimeBar(boolean z) {
        this.X0 = z;
        F0();
    }

    public void setShowNextButton(boolean z) {
        this.i1.Y(this.h0, z);
        x0();
    }

    public void setShowPreviousButton(boolean z) {
        this.i1.Y(this.g0, z);
        x0();
    }

    public void setShowRewindButton(boolean z) {
        this.i1.Y(this.k0, z);
        x0();
    }

    public void setShowShuffleButton(boolean z) {
        this.i1.Y(this.o0, z);
        E0();
    }

    public void setShowSubtitleButton(boolean z) {
        this.i1.Y(this.t1, z);
    }

    public void setShowTimeoutMs(int i2) {
        this.a1 = i2;
        if (f0()) {
            this.i1.W();
        }
    }

    public void setShowVrButton(boolean z) {
        this.i1.Y(this.p0, z);
    }

    public void setTimeBarMinUpdateInterval(int i2) {
        this.b1 = androidx.media3.common.util.b.q(i2, 16, 1000);
    }

    public void setVrButtonListener(View.OnClickListener onClickListener) {
        View view = this.p0;
        if (view != null) {
            view.setOnClickListener(onClickListener);
            t0(onClickListener != null, this.p0);
        }
    }

    public final void t0(boolean z, View view) {
        if (view == null) {
            return;
        }
        view.setEnabled(z);
        view.setAlpha(z ? this.G0 : this.H0);
    }

    public final void u0() {
        q qVar = this.S0;
        int x = (int) ((qVar != null ? qVar.x() : u84.DEFAULT_POLLING_FREQUENCY) / 1000);
        TextView textView = this.l0;
        if (textView != null) {
            textView.setText(String.valueOf(x));
        }
        View view = this.j0;
        if (view != null) {
            view.setContentDescription(this.j1.getQuantityString(h13.exo_controls_fastforward_by_amount_description, x, Integer.valueOf(x)));
        }
    }

    public final void v0(ImageView imageView, boolean z) {
        if (imageView == null) {
            return;
        }
        if (z) {
            imageView.setImageDrawable(this.O0);
            imageView.setContentDescription(this.Q0);
            return;
        }
        imageView.setImageDrawable(this.P0);
        imageView.setContentDescription(this.R0);
    }

    public final void x0() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (h0() && this.W0) {
            q qVar = this.S0;
            boolean z5 = false;
            if (qVar != null) {
                boolean J = qVar.J(5);
                z2 = qVar.J(7);
                boolean J2 = qVar.J(11);
                z4 = qVar.J(12);
                z = qVar.J(9);
                z3 = J;
                z5 = J2;
            } else {
                z = false;
                z2 = false;
                z3 = false;
                z4 = false;
            }
            if (z5) {
                C0();
            }
            if (z4) {
                u0();
            }
            t0(z2, this.g0);
            t0(z5, this.k0);
            t0(z4, this.j0);
            t0(z, this.h0);
            androidx.media3.ui.b bVar = this.s0;
            if (bVar != null) {
                bVar.setEnabled(z3);
            }
        }
    }

    public final void y0() {
        if (h0() && this.W0 && this.i0 != null) {
            if (q0()) {
                ((ImageView) this.i0).setImageDrawable(this.j1.getDrawable(oz2.exo_styled_controls_pause));
                this.i0.setContentDescription(this.j1.getString(m13.exo_controls_pause_description));
                return;
            }
            ((ImageView) this.i0).setImageDrawable(this.j1.getDrawable(oz2.exo_styled_controls_play));
            this.i0.setContentDescription(this.j1.getString(m13.exo_controls_play_description));
        }
    }

    public final void z0() {
        q qVar = this.S0;
        if (qVar == null) {
            return;
        }
        this.m1.f(qVar.e().a);
        this.l1.c(0, this.m1.b());
    }

    public PlayerControlView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PlayerControlView(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, attributeSet);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r9v1 */
    /* JADX WARN: Type inference failed for: r9v2 */
    /* JADX WARN: Type inference failed for: r9v3, types: [android.view.ViewGroup, androidx.media3.ui.PlayerControlView$a] */
    /* JADX WARN: Type inference failed for: r9v4 */
    public PlayerControlView(Context context, AttributeSet attributeSet, int i2, AttributeSet attributeSet2) {
        super(context, attributeSet, i2);
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        c cVar;
        boolean z9;
        boolean z10;
        ?? r9;
        Resources resources;
        boolean z11;
        int i3 = w03.exo_player_control_view;
        this.a1 = 5000;
        this.c1 = 0;
        this.b1 = 200;
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, b33.PlayerControlView, i2, 0);
            try {
                i3 = obtainStyledAttributes.getResourceId(b33.PlayerControlView_controller_layout_id, i3);
                this.a1 = obtainStyledAttributes.getInt(b33.PlayerControlView_show_timeout, this.a1);
                this.c1 = a0(obtainStyledAttributes, this.c1);
                boolean z12 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_show_rewind_button, true);
                boolean z13 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_show_fastforward_button, true);
                boolean z14 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_show_previous_button, true);
                boolean z15 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_show_next_button, true);
                boolean z16 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_show_shuffle_button, false);
                boolean z17 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_show_subtitle_button, false);
                boolean z18 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_show_vr_button, false);
                setTimeBarMinUpdateInterval(obtainStyledAttributes.getInt(b33.PlayerControlView_time_bar_min_update_interval, this.b1));
                boolean z19 = obtainStyledAttributes.getBoolean(b33.PlayerControlView_animation_enabled, true);
                obtainStyledAttributes.recycle();
                z2 = z16;
                z3 = z17;
                z5 = z12;
                z6 = z13;
                z7 = z14;
                z4 = z19;
                z8 = z15;
                z = z18;
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        } else {
            z = false;
            z2 = false;
            z3 = false;
            z4 = true;
            z5 = true;
            z6 = true;
            z7 = true;
            z8 = true;
        }
        LayoutInflater.from(context).inflate(i3, this);
        setDescendantFocusability(262144);
        c cVar2 = new c();
        this.a = cVar2;
        this.f0 = new CopyOnWriteArrayList<>();
        this.v0 = new u.b();
        this.w0 = new u.c();
        StringBuilder sb = new StringBuilder();
        this.t0 = sb;
        this.u0 = new Formatter(sb, Locale.getDefault());
        this.d1 = new long[0];
        this.e1 = new boolean[0];
        this.f1 = new long[0];
        this.g1 = new boolean[0];
        this.x0 = new Runnable() { // from class: qr2
            @Override // java.lang.Runnable
            public final void run() {
                PlayerControlView.this.A0();
            }
        };
        this.q0 = (TextView) findViewById(n03.exo_duration);
        this.r0 = (TextView) findViewById(n03.exo_position);
        ImageView imageView = (ImageView) findViewById(n03.exo_subtitle);
        this.t1 = imageView;
        if (imageView != null) {
            imageView.setOnClickListener(cVar2);
        }
        ImageView imageView2 = (ImageView) findViewById(n03.exo_fullscreen);
        this.u1 = imageView2;
        e0(imageView2, new View.OnClickListener() { // from class: or2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PlayerControlView.this.j0(view);
            }
        });
        ImageView imageView3 = (ImageView) findViewById(n03.exo_minimal_fullscreen);
        this.v1 = imageView3;
        e0(imageView3, new View.OnClickListener() { // from class: or2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PlayerControlView.this.j0(view);
            }
        });
        View findViewById = findViewById(n03.exo_settings);
        this.w1 = findViewById;
        if (findViewById != null) {
            findViewById.setOnClickListener(cVar2);
        }
        View findViewById2 = findViewById(n03.exo_playback_speed);
        this.x1 = findViewById2;
        if (findViewById2 != null) {
            findViewById2.setOnClickListener(cVar2);
        }
        View findViewById3 = findViewById(n03.exo_audio_track);
        this.y1 = findViewById3;
        if (findViewById3 != null) {
            findViewById3.setOnClickListener(cVar2);
        }
        int i4 = n03.exo_progress;
        androidx.media3.ui.b bVar = (androidx.media3.ui.b) findViewById(i4);
        View findViewById4 = findViewById(n03.exo_progress_placeholder);
        if (bVar != null) {
            this.s0 = bVar;
            cVar = cVar2;
            z9 = z4;
            z10 = z;
            r9 = 0;
        } else if (findViewById4 != null) {
            r9 = 0;
            cVar = cVar2;
            z9 = z4;
            z10 = z;
            DefaultTimeBar defaultTimeBar = new DefaultTimeBar(context, null, 0, attributeSet2, x13.ExoStyledControls_TimeBar);
            defaultTimeBar.setId(i4);
            defaultTimeBar.setLayoutParams(findViewById4.getLayoutParams());
            ViewGroup viewGroup = (ViewGroup) findViewById4.getParent();
            int indexOfChild = viewGroup.indexOfChild(findViewById4);
            viewGroup.removeView(findViewById4);
            viewGroup.addView(defaultTimeBar, indexOfChild);
            this.s0 = defaultTimeBar;
        } else {
            cVar = cVar2;
            z9 = z4;
            z10 = z;
            r9 = 0;
            this.s0 = null;
        }
        androidx.media3.ui.b bVar2 = this.s0;
        c cVar3 = cVar;
        if (bVar2 != null) {
            bVar2.a(cVar3);
        }
        View findViewById5 = findViewById(n03.exo_play_pause);
        this.i0 = findViewById5;
        if (findViewById5 != null) {
            findViewById5.setOnClickListener(cVar3);
        }
        View findViewById6 = findViewById(n03.exo_prev);
        this.g0 = findViewById6;
        if (findViewById6 != null) {
            findViewById6.setOnClickListener(cVar3);
        }
        View findViewById7 = findViewById(n03.exo_next);
        this.h0 = findViewById7;
        if (findViewById7 != null) {
            findViewById7.setOnClickListener(cVar3);
        }
        Typeface g2 = g83.g(context, wz2.roboto_medium_numbers);
        View findViewById8 = findViewById(n03.exo_rew);
        TextView textView = findViewById8 == null ? (TextView) findViewById(n03.exo_rew_with_amount) : r9;
        this.m0 = textView;
        if (textView != null) {
            textView.setTypeface(g2);
        }
        findViewById8 = findViewById8 == null ? textView : findViewById8;
        this.k0 = findViewById8;
        if (findViewById8 != null) {
            findViewById8.setOnClickListener(cVar3);
        }
        View findViewById9 = findViewById(n03.exo_ffwd);
        TextView textView2 = findViewById9 == null ? (TextView) findViewById(n03.exo_ffwd_with_amount) : r9;
        this.l0 = textView2;
        if (textView2 != null) {
            textView2.setTypeface(g2);
        }
        findViewById9 = findViewById9 == null ? textView2 : findViewById9;
        this.j0 = findViewById9;
        if (findViewById9 != null) {
            findViewById9.setOnClickListener(cVar3);
        }
        ImageView imageView4 = (ImageView) findViewById(n03.exo_repeat_toggle);
        this.n0 = imageView4;
        if (imageView4 != null) {
            imageView4.setOnClickListener(cVar3);
        }
        ImageView imageView5 = (ImageView) findViewById(n03.exo_shuffle);
        this.o0 = imageView5;
        if (imageView5 != null) {
            imageView5.setOnClickListener(cVar3);
        }
        this.j1 = context.getResources();
        this.G0 = resources.getInteger(u03.exo_media_button_opacity_percentage_enabled) / 100.0f;
        this.H0 = this.j1.getInteger(u03.exo_media_button_opacity_percentage_disabled) / 100.0f;
        View findViewById10 = findViewById(n03.exo_vr);
        this.p0 = findViewById10;
        if (findViewById10 != null) {
            t0(false, findViewById10);
        }
        js2 js2Var = new js2(this);
        this.i1 = js2Var;
        js2Var.X(z9);
        this.l1 = new h(new String[]{this.j1.getString(m13.exo_controls_playback_speed), this.j1.getString(m13.exo_track_selection_title_audio)}, new Drawable[]{this.j1.getDrawable(oz2.exo_styled_controls_speed), this.j1.getDrawable(oz2.exo_styled_controls_audiotrack)});
        this.p1 = this.j1.getDimensionPixelSize(gz2.exo_settings_offset);
        RecyclerView recyclerView = (RecyclerView) LayoutInflater.from(context).inflate(w03.exo_styled_settings_list, (ViewGroup) r9);
        this.k1 = recyclerView;
        recyclerView.setAdapter(this.l1);
        this.k1.setLayoutManager(new LinearLayoutManager(getContext()));
        PopupWindow popupWindow = new PopupWindow((View) this.k1, -2, -2, true);
        this.n1 = popupWindow;
        if (androidx.media3.common.util.b.a < 23) {
            z11 = false;
            popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        } else {
            z11 = false;
        }
        this.n1.setOnDismissListener(cVar3);
        this.o1 = true;
        this.s1 = new zk0(getResources());
        this.K0 = this.j1.getDrawable(oz2.exo_styled_controls_subtitle_on);
        this.L0 = this.j1.getDrawable(oz2.exo_styled_controls_subtitle_off);
        this.M0 = this.j1.getString(m13.exo_controls_cc_enabled_description);
        this.N0 = this.j1.getString(m13.exo_controls_cc_disabled_description);
        this.q1 = new j();
        this.r1 = new b();
        this.m1 = new e(this.j1.getStringArray(xx2.exo_controls_playback_speeds), z1);
        this.O0 = this.j1.getDrawable(oz2.exo_styled_controls_fullscreen_exit);
        this.P0 = this.j1.getDrawable(oz2.exo_styled_controls_fullscreen_enter);
        this.y0 = this.j1.getDrawable(oz2.exo_styled_controls_repeat_off);
        this.z0 = this.j1.getDrawable(oz2.exo_styled_controls_repeat_one);
        this.A0 = this.j1.getDrawable(oz2.exo_styled_controls_repeat_all);
        this.E0 = this.j1.getDrawable(oz2.exo_styled_controls_shuffle_on);
        this.F0 = this.j1.getDrawable(oz2.exo_styled_controls_shuffle_off);
        this.Q0 = this.j1.getString(m13.exo_controls_fullscreen_exit_description);
        this.R0 = this.j1.getString(m13.exo_controls_fullscreen_enter_description);
        this.B0 = this.j1.getString(m13.exo_controls_repeat_off_description);
        this.C0 = this.j1.getString(m13.exo_controls_repeat_one_description);
        this.D0 = this.j1.getString(m13.exo_controls_repeat_all_description);
        this.I0 = this.j1.getString(m13.exo_controls_shuffle_on_description);
        this.J0 = this.j1.getString(m13.exo_controls_shuffle_off_description);
        this.i1.Y((ViewGroup) findViewById(n03.exo_bottom_bar), true);
        this.i1.Y(this.j0, z6);
        this.i1.Y(this.k0, z5);
        this.i1.Y(this.g0, z7);
        this.i1.Y(this.h0, z8);
        this.i1.Y(this.o0, z2);
        this.i1.Y(this.t1, z3);
        this.i1.Y(this.p0, z10);
        this.i1.Y(this.n0, this.c1 != 0 ? true : z11);
        addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: pr2
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12) {
                PlayerControlView.this.k0(view, i5, i6, i7, i8, i9, i10, i11, i12);
            }
        });
    }
}
