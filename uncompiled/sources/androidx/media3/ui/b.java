package androidx.media3.ui;

/* compiled from: TimeBar.java */
/* loaded from: classes.dex */
public interface b {

    /* compiled from: TimeBar.java */
    /* loaded from: classes.dex */
    public interface a {
        void A(b bVar, long j);

        void K(b bVar, long j);

        void O(b bVar, long j, boolean z);
    }

    void a(a aVar);

    long getPreferredUpdateDelay();

    void setAdGroupTimesMs(long[] jArr, boolean[] zArr, int i);

    void setBufferedPosition(long j);

    void setDuration(long j);

    void setEnabled(boolean z);

    void setPosition(long j);
}
