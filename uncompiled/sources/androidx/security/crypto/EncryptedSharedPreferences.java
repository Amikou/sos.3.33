package androidx.security.crypto;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.ArraySet;
import android.util.Pair;
import com.google.crypto.tink.KeyTemplate;
import com.google.crypto.tink.d;
import com.google.crypto.tink.h;
import defpackage.xc;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes.dex */
public final class EncryptedSharedPreferences implements SharedPreferences {
    public final SharedPreferences a;
    public final List<SharedPreferences.OnSharedPreferenceChangeListener> b = new ArrayList();
    public final String c;
    public final com.google.crypto.tink.a d;
    public final d e;

    /* loaded from: classes.dex */
    public enum EncryptedType {
        STRING(0),
        STRING_SET(1),
        INT(2),
        LONG(3),
        FLOAT(4),
        BOOLEAN(5);
        
        private final int mId;

        EncryptedType(int i) {
            this.mId = i;
        }

        public static EncryptedType fromId(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                if (i != 5) {
                                    return null;
                                }
                                return BOOLEAN;
                            }
                            return FLOAT;
                        }
                        return LONG;
                    }
                    return INT;
                }
                return STRING_SET;
            }
            return STRING;
        }

        public int getId() {
            return this.mId;
        }
    }

    /* loaded from: classes.dex */
    public enum PrefKeyEncryptionScheme {
        AES256_SIV(pa.j());
        
        private final KeyTemplate mDeterministicAeadKeyTemplate;

        PrefKeyEncryptionScheme(KeyTemplate keyTemplate) {
            this.mDeterministicAeadKeyTemplate = keyTemplate;
        }

        public KeyTemplate getKeyTemplate() {
            return this.mDeterministicAeadKeyTemplate;
        }
    }

    /* loaded from: classes.dex */
    public enum PrefValueEncryptionScheme {
        AES256_GCM(la.j());
        
        private final KeyTemplate mAeadKeyTemplate;

        PrefValueEncryptionScheme(KeyTemplate keyTemplate) {
            this.mAeadKeyTemplate = keyTemplate;
        }

        public KeyTemplate getKeyTemplate() {
            return this.mAeadKeyTemplate;
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[EncryptedType.values().length];
            a = iArr;
            try {
                iArr[EncryptedType.STRING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[EncryptedType.INT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[EncryptedType.LONG.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[EncryptedType.FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[EncryptedType.BOOLEAN.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[EncryptedType.STRING_SET.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    /* loaded from: classes.dex */
    public static final class b implements SharedPreferences.Editor {
        public final EncryptedSharedPreferences a;
        public final SharedPreferences.Editor b;
        public AtomicBoolean d = new AtomicBoolean(false);
        public final List<String> c = new CopyOnWriteArrayList();

        public b(EncryptedSharedPreferences encryptedSharedPreferences, SharedPreferences.Editor editor) {
            this.a = encryptedSharedPreferences;
            this.b = editor;
        }

        public final void a() {
            if (this.d.getAndSet(false)) {
                for (String str : this.a.getAll().keySet()) {
                    if (!this.c.contains(str) && !this.a.f(str)) {
                        this.b.remove(this.a.c(str));
                    }
                }
            }
        }

        @Override // android.content.SharedPreferences.Editor
        public void apply() {
            a();
            this.b.apply();
            b();
            this.c.clear();
        }

        public final void b() {
            for (SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener : this.a.b) {
                for (String str : this.c) {
                    onSharedPreferenceChangeListener.onSharedPreferenceChanged(this.a, str);
                }
            }
        }

        public final void c(String str, byte[] bArr) {
            if (!this.a.f(str)) {
                this.c.add(str);
                if (str == null) {
                    str = "__NULL__";
                }
                try {
                    Pair<String, String> d = this.a.d(str, bArr);
                    this.b.putString((String) d.first, (String) d.second);
                    return;
                } catch (GeneralSecurityException e) {
                    throw new SecurityException("Could not encrypt data: " + e.getMessage(), e);
                }
            }
            throw new SecurityException(str + " is a reserved key for the encryption keyset.");
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor clear() {
            this.d.set(true);
            return this;
        }

        @Override // android.content.SharedPreferences.Editor
        public boolean commit() {
            a();
            try {
                return this.b.commit();
            } finally {
                b();
                this.c.clear();
            }
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putBoolean(String str, boolean z) {
            ByteBuffer allocate = ByteBuffer.allocate(5);
            allocate.putInt(EncryptedType.BOOLEAN.getId());
            allocate.put(z ? (byte) 1 : (byte) 0);
            c(str, allocate.array());
            return this;
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putFloat(String str, float f) {
            ByteBuffer allocate = ByteBuffer.allocate(8);
            allocate.putInt(EncryptedType.FLOAT.getId());
            allocate.putFloat(f);
            c(str, allocate.array());
            return this;
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putInt(String str, int i) {
            ByteBuffer allocate = ByteBuffer.allocate(8);
            allocate.putInt(EncryptedType.INT.getId());
            allocate.putInt(i);
            c(str, allocate.array());
            return this;
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putLong(String str, long j) {
            ByteBuffer allocate = ByteBuffer.allocate(12);
            allocate.putInt(EncryptedType.LONG.getId());
            allocate.putLong(j);
            c(str, allocate.array());
            return this;
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putString(String str, String str2) {
            if (str2 == null) {
                str2 = "__NULL__";
            }
            byte[] bytes = str2.getBytes(StandardCharsets.UTF_8);
            int length = bytes.length;
            ByteBuffer allocate = ByteBuffer.allocate(length + 8);
            allocate.putInt(EncryptedType.STRING.getId());
            allocate.putInt(length);
            allocate.put(bytes);
            c(str, allocate.array());
            return this;
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
            if (set == null) {
                set = new ArraySet<>();
                set.add("__NULL__");
            }
            ArrayList<byte[]> arrayList = new ArrayList(set.size());
            int size = set.size() * 4;
            for (String str2 : set) {
                byte[] bytes = str2.getBytes(StandardCharsets.UTF_8);
                arrayList.add(bytes);
                size += bytes.length;
            }
            ByteBuffer allocate = ByteBuffer.allocate(size + 4);
            allocate.putInt(EncryptedType.STRING_SET.getId());
            for (byte[] bArr : arrayList) {
                allocate.putInt(bArr.length);
                allocate.put(bArr);
            }
            c(str, allocate.array());
            return this;
        }

        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor remove(String str) {
            if (!this.a.f(str)) {
                this.b.remove(this.a.c(str));
                this.c.remove(str);
                return this;
            }
            throw new SecurityException(str + " is a reserved key for the encryption keyset.");
        }
    }

    public EncryptedSharedPreferences(String str, String str2, SharedPreferences sharedPreferences, com.google.crypto.tink.a aVar, d dVar) {
        this.c = str;
        this.a = sharedPreferences;
        this.d = aVar;
        this.e = dVar;
    }

    public static SharedPreferences a(String str, String str2, Context context, PrefKeyEncryptionScheme prefKeyEncryptionScheme, PrefValueEncryptionScheme prefValueEncryptionScheme) throws GeneralSecurityException, IOException {
        tm0.b();
        ca.b();
        xc.b j = new xc.b().h(prefKeyEncryptionScheme.getKeyTemplate()).j(context, "__androidx_security_crypto_encrypted_prefs_key_keyset__", str);
        h c = j.i("android-keystore://" + str2).d().c();
        xc.b j2 = new xc.b().h(prefValueEncryptionScheme.getKeyTemplate()).j(context, "__androidx_security_crypto_encrypted_prefs_value_keyset__", str);
        h c2 = j2.i("android-keystore://" + str2).d().c();
        d dVar = (d) c.h(d.class);
        return new EncryptedSharedPreferences(str, str2, context.getSharedPreferences(str, 0), (com.google.crypto.tink.a) c2.h(com.google.crypto.tink.a.class), dVar);
    }

    public String b(String str) {
        try {
            String str2 = new String(this.e.b(jm.a(str, 0), this.c.getBytes()), StandardCharsets.UTF_8);
            if (str2.equals("__NULL__")) {
                return null;
            }
            return str2;
        } catch (GeneralSecurityException e) {
            throw new SecurityException("Could not decrypt key. " + e.getMessage(), e);
        }
    }

    public String c(String str) {
        if (str == null) {
            str = "__NULL__";
        }
        try {
            return jm.d(this.e.a(str.getBytes(StandardCharsets.UTF_8), this.c.getBytes()));
        } catch (GeneralSecurityException e) {
            throw new SecurityException("Could not encrypt key. " + e.getMessage(), e);
        }
    }

    @Override // android.content.SharedPreferences
    public boolean contains(String str) {
        if (!f(str)) {
            return this.a.contains(c(str));
        }
        throw new SecurityException(str + " is a reserved key for the encryption keyset.");
    }

    public Pair<String, String> d(String str, byte[] bArr) throws GeneralSecurityException {
        String c = c(str);
        return new Pair<>(c, jm.d(this.d.a(bArr, c.getBytes(StandardCharsets.UTF_8))));
    }

    public final Object e(String str) {
        if (!f(str)) {
            if (str == null) {
                str = "__NULL__";
            }
            try {
                String c = c(str);
                String string = this.a.getString(c, null);
                if (string != null) {
                    ByteBuffer wrap = ByteBuffer.wrap(this.d.b(jm.a(string, 0), c.getBytes(StandardCharsets.UTF_8)));
                    wrap.position(0);
                    switch (a.a[EncryptedType.fromId(wrap.getInt()).ordinal()]) {
                        case 1:
                            int i = wrap.getInt();
                            ByteBuffer slice = wrap.slice();
                            wrap.limit(i);
                            String charBuffer = StandardCharsets.UTF_8.decode(slice).toString();
                            if (charBuffer.equals("__NULL__")) {
                                return null;
                            }
                            return charBuffer;
                        case 2:
                            return Integer.valueOf(wrap.getInt());
                        case 3:
                            return Long.valueOf(wrap.getLong());
                        case 4:
                            return Float.valueOf(wrap.getFloat());
                        case 5:
                            return Boolean.valueOf(wrap.get() != 0);
                        case 6:
                            ArraySet arraySet = new ArraySet();
                            while (wrap.hasRemaining()) {
                                int i2 = wrap.getInt();
                                ByteBuffer slice2 = wrap.slice();
                                slice2.limit(i2);
                                wrap.position(wrap.position() + i2);
                                arraySet.add(StandardCharsets.UTF_8.decode(slice2).toString());
                            }
                            if (arraySet.size() == 1 && "__NULL__".equals(arraySet.valueAt(0))) {
                                return null;
                            }
                            return arraySet;
                        default:
                            return null;
                    }
                }
                return null;
            } catch (GeneralSecurityException e) {
                throw new SecurityException("Could not decrypt value. " + e.getMessage(), e);
            }
        }
        throw new SecurityException(str + " is a reserved key for the encryption keyset.");
    }

    @Override // android.content.SharedPreferences
    public SharedPreferences.Editor edit() {
        return new b(this, this.a.edit());
    }

    public boolean f(String str) {
        return "__androidx_security_crypto_encrypted_prefs_key_keyset__".equals(str) || "__androidx_security_crypto_encrypted_prefs_value_keyset__".equals(str);
    }

    @Override // android.content.SharedPreferences
    public Map<String, ?> getAll() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<String, ?> entry : this.a.getAll().entrySet()) {
            if (!f(entry.getKey())) {
                String b2 = b(entry.getKey());
                hashMap.put(b2, e(b2));
            }
        }
        return hashMap;
    }

    @Override // android.content.SharedPreferences
    public boolean getBoolean(String str, boolean z) {
        Object e = e(str);
        return (e == null || !(e instanceof Boolean)) ? z : ((Boolean) e).booleanValue();
    }

    @Override // android.content.SharedPreferences
    public float getFloat(String str, float f) {
        Object e = e(str);
        return (e == null || !(e instanceof Float)) ? f : ((Float) e).floatValue();
    }

    @Override // android.content.SharedPreferences
    public int getInt(String str, int i) {
        Object e = e(str);
        return (e == null || !(e instanceof Integer)) ? i : ((Integer) e).intValue();
    }

    @Override // android.content.SharedPreferences
    public long getLong(String str, long j) {
        Object e = e(str);
        return (e == null || !(e instanceof Long)) ? j : ((Long) e).longValue();
    }

    @Override // android.content.SharedPreferences
    public String getString(String str, String str2) {
        Object e = e(str);
        return (e == null || !(e instanceof String)) ? str2 : (String) e;
    }

    @Override // android.content.SharedPreferences
    public Set<String> getStringSet(String str, Set<String> set) {
        Set<String> arraySet;
        Object e = e(str);
        if (e instanceof Set) {
            arraySet = (Set) e;
        } else {
            arraySet = new ArraySet<>();
        }
        return arraySet.size() > 0 ? arraySet : set;
    }

    @Override // android.content.SharedPreferences
    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.b.add(onSharedPreferenceChangeListener);
    }

    @Override // android.content.SharedPreferences
    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.b.remove(onSharedPreferenceChangeListener);
    }
}
