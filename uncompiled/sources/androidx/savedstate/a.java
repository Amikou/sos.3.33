package androidx.savedstate;

import android.os.Bundle;
import androidx.lifecycle.Lifecycle;

/* compiled from: SavedStateRegistryController.java */
/* loaded from: classes.dex */
public final class a {
    public final fc3 a;
    public final SavedStateRegistry b = new SavedStateRegistry();

    public a(fc3 fc3Var) {
        this.a = fc3Var;
    }

    public static a a(fc3 fc3Var) {
        return new a(fc3Var);
    }

    public SavedStateRegistry b() {
        return this.b;
    }

    public void c(Bundle bundle) {
        Lifecycle lifecycle = this.a.getLifecycle();
        if (lifecycle.b() == Lifecycle.State.INITIALIZED) {
            lifecycle.a(new Recreator(this.a));
            this.b.b(lifecycle, bundle);
            return;
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage");
    }

    public void d(Bundle bundle) {
        this.b.c(bundle);
    }
}
