package androidx.drawerlayout.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.accessibility.AccessibilityEvent;
import androidx.customview.view.AbsSavedState;
import com.github.mikephil.charting.utils.Utils;
import defpackage.b6;
import defpackage.e6;
import defpackage.ji4;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class DrawerLayout extends ViewGroup implements jn2 {
    public static final int[] P0 = {16843828};
    public static final int[] Q0 = {16842931};
    public static final boolean R0;
    public static final boolean S0;
    public static boolean T0;
    public Drawable A0;
    public Drawable B0;
    public Drawable C0;
    public CharSequence D0;
    public CharSequence E0;
    public Object F0;
    public boolean G0;
    public Drawable H0;
    public Drawable I0;
    public Drawable J0;
    public Drawable K0;
    public final ArrayList<View> L0;
    public Rect M0;
    public Matrix N0;
    public final e6 O0;
    public final d a;
    public float f0;
    public int g0;
    public int h0;
    public float i0;
    public Paint j0;
    public final ji4 k0;
    public final ji4 l0;
    public final f m0;
    public final f n0;
    public int o0;
    public boolean p0;
    public boolean q0;
    public int r0;
    public int s0;
    public int t0;
    public int u0;
    public boolean v0;
    public e w0;
    public List<e> x0;
    public float y0;
    public float z0;

    /* loaded from: classes.dex */
    public class a implements e6 {
        public a() {
        }

        @Override // defpackage.e6
        public boolean a(View view, e6.a aVar) {
            if (!DrawerLayout.this.A(view) || DrawerLayout.this.p(view) == 2) {
                return false;
            }
            DrawerLayout.this.d(view);
            return true;
        }
    }

    /* loaded from: classes.dex */
    public class b implements View.OnApplyWindowInsetsListener {
        public b(DrawerLayout drawerLayout) {
        }

        @Override // android.view.View.OnApplyWindowInsetsListener
        public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
            ((DrawerLayout) view).setChildInsets(windowInsets, windowInsets.getSystemWindowInsetTop() > 0);
            return windowInsets.consumeSystemWindowInsets();
        }
    }

    /* loaded from: classes.dex */
    public class c extends z5 {
        public final Rect d = new Rect();

        public c() {
        }

        @Override // defpackage.z5
        public boolean a(View view, AccessibilityEvent accessibilityEvent) {
            if (accessibilityEvent.getEventType() == 32) {
                List<CharSequence> text = accessibilityEvent.getText();
                View n = DrawerLayout.this.n();
                if (n != null) {
                    CharSequence q = DrawerLayout.this.q(DrawerLayout.this.r(n));
                    if (q != null) {
                        text.add(q);
                        return true;
                    }
                    return true;
                }
                return true;
            }
            return super.a(view, accessibilityEvent);
        }

        @Override // defpackage.z5
        public void f(View view, AccessibilityEvent accessibilityEvent) {
            super.f(view, accessibilityEvent);
            accessibilityEvent.setClassName("androidx.drawerlayout.widget.DrawerLayout");
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            if (DrawerLayout.R0) {
                super.g(view, b6Var);
            } else {
                b6 P = b6.P(b6Var);
                super.g(view, P);
                b6Var.B0(view);
                ViewParent K = ei4.K(view);
                if (K instanceof View) {
                    b6Var.t0((View) K);
                }
                o(b6Var, P);
                P.S();
                n(b6Var, (ViewGroup) view);
            }
            b6Var.c0("androidx.drawerlayout.widget.DrawerLayout");
            b6Var.k0(false);
            b6Var.l0(false);
            b6Var.T(b6.a.e);
            b6Var.T(b6.a.f);
        }

        @Override // defpackage.z5
        public boolean i(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            if (DrawerLayout.R0 || DrawerLayout.y(view)) {
                return super.i(viewGroup, view, accessibilityEvent);
            }
            return false;
        }

        public final void n(b6 b6Var, ViewGroup viewGroup) {
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (DrawerLayout.y(childAt)) {
                    b6Var.c(childAt);
                }
            }
        }

        public final void o(b6 b6Var, b6 b6Var2) {
            Rect rect = this.d;
            b6Var2.n(rect);
            b6Var.Y(rect);
            b6Var.G0(b6Var2.N());
            b6Var.r0(b6Var2.v());
            b6Var.c0(b6Var2.p());
            b6Var.g0(b6Var2.r());
            b6Var.i0(b6Var2.F());
            b6Var.l0(b6Var2.H());
            b6Var.V(b6Var2.B());
            b6Var.z0(b6Var2.L());
            b6Var.a(b6Var2.k());
        }
    }

    /* loaded from: classes.dex */
    public static final class d extends z5 {
        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            if (DrawerLayout.y(view)) {
                return;
            }
            b6Var.t0(null);
        }
    }

    /* loaded from: classes.dex */
    public interface e {
        void a(View view);

        void b(View view);

        void c(int i);

        void d(View view, float f);
    }

    /* loaded from: classes.dex */
    public class f extends ji4.c {
        public final int a;
        public ji4 b;
        public final Runnable c = new a();

        /* loaded from: classes.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                f.this.o();
            }
        }

        public f(int i) {
            this.a = i;
        }

        @Override // defpackage.ji4.c
        public int a(View view, int i, int i2) {
            if (DrawerLayout.this.c(view, 3)) {
                return Math.max(-view.getWidth(), Math.min(i, 0));
            }
            int width = DrawerLayout.this.getWidth();
            return Math.max(width - view.getWidth(), Math.min(i, width));
        }

        @Override // defpackage.ji4.c
        public int b(View view, int i, int i2) {
            return view.getTop();
        }

        @Override // defpackage.ji4.c
        public int d(View view) {
            if (DrawerLayout.this.B(view)) {
                return view.getWidth();
            }
            return 0;
        }

        @Override // defpackage.ji4.c
        public void f(int i, int i2) {
            View l;
            if ((i & 1) == 1) {
                l = DrawerLayout.this.l(3);
            } else {
                l = DrawerLayout.this.l(5);
            }
            if (l == null || DrawerLayout.this.p(l) != 0) {
                return;
            }
            this.b.c(l, i2);
        }

        @Override // defpackage.ji4.c
        public boolean g(int i) {
            return false;
        }

        @Override // defpackage.ji4.c
        public void h(int i, int i2) {
            DrawerLayout.this.postDelayed(this.c, 160L);
        }

        @Override // defpackage.ji4.c
        public void i(View view, int i) {
            ((LayoutParams) view.getLayoutParams()).c = false;
            n();
        }

        @Override // defpackage.ji4.c
        public void j(int i) {
            DrawerLayout.this.P(i, this.b.w());
        }

        @Override // defpackage.ji4.c
        public void k(View view, int i, int i2, int i3, int i4) {
            int width = view.getWidth();
            float width2 = (DrawerLayout.this.c(view, 3) ? i + width : DrawerLayout.this.getWidth() - i) / width;
            DrawerLayout.this.M(view, width2);
            view.setVisibility(width2 == Utils.FLOAT_EPSILON ? 4 : 0);
            DrawerLayout.this.invalidate();
        }

        @Override // defpackage.ji4.c
        public void l(View view, float f, float f2) {
            int i;
            float s = DrawerLayout.this.s(view);
            int width = view.getWidth();
            if (DrawerLayout.this.c(view, 3)) {
                int i2 = (f > Utils.FLOAT_EPSILON ? 1 : (f == Utils.FLOAT_EPSILON ? 0 : -1));
                i = (i2 > 0 || (i2 == 0 && s > 0.5f)) ? 0 : -width;
            } else {
                int width2 = DrawerLayout.this.getWidth();
                if (f < Utils.FLOAT_EPSILON || (f == Utils.FLOAT_EPSILON && s > 0.5f)) {
                    width2 -= width;
                }
                i = width2;
            }
            this.b.P(i, view.getTop());
            DrawerLayout.this.invalidate();
        }

        @Override // defpackage.ji4.c
        public boolean m(View view, int i) {
            return DrawerLayout.this.B(view) && DrawerLayout.this.c(view, this.a) && DrawerLayout.this.p(view) == 0;
        }

        public final void n() {
            View l = DrawerLayout.this.l(this.a == 3 ? 5 : 3);
            if (l != null) {
                DrawerLayout.this.d(l);
            }
        }

        public void o() {
            View l;
            int width;
            int y = this.b.y();
            boolean z = this.a == 3;
            if (z) {
                l = DrawerLayout.this.l(3);
                width = (l != null ? -l.getWidth() : 0) + y;
            } else {
                l = DrawerLayout.this.l(5);
                width = DrawerLayout.this.getWidth() - y;
            }
            if (l != null) {
                if (((!z || l.getLeft() >= width) && (z || l.getLeft() <= width)) || DrawerLayout.this.p(l) != 0) {
                    return;
                }
                this.b.R(l, width, l.getTop());
                ((LayoutParams) l.getLayoutParams()).c = true;
                DrawerLayout.this.invalidate();
                n();
                DrawerLayout.this.b();
            }
        }

        public void p() {
            DrawerLayout.this.removeCallbacks(this.c);
        }

        public void q(ji4 ji4Var) {
            this.b = ji4Var;
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        R0 = i >= 19;
        S0 = i >= 21;
        T0 = i >= 29;
    }

    public DrawerLayout(Context context) {
        this(context, null);
    }

    public static String u(int i) {
        return (i & 3) == 3 ? "LEFT" : (i & 5) == 5 ? "RIGHT" : Integer.toHexString(i);
    }

    public static boolean v(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    public static boolean y(View view) {
        return (ei4.C(view) == 4 || ei4.C(view) == 2) ? false : true;
    }

    public boolean A(View view) {
        if (B(view)) {
            return (((LayoutParams) view.getLayoutParams()).d & 1) == 1;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public boolean B(View view) {
        int b2 = ri1.b(((LayoutParams) view.getLayoutParams()).a, ei4.E(view));
        return ((b2 & 3) == 0 && (b2 & 5) == 0) ? false : true;
    }

    public boolean C(View view) {
        if (B(view)) {
            return ((LayoutParams) view.getLayoutParams()).b > Utils.FLOAT_EPSILON;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public final boolean D(float f2, float f3, View view) {
        if (this.M0 == null) {
            this.M0 = new Rect();
        }
        view.getHitRect(this.M0);
        return this.M0.contains((int) f2, (int) f3);
    }

    public final void E(Drawable drawable, int i) {
        if (drawable == null || !androidx.core.graphics.drawable.a.h(drawable)) {
            return;
        }
        androidx.core.graphics.drawable.a.m(drawable, i);
    }

    public void F(View view, float f2) {
        float s = s(view);
        float width = view.getWidth();
        int i = ((int) (width * f2)) - ((int) (s * width));
        if (!c(view, 3)) {
            i = -i;
        }
        view.offsetLeftAndRight(i);
        M(view, f2);
    }

    public void G(View view) {
        H(view, true);
    }

    public void H(View view, boolean z) {
        if (B(view)) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (this.q0) {
                layoutParams.b = 1.0f;
                layoutParams.d = 1;
                O(view, true);
                N(view);
            } else if (z) {
                layoutParams.d |= 2;
                if (c(view, 3)) {
                    this.k0.R(view, 0, view.getTop());
                } else {
                    this.l0.R(view, getWidth() - view.getWidth(), view.getTop());
                }
            } else {
                F(view, 1.0f);
                P(0, view);
                view.setVisibility(0);
            }
            invalidate();
            return;
        }
        throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
    }

    public void I(e eVar) {
        List<e> list;
        if (eVar == null || (list = this.x0) == null) {
            return;
        }
        list.remove(eVar);
    }

    public final Drawable J() {
        int E = ei4.E(this);
        if (E == 0) {
            Drawable drawable = this.H0;
            if (drawable != null) {
                E(drawable, E);
                return this.H0;
            }
        } else {
            Drawable drawable2 = this.I0;
            if (drawable2 != null) {
                E(drawable2, E);
                return this.I0;
            }
        }
        return this.J0;
    }

    public final Drawable K() {
        int E = ei4.E(this);
        if (E == 0) {
            Drawable drawable = this.I0;
            if (drawable != null) {
                E(drawable, E);
                return this.I0;
            }
        } else {
            Drawable drawable2 = this.H0;
            if (drawable2 != null) {
                E(drawable2, E);
                return this.H0;
            }
        }
        return this.K0;
    }

    public final void L() {
        if (S0) {
            return;
        }
        this.B0 = J();
        this.C0 = K();
    }

    public void M(View view, float f2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 == layoutParams.b) {
            return;
        }
        layoutParams.b = f2;
        j(view, f2);
    }

    public final void N(View view) {
        b6.a aVar = b6.a.l;
        ei4.n0(view, aVar.b());
        if (!A(view) || p(view) == 2) {
            return;
        }
        ei4.p0(view, aVar, null, this.O0);
    }

    public final void O(View view, boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((!z && !B(childAt)) || (z && childAt == view)) {
                ei4.D0(childAt, 1);
            } else {
                ei4.D0(childAt, 4);
            }
        }
    }

    public void P(int i, View view) {
        int B = this.k0.B();
        int B2 = this.l0.B();
        int i2 = 2;
        if (B == 1 || B2 == 1) {
            i2 = 1;
        } else if (B != 2 && B2 != 2) {
            i2 = 0;
        }
        if (view != null && i == 0) {
            float f2 = ((LayoutParams) view.getLayoutParams()).b;
            if (f2 == Utils.FLOAT_EPSILON) {
                h(view);
            } else if (f2 == 1.0f) {
                i(view);
            }
        }
        if (i2 != this.o0) {
            this.o0 = i2;
            List<e> list = this.x0;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.x0.get(size).c(i2);
                }
            }
        }
    }

    public void a(e eVar) {
        if (eVar == null) {
            return;
        }
        if (this.x0 == null) {
            this.x0 = new ArrayList();
        }
        this.x0.add(eVar);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        if (getDescendantFocusability() == 393216) {
            return;
        }
        int childCount = getChildCount();
        boolean z = false;
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (B(childAt)) {
                if (A(childAt)) {
                    childAt.addFocusables(arrayList, i, i2);
                    z = true;
                }
            } else {
                this.L0.add(childAt);
            }
        }
        if (!z) {
            int size = this.L0.size();
            for (int i4 = 0; i4 < size; i4++) {
                View view = this.L0.get(i4);
                if (view.getVisibility() == 0) {
                    view.addFocusables(arrayList, i, i2);
                }
            }
        }
        this.L0.clear();
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (m() == null && !B(view)) {
            ei4.D0(view, 1);
        } else {
            ei4.D0(view, 4);
        }
        if (R0) {
            return;
        }
        ei4.t0(view, this.a);
    }

    public void b() {
        if (this.v0) {
            return;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            getChildAt(i).dispatchTouchEvent(obtain);
        }
        obtain.recycle();
        this.v0 = true;
    }

    public boolean c(View view, int i) {
        return (r(view) & i) == i;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.view.View
    public void computeScroll() {
        int childCount = getChildCount();
        float f2 = Utils.FLOAT_EPSILON;
        for (int i = 0; i < childCount; i++) {
            f2 = Math.max(f2, ((LayoutParams) getChildAt(i).getLayoutParams()).b);
        }
        this.i0 = f2;
        boolean n = this.k0.n(true);
        boolean n2 = this.l0.n(true);
        if (n || n2) {
            ei4.j0(this);
        }
    }

    public void d(View view) {
        e(view, true);
    }

    @Override // android.view.View
    public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
        if ((motionEvent.getSource() & 2) != 0 && motionEvent.getAction() != 10 && this.i0 > Utils.FLOAT_EPSILON) {
            int childCount = getChildCount();
            if (childCount != 0) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                for (int i = childCount - 1; i >= 0; i--) {
                    View childAt = getChildAt(i);
                    if (D(x, y, childAt) && !z(childAt) && k(motionEvent, childAt)) {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }
        return super.dispatchGenericMotionEvent(motionEvent);
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        int i;
        int height = getHeight();
        boolean z = z(view);
        int width = getWidth();
        int save = canvas.save();
        int i2 = 0;
        if (z) {
            int childCount = getChildCount();
            int i3 = 0;
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                if (childAt != view && childAt.getVisibility() == 0 && v(childAt) && B(childAt) && childAt.getHeight() >= height) {
                    if (c(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right > i3) {
                            i3 = right;
                        }
                    } else {
                        int left = childAt.getLeft();
                        if (left < width) {
                            width = left;
                        }
                    }
                }
            }
            canvas.clipRect(i3, 0, width, getHeight());
            i2 = i3;
        }
        boolean drawChild = super.drawChild(canvas, view, j);
        canvas.restoreToCount(save);
        float f2 = this.i0;
        if (f2 > Utils.FLOAT_EPSILON && z) {
            this.j0.setColor((this.h0 & 16777215) | (((int) ((((-16777216) & i) >>> 24) * f2)) << 24));
            canvas.drawRect(i2, Utils.FLOAT_EPSILON, width, getHeight(), this.j0);
        } else if (this.B0 != null && c(view, 3)) {
            int intrinsicWidth = this.B0.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max((float) Utils.FLOAT_EPSILON, Math.min(right2 / this.k0.y(), 1.0f));
            this.B0.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.B0.setAlpha((int) (max * 255.0f));
            this.B0.draw(canvas);
        } else if (this.C0 != null && c(view, 5)) {
            int intrinsicWidth2 = this.C0.getIntrinsicWidth();
            int left2 = view.getLeft();
            float max2 = Math.max((float) Utils.FLOAT_EPSILON, Math.min((getWidth() - left2) / this.l0.y(), 1.0f));
            this.C0.setBounds(left2 - intrinsicWidth2, view.getTop(), left2, view.getBottom());
            this.C0.setAlpha((int) (max2 * 255.0f));
            this.C0.draw(canvas);
        }
        return drawChild;
    }

    public void e(View view, boolean z) {
        if (B(view)) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (this.q0) {
                layoutParams.b = Utils.FLOAT_EPSILON;
                layoutParams.d = 0;
            } else if (z) {
                layoutParams.d |= 4;
                if (c(view, 3)) {
                    this.k0.R(view, -view.getWidth(), view.getTop());
                } else {
                    this.l0.R(view, getWidth(), view.getTop());
                }
            } else {
                F(view, Utils.FLOAT_EPSILON);
                P(0, view);
                view.setVisibility(4);
            }
            invalidate();
            return;
        }
        throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
    }

    public void f() {
        g(false);
    }

    public void g(boolean z) {
        boolean R;
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (B(childAt) && (!z || layoutParams.c)) {
                int width = childAt.getWidth();
                if (c(childAt, 3)) {
                    R = this.k0.R(childAt, -width, childAt.getTop());
                } else {
                    R = this.l0.R(childAt, getWidth(), childAt.getTop());
                }
                z2 |= R;
                layoutParams.c = false;
            }
        }
        this.m0.p();
        this.n0.p();
        if (z2) {
            invalidate();
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    public float getDrawerElevation() {
        return S0 ? this.f0 : Utils.FLOAT_EPSILON;
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.A0;
    }

    public void h(View view) {
        View rootView;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if ((layoutParams.d & 1) == 1) {
            layoutParams.d = 0;
            List<e> list = this.x0;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.x0.get(size).b(view);
                }
            }
            O(view, false);
            N(view);
            if (!hasWindowFocus() || (rootView = getRootView()) == null) {
                return;
            }
            rootView.sendAccessibilityEvent(32);
        }
    }

    public void i(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if ((layoutParams.d & 1) == 0) {
            layoutParams.d = 1;
            List<e> list = this.x0;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.x0.get(size).a(view);
                }
            }
            O(view, true);
            N(view);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
        }
    }

    public void j(View view, float f2) {
        List<e> list = this.x0;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.x0.get(size).d(view, f2);
            }
        }
    }

    public final boolean k(MotionEvent motionEvent, View view) {
        if (!view.getMatrix().isIdentity()) {
            MotionEvent t = t(motionEvent, view);
            boolean dispatchGenericMotionEvent = view.dispatchGenericMotionEvent(t);
            t.recycle();
            return dispatchGenericMotionEvent;
        }
        float scrollX = getScrollX() - view.getLeft();
        float scrollY = getScrollY() - view.getTop();
        motionEvent.offsetLocation(scrollX, scrollY);
        boolean dispatchGenericMotionEvent2 = view.dispatchGenericMotionEvent(motionEvent);
        motionEvent.offsetLocation(-scrollX, -scrollY);
        return dispatchGenericMotionEvent2;
    }

    public View l(int i) {
        int b2 = ri1.b(i, ei4.E(this)) & 7;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((r(childAt) & 7) == b2) {
                return childAt;
            }
        }
        return null;
    }

    public View m() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((((LayoutParams) childAt.getLayoutParams()).d & 1) == 1) {
                return childAt;
            }
        }
        return null;
    }

    public View n() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (B(childAt) && C(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    public int o(int i) {
        int E = ei4.E(this);
        if (i == 3) {
            int i2 = this.r0;
            if (i2 != 3) {
                return i2;
            }
            int i3 = E == 0 ? this.t0 : this.u0;
            if (i3 != 3) {
                return i3;
            }
            return 0;
        } else if (i == 5) {
            int i4 = this.s0;
            if (i4 != 3) {
                return i4;
            }
            int i5 = E == 0 ? this.u0 : this.t0;
            if (i5 != 3) {
                return i5;
            }
            return 0;
        } else if (i == 8388611) {
            int i6 = this.t0;
            if (i6 != 3) {
                return i6;
            }
            int i7 = E == 0 ? this.r0 : this.s0;
            if (i7 != 3) {
                return i7;
            }
            return 0;
        } else if (i != 8388613) {
            return 0;
        } else {
            int i8 = this.u0;
            if (i8 != 3) {
                return i8;
            }
            int i9 = E == 0 ? this.s0 : this.r0;
            if (i9 != 3) {
                return i9;
            }
            return 0;
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.q0 = true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.q0 = true;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        Object obj;
        super.onDraw(canvas);
        if (!this.G0 || this.A0 == null) {
            return;
        }
        int systemWindowInsetTop = (Build.VERSION.SDK_INT < 21 || (obj = this.F0) == null) ? 0 : ((WindowInsets) obj).getSystemWindowInsetTop();
        if (systemWindowInsetTop > 0) {
            this.A0.setBounds(0, 0, getWidth(), systemWindowInsetTop);
            this.A0.draw(canvas);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x001b, code lost:
        if (r0 != 3) goto L7;
     */
    @Override // android.view.ViewGroup
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r7) {
        /*
            r6 = this;
            int r0 = r7.getActionMasked()
            ji4 r1 = r6.k0
            boolean r1 = r1.Q(r7)
            ji4 r2 = r6.l0
            boolean r2 = r2.Q(r7)
            r1 = r1 | r2
            r2 = 1
            r3 = 0
            if (r0 == 0) goto L38
            if (r0 == r2) goto L31
            r7 = 2
            r4 = 3
            if (r0 == r7) goto L1e
            if (r0 == r4) goto L31
            goto L36
        L1e:
            ji4 r7 = r6.k0
            boolean r7 = r7.e(r4)
            if (r7 == 0) goto L36
            androidx.drawerlayout.widget.DrawerLayout$f r7 = r6.m0
            r7.p()
            androidx.drawerlayout.widget.DrawerLayout$f r7 = r6.n0
            r7.p()
            goto L36
        L31:
            r6.g(r2)
            r6.v0 = r3
        L36:
            r7 = r3
            goto L60
        L38:
            float r0 = r7.getX()
            float r7 = r7.getY()
            r6.y0 = r0
            r6.z0 = r7
            float r4 = r6.i0
            r5 = 0
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 <= 0) goto L5d
            ji4 r4 = r6.k0
            int r0 = (int) r0
            int r7 = (int) r7
            android.view.View r7 = r4.u(r0, r7)
            if (r7 == 0) goto L5d
            boolean r7 = r6.z(r7)
            if (r7 == 0) goto L5d
            r7 = r2
            goto L5e
        L5d:
            r7 = r3
        L5e:
            r6.v0 = r3
        L60:
            if (r1 != 0) goto L70
            if (r7 != 0) goto L70
            boolean r7 = r6.w()
            if (r7 != 0) goto L70
            boolean r7 = r6.v0
            if (r7 == 0) goto L6f
            goto L70
        L6f:
            r2 = r3
        L70:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.drawerlayout.widget.DrawerLayout.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.View, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && x()) {
            keyEvent.startTracking();
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    @Override // android.view.View, android.view.KeyEvent.Callback
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 4) {
            View n = n();
            if (n != null && p(n) == 0) {
                f();
            }
            return n != null;
        }
        return super.onKeyUp(i, keyEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        WindowInsets rootWindowInsets;
        int i5;
        float f2;
        int i6;
        boolean z2 = true;
        this.p0 = true;
        int i7 = i3 - i;
        int childCount = getChildCount();
        int i8 = 0;
        while (i8 < childCount) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (z(childAt)) {
                    int i9 = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
                    childAt.layout(i9, ((ViewGroup.MarginLayoutParams) layoutParams).topMargin, childAt.getMeasuredWidth() + i9, ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (c(childAt, 3)) {
                        float f3 = measuredWidth;
                        i6 = (-measuredWidth) + ((int) (layoutParams.b * f3));
                        f2 = (measuredWidth + i6) / f3;
                    } else {
                        float f4 = measuredWidth;
                        f2 = (i7 - i5) / f4;
                        i6 = i7 - ((int) (layoutParams.b * f4));
                    }
                    boolean z3 = f2 != layoutParams.b ? z2 : false;
                    int i10 = layoutParams.a & 112;
                    if (i10 == 16) {
                        int i11 = i4 - i2;
                        int i12 = (i11 - measuredHeight) / 2;
                        int i13 = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                        if (i12 < i13) {
                            i12 = i13;
                        } else {
                            int i14 = i12 + measuredHeight;
                            int i15 = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                            if (i14 > i11 - i15) {
                                i12 = (i11 - i15) - measuredHeight;
                            }
                        }
                        childAt.layout(i6, i12, measuredWidth + i6, measuredHeight + i12);
                    } else if (i10 != 80) {
                        int i16 = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                        childAt.layout(i6, i16, measuredWidth + i6, measuredHeight + i16);
                    } else {
                        int i17 = i4 - i2;
                        childAt.layout(i6, (i17 - ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i6, i17 - ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin);
                    }
                    if (z3) {
                        M(childAt, f2);
                    }
                    int i18 = layoutParams.b > Utils.FLOAT_EPSILON ? 0 : 4;
                    if (childAt.getVisibility() != i18) {
                        childAt.setVisibility(i18);
                    }
                }
            }
            i8++;
            z2 = true;
        }
        if (T0 && (rootWindowInsets = getRootWindowInsets()) != null) {
            cr1 i19 = jp4.x(rootWindowInsets).i();
            ji4 ji4Var = this.k0;
            ji4Var.M(Math.max(ji4Var.x(), i19.a));
            ji4 ji4Var2 = this.l0;
            ji4Var2.M(Math.max(ji4Var2.x(), i19.c));
        }
        this.p0 = false;
        this.q0 = false;
    }

    @Override // android.view.View
    @SuppressLint({"WrongConstant"})
    public void onMeasure(int i, int i2) {
        int r;
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode != 1073741824 || mode2 != 1073741824) {
            if (!isInEditMode()) {
                throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
            }
            if (mode == 0) {
                size = 300;
            }
            if (mode2 == 0) {
                size2 = 300;
            }
        }
        setMeasuredDimension(size, size2);
        int i3 = 0;
        boolean z = this.F0 != null && ei4.B(this);
        int E = ei4.E(this);
        int childCount = getChildCount();
        int i4 = 0;
        boolean z2 = false;
        boolean z3 = false;
        while (i4 < childCount) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (z) {
                    int b2 = ri1.b(layoutParams.a, E);
                    if (ei4.B(childAt)) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            WindowInsets windowInsets = (WindowInsets) this.F0;
                            if (b2 == 3) {
                                windowInsets = windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), i3, windowInsets.getSystemWindowInsetBottom());
                            } else if (b2 == 5) {
                                windowInsets = windowInsets.replaceSystemWindowInsets(i3, windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
                            }
                            childAt.dispatchApplyWindowInsets(windowInsets);
                        }
                    } else if (Build.VERSION.SDK_INT >= 21) {
                        WindowInsets windowInsets2 = (WindowInsets) this.F0;
                        if (b2 == 3) {
                            windowInsets2 = windowInsets2.replaceSystemWindowInsets(windowInsets2.getSystemWindowInsetLeft(), windowInsets2.getSystemWindowInsetTop(), i3, windowInsets2.getSystemWindowInsetBottom());
                        } else if (b2 == 5) {
                            windowInsets2 = windowInsets2.replaceSystemWindowInsets(i3, windowInsets2.getSystemWindowInsetTop(), windowInsets2.getSystemWindowInsetRight(), windowInsets2.getSystemWindowInsetBottom());
                        }
                        ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin = windowInsets2.getSystemWindowInsetLeft();
                        ((ViewGroup.MarginLayoutParams) layoutParams).topMargin = windowInsets2.getSystemWindowInsetTop();
                        ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin = windowInsets2.getSystemWindowInsetRight();
                        ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin = windowInsets2.getSystemWindowInsetBottom();
                    }
                }
                if (z(childAt)) {
                    childAt.measure(View.MeasureSpec.makeMeasureSpec((size - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin) - ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec((size2 - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin) - ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin, 1073741824));
                } else if (B(childAt)) {
                    if (S0) {
                        float y = ei4.y(childAt);
                        float f2 = this.f0;
                        if (y != f2) {
                            ei4.A0(childAt, f2);
                        }
                    }
                    int i5 = (r(childAt) & 7) == 3 ? 1 : i3;
                    if ((i5 != 0 && z2) || (i5 == 0 && z3)) {
                        throw new IllegalStateException("Child drawer has absolute gravity " + u(r) + " but this DrawerLayout already has a drawer view along that edge");
                    }
                    if (i5 != 0) {
                        z2 = true;
                    } else {
                        z3 = true;
                    }
                    childAt.measure(ViewGroup.getChildMeasureSpec(i, this.g0 + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin, ((ViewGroup.MarginLayoutParams) layoutParams).width), ViewGroup.getChildMeasureSpec(i2, ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin, ((ViewGroup.MarginLayoutParams) layoutParams).height));
                    i4++;
                    i3 = 0;
                } else {
                    throw new IllegalStateException("Child " + childAt + " at index " + i4 + " does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY");
                }
            }
            i4++;
            i3 = 0;
        }
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        View l;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        int i = savedState.g0;
        if (i != 0 && (l = l(i)) != null) {
            G(l);
        }
        int i2 = savedState.h0;
        if (i2 != 3) {
            setDrawerLockMode(i2, 3);
        }
        int i3 = savedState.i0;
        if (i3 != 3) {
            setDrawerLockMode(i3, 5);
        }
        int i4 = savedState.j0;
        if (i4 != 3) {
            setDrawerLockMode(i4, 8388611);
        }
        int i5 = savedState.k0;
        if (i5 != 3) {
            setDrawerLockMode(i5, 8388613);
        }
    }

    @Override // android.view.View
    public void onRtlPropertiesChanged(int i) {
        L();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            LayoutParams layoutParams = (LayoutParams) getChildAt(i).getLayoutParams();
            int i2 = layoutParams.d;
            boolean z = i2 == 1;
            boolean z2 = i2 == 2;
            if (z || z2) {
                savedState.g0 = layoutParams.a;
                break;
            }
        }
        savedState.h0 = this.r0;
        savedState.i0 = this.s0;
        savedState.j0 = this.t0;
        savedState.k0 = this.u0;
        return savedState;
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x0058, code lost:
        if (p(r7) != 2) goto L19;
     */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onTouchEvent(android.view.MotionEvent r7) {
        /*
            r6 = this;
            ji4 r0 = r6.k0
            r0.G(r7)
            ji4 r0 = r6.l0
            r0.G(r7)
            int r0 = r7.getAction()
            r0 = r0 & 255(0xff, float:3.57E-43)
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L5f
            if (r0 == r2) goto L20
            r7 = 3
            if (r0 == r7) goto L1a
            goto L6d
        L1a:
            r6.g(r2)
            r6.v0 = r1
            goto L6d
        L20:
            float r0 = r7.getX()
            float r7 = r7.getY()
            ji4 r3 = r6.k0
            int r4 = (int) r0
            int r5 = (int) r7
            android.view.View r3 = r3.u(r4, r5)
            if (r3 == 0) goto L5a
            boolean r3 = r6.z(r3)
            if (r3 == 0) goto L5a
            float r3 = r6.y0
            float r0 = r0 - r3
            float r3 = r6.z0
            float r7 = r7 - r3
            ji4 r3 = r6.k0
            int r3 = r3.A()
            float r0 = r0 * r0
            float r7 = r7 * r7
            float r0 = r0 + r7
            int r3 = r3 * r3
            float r7 = (float) r3
            int r7 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r7 >= 0) goto L5a
            android.view.View r7 = r6.m()
            if (r7 == 0) goto L5a
            int r7 = r6.p(r7)
            r0 = 2
            if (r7 != r0) goto L5b
        L5a:
            r1 = r2
        L5b:
            r6.g(r1)
            goto L6d
        L5f:
            float r0 = r7.getX()
            float r7 = r7.getY()
            r6.y0 = r0
            r6.z0 = r7
            r6.v0 = r1
        L6d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.drawerlayout.widget.DrawerLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public int p(View view) {
        if (B(view)) {
            return o(((LayoutParams) view.getLayoutParams()).a);
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public CharSequence q(int i) {
        int b2 = ri1.b(i, ei4.E(this));
        if (b2 == 3) {
            return this.D0;
        }
        if (b2 == 5) {
            return this.E0;
        }
        return null;
    }

    public int r(View view) {
        return ri1.b(((LayoutParams) view.getLayoutParams()).a, ei4.E(this));
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z) {
            g(true);
        }
    }

    @Override // android.view.View, android.view.ViewParent
    public void requestLayout() {
        if (this.p0) {
            return;
        }
        super.requestLayout();
    }

    public float s(View view) {
        return ((LayoutParams) view.getLayoutParams()).b;
    }

    public void setChildInsets(Object obj, boolean z) {
        this.F0 = obj;
        this.G0 = z;
        setWillNotDraw(!z && getBackground() == null);
        requestLayout();
    }

    public void setDrawerElevation(float f2) {
        this.f0 = f2;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (B(childAt)) {
                ei4.A0(childAt, this.f0);
            }
        }
    }

    @Deprecated
    public void setDrawerListener(e eVar) {
        e eVar2 = this.w0;
        if (eVar2 != null) {
            I(eVar2);
        }
        if (eVar != null) {
            a(eVar);
        }
        this.w0 = eVar;
    }

    public void setDrawerLockMode(int i) {
        setDrawerLockMode(i, 3);
        setDrawerLockMode(i, 5);
    }

    public void setDrawerShadow(Drawable drawable, int i) {
        if (S0) {
            return;
        }
        if ((i & 8388611) == 8388611) {
            this.H0 = drawable;
        } else if ((i & 8388613) == 8388613) {
            this.I0 = drawable;
        } else if ((i & 3) == 3) {
            this.J0 = drawable;
        } else if ((i & 5) != 5) {
            return;
        } else {
            this.K0 = drawable;
        }
        L();
        invalidate();
    }

    public void setDrawerTitle(int i, CharSequence charSequence) {
        int b2 = ri1.b(i, ei4.E(this));
        if (b2 == 3) {
            this.D0 = charSequence;
        } else if (b2 == 5) {
            this.E0 = charSequence;
        }
    }

    public void setScrimColor(int i) {
        this.h0 = i;
        invalidate();
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.A0 = drawable;
        invalidate();
    }

    public void setStatusBarBackgroundColor(int i) {
        this.A0 = new ColorDrawable(i);
        invalidate();
    }

    public final MotionEvent t(MotionEvent motionEvent, View view) {
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.offsetLocation(getScrollX() - view.getLeft(), getScrollY() - view.getTop());
        Matrix matrix = view.getMatrix();
        if (!matrix.isIdentity()) {
            if (this.N0 == null) {
                this.N0 = new Matrix();
            }
            matrix.invert(this.N0);
            obtain.transform(this.N0);
        }
        return obtain;
    }

    public final boolean w() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((LayoutParams) getChildAt(i).getLayoutParams()).c) {
                return true;
            }
        }
        return false;
    }

    public final boolean x() {
        return n() != null;
    }

    public boolean z(View view) {
        return ((LayoutParams) view.getLayoutParams()).a == 0;
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, ey2.drawerLayoutStyle);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new d();
        this.h0 = -1728053248;
        this.j0 = new Paint();
        this.q0 = true;
        this.r0 = 3;
        this.s0 = 3;
        this.t0 = 3;
        this.u0 = 3;
        this.H0 = null;
        this.I0 = null;
        this.J0 = null;
        this.K0 = null;
        this.O0 = new a();
        setDescendantFocusability(262144);
        float f2 = getResources().getDisplayMetrics().density;
        this.g0 = (int) ((64.0f * f2) + 0.5f);
        float f3 = f2 * 400.0f;
        f fVar = new f(3);
        this.m0 = fVar;
        f fVar2 = new f(5);
        this.n0 = fVar2;
        ji4 o = ji4.o(this, 1.0f, fVar);
        this.k0 = o;
        o.N(1);
        o.O(f3);
        fVar.q(o);
        ji4 o2 = ji4.o(this, 1.0f, fVar2);
        this.l0 = o2;
        o2.N(2);
        o2.O(f3);
        fVar2.q(o2);
        setFocusableInTouchMode(true);
        ei4.D0(this, 1);
        ei4.t0(this, new c());
        setMotionEventSplittingEnabled(false);
        if (ei4.B(this)) {
            if (Build.VERSION.SDK_INT >= 21) {
                setOnApplyWindowInsetsListener(new b(this));
                setSystemUiVisibility(1280);
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(P0);
                try {
                    this.A0 = obtainStyledAttributes.getDrawable(0);
                } finally {
                    obtainStyledAttributes.recycle();
                }
            } else {
                this.A0 = null;
            }
        }
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, z23.DrawerLayout, i, 0);
        try {
            int i2 = z23.DrawerLayout_elevation;
            if (obtainStyledAttributes2.hasValue(i2)) {
                this.f0 = obtainStyledAttributes2.getDimension(i2, Utils.FLOAT_EPSILON);
            } else {
                this.f0 = getResources().getDimension(fz2.def_drawer_elevation);
            }
            obtainStyledAttributes2.recycle();
            this.L0 = new ArrayList<>();
        } catch (Throwable th) {
            obtainStyledAttributes2.recycle();
            throw th;
        }
    }

    public void setDrawerLockMode(int i, int i2) {
        View l;
        int b2 = ri1.b(i2, ei4.E(this));
        if (i2 == 3) {
            this.r0 = i;
        } else if (i2 == 5) {
            this.s0 = i;
        } else if (i2 == 8388611) {
            this.t0 = i;
        } else if (i2 == 8388613) {
            this.u0 = i;
        }
        if (i != 0) {
            (b2 == 3 ? this.k0 : this.l0).b();
        }
        if (i != 1) {
            if (i == 2 && (l = l(b2)) != null) {
                G(l);
                return;
            }
            return;
        }
        View l2 = l(b2);
        if (l2 != null) {
            d(l2);
        }
    }

    public void setStatusBarBackground(int i) {
        this.A0 = i != 0 ? m70.f(getContext(), i) : null;
        invalidate();
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int a;
        public float b;
        public boolean c;
        public int d;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.Q0);
            this.a = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.a = 0;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.a = 0;
            this.a = layoutParams.a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.a = 0;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.a = 0;
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* loaded from: classes.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int g0;
        public int h0;
        public int i0;
        public int j0;
        public int k0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = 0;
            this.g0 = parcel.readInt();
            this.h0 = parcel.readInt();
            this.i0 = parcel.readInt();
            this.j0 = parcel.readInt();
            this.k0 = parcel.readInt();
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.g0);
            parcel.writeInt(this.h0);
            parcel.writeInt(this.i0);
            parcel.writeInt(this.j0);
            parcel.writeInt(this.k0);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
            this.g0 = 0;
        }
    }

    public void setDrawerShadow(int i, int i2) {
        setDrawerShadow(m70.f(getContext(), i), i2);
    }

    public void setDrawerLockMode(int i, View view) {
        if (B(view)) {
            setDrawerLockMode(i, ((LayoutParams) view.getLayoutParams()).a);
            return;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer with appropriate layout_gravity");
    }
}
