package androidx.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.activity.OnBackPressedDispatcher;
import androidx.lifecycle.Lifecycle;
import androidx.navigation.d;
import androidx.navigation.h;
import androidx.navigation.i;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/* loaded from: classes.dex */
public class NavController {
    public final Context a;
    public Activity b;
    public g c;
    public e d;
    public Bundle e;
    public Parcelable[] f;
    public boolean g;
    public rz1 i;
    public zd2 j;
    public final Deque<xd2> h = new ArrayDeque();
    public j k = new j();
    public final CopyOnWriteArrayList<b> l = new CopyOnWriteArrayList<>();
    public final qz1 m = new androidx.lifecycle.e() { // from class: androidx.navigation.NavController.1
        @Override // androidx.lifecycle.e
        public void e(rz1 rz1Var, Lifecycle.Event event) {
            NavController navController = NavController.this;
            if (navController.d != null) {
                for (xd2 xd2Var : navController.h) {
                    xd2Var.f(event);
                }
            }
        }
    };
    public final fm2 n = new a(false);
    public boolean o = true;

    /* loaded from: classes.dex */
    public class a extends fm2 {
        public a(boolean z) {
            super(z);
        }

        @Override // defpackage.fm2
        public void b() {
            NavController.this.y();
        }
    }

    /* loaded from: classes.dex */
    public interface b {
        void a(NavController navController, d dVar, Bundle bundle);
    }

    public NavController(Context context) {
        this.a = context;
        while (true) {
            if (!(context instanceof ContextWrapper)) {
                break;
            } else if (context instanceof Activity) {
                this.b = (Activity) context;
                break;
            } else {
                context = ((ContextWrapper) context).getBaseContext();
            }
        }
        j jVar = this.k;
        jVar.a(new f(jVar));
        this.k.a(new androidx.navigation.a(this.a));
    }

    public boolean A(int i, boolean z) {
        boolean z2;
        boolean z3 = false;
        if (this.h.isEmpty()) {
            return false;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<xd2> descendingIterator = this.h.descendingIterator();
        while (true) {
            if (!descendingIterator.hasNext()) {
                z2 = false;
                break;
            }
            d b2 = descendingIterator.next().b();
            i e = this.k.e(b2.w());
            if (z || b2.s() != i) {
                arrayList.add(e);
            }
            if (b2.s() == i) {
                z2 = true;
                break;
            }
        }
        if (!z2) {
            String q = d.q(this.a, i);
            StringBuilder sb = new StringBuilder();
            sb.append("Ignoring popBackStack to destination ");
            sb.append(q);
            sb.append(" as it was not found on the current back stack");
            return false;
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext() && ((i) it.next()).e()) {
            xd2 removeLast = this.h.removeLast();
            if (removeLast.getLifecycle().b().isAtLeast(Lifecycle.State.CREATED)) {
                removeLast.i(Lifecycle.State.DESTROYED);
            }
            zd2 zd2Var = this.j;
            if (zd2Var != null) {
                zd2Var.a(removeLast.j0);
            }
            z3 = true;
        }
        L();
        return z3;
    }

    public void B(b bVar) {
        this.l.remove(bVar);
    }

    public void C(Bundle bundle) {
        if (bundle == null) {
            return;
        }
        bundle.setClassLoader(this.a.getClassLoader());
        this.e = bundle.getBundle("android-support-nav:controller:navigatorState");
        this.f = bundle.getParcelableArray("android-support-nav:controller:backStack");
        this.g = bundle.getBoolean("android-support-nav:controller:deepLinkHandled");
    }

    public Bundle D() {
        Bundle bundle;
        ArrayList<String> arrayList = new ArrayList<>();
        Bundle bundle2 = new Bundle();
        for (Map.Entry<String, i<? extends d>> entry : this.k.f().entrySet()) {
            String key = entry.getKey();
            Bundle d = entry.getValue().d();
            if (d != null) {
                arrayList.add(key);
                bundle2.putBundle(key, d);
            }
        }
        if (arrayList.isEmpty()) {
            bundle = null;
        } else {
            bundle = new Bundle();
            bundle2.putStringArrayList("android-support-nav:controller:navigatorState:names", arrayList);
            bundle.putBundle("android-support-nav:controller:navigatorState", bundle2);
        }
        if (!this.h.isEmpty()) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            Parcelable[] parcelableArr = new Parcelable[this.h.size()];
            int i = 0;
            for (xd2 xd2Var : this.h) {
                parcelableArr[i] = new NavBackStackEntryState(xd2Var);
                i++;
            }
            bundle.putParcelableArray("android-support-nav:controller:backStack", parcelableArr);
        }
        if (this.g) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android-support-nav:controller:deepLinkHandled", this.g);
        }
        return bundle;
    }

    public void E(int i) {
        F(i, null);
    }

    public void F(int i, Bundle bundle) {
        H(l().c(i), bundle);
    }

    public void G(e eVar) {
        H(eVar, null);
    }

    public void H(e eVar, Bundle bundle) {
        e eVar2 = this.d;
        if (eVar2 != null) {
            A(eVar2.s(), true);
        }
        this.d = eVar;
        x(bundle);
    }

    public void I(rz1 rz1Var) {
        if (rz1Var == this.i) {
            return;
        }
        this.i = rz1Var;
        rz1Var.getLifecycle().a(this.m);
    }

    public void J(OnBackPressedDispatcher onBackPressedDispatcher) {
        if (this.i != null) {
            this.n.d();
            onBackPressedDispatcher.a(this.i, this.n);
            this.i.getLifecycle().c(this.m);
            this.i.getLifecycle().a(this.m);
            return;
        }
        throw new IllegalStateException("You must call setLifecycleOwner() before calling setOnBackPressedDispatcher()");
    }

    public void K(gj4 gj4Var) {
        if (this.j == zd2.b(gj4Var)) {
            return;
        }
        if (this.h.isEmpty()) {
            this.j = zd2.b(gj4Var);
            return;
        }
        throw new IllegalStateException("ViewModelStore should be set before setGraph call");
    }

    public final void L() {
        boolean z = true;
        this.n.f((!this.o || j() <= 1) ? false : false);
    }

    public void a(b bVar) {
        if (!this.h.isEmpty()) {
            xd2 peekLast = this.h.peekLast();
            bVar.a(this, peekLast.b(), peekLast.a());
        }
        this.l.add(bVar);
    }

    public final boolean b() {
        while (!this.h.isEmpty() && (this.h.peekLast().b() instanceof e) && A(this.h.peekLast().b().s(), true)) {
        }
        if (this.h.isEmpty()) {
            return false;
        }
        d b2 = this.h.peekLast().b();
        d dVar = null;
        if (b2 instanceof g71) {
            Iterator<xd2> descendingIterator = this.h.descendingIterator();
            while (true) {
                if (!descendingIterator.hasNext()) {
                    break;
                }
                d b3 = descendingIterator.next().b();
                if (!(b3 instanceof e) && !(b3 instanceof g71)) {
                    dVar = b3;
                    break;
                }
            }
        }
        HashMap hashMap = new HashMap();
        Iterator<xd2> descendingIterator2 = this.h.descendingIterator();
        while (descendingIterator2.hasNext()) {
            xd2 next = descendingIterator2.next();
            Lifecycle.State c = next.c();
            d b4 = next.b();
            if (b2 != null && b4.s() == b2.s()) {
                Lifecycle.State state = Lifecycle.State.RESUMED;
                if (c != state) {
                    hashMap.put(next, state);
                }
                b2 = b2.y();
            } else if (dVar != null && b4.s() == dVar.s()) {
                if (c == Lifecycle.State.RESUMED) {
                    next.i(Lifecycle.State.STARTED);
                } else {
                    Lifecycle.State state2 = Lifecycle.State.STARTED;
                    if (c != state2) {
                        hashMap.put(next, state2);
                    }
                }
                dVar = dVar.y();
            } else {
                next.i(Lifecycle.State.CREATED);
            }
        }
        for (xd2 xd2Var : this.h) {
            Lifecycle.State state3 = (Lifecycle.State) hashMap.get(xd2Var);
            if (state3 != null) {
                xd2Var.i(state3);
            } else {
                xd2Var.j();
            }
        }
        xd2 peekLast = this.h.peekLast();
        Iterator<b> it = this.l.iterator();
        while (it.hasNext()) {
            it.next().a(this, peekLast.b(), peekLast.a());
        }
        return true;
    }

    public void c(boolean z) {
        this.o = z;
        L();
    }

    public d d(int i) {
        e b2;
        e y;
        e eVar = this.d;
        if (eVar == null) {
            return null;
        }
        if (eVar.s() == i) {
            return this.d;
        }
        if (this.h.isEmpty()) {
            b2 = this.d;
        } else {
            b2 = this.h.getLast().b();
        }
        if (b2 instanceof e) {
            y = b2;
        } else {
            y = b2.y();
        }
        return y.J(i);
    }

    public final String e(int[] iArr) {
        e eVar;
        e eVar2 = this.d;
        int i = 0;
        while (true) {
            d dVar = null;
            if (i >= iArr.length) {
                return null;
            }
            int i2 = iArr[i];
            if (i == 0) {
                if (this.d.s() == i2) {
                    dVar = this.d;
                }
            } else {
                dVar = eVar2.J(i2);
            }
            if (dVar == null) {
                return d.q(this.a, i2);
            }
            if (i != iArr.length - 1) {
                while (true) {
                    eVar = (e) dVar;
                    if (!(eVar.J(eVar.N()) instanceof e)) {
                        break;
                    }
                    dVar = eVar.J(eVar.N());
                }
                eVar2 = eVar;
            }
            i++;
        }
    }

    public xd2 f(int i) {
        xd2 xd2Var;
        Iterator<xd2> descendingIterator = this.h.descendingIterator();
        while (true) {
            if (!descendingIterator.hasNext()) {
                xd2Var = null;
                break;
            }
            xd2Var = descendingIterator.next();
            if (xd2Var.b().s() == i) {
                break;
            }
        }
        if (xd2Var != null) {
            return xd2Var;
        }
        throw new IllegalArgumentException("No destination with ID " + i + " is on the NavController's back stack. The current destination is " + i());
    }

    public Context g() {
        return this.a;
    }

    public xd2 h() {
        if (this.h.isEmpty()) {
            return null;
        }
        return this.h.getLast();
    }

    public d i() {
        xd2 h = h();
        if (h != null) {
            return h.b();
        }
        return null;
    }

    public final int j() {
        int i = 0;
        for (xd2 xd2Var : this.h) {
            if (!(xd2Var.b() instanceof e)) {
                i++;
            }
        }
        return i;
    }

    public e k() {
        e eVar = this.d;
        if (eVar != null) {
            return eVar;
        }
        throw new IllegalStateException("You must call setGraph() before calling getGraph()");
    }

    public g l() {
        if (this.c == null) {
            this.c = new g(this.a, this.k);
        }
        return this.c;
    }

    public j m() {
        return this.k;
    }

    public xd2 n() {
        Iterator<xd2> descendingIterator = this.h.descendingIterator();
        if (descendingIterator.hasNext()) {
            descendingIterator.next();
        }
        while (descendingIterator.hasNext()) {
            xd2 next = descendingIterator.next();
            if (!(next.b() instanceof e)) {
                return next;
            }
        }
        return null;
    }

    public boolean o(Intent intent) {
        d.a z;
        e eVar;
        if (intent == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        int[] intArray = extras != null ? extras.getIntArray("android-support-nav:controller:deepLinkIds") : null;
        Bundle bundle = new Bundle();
        Bundle bundle2 = extras != null ? extras.getBundle("android-support-nav:controller:deepLinkExtras") : null;
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
        if ((intArray == null || intArray.length == 0) && intent.getData() != null && (z = this.d.z(new be2(intent))) != null) {
            d d = z.d();
            int[] m = d.m();
            bundle.putAll(d.k(z.e()));
            intArray = m;
        }
        if (intArray == null || intArray.length == 0) {
            return false;
        }
        String e = e(intArray);
        if (e != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Could not find destination ");
            sb.append(e);
            sb.append(" in the navigation graph, ignoring the deep link from ");
            sb.append(intent);
            return false;
        }
        bundle.putParcelable("android-support-nav:controller:deepLinkIntent", intent);
        int flags = intent.getFlags();
        int i = 268435456 & flags;
        if (i != 0 && (flags & 32768) == 0) {
            intent.addFlags(32768);
            v34.n(this.a).i(intent).q();
            Activity activity = this.b;
            if (activity != null) {
                activity.finish();
                this.b.overridePendingTransition(0, 0);
            }
            return true;
        } else if (i != 0) {
            if (!this.h.isEmpty()) {
                A(this.d.s(), true);
            }
            int i2 = 0;
            while (i2 < intArray.length) {
                int i3 = i2 + 1;
                int i4 = intArray[i2];
                d d2 = d(i4);
                if (d2 != null) {
                    v(d2, bundle, new h.a().b(0).c(0).a(), null);
                    i2 = i3;
                } else {
                    throw new IllegalStateException("Deep Linking failed: destination " + d.q(this.a, i4) + " cannot be found from the current destination " + i());
                }
            }
            return true;
        } else {
            e eVar2 = this.d;
            int i5 = 0;
            while (i5 < intArray.length) {
                int i6 = intArray[i5];
                d J = i5 == 0 ? this.d : eVar2.J(i6);
                if (J != null) {
                    if (i5 != intArray.length - 1) {
                        while (true) {
                            eVar = (e) J;
                            if (!(eVar.J(eVar.N()) instanceof e)) {
                                break;
                            }
                            J = eVar.J(eVar.N());
                        }
                        eVar2 = eVar;
                    } else {
                        v(J, J.k(bundle), new h.a().g(this.d.s(), true).b(0).c(0).a(), null);
                    }
                    i5++;
                } else {
                    throw new IllegalStateException("Deep Linking failed: destination " + d.q(this.a, i6) + " cannot be found in graph " + eVar2);
                }
            }
            this.g = true;
            return true;
        }
    }

    public void p(int i) {
        q(i, null);
    }

    public void q(int i, Bundle bundle) {
        r(i, bundle, null);
    }

    public void r(int i, Bundle bundle, h hVar) {
        s(i, bundle, hVar, null);
    }

    public void s(int i, Bundle bundle, h hVar, i.a aVar) {
        d b2;
        int i2;
        if (this.h.isEmpty()) {
            b2 = this.d;
        } else {
            b2 = this.h.getLast().b();
        }
        if (b2 != null) {
            wd2 n = b2.n(i);
            Bundle bundle2 = null;
            if (n != null) {
                if (hVar == null) {
                    hVar = n.c();
                }
                i2 = n.b();
                Bundle a2 = n.a();
                if (a2 != null) {
                    bundle2 = new Bundle();
                    bundle2.putAll(a2);
                }
            } else {
                i2 = i;
            }
            if (bundle != null) {
                if (bundle2 == null) {
                    bundle2 = new Bundle();
                }
                bundle2.putAll(bundle);
            }
            if (i2 == 0 && hVar != null && hVar.e() != -1) {
                z(hVar.e(), hVar.f());
                return;
            } else if (i2 != 0) {
                d d = d(i2);
                if (d == null) {
                    String q = d.q(this.a, i2);
                    if (n != null) {
                        throw new IllegalArgumentException("Navigation destination " + q + " referenced from action " + d.q(this.a, i) + " cannot be found from the current destination " + b2);
                    }
                    throw new IllegalArgumentException("Navigation action/destination " + q + " cannot be found from the current destination " + b2);
                }
                v(d, bundle2, hVar, aVar);
                return;
            } else {
                throw new IllegalArgumentException("Destination id == 0 can only be used in conjunction with a valid navOptions.popUpTo");
            }
        }
        throw new IllegalStateException("no current navigation node");
    }

    public void t(ce2 ce2Var) {
        q(ce2Var.b(), ce2Var.a());
    }

    public void u(ce2 ce2Var, h hVar) {
        r(ce2Var.b(), ce2Var.a(), hVar);
    }

    public final void v(d dVar, Bundle bundle, h hVar, i.a aVar) {
        boolean z = false;
        boolean A = (hVar == null || hVar.e() == -1) ? false : A(hVar.e(), hVar.f());
        i e = this.k.e(dVar.w());
        Bundle k = dVar.k(bundle);
        d b2 = e.b(dVar, k, hVar, aVar);
        if (b2 != null) {
            if (!(b2 instanceof g71)) {
                while (!this.h.isEmpty() && (this.h.peekLast().b() instanceof g71) && A(this.h.peekLast().b().s(), true)) {
                }
            }
            ArrayDeque arrayDeque = new ArrayDeque();
            if (dVar instanceof e) {
                e eVar = b2;
                while (true) {
                    e y = eVar.y();
                    if (y != null) {
                        arrayDeque.addFirst(new xd2(this.a, y, k, this.i, this.j));
                        if (!this.h.isEmpty() && this.h.getLast().b() == y) {
                            A(y.s(), true);
                        }
                    }
                    if (y == null || y == dVar) {
                        break;
                    }
                    eVar = y;
                }
            }
            e b3 = arrayDeque.isEmpty() ? b2 : ((xd2) arrayDeque.getFirst()).b();
            while (b3 != null && d(b3.s()) == null) {
                b3 = b3.y();
                if (b3 != null) {
                    arrayDeque.addFirst(new xd2(this.a, b3, k, this.i, this.j));
                }
            }
            d b4 = arrayDeque.isEmpty() ? b2 : ((xd2) arrayDeque.getLast()).b();
            while (!this.h.isEmpty() && (this.h.getLast().b() instanceof e) && ((e) this.h.getLast().b()).K(b4.s(), false) == null && A(this.h.getLast().b().s(), true)) {
            }
            this.h.addAll(arrayDeque);
            if (this.h.isEmpty() || this.h.getFirst().b() != this.d) {
                this.h.addFirst(new xd2(this.a, this.d, k, this.i, this.j));
            }
            this.h.add(new xd2(this.a, b2, b2.k(k), this.i, this.j));
        } else if (hVar != null && hVar.g()) {
            xd2 peekLast = this.h.peekLast();
            if (peekLast != null) {
                peekLast.g(k);
            }
            z = true;
        }
        L();
        if (A || b2 != null || z) {
            b();
        }
    }

    public boolean w() {
        if (j() == 1) {
            d i = i();
            int s = i.s();
            for (e y = i.y(); y != null; y = y.y()) {
                if (y.N() != s) {
                    Bundle bundle = new Bundle();
                    Activity activity = this.b;
                    if (activity != null && activity.getIntent() != null && this.b.getIntent().getData() != null) {
                        bundle.putParcelable("android-support-nav:controller:deepLinkIntent", this.b.getIntent());
                        d.a z = this.d.z(new be2(this.b.getIntent()));
                        if (z != null) {
                            bundle.putAll(z.d().k(z.e()));
                        }
                    }
                    new ae2(this).d(y.s()).c(bundle).a().q();
                    Activity activity2 = this.b;
                    if (activity2 != null) {
                        activity2.finish();
                    }
                    return true;
                }
                s = y.s();
            }
            return false;
        }
        return y();
    }

    public final void x(Bundle bundle) {
        Activity activity;
        ArrayList<String> stringArrayList;
        Bundle bundle2 = this.e;
        if (bundle2 != null && (stringArrayList = bundle2.getStringArrayList("android-support-nav:controller:navigatorState:names")) != null) {
            Iterator<String> it = stringArrayList.iterator();
            while (it.hasNext()) {
                String next = it.next();
                i e = this.k.e(next);
                Bundle bundle3 = this.e.getBundle(next);
                if (bundle3 != null) {
                    e.c(bundle3);
                }
            }
        }
        Parcelable[] parcelableArr = this.f;
        boolean z = false;
        if (parcelableArr != null) {
            for (Parcelable parcelable : parcelableArr) {
                NavBackStackEntryState navBackStackEntryState = (NavBackStackEntryState) parcelable;
                d d = d(navBackStackEntryState.b());
                if (d != null) {
                    Bundle a2 = navBackStackEntryState.a();
                    if (a2 != null) {
                        a2.setClassLoader(this.a.getClassLoader());
                    }
                    this.h.add(new xd2(this.a, d, a2, this.i, this.j, navBackStackEntryState.d(), navBackStackEntryState.c()));
                } else {
                    throw new IllegalStateException("Restoring the Navigation back stack failed: destination " + d.q(this.a, navBackStackEntryState.b()) + " cannot be found from the current destination " + i());
                }
            }
            L();
            this.f = null;
        }
        if (this.d != null && this.h.isEmpty()) {
            if (!this.g && (activity = this.b) != null && o(activity.getIntent())) {
                z = true;
            }
            if (z) {
                return;
            }
            v(this.d, bundle, null, null);
            return;
        }
        b();
    }

    public boolean y() {
        if (this.h.isEmpty()) {
            return false;
        }
        return z(i().s(), true);
    }

    public boolean z(int i, boolean z) {
        return A(i, z) && b();
    }
}
