package androidx.navigation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import androidx.navigation.b;
import androidx.navigation.c;
import androidx.navigation.h;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: NavInflater.java */
/* loaded from: classes.dex */
public final class g {
    public static final ThreadLocal<TypedValue> c = new ThreadLocal<>();
    public Context a;
    public j b;

    public g(Context context, j jVar) {
        this.a = context;
        this.b = jVar;
    }

    public static ee2 a(TypedValue typedValue, ee2 ee2Var, ee2 ee2Var2, String str, String str2) throws XmlPullParserException {
        if (ee2Var == null || ee2Var == ee2Var2) {
            return ee2Var != null ? ee2Var : ee2Var2;
        }
        throw new XmlPullParserException("Type is " + str + " but found " + str2 + ": " + typedValue.data);
    }

    public final d b(Resources resources, XmlResourceParser xmlResourceParser, AttributeSet attributeSet, int i) throws XmlPullParserException, IOException {
        int depth;
        d a = this.b.e(xmlResourceParser.getName()).a();
        a.B(this.a, attributeSet);
        int depth2 = xmlResourceParser.getDepth() + 1;
        while (true) {
            int next = xmlResourceParser.next();
            if (next == 1 || ((depth = xmlResourceParser.getDepth()) < depth2 && next == 3)) {
                break;
            } else if (next == 2 && depth <= depth2) {
                String name = xmlResourceParser.getName();
                if ("argument".equals(name)) {
                    g(resources, a, attributeSet, i);
                } else if ("deepLink".equals(name)) {
                    h(resources, a, attributeSet);
                } else if ("action".equals(name)) {
                    d(resources, a, attributeSet, xmlResourceParser, i);
                } else if ("include".equals(name) && (a instanceof e)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, c33.NavInclude);
                    ((e) a).I(c(obtainAttributes.getResourceId(c33.NavInclude_graph, 0)));
                    obtainAttributes.recycle();
                } else if (a instanceof e) {
                    ((e) a).I(b(resources, xmlResourceParser, attributeSet, i));
                }
            }
        }
        return a;
    }

    @SuppressLint({"ResourceType"})
    public e c(int i) {
        int next;
        Resources resources = this.a.getResources();
        XmlResourceParser xml = resources.getXml(i);
        AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
        while (true) {
            try {
                try {
                    next = xml.next();
                    if (next == 2 || next == 1) {
                        break;
                    }
                } catch (Exception e) {
                    throw new RuntimeException("Exception inflating " + resources.getResourceName(i) + " line " + xml.getLineNumber(), e);
                }
            } finally {
                xml.close();
            }
        }
        if (next == 2) {
            String name = xml.getName();
            d b = b(resources, xml, asAttributeSet, i);
            if (b instanceof e) {
                return (e) b;
            }
            throw new IllegalArgumentException("Root element <" + name + "> did not inflate into a NavGraph");
        }
        throw new XmlPullParserException("No start tag found");
    }

    public final void d(Resources resources, d dVar, AttributeSet attributeSet, XmlResourceParser xmlResourceParser, int i) throws IOException, XmlPullParserException {
        int depth;
        TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, c23.NavAction);
        int resourceId = obtainAttributes.getResourceId(c23.NavAction_android_id, 0);
        wd2 wd2Var = new wd2(obtainAttributes.getResourceId(c23.NavAction_destination, 0));
        h.a aVar = new h.a();
        aVar.d(obtainAttributes.getBoolean(c23.NavAction_launchSingleTop, false));
        aVar.g(obtainAttributes.getResourceId(c23.NavAction_popUpTo, -1), obtainAttributes.getBoolean(c23.NavAction_popUpToInclusive, false));
        aVar.b(obtainAttributes.getResourceId(c23.NavAction_enterAnim, -1));
        aVar.c(obtainAttributes.getResourceId(c23.NavAction_exitAnim, -1));
        aVar.e(obtainAttributes.getResourceId(c23.NavAction_popEnterAnim, -1));
        aVar.f(obtainAttributes.getResourceId(c23.NavAction_popExitAnim, -1));
        wd2Var.e(aVar.a());
        Bundle bundle = new Bundle();
        int depth2 = xmlResourceParser.getDepth() + 1;
        while (true) {
            int next = xmlResourceParser.next();
            if (next == 1 || ((depth = xmlResourceParser.getDepth()) < depth2 && next == 3)) {
                break;
            } else if (next == 2 && depth <= depth2 && "argument".equals(xmlResourceParser.getName())) {
                f(resources, bundle, attributeSet, i);
            }
        }
        if (!bundle.isEmpty()) {
            wd2Var.d(bundle);
        }
        dVar.D(resourceId, wd2Var);
        obtainAttributes.recycle();
    }

    public final b e(TypedArray typedArray, Resources resources, int i) throws XmlPullParserException {
        b.a aVar = new b.a();
        aVar.c(typedArray.getBoolean(c23.NavArgument_nullable, false));
        ThreadLocal<TypedValue> threadLocal = c;
        TypedValue typedValue = threadLocal.get();
        if (typedValue == null) {
            typedValue = new TypedValue();
            threadLocal.set(typedValue);
        }
        String string = typedArray.getString(c23.NavArgument_argType);
        Object obj = null;
        ee2<Integer> a = string != null ? ee2.a(string, resources.getResourcePackageName(i)) : null;
        int i2 = c23.NavArgument_android_defaultValue;
        if (typedArray.getValue(i2, typedValue)) {
            ee2<Integer> ee2Var = ee2.c;
            if (a == ee2Var) {
                int i3 = typedValue.resourceId;
                if (i3 != 0) {
                    obj = Integer.valueOf(i3);
                } else if (typedValue.type == 16 && typedValue.data == 0) {
                    obj = 0;
                } else {
                    throw new XmlPullParserException("unsupported value '" + ((Object) typedValue.string) + "' for " + a.c() + ". Must be a reference to a resource.");
                }
            } else {
                int i4 = typedValue.resourceId;
                if (i4 != 0) {
                    if (a == null) {
                        a = ee2Var;
                        obj = Integer.valueOf(i4);
                    } else {
                        throw new XmlPullParserException("unsupported value '" + ((Object) typedValue.string) + "' for " + a.c() + ". You must use a \"" + ee2Var.c() + "\" type to reference other resources.");
                    }
                } else if (a == ee2.k) {
                    obj = typedArray.getString(i2);
                } else {
                    int i5 = typedValue.type;
                    if (i5 == 3) {
                        String charSequence = typedValue.string.toString();
                        if (a == null) {
                            a = ee2.d(charSequence);
                        }
                        obj = a.h(charSequence);
                    } else if (i5 == 4) {
                        a = a(typedValue, a, ee2.g, string, "float");
                        obj = Float.valueOf(typedValue.getFloat());
                    } else if (i5 == 5) {
                        a = a(typedValue, a, ee2.b, string, "dimension");
                        obj = Integer.valueOf((int) typedValue.getDimension(resources.getDisplayMetrics()));
                    } else if (i5 == 18) {
                        a = a(typedValue, a, ee2.i, string, "boolean");
                        obj = Boolean.valueOf(typedValue.data != 0);
                    } else if (i5 >= 16 && i5 <= 31) {
                        ee2<Float> ee2Var2 = ee2.g;
                        if (a == ee2Var2) {
                            a = a(typedValue, a, ee2Var2, string, "float");
                            obj = Float.valueOf(typedValue.data);
                        } else {
                            a = a(typedValue, a, ee2.b, string, "integer");
                            obj = Integer.valueOf(typedValue.data);
                        }
                    } else {
                        throw new XmlPullParserException("unsupported argument type " + typedValue.type);
                    }
                }
            }
        }
        if (obj != null) {
            aVar.b(obj);
        }
        if (a != null) {
            aVar.d(a);
        }
        return aVar.a();
    }

    public final void f(Resources resources, Bundle bundle, AttributeSet attributeSet, int i) throws XmlPullParserException {
        TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, c23.NavArgument);
        String string = obtainAttributes.getString(c23.NavArgument_android_name);
        if (string != null) {
            b e = e(obtainAttributes, resources, i);
            if (e.b()) {
                e.c(string, bundle);
            }
            obtainAttributes.recycle();
            return;
        }
        throw new XmlPullParserException("Arguments must have a name");
    }

    public final void g(Resources resources, d dVar, AttributeSet attributeSet, int i) throws XmlPullParserException {
        TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, c23.NavArgument);
        String string = obtainAttributes.getString(c23.NavArgument_android_name);
        if (string != null) {
            dVar.e(string, e(obtainAttributes, resources, i));
            obtainAttributes.recycle();
            return;
        }
        throw new XmlPullParserException("Arguments must have a name");
    }

    public final void h(Resources resources, d dVar, AttributeSet attributeSet) throws XmlPullParserException {
        TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, c23.NavDeepLink);
        String string = obtainAttributes.getString(c23.NavDeepLink_uri);
        String string2 = obtainAttributes.getString(c23.NavDeepLink_action);
        String string3 = obtainAttributes.getString(c23.NavDeepLink_mimeType);
        if (TextUtils.isEmpty(string) && TextUtils.isEmpty(string2) && TextUtils.isEmpty(string3)) {
            throw new XmlPullParserException("Every <deepLink> must include at least one of app:uri, app:action, or app:mimeType");
        }
        c.a aVar = new c.a();
        if (string != null) {
            aVar.d(string.replace("${applicationId}", this.a.getPackageName()));
        }
        if (!TextUtils.isEmpty(string2)) {
            aVar.b(string2.replace("${applicationId}", this.a.getPackageName()));
        }
        if (string3 != null) {
            aVar.c(string3.replace("${applicationId}", this.a.getPackageName()));
        }
        dVar.i(aVar.a());
        obtainAttributes.recycle();
    }
}
