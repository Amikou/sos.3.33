package androidx.navigation;

import android.net.Uri;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: NavDeepLink.java */
/* loaded from: classes.dex */
public final class c {
    public static final Pattern i = Pattern.compile("^[a-zA-Z]+[+\\w\\-.]*:");
    public final ArrayList<String> a = new ArrayList<>();
    public final Map<String, C0050c> b = new HashMap();
    public Pattern c;
    public boolean d;
    public boolean e;
    public final String f;
    public Pattern g;
    public final String h;

    /* compiled from: NavDeepLink.java */
    /* loaded from: classes.dex */
    public static final class a {
        public String a;
        public String b;
        public String c;

        public c a() {
            return new c(this.a, this.b, this.c);
        }

        public a b(String str) {
            if (!str.isEmpty()) {
                this.b = str;
                return this;
            }
            throw new IllegalArgumentException("The NavDeepLink cannot have an empty action.");
        }

        public a c(String str) {
            this.c = str;
            return this;
        }

        public a d(String str) {
            this.a = str;
            return this;
        }
    }

    /* compiled from: NavDeepLink.java */
    /* loaded from: classes.dex */
    public static class b implements Comparable<b> {
        public String a;
        public String f0;

        public b(String str) {
            String[] split = str.split("/", -1);
            this.a = split[0];
            this.f0 = split[1];
        }

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(b bVar) {
            int i = this.a.equals(bVar.a) ? 2 : 0;
            return this.f0.equals(bVar.f0) ? i + 1 : i;
        }
    }

    /* compiled from: NavDeepLink.java */
    /* renamed from: androidx.navigation.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0050c {
        public String a;
        public ArrayList<String> b = new ArrayList<>();

        public void a(String str) {
            this.b.add(str);
        }

        public String b(int i) {
            return this.b.get(i);
        }

        public String c() {
            return this.a;
        }

        public void d(String str) {
            this.a = str;
        }

        public int e() {
            return this.b.size();
        }
    }

    public c(String str, String str2, String str3) {
        this.c = null;
        int i2 = 0;
        this.d = false;
        this.e = false;
        this.g = null;
        this.f = str2;
        this.h = str3;
        if (str != null) {
            Uri parse = Uri.parse(str);
            this.e = parse.getQuery() != null;
            StringBuilder sb = new StringBuilder("^");
            if (!i.matcher(str).find()) {
                sb.append("http[s]?://");
            }
            Pattern compile = Pattern.compile("\\{(.+?)\\}");
            if (this.e) {
                Matcher matcher = Pattern.compile("(\\?)").matcher(str);
                if (matcher.find()) {
                    a(str.substring(0, matcher.start()), sb, compile);
                }
                this.d = false;
                for (String str4 : parse.getQueryParameterNames()) {
                    StringBuilder sb2 = new StringBuilder();
                    String queryParameter = parse.getQueryParameter(str4);
                    Matcher matcher2 = compile.matcher(queryParameter);
                    C0050c c0050c = new C0050c();
                    int i3 = i2;
                    while (matcher2.find()) {
                        c0050c.a(matcher2.group(1));
                        sb2.append(Pattern.quote(queryParameter.substring(i3, matcher2.start())));
                        sb2.append("(.+?)?");
                        i3 = matcher2.end();
                    }
                    if (i3 < queryParameter.length()) {
                        sb2.append(Pattern.quote(queryParameter.substring(i3)));
                    }
                    c0050c.d(sb2.toString().replace(".*", "\\E.*\\Q"));
                    this.b.put(str4, c0050c);
                    i2 = 0;
                }
            } else {
                this.d = a(str, sb, compile);
            }
            this.c = Pattern.compile(sb.toString().replace(".*", "\\E.*\\Q"), 2);
        }
        if (str3 != null) {
            if (Pattern.compile("^[\\s\\S]+/[\\s\\S]+$").matcher(str3).matches()) {
                b bVar = new b(str3);
                this.g = Pattern.compile(("^(" + bVar.a + "|[*]+)/(" + bVar.f0 + "|[*]+)$").replace("*|[*]", "[\\s\\S]"));
                return;
            }
            throw new IllegalArgumentException("The given mimeType " + str3 + " does not match to required \"type/subtype\" format");
        }
    }

    public final boolean a(String str, StringBuilder sb, Pattern pattern) {
        Matcher matcher = pattern.matcher(str);
        boolean z = !str.contains(".*");
        int i2 = 0;
        while (matcher.find()) {
            this.a.add(matcher.group(1));
            sb.append(Pattern.quote(str.substring(i2, matcher.start())));
            sb.append("(.+?)");
            i2 = matcher.end();
            z = false;
        }
        if (i2 < str.length()) {
            sb.append(Pattern.quote(str.substring(i2)));
        }
        sb.append("($|(\\?(.)*))");
        return z;
    }

    public String b() {
        return this.f;
    }

    public Bundle c(Uri uri, Map<String, androidx.navigation.b> map) {
        Matcher matcher;
        Matcher matcher2 = this.c.matcher(uri.toString());
        if (matcher2.matches()) {
            Bundle bundle = new Bundle();
            int size = this.a.size();
            int i2 = 0;
            while (i2 < size) {
                String str = this.a.get(i2);
                i2++;
                if (f(bundle, str, Uri.decode(matcher2.group(i2)), map.get(str))) {
                    return null;
                }
            }
            if (this.e) {
                for (String str2 : this.b.keySet()) {
                    C0050c c0050c = this.b.get(str2);
                    String queryParameter = uri.getQueryParameter(str2);
                    if (queryParameter != null) {
                        matcher = Pattern.compile(c0050c.c()).matcher(queryParameter);
                        if (!matcher.matches()) {
                            return null;
                        }
                    } else {
                        matcher = null;
                    }
                    for (int i3 = 0; i3 < c0050c.e(); i3++) {
                        String decode = matcher != null ? Uri.decode(matcher.group(i3 + 1)) : null;
                        String b2 = c0050c.b(i3);
                        androidx.navigation.b bVar = map.get(b2);
                        if (decode != null && !decode.replaceAll("[{}]", "").equals(b2) && f(bundle, b2, decode, bVar)) {
                            return null;
                        }
                    }
                }
            }
            return bundle;
        }
        return null;
    }

    public int d(String str) {
        if (this.h == null || !this.g.matcher(str).matches()) {
            return -1;
        }
        return new b(this.h).compareTo(new b(str));
    }

    public boolean e() {
        return this.d;
    }

    public final boolean f(Bundle bundle, String str, String str2, androidx.navigation.b bVar) {
        if (bVar != null) {
            try {
                bVar.a().g(bundle, str, str2);
                return false;
            } catch (IllegalArgumentException unused) {
                return true;
            }
        }
        bundle.putString(str, str2);
        return false;
    }
}
