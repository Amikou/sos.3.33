package androidx.navigation;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.navigation.i;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ActivityNavigator.java */
@i.b("activity")
/* loaded from: classes.dex */
public class a extends i<C0049a> {
    public Context a;
    public Activity b;

    /* compiled from: ActivityNavigator.java */
    /* renamed from: androidx.navigation.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0049a extends d {
        public Intent m0;
        public String n0;

        public C0049a(i<? extends C0049a> iVar) {
            super(iVar);
        }

        @Override // androidx.navigation.d
        public void B(Context context, AttributeSet attributeSet) {
            super.B(context, attributeSet);
            TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, c33.ActivityNavigator);
            String string = obtainAttributes.getString(c33.ActivityNavigator_targetPackage);
            if (string != null) {
                string = string.replace("${applicationId}", context.getPackageName());
            }
            S(string);
            String string2 = obtainAttributes.getString(c33.ActivityNavigator_android_name);
            if (string2 != null) {
                if (string2.charAt(0) == '.') {
                    string2 = context.getPackageName() + string2;
                }
                O(new ComponentName(context, string2));
            }
            N(obtainAttributes.getString(c33.ActivityNavigator_action));
            String string3 = obtainAttributes.getString(c33.ActivityNavigator_data);
            if (string3 != null) {
                P(Uri.parse(string3));
            }
            R(obtainAttributes.getString(c33.ActivityNavigator_dataPattern));
            obtainAttributes.recycle();
        }

        @Override // androidx.navigation.d
        public boolean H() {
            return false;
        }

        public final String I() {
            Intent intent = this.m0;
            if (intent == null) {
                return null;
            }
            return intent.getAction();
        }

        public final ComponentName J() {
            Intent intent = this.m0;
            if (intent == null) {
                return null;
            }
            return intent.getComponent();
        }

        public final String K() {
            return this.n0;
        }

        public final Intent L() {
            return this.m0;
        }

        public final C0049a N(String str) {
            if (this.m0 == null) {
                this.m0 = new Intent();
            }
            this.m0.setAction(str);
            return this;
        }

        public final C0049a O(ComponentName componentName) {
            if (this.m0 == null) {
                this.m0 = new Intent();
            }
            this.m0.setComponent(componentName);
            return this;
        }

        public final C0049a P(Uri uri) {
            if (this.m0 == null) {
                this.m0 = new Intent();
            }
            this.m0.setData(uri);
            return this;
        }

        public final C0049a R(String str) {
            this.n0 = str;
            return this;
        }

        public final C0049a S(String str) {
            if (this.m0 == null) {
                this.m0 = new Intent();
            }
            this.m0.setPackage(str);
            return this;
        }

        @Override // androidx.navigation.d
        public String toString() {
            ComponentName J = J();
            StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            if (J != null) {
                sb.append(" class=");
                sb.append(J.getClassName());
            } else {
                String I = I();
                if (I != null) {
                    sb.append(" action=");
                    sb.append(I);
                }
            }
            return sb.toString();
        }
    }

    /* compiled from: ActivityNavigator.java */
    /* loaded from: classes.dex */
    public static final class b implements i.a {
        public final int a;
        public final o7 b;

        public o7 a() {
            return this.b;
        }

        public int b() {
            return this.a;
        }
    }

    public a(Context context) {
        this.a = context;
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                this.b = (Activity) context;
                return;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
    }

    @Override // androidx.navigation.i
    public boolean e() {
        Activity activity = this.b;
        if (activity != null) {
            activity.finish();
            return true;
        }
        return false;
    }

    @Override // androidx.navigation.i
    /* renamed from: f */
    public C0049a a() {
        return new C0049a(this);
    }

    public final Context g() {
        return this.a;
    }

    @Override // androidx.navigation.i
    /* renamed from: h */
    public d b(C0049a c0049a, Bundle bundle, h hVar, i.a aVar) {
        Intent intent;
        int intExtra;
        if (c0049a.L() != null) {
            Intent intent2 = new Intent(c0049a.L());
            if (bundle != null) {
                intent2.putExtras(bundle);
                String K = c0049a.K();
                if (!TextUtils.isEmpty(K)) {
                    StringBuffer stringBuffer = new StringBuffer();
                    Matcher matcher = Pattern.compile("\\{(.+?)\\}").matcher(K);
                    while (matcher.find()) {
                        String group = matcher.group(1);
                        if (bundle.containsKey(group)) {
                            matcher.appendReplacement(stringBuffer, "");
                            stringBuffer.append(Uri.encode(bundle.get(group).toString()));
                        } else {
                            throw new IllegalArgumentException("Could not find " + group + " in " + bundle + " to fill data pattern " + K);
                        }
                    }
                    matcher.appendTail(stringBuffer);
                    intent2.setData(Uri.parse(stringBuffer.toString()));
                }
            }
            boolean z = aVar instanceof b;
            if (z) {
                intent2.addFlags(((b) aVar).b());
            }
            if (!(this.a instanceof Activity)) {
                intent2.addFlags(268435456);
            }
            if (hVar != null && hVar.g()) {
                intent2.addFlags(536870912);
            }
            Activity activity = this.b;
            if (activity != null && (intent = activity.getIntent()) != null && (intExtra = intent.getIntExtra("android-support-navigation:ActivityNavigator:current", 0)) != 0) {
                intent2.putExtra("android-support-navigation:ActivityNavigator:source", intExtra);
            }
            intent2.putExtra("android-support-navigation:ActivityNavigator:current", c0049a.s());
            Resources resources = g().getResources();
            if (hVar != null) {
                int c = hVar.c();
                int d = hVar.d();
                if ((c > 0 && resources.getResourceTypeName(c).equals("animator")) || (d > 0 && resources.getResourceTypeName(d).equals("animator"))) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Activity destinations do not support Animator resource. Ignoring popEnter resource ");
                    sb.append(resources.getResourceName(c));
                    sb.append(" and popExit resource ");
                    sb.append(resources.getResourceName(d));
                    sb.append("when launching ");
                    sb.append(c0049a);
                } else {
                    intent2.putExtra("android-support-navigation:ActivityNavigator:popEnterAnim", c);
                    intent2.putExtra("android-support-navigation:ActivityNavigator:popExitAnim", d);
                }
            }
            if (z) {
                ((b) aVar).a();
                this.a.startActivity(intent2);
            } else {
                this.a.startActivity(intent2);
            }
            if (hVar == null || this.b == null) {
                return null;
            }
            int a = hVar.a();
            int b2 = hVar.b();
            if ((a <= 0 || !resources.getResourceTypeName(a).equals("animator")) && (b2 <= 0 || !resources.getResourceTypeName(b2).equals("animator"))) {
                if (a >= 0 || b2 >= 0) {
                    this.b.overridePendingTransition(Math.max(a, 0), Math.max(b2, 0));
                    return null;
                }
                return null;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Activity destinations do not support Animator resource. Ignoring enter resource ");
            sb2.append(resources.getResourceName(a));
            sb2.append(" and exit resource ");
            sb2.append(resources.getResourceName(b2));
            sb2.append("when launching ");
            sb2.append(c0049a);
            return null;
        }
        throw new IllegalStateException("Destination " + c0049a.s() + " does not have an Intent set.");
    }
}
