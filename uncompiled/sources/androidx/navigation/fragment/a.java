package androidx.navigation.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.d;
import androidx.navigation.i;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: FragmentNavigator.java */
@i.b("fragment")
/* loaded from: classes.dex */
public class a extends i<C0051a> {
    public final Context a;
    public final FragmentManager b;
    public final int c;
    public ArrayDeque<Integer> d = new ArrayDeque<>();

    /* compiled from: FragmentNavigator.java */
    /* renamed from: androidx.navigation.fragment.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0051a extends d {
        public String m0;

        public C0051a(i<? extends C0051a> iVar) {
            super(iVar);
        }

        @Override // androidx.navigation.d
        public void B(Context context, AttributeSet attributeSet) {
            super.B(context, attributeSet);
            TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, d23.FragmentNavigator);
            String string = obtainAttributes.getString(d23.FragmentNavigator_android_name);
            if (string != null) {
                J(string);
            }
            obtainAttributes.recycle();
        }

        public final String I() {
            String str = this.m0;
            if (str != null) {
                return str;
            }
            throw new IllegalStateException("Fragment class was not set");
        }

        public final C0051a J(String str) {
            this.m0 = str;
            return this;
        }

        @Override // androidx.navigation.d
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append(" class=");
            String str = this.m0;
            if (str == null) {
                sb.append("null");
            } else {
                sb.append(str);
            }
            return sb.toString();
        }
    }

    /* compiled from: FragmentNavigator.java */
    /* loaded from: classes.dex */
    public static final class b implements i.a {
        public final LinkedHashMap<View, String> a;

        public Map<View, String> a() {
            return Collections.unmodifiableMap(this.a);
        }
    }

    public a(Context context, FragmentManager fragmentManager, int i) {
        this.a = context;
        this.b = fragmentManager;
        this.c = i;
    }

    @Override // androidx.navigation.i
    public void c(Bundle bundle) {
        int[] intArray;
        if (bundle == null || (intArray = bundle.getIntArray("androidx-nav-fragment:navigator:backStackIds")) == null) {
            return;
        }
        this.d.clear();
        for (int i : intArray) {
            this.d.add(Integer.valueOf(i));
        }
    }

    @Override // androidx.navigation.i
    public Bundle d() {
        Bundle bundle = new Bundle();
        int[] iArr = new int[this.d.size()];
        Iterator<Integer> it = this.d.iterator();
        int i = 0;
        while (it.hasNext()) {
            iArr[i] = it.next().intValue();
            i++;
        }
        bundle.putIntArray("androidx-nav-fragment:navigator:backStackIds", iArr);
        return bundle;
    }

    @Override // androidx.navigation.i
    public boolean e() {
        if (this.d.isEmpty() || this.b.M0()) {
            return false;
        }
        this.b.a1(g(this.d.size(), this.d.peekLast().intValue()), 1);
        this.d.removeLast();
        return true;
    }

    @Override // androidx.navigation.i
    /* renamed from: f */
    public C0051a a() {
        return new C0051a(this);
    }

    public final String g(int i, int i2) {
        return i + "-" + i2;
    }

    @Deprecated
    public Fragment h(Context context, FragmentManager fragmentManager, String str, Bundle bundle) {
        return fragmentManager.s0().a(context.getClassLoader(), str);
    }

    /* JADX WARN: Removed duplicated region for block: B:56:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0123  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x012d A[RETURN] */
    @Override // androidx.navigation.i
    /* renamed from: i */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public androidx.navigation.d b(androidx.navigation.fragment.a.C0051a r9, android.os.Bundle r10, androidx.navigation.h r11, androidx.navigation.i.a r12) {
        /*
            Method dump skipped, instructions count: 302
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.navigation.fragment.a.b(androidx.navigation.fragment.a$a, android.os.Bundle, androidx.navigation.h, androidx.navigation.i$a):androidx.navigation.d");
    }
}
