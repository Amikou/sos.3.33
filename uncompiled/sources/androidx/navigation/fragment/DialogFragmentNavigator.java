package androidx.navigation.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.AttributeSet;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.e;
import androidx.navigation.d;
import androidx.navigation.h;
import androidx.navigation.i;
import java.util.HashSet;

@i.b("dialog")
/* loaded from: classes.dex */
public final class DialogFragmentNavigator extends i<a> {
    public final Context a;
    public final FragmentManager b;
    public int c = 0;
    public final HashSet<String> d = new HashSet<>();
    public e e = new e(this) { // from class: androidx.navigation.fragment.DialogFragmentNavigator.1
        @Override // androidx.lifecycle.e
        public void e(rz1 rz1Var, Lifecycle.Event event) {
            if (event == Lifecycle.Event.ON_STOP) {
                sn0 sn0Var = (sn0) rz1Var;
                if (sn0Var.q().isShowing()) {
                    return;
                }
                NavHostFragment.f(sn0Var).y();
            }
        }
    };

    /* loaded from: classes.dex */
    public static class a extends d implements g71 {
        public String m0;

        public a(i<? extends a> iVar) {
            super(iVar);
        }

        @Override // androidx.navigation.d
        public void B(Context context, AttributeSet attributeSet) {
            super.B(context, attributeSet);
            TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, d23.DialogFragmentNavigator);
            String string = obtainAttributes.getString(d23.DialogFragmentNavigator_android_name);
            if (string != null) {
                J(string);
            }
            obtainAttributes.recycle();
        }

        public final String I() {
            String str = this.m0;
            if (str != null) {
                return str;
            }
            throw new IllegalStateException("DialogFragment class was not set");
        }

        public final a J(String str) {
            this.m0 = str;
            return this;
        }
    }

    public DialogFragmentNavigator(Context context, FragmentManager fragmentManager) {
        this.a = context;
        this.b = fragmentManager;
    }

    @Override // androidx.navigation.i
    public void c(Bundle bundle) {
        if (bundle != null) {
            this.c = bundle.getInt("androidx-nav-dialogfragment:navigator:count", 0);
            for (int i = 0; i < this.c; i++) {
                FragmentManager fragmentManager = this.b;
                sn0 sn0Var = (sn0) fragmentManager.k0("androidx-nav-fragment:navigator:dialog:" + i);
                if (sn0Var != null) {
                    sn0Var.getLifecycle().a(this.e);
                } else {
                    HashSet<String> hashSet = this.d;
                    hashSet.add("androidx-nav-fragment:navigator:dialog:" + i);
                }
            }
        }
    }

    @Override // androidx.navigation.i
    public Bundle d() {
        if (this.c == 0) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("androidx-nav-dialogfragment:navigator:count", this.c);
        return bundle;
    }

    @Override // androidx.navigation.i
    public boolean e() {
        if (this.c == 0 || this.b.M0()) {
            return false;
        }
        FragmentManager fragmentManager = this.b;
        StringBuilder sb = new StringBuilder();
        sb.append("androidx-nav-fragment:navigator:dialog:");
        int i = this.c - 1;
        this.c = i;
        sb.append(i);
        Fragment k0 = fragmentManager.k0(sb.toString());
        if (k0 != null) {
            k0.getLifecycle().c(this.e);
            ((sn0) k0).h();
        }
        return true;
    }

    @Override // androidx.navigation.i
    /* renamed from: f */
    public a a() {
        return new a(this);
    }

    @Override // androidx.navigation.i
    /* renamed from: g */
    public d b(a aVar, Bundle bundle, h hVar, i.a aVar2) {
        if (this.b.M0()) {
            return null;
        }
        String I = aVar.I();
        if (I.charAt(0) == '.') {
            I = this.a.getPackageName() + I;
        }
        Fragment a2 = this.b.s0().a(this.a.getClassLoader(), I);
        if (sn0.class.isAssignableFrom(a2.getClass())) {
            sn0 sn0Var = (sn0) a2;
            sn0Var.setArguments(bundle);
            sn0Var.getLifecycle().a(this.e);
            FragmentManager fragmentManager = this.b;
            StringBuilder sb = new StringBuilder();
            sb.append("androidx-nav-fragment:navigator:dialog:");
            int i = this.c;
            this.c = i + 1;
            sb.append(i);
            sn0Var.u(fragmentManager, sb.toString());
            return aVar;
        }
        throw new IllegalArgumentException("Dialog destination " + aVar.I() + " is not an instance of DialogFragment");
    }

    public void h(Fragment fragment) {
        if (this.d.remove(fragment.getTag())) {
            fragment.getLifecycle().a(this.e);
        }
    }
}
