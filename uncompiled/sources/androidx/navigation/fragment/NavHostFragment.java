package androidx.navigation.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.navigation.NavController;
import androidx.navigation.fragment.a;
import androidx.navigation.i;

/* loaded from: classes.dex */
public class NavHostFragment extends Fragment {
    public de2 a;
    public Boolean f0 = null;
    public View g0;
    public int h0;
    public boolean i0;

    public static NavController f(Fragment fragment) {
        for (Fragment fragment2 = fragment; fragment2 != null; fragment2 = fragment2.getParentFragment()) {
            if (fragment2 instanceof NavHostFragment) {
                return ((NavHostFragment) fragment2).h();
            }
            Fragment z0 = fragment2.getParentFragmentManager().z0();
            if (z0 instanceof NavHostFragment) {
                return ((NavHostFragment) z0).h();
            }
        }
        View view = fragment.getView();
        if (view != null) {
            return he2.b(view);
        }
        Dialog k = fragment instanceof sn0 ? ((sn0) fragment).k() : null;
        if (k != null && k.getWindow() != null) {
            return he2.b(k.getWindow().getDecorView());
        }
        throw new IllegalStateException("Fragment " + fragment + " does not have a NavController set");
    }

    @Deprecated
    public i<? extends a.C0051a> e() {
        return new a(requireContext(), getChildFragmentManager(), g());
    }

    public final int g() {
        int id = getId();
        return (id == 0 || id == -1) ? p03.nav_host_fragment_container : id;
    }

    public final NavController h() {
        de2 de2Var = this.a;
        if (de2Var != null) {
            return de2Var;
        }
        throw new IllegalStateException("NavController is not available before onCreate()");
    }

    public void i(NavController navController) {
        navController.m().a(new DialogFragmentNavigator(requireContext(), getChildFragmentManager()));
        navController.m().a(e());
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (this.i0) {
            getParentFragmentManager().n().v(this).j();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        ((DialogFragmentNavigator) this.a.m().d(DialogFragmentNavigator.class)).h(fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        Bundle bundle2;
        de2 de2Var = new de2(requireContext());
        this.a = de2Var;
        de2Var.I(this);
        this.a.J(requireActivity().getOnBackPressedDispatcher());
        de2 de2Var2 = this.a;
        Boolean bool = this.f0;
        de2Var2.c(bool != null && bool.booleanValue());
        this.f0 = null;
        this.a.K(getViewModelStore());
        i(this.a);
        if (bundle != null) {
            bundle2 = bundle.getBundle("android-support-nav:fragment:navControllerState");
            if (bundle.getBoolean("android-support-nav:fragment:defaultHost", false)) {
                this.i0 = true;
                getParentFragmentManager().n().v(this).j();
            }
            this.h0 = bundle.getInt("android-support-nav:fragment:graphId");
        } else {
            bundle2 = null;
        }
        if (bundle2 != null) {
            this.a.C(bundle2);
        }
        int i = this.h0;
        if (i != 0) {
            this.a.E(i);
        } else {
            Bundle arguments = getArguments();
            int i2 = arguments != null ? arguments.getInt("android-support-nav:fragment:graphId") : 0;
            Bundle bundle3 = arguments != null ? arguments.getBundle("android-support-nav:fragment:startDestinationArgs") : null;
            if (i2 != 0) {
                this.a.F(i2, bundle3);
            }
        }
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FragmentContainerView fragmentContainerView = new FragmentContainerView(layoutInflater.getContext());
        fragmentContainerView.setId(g());
        return fragmentContainerView;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        View view = this.g0;
        if (view != null && he2.b(view) == this.a) {
            he2.e(this.g0, null);
        }
        this.g0 = null;
    }

    @Override // androidx.fragment.app.Fragment
    public void onInflate(Context context, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(context, attributeSet, bundle);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, c33.NavHost);
        int resourceId = obtainStyledAttributes.getResourceId(c33.NavHost_navGraph, 0);
        if (resourceId != 0) {
            this.h0 = resourceId;
        }
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, d23.NavHostFragment);
        if (obtainStyledAttributes2.getBoolean(d23.NavHostFragment_defaultNavHost, false)) {
            this.i0 = true;
        }
        obtainStyledAttributes2.recycle();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPrimaryNavigationFragmentChanged(boolean z) {
        de2 de2Var = this.a;
        if (de2Var != null) {
            de2Var.c(z);
        } else {
            this.f0 = Boolean.valueOf(z);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Bundle D = this.a.D();
        if (D != null) {
            bundle.putBundle("android-support-nav:fragment:navControllerState", D);
        }
        if (this.i0) {
            bundle.putBoolean("android-support-nav:fragment:defaultHost", true);
        }
        int i = this.h0;
        if (i != 0) {
            bundle.putInt("android-support-nav:fragment:graphId", i);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (view instanceof ViewGroup) {
            he2.e(view, this.a);
            if (view.getParent() != null) {
                View view2 = (View) view.getParent();
                this.g0 = view2;
                if (view2.getId() == getId()) {
                    he2.e(this.g0, this.a);
                    return;
                }
                return;
            }
            return;
        }
        throw new IllegalStateException("created host view " + view + " is not a ViewGroup");
    }
}
