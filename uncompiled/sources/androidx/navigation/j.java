package androidx.navigation;

import android.annotation.SuppressLint;
import androidx.navigation.i;
import java.util.HashMap;
import java.util.Map;

/* compiled from: NavigatorProvider.java */
@SuppressLint({"TypeParameterUnusedInFormals"})
/* loaded from: classes.dex */
public class j {
    public static final HashMap<Class<?>, String> b = new HashMap<>();
    public final HashMap<String, i<? extends d>> a = new HashMap<>();

    public static String c(Class<? extends i> cls) {
        HashMap<Class<?>, String> hashMap = b;
        String str = hashMap.get(cls);
        if (str == null) {
            i.b bVar = (i.b) cls.getAnnotation(i.b.class);
            str = bVar != null ? bVar.value() : null;
            if (g(str)) {
                hashMap.put(cls, str);
            } else {
                throw new IllegalArgumentException("No @Navigator.Name annotation found for " + cls.getSimpleName());
            }
        }
        return str;
    }

    public static boolean g(String str) {
        return (str == null || str.isEmpty()) ? false : true;
    }

    public final i<? extends d> a(i<? extends d> iVar) {
        return b(c(iVar.getClass()), iVar);
    }

    public i<? extends d> b(String str, i<? extends d> iVar) {
        if (g(str)) {
            return this.a.put(str, iVar);
        }
        throw new IllegalArgumentException("navigator name cannot be an empty string");
    }

    public final <T extends i<?>> T d(Class<T> cls) {
        return (T) e(c(cls));
    }

    public <T extends i<?>> T e(String str) {
        if (g(str)) {
            i<? extends d> iVar = this.a.get(str);
            if (iVar != null) {
                return iVar;
            }
            throw new IllegalStateException("Could not find Navigator with name \"" + str + "\". You must call NavController.addNavigator() for each navigation type.");
        }
        throw new IllegalArgumentException("navigator name cannot be an empty string");
    }

    public Map<String, i<? extends d>> f() {
        return this.a;
    }
}
