package androidx.navigation;

import android.os.Bundle;
import androidx.navigation.i;

/* compiled from: NavGraphNavigator.java */
@i.b("navigation")
/* loaded from: classes.dex */
public class f extends i<e> {
    public final j a;

    public f(j jVar) {
        this.a = jVar;
    }

    @Override // androidx.navigation.i
    public boolean e() {
        return true;
    }

    @Override // androidx.navigation.i
    /* renamed from: f */
    public e a() {
        return new e(this);
    }

    @Override // androidx.navigation.i
    /* renamed from: g */
    public d b(e eVar, Bundle bundle, h hVar, i.a aVar) {
        int N = eVar.N();
        if (N != 0) {
            d K = eVar.K(N, false);
            if (K != null) {
                return this.a.e(K.w()).b(K, K.k(bundle), hVar, aVar);
            }
            String L = eVar.L();
            throw new IllegalArgumentException("navigation destination " + L + " is not a direct child of this NavGraph");
        }
        throw new IllegalStateException("no start destination defined via app:startDestination for " + eVar.p());
    }
}
