package androidx.navigation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.navigation.d;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: NavGraph.java */
/* loaded from: classes.dex */
public class e extends d implements Iterable<d> {
    public final lr3<d> m0;
    public int n0;
    public String o0;

    /* compiled from: NavGraph.java */
    /* loaded from: classes.dex */
    public class a implements Iterator<d> {
        public int a = -1;
        public boolean f0 = false;

        public a() {
        }

        @Override // java.util.Iterator
        /* renamed from: a */
        public d next() {
            if (hasNext()) {
                this.f0 = true;
                lr3<d> lr3Var = e.this.m0;
                int i = this.a + 1;
                this.a = i;
                return lr3Var.p(i);
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.a + 1 < e.this.m0.o();
        }

        @Override // java.util.Iterator
        public void remove() {
            if (this.f0) {
                e.this.m0.p(this.a).G(null);
                e.this.m0.l(this.a);
                this.a--;
                this.f0 = false;
                return;
            }
            throw new IllegalStateException("You must call next() before you can remove an element");
        }
    }

    public e(i<? extends e> iVar) {
        super(iVar);
        this.m0 = new lr3<>();
    }

    @Override // androidx.navigation.d
    public void B(Context context, AttributeSet attributeSet) {
        super.B(context, attributeSet);
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, c23.NavGraphNavigator);
        O(obtainAttributes.getResourceId(c23.NavGraphNavigator_startDestination, 0));
        this.o0 = d.q(context, this.n0);
        obtainAttributes.recycle();
    }

    public final void I(d dVar) {
        int s = dVar.s();
        if (s != 0) {
            if (s != s()) {
                d f = this.m0.f(s);
                if (f == dVar) {
                    return;
                }
                if (dVar.y() == null) {
                    if (f != null) {
                        f.G(null);
                    }
                    dVar.G(this);
                    this.m0.k(dVar.s(), dVar);
                    return;
                }
                throw new IllegalStateException("Destination already has a parent set. Call NavGraph.remove() to remove the previous parent.");
            }
            throw new IllegalArgumentException("Destination " + dVar + " cannot have the same id as graph " + this);
        }
        throw new IllegalArgumentException("Destinations must have an id. Call setId() or include an android:id in your navigation XML.");
    }

    public final d J(int i) {
        return K(i, true);
    }

    public final d K(int i, boolean z) {
        d f = this.m0.f(i);
        if (f != null) {
            return f;
        }
        if (!z || y() == null) {
            return null;
        }
        return y().J(i);
    }

    public String L() {
        if (this.o0 == null) {
            this.o0 = Integer.toString(this.n0);
        }
        return this.o0;
    }

    public final int N() {
        return this.n0;
    }

    public final void O(int i) {
        if (i != s()) {
            this.n0 = i;
            this.o0 = null;
            return;
        }
        throw new IllegalArgumentException("Start destination " + i + " cannot use the same id as the graph " + this);
    }

    @Override // java.lang.Iterable
    public final Iterator<d> iterator() {
        return new a();
    }

    @Override // androidx.navigation.d
    public String p() {
        return s() != 0 ? super.p() : "the root navigation";
    }

    @Override // androidx.navigation.d
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(" startDestination=");
        d J = J(N());
        if (J == null) {
            String str = this.o0;
            if (str == null) {
                sb.append("0x");
                sb.append(Integer.toHexString(this.n0));
            } else {
                sb.append(str);
            }
        } else {
            sb.append("{");
            sb.append(J.toString());
            sb.append("}");
        }
        return sb.toString();
    }

    @Override // androidx.navigation.d
    public d.a z(be2 be2Var) {
        d.a z = super.z(be2Var);
        Iterator<d> it = iterator();
        while (it.hasNext()) {
            d.a z2 = it.next().z(be2Var);
            if (z2 != null && (z == null || z2.compareTo(z) > 0)) {
                z = z2;
            }
        }
        return z;
    }
}
