package androidx.navigation;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.UUID;

/* JADX INFO: Access modifiers changed from: package-private */
@SuppressLint({"BanParcelableUsage"})
/* loaded from: classes.dex */
public final class NavBackStackEntryState implements Parcelable {
    public static final Parcelable.Creator<NavBackStackEntryState> CREATOR = new a();
    public final UUID a;
    public final int f0;
    public final Bundle g0;
    public final Bundle h0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<NavBackStackEntryState> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public NavBackStackEntryState createFromParcel(Parcel parcel) {
            return new NavBackStackEntryState(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public NavBackStackEntryState[] newArray(int i) {
            return new NavBackStackEntryState[i];
        }
    }

    public NavBackStackEntryState(xd2 xd2Var) {
        this.a = xd2Var.j0;
        this.f0 = xd2Var.b().s();
        this.g0 = xd2Var.a();
        Bundle bundle = new Bundle();
        this.h0 = bundle;
        xd2Var.h(bundle);
    }

    public Bundle a() {
        return this.g0;
    }

    public int b() {
        return this.f0;
    }

    public Bundle c() {
        return this.h0;
    }

    public UUID d() {
        return this.a;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a.toString());
        parcel.writeInt(this.f0);
        parcel.writeBundle(this.g0);
        parcel.writeBundle(this.h0);
    }

    public NavBackStackEntryState(Parcel parcel) {
        this.a = UUID.fromString(parcel.readString());
        this.f0 = parcel.readInt();
        this.g0 = parcel.readBundle(NavBackStackEntryState.class.getClassLoader());
        this.h0 = parcel.readBundle(NavBackStackEntryState.class.getClassLoader());
    }
}
