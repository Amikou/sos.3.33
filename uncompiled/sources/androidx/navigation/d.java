package androidx.navigation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: NavDestination.java */
/* loaded from: classes.dex */
public class d {
    public final String a;
    public e f0;
    public int g0;
    public String h0;
    public CharSequence i0;
    public ArrayList<c> j0;
    public lr3<wd2> k0;
    public HashMap<String, b> l0;

    /* compiled from: NavDestination.java */
    /* loaded from: classes.dex */
    public static class a implements Comparable<a> {
        public final d a;
        public final Bundle f0;
        public final boolean g0;
        public final boolean h0;
        public final int i0;

        public a(d dVar, Bundle bundle, boolean z, boolean z2, int i) {
            this.a = dVar;
            this.f0 = bundle;
            this.g0 = z;
            this.h0 = z2;
            this.i0 = i;
        }

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(a aVar) {
            boolean z = this.g0;
            if (!z || aVar.g0) {
                if (z || !aVar.g0) {
                    Bundle bundle = this.f0;
                    if (bundle == null || aVar.f0 != null) {
                        if (bundle != null || aVar.f0 == null) {
                            if (bundle != null) {
                                int size = bundle.size() - aVar.f0.size();
                                if (size > 0) {
                                    return 1;
                                }
                                if (size < 0) {
                                    return -1;
                                }
                            }
                            boolean z2 = this.h0;
                            if (!z2 || aVar.h0) {
                                if (z2 || !aVar.h0) {
                                    return this.i0 - aVar.i0;
                                }
                                return -1;
                            }
                            return 1;
                        }
                        return -1;
                    }
                    return 1;
                }
                return -1;
            }
            return 1;
        }

        public d d() {
            return this.a;
        }

        public Bundle e() {
            return this.f0;
        }
    }

    static {
        new HashMap();
    }

    public d(i<? extends d> iVar) {
        this(j.c(iVar.getClass()));
    }

    public static String q(Context context, int i) {
        if (i <= 16777215) {
            return Integer.toString(i);
        }
        try {
            return context.getResources().getResourceName(i);
        } catch (Resources.NotFoundException unused) {
            return Integer.toString(i);
        }
    }

    public void B(Context context, AttributeSet attributeSet) {
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, c23.Navigator);
        E(obtainAttributes.getResourceId(c23.Navigator_android_id, 0));
        this.h0 = q(context, this.g0);
        F(obtainAttributes.getText(c23.Navigator_android_label));
        obtainAttributes.recycle();
    }

    public final void D(int i, wd2 wd2Var) {
        if (H()) {
            if (i != 0) {
                if (this.k0 == null) {
                    this.k0 = new lr3<>();
                }
                this.k0.k(i, wd2Var);
                return;
            }
            throw new IllegalArgumentException("Cannot have an action with actionId 0");
        }
        throw new UnsupportedOperationException("Cannot add action " + i + " to " + this + " as it does not support actions, indicating that it is a terminal destination in your navigation graph and will never trigger actions.");
    }

    public final void E(int i) {
        this.g0 = i;
        this.h0 = null;
    }

    public final void F(CharSequence charSequence) {
        this.i0 = charSequence;
    }

    public final void G(e eVar) {
        this.f0 = eVar;
    }

    public boolean H() {
        return true;
    }

    public final void e(String str, b bVar) {
        if (this.l0 == null) {
            this.l0 = new HashMap<>();
        }
        this.l0.put(str, bVar);
    }

    public final void i(c cVar) {
        if (this.j0 == null) {
            this.j0 = new ArrayList<>();
        }
        this.j0.add(cVar);
    }

    public Bundle k(Bundle bundle) {
        HashMap<String, b> hashMap;
        if (bundle == null && ((hashMap = this.l0) == null || hashMap.isEmpty())) {
            return null;
        }
        Bundle bundle2 = new Bundle();
        HashMap<String, b> hashMap2 = this.l0;
        if (hashMap2 != null) {
            for (Map.Entry<String, b> entry : hashMap2.entrySet()) {
                entry.getValue().c(entry.getKey(), bundle2);
            }
        }
        if (bundle != null) {
            bundle2.putAll(bundle);
            HashMap<String, b> hashMap3 = this.l0;
            if (hashMap3 != null) {
                for (Map.Entry<String, b> entry2 : hashMap3.entrySet()) {
                    if (!entry2.getValue().d(entry2.getKey(), bundle2)) {
                        throw new IllegalArgumentException("Wrong argument type for '" + entry2.getKey() + "' in argument bundle. " + entry2.getValue().a().c() + " expected.");
                    }
                }
            }
        }
        return bundle2;
    }

    public int[] m() {
        ArrayDeque arrayDeque = new ArrayDeque();
        d dVar = this;
        while (true) {
            e y = dVar.y();
            if (y == null || y.N() != dVar.s()) {
                arrayDeque.addFirst(dVar);
            }
            if (y == null) {
                break;
            }
            dVar = y;
        }
        int[] iArr = new int[arrayDeque.size()];
        int i = 0;
        Iterator it = arrayDeque.iterator();
        while (it.hasNext()) {
            iArr[i] = ((d) it.next()).s();
            i++;
        }
        return iArr;
    }

    public final wd2 n(int i) {
        lr3<wd2> lr3Var = this.k0;
        wd2 f = lr3Var == null ? null : lr3Var.f(i);
        if (f != null) {
            return f;
        }
        if (y() != null) {
            return y().n(i);
        }
        return null;
    }

    public final Map<String, b> o() {
        HashMap<String, b> hashMap = this.l0;
        return hashMap == null ? Collections.emptyMap() : Collections.unmodifiableMap(hashMap);
    }

    public String p() {
        if (this.h0 == null) {
            this.h0 = Integer.toString(this.g0);
        }
        return this.h0;
    }

    public final int s() {
        return this.g0;
    }

    public final CharSequence t() {
        return this.i0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("(");
        String str = this.h0;
        if (str == null) {
            sb.append("0x");
            sb.append(Integer.toHexString(this.g0));
        } else {
            sb.append(str);
        }
        sb.append(")");
        if (this.i0 != null) {
            sb.append(" label=");
            sb.append(this.i0);
        }
        return sb.toString();
    }

    public final String w() {
        return this.a;
    }

    public final e y() {
        return this.f0;
    }

    public a z(be2 be2Var) {
        ArrayList<c> arrayList = this.j0;
        if (arrayList == null) {
            return null;
        }
        Iterator<c> it = arrayList.iterator();
        a aVar = null;
        while (it.hasNext()) {
            c next = it.next();
            Uri c = be2Var.c();
            Bundle c2 = c != null ? next.c(c, o()) : null;
            String a2 = be2Var.a();
            boolean z = a2 != null && a2.equals(next.b());
            String b = be2Var.b();
            int d = b != null ? next.d(b) : -1;
            if (c2 != null || z || d > -1) {
                a aVar2 = new a(this, c2, next.e(), z, d);
                if (aVar == null || aVar2.compareTo(aVar) > 0) {
                    aVar = aVar2;
                }
            }
        }
        return aVar;
    }

    public d(String str) {
        this.a = str;
    }
}
