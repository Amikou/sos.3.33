package androidx.navigation;

import android.os.Bundle;

/* compiled from: NavArgument.java */
/* loaded from: classes.dex */
public final class b {
    public final ee2 a;
    public final boolean b;
    public final boolean c;
    public final Object d;

    /* compiled from: NavArgument.java */
    /* loaded from: classes.dex */
    public static final class a {
        public ee2<?> a;
        public Object c;
        public boolean b = false;
        public boolean d = false;

        public b a() {
            if (this.a == null) {
                this.a = ee2.e(this.c);
            }
            return new b(this.a, this.b, this.c, this.d);
        }

        public a b(Object obj) {
            this.c = obj;
            this.d = true;
            return this;
        }

        public a c(boolean z) {
            this.b = z;
            return this;
        }

        public a d(ee2<?> ee2Var) {
            this.a = ee2Var;
            return this;
        }
    }

    public b(ee2<?> ee2Var, boolean z, Object obj, boolean z2) {
        if (!ee2Var.f() && z) {
            throw new IllegalArgumentException(ee2Var.c() + " does not allow nullable values");
        } else if (!z && z2 && obj == null) {
            throw new IllegalArgumentException("Argument with type " + ee2Var.c() + " has null value but is not nullable.");
        } else {
            this.a = ee2Var;
            this.b = z;
            this.d = obj;
            this.c = z2;
        }
    }

    public ee2<?> a() {
        return this.a;
    }

    public boolean b() {
        return this.c;
    }

    public void c(String str, Bundle bundle) {
        if (this.c) {
            this.a.i(bundle, str, this.d);
        }
    }

    public boolean d(String str, Bundle bundle) {
        if (!this.b && bundle.containsKey(str) && bundle.get(str) == null) {
            return false;
        }
        try {
            this.a.b(bundle, str);
            return true;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || b.class != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (this.b == bVar.b && this.c == bVar.c && this.a.equals(bVar.a)) {
            Object obj2 = this.d;
            return obj2 != null ? obj2.equals(bVar.d) : bVar.d == null;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = ((((this.a.hashCode() * 31) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31;
        Object obj = this.d;
        return hashCode + (obj != null ? obj.hashCode() : 0);
    }
}
