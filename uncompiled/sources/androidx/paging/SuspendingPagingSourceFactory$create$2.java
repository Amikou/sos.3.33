package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SuspendingPagingSourceFactory.kt */
@a(c = "androidx.paging.SuspendingPagingSourceFactory$create$2", f = "SuspendingPagingSourceFactory.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class SuspendingPagingSourceFactory$create$2 extends SuspendLambda implements hd1<c90, q70<? super gp2<Key, Value>>, Object> {
    public int label;
    public final /* synthetic */ SuspendingPagingSourceFactory this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SuspendingPagingSourceFactory$create$2(SuspendingPagingSourceFactory suspendingPagingSourceFactory, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = suspendingPagingSourceFactory;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new SuspendingPagingSourceFactory$create$2(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, Object obj) {
        return ((SuspendingPagingSourceFactory$create$2) create(c90Var, (q70) obj)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        rc1 rc1Var;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            rc1Var = this.this$0.f0;
            return rc1Var.invoke();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
