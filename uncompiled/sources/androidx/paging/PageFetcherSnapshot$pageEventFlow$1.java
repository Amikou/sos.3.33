package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PageFetcherSnapshot.kt */
@a(c = "androidx.paging.PageFetcherSnapshot$pageEventFlow$1", f = "PageFetcherSnapshot.kt", l = {595, 160, 607}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcherSnapshot$pageEventFlow$1 extends SuspendLambda implements hd1<lp3<yo2<Value>>, q70<? super te4>, Object> {
    private /* synthetic */ Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public final /* synthetic */ PageFetcherSnapshot this$0;

    /* compiled from: PageFetcherSnapshot.kt */
    @a(c = "androidx.paging.PageFetcherSnapshot$pageEventFlow$1$2", f = "PageFetcherSnapshot.kt", l = {589}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcherSnapshot$pageEventFlow$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ lp3 $this_cancelableChannelFlow;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(lp3 lp3Var, q70 q70Var) {
            super(2, q70Var);
            this.$this_cancelableChannelFlow = lp3Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            return new AnonymousClass2(this.$this_cancelableChannelFlow, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass2) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            kx kxVar;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                kxVar = PageFetcherSnapshot$pageEventFlow$1.this.this$0.d;
                j71 h = n71.h(kxVar);
                PageFetcherSnapshot$pageEventFlow$1$2$invokeSuspend$$inlined$collect$1 pageFetcherSnapshot$pageEventFlow$1$2$invokeSuspend$$inlined$collect$1 = new PageFetcherSnapshot$pageEventFlow$1$2$invokeSuspend$$inlined$collect$1(this);
                this.label = 1;
                if (h.a(pageFetcherSnapshot$pageEventFlow$1$2$invokeSuspend$$inlined$collect$1, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* compiled from: PageFetcherSnapshot.kt */
    @kotlin.coroutines.jvm.internal.a(c = "androidx.paging.PageFetcherSnapshot$pageEventFlow$1$3", f = "PageFetcherSnapshot.kt", l = {589}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcherSnapshot$pageEventFlow$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ kx $retryChannel;
        public int label;

        /* compiled from: Collect.kt */
        /* renamed from: androidx.paging.PageFetcherSnapshot$pageEventFlow$1$3$a */
        /* loaded from: classes.dex */
        public static final class a implements k71<te4> {
            public a() {
            }

            @Override // defpackage.k71
            public Object emit(te4 te4Var, q70 q70Var) {
                AnonymousClass3.this.$retryChannel.offer(te4Var);
                return te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass3(kx kxVar, q70 q70Var) {
            super(2, q70Var);
            this.$retryChannel = kxVar;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            return new AnonymousClass3(this.$retryChannel, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass3) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            j71 j71Var;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71Var = PageFetcherSnapshot$pageEventFlow$1.this.this$0.k;
                a aVar = new a();
                this.label = 1;
                if (j71Var.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* compiled from: PageFetcherSnapshot.kt */
    @a(c = "androidx.paging.PageFetcherSnapshot$pageEventFlow$1$4", f = "PageFetcherSnapshot.kt", l = {589}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcherSnapshot$pageEventFlow$1$4  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass4 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ kx $retryChannel;
        private /* synthetic */ Object L$0;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass4(kx kxVar, q70 q70Var) {
            super(2, q70Var);
            this.$retryChannel = kxVar;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            AnonymousClass4 anonymousClass4 = new AnonymousClass4(this.$retryChannel, q70Var);
            anonymousClass4.L$0 = obj;
            return anonymousClass4;
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass4) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 h = n71.h(this.$retryChannel);
                PageFetcherSnapshot$pageEventFlow$1$4$invokeSuspend$$inlined$collect$1 pageFetcherSnapshot$pageEventFlow$1$4$invokeSuspend$$inlined$collect$1 = new PageFetcherSnapshot$pageEventFlow$1$4$invokeSuspend$$inlined$collect$1(this, (c90) this.L$0);
                this.label = 1;
                if (h.a(pageFetcherSnapshot$pageEventFlow$1$4$invokeSuspend$$inlined$collect$1, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcherSnapshot$pageEventFlow$1(PageFetcherSnapshot pageFetcherSnapshot, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = pageFetcherSnapshot;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        PageFetcherSnapshot$pageEventFlow$1 pageFetcherSnapshot$pageEventFlow$1 = new PageFetcherSnapshot$pageEventFlow$1(this.this$0, q70Var);
        pageFetcherSnapshot$pageEventFlow$1.L$0 = obj;
        return pageFetcherSnapshot$pageEventFlow$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((PageFetcherSnapshot$pageEventFlow$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x00e0 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00f9 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00fa  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0111  */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
            Method dump skipped, instructions count: 298
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcherSnapshot$pageEventFlow$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
