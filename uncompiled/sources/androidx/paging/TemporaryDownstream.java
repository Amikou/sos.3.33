package androidx.paging;

import defpackage.uj3;

/* compiled from: CachedPageEventFlow.kt */
/* loaded from: classes.dex */
public final class TemporaryDownstream<T> {
    public final kx<lq1<yo2<T>>> a = rx.b(Integer.MAX_VALUE, null, null, 6, null);

    public final void a() {
        uj3.a.a(this.a, null, 1, null);
    }

    public final j71<lq1<yo2<T>>> b() {
        return n71.h(this.a);
    }

    /* JADX WARN: Can't wrap try/catch for region: R(9:1|(2:3|(7:5|6|7|(1:(1:10)(2:16|17))(3:18|19|(1:21))|11|12|13))|23|6|7|(0)(0)|11|12|13) */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x003f, code lost:
        r3 = false;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0031  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object c(defpackage.lq1<? extends defpackage.yo2<T>> r5, defpackage.q70<? super java.lang.Boolean> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof androidx.paging.TemporaryDownstream$send$1
            if (r0 == 0) goto L13
            r0 = r6
            androidx.paging.TemporaryDownstream$send$1 r0 = (androidx.paging.TemporaryDownstream$send$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.TemporaryDownstream$send$1 r0 = new androidx.paging.TemporaryDownstream$send$1
            r0.<init>(r4, r6)
        L18:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L31
            if (r2 != r3) goto L29
            defpackage.o83.b(r6)     // Catch: kotlinx.coroutines.channels.ClosedSendChannelException -> L3f
            goto L40
        L29:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L31:
            defpackage.o83.b(r6)
            kx<lq1<yo2<T>>> r6 = r4.a     // Catch: kotlinx.coroutines.channels.ClosedSendChannelException -> L3f
            r0.label = r3     // Catch: kotlinx.coroutines.channels.ClosedSendChannelException -> L3f
            java.lang.Object r5 = r6.h(r5, r0)     // Catch: kotlinx.coroutines.channels.ClosedSendChannelException -> L3f
            if (r5 != r1) goto L40
            return r1
        L3f:
            r3 = 0
        L40:
            java.lang.Boolean r5 = defpackage.hr.a(r3)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.TemporaryDownstream.c(lq1, q70):java.lang.Object");
    }
}
