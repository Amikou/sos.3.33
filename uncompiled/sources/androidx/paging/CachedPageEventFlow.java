package androidx.paging;

import androidx.paging.multicast.Multicaster;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: CachedPageEventFlow.kt */
/* loaded from: classes.dex */
public final class CachedPageEventFlow<T> {
    public final FlattenedPageController<T> a;
    public final AtomicBoolean b;
    public final Multicaster<lq1<yo2<T>>> c;
    public final j71<yo2<T>> d;

    public CachedPageEventFlow(j71<? extends yo2<T>> j71Var, c90 c90Var) {
        fs1.f(j71Var, "src");
        fs1.f(c90Var, "scope");
        FlattenedPageController<T> flattenedPageController = new FlattenedPageController<>();
        this.a = flattenedPageController;
        this.b = new AtomicBoolean(false);
        this.c = new Multicaster<>(c90Var, 0, n71.n(new CachedPageEventFlow$multicastedSrc$1(this, j71Var, null)), false, new CachedPageEventFlow$multicastedSrc$2(flattenedPageController), true, 8, null);
        this.d = SimpleChannelFlowKt.a(new CachedPageEventFlow$downstreamFlow$1(this, null));
    }

    public final Object d(q70<? super te4> q70Var) {
        Object g = this.c.g(q70Var);
        return g == gs1.d() ? g : te4.a;
    }

    public final j71<yo2<T>> e() {
        return this.d;
    }
}
