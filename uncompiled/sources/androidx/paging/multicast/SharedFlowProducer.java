package androidx.paging.multicast;

import androidx.paging.multicast.ChannelManager;
import defpackage.st1;
import kotlinx.coroutines.CoroutineStart;

/* compiled from: SharedFlowProducer.kt */
/* loaded from: classes.dex */
public final class SharedFlowProducer<T> {
    public final st1 a;
    public final c90 b;
    public final j71<T> c;
    public final hd1<ChannelManager.b.AbstractC0053b<T>, q70<? super te4>, Object> d;

    /* JADX WARN: Multi-variable type inference failed */
    public SharedFlowProducer(c90 c90Var, j71<? extends T> j71Var, hd1<? super ChannelManager.b.AbstractC0053b<T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        st1 b;
        fs1.f(c90Var, "scope");
        fs1.f(j71Var, "src");
        fs1.f(hd1Var, "sendUpsteamMessage");
        this.b = c90Var;
        this.c = j71Var;
        this.d = hd1Var;
        b = as.b(c90Var, null, CoroutineStart.LAZY, new SharedFlowProducer$collectionJob$1(this, null), 1, null);
        this.a = b;
    }

    public final void d() {
        st1.a.a(this.a, null, 1, null);
    }

    public final Object e(q70<? super te4> q70Var) {
        Object g = xt1.g(this.a, q70Var);
        return g == gs1.d() ? g : te4.a;
    }

    public final void f() {
        as.b(this.b, null, null, new SharedFlowProducer$start$1(this, null), 3, null);
    }
}
