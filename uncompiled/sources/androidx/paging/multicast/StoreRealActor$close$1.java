package androidx.paging.multicast;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: StoreRealActor.kt */
@a(c = "androidx.paging.multicast.StoreRealActor", f = "StoreRealActor.kt", l = {74, 76}, m = "close")
/* loaded from: classes.dex */
public final class StoreRealActor$close$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ StoreRealActor this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreRealActor$close$1(StoreRealActor storeRealActor, q70 q70Var) {
        super(q70Var);
        this.this$0 = storeRealActor;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.c(this);
    }
}
