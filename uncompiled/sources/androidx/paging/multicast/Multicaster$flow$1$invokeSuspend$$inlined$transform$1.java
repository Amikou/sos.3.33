package androidx.paging.multicast;

import androidx.paging.multicast.ChannelManager;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Emitters.kt */
@a(c = "androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1", f = "Multicaster.kt", l = {215}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class Multicaster$flow$1$invokeSuspend$$inlined$transform$1 extends SuspendLambda implements hd1<k71<? super T>, q70<? super te4>, Object> {
    public final /* synthetic */ j71 $this_transform;
    private /* synthetic */ Object L$0;
    public int label;

    /* compiled from: Collect.kt */
    /* renamed from: androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 implements k71<ChannelManager.b.AbstractC0053b.c<T>> {
        public final /* synthetic */ k71 a;

        @a(c = "androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1$1", f = "Multicaster.kt", l = {134}, m = "emit")
        /* renamed from: androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C00551 extends ContinuationImpl {
            public Object L$0;
            public int label;
            public /* synthetic */ Object result;

            public C00551(q70 q70Var) {
                super(q70Var);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                this.result = obj;
                this.label |= Integer.MIN_VALUE;
                return AnonymousClass1.this.emit(null, this);
            }
        }

        public AnonymousClass1(Multicaster$flow$1$invokeSuspend$$inlined$transform$1 multicaster$flow$1$invokeSuspend$$inlined$transform$1, k71 k71Var) {
            this.a = k71Var;
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
        @Override // defpackage.k71
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public java.lang.Object emit(java.lang.Object r5, defpackage.q70 r6) {
            /*
                r4 = this;
                boolean r0 = r6 instanceof androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1.AnonymousClass1.C00551
                if (r0 == 0) goto L13
                r0 = r6
                androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1$1$1 r0 = (androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1.AnonymousClass1.C00551) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L13
                int r1 = r1 - r2
                r0.label = r1
                goto L18
            L13:
                androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1$1$1 r0 = new androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1$1$1
                r0.<init>(r6)
            L18:
                java.lang.Object r6 = r0.result
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 1
                if (r2 == 0) goto L35
                if (r2 != r3) goto L2d
                java.lang.Object r5 = r0.L$0
                androidx.paging.multicast.ChannelManager$b$b$c r5 = (androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c) r5
                defpackage.o83.b(r6)
                goto L4b
            L2d:
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
                r5.<init>(r6)
                throw r5
            L35:
                defpackage.o83.b(r6)
                k71 r6 = r4.a
                androidx.paging.multicast.ChannelManager$b$b$c r5 = (androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c) r5
                java.lang.Object r2 = r5.b()
                r0.L$0 = r5
                r0.label = r3
                java.lang.Object r6 = r6.emit(r2, r0)
                if (r6 != r1) goto L4b
                return r1
            L4b:
                n30 r5 = r5.a()
                te4 r6 = defpackage.te4.a
                r5.u(r6)
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.Multicaster$flow$1$invokeSuspend$$inlined$transform$1.AnonymousClass1.emit(java.lang.Object, q70):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Multicaster$flow$1$invokeSuspend$$inlined$transform$1(j71 j71Var, q70 q70Var) {
        super(2, q70Var);
        this.$this_transform = j71Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        Multicaster$flow$1$invokeSuspend$$inlined$transform$1 multicaster$flow$1$invokeSuspend$$inlined$transform$1 = new Multicaster$flow$1$invokeSuspend$$inlined$transform$1(this.$this_transform, q70Var);
        multicaster$flow$1$invokeSuspend$$inlined$transform$1.L$0 = obj;
        return multicaster$flow$1$invokeSuspend$$inlined$transform$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((Multicaster$flow$1$invokeSuspend$$inlined$transform$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            j71 j71Var = this.$this_transform;
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this, (k71) this.L$0);
            this.label = 1;
            if (j71Var.a(anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
