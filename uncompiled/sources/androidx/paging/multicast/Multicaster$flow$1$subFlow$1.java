package androidx.paging.multicast;

import androidx.paging.multicast.ChannelManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Multicaster.kt */
@a(c = "androidx.paging.multicast.Multicaster$flow$1$subFlow$1", f = "Multicaster.kt", l = {78}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class Multicaster$flow$1$subFlow$1 extends SuspendLambda implements hd1<k71<? super ChannelManager.b.AbstractC0053b.c<T>>, q70<? super te4>, Object> {
    public final /* synthetic */ kx $channel;
    public int label;
    public final /* synthetic */ Multicaster$flow$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Multicaster$flow$1$subFlow$1(Multicaster$flow$1 multicaster$flow$1, kx kxVar, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = multicaster$flow$1;
        this.$channel = kxVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new Multicaster$flow$1$subFlow$1(this.this$0, this.$channel, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((Multicaster$flow$1$subFlow$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        ChannelManager h;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            h = this.this$0.this$0.h();
            kx kxVar = this.$channel;
            this.label = 1;
            if (h.g(kxVar, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
