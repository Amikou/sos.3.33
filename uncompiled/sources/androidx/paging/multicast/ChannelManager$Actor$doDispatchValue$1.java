package androidx.paging.multicast;

import androidx.paging.multicast.ChannelManager;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ChannelManager.kt */
@a(c = "androidx.paging.multicast.ChannelManager$Actor", f = "ChannelManager.kt", l = {172, 181}, m = "doDispatchValue")
/* loaded from: classes.dex */
public final class ChannelManager$Actor$doDispatchValue$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ ChannelManager.Actor this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelManager$Actor$doDispatchValue$1(ChannelManager.Actor actor, q70 q70Var) {
        super(q70Var);
        this.this$0 = actor;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.l(null, this);
    }
}
