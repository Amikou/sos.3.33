package androidx.paging.multicast;

import kotlin.jvm.internal.Lambda;

/* compiled from: Multicaster.kt */
/* loaded from: classes.dex */
public final class Multicaster$channelManager$2 extends Lambda implements rc1<ChannelManager<T>> {
    public final /* synthetic */ int $bufferSize;
    public final /* synthetic */ Multicaster this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Multicaster$channelManager$2(Multicaster multicaster, int i) {
        super(0);
        this.this$0 = multicaster;
        this.$bufferSize = i;
    }

    @Override // defpackage.rc1
    public final ChannelManager<T> invoke() {
        c90 c90Var;
        j71 j71Var;
        boolean z;
        hd1 hd1Var;
        boolean z2;
        c90Var = this.this$0.c;
        int i = this.$bufferSize;
        j71Var = this.this$0.d;
        z = this.this$0.e;
        hd1Var = this.this$0.f;
        z2 = this.this$0.g;
        return new ChannelManager<>(c90Var, i, z, hd1Var, z2, j71Var);
    }
}
