package androidx.paging.multicast;

import androidx.paging.multicast.ChannelManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.channels.ClosedSendChannelException;

/* compiled from: SharedFlowProducer.kt */
@a(c = "androidx.paging.multicast.SharedFlowProducer$start$1", f = "SharedFlowProducer.kt", l = {75, 80, 80}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class SharedFlowProducer$start$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public Object L$0;
    public int label;
    public final /* synthetic */ SharedFlowProducer this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SharedFlowProducer$start$1(SharedFlowProducer sharedFlowProducer, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = sharedFlowProducer;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new SharedFlowProducer$start$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SharedFlowProducer$start$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v2, types: [androidx.paging.multicast.SharedFlowProducer] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        hd1 hd1Var;
        st1 st1Var;
        hd1 hd1Var2;
        Object d = gs1.d();
        int i = this.label;
        int i2 = 3;
        try {
            try {
            } catch (Throwable th) {
                try {
                    hd1Var = this.this$0.d;
                    ChannelManager.b.AbstractC0053b.C0054b c0054b = new ChannelManager.b.AbstractC0053b.C0054b(this.this$0);
                    this.L$0 = th;
                    this.label = i2;
                    if (hd1Var.invoke(c0054b, this) == d) {
                        return d;
                    }
                } catch (ClosedSendChannelException unused) {
                }
                throw th;
            }
        } catch (ClosedSendChannelException unused2) {
        }
        if (i == 0) {
            o83.b(obj);
            st1Var = this.this$0.a;
            this.label = 1;
            if (st1Var.F(this) == d) {
                return d;
            }
        } else if (i != 1) {
            if (i == 2) {
                o83.b(obj);
                return te4.a;
            } else if (i != 3) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                Throwable th2 = (Throwable) this.L$0;
                try {
                    o83.b(obj);
                    throw th2;
                } catch (ClosedSendChannelException unused3) {
                    throw th2;
                }
            }
        } else {
            o83.b(obj);
        }
        hd1Var2 = this.this$0.d;
        i2 = this.this$0;
        ChannelManager.b.AbstractC0053b.C0054b c0054b2 = new ChannelManager.b.AbstractC0053b.C0054b(i2);
        this.label = 2;
        if (hd1Var2.invoke(c0054b2, this) == d) {
            return d;
        }
        return te4.a;
    }
}
