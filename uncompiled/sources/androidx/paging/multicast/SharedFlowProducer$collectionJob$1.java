package androidx.paging.multicast;

import androidx.paging.multicast.ChannelManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.channels.ClosedSendChannelException;

/* compiled from: SharedFlowProducer.kt */
@a(c = "androidx.paging.multicast.SharedFlowProducer$collectionJob$1", f = "SharedFlowProducer.kt", l = {97}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class SharedFlowProducer$collectionJob$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SharedFlowProducer this$0;

    /* compiled from: SharedFlowProducer.kt */
    @a(c = "androidx.paging.multicast.SharedFlowProducer$collectionJob$1$1", f = "SharedFlowProducer.kt", l = {50}, m = "invokeSuspend")
    /* renamed from: androidx.paging.multicast.SharedFlowProducer$collectionJob$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements kd1<k71<? super T>, Throwable, q70<? super te4>, Object> {
        private /* synthetic */ Object L$0;
        public int label;

        public AnonymousClass1(q70 q70Var) {
            super(3, q70Var);
        }

        public final q70<te4> create(k71<? super T> k71Var, Throwable th, q70<? super te4> q70Var) {
            fs1.f(k71Var, "$this$create");
            fs1.f(th, "it");
            fs1.f(q70Var, "continuation");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(q70Var);
            anonymousClass1.L$0 = th;
            return anonymousClass1;
        }

        @Override // defpackage.kd1
        public final Object invoke(Object obj, Throwable th, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create((k71) obj, th, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            hd1 hd1Var;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                hd1Var = SharedFlowProducer$collectionJob$1.this.this$0.d;
                ChannelManager.b.AbstractC0053b.a aVar = new ChannelManager.b.AbstractC0053b.a((Throwable) this.L$0);
                this.label = 1;
                if (hd1Var.invoke(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SharedFlowProducer$collectionJob$1(SharedFlowProducer sharedFlowProducer, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = sharedFlowProducer;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new SharedFlowProducer$collectionJob$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SharedFlowProducer$collectionJob$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        j71 j71Var;
        Object d = gs1.d();
        int i = this.label;
        try {
            if (i == 0) {
                o83.b(obj);
                j71Var = this.this$0.c;
                j71 c = n71.c(j71Var, new AnonymousClass1(null));
                SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1 sharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1 = new SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1(this);
                this.label = 1;
                if (c.a(sharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
        } catch (ClosedSendChannelException unused) {
        }
        return te4.a;
    }
}
