package androidx.paging.multicast;

import defpackage.uj3;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ChannelManager.kt */
/* loaded from: classes.dex */
public final class ChannelManager<T> {
    public final ChannelManager<T>.Actor a;
    public final c90 b;
    public final int c;
    public final boolean d;
    public final hd1<T, q70<? super te4>, Object> e;
    public final boolean f;
    public final j71<T> g;

    /* compiled from: ChannelManager.kt */
    /* loaded from: classes.dex */
    public final class Actor extends StoreRealActor<b<T>> {
        public final rr<T> f;
        public SharedFlowProducer<T> g;
        public boolean h;
        public n30<te4> i;
        public final List<a<T>> j;

        public Actor() {
            super(ChannelManager.this.b);
            this.f = sx.b(ChannelManager.this.c);
            this.j = new ArrayList();
        }

        @Override // androidx.paging.multicast.StoreRealActor
        public void f() {
            Iterator<T> it = this.j.iterator();
            while (it.hasNext()) {
                ((a) it.next()).a();
            }
            this.j.clear();
            SharedFlowProducer<T> sharedFlowProducer = this.g;
            if (sharedFlowProducer != null) {
                sharedFlowProducer.d();
            }
        }

        public final void h() {
            if (this.g == null) {
                SharedFlowProducer<T> p = p();
                this.g = p;
                this.h = false;
                fs1.d(p);
                p.f();
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x003a  */
        /* JADX WARN: Removed duplicated region for block: B:33:0x0098  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final /* synthetic */ java.lang.Object i(androidx.paging.multicast.ChannelManager.a<T> r6, defpackage.q70<? super defpackage.te4> r7) {
            /*
                Method dump skipped, instructions count: 241
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.ChannelManager.Actor.i(androidx.paging.multicast.ChannelManager$a, q70):java.lang.Object");
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final /* synthetic */ java.lang.Object j(androidx.paging.multicast.ChannelManager.b.a<T> r7, defpackage.q70<? super defpackage.te4> r8) {
            /*
                r6 = this;
                boolean r0 = r8 instanceof androidx.paging.multicast.ChannelManager$Actor$doAdd$1
                if (r0 == 0) goto L13
                r0 = r8
                androidx.paging.multicast.ChannelManager$Actor$doAdd$1 r0 = (androidx.paging.multicast.ChannelManager$Actor$doAdd$1) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L13
                int r1 = r1 - r2
                r0.label = r1
                goto L18
            L13:
                androidx.paging.multicast.ChannelManager$Actor$doAdd$1 r0 = new androidx.paging.multicast.ChannelManager$Actor$doAdd$1
                r0.<init>(r6, r8)
            L18:
                java.lang.Object r8 = r0.result
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 1
                if (r2 == 0) goto L35
                if (r2 != r3) goto L2d
                java.lang.Object r7 = r0.L$0
                androidx.paging.multicast.ChannelManager$Actor r7 = (androidx.paging.multicast.ChannelManager.Actor) r7
                defpackage.o83.b(r8)
                goto L50
            L2d:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r8)
                throw r7
            L35:
                defpackage.o83.b(r8)
                androidx.paging.multicast.ChannelManager$a r8 = new androidx.paging.multicast.ChannelManager$a
                uj3 r7 = r7.a()
                r2 = 0
                r4 = 2
                r5 = 0
                r8.<init>(r7, r2, r4, r5)
                r0.L$0 = r6
                r0.label = r3
                java.lang.Object r7 = r6.i(r8, r0)
                if (r7 != r1) goto L4f
                return r1
            L4f:
                r7 = r6
            L50:
                r7.h()
                te4 r7 = defpackage.te4.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.ChannelManager.Actor.j(androidx.paging.multicast.ChannelManager$b$a, q70):java.lang.Object");
        }

        public final void k(b.AbstractC0053b.a<T> aVar) {
            this.h = true;
            Iterator<T> it = this.j.iterator();
            while (it.hasNext()) {
                ((a) it.next()).b(aVar.a());
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
        /* JADX WARN: Removed duplicated region for block: B:16:0x0048  */
        /* JADX WARN: Removed duplicated region for block: B:22:0x0072  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x0086  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final /* synthetic */ java.lang.Object l(androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c<T> r6, defpackage.q70<? super defpackage.te4> r7) {
            /*
                r5 = this;
                boolean r0 = r7 instanceof androidx.paging.multicast.ChannelManager$Actor$doDispatchValue$1
                if (r0 == 0) goto L13
                r0 = r7
                androidx.paging.multicast.ChannelManager$Actor$doDispatchValue$1 r0 = (androidx.paging.multicast.ChannelManager$Actor$doDispatchValue$1) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L13
                int r1 = r1 - r2
                r0.label = r1
                goto L18
            L13:
                androidx.paging.multicast.ChannelManager$Actor$doDispatchValue$1 r0 = new androidx.paging.multicast.ChannelManager$Actor$doDispatchValue$1
                r0.<init>(r5, r7)
            L18:
                java.lang.Object r7 = r0.result
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 2
                r4 = 1
                if (r2 == 0) goto L48
                if (r2 == r4) goto L3c
                if (r2 != r3) goto L34
                java.lang.Object r6 = r0.L$1
                java.util.Iterator r6 = (java.util.Iterator) r6
                java.lang.Object r2 = r0.L$0
                androidx.paging.multicast.ChannelManager$b$b$c r2 = (androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c) r2
                defpackage.o83.b(r7)
                goto L80
            L34:
                java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
                java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
                r6.<init>(r7)
                throw r6
            L3c:
                java.lang.Object r6 = r0.L$1
                androidx.paging.multicast.ChannelManager$b$b$c r6 = (androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c) r6
                java.lang.Object r2 = r0.L$0
                androidx.paging.multicast.ChannelManager$Actor r2 = (androidx.paging.multicast.ChannelManager.Actor) r2
                defpackage.o83.b(r7)
                goto L63
            L48:
                defpackage.o83.b(r7)
                androidx.paging.multicast.ChannelManager r7 = androidx.paging.multicast.ChannelManager.this
                hd1 r7 = androidx.paging.multicast.ChannelManager.c(r7)
                java.lang.Object r2 = r6.b()
                r0.L$0 = r5
                r0.L$1 = r6
                r0.label = r4
                java.lang.Object r7 = r7.invoke(r2, r0)
                if (r7 != r1) goto L62
                return r1
            L62:
                r2 = r5
            L63:
                rr<T> r7 = r2.f
                r7.a(r6)
                r2.h = r4
                rr<T> r7 = r2.f
                boolean r7 = r7.isEmpty()
                if (r7 == 0) goto L78
                n30 r7 = r6.a()
                r2.i = r7
            L78:
                java.util.List<androidx.paging.multicast.ChannelManager$a<T>> r7 = r2.j
                java.util.Iterator r7 = r7.iterator()
                r2 = r6
                r6 = r7
            L80:
                boolean r7 = r6.hasNext()
                if (r7 == 0) goto L99
                java.lang.Object r7 = r6.next()
                androidx.paging.multicast.ChannelManager$a r7 = (androidx.paging.multicast.ChannelManager.a) r7
                r0.L$0 = r2
                r0.L$1 = r6
                r0.label = r3
                java.lang.Object r7 = r7.c(r2, r0)
                if (r7 != r1) goto L80
                return r1
            L99:
                te4 r6 = defpackage.te4.a
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.ChannelManager.Actor.l(androidx.paging.multicast.ChannelManager$b$b$c, q70):java.lang.Object");
        }

        public final void m(SharedFlowProducer<T> sharedFlowProducer) {
            if (this.g != sharedFlowProducer) {
                return;
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Iterator<T> it = this.j.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                if (aVar.d()) {
                    if (!ChannelManager.this.d) {
                        aVar.a();
                    } else {
                        arrayList.add(aVar);
                    }
                } else if (!this.h) {
                    if (!ChannelManager.this.d) {
                        aVar.a();
                    } else {
                        arrayList.add(aVar);
                    }
                } else {
                    arrayList2.add(aVar);
                }
            }
            this.j.clear();
            this.j.addAll(arrayList2);
            this.j.addAll(arrayList);
            this.g = null;
            if (!arrayList2.isEmpty()) {
                h();
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final /* synthetic */ java.lang.Object n(defpackage.uj3<? super androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c<T>> r6, defpackage.q70<? super defpackage.te4> r7) {
            /*
                r5 = this;
                boolean r0 = r7 instanceof androidx.paging.multicast.ChannelManager$Actor$doRemove$1
                if (r0 == 0) goto L13
                r0 = r7
                androidx.paging.multicast.ChannelManager$Actor$doRemove$1 r0 = (androidx.paging.multicast.ChannelManager$Actor$doRemove$1) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L13
                int r1 = r1 - r2
                r0.label = r1
                goto L18
            L13:
                androidx.paging.multicast.ChannelManager$Actor$doRemove$1 r0 = new androidx.paging.multicast.ChannelManager$Actor$doRemove$1
                r0.<init>(r5, r7)
            L18:
                java.lang.Object r7 = r0.result
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 1
                if (r2 == 0) goto L31
                if (r2 != r3) goto L29
                defpackage.o83.b(r7)
                goto L7e
            L29:
                java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
                java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
                r6.<init>(r7)
                throw r6
            L31:
                defpackage.o83.b(r7)
                java.util.List<androidx.paging.multicast.ChannelManager$a<T>> r7 = r5.j
                r2 = 0
                java.util.Iterator r7 = r7.iterator()
            L3b:
                boolean r4 = r7.hasNext()
                if (r4 == 0) goto L59
                java.lang.Object r4 = r7.next()
                androidx.paging.multicast.ChannelManager$a r4 = (androidx.paging.multicast.ChannelManager.a) r4
                boolean r4 = r4.e(r6)
                java.lang.Boolean r4 = defpackage.hr.a(r4)
                boolean r4 = r4.booleanValue()
                if (r4 == 0) goto L56
                goto L5a
            L56:
                int r2 = r2 + 1
                goto L3b
            L59:
                r2 = -1
            L5a:
                if (r2 < 0) goto L7e
                java.util.List<androidx.paging.multicast.ChannelManager$a<T>> r6 = r5.j
                r6.remove(r2)
                java.util.List<androidx.paging.multicast.ChannelManager$a<T>> r6 = r5.j
                boolean r6 = r6.isEmpty()
                if (r6 == 0) goto L7e
                androidx.paging.multicast.ChannelManager r6 = androidx.paging.multicast.ChannelManager.this
                boolean r6 = androidx.paging.multicast.ChannelManager.b(r6)
                if (r6 != 0) goto L7e
                androidx.paging.multicast.SharedFlowProducer<T> r6 = r5.g
                if (r6 == 0) goto L7e
                r0.label = r3
                java.lang.Object r6 = r6.e(r0)
                if (r6 != r1) goto L7e
                return r1
            L7e:
                te4 r6 = defpackage.te4.a
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.ChannelManager.Actor.n(uj3, q70):java.lang.Object");
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
        /* JADX WARN: Removed duplicated region for block: B:17:0x0038  */
        @Override // androidx.paging.multicast.StoreRealActor
        /* renamed from: o */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public java.lang.Object e(androidx.paging.multicast.ChannelManager.b<T> r7, defpackage.q70<? super defpackage.te4> r8) {
            /*
                r6 = this;
                boolean r0 = r8 instanceof androidx.paging.multicast.ChannelManager$Actor$handle$1
                if (r0 == 0) goto L13
                r0 = r8
                androidx.paging.multicast.ChannelManager$Actor$handle$1 r0 = (androidx.paging.multicast.ChannelManager$Actor$handle$1) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L13
                int r1 = r1 - r2
                r0.label = r1
                goto L18
            L13:
                androidx.paging.multicast.ChannelManager$Actor$handle$1 r0 = new androidx.paging.multicast.ChannelManager$Actor$handle$1
                r0.<init>(r6, r8)
            L18:
                java.lang.Object r8 = r0.result
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 3
                r4 = 2
                r5 = 1
                if (r2 == 0) goto L38
                if (r2 == r5) goto L34
                if (r2 == r4) goto L34
                if (r2 != r3) goto L2c
                goto L34
            L2c:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r8)
                throw r7
            L34:
                defpackage.o83.b(r8)
                goto L83
            L38:
                defpackage.o83.b(r8)
                boolean r8 = r7 instanceof androidx.paging.multicast.ChannelManager.b.a
                if (r8 == 0) goto L4a
                androidx.paging.multicast.ChannelManager$b$a r7 = (androidx.paging.multicast.ChannelManager.b.a) r7
                r0.label = r5
                java.lang.Object r7 = r6.j(r7, r0)
                if (r7 != r1) goto L83
                return r1
            L4a:
                boolean r8 = r7 instanceof androidx.paging.multicast.ChannelManager.b.c
                if (r8 == 0) goto L5d
                androidx.paging.multicast.ChannelManager$b$c r7 = (androidx.paging.multicast.ChannelManager.b.c) r7
                uj3 r7 = r7.a()
                r0.label = r4
                java.lang.Object r7 = r6.n(r7, r0)
                if (r7 != r1) goto L83
                return r1
            L5d:
                boolean r8 = r7 instanceof androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c
                if (r8 == 0) goto L6c
                androidx.paging.multicast.ChannelManager$b$b$c r7 = (androidx.paging.multicast.ChannelManager.b.AbstractC0053b.c) r7
                r0.label = r3
                java.lang.Object r7 = r6.l(r7, r0)
                if (r7 != r1) goto L83
                return r1
            L6c:
                boolean r8 = r7 instanceof androidx.paging.multicast.ChannelManager.b.AbstractC0053b.a
                if (r8 == 0) goto L76
                androidx.paging.multicast.ChannelManager$b$b$a r7 = (androidx.paging.multicast.ChannelManager.b.AbstractC0053b.a) r7
                r6.k(r7)
                goto L83
            L76:
                boolean r8 = r7 instanceof androidx.paging.multicast.ChannelManager.b.AbstractC0053b.C0054b
                if (r8 == 0) goto L83
                androidx.paging.multicast.ChannelManager$b$b$b r7 = (androidx.paging.multicast.ChannelManager.b.AbstractC0053b.C0054b) r7
                androidx.paging.multicast.SharedFlowProducer r7 = r7.a()
                r6.m(r7)
            L83:
                te4 r7 = defpackage.te4.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.ChannelManager.Actor.e(androidx.paging.multicast.ChannelManager$b, q70):java.lang.Object");
        }

        public final SharedFlowProducer<T> p() {
            return new SharedFlowProducer<>(ChannelManager.this.b, ChannelManager.this.g, new ChannelManager$Actor$newProducer$1(this));
        }
    }

    /* compiled from: ChannelManager.kt */
    /* loaded from: classes.dex */
    public static final class a<T> {
        public final uj3<b.AbstractC0053b.c<T>> a;
        public boolean b;

        /* JADX WARN: Multi-variable type inference failed */
        public a(uj3<? super b.AbstractC0053b.c<T>> uj3Var, boolean z) {
            fs1.f(uj3Var, "channel");
            this.a = uj3Var;
            this.b = z;
        }

        public final void a() {
            uj3.a.a(this.a, null, 1, null);
        }

        public final void b(Throwable th) {
            fs1.f(th, "error");
            this.b = true;
            this.a.l(th);
        }

        public final Object c(b.AbstractC0053b.c<T> cVar, q70<? super te4> q70Var) {
            this.b = true;
            Object h = this.a.h(cVar, q70Var);
            return h == gs1.d() ? h : te4.a;
        }

        public final boolean d() {
            return this.b;
        }

        public final boolean e(uj3<? super b.AbstractC0053b.c<T>> uj3Var) {
            fs1.f(uj3Var, "channel");
            return this.a == uj3Var;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    return fs1.b(this.a, aVar.a) && this.b == aVar.b;
                }
                return false;
            }
            return true;
        }

        public final boolean f(a<T> aVar) {
            fs1.f(aVar, "entry");
            return this.a == aVar.a;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            uj3<b.AbstractC0053b.c<T>> uj3Var = this.a;
            int hashCode = (uj3Var != null ? uj3Var.hashCode() : 0) * 31;
            boolean z = this.b;
            int i = z;
            if (z != 0) {
                i = 1;
            }
            return hashCode + i;
        }

        public String toString() {
            return "ChannelEntry(channel=" + this.a + ", _receivedValue=" + this.b + ")";
        }

        public /* synthetic */ a(uj3 uj3Var, boolean z, int i, qi0 qi0Var) {
            this(uj3Var, (i & 2) != 0 ? false : z);
        }
    }

    /* compiled from: ChannelManager.kt */
    /* loaded from: classes.dex */
    public static abstract class b<T> {

        /* compiled from: ChannelManager.kt */
        /* loaded from: classes.dex */
        public static final class a<T> extends b<T> {
            public final uj3<AbstractC0053b.c<T>> a;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public a(uj3<? super AbstractC0053b.c<T>> uj3Var) {
                super(null);
                fs1.f(uj3Var, "channel");
                this.a = uj3Var;
            }

            public final uj3<AbstractC0053b.c<T>> a() {
                return this.a;
            }
        }

        /* compiled from: ChannelManager.kt */
        /* renamed from: androidx.paging.multicast.ChannelManager$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static abstract class AbstractC0053b<T> extends b<T> {

            /* compiled from: ChannelManager.kt */
            /* renamed from: androidx.paging.multicast.ChannelManager$b$b$a */
            /* loaded from: classes.dex */
            public static final class a<T> extends AbstractC0053b<T> {
                public final Throwable a;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public a(Throwable th) {
                    super(null);
                    fs1.f(th, "error");
                    this.a = th;
                }

                public final Throwable a() {
                    return this.a;
                }
            }

            /* compiled from: ChannelManager.kt */
            /* renamed from: androidx.paging.multicast.ChannelManager$b$b$b  reason: collision with other inner class name */
            /* loaded from: classes.dex */
            public static final class C0054b<T> extends AbstractC0053b<T> {
                public final SharedFlowProducer<T> a;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public C0054b(SharedFlowProducer<T> sharedFlowProducer) {
                    super(null);
                    fs1.f(sharedFlowProducer, "producer");
                    this.a = sharedFlowProducer;
                }

                public final SharedFlowProducer<T> a() {
                    return this.a;
                }
            }

            /* compiled from: ChannelManager.kt */
            /* renamed from: androidx.paging.multicast.ChannelManager$b$b$c */
            /* loaded from: classes.dex */
            public static final class c<T> extends AbstractC0053b<T> {
                public final T a;
                public final n30<te4> b;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public c(T t, n30<te4> n30Var) {
                    super(null);
                    fs1.f(n30Var, "delivered");
                    this.a = t;
                    this.b = n30Var;
                }

                public final n30<te4> a() {
                    return this.b;
                }

                public final T b() {
                    return this.a;
                }
            }

            public AbstractC0053b() {
                super(null);
            }

            public /* synthetic */ AbstractC0053b(qi0 qi0Var) {
                this();
            }
        }

        /* compiled from: ChannelManager.kt */
        /* loaded from: classes.dex */
        public static final class c<T> extends b<T> {
            public final uj3<AbstractC0053b.c<T>> a;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public c(uj3<? super AbstractC0053b.c<T>> uj3Var) {
                super(null);
                fs1.f(uj3Var, "channel");
                this.a = uj3Var;
            }

            public final uj3<AbstractC0053b.c<T>> a() {
                return this.a;
            }
        }

        public b() {
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ChannelManager(c90 c90Var, int i, boolean z, hd1<? super T, ? super q70<? super te4>, ? extends Object> hd1Var, boolean z2, j71<? extends T> j71Var) {
        fs1.f(c90Var, "scope");
        fs1.f(hd1Var, "onEach");
        fs1.f(j71Var, "upstream");
        this.b = c90Var;
        this.c = i;
        this.d = z;
        this.e = hd1Var;
        this.f = z2;
        this.g = j71Var;
        this.a = new Actor();
    }

    public final Object g(uj3<? super b.AbstractC0053b.c<T>> uj3Var, q70<? super te4> q70Var) {
        Object g = this.a.g(new b.a(uj3Var), q70Var);
        return g == gs1.d() ? g : te4.a;
    }

    public final Object h(q70<? super te4> q70Var) {
        Object c = this.a.c(q70Var);
        return c == gs1.d() ? c : te4.a;
    }

    public final Object i(uj3<? super b.AbstractC0053b.c<T>> uj3Var, q70<? super te4> q70Var) {
        Object g = this.a.g(new b.c(uj3Var), q70Var);
        return g == gs1.d() ? g : te4.a;
    }
}
