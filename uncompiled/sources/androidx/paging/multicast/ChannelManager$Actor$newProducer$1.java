package androidx.paging.multicast;

import androidx.paging.multicast.ChannelManager;
import kotlin.jvm.internal.FunctionReferenceImpl;

/* compiled from: ChannelManager.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class ChannelManager$Actor$newProducer$1 extends FunctionReferenceImpl implements hd1<ChannelManager.b<T>, q70<? super te4>, Object> {
    public ChannelManager$Actor$newProducer$1(ChannelManager.Actor actor) {
        super(2, actor, ChannelManager.Actor.class, "send", "send(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", 0);
    }

    @Override // defpackage.hd1
    public final Object invoke(ChannelManager.b<T> bVar, q70<? super te4> q70Var) {
        return ((ChannelManager.Actor) this.receiver).g(bVar, q70Var);
    }
}
