package androidx.paging.multicast;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Multicaster.kt */
@a(c = "androidx.paging.multicast.Multicaster$flow$1", f = "Multicaster.kt", l = {100}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class Multicaster$flow$1 extends SuspendLambda implements hd1<k71<? super T>, q70<? super te4>, Object> {
    private /* synthetic */ Object L$0;
    public int label;
    public final /* synthetic */ Multicaster this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Multicaster$flow$1(Multicaster multicaster, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = multicaster;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        Multicaster$flow$1 multicaster$flow$1 = new Multicaster$flow$1(this.this$0, q70Var);
        multicaster$flow$1.L$0 = obj;
        return multicaster$flow$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((Multicaster$flow$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            kx b = rx.b(Integer.MAX_VALUE, null, null, 6, null);
            j71 s = n71.s(n71.n(new Multicaster$flow$1$invokeSuspend$$inlined$transform$1(n71.u(n71.h(b), new Multicaster$flow$1$subFlow$1(this, b, null)), null)), new Multicaster$flow$1$subFlow$3(this, b, null));
            this.label = 1;
            if (s.a((k71) this.L$0, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
