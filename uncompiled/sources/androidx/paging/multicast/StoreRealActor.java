package androidx.paging.multicast;

import defpackage.uj3;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.coroutines.jvm.internal.SuspendLambda;

/* compiled from: StoreRealActor.kt */
/* loaded from: classes.dex */
public abstract class StoreRealActor<T> {
    public final kx<Object> a;
    public final n30<te4> b;
    public final AtomicBoolean c;
    public static final a e = new a(null);
    public static final Object d = new Object();

    /* compiled from: StoreRealActor.kt */
    @kotlin.coroutines.jvm.internal.a(c = "androidx.paging.multicast.StoreRealActor$1", f = "StoreRealActor.kt", l = {45}, m = "invokeSuspend")
    /* renamed from: androidx.paging.multicast.StoreRealActor$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<Object, q70<? super te4>, Object> {
        private /* synthetic */ Object L$0;
        public int label;

        public AnonymousClass1(q70 q70Var) {
            super(2, q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(q70Var);
            anonymousClass1.L$0 = obj;
            return anonymousClass1;
        }

        @Override // defpackage.hd1
        public final Object invoke(Object obj, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(obj, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                Object obj2 = this.L$0;
                if (obj2 == StoreRealActor.e.a()) {
                    StoreRealActor.this.d();
                } else {
                    StoreRealActor storeRealActor = StoreRealActor.this;
                    this.label = 1;
                    if (storeRealActor.e(obj2, this) == d) {
                        return d;
                    }
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* compiled from: StoreRealActor.kt */
    @kotlin.coroutines.jvm.internal.a(c = "androidx.paging.multicast.StoreRealActor$2", f = "StoreRealActor.kt", l = {}, m = "invokeSuspend")
    /* renamed from: androidx.paging.multicast.StoreRealActor$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super Object>, Throwable, q70<? super te4>, Object> {
        public int label;

        public AnonymousClass2(q70 q70Var) {
            super(3, q70Var);
        }

        public final q70<te4> create(k71<Object> k71Var, Throwable th, q70<? super te4> q70Var) {
            fs1.f(k71Var, "$this$create");
            fs1.f(q70Var, "continuation");
            return new AnonymousClass2(q70Var);
        }

        @Override // defpackage.kd1
        public final Object invoke(k71<? super Object> k71Var, Throwable th, q70<? super te4> q70Var) {
            return ((AnonymousClass2) create(k71Var, th, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                StoreRealActor.this.d();
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* compiled from: StoreRealActor.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a() {
        }

        public final Object a() {
            return StoreRealActor.d;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public StoreRealActor(c90 c90Var) {
        fs1.f(c90Var, "scope");
        kx<Object> b = rx.b(0, null, null, 6, null);
        this.a = b;
        this.b = p30.b(null, 1, null);
        this.c = new AtomicBoolean(false);
        n71.q(n71.s(n71.t(n71.h(b), new AnonymousClass1(null)), new AnonymousClass2(null)), c90Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x003c  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x005c A[RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object c(defpackage.q70<? super defpackage.te4> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof androidx.paging.multicast.StoreRealActor$close$1
            if (r0 == 0) goto L13
            r0 = r6
            androidx.paging.multicast.StoreRealActor$close$1 r0 = (androidx.paging.multicast.StoreRealActor$close$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.multicast.StoreRealActor$close$1 r0 = new androidx.paging.multicast.StoreRealActor$close$1
            r0.<init>(r5, r6)
        L18:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L3c
            if (r2 == r4) goto L34
            if (r2 != r3) goto L2c
            defpackage.o83.b(r6)
            goto L5d
        L2c:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L34:
            java.lang.Object r2 = r0.L$0
            androidx.paging.multicast.StoreRealActor r2 = (androidx.paging.multicast.StoreRealActor) r2
            defpackage.o83.b(r6)
            goto L4f
        L3c:
            defpackage.o83.b(r6)
            kx<java.lang.Object> r6 = r5.a
            java.lang.Object r2 = androidx.paging.multicast.StoreRealActor.d
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r6 = r6.h(r2, r0)
            if (r6 != r1) goto L4e
            return r1
        L4e:
            r2 = r5
        L4f:
            n30<te4> r6 = r2.b
            r2 = 0
            r0.L$0 = r2
            r0.label = r3
            java.lang.Object r6 = r6.r(r0)
            if (r6 != r1) goto L5d
            return r1
        L5d:
            te4 r6 = defpackage.te4.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.StoreRealActor.c(q70):java.lang.Object");
    }

    public final void d() {
        if (this.c.compareAndSet(false, true)) {
            try {
                f();
            } finally {
                uj3.a.a(this.a, null, 1, null);
                this.b.u(te4.a);
            }
        }
    }

    public abstract Object e(T t, q70<? super te4> q70Var);

    public void f() {
    }

    public final Object g(T t, q70<? super te4> q70Var) {
        Object h = this.a.h(t, q70Var);
        return h == gs1.d() ? h : te4.a;
    }
}
