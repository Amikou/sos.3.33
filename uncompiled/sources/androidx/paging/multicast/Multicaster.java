package androidx.paging.multicast;

import kotlin.LazyThreadSafetyMode;

/* compiled from: Multicaster.kt */
/* loaded from: classes.dex */
public final class Multicaster<T> {
    public final sy1 a;
    public final j71<T> b;
    public final c90 c;
    public final j71<T> d;
    public final boolean e;
    public final hd1<T, q70<? super te4>, Object> f;
    public final boolean g;

    /* JADX WARN: Multi-variable type inference failed */
    public Multicaster(c90 c90Var, int i, j71<? extends T> j71Var, boolean z, hd1<? super T, ? super q70<? super te4>, ? extends Object> hd1Var, boolean z2) {
        fs1.f(c90Var, "scope");
        fs1.f(j71Var, "source");
        fs1.f(hd1Var, "onEach");
        this.c = c90Var;
        this.d = j71Var;
        this.e = z;
        this.f = hd1Var;
        this.g = z2;
        this.a = zy1.b(LazyThreadSafetyMode.SYNCHRONIZED, new Multicaster$channelManager$2(this, i));
        this.b = n71.n(new Multicaster$flow$1(this, null));
    }

    public final Object g(q70<? super te4> q70Var) {
        Object h = h().h(q70Var);
        return h == gs1.d() ? h : te4.a;
    }

    public final ChannelManager<T> h() {
        return (ChannelManager) this.a.getValue();
    }

    public final j71<T> i() {
        return this.b;
    }

    public /* synthetic */ Multicaster(c90 c90Var, int i, j71 j71Var, boolean z, hd1 hd1Var, boolean z2, int i2, qi0 qi0Var) {
        this(c90Var, (i2 & 2) != 0 ? 0 : i, j71Var, (i2 & 8) != 0 ? false : z, hd1Var, (i2 & 32) != 0 ? false : z2);
    }
}
