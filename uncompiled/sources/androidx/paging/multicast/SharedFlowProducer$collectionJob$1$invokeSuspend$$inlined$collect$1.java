package androidx.paging.multicast;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Collect.kt */
/* loaded from: classes.dex */
public final class SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1 implements k71<T> {
    public final /* synthetic */ SharedFlowProducer$collectionJob$1 a;

    @a(c = "androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1", f = "SharedFlowProducer.kt", l = {135, 141}, m = "emit")
    /* renamed from: androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends ContinuationImpl {
        public Object L$0;
        public int label;
        public /* synthetic */ Object result;

        public AnonymousClass1(q70 q70Var) {
            super(q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1.this.emit(null, this);
        }
    }

    public SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1(SharedFlowProducer$collectionJob$1 sharedFlowProducer$collectionJob$1) {
        this.a = sharedFlowProducer$collectionJob$1;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x003d  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0067 A[RETURN] */
    @Override // defpackage.k71
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object emit(java.lang.Object r8, defpackage.q70 r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1.AnonymousClass1
            if (r0 == 0) goto L13
            r0 = r9
            androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1$1 r0 = (androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1.AnonymousClass1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1$1 r0 = new androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1$1
            r0.<init>(r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 0
            r5 = 1
            if (r2 == 0) goto L3d
            if (r2 == r5) goto L35
            if (r2 != r3) goto L2d
            defpackage.o83.b(r9)
            goto L68
        L2d:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L35:
            java.lang.Object r8 = r0.L$0
            n30 r8 = (defpackage.n30) r8
            defpackage.o83.b(r9)
            goto L5d
        L3d:
            defpackage.o83.b(r9)
            n30 r9 = defpackage.p30.b(r4, r5, r4)
            androidx.paging.multicast.SharedFlowProducer$collectionJob$1 r2 = r7.a
            androidx.paging.multicast.SharedFlowProducer r2 = r2.this$0
            hd1 r2 = androidx.paging.multicast.SharedFlowProducer.b(r2)
            androidx.paging.multicast.ChannelManager$b$b$c r6 = new androidx.paging.multicast.ChannelManager$b$b$c
            r6.<init>(r8, r9)
            r0.L$0 = r9
            r0.label = r5
            java.lang.Object r8 = r2.invoke(r6, r0)
            if (r8 != r1) goto L5c
            return r1
        L5c:
            r8 = r9
        L5d:
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r8 = r8.r(r0)
            if (r8 != r1) goto L68
            return r1
        L68:
            te4 r8 = defpackage.te4.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.multicast.SharedFlowProducer$collectionJob$1$invokeSuspend$$inlined$collect$1.emit(java.lang.Object, q70):java.lang.Object");
    }
}
