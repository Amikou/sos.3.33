package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PageFetcherSnapshot.kt */
@a(c = "androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$3", f = "PageFetcherSnapshot.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcherSnapshot$collectAsGenerationalViewportHints$3 extends SuspendLambda implements kd1<ve1, ve1, q70<? super ve1>, Object> {
    public final /* synthetic */ LoadType $loadType;
    private /* synthetic */ Object L$0;
    private /* synthetic */ Object L$1;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcherSnapshot$collectAsGenerationalViewportHints$3(LoadType loadType, q70 q70Var) {
        super(3, q70Var);
        this.$loadType = loadType;
    }

    public final q70<te4> create(ve1 ve1Var, ve1 ve1Var2, q70<? super ve1> q70Var) {
        fs1.f(ve1Var, "previous");
        fs1.f(ve1Var2, "next");
        fs1.f(q70Var, "continuation");
        PageFetcherSnapshot$collectAsGenerationalViewportHints$3 pageFetcherSnapshot$collectAsGenerationalViewportHints$3 = new PageFetcherSnapshot$collectAsGenerationalViewportHints$3(this.$loadType, q70Var);
        pageFetcherSnapshot$collectAsGenerationalViewportHints$3.L$0 = ve1Var;
        pageFetcherSnapshot$collectAsGenerationalViewportHints$3.L$1 = ve1Var2;
        return pageFetcherSnapshot$collectAsGenerationalViewportHints$3;
    }

    @Override // defpackage.kd1
    public final Object invoke(ve1 ve1Var, ve1 ve1Var2, q70<? super ve1> q70Var) {
        return ((PageFetcherSnapshot$collectAsGenerationalViewportHints$3) create(ve1Var, ve1Var2, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            ve1 ve1Var = (ve1) this.L$0;
            ve1 ve1Var2 = (ve1) this.L$1;
            return ap2.a(ve1Var2, ve1Var, this.$loadType) ? ve1Var2 : ve1Var;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
