package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;

/* compiled from: CachedPageEventFlow.kt */
@kotlin.coroutines.jvm.internal.a(c = "androidx.paging.CachedPageEventFlow$downstreamFlow$1$historyCollection$1", f = "CachedPageEventFlow.kt", l = {292}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CachedPageEventFlow$downstreamFlow$1$historyCollection$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Ref$IntRef $lastReceivedHistoryIndex;
    public final /* synthetic */ TemporaryDownstream $snapshot;
    public final /* synthetic */ lp3 $this_simpleChannelFlow;
    public int label;

    /* compiled from: Collect.kt */
    /* loaded from: classes.dex */
    public static final class a implements k71<lq1<? extends yo2<T>>> {
        public a() {
        }

        @Override // defpackage.k71
        public Object emit(Object obj, q70 q70Var) {
            lq1 lq1Var = (lq1) obj;
            CachedPageEventFlow$downstreamFlow$1$historyCollection$1.this.$lastReceivedHistoryIndex.element = lq1Var.a();
            Object h = CachedPageEventFlow$downstreamFlow$1$historyCollection$1.this.$this_simpleChannelFlow.h(lq1Var.b(), q70Var);
            return h == gs1.d() ? h : te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CachedPageEventFlow$downstreamFlow$1$historyCollection$1(lp3 lp3Var, TemporaryDownstream temporaryDownstream, Ref$IntRef ref$IntRef, q70 q70Var) {
        super(2, q70Var);
        this.$this_simpleChannelFlow = lp3Var;
        this.$snapshot = temporaryDownstream;
        this.$lastReceivedHistoryIndex = ref$IntRef;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new CachedPageEventFlow$downstreamFlow$1$historyCollection$1(this.$this_simpleChannelFlow, this.$snapshot, this.$lastReceivedHistoryIndex, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CachedPageEventFlow$downstreamFlow$1$historyCollection$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            j71 b = this.$snapshot.b();
            a aVar = new a();
            this.label = 1;
            if (b.a(aVar, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
