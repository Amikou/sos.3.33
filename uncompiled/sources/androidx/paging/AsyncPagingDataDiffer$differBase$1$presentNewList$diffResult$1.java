package androidx.paging;

import androidx.recyclerview.widget.g;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: AsyncPagingDataDiffer.kt */
@a(c = "androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$diffResult$1", f = "AsyncPagingDataDiffer.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class AsyncPagingDataDiffer$differBase$1$presentNewList$diffResult$1 extends SuspendLambda implements hd1<c90, q70<? super ui2>, Object> {
    public final /* synthetic */ vi2 $newList;
    public final /* synthetic */ vi2 $previousList;
    public int label;
    public final /* synthetic */ AsyncPagingDataDiffer$differBase$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AsyncPagingDataDiffer$differBase$1$presentNewList$diffResult$1(AsyncPagingDataDiffer$differBase$1 asyncPagingDataDiffer$differBase$1, vi2 vi2Var, vi2 vi2Var2, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = asyncPagingDataDiffer$differBase$1;
        this.$previousList = vi2Var;
        this.$newList = vi2Var2;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new AsyncPagingDataDiffer$differBase$1$presentNewList$diffResult$1(this.this$0, this.$previousList, this.$newList, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super ui2> q70Var) {
        return ((AsyncPagingDataDiffer$differBase$1$presentNewList$diffResult$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        g.f fVar;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            vi2 vi2Var = this.$previousList;
            vi2 vi2Var2 = this.$newList;
            fVar = this.this$0.l.f;
            return wi2.a(vi2Var, vi2Var2, fVar);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
