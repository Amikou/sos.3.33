package androidx.paging;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PageFetcherSnapshot.kt */
@a(c = "androidx.paging.PageFetcherSnapshot$startConsumingHints$1", f = "PageFetcherSnapshot.kt", l = {212}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcherSnapshot$startConsumingHints$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ PageFetcherSnapshot this$0;

    /* compiled from: PageFetcherSnapshot.kt */
    @a(c = "androidx.paging.PageFetcherSnapshot$startConsumingHints$1$2", f = "PageFetcherSnapshot.kt", l = {}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcherSnapshot$startConsumingHints$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends SuspendLambda implements hd1<xk4, q70<? super te4>, Object> {
        public int label;

        public AnonymousClass2(q70 q70Var) {
            super(2, q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            return new AnonymousClass2(q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(xk4 xk4Var, q70<? super te4> q70Var) {
            return ((AnonymousClass2) create(xk4Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            rc1 rc1Var;
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                rc1Var = PageFetcherSnapshot$startConsumingHints$1.this.this$0.o;
                rc1Var.invoke();
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcherSnapshot$startConsumingHints$1(PageFetcherSnapshot pageFetcherSnapshot, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = pageFetcherSnapshot;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new PageFetcherSnapshot$startConsumingHints$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((PageFetcherSnapshot$startConsumingHints$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        final ib2 ib2Var;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            ib2Var = this.this$0.a;
            j71<xk4> j71Var = new j71<xk4>() { // from class: androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1

                /* compiled from: Collect.kt */
                /* renamed from: androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1$2  reason: invalid class name */
                /* loaded from: classes.dex */
                public static final class AnonymousClass2 implements k71<xk4> {
                    public final /* synthetic */ k71 a;
                    public final /* synthetic */ PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1 f0;

                    @a(c = "androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1$2", f = "PageFetcherSnapshot.kt", l = {136}, m = "emit")
                    /* renamed from: androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1$2$1  reason: invalid class name */
                    /* loaded from: classes.dex */
                    public static final class AnonymousClass1 extends ContinuationImpl {
                        public Object L$0;
                        public Object L$1;
                        public int label;
                        public /* synthetic */ Object result;

                        public AnonymousClass1(q70 q70Var) {
                            super(q70Var);
                        }

                        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                        public final Object invokeSuspend(Object obj) {
                            this.result = obj;
                            this.label |= Integer.MIN_VALUE;
                            return AnonymousClass2.this.emit(null, this);
                        }
                    }

                    public AnonymousClass2(k71 k71Var, PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1 pageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1) {
                        this.a = k71Var;
                        this.f0 = pageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1;
                    }

                    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
                    /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
                    /* JADX WARN: Removed duplicated region for block: B:23:0x006f  */
                    /* JADX WARN: Removed duplicated region for block: B:27:0x007b  */
                    @Override // defpackage.k71
                    /*
                        Code decompiled incorrectly, please refer to instructions dump.
                        To view partially-correct code enable 'Show inconsistent code' option in preferences
                    */
                    public java.lang.Object emit(defpackage.xk4 r7, defpackage.q70 r8) {
                        /*
                            r6 = this;
                            boolean r0 = r8 instanceof androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1.AnonymousClass2.AnonymousClass1
                            if (r0 == 0) goto L13
                            r0 = r8
                            androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1$2$1 r0 = (androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1.AnonymousClass2.AnonymousClass1) r0
                            int r1 = r0.label
                            r2 = -2147483648(0xffffffff80000000, float:-0.0)
                            r3 = r1 & r2
                            if (r3 == 0) goto L13
                            int r1 = r1 - r2
                            r0.label = r1
                            goto L18
                        L13:
                            androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1$2$1 r0 = new androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1$2$1
                            r0.<init>(r8)
                        L18:
                            java.lang.Object r8 = r0.result
                            java.lang.Object r1 = defpackage.gs1.d()
                            int r2 = r0.label
                            r3 = 1
                            if (r2 == 0) goto L31
                            if (r2 != r3) goto L29
                            defpackage.o83.b(r8)
                            goto L78
                        L29:
                            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
                            r7.<init>(r8)
                            throw r7
                        L31:
                            defpackage.o83.b(r8)
                            k71 r8 = r6.a
                            r2 = r7
                            xk4 r2 = (defpackage.xk4) r2
                            int r4 = r2.d()
                            int r4 = r4 * (-1)
                            androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1 r5 = r6.f0
                            androidx.paging.PageFetcherSnapshot$startConsumingHints$1 r5 = r2
                            androidx.paging.PageFetcherSnapshot r5 = r5.this$0
                            ep2 r5 = androidx.paging.PageFetcherSnapshot.a(r5)
                            int r5 = r5.f
                            if (r4 > r5) goto L64
                            int r2 = r2.c()
                            int r2 = r2 * (-1)
                            androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1 r4 = r6.f0
                            androidx.paging.PageFetcherSnapshot$startConsumingHints$1 r4 = r2
                            androidx.paging.PageFetcherSnapshot r4 = r4.this$0
                            ep2 r4 = androidx.paging.PageFetcherSnapshot.a(r4)
                            int r4 = r4.f
                            if (r2 <= r4) goto L62
                            goto L64
                        L62:
                            r2 = 0
                            goto L65
                        L64:
                            r2 = r3
                        L65:
                            java.lang.Boolean r2 = defpackage.hr.a(r2)
                            boolean r2 = r2.booleanValue()
                            if (r2 == 0) goto L7b
                            r0.label = r3
                            java.lang.Object r7 = r8.emit(r7, r0)
                            if (r7 != r1) goto L78
                            return r1
                        L78:
                            te4 r7 = defpackage.te4.a
                            goto L7d
                        L7b:
                            te4 r7 = defpackage.te4.a
                        L7d:
                            return r7
                        */
                        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcherSnapshot$startConsumingHints$1$invokeSuspend$$inlined$filter$1.AnonymousClass2.emit(java.lang.Object, q70):java.lang.Object");
                    }
                }

                @Override // defpackage.j71
                public Object a(k71<? super xk4> k71Var, q70 q70Var) {
                    Object a = j71.this.a(new AnonymousClass2(k71Var, this), q70Var);
                    return a == gs1.d() ? a : te4.a;
                }
            };
            AnonymousClass2 anonymousClass2 = new AnonymousClass2(null);
            this.label = 1;
            if (n71.f(j71Var, anonymousClass2, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
