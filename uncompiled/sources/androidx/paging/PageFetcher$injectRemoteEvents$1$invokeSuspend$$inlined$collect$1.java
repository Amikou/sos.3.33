package androidx.paging;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Collect.kt */
/* loaded from: classes.dex */
public final class PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1 implements k71<yo2<Value>> {
    public final /* synthetic */ PageFetcher$injectRemoteEvents$1 a;
    public final /* synthetic */ lp3 f0;
    public final /* synthetic */ hb2 g0;

    @a(c = "androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1", f = "PageFetcher.kt", l = {139, 147, 155}, m = "emit")
    /* renamed from: androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends ContinuationImpl {
        public int label;
        public /* synthetic */ Object result;

        public AnonymousClass1(q70 q70Var) {
            super(q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1.this.emit(null, this);
        }
    }

    public PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1(PageFetcher$injectRemoteEvents$1 pageFetcher$injectRemoteEvents$1, lp3 lp3Var, hb2 hb2Var) {
        this.a = pageFetcher$injectRemoteEvents$1;
        this.f0 = lp3Var;
        this.g0 = hb2Var;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x003d  */
    @Override // defpackage.k71
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object emit(java.lang.Object r17, defpackage.q70 r18) {
        /*
            r16 = this;
            r0 = r16
            r1 = r18
            boolean r2 = r1 instanceof androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1.AnonymousClass1
            if (r2 == 0) goto L17
            r2 = r1
            androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1$1 r2 = (androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1.AnonymousClass1) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L17
            int r3 = r3 - r4
            r2.label = r3
            goto L1c
        L17:
            androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1$1 r2 = new androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1$1
            r2.<init>(r1)
        L1c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = defpackage.gs1.d()
            int r4 = r2.label
            r5 = 3
            r6 = 2
            r7 = 1
            if (r4 == 0) goto L3d
            if (r4 == r7) goto L38
            if (r4 == r6) goto L38
            if (r4 != r5) goto L30
            goto L38
        L30:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L38:
            defpackage.o83.b(r1)
            goto Lc7
        L3d:
            defpackage.o83.b(r1)
            r1 = r17
            yo2 r1 = (defpackage.yo2) r1
            boolean r4 = r1 instanceof defpackage.yo2.b
            if (r4 == 0) goto L82
            hb2 r4 = r0.g0
            r8 = r1
            yo2$b r8 = (defpackage.yo2.b) r8
            a30 r1 = r8.d()
            x02 r1 = r1.f()
            androidx.paging.PageFetcher$injectRemoteEvents$1 r5 = r0.a
            o63 r5 = r5.$accessor
            ws3 r5 = r5.getState()
            java.lang.Object r5 = r5.getValue()
            x02 r5 = (defpackage.x02) r5
            r4.f(r1, r5)
            lp3 r1 = r0.f0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            hb2 r4 = r0.g0
            a30 r13 = r4.h()
            r14 = 15
            r15 = 0
            yo2$b r4 = defpackage.yo2.b.c(r8, r9, r10, r11, r12, r13, r14, r15)
            r2.label = r7
            java.lang.Object r1 = r1.h(r4, r2)
            if (r1 != r3) goto Lc7
            return r3
        L82:
            boolean r4 = r1 instanceof defpackage.yo2.a
            if (r4 == 0) goto La4
            hb2 r4 = r0.g0
            r5 = r1
            yo2$a r5 = (defpackage.yo2.a) r5
            androidx.paging.LoadType r5 = r5.a()
            r7 = 0
            w02$c$a r8 = defpackage.w02.c.d
            w02$c r8 = r8.b()
            r4.g(r5, r7, r8)
            lp3 r4 = r0.f0
            r2.label = r6
            java.lang.Object r1 = r4.h(r1, r2)
            if (r1 != r3) goto Lc7
            return r3
        La4:
            boolean r4 = r1 instanceof defpackage.yo2.c
            if (r4 == 0) goto Lc7
            hb2 r4 = r0.g0
            r6 = r1
            yo2$c r6 = (defpackage.yo2.c) r6
            androidx.paging.LoadType r7 = r6.c()
            boolean r8 = r6.a()
            w02 r6 = r6.b()
            r4.g(r7, r8, r6)
            lp3 r4 = r0.f0
            r2.label = r5
            java.lang.Object r1 = r4.h(r1, r2)
            if (r1 != r3) goto Lc7
            return r3
        Lc7:
            te4 r1 = defpackage.te4.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1.emit(java.lang.Object, q70):java.lang.Object");
    }
}
