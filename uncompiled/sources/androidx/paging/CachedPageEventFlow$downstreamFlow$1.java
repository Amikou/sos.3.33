package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.AwaitKt;

/* compiled from: CachedPageEventFlow.kt */
@a(c = "androidx.paging.CachedPageEventFlow$downstreamFlow$1", f = "CachedPageEventFlow.kt", l = {83, 117}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CachedPageEventFlow$downstreamFlow$1 extends SuspendLambda implements hd1<lp3<yo2<T>>, q70<? super te4>, Object> {
    private /* synthetic */ Object L$0;
    public int label;
    public final /* synthetic */ CachedPageEventFlow this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CachedPageEventFlow$downstreamFlow$1(CachedPageEventFlow cachedPageEventFlow, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = cachedPageEventFlow;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        CachedPageEventFlow$downstreamFlow$1 cachedPageEventFlow$downstreamFlow$1 = new CachedPageEventFlow$downstreamFlow$1(this.this$0, q70Var);
        cachedPageEventFlow$downstreamFlow$1.L$0 = obj;
        return cachedPageEventFlow$downstreamFlow$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((CachedPageEventFlow$downstreamFlow$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        lp3 lp3Var;
        FlattenedPageController flattenedPageController;
        Object a;
        st1 b;
        st1 b2;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            lp3Var = (lp3) this.L$0;
            flattenedPageController = this.this$0.a;
            this.L$0 = lp3Var;
            this.label = 1;
            a = flattenedPageController.a(this);
            if (a == d) {
                return d;
            }
        } else if (i != 1) {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        } else {
            lp3Var = (lp3) this.L$0;
            o83.b(obj);
            a = obj;
        }
        lp3 lp3Var2 = lp3Var;
        TemporaryDownstream temporaryDownstream = (TemporaryDownstream) a;
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = Integer.MIN_VALUE;
        b = as.b(lp3Var2, null, null, new CachedPageEventFlow$downstreamFlow$1$historyCollection$1(lp3Var2, temporaryDownstream, ref$IntRef, null), 3, null);
        b2 = as.b(lp3Var2, null, null, new CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1(this, lp3Var2, temporaryDownstream, b, ref$IntRef, null), 3, null);
        st1[] st1VarArr = {b2, b};
        this.L$0 = null;
        this.label = 2;
        if (AwaitKt.a(st1VarArr, this) == d) {
            return d;
        }
        return te4.a;
    }
}
