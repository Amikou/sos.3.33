package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Pager.kt */
@a(c = "androidx.paging.Pager$flow$2", f = "Pager.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class Pager$flow$2 extends SuspendLambda implements tc1<q70<? super gp2<Key, Value>>, Object> {
    public final /* synthetic */ rc1 $pagingSourceFactory;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Pager$flow$2(rc1 rc1Var, q70 q70Var) {
        super(1, q70Var);
        this.$pagingSourceFactory = rc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new Pager$flow$2(this.$pagingSourceFactory, q70Var);
    }

    @Override // defpackage.tc1
    public final Object invoke(Object obj) {
        return ((Pager$flow$2) create((q70) obj)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            return this.$pagingSourceFactory.invoke();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
