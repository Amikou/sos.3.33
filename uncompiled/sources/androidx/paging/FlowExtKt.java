package androidx.paging;

/* compiled from: FlowExt.kt */
/* loaded from: classes.dex */
public final class FlowExtKt {
    public static final Object a = new Object();

    public static final <T> j71<T> b(j71<? extends T> j71Var, kd1<? super T, ? super T, ? super q70<? super T>, ? extends Object> kd1Var) {
        fs1.f(j71Var, "$this$simpleRunningReduce");
        fs1.f(kd1Var, "operation");
        return n71.n(new FlowExtKt$simpleRunningReduce$1(j71Var, kd1Var, null));
    }

    public static final <T, R> j71<R> c(j71<? extends T> j71Var, R r, kd1<? super R, ? super T, ? super q70<? super R>, ? extends Object> kd1Var) {
        fs1.f(j71Var, "$this$simpleScan");
        fs1.f(kd1Var, "operation");
        return n71.n(new FlowExtKt$simpleScan$1(j71Var, r, kd1Var, null));
    }

    public static final <T, R> j71<R> d(j71<? extends T> j71Var, kd1<? super k71<? super R>, ? super T, ? super q70<? super te4>, ? extends Object> kd1Var) {
        fs1.f(j71Var, "$this$simpleTransformLatest");
        fs1.f(kd1Var, "transform");
        return SimpleChannelFlowKt.a(new FlowExtKt$simpleTransformLatest$1(j71Var, kd1Var, null));
    }
}
