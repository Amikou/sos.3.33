package androidx.paging;

import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPageEventFlow.kt */
@a(c = "androidx.paging.CachedPageEventFlow$multicastedSrc$1", f = "CachedPageEventFlow.kt", l = {292}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CachedPageEventFlow$multicastedSrc$1 extends SuspendLambda implements hd1<k71<? super lq1<? extends yo2<T>>>, q70<? super te4>, Object> {
    public final /* synthetic */ j71 $src;
    private /* synthetic */ Object L$0;
    public int label;
    public final /* synthetic */ CachedPageEventFlow this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CachedPageEventFlow$multicastedSrc$1(CachedPageEventFlow cachedPageEventFlow, j71 j71Var, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = cachedPageEventFlow;
        this.$src = j71Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        CachedPageEventFlow$multicastedSrc$1 cachedPageEventFlow$multicastedSrc$1 = new CachedPageEventFlow$multicastedSrc$1(this.this$0, this.$src, q70Var);
        cachedPageEventFlow$multicastedSrc$1.L$0 = obj;
        return cachedPageEventFlow$multicastedSrc$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((CachedPageEventFlow$multicastedSrc$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        AtomicBoolean atomicBoolean;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            k71 k71Var = (k71) this.L$0;
            atomicBoolean = this.this$0.b;
            if (atomicBoolean.compareAndSet(false, true)) {
                j71 w = n71.w(this.$src);
                this.label = 1;
                if (w.a(k71Var, this) == d) {
                    return d;
                }
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
