package androidx.paging;

import androidx.paging.PageFetcherSnapshotState;
import defpackage.gp2;
import defpackage.st1;
import defpackage.w02;
import defpackage.xk4;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: PageFetcherSnapshot.kt */
/* loaded from: classes.dex */
public final class PageFetcherSnapshot<Key, Value> {
    public final ib2<xk4> a;
    public xk4.a b;
    public final AtomicBoolean c;
    public final kx<yo2<Value>> d;
    public final PageFetcherSnapshotState.a<Key, Value> e;
    public final q30 f;
    public final j71<yo2<Value>> g;
    public final Key h;
    public final gp2<Key, Value> i;
    public final ep2 j;
    public final j71<te4> k;
    public final boolean l;
    public final p63<Key, Value> m;
    public final ip2<Key, Value> n;
    public final rc1<te4> o;

    /* compiled from: Collect.kt */
    /* loaded from: classes.dex */
    public static final class a implements k71<ve1> {
        public final /* synthetic */ LoadType f0;

        public a(LoadType loadType) {
            this.f0 = loadType;
        }

        @Override // defpackage.k71
        public Object emit(ve1 ve1Var, q70 q70Var) {
            Object q = PageFetcherSnapshot.this.q(this.f0, ve1Var, q70Var);
            return q == gs1.d() ? q : te4.a;
        }
    }

    public PageFetcherSnapshot(Key key, gp2<Key, Value> gp2Var, ep2 ep2Var, j71<te4> j71Var, boolean z, p63<Key, Value> p63Var, ip2<Key, Value> ip2Var, rc1<te4> rc1Var) {
        q30 b;
        fs1.f(gp2Var, "pagingSource");
        fs1.f(ep2Var, "config");
        fs1.f(j71Var, "retryFlow");
        fs1.f(rc1Var, "invalidate");
        this.h = key;
        this.i = gp2Var;
        this.j = ep2Var;
        this.k = j71Var;
        this.l = z;
        this.m = p63Var;
        this.n = ip2Var;
        this.o = rc1Var;
        if (ep2Var.f == Integer.MIN_VALUE || gp2Var.b()) {
            this.a = vn3.b(1, 0, null, 6, null);
            this.c = new AtomicBoolean(false);
            this.d = rx.b(-2, null, null, 6, null);
            this.e = new PageFetcherSnapshotState.a<>(ep2Var);
            b = zt1.b(null, 1, null);
            this.f = b;
            this.g = CancelableChannelFlowKt.a(b, new PageFetcherSnapshot$pageEventFlow$1(this, null));
            return;
        }
        throw new IllegalArgumentException("PagingConfig.jumpThreshold was set, but the associated PagingSource has not marked support for jumps by overriding PagingSource.jumpingSupported to true.".toString());
    }

    public final void l(xk4 xk4Var) {
        fs1.f(xk4Var, "viewportHint");
        if (xk4Var instanceof xk4.a) {
            this.b = (xk4.a) xk4Var;
        }
        this.a.d(xk4Var);
    }

    public final void m() {
        st1.a.a(this.f, null, 1, null);
    }

    public final /* synthetic */ Object n(j71<Integer> j71Var, LoadType loadType, q70<? super te4> q70Var) {
        Object a2 = n71.g(FlowExtKt.b(FlowExtKt.d(j71Var, new PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1(null, this, loadType)), new PageFetcherSnapshot$collectAsGenerationalViewportHints$3(loadType, null))).a(new a(loadType), q70Var);
        return a2 == gs1.d() ? a2 : te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x003e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object o(defpackage.q70<? super defpackage.ip2<Key, Value>> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof androidx.paging.PageFetcherSnapshot$currentPagingState$1
            if (r0 == 0) goto L13
            r0 = r6
            androidx.paging.PageFetcherSnapshot$currentPagingState$1 r0 = (androidx.paging.PageFetcherSnapshot$currentPagingState$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.PageFetcherSnapshot$currentPagingState$1 r0 = new androidx.paging.PageFetcherSnapshot$currentPagingState$1
            r0.<init>(r5, r6)
        L18:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L3e
            if (r2 != r4) goto L36
            java.lang.Object r1 = r0.L$2
            kb2 r1 = (defpackage.kb2) r1
            java.lang.Object r2 = r0.L$1
            androidx.paging.PageFetcherSnapshotState$a r2 = (androidx.paging.PageFetcherSnapshotState.a) r2
            java.lang.Object r0 = r0.L$0
            androidx.paging.PageFetcherSnapshot r0 = (androidx.paging.PageFetcherSnapshot) r0
            defpackage.o83.b(r6)
            goto L58
        L36:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L3e:
            defpackage.o83.b(r6)
            androidx.paging.PageFetcherSnapshotState$a<Key, Value> r2 = r5.e
            kb2 r6 = androidx.paging.PageFetcherSnapshotState.a.a(r2)
            r0.L$0 = r5
            r0.L$1 = r2
            r0.L$2 = r6
            r0.label = r4
            java.lang.Object r0 = r6.b(r3, r0)
            if (r0 != r1) goto L56
            return r1
        L56:
            r0 = r5
            r1 = r6
        L58:
            androidx.paging.PageFetcherSnapshotState r6 = androidx.paging.PageFetcherSnapshotState.a.b(r2)     // Catch: java.lang.Throwable -> L66
            xk4$a r0 = r0.b     // Catch: java.lang.Throwable -> L66
            ip2 r6 = r6.g(r0)     // Catch: java.lang.Throwable -> L66
            r1.a(r3)
            return r6
        L66:
            r6 = move-exception
            r1.a(r3)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcherSnapshot.o(q70):java.lang.Object");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:104:0x026e  */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:111:0x02a2 A[Catch: all -> 0x02c3, TRY_LEAVE, TryCatch #0 {all -> 0x02c3, blocks: (B:109:0x028b, B:111:0x02a2), top: B:125:0x028b }] */
    /* JADX WARN: Removed duplicated region for block: B:12:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x004f  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0064  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0078  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x008d  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x00a4  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x00b9  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00c3  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00e2  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0112 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0113  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0130 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0131  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x0139  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x0182 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:58:0x019b A[Catch: all -> 0x0264, TryCatch #1 {all -> 0x0264, blocks: (B:56:0x0183, B:58:0x019b, B:59:0x01a4, B:61:0x01ad, B:62:0x01b6), top: B:127:0x0183 }] */
    /* JADX WARN: Removed duplicated region for block: B:61:0x01ad A[Catch: all -> 0x0264, TryCatch #1 {all -> 0x0264, blocks: (B:56:0x0183, B:58:0x019b, B:59:0x01a4, B:61:0x01ad, B:62:0x01b6), top: B:127:0x0183 }] */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01bd  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x01f8 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x01f9  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x0205  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x020b  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x024a  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x0257  */
    /* JADX WARN: Type inference failed for: r2v0, types: [int] */
    /* JADX WARN: Type inference failed for: r2v1, types: [kb2] */
    /* JADX WARN: Type inference failed for: r2v2, types: [kb2] */
    /* JADX WARN: Type inference failed for: r2v33, types: [kb2] */
    /* JADX WARN: Type inference failed for: r2v46 */
    /* JADX WARN: Type inference failed for: r2v47 */
    /* JADX WARN: Type inference failed for: r2v48 */
    /* JADX WARN: Type inference failed for: r2v49 */
    /* JADX WARN: Type inference failed for: r2v9, types: [kb2] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final /* synthetic */ java.lang.Object p(defpackage.q70<? super defpackage.te4> r12) {
        /*
            Method dump skipped, instructions count: 748
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcherSnapshot.p(q70):java.lang.Object");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x044b  */
    /* JADX WARN: Removed duplicated region for block: B:12:0x0034  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x0483  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:147:0x04bf A[Catch: all -> 0x0108, TRY_LEAVE, TryCatch #4 {all -> 0x0108, blocks: (B:145:0x04aa, B:147:0x04bf, B:151:0x04d5, B:158:0x04e6, B:23:0x0103), top: B:258:0x0029 }] */
    /* JADX WARN: Removed duplicated region for block: B:166:0x0508  */
    /* JADX WARN: Removed duplicated region for block: B:167:0x050b  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:170:0x0536 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:171:0x0537  */
    /* JADX WARN: Removed duplicated region for block: B:174:0x0550 A[Catch: all -> 0x0681, TRY_LEAVE, TryCatch #0 {all -> 0x0681, blocks: (B:172:0x0542, B:174:0x0550, B:178:0x056e), top: B:251:0x0542 }] */
    /* JADX WARN: Removed duplicated region for block: B:185:0x058d  */
    /* JADX WARN: Removed duplicated region for block: B:188:0x05a3 A[Catch: all -> 0x00bd, TryCatch #8 {all -> 0x00bd, blocks: (B:182:0x0580, B:186:0x0590, B:188:0x05a3, B:190:0x05af, B:192:0x05b3, B:194:0x05c0, B:193:0x05ba, B:195:0x05c3, B:199:0x05e5, B:203:0x05f9, B:14:0x0082, B:17:0x00b8), top: B:258:0x0029 }] */
    /* JADX WARN: Removed duplicated region for block: B:192:0x05b3 A[Catch: all -> 0x00bd, TryCatch #8 {all -> 0x00bd, blocks: (B:182:0x0580, B:186:0x0590, B:188:0x05a3, B:190:0x05af, B:192:0x05b3, B:194:0x05c0, B:193:0x05ba, B:195:0x05c3, B:199:0x05e5, B:203:0x05f9, B:14:0x0082, B:17:0x00b8), top: B:258:0x0029 }] */
    /* JADX WARN: Removed duplicated region for block: B:193:0x05ba A[Catch: all -> 0x00bd, TryCatch #8 {all -> 0x00bd, blocks: (B:182:0x0580, B:186:0x0590, B:188:0x05a3, B:190:0x05af, B:192:0x05b3, B:194:0x05c0, B:193:0x05ba, B:195:0x05c3, B:199:0x05e5, B:203:0x05f9, B:14:0x0082, B:17:0x00b8), top: B:258:0x0029 }] */
    /* JADX WARN: Removed duplicated region for block: B:201:0x05ed A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:202:0x05ee  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x00c1  */
    /* JADX WARN: Removed duplicated region for block: B:222:0x064d A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:226:0x065e  */
    /* JADX WARN: Removed duplicated region for block: B:228:0x0667  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00f3  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x010c  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x012e  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0160  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0188  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x01b0  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x01da  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x01f8  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x023b  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x028b A[Catch: all -> 0x0694, TryCatch #2 {all -> 0x0694, blocks: (B:47:0x022c, B:71:0x02de, B:53:0x0242, B:54:0x0247, B:55:0x0248, B:57:0x0259, B:58:0x0265, B:60:0x026f, B:62:0x0288, B:63:0x028b, B:65:0x02a4, B:68:0x02c2, B:70:0x02db), top: B:255:0x022c }] */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0309 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:75:0x030a  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0329 A[Catch: all -> 0x068c, TRY_LEAVE, TryCatch #6 {all -> 0x068c, blocks: (B:76:0x0314, B:78:0x0329), top: B:262:0x0314 }] */
    /* JADX WARN: Removed duplicated region for block: B:83:0x0348  */
    /* JADX WARN: Removed duplicated region for block: B:87:0x035b  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x038c  */
    /* JADX WARN: Type inference failed for: r12v54, types: [androidx.paging.PageFetcherSnapshot, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r13v13 */
    /* JADX WARN: Type inference failed for: r13v16, types: [T] */
    /* JADX WARN: Type inference failed for: r13v48 */
    /* JADX WARN: Type inference failed for: r1v16, types: [T, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r4v1 */
    /* JADX WARN: Type inference failed for: r4v16 */
    /* JADX WARN: Type inference failed for: r4v17 */
    /* JADX WARN: Type inference failed for: r4v6, types: [kb2] */
    /* JADX WARN: Type inference failed for: r5v0, types: [int] */
    /* JADX WARN: Type inference failed for: r5v1 */
    /* JADX WARN: Type inference failed for: r5v19, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v2 */
    /* JADX WARN: Type inference failed for: r5v44, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v50 */
    /* JADX WARN: Type inference failed for: r5v75 */
    /* JADX WARN: Type inference failed for: r6v30, types: [java.lang.Object] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:217:0x0620 -> B:229:0x066e). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:219:0x0624 -> B:229:0x066e). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:221:0x064b -> B:256:0x064e). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final /* synthetic */ java.lang.Object q(androidx.paging.LoadType r18, defpackage.ve1 r19, defpackage.q70<? super defpackage.te4> r20) {
        /*
            Method dump skipped, instructions count: 1728
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcherSnapshot.q(androidx.paging.LoadType, ve1, q70):java.lang.Object");
    }

    public final j71<yo2<Value>> r() {
        return this.g;
    }

    public final gp2<Key, Value> s() {
        return this.i;
    }

    public final p63<Key, Value> t() {
        return this.m;
    }

    public final gp2.a<Key> u(LoadType loadType, Key key) {
        return gp2.a.c.a(loadType, key, loadType == LoadType.REFRESH ? this.j.d : this.j.a, this.j.c);
    }

    public final Key v(PageFetcherSnapshotState<Key, Value> pageFetcherSnapshotState, LoadType loadType, int i, int i2) {
        if (i == pageFetcherSnapshotState.j(loadType) && !(pageFetcherSnapshotState.p().d(loadType) instanceof w02.a) && i2 < this.j.b) {
            if (loadType == LoadType.PREPEND) {
                return (Key) ((gp2.b.C0179b) j20.L(pageFetcherSnapshotState.m())).e();
            }
            return (Key) ((gp2.b.C0179b) j20.U(pageFetcherSnapshotState.m())).d();
        }
        return null;
    }

    public final /* synthetic */ Object w(LoadType loadType, xk4 xk4Var, q70<? super te4> q70Var) {
        if (zo2.b[loadType.ordinal()] != 1) {
            if (xk4Var != null) {
                this.a.d(xk4Var);
            } else {
                throw new IllegalStateException("Cannot retry APPEND / PREPEND load on PagingSource without ViewportHint".toString());
            }
        } else {
            Object p = p(q70Var);
            if (p == gs1.d()) {
                return p;
            }
        }
        return te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final /* synthetic */ java.lang.Object x(androidx.paging.PageFetcherSnapshotState<Key, Value> r6, androidx.paging.LoadType r7, defpackage.q70<? super defpackage.te4> r8) {
        /*
            r5 = this;
            boolean r0 = r8 instanceof androidx.paging.PageFetcherSnapshot$setLoading$1
            if (r0 == 0) goto L13
            r0 = r8
            androidx.paging.PageFetcherSnapshot$setLoading$1 r0 = (androidx.paging.PageFetcherSnapshot$setLoading$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.PageFetcherSnapshot$setLoading$1 r0 = new androidx.paging.PageFetcherSnapshot$setLoading$1
            r0.<init>(r5, r8)
        L18:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L31
            if (r2 != r3) goto L29
            defpackage.o83.b(r8)
            goto L4d
        L29:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L31:
            defpackage.o83.b(r8)
            w02$b r8 = defpackage.w02.b.b
            boolean r6 = r6.u(r7, r8)
            if (r6 == 0) goto L4d
            kx<yo2<Value>> r6 = r5.d
            yo2$c r2 = new yo2$c
            r4 = 0
            r2.<init>(r7, r4, r8)
            r0.label = r3
            java.lang.Object r6 = r6.h(r2, r0)
            if (r6 != r1) goto L4d
            return r1
        L4d:
            te4 r6 = defpackage.te4.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcherSnapshot.x(androidx.paging.PageFetcherSnapshotState, androidx.paging.LoadType, q70):java.lang.Object");
    }

    public final void y(c90 c90Var) {
        if (this.j.f != Integer.MIN_VALUE) {
            as.b(c90Var, null, null, new PageFetcherSnapshot$startConsumingHints$1(this, null), 3, null);
        }
        as.b(c90Var, null, null, new PageFetcherSnapshot$startConsumingHints$2(this, null), 3, null);
        as.b(c90Var, null, null, new PageFetcherSnapshot$startConsumingHints$3(this, null), 3, null);
    }
}
