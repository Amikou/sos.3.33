package androidx.paging;

import androidx.paging.ActiveFlowTracker;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPagingData.kt */
@a(c = "androidx.paging.MulticastedPagingData$accumulated$2", f = "CachedPagingData.kt", l = {40}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class MulticastedPagingData$accumulated$2 extends SuspendLambda implements kd1<k71<? super yo2<T>>, Throwable, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ MulticastedPagingData this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MulticastedPagingData$accumulated$2(MulticastedPagingData multicastedPagingData, q70 q70Var) {
        super(3, q70Var);
        this.this$0 = multicastedPagingData;
    }

    public final q70<te4> create(k71<? super yo2<T>> k71Var, Throwable th, q70<? super te4> q70Var) {
        fs1.f(k71Var, "$this$create");
        fs1.f(q70Var, "continuation");
        return new MulticastedPagingData$accumulated$2(this.this$0, q70Var);
    }

    @Override // defpackage.kd1
    public final Object invoke(Object obj, Throwable th, q70<? super te4> q70Var) {
        return ((MulticastedPagingData$accumulated$2) create((k71) obj, th, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            ActiveFlowTracker c = this.this$0.c();
            if (c != null) {
                ActiveFlowTracker.FlowType flowType = ActiveFlowTracker.FlowType.PAGE_EVENT_FLOW;
                this.label = 1;
                if (c.b(flowType, this) == d) {
                    return d;
                }
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
