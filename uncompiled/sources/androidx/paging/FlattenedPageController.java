package androidx.paging;

import java.util.List;

/* compiled from: CachedPageEventFlow.kt */
/* loaded from: classes.dex */
public final class FlattenedPageController<T> {
    public final x61<T> a = new x61<>();
    public List<TemporaryDownstream<T>> b = b20.g();
    public final kb2 c = lb2.b(false, 1, null);

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0058  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0086 A[Catch: all -> 0x0040, TryCatch #0 {all -> 0x0040, blocks: (B:13:0x003b, B:25:0x0080, B:27:0x0086, B:29:0x008e, B:30:0x0091, B:24:0x006c), top: B:38:0x0023 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object a(defpackage.q70<? super androidx.paging.TemporaryDownstream<T>> r13) {
        /*
            r12 = this;
            boolean r0 = r13 instanceof androidx.paging.FlattenedPageController$createTemporaryDownstream$1
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 == 0) goto L13
            r0 = r13
            androidx.paging.FlattenedPageController$createTemporaryDownstream$1 r0 = (androidx.paging.FlattenedPageController$createTemporaryDownstream$1) r0
            int r2 = r0.label
            r3 = r2 & r1
            if (r3 == 0) goto L13
            int r2 = r2 - r1
            r0.label = r2
            goto L18
        L13:
            androidx.paging.FlattenedPageController$createTemporaryDownstream$1 r0 = new androidx.paging.FlattenedPageController$createTemporaryDownstream$1
            r0.<init>(r12, r13)
        L18:
            java.lang.Object r13 = r0.result
            java.lang.Object r2 = defpackage.gs1.d()
            int r3 = r0.label
            r4 = 2
            r5 = 0
            r6 = 1
            if (r3 == 0) goto L58
            if (r3 == r6) goto L4b
            if (r3 != r4) goto L43
            int r3 = r0.I$0
            java.lang.Object r6 = r0.L$3
            java.util.Iterator r6 = (java.util.Iterator) r6
            java.lang.Object r7 = r0.L$2
            androidx.paging.TemporaryDownstream r7 = (androidx.paging.TemporaryDownstream) r7
            java.lang.Object r8 = r0.L$1
            androidx.paging.TemporaryDownstream r8 = (androidx.paging.TemporaryDownstream) r8
            java.lang.Object r9 = r0.L$0
            kb2 r9 = (defpackage.kb2) r9
            defpackage.o83.b(r13)     // Catch: java.lang.Throwable -> L40
            r13 = r3
            goto L80
        L40:
            r13 = move-exception
            goto Lba
        L43:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r0)
            throw r13
        L4b:
            java.lang.Object r3 = r0.L$1
            kb2 r3 = (defpackage.kb2) r3
            java.lang.Object r6 = r0.L$0
            androidx.paging.FlattenedPageController r6 = (androidx.paging.FlattenedPageController) r6
            defpackage.o83.b(r13)
            r9 = r3
            goto L6c
        L58:
            defpackage.o83.b(r13)
            kb2 r13 = r12.c
            r0.L$0 = r12
            r0.L$1 = r13
            r0.label = r6
            java.lang.Object r3 = r13.b(r5, r0)
            if (r3 != r2) goto L6a
            return r2
        L6a:
            r6 = r12
            r9 = r13
        L6c:
            androidx.paging.TemporaryDownstream r13 = new androidx.paging.TemporaryDownstream     // Catch: java.lang.Throwable -> L40
            r13.<init>()     // Catch: java.lang.Throwable -> L40
            x61<T> r3 = r6.a     // Catch: java.lang.Throwable -> L40
            java.util.List r3 = r3.b()     // Catch: java.lang.Throwable -> L40
            r6 = 0
            java.util.Iterator r3 = r3.iterator()     // Catch: java.lang.Throwable -> L40
            r7 = r13
            r8 = r7
            r13 = r6
            r6 = r3
        L80:
            boolean r3 = r6.hasNext()     // Catch: java.lang.Throwable -> L40
            if (r3 == 0) goto Lb6
            java.lang.Object r3 = r6.next()     // Catch: java.lang.Throwable -> L40
            int r10 = r13 + 1
            if (r13 >= 0) goto L91
            defpackage.b20.p()     // Catch: java.lang.Throwable -> L40
        L91:
            java.lang.Integer r13 = defpackage.hr.d(r13)     // Catch: java.lang.Throwable -> L40
            yo2 r3 = (defpackage.yo2) r3     // Catch: java.lang.Throwable -> L40
            int r13 = r13.intValue()     // Catch: java.lang.Throwable -> L40
            lq1 r11 = new lq1     // Catch: java.lang.Throwable -> L40
            int r13 = r13 + r1
            r11.<init>(r13, r3)     // Catch: java.lang.Throwable -> L40
            r0.L$0 = r9     // Catch: java.lang.Throwable -> L40
            r0.L$1 = r8     // Catch: java.lang.Throwable -> L40
            r0.L$2 = r7     // Catch: java.lang.Throwable -> L40
            r0.L$3 = r6     // Catch: java.lang.Throwable -> L40
            r0.I$0 = r10     // Catch: java.lang.Throwable -> L40
            r0.label = r4     // Catch: java.lang.Throwable -> L40
            java.lang.Object r13 = r7.c(r11, r0)     // Catch: java.lang.Throwable -> L40
            if (r13 != r2) goto Lb4
            return r2
        Lb4:
            r13 = r10
            goto L80
        Lb6:
            r9.a(r5)
            return r8
        Lba:
            r9.a(r5)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.FlattenedPageController.a(q70):java.lang.Object");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0097 A[Catch: all -> 0x0044, TryCatch #0 {all -> 0x0044, blocks: (B:13:0x003f, B:30:0x00b3, B:25:0x0091, B:27:0x0097, B:33:0x00c7, B:32:0x00c3, B:24:0x0077), top: B:38:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00c3 A[Catch: all -> 0x0044, TryCatch #0 {all -> 0x0044, blocks: (B:13:0x003f, B:30:0x00b3, B:25:0x0091, B:27:0x0097, B:33:0x00c7, B:32:0x00c3, B:24:0x0077), top: B:38:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00c7 A[Catch: all -> 0x0044, TRY_LEAVE, TryCatch #0 {all -> 0x0044, blocks: (B:13:0x003f, B:30:0x00b3, B:25:0x0091, B:27:0x0097, B:33:0x00c7, B:32:0x00c3, B:24:0x0077), top: B:38:0x0023 }] */
    /* JADX WARN: Type inference failed for: r4v10, types: [java.util.Collection] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:28:0x00b0 -> B:30:0x00b3). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object b(defpackage.lq1<? extends defpackage.yo2<T>> r10, defpackage.q70<? super defpackage.te4> r11) {
        /*
            Method dump skipped, instructions count: 213
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.FlattenedPageController.b(lq1, q70):java.lang.Object");
    }
}
