package androidx.paging;

import androidx.paging.PageFetcherSnapshotState;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PageFetcherSnapshot.kt */
@a(c = "androidx.paging.PageFetcherSnapshot$startConsumingHints$3", f = "PageFetcherSnapshot.kt", l = {595, 223}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcherSnapshot$startConsumingHints$3 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public final /* synthetic */ PageFetcherSnapshot this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcherSnapshot$startConsumingHints$3(PageFetcherSnapshot pageFetcherSnapshot, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = pageFetcherSnapshot;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new PageFetcherSnapshot$startConsumingHints$3(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((PageFetcherSnapshot$startConsumingHints$3) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        PageFetcherSnapshot pageFetcherSnapshot;
        PageFetcherSnapshotState.a aVar;
        kb2 kb2Var;
        kb2 kb2Var2;
        PageFetcherSnapshotState pageFetcherSnapshotState;
        Object d = gs1.d();
        int i = this.label;
        try {
            if (i == 0) {
                o83.b(obj);
                pageFetcherSnapshot = this.this$0;
                aVar = pageFetcherSnapshot.e;
                kb2Var = aVar.a;
                this.L$0 = aVar;
                this.L$1 = kb2Var;
                this.L$2 = pageFetcherSnapshot;
                this.label = 1;
                if (kb2Var.b(null, this) == d) {
                    return d;
                }
                kb2Var2 = kb2Var;
            } else if (i != 1) {
                if (i != 2) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                o83.b(obj);
                return te4.a;
            } else {
                pageFetcherSnapshot = (PageFetcherSnapshot) this.L$2;
                kb2Var2 = (kb2) this.L$1;
                aVar = (PageFetcherSnapshotState.a) this.L$0;
                o83.b(obj);
            }
            pageFetcherSnapshotState = aVar.b;
            j71<Integer> e = pageFetcherSnapshotState.e();
            kb2Var2.a(null);
            LoadType loadType = LoadType.APPEND;
            this.L$0 = null;
            this.L$1 = null;
            this.L$2 = null;
            this.label = 2;
            if (pageFetcherSnapshot.n(e, loadType, this) == d) {
                return d;
            }
            return te4.a;
        } catch (Throwable th) {
            kb2Var2.a(null);
            throw th;
        }
    }
}
