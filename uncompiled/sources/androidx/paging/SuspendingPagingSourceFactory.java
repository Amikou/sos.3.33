package androidx.paging;

import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.a;

/* compiled from: SuspendingPagingSourceFactory.kt */
/* loaded from: classes.dex */
public final class SuspendingPagingSourceFactory<Key, Value> implements rc1<gp2<Key, Value>> {
    public final CoroutineDispatcher a;
    public final rc1<gp2<Key, Value>> f0;

    public final Object b(q70<? super gp2<Key, Value>> q70Var) {
        return a.e(this.a, new SuspendingPagingSourceFactory$create$2(this, null), q70Var);
    }

    @Override // defpackage.rc1
    /* renamed from: c */
    public gp2<Key, Value> invoke() {
        return this.f0.invoke();
    }
}
