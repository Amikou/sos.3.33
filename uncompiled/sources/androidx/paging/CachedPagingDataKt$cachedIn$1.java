package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPagingData.kt */
@a(c = "androidx.paging.CachedPagingDataKt$cachedIn$1", f = "CachedPagingData.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CachedPagingDataKt$cachedIn$1 extends SuspendLambda implements hd1<fp2<T>, q70<? super te4>, Object> {
    public int label;

    public CachedPagingDataKt$cachedIn$1(q70 q70Var) {
        super(2, q70Var);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new CachedPagingDataKt$cachedIn$1(q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((CachedPagingDataKt$cachedIn$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
