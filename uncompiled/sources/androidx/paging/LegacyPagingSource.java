package androidx.paging;

import defpackage.fe0;
import defpackage.gp2;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: LegacyPagingSource.kt */
/* loaded from: classes.dex */
public final class LegacyPagingSource<Key, Value> extends gp2<Key, Value> {
    public int c;
    public final CoroutineDispatcher d;
    public final fe0<Key, Value> e;

    /* compiled from: LegacyPagingSource.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    @Override // defpackage.gp2
    public boolean b() {
        throw null;
    }

    @Override // defpackage.gp2
    public Key d(ip2<Key, Value> ip2Var) {
        fs1.f(ip2Var, "state");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [fe0$b, T] */
    @Override // defpackage.gp2
    public Object f(gp2.a<Key> aVar, q70<? super gp2.b<Key, Value>> q70Var) {
        LoadType loadType;
        if (aVar instanceof gp2.a.d) {
            loadType = LoadType.REFRESH;
        } else if (aVar instanceof gp2.a.C0178a) {
            loadType = LoadType.APPEND;
        } else if (!(aVar instanceof gp2.a.c)) {
            throw new NoWhenBranchMatchedException();
        } else {
            loadType = LoadType.PREPEND;
        }
        LoadType loadType2 = loadType;
        if (this.c == Integer.MIN_VALUE) {
            System.out.println((Object) "WARNING: pageSize on the LegacyPagingSource is not set.\nWhen using legacy DataSource / DataSourceFactory with Paging3, page size\nshould've been set by the paging library but it is not set yet.\n\nIf you are seeing this message in tests where you are testing DataSource\nin isolation (without a Pager), it is expected and page size will be estimated\nbased on parameters.\n\nIf you are seeing this message despite using a Pager, please file a bug:\nhttps://issuetracker.google.com/issues/new?component=413106");
            this.c = j(aVar);
        }
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = new fe0.b(loadType2, aVar.a(), aVar.b(), aVar.c(), this.c);
        return kotlinx.coroutines.a.e(this.d, new LegacyPagingSource$load$2(this, ref$ObjectRef, aVar, null), q70Var);
    }

    public final fe0<Key, Value> i() {
        return this.e;
    }

    public final int j(gp2.a<Key> aVar) {
        if ((aVar instanceof gp2.a.d) && aVar.b() % 3 == 0) {
            return aVar.b() / 3;
        }
        return aVar.b();
    }

    public final void k(int i) {
        int i2 = this.c;
        if (i2 == Integer.MIN_VALUE || i == i2) {
            this.c = i;
            return;
        }
        throw new IllegalStateException(("Page size is already set to " + this.c + '.').toString());
    }
}
