package androidx.paging;

import kotlin.jvm.internal.FunctionReferenceImpl;

/* compiled from: Pager.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class Pager$flow$1 extends FunctionReferenceImpl implements tc1<q70<? super gp2<Key, Value>>, Object> {
    public Pager$flow$1(rc1 rc1Var) {
        super(1, rc1Var, SuspendingPagingSourceFactory.class, "create", "create(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", 0);
    }

    @Override // defpackage.tc1
    public final Object invoke(q70<? super gp2<Key, Value>> q70Var) {
        return ((SuspendingPagingSourceFactory) ((rc1) this.receiver)).b(q70Var);
    }
}
