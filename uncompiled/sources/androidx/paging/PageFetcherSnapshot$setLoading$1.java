package androidx.paging;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PageFetcherSnapshot.kt */
@a(c = "androidx.paging.PageFetcherSnapshot", f = "PageFetcherSnapshot.kt", l = {506}, m = "setLoading")
/* loaded from: classes.dex */
public final class PageFetcherSnapshot$setLoading$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ PageFetcherSnapshot this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcherSnapshot$setLoading$1(PageFetcherSnapshot pageFetcherSnapshot, q70 q70Var) {
        super(q70Var);
        this.this$0 = pageFetcherSnapshot;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.x(null, null, this);
    }
}
