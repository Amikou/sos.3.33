package androidx.paging;

import defpackage.yo2;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;

/* compiled from: PageFetcher.kt */
@a(c = "androidx.paging.PageFetcher$injectRemoteEvents$1", f = "PageFetcher.kt", l = {253}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcher$injectRemoteEvents$1 extends SuspendLambda implements hd1<lp3<yo2<Value>>, q70<? super te4>, Object> {
    public final /* synthetic */ o63 $accessor;
    public final /* synthetic */ PageFetcherSnapshot $this_injectRemoteEvents;
    private /* synthetic */ Object L$0;
    public int label;

    /* compiled from: PageFetcher.kt */
    @a(c = "androidx.paging.PageFetcher$injectRemoteEvents$1$1", f = "PageFetcher.kt", l = {140}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcher$injectRemoteEvents$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements kd1<LoadType, w02, q70<? super te4>, Object> {
        public final /* synthetic */ lp3 $this_simpleChannelFlow;
        private /* synthetic */ Object L$0;
        private /* synthetic */ Object L$1;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(lp3 lp3Var, q70 q70Var) {
            super(3, q70Var);
            this.$this_simpleChannelFlow = lp3Var;
        }

        public final q70<te4> create(LoadType loadType, w02 w02Var, q70<? super te4> q70Var) {
            fs1.f(loadType, "type");
            fs1.f(w02Var, "state");
            fs1.f(q70Var, "continuation");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$this_simpleChannelFlow, q70Var);
            anonymousClass1.L$0 = loadType;
            anonymousClass1.L$1 = w02Var;
            return anonymousClass1;
        }

        @Override // defpackage.kd1
        public final Object invoke(LoadType loadType, w02 w02Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(loadType, w02Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                LoadType loadType = (LoadType) this.L$0;
                w02 w02Var = (w02) this.L$1;
                if (yo2.c.d.a(w02Var, true)) {
                    lp3 lp3Var = this.$this_simpleChannelFlow;
                    yo2.c cVar = new yo2.c(loadType, true, w02Var);
                    this.L$0 = null;
                    this.label = 1;
                    if (lp3Var.h(cVar, this) == d) {
                        return d;
                    }
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* compiled from: PageFetcher.kt */
    @a(c = "androidx.paging.PageFetcher$injectRemoteEvents$1$2", f = "PageFetcher.kt", l = {253}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcher$injectRemoteEvents$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ AnonymousClass1 $dispatchIfValid$1;
        public final /* synthetic */ hb2 $loadStates;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(hb2 hb2Var, AnonymousClass1 anonymousClass1, q70 q70Var) {
            super(2, q70Var);
            this.$loadStates = hb2Var;
            this.$dispatchIfValid$1 = anonymousClass1;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            return new AnonymousClass2(this.$loadStates, this.$dispatchIfValid$1, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass2) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Type inference failed for: r1v2, types: [T, x02] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = x02.e.a();
                ws3<x02> state = PageFetcher$injectRemoteEvents$1.this.$accessor.getState();
                PageFetcher$injectRemoteEvents$1$2$invokeSuspend$$inlined$collect$1 pageFetcher$injectRemoteEvents$1$2$invokeSuspend$$inlined$collect$1 = new PageFetcher$injectRemoteEvents$1$2$invokeSuspend$$inlined$collect$1(this, ref$ObjectRef);
                this.label = 1;
                if (state.a(pageFetcher$injectRemoteEvents$1$2$invokeSuspend$$inlined$collect$1, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcher$injectRemoteEvents$1(PageFetcherSnapshot pageFetcherSnapshot, o63 o63Var, q70 q70Var) {
        super(2, q70Var);
        this.$this_injectRemoteEvents = pageFetcherSnapshot;
        this.$accessor = o63Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        PageFetcher$injectRemoteEvents$1 pageFetcher$injectRemoteEvents$1 = new PageFetcher$injectRemoteEvents$1(this.$this_injectRemoteEvents, this.$accessor, q70Var);
        pageFetcher$injectRemoteEvents$1.L$0 = obj;
        return pageFetcher$injectRemoteEvents$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((PageFetcher$injectRemoteEvents$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            lp3 lp3Var = (lp3) this.L$0;
            hb2 hb2Var = new hb2();
            as.b(lp3Var, null, null, new AnonymousClass2(hb2Var, new AnonymousClass1(lp3Var, null), null), 3, null);
            j71 r = this.$this_injectRemoteEvents.r();
            PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1 pageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1 = new PageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1(this, lp3Var, hb2Var);
            this.label = 1;
            if (r.a(pageFetcher$injectRemoteEvents$1$invokeSuspend$$inlined$collect$1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
