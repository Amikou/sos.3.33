package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: FlowExt.kt */
@a(c = "androidx.paging.FlowExtKt$simpleTransformLatest$1", f = "FlowExt.kt", l = {76}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class FlowExtKt$simpleTransformLatest$1 extends SuspendLambda implements hd1<lp3<R>, q70<? super te4>, Object> {
    public final /* synthetic */ j71 $this_simpleTransformLatest;
    public final /* synthetic */ kd1 $transform;
    private /* synthetic */ Object L$0;
    public int label;

    /* compiled from: FlowExt.kt */
    @a(c = "androidx.paging.FlowExtKt$simpleTransformLatest$1$1", f = "FlowExt.kt", l = {77}, m = "invokeSuspend")
    /* renamed from: androidx.paging.FlowExtKt$simpleTransformLatest$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<T, q70<? super te4>, Object> {
        public final /* synthetic */ nx $collector;
        private /* synthetic */ Object L$0;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(nx nxVar, q70 q70Var) {
            super(2, q70Var);
            this.$collector = nxVar;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$collector, q70Var);
            anonymousClass1.L$0 = obj;
            return anonymousClass1;
        }

        @Override // defpackage.hd1
        public final Object invoke(Object obj, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(obj, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                Object obj2 = this.L$0;
                kd1 kd1Var = FlowExtKt$simpleTransformLatest$1.this.$transform;
                nx nxVar = this.$collector;
                this.label = 1;
                if (kd1Var.invoke(nxVar, obj2, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FlowExtKt$simpleTransformLatest$1(j71 j71Var, kd1 kd1Var, q70 q70Var) {
        super(2, q70Var);
        this.$this_simpleTransformLatest = j71Var;
        this.$transform = kd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        FlowExtKt$simpleTransformLatest$1 flowExtKt$simpleTransformLatest$1 = new FlowExtKt$simpleTransformLatest$1(this.$this_simpleTransformLatest, this.$transform, q70Var);
        flowExtKt$simpleTransformLatest$1.L$0 = obj;
        return flowExtKt$simpleTransformLatest$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((FlowExtKt$simpleTransformLatest$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            j71 j71Var = this.$this_simpleTransformLatest;
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(new nx((lp3) this.L$0), null);
            this.label = 1;
            if (n71.f(j71Var, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
