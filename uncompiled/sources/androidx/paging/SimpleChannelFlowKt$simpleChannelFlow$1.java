package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SimpleChannelFlow.kt */
@a(c = "androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1", f = "SimpleChannelFlow.kt", l = {46}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class SimpleChannelFlowKt$simpleChannelFlow$1 extends SuspendLambda implements hd1<k71<? super T>, q70<? super te4>, Object> {
    public final /* synthetic */ hd1 $block;
    private /* synthetic */ Object L$0;
    public int label;

    /* compiled from: SimpleChannelFlow.kt */
    @a(c = "androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1$1", f = "SimpleChannelFlow.kt", l = {64, 65}, m = "invokeSuspend")
    /* renamed from: androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ k71 $this_flow;
        private /* synthetic */ Object L$0;
        public Object L$1;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(k71 k71Var, q70 q70Var) {
            super(2, q70Var);
            this.$this_flow = k71Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$this_flow, q70Var);
            anonymousClass1.L$0 = obj;
            return anonymousClass1;
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x005e A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:15:0x005f  */
        /* JADX WARN: Removed duplicated region for block: B:18:0x006c  */
        /* JADX WARN: Removed duplicated region for block: B:22:0x0083  */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:21:0x007f -> B:12:0x0052). Please submit an issue!!! */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = defpackage.gs1.d()
                int r1 = r12.label
                r2 = 2
                r3 = 1
                r4 = 0
                if (r1 == 0) goto L32
                if (r1 == r3) goto L25
                if (r1 != r2) goto L1d
                java.lang.Object r1 = r12.L$1
                qx r1 = (defpackage.qx) r1
                java.lang.Object r5 = r12.L$0
                st1 r5 = (defpackage.st1) r5
                defpackage.o83.b(r13)
                r13 = r1
                r1 = r5
                goto L51
            L1d:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L25:
                java.lang.Object r1 = r12.L$1
                qx r1 = (defpackage.qx) r1
                java.lang.Object r5 = r12.L$0
                st1 r5 = (defpackage.st1) r5
                defpackage.o83.b(r13)
                r6 = r12
                goto L64
            L32:
                defpackage.o83.b(r13)
                java.lang.Object r13 = r12.L$0
                r5 = r13
                c90 r5 = (defpackage.c90) r5
                r13 = 0
                r1 = 6
                kx r13 = defpackage.rx.b(r13, r4, r4, r1, r4)
                r6 = 0
                r7 = 0
                androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1 r8 = new androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1
                r8.<init>(r12, r13, r4)
                r9 = 3
                r10 = 0
                st1 r1 = kotlinx.coroutines.a.b(r5, r6, r7, r8, r9, r10)
                qx r13 = r13.iterator()
            L51:
                r5 = r12
            L52:
                r5.L$0 = r1
                r5.L$1 = r13
                r5.label = r3
                java.lang.Object r6 = r13.a(r5)
                if (r6 != r0) goto L5f
                return r0
            L5f:
                r11 = r1
                r1 = r13
                r13 = r6
                r6 = r5
                r5 = r11
            L64:
                java.lang.Boolean r13 = (java.lang.Boolean) r13
                boolean r13 = r13.booleanValue()
                if (r13 == 0) goto L83
                java.lang.Object r13 = r1.next()
                k71 r7 = r6.$this_flow
                r6.L$0 = r5
                r6.L$1 = r1
                r6.label = r2
                java.lang.Object r13 = r7.emit(r13, r6)
                if (r13 != r0) goto L7f
                return r0
            L7f:
                r13 = r1
                r1 = r5
                r5 = r6
                goto L52
            L83:
                defpackage.st1.a.a(r5, r4, r3, r4)
                te4 r13 = defpackage.te4.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SimpleChannelFlowKt$simpleChannelFlow$1(hd1 hd1Var, q70 q70Var) {
        super(2, q70Var);
        this.$block = hd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        SimpleChannelFlowKt$simpleChannelFlow$1 simpleChannelFlowKt$simpleChannelFlow$1 = new SimpleChannelFlowKt$simpleChannelFlow$1(this.$block, q70Var);
        simpleChannelFlowKt$simpleChannelFlow$1.L$0 = obj;
        return simpleChannelFlowKt$simpleChannelFlow$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((SimpleChannelFlowKt$simpleChannelFlow$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            AnonymousClass1 anonymousClass1 = new AnonymousClass1((k71) this.L$0, null);
            this.label = 1;
            if (d90.b(anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
