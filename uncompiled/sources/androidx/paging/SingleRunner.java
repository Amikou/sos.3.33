package androidx.paging;

import java.util.concurrent.CancellationException;

/* compiled from: SingleRunner.kt */
/* loaded from: classes.dex */
public final class SingleRunner {
    public final Holder a;

    /* compiled from: SingleRunner.kt */
    /* loaded from: classes.dex */
    public static final class CancelIsolatedRunnerException extends CancellationException {
        private final SingleRunner runner;

        public CancelIsolatedRunnerException(SingleRunner singleRunner) {
            fs1.f(singleRunner, "runner");
            this.runner = singleRunner;
        }

        public final SingleRunner getRunner() {
            return this.runner;
        }
    }

    /* compiled from: SingleRunner.kt */
    /* loaded from: classes.dex */
    public static final class Holder {
        public final kb2 a;
        public st1 b;
        public int c;
        public final SingleRunner d;
        public final boolean e;

        public Holder(SingleRunner singleRunner, boolean z) {
            fs1.f(singleRunner, "singleRunner");
            this.d = singleRunner;
            this.e = z;
            this.a = lb2.b(false, 1, null);
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x0040  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x0059 A[Catch: all -> 0x0061, TryCatch #0 {all -> 0x0061, blocks: (B:18:0x0055, B:20:0x0059, B:21:0x005b), top: B:27:0x0055 }] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object a(defpackage.st1 r6, defpackage.q70<? super defpackage.te4> r7) {
            /*
                r5 = this;
                boolean r0 = r7 instanceof androidx.paging.SingleRunner$Holder$onFinish$1
                if (r0 == 0) goto L13
                r0 = r7
                androidx.paging.SingleRunner$Holder$onFinish$1 r0 = (androidx.paging.SingleRunner$Holder$onFinish$1) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L13
                int r1 = r1 - r2
                r0.label = r1
                goto L18
            L13:
                androidx.paging.SingleRunner$Holder$onFinish$1 r0 = new androidx.paging.SingleRunner$Holder$onFinish$1
                r0.<init>(r5, r7)
            L18:
                java.lang.Object r7 = r0.result
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 1
                r4 = 0
                if (r2 == 0) goto L40
                if (r2 != r3) goto L38
                java.lang.Object r6 = r0.L$2
                kb2 r6 = (defpackage.kb2) r6
                java.lang.Object r1 = r0.L$1
                st1 r1 = (defpackage.st1) r1
                java.lang.Object r0 = r0.L$0
                androidx.paging.SingleRunner$Holder r0 = (androidx.paging.SingleRunner.Holder) r0
                defpackage.o83.b(r7)
                r7 = r6
                r6 = r1
                goto L55
            L38:
                java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
                java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
                r6.<init>(r7)
                throw r6
            L40:
                defpackage.o83.b(r7)
                kb2 r7 = r5.a
                r0.L$0 = r5
                r0.L$1 = r6
                r0.L$2 = r7
                r0.label = r3
                java.lang.Object r0 = r7.b(r4, r0)
                if (r0 != r1) goto L54
                return r1
            L54:
                r0 = r5
            L55:
                st1 r1 = r0.b     // Catch: java.lang.Throwable -> L61
                if (r6 != r1) goto L5b
                r0.b = r4     // Catch: java.lang.Throwable -> L61
            L5b:
                te4 r6 = defpackage.te4.a     // Catch: java.lang.Throwable -> L61
                r7.a(r4)
                return r6
            L61:
                r6 = move-exception
                r7.a(r4)
                throw r6
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.SingleRunner.Holder.a(st1, q70):java.lang.Object");
        }

        /* JADX WARN: Code restructure failed: missing block: B:36:0x008b, code lost:
            r12.a(new androidx.paging.SingleRunner.CancelIsolatedRunnerException(r6.d));
         */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x0059  */
        /* JADX WARN: Removed duplicated region for block: B:38:0x0097 A[Catch: all -> 0x003c, TryCatch #0 {all -> 0x003c, blocks: (B:13:0x0037, B:43:0x00ac, B:44:0x00b0, B:24:0x0072, B:26:0x0076, B:28:0x007c, B:31:0x0082, B:36:0x008b, B:38:0x0097), top: B:49:0x0023 }] */
        /* JADX WARN: Type inference failed for: r11v0, types: [st1, java.lang.Object] */
        /* JADX WARN: Type inference failed for: r11v1, types: [kb2] */
        /* JADX WARN: Type inference failed for: r11v14 */
        /* JADX WARN: Type inference failed for: r11v15 */
        /* JADX WARN: Type inference failed for: r11v4, types: [kb2] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object b(int r10, defpackage.st1 r11, defpackage.q70<? super java.lang.Boolean> r12) {
            /*
                r9 = this;
                boolean r0 = r12 instanceof androidx.paging.SingleRunner$Holder$tryEnqueue$1
                if (r0 == 0) goto L13
                r0 = r12
                androidx.paging.SingleRunner$Holder$tryEnqueue$1 r0 = (androidx.paging.SingleRunner$Holder$tryEnqueue$1) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L13
                int r1 = r1 - r2
                r0.label = r1
                goto L18
            L13:
                androidx.paging.SingleRunner$Holder$tryEnqueue$1 r0 = new androidx.paging.SingleRunner$Holder$tryEnqueue$1
                r0.<init>(r9, r12)
            L18:
                java.lang.Object r12 = r0.result
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 2
                r4 = 0
                r5 = 1
                if (r2 == 0) goto L59
                if (r2 == r5) goto L47
                if (r2 != r3) goto L3f
                int r10 = r0.I$0
                java.lang.Object r11 = r0.L$2
                kb2 r11 = (defpackage.kb2) r11
                java.lang.Object r1 = r0.L$1
                st1 r1 = (defpackage.st1) r1
                java.lang.Object r0 = r0.L$0
                androidx.paging.SingleRunner$Holder r0 = (androidx.paging.SingleRunner.Holder) r0
                defpackage.o83.b(r12)     // Catch: java.lang.Throwable -> L3c
                goto Laa
            L3c:
                r10 = move-exception
                goto Lb8
            L3f:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r11)
                throw r10
            L47:
                int r10 = r0.I$0
                java.lang.Object r11 = r0.L$2
                kb2 r11 = (defpackage.kb2) r11
                java.lang.Object r2 = r0.L$1
                st1 r2 = (defpackage.st1) r2
                java.lang.Object r6 = r0.L$0
                androidx.paging.SingleRunner$Holder r6 = (androidx.paging.SingleRunner.Holder) r6
                defpackage.o83.b(r12)
                goto L72
            L59:
                defpackage.o83.b(r12)
                kb2 r12 = r9.a
                r0.L$0 = r9
                r0.L$1 = r11
                r0.L$2 = r12
                r0.I$0 = r10
                r0.label = r5
                java.lang.Object r2 = r12.b(r4, r0)
                if (r2 != r1) goto L6f
                return r1
            L6f:
                r6 = r9
                r2 = r11
                r11 = r12
            L72:
                st1 r12 = r6.b     // Catch: java.lang.Throwable -> L3c
                if (r12 == 0) goto L89
                boolean r7 = r12.b()     // Catch: java.lang.Throwable -> L3c
                if (r7 == 0) goto L89
                int r7 = r6.c     // Catch: java.lang.Throwable -> L3c
                if (r7 < r10) goto L89
                if (r7 != r10) goto L87
                boolean r7 = r6.e     // Catch: java.lang.Throwable -> L3c
                if (r7 == 0) goto L87
                goto L89
            L87:
                r5 = 0
                goto Lb0
            L89:
                if (r12 == 0) goto L95
                androidx.paging.SingleRunner$CancelIsolatedRunnerException r7 = new androidx.paging.SingleRunner$CancelIsolatedRunnerException     // Catch: java.lang.Throwable -> L3c
                androidx.paging.SingleRunner r8 = r6.d     // Catch: java.lang.Throwable -> L3c
                r7.<init>(r8)     // Catch: java.lang.Throwable -> L3c
                r12.a(r7)     // Catch: java.lang.Throwable -> L3c
            L95:
                if (r12 == 0) goto Lac
                r0.L$0 = r6     // Catch: java.lang.Throwable -> L3c
                r0.L$1 = r2     // Catch: java.lang.Throwable -> L3c
                r0.L$2 = r11     // Catch: java.lang.Throwable -> L3c
                r0.I$0 = r10     // Catch: java.lang.Throwable -> L3c
                r0.label = r3     // Catch: java.lang.Throwable -> L3c
                java.lang.Object r12 = r12.F(r0)     // Catch: java.lang.Throwable -> L3c
                if (r12 != r1) goto La8
                return r1
            La8:
                r1 = r2
                r0 = r6
            Laa:
                r6 = r0
                r2 = r1
            Lac:
                r6.b = r2     // Catch: java.lang.Throwable -> L3c
                r6.c = r10     // Catch: java.lang.Throwable -> L3c
            Lb0:
                java.lang.Boolean r10 = defpackage.hr.a(r5)     // Catch: java.lang.Throwable -> L3c
                r11.a(r4)
                return r10
            Lb8:
                r11.a(r4)
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.SingleRunner.Holder.b(int, st1, q70):java.lang.Object");
        }
    }

    /* compiled from: SingleRunner.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public SingleRunner() {
        this(false, 1, null);
    }

    public SingleRunner(boolean z) {
        this.a = new Holder(this, z);
    }

    public static /* synthetic */ Object c(SingleRunner singleRunner, int i, tc1 tc1Var, q70 q70Var, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return singleRunner.b(i, tc1Var, q70Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0037  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0056  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object b(int r5, defpackage.tc1<? super defpackage.q70<? super defpackage.te4>, ? extends java.lang.Object> r6, defpackage.q70<? super defpackage.te4> r7) {
        /*
            r4 = this;
            boolean r0 = r7 instanceof androidx.paging.SingleRunner$runInIsolation$1
            if (r0 == 0) goto L13
            r0 = r7
            androidx.paging.SingleRunner$runInIsolation$1 r0 = (androidx.paging.SingleRunner$runInIsolation$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.SingleRunner$runInIsolation$1 r0 = new androidx.paging.SingleRunner$runInIsolation$1
            r0.<init>(r4, r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L37
            if (r2 != r3) goto L2f
            java.lang.Object r5 = r0.L$0
            androidx.paging.SingleRunner r5 = (androidx.paging.SingleRunner) r5
            defpackage.o83.b(r7)     // Catch: androidx.paging.SingleRunner.CancelIsolatedRunnerException -> L2d
            goto L53
        L2d:
            r6 = move-exception
            goto L4d
        L2f:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L37:
            defpackage.o83.b(r7)
            androidx.paging.SingleRunner$runInIsolation$2 r7 = new androidx.paging.SingleRunner$runInIsolation$2     // Catch: androidx.paging.SingleRunner.CancelIsolatedRunnerException -> L4b
            r2 = 0
            r7.<init>(r4, r5, r6, r2)     // Catch: androidx.paging.SingleRunner.CancelIsolatedRunnerException -> L4b
            r0.L$0 = r4     // Catch: androidx.paging.SingleRunner.CancelIsolatedRunnerException -> L4b
            r0.label = r3     // Catch: androidx.paging.SingleRunner.CancelIsolatedRunnerException -> L4b
            java.lang.Object r5 = defpackage.d90.b(r7, r0)     // Catch: androidx.paging.SingleRunner.CancelIsolatedRunnerException -> L4b
            if (r5 != r1) goto L53
            return r1
        L4b:
            r6 = move-exception
            r5 = r4
        L4d:
            androidx.paging.SingleRunner r7 = r6.getRunner()
            if (r7 != r5) goto L56
        L53:
            te4 r5 = defpackage.te4.a
            return r5
        L56:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.SingleRunner.b(int, tc1, q70):java.lang.Object");
    }

    public /* synthetic */ SingleRunner(boolean z, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? true : z);
    }
}
