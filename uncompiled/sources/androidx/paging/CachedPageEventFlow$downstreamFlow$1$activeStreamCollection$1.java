package androidx.paging;

import androidx.paging.multicast.Multicaster;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.channels.ClosedSendChannelException;

/* compiled from: CachedPageEventFlow.kt */
@a(c = "androidx.paging.CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1", f = "CachedPageEventFlow.kt", l = {292}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ st1 $historyCollection;
    public final /* synthetic */ Ref$IntRef $lastReceivedHistoryIndex;
    public final /* synthetic */ TemporaryDownstream $snapshot;
    public final /* synthetic */ lp3 $this_simpleChannelFlow;
    public int label;
    public final /* synthetic */ CachedPageEventFlow$downstreamFlow$1 this$0;

    /* compiled from: CachedPageEventFlow.kt */
    @a(c = "androidx.paging.CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$1", f = "CachedPageEventFlow.kt", l = {}, m = "invokeSuspend")
    /* renamed from: androidx.paging.CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements kd1<k71<? super lq1<? extends yo2<T>>>, Throwable, q70<? super te4>, Object> {
        private /* synthetic */ Object L$0;
        public int label;

        public AnonymousClass1(q70 q70Var) {
            super(3, q70Var);
        }

        public final q70<te4> create(k71<? super lq1<? extends yo2<T>>> k71Var, Throwable th, q70<? super te4> q70Var) {
            fs1.f(k71Var, "$this$create");
            fs1.f(th, "throwable");
            fs1.f(q70Var, "continuation");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(q70Var);
            anonymousClass1.L$0 = th;
            return anonymousClass1;
        }

        @Override // defpackage.kd1
        public final Object invoke(Object obj, Throwable th, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create((k71) obj, th, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                Throwable th = (Throwable) this.L$0;
                if (th instanceof ClosedSendChannelException) {
                    return te4.a;
                }
                throw th;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* compiled from: CachedPageEventFlow.kt */
    @a(c = "androidx.paging.CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$2", f = "CachedPageEventFlow.kt", l = {}, m = "invokeSuspend")
    /* renamed from: androidx.paging.CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super lq1<? extends yo2<T>>>, Throwable, q70<? super te4>, Object> {
        public int label;

        public AnonymousClass2(q70 q70Var) {
            super(3, q70Var);
        }

        public final q70<te4> create(k71<? super lq1<? extends yo2<T>>> k71Var, Throwable th, q70<? super te4> q70Var) {
            fs1.f(k71Var, "$this$create");
            fs1.f(q70Var, "continuation");
            return new AnonymousClass2(q70Var);
        }

        @Override // defpackage.kd1
        public final Object invoke(Object obj, Throwable th, q70<? super te4> q70Var) {
            return ((AnonymousClass2) create((k71) obj, th, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1.this.$snapshot.a();
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1(CachedPageEventFlow$downstreamFlow$1 cachedPageEventFlow$downstreamFlow$1, lp3 lp3Var, TemporaryDownstream temporaryDownstream, st1 st1Var, Ref$IntRef ref$IntRef, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = cachedPageEventFlow$downstreamFlow$1;
        this.$this_simpleChannelFlow = lp3Var;
        this.$snapshot = temporaryDownstream;
        this.$historyCollection = st1Var;
        this.$lastReceivedHistoryIndex = ref$IntRef;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1(this.this$0, this.$this_simpleChannelFlow, this.$snapshot, this.$historyCollection, this.$lastReceivedHistoryIndex, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Multicaster multicaster;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            multicaster = this.this$0.this$0.c;
            j71 s = n71.s(n71.c(multicaster.i(), new AnonymousClass1(null)), new AnonymousClass2(null));
            CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$invokeSuspend$$inlined$collect$1 cachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$invokeSuspend$$inlined$collect$1 = new CachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$invokeSuspend$$inlined$collect$1(this);
            this.label = 1;
            if (s.a(cachedPageEventFlow$downstreamFlow$1$activeStreamCollection$1$invokeSuspend$$inlined$collect$1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
