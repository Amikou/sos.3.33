package androidx.paging;

import kotlin.Pair;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ConflatedEventBus.kt */
/* loaded from: classes.dex */
public final class ConflatedEventBus<T> {
    public final jb2<Pair<Integer, T>> a;
    public final j71<T> b;

    public ConflatedEventBus(T t) {
        final jb2<Pair<Integer, T>> a = xs3.a(new Pair(Integer.MIN_VALUE, t));
        this.a = a;
        this.b = new j71<T>() { // from class: androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1

            /* compiled from: Collect.kt */
            /* renamed from: androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1$2  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass2 implements k71<Pair<? extends Integer, ? extends T>> {
                public final /* synthetic */ k71 a;

                @a(c = "androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1$2", f = "ConflatedEventBus.kt", l = {136}, m = "emit")
                /* renamed from: androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1$2$1  reason: invalid class name */
                /* loaded from: classes.dex */
                public static final class AnonymousClass1 extends ContinuationImpl {
                    public Object L$0;
                    public int label;
                    public /* synthetic */ Object result;

                    public AnonymousClass1(q70 q70Var) {
                        super(q70Var);
                    }

                    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                    public final Object invokeSuspend(Object obj) {
                        this.result = obj;
                        this.label |= Integer.MIN_VALUE;
                        return AnonymousClass2.this.emit(null, this);
                    }
                }

                public AnonymousClass2(k71 k71Var, ConflatedEventBus$$special$$inlined$mapNotNull$1 conflatedEventBus$$special$$inlined$mapNotNull$1) {
                    this.a = k71Var;
                }

                /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
                /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
                @Override // defpackage.k71
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public java.lang.Object emit(java.lang.Object r5, defpackage.q70 r6) {
                    /*
                        r4 = this;
                        boolean r0 = r6 instanceof androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1.AnonymousClass2.AnonymousClass1
                        if (r0 == 0) goto L13
                        r0 = r6
                        androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1$2$1 r0 = (androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1.AnonymousClass2.AnonymousClass1) r0
                        int r1 = r0.label
                        r2 = -2147483648(0xffffffff80000000, float:-0.0)
                        r3 = r1 & r2
                        if (r3 == 0) goto L13
                        int r1 = r1 - r2
                        r0.label = r1
                        goto L18
                    L13:
                        androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1$2$1 r0 = new androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1$2$1
                        r0.<init>(r6)
                    L18:
                        java.lang.Object r6 = r0.result
                        java.lang.Object r1 = defpackage.gs1.d()
                        int r2 = r0.label
                        r3 = 1
                        if (r2 == 0) goto L31
                        if (r2 != r3) goto L29
                        defpackage.o83.b(r6)
                        goto L47
                    L29:
                        java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                        java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
                        r5.<init>(r6)
                        throw r5
                    L31:
                        defpackage.o83.b(r6)
                        k71 r6 = r4.a
                        kotlin.Pair r5 = (kotlin.Pair) r5
                        java.lang.Object r5 = r5.getSecond()
                        if (r5 == 0) goto L4a
                        r0.label = r3
                        java.lang.Object r5 = r6.emit(r5, r0)
                        if (r5 != r1) goto L47
                        return r1
                    L47:
                        te4 r5 = defpackage.te4.a
                        goto L4c
                    L4a:
                        te4 r5 = defpackage.te4.a
                    L4c:
                        return r5
                    */
                    throw new UnsupportedOperationException("Method not decompiled: androidx.paging.ConflatedEventBus$$special$$inlined$mapNotNull$1.AnonymousClass2.emit(java.lang.Object, q70):java.lang.Object");
                }
            }

            @Override // defpackage.j71
            public Object a(k71 k71Var, q70 q70Var) {
                Object a2 = j71.this.a(new AnonymousClass2(k71Var, this), q70Var);
                return a2 == gs1.d() ? a2 : te4.a;
            }
        };
    }

    public final j71<T> a() {
        return this.b;
    }

    public final void b(T t) {
        fs1.f(t, "data");
        jb2<Pair<Integer, T>> jb2Var = this.a;
        jb2Var.setValue(new Pair<>(Integer.valueOf(jb2Var.getValue().getFirst().intValue() + 1), t));
    }

    public /* synthetic */ ConflatedEventBus(Object obj, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : obj);
    }
}
