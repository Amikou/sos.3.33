package androidx.paging;

import kotlin.jvm.internal.FunctionReferenceImpl;

/* compiled from: PageFetcher.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class PageFetcher$generateNewPagingSource$3 extends FunctionReferenceImpl implements rc1<te4> {
    public PageFetcher$generateNewPagingSource$3(PageFetcher pageFetcher) {
        super(0, pageFetcher, PageFetcher.class, "invalidate", "invalidate()V", 0);
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ((PageFetcher) this.receiver).k();
    }
}
