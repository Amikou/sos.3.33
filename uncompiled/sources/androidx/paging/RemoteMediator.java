package androidx.paging;

/* compiled from: RemoteMediator.kt */
/* loaded from: classes.dex */
public abstract class RemoteMediator<Key, Value> {

    /* compiled from: RemoteMediator.kt */
    /* loaded from: classes.dex */
    public enum InitializeAction {
        LAUNCH_INITIAL_REFRESH,
        SKIP_INITIAL_REFRESH
    }
}
