package androidx.paging;

/* compiled from: PageFetcher.kt */
/* loaded from: classes.dex */
public final class PageFetcher<Key, Value> {
    public final ConflatedEventBus<Boolean> a;
    public final ConflatedEventBus<te4> b;
    public final j71<fp2<Value>> c;
    public final tc1<q70<? super gp2<Key, Value>>, Object> d;
    public final Key e;
    public final ep2 f;
    public final RemoteMediator<Key, Value> g;

    /* compiled from: PageFetcher.kt */
    /* loaded from: classes.dex */
    public static final class a<Key, Value> {
        public final PageFetcherSnapshot<Key, Value> a;
        public final ip2<Key, Value> b;

        public a(PageFetcherSnapshot<Key, Value> pageFetcherSnapshot, ip2<Key, Value> ip2Var) {
            fs1.f(pageFetcherSnapshot, "snapshot");
            this.a = pageFetcherSnapshot;
            this.b = ip2Var;
        }

        public final PageFetcherSnapshot<Key, Value> a() {
            return this.a;
        }

        public final ip2<Key, Value> b() {
            return this.b;
        }
    }

    /* compiled from: PageFetcher.kt */
    /* loaded from: classes.dex */
    public final class b<Key, Value> implements le4 {
        public final PageFetcherSnapshot<Key, Value> a;
        public final /* synthetic */ PageFetcher b;

        public b(PageFetcher pageFetcher, PageFetcherSnapshot<Key, Value> pageFetcherSnapshot, ConflatedEventBus<te4> conflatedEventBus) {
            fs1.f(pageFetcherSnapshot, "pageFetcherSnapshot");
            fs1.f(conflatedEventBus, "retryEventBus");
            this.b = pageFetcher;
            this.a = pageFetcherSnapshot;
        }

        @Override // defpackage.le4
        public void a() {
            this.b.l();
        }

        @Override // defpackage.le4
        public void b(xk4 xk4Var) {
            fs1.f(xk4Var, "viewportHint");
            this.a.l(xk4Var);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public PageFetcher(tc1<? super q70<? super gp2<Key, Value>>, ? extends Object> tc1Var, Key key, ep2 ep2Var, RemoteMediator<Key, Value> remoteMediator) {
        fs1.f(tc1Var, "pagingSourceFactory");
        fs1.f(ep2Var, "config");
        this.d = tc1Var;
        this.e = key;
        this.f = ep2Var;
        this.a = new ConflatedEventBus<>(null, 1, null);
        this.b = new ConflatedEventBus<>(null, 1, null);
        this.c = SimpleChannelFlowKt.a(new PageFetcher$flow$1(this, null));
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0052  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x005f  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0062  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x007a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final /* synthetic */ java.lang.Object h(defpackage.gp2<Key, Value> r5, defpackage.q70<? super defpackage.gp2<Key, Value>> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof androidx.paging.PageFetcher$generateNewPagingSource$1
            if (r0 == 0) goto L13
            r0 = r6
            androidx.paging.PageFetcher$generateNewPagingSource$1 r0 = (androidx.paging.PageFetcher$generateNewPagingSource$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.PageFetcher$generateNewPagingSource$1 r0 = new androidx.paging.PageFetcher$generateNewPagingSource$1
            r0.<init>(r4, r6)
        L18:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L39
            if (r2 != r3) goto L31
            java.lang.Object r5 = r0.L$1
            gp2 r5 = (defpackage.gp2) r5
            java.lang.Object r0 = r0.L$0
            androidx.paging.PageFetcher r0 = (androidx.paging.PageFetcher) r0
            defpackage.o83.b(r6)
            goto L4c
        L31:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L39:
            defpackage.o83.b(r6)
            tc1<q70<? super gp2<Key, Value>>, java.lang.Object> r6 = r4.d
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r6 = r6.invoke(r0)
            if (r6 != r1) goto L4b
            return r1
        L4b:
            r0 = r4
        L4c:
            gp2 r6 = (defpackage.gp2) r6
            boolean r1 = r6 instanceof androidx.paging.LegacyPagingSource
            if (r1 == 0) goto L5c
            r1 = r6
            androidx.paging.LegacyPagingSource r1 = (androidx.paging.LegacyPagingSource) r1
            ep2 r2 = r0.f
            int r2 = r2.a
            r1.k(r2)
        L5c:
            if (r6 == r5) goto L5f
            goto L60
        L5f:
            r3 = 0
        L60:
            if (r3 == 0) goto L7a
            androidx.paging.PageFetcher$generateNewPagingSource$3 r1 = new androidx.paging.PageFetcher$generateNewPagingSource$3
            r1.<init>(r0)
            r6.g(r1)
            if (r5 == 0) goto L74
            androidx.paging.PageFetcher$generateNewPagingSource$4 r1 = new androidx.paging.PageFetcher$generateNewPagingSource$4
            r1.<init>(r0)
            r5.h(r1)
        L74:
            if (r5 == 0) goto L79
            r5.e()
        L79:
            return r6
        L7a:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "An instance of PagingSource was re-used when Pager expected to create a new\ninstance. Ensure that the pagingSourceFactory passed to Pager always returns a\nnew instance of PagingSource."
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcher.h(gp2, q70):java.lang.Object");
    }

    public final j71<fp2<Value>> i() {
        return this.c;
    }

    public final j71<yo2<Value>> j(PageFetcherSnapshot<Key, Value> pageFetcherSnapshot, o63<Key, Value> o63Var) {
        if (o63Var == null) {
            return pageFetcherSnapshot.r();
        }
        return SimpleChannelFlowKt.a(new PageFetcher$injectRemoteEvents$1(pageFetcherSnapshot, o63Var, null));
    }

    public final void k() {
        this.a.b(Boolean.FALSE);
    }

    public final void l() {
        this.a.b(Boolean.TRUE);
    }
}
