package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;

/* compiled from: FlowExt.kt */
@a(c = "androidx.paging.FlowExtKt$simpleRunningReduce$1", f = "FlowExt.kt", l = {102}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class FlowExtKt$simpleRunningReduce$1 extends SuspendLambda implements hd1<k71<? super T>, q70<? super te4>, Object> {
    public final /* synthetic */ kd1 $operation;
    public final /* synthetic */ j71 $this_simpleRunningReduce;
    private /* synthetic */ Object L$0;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FlowExtKt$simpleRunningReduce$1(j71 j71Var, kd1 kd1Var, q70 q70Var) {
        super(2, q70Var);
        this.$this_simpleRunningReduce = j71Var;
        this.$operation = kd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        FlowExtKt$simpleRunningReduce$1 flowExtKt$simpleRunningReduce$1 = new FlowExtKt$simpleRunningReduce$1(this.$this_simpleRunningReduce, this.$operation, q70Var);
        flowExtKt$simpleRunningReduce$1.L$0 = obj;
        return flowExtKt$simpleRunningReduce$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((FlowExtKt$simpleRunningReduce$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [T, java.lang.Object] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        ?? r3;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            r3 = FlowExtKt.a;
            ref$ObjectRef.element = r3;
            j71 j71Var = this.$this_simpleRunningReduce;
            FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1 flowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1 = new FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1(this, (k71) this.L$0, ref$ObjectRef);
            this.label = 1;
            if (j71Var.a(flowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
