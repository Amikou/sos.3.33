package androidx.paging;

import defpackage.fe0;
import defpackage.gp2;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;

/* compiled from: LegacyPagingSource.kt */
@a(c = "androidx.paging.LegacyPagingSource$load$2", f = "LegacyPagingSource.kt", l = {116}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class LegacyPagingSource$load$2 extends SuspendLambda implements hd1<c90, q70<? super gp2.b.C0179b<Key, Value>>, Object> {
    public final /* synthetic */ Ref$ObjectRef $dataSourceParams;
    public final /* synthetic */ gp2.a $params;
    public int label;
    public final /* synthetic */ LegacyPagingSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public LegacyPagingSource$load$2(LegacyPagingSource legacyPagingSource, Ref$ObjectRef ref$ObjectRef, gp2.a aVar, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = legacyPagingSource;
        this.$dataSourceParams = ref$ObjectRef;
        this.$params = aVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new LegacyPagingSource$load$2(this.this$0, this.$dataSourceParams, this.$params, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, Object obj) {
        return ((LegacyPagingSource$load$2) create(c90Var, (q70) obj)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        int i = this.label;
        Object obj2 = null;
        if (i == 0) {
            o83.b(obj);
            this.this$0.i();
            fe0.b bVar = (fe0.b) this.$dataSourceParams.element;
            this.label = 1;
            throw null;
        } else if (i == 1) {
            o83.b(obj);
            fe0.a aVar = (fe0.a) obj;
            List<Value> list = aVar.a;
            Object d = (list.isEmpty() && (this.$params instanceof gp2.a.c)) ? null : aVar.d();
            if (!aVar.a.isEmpty() || !(this.$params instanceof gp2.a.C0178a)) {
                obj2 = aVar.c();
            }
            return new gp2.b.C0179b(list, d, obj2, aVar.b(), aVar.a());
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
