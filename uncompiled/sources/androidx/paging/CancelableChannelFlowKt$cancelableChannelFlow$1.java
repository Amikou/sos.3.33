package androidx.paging;

import defpackage.uj3;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Lambda;

/* compiled from: CancelableChannelFlow.kt */
@a(c = "androidx.paging.CancelableChannelFlowKt$cancelableChannelFlow$1", f = "CancelableChannelFlow.kt", l = {30}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CancelableChannelFlowKt$cancelableChannelFlow$1 extends SuspendLambda implements hd1<lp3<T>, q70<? super te4>, Object> {
    public final /* synthetic */ hd1 $block;
    public final /* synthetic */ st1 $controller;
    private /* synthetic */ Object L$0;
    public int label;

    /* compiled from: CancelableChannelFlow.kt */
    /* renamed from: androidx.paging.CancelableChannelFlowKt$cancelableChannelFlow$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<Throwable, te4> {
        public final /* synthetic */ lp3 $this_simpleChannelFlow;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(lp3 lp3Var) {
            super(1);
            this.$this_simpleChannelFlow = lp3Var;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
            invoke2(th);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Throwable th) {
            uj3.a.a(this.$this_simpleChannelFlow, null, 1, null);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CancelableChannelFlowKt$cancelableChannelFlow$1(st1 st1Var, hd1 hd1Var, q70 q70Var) {
        super(2, q70Var);
        this.$controller = st1Var;
        this.$block = hd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        CancelableChannelFlowKt$cancelableChannelFlow$1 cancelableChannelFlowKt$cancelableChannelFlow$1 = new CancelableChannelFlowKt$cancelableChannelFlow$1(this.$controller, this.$block, q70Var);
        cancelableChannelFlowKt$cancelableChannelFlow$1.L$0 = obj;
        return cancelableChannelFlowKt$cancelableChannelFlow$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((CancelableChannelFlowKt$cancelableChannelFlow$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            lp3 lp3Var = (lp3) this.L$0;
            this.$controller.z(new AnonymousClass1(lp3Var));
            hd1 hd1Var = this.$block;
            this.label = 1;
            if (hd1Var.invoke(lp3Var, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
