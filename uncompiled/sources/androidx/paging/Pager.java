package androidx.paging;

/* compiled from: Pager.kt */
/* loaded from: classes.dex */
public final class Pager<Key, Value> {
    public final j71<fp2<Value>> a;

    public Pager(ep2 ep2Var, Key key, RemoteMediator<Key, Value> remoteMediator, rc1<? extends gp2<Key, Value>> rc1Var) {
        tc1 pager$flow$2;
        fs1.f(ep2Var, "config");
        fs1.f(rc1Var, "pagingSourceFactory");
        if (rc1Var instanceof SuspendingPagingSourceFactory) {
            pager$flow$2 = new Pager$flow$1(rc1Var);
        } else {
            pager$flow$2 = new Pager$flow$2(rc1Var, null);
        }
        this.a = new PageFetcher(pager$flow$2, key, ep2Var, remoteMediator).i();
    }

    public final j71<fp2<Value>> a() {
        return this.a;
    }

    public /* synthetic */ Pager(ep2 ep2Var, Object obj, rc1 rc1Var, int i, qi0 qi0Var) {
        this(ep2Var, (i & 2) != 0 ? null : obj, rc1Var);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public Pager(ep2 ep2Var, Key key, rc1<? extends gp2<Key, Value>> rc1Var) {
        this(ep2Var, key, null, rc1Var);
        fs1.f(ep2Var, "config");
        fs1.f(rc1Var, "pagingSourceFactory");
    }
}
