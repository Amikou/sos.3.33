package androidx.paging;

import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.a;

/* compiled from: Collect.kt */
/* loaded from: classes.dex */
public final class PagingDataDiffer$collectFrom$2$invokeSuspend$$inlined$collect$1 implements k71<yo2<T>> {
    public final /* synthetic */ PagingDataDiffer$collectFrom$2 a;

    public PagingDataDiffer$collectFrom$2$invokeSuspend$$inlined$collect$1(PagingDataDiffer$collectFrom$2 pagingDataDiffer$collectFrom$2) {
        this.a = pagingDataDiffer$collectFrom$2;
    }

    @Override // defpackage.k71
    public Object emit(Object obj, q70 q70Var) {
        CoroutineDispatcher coroutineDispatcher;
        coroutineDispatcher = this.a.this$0.k;
        Object e = a.e(coroutineDispatcher, new PagingDataDiffer$collectFrom$2$invokeSuspend$$inlined$collect$1$lambda$1((yo2) obj, null, this), q70Var);
        return e == gs1.d() ? e : te4.a;
    }
}
