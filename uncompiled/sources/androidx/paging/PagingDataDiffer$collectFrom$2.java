package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PagingDataDiffer.kt */
@a(c = "androidx.paging.PagingDataDiffer$collectFrom$2", f = "PagingDataDiffer.kt", l = {390}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PagingDataDiffer$collectFrom$2 extends SuspendLambda implements tc1<q70<? super te4>, Object> {
    public final /* synthetic */ fp2 $pagingData;
    public int label;
    public final /* synthetic */ PagingDataDiffer this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PagingDataDiffer$collectFrom$2(PagingDataDiffer pagingDataDiffer, fp2 fp2Var, q70 q70Var) {
        super(1, q70Var);
        this.this$0 = pagingDataDiffer;
        this.$pagingData = fp2Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new PagingDataDiffer$collectFrom$2(this.this$0, this.$pagingData, q70Var);
    }

    @Override // defpackage.tc1
    public final Object invoke(q70<? super te4> q70Var) {
        return ((PagingDataDiffer$collectFrom$2) create(q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            this.this$0.b = this.$pagingData.b();
            j71 a = this.$pagingData.a();
            PagingDataDiffer$collectFrom$2$invokeSuspend$$inlined$collect$1 pagingDataDiffer$collectFrom$2$invokeSuspend$$inlined$collect$1 = new PagingDataDiffer$collectFrom$2$invokeSuspend$$inlined$collect$1(this);
            this.label = 1;
            if (a.a(pagingDataDiffer$collectFrom$2$invokeSuspend$$inlined$collect$1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
