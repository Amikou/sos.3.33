package androidx.paging;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPageEventFlow.kt */
@a(c = "androidx.paging.FlattenedPageController", f = "CachedPageEventFlow.kt", l = {310, 188}, m = "createTemporaryDownstream")
/* loaded from: classes.dex */
public final class FlattenedPageController$createTemporaryDownstream$1 extends ContinuationImpl {
    public int I$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ FlattenedPageController this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FlattenedPageController$createTemporaryDownstream$1(FlattenedPageController flattenedPageController, q70 q70Var) {
        super(q70Var);
        this.this$0 = flattenedPageController;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a(this);
    }
}
