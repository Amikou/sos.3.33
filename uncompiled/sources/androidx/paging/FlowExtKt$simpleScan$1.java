package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;

/* compiled from: FlowExt.kt */
@a(c = "androidx.paging.FlowExtKt$simpleScan$1", f = "FlowExt.kt", l = {42, 102}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class FlowExtKt$simpleScan$1 extends SuspendLambda implements hd1<k71<? super R>, q70<? super te4>, Object> {
    public final /* synthetic */ Object $initial;
    public final /* synthetic */ kd1 $operation;
    public final /* synthetic */ j71 $this_simpleScan;
    private /* synthetic */ Object L$0;
    public Object L$1;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FlowExtKt$simpleScan$1(j71 j71Var, Object obj, kd1 kd1Var, q70 q70Var) {
        super(2, q70Var);
        this.$this_simpleScan = j71Var;
        this.$initial = obj;
        this.$operation = kd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        FlowExtKt$simpleScan$1 flowExtKt$simpleScan$1 = new FlowExtKt$simpleScan$1(this.$this_simpleScan, this.$initial, this.$operation, q70Var);
        flowExtKt$simpleScan$1.L$0 = obj;
        return flowExtKt$simpleScan$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((FlowExtKt$simpleScan$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [T, java.lang.Object] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Ref$ObjectRef ref$ObjectRef;
        k71 k71Var;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            k71 k71Var2 = (k71) this.L$0;
            ref$ObjectRef = new Ref$ObjectRef();
            ?? r4 = this.$initial;
            ref$ObjectRef.element = r4;
            this.L$0 = k71Var2;
            this.L$1 = ref$ObjectRef;
            this.label = 1;
            if (k71Var2.emit(r4, this) == d) {
                return d;
            }
            k71Var = k71Var2;
        } else if (i != 1) {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        } else {
            ref$ObjectRef = (Ref$ObjectRef) this.L$1;
            k71Var = (k71) this.L$0;
            o83.b(obj);
        }
        j71 j71Var = this.$this_simpleScan;
        FlowExtKt$simpleScan$1$invokeSuspend$$inlined$collect$1 flowExtKt$simpleScan$1$invokeSuspend$$inlined$collect$1 = new FlowExtKt$simpleScan$1$invokeSuspend$$inlined$collect$1(this, k71Var, ref$ObjectRef);
        this.L$0 = null;
        this.L$1 = null;
        this.label = 2;
        if (j71Var.a(flowExtKt$simpleScan$1$invokeSuspend$$inlined$collect$1, this) == d) {
            return d;
        }
        return te4.a;
    }
}
