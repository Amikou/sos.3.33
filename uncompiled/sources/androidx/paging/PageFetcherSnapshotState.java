package androidx.paging;

import defpackage.gp2;
import defpackage.w02;
import defpackage.xk4;
import defpackage.yo2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.NoWhenBranchMatchedException;

/* compiled from: PageFetcherSnapshotState.kt */
/* loaded from: classes.dex */
public final class PageFetcherSnapshotState<Key, Value> {
    public final List<gp2.b.C0179b<Key, Value>> a;
    public final List<gp2.b.C0179b<Key, Value>> b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public final kx<Integer> h;
    public final kx<Integer> i;
    public final Map<LoadType, xk4> j;
    public x02 k;
    public final ep2 l;

    /* compiled from: PageFetcherSnapshotState.kt */
    /* loaded from: classes.dex */
    public static final class a<Key, Value> {
        public final kb2 a;
        public final PageFetcherSnapshotState<Key, Value> b;
        public final ep2 c;

        public a(ep2 ep2Var) {
            fs1.f(ep2Var, "config");
            this.c = ep2Var;
            this.a = lb2.b(false, 1, null);
            this.b = new PageFetcherSnapshotState<>(ep2Var, null);
        }

        public static final /* synthetic */ kb2 a(a aVar) {
            return aVar.a;
        }

        public static final /* synthetic */ PageFetcherSnapshotState b(a aVar) {
            return aVar.b;
        }
    }

    public PageFetcherSnapshotState(ep2 ep2Var) {
        this.l = ep2Var;
        ArrayList arrayList = new ArrayList();
        this.a = arrayList;
        this.b = arrayList;
        this.h = rx.b(-1, null, null, 6, null);
        this.i = rx.b(-1, null, null, 6, null);
        this.j = new LinkedHashMap();
        this.k = x02.e.a();
    }

    public final j71<Integer> e() {
        return n71.u(n71.h(this.i), new PageFetcherSnapshotState$consumeAppendGenerationIdAsFlow$1(this, null));
    }

    public final j71<Integer> f() {
        return n71.u(n71.h(this.h), new PageFetcherSnapshotState$consumePrependGenerationIdAsFlow$1(this, null));
    }

    public final ip2<Key, Value> g(xk4.a aVar) {
        Integer num;
        int size;
        List k0 = j20.k0(this.b);
        if (aVar != null) {
            int o = o();
            int i = -this.c;
            int i2 = b20.i(this.b) - this.c;
            int f = aVar.f();
            for (int i3 = i; i3 < f; i3++) {
                if (i3 > i2) {
                    size = this.l.a;
                } else {
                    size = this.b.get(this.c + i3).a().size();
                }
                o += size;
            }
            int e = o + aVar.e();
            if (aVar.f() < i) {
                e -= this.l.a;
            }
            num = Integer.valueOf(e);
        } else {
            num = null;
        }
        return new ip2<>(k0, num, this.l, o());
    }

    public final void h(yo2.a<Value> aVar) {
        fs1.f(aVar, "event");
        if (aVar.d() <= this.b.size()) {
            this.j.remove(aVar.a());
            this.k = this.k.h(aVar.a(), w02.c.d.b());
            int i = bp2.e[aVar.a().ordinal()];
            if (i == 1) {
                int d = aVar.d();
                for (int i2 = 0; i2 < d; i2++) {
                    this.a.remove(0);
                }
                this.c -= aVar.d();
                t(aVar.e());
                int i3 = this.f + 1;
                this.f = i3;
                this.h.offer(Integer.valueOf(i3));
                return;
            } else if (i == 2) {
                int d2 = aVar.d();
                for (int i4 = 0; i4 < d2; i4++) {
                    this.a.remove(this.b.size() - 1);
                }
                s(aVar.e());
                int i5 = this.g + 1;
                this.g = i5;
                this.i.offer(Integer.valueOf(i5));
                return;
            } else {
                throw new IllegalArgumentException("cannot drop " + aVar.a());
            }
        }
        throw new IllegalStateException(("invalid drop count. have " + this.b.size() + " but wanted to drop " + aVar.d()).toString());
    }

    public final yo2.a<Value> i(LoadType loadType, xk4 xk4Var) {
        int i;
        int i2;
        int i3;
        int size;
        int d;
        fs1.f(loadType, "loadType");
        fs1.f(xk4Var, "hint");
        yo2.a<Value> aVar = null;
        if (this.l.e != Integer.MAX_VALUE && this.b.size() > 2 && q() > this.l.e) {
            int i4 = 0;
            if (loadType != LoadType.REFRESH) {
                int i5 = 0;
                int i6 = 0;
                while (i5 < this.b.size() && q() - i6 > this.l.e) {
                    if (bp2.f[loadType.ordinal()] != 1) {
                        List<gp2.b.C0179b<Key, Value>> list = this.b;
                        size = list.get(b20.i(list) - i5).a().size();
                    } else {
                        size = this.b.get(i5).a().size();
                    }
                    if (bp2.g[loadType.ordinal()] != 1) {
                        d = xk4Var.c();
                    } else {
                        d = xk4Var.d();
                    }
                    if ((d - i6) - size < this.l.b) {
                        break;
                    }
                    i6 += size;
                    i5++;
                }
                if (i5 != 0) {
                    if (bp2.h[loadType.ordinal()] != 1) {
                        i = (b20.i(this.b) - this.c) - (i5 - 1);
                    } else {
                        i = -this.c;
                    }
                    if (bp2.i[loadType.ordinal()] != 1) {
                        i2 = b20.i(this.b);
                        i3 = this.c;
                    } else {
                        i2 = i5 - 1;
                        i3 = this.c;
                    }
                    int i7 = i2 - i3;
                    if (this.l.c) {
                        i4 = (loadType == LoadType.PREPEND ? o() : n()) + i6;
                    }
                    aVar = new yo2.a<>(loadType, i, i7, i4);
                }
                return aVar;
            }
            throw new IllegalArgumentException(("Drop LoadType must be PREPEND or APPEND, but got " + loadType).toString());
        }
        return null;
    }

    public final int j(LoadType loadType) {
        fs1.f(loadType, "loadType");
        int i = bp2.a[loadType.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return this.g;
                }
                throw new NoWhenBranchMatchedException();
            }
            return this.f;
        }
        throw new IllegalArgumentException("Cannot get loadId for loadType: REFRESH");
    }

    public final Map<LoadType, xk4> k() {
        return this.j;
    }

    public final int l() {
        return this.c;
    }

    public final List<gp2.b.C0179b<Key, Value>> m() {
        return this.b;
    }

    public final int n() {
        if (this.l.c) {
            return this.e;
        }
        return 0;
    }

    public final int o() {
        if (this.l.c) {
            return this.d;
        }
        return 0;
    }

    public final x02 p() {
        return this.k;
    }

    public final int q() {
        Iterator<T> it = this.b.iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((gp2.b.C0179b) it.next()).a().size();
        }
        return i;
    }

    public final boolean r(int i, LoadType loadType, gp2.b.C0179b<Key, Value> c0179b) {
        int c;
        int b;
        fs1.f(loadType, "loadType");
        fs1.f(c0179b, "page");
        int i2 = bp2.d[loadType.ordinal()];
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 == 3) {
                    if (!this.b.isEmpty()) {
                        if (i != this.g) {
                            return false;
                        }
                        this.a.add(c0179b);
                        if (c0179b.b() == Integer.MIN_VALUE) {
                            b = u33.b(n() - c0179b.a().size(), 0);
                        } else {
                            b = c0179b.b();
                        }
                        s(b);
                        this.j.remove(LoadType.APPEND);
                    } else {
                        throw new IllegalStateException("should've received an init before append".toString());
                    }
                }
            } else if (!this.b.isEmpty()) {
                if (i != this.f) {
                    return false;
                }
                this.a.add(0, c0179b);
                this.c++;
                if (c0179b.c() == Integer.MIN_VALUE) {
                    c = u33.b(o() - c0179b.a().size(), 0);
                } else {
                    c = c0179b.c();
                }
                t(c);
                this.j.remove(LoadType.PREPEND);
            } else {
                throw new IllegalStateException("should've received an init before prepend".toString());
            }
        } else if (!this.b.isEmpty()) {
            throw new IllegalStateException("cannot receive multiple init calls".toString());
        } else {
            if (i == 0) {
                this.a.add(c0179b);
                this.c = 0;
                s(c0179b.b());
                t(c0179b.c());
            } else {
                throw new IllegalStateException("init loadId must be the initial value, 0".toString());
            }
        }
        return true;
    }

    public final void s(int i) {
        if (i == Integer.MIN_VALUE) {
            i = 0;
        }
        this.e = i;
    }

    public final void t(int i) {
        if (i == Integer.MIN_VALUE) {
            i = 0;
        }
        this.d = i;
    }

    public final boolean u(LoadType loadType, w02 w02Var) {
        fs1.f(loadType, "type");
        fs1.f(w02Var, "newState");
        if (fs1.b(this.k.d(loadType), w02Var)) {
            return false;
        }
        this.k = this.k.h(loadType, w02Var);
        return true;
    }

    public final yo2<Value> v(gp2.b.C0179b<Key, Value> c0179b, LoadType loadType) {
        fs1.f(c0179b, "$this$toPageEvent");
        fs1.f(loadType, "loadType");
        int i = bp2.b[loadType.ordinal()];
        int i2 = 0;
        if (i != 1) {
            if (i == 2) {
                i2 = 0 - this.c;
            } else if (i != 3) {
                throw new NoWhenBranchMatchedException();
            } else {
                i2 = (this.b.size() - this.c) - 1;
            }
        }
        List b = a20.b(new xa4(i2, c0179b.a()));
        int i3 = bp2.c[loadType.ordinal()];
        if (i3 != 1) {
            if (i3 != 2) {
                if (i3 == 3) {
                    return yo2.b.g.a(b, n(), new a30(this.k.g(), this.k.f(), this.k.e(), this.k, null));
                }
                throw new NoWhenBranchMatchedException();
            }
            return yo2.b.g.b(b, o(), new a30(this.k.g(), this.k.f(), this.k.e(), this.k, null));
        }
        return yo2.b.g.c(b, o(), n(), new a30(this.k.g(), this.k.f(), this.k.e(), this.k, null));
    }

    public /* synthetic */ PageFetcherSnapshotState(ep2 ep2Var, qi0 qi0Var) {
        this(ep2Var);
    }
}
