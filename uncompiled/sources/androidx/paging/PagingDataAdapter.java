package androidx.paging;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.a0;
import androidx.recyclerview.widget.g;
import defpackage.w02;
import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: PagingDataAdapter.kt */
/* loaded from: classes.dex */
public abstract class PagingDataAdapter<T, VH extends RecyclerView.a0> extends RecyclerView.Adapter<VH> {
    public boolean a;
    public final AsyncPagingDataDiffer<T> b;
    public final j71<a30> c;

    /* compiled from: PagingDataAdapter.kt */
    /* renamed from: androidx.paging.PagingDataAdapter$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public AnonymousClass1() {
            super(0);
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            if (PagingDataAdapter.this.getStateRestorationPolicy() != RecyclerView.Adapter.StateRestorationPolicy.PREVENT || PagingDataAdapter.this.a) {
                return;
            }
            PagingDataAdapter.this.setStateRestorationPolicy(RecyclerView.Adapter.StateRestorationPolicy.ALLOW);
        }
    }

    /* compiled from: PagingDataAdapter.kt */
    /* loaded from: classes.dex */
    public static final class a extends RecyclerView.i {
        public final /* synthetic */ AnonymousClass1 b;

        public a(AnonymousClass1 anonymousClass1) {
            this.b = anonymousClass1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onItemRangeInserted(int i, int i2) {
            this.b.invoke2();
            PagingDataAdapter.this.unregisterAdapterDataObserver(this);
            super.onItemRangeInserted(i, i2);
        }
    }

    /* compiled from: PagingDataAdapter.kt */
    /* loaded from: classes.dex */
    public static final class b implements tc1<a30, te4> {
        public boolean a = true;
        public final /* synthetic */ AnonymousClass1 g0;

        public b(AnonymousClass1 anonymousClass1) {
            this.g0 = anonymousClass1;
        }

        public void a(a30 a30Var) {
            fs1.f(a30Var, "loadStates");
            if (this.a) {
                this.a = false;
            } else if (a30Var.f().g() instanceof w02.c) {
                this.g0.invoke2();
                PagingDataAdapter.this.d(this);
            }
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(a30 a30Var) {
            a(a30Var);
            return te4.a;
        }
    }

    public /* synthetic */ PagingDataAdapter(g.f fVar, CoroutineDispatcher coroutineDispatcher, CoroutineDispatcher coroutineDispatcher2, int i, qi0 qi0Var) {
        this(fVar, (i & 2) != 0 ? tp0.c() : coroutineDispatcher, (i & 4) != 0 ? tp0.a() : coroutineDispatcher2);
    }

    public final void b(tc1<? super a30, te4> tc1Var) {
        fs1.f(tc1Var, "listener");
        this.b.d(tc1Var);
    }

    public final void c() {
        this.b.j();
    }

    public final void d(tc1<? super a30, te4> tc1Var) {
        fs1.f(tc1Var, "listener");
        this.b.k(tc1Var);
    }

    public final Object e(fp2<T> fp2Var, q70<? super te4> q70Var) {
        Object l = this.b.l(fp2Var, q70Var);
        return l == gs1.d() ? l : te4.a;
    }

    public final T getItem(int i) {
        return this.b.g(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.b.h();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public final long getItemId(int i) {
        return super.getItemId(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public final void setHasStableIds(boolean z) {
        throw new UnsupportedOperationException("Stable ids are unsupported on PagingDataAdapter.");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void setStateRestorationPolicy(RecyclerView.Adapter.StateRestorationPolicy stateRestorationPolicy) {
        fs1.f(stateRestorationPolicy, "strategy");
        this.a = true;
        super.setStateRestorationPolicy(stateRestorationPolicy);
    }

    public PagingDataAdapter(g.f<T> fVar, CoroutineDispatcher coroutineDispatcher, CoroutineDispatcher coroutineDispatcher2) {
        fs1.f(fVar, "diffCallback");
        fs1.f(coroutineDispatcher, "mainDispatcher");
        fs1.f(coroutineDispatcher2, "workerDispatcher");
        AsyncPagingDataDiffer<T> asyncPagingDataDiffer = new AsyncPagingDataDiffer<>(fVar, new androidx.recyclerview.widget.b(this), coroutineDispatcher, coroutineDispatcher2);
        this.b = asyncPagingDataDiffer;
        super.setStateRestorationPolicy(RecyclerView.Adapter.StateRestorationPolicy.PREVENT);
        AnonymousClass1 anonymousClass1 = new AnonymousClass1();
        registerAdapterDataObserver(new a(anonymousClass1));
        b(new b(anonymousClass1));
        this.c = asyncPagingDataDiffer.i();
    }
}
