package androidx.paging;

/* compiled from: CancelableChannelFlow.kt */
/* loaded from: classes.dex */
public final class CancelableChannelFlowKt {
    public static final <T> j71<T> a(st1 st1Var, hd1<? super lp3<T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        fs1.f(st1Var, "controller");
        fs1.f(hd1Var, "block");
        return SimpleChannelFlowKt.a(new CancelableChannelFlowKt$cancelableChannelFlow$1(st1Var, hd1Var, null));
    }
}
