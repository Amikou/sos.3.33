package androidx.paging;

import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: AsyncPagingDataDiffer.kt */
/* loaded from: classes.dex */
public final class AsyncPagingDataDiffer$differBase$1 extends PagingDataDiffer<T> {
    public final /* synthetic */ AsyncPagingDataDiffer l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AsyncPagingDataDiffer$differBase$1(AsyncPagingDataDiffer asyncPagingDataDiffer, po0 po0Var, CoroutineDispatcher coroutineDispatcher) {
        super(po0Var, coroutineDispatcher);
        this.l = asyncPagingDataDiffer;
    }

    @Override // androidx.paging.PagingDataDiffer
    public boolean v() {
        return this.l.f();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0046  */
    @Override // androidx.paging.PagingDataDiffer
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object w(defpackage.vi2<T> r5, defpackage.vi2<T> r6, defpackage.a30 r7, int r8, defpackage.rc1<defpackage.te4> r9, defpackage.q70<? super java.lang.Integer> r10) {
        /*
            r4 = this;
            boolean r7 = r10 instanceof androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$1
            if (r7 == 0) goto L13
            r7 = r10
            androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$1 r7 = (androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$1) r7
            int r0 = r7.label
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = r0 & r1
            if (r2 == 0) goto L13
            int r0 = r0 - r1
            r7.label = r0
            goto L18
        L13:
            androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$1 r7 = new androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$1
            r7.<init>(r4, r10)
        L18:
            java.lang.Object r10 = r7.result
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r7.label
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L46
            if (r1 != r2) goto L3e
            int r8 = r7.I$0
            java.lang.Object r5 = r7.L$3
            r9 = r5
            rc1 r9 = (defpackage.rc1) r9
            java.lang.Object r5 = r7.L$2
            r6 = r5
            vi2 r6 = (defpackage.vi2) r6
            java.lang.Object r5 = r7.L$1
            vi2 r5 = (defpackage.vi2) r5
            java.lang.Object r7 = r7.L$0
            androidx.paging.AsyncPagingDataDiffer$differBase$1 r7 = (androidx.paging.AsyncPagingDataDiffer$differBase$1) r7
            defpackage.o83.b(r10)
            goto L97
        L3e:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L46:
            defpackage.o83.b(r10)
            int r10 = r5.a()
            r1 = 0
            if (r10 != 0) goto L61
            r9.invoke()
            androidx.paging.AsyncPagingDataDiffer r5 = r4.l
            po0 r5 = r5.e()
            int r6 = r6.a()
            r5.onInserted(r1, r6)
            goto Lad
        L61:
            int r10 = r6.a()
            if (r10 != 0) goto L78
            r9.invoke()
            androidx.paging.AsyncPagingDataDiffer r6 = r4.l
            po0 r6 = r6.e()
            int r5 = r5.a()
            r6.onRemoved(r1, r5)
            goto Lad
        L78:
            androidx.paging.AsyncPagingDataDiffer r10 = r4.l
            kotlinx.coroutines.CoroutineDispatcher r10 = androidx.paging.AsyncPagingDataDiffer.c(r10)
            androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$diffResult$1 r1 = new androidx.paging.AsyncPagingDataDiffer$differBase$1$presentNewList$diffResult$1
            r1.<init>(r4, r5, r6, r3)
            r7.L$0 = r4
            r7.L$1 = r5
            r7.L$2 = r6
            r7.L$3 = r9
            r7.I$0 = r8
            r7.label = r2
            java.lang.Object r10 = kotlinx.coroutines.a.e(r10, r1, r7)
            if (r10 != r0) goto L96
            return r0
        L96:
            r7 = r4
        L97:
            ui2 r10 = (defpackage.ui2) r10
            r9.invoke()
            androidx.paging.AsyncPagingDataDiffer r7 = r7.l
            i02 r7 = androidx.paging.AsyncPagingDataDiffer.b(r7)
            defpackage.wi2.b(r5, r7, r6, r10)
            int r5 = defpackage.wi2.c(r5, r10, r6, r8)
            java.lang.Integer r3 = defpackage.hr.d(r5)
        Lad:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.AsyncPagingDataDiffer$differBase$1.w(vi2, vi2, a30, int, rc1, q70):java.lang.Object");
    }
}
