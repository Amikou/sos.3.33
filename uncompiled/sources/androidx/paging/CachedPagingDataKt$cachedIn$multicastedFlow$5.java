package androidx.paging;

import androidx.paging.ActiveFlowTracker;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPagingData.kt */
@a(c = "androidx.paging.CachedPagingDataKt$cachedIn$multicastedFlow$5", f = "CachedPagingData.kt", l = {100}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CachedPagingDataKt$cachedIn$multicastedFlow$5 extends SuspendLambda implements kd1<k71<? super fp2<T>>, Throwable, q70<? super te4>, Object> {
    public final /* synthetic */ ActiveFlowTracker $tracker;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CachedPagingDataKt$cachedIn$multicastedFlow$5(ActiveFlowTracker activeFlowTracker, q70 q70Var) {
        super(3, q70Var);
        this.$tracker = activeFlowTracker;
    }

    public final q70<te4> create(k71<? super fp2<T>> k71Var, Throwable th, q70<? super te4> q70Var) {
        fs1.f(k71Var, "$this$create");
        fs1.f(q70Var, "continuation");
        return new CachedPagingDataKt$cachedIn$multicastedFlow$5(this.$tracker, q70Var);
    }

    @Override // defpackage.kd1
    public final Object invoke(Object obj, Throwable th, q70<? super te4> q70Var) {
        return ((CachedPagingDataKt$cachedIn$multicastedFlow$5) create((k71) obj, th, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            ActiveFlowTracker activeFlowTracker = this.$tracker;
            if (activeFlowTracker != null) {
                ActiveFlowTracker.FlowType flowType = ActiveFlowTracker.FlowType.PAGED_DATA_FLOW;
                this.label = 1;
                if (activeFlowTracker.b(flowType, this) == d) {
                    return d;
                }
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
