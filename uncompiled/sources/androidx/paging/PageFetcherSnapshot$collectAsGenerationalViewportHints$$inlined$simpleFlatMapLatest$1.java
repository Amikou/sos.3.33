package androidx.paging;

import androidx.paging.PageFetcherSnapshotState;
import defpackage.w02;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: FlowExt.kt */
@a(c = "androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1", f = "PageFetcherSnapshot.kt", l = {109, 130}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1 extends SuspendLambda implements kd1<k71<? super ve1>, Integer, q70<? super te4>, Object> {
    public final /* synthetic */ LoadType $loadType$inlined;
    public int I$0;
    private /* synthetic */ Object L$0;
    private /* synthetic */ Object L$1;
    public Object L$2;
    public int label;
    public final /* synthetic */ PageFetcherSnapshot this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1(q70 q70Var, PageFetcherSnapshot pageFetcherSnapshot, LoadType loadType) {
        super(3, q70Var);
        this.this$0 = pageFetcherSnapshot;
        this.$loadType$inlined = loadType;
    }

    public final q70<te4> create(k71<? super ve1> k71Var, Integer num, q70<? super te4> q70Var) {
        fs1.f(k71Var, "$this$create");
        fs1.f(q70Var, "continuation");
        PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1 pageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1 = new PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1(q70Var, this.this$0, this.$loadType$inlined);
        pageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1.L$0 = k71Var;
        pageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1.L$1 = num;
        return pageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1;
    }

    @Override // defpackage.kd1
    public final Object invoke(k71<? super ve1> k71Var, Integer num, q70<? super te4> q70Var) {
        return ((PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1) create(k71Var, num, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        k71<? super ve1> k71Var;
        final int intValue;
        PageFetcherSnapshotState.a aVar;
        kb2 kb2Var;
        PageFetcherSnapshotState pageFetcherSnapshotState;
        ib2 ib2Var;
        j71<ve1> j71Var;
        Object d = gs1.d();
        int i = this.label;
        try {
            if (i == 0) {
                o83.b(obj);
                k71Var = (k71) this.L$0;
                intValue = ((Number) this.L$1).intValue();
                aVar = this.this$0.e;
                kb2Var = aVar.a;
                this.L$0 = k71Var;
                this.L$1 = aVar;
                this.L$2 = kb2Var;
                this.I$0 = intValue;
                this.label = 1;
                if (kb2Var.b(null, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                if (i != 2) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                o83.b(obj);
                return te4.a;
            } else {
                intValue = this.I$0;
                kb2Var = (kb2) this.L$2;
                aVar = (PageFetcherSnapshotState.a) this.L$1;
                k71Var = (k71) this.L$0;
                o83.b(obj);
            }
            pageFetcherSnapshotState = aVar.b;
            w02 d2 = pageFetcherSnapshotState.p().d(this.$loadType$inlined);
            w02.c.a aVar2 = w02.c.d;
            if (fs1.b(d2, aVar2.a())) {
                j71Var = n71.p(new ve1[0]);
            } else {
                if (!(pageFetcherSnapshotState.p().d(this.$loadType$inlined) instanceof w02.a)) {
                    pageFetcherSnapshotState.u(this.$loadType$inlined, aVar2.b());
                }
                te4 te4Var = te4.a;
                kb2Var.a(null);
                ib2Var = this.this$0.a;
                final j71 j = n71.j(ib2Var, intValue == 0 ? 0 : 1);
                j71Var = new j71<ve1>() { // from class: androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1

                    /* compiled from: Collect.kt */
                    /* renamed from: androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1$1  reason: invalid class name */
                    /* loaded from: classes.dex */
                    public static final class AnonymousClass1 implements k71<xk4> {
                        public final /* synthetic */ k71 a;
                        public final /* synthetic */ PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1 f0;

                        /* renamed from: androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1$1$1  reason: invalid class name and collision with other inner class name */
                        /* loaded from: classes.dex */
                        public static final class C00521 extends ContinuationImpl {
                            public Object L$0;
                            public int label;
                            public /* synthetic */ Object result;

                            public C00521(q70 q70Var) {
                                super(q70Var);
                            }

                            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                            public final Object invokeSuspend(Object obj) {
                                this.result = obj;
                                this.label |= Integer.MIN_VALUE;
                                return AnonymousClass1.this.emit(null, this);
                            }
                        }

                        public AnonymousClass1(k71 k71Var, PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1 pageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1) {
                            this.a = k71Var;
                            this.f0 = pageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1;
                        }

                        /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
                        /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
                        @Override // defpackage.k71
                        /*
                            Code decompiled incorrectly, please refer to instructions dump.
                            To view partially-correct code enable 'Show inconsistent code' option in preferences
                        */
                        public java.lang.Object emit(defpackage.xk4 r6, defpackage.q70 r7) {
                            /*
                                r5 = this;
                                boolean r0 = r7 instanceof androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1.AnonymousClass1.C00521
                                if (r0 == 0) goto L13
                                r0 = r7
                                androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1$1$1 r0 = (androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1.AnonymousClass1.C00521) r0
                                int r1 = r0.label
                                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                                r3 = r1 & r2
                                if (r3 == 0) goto L13
                                int r1 = r1 - r2
                                r0.label = r1
                                goto L18
                            L13:
                                androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1$1$1 r0 = new androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1$1$1
                                r0.<init>(r7)
                            L18:
                                java.lang.Object r7 = r0.result
                                java.lang.Object r1 = defpackage.gs1.d()
                                int r2 = r0.label
                                r3 = 1
                                if (r2 == 0) goto L31
                                if (r2 != r3) goto L29
                                defpackage.o83.b(r7)
                                goto L4a
                            L29:
                                java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
                                java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
                                r6.<init>(r7)
                                throw r6
                            L31:
                                defpackage.o83.b(r7)
                                k71 r7 = r5.a
                                xk4 r6 = (defpackage.xk4) r6
                                ve1 r2 = new ve1
                                androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1 r4 = r5.f0
                                int r4 = r2
                                r2.<init>(r4, r6)
                                r0.label = r3
                                java.lang.Object r6 = r7.emit(r2, r0)
                                if (r6 != r1) goto L4a
                                return r1
                            L4a:
                                te4 r6 = defpackage.te4.a
                                return r6
                            */
                            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcherSnapshot$collectAsGenerationalViewportHints$$inlined$simpleFlatMapLatest$1$lambda$1.AnonymousClass1.emit(java.lang.Object, q70):java.lang.Object");
                        }
                    }

                    @Override // defpackage.j71
                    public Object a(k71<? super ve1> k71Var2, q70 q70Var) {
                        Object a = j71.this.a(new AnonymousClass1(k71Var2, this), q70Var);
                        return a == gs1.d() ? a : te4.a;
                    }
                };
            }
            this.L$0 = null;
            this.L$1 = null;
            this.L$2 = null;
            this.label = 2;
            if (j71Var.a(k71Var, this) == d) {
                return d;
            }
            return te4.a;
        } finally {
            kb2Var.a(null);
        }
    }
}
