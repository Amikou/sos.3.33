package androidx.paging;

/* compiled from: CachedPagingData.kt */
/* loaded from: classes.dex */
public interface ActiveFlowTracker {

    /* compiled from: CachedPagingData.kt */
    /* loaded from: classes.dex */
    public enum FlowType {
        PAGED_DATA_FLOW,
        PAGE_EVENT_FLOW
    }

    Object a(FlowType flowType, q70<? super te4> q70Var);

    Object b(FlowType flowType, q70<? super te4> q70Var);
}
