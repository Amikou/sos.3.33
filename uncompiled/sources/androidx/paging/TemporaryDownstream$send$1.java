package androidx.paging;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPageEventFlow.kt */
@a(c = "androidx.paging.TemporaryDownstream", f = "CachedPageEventFlow.kt", l = {149}, m = "send")
/* loaded from: classes.dex */
public final class TemporaryDownstream$send$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ TemporaryDownstream this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TemporaryDownstream$send$1(TemporaryDownstream temporaryDownstream, q70 q70Var) {
        super(q70Var);
        this.this$0 = temporaryDownstream;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.c(null, this);
    }
}
