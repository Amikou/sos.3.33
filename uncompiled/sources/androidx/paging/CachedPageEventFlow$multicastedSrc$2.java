package androidx.paging;

import kotlin.jvm.internal.FunctionReferenceImpl;

/* compiled from: CachedPageEventFlow.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class CachedPageEventFlow$multicastedSrc$2 extends FunctionReferenceImpl implements hd1<lq1<? extends yo2<T>>, q70<? super te4>, Object> {
    public CachedPageEventFlow$multicastedSrc$2(FlattenedPageController flattenedPageController) {
        super(2, flattenedPageController, FlattenedPageController.class, "record", "record(Lkotlin/collections/IndexedValue;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", 0);
    }

    @Override // defpackage.hd1
    public final Object invoke(lq1<? extends yo2<T>> lq1Var, q70<? super te4> q70Var) {
        return ((FlattenedPageController) this.receiver).b(lq1Var, q70Var);
    }
}
