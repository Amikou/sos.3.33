package androidx.paging;

import androidx.paging.PagePresenter;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: PagingDataDiffer.kt */
/* loaded from: classes.dex */
public abstract class PagingDataDiffer<T> {
    public PagePresenter<T> a;
    public le4 b;
    public final hb2 c;
    public final CopyOnWriteArrayList<tc1<a30, te4>> d;
    public final SingleRunner e;
    public volatile boolean f;
    public volatile int g;
    public final a h;
    public final jb2<a30> i;
    public final po0 j;
    public final CoroutineDispatcher k;

    /* compiled from: PagingDataDiffer.kt */
    /* renamed from: androidx.paging.PagingDataDiffer$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<a30, te4> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(a30 a30Var) {
            invoke2(a30Var);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(a30 a30Var) {
            fs1.f(a30Var, "it");
            PagingDataDiffer.this.i.setValue(a30Var);
        }
    }

    /* compiled from: PagingDataDiffer.kt */
    /* loaded from: classes.dex */
    public static final class a implements PagePresenter.b {
        public a() {
        }

        @Override // androidx.paging.PagePresenter.b
        public void a(int i, int i2) {
            PagingDataDiffer.this.j.a(i, i2);
        }

        @Override // androidx.paging.PagePresenter.b
        public void b(LoadType loadType, boolean z, w02 w02Var) {
            fs1.f(loadType, "loadType");
            fs1.f(w02Var, "loadState");
            if (fs1.b(PagingDataDiffer.this.c.d(loadType, z), w02Var)) {
                return;
            }
            PagingDataDiffer.this.c.g(loadType, z, w02Var);
            a30 h = PagingDataDiffer.this.c.h();
            for (tc1 tc1Var : PagingDataDiffer.this.d) {
                tc1Var.invoke(h);
            }
        }

        @Override // androidx.paging.PagePresenter.b
        public void onInserted(int i, int i2) {
            PagingDataDiffer.this.j.onInserted(i, i2);
        }

        @Override // androidx.paging.PagePresenter.b
        public void onRemoved(int i, int i2) {
            PagingDataDiffer.this.j.onRemoved(i, i2);
        }
    }

    public PagingDataDiffer(po0 po0Var, CoroutineDispatcher coroutineDispatcher) {
        fs1.f(po0Var, "differCallback");
        fs1.f(coroutineDispatcher, "mainDispatcher");
        this.j = po0Var;
        this.k = coroutineDispatcher;
        this.a = PagePresenter.f.a();
        hb2 hb2Var = new hb2();
        this.c = hb2Var;
        this.d = new CopyOnWriteArrayList<>();
        this.e = new SingleRunner(false, 1, null);
        this.h = new a();
        this.i = xs3.a(hb2Var.h());
        p(new AnonymousClass1());
    }

    public final void p(tc1<? super a30, te4> tc1Var) {
        fs1.f(tc1Var, "listener");
        this.d.add(tc1Var);
        tc1Var.invoke(this.c.h());
    }

    public final Object q(fp2<T> fp2Var, q70<? super te4> q70Var) {
        Object c = SingleRunner.c(this.e, 0, new PagingDataDiffer$collectFrom$2(this, fp2Var, null), q70Var, 1, null);
        return c == gs1.d() ? c : te4.a;
    }

    public final void r(a30 a30Var) {
        if (fs1.b(this.c.h(), a30Var)) {
            return;
        }
        this.c.e(a30Var);
        Iterator<T> it = this.d.iterator();
        while (it.hasNext()) {
            ((tc1) it.next()).invoke(a30Var);
        }
    }

    public final T s(int i) {
        this.f = true;
        this.g = i;
        le4 le4Var = this.b;
        if (le4Var != null) {
            le4Var.b(this.a.g(i));
        }
        return this.a.l(i);
    }

    public final j71<a30> t() {
        return this.i;
    }

    public final int u() {
        return this.a.a();
    }

    public abstract boolean v();

    public abstract Object w(vi2<T> vi2Var, vi2<T> vi2Var2, a30 a30Var, int i, rc1<te4> rc1Var, q70<? super Integer> q70Var);

    public final void x() {
        le4 le4Var = this.b;
        if (le4Var != null) {
            le4Var.a();
        }
    }

    public final void y(tc1<? super a30, te4> tc1Var) {
        fs1.f(tc1Var, "listener");
        this.d.remove(tc1Var);
    }
}
