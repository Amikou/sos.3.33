package androidx.paging;

import defpackage.w02;
import defpackage.xk4;
import defpackage.yo2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/* compiled from: PagePresenter.kt */
/* loaded from: classes.dex */
public final class PagePresenter<T> implements vi2<T> {
    public final List<xa4<T>> a;
    public int b;
    public int c;
    public int d;
    public static final a f = new a(null);
    public static final PagePresenter<Object> e = new PagePresenter<>(yo2.b.g.d());

    /* compiled from: PagePresenter.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a() {
        }

        public final <T> PagePresenter<T> a() {
            PagePresenter<T> pagePresenter = PagePresenter.e;
            Objects.requireNonNull(pagePresenter, "null cannot be cast to non-null type androidx.paging.PagePresenter<T>");
            return pagePresenter;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: PagePresenter.kt */
    /* loaded from: classes.dex */
    public interface b {
        void a(int i, int i2);

        void b(LoadType loadType, boolean z, w02 w02Var);

        void onInserted(int i, int i2);

        void onRemoved(int i, int i2);
    }

    public PagePresenter(yo2.b<T> bVar) {
        fs1.f(bVar, "insertEvent");
        this.a = j20.m0(bVar.f());
        this.b = k(bVar.f());
        this.c = bVar.h();
        this.d = bVar.g();
    }

    @Override // defpackage.vi2
    public int a() {
        return c() + b() + d();
    }

    @Override // defpackage.vi2
    public int b() {
        return this.b;
    }

    @Override // defpackage.vi2
    public int c() {
        return this.c;
    }

    @Override // defpackage.vi2
    public int d() {
        return this.d;
    }

    @Override // defpackage.vi2
    public T e(int i) {
        int size = this.a.size();
        int i2 = 0;
        while (i2 < size) {
            int size2 = this.a.get(i2).b().size();
            if (size2 > i) {
                break;
            }
            i -= size2;
            i2++;
        }
        return this.a.get(i2).b().get(i);
    }

    public final xk4.a g(int i) {
        int i2 = 0;
        int c = i - c();
        while (c >= this.a.get(i2).b().size() && i2 < b20.i(this.a)) {
            c -= this.a.get(i2).b().size();
            i2++;
        }
        return this.a.get(i2).d(c, i - c(), ((a() - i) - d()) - 1, m(), n());
    }

    public final void h(int i) {
        if (i < 0 || i >= a()) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + a());
        }
    }

    public final void i(yo2.a<T> aVar, b bVar) {
        int a2 = a();
        LoadType a3 = aVar.a();
        LoadType loadType = LoadType.PREPEND;
        if (a3 == loadType) {
            int c = c();
            this.b = b() - j(new sr1(aVar.c(), aVar.b()));
            this.c = aVar.e();
            int a4 = a() - a2;
            if (a4 > 0) {
                bVar.onInserted(0, a4);
            } else if (a4 < 0) {
                bVar.onRemoved(0, -a4);
            }
            int max = Math.max(0, c + a4);
            int e2 = aVar.e() - max;
            if (e2 > 0) {
                bVar.a(max, e2);
            }
            bVar.b(loadType, false, w02.c.d.b());
            return;
        }
        int d = d();
        this.b = b() - j(new sr1(aVar.c(), aVar.b()));
        this.d = aVar.e();
        int a5 = a() - a2;
        if (a5 > 0) {
            bVar.onInserted(a2, a5);
        } else if (a5 < 0) {
            bVar.onRemoved(a2 + a5, -a5);
        }
        int e3 = aVar.e() - (d - (a5 < 0 ? Math.min(d, -a5) : 0));
        if (e3 > 0) {
            bVar.a(a() - aVar.e(), e3);
        }
        bVar.b(LoadType.APPEND, false, w02.c.d.b());
    }

    public final int j(sr1 sr1Var) {
        boolean z;
        Iterator<xa4<T>> it = this.a.iterator();
        int i = 0;
        while (it.hasNext()) {
            xa4<T> next = it.next();
            int[] c = next.c();
            int length = c.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z = false;
                    break;
                } else if (sr1Var.s(c[i2])) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                i += next.b().size();
                it.remove();
            }
        }
        return i;
    }

    public final int k(List<xa4<T>> list) {
        Iterator<T> it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((xa4) it.next()).b().size();
        }
        return i;
    }

    public final T l(int i) {
        h(i);
        int c = i - c();
        if (c < 0 || c >= b()) {
            return null;
        }
        return e(c);
    }

    public final int m() {
        Integer B = ai.B(((xa4) j20.L(this.a)).c());
        fs1.d(B);
        return B.intValue();
    }

    public final int n() {
        Integer A = ai.A(((xa4) j20.U(this.a)).c());
        fs1.d(A);
        return A.intValue();
    }

    public final xk4.b o() {
        int b2 = b() / 2;
        return new xk4.b(b2, b2, m(), n());
    }

    public final void p(yo2.b<T> bVar, b bVar2) {
        int k = k(bVar.f());
        int a2 = a();
        int i = cp2.a[bVar.e().ordinal()];
        if (i != 1) {
            if (i == 2) {
                int min = Math.min(c(), k);
                int i2 = k - min;
                this.a.addAll(0, bVar.f());
                this.b = b() + k;
                this.c = bVar.h();
                bVar2.a(c() - min, min);
                bVar2.onInserted(0, i2);
                int a3 = (a() - a2) - i2;
                if (a3 > 0) {
                    bVar2.onInserted(0, a3);
                } else if (a3 < 0) {
                    bVar2.onRemoved(0, -a3);
                }
            } else if (i == 3) {
                int min2 = Math.min(d(), k);
                int c = c() + b();
                int i3 = k - min2;
                List<xa4<T>> list = this.a;
                list.addAll(list.size(), bVar.f());
                this.b = b() + k;
                this.d = bVar.g();
                bVar2.a(c, min2);
                bVar2.onInserted(c + min2, i3);
                int a4 = (a() - a2) - i3;
                if (a4 > 0) {
                    bVar2.onInserted(a() - a4, a4);
                } else if (a4 < 0) {
                    bVar2.onRemoved(a(), -a4);
                }
            }
            bVar.d().a(new PagePresenter$insertPage$1(bVar2));
            return;
        }
        throw new IllegalArgumentException();
    }

    public final void q(yo2<T> yo2Var, b bVar) {
        fs1.f(yo2Var, "pageEvent");
        fs1.f(bVar, "callback");
        if (yo2Var instanceof yo2.b) {
            p((yo2.b) yo2Var, bVar);
        } else if (yo2Var instanceof yo2.a) {
            i((yo2.a) yo2Var, bVar);
        } else if (yo2Var instanceof yo2.c) {
            yo2.c cVar = (yo2.c) yo2Var;
            bVar.b(cVar.c(), cVar.a(), cVar.b());
        }
    }

    public String toString() {
        int b2 = b();
        ArrayList arrayList = new ArrayList(b2);
        for (int i = 0; i < b2; i++) {
            arrayList.add(e(i));
        }
        String S = j20.S(arrayList, null, null, null, 0, null, null, 63, null);
        return "[(" + c() + " placeholders), " + S + ", (" + d() + " placeholders)]";
    }
}
