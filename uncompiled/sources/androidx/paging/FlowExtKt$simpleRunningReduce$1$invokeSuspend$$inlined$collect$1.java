package androidx.paging;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;

/* compiled from: Collect.kt */
/* loaded from: classes.dex */
public final class FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1 implements k71<T> {
    public final /* synthetic */ FlowExtKt$simpleRunningReduce$1 a;
    public final /* synthetic */ k71 f0;
    public final /* synthetic */ Ref$ObjectRef g0;

    @a(c = "androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1", f = "FlowExt.kt", l = {137, 140}, m = "emit")
    /* renamed from: androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends ContinuationImpl {
        public Object L$0;
        public Object L$1;
        public int label;
        public /* synthetic */ Object result;

        public AnonymousClass1(q70 q70Var) {
            super(q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1.this.emit(null, this);
        }
    }

    public FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1(FlowExtKt$simpleRunningReduce$1 flowExtKt$simpleRunningReduce$1, k71 k71Var, Ref$ObjectRef ref$ObjectRef) {
        this.a = flowExtKt$simpleRunningReduce$1;
        this.f0 = k71Var;
        this.g0 = ref$ObjectRef;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0043  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0084 A[RETURN] */
    @Override // defpackage.k71
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object emit(java.lang.Object r8, defpackage.q70 r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1.AnonymousClass1
            if (r0 == 0) goto L13
            r0 = r9
            androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1$1 r0 = (androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1.AnonymousClass1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1$1 r0 = new androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1$1
            r0.<init>(r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L43
            if (r2 == r4) goto L34
            if (r2 != r3) goto L2c
            defpackage.o83.b(r9)
            goto L85
        L2c:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L34:
            java.lang.Object r8 = r0.L$1
            kotlin.jvm.internal.Ref$ObjectRef r8 = (kotlin.jvm.internal.Ref$ObjectRef) r8
            java.lang.Object r2 = r0.L$0
            androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1 r2 = (androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1) r2
            defpackage.o83.b(r9)
            r6 = r9
            r9 = r8
            r8 = r6
            goto L6f
        L43:
            defpackage.o83.b(r9)
            kotlin.jvm.internal.Ref$ObjectRef r9 = r7.g0
            T r2 = r9.element
            java.lang.Object r5 = androidx.paging.FlowExtKt.a()
            if (r2 != r5) goto L52
        L50:
            r2 = r7
            goto L6f
        L52:
            androidx.paging.FlowExtKt$simpleRunningReduce$1 r2 = r7.a
            kd1 r2 = r2.$operation
            kotlin.jvm.internal.Ref$ObjectRef r5 = r7.g0
            T r5 = r5.element
            r0.L$0 = r7
            r0.L$1 = r9
            r0.label = r4
            r4 = 6
            defpackage.uq1.c(r4)
            java.lang.Object r8 = r2.invoke(r5, r8, r0)
            r2 = 7
            defpackage.uq1.c(r2)
            if (r8 != r1) goto L50
            return r1
        L6f:
            r9.element = r8
            k71 r8 = r2.f0
            kotlin.jvm.internal.Ref$ObjectRef r9 = r2.g0
            T r9 = r9.element
            r2 = 0
            r0.L$0 = r2
            r0.L$1 = r2
            r0.label = r3
            java.lang.Object r8 = r8.emit(r9, r0)
            if (r8 != r1) goto L85
            return r1
        L85:
            te4 r8 = defpackage.te4.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.FlowExtKt$simpleRunningReduce$1$invokeSuspend$$inlined$collect$1.emit(java.lang.Object, q70):java.lang.Object");
    }
}
