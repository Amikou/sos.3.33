package androidx.paging;

import androidx.paging.PageFetcher;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.FunctionReferenceImpl;

/* compiled from: PageFetcher.kt */
@kotlin.coroutines.jvm.internal.a(c = "androidx.paging.PageFetcher$flow$1", f = "PageFetcher.kt", l = {254}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcher$flow$1 extends SuspendLambda implements hd1<lp3<fp2<Value>>, q70<? super te4>, Object> {
    private /* synthetic */ Object L$0;
    public int label;
    public final /* synthetic */ PageFetcher this$0;

    /* compiled from: PageFetcher.kt */
    @kotlin.coroutines.jvm.internal.a(c = "androidx.paging.PageFetcher$flow$1$1", f = "PageFetcher.kt", l = {58, 58}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcher$flow$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<k71<? super Boolean>, q70<? super te4>, Object> {
        public final /* synthetic */ o63 $remoteMediatorAccessor;
        private /* synthetic */ Object L$0;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(o63 o63Var, q70 q70Var) {
            super(2, q70Var);
            this.$remoteMediatorAccessor = o63Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$remoteMediatorAccessor, q70Var);
            anonymousClass1.L$0 = obj;
            return anonymousClass1;
        }

        @Override // defpackage.hd1
        public final Object invoke(k71<? super Boolean> k71Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(k71Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:20:0x0043  */
        /* JADX WARN: Removed duplicated region for block: B:23:0x0052 A[RETURN] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = defpackage.gs1.d()
                int r1 = r6.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L23
                if (r1 == r4) goto L1b
                if (r1 != r3) goto L13
                defpackage.o83.b(r7)
                goto L53
            L13:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L1b:
                java.lang.Object r1 = r6.L$0
                k71 r1 = (defpackage.k71) r1
                defpackage.o83.b(r7)
                goto L3a
            L23:
                defpackage.o83.b(r7)
                java.lang.Object r7 = r6.L$0
                r1 = r7
                k71 r1 = (defpackage.k71) r1
                o63 r7 = r6.$remoteMediatorAccessor
                if (r7 == 0) goto L3d
                r6.L$0 = r1
                r6.label = r4
                java.lang.Object r7 = r7.b(r6)
                if (r7 != r0) goto L3a
                return r0
            L3a:
                androidx.paging.RemoteMediator$InitializeAction r7 = (androidx.paging.RemoteMediator.InitializeAction) r7
                goto L3e
            L3d:
                r7 = r2
            L3e:
                androidx.paging.RemoteMediator$InitializeAction r5 = androidx.paging.RemoteMediator.InitializeAction.LAUNCH_INITIAL_REFRESH
                if (r7 != r5) goto L43
                goto L44
            L43:
                r4 = 0
            L44:
                java.lang.Boolean r7 = defpackage.hr.a(r4)
                r6.L$0 = r2
                r6.label = r3
                java.lang.Object r7 = r1.emit(r7, r6)
                if (r7 != r0) goto L53
                return r0
            L53:
                te4 r7 = defpackage.te4.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcher$flow$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* compiled from: PageFetcher.kt */
    @kotlin.coroutines.jvm.internal.a(c = "androidx.paging.PageFetcher$flow$1$2", f = "PageFetcher.kt", l = {63, 66, 69}, m = "invokeSuspend")
    /* renamed from: androidx.paging.PageFetcher$flow$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends SuspendLambda implements kd1<PageFetcher.a<Key, Value>, Boolean, q70<? super PageFetcher.a<Key, Value>>, Object> {
        public final /* synthetic */ o63 $remoteMediatorAccessor;
        private /* synthetic */ Object L$0;
        public Object L$1;
        public Object L$2;
        private /* synthetic */ boolean Z$0;
        public int label;

        /* compiled from: PageFetcher.kt */
        /* renamed from: androidx.paging.PageFetcher$flow$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final /* synthetic */ class AnonymousClass1 extends FunctionReferenceImpl implements rc1<te4> {
            public AnonymousClass1(PageFetcher pageFetcher) {
                super(0, pageFetcher, PageFetcher.class, "refresh", "refresh()V", 0);
            }

            @Override // defpackage.rc1
            public /* bridge */ /* synthetic */ te4 invoke() {
                invoke2();
                return te4.a;
            }

            @Override // defpackage.rc1
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                ((PageFetcher) this.receiver).l();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(o63 o63Var, q70 q70Var) {
            super(3, q70Var);
            this.$remoteMediatorAccessor = o63Var;
        }

        public final q70<te4> create(PageFetcher.a<Key, Value> aVar, boolean z, q70<? super PageFetcher.a<Key, Value>> q70Var) {
            fs1.f(q70Var, "continuation");
            AnonymousClass2 anonymousClass2 = new AnonymousClass2(this.$remoteMediatorAccessor, q70Var);
            anonymousClass2.L$0 = aVar;
            anonymousClass2.Z$0 = z;
            return anonymousClass2;
        }

        @Override // defpackage.kd1
        public final Object invoke(Object obj, Boolean bool, Object obj2) {
            return ((AnonymousClass2) create((PageFetcher.a) obj, bool.booleanValue(), (q70) obj2)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:25:0x00aa  */
        /* JADX WARN: Removed duplicated region for block: B:30:0x00cf  */
        /* JADX WARN: Removed duplicated region for block: B:40:0x00f4  */
        /* JADX WARN: Removed duplicated region for block: B:41:0x00f9  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x00fc  */
        /* JADX WARN: Removed duplicated region for block: B:49:0x0108 A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:58:0x0123  */
        /* JADX WARN: Removed duplicated region for block: B:59:0x0128  */
        /* JADX WARN: Removed duplicated region for block: B:61:0x012b  */
        /* JADX WARN: Removed duplicated region for block: B:68:0x013f  */
        /* JADX WARN: Removed duplicated region for block: B:74:0x0155  */
        /* JADX WARN: Type inference failed for: r8v12, types: [T, gp2] */
        /* JADX WARN: Type inference failed for: r8v4, types: [T, gp2] */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:28:0x00c3 -> B:29:0x00c7). Please submit an issue!!! */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r22) {
            /*
                Method dump skipped, instructions count: 404
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.paging.PageFetcher$flow$1.AnonymousClass2.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* compiled from: Collect.kt */
    /* loaded from: classes.dex */
    public static final class a implements k71<fp2<Value>> {
        public final /* synthetic */ lp3 a;

        public a(lp3 lp3Var) {
            this.a = lp3Var;
        }

        @Override // defpackage.k71
        public Object emit(Object obj, q70 q70Var) {
            Object h = this.a.h((fp2) obj, q70Var);
            return h == gs1.d() ? h : te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcher$flow$1(PageFetcher pageFetcher, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = pageFetcher;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        PageFetcher$flow$1 pageFetcher$flow$1 = new PageFetcher$flow$1(this.this$0, q70Var);
        pageFetcher$flow$1.L$0 = obj;
        return pageFetcher$flow$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, q70<? super te4> q70Var) {
        return ((PageFetcher$flow$1) create(obj, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        ConflatedEventBus conflatedEventBus;
        RemoteMediator unused;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            unused = this.this$0.g;
            conflatedEventBus = this.this$0.a;
            j71 d2 = FlowExtKt.d(n71.m(FlowExtKt.c(n71.u(conflatedEventBus.a(), new AnonymousClass1(null, null)), null, new AnonymousClass2(null, null))), new PageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1(null, this, null));
            a aVar = new a((lp3) this.L$0);
            this.label = 1;
            if (d2.a(aVar, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
