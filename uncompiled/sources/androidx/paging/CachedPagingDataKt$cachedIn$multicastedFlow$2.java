package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPagingData.kt */
@a(c = "androidx.paging.CachedPagingDataKt$cachedIn$multicastedFlow$2", f = "CachedPagingData.kt", l = {93}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CachedPagingDataKt$cachedIn$multicastedFlow$2 extends SuspendLambda implements kd1<MulticastedPagingData<T>, MulticastedPagingData<T>, q70<? super MulticastedPagingData<T>>, Object> {
    private /* synthetic */ Object L$0;
    private /* synthetic */ Object L$1;
    public int label;

    public CachedPagingDataKt$cachedIn$multicastedFlow$2(q70 q70Var) {
        super(3, q70Var);
    }

    public final q70<te4> create(MulticastedPagingData<T> multicastedPagingData, MulticastedPagingData<T> multicastedPagingData2, q70<? super MulticastedPagingData<T>> q70Var) {
        fs1.f(multicastedPagingData, "prev");
        fs1.f(multicastedPagingData2, "next");
        fs1.f(q70Var, "continuation");
        CachedPagingDataKt$cachedIn$multicastedFlow$2 cachedPagingDataKt$cachedIn$multicastedFlow$2 = new CachedPagingDataKt$cachedIn$multicastedFlow$2(q70Var);
        cachedPagingDataKt$cachedIn$multicastedFlow$2.L$0 = multicastedPagingData;
        cachedPagingDataKt$cachedIn$multicastedFlow$2.L$1 = multicastedPagingData2;
        return cachedPagingDataKt$cachedIn$multicastedFlow$2;
    }

    @Override // defpackage.kd1
    public final Object invoke(Object obj, Object obj2, Object obj3) {
        return ((CachedPagingDataKt$cachedIn$multicastedFlow$2) create((MulticastedPagingData) obj, (MulticastedPagingData) obj2, (q70) obj3)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i != 0) {
            if (i == 1) {
                MulticastedPagingData multicastedPagingData = (MulticastedPagingData) this.L$0;
                o83.b(obj);
                return multicastedPagingData;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        o83.b(obj);
        MulticastedPagingData multicastedPagingData2 = (MulticastedPagingData) this.L$1;
        this.L$0 = multicastedPagingData2;
        this.label = 1;
        return ((MulticastedPagingData) this.L$0).b(this) == d ? d : multicastedPagingData2;
    }
}
