package androidx.paging;

import androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1;
import defpackage.uj3;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SimpleChannelFlow.kt */
@a(c = "androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1", f = "SimpleChannelFlow.kt", l = {52}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ kx $channel;
    public int label;
    public final /* synthetic */ SimpleChannelFlowKt$simpleChannelFlow$1.AnonymousClass1 this$0;

    /* compiled from: SimpleChannelFlow.kt */
    @a(c = "androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1$1", f = "SimpleChannelFlow.kt", l = {57}, m = "invokeSuspend")
    /* renamed from: androidx.paging.SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        private /* synthetic */ Object L$0;
        public int label;

        public AnonymousClass1(q70 q70Var) {
            super(2, q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            fs1.f(q70Var, "completion");
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(q70Var);
            anonymousClass1.L$0 = obj;
            return anonymousClass1;
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                mp3 mp3Var = new mp3((c90) this.L$0, SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1.this.$channel);
                hd1 hd1Var = SimpleChannelFlowKt$simpleChannelFlow$1.this.$block;
                this.label = 1;
                if (hd1Var.invoke(mp3Var, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1(SimpleChannelFlowKt$simpleChannelFlow$1.AnonymousClass1 anonymousClass1, kx kxVar, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = anonymousClass1;
        this.$channel = kxVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1(this.this$0, this.$channel, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SimpleChannelFlowKt$simpleChannelFlow$1$1$producer$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        try {
            if (i == 0) {
                o83.b(obj);
                AnonymousClass1 anonymousClass1 = new AnonymousClass1(null);
                this.label = 1;
                if (d90.b(anonymousClass1, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            uj3.a.a(this.$channel, null, 1, null);
        } catch (Throwable th) {
            this.$channel.l(th);
        }
        return te4.a;
    }
}
