package androidx.paging;

/* compiled from: CachedPagingData.kt */
/* loaded from: classes.dex */
public final class MulticastedPagingData<T> {
    public final CachedPageEventFlow<T> a;
    public final c90 b;
    public final fp2<T> c;
    public final ActiveFlowTracker d;

    public MulticastedPagingData(c90 c90Var, fp2<T> fp2Var, ActiveFlowTracker activeFlowTracker) {
        fs1.f(c90Var, "scope");
        fs1.f(fp2Var, "parent");
        this.b = c90Var;
        this.c = fp2Var;
        this.d = activeFlowTracker;
        this.a = new CachedPageEventFlow<>(n71.s(n71.u(fp2Var.a(), new MulticastedPagingData$accumulated$1(this, null)), new MulticastedPagingData$accumulated$2(this, null)), c90Var);
    }

    public final fp2<T> a() {
        return new fp2<>(this.a.e(), this.c.b());
    }

    public final Object b(q70<? super te4> q70Var) {
        Object d = this.a.d(q70Var);
        return d == gs1.d() ? d : te4.a;
    }

    public final ActiveFlowTracker c() {
        return this.d;
    }

    public /* synthetic */ MulticastedPagingData(c90 c90Var, fp2 fp2Var, ActiveFlowTracker activeFlowTracker, int i, qi0 qi0Var) {
        this(c90Var, fp2Var, (i & 4) != 0 ? null : activeFlowTracker);
    }
}
