package androidx.paging;

import androidx.paging.PagePresenter;
import kotlin.jvm.internal.Lambda;

/* compiled from: PagePresenter.kt */
/* loaded from: classes.dex */
public final class PagePresenter$insertPage$1 extends Lambda implements kd1<LoadType, Boolean, w02, te4> {
    public final /* synthetic */ PagePresenter.b $callback;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PagePresenter$insertPage$1(PagePresenter.b bVar) {
        super(3);
        this.$callback = bVar;
    }

    @Override // defpackage.kd1
    public /* bridge */ /* synthetic */ te4 invoke(LoadType loadType, Boolean bool, w02 w02Var) {
        invoke(loadType, bool.booleanValue(), w02Var);
        return te4.a;
    }

    public final void invoke(LoadType loadType, boolean z, w02 w02Var) {
        fs1.f(loadType, "type");
        fs1.f(w02Var, "state");
        this.$callback.b(loadType, z, w02Var);
    }
}
