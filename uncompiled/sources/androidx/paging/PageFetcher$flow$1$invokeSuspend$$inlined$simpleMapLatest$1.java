package androidx.paging;

import androidx.paging.PageFetcher;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: FlowExt.kt */
@a(c = "androidx.paging.PageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1", f = "PageFetcher.kt", l = {105}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1 extends SuspendLambda implements kd1<k71<? super fp2<Value>>, PageFetcher.a<Key, Value>, q70<? super te4>, Object> {
    public final /* synthetic */ o63 $remoteMediatorAccessor$inlined;
    private /* synthetic */ Object L$0;
    private /* synthetic */ Object L$1;
    public int label;
    public final /* synthetic */ PageFetcher$flow$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1(q70 q70Var, PageFetcher$flow$1 pageFetcher$flow$1, o63 o63Var) {
        super(3, q70Var);
        this.this$0 = pageFetcher$flow$1;
        this.$remoteMediatorAccessor$inlined = o63Var;
    }

    public final q70<te4> create(k71<? super fp2<Value>> k71Var, PageFetcher.a<Key, Value> aVar, q70<? super te4> q70Var) {
        fs1.f(k71Var, "$this$create");
        fs1.f(q70Var, "continuation");
        PageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1 pageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1 = new PageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1(q70Var, this.this$0, this.$remoteMediatorAccessor$inlined);
        pageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1.L$0 = k71Var;
        pageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1.L$1 = aVar;
        return pageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1;
    }

    @Override // defpackage.kd1
    public final Object invoke(Object obj, Object obj2, q70<? super te4> q70Var) {
        return ((PageFetcher$flow$1$invokeSuspend$$inlined$simpleMapLatest$1) create((k71) obj, obj2, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        j71 j;
        ConflatedEventBus conflatedEventBus;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            PageFetcher.a aVar = (PageFetcher.a) this.L$1;
            j = this.this$0.this$0.j(aVar.a(), this.$remoteMediatorAccessor$inlined);
            PageFetcher pageFetcher = this.this$0.this$0;
            PageFetcherSnapshot a = aVar.a();
            conflatedEventBus = this.this$0.this$0.b;
            fp2 fp2Var = new fp2(j, new PageFetcher.b(pageFetcher, a, conflatedEventBus));
            this.label = 1;
            if (((k71) this.L$0).emit(fp2Var, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
