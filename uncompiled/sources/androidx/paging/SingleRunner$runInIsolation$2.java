package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SingleRunner.kt */
@a(c = "androidx.paging.SingleRunner$runInIsolation$2", f = "SingleRunner.kt", l = {55, 59, 61, 61}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class SingleRunner$runInIsolation$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ tc1 $block;
    public final /* synthetic */ int $priority;
    private /* synthetic */ Object L$0;
    public int label;
    public final /* synthetic */ SingleRunner this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SingleRunner$runInIsolation$2(SingleRunner singleRunner, int i, tc1 tc1Var, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = singleRunner;
        this.$priority = i;
        this.$block = tc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        SingleRunner$runInIsolation$2 singleRunner$runInIsolation$2 = new SingleRunner$runInIsolation$2(this.this$0, this.$priority, this.$block, q70Var);
        singleRunner$runInIsolation$2.L$0 = obj;
        return singleRunner$runInIsolation$2;
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SingleRunner$runInIsolation$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:31:0x008d A[RETURN] */
    /* JADX WARN: Type inference failed for: r1v0, types: [int] */
    /* JADX WARN: Type inference failed for: r1v1, types: [st1] */
    /* JADX WARN: Type inference failed for: r1v14 */
    /* JADX WARN: Type inference failed for: r1v15 */
    /* JADX WARN: Type inference failed for: r1v9, types: [st1] */
    /* JADX WARN: Type inference failed for: r3v2, types: [androidx.paging.SingleRunner$Holder] */
    /* JADX WARN: Type inference failed for: r9v15, types: [androidx.paging.SingleRunner$Holder] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r8.label
            r2 = 4
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 == 0) goto L3c
            if (r1 == r5) goto L34
            if (r1 == r4) goto L2a
            if (r1 == r3) goto L25
            if (r1 == r2) goto L1c
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L1c:
            java.lang.Object r0 = r8.L$0
            java.lang.Throwable r0 = (java.lang.Throwable) r0
            defpackage.o83.b(r9)
            goto La0
        L25:
            defpackage.o83.b(r9)
            goto La1
        L2a:
            java.lang.Object r1 = r8.L$0
            st1 r1 = (defpackage.st1) r1
            defpackage.o83.b(r9)     // Catch: java.lang.Throwable -> L32
            goto L7c
        L32:
            r9 = move-exception
            goto L8e
        L34:
            java.lang.Object r1 = r8.L$0
            st1 r1 = (defpackage.st1) r1
            defpackage.o83.b(r9)
            goto L67
        L3c:
            defpackage.o83.b(r9)
            java.lang.Object r9 = r8.L$0
            c90 r9 = (defpackage.c90) r9
            kotlin.coroutines.CoroutineContext r9 = r9.m()
            st1$b r1 = defpackage.st1.f
            kotlin.coroutines.CoroutineContext$a r9 = r9.get(r1)
            if (r9 == 0) goto La4
            st1 r9 = (defpackage.st1) r9
            androidx.paging.SingleRunner r1 = r8.this$0
            androidx.paging.SingleRunner$Holder r1 = androidx.paging.SingleRunner.a(r1)
            int r6 = r8.$priority
            r8.L$0 = r9
            r8.label = r5
            java.lang.Object r1 = r1.b(r6, r9, r8)
            if (r1 != r0) goto L64
            return r0
        L64:
            r7 = r1
            r1 = r9
            r9 = r7
        L67:
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto La1
            tc1 r9 = r8.$block     // Catch: java.lang.Throwable -> L32
            r8.L$0 = r1     // Catch: java.lang.Throwable -> L32
            r8.label = r4     // Catch: java.lang.Throwable -> L32
            java.lang.Object r9 = r9.invoke(r8)     // Catch: java.lang.Throwable -> L32
            if (r9 != r0) goto L7c
            return r0
        L7c:
            androidx.paging.SingleRunner r9 = r8.this$0
            androidx.paging.SingleRunner$Holder r9 = androidx.paging.SingleRunner.a(r9)
            r2 = 0
            r8.L$0 = r2
            r8.label = r3
            java.lang.Object r9 = r9.a(r1, r8)
            if (r9 != r0) goto La1
            return r0
        L8e:
            androidx.paging.SingleRunner r3 = r8.this$0
            androidx.paging.SingleRunner$Holder r3 = androidx.paging.SingleRunner.a(r3)
            r8.L$0 = r9
            r8.label = r2
            java.lang.Object r1 = r3.a(r1, r8)
            if (r1 != r0) goto L9f
            return r0
        L9f:
            r0 = r9
        La0:
            throw r0
        La1:
            te4 r9 = defpackage.te4.a
            return r9
        La4:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "Internal error. coroutineScope should've created a job."
            java.lang.String r0 = r0.toString()
            r9.<init>(r0)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.paging.SingleRunner$runInIsolation$2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
