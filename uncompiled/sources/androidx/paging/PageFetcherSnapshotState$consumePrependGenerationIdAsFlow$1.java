package androidx.paging;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PageFetcherSnapshotState.kt */
@a(c = "androidx.paging.PageFetcherSnapshotState$consumePrependGenerationIdAsFlow$1", f = "PageFetcherSnapshotState.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PageFetcherSnapshotState$consumePrependGenerationIdAsFlow$1 extends SuspendLambda implements hd1<k71<? super Integer>, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ PageFetcherSnapshotState this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PageFetcherSnapshotState$consumePrependGenerationIdAsFlow$1(PageFetcherSnapshotState pageFetcherSnapshotState, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = pageFetcherSnapshotState;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new PageFetcherSnapshotState$consumePrependGenerationIdAsFlow$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(k71<? super Integer> k71Var, q70<? super te4> q70Var) {
        return ((PageFetcherSnapshotState$consumePrependGenerationIdAsFlow$1) create(k71Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        kx kxVar;
        int i;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            kxVar = this.this$0.h;
            i = this.this$0.f;
            kxVar.offer(hr.d(i));
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
