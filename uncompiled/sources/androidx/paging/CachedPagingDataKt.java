package androidx.paging;

import androidx.paging.multicast.Multicaster;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CachedPagingData.kt */
/* loaded from: classes.dex */
public final class CachedPagingDataKt {
    public static final <T> j71<fp2<T>> a(j71<fp2<T>> j71Var, c90 c90Var) {
        fs1.f(j71Var, "$this$cachedIn");
        fs1.f(c90Var, "scope");
        return b(j71Var, c90Var, null);
    }

    public static final <T> j71<fp2<T>> b(final j71<fp2<T>> j71Var, final c90 c90Var, ActiveFlowTracker activeFlowTracker) {
        fs1.f(j71Var, "$this$cachedIn");
        fs1.f(c90Var, "scope");
        final j71 b = FlowExtKt.b(new j71<MulticastedPagingData<T>>() { // from class: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1

            /* compiled from: Collect.kt */
            /* renamed from: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1$2  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass2 implements k71<fp2<T>> {
                public final /* synthetic */ k71 a;
                public final /* synthetic */ CachedPagingDataKt$cachedIn$$inlined$map$1 f0;

                @a(c = "androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1$2", f = "CachedPagingData.kt", l = {135}, m = "emit")
                /* renamed from: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1$2$1  reason: invalid class name */
                /* loaded from: classes.dex */
                public static final class AnonymousClass1 extends ContinuationImpl {
                    public Object L$0;
                    public int label;
                    public /* synthetic */ Object result;

                    public AnonymousClass1(q70 q70Var) {
                        super(q70Var);
                    }

                    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                    public final Object invokeSuspend(Object obj) {
                        this.result = obj;
                        this.label |= Integer.MIN_VALUE;
                        return AnonymousClass2.this.emit(null, this);
                    }
                }

                public AnonymousClass2(k71 k71Var, CachedPagingDataKt$cachedIn$$inlined$map$1 cachedPagingDataKt$cachedIn$$inlined$map$1) {
                    this.a = k71Var;
                    this.f0 = cachedPagingDataKt$cachedIn$$inlined$map$1;
                }

                /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
                /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
                @Override // defpackage.k71
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public java.lang.Object emit(java.lang.Object r11, defpackage.q70 r12) {
                    /*
                        r10 = this;
                        boolean r0 = r12 instanceof androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1.AnonymousClass2.AnonymousClass1
                        if (r0 == 0) goto L13
                        r0 = r12
                        androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1$2$1 r0 = (androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1.AnonymousClass2.AnonymousClass1) r0
                        int r1 = r0.label
                        r2 = -2147483648(0xffffffff80000000, float:-0.0)
                        r3 = r1 & r2
                        if (r3 == 0) goto L13
                        int r1 = r1 - r2
                        r0.label = r1
                        goto L18
                    L13:
                        androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1$2$1 r0 = new androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1$2$1
                        r0.<init>(r12)
                    L18:
                        java.lang.Object r12 = r0.result
                        java.lang.Object r1 = defpackage.gs1.d()
                        int r2 = r0.label
                        r3 = 1
                        if (r2 == 0) goto L31
                        if (r2 != r3) goto L29
                        defpackage.o83.b(r12)
                        goto L4f
                    L29:
                        java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
                        java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
                        r11.<init>(r12)
                        throw r11
                    L31:
                        defpackage.o83.b(r12)
                        k71 r12 = r10.a
                        r6 = r11
                        fp2 r6 = (defpackage.fp2) r6
                        androidx.paging.MulticastedPagingData r11 = new androidx.paging.MulticastedPagingData
                        androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1 r2 = r10.f0
                        c90 r5 = r2
                        r7 = 0
                        r8 = 4
                        r9 = 0
                        r4 = r11
                        r4.<init>(r5, r6, r7, r8, r9)
                        r0.label = r3
                        java.lang.Object r11 = r12.emit(r11, r0)
                        if (r11 != r1) goto L4f
                        return r1
                    L4f:
                        te4 r11 = defpackage.te4.a
                        return r11
                    */
                    throw new UnsupportedOperationException("Method not decompiled: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$1.AnonymousClass2.emit(java.lang.Object, q70):java.lang.Object");
                }
            }

            @Override // defpackage.j71
            public Object a(k71 k71Var, q70 q70Var) {
                Object a = j71.this.a(new AnonymousClass2(k71Var, this), q70Var);
                return a == gs1.d() ? a : te4.a;
            }
        }, new CachedPagingDataKt$cachedIn$multicastedFlow$2(null));
        return new Multicaster(c90Var, 1, n71.s(n71.u(new j71<fp2<T>>() { // from class: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2

            /* compiled from: Collect.kt */
            /* renamed from: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2$2  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass2 implements k71<MulticastedPagingData<T>> {
                public final /* synthetic */ k71 a;

                @a(c = "androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2$2", f = "CachedPagingData.kt", l = {135}, m = "emit")
                /* renamed from: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2$2$1  reason: invalid class name */
                /* loaded from: classes.dex */
                public static final class AnonymousClass1 extends ContinuationImpl {
                    public Object L$0;
                    public int label;
                    public /* synthetic */ Object result;

                    public AnonymousClass1(q70 q70Var) {
                        super(q70Var);
                    }

                    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                    public final Object invokeSuspend(Object obj) {
                        this.result = obj;
                        this.label |= Integer.MIN_VALUE;
                        return AnonymousClass2.this.emit(null, this);
                    }
                }

                public AnonymousClass2(k71 k71Var, CachedPagingDataKt$cachedIn$$inlined$map$2 cachedPagingDataKt$cachedIn$$inlined$map$2) {
                    this.a = k71Var;
                }

                /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
                /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
                @Override // defpackage.k71
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public java.lang.Object emit(java.lang.Object r5, defpackage.q70 r6) {
                    /*
                        r4 = this;
                        boolean r0 = r6 instanceof androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2.AnonymousClass2.AnonymousClass1
                        if (r0 == 0) goto L13
                        r0 = r6
                        androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2$2$1 r0 = (androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2.AnonymousClass2.AnonymousClass1) r0
                        int r1 = r0.label
                        r2 = -2147483648(0xffffffff80000000, float:-0.0)
                        r3 = r1 & r2
                        if (r3 == 0) goto L13
                        int r1 = r1 - r2
                        r0.label = r1
                        goto L18
                    L13:
                        androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2$2$1 r0 = new androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2$2$1
                        r0.<init>(r6)
                    L18:
                        java.lang.Object r6 = r0.result
                        java.lang.Object r1 = defpackage.gs1.d()
                        int r2 = r0.label
                        r3 = 1
                        if (r2 == 0) goto L31
                        if (r2 != r3) goto L29
                        defpackage.o83.b(r6)
                        goto L45
                    L29:
                        java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                        java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
                        r5.<init>(r6)
                        throw r5
                    L31:
                        defpackage.o83.b(r6)
                        k71 r6 = r4.a
                        androidx.paging.MulticastedPagingData r5 = (androidx.paging.MulticastedPagingData) r5
                        fp2 r5 = r5.a()
                        r0.label = r3
                        java.lang.Object r5 = r6.emit(r5, r0)
                        if (r5 != r1) goto L45
                        return r1
                    L45:
                        te4 r5 = defpackage.te4.a
                        return r5
                    */
                    throw new UnsupportedOperationException("Method not decompiled: androidx.paging.CachedPagingDataKt$cachedIn$$inlined$map$2.AnonymousClass2.emit(java.lang.Object, q70):java.lang.Object");
                }
            }

            @Override // defpackage.j71
            public Object a(k71 k71Var, q70 q70Var) {
                Object a = j71.this.a(new AnonymousClass2(k71Var, this), q70Var);
                return a == gs1.d() ? a : te4.a;
            }
        }, new CachedPagingDataKt$cachedIn$multicastedFlow$4(activeFlowTracker, null)), new CachedPagingDataKt$cachedIn$multicastedFlow$5(activeFlowTracker, null)), false, new CachedPagingDataKt$cachedIn$1(null), true, 8, null).i();
    }
}
