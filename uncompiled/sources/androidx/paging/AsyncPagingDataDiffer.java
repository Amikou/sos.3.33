package androidx.paging;

import androidx.recyclerview.widget.g;
import java.util.concurrent.atomic.AtomicInteger;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: AsyncPagingDataDiffer.kt */
/* loaded from: classes.dex */
public final class AsyncPagingDataDiffer<T> {
    public final po0 a;
    public boolean b;
    public final AsyncPagingDataDiffer$differBase$1 c;
    public final AtomicInteger d;
    public final j71<a30> e;
    public final g.f<T> f;
    public final i02 g;
    public final CoroutineDispatcher h;
    public final CoroutineDispatcher i;

    /* compiled from: AsyncPagingDataDiffer.kt */
    /* loaded from: classes.dex */
    public static final class a implements po0 {
        public a() {
        }

        @Override // defpackage.po0
        public void a(int i, int i2) {
            if (i2 > 0) {
                AsyncPagingDataDiffer.this.g.onChanged(i, i2, null);
            }
        }

        @Override // defpackage.po0
        public void onInserted(int i, int i2) {
            if (i2 > 0) {
                AsyncPagingDataDiffer.this.g.onInserted(i, i2);
            }
        }

        @Override // defpackage.po0
        public void onRemoved(int i, int i2) {
            if (i2 > 0) {
                AsyncPagingDataDiffer.this.g.onRemoved(i, i2);
            }
        }
    }

    public AsyncPagingDataDiffer(g.f<T> fVar, i02 i02Var, CoroutineDispatcher coroutineDispatcher, CoroutineDispatcher coroutineDispatcher2) {
        fs1.f(fVar, "diffCallback");
        fs1.f(i02Var, "updateCallback");
        fs1.f(coroutineDispatcher, "mainDispatcher");
        fs1.f(coroutineDispatcher2, "workerDispatcher");
        this.f = fVar;
        this.g = i02Var;
        this.h = coroutineDispatcher;
        this.i = coroutineDispatcher2;
        a aVar = new a();
        this.a = aVar;
        AsyncPagingDataDiffer$differBase$1 asyncPagingDataDiffer$differBase$1 = new AsyncPagingDataDiffer$differBase$1(this, aVar, coroutineDispatcher);
        this.c = asyncPagingDataDiffer$differBase$1;
        this.d = new AtomicInteger(0);
        this.e = asyncPagingDataDiffer$differBase$1.t();
    }

    public final void d(tc1<? super a30, te4> tc1Var) {
        fs1.f(tc1Var, "listener");
        this.c.p(tc1Var);
    }

    public final po0 e() {
        return this.a;
    }

    public final boolean f() {
        return this.b;
    }

    public final T g(int i) {
        try {
            this.b = true;
            return this.c.s(i);
        } finally {
            this.b = false;
        }
    }

    public final int h() {
        return this.c.u();
    }

    public final j71<a30> i() {
        return this.e;
    }

    public final void j() {
        this.c.x();
    }

    public final void k(tc1<? super a30, te4> tc1Var) {
        fs1.f(tc1Var, "listener");
        this.c.y(tc1Var);
    }

    public final Object l(fp2<T> fp2Var, q70<? super te4> q70Var) {
        this.d.incrementAndGet();
        Object q = this.c.q(fp2Var, q70Var);
        return q == gs1.d() ? q : te4.a;
    }
}
