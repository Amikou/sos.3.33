package androidx.core.content;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import defpackage.ym1;

/* loaded from: classes.dex */
public abstract class UnusedAppRestrictionsBackportService extends Service {
    public ym1.a a = new a();

    /* loaded from: classes.dex */
    public class a extends ym1.a {
        public a() {
        }

        @Override // defpackage.ym1
        public void B(xm1 xm1Var) throws RemoteException {
            if (xm1Var == null) {
                return;
            }
            UnusedAppRestrictionsBackportService.this.a(new ff4(xm1Var));
        }
    }

    public abstract void a(ff4 ff4Var);

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.a;
    }
}
