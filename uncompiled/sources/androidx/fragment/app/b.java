package androidx.fragment.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import androidx.fragment.app.SpecialEffectsController;
import androidx.fragment.app.c;
import defpackage.tv;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: DefaultSpecialEffectsController.java */
/* loaded from: classes.dex */
public class b extends SpecialEffectsController {

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[SpecialEffectsController.Operation.State.values().length];
            a = iArr;
            try {
                iArr[SpecialEffectsController.Operation.State.GONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[SpecialEffectsController.Operation.State.INVISIBLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[SpecialEffectsController.Operation.State.REMOVED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[SpecialEffectsController.Operation.State.VISIBLE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* renamed from: androidx.fragment.app.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class RunnableC0025b implements Runnable {
        public final /* synthetic */ List a;
        public final /* synthetic */ SpecialEffectsController.Operation f0;

        public RunnableC0025b(List list, SpecialEffectsController.Operation operation) {
            this.a = list;
            this.f0 = operation;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a.contains(this.f0)) {
                this.a.remove(this.f0);
                b.this.s(this.f0);
            }
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class c extends AnimatorListenerAdapter {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ boolean g0;
        public final /* synthetic */ SpecialEffectsController.Operation h0;
        public final /* synthetic */ k i0;

        public c(b bVar, ViewGroup viewGroup, View view, boolean z, SpecialEffectsController.Operation operation, k kVar) {
            this.a = viewGroup;
            this.f0 = view;
            this.g0 = z;
            this.h0 = operation;
            this.i0 = kVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.a.endViewTransition(this.f0);
            if (this.g0) {
                this.h0.e().applyState(this.f0);
            }
            this.i0.a();
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class d implements tv.a {
        public final /* synthetic */ Animator a;

        public d(b bVar, Animator animator) {
            this.a = animator;
        }

        @Override // defpackage.tv.a
        public void a() {
            this.a.end();
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class e implements Animation.AnimationListener {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ View b;
        public final /* synthetic */ k c;

        /* compiled from: DefaultSpecialEffectsController.java */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                e eVar = e.this;
                eVar.a.endViewTransition(eVar.b);
                e.this.c.a();
            }
        }

        public e(b bVar, ViewGroup viewGroup, View view, k kVar) {
            this.a = viewGroup;
            this.b = view;
            this.c = kVar;
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            this.a.post(new a());
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationRepeat(Animation animation) {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationStart(Animation animation) {
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class f implements tv.a {
        public final /* synthetic */ View a;
        public final /* synthetic */ ViewGroup b;
        public final /* synthetic */ k c;

        public f(b bVar, View view, ViewGroup viewGroup, k kVar) {
            this.a = view;
            this.b = viewGroup;
            this.c = kVar;
        }

        @Override // defpackage.tv.a
        public void a() {
            this.a.clearAnimation();
            this.b.endViewTransition(this.a);
            this.c.a();
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class g implements Runnable {
        public final /* synthetic */ SpecialEffectsController.Operation a;
        public final /* synthetic */ SpecialEffectsController.Operation f0;
        public final /* synthetic */ boolean g0;
        public final /* synthetic */ rh h0;

        public g(b bVar, SpecialEffectsController.Operation operation, SpecialEffectsController.Operation operation2, boolean z, rh rhVar) {
            this.a = operation;
            this.f0 = operation2;
            this.g0 = z;
            this.h0 = rhVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            androidx.fragment.app.k.f(this.a.f(), this.f0.f(), this.g0, this.h0, false);
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class h implements Runnable {
        public final /* synthetic */ mb1 a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ Rect g0;

        public h(b bVar, mb1 mb1Var, View view, Rect rect) {
            this.a = mb1Var;
            this.f0 = view;
            this.g0 = rect;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.k(this.f0, this.g0);
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class i implements Runnable {
        public final /* synthetic */ ArrayList a;

        public i(b bVar, ArrayList arrayList) {
            this.a = arrayList;
        }

        @Override // java.lang.Runnable
        public void run() {
            androidx.fragment.app.k.A(this.a, 4);
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public class j implements Runnable {
        public final /* synthetic */ m a;

        public j(b bVar, m mVar) {
            this.a = mVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.a();
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public static class k extends l {
        public boolean c;
        public boolean d;
        public c.d e;

        public k(SpecialEffectsController.Operation operation, tv tvVar, boolean z) {
            super(operation, tvVar);
            this.d = false;
            this.c = z;
        }

        public c.d e(Context context) {
            if (this.d) {
                return this.e;
            }
            c.d c = androidx.fragment.app.c.c(context, b().f(), b().e() == SpecialEffectsController.Operation.State.VISIBLE, this.c);
            this.e = c;
            this.d = true;
            return c;
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public static class l {
        public final SpecialEffectsController.Operation a;
        public final tv b;

        public l(SpecialEffectsController.Operation operation, tv tvVar) {
            this.a = operation;
            this.b = tvVar;
        }

        public void a() {
            this.a.d(this.b);
        }

        public SpecialEffectsController.Operation b() {
            return this.a;
        }

        public tv c() {
            return this.b;
        }

        public boolean d() {
            SpecialEffectsController.Operation.State state;
            SpecialEffectsController.Operation.State from = SpecialEffectsController.Operation.State.from(this.a.f().mView);
            SpecialEffectsController.Operation.State e = this.a.e();
            return from == e || !(from == (state = SpecialEffectsController.Operation.State.VISIBLE) || e == state);
        }
    }

    /* compiled from: DefaultSpecialEffectsController.java */
    /* loaded from: classes.dex */
    public static class m extends l {
        public final Object c;
        public final boolean d;
        public final Object e;

        public m(SpecialEffectsController.Operation operation, tv tvVar, boolean z, boolean z2) {
            super(operation, tvVar);
            Object exitTransition;
            Object enterTransition;
            boolean allowEnterTransitionOverlap;
            if (operation.e() == SpecialEffectsController.Operation.State.VISIBLE) {
                if (z) {
                    enterTransition = operation.f().getReenterTransition();
                } else {
                    enterTransition = operation.f().getEnterTransition();
                }
                this.c = enterTransition;
                if (z) {
                    allowEnterTransitionOverlap = operation.f().getAllowReturnTransitionOverlap();
                } else {
                    allowEnterTransitionOverlap = operation.f().getAllowEnterTransitionOverlap();
                }
                this.d = allowEnterTransitionOverlap;
            } else {
                if (z) {
                    exitTransition = operation.f().getReturnTransition();
                } else {
                    exitTransition = operation.f().getExitTransition();
                }
                this.c = exitTransition;
                this.d = true;
            }
            if (!z2) {
                this.e = null;
            } else if (z) {
                this.e = operation.f().getSharedElementReturnTransition();
            } else {
                this.e = operation.f().getSharedElementEnterTransition();
            }
        }

        public mb1 e() {
            mb1 f = f(this.c);
            mb1 f2 = f(this.e);
            if (f == null || f2 == null || f == f2) {
                return f != null ? f : f2;
            }
            throw new IllegalArgumentException("Mixing framework transitions and AndroidX transitions is not allowed. Fragment " + b().f() + " returned Transition " + this.c + " which uses a different Transition  type than its shared element transition " + this.e);
        }

        public final mb1 f(Object obj) {
            if (obj == null) {
                return null;
            }
            mb1 mb1Var = androidx.fragment.app.k.b;
            if (mb1Var == null || !mb1Var.e(obj)) {
                mb1 mb1Var2 = androidx.fragment.app.k.c;
                if (mb1Var2 == null || !mb1Var2.e(obj)) {
                    throw new IllegalArgumentException("Transition " + obj + " for fragment " + b().f() + " is not a valid framework Transition or AndroidX Transition");
                }
                return mb1Var2;
            }
            return mb1Var;
        }

        public Object g() {
            return this.e;
        }

        public Object h() {
            return this.c;
        }

        public boolean i() {
            return this.e != null;
        }

        public boolean j() {
            return this.d;
        }
    }

    public b(ViewGroup viewGroup) {
        super(viewGroup);
    }

    @Override // androidx.fragment.app.SpecialEffectsController
    public void f(List<SpecialEffectsController.Operation> list, boolean z) {
        SpecialEffectsController.Operation operation = null;
        SpecialEffectsController.Operation operation2 = null;
        for (SpecialEffectsController.Operation operation3 : list) {
            SpecialEffectsController.Operation.State from = SpecialEffectsController.Operation.State.from(operation3.f().mView);
            int i2 = a.a[operation3.e().ordinal()];
            if (i2 != 1 && i2 != 2 && i2 != 3) {
                if (i2 == 4 && from != SpecialEffectsController.Operation.State.VISIBLE) {
                    operation2 = operation3;
                }
            } else if (from == SpecialEffectsController.Operation.State.VISIBLE && operation == null) {
                operation = operation3;
            }
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList<SpecialEffectsController.Operation> arrayList3 = new ArrayList(list);
        for (SpecialEffectsController.Operation operation4 : list) {
            tv tvVar = new tv();
            operation4.j(tvVar);
            arrayList.add(new k(operation4, tvVar, z));
            tv tvVar2 = new tv();
            operation4.j(tvVar2);
            boolean z2 = false;
            if (z) {
                if (operation4 != operation) {
                    arrayList2.add(new m(operation4, tvVar2, z, z2));
                    operation4.a(new RunnableC0025b(arrayList3, operation4));
                }
                z2 = true;
                arrayList2.add(new m(operation4, tvVar2, z, z2));
                operation4.a(new RunnableC0025b(arrayList3, operation4));
            } else {
                if (operation4 != operation2) {
                    arrayList2.add(new m(operation4, tvVar2, z, z2));
                    operation4.a(new RunnableC0025b(arrayList3, operation4));
                }
                z2 = true;
                arrayList2.add(new m(operation4, tvVar2, z, z2));
                operation4.a(new RunnableC0025b(arrayList3, operation4));
            }
        }
        Map<SpecialEffectsController.Operation, Boolean> x = x(arrayList2, arrayList3, z, operation, operation2);
        w(arrayList, arrayList3, x.containsValue(Boolean.TRUE), x);
        for (SpecialEffectsController.Operation operation5 : arrayList3) {
            s(operation5);
        }
        arrayList3.clear();
    }

    public void s(SpecialEffectsController.Operation operation) {
        operation.e().applyState(operation.f().mView);
    }

    public void t(ArrayList<View> arrayList, View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (ki4.a(viewGroup)) {
                if (arrayList.contains(view)) {
                    return;
                }
                arrayList.add(viewGroup);
                return;
            }
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                if (childAt.getVisibility() == 0) {
                    t(arrayList, childAt);
                }
            }
        } else if (!arrayList.contains(view)) {
            arrayList.add(view);
        }
    }

    public void u(Map<String, View> map, View view) {
        String N = ei4.N(view);
        if (N != null) {
            map.put(N, view);
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                if (childAt.getVisibility() == 0) {
                    u(map, childAt);
                }
            }
        }
    }

    public void v(rh<String, View> rhVar, Collection<String> collection) {
        Iterator<Map.Entry<String, View>> it = rhVar.entrySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(ei4.N(it.next().getValue()))) {
                it.remove();
            }
        }
    }

    public final void w(List<k> list, List<SpecialEffectsController.Operation> list2, boolean z, Map<SpecialEffectsController.Operation, Boolean> map) {
        ViewGroup m2 = m();
        Context context = m2.getContext();
        ArrayList arrayList = new ArrayList();
        boolean z2 = false;
        for (k kVar : list) {
            if (kVar.d()) {
                kVar.a();
            } else {
                c.d e2 = kVar.e(context);
                if (e2 == null) {
                    kVar.a();
                } else {
                    Animator animator = e2.b;
                    if (animator == null) {
                        arrayList.add(kVar);
                    } else {
                        SpecialEffectsController.Operation b = kVar.b();
                        Fragment f2 = b.f();
                        if (Boolean.TRUE.equals(map.get(b))) {
                            if (FragmentManager.H0(2)) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Ignoring Animator set on ");
                                sb.append(f2);
                                sb.append(" as this Fragment was involved in a Transition.");
                            }
                            kVar.a();
                        } else {
                            boolean z3 = b.e() == SpecialEffectsController.Operation.State.GONE;
                            if (z3) {
                                list2.remove(b);
                            }
                            View view = f2.mView;
                            m2.startViewTransition(view);
                            animator.addListener(new c(this, m2, view, z3, b, kVar));
                            animator.setTarget(view);
                            animator.start();
                            kVar.c().d(new d(this, animator));
                            z2 = true;
                        }
                    }
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            k kVar2 = (k) it.next();
            SpecialEffectsController.Operation b2 = kVar2.b();
            Fragment f3 = b2.f();
            if (z) {
                if (FragmentManager.H0(2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Ignoring Animation set on ");
                    sb2.append(f3);
                    sb2.append(" as Animations cannot run alongside Transitions.");
                }
                kVar2.a();
            } else if (z2) {
                if (FragmentManager.H0(2)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Ignoring Animation set on ");
                    sb3.append(f3);
                    sb3.append(" as Animations cannot run alongside Animators.");
                }
                kVar2.a();
            } else {
                View view2 = f3.mView;
                Animation animation = (Animation) du2.e(((c.d) du2.e(kVar2.e(context))).a);
                if (b2.e() != SpecialEffectsController.Operation.State.REMOVED) {
                    view2.startAnimation(animation);
                    kVar2.a();
                } else {
                    m2.startViewTransition(view2);
                    c.e eVar = new c.e(animation, m2, view2);
                    eVar.setAnimationListener(new e(this, m2, view2, kVar2));
                    view2.startAnimation(eVar);
                }
                kVar2.c().d(new f(this, view2, m2, kVar2));
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final Map<SpecialEffectsController.Operation, Boolean> x(List<m> list, List<SpecialEffectsController.Operation> list2, boolean z, SpecialEffectsController.Operation operation, SpecialEffectsController.Operation operation2) {
        View view;
        Object obj;
        ArrayList<View> arrayList;
        Object obj2;
        ArrayList<View> arrayList2;
        SpecialEffectsController.Operation operation3;
        SpecialEffectsController.Operation operation4;
        View view2;
        Object n;
        rh rhVar;
        ArrayList<View> arrayList3;
        SpecialEffectsController.Operation operation5;
        ArrayList<View> arrayList4;
        Rect rect;
        View view3;
        mb1 mb1Var;
        SpecialEffectsController.Operation operation6;
        View view4;
        boolean z2 = z;
        SpecialEffectsController.Operation operation7 = operation;
        SpecialEffectsController.Operation operation8 = operation2;
        HashMap hashMap = new HashMap();
        mb1 mb1Var2 = null;
        for (m mVar : list) {
            if (!mVar.d()) {
                mb1 e2 = mVar.e();
                if (mb1Var2 == null) {
                    mb1Var2 = e2;
                } else if (e2 != null && mb1Var2 != e2) {
                    throw new IllegalArgumentException("Mixing framework transitions and AndroidX transitions is not allowed. Fragment " + mVar.b().f() + " returned Transition " + mVar.h() + " which uses a different Transition  type than other Fragments.");
                }
            }
        }
        if (mb1Var2 == null) {
            for (m mVar2 : list) {
                hashMap.put(mVar2.b(), Boolean.FALSE);
                mVar2.a();
            }
            return hashMap;
        }
        View view5 = new View(m().getContext());
        Rect rect2 = new Rect();
        ArrayList<View> arrayList5 = new ArrayList<>();
        ArrayList<View> arrayList6 = new ArrayList<>();
        rh rhVar2 = new rh();
        Object obj3 = null;
        View view6 = null;
        boolean z3 = false;
        for (m mVar3 : list) {
            if (!mVar3.i() || operation7 == null || operation8 == null) {
                rhVar = rhVar2;
                arrayList3 = arrayList6;
                operation5 = operation7;
                arrayList4 = arrayList5;
                rect = rect2;
                view3 = view5;
                mb1Var = mb1Var2;
                operation6 = operation8;
                view6 = view6;
            } else {
                Object B = mb1Var2.B(mb1Var2.g(mVar3.g()));
                ArrayList<String> sharedElementSourceNames = operation2.f().getSharedElementSourceNames();
                ArrayList<String> sharedElementSourceNames2 = operation.f().getSharedElementSourceNames();
                ArrayList<String> sharedElementTargetNames = operation.f().getSharedElementTargetNames();
                View view7 = view6;
                int i2 = 0;
                while (i2 < sharedElementTargetNames.size()) {
                    int indexOf = sharedElementSourceNames.indexOf(sharedElementTargetNames.get(i2));
                    ArrayList<String> arrayList7 = sharedElementTargetNames;
                    if (indexOf != -1) {
                        sharedElementSourceNames.set(indexOf, sharedElementSourceNames2.get(i2));
                    }
                    i2++;
                    sharedElementTargetNames = arrayList7;
                }
                ArrayList<String> sharedElementTargetNames2 = operation2.f().getSharedElementTargetNames();
                if (!z2) {
                    operation.f().getExitTransitionCallback();
                    operation2.f().getEnterTransitionCallback();
                } else {
                    operation.f().getEnterTransitionCallback();
                    operation2.f().getExitTransitionCallback();
                }
                int i3 = 0;
                for (int size = sharedElementSourceNames.size(); i3 < size; size = size) {
                    rhVar2.put(sharedElementSourceNames.get(i3), sharedElementTargetNames2.get(i3));
                    i3++;
                }
                rh<String, View> rhVar3 = new rh<>();
                u(rhVar3, operation.f().mView);
                rhVar3.o(sharedElementSourceNames);
                rhVar2.o(rhVar3.keySet());
                rh<String, View> rhVar4 = new rh<>();
                u(rhVar4, operation2.f().mView);
                rhVar4.o(sharedElementTargetNames2);
                rhVar4.o(rhVar2.values());
                androidx.fragment.app.k.x(rhVar2, rhVar4);
                v(rhVar3, rhVar2.keySet());
                v(rhVar4, rhVar2.values());
                if (rhVar2.isEmpty()) {
                    arrayList5.clear();
                    arrayList6.clear();
                    rhVar = rhVar2;
                    arrayList3 = arrayList6;
                    operation5 = operation7;
                    arrayList4 = arrayList5;
                    rect = rect2;
                    view3 = view5;
                    mb1Var = mb1Var2;
                    view6 = view7;
                    obj3 = null;
                    operation6 = operation8;
                } else {
                    androidx.fragment.app.k.f(operation2.f(), operation.f(), z2, rhVar3, true);
                    rhVar = rhVar2;
                    ArrayList<View> arrayList8 = arrayList6;
                    vm2.a(m(), new g(this, operation2, operation, z, rhVar4));
                    arrayList5.addAll(rhVar3.values());
                    if (sharedElementSourceNames.isEmpty()) {
                        view6 = view7;
                    } else {
                        View view8 = rhVar3.get(sharedElementSourceNames.get(0));
                        mb1Var2.v(B, view8);
                        view6 = view8;
                    }
                    arrayList3 = arrayList8;
                    arrayList3.addAll(rhVar4.values());
                    if (!sharedElementTargetNames2.isEmpty() && (view4 = rhVar4.get(sharedElementTargetNames2.get(0))) != null) {
                        vm2.a(m(), new h(this, mb1Var2, view4, rect2));
                        z3 = true;
                    }
                    mb1Var2.z(B, view5, arrayList5);
                    arrayList4 = arrayList5;
                    rect = rect2;
                    view3 = view5;
                    mb1Var = mb1Var2;
                    mb1Var2.t(B, null, null, null, null, B, arrayList3);
                    Boolean bool = Boolean.TRUE;
                    operation5 = operation;
                    hashMap.put(operation5, bool);
                    operation6 = operation2;
                    hashMap.put(operation6, bool);
                    obj3 = B;
                }
            }
            operation7 = operation5;
            arrayList5 = arrayList4;
            rect2 = rect;
            view5 = view3;
            operation8 = operation6;
            rhVar2 = rhVar;
            z2 = z;
            arrayList6 = arrayList3;
            mb1Var2 = mb1Var;
        }
        View view9 = view6;
        rh rhVar5 = rhVar2;
        Collection<?> collection = arrayList6;
        SpecialEffectsController.Operation operation9 = operation7;
        Collection<?> collection2 = arrayList5;
        Rect rect3 = rect2;
        View view10 = view5;
        mb1 mb1Var3 = mb1Var2;
        boolean z4 = false;
        SpecialEffectsController.Operation operation10 = operation8;
        ArrayList arrayList9 = new ArrayList();
        Object obj4 = null;
        Object obj5 = null;
        for (m mVar4 : list) {
            if (mVar4.d()) {
                hashMap.put(mVar4.b(), Boolean.FALSE);
                mVar4.a();
            } else {
                Object g2 = mb1Var3.g(mVar4.h());
                SpecialEffectsController.Operation b = mVar4.b();
                boolean z5 = (obj3 == null || !(b == operation9 || b == operation10)) ? z4 : true;
                if (g2 == null) {
                    if (!z5) {
                        hashMap.put(b, Boolean.FALSE);
                        mVar4.a();
                    }
                    arrayList2 = collection;
                    arrayList = collection2;
                    view = view10;
                    n = obj4;
                    operation3 = operation10;
                    view2 = view9;
                } else {
                    ArrayList<View> arrayList10 = new ArrayList<>();
                    Object obj6 = obj4;
                    t(arrayList10, b.f().mView);
                    if (z5) {
                        if (b == operation9) {
                            arrayList10.removeAll(collection2);
                        } else {
                            arrayList10.removeAll(collection);
                        }
                    }
                    if (arrayList10.isEmpty()) {
                        mb1Var3.a(g2, view10);
                        arrayList2 = collection;
                        arrayList = collection2;
                        view = view10;
                        operation4 = b;
                        obj2 = obj5;
                        operation3 = operation10;
                        obj = obj6;
                    } else {
                        mb1Var3.b(g2, arrayList10);
                        view = view10;
                        obj = obj6;
                        arrayList = collection2;
                        obj2 = obj5;
                        arrayList2 = collection;
                        operation3 = operation10;
                        mb1Var3.t(g2, g2, arrayList10, null, null, null, null);
                        if (b.e() == SpecialEffectsController.Operation.State.GONE) {
                            operation4 = b;
                            list2.remove(operation4);
                            ArrayList<View> arrayList11 = new ArrayList<>(arrayList10);
                            arrayList11.remove(operation4.f().mView);
                            mb1Var3.r(g2, operation4.f().mView, arrayList11);
                            vm2.a(m(), new i(this, arrayList10));
                        } else {
                            operation4 = b;
                        }
                    }
                    if (operation4.e() == SpecialEffectsController.Operation.State.VISIBLE) {
                        arrayList9.addAll(arrayList10);
                        if (z3) {
                            mb1Var3.u(g2, rect3);
                        }
                        view2 = view9;
                    } else {
                        view2 = view9;
                        mb1Var3.v(g2, view2);
                    }
                    hashMap.put(operation4, Boolean.TRUE);
                    if (mVar4.j()) {
                        obj5 = mb1Var3.n(obj2, g2, null);
                        n = obj;
                    } else {
                        n = mb1Var3.n(obj, g2, null);
                        obj5 = obj2;
                    }
                }
                operation10 = operation3;
                obj4 = n;
                view9 = view2;
                view10 = view;
                collection2 = arrayList;
                collection = arrayList2;
                z4 = false;
            }
        }
        ArrayList<View> arrayList12 = collection;
        ArrayList<View> arrayList13 = collection2;
        SpecialEffectsController.Operation operation11 = operation10;
        Object m2 = mb1Var3.m(obj5, obj4, obj3);
        for (m mVar5 : list) {
            if (!mVar5.d()) {
                Object h2 = mVar5.h();
                SpecialEffectsController.Operation b2 = mVar5.b();
                boolean z6 = obj3 != null && (b2 == operation9 || b2 == operation11);
                if (h2 != null || z6) {
                    if (!ei4.W(m())) {
                        if (FragmentManager.H0(2)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("SpecialEffectsController: Container ");
                            sb.append(m());
                            sb.append(" has not been laid out. Completing operation ");
                            sb.append(b2);
                        }
                        mVar5.a();
                    } else {
                        mb1Var3.w(mVar5.b().f(), m2, mVar5.c(), new j(this, mVar5));
                    }
                }
            }
        }
        if (ei4.W(m())) {
            androidx.fragment.app.k.A(arrayList9, 4);
            ArrayList<String> o = mb1Var3.o(arrayList12);
            mb1Var3.c(m(), m2);
            mb1Var3.y(m(), arrayList13, arrayList12, o, rhVar5);
            androidx.fragment.app.k.A(arrayList9, 0);
            mb1Var3.A(obj3, arrayList13, arrayList12);
            return hashMap;
        }
        return hashMap;
    }
}
