package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* JADX INFO: Access modifiers changed from: package-private */
@SuppressLint({"BanParcelableUsage"})
/* loaded from: classes.dex */
public final class FragmentState implements Parcelable {
    public static final Parcelable.Creator<FragmentState> CREATOR = new a();
    public final String a;
    public final String f0;
    public final boolean g0;
    public final int h0;
    public final int i0;
    public final String j0;
    public final boolean k0;
    public final boolean l0;
    public final boolean m0;
    public final Bundle n0;
    public final boolean o0;
    public final int p0;
    public Bundle q0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<FragmentState> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public FragmentState createFromParcel(Parcel parcel) {
            return new FragmentState(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public FragmentState[] newArray(int i) {
            return new FragmentState[i];
        }
    }

    public FragmentState(Fragment fragment) {
        this.a = fragment.getClass().getName();
        this.f0 = fragment.mWho;
        this.g0 = fragment.mFromLayout;
        this.h0 = fragment.mFragmentId;
        this.i0 = fragment.mContainerId;
        this.j0 = fragment.mTag;
        this.k0 = fragment.mRetainInstance;
        this.l0 = fragment.mRemoving;
        this.m0 = fragment.mDetached;
        this.n0 = fragment.mArguments;
        this.o0 = fragment.mHidden;
        this.p0 = fragment.mMaxState.ordinal();
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentState{");
        sb.append(this.a);
        sb.append(" (");
        sb.append(this.f0);
        sb.append(")}:");
        if (this.g0) {
            sb.append(" fromLayout");
        }
        if (this.i0 != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.i0));
        }
        String str = this.j0;
        if (str != null && !str.isEmpty()) {
            sb.append(" tag=");
            sb.append(this.j0);
        }
        if (this.k0) {
            sb.append(" retainInstance");
        }
        if (this.l0) {
            sb.append(" removing");
        }
        if (this.m0) {
            sb.append(" detached");
        }
        if (this.o0) {
            sb.append(" hidden");
        }
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.f0);
        parcel.writeInt(this.g0 ? 1 : 0);
        parcel.writeInt(this.h0);
        parcel.writeInt(this.i0);
        parcel.writeString(this.j0);
        parcel.writeInt(this.k0 ? 1 : 0);
        parcel.writeInt(this.l0 ? 1 : 0);
        parcel.writeInt(this.m0 ? 1 : 0);
        parcel.writeBundle(this.n0);
        parcel.writeInt(this.o0 ? 1 : 0);
        parcel.writeBundle(this.q0);
        parcel.writeInt(this.p0);
    }

    public FragmentState(Parcel parcel) {
        this.a = parcel.readString();
        this.f0 = parcel.readString();
        this.g0 = parcel.readInt() != 0;
        this.h0 = parcel.readInt();
        this.i0 = parcel.readInt();
        this.j0 = parcel.readString();
        this.k0 = parcel.readInt() != 0;
        this.l0 = parcel.readInt() != 0;
        this.m0 = parcel.readInt() != 0;
        this.n0 = parcel.readBundle();
        this.o0 = parcel.readInt() != 0;
        this.q0 = parcel.readBundle();
        this.p0 = parcel.readInt();
    }
}
