package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;

/* compiled from: FragmentHostCallback.java */
/* loaded from: classes.dex */
public abstract class e<E> extends x91 {
    public final Activity a;
    public final Context f0;
    public final Handler g0;
    public final FragmentManager h0;

    public e(FragmentActivity fragmentActivity) {
        this(fragmentActivity, fragmentActivity, new Handler(), 0);
    }

    @Override // defpackage.x91
    public View c(int i) {
        return null;
    }

    @Override // defpackage.x91
    public boolean d() {
        return true;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public Activity e() {
        return this.a;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public Context f() {
        return this.f0;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public Handler g() {
        return this.g0;
    }

    public abstract E h();

    public LayoutInflater i() {
        return LayoutInflater.from(this.f0);
    }

    @Deprecated
    public void j(Fragment fragment, String[] strArr, int i) {
    }

    public boolean k(Fragment fragment) {
        return true;
    }

    public boolean l(String str) {
        return false;
    }

    public void m(Fragment fragment, @SuppressLint({"UnknownNullness"}) Intent intent, int i, Bundle bundle) {
        if (i == -1) {
            m70.k(this.f0, intent, bundle);
            return;
        }
        throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
    }

    @Deprecated
    public void n(Fragment fragment, @SuppressLint({"UnknownNullness"}) IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) throws IntentSender.SendIntentException {
        if (i == -1) {
            androidx.core.app.a.w(this.a, intentSender, i, intent, i2, i3, i4, bundle);
            return;
        }
        throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
    }

    public void o() {
    }

    public e(Activity activity, Context context, Handler handler, int i) {
        this.h0 = new ma1();
        this.a = activity;
        this.f0 = (Context) du2.f(context, "context == null");
        this.g0 = (Handler) du2.f(handler, "handler == null");
    }
}
