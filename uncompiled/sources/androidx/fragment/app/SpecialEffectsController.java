package androidx.fragment.app;

import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;
import defpackage.tv;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes.dex */
public abstract class SpecialEffectsController {
    public final ViewGroup a;
    public final ArrayList<Operation> b = new ArrayList<>();
    public final ArrayList<Operation> c = new ArrayList<>();
    public boolean d = false;
    public boolean e = false;

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ d a;

        public a(d dVar) {
            this.a = dVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (SpecialEffectsController.this.b.contains(this.a)) {
                this.a.e().applyState(this.a.f().mView);
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ d a;

        public b(d dVar) {
            this.a = dVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            SpecialEffectsController.this.b.remove(this.a);
            SpecialEffectsController.this.c.remove(this.a);
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class c {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[Operation.LifecycleImpact.values().length];
            b = iArr;
            try {
                iArr[Operation.LifecycleImpact.ADDING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[Operation.LifecycleImpact.REMOVING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[Operation.LifecycleImpact.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[Operation.State.values().length];
            a = iArr2;
            try {
                iArr2[Operation.State.REMOVED.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[Operation.State.VISIBLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[Operation.State.GONE.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[Operation.State.INVISIBLE.ordinal()] = 4;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes.dex */
    public static class d extends Operation {
        public final h h;

        public d(Operation.State state, Operation.LifecycleImpact lifecycleImpact, h hVar, tv tvVar) {
            super(state, lifecycleImpact, hVar.k(), tvVar);
            this.h = hVar;
        }

        @Override // androidx.fragment.app.SpecialEffectsController.Operation
        public void c() {
            super.c();
            this.h.m();
        }

        @Override // androidx.fragment.app.SpecialEffectsController.Operation
        public void l() {
            if (g() == Operation.LifecycleImpact.ADDING) {
                Fragment k = this.h.k();
                View findFocus = k.mView.findFocus();
                if (findFocus != null) {
                    k.setFocusedView(findFocus);
                    if (FragmentManager.H0(2)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("requestFocus: Saved focused view ");
                        sb.append(findFocus);
                        sb.append(" for Fragment ");
                        sb.append(k);
                    }
                }
                View requireView = f().requireView();
                if (requireView.getParent() == null) {
                    this.h.b();
                    requireView.setAlpha(Utils.FLOAT_EPSILON);
                }
                if (requireView.getAlpha() == Utils.FLOAT_EPSILON && requireView.getVisibility() == 0) {
                    requireView.setVisibility(4);
                }
                requireView.setAlpha(k.getPostOnViewCreatedAlpha());
            }
        }
    }

    public SpecialEffectsController(ViewGroup viewGroup) {
        this.a = viewGroup;
    }

    public static SpecialEffectsController n(ViewGroup viewGroup, mr3 mr3Var) {
        int i = k03.special_effects_controller_view_tag;
        Object tag = viewGroup.getTag(i);
        if (tag instanceof SpecialEffectsController) {
            return (SpecialEffectsController) tag;
        }
        SpecialEffectsController a2 = mr3Var.a(viewGroup);
        viewGroup.setTag(i, a2);
        return a2;
    }

    public static SpecialEffectsController o(ViewGroup viewGroup, FragmentManager fragmentManager) {
        return n(viewGroup, fragmentManager.A0());
    }

    public final void a(Operation.State state, Operation.LifecycleImpact lifecycleImpact, h hVar) {
        synchronized (this.b) {
            tv tvVar = new tv();
            Operation h = h(hVar.k());
            if (h != null) {
                h.k(state, lifecycleImpact);
                return;
            }
            d dVar = new d(state, lifecycleImpact, hVar, tvVar);
            this.b.add(dVar);
            dVar.a(new a(dVar));
            dVar.a(new b(dVar));
        }
    }

    public void b(Operation.State state, h hVar) {
        if (FragmentManager.H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing add operation for fragment ");
            sb.append(hVar.k());
        }
        a(state, Operation.LifecycleImpact.ADDING, hVar);
    }

    public void c(h hVar) {
        if (FragmentManager.H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing hide operation for fragment ");
            sb.append(hVar.k());
        }
        a(Operation.State.GONE, Operation.LifecycleImpact.NONE, hVar);
    }

    public void d(h hVar) {
        if (FragmentManager.H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing remove operation for fragment ");
            sb.append(hVar.k());
        }
        a(Operation.State.REMOVED, Operation.LifecycleImpact.REMOVING, hVar);
    }

    public void e(h hVar) {
        if (FragmentManager.H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing show operation for fragment ");
            sb.append(hVar.k());
        }
        a(Operation.State.VISIBLE, Operation.LifecycleImpact.NONE, hVar);
    }

    public abstract void f(List<Operation> list, boolean z);

    public void g() {
        if (this.e) {
            return;
        }
        if (!ei4.V(this.a)) {
            j();
            this.d = false;
            return;
        }
        synchronized (this.b) {
            if (!this.b.isEmpty()) {
                ArrayList arrayList = new ArrayList(this.c);
                this.c.clear();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    Operation operation = (Operation) it.next();
                    if (FragmentManager.H0(2)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("SpecialEffectsController: Cancelling operation ");
                        sb.append(operation);
                    }
                    operation.b();
                    if (!operation.i()) {
                        this.c.add(operation);
                    }
                }
                q();
                ArrayList arrayList2 = new ArrayList(this.b);
                this.b.clear();
                this.c.addAll(arrayList2);
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    ((Operation) it2.next()).l();
                }
                f(arrayList2, this.d);
                this.d = false;
            }
        }
    }

    public final Operation h(Fragment fragment) {
        Iterator<Operation> it = this.b.iterator();
        while (it.hasNext()) {
            Operation next = it.next();
            if (next.f().equals(fragment) && !next.h()) {
                return next;
            }
        }
        return null;
    }

    public final Operation i(Fragment fragment) {
        Iterator<Operation> it = this.c.iterator();
        while (it.hasNext()) {
            Operation next = it.next();
            if (next.f().equals(fragment) && !next.h()) {
                return next;
            }
        }
        return null;
    }

    public void j() {
        String str;
        String str2;
        boolean V = ei4.V(this.a);
        synchronized (this.b) {
            q();
            Iterator<Operation> it = this.b.iterator();
            while (it.hasNext()) {
                it.next().l();
            }
            Iterator it2 = new ArrayList(this.c).iterator();
            while (it2.hasNext()) {
                Operation operation = (Operation) it2.next();
                if (FragmentManager.H0(2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("SpecialEffectsController: ");
                    if (V) {
                        str2 = "";
                    } else {
                        str2 = "Container " + this.a + " is not attached to window. ";
                    }
                    sb.append(str2);
                    sb.append("Cancelling running operation ");
                    sb.append(operation);
                }
                operation.b();
            }
            Iterator it3 = new ArrayList(this.b).iterator();
            while (it3.hasNext()) {
                Operation operation2 = (Operation) it3.next();
                if (FragmentManager.H0(2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("SpecialEffectsController: ");
                    if (V) {
                        str = "";
                    } else {
                        str = "Container " + this.a + " is not attached to window. ";
                    }
                    sb2.append(str);
                    sb2.append("Cancelling pending operation ");
                    sb2.append(operation2);
                }
                operation2.b();
            }
        }
    }

    public void k() {
        if (this.e) {
            this.e = false;
            g();
        }
    }

    public Operation.LifecycleImpact l(h hVar) {
        Operation h = h(hVar.k());
        Operation.LifecycleImpact g = h != null ? h.g() : null;
        Operation i = i(hVar.k());
        return (i == null || !(g == null || g == Operation.LifecycleImpact.NONE)) ? g : i.g();
    }

    public ViewGroup m() {
        return this.a;
    }

    public void p() {
        synchronized (this.b) {
            q();
            this.e = false;
            int size = this.b.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                Operation operation = this.b.get(size);
                Operation.State from = Operation.State.from(operation.f().mView);
                Operation.State e = operation.e();
                Operation.State state = Operation.State.VISIBLE;
                if (e == state && from != state) {
                    this.e = operation.f().isPostponed();
                    break;
                }
                size--;
            }
        }
    }

    public final void q() {
        Iterator<Operation> it = this.b.iterator();
        while (it.hasNext()) {
            Operation next = it.next();
            if (next.g() == Operation.LifecycleImpact.ADDING) {
                next.k(Operation.State.from(next.f().requireView().getVisibility()), Operation.LifecycleImpact.NONE);
            }
        }
    }

    public void r(boolean z) {
        this.d = z;
    }

    /* loaded from: classes.dex */
    public static class Operation {
        public State a;
        public LifecycleImpact b;
        public final Fragment c;
        public final List<Runnable> d = new ArrayList();
        public final HashSet<tv> e = new HashSet<>();
        public boolean f = false;
        public boolean g = false;

        /* loaded from: classes.dex */
        public enum LifecycleImpact {
            NONE,
            ADDING,
            REMOVING
        }

        /* loaded from: classes.dex */
        public class a implements tv.a {
            public a() {
            }

            @Override // defpackage.tv.a
            public void a() {
                Operation.this.b();
            }
        }

        public Operation(State state, LifecycleImpact lifecycleImpact, Fragment fragment, tv tvVar) {
            this.a = state;
            this.b = lifecycleImpact;
            this.c = fragment;
            tvVar.d(new a());
        }

        public final void a(Runnable runnable) {
            this.d.add(runnable);
        }

        public final void b() {
            if (h()) {
                return;
            }
            this.f = true;
            if (this.e.isEmpty()) {
                c();
                return;
            }
            Iterator it = new ArrayList(this.e).iterator();
            while (it.hasNext()) {
                ((tv) it.next()).a();
            }
        }

        public void c() {
            if (this.g) {
                return;
            }
            if (FragmentManager.H0(2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("SpecialEffectsController: ");
                sb.append(this);
                sb.append(" has called complete.");
            }
            this.g = true;
            for (Runnable runnable : this.d) {
                runnable.run();
            }
        }

        public final void d(tv tvVar) {
            if (this.e.remove(tvVar) && this.e.isEmpty()) {
                c();
            }
        }

        public State e() {
            return this.a;
        }

        public final Fragment f() {
            return this.c;
        }

        public LifecycleImpact g() {
            return this.b;
        }

        public final boolean h() {
            return this.f;
        }

        public final boolean i() {
            return this.g;
        }

        public final void j(tv tvVar) {
            l();
            this.e.add(tvVar);
        }

        public final void k(State state, LifecycleImpact lifecycleImpact) {
            int i = c.b[lifecycleImpact.ordinal()];
            if (i == 1) {
                if (this.a == State.REMOVED) {
                    if (FragmentManager.H0(2)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("SpecialEffectsController: For fragment ");
                        sb.append(this.c);
                        sb.append(" mFinalState = REMOVED -> VISIBLE. mLifecycleImpact = ");
                        sb.append(this.b);
                        sb.append(" to ADDING.");
                    }
                    this.a = State.VISIBLE;
                    this.b = LifecycleImpact.ADDING;
                }
            } else if (i != 2) {
                if (i == 3 && this.a != State.REMOVED) {
                    if (FragmentManager.H0(2)) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("SpecialEffectsController: For fragment ");
                        sb2.append(this.c);
                        sb2.append(" mFinalState = ");
                        sb2.append(this.a);
                        sb2.append(" -> ");
                        sb2.append(state);
                        sb2.append(". ");
                    }
                    this.a = state;
                }
            } else {
                if (FragmentManager.H0(2)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: For fragment ");
                    sb3.append(this.c);
                    sb3.append(" mFinalState = ");
                    sb3.append(this.a);
                    sb3.append(" -> REMOVED. mLifecycleImpact  = ");
                    sb3.append(this.b);
                    sb3.append(" to REMOVING.");
                }
                this.a = State.REMOVED;
                this.b = LifecycleImpact.REMOVING;
            }
        }

        public void l() {
        }

        public String toString() {
            return "Operation {" + Integer.toHexString(System.identityHashCode(this)) + "} {mFinalState = " + this.a + "} {mLifecycleImpact = " + this.b + "} {mFragment = " + this.c + "}";
        }

        /* loaded from: classes.dex */
        public enum State {
            REMOVED,
            VISIBLE,
            GONE,
            INVISIBLE;

            public static State from(View view) {
                if (view.getAlpha() == Utils.FLOAT_EPSILON && view.getVisibility() == 0) {
                    return INVISIBLE;
                }
                return from(view.getVisibility());
            }

            public void applyState(View view) {
                int i = c.a[ordinal()];
                if (i == 1) {
                    ViewGroup viewGroup = (ViewGroup) view.getParent();
                    if (viewGroup != null) {
                        if (FragmentManager.H0(2)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("SpecialEffectsController: Removing view ");
                            sb.append(view);
                            sb.append(" from container ");
                            sb.append(viewGroup);
                        }
                        viewGroup.removeView(view);
                    }
                } else if (i == 2) {
                    if (FragmentManager.H0(2)) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("SpecialEffectsController: Setting view ");
                        sb2.append(view);
                        sb2.append(" to VISIBLE");
                    }
                    view.setVisibility(0);
                } else if (i == 3) {
                    if (FragmentManager.H0(2)) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("SpecialEffectsController: Setting view ");
                        sb3.append(view);
                        sb3.append(" to GONE");
                    }
                    view.setVisibility(8);
                } else if (i != 4) {
                } else {
                    if (FragmentManager.H0(2)) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("SpecialEffectsController: Setting view ");
                        sb4.append(view);
                        sb4.append(" to INVISIBLE");
                    }
                    view.setVisibility(4);
                }
            }

            public static State from(int i) {
                if (i != 0) {
                    if (i != 4) {
                        if (i == 8) {
                            return GONE;
                        }
                        throw new IllegalArgumentException("Unknown visibility " + i);
                    }
                    return INVISIBLE;
                }
                return VISIBLE;
            }
        }
    }
}
