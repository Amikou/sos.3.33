package androidx.fragment.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: FragmentLayoutInflaterFactory.java */
/* loaded from: classes.dex */
public class f implements LayoutInflater.Factory2 {
    public final FragmentManager a;

    /* compiled from: FragmentLayoutInflaterFactory.java */
    /* loaded from: classes.dex */
    public class a implements View.OnAttachStateChangeListener {
        public final /* synthetic */ h a;

        public a(h hVar) {
            this.a = hVar;
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
            Fragment k = this.a.k();
            this.a.m();
            SpecialEffectsController.o((ViewGroup) k.mView.getParent(), f.this.a).j();
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
        }
    }

    public f(FragmentManager fragmentManager) {
        this.a = fragmentManager;
    }

    @Override // android.view.LayoutInflater.Factory
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    @Override // android.view.LayoutInflater.Factory2
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        h x;
        if (FragmentContainerView.class.getName().equals(str)) {
            return new FragmentContainerView(context, attributeSet, this.a);
        }
        if ("fragment".equals(str)) {
            String attributeValue = attributeSet.getAttributeValue(null, "class");
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a33.Fragment);
            if (attributeValue == null) {
                attributeValue = obtainStyledAttributes.getString(a33.Fragment_android_name);
            }
            int resourceId = obtainStyledAttributes.getResourceId(a33.Fragment_android_id, -1);
            String string = obtainStyledAttributes.getString(a33.Fragment_android_tag);
            obtainStyledAttributes.recycle();
            if (attributeValue == null || !d.b(context.getClassLoader(), attributeValue)) {
                return null;
            }
            int id = view != null ? view.getId() : 0;
            if (id == -1 && resourceId == -1 && string == null) {
                throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + attributeValue);
            }
            Fragment j0 = resourceId != -1 ? this.a.j0(resourceId) : null;
            if (j0 == null && string != null) {
                j0 = this.a.k0(string);
            }
            if (j0 == null && id != -1) {
                j0 = this.a.j0(id);
            }
            if (j0 == null) {
                j0 = this.a.s0().a(context.getClassLoader(), attributeValue);
                j0.mFromLayout = true;
                j0.mFragmentId = resourceId != 0 ? resourceId : id;
                j0.mContainerId = id;
                j0.mTag = string;
                j0.mInLayout = true;
                FragmentManager fragmentManager = this.a;
                j0.mFragmentManager = fragmentManager;
                j0.mHost = fragmentManager.v0();
                j0.onInflate(this.a.v0().f(), attributeSet, j0.mSavedFragmentState);
                x = this.a.g(j0);
                if (FragmentManager.H0(2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Fragment ");
                    sb.append(j0);
                    sb.append(" has been inflated via the <fragment> tag: id=0x");
                    sb.append(Integer.toHexString(resourceId));
                }
            } else if (!j0.mInLayout) {
                j0.mInLayout = true;
                FragmentManager fragmentManager2 = this.a;
                j0.mFragmentManager = fragmentManager2;
                j0.mHost = fragmentManager2.v0();
                j0.onInflate(this.a.v0().f(), attributeSet, j0.mSavedFragmentState);
                x = this.a.x(j0);
                if (FragmentManager.H0(2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Retained Fragment ");
                    sb2.append(j0);
                    sb2.append(" has been re-attached via the <fragment> tag: id=0x");
                    sb2.append(Integer.toHexString(resourceId));
                }
            } else {
                throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(id) + " with another fragment for " + attributeValue);
            }
            j0.mContainer = (ViewGroup) view;
            x.m();
            x.j();
            View view2 = j0.mView;
            if (view2 != null) {
                if (resourceId != 0) {
                    view2.setId(resourceId);
                }
                if (j0.mView.getTag() == null) {
                    j0.mView.setTag(string);
                }
                j0.mView.addOnAttachStateChangeListener(new a(x));
                return j0.mView;
            }
            throw new IllegalStateException("Fragment " + attributeValue + " did not create a view.");
        }
        return null;
    }
}
