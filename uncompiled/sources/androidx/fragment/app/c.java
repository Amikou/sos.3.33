package androidx.fragment.app;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import androidx.fragment.app.k;
import defpackage.tv;

/* compiled from: FragmentAnim.java */
/* loaded from: classes.dex */
public class c {

    /* compiled from: FragmentAnim.java */
    /* loaded from: classes.dex */
    public class a implements tv.a {
        public final /* synthetic */ Fragment a;

        public a(Fragment fragment) {
            this.a = fragment;
        }

        @Override // defpackage.tv.a
        public void a() {
            if (this.a.getAnimatingAway() != null) {
                View animatingAway = this.a.getAnimatingAway();
                this.a.setAnimatingAway(null);
                animatingAway.clearAnimation();
            }
            this.a.setAnimator(null);
        }
    }

    /* compiled from: FragmentAnim.java */
    /* loaded from: classes.dex */
    public class b implements Animation.AnimationListener {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ Fragment b;
        public final /* synthetic */ k.g c;
        public final /* synthetic */ tv d;

        /* compiled from: FragmentAnim.java */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                if (b.this.b.getAnimatingAway() != null) {
                    b.this.b.setAnimatingAway(null);
                    b bVar = b.this;
                    bVar.c.b(bVar.b, bVar.d);
                }
            }
        }

        public b(ViewGroup viewGroup, Fragment fragment, k.g gVar, tv tvVar) {
            this.a = viewGroup;
            this.b = fragment;
            this.c = gVar;
            this.d = tvVar;
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            this.a.post(new a());
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationRepeat(Animation animation) {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationStart(Animation animation) {
        }
    }

    /* compiled from: FragmentAnim.java */
    /* renamed from: androidx.fragment.app.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0026c extends AnimatorListenerAdapter {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ Fragment g0;
        public final /* synthetic */ k.g h0;
        public final /* synthetic */ tv i0;

        public C0026c(ViewGroup viewGroup, View view, Fragment fragment, k.g gVar, tv tvVar) {
            this.a = viewGroup;
            this.f0 = view;
            this.g0 = fragment;
            this.h0 = gVar;
            this.i0 = tvVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.a.endViewTransition(this.f0);
            Animator animator2 = this.g0.getAnimator();
            this.g0.setAnimator(null);
            if (animator2 == null || this.a.indexOfChild(this.f0) >= 0) {
                return;
            }
            this.h0.b(this.g0, this.i0);
        }
    }

    public static void a(Fragment fragment, d dVar, k.g gVar) {
        View view = fragment.mView;
        ViewGroup viewGroup = fragment.mContainer;
        viewGroup.startViewTransition(view);
        tv tvVar = new tv();
        tvVar.d(new a(fragment));
        gVar.a(fragment, tvVar);
        if (dVar.a != null) {
            e eVar = new e(dVar.a, viewGroup, view);
            fragment.setAnimatingAway(fragment.mView);
            eVar.setAnimationListener(new b(viewGroup, fragment, gVar, tvVar));
            fragment.mView.startAnimation(eVar);
            return;
        }
        Animator animator = dVar.b;
        fragment.setAnimator(animator);
        animator.addListener(new C0026c(viewGroup, view, fragment, gVar, tvVar));
        animator.setTarget(fragment.mView);
        animator.start();
    }

    public static int b(Fragment fragment, boolean z, boolean z2) {
        if (z2) {
            if (z) {
                return fragment.getPopEnterAnim();
            }
            return fragment.getPopExitAnim();
        } else if (z) {
            return fragment.getEnterAnim();
        } else {
            return fragment.getExitAnim();
        }
    }

    public static d c(Context context, Fragment fragment, boolean z, boolean z2) {
        int nextTransition = fragment.getNextTransition();
        int b2 = b(fragment, z, z2);
        boolean z3 = false;
        fragment.setAnimations(0, 0, 0, 0);
        ViewGroup viewGroup = fragment.mContainer;
        if (viewGroup != null) {
            int i = k03.visible_removing_fragment_view_tag;
            if (viewGroup.getTag(i) != null) {
                fragment.mContainer.setTag(i, null);
            }
        }
        ViewGroup viewGroup2 = fragment.mContainer;
        if (viewGroup2 == null || viewGroup2.getLayoutTransition() == null) {
            Animation onCreateAnimation = fragment.onCreateAnimation(nextTransition, z, b2);
            if (onCreateAnimation != null) {
                return new d(onCreateAnimation);
            }
            Animator onCreateAnimator = fragment.onCreateAnimator(nextTransition, z, b2);
            if (onCreateAnimator != null) {
                return new d(onCreateAnimator);
            }
            if (b2 == 0 && nextTransition != 0) {
                b2 = d(nextTransition, z);
            }
            if (b2 != 0) {
                boolean equals = "anim".equals(context.getResources().getResourceTypeName(b2));
                if (equals) {
                    try {
                        Animation loadAnimation = AnimationUtils.loadAnimation(context, b2);
                        if (loadAnimation != null) {
                            return new d(loadAnimation);
                        }
                        z3 = true;
                    } catch (Resources.NotFoundException e2) {
                        throw e2;
                    } catch (RuntimeException unused) {
                    }
                }
                if (!z3) {
                    try {
                        Animator loadAnimator = AnimatorInflater.loadAnimator(context, b2);
                        if (loadAnimator != null) {
                            return new d(loadAnimator);
                        }
                    } catch (RuntimeException e3) {
                        if (!equals) {
                            Animation loadAnimation2 = AnimationUtils.loadAnimation(context, b2);
                            if (loadAnimation2 != null) {
                                return new d(loadAnimation2);
                            }
                        } else {
                            throw e3;
                        }
                    }
                }
            }
            return null;
        }
        return null;
    }

    public static int d(int i, boolean z) {
        if (i == 4097) {
            return z ? wx2.fragment_open_enter : wx2.fragment_open_exit;
        } else if (i == 4099) {
            return z ? wx2.fragment_fade_enter : wx2.fragment_fade_exit;
        } else if (i != 8194) {
            return -1;
        } else {
            return z ? wx2.fragment_close_enter : wx2.fragment_close_exit;
        }
    }

    /* compiled from: FragmentAnim.java */
    /* loaded from: classes.dex */
    public static class d {
        public final Animation a;
        public final Animator b;

        public d(Animation animation) {
            this.a = animation;
            this.b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }

        public d(Animator animator) {
            this.a = null;
            this.b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }
    }

    /* compiled from: FragmentAnim.java */
    /* loaded from: classes.dex */
    public static class e extends AnimationSet implements Runnable {
        public final ViewGroup a;
        public final View f0;
        public boolean g0;
        public boolean h0;
        public boolean i0;

        public e(Animation animation, ViewGroup viewGroup, View view) {
            super(false);
            this.i0 = true;
            this.a = viewGroup;
            this.f0 = view;
            addAnimation(animation);
            viewGroup.post(this);
        }

        @Override // android.view.animation.AnimationSet, android.view.animation.Animation
        public boolean getTransformation(long j, Transformation transformation) {
            this.i0 = true;
            if (this.g0) {
                return !this.h0;
            }
            if (!super.getTransformation(j, transformation)) {
                this.g0 = true;
                vm2.a(this.a, this);
            }
            return true;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (!this.g0 && this.i0) {
                this.i0 = false;
                this.a.post(this);
                return;
            }
            this.a.endViewTransition(this.f0);
            this.h0 = true;
        }

        @Override // android.view.animation.Animation
        public boolean getTransformation(long j, Transformation transformation, float f) {
            this.i0 = true;
            if (this.g0) {
                return !this.h0;
            }
            if (!super.getTransformation(j, transformation, f)) {
                this.g0 = true;
                vm2.a(this.a, this);
            }
            return true;
        }
    }
}
