package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

@Deprecated
/* loaded from: classes.dex */
public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    public final ArrayList<a> a;
    public FrameLayout f0;
    public Context g0;
    public FragmentManager h0;
    public int i0;
    public TabHost.OnTabChangeListener j0;
    public a k0;
    public boolean l0;

    /* loaded from: classes.dex */
    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public String a;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.a + "}";
        }

        @Override // android.view.View.BaseSavedState, android.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.a);
        }

        public SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readString();
        }
    }

    /* loaded from: classes.dex */
    public static final class a {
        public final String a;
        public final Class<?> b;
        public final Bundle c;
        public Fragment d;
    }

    @Deprecated
    public FragmentTabHost(Context context) {
        super(context, null);
        this.a = new ArrayList<>();
        e(context, null);
    }

    public final j a(String str, j jVar) {
        Fragment fragment;
        a d = d(str);
        if (this.k0 != d) {
            if (jVar == null) {
                jVar = this.h0.n();
            }
            a aVar = this.k0;
            if (aVar != null && (fragment = aVar.d) != null) {
                jVar.n(fragment);
            }
            if (d != null) {
                Fragment fragment2 = d.d;
                if (fragment2 == null) {
                    Fragment a2 = this.h0.s0().a(this.g0.getClassLoader(), d.b.getName());
                    d.d = a2;
                    a2.setArguments(d.c);
                    jVar.c(this.i0, d.d, d.a);
                } else {
                    jVar.i(fragment2);
                }
            }
            this.k0 = d;
        }
        return jVar;
    }

    public final void b() {
        if (this.f0 == null) {
            FrameLayout frameLayout = (FrameLayout) findViewById(this.i0);
            this.f0 = frameLayout;
            if (frameLayout != null) {
                return;
            }
            throw new IllegalStateException("No tab content FrameLayout found for id " + this.i0);
        }
    }

    public final void c(Context context) {
        if (findViewById(16908307) == null) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            addView(linearLayout, new FrameLayout.LayoutParams(-1, -1));
            TabWidget tabWidget = new TabWidget(context);
            tabWidget.setId(16908307);
            tabWidget.setOrientation(0);
            linearLayout.addView(tabWidget, new LinearLayout.LayoutParams(-1, -2, Utils.FLOAT_EPSILON));
            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setId(16908305);
            linearLayout.addView(frameLayout, new LinearLayout.LayoutParams(0, 0, Utils.FLOAT_EPSILON));
            FrameLayout frameLayout2 = new FrameLayout(context);
            this.f0 = frameLayout2;
            frameLayout2.setId(this.i0);
            linearLayout.addView(frameLayout2, new LinearLayout.LayoutParams(-1, 0, 1.0f));
        }
    }

    public final a d(String str) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            a aVar = this.a.get(i);
            if (aVar.a.equals(str)) {
                return aVar;
            }
        }
        return null;
    }

    public final void e(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.i0 = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
    }

    @Override // android.view.ViewGroup, android.view.View
    @Deprecated
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        int size = this.a.size();
        j jVar = null;
        for (int i = 0; i < size; i++) {
            a aVar = this.a.get(i);
            Fragment k0 = this.h0.k0(aVar.a);
            aVar.d = k0;
            if (k0 != null && !k0.isDetached()) {
                if (aVar.a.equals(currentTabTag)) {
                    this.k0 = aVar;
                } else {
                    if (jVar == null) {
                        jVar = this.h0.n();
                    }
                    jVar.n(aVar.d);
                }
            }
        }
        this.l0 = true;
        j a2 = a(currentTabTag, jVar);
        if (a2 != null) {
            a2.j();
            this.h0.g0();
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    @Deprecated
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.l0 = false;
    }

    @Override // android.view.View
    @Deprecated
    public void onRestoreInstanceState(@SuppressLint({"UnknownNullness"}) Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.a);
    }

    @Override // android.view.View
    @Deprecated
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = getCurrentTabTag();
        return savedState;
    }

    @Override // android.widget.TabHost.OnTabChangeListener
    @Deprecated
    public void onTabChanged(String str) {
        j a2;
        if (this.l0 && (a2 = a(str, null)) != null) {
            a2.j();
        }
        TabHost.OnTabChangeListener onTabChangeListener = this.j0;
        if (onTabChangeListener != null) {
            onTabChangeListener.onTabChanged(str);
        }
    }

    @Override // android.widget.TabHost
    @Deprecated
    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.j0 = onTabChangeListener;
    }

    @Override // android.widget.TabHost
    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    @Deprecated
    public void setup(Context context, FragmentManager fragmentManager) {
        c(context);
        super.setup();
        this.g0 = context;
        this.h0 = fragmentManager;
        b();
    }

    @Deprecated
    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new ArrayList<>();
        e(context, attributeSet);
    }

    @Deprecated
    public void setup(Context context, FragmentManager fragmentManager, int i) {
        c(context);
        super.setup();
        this.g0 = context;
        this.h0 = fragmentManager;
        this.i0 = i;
        b();
        this.f0.setId(i);
        if (getId() == -1) {
            setId(16908306);
        }
    }
}
