package androidx.fragment.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import androidx.activity.OnBackPressedDispatcher;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultRegistry;
import androidx.activity.result.IntentSenderRequest;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.c;
import androidx.fragment.app.j;
import androidx.fragment.app.k;
import androidx.lifecycle.Lifecycle;
import com.github.mikephil.charting.utils.Utils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* loaded from: classes.dex */
public abstract class FragmentManager {
    public static boolean O = false;
    public static boolean P = true;
    public w7<IntentSenderRequest> A;
    public w7<String[]> B;
    public boolean D;
    public boolean E;
    public boolean F;
    public boolean G;
    public boolean H;
    public ArrayList<androidx.fragment.app.a> I;
    public ArrayList<Boolean> J;
    public ArrayList<Fragment> K;
    public ArrayList<p> L;
    public na1 M;
    public boolean b;
    public ArrayList<androidx.fragment.app.a> d;
    public ArrayList<Fragment> e;
    public OnBackPressedDispatcher g;
    public ArrayList<m> l;
    public androidx.fragment.app.e<?> r;
    public x91 s;
    public Fragment t;
    public Fragment u;
    public w7<Intent> z;
    public final ArrayList<n> a = new ArrayList<>();
    public final androidx.fragment.app.i c = new androidx.fragment.app.i();
    public final androidx.fragment.app.f f = new androidx.fragment.app.f(this);
    public final fm2 h = new c(false);
    public final AtomicInteger i = new AtomicInteger();
    public final Map<String, Bundle> j = Collections.synchronizedMap(new HashMap());
    public final Map<String, Object> k = Collections.synchronizedMap(new HashMap());
    public Map<Fragment, HashSet<tv>> m = Collections.synchronizedMap(new HashMap());
    public final k.g n = new d();
    public final androidx.fragment.app.g o = new androidx.fragment.app.g(this);
    public final CopyOnWriteArrayList<qa1> p = new CopyOnWriteArrayList<>();
    public int q = -1;
    public androidx.fragment.app.d v = null;
    public androidx.fragment.app.d w = new e();
    public mr3 x = null;
    public mr3 y = new f(this);
    public ArrayDeque<LaunchedFragmentInfo> C = new ArrayDeque<>();
    public Runnable N = new g();

    /* renamed from: androidx.fragment.app.FragmentManager$6  reason: invalid class name */
    /* loaded from: classes.dex */
    class AnonymousClass6 implements androidx.lifecycle.e {
        public final /* synthetic */ String a;
        public final /* synthetic */ ua1 f0;
        public final /* synthetic */ Lifecycle g0;
        public final /* synthetic */ FragmentManager h0;

        @Override // androidx.lifecycle.e
        public void e(rz1 rz1Var, Lifecycle.Event event) {
            Bundle bundle;
            if (event == Lifecycle.Event.ON_START && (bundle = (Bundle) this.h0.j.get(this.a)) != null) {
                this.f0.a(this.a, bundle);
                this.h0.s(this.a);
            }
            if (event == Lifecycle.Event.ON_DESTROY) {
                this.g0.c(this);
                this.h0.k.remove(this.a);
            }
        }
    }

    /* loaded from: classes.dex */
    public class a implements r7<ActivityResult> {
        public a() {
        }

        @Override // defpackage.r7
        /* renamed from: b */
        public void a(ActivityResult activityResult) {
            LaunchedFragmentInfo pollFirst = FragmentManager.this.C.pollFirst();
            if (pollFirst == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("No IntentSenders were started for ");
                sb.append(this);
                return;
            }
            String str = pollFirst.a;
            int i = pollFirst.f0;
            Fragment i2 = FragmentManager.this.c.i(str);
            if (i2 == null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Intent Sender result delivered for unknown Fragment ");
                sb2.append(str);
                return;
            }
            i2.onActivityResult(i, activityResult.b(), activityResult.a());
        }
    }

    /* loaded from: classes.dex */
    public class b implements r7<Map<String, Boolean>> {
        public b() {
        }

        @Override // defpackage.r7
        @SuppressLint({"SyntheticAccessor"})
        /* renamed from: b */
        public void a(Map<String, Boolean> map) {
            String[] strArr = (String[]) map.keySet().toArray(new String[0]);
            ArrayList arrayList = new ArrayList(map.values());
            int[] iArr = new int[arrayList.size()];
            for (int i = 0; i < arrayList.size(); i++) {
                iArr[i] = ((Boolean) arrayList.get(i)).booleanValue() ? 0 : -1;
            }
            LaunchedFragmentInfo pollFirst = FragmentManager.this.C.pollFirst();
            if (pollFirst == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("No permissions were requested for ");
                sb.append(this);
                return;
            }
            String str = pollFirst.a;
            int i2 = pollFirst.f0;
            Fragment i3 = FragmentManager.this.c.i(str);
            if (i3 == null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Permission request result delivered for unknown Fragment ");
                sb2.append(str);
                return;
            }
            i3.onRequestPermissionsResult(i2, strArr, iArr);
        }
    }

    /* loaded from: classes.dex */
    public class c extends fm2 {
        public c(boolean z) {
            super(z);
        }

        @Override // defpackage.fm2
        public void b() {
            FragmentManager.this.D0();
        }
    }

    /* loaded from: classes.dex */
    public class d implements k.g {
        public d() {
        }

        @Override // androidx.fragment.app.k.g
        public void a(Fragment fragment, tv tvVar) {
            FragmentManager.this.f(fragment, tvVar);
        }

        @Override // androidx.fragment.app.k.g
        public void b(Fragment fragment, tv tvVar) {
            if (tvVar.c()) {
                return;
            }
            FragmentManager.this.g1(fragment, tvVar);
        }
    }

    /* loaded from: classes.dex */
    public class e extends androidx.fragment.app.d {
        public e() {
        }

        @Override // androidx.fragment.app.d
        public Fragment a(ClassLoader classLoader, String str) {
            return FragmentManager.this.v0().b(FragmentManager.this.v0().f(), str, null);
        }
    }

    /* loaded from: classes.dex */
    public class f implements mr3 {
        public f(FragmentManager fragmentManager) {
        }

        @Override // defpackage.mr3
        public SpecialEffectsController a(ViewGroup viewGroup) {
            return new androidx.fragment.app.b(viewGroup);
        }
    }

    /* loaded from: classes.dex */
    public class g implements Runnable {
        public g() {
        }

        @Override // java.lang.Runnable
        public void run() {
            FragmentManager.this.c0(true);
        }
    }

    /* loaded from: classes.dex */
    public class h extends AnimatorListenerAdapter {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ Fragment g0;

        public h(FragmentManager fragmentManager, ViewGroup viewGroup, View view, Fragment fragment) {
            this.a = viewGroup;
            this.f0 = view;
            this.g0 = fragment;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.a.endViewTransition(this.f0);
            animator.removeListener(this);
            Fragment fragment = this.g0;
            View view = fragment.mView;
            if (view == null || !fragment.mHidden) {
                return;
            }
            view.setVisibility(8);
        }
    }

    /* loaded from: classes.dex */
    public class i implements qa1 {
        public final /* synthetic */ Fragment a;

        public i(FragmentManager fragmentManager, Fragment fragment) {
            this.a = fragment;
        }

        @Override // defpackage.qa1
        public void a(FragmentManager fragmentManager, Fragment fragment) {
            this.a.onAttachFragment(fragment);
        }
    }

    /* loaded from: classes.dex */
    public class j implements r7<ActivityResult> {
        public j() {
        }

        @Override // defpackage.r7
        /* renamed from: b */
        public void a(ActivityResult activityResult) {
            LaunchedFragmentInfo pollFirst = FragmentManager.this.C.pollFirst();
            if (pollFirst == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("No Activities were started for result for ");
                sb.append(this);
                return;
            }
            String str = pollFirst.a;
            int i = pollFirst.f0;
            Fragment i2 = FragmentManager.this.c.i(str);
            if (i2 == null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Activity result delivered for unknown Fragment ");
                sb2.append(str);
                return;
            }
            i2.onActivityResult(i, activityResult.b(), activityResult.a());
        }
    }

    /* loaded from: classes.dex */
    public static class k extends s7<IntentSenderRequest, ActivityResult> {
        @Override // defpackage.s7
        /* renamed from: d */
        public Intent a(Context context, IntentSenderRequest intentSenderRequest) {
            Bundle bundleExtra;
            Intent intent = new Intent("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST");
            Intent a = intentSenderRequest.a();
            if (a != null && (bundleExtra = a.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE")) != null) {
                intent.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundleExtra);
                a.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                if (a.getBooleanExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", false)) {
                    intentSenderRequest = new IntentSenderRequest.b(intentSenderRequest.d()).b(null).c(intentSenderRequest.c(), intentSenderRequest.b()).a();
                }
            }
            intent.putExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST", intentSenderRequest);
            if (FragmentManager.H0(2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("CreateIntent created the following intent: ");
                sb.append(intent);
            }
            return intent;
        }

        @Override // defpackage.s7
        /* renamed from: e */
        public ActivityResult c(int i, Intent intent) {
            return new ActivityResult(i, intent);
        }
    }

    /* loaded from: classes.dex */
    public static abstract class l {
        @Deprecated
        public void a(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void b(FragmentManager fragmentManager, Fragment fragment, Context context) {
        }

        public void c(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void d(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void e(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void f(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void g(FragmentManager fragmentManager, Fragment fragment, Context context) {
        }

        public void h(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void i(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void j(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void k(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void l(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void m(FragmentManager fragmentManager, Fragment fragment, View view, Bundle bundle) {
        }

        public void n(FragmentManager fragmentManager, Fragment fragment) {
        }
    }

    /* loaded from: classes.dex */
    public interface m {
        void onBackStackChanged();
    }

    /* loaded from: classes.dex */
    public interface n {
        boolean a(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2);
    }

    /* loaded from: classes.dex */
    public class o implements n {
        public final String a;
        public final int b;
        public final int c;

        public o(String str, int i, int i2) {
            this.a = str;
            this.b = i;
            this.c = i2;
        }

        @Override // androidx.fragment.app.FragmentManager.n
        public boolean a(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2) {
            Fragment fragment = FragmentManager.this.u;
            if (fragment == null || this.b >= 0 || this.a != null || !fragment.getChildFragmentManager().b1()) {
                return FragmentManager.this.d1(arrayList, arrayList2, this.a, this.b, this.c);
            }
            return false;
        }
    }

    /* loaded from: classes.dex */
    public static class p implements Fragment.k {
        public final boolean a;
        public final androidx.fragment.app.a b;
        public int c;

        public p(androidx.fragment.app.a aVar, boolean z) {
            this.a = z;
            this.b = aVar;
        }

        @Override // androidx.fragment.app.Fragment.k
        public void a() {
            this.c++;
        }

        @Override // androidx.fragment.app.Fragment.k
        public void b() {
            int i = this.c - 1;
            this.c = i;
            if (i != 0) {
                return;
            }
            this.b.r.o1();
        }

        public void c() {
            androidx.fragment.app.a aVar = this.b;
            aVar.r.v(aVar, this.a, false, false);
        }

        public void d() {
            boolean z = this.c > 0;
            for (Fragment fragment : this.b.r.u0()) {
                fragment.setOnStartEnterTransitionListener(null);
                if (z && fragment.isPostponed()) {
                    fragment.startPostponedEnterTransition();
                }
            }
            androidx.fragment.app.a aVar = this.b;
            aVar.r.v(aVar, this.a, !z, true);
        }

        public boolean e() {
            return this.c == 0;
        }
    }

    public static Fragment B0(View view) {
        Object tag = view.getTag(k03.fragment_container_view_tag);
        if (tag instanceof Fragment) {
            return (Fragment) tag;
        }
        return null;
    }

    public static boolean H0(int i2) {
        return O || Log.isLoggable("FragmentManager", i2);
    }

    public static void e0(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        while (i2 < i3) {
            androidx.fragment.app.a aVar = arrayList.get(i2);
            if (arrayList2.get(i2).booleanValue()) {
                aVar.y(-1);
                aVar.D(i2 == i3 + (-1));
            } else {
                aVar.y(1);
                aVar.C();
            }
            i2++;
        }
    }

    public static int m1(int i2) {
        if (i2 != 4097) {
            if (i2 != 4099) {
                return i2 != 8194 ? 0 : 4097;
            }
            return 4099;
        }
        return 8194;
    }

    public void A() {
        this.E = false;
        this.F = false;
        this.M.j(false);
        U(4);
    }

    public mr3 A0() {
        mr3 mr3Var = this.x;
        if (mr3Var != null) {
            return mr3Var;
        }
        Fragment fragment = this.t;
        if (fragment != null) {
            return fragment.mFragmentManager.A0();
        }
        return this.y;
    }

    public void B() {
        this.E = false;
        this.F = false;
        this.M.j(false);
        U(0);
    }

    public void C(Configuration configuration) {
        for (Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performConfigurationChanged(configuration);
            }
        }
    }

    public gj4 C0(Fragment fragment) {
        return this.M.g(fragment);
    }

    public boolean D(MenuItem menuItem) {
        if (this.q < 1) {
            return false;
        }
        for (Fragment fragment : this.c.n()) {
            if (fragment != null && fragment.performContextItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void D0() {
        c0(true);
        if (this.h.c()) {
            b1();
        } else {
            this.g.d();
        }
    }

    public void E() {
        this.E = false;
        this.F = false;
        this.M.j(false);
        U(1);
    }

    public void E0(Fragment fragment) {
        if (H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("hide: ");
            sb.append(fragment);
        }
        if (fragment.mHidden) {
            return;
        }
        fragment.mHidden = true;
        fragment.mHiddenChanged = true ^ fragment.mHiddenChanged;
        s1(fragment);
    }

    public boolean F(Menu menu, MenuInflater menuInflater) {
        if (this.q < 1) {
            return false;
        }
        ArrayList<Fragment> arrayList = null;
        boolean z = false;
        for (Fragment fragment : this.c.n()) {
            if (fragment != null && J0(fragment) && fragment.performCreateOptionsMenu(menu, menuInflater)) {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                arrayList.add(fragment);
                z = true;
            }
        }
        if (this.e != null) {
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                Fragment fragment2 = this.e.get(i2);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    fragment2.onDestroyOptionsMenu();
                }
            }
        }
        this.e = arrayList;
        return z;
    }

    public void F0(Fragment fragment) {
        if (fragment.mAdded && I0(fragment)) {
            this.D = true;
        }
    }

    public void G() {
        this.G = true;
        c0(true);
        Z();
        U(-1);
        this.r = null;
        this.s = null;
        this.t = null;
        if (this.g != null) {
            this.h.d();
            this.g = null;
        }
        w7<Intent> w7Var = this.z;
        if (w7Var != null) {
            w7Var.c();
            this.A.c();
            this.B.c();
        }
    }

    public boolean G0() {
        return this.G;
    }

    public void H() {
        U(1);
    }

    public void I() {
        for (Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performLowMemory();
            }
        }
    }

    public final boolean I0(Fragment fragment) {
        return (fragment.mHasMenu && fragment.mMenuVisible) || fragment.mChildFragmentManager.p();
    }

    public void J(boolean z) {
        for (Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performMultiWindowModeChanged(z);
            }
        }
    }

    public boolean J0(Fragment fragment) {
        if (fragment == null) {
            return true;
        }
        return fragment.isMenuVisible();
    }

    public void K(Fragment fragment) {
        Iterator<qa1> it = this.p.iterator();
        while (it.hasNext()) {
            it.next().a(this, fragment);
        }
    }

    public boolean K0(Fragment fragment) {
        if (fragment == null) {
            return true;
        }
        FragmentManager fragmentManager = fragment.mFragmentManager;
        return fragment.equals(fragmentManager.z0()) && K0(fragmentManager.t);
    }

    public boolean L(MenuItem menuItem) {
        if (this.q < 1) {
            return false;
        }
        for (Fragment fragment : this.c.n()) {
            if (fragment != null && fragment.performOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public boolean L0(int i2) {
        return this.q >= i2;
    }

    public void M(Menu menu) {
        if (this.q < 1) {
            return;
        }
        for (Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performOptionsMenuClosed(menu);
            }
        }
    }

    public boolean M0() {
        return this.E || this.F;
    }

    public final void N(Fragment fragment) {
        if (fragment == null || !fragment.equals(i0(fragment.mWho))) {
            return;
        }
        fragment.performPrimaryNavigationFragmentChanged();
    }

    public void N0(Fragment fragment, String[] strArr, int i2) {
        if (this.B != null) {
            this.C.addLast(new LaunchedFragmentInfo(fragment.mWho, i2));
            this.B.a(strArr);
            return;
        }
        this.r.j(fragment, strArr, i2);
    }

    public void O() {
        U(5);
    }

    public void O0(Fragment fragment, @SuppressLint({"UnknownNullness"}) Intent intent, int i2, Bundle bundle) {
        if (this.z != null) {
            this.C.addLast(new LaunchedFragmentInfo(fragment.mWho, i2));
            if (intent != null && bundle != null) {
                intent.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundle);
            }
            this.z.a(intent);
            return;
        }
        this.r.m(fragment, intent, i2, bundle);
    }

    public void P(boolean z) {
        for (Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performPictureInPictureModeChanged(z);
            }
        }
    }

    public void P0(Fragment fragment, @SuppressLint({"UnknownNullness"}) IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) throws IntentSender.SendIntentException {
        Intent intent2;
        if (this.A != null) {
            if (bundle != null) {
                if (intent == null) {
                    intent2 = new Intent();
                    intent2.putExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", true);
                } else {
                    intent2 = intent;
                }
                if (H0(2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("ActivityOptions ");
                    sb.append(bundle);
                    sb.append(" were added to fillInIntent ");
                    sb.append(intent2);
                    sb.append(" for fragment ");
                    sb.append(fragment);
                }
                intent2.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundle);
            } else {
                intent2 = intent;
            }
            IntentSenderRequest a2 = new IntentSenderRequest.b(intentSender).b(intent2).c(i4, i3).a();
            this.C.addLast(new LaunchedFragmentInfo(fragment.mWho, i2));
            if (H0(2)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(fragment);
                sb2.append("is launching an IntentSender for result ");
            }
            this.A.a(a2);
            return;
        }
        this.r.n(fragment, intentSender, i2, intent, i3, i4, i5, bundle);
    }

    public boolean Q(Menu menu) {
        boolean z = false;
        if (this.q < 1) {
            return false;
        }
        for (Fragment fragment : this.c.n()) {
            if (fragment != null && J0(fragment) && fragment.performPrepareOptionsMenu(menu)) {
                z = true;
            }
        }
        return z;
    }

    public final void Q0(uh<Fragment> uhVar) {
        int size = uhVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment q = uhVar.q(i2);
            if (!q.mAdded) {
                View requireView = q.requireView();
                q.mPostponedAlpha = requireView.getAlpha();
                requireView.setAlpha(Utils.FLOAT_EPSILON);
            }
        }
    }

    public void R() {
        w1();
        N(this.u);
    }

    public void R0(Fragment fragment) {
        if (!this.c.c(fragment.mWho)) {
            if (H0(3)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Ignoring moving ");
                sb.append(fragment);
                sb.append(" to state ");
                sb.append(this.q);
                sb.append("since it is not added to ");
                sb.append(this);
                return;
            }
            return;
        }
        T0(fragment);
        View view = fragment.mView;
        if (view != null && fragment.mIsNewlyAdded && fragment.mContainer != null) {
            float f2 = fragment.mPostponedAlpha;
            if (f2 > Utils.FLOAT_EPSILON) {
                view.setAlpha(f2);
            }
            fragment.mPostponedAlpha = Utils.FLOAT_EPSILON;
            fragment.mIsNewlyAdded = false;
            c.d c2 = androidx.fragment.app.c.c(this.r.f(), fragment, true, fragment.getPopDirection());
            if (c2 != null) {
                Animation animation = c2.a;
                if (animation != null) {
                    fragment.mView.startAnimation(animation);
                } else {
                    c2.b.setTarget(fragment.mView);
                    c2.b.start();
                }
            }
        }
        if (fragment.mHiddenChanged) {
            w(fragment);
        }
    }

    public void S() {
        this.E = false;
        this.F = false;
        this.M.j(false);
        U(7);
    }

    public void S0(int i2, boolean z) {
        androidx.fragment.app.e<?> eVar;
        if (this.r == null && i2 != -1) {
            throw new IllegalStateException("No activity");
        }
        if (z || i2 != this.q) {
            this.q = i2;
            if (P) {
                this.c.r();
            } else {
                for (Fragment fragment : this.c.n()) {
                    R0(fragment);
                }
                for (androidx.fragment.app.h hVar : this.c.k()) {
                    Fragment k2 = hVar.k();
                    if (!k2.mIsNewlyAdded) {
                        R0(k2);
                    }
                    if (k2.mRemoving && !k2.isInBackStack()) {
                        this.c.q(hVar);
                    }
                }
            }
            u1();
            if (this.D && (eVar = this.r) != null && this.q == 7) {
                eVar.o();
                this.D = false;
            }
        }
    }

    public void T() {
        this.E = false;
        this.F = false;
        this.M.j(false);
        U(5);
    }

    public void T0(Fragment fragment) {
        U0(fragment, this.q);
    }

    public final void U(int i2) {
        try {
            this.b = true;
            this.c.d(i2);
            S0(i2, false);
            if (P) {
                for (SpecialEffectsController specialEffectsController : t()) {
                    specialEffectsController.j();
                }
            }
            this.b = false;
            c0(true);
        } catch (Throwable th) {
            this.b = false;
            throw th;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x0051, code lost:
        if (r2 != 5) goto L26;
     */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0066  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0070  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:99:0x0150  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void U0(androidx.fragment.app.Fragment r10, int r11) {
        /*
            Method dump skipped, instructions count: 384
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentManager.U0(androidx.fragment.app.Fragment, int):void");
    }

    public void V() {
        this.F = true;
        this.M.j(true);
        U(4);
    }

    public void V0() {
        if (this.r == null) {
            return;
        }
        this.E = false;
        this.F = false;
        this.M.j(false);
        for (Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.noteStateNotSaved();
            }
        }
    }

    public void W() {
        U(2);
    }

    public void W0(FragmentContainerView fragmentContainerView) {
        View view;
        for (androidx.fragment.app.h hVar : this.c.k()) {
            Fragment k2 = hVar.k();
            if (k2.mContainerId == fragmentContainerView.getId() && (view = k2.mView) != null && view.getParent() == null) {
                k2.mContainer = fragmentContainerView;
                hVar.b();
            }
        }
    }

    public final void X() {
        if (this.H) {
            this.H = false;
            u1();
        }
    }

    public void X0(androidx.fragment.app.h hVar) {
        Fragment k2 = hVar.k();
        if (k2.mDeferStart) {
            if (this.b) {
                this.H = true;
                return;
            }
            k2.mDeferStart = false;
            if (P) {
                hVar.m();
            } else {
                T0(k2);
            }
        }
    }

    public void Y(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        String str2 = str + "    ";
        this.c.e(str, fileDescriptor, printWriter, strArr);
        ArrayList<Fragment> arrayList = this.e;
        if (arrayList != null && (size2 = arrayList.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i2 = 0; i2 < size2; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(this.e.get(i2).toString());
            }
        }
        ArrayList<androidx.fragment.app.a> arrayList2 = this.d;
        if (arrayList2 != null && (size = arrayList2.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i3 = 0; i3 < size; i3++) {
                androidx.fragment.app.a aVar = this.d.get(i3);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(aVar.toString());
                aVar.A(str2, printWriter);
            }
        }
        printWriter.print(str);
        printWriter.println("Back Stack Index: " + this.i.get());
        synchronized (this.a) {
            int size3 = this.a.size();
            if (size3 > 0) {
                printWriter.print(str);
                printWriter.println("Pending Actions:");
                for (int i4 = 0; i4 < size3; i4++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i4);
                    printWriter.print(": ");
                    printWriter.println(this.a.get(i4));
                }
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.r);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.s);
        if (this.t != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.t);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.q);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.E);
        printWriter.print(" mStopped=");
        printWriter.print(this.F);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.G);
        if (this.D) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.D);
        }
    }

    public void Y0() {
        a0(new o(null, -1, 0), false);
    }

    public final void Z() {
        if (P) {
            for (SpecialEffectsController specialEffectsController : t()) {
                specialEffectsController.j();
            }
        } else if (!this.m.isEmpty()) {
            for (Fragment fragment : this.m.keySet()) {
                o(fragment);
                T0(fragment);
            }
        }
    }

    public void Z0(int i2, int i3) {
        if (i2 >= 0) {
            a0(new o(null, i2, i3), false);
            return;
        }
        throw new IllegalArgumentException("Bad id: " + i2);
    }

    public void a0(n nVar, boolean z) {
        if (!z) {
            if (this.r == null) {
                if (this.G) {
                    throw new IllegalStateException("FragmentManager has been destroyed");
                }
                throw new IllegalStateException("FragmentManager has not been attached to a host.");
            }
            q();
        }
        synchronized (this.a) {
            if (this.r == null) {
                if (!z) {
                    throw new IllegalStateException("Activity has been destroyed");
                }
                return;
            }
            this.a.add(nVar);
            o1();
        }
    }

    public void a1(String str, int i2) {
        a0(new o(str, -1, i2), false);
    }

    public final void b0(boolean z) {
        if (!this.b) {
            if (this.r == null) {
                if (this.G) {
                    throw new IllegalStateException("FragmentManager has been destroyed");
                }
                throw new IllegalStateException("FragmentManager has not been attached to a host.");
            } else if (Looper.myLooper() == this.r.g().getLooper()) {
                if (!z) {
                    q();
                }
                if (this.I == null) {
                    this.I = new ArrayList<>();
                    this.J = new ArrayList<>();
                }
                this.b = true;
                try {
                    h0(null, null);
                    return;
                } finally {
                    this.b = false;
                }
            } else {
                throw new IllegalStateException("Must be called from main thread of fragment host");
            }
        }
        throw new IllegalStateException("FragmentManager is already executing transactions");
    }

    public boolean b1() {
        return c1(null, -1, 0);
    }

    public boolean c0(boolean z) {
        b0(z);
        boolean z2 = false;
        while (n0(this.I, this.J)) {
            this.b = true;
            try {
                i1(this.I, this.J);
                r();
                z2 = true;
            } catch (Throwable th) {
                r();
                throw th;
            }
        }
        w1();
        X();
        this.c.b();
        return z2;
    }

    public final boolean c1(String str, int i2, int i3) {
        c0(false);
        b0(true);
        Fragment fragment = this.u;
        if (fragment == null || i2 >= 0 || str != null || !fragment.getChildFragmentManager().b1()) {
            boolean d1 = d1(this.I, this.J, str, i2, i3);
            if (d1) {
                this.b = true;
                try {
                    i1(this.I, this.J);
                } finally {
                    r();
                }
            }
            w1();
            X();
            this.c.b();
            return d1;
        }
        return true;
    }

    public final void d(uh<Fragment> uhVar) {
        int i2 = this.q;
        if (i2 < 1) {
            return;
        }
        int min = Math.min(i2, 5);
        for (Fragment fragment : this.c.n()) {
            if (fragment.mState < min) {
                U0(fragment, min);
                if (fragment.mView != null && !fragment.mHidden && fragment.mIsNewlyAdded) {
                    uhVar.add(fragment);
                }
            }
        }
    }

    public void d0(n nVar, boolean z) {
        if (z && (this.r == null || this.G)) {
            return;
        }
        b0(z);
        if (nVar.a(this.I, this.J)) {
            this.b = true;
            try {
                i1(this.I, this.J);
            } finally {
                r();
            }
        }
        w1();
        X();
        this.c.b();
    }

    public boolean d1(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2, String str, int i2, int i3) {
        int i4;
        ArrayList<androidx.fragment.app.a> arrayList3 = this.d;
        if (arrayList3 == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = arrayList3.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(this.d.remove(size));
            arrayList2.add(Boolean.TRUE);
        } else {
            if (str != null || i2 >= 0) {
                int size2 = arrayList3.size() - 1;
                while (size2 >= 0) {
                    androidx.fragment.app.a aVar = this.d.get(size2);
                    if ((str != null && str.equals(aVar.F())) || (i2 >= 0 && i2 == aVar.t)) {
                        break;
                    }
                    size2--;
                }
                if (size2 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    while (true) {
                        size2--;
                        if (size2 < 0) {
                            break;
                        }
                        androidx.fragment.app.a aVar2 = this.d.get(size2);
                        if (str == null || !str.equals(aVar2.F())) {
                            if (i2 < 0 || i2 != aVar2.t) {
                                break;
                            }
                        }
                    }
                }
                i4 = size2;
            } else {
                i4 = -1;
            }
            if (i4 == this.d.size() - 1) {
                return false;
            }
            for (int size3 = this.d.size() - 1; size3 > i4; size3--) {
                arrayList.add(this.d.remove(size3));
                arrayList2.add(Boolean.TRUE);
            }
        }
        return true;
    }

    public void e(androidx.fragment.app.a aVar) {
        if (this.d == null) {
            this.d = new ArrayList<>();
        }
        this.d.add(aVar);
    }

    public final int e1(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3, uh<Fragment> uhVar) {
        int i4 = i3;
        for (int i5 = i3 - 1; i5 >= i2; i5--) {
            androidx.fragment.app.a aVar = arrayList.get(i5);
            boolean booleanValue = arrayList2.get(i5).booleanValue();
            if (aVar.J() && !aVar.H(arrayList, i5 + 1, i3)) {
                if (this.L == null) {
                    this.L = new ArrayList<>();
                }
                p pVar = new p(aVar, booleanValue);
                this.L.add(pVar);
                aVar.L(pVar);
                if (booleanValue) {
                    aVar.C();
                } else {
                    aVar.D(false);
                }
                i4--;
                if (i5 != i4) {
                    arrayList.remove(i5);
                    arrayList.add(i4, aVar);
                }
                d(uhVar);
            }
        }
        return i4;
    }

    public void f(Fragment fragment, tv tvVar) {
        if (this.m.get(fragment) == null) {
            this.m.put(fragment, new HashSet<>());
        }
        this.m.get(fragment).add(tvVar);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:110:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00c5  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x0143  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x019d  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x01be  */
    /* JADX WARN: Type inference failed for: r1v17 */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4, types: [int, boolean] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void f0(java.util.ArrayList<androidx.fragment.app.a> r18, java.util.ArrayList<java.lang.Boolean> r19, int r20, int r21) {
        /*
            Method dump skipped, instructions count: 450
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentManager.f0(java.util.ArrayList, java.util.ArrayList, int, int):void");
    }

    public void f1(l lVar, boolean z) {
        this.o.o(lVar, z);
    }

    public androidx.fragment.app.h g(Fragment fragment) {
        if (H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("add: ");
            sb.append(fragment);
        }
        androidx.fragment.app.h x = x(fragment);
        fragment.mFragmentManager = this;
        this.c.p(x);
        if (!fragment.mDetached) {
            this.c.a(fragment);
            fragment.mRemoving = false;
            if (fragment.mView == null) {
                fragment.mHiddenChanged = false;
            }
            if (I0(fragment)) {
                this.D = true;
            }
        }
        return x;
    }

    public boolean g0() {
        boolean c0 = c0(true);
        m0();
        return c0;
    }

    public void g1(Fragment fragment, tv tvVar) {
        HashSet<tv> hashSet = this.m.get(fragment);
        if (hashSet != null && hashSet.remove(tvVar) && hashSet.isEmpty()) {
            this.m.remove(fragment);
            if (fragment.mState < 5) {
                y(fragment);
                T0(fragment);
            }
        }
    }

    public void h(qa1 qa1Var) {
        this.p.add(qa1Var);
    }

    public final void h0(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2) {
        int indexOf;
        int indexOf2;
        ArrayList<p> arrayList3 = this.L;
        int size = arrayList3 == null ? 0 : arrayList3.size();
        int i2 = 0;
        while (i2 < size) {
            p pVar = this.L.get(i2);
            if (arrayList != null && !pVar.a && (indexOf2 = arrayList.indexOf(pVar.b)) != -1 && arrayList2 != null && arrayList2.get(indexOf2).booleanValue()) {
                this.L.remove(i2);
                i2--;
                size--;
                pVar.c();
            } else if (pVar.e() || (arrayList != null && pVar.b.H(arrayList, 0, arrayList.size()))) {
                this.L.remove(i2);
                i2--;
                size--;
                if (arrayList != null && !pVar.a && (indexOf = arrayList.indexOf(pVar.b)) != -1 && arrayList2 != null && arrayList2.get(indexOf).booleanValue()) {
                    pVar.c();
                } else {
                    pVar.d();
                }
            }
            i2++;
        }
    }

    public void h1(Fragment fragment) {
        if (H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("remove: ");
            sb.append(fragment);
            sb.append(" nesting=");
            sb.append(fragment.mBackStackNesting);
        }
        boolean z = !fragment.isInBackStack();
        if (!fragment.mDetached || z) {
            this.c.s(fragment);
            if (I0(fragment)) {
                this.D = true;
            }
            fragment.mRemoving = true;
            s1(fragment);
        }
    }

    public void i(m mVar) {
        if (this.l == null) {
            this.l = new ArrayList<>();
        }
        this.l.add(mVar);
    }

    public Fragment i0(String str) {
        return this.c.f(str);
    }

    public final void i1(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2) {
        if (arrayList.isEmpty()) {
            return;
        }
        if (arrayList.size() == arrayList2.size()) {
            h0(arrayList, arrayList2);
            int size = arrayList.size();
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                if (!arrayList.get(i2).p) {
                    if (i3 != i2) {
                        f0(arrayList, arrayList2, i3, i2);
                    }
                    i3 = i2 + 1;
                    if (arrayList2.get(i2).booleanValue()) {
                        while (i3 < size && arrayList2.get(i3).booleanValue() && !arrayList.get(i3).p) {
                            i3++;
                        }
                    }
                    f0(arrayList, arrayList2, i2, i3);
                    i2 = i3 - 1;
                }
                i2++;
            }
            if (i3 != size) {
                f0(arrayList, arrayList2, i3, size);
                return;
            }
            return;
        }
        throw new IllegalStateException("Internal error with the back stack records");
    }

    public void j(Fragment fragment) {
        this.M.a(fragment);
    }

    public Fragment j0(int i2) {
        return this.c.g(i2);
    }

    public void j1(Fragment fragment) {
        this.M.i(fragment);
    }

    public int k() {
        return this.i.getAndIncrement();
    }

    public Fragment k0(String str) {
        return this.c.h(str);
    }

    public final void k1() {
        if (this.l != null) {
            for (int i2 = 0; i2 < this.l.size(); i2++) {
                this.l.get(i2).onBackStackChanged();
            }
        }
    }

    @SuppressLint({"SyntheticAccessor"})
    public void l(androidx.fragment.app.e<?> eVar, x91 x91Var, Fragment fragment) {
        String str;
        if (this.r == null) {
            this.r = eVar;
            this.s = x91Var;
            this.t = fragment;
            if (fragment != null) {
                h(new i(this, fragment));
            } else if (eVar instanceof qa1) {
                h((qa1) eVar);
            }
            if (this.t != null) {
                w1();
            }
            if (eVar instanceof gm2) {
                gm2 gm2Var = (gm2) eVar;
                OnBackPressedDispatcher onBackPressedDispatcher = gm2Var.getOnBackPressedDispatcher();
                this.g = onBackPressedDispatcher;
                rz1 rz1Var = gm2Var;
                if (fragment != null) {
                    rz1Var = fragment;
                }
                onBackPressedDispatcher.a(rz1Var, this.h);
            }
            if (fragment != null) {
                this.M = fragment.mFragmentManager.p0(fragment);
            } else if (eVar instanceof hj4) {
                this.M = na1.e(((hj4) eVar).getViewModelStore());
            } else {
                this.M = new na1(false);
            }
            this.M.j(M0());
            this.c.x(this.M);
            androidx.fragment.app.e<?> eVar2 = this.r;
            if (eVar2 instanceof x7) {
                ActivityResultRegistry activityResultRegistry = ((x7) eVar2).getActivityResultRegistry();
                if (fragment != null) {
                    str = fragment.mWho + ":";
                } else {
                    str = "";
                }
                String str2 = "FragmentManager:" + str;
                this.z = activityResultRegistry.i(str2 + "StartActivityForResult", new v7(), new j());
                this.A = activityResultRegistry.i(str2 + "StartIntentSenderForResult", new k(), new a());
                this.B = activityResultRegistry.i(str2 + "RequestPermissions", new t7(), new b());
                return;
            }
            return;
        }
        throw new IllegalStateException("Already attached");
    }

    public Fragment l0(String str) {
        return this.c.i(str);
    }

    public void l1(Parcelable parcelable) {
        androidx.fragment.app.h hVar;
        if (parcelable == null) {
            return;
        }
        FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
        if (fragmentManagerState.a == null) {
            return;
        }
        this.c.t();
        Iterator<FragmentState> it = fragmentManagerState.a.iterator();
        while (it.hasNext()) {
            FragmentState next = it.next();
            if (next != null) {
                Fragment c2 = this.M.c(next.f0);
                if (c2 != null) {
                    if (H0(2)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("restoreSaveState: re-attaching retained ");
                        sb.append(c2);
                    }
                    hVar = new androidx.fragment.app.h(this.o, this.c, c2, next);
                } else {
                    hVar = new androidx.fragment.app.h(this.o, this.c, this.r.f().getClassLoader(), s0(), next);
                }
                Fragment k2 = hVar.k();
                k2.mFragmentManager = this;
                if (H0(2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("restoreSaveState: active (");
                    sb2.append(k2.mWho);
                    sb2.append("): ");
                    sb2.append(k2);
                }
                hVar.o(this.r.f().getClassLoader());
                this.c.p(hVar);
                hVar.t(this.q);
            }
        }
        for (Fragment fragment : this.M.f()) {
            if (!this.c.c(fragment.mWho)) {
                if (H0(2)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Discarding retained Fragment ");
                    sb3.append(fragment);
                    sb3.append(" that was not found in the set of active Fragments ");
                    sb3.append(fragmentManagerState.a);
                }
                this.M.i(fragment);
                fragment.mFragmentManager = this;
                androidx.fragment.app.h hVar2 = new androidx.fragment.app.h(this.o, this.c, fragment);
                hVar2.t(1);
                hVar2.m();
                fragment.mRemoving = true;
                hVar2.m();
            }
        }
        this.c.u(fragmentManagerState.f0);
        if (fragmentManagerState.g0 != null) {
            this.d = new ArrayList<>(fragmentManagerState.g0.length);
            int i2 = 0;
            while (true) {
                BackStackState[] backStackStateArr = fragmentManagerState.g0;
                if (i2 >= backStackStateArr.length) {
                    break;
                }
                androidx.fragment.app.a a2 = backStackStateArr[i2].a(this);
                if (H0(2)) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("restoreAllState: back stack #");
                    sb4.append(i2);
                    sb4.append(" (index ");
                    sb4.append(a2.t);
                    sb4.append("): ");
                    sb4.append(a2);
                    PrintWriter printWriter = new PrintWriter(new u12("FragmentManager"));
                    a2.B("  ", printWriter, false);
                    printWriter.close();
                }
                this.d.add(a2);
                i2++;
            }
        } else {
            this.d = null;
        }
        this.i.set(fragmentManagerState.h0);
        String str = fragmentManagerState.i0;
        if (str != null) {
            Fragment i0 = i0(str);
            this.u = i0;
            N(i0);
        }
        ArrayList<String> arrayList = fragmentManagerState.j0;
        if (arrayList != null) {
            for (int i3 = 0; i3 < arrayList.size(); i3++) {
                Bundle bundle = fragmentManagerState.k0.get(i3);
                bundle.setClassLoader(this.r.f().getClassLoader());
                this.j.put(arrayList.get(i3), bundle);
            }
        }
        this.C = new ArrayDeque<>(fragmentManagerState.l0);
    }

    public void m(Fragment fragment) {
        if (H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("attach: ");
            sb.append(fragment);
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (fragment.mAdded) {
                return;
            }
            this.c.a(fragment);
            if (H0(2)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("add from attach: ");
                sb2.append(fragment);
            }
            if (I0(fragment)) {
                this.D = true;
            }
        }
    }

    public final void m0() {
        if (P) {
            for (SpecialEffectsController specialEffectsController : t()) {
                specialEffectsController.k();
            }
        } else if (this.L != null) {
            while (!this.L.isEmpty()) {
                this.L.remove(0).d();
            }
        }
    }

    public androidx.fragment.app.j n() {
        return new androidx.fragment.app.a(this);
    }

    public final boolean n0(ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2) {
        synchronized (this.a) {
            if (this.a.isEmpty()) {
                return false;
            }
            int size = this.a.size();
            boolean z = false;
            for (int i2 = 0; i2 < size; i2++) {
                z |= this.a.get(i2).a(arrayList, arrayList2);
            }
            this.a.clear();
            this.r.g().removeCallbacks(this.N);
            return z;
        }
    }

    public Parcelable n1() {
        int size;
        m0();
        Z();
        c0(true);
        this.E = true;
        this.M.j(true);
        ArrayList<FragmentState> v = this.c.v();
        BackStackState[] backStackStateArr = null;
        if (v.isEmpty()) {
            H0(2);
            return null;
        }
        ArrayList<String> w = this.c.w();
        ArrayList<androidx.fragment.app.a> arrayList = this.d;
        if (arrayList != null && (size = arrayList.size()) > 0) {
            backStackStateArr = new BackStackState[size];
            for (int i2 = 0; i2 < size; i2++) {
                backStackStateArr[i2] = new BackStackState(this.d.get(i2));
                if (H0(2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("saveAllState: adding back stack #");
                    sb.append(i2);
                    sb.append(": ");
                    sb.append(this.d.get(i2));
                }
            }
        }
        FragmentManagerState fragmentManagerState = new FragmentManagerState();
        fragmentManagerState.a = v;
        fragmentManagerState.f0 = w;
        fragmentManagerState.g0 = backStackStateArr;
        fragmentManagerState.h0 = this.i.get();
        Fragment fragment = this.u;
        if (fragment != null) {
            fragmentManagerState.i0 = fragment.mWho;
        }
        fragmentManagerState.j0.addAll(this.j.keySet());
        fragmentManagerState.k0.addAll(this.j.values());
        fragmentManagerState.l0 = new ArrayList<>(this.C);
        return fragmentManagerState;
    }

    public final void o(Fragment fragment) {
        HashSet<tv> hashSet = this.m.get(fragment);
        if (hashSet != null) {
            Iterator<tv> it = hashSet.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            hashSet.clear();
            y(fragment);
            this.m.remove(fragment);
        }
    }

    public int o0() {
        ArrayList<androidx.fragment.app.a> arrayList = this.d;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    public void o1() {
        synchronized (this.a) {
            ArrayList<p> arrayList = this.L;
            boolean z = (arrayList == null || arrayList.isEmpty()) ? false : true;
            boolean z2 = this.a.size() == 1;
            if (z || z2) {
                this.r.g().removeCallbacks(this.N);
                this.r.g().post(this.N);
                w1();
            }
        }
    }

    public boolean p() {
        boolean z = false;
        for (Fragment fragment : this.c.l()) {
            if (fragment != null) {
                z = I0(fragment);
                continue;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }

    public final na1 p0(Fragment fragment) {
        return this.M.d(fragment);
    }

    public void p1(Fragment fragment, boolean z) {
        ViewGroup r0 = r0(fragment);
        if (r0 == null || !(r0 instanceof FragmentContainerView)) {
            return;
        }
        ((FragmentContainerView) r0).setDrawDisappearingViewsLast(!z);
    }

    public final void q() {
        if (M0()) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    public x91 q0() {
        return this.s;
    }

    public void q1(Fragment fragment, Lifecycle.State state) {
        if (fragment.equals(i0(fragment.mWho)) && (fragment.mHost == null || fragment.mFragmentManager == this)) {
            fragment.mMaxState = state;
            return;
        }
        throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
    }

    public final void r() {
        this.b = false;
        this.J.clear();
        this.I.clear();
    }

    public final ViewGroup r0(Fragment fragment) {
        ViewGroup viewGroup = fragment.mContainer;
        if (viewGroup != null) {
            return viewGroup;
        }
        if (fragment.mContainerId > 0 && this.s.d()) {
            View c2 = this.s.c(fragment.mContainerId);
            if (c2 instanceof ViewGroup) {
                return (ViewGroup) c2;
            }
        }
        return null;
    }

    public void r1(Fragment fragment) {
        if (fragment != null && (!fragment.equals(i0(fragment.mWho)) || (fragment.mHost != null && fragment.mFragmentManager != this))) {
            throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
        }
        Fragment fragment2 = this.u;
        this.u = fragment;
        N(fragment2);
        N(this.u);
    }

    public final void s(String str) {
        this.j.remove(str);
    }

    public androidx.fragment.app.d s0() {
        androidx.fragment.app.d dVar = this.v;
        if (dVar != null) {
            return dVar;
        }
        Fragment fragment = this.t;
        if (fragment != null) {
            return fragment.mFragmentManager.s0();
        }
        return this.w;
    }

    public final void s1(Fragment fragment) {
        ViewGroup r0 = r0(fragment);
        if (r0 == null || fragment.getEnterAnim() + fragment.getExitAnim() + fragment.getPopEnterAnim() + fragment.getPopExitAnim() <= 0) {
            return;
        }
        int i2 = k03.visible_removing_fragment_view_tag;
        if (r0.getTag(i2) == null) {
            r0.setTag(i2, fragment);
        }
        ((Fragment) r0.getTag(i2)).setPopDirection(fragment.getPopDirection());
    }

    public final Set<SpecialEffectsController> t() {
        HashSet hashSet = new HashSet();
        for (androidx.fragment.app.h hVar : this.c.k()) {
            ViewGroup viewGroup = hVar.k().mContainer;
            if (viewGroup != null) {
                hashSet.add(SpecialEffectsController.n(viewGroup, A0()));
            }
        }
        return hashSet;
    }

    public androidx.fragment.app.i t0() {
        return this.c;
    }

    public void t1(Fragment fragment) {
        if (H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("show: ");
            sb.append(fragment);
        }
        if (fragment.mHidden) {
            fragment.mHidden = false;
            fragment.mHiddenChanged = !fragment.mHiddenChanged;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        Fragment fragment = this.t;
        if (fragment != null) {
            sb.append(fragment.getClass().getSimpleName());
            sb.append("{");
            sb.append(Integer.toHexString(System.identityHashCode(this.t)));
            sb.append("}");
        } else {
            androidx.fragment.app.e<?> eVar = this.r;
            if (eVar != null) {
                sb.append(eVar.getClass().getSimpleName());
                sb.append("{");
                sb.append(Integer.toHexString(System.identityHashCode(this.r)));
                sb.append("}");
            } else {
                sb.append("null");
            }
        }
        sb.append("}}");
        return sb.toString();
    }

    public final Set<SpecialEffectsController> u(ArrayList<androidx.fragment.app.a> arrayList, int i2, int i3) {
        ViewGroup viewGroup;
        HashSet hashSet = new HashSet();
        while (i2 < i3) {
            Iterator<j.a> it = arrayList.get(i2).a.iterator();
            while (it.hasNext()) {
                Fragment fragment = it.next().b;
                if (fragment != null && (viewGroup = fragment.mContainer) != null) {
                    hashSet.add(SpecialEffectsController.o(viewGroup, this));
                }
            }
            i2++;
        }
        return hashSet;
    }

    public List<Fragment> u0() {
        return this.c.n();
    }

    public final void u1() {
        for (androidx.fragment.app.h hVar : this.c.k()) {
            X0(hVar);
        }
    }

    public void v(androidx.fragment.app.a aVar, boolean z, boolean z2, boolean z3) {
        if (z) {
            aVar.D(z3);
        } else {
            aVar.C();
        }
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(aVar);
        arrayList2.add(Boolean.valueOf(z));
        if (z2 && this.q >= 1) {
            androidx.fragment.app.k.B(this.r.f(), this.s, arrayList, arrayList2, 0, 1, true, this.n);
        }
        if (z3) {
            S0(this.q, true);
        }
        for (Fragment fragment : this.c.l()) {
            if (fragment != null && fragment.mView != null && fragment.mIsNewlyAdded && aVar.G(fragment.mContainerId)) {
                float f2 = fragment.mPostponedAlpha;
                if (f2 > Utils.FLOAT_EPSILON) {
                    fragment.mView.setAlpha(f2);
                }
                if (z3) {
                    fragment.mPostponedAlpha = Utils.FLOAT_EPSILON;
                } else {
                    fragment.mPostponedAlpha = -1.0f;
                    fragment.mIsNewlyAdded = false;
                }
            }
        }
    }

    public androidx.fragment.app.e<?> v0() {
        return this.r;
    }

    public void v1(l lVar) {
        this.o.p(lVar);
    }

    public final void w(Fragment fragment) {
        Animator animator;
        if (fragment.mView != null) {
            c.d c2 = androidx.fragment.app.c.c(this.r.f(), fragment, !fragment.mHidden, fragment.getPopDirection());
            if (c2 != null && (animator = c2.b) != null) {
                animator.setTarget(fragment.mView);
                if (fragment.mHidden) {
                    if (fragment.isHideReplaced()) {
                        fragment.setHideReplaced(false);
                    } else {
                        ViewGroup viewGroup = fragment.mContainer;
                        View view = fragment.mView;
                        viewGroup.startViewTransition(view);
                        c2.b.addListener(new h(this, viewGroup, view, fragment));
                    }
                } else {
                    fragment.mView.setVisibility(0);
                }
                c2.b.start();
            } else {
                if (c2 != null) {
                    fragment.mView.startAnimation(c2.a);
                    c2.a.start();
                }
                fragment.mView.setVisibility((!fragment.mHidden || fragment.isHideReplaced()) ? 0 : 8);
                if (fragment.isHideReplaced()) {
                    fragment.setHideReplaced(false);
                }
            }
        }
        F0(fragment);
        fragment.mHiddenChanged = false;
        fragment.onHiddenChanged(fragment.mHidden);
    }

    public LayoutInflater.Factory2 w0() {
        return this.f;
    }

    public final void w1() {
        synchronized (this.a) {
            boolean z = true;
            if (!this.a.isEmpty()) {
                this.h.f(true);
            } else {
                this.h.f((o0() <= 0 || !K0(this.t)) ? false : false);
            }
        }
    }

    public androidx.fragment.app.h x(Fragment fragment) {
        androidx.fragment.app.h m2 = this.c.m(fragment.mWho);
        if (m2 != null) {
            return m2;
        }
        androidx.fragment.app.h hVar = new androidx.fragment.app.h(this.o, this.c, fragment);
        hVar.o(this.r.f().getClassLoader());
        hVar.t(this.q);
        return hVar;
    }

    public androidx.fragment.app.g x0() {
        return this.o;
    }

    public final void y(Fragment fragment) {
        fragment.performDestroyView();
        this.o.n(fragment, false);
        fragment.mContainer = null;
        fragment.mView = null;
        fragment.mViewLifecycleOwner = null;
        fragment.mViewLifecycleOwnerLiveData.setValue(null);
        fragment.mInLayout = false;
    }

    public Fragment y0() {
        return this.t;
    }

    public void z(Fragment fragment) {
        if (H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("detach: ");
            sb.append(fragment);
        }
        if (fragment.mDetached) {
            return;
        }
        fragment.mDetached = true;
        if (fragment.mAdded) {
            if (H0(2)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("remove from detach: ");
                sb2.append(fragment);
            }
            this.c.s(fragment);
            if (I0(fragment)) {
                this.D = true;
            }
            s1(fragment);
        }
    }

    public Fragment z0() {
        return this.u;
    }

    @SuppressLint({"BanParcelableUsage"})
    /* loaded from: classes.dex */
    public static class LaunchedFragmentInfo implements Parcelable {
        public static final Parcelable.Creator<LaunchedFragmentInfo> CREATOR = new a();
        public String a;
        public int f0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<LaunchedFragmentInfo> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public LaunchedFragmentInfo createFromParcel(Parcel parcel) {
                return new LaunchedFragmentInfo(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public LaunchedFragmentInfo[] newArray(int i) {
                return new LaunchedFragmentInfo[i];
            }
        }

        public LaunchedFragmentInfo(String str, int i) {
            this.a = str;
            this.f0 = i;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.a);
            parcel.writeInt(this.f0);
        }

        public LaunchedFragmentInfo(Parcel parcel) {
            this.a = parcel.readString();
            this.f0 = parcel.readInt();
        }
    }
}
