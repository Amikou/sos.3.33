package androidx.fragment.app;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.FragmentManager;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: FragmentLifecycleCallbacksDispatcher.java */
/* loaded from: classes.dex */
public class g {
    public final CopyOnWriteArrayList<a> a = new CopyOnWriteArrayList<>();
    public final FragmentManager b;

    /* compiled from: FragmentLifecycleCallbacksDispatcher.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final FragmentManager.l a;
        public final boolean b;

        public a(FragmentManager.l lVar, boolean z) {
            this.a = lVar;
            this.b = z;
        }
    }

    public g(FragmentManager fragmentManager) {
        this.b = fragmentManager;
    }

    public void a(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().a(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.a(this.b, fragment, bundle);
            }
        }
    }

    public void b(Fragment fragment, boolean z) {
        Context f = this.b.v0().f();
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().b(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.b(this.b, fragment, f);
            }
        }
    }

    public void c(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().c(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.c(this.b, fragment, bundle);
            }
        }
    }

    public void d(Fragment fragment, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().d(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.d(this.b, fragment);
            }
        }
    }

    public void e(Fragment fragment, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().e(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.e(this.b, fragment);
            }
        }
    }

    public void f(Fragment fragment, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().f(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.f(this.b, fragment);
            }
        }
    }

    public void g(Fragment fragment, boolean z) {
        Context f = this.b.v0().f();
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().g(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.g(this.b, fragment, f);
            }
        }
    }

    public void h(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().h(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.h(this.b, fragment, bundle);
            }
        }
    }

    public void i(Fragment fragment, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().i(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.i(this.b, fragment);
            }
        }
    }

    public void j(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().j(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.j(this.b, fragment, bundle);
            }
        }
    }

    public void k(Fragment fragment, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().k(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.k(this.b, fragment);
            }
        }
    }

    public void l(Fragment fragment, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().l(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.l(this.b, fragment);
            }
        }
    }

    public void m(Fragment fragment, View view, Bundle bundle, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().m(fragment, view, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.m(this.b, fragment, view, bundle);
            }
        }
    }

    public void n(Fragment fragment, boolean z) {
        Fragment y0 = this.b.y0();
        if (y0 != null) {
            y0.getParentFragmentManager().x0().n(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.n(this.b, fragment);
            }
        }
    }

    public void o(FragmentManager.l lVar, boolean z) {
        this.a.add(new a(lVar, z));
    }

    public void p(FragmentManager.l lVar) {
        synchronized (this.a) {
            int i = 0;
            int size = this.a.size();
            while (true) {
                if (i >= size) {
                    break;
                } else if (this.a.get(i).a == lVar) {
                    this.a.remove(i);
                    break;
                } else {
                    i++;
                }
            }
        }
    }
}
