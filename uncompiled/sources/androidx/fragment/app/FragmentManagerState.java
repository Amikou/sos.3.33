package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;

/* JADX INFO: Access modifiers changed from: package-private */
@SuppressLint({"BanParcelableUsage"})
/* loaded from: classes.dex */
public final class FragmentManagerState implements Parcelable {
    public static final Parcelable.Creator<FragmentManagerState> CREATOR = new a();
    public ArrayList<FragmentState> a;
    public ArrayList<String> f0;
    public BackStackState[] g0;
    public int h0;
    public String i0;
    public ArrayList<String> j0;
    public ArrayList<Bundle> k0;
    public ArrayList<FragmentManager.LaunchedFragmentInfo> l0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<FragmentManagerState> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public FragmentManagerState createFromParcel(Parcel parcel) {
            return new FragmentManagerState(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public FragmentManagerState[] newArray(int i) {
            return new FragmentManagerState[i];
        }
    }

    public FragmentManagerState() {
        this.i0 = null;
        this.j0 = new ArrayList<>();
        this.k0 = new ArrayList<>();
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.a);
        parcel.writeStringList(this.f0);
        parcel.writeTypedArray(this.g0, i);
        parcel.writeInt(this.h0);
        parcel.writeString(this.i0);
        parcel.writeStringList(this.j0);
        parcel.writeTypedList(this.k0);
        parcel.writeTypedList(this.l0);
    }

    public FragmentManagerState(Parcel parcel) {
        this.i0 = null;
        this.j0 = new ArrayList<>();
        this.k0 = new ArrayList<>();
        this.a = parcel.createTypedArrayList(FragmentState.CREATOR);
        this.f0 = parcel.createStringArrayList();
        this.g0 = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
        this.h0 = parcel.readInt();
        this.i0 = parcel.readString();
        this.j0 = parcel.createStringArrayList();
        this.k0 = parcel.createTypedArrayList(Bundle.CREATOR);
        this.l0 = parcel.createTypedArrayList(FragmentManager.LaunchedFragmentInfo.CREATOR);
    }
}
