package androidx.fragment.app;

import androidx.lifecycle.l;

/* compiled from: FragmentViewModelLazy.kt */
/* loaded from: classes.dex */
public final class FragmentViewModelLazyKt {
    public static final <VM extends dj4> sy1<VM> a(Fragment fragment, qw1<VM> qw1Var, rc1<? extends gj4> rc1Var, rc1<? extends l.b> rc1Var2) {
        fs1.g(fragment, "$this$createViewModelLazy");
        fs1.g(qw1Var, "viewModelClass");
        fs1.g(rc1Var, "storeProducer");
        if (rc1Var2 == null) {
            rc1Var2 = new FragmentViewModelLazyKt$createViewModelLazy$factoryPromise$1(fragment);
        }
        return new fj4(qw1Var, rc1Var, rc1Var2);
    }
}
