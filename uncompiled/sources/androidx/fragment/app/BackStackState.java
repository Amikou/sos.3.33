package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.fragment.app.j;
import androidx.lifecycle.Lifecycle;
import java.util.ArrayList;

/* JADX INFO: Access modifiers changed from: package-private */
@SuppressLint({"BanParcelableUsage"})
/* loaded from: classes.dex */
public final class BackStackState implements Parcelable {
    public static final Parcelable.Creator<BackStackState> CREATOR = new a();
    public final int[] a;
    public final ArrayList<String> f0;
    public final int[] g0;
    public final int[] h0;
    public final int i0;
    public final String j0;
    public final int k0;
    public final int l0;
    public final CharSequence m0;
    public final int n0;
    public final CharSequence o0;
    public final ArrayList<String> p0;
    public final ArrayList<String> q0;
    public final boolean r0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<BackStackState> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public BackStackState createFromParcel(Parcel parcel) {
            return new BackStackState(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public BackStackState[] newArray(int i) {
            return new BackStackState[i];
        }
    }

    public BackStackState(androidx.fragment.app.a aVar) {
        int size = aVar.a.size();
        this.a = new int[size * 5];
        if (aVar.g) {
            this.f0 = new ArrayList<>(size);
            this.g0 = new int[size];
            this.h0 = new int[size];
            int i = 0;
            int i2 = 0;
            while (i < size) {
                j.a aVar2 = aVar.a.get(i);
                int i3 = i2 + 1;
                this.a[i2] = aVar2.a;
                ArrayList<String> arrayList = this.f0;
                Fragment fragment = aVar2.b;
                arrayList.add(fragment != null ? fragment.mWho : null);
                int[] iArr = this.a;
                int i4 = i3 + 1;
                iArr[i3] = aVar2.c;
                int i5 = i4 + 1;
                iArr[i4] = aVar2.d;
                int i6 = i5 + 1;
                iArr[i5] = aVar2.e;
                iArr[i6] = aVar2.f;
                this.g0[i] = aVar2.g.ordinal();
                this.h0[i] = aVar2.h.ordinal();
                i++;
                i2 = i6 + 1;
            }
            this.i0 = aVar.f;
            this.j0 = aVar.i;
            this.k0 = aVar.t;
            this.l0 = aVar.j;
            this.m0 = aVar.k;
            this.n0 = aVar.l;
            this.o0 = aVar.m;
            this.p0 = aVar.n;
            this.q0 = aVar.o;
            this.r0 = aVar.p;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    public androidx.fragment.app.a a(FragmentManager fragmentManager) {
        androidx.fragment.app.a aVar = new androidx.fragment.app.a(fragmentManager);
        int i = 0;
        int i2 = 0;
        while (i < this.a.length) {
            j.a aVar2 = new j.a();
            int i3 = i + 1;
            aVar2.a = this.a[i];
            if (FragmentManager.H0(2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Instantiate ");
                sb.append(aVar);
                sb.append(" op #");
                sb.append(i2);
                sb.append(" base fragment #");
                sb.append(this.a[i3]);
            }
            String str = this.f0.get(i2);
            if (str != null) {
                aVar2.b = fragmentManager.i0(str);
            } else {
                aVar2.b = null;
            }
            aVar2.g = Lifecycle.State.values()[this.g0[i2]];
            aVar2.h = Lifecycle.State.values()[this.h0[i2]];
            int[] iArr = this.a;
            int i4 = i3 + 1;
            int i5 = iArr[i3];
            aVar2.c = i5;
            int i6 = i4 + 1;
            int i7 = iArr[i4];
            aVar2.d = i7;
            int i8 = i6 + 1;
            int i9 = iArr[i6];
            aVar2.e = i9;
            int i10 = iArr[i8];
            aVar2.f = i10;
            aVar.b = i5;
            aVar.c = i7;
            aVar.d = i9;
            aVar.e = i10;
            aVar.f(aVar2);
            i2++;
            i = i8 + 1;
        }
        aVar.f = this.i0;
        aVar.i = this.j0;
        aVar.t = this.k0;
        aVar.g = true;
        aVar.j = this.l0;
        aVar.k = this.m0;
        aVar.l = this.n0;
        aVar.m = this.o0;
        aVar.n = this.p0;
        aVar.o = this.q0;
        aVar.p = this.r0;
        aVar.y(1);
        return aVar;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(this.a);
        parcel.writeStringList(this.f0);
        parcel.writeIntArray(this.g0);
        parcel.writeIntArray(this.h0);
        parcel.writeInt(this.i0);
        parcel.writeString(this.j0);
        parcel.writeInt(this.k0);
        parcel.writeInt(this.l0);
        TextUtils.writeToParcel(this.m0, parcel, 0);
        parcel.writeInt(this.n0);
        TextUtils.writeToParcel(this.o0, parcel, 0);
        parcel.writeStringList(this.p0);
        parcel.writeStringList(this.q0);
        parcel.writeInt(this.r0 ? 1 : 0);
    }

    public BackStackState(Parcel parcel) {
        this.a = parcel.createIntArray();
        this.f0 = parcel.createStringArrayList();
        this.g0 = parcel.createIntArray();
        this.h0 = parcel.createIntArray();
        this.i0 = parcel.readInt();
        this.j0 = parcel.readString();
        this.k0 = parcel.readInt();
        this.l0 = parcel.readInt();
        this.m0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.n0 = parcel.readInt();
        this.o0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.p0 = parcel.createStringArrayList();
        this.q0 = parcel.createStringArrayList();
        this.r0 = parcel.readInt() != 0;
    }
}
