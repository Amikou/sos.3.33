package androidx.fragment.app;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: FragmentTransition.java */
/* loaded from: classes.dex */
public class k {
    public static final int[] a = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};
    public static final mb1 b;
    public static final mb1 c;

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ g a;
        public final /* synthetic */ Fragment f0;
        public final /* synthetic */ tv g0;

        public a(g gVar, Fragment fragment, tv tvVar) {
            this.a = gVar;
            this.f0 = fragment;
            this.g0 = tvVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.b(this.f0, this.g0);
        }
    }

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ ArrayList a;

        public b(ArrayList arrayList) {
            this.a = arrayList;
        }

        @Override // java.lang.Runnable
        public void run() {
            k.A(this.a, 4);
        }
    }

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public class c implements Runnable {
        public final /* synthetic */ g a;
        public final /* synthetic */ Fragment f0;
        public final /* synthetic */ tv g0;

        public c(g gVar, Fragment fragment, tv tvVar) {
            this.a = gVar;
            this.f0 = fragment;
            this.g0 = tvVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.b(this.f0, this.g0);
        }
    }

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public class d implements Runnable {
        public final /* synthetic */ Object a;
        public final /* synthetic */ mb1 f0;
        public final /* synthetic */ View g0;
        public final /* synthetic */ Fragment h0;
        public final /* synthetic */ ArrayList i0;
        public final /* synthetic */ ArrayList j0;
        public final /* synthetic */ ArrayList k0;
        public final /* synthetic */ Object l0;

        public d(Object obj, mb1 mb1Var, View view, Fragment fragment, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, Object obj2) {
            this.a = obj;
            this.f0 = mb1Var;
            this.g0 = view;
            this.h0 = fragment;
            this.i0 = arrayList;
            this.j0 = arrayList2;
            this.k0 = arrayList3;
            this.l0 = obj2;
        }

        @Override // java.lang.Runnable
        public void run() {
            Object obj = this.a;
            if (obj != null) {
                this.f0.p(obj, this.g0);
                this.j0.addAll(k.k(this.f0, this.a, this.h0, this.i0, this.g0));
            }
            if (this.k0 != null) {
                if (this.l0 != null) {
                    ArrayList<View> arrayList = new ArrayList<>();
                    arrayList.add(this.g0);
                    this.f0.q(this.l0, this.k0, arrayList);
                }
                this.k0.clear();
                this.k0.add(this.g0);
            }
        }
    }

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public class e implements Runnable {
        public final /* synthetic */ Fragment a;
        public final /* synthetic */ Fragment f0;
        public final /* synthetic */ boolean g0;
        public final /* synthetic */ rh h0;
        public final /* synthetic */ View i0;
        public final /* synthetic */ mb1 j0;
        public final /* synthetic */ Rect k0;

        public e(Fragment fragment, Fragment fragment2, boolean z, rh rhVar, View view, mb1 mb1Var, Rect rect) {
            this.a = fragment;
            this.f0 = fragment2;
            this.g0 = z;
            this.h0 = rhVar;
            this.i0 = view;
            this.j0 = mb1Var;
            this.k0 = rect;
        }

        @Override // java.lang.Runnable
        public void run() {
            k.f(this.a, this.f0, this.g0, this.h0, false);
            View view = this.i0;
            if (view != null) {
                this.j0.k(view, this.k0);
            }
        }
    }

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public class f implements Runnable {
        public final /* synthetic */ mb1 a;
        public final /* synthetic */ rh f0;
        public final /* synthetic */ Object g0;
        public final /* synthetic */ h h0;
        public final /* synthetic */ ArrayList i0;
        public final /* synthetic */ View j0;
        public final /* synthetic */ Fragment k0;
        public final /* synthetic */ Fragment l0;
        public final /* synthetic */ boolean m0;
        public final /* synthetic */ ArrayList n0;
        public final /* synthetic */ Object o0;
        public final /* synthetic */ Rect p0;

        public f(mb1 mb1Var, rh rhVar, Object obj, h hVar, ArrayList arrayList, View view, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList2, Object obj2, Rect rect) {
            this.a = mb1Var;
            this.f0 = rhVar;
            this.g0 = obj;
            this.h0 = hVar;
            this.i0 = arrayList;
            this.j0 = view;
            this.k0 = fragment;
            this.l0 = fragment2;
            this.m0 = z;
            this.n0 = arrayList2;
            this.o0 = obj2;
            this.p0 = rect;
        }

        @Override // java.lang.Runnable
        public void run() {
            rh<String, View> h = k.h(this.a, this.f0, this.g0, this.h0);
            if (h != null) {
                this.i0.addAll(h.values());
                this.i0.add(this.j0);
            }
            k.f(this.k0, this.l0, this.m0, h, false);
            Object obj = this.g0;
            if (obj != null) {
                this.a.A(obj, this.n0, this.i0);
                View s = k.s(h, this.h0, this.o0, this.m0);
                if (s != null) {
                    this.a.k(s, this.p0);
                }
            }
        }
    }

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public interface g {
        void a(Fragment fragment, tv tvVar);

        void b(Fragment fragment, tv tvVar);
    }

    /* compiled from: FragmentTransition.java */
    /* loaded from: classes.dex */
    public static class h {
        public Fragment a;
        public boolean b;
        public androidx.fragment.app.a c;
        public Fragment d;
        public boolean e;
        public androidx.fragment.app.a f;
    }

    static {
        b = Build.VERSION.SDK_INT >= 21 ? new lb1() : null;
        c = w();
    }

    public static void A(ArrayList<View> arrayList, int i) {
        if (arrayList == null) {
            return;
        }
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            arrayList.get(size).setVisibility(i);
        }
    }

    public static void B(Context context, x91 x91Var, ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z, g gVar) {
        ViewGroup viewGroup;
        SparseArray sparseArray = new SparseArray();
        for (int i3 = i; i3 < i2; i3++) {
            androidx.fragment.app.a aVar = arrayList.get(i3);
            if (arrayList2.get(i3).booleanValue()) {
                e(aVar, sparseArray, z);
            } else {
                c(aVar, sparseArray, z);
            }
        }
        if (sparseArray.size() != 0) {
            View view = new View(context);
            int size = sparseArray.size();
            for (int i4 = 0; i4 < size; i4++) {
                int keyAt = sparseArray.keyAt(i4);
                rh<String, String> d2 = d(keyAt, arrayList, arrayList2, i, i2);
                h hVar = (h) sparseArray.valueAt(i4);
                if (x91Var.d() && (viewGroup = (ViewGroup) x91Var.c(keyAt)) != null) {
                    if (z) {
                        o(viewGroup, hVar, view, d2, gVar);
                    } else {
                        n(viewGroup, hVar, view, d2, gVar);
                    }
                }
            }
        }
    }

    public static boolean C() {
        return (b == null && c == null) ? false : true;
    }

    public static void a(ArrayList<View> arrayList, rh<String, View> rhVar, Collection<String> collection) {
        for (int size = rhVar.size() - 1; size >= 0; size--) {
            View m = rhVar.m(size);
            if (collection.contains(ei4.N(m))) {
                arrayList.add(m);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:30:0x0039, code lost:
        if (r0.mAdded != false) goto L59;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x006f, code lost:
        r9 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x008b, code lost:
        if (r0.mHidden == false) goto L59;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x008d, code lost:
        r9 = true;
     */
    /* JADX WARN: Removed duplicated region for block: B:74:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x00a8 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x00b6  */
    /* JADX WARN: Removed duplicated region for block: B:86:0x00c8 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:91:0x00da A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:97:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void b(androidx.fragment.app.a r8, androidx.fragment.app.j.a r9, android.util.SparseArray<androidx.fragment.app.k.h> r10, boolean r11, boolean r12) {
        /*
            Method dump skipped, instructions count: 229
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.k.b(androidx.fragment.app.a, androidx.fragment.app.j$a, android.util.SparseArray, boolean, boolean):void");
    }

    public static void c(androidx.fragment.app.a aVar, SparseArray<h> sparseArray, boolean z) {
        int size = aVar.a.size();
        for (int i = 0; i < size; i++) {
            b(aVar, aVar.a.get(i), sparseArray, false, z);
        }
    }

    public static rh<String, String> d(int i, ArrayList<androidx.fragment.app.a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        rh<String, String> rhVar = new rh<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            androidx.fragment.app.a aVar = arrayList.get(i4);
            if (aVar.G(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                ArrayList<String> arrayList5 = aVar.n;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        arrayList3 = aVar.n;
                        arrayList4 = aVar.o;
                    } else {
                        ArrayList<String> arrayList6 = aVar.n;
                        arrayList3 = aVar.o;
                        arrayList4 = arrayList6;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = arrayList4.get(i5);
                        String str2 = arrayList3.get(i5);
                        String remove = rhVar.remove(str2);
                        if (remove != null) {
                            rhVar.put(str, remove);
                        } else {
                            rhVar.put(str, str2);
                        }
                    }
                }
            }
        }
        return rhVar;
    }

    public static void e(androidx.fragment.app.a aVar, SparseArray<h> sparseArray, boolean z) {
        if (aVar.r.q0().d()) {
            for (int size = aVar.a.size() - 1; size >= 0; size--) {
                b(aVar, aVar.a.get(size), sparseArray, true, z);
            }
        }
    }

    public static void f(Fragment fragment, Fragment fragment2, boolean z, rh<String, View> rhVar, boolean z2) {
        if (z) {
            fragment2.getEnterTransitionCallback();
        } else {
            fragment.getEnterTransitionCallback();
        }
    }

    public static boolean g(mb1 mb1Var, List<Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!mb1Var.e(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    public static rh<String, View> h(mb1 mb1Var, rh<String, String> rhVar, Object obj, h hVar) {
        ArrayList<String> arrayList;
        Fragment fragment = hVar.a;
        View view = fragment.getView();
        if (!rhVar.isEmpty() && obj != null && view != null) {
            rh<String, View> rhVar2 = new rh<>();
            mb1Var.j(rhVar2, view);
            androidx.fragment.app.a aVar = hVar.c;
            if (hVar.b) {
                fragment.getExitTransitionCallback();
                arrayList = aVar.n;
            } else {
                fragment.getEnterTransitionCallback();
                arrayList = aVar.o;
            }
            if (arrayList != null) {
                rhVar2.o(arrayList);
                rhVar2.o(rhVar.values());
            }
            x(rhVar, rhVar2);
            return rhVar2;
        }
        rhVar.clear();
        return null;
    }

    public static rh<String, View> i(mb1 mb1Var, rh<String, String> rhVar, Object obj, h hVar) {
        ArrayList<String> arrayList;
        if (!rhVar.isEmpty() && obj != null) {
            Fragment fragment = hVar.d;
            rh<String, View> rhVar2 = new rh<>();
            mb1Var.j(rhVar2, fragment.requireView());
            androidx.fragment.app.a aVar = hVar.f;
            if (hVar.e) {
                fragment.getEnterTransitionCallback();
                arrayList = aVar.o;
            } else {
                fragment.getExitTransitionCallback();
                arrayList = aVar.n;
            }
            if (arrayList != null) {
                rhVar2.o(arrayList);
            }
            rhVar.o(rhVar2.keySet());
            return rhVar2;
        }
        rhVar.clear();
        return null;
    }

    public static mb1 j(Fragment fragment, Fragment fragment2) {
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        mb1 mb1Var = b;
        if (mb1Var == null || !g(mb1Var, arrayList)) {
            mb1 mb1Var2 = c;
            if (mb1Var2 == null || !g(mb1Var2, arrayList)) {
                if (mb1Var == null && mb1Var2 == null) {
                    return null;
                }
                throw new IllegalArgumentException("Invalid Transition types");
            }
            return mb1Var2;
        }
        return mb1Var;
    }

    public static ArrayList<View> k(mb1 mb1Var, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj != null) {
            ArrayList<View> arrayList2 = new ArrayList<>();
            View view2 = fragment.getView();
            if (view2 != null) {
                mb1Var.f(arrayList2, view2);
            }
            if (arrayList != null) {
                arrayList2.removeAll(arrayList);
            }
            if (arrayList2.isEmpty()) {
                return arrayList2;
            }
            arrayList2.add(view);
            mb1Var.b(obj, arrayList2);
            return arrayList2;
        }
        return null;
    }

    public static Object l(mb1 mb1Var, ViewGroup viewGroup, View view, rh<String, String> rhVar, h hVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object t;
        rh<String, String> rhVar2;
        Object obj3;
        Rect rect;
        Fragment fragment = hVar.a;
        Fragment fragment2 = hVar.d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hVar.b;
        if (rhVar.isEmpty()) {
            rhVar2 = rhVar;
            t = null;
        } else {
            t = t(mb1Var, fragment, fragment2, z);
            rhVar2 = rhVar;
        }
        rh<String, View> i = i(mb1Var, rhVar2, t, hVar);
        if (rhVar.isEmpty()) {
            obj3 = null;
        } else {
            arrayList.addAll(i.values());
            obj3 = t;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        f(fragment, fragment2, z, i, true);
        if (obj3 != null) {
            rect = new Rect();
            mb1Var.z(obj3, view, arrayList);
            z(mb1Var, obj3, obj2, i, hVar.e, hVar.f);
            if (obj != null) {
                mb1Var.u(obj, rect);
            }
        } else {
            rect = null;
        }
        vm2.a(viewGroup, new f(mb1Var, rhVar, obj3, hVar, arrayList2, view, fragment, fragment2, z, arrayList, obj, rect));
        return obj3;
    }

    public static Object m(mb1 mb1Var, ViewGroup viewGroup, View view, rh<String, String> rhVar, h hVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        View view2;
        Rect rect;
        Fragment fragment = hVar.a;
        Fragment fragment2 = hVar.d;
        if (fragment != null) {
            fragment.requireView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hVar.b;
        Object t = rhVar.isEmpty() ? null : t(mb1Var, fragment, fragment2, z);
        rh<String, View> i = i(mb1Var, rhVar, t, hVar);
        rh<String, View> h2 = h(mb1Var, rhVar, t, hVar);
        if (rhVar.isEmpty()) {
            if (i != null) {
                i.clear();
            }
            if (h2 != null) {
                h2.clear();
            }
            obj3 = null;
        } else {
            a(arrayList, i, rhVar.keySet());
            a(arrayList2, h2, rhVar.values());
            obj3 = t;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        f(fragment, fragment2, z, i, true);
        if (obj3 != null) {
            arrayList2.add(view);
            mb1Var.z(obj3, view, arrayList);
            z(mb1Var, obj3, obj2, i, hVar.e, hVar.f);
            Rect rect2 = new Rect();
            View s = s(h2, hVar, obj, z);
            if (s != null) {
                mb1Var.u(obj, rect2);
            }
            rect = rect2;
            view2 = s;
        } else {
            view2 = null;
            rect = null;
        }
        vm2.a(viewGroup, new e(fragment, fragment2, z, h2, view2, mb1Var, rect));
        return obj3;
    }

    public static void n(ViewGroup viewGroup, h hVar, View view, rh<String, String> rhVar, g gVar) {
        Object obj;
        Fragment fragment = hVar.a;
        Fragment fragment2 = hVar.d;
        mb1 j = j(fragment2, fragment);
        if (j == null) {
            return;
        }
        boolean z = hVar.b;
        boolean z2 = hVar.e;
        Object q = q(j, fragment, z);
        Object r = r(j, fragment2, z2);
        ArrayList arrayList = new ArrayList();
        ArrayList<View> arrayList2 = new ArrayList<>();
        Object l = l(j, viewGroup, view, rhVar, hVar, arrayList, arrayList2, q, r);
        if (q == null && l == null) {
            obj = r;
            if (obj == null) {
                return;
            }
        } else {
            obj = r;
        }
        ArrayList<View> k = k(j, obj, fragment2, arrayList, view);
        Object obj2 = (k == null || k.isEmpty()) ? null : null;
        j.a(q, view);
        Object u = u(j, q, obj2, l, fragment, hVar.b);
        if (fragment2 != null && k != null && (k.size() > 0 || arrayList.size() > 0)) {
            tv tvVar = new tv();
            gVar.a(fragment2, tvVar);
            j.w(fragment2, u, tvVar, new c(gVar, fragment2, tvVar));
        }
        if (u != null) {
            ArrayList<View> arrayList3 = new ArrayList<>();
            j.t(u, q, arrayList3, obj2, k, l, arrayList2);
            y(j, viewGroup, fragment, view, arrayList2, q, arrayList3, obj2, k);
            j.x(viewGroup, arrayList2, rhVar);
            j.c(viewGroup, u);
            j.s(viewGroup, arrayList2, rhVar);
        }
    }

    public static void o(ViewGroup viewGroup, h hVar, View view, rh<String, String> rhVar, g gVar) {
        Object obj;
        Fragment fragment = hVar.a;
        Fragment fragment2 = hVar.d;
        mb1 j = j(fragment2, fragment);
        if (j == null) {
            return;
        }
        boolean z = hVar.b;
        boolean z2 = hVar.e;
        ArrayList<View> arrayList = new ArrayList<>();
        ArrayList<View> arrayList2 = new ArrayList<>();
        Object q = q(j, fragment, z);
        Object r = r(j, fragment2, z2);
        Object m = m(j, viewGroup, view, rhVar, hVar, arrayList2, arrayList, q, r);
        if (q == null && m == null) {
            obj = r;
            if (obj == null) {
                return;
            }
        } else {
            obj = r;
        }
        ArrayList<View> k = k(j, obj, fragment2, arrayList2, view);
        ArrayList<View> k2 = k(j, q, fragment, arrayList, view);
        A(k2, 4);
        Object u = u(j, q, obj, m, fragment, z);
        if (fragment2 != null && k != null && (k.size() > 0 || arrayList2.size() > 0)) {
            tv tvVar = new tv();
            gVar.a(fragment2, tvVar);
            j.w(fragment2, u, tvVar, new a(gVar, fragment2, tvVar));
        }
        if (u != null) {
            v(j, obj, fragment2, k);
            ArrayList<String> o = j.o(arrayList);
            j.t(u, q, k2, obj, k, m, arrayList);
            j.c(viewGroup, u);
            j.y(viewGroup, arrayList2, arrayList, o, rhVar);
            A(k2, 0);
            j.A(m, arrayList2, arrayList);
        }
    }

    public static h p(h hVar, SparseArray<h> sparseArray, int i) {
        if (hVar == null) {
            h hVar2 = new h();
            sparseArray.put(i, hVar2);
            return hVar2;
        }
        return hVar;
    }

    public static Object q(mb1 mb1Var, Fragment fragment, boolean z) {
        Object enterTransition;
        if (fragment == null) {
            return null;
        }
        if (z) {
            enterTransition = fragment.getReenterTransition();
        } else {
            enterTransition = fragment.getEnterTransition();
        }
        return mb1Var.g(enterTransition);
    }

    public static Object r(mb1 mb1Var, Fragment fragment, boolean z) {
        Object exitTransition;
        if (fragment == null) {
            return null;
        }
        if (z) {
            exitTransition = fragment.getReturnTransition();
        } else {
            exitTransition = fragment.getExitTransition();
        }
        return mb1Var.g(exitTransition);
    }

    public static View s(rh<String, View> rhVar, h hVar, Object obj, boolean z) {
        ArrayList<String> arrayList;
        String str;
        androidx.fragment.app.a aVar = hVar.c;
        if (obj == null || rhVar == null || (arrayList = aVar.n) == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = aVar.n.get(0);
        } else {
            str = aVar.o.get(0);
        }
        return rhVar.get(str);
    }

    public static Object t(mb1 mb1Var, Fragment fragment, Fragment fragment2, boolean z) {
        Object sharedElementEnterTransition;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            sharedElementEnterTransition = fragment2.getSharedElementReturnTransition();
        } else {
            sharedElementEnterTransition = fragment.getSharedElementEnterTransition();
        }
        return mb1Var.B(mb1Var.g(sharedElementEnterTransition));
    }

    public static Object u(mb1 mb1Var, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || fragment == null) {
            z2 = true;
        } else if (z) {
            z2 = fragment.getAllowReturnTransitionOverlap();
        } else {
            z2 = fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return mb1Var.n(obj2, obj, obj3);
        }
        return mb1Var.m(obj2, obj, obj3);
    }

    public static void v(mb1 mb1Var, Object obj, Fragment fragment, ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            mb1Var.r(obj, fragment.getView(), arrayList);
            vm2.a(fragment.mContainer, new b(arrayList));
        }
    }

    public static mb1 w() {
        try {
            return (mb1) androidx.transition.b.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    public static void x(rh<String, String> rhVar, rh<String, View> rhVar2) {
        for (int size = rhVar.size() - 1; size >= 0; size--) {
            if (!rhVar2.containsKey(rhVar.m(size))) {
                rhVar.k(size);
            }
        }
    }

    public static void y(mb1 mb1Var, ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        vm2.a(viewGroup, new d(obj, mb1Var, view, fragment, arrayList, arrayList2, arrayList3, obj2));
    }

    public static void z(mb1 mb1Var, Object obj, Object obj2, rh<String, View> rhVar, boolean z, androidx.fragment.app.a aVar) {
        String str;
        ArrayList<String> arrayList = aVar.n;
        if (arrayList == null || arrayList.isEmpty()) {
            return;
        }
        if (z) {
            str = aVar.o.get(0);
        } else {
            str = aVar.n.get(0);
        }
        View view = rhVar.get(str);
        mb1Var.v(obj, view);
        if (obj2 != null) {
            mb1Var.v(obj2, view);
        }
    }
}
