package androidx.media;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.browse.MediaBrowser;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.service.media.MediaBrowserService;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes.dex */
public abstract class MediaBrowserServiceCompat extends Service {
    public static final boolean l0 = Log.isLoggable("MBServiceCompat", 3);
    public g a;
    public f i0;
    public MediaSessionCompat.Token k0;
    public final f f0 = new f("android.media.session.MediaController", -1, -1, null, null);
    public final ArrayList<f> g0 = new ArrayList<>();
    public final rh<IBinder, f> h0 = new rh<>();
    public final r j0 = new r();

    /* loaded from: classes.dex */
    public class a extends m<List<MediaBrowserCompat.MediaItem>> {
        public final /* synthetic */ f f;
        public final /* synthetic */ String g;
        public final /* synthetic */ Bundle h;
        public final /* synthetic */ Bundle i;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Object obj, f fVar, String str, Bundle bundle, Bundle bundle2) {
            super(obj);
            this.f = fVar;
            this.g = str;
            this.h = bundle;
            this.i = bundle2;
        }

        @Override // androidx.media.MediaBrowserServiceCompat.m
        /* renamed from: h */
        public void d(List<MediaBrowserCompat.MediaItem> list) {
            if (MediaBrowserServiceCompat.this.h0.get(this.f.d.asBinder()) != this.f) {
                if (MediaBrowserServiceCompat.l0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Not sending onLoadChildren result for connection that has been disconnected. pkg=");
                    sb.append(this.f.a);
                    sb.append(" id=");
                    sb.append(this.g);
                    return;
                }
                return;
            }
            if ((a() & 1) != 0) {
                list = MediaBrowserServiceCompat.this.b(list, this.h);
            }
            try {
                this.f.d.a(this.g, list, this.h, this.i);
            } catch (RemoteException unused) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Calling onLoadChildren() failed for id=");
                sb2.append(this.g);
                sb2.append(" package=");
                sb2.append(this.f.a);
            }
        }
    }

    /* loaded from: classes.dex */
    public class b extends m<MediaBrowserCompat.MediaItem> {
        public final /* synthetic */ ResultReceiver f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(MediaBrowserServiceCompat mediaBrowserServiceCompat, Object obj, ResultReceiver resultReceiver) {
            super(obj);
            this.f = resultReceiver;
        }

        @Override // androidx.media.MediaBrowserServiceCompat.m
        /* renamed from: h */
        public void d(MediaBrowserCompat.MediaItem mediaItem) {
            if ((a() & 2) != 0) {
                this.f.b(-1, null);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("media_item", mediaItem);
            this.f.b(0, bundle);
        }
    }

    /* loaded from: classes.dex */
    public class c extends m<List<MediaBrowserCompat.MediaItem>> {
        public final /* synthetic */ ResultReceiver f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(MediaBrowserServiceCompat mediaBrowserServiceCompat, Object obj, ResultReceiver resultReceiver) {
            super(obj);
            this.f = resultReceiver;
        }

        @Override // androidx.media.MediaBrowserServiceCompat.m
        /* renamed from: h */
        public void d(List<MediaBrowserCompat.MediaItem> list) {
            if ((a() & 4) == 0 && list != null) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArray("search_results", (Parcelable[]) list.toArray(new MediaBrowserCompat.MediaItem[0]));
                this.f.b(0, bundle);
                return;
            }
            this.f.b(-1, null);
        }
    }

    /* loaded from: classes.dex */
    public class d extends m<Bundle> {
        public final /* synthetic */ ResultReceiver f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(MediaBrowserServiceCompat mediaBrowserServiceCompat, Object obj, ResultReceiver resultReceiver) {
            super(obj);
            this.f = resultReceiver;
        }

        @Override // androidx.media.MediaBrowserServiceCompat.m
        public void c(Bundle bundle) {
            this.f.b(-1, bundle);
        }

        @Override // androidx.media.MediaBrowserServiceCompat.m
        /* renamed from: h */
        public void d(Bundle bundle) {
            this.f.b(0, bundle);
        }
    }

    /* loaded from: classes.dex */
    public static final class e {
        public final String a;
        public final Bundle b;

        public e(String str, Bundle bundle) {
            if (str != null) {
                this.a = str;
                this.b = bundle;
                return;
            }
            throw new IllegalArgumentException("The root id in BrowserRoot cannot be null. Use null for BrowserRoot instead");
        }

        public Bundle c() {
            return this.b;
        }

        public String d() {
            return this.a;
        }
    }

    /* loaded from: classes.dex */
    public class f implements IBinder.DeathRecipient {
        public final String a;
        public final int b;
        public final int c;
        public final p d;
        public final HashMap<String, List<jp2<IBinder, Bundle>>> e = new HashMap<>();
        public e f;

        /* loaded from: classes.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = f.this;
                MediaBrowserServiceCompat.this.h0.remove(fVar.d.asBinder());
            }
        }

        public f(String str, int i, int i2, Bundle bundle, p pVar) {
            this.a = str;
            this.b = i;
            this.c = i2;
            new n62(str, i, i2);
            this.d = pVar;
        }

        @Override // android.os.IBinder.DeathRecipient
        public void binderDied() {
            MediaBrowserServiceCompat.this.j0.post(new a());
        }
    }

    /* loaded from: classes.dex */
    public interface g {
        void a();

        IBinder b(Intent intent);
    }

    /* loaded from: classes.dex */
    public class h implements g {
        public final List<Bundle> a = new ArrayList();
        public MediaBrowserService b;
        public Messenger c;

        /* loaded from: classes.dex */
        public class a extends m<List<MediaBrowserCompat.MediaItem>> {
            public final /* synthetic */ n f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(h hVar, Object obj, n nVar) {
                super(obj);
                this.f = nVar;
            }

            @Override // androidx.media.MediaBrowserServiceCompat.m
            /* renamed from: h */
            public void d(List<MediaBrowserCompat.MediaItem> list) {
                ArrayList arrayList;
                if (list != null) {
                    arrayList = new ArrayList();
                    for (MediaBrowserCompat.MediaItem mediaItem : list) {
                        Parcel obtain = Parcel.obtain();
                        mediaItem.writeToParcel(obtain, 0);
                        arrayList.add(obtain);
                    }
                } else {
                    arrayList = null;
                }
                this.f.b(arrayList);
            }
        }

        /* loaded from: classes.dex */
        public class b extends MediaBrowserService {
            public b(Context context) {
                attachBaseContext(context);
            }

            @Override // android.service.media.MediaBrowserService
            @SuppressLint({"SyntheticAccessor"})
            public MediaBrowserService.BrowserRoot onGetRoot(String str, int i, Bundle bundle) {
                MediaSessionCompat.a(bundle);
                e c = h.this.c(str, i, bundle == null ? null : new Bundle(bundle));
                if (c == null) {
                    return null;
                }
                return new MediaBrowserService.BrowserRoot(c.a, c.b);
            }

            @Override // android.service.media.MediaBrowserService
            public void onLoadChildren(String str, MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result) {
                h.this.d(str, new n<>(result));
            }
        }

        public h() {
        }

        @Override // androidx.media.MediaBrowserServiceCompat.g
        public void a() {
            b bVar = new b(MediaBrowserServiceCompat.this);
            this.b = bVar;
            bVar.onCreate();
        }

        @Override // androidx.media.MediaBrowserServiceCompat.g
        public IBinder b(Intent intent) {
            return this.b.onBind(intent);
        }

        public e c(String str, int i, Bundle bundle) {
            int i2;
            Bundle bundle2;
            if (bundle == null || bundle.getInt("extra_client_version", 0) == 0) {
                i2 = -1;
                bundle2 = null;
            } else {
                bundle.remove("extra_client_version");
                this.c = new Messenger(MediaBrowserServiceCompat.this.j0);
                bundle2 = new Bundle();
                bundle2.putInt("extra_service_version", 2);
                bs.b(bundle2, "extra_messenger", this.c.getBinder());
                MediaSessionCompat.Token token = MediaBrowserServiceCompat.this.k0;
                if (token != null) {
                    android.support.v4.media.session.b c = token.c();
                    bs.b(bundle2, "extra_session_binder", c == null ? null : c.asBinder());
                } else {
                    this.a.add(bundle2);
                }
                int i3 = bundle.getInt("extra_calling_pid", -1);
                bundle.remove("extra_calling_pid");
                i2 = i3;
            }
            f fVar = new f(str, i2, i, bundle, null);
            MediaBrowserServiceCompat mediaBrowserServiceCompat = MediaBrowserServiceCompat.this;
            mediaBrowserServiceCompat.i0 = fVar;
            e e = mediaBrowserServiceCompat.e(str, i, bundle);
            MediaBrowserServiceCompat mediaBrowserServiceCompat2 = MediaBrowserServiceCompat.this;
            mediaBrowserServiceCompat2.i0 = null;
            if (e == null) {
                return null;
            }
            if (this.c != null) {
                mediaBrowserServiceCompat2.g0.add(fVar);
            }
            if (bundle2 == null) {
                bundle2 = e.c();
            } else if (e.c() != null) {
                bundle2.putAll(e.c());
            }
            return new e(e.d(), bundle2);
        }

        public void d(String str, n<List<Parcel>> nVar) {
            a aVar = new a(this, str, nVar);
            MediaBrowserServiceCompat mediaBrowserServiceCompat = MediaBrowserServiceCompat.this;
            mediaBrowserServiceCompat.i0 = mediaBrowserServiceCompat.f0;
            mediaBrowserServiceCompat.f(str, aVar);
            MediaBrowserServiceCompat.this.i0 = null;
        }
    }

    /* loaded from: classes.dex */
    public class i extends h {

        /* loaded from: classes.dex */
        public class a extends m<MediaBrowserCompat.MediaItem> {
            public final /* synthetic */ n f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(i iVar, Object obj, n nVar) {
                super(obj);
                this.f = nVar;
            }

            @Override // androidx.media.MediaBrowserServiceCompat.m
            /* renamed from: h */
            public void d(MediaBrowserCompat.MediaItem mediaItem) {
                if (mediaItem == null) {
                    this.f.b(null);
                    return;
                }
                Parcel obtain = Parcel.obtain();
                mediaItem.writeToParcel(obtain, 0);
                this.f.b(obtain);
            }
        }

        /* loaded from: classes.dex */
        public class b extends h.b {
            public b(Context context) {
                super(context);
            }

            @Override // android.service.media.MediaBrowserService
            public void onLoadItem(String str, MediaBrowserService.Result<MediaBrowser.MediaItem> result) {
                i.this.e(str, new n<>(result));
            }
        }

        public i() {
            super();
        }

        @Override // androidx.media.MediaBrowserServiceCompat.h, androidx.media.MediaBrowserServiceCompat.g
        public void a() {
            b bVar = new b(MediaBrowserServiceCompat.this);
            this.b = bVar;
            bVar.onCreate();
        }

        public void e(String str, n<Parcel> nVar) {
            a aVar = new a(this, str, nVar);
            MediaBrowserServiceCompat mediaBrowserServiceCompat = MediaBrowserServiceCompat.this;
            mediaBrowserServiceCompat.i0 = mediaBrowserServiceCompat.f0;
            mediaBrowserServiceCompat.h(str, aVar);
            MediaBrowserServiceCompat.this.i0 = null;
        }
    }

    /* loaded from: classes.dex */
    public class j extends i {

        /* loaded from: classes.dex */
        public class a extends m<List<MediaBrowserCompat.MediaItem>> {
            public final /* synthetic */ n f;
            public final /* synthetic */ Bundle g;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(Object obj, n nVar, Bundle bundle) {
                super(obj);
                this.f = nVar;
                this.g = bundle;
            }

            @Override // androidx.media.MediaBrowserServiceCompat.m
            /* renamed from: h */
            public void d(List<MediaBrowserCompat.MediaItem> list) {
                if (list == null) {
                    this.f.b(null);
                    return;
                }
                if ((a() & 1) != 0) {
                    list = MediaBrowserServiceCompat.this.b(list, this.g);
                }
                ArrayList arrayList = new ArrayList();
                for (MediaBrowserCompat.MediaItem mediaItem : list) {
                    Parcel obtain = Parcel.obtain();
                    mediaItem.writeToParcel(obtain, 0);
                    arrayList.add(obtain);
                }
                this.f.b(arrayList);
            }
        }

        /* loaded from: classes.dex */
        public class b extends i.b {
            public b(Context context) {
                super(context);
            }

            @Override // android.service.media.MediaBrowserService
            public void onLoadChildren(String str, MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result, Bundle bundle) {
                MediaSessionCompat.a(bundle);
                j jVar = j.this;
                MediaBrowserServiceCompat mediaBrowserServiceCompat = MediaBrowserServiceCompat.this;
                mediaBrowserServiceCompat.i0 = mediaBrowserServiceCompat.f0;
                jVar.f(str, new n<>(result), bundle);
                MediaBrowserServiceCompat.this.i0 = null;
            }
        }

        public j() {
            super();
        }

        @Override // androidx.media.MediaBrowserServiceCompat.i, androidx.media.MediaBrowserServiceCompat.h, androidx.media.MediaBrowserServiceCompat.g
        public void a() {
            b bVar = new b(MediaBrowserServiceCompat.this);
            this.b = bVar;
            bVar.onCreate();
        }

        public void f(String str, n<List<Parcel>> nVar, Bundle bundle) {
            a aVar = new a(str, nVar, bundle);
            MediaBrowserServiceCompat mediaBrowserServiceCompat = MediaBrowserServiceCompat.this;
            mediaBrowserServiceCompat.i0 = mediaBrowserServiceCompat.f0;
            mediaBrowserServiceCompat.g(str, aVar, bundle);
            MediaBrowserServiceCompat.this.i0 = null;
        }
    }

    /* loaded from: classes.dex */
    public class k extends j {
        public k(MediaBrowserServiceCompat mediaBrowserServiceCompat) {
            super();
        }
    }

    /* loaded from: classes.dex */
    public class l implements g {
        public Messenger a;

        public l() {
        }

        @Override // androidx.media.MediaBrowserServiceCompat.g
        public void a() {
            this.a = new Messenger(MediaBrowserServiceCompat.this.j0);
        }

        @Override // androidx.media.MediaBrowserServiceCompat.g
        public IBinder b(Intent intent) {
            if ("android.media.browse.MediaBrowserService".equals(intent.getAction())) {
                return this.a.getBinder();
            }
            return null;
        }
    }

    /* loaded from: classes.dex */
    public static class m<T> {
        public final Object a;
        public boolean b;
        public boolean c;
        public boolean d;
        public int e;

        public m(Object obj) {
            this.a = obj;
        }

        public int a() {
            return this.e;
        }

        public boolean b() {
            return this.b || this.c || this.d;
        }

        public void c(Bundle bundle) {
            throw new UnsupportedOperationException("It is not supported to send an error for " + this.a);
        }

        public void d(T t) {
            throw null;
        }

        public void e(Bundle bundle) {
            if (!this.c && !this.d) {
                this.d = true;
                c(bundle);
                return;
            }
            throw new IllegalStateException("sendError() called when either sendResult() or sendError() had already been called for: " + this.a);
        }

        public void f(T t) {
            if (!this.c && !this.d) {
                this.c = true;
                d(t);
                return;
            }
            throw new IllegalStateException("sendResult() called when either sendResult() or sendError() had already been called for: " + this.a);
        }

        public void g(int i) {
            this.e = i;
        }
    }

    /* loaded from: classes.dex */
    public static class n<T> {
        public MediaBrowserService.Result a;

        public n(MediaBrowserService.Result result) {
            this.a = result;
        }

        public List<MediaBrowser.MediaItem> a(List<Parcel> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Parcel parcel : list) {
                parcel.setDataPosition(0);
                arrayList.add((MediaBrowser.MediaItem) MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return arrayList;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public void b(T t) {
            if (t instanceof List) {
                this.a.sendResult(a((List) t));
            } else if (t instanceof Parcel) {
                Parcel parcel = (Parcel) t;
                parcel.setDataPosition(0);
                this.a.sendResult(MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            } else {
                this.a.sendResult(null);
            }
        }
    }

    /* loaded from: classes.dex */
    public class o {

        /* loaded from: classes.dex */
        public class a implements Runnable {
            public final /* synthetic */ p a;
            public final /* synthetic */ String f0;
            public final /* synthetic */ int g0;
            public final /* synthetic */ int h0;
            public final /* synthetic */ Bundle i0;

            public a(p pVar, String str, int i, int i2, Bundle bundle) {
                this.a = pVar;
                this.f0 = str;
                this.g0 = i;
                this.h0 = i2;
                this.i0 = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                IBinder asBinder = this.a.asBinder();
                MediaBrowserServiceCompat.this.h0.remove(asBinder);
                f fVar = new f(this.f0, this.g0, this.h0, this.i0, this.a);
                MediaBrowserServiceCompat mediaBrowserServiceCompat = MediaBrowserServiceCompat.this;
                mediaBrowserServiceCompat.i0 = fVar;
                e e = mediaBrowserServiceCompat.e(this.f0, this.h0, this.i0);
                fVar.f = e;
                MediaBrowserServiceCompat mediaBrowserServiceCompat2 = MediaBrowserServiceCompat.this;
                mediaBrowserServiceCompat2.i0 = null;
                if (e == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("No root for client ");
                    sb.append(this.f0);
                    sb.append(" from service ");
                    sb.append(a.class.getName());
                    try {
                        this.a.b();
                        return;
                    } catch (RemoteException unused) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Calling onConnectFailed() failed. Ignoring. pkg=");
                        sb2.append(this.f0);
                        return;
                    }
                }
                try {
                    mediaBrowserServiceCompat2.h0.put(asBinder, fVar);
                    asBinder.linkToDeath(fVar, 0);
                    if (MediaBrowserServiceCompat.this.k0 != null) {
                        this.a.c(fVar.f.d(), MediaBrowserServiceCompat.this.k0, fVar.f.c());
                    }
                } catch (RemoteException unused2) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Calling onConnect() failed. Dropping client. pkg=");
                    sb3.append(this.f0);
                    MediaBrowserServiceCompat.this.h0.remove(asBinder);
                }
            }
        }

        /* loaded from: classes.dex */
        public class b implements Runnable {
            public final /* synthetic */ p a;

            public b(p pVar) {
                this.a = pVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                f remove = MediaBrowserServiceCompat.this.h0.remove(this.a.asBinder());
                if (remove != null) {
                    remove.d.asBinder().unlinkToDeath(remove, 0);
                }
            }
        }

        /* loaded from: classes.dex */
        public class c implements Runnable {
            public final /* synthetic */ p a;
            public final /* synthetic */ String f0;
            public final /* synthetic */ IBinder g0;
            public final /* synthetic */ Bundle h0;

            public c(p pVar, String str, IBinder iBinder, Bundle bundle) {
                this.a = pVar;
                this.f0 = str;
                this.g0 = iBinder;
                this.h0 = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = MediaBrowserServiceCompat.this.h0.get(this.a.asBinder());
                if (fVar == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("addSubscription for callback that isn't registered id=");
                    sb.append(this.f0);
                    return;
                }
                MediaBrowserServiceCompat.this.a(this.f0, fVar, this.g0, this.h0);
            }
        }

        /* loaded from: classes.dex */
        public class d implements Runnable {
            public final /* synthetic */ p a;
            public final /* synthetic */ String f0;
            public final /* synthetic */ IBinder g0;

            public d(p pVar, String str, IBinder iBinder) {
                this.a = pVar;
                this.f0 = str;
                this.g0 = iBinder;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = MediaBrowserServiceCompat.this.h0.get(this.a.asBinder());
                if (fVar == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("removeSubscription for callback that isn't registered id=");
                    sb.append(this.f0);
                } else if (MediaBrowserServiceCompat.this.p(this.f0, fVar, this.g0)) {
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("removeSubscription called for ");
                    sb2.append(this.f0);
                    sb2.append(" which is not subscribed");
                }
            }
        }

        /* loaded from: classes.dex */
        public class e implements Runnable {
            public final /* synthetic */ p a;
            public final /* synthetic */ String f0;
            public final /* synthetic */ ResultReceiver g0;

            public e(p pVar, String str, ResultReceiver resultReceiver) {
                this.a = pVar;
                this.f0 = str;
                this.g0 = resultReceiver;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = MediaBrowserServiceCompat.this.h0.get(this.a.asBinder());
                if (fVar == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("getMediaItem for callback that isn't registered id=");
                    sb.append(this.f0);
                    return;
                }
                MediaBrowserServiceCompat.this.n(this.f0, fVar, this.g0);
            }
        }

        /* loaded from: classes.dex */
        public class f implements Runnable {
            public final /* synthetic */ p a;
            public final /* synthetic */ int f0;
            public final /* synthetic */ String g0;
            public final /* synthetic */ int h0;
            public final /* synthetic */ Bundle i0;

            public f(p pVar, int i, String str, int i2, Bundle bundle) {
                this.a = pVar;
                this.f0 = i;
                this.g0 = str;
                this.h0 = i2;
                this.i0 = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar;
                IBinder asBinder = this.a.asBinder();
                MediaBrowserServiceCompat.this.h0.remove(asBinder);
                Iterator<f> it = MediaBrowserServiceCompat.this.g0.iterator();
                while (true) {
                    fVar = null;
                    if (!it.hasNext()) {
                        break;
                    }
                    f next = it.next();
                    if (next.c == this.f0) {
                        if (TextUtils.isEmpty(this.g0) || this.h0 <= 0) {
                            fVar = new f(next.a, next.b, next.c, this.i0, this.a);
                        }
                        it.remove();
                    }
                }
                if (fVar == null) {
                    fVar = new f(this.g0, this.h0, this.f0, this.i0, this.a);
                }
                MediaBrowserServiceCompat.this.h0.put(asBinder, fVar);
                try {
                    asBinder.linkToDeath(fVar, 0);
                } catch (RemoteException unused) {
                }
            }
        }

        /* loaded from: classes.dex */
        public class g implements Runnable {
            public final /* synthetic */ p a;

            public g(p pVar) {
                this.a = pVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                IBinder asBinder = this.a.asBinder();
                f remove = MediaBrowserServiceCompat.this.h0.remove(asBinder);
                if (remove != null) {
                    asBinder.unlinkToDeath(remove, 0);
                }
            }
        }

        /* loaded from: classes.dex */
        public class h implements Runnable {
            public final /* synthetic */ p a;
            public final /* synthetic */ String f0;
            public final /* synthetic */ Bundle g0;
            public final /* synthetic */ ResultReceiver h0;

            public h(p pVar, String str, Bundle bundle, ResultReceiver resultReceiver) {
                this.a = pVar;
                this.f0 = str;
                this.g0 = bundle;
                this.h0 = resultReceiver;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = MediaBrowserServiceCompat.this.h0.get(this.a.asBinder());
                if (fVar == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("search for callback that isn't registered query=");
                    sb.append(this.f0);
                    return;
                }
                MediaBrowserServiceCompat.this.o(this.f0, this.g0, fVar, this.h0);
            }
        }

        /* loaded from: classes.dex */
        public class i implements Runnable {
            public final /* synthetic */ p a;
            public final /* synthetic */ String f0;
            public final /* synthetic */ Bundle g0;
            public final /* synthetic */ ResultReceiver h0;

            public i(p pVar, String str, Bundle bundle, ResultReceiver resultReceiver) {
                this.a = pVar;
                this.f0 = str;
                this.g0 = bundle;
                this.h0 = resultReceiver;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = MediaBrowserServiceCompat.this.h0.get(this.a.asBinder());
                if (fVar == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("sendCustomAction for callback that isn't registered action=");
                    sb.append(this.f0);
                    sb.append(", extras=");
                    sb.append(this.g0);
                    return;
                }
                MediaBrowserServiceCompat.this.l(this.f0, this.g0, fVar, this.h0);
            }
        }

        public o() {
        }

        public void a(String str, IBinder iBinder, Bundle bundle, p pVar) {
            MediaBrowserServiceCompat.this.j0.a(new c(pVar, str, iBinder, bundle));
        }

        public void b(String str, int i2, int i3, Bundle bundle, p pVar) {
            if (MediaBrowserServiceCompat.this.c(str, i3)) {
                MediaBrowserServiceCompat.this.j0.a(new a(pVar, str, i2, i3, bundle));
                return;
            }
            throw new IllegalArgumentException("Package/uid mismatch: uid=" + i3 + " package=" + str);
        }

        public void c(p pVar) {
            MediaBrowserServiceCompat.this.j0.a(new b(pVar));
        }

        public void d(String str, ResultReceiver resultReceiver, p pVar) {
            if (TextUtils.isEmpty(str) || resultReceiver == null) {
                return;
            }
            MediaBrowserServiceCompat.this.j0.a(new e(pVar, str, resultReceiver));
        }

        public void e(p pVar, String str, int i2, int i3, Bundle bundle) {
            MediaBrowserServiceCompat.this.j0.a(new f(pVar, i3, str, i2, bundle));
        }

        public void f(String str, IBinder iBinder, p pVar) {
            MediaBrowserServiceCompat.this.j0.a(new d(pVar, str, iBinder));
        }

        public void g(String str, Bundle bundle, ResultReceiver resultReceiver, p pVar) {
            if (TextUtils.isEmpty(str) || resultReceiver == null) {
                return;
            }
            MediaBrowserServiceCompat.this.j0.a(new h(pVar, str, bundle, resultReceiver));
        }

        public void h(String str, Bundle bundle, ResultReceiver resultReceiver, p pVar) {
            if (TextUtils.isEmpty(str) || resultReceiver == null) {
                return;
            }
            MediaBrowserServiceCompat.this.j0.a(new i(pVar, str, bundle, resultReceiver));
        }

        public void i(p pVar) {
            MediaBrowserServiceCompat.this.j0.a(new g(pVar));
        }
    }

    /* loaded from: classes.dex */
    public interface p {
        void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException;

        IBinder asBinder();

        void b() throws RemoteException;

        void c(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException;
    }

    /* loaded from: classes.dex */
    public static class q implements p {
        public final Messenger a;

        public q(Messenger messenger) {
            this.a = messenger;
        }

        @Override // androidx.media.MediaBrowserServiceCompat.p
        public void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException {
            Bundle bundle3 = new Bundle();
            bundle3.putString("data_media_item_id", str);
            bundle3.putBundle("data_options", bundle);
            bundle3.putBundle("data_notify_children_changed_options", bundle2);
            if (list != null) {
                bundle3.putParcelableArrayList("data_media_item_list", list instanceof ArrayList ? (ArrayList) list : new ArrayList<>(list));
            }
            d(3, bundle3);
        }

        @Override // androidx.media.MediaBrowserServiceCompat.p
        public IBinder asBinder() {
            return this.a.getBinder();
        }

        @Override // androidx.media.MediaBrowserServiceCompat.p
        public void b() throws RemoteException {
            d(2, null);
        }

        @Override // androidx.media.MediaBrowserServiceCompat.p
        public void c(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putInt("extra_service_version", 2);
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bundle2.putParcelable("data_media_session_token", token);
            bundle2.putBundle("data_root_hints", bundle);
            d(1, bundle2);
        }

        public final void d(int i, Bundle bundle) throws RemoteException {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 2;
            obtain.setData(bundle);
            this.a.send(obtain);
        }
    }

    /* loaded from: classes.dex */
    public final class r extends Handler {
        public final o a;

        public r() {
            this.a = new o();
        }

        public void a(Runnable runnable) {
            if (Thread.currentThread() == getLooper().getThread()) {
                runnable.run();
            } else {
                post(runnable);
            }
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            Bundle data = message.getData();
            switch (message.what) {
                case 1:
                    Bundle bundle = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle);
                    this.a.b(data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle, new q(message.replyTo));
                    return;
                case 2:
                    this.a.c(new q(message.replyTo));
                    return;
                case 3:
                    Bundle bundle2 = data.getBundle("data_options");
                    MediaSessionCompat.a(bundle2);
                    this.a.a(data.getString("data_media_item_id"), bs.a(data, "data_callback_token"), bundle2, new q(message.replyTo));
                    return;
                case 4:
                    this.a.f(data.getString("data_media_item_id"), bs.a(data, "data_callback_token"), new q(message.replyTo));
                    return;
                case 5:
                    this.a.d(data.getString("data_media_item_id"), (ResultReceiver) data.getParcelable("data_result_receiver"), new q(message.replyTo));
                    return;
                case 6:
                    Bundle bundle3 = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle3);
                    this.a.e(new q(message.replyTo), data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle3);
                    return;
                case 7:
                    this.a.i(new q(message.replyTo));
                    return;
                case 8:
                    Bundle bundle4 = data.getBundle("data_search_extras");
                    MediaSessionCompat.a(bundle4);
                    this.a.g(data.getString("data_search_query"), bundle4, (ResultReceiver) data.getParcelable("data_result_receiver"), new q(message.replyTo));
                    return;
                case 9:
                    Bundle bundle5 = data.getBundle("data_custom_action_extras");
                    MediaSessionCompat.a(bundle5);
                    this.a.h(data.getString("data_custom_action"), bundle5, (ResultReceiver) data.getParcelable("data_result_receiver"), new q(message.replyTo));
                    return;
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unhandled message: ");
                    sb.append(message);
                    sb.append("\n  Service version: ");
                    sb.append(2);
                    sb.append("\n  Client version: ");
                    sb.append(message.arg1);
                    return;
            }
        }

        @Override // android.os.Handler
        public boolean sendMessageAtTime(Message message, long j) {
            Bundle data = message.getData();
            data.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", Binder.getCallingUid());
            int callingPid = Binder.getCallingPid();
            if (callingPid > 0) {
                data.putInt("data_calling_pid", callingPid);
            } else if (!data.containsKey("data_calling_pid")) {
                data.putInt("data_calling_pid", -1);
            }
            return super.sendMessageAtTime(message, j);
        }
    }

    public void a(String str, f fVar, IBinder iBinder, Bundle bundle) {
        List<jp2<IBinder, Bundle>> list = fVar.e.get(str);
        if (list == null) {
            list = new ArrayList<>();
        }
        for (jp2<IBinder, Bundle> jp2Var : list) {
            if (iBinder == jp2Var.a && r52.a(bundle, jp2Var.b)) {
                return;
            }
        }
        list.add(new jp2<>(iBinder, bundle));
        fVar.e.put(str, list);
        m(str, fVar, bundle, null);
        j(str, bundle);
    }

    public List<MediaBrowserCompat.MediaItem> b(List<MediaBrowserCompat.MediaItem> list, Bundle bundle) {
        if (list == null) {
            return null;
        }
        int i2 = bundle.getInt("android.media.browse.extra.PAGE", -1);
        int i3 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (i2 == -1 && i3 == -1) {
            return list;
        }
        int i4 = i3 * i2;
        int i5 = i4 + i3;
        if (i2 >= 0 && i3 >= 1 && i4 < list.size()) {
            if (i5 > list.size()) {
                i5 = list.size();
            }
            return list.subList(i4, i5);
        }
        return Collections.emptyList();
    }

    public boolean c(String str, int i2) {
        if (str == null) {
            return false;
        }
        for (String str2 : getPackageManager().getPackagesForUid(i2)) {
            if (str2.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public void d(String str, Bundle bundle, m<Bundle> mVar) {
        mVar.e(null);
    }

    @Override // android.app.Service
    public void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public abstract e e(String str, int i2, Bundle bundle);

    public abstract void f(String str, m<List<MediaBrowserCompat.MediaItem>> mVar);

    public void g(String str, m<List<MediaBrowserCompat.MediaItem>> mVar, Bundle bundle) {
        mVar.g(1);
        f(str, mVar);
    }

    public void h(String str, m<MediaBrowserCompat.MediaItem> mVar) {
        mVar.g(2);
        mVar.f(null);
    }

    public void i(String str, Bundle bundle, m<List<MediaBrowserCompat.MediaItem>> mVar) {
        mVar.g(4);
        mVar.f(null);
    }

    public void j(String str, Bundle bundle) {
    }

    public void k(String str) {
    }

    public void l(String str, Bundle bundle, f fVar, ResultReceiver resultReceiver) {
        d dVar = new d(this, str, resultReceiver);
        d(str, bundle, dVar);
        if (dVar.b()) {
            return;
        }
        throw new IllegalStateException("onCustomAction must call detach() or sendResult() or sendError() before returning for action=" + str + " extras=" + bundle);
    }

    public void m(String str, f fVar, Bundle bundle, Bundle bundle2) {
        a aVar = new a(str, fVar, str, bundle, bundle2);
        if (bundle == null) {
            f(str, aVar);
        } else {
            g(str, aVar, bundle);
        }
        if (aVar.b()) {
            return;
        }
        throw new IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + fVar.a + " id=" + str);
    }

    public void n(String str, f fVar, ResultReceiver resultReceiver) {
        b bVar = new b(this, str, resultReceiver);
        h(str, bVar);
        if (bVar.b()) {
            return;
        }
        throw new IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + str);
    }

    public void o(String str, Bundle bundle, f fVar, ResultReceiver resultReceiver) {
        c cVar = new c(this, str, resultReceiver);
        i(str, bundle, cVar);
        if (cVar.b()) {
            return;
        }
        throw new IllegalStateException("onSearch must call detach() or sendResult() before returning for query=" + str);
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.a.b(intent);
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 28) {
            this.a = new k(this);
        } else if (i2 >= 26) {
            this.a = new j();
        } else if (i2 >= 23) {
            this.a = new i();
        } else if (i2 >= 21) {
            this.a = new h();
        } else {
            this.a = new l();
        }
        this.a.a();
    }

    public boolean p(String str, f fVar, IBinder iBinder) {
        boolean z = false;
        try {
            if (iBinder == null) {
                return fVar.e.remove(str) != null;
            }
            List<jp2<IBinder, Bundle>> list = fVar.e.get(str);
            if (list != null) {
                Iterator<jp2<IBinder, Bundle>> it = list.iterator();
                while (it.hasNext()) {
                    if (iBinder == it.next().a) {
                        it.remove();
                        z = true;
                    }
                }
                if (list.size() == 0) {
                    fVar.e.remove(str);
                }
            }
            return z;
        } finally {
            k(str);
        }
    }
}
