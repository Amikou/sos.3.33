package androidx.activity;

import android.annotation.SuppressLint;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.e;
import java.util.ArrayDeque;
import java.util.Iterator;

/* loaded from: classes.dex */
public final class OnBackPressedDispatcher {
    public final Runnable a;
    public final ArrayDeque<fm2> b = new ArrayDeque<>();

    /* loaded from: classes.dex */
    public class LifecycleOnBackPressedCancellable implements e, nv {
        public final Lifecycle a;
        public final fm2 f0;
        public nv g0;

        public LifecycleOnBackPressedCancellable(Lifecycle lifecycle, fm2 fm2Var) {
            this.a = lifecycle;
            this.f0 = fm2Var;
            lifecycle.a(this);
        }

        @Override // defpackage.nv
        public void cancel() {
            this.a.c(this);
            this.f0.e(this);
            nv nvVar = this.g0;
            if (nvVar != null) {
                nvVar.cancel();
                this.g0 = null;
            }
        }

        @Override // androidx.lifecycle.e
        public void e(rz1 rz1Var, Lifecycle.Event event) {
            if (event == Lifecycle.Event.ON_START) {
                this.g0 = OnBackPressedDispatcher.this.b(this.f0);
            } else if (event == Lifecycle.Event.ON_STOP) {
                nv nvVar = this.g0;
                if (nvVar != null) {
                    nvVar.cancel();
                }
            } else if (event == Lifecycle.Event.ON_DESTROY) {
                cancel();
            }
        }
    }

    /* loaded from: classes.dex */
    public class a implements nv {
        public final fm2 a;

        public a(fm2 fm2Var) {
            this.a = fm2Var;
        }

        @Override // defpackage.nv
        public void cancel() {
            OnBackPressedDispatcher.this.b.remove(this.a);
            this.a.e(this);
        }
    }

    public OnBackPressedDispatcher(Runnable runnable) {
        this.a = runnable;
    }

    @SuppressLint({"LambdaLast"})
    public void a(rz1 rz1Var, fm2 fm2Var) {
        Lifecycle lifecycle = rz1Var.getLifecycle();
        if (lifecycle.b() == Lifecycle.State.DESTROYED) {
            return;
        }
        fm2Var.a(new LifecycleOnBackPressedCancellable(lifecycle, fm2Var));
    }

    public nv b(fm2 fm2Var) {
        this.b.add(fm2Var);
        a aVar = new a(fm2Var);
        fm2Var.a(aVar);
        return aVar;
    }

    public boolean c() {
        Iterator<fm2> descendingIterator = this.b.descendingIterator();
        while (descendingIterator.hasNext()) {
            if (descendingIterator.next().c()) {
                return true;
            }
        }
        return false;
    }

    public void d() {
        Iterator<fm2> descendingIterator = this.b.descendingIterator();
        while (descendingIterator.hasNext()) {
            fm2 next = descendingIterator.next();
            if (next.c()) {
                next.b();
                return;
            }
        }
        Runnable runnable = this.a;
        if (runnable != null) {
            runnable.run();
        }
    }
}
