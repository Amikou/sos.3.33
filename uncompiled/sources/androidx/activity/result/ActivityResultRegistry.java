package androidx.activity.result;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/* loaded from: classes.dex */
public abstract class ActivityResultRegistry {
    public Random a = new Random();
    public final Map<Integer, String> b = new HashMap();
    public final Map<String, Integer> c = new HashMap();
    public final Map<String, d> d = new HashMap();
    public ArrayList<String> e = new ArrayList<>();
    public final transient Map<String, c<?>> f = new HashMap();
    public final Map<String, Object> g = new HashMap();
    public final Bundle h = new Bundle();

    /* loaded from: classes.dex */
    public class a extends w7<I> {
        public final /* synthetic */ String a;
        public final /* synthetic */ int b;
        public final /* synthetic */ s7 c;

        public a(String str, int i, s7 s7Var) {
            this.a = str;
            this.b = i;
            this.c = s7Var;
        }

        @Override // defpackage.w7
        public void b(I i, o7 o7Var) {
            ActivityResultRegistry.this.e.add(this.a);
            Integer num = ActivityResultRegistry.this.c.get(this.a);
            ActivityResultRegistry.this.f(num != null ? num.intValue() : this.b, this.c, i, o7Var);
        }

        @Override // defpackage.w7
        public void c() {
            ActivityResultRegistry.this.l(this.a);
        }
    }

    /* loaded from: classes.dex */
    public class b extends w7<I> {
        public final /* synthetic */ String a;
        public final /* synthetic */ int b;
        public final /* synthetic */ s7 c;

        public b(String str, int i, s7 s7Var) {
            this.a = str;
            this.b = i;
            this.c = s7Var;
        }

        @Override // defpackage.w7
        public void b(I i, o7 o7Var) {
            ActivityResultRegistry.this.e.add(this.a);
            Integer num = ActivityResultRegistry.this.c.get(this.a);
            ActivityResultRegistry.this.f(num != null ? num.intValue() : this.b, this.c, i, o7Var);
        }

        @Override // defpackage.w7
        public void c() {
            ActivityResultRegistry.this.l(this.a);
        }
    }

    /* loaded from: classes.dex */
    public static class c<O> {
        public final r7<O> a;
        public final s7<?, O> b;

        public c(r7<O> r7Var, s7<?, O> s7Var) {
            this.a = r7Var;
            this.b = s7Var;
        }
    }

    /* loaded from: classes.dex */
    public static class d {
        public final Lifecycle a;
        public final ArrayList<e> b = new ArrayList<>();

        public d(Lifecycle lifecycle) {
            this.a = lifecycle;
        }

        public void a(e eVar) {
            this.a.a(eVar);
            this.b.add(eVar);
        }

        public void b() {
            Iterator<e> it = this.b.iterator();
            while (it.hasNext()) {
                this.a.c(it.next());
            }
            this.b.clear();
        }
    }

    public final void a(int i, String str) {
        this.b.put(Integer.valueOf(i), str);
        this.c.put(str, Integer.valueOf(i));
    }

    public final boolean b(int i, int i2, Intent intent) {
        String str = this.b.get(Integer.valueOf(i));
        if (str == null) {
            return false;
        }
        this.e.remove(str);
        d(str, i2, intent, this.f.get(str));
        return true;
    }

    public final <O> boolean c(int i, @SuppressLint({"UnknownNullness"}) O o) {
        r7<?> r7Var;
        String str = this.b.get(Integer.valueOf(i));
        if (str == null) {
            return false;
        }
        this.e.remove(str);
        c<?> cVar = this.f.get(str);
        if (cVar != null && (r7Var = cVar.a) != null) {
            r7Var.a(o);
            return true;
        }
        this.h.remove(str);
        this.g.put(str, o);
        return true;
    }

    public final <O> void d(String str, int i, Intent intent, c<O> cVar) {
        r7<O> r7Var;
        if (cVar != null && (r7Var = cVar.a) != null) {
            r7Var.a(cVar.b.c(i, intent));
            return;
        }
        this.g.remove(str);
        this.h.putParcelable(str, new ActivityResult(i, intent));
    }

    public final int e() {
        int nextInt = this.a.nextInt(2147418112);
        while (true) {
            int i = nextInt + 65536;
            if (!this.b.containsKey(Integer.valueOf(i))) {
                return i;
            }
            nextInt = this.a.nextInt(2147418112);
        }
    }

    public abstract <I, O> void f(int i, s7<I, O> s7Var, @SuppressLint({"UnknownNullness"}) I i2, o7 o7Var);

    public final void g(Bundle bundle) {
        if (bundle == null) {
            return;
        }
        ArrayList<Integer> integerArrayList = bundle.getIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS");
        ArrayList<String> stringArrayList = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS");
        if (stringArrayList == null || integerArrayList == null) {
            return;
        }
        this.e = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS");
        this.a = (Random) bundle.getSerializable("KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT");
        this.h.putAll(bundle.getBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT"));
        for (int i = 0; i < stringArrayList.size(); i++) {
            String str = stringArrayList.get(i);
            if (this.c.containsKey(str)) {
                Integer remove = this.c.remove(str);
                if (!this.h.containsKey(str)) {
                    this.b.remove(remove);
                }
            }
            a(integerArrayList.get(i).intValue(), stringArrayList.get(i));
        }
    }

    public final void h(Bundle bundle) {
        bundle.putIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS", new ArrayList<>(this.c.values()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS", new ArrayList<>(this.c.keySet()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS", new ArrayList<>(this.e));
        bundle.putBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT", (Bundle) this.h.clone());
        bundle.putSerializable("KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT", this.a);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final <I, O> w7<I> i(String str, s7<I, O> s7Var, r7<O> r7Var) {
        int k = k(str);
        this.f.put(str, new c<>(r7Var, s7Var));
        if (this.g.containsKey(str)) {
            Object obj = this.g.get(str);
            this.g.remove(str);
            r7Var.a(obj);
        }
        ActivityResult activityResult = (ActivityResult) this.h.getParcelable(str);
        if (activityResult != null) {
            this.h.remove(str);
            r7Var.a(s7Var.c(activityResult.b(), activityResult.a()));
        }
        return new b(str, k, s7Var);
    }

    public final <I, O> w7<I> j(final String str, rz1 rz1Var, final s7<I, O> s7Var, final r7<O> r7Var) {
        Lifecycle lifecycle = rz1Var.getLifecycle();
        if (!lifecycle.b().isAtLeast(Lifecycle.State.STARTED)) {
            int k = k(str);
            d dVar = this.d.get(str);
            if (dVar == null) {
                dVar = new d(lifecycle);
            }
            dVar.a(new e() { // from class: androidx.activity.result.ActivityResultRegistry.1
                @Override // androidx.lifecycle.e
                public void e(rz1 rz1Var2, Lifecycle.Event event) {
                    if (Lifecycle.Event.ON_START.equals(event)) {
                        ActivityResultRegistry.this.f.put(str, new c<>(r7Var, s7Var));
                        if (ActivityResultRegistry.this.g.containsKey(str)) {
                            Object obj = ActivityResultRegistry.this.g.get(str);
                            ActivityResultRegistry.this.g.remove(str);
                            r7Var.a(obj);
                        }
                        ActivityResult activityResult = (ActivityResult) ActivityResultRegistry.this.h.getParcelable(str);
                        if (activityResult != null) {
                            ActivityResultRegistry.this.h.remove(str);
                            r7Var.a(s7Var.c(activityResult.b(), activityResult.a()));
                        }
                    } else if (Lifecycle.Event.ON_STOP.equals(event)) {
                        ActivityResultRegistry.this.f.remove(str);
                    } else if (Lifecycle.Event.ON_DESTROY.equals(event)) {
                        ActivityResultRegistry.this.l(str);
                    }
                }
            });
            this.d.put(str, dVar);
            return new a(str, k, s7Var);
        }
        throw new IllegalStateException("LifecycleOwner " + rz1Var + " is attempting to register while current state is " + lifecycle.b() + ". LifecycleOwners must call register before they are STARTED.");
    }

    public final int k(String str) {
        Integer num = this.c.get(str);
        if (num != null) {
            return num.intValue();
        }
        int e = e();
        a(e, str);
        return e;
    }

    public final void l(String str) {
        Integer remove;
        if (!this.e.contains(str) && (remove = this.c.remove(str)) != null) {
            this.b.remove(remove);
        }
        this.f.remove(str);
        if (this.g.containsKey(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Dropping pending result for request ");
            sb.append(str);
            sb.append(": ");
            sb.append(this.g.get(str));
            this.g.remove(str);
        }
        if (this.h.containsKey(str)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Dropping pending result for request ");
            sb2.append(str);
            sb2.append(": ");
            sb2.append(this.h.getParcelable(str));
            this.h.remove(str);
        }
        d dVar = this.d.get(str);
        if (dVar != null) {
            dVar.b();
            this.d.remove(str);
        }
    }
}
