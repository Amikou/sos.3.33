package androidx.activity.result;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint({"BanParcelableUsage"})
/* loaded from: classes.dex */
public final class IntentSenderRequest implements Parcelable {
    public static final Parcelable.Creator<IntentSenderRequest> CREATOR = new a();
    public final IntentSender a;
    public final Intent f0;
    public final int g0;
    public final int h0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<IntentSenderRequest> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public IntentSenderRequest createFromParcel(Parcel parcel) {
            return new IntentSenderRequest(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public IntentSenderRequest[] newArray(int i) {
            return new IntentSenderRequest[i];
        }
    }

    /* loaded from: classes.dex */
    public static final class b {
        public IntentSender a;
        public Intent b;
        public int c;
        public int d;

        public b(IntentSender intentSender) {
            this.a = intentSender;
        }

        public IntentSenderRequest a() {
            return new IntentSenderRequest(this.a, this.b, this.c, this.d);
        }

        public b b(Intent intent) {
            this.b = intent;
            return this;
        }

        public b c(int i, int i2) {
            this.d = i;
            this.c = i2;
            return this;
        }
    }

    public IntentSenderRequest(IntentSender intentSender, Intent intent, int i, int i2) {
        this.a = intentSender;
        this.f0 = intent;
        this.g0 = i;
        this.h0 = i2;
    }

    public Intent a() {
        return this.f0;
    }

    public int b() {
        return this.g0;
    }

    public int c() {
        return this.h0;
    }

    public IntentSender d() {
        return this.a;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
        parcel.writeParcelable(this.f0, i);
        parcel.writeInt(this.g0);
        parcel.writeInt(this.h0);
    }

    public IntentSenderRequest(Parcel parcel) {
        this.a = (IntentSender) parcel.readParcelable(IntentSender.class.getClassLoader());
        this.f0 = (Intent) parcel.readParcelable(Intent.class.getClassLoader());
        this.g0 = parcel.readInt();
        this.h0 = parcel.readInt();
    }
}
