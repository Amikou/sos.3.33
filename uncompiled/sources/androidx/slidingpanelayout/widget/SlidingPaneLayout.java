package androidx.slidingpanelayout.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import androidx.customview.view.AbsSavedState;
import com.github.mikephil.charting.utils.Utils;
import defpackage.ji4;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/* loaded from: classes.dex */
public class SlidingPaneLayout extends ViewGroup {
    public boolean A0;
    public int a;
    public int f0;
    public Drawable g0;
    public Drawable h0;
    public final int i0;
    public boolean j0;
    public View k0;
    public float l0;
    public float m0;
    public int n0;
    public boolean o0;
    public int p0;
    public float q0;
    public float r0;
    public d s0;
    public final ji4 t0;
    public boolean u0;
    public boolean v0;
    public final Rect w0;
    public final ArrayList<b> x0;
    public Method y0;
    public Field z0;

    /* loaded from: classes.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public boolean g0;

        /* loaded from: classes.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.g0 ? 1 : 0);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = parcel.readInt() != 0;
        }
    }

    /* loaded from: classes.dex */
    public class a extends z5 {
        public final Rect d = new Rect();

        public a() {
        }

        @Override // defpackage.z5
        public void f(View view, AccessibilityEvent accessibilityEvent) {
            super.f(view, accessibilityEvent);
            accessibilityEvent.setClassName(SlidingPaneLayout.class.getName());
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            b6 P = b6.P(b6Var);
            super.g(view, P);
            n(b6Var, P);
            P.S();
            b6Var.c0(SlidingPaneLayout.class.getName());
            b6Var.B0(view);
            ViewParent K = ei4.K(view);
            if (K instanceof View) {
                b6Var.t0((View) K);
            }
            int childCount = SlidingPaneLayout.this.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = SlidingPaneLayout.this.getChildAt(i);
                if (!o(childAt) && childAt.getVisibility() == 0) {
                    ei4.D0(childAt, 1);
                    b6Var.c(childAt);
                }
            }
        }

        @Override // defpackage.z5
        public boolean i(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            if (o(view)) {
                return false;
            }
            return super.i(viewGroup, view, accessibilityEvent);
        }

        public final void n(b6 b6Var, b6 b6Var2) {
            Rect rect = this.d;
            b6Var2.m(rect);
            b6Var.X(rect);
            b6Var2.n(rect);
            b6Var.Y(rect);
            b6Var.G0(b6Var2.N());
            b6Var.r0(b6Var2.v());
            b6Var.c0(b6Var2.p());
            b6Var.g0(b6Var2.r());
            b6Var.i0(b6Var2.F());
            b6Var.d0(b6Var2.E());
            b6Var.k0(b6Var2.G());
            b6Var.l0(b6Var2.H());
            b6Var.V(b6Var2.B());
            b6Var.z0(b6Var2.L());
            b6Var.o0(b6Var2.I());
            b6Var.a(b6Var2.k());
            b6Var.q0(b6Var2.t());
        }

        public boolean o(View view) {
            return SlidingPaneLayout.this.h(view);
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final View a;

        public b(View view) {
            this.a = view;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a.getParent() == SlidingPaneLayout.this) {
                this.a.setLayerType(0, null);
                SlidingPaneLayout.this.g(this.a);
            }
            SlidingPaneLayout.this.x0.remove(this);
        }
    }

    /* loaded from: classes.dex */
    public class c extends ji4.c {
        public c() {
        }

        @Override // defpackage.ji4.c
        public int a(View view, int i, int i2) {
            LayoutParams layoutParams = (LayoutParams) SlidingPaneLayout.this.k0.getLayoutParams();
            if (SlidingPaneLayout.this.i()) {
                int width = SlidingPaneLayout.this.getWidth() - ((SlidingPaneLayout.this.getPaddingRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin) + SlidingPaneLayout.this.k0.getWidth());
                return Math.max(Math.min(i, width), width - SlidingPaneLayout.this.n0);
            }
            int paddingLeft = SlidingPaneLayout.this.getPaddingLeft() + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
            return Math.min(Math.max(i, paddingLeft), SlidingPaneLayout.this.n0 + paddingLeft);
        }

        @Override // defpackage.ji4.c
        public int b(View view, int i, int i2) {
            return view.getTop();
        }

        @Override // defpackage.ji4.c
        public int d(View view) {
            return SlidingPaneLayout.this.n0;
        }

        @Override // defpackage.ji4.c
        public void f(int i, int i2) {
            SlidingPaneLayout slidingPaneLayout = SlidingPaneLayout.this;
            slidingPaneLayout.t0.c(slidingPaneLayout.k0, i2);
        }

        @Override // defpackage.ji4.c
        public void i(View view, int i) {
            SlidingPaneLayout.this.p();
        }

        @Override // defpackage.ji4.c
        public void j(int i) {
            if (SlidingPaneLayout.this.t0.B() == 0) {
                SlidingPaneLayout slidingPaneLayout = SlidingPaneLayout.this;
                if (slidingPaneLayout.l0 == Utils.FLOAT_EPSILON) {
                    slidingPaneLayout.r(slidingPaneLayout.k0);
                    SlidingPaneLayout slidingPaneLayout2 = SlidingPaneLayout.this;
                    slidingPaneLayout2.d(slidingPaneLayout2.k0);
                    SlidingPaneLayout.this.u0 = false;
                    return;
                }
                slidingPaneLayout.e(slidingPaneLayout.k0);
                SlidingPaneLayout.this.u0 = true;
            }
        }

        @Override // defpackage.ji4.c
        public void k(View view, int i, int i2, int i3, int i4) {
            SlidingPaneLayout.this.l(i);
            SlidingPaneLayout.this.invalidate();
        }

        @Override // defpackage.ji4.c
        public void l(View view, float f, float f2) {
            int paddingLeft;
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (SlidingPaneLayout.this.i()) {
                int paddingRight = SlidingPaneLayout.this.getPaddingRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                if (f < Utils.FLOAT_EPSILON || (f == Utils.FLOAT_EPSILON && SlidingPaneLayout.this.l0 > 0.5f)) {
                    paddingRight += SlidingPaneLayout.this.n0;
                }
                paddingLeft = (SlidingPaneLayout.this.getWidth() - paddingRight) - SlidingPaneLayout.this.k0.getWidth();
            } else {
                paddingLeft = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + SlidingPaneLayout.this.getPaddingLeft();
                int i = (f > Utils.FLOAT_EPSILON ? 1 : (f == Utils.FLOAT_EPSILON ? 0 : -1));
                if (i > 0 || (i == 0 && SlidingPaneLayout.this.l0 > 0.5f)) {
                    paddingLeft += SlidingPaneLayout.this.n0;
                }
            }
            SlidingPaneLayout.this.t0.P(paddingLeft, view.getTop());
            SlidingPaneLayout.this.invalidate();
        }

        @Override // defpackage.ji4.c
        public boolean m(View view, int i) {
            if (SlidingPaneLayout.this.o0) {
                return false;
            }
            return ((LayoutParams) view.getLayoutParams()).b;
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a(View view, float f);

        void b(View view);

        void c(View view);
    }

    public SlidingPaneLayout(Context context) {
        this(context, null);
    }

    public static boolean s(View view) {
        Drawable background;
        if (view.isOpaque()) {
            return true;
        }
        return Build.VERSION.SDK_INT < 18 && (background = view.getBackground()) != null && background.getOpacity() == -1;
    }

    public boolean a() {
        return b(this.k0, 0);
    }

    public final boolean b(View view, int i) {
        if (this.v0 || q(Utils.FLOAT_EPSILON, i)) {
            this.u0 = false;
            return true;
        }
        return false;
    }

    public final void c(View view, float f, int i) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f > Utils.FLOAT_EPSILON && i != 0) {
            int i2 = (((int) ((((-16777216) & i) >>> 24) * f)) << 24) | (i & 16777215);
            if (layoutParams.d == null) {
                layoutParams.d = new Paint();
            }
            layoutParams.d.setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.SRC_OVER));
            if (view.getLayerType() != 2) {
                view.setLayerType(2, layoutParams.d);
            }
            g(view);
        } else if (view.getLayerType() != 0) {
            Paint paint = layoutParams.d;
            if (paint != null) {
                paint.setColorFilter(null);
            }
            b bVar = new b(view);
            this.x0.add(bVar);
            ei4.l0(this, bVar);
        }
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.view.View
    public void computeScroll() {
        if (this.t0.n(true)) {
            if (!this.j0) {
                this.t0.a();
            } else {
                ei4.j0(this);
            }
        }
    }

    public void d(View view) {
        d dVar = this.s0;
        if (dVar != null) {
            dVar.c(view);
        }
        sendAccessibilityEvent(32);
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        Drawable drawable;
        int i;
        int i2;
        super.draw(canvas);
        if (i()) {
            drawable = this.h0;
        } else {
            drawable = this.g0;
        }
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt == null || drawable == null) {
            return;
        }
        int top = childAt.getTop();
        int bottom = childAt.getBottom();
        int intrinsicWidth = drawable.getIntrinsicWidth();
        if (i()) {
            i2 = childAt.getRight();
            i = intrinsicWidth + i2;
        } else {
            int left = childAt.getLeft();
            int i3 = left - intrinsicWidth;
            i = left;
            i2 = i3;
        }
        drawable.setBounds(i2, top, i, bottom);
        drawable.draw(canvas);
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int save = canvas.save();
        if (this.j0 && !layoutParams.b && this.k0 != null) {
            canvas.getClipBounds(this.w0);
            if (i()) {
                Rect rect = this.w0;
                rect.left = Math.max(rect.left, this.k0.getRight());
            } else {
                Rect rect2 = this.w0;
                rect2.right = Math.min(rect2.right, this.k0.getLeft());
            }
            canvas.clipRect(this.w0);
        }
        boolean drawChild = super.drawChild(canvas, view, j);
        canvas.restoreToCount(save);
        return drawChild;
    }

    public void e(View view) {
        d dVar = this.s0;
        if (dVar != null) {
            dVar.b(view);
        }
        sendAccessibilityEvent(32);
    }

    public void f(View view) {
        d dVar = this.s0;
        if (dVar != null) {
            dVar.a(view, this.l0);
        }
    }

    public void g(View view) {
        Field field;
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            ei4.F0(view, ((LayoutParams) view.getLayoutParams()).d);
            return;
        }
        if (i >= 16) {
            if (!this.A0) {
                try {
                    this.y0 = View.class.getDeclaredMethod("getDisplayList", null);
                } catch (NoSuchMethodException unused) {
                }
                try {
                    Field declaredField = View.class.getDeclaredField("mRecreateDisplayList");
                    this.z0 = declaredField;
                    declaredField.setAccessible(true);
                } catch (NoSuchFieldException unused2) {
                }
                this.A0 = true;
            }
            if (this.y0 != null && (field = this.z0) != null) {
                try {
                    field.setBoolean(view, true);
                    this.y0.invoke(view, null);
                } catch (Exception unused3) {
                }
            } else {
                view.invalidate();
                return;
            }
        }
        ei4.k0(this, view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    public int getCoveredFadeColor() {
        return this.f0;
    }

    public int getParallaxDistance() {
        return this.p0;
    }

    public int getSliderFadeColor() {
        return this.a;
    }

    public boolean h(View view) {
        if (view == null) {
            return false;
        }
        return this.j0 && ((LayoutParams) view.getLayoutParams()).c && this.l0 > Utils.FLOAT_EPSILON;
    }

    public boolean i() {
        return ei4.E(this) == 1;
    }

    public boolean j() {
        return !this.j0 || this.l0 == 1.0f;
    }

    public boolean k() {
        return this.j0;
    }

    public void l(int i) {
        if (this.k0 == null) {
            this.l0 = Utils.FLOAT_EPSILON;
            return;
        }
        boolean i2 = i();
        LayoutParams layoutParams = (LayoutParams) this.k0.getLayoutParams();
        int width = this.k0.getWidth();
        if (i2) {
            i = (getWidth() - i) - width;
        }
        float paddingRight = (i - ((i2 ? getPaddingRight() : getPaddingLeft()) + (i2 ? ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin : ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin))) / this.n0;
        this.l0 = paddingRight;
        if (this.p0 != 0) {
            o(paddingRight);
        }
        if (layoutParams.c) {
            c(this.k0, this.l0, this.a);
        }
        f(this.k0);
    }

    public boolean m() {
        return n(this.k0, 0);
    }

    public final boolean n(View view, int i) {
        if (this.v0 || q(1.0f, i)) {
            this.u0 = true;
            return true;
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0023  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void o(float r10) {
        /*
            r9 = this;
            boolean r0 = r9.i()
            android.view.View r1 = r9.k0
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            androidx.slidingpanelayout.widget.SlidingPaneLayout$LayoutParams r1 = (androidx.slidingpanelayout.widget.SlidingPaneLayout.LayoutParams) r1
            boolean r2 = r1.c
            r3 = 0
            if (r2 == 0) goto L1c
            if (r0 == 0) goto L16
            int r1 = r1.rightMargin
            goto L18
        L16:
            int r1 = r1.leftMargin
        L18:
            if (r1 > 0) goto L1c
            r1 = 1
            goto L1d
        L1c:
            r1 = r3
        L1d:
            int r2 = r9.getChildCount()
        L21:
            if (r3 >= r2) goto L57
            android.view.View r4 = r9.getChildAt(r3)
            android.view.View r5 = r9.k0
            if (r4 != r5) goto L2c
            goto L54
        L2c:
            float r5 = r9.m0
            r6 = 1065353216(0x3f800000, float:1.0)
            float r5 = r6 - r5
            int r7 = r9.p0
            float r8 = (float) r7
            float r5 = r5 * r8
            int r5 = (int) r5
            r9.m0 = r10
            float r8 = r6 - r10
            float r7 = (float) r7
            float r8 = r8 * r7
            int r7 = (int) r8
            int r5 = r5 - r7
            if (r0 == 0) goto L42
            int r5 = -r5
        L42:
            r4.offsetLeftAndRight(r5)
            if (r1 == 0) goto L54
            float r5 = r9.m0
            if (r0 == 0) goto L4d
            float r5 = r5 - r6
            goto L4f
        L4d:
            float r5 = r6 - r5
        L4f:
            int r6 = r9.f0
            r9.c(r4, r5, r6)
        L54:
            int r3 = r3 + 1
            goto L21
        L57:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.slidingpanelayout.widget.SlidingPaneLayout.o(float):void");
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.v0 = true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.v0 = true;
        int size = this.x0.size();
        for (int i = 0; i < size; i++) {
            this.x0.get(i).run();
        }
        this.x0.clear();
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int actionMasked = motionEvent.getActionMasked();
        if (!this.j0 && actionMasked == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            this.u0 = !this.t0.F(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.j0 || (this.o0 && actionMasked != 0)) {
            this.t0.b();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (actionMasked == 3 || actionMasked == 1) {
            this.t0.b();
            return false;
        } else {
            if (actionMasked == 0) {
                this.o0 = false;
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.q0 = x;
                this.r0 = y;
                if (this.t0.F(this.k0, (int) x, (int) y) && h(this.k0)) {
                    z = true;
                    return this.t0.Q(motionEvent) || z;
                }
            } else if (actionMasked == 2) {
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                float abs = Math.abs(x2 - this.q0);
                float abs2 = Math.abs(y2 - this.r0);
                if (abs > this.t0.A() && abs2 > abs) {
                    this.t0.b();
                    this.o0 = true;
                    return false;
                }
            }
            z = false;
            if (this.t0.Q(motionEvent)) {
                return true;
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        boolean i9 = i();
        if (i9) {
            this.t0.N(2);
        } else {
            this.t0.N(1);
        }
        int i10 = i3 - i;
        int paddingRight = i9 ? getPaddingRight() : getPaddingLeft();
        int paddingLeft = i9 ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.v0) {
            this.l0 = (this.j0 && this.u0) ? 1.0f : Utils.FLOAT_EPSILON;
        }
        int i11 = paddingRight;
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt = getChildAt(i12);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                if (layoutParams.b) {
                    int i13 = i10 - paddingLeft;
                    int min = (Math.min(paddingRight, i13 - this.i0) - i11) - (((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin);
                    this.n0 = min;
                    int i14 = i9 ? ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin : ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
                    layoutParams.c = ((i11 + i14) + min) + (measuredWidth / 2) > i13;
                    int i15 = (int) (min * this.l0);
                    i11 += i14 + i15;
                    this.l0 = i15 / min;
                    i5 = 0;
                } else if (!this.j0 || (i6 = this.p0) == 0) {
                    i11 = paddingRight;
                    i5 = 0;
                } else {
                    i5 = (int) ((1.0f - this.l0) * i6);
                    i11 = paddingRight;
                }
                if (i9) {
                    i8 = (i10 - i11) + i5;
                    i7 = i8 - measuredWidth;
                } else {
                    i7 = i11 - i5;
                    i8 = i7 + measuredWidth;
                }
                childAt.layout(i7, paddingTop, i8, childAt.getMeasuredHeight() + paddingTop);
                paddingRight += childAt.getWidth();
            }
        }
        if (this.v0) {
            if (this.j0) {
                if (this.p0 != 0) {
                    o(this.l0);
                }
                if (((LayoutParams) this.k0.getLayoutParams()).c) {
                    c(this.k0, this.l0, this.a);
                }
            } else {
                for (int i16 = 0; i16 < childCount; i16++) {
                    c(getChildAt(i16), Utils.FLOAT_EPSILON, this.a);
                }
            }
            r(this.k0);
        }
        this.v0 = false;
    }

    /* JADX WARN: Removed duplicated region for block: B:47:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00db  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0102  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x0105  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x010b  */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onMeasure(int r21, int r22) {
        /*
            Method dump skipped, instructions count: 555
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.slidingpanelayout.widget.SlidingPaneLayout.onMeasure(int, int):void");
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        if (savedState.g0) {
            m();
        } else {
            a();
        }
        this.u0 = savedState.g0;
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.g0 = k() ? j() : this.u0;
        return savedState;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            this.v0 = true;
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.j0) {
            return super.onTouchEvent(motionEvent);
        }
        this.t0.G(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked == 1 && h(this.k0)) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float f = x - this.q0;
                float f2 = y - this.r0;
                int A = this.t0.A();
                if ((f * f) + (f2 * f2) < A * A && this.t0.F(this.k0, (int) x, (int) y)) {
                    b(this.k0, 0);
                }
            }
        } else {
            float x2 = motionEvent.getX();
            float y2 = motionEvent.getY();
            this.q0 = x2;
            this.r0 = y2;
        }
        return true;
    }

    public void p() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    public boolean q(float f, int i) {
        int paddingLeft;
        if (this.j0) {
            boolean i2 = i();
            LayoutParams layoutParams = (LayoutParams) this.k0.getLayoutParams();
            if (i2) {
                paddingLeft = (int) (getWidth() - (((getPaddingRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin) + (f * this.n0)) + this.k0.getWidth()));
            } else {
                paddingLeft = (int) (getPaddingLeft() + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + (f * this.n0));
            }
            ji4 ji4Var = this.t0;
            View view = this.k0;
            if (ji4Var.R(view, paddingLeft, view.getTop())) {
                p();
                ei4.j0(this);
                return true;
            }
            return false;
        }
        return false;
    }

    public void r(View view) {
        int i;
        int i2;
        int i3;
        int i4;
        View childAt;
        boolean z;
        View view2 = view;
        boolean i5 = i();
        int width = i5 ? getWidth() - getPaddingRight() : getPaddingLeft();
        int paddingLeft = i5 ? getPaddingLeft() : getWidth() - getPaddingRight();
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        if (view2 == null || !s(view)) {
            i = 0;
            i2 = 0;
            i3 = 0;
            i4 = 0;
        } else {
            i = view.getLeft();
            i2 = view.getRight();
            i3 = view.getTop();
            i4 = view.getBottom();
        }
        int childCount = getChildCount();
        int i6 = 0;
        while (i6 < childCount && (childAt = getChildAt(i6)) != view2) {
            if (childAt.getVisibility() == 8) {
                z = i5;
            } else {
                z = i5;
                childAt.setVisibility((Math.max(i5 ? paddingLeft : width, childAt.getLeft()) < i || Math.max(paddingTop, childAt.getTop()) < i3 || Math.min(i5 ? width : paddingLeft, childAt.getRight()) > i2 || Math.min(height, childAt.getBottom()) > i4) ? 0 : 4);
            }
            i6++;
            view2 = view;
            i5 = z;
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (isInTouchMode() || this.j0) {
            return;
        }
        this.u0 = view == this.k0;
    }

    public void setCoveredFadeColor(int i) {
        this.f0 = i;
    }

    public void setPanelSlideListener(d dVar) {
        this.s0 = dVar;
    }

    public void setParallaxDistance(int i) {
        this.p0 = i;
        requestLayout();
    }

    @Deprecated
    public void setShadowDrawable(Drawable drawable) {
        setShadowDrawableLeft(drawable);
    }

    public void setShadowDrawableLeft(Drawable drawable) {
        this.g0 = drawable;
    }

    public void setShadowDrawableRight(Drawable drawable) {
        this.h0 = drawable;
    }

    @Deprecated
    public void setShadowResource(int i) {
        setShadowDrawable(getResources().getDrawable(i));
    }

    public void setShadowResourceLeft(int i) {
        setShadowDrawableLeft(m70.f(getContext(), i));
    }

    public void setShadowResourceRight(int i) {
        setShadowDrawableRight(m70.f(getContext(), i));
    }

    public void setSliderFadeColor(int i) {
        this.a = i;
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public static final int[] e = {16843137};
        public float a;
        public boolean b;
        public boolean c;
        public Paint d;

        public LayoutParams() {
            super(-1, -1);
            this.a = Utils.FLOAT_EPSILON;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.a = Utils.FLOAT_EPSILON;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.a = Utils.FLOAT_EPSILON;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = Utils.FLOAT_EPSILON;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e);
            this.a = obtainStyledAttributes.getFloat(0, Utils.FLOAT_EPSILON);
            obtainStyledAttributes.recycle();
        }
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = -858993460;
        this.v0 = true;
        this.w0 = new Rect();
        this.x0 = new ArrayList<>();
        float f = context.getResources().getDisplayMetrics().density;
        this.i0 = (int) ((32.0f * f) + 0.5f);
        setWillNotDraw(false);
        ei4.t0(this, new a());
        ei4.D0(this, 1);
        ji4 o = ji4.o(this, 0.5f, new c());
        this.t0 = o;
        o.O(f * 400.0f);
    }
}
