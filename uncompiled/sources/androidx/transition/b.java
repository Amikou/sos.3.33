package androidx.transition;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.transition.Transition;
import defpackage.tv;
import java.util.ArrayList;
import java.util.List;

/* compiled from: FragmentTransitionSupport.java */
@SuppressLint({"RestrictedApi"})
/* loaded from: classes.dex */
public class b extends mb1 {

    /* compiled from: FragmentTransitionSupport.java */
    /* loaded from: classes.dex */
    public class a extends Transition.e {
        public final /* synthetic */ Rect a;

        public a(b bVar, Rect rect) {
            this.a = rect;
        }

        @Override // androidx.transition.Transition.e
        public Rect a(Transition transition) {
            return this.a;
        }
    }

    /* compiled from: FragmentTransitionSupport.java */
    /* renamed from: androidx.transition.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0066b implements Transition.f {
        public final /* synthetic */ View a;
        public final /* synthetic */ ArrayList f0;

        public C0066b(b bVar, View view, ArrayList arrayList) {
            this.a = view;
            this.f0 = arrayList;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionCancel(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            transition.b0(this);
            this.a.setVisibility(8);
            int size = this.f0.size();
            for (int i = 0; i < size; i++) {
                ((View) this.f0.get(i)).setVisibility(0);
            }
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionPause(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionResume(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionStart(Transition transition) {
            transition.b0(this);
            transition.a(this);
        }
    }

    /* compiled from: FragmentTransitionSupport.java */
    /* loaded from: classes.dex */
    public class c extends androidx.transition.c {
        public final /* synthetic */ Object a;
        public final /* synthetic */ ArrayList f0;
        public final /* synthetic */ Object g0;
        public final /* synthetic */ ArrayList h0;
        public final /* synthetic */ Object i0;
        public final /* synthetic */ ArrayList j0;

        public c(Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3) {
            this.a = obj;
            this.f0 = arrayList;
            this.g0 = obj2;
            this.h0 = arrayList2;
            this.i0 = obj3;
            this.j0 = arrayList3;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            transition.b0(this);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionStart(Transition transition) {
            Object obj = this.a;
            if (obj != null) {
                b.this.q(obj, this.f0, null);
            }
            Object obj2 = this.g0;
            if (obj2 != null) {
                b.this.q(obj2, this.h0, null);
            }
            Object obj3 = this.i0;
            if (obj3 != null) {
                b.this.q(obj3, this.j0, null);
            }
        }
    }

    /* compiled from: FragmentTransitionSupport.java */
    /* loaded from: classes.dex */
    public class d implements tv.a {
        public final /* synthetic */ Transition a;

        public d(b bVar, Transition transition) {
            this.a = transition;
        }

        @Override // defpackage.tv.a
        public void a() {
            this.a.cancel();
        }
    }

    /* compiled from: FragmentTransitionSupport.java */
    /* loaded from: classes.dex */
    public class e implements Transition.f {
        public final /* synthetic */ Runnable a;

        public e(b bVar, Runnable runnable) {
            this.a = runnable;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionCancel(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            this.a.run();
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionPause(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionResume(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionStart(Transition transition) {
        }
    }

    /* compiled from: FragmentTransitionSupport.java */
    /* loaded from: classes.dex */
    public class f extends Transition.e {
        public final /* synthetic */ Rect a;

        public f(b bVar, Rect rect) {
            this.a = rect;
        }

        @Override // androidx.transition.Transition.e
        public Rect a(Transition transition) {
            Rect rect = this.a;
            if (rect == null || rect.isEmpty()) {
                return null;
            }
            return this.a;
        }
    }

    public static boolean C(Transition transition) {
        return (mb1.l(transition.J()) && mb1.l(transition.K()) && mb1.l(transition.L())) ? false : true;
    }

    @Override // defpackage.mb1
    public void A(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.M().clear();
            transitionSet.M().addAll(arrayList2);
            q(transitionSet, arrayList, arrayList2);
        }
    }

    @Override // defpackage.mb1
    public Object B(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.u0((Transition) obj);
        return transitionSet;
    }

    @Override // defpackage.mb1
    public void a(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).b(view);
        }
    }

    @Override // defpackage.mb1
    public void b(Object obj, ArrayList<View> arrayList) {
        Transition transition = (Transition) obj;
        if (transition == null) {
            return;
        }
        int i = 0;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int x0 = transitionSet.x0();
            while (i < x0) {
                b(transitionSet.w0(i), arrayList);
                i++;
            }
        } else if (C(transition) || !mb1.l(transition.M())) {
        } else {
            int size = arrayList.size();
            while (i < size) {
                transition.b(arrayList.get(i));
                i++;
            }
        }
    }

    @Override // defpackage.mb1
    public void c(ViewGroup viewGroup, Object obj) {
        androidx.transition.d.a(viewGroup, (Transition) obj);
    }

    @Override // defpackage.mb1
    public boolean e(Object obj) {
        return obj instanceof Transition;
    }

    @Override // defpackage.mb1
    public Object g(Object obj) {
        if (obj != null) {
            return ((Transition) obj).clone();
        }
        return null;
    }

    @Override // defpackage.mb1
    public Object m(Object obj, Object obj2, Object obj3) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition != null && transition2 != null) {
            transition = new TransitionSet().u0(transition).u0(transition2).D0(1);
        } else if (transition == null) {
            transition = transition2 != null ? transition2 : null;
        }
        if (transition3 != null) {
            TransitionSet transitionSet = new TransitionSet();
            if (transition != null) {
                transitionSet.u0(transition);
            }
            transitionSet.u0(transition3);
            return transitionSet;
        }
        return transition;
    }

    @Override // defpackage.mb1
    public Object n(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.u0((Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.u0((Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.u0((Transition) obj3);
        }
        return transitionSet;
    }

    @Override // defpackage.mb1
    public void p(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).c0(view);
        }
    }

    @Override // defpackage.mb1
    public void q(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        Transition transition = (Transition) obj;
        int i = 0;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int x0 = transitionSet.x0();
            while (i < x0) {
                q(transitionSet.w0(i), arrayList, arrayList2);
                i++;
            }
        } else if (!C(transition)) {
            List<View> M = transition.M();
            if (M.size() == arrayList.size() && M.containsAll(arrayList)) {
                int size = arrayList2 == null ? 0 : arrayList2.size();
                while (i < size) {
                    transition.b(arrayList2.get(i));
                    i++;
                }
                for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                    transition.c0(arrayList.get(size2));
                }
            }
        }
    }

    @Override // defpackage.mb1
    public void r(Object obj, View view, ArrayList<View> arrayList) {
        ((Transition) obj).a(new C0066b(this, view, arrayList));
    }

    @Override // defpackage.mb1
    public void t(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        ((Transition) obj).a(new c(obj2, arrayList, obj3, arrayList2, obj4, arrayList3));
    }

    @Override // defpackage.mb1
    public void u(Object obj, Rect rect) {
        if (obj != null) {
            ((Transition) obj).j0(new f(this, rect));
        }
    }

    @Override // defpackage.mb1
    public void v(Object obj, View view) {
        if (view != null) {
            Rect rect = new Rect();
            k(view, rect);
            ((Transition) obj).j0(new a(this, rect));
        }
    }

    @Override // defpackage.mb1
    public void w(Fragment fragment, Object obj, tv tvVar, Runnable runnable) {
        Transition transition = (Transition) obj;
        tvVar.d(new d(this, transition));
        transition.a(new e(this, runnable));
    }

    @Override // defpackage.mb1
    public void z(Object obj, View view, ArrayList<View> arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        List<View> M = transitionSet.M();
        M.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            mb1.d(M, arrayList.get(i));
        }
        M.add(view);
        arrayList.add(view);
        b(transitionSet, arrayList);
    }
}
