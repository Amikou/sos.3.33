package androidx.transition;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: TransitionManager.java */
/* loaded from: classes.dex */
public class d {
    public static Transition a = new AutoTransition();
    public static ThreadLocal<WeakReference<rh<ViewGroup, ArrayList<Transition>>>> b = new ThreadLocal<>();
    public static ArrayList<ViewGroup> c = new ArrayList<>();

    /* compiled from: TransitionManager.java */
    /* loaded from: classes.dex */
    public static class a implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
        public Transition a;
        public ViewGroup f0;

        /* compiled from: TransitionManager.java */
        /* renamed from: androidx.transition.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0067a extends c {
            public final /* synthetic */ rh a;

            public C0067a(rh rhVar) {
                this.a = rhVar;
            }

            @Override // androidx.transition.Transition.f
            public void onTransitionEnd(Transition transition) {
                ((ArrayList) this.a.get(a.this.f0)).remove(transition);
                transition.b0(this);
            }
        }

        public a(Transition transition, ViewGroup viewGroup) {
            this.a = transition;
            this.f0 = viewGroup;
        }

        public final void a() {
            this.f0.getViewTreeObserver().removeOnPreDrawListener(this);
            this.f0.removeOnAttachStateChangeListener(this);
        }

        @Override // android.view.ViewTreeObserver.OnPreDrawListener
        public boolean onPreDraw() {
            a();
            if (d.c.remove(this.f0)) {
                rh<ViewGroup, ArrayList<Transition>> d = d.d();
                ArrayList<Transition> arrayList = d.get(this.f0);
                ArrayList arrayList2 = null;
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                    d.put(this.f0, arrayList);
                } else if (arrayList.size() > 0) {
                    arrayList2 = new ArrayList(arrayList);
                }
                arrayList.add(this.a);
                this.a.a(new C0067a(d));
                this.a.o(this.f0, false);
                if (arrayList2 != null) {
                    Iterator it = arrayList2.iterator();
                    while (it.hasNext()) {
                        ((Transition) it.next()).d0(this.f0);
                    }
                }
                this.a.a0(this.f0);
                return true;
            }
            return true;
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
            a();
            d.c.remove(this.f0);
            ArrayList<Transition> arrayList = d.d().get(this.f0);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator<Transition> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().d0(this.f0);
                }
            }
            this.a.p(true);
        }
    }

    public static void a(ViewGroup viewGroup, Transition transition) {
        if (c.contains(viewGroup) || !ei4.W(viewGroup)) {
            return;
        }
        c.add(viewGroup);
        if (transition == null) {
            transition = a;
        }
        Transition clone = transition.clone();
        g(viewGroup, clone);
        wc3.f(viewGroup, null);
        f(viewGroup, clone);
    }

    public static void b(wc3 wc3Var, Transition transition) {
        ViewGroup d = wc3Var.d();
        if (c.contains(d)) {
            return;
        }
        wc3 c2 = wc3.c(d);
        if (transition == null) {
            if (c2 != null) {
                c2.b();
            }
            wc3Var.a();
            return;
        }
        c.add(d);
        Transition clone = transition.clone();
        clone.o0(d);
        if (c2 != null && c2.e()) {
            clone.g0(true);
        }
        g(d, clone);
        wc3Var.a();
        f(d, clone);
    }

    public static void c(ViewGroup viewGroup) {
        c.remove(viewGroup);
        ArrayList<Transition> arrayList = d().get(viewGroup);
        if (arrayList == null || arrayList.isEmpty()) {
            return;
        }
        ArrayList arrayList2 = new ArrayList(arrayList);
        for (int size = arrayList2.size() - 1; size >= 0; size--) {
            ((Transition) arrayList2.get(size)).v(viewGroup);
        }
    }

    public static rh<ViewGroup, ArrayList<Transition>> d() {
        rh<ViewGroup, ArrayList<Transition>> rhVar;
        WeakReference<rh<ViewGroup, ArrayList<Transition>>> weakReference = b.get();
        if (weakReference == null || (rhVar = weakReference.get()) == null) {
            rh<ViewGroup, ArrayList<Transition>> rhVar2 = new rh<>();
            b.set(new WeakReference<>(rhVar2));
            return rhVar2;
        }
        return rhVar;
    }

    public static void e(wc3 wc3Var, Transition transition) {
        b(wc3Var, transition);
    }

    public static void f(ViewGroup viewGroup, Transition transition) {
        if (transition == null || viewGroup == null) {
            return;
        }
        a aVar = new a(transition, viewGroup);
        viewGroup.addOnAttachStateChangeListener(aVar);
        viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
    }

    public static void g(ViewGroup viewGroup, Transition transition) {
        ArrayList<Transition> arrayList = d().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator<Transition> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().Z(viewGroup);
            }
        }
        if (transition != null) {
            transition.o(viewGroup, true);
        }
        wc3 c2 = wc3.c(viewGroup);
        if (c2 != null) {
            c2.b();
        }
    }
}
