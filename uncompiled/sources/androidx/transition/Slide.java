package androidx.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import org.xmlpull.v1.XmlPullParser;

/* loaded from: classes.dex */
public class Slide extends Visibility {
    public static final TimeInterpolator Q0 = new DecelerateInterpolator();
    public static final TimeInterpolator R0 = new AccelerateInterpolator();
    public static final g S0 = new a();
    public static final g T0 = new b();
    public static final g U0 = new c();
    public static final g V0 = new d();
    public static final g W0 = new e();
    public static final g X0 = new f();
    public g P0;

    /* loaded from: classes.dex */
    public static class a extends h {
        public a() {
            super(null);
        }

        @Override // androidx.transition.Slide.g
        public float b(ViewGroup viewGroup, View view) {
            return view.getTranslationX() - viewGroup.getWidth();
        }
    }

    /* loaded from: classes.dex */
    public static class b extends h {
        public b() {
            super(null);
        }

        @Override // androidx.transition.Slide.g
        public float b(ViewGroup viewGroup, View view) {
            if (ei4.E(viewGroup) == 1) {
                return view.getTranslationX() + viewGroup.getWidth();
            }
            return view.getTranslationX() - viewGroup.getWidth();
        }
    }

    /* loaded from: classes.dex */
    public static class c extends i {
        public c() {
            super(null);
        }

        @Override // androidx.transition.Slide.g
        public float a(ViewGroup viewGroup, View view) {
            return view.getTranslationY() - viewGroup.getHeight();
        }
    }

    /* loaded from: classes.dex */
    public static class d extends h {
        public d() {
            super(null);
        }

        @Override // androidx.transition.Slide.g
        public float b(ViewGroup viewGroup, View view) {
            return view.getTranslationX() + viewGroup.getWidth();
        }
    }

    /* loaded from: classes.dex */
    public static class e extends h {
        public e() {
            super(null);
        }

        @Override // androidx.transition.Slide.g
        public float b(ViewGroup viewGroup, View view) {
            if (ei4.E(viewGroup) == 1) {
                return view.getTranslationX() - viewGroup.getWidth();
            }
            return view.getTranslationX() + viewGroup.getWidth();
        }
    }

    /* loaded from: classes.dex */
    public static class f extends i {
        public f() {
            super(null);
        }

        @Override // androidx.transition.Slide.g
        public float a(ViewGroup viewGroup, View view) {
            return view.getTranslationY() + viewGroup.getHeight();
        }
    }

    /* loaded from: classes.dex */
    public interface g {
        float a(ViewGroup viewGroup, View view);

        float b(ViewGroup viewGroup, View view);
    }

    /* loaded from: classes.dex */
    public static abstract class h implements g {
        public h() {
        }

        @Override // androidx.transition.Slide.g
        public float a(ViewGroup viewGroup, View view) {
            return view.getTranslationY();
        }

        public /* synthetic */ h(a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public static abstract class i implements g {
        public i() {
        }

        @Override // androidx.transition.Slide.g
        public float b(ViewGroup viewGroup, View view) {
            return view.getTranslationX();
        }

        public /* synthetic */ i(a aVar) {
            this();
        }
    }

    public Slide() {
        this.P0 = X0;
        B0(80);
    }

    private void s0(kb4 kb4Var) {
        int[] iArr = new int[2];
        kb4Var.b.getLocationOnScreen(iArr);
        kb4Var.a.put("android:slide:screenPosition", iArr);
    }

    public void B0(int i2) {
        if (i2 == 3) {
            this.P0 = S0;
        } else if (i2 == 5) {
            this.P0 = V0;
        } else if (i2 == 48) {
            this.P0 = U0;
        } else if (i2 == 80) {
            this.P0 = X0;
        } else if (i2 == 8388611) {
            this.P0 = T0;
        } else if (i2 == 8388613) {
            this.P0 = W0;
        } else {
            throw new IllegalArgumentException("Invalid slide direction");
        }
        ro3 ro3Var = new ro3();
        ro3Var.j(i2);
        n0(ro3Var);
    }

    @Override // androidx.transition.Visibility, androidx.transition.Transition
    public void h(kb4 kb4Var) {
        super.h(kb4Var);
        s0(kb4Var);
    }

    @Override // androidx.transition.Visibility, androidx.transition.Transition
    public void l(kb4 kb4Var) {
        super.l(kb4Var);
        s0(kb4Var);
    }

    @Override // androidx.transition.Visibility
    public Animator w0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        if (kb4Var2 == null) {
            return null;
        }
        int[] iArr = (int[]) kb4Var2.a.get("android:slide:screenPosition");
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        return androidx.transition.f.a(view, kb4Var2, iArr[0], iArr[1], this.P0.b(viewGroup, view), this.P0.a(viewGroup, view), translationX, translationY, Q0, this);
    }

    @Override // androidx.transition.Visibility
    public Animator z0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        if (kb4Var == null) {
            return null;
        }
        int[] iArr = (int[]) kb4Var.a.get("android:slide:screenPosition");
        return androidx.transition.f.a(view, kb4Var, iArr[0], iArr[1], view.getTranslationX(), view.getTranslationY(), this.P0.b(viewGroup, view), this.P0.a(viewGroup, view), R0, this);
    }

    public Slide(int i2) {
        this.P0 = X0;
        B0(i2);
    }

    @SuppressLint({"RestrictedApi"})
    public Slide(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.P0 = X0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hv3.f);
        int g2 = xd4.g(obtainStyledAttributes, (XmlPullParser) attributeSet, "slideEdge", 0, 80);
        obtainStyledAttributes.recycle();
        B0(g2);
    }
}
