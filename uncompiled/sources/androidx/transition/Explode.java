package androidx.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class Explode extends Visibility {
    public static final TimeInterpolator Q0 = new DecelerateInterpolator();
    public static final TimeInterpolator R0 = new AccelerateInterpolator();
    public int[] P0;

    public Explode() {
        this.P0 = new int[2];
        n0(new zy());
    }

    public static float B0(float f, float f2) {
        return (float) Math.sqrt((f * f) + (f2 * f2));
    }

    public static float C0(View view, int i, int i2) {
        return B0(Math.max(i, view.getWidth() - i), Math.max(i2, view.getHeight() - i2));
    }

    private void s0(kb4 kb4Var) {
        View view = kb4Var.b;
        view.getLocationOnScreen(this.P0);
        int[] iArr = this.P0;
        int i = iArr[0];
        int i2 = iArr[1];
        kb4Var.a.put("android:explode:screenBounds", new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2));
    }

    public final void D0(View view, Rect rect, int[] iArr) {
        int centerY;
        int i;
        view.getLocationOnScreen(this.P0);
        int[] iArr2 = this.P0;
        int i2 = iArr2[0];
        int i3 = iArr2[1];
        Rect y = y();
        if (y == null) {
            i = (view.getWidth() / 2) + i2 + Math.round(view.getTranslationX());
            centerY = (view.getHeight() / 2) + i3 + Math.round(view.getTranslationY());
        } else {
            int centerX = y.centerX();
            centerY = y.centerY();
            i = centerX;
        }
        float centerX2 = rect.centerX() - i;
        float centerY2 = rect.centerY() - centerY;
        if (centerX2 == Utils.FLOAT_EPSILON && centerY2 == Utils.FLOAT_EPSILON) {
            centerX2 = ((float) (Math.random() * 2.0d)) - 1.0f;
            centerY2 = ((float) (Math.random() * 2.0d)) - 1.0f;
        }
        float B0 = B0(centerX2, centerY2);
        float C0 = C0(view, i - i2, centerY - i3);
        iArr[0] = Math.round((centerX2 / B0) * C0);
        iArr[1] = Math.round(C0 * (centerY2 / B0));
    }

    @Override // androidx.transition.Visibility, androidx.transition.Transition
    public void h(kb4 kb4Var) {
        super.h(kb4Var);
        s0(kb4Var);
    }

    @Override // androidx.transition.Visibility, androidx.transition.Transition
    public void l(kb4 kb4Var) {
        super.l(kb4Var);
        s0(kb4Var);
    }

    @Override // androidx.transition.Visibility
    public Animator w0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        if (kb4Var2 == null) {
            return null;
        }
        Rect rect = (Rect) kb4Var2.a.get("android:explode:screenBounds");
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        D0(viewGroup, rect, this.P0);
        int[] iArr = this.P0;
        return f.a(view, kb4Var2, rect.left, rect.top, translationX + iArr[0], translationY + iArr[1], translationX, translationY, Q0, this);
    }

    @Override // androidx.transition.Visibility
    public Animator z0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        float f;
        float f2;
        if (kb4Var == null) {
            return null;
        }
        Rect rect = (Rect) kb4Var.a.get("android:explode:screenBounds");
        int i = rect.left;
        int i2 = rect.top;
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        int[] iArr = (int[]) kb4Var.b.getTag(zz2.transition_position);
        if (iArr != null) {
            f = (iArr[0] - rect.left) + translationX;
            f2 = (iArr[1] - rect.top) + translationY;
            rect.offsetTo(iArr[0], iArr[1]);
        } else {
            f = translationX;
            f2 = translationY;
        }
        D0(viewGroup, rect, this.P0);
        int[] iArr2 = this.P0;
        return f.a(view, kb4Var, i, i2, translationX, translationY, f + iArr2[0], f2 + iArr2[1], R0, this);
    }

    public Explode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.P0 = new int[2];
        n0(new zy());
    }
}
