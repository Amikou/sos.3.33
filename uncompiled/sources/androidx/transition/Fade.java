package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class Fade extends Visibility {

    /* loaded from: classes.dex */
    public class a extends c {
        public final /* synthetic */ View a;

        public a(Fade fade, View view) {
            this.a = view;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            nk4.h(this.a, 1.0f);
            nk4.a(this.a);
            transition.b0(this);
        }
    }

    /* loaded from: classes.dex */
    public static class b extends AnimatorListenerAdapter {
        public final View a;
        public boolean f0 = false;

        public b(View view) {
            this.a = view;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            nk4.h(this.a, 1.0f);
            if (this.f0) {
                this.a.setLayerType(0, null);
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            if (ei4.S(this.a) && this.a.getLayerType() == 0) {
                this.f0 = true;
                this.a.setLayerType(2, null);
            }
        }
    }

    public Fade(int i) {
        A0(i);
    }

    public static float C0(kb4 kb4Var, float f) {
        Float f2;
        return (kb4Var == null || (f2 = (Float) kb4Var.a.get("android:fade:transitionAlpha")) == null) ? f : f2.floatValue();
    }

    public final Animator B0(View view, float f, float f2) {
        if (f == f2) {
            return null;
        }
        nk4.h(view, f);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, nk4.b, f2);
        ofFloat.addListener(new b(view));
        a(new a(this, view));
        return ofFloat;
    }

    @Override // androidx.transition.Visibility, androidx.transition.Transition
    public void l(kb4 kb4Var) {
        super.l(kb4Var);
        kb4Var.a.put("android:fade:transitionAlpha", Float.valueOf(nk4.c(kb4Var.b)));
    }

    @Override // androidx.transition.Visibility
    public Animator w0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        float f = Utils.FLOAT_EPSILON;
        float C0 = C0(kb4Var, Utils.FLOAT_EPSILON);
        if (C0 != 1.0f) {
            f = C0;
        }
        return B0(view, f, 1.0f);
    }

    @Override // androidx.transition.Visibility
    public Animator z0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        nk4.e(view);
        return B0(view, C0(kb4Var, 1.0f), Utils.FLOAT_EPSILON);
    }

    public Fade() {
    }

    @SuppressLint({"RestrictedApi"})
    public Fade(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hv3.d);
        A0(xd4.g(obtainStyledAttributes, (XmlResourceParser) attributeSet, "fadingMode", 0, t0()));
        obtainStyledAttributes.recycle();
    }
}
