package androidx.transition;

import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes.dex */
public class AutoTransition extends TransitionSet {
    public AutoTransition() {
        K0();
    }

    public final void K0() {
        D0(1);
        u0(new Fade(2)).u0(new ChangeBounds()).u0(new Fade(1));
    }

    public AutoTransition(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        K0();
    }
}
