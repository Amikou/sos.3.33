package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.transition.Transition;
import androidx.transition.a;

/* loaded from: classes.dex */
public abstract class Visibility extends Transition {
    public static final String[] O0 = {"android:visibility:visibility", "android:visibility:parent"};
    public int N0;

    /* loaded from: classes.dex */
    public class a extends androidx.transition.c {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ View g0;

        public a(ViewGroup viewGroup, View view, View view2) {
            this.a = viewGroup;
            this.f0 = view;
            this.g0 = view2;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            this.g0.setTag(zz2.save_overlay_view, null);
            qi4.b(this.a).d(this.f0);
            transition.b0(this);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionPause(Transition transition) {
            qi4.b(this.a).d(this.f0);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionResume(Transition transition) {
            if (this.f0.getParent() == null) {
                qi4.b(this.a).c(this.f0);
            } else {
                Visibility.this.cancel();
            }
        }
    }

    /* loaded from: classes.dex */
    public static class b extends AnimatorListenerAdapter implements Transition.f, a.InterfaceC0065a {
        public final View a;
        public final int f0;
        public final ViewGroup g0;
        public final boolean h0;
        public boolean i0;
        public boolean j0 = false;

        public b(View view, int i, boolean z) {
            this.a = view;
            this.f0 = i;
            this.g0 = (ViewGroup) view.getParent();
            this.h0 = z;
            b(true);
        }

        public final void a() {
            if (!this.j0) {
                nk4.i(this.a, this.f0);
                ViewGroup viewGroup = this.g0;
                if (viewGroup != null) {
                    viewGroup.invalidate();
                }
            }
            b(false);
        }

        public final void b(boolean z) {
            ViewGroup viewGroup;
            if (!this.h0 || this.i0 == z || (viewGroup = this.g0) == null) {
                return;
            }
            this.i0 = z;
            qi4.d(viewGroup, z);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.j0 = true;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            a();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener, androidx.transition.a.InterfaceC0065a
        public void onAnimationPause(Animator animator) {
            if (this.j0) {
                return;
            }
            nk4.i(this.a, this.f0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener, androidx.transition.a.InterfaceC0065a
        public void onAnimationResume(Animator animator) {
            if (this.j0) {
                return;
            }
            nk4.i(this.a, 0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionCancel(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            a();
            transition.b0(this);
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionPause(Transition transition) {
            b(false);
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionResume(Transition transition) {
            b(true);
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionStart(Transition transition) {
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public boolean a;
        public boolean b;
        public int c;
        public int d;
        public ViewGroup e;
        public ViewGroup f;
    }

    public Visibility() {
        this.N0 = 3;
    }

    public void A0(int i) {
        if ((i & (-4)) == 0) {
            this.N0 = i;
            return;
        }
        throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
    }

    @Override // androidx.transition.Transition
    public String[] N() {
        return O0;
    }

    @Override // androidx.transition.Transition
    public boolean P(kb4 kb4Var, kb4 kb4Var2) {
        if (kb4Var == null && kb4Var2 == null) {
            return false;
        }
        if (kb4Var == null || kb4Var2 == null || kb4Var2.a.containsKey("android:visibility:visibility") == kb4Var.a.containsKey("android:visibility:visibility")) {
            c u0 = u0(kb4Var, kb4Var2);
            if (u0.a) {
                return u0.c == 0 || u0.d == 0;
            }
            return false;
        }
        return false;
    }

    @Override // androidx.transition.Transition
    public void h(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public void l(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public Animator s(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        c u0 = u0(kb4Var, kb4Var2);
        if (u0.a) {
            if (u0.e == null && u0.f == null) {
                return null;
            }
            if (u0.b) {
                return v0(viewGroup, kb4Var, u0.c, kb4Var2, u0.d);
            }
            return x0(viewGroup, kb4Var, u0.c, kb4Var2, u0.d);
        }
        return null;
    }

    public final void s0(kb4 kb4Var) {
        kb4Var.a.put("android:visibility:visibility", Integer.valueOf(kb4Var.b.getVisibility()));
        kb4Var.a.put("android:visibility:parent", kb4Var.b.getParent());
        int[] iArr = new int[2];
        kb4Var.b.getLocationOnScreen(iArr);
        kb4Var.a.put("android:visibility:screenLocation", iArr);
    }

    public int t0() {
        return this.N0;
    }

    public final c u0(kb4 kb4Var, kb4 kb4Var2) {
        c cVar = new c();
        cVar.a = false;
        cVar.b = false;
        if (kb4Var != null && kb4Var.a.containsKey("android:visibility:visibility")) {
            cVar.c = ((Integer) kb4Var.a.get("android:visibility:visibility")).intValue();
            cVar.e = (ViewGroup) kb4Var.a.get("android:visibility:parent");
        } else {
            cVar.c = -1;
            cVar.e = null;
        }
        if (kb4Var2 != null && kb4Var2.a.containsKey("android:visibility:visibility")) {
            cVar.d = ((Integer) kb4Var2.a.get("android:visibility:visibility")).intValue();
            cVar.f = (ViewGroup) kb4Var2.a.get("android:visibility:parent");
        } else {
            cVar.d = -1;
            cVar.f = null;
        }
        if (kb4Var != null && kb4Var2 != null) {
            int i = cVar.c;
            int i2 = cVar.d;
            if (i == i2 && cVar.e == cVar.f) {
                return cVar;
            }
            if (i != i2) {
                if (i == 0) {
                    cVar.b = false;
                    cVar.a = true;
                } else if (i2 == 0) {
                    cVar.b = true;
                    cVar.a = true;
                }
            } else if (cVar.f == null) {
                cVar.b = false;
                cVar.a = true;
            } else if (cVar.e == null) {
                cVar.b = true;
                cVar.a = true;
            }
        } else if (kb4Var == null && cVar.d == 0) {
            cVar.b = true;
            cVar.a = true;
        } else if (kb4Var2 == null && cVar.c == 0) {
            cVar.b = false;
            cVar.a = true;
        }
        return cVar;
    }

    public Animator v0(ViewGroup viewGroup, kb4 kb4Var, int i, kb4 kb4Var2, int i2) {
        if ((this.N0 & 1) != 1 || kb4Var2 == null) {
            return null;
        }
        if (kb4Var == null) {
            View view = (View) kb4Var2.b.getParent();
            if (u0(C(view, false), O(view, false)).a) {
                return null;
            }
        }
        return w0(viewGroup, kb4Var2.b, kb4Var, kb4Var2);
    }

    public Animator w0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:42:0x0089, code lost:
        if (r17.z0 != false) goto L52;
     */
    /* JADX WARN: Removed duplicated region for block: B:27:0x004a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public android.animation.Animator x0(android.view.ViewGroup r18, defpackage.kb4 r19, int r20, defpackage.kb4 r21, int r22) {
        /*
            Method dump skipped, instructions count: 264
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.transition.Visibility.x0(android.view.ViewGroup, kb4, int, kb4, int):android.animation.Animator");
    }

    public Animator z0(ViewGroup viewGroup, View view, kb4 kb4Var, kb4 kb4Var2) {
        return null;
    }

    @SuppressLint({"RestrictedApi"})
    public Visibility(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.N0 = 3;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hv3.c);
        int g = xd4.g(obtainStyledAttributes, (XmlResourceParser) attributeSet, "transitionVisibilityMode", 0, 0);
        obtainStyledAttributes.recycle();
        if (g != 0) {
            A0(g);
        }
    }
}
