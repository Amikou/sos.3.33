package androidx.transition;

import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.transition.Transition;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes.dex */
public class TransitionSet extends Transition {
    public ArrayList<Transition> N0;
    public boolean O0;
    public int P0;
    public boolean Q0;
    public int R0;

    /* loaded from: classes.dex */
    public class a extends c {
        public final /* synthetic */ Transition a;

        public a(TransitionSet transitionSet, Transition transition) {
            this.a = transition;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            this.a.f0();
            transition.b0(this);
        }
    }

    /* loaded from: classes.dex */
    public static class b extends c {
        public TransitionSet a;

        public b(TransitionSet transitionSet) {
            this.a = transitionSet;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            TransitionSet transitionSet = this.a;
            int i = transitionSet.P0 - 1;
            transitionSet.P0 = i;
            if (i == 0) {
                transitionSet.Q0 = false;
                transitionSet.u();
            }
            transition.b0(this);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionStart(Transition transition) {
            TransitionSet transitionSet = this.a;
            if (transitionSet.Q0) {
                return;
            }
            transitionSet.q0();
            this.a.Q0 = true;
        }
    }

    public TransitionSet() {
        this.N0 = new ArrayList<>();
        this.O0 = true;
        this.Q0 = false;
        this.R0 = 0;
    }

    @Override // androidx.transition.Transition
    /* renamed from: A0 */
    public TransitionSet c0(View view) {
        for (int i = 0; i < this.N0.size(); i++) {
            this.N0.get(i).c0(view);
        }
        return (TransitionSet) super.c0(view);
    }

    @Override // androidx.transition.Transition
    /* renamed from: B0 */
    public TransitionSet h0(long j) {
        ArrayList<Transition> arrayList;
        super.h0(j);
        if (this.g0 >= 0 && (arrayList = this.N0) != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                this.N0.get(i).h0(j);
            }
        }
        return this;
    }

    @Override // androidx.transition.Transition
    /* renamed from: C0 */
    public TransitionSet k0(TimeInterpolator timeInterpolator) {
        this.R0 |= 1;
        ArrayList<Transition> arrayList = this.N0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                this.N0.get(i).k0(timeInterpolator);
            }
        }
        return (TransitionSet) super.k0(timeInterpolator);
    }

    public TransitionSet D0(int i) {
        if (i == 0) {
            this.O0 = true;
        } else if (i == 1) {
            this.O0 = false;
        } else {
            throw new AndroidRuntimeException("Invalid parameter for TransitionSet ordering: " + i);
        }
        return this;
    }

    @Override // androidx.transition.Transition
    /* renamed from: E0 */
    public TransitionSet o0(ViewGroup viewGroup) {
        super.o0(viewGroup);
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).o0(viewGroup);
        }
        return this;
    }

    @Override // androidx.transition.Transition
    /* renamed from: G0 */
    public TransitionSet p0(long j) {
        return (TransitionSet) super.p0(j);
    }

    public final void I0() {
        b bVar = new b(this);
        Iterator<Transition> it = this.N0.iterator();
        while (it.hasNext()) {
            it.next().a(bVar);
        }
        this.P0 = this.N0.size();
    }

    @Override // androidx.transition.Transition
    public void Z(View view) {
        super.Z(view);
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).Z(view);
        }
    }

    @Override // androidx.transition.Transition
    public void cancel() {
        super.cancel();
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).cancel();
        }
    }

    @Override // androidx.transition.Transition
    public void d0(View view) {
        super.d0(view);
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).d0(view);
        }
    }

    @Override // androidx.transition.Transition
    public void f0() {
        if (this.N0.isEmpty()) {
            q0();
            u();
            return;
        }
        I0();
        if (!this.O0) {
            for (int i = 1; i < this.N0.size(); i++) {
                this.N0.get(i - 1).a(new a(this, this.N0.get(i)));
            }
            Transition transition = this.N0.get(0);
            if (transition != null) {
                transition.f0();
                return;
            }
            return;
        }
        Iterator<Transition> it = this.N0.iterator();
        while (it.hasNext()) {
            it.next().f0();
        }
    }

    @Override // androidx.transition.Transition
    public void g0(boolean z) {
        super.g0(z);
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).g0(z);
        }
    }

    @Override // androidx.transition.Transition
    public void h(kb4 kb4Var) {
        if (R(kb4Var.b)) {
            Iterator<Transition> it = this.N0.iterator();
            while (it.hasNext()) {
                Transition next = it.next();
                if (next.R(kb4Var.b)) {
                    next.h(kb4Var);
                    kb4Var.c.add(next);
                }
            }
        }
    }

    @Override // androidx.transition.Transition
    public void j0(Transition.e eVar) {
        super.j0(eVar);
        this.R0 |= 8;
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).j0(eVar);
        }
    }

    @Override // androidx.transition.Transition
    public void k(kb4 kb4Var) {
        super.k(kb4Var);
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).k(kb4Var);
        }
    }

    @Override // androidx.transition.Transition
    public void l(kb4 kb4Var) {
        if (R(kb4Var.b)) {
            Iterator<Transition> it = this.N0.iterator();
            while (it.hasNext()) {
                Transition next = it.next();
                if (next.R(kb4Var.b)) {
                    next.l(kb4Var);
                    kb4Var.c.add(next);
                }
            }
        }
    }

    @Override // androidx.transition.Transition
    public void m0(PathMotion pathMotion) {
        super.m0(pathMotion);
        this.R0 |= 4;
        if (this.N0 != null) {
            for (int i = 0; i < this.N0.size(); i++) {
                this.N0.get(i).m0(pathMotion);
            }
        }
    }

    @Override // androidx.transition.Transition
    public void n0(jb4 jb4Var) {
        super.n0(jb4Var);
        this.R0 |= 2;
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).n0(jb4Var);
        }
    }

    @Override // androidx.transition.Transition
    /* renamed from: r */
    public Transition clone() {
        TransitionSet transitionSet = (TransitionSet) super.clone();
        transitionSet.N0 = new ArrayList<>();
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            transitionSet.v0(this.N0.get(i).clone());
        }
        return transitionSet;
    }

    @Override // androidx.transition.Transition
    public String r0(String str) {
        String r0 = super.r0(str);
        for (int i = 0; i < this.N0.size(); i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(r0);
            sb.append("\n");
            sb.append(this.N0.get(i).r0(str + "  "));
            r0 = sb.toString();
        }
        return r0;
    }

    @Override // androidx.transition.Transition
    /* renamed from: s0 */
    public TransitionSet a(Transition.f fVar) {
        return (TransitionSet) super.a(fVar);
    }

    @Override // androidx.transition.Transition
    public void t(ViewGroup viewGroup, lb4 lb4Var, lb4 lb4Var2, ArrayList<kb4> arrayList, ArrayList<kb4> arrayList2) {
        long I = I();
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            Transition transition = this.N0.get(i);
            if (I > 0 && (this.O0 || i == 0)) {
                long I2 = transition.I();
                if (I2 > 0) {
                    transition.p0(I2 + I);
                } else {
                    transition.p0(I);
                }
            }
            transition.t(viewGroup, lb4Var, lb4Var2, arrayList, arrayList2);
        }
    }

    @Override // androidx.transition.Transition
    /* renamed from: t0 */
    public TransitionSet b(View view) {
        for (int i = 0; i < this.N0.size(); i++) {
            this.N0.get(i).b(view);
        }
        return (TransitionSet) super.b(view);
    }

    public TransitionSet u0(Transition transition) {
        v0(transition);
        long j = this.g0;
        if (j >= 0) {
            transition.h0(j);
        }
        if ((this.R0 & 1) != 0) {
            transition.k0(B());
        }
        if ((this.R0 & 2) != 0) {
            transition.n0(G());
        }
        if ((this.R0 & 4) != 0) {
            transition.m0(E());
        }
        if ((this.R0 & 8) != 0) {
            transition.j0(A());
        }
        return this;
    }

    @Override // androidx.transition.Transition
    public void v(ViewGroup viewGroup) {
        super.v(viewGroup);
        int size = this.N0.size();
        for (int i = 0; i < size; i++) {
            this.N0.get(i).v(viewGroup);
        }
    }

    public final void v0(Transition transition) {
        this.N0.add(transition);
        transition.v0 = this;
    }

    public Transition w0(int i) {
        if (i < 0 || i >= this.N0.size()) {
            return null;
        }
        return this.N0.get(i);
    }

    public int x0() {
        return this.N0.size();
    }

    @Override // androidx.transition.Transition
    /* renamed from: z0 */
    public TransitionSet b0(Transition.f fVar) {
        return (TransitionSet) super.b0(fVar);
    }

    @SuppressLint({"RestrictedApi"})
    public TransitionSet(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.N0 = new ArrayList<>();
        this.O0 = true;
        this.Q0 = false;
        this.R0 = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hv3.g);
        D0(xd4.g(obtainStyledAttributes, (XmlResourceParser) attributeSet, "transitionOrdering", 0, 0));
        obtainStyledAttributes.recycle();
    }
}
