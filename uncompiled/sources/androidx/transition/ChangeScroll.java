package androidx.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/* loaded from: classes.dex */
public class ChangeScroll extends Transition {
    public static final String[] N0 = {"android:changeScroll:x", "android:changeScroll:y"};

    public ChangeScroll() {
    }

    @Override // androidx.transition.Transition
    public String[] N() {
        return N0;
    }

    @Override // androidx.transition.Transition
    public void h(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public void l(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public Animator s(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        ObjectAnimator objectAnimator;
        ObjectAnimator objectAnimator2 = null;
        if (kb4Var == null || kb4Var2 == null) {
            return null;
        }
        View view = kb4Var2.b;
        int intValue = ((Integer) kb4Var.a.get("android:changeScroll:x")).intValue();
        int intValue2 = ((Integer) kb4Var2.a.get("android:changeScroll:x")).intValue();
        int intValue3 = ((Integer) kb4Var.a.get("android:changeScroll:y")).intValue();
        int intValue4 = ((Integer) kb4Var2.a.get("android:changeScroll:y")).intValue();
        if (intValue != intValue2) {
            view.setScrollX(intValue);
            objectAnimator = ObjectAnimator.ofInt(view, "scrollX", intValue, intValue2);
        } else {
            objectAnimator = null;
        }
        if (intValue3 != intValue4) {
            view.setScrollY(intValue3);
            objectAnimator2 = ObjectAnimator.ofInt(view, "scrollY", intValue3, intValue4);
        }
        return e.c(objectAnimator, objectAnimator2);
    }

    public final void s0(kb4 kb4Var) {
        kb4Var.a.put("android:changeScroll:x", Integer.valueOf(kb4Var.b.getScrollX()));
        kb4Var.a.put("android:changeScroll:y", Integer.valueOf(kb4Var.b.getScrollY()));
    }

    public ChangeScroll(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
