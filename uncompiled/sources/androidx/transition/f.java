package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.view.View;
import androidx.transition.Transition;

/* compiled from: TranslationAnimationCreator.java */
/* loaded from: classes.dex */
public class f {

    /* compiled from: TranslationAnimationCreator.java */
    /* loaded from: classes.dex */
    public static class a extends AnimatorListenerAdapter implements Transition.f {
        public final View a;
        public final View f0;
        public final int g0;
        public final int h0;
        public int[] i0;
        public float j0;
        public float k0;
        public final float l0;
        public final float m0;

        public a(View view, View view2, int i, int i2, float f, float f2) {
            this.f0 = view;
            this.a = view2;
            this.g0 = i - Math.round(view.getTranslationX());
            this.h0 = i2 - Math.round(view.getTranslationY());
            this.l0 = f;
            this.m0 = f2;
            int i3 = zz2.transition_position;
            int[] iArr = (int[]) view2.getTag(i3);
            this.i0 = iArr;
            if (iArr != null) {
                view2.setTag(i3, null);
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            if (this.i0 == null) {
                this.i0 = new int[2];
            }
            this.i0[0] = Math.round(this.g0 + this.f0.getTranslationX());
            this.i0[1] = Math.round(this.h0 + this.f0.getTranslationY());
            this.a.setTag(zz2.transition_position, this.i0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener
        public void onAnimationPause(Animator animator) {
            this.j0 = this.f0.getTranslationX();
            this.k0 = this.f0.getTranslationY();
            this.f0.setTranslationX(this.l0);
            this.f0.setTranslationY(this.m0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener
        public void onAnimationResume(Animator animator) {
            this.f0.setTranslationX(this.j0);
            this.f0.setTranslationY(this.k0);
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionCancel(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            this.f0.setTranslationX(this.l0);
            this.f0.setTranslationY(this.m0);
            transition.b0(this);
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionPause(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionResume(Transition transition) {
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionStart(Transition transition) {
        }
    }

    public static Animator a(View view, kb4 kb4Var, int i, int i2, float f, float f2, float f3, float f4, TimeInterpolator timeInterpolator, Transition transition) {
        float f5;
        float f6;
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        int[] iArr = (int[]) kb4Var.b.getTag(zz2.transition_position);
        if (iArr != null) {
            f5 = (iArr[0] - i) + translationX;
            f6 = (iArr[1] - i2) + translationY;
        } else {
            f5 = f;
            f6 = f2;
        }
        int round = i + Math.round(f5 - translationX);
        int round2 = i2 + Math.round(f6 - translationY);
        view.setTranslationX(f5);
        view.setTranslationY(f6);
        if (f5 == f3 && f6 == f4) {
            return null;
        }
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(view, PropertyValuesHolder.ofFloat(View.TRANSLATION_X, f5, f3), PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, f6, f4));
        a aVar = new a(view, kb4Var.b, round, round2, translationX, translationY);
        transition.a(aVar);
        ofPropertyValuesHolder.addListener(aVar);
        androidx.transition.a.a(ofPropertyValuesHolder, aVar);
        ofPropertyValuesHolder.setInterpolator(timeInterpolator);
        return ofPropertyValuesHolder;
    }
}
