package androidx.transition;

import androidx.transition.Transition;

/* compiled from: TransitionListenerAdapter.java */
/* loaded from: classes.dex */
public class c implements Transition.f {
    @Override // androidx.transition.Transition.f
    public void onTransitionCancel(Transition transition) {
    }

    @Override // androidx.transition.Transition.f
    public void onTransitionPause(Transition transition) {
    }

    @Override // androidx.transition.Transition.f
    public void onTransitionResume(Transition transition) {
    }

    @Override // androidx.transition.Transition.f
    public void onTransitionStart(Transition transition) {
    }
}
