package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;

/* loaded from: classes.dex */
public class ChangeClipBounds extends Transition {
    public static final String[] N0 = {"android:clipBounds:clip"};

    /* loaded from: classes.dex */
    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ View a;

        public a(ChangeClipBounds changeClipBounds, View view) {
            this.a = view;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            ei4.z0(this.a, null);
        }
    }

    public ChangeClipBounds() {
    }

    @Override // androidx.transition.Transition
    public String[] N() {
        return N0;
    }

    @Override // androidx.transition.Transition
    public void h(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public void l(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public Animator s(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        ObjectAnimator objectAnimator = null;
        if (kb4Var != null && kb4Var2 != null && kb4Var.a.containsKey("android:clipBounds:clip") && kb4Var2.a.containsKey("android:clipBounds:clip")) {
            Rect rect = (Rect) kb4Var.a.get("android:clipBounds:clip");
            Rect rect2 = (Rect) kb4Var2.a.get("android:clipBounds:clip");
            boolean z = rect2 == null;
            if (rect == null && rect2 == null) {
                return null;
            }
            if (rect == null) {
                rect = (Rect) kb4Var.a.get("android:clipBounds:bounds");
            } else if (rect2 == null) {
                rect2 = (Rect) kb4Var2.a.get("android:clipBounds:bounds");
            }
            if (rect.equals(rect2)) {
                return null;
            }
            ei4.z0(kb4Var2.b, rect);
            objectAnimator = ObjectAnimator.ofObject(kb4Var2.b, (Property<View, V>) nk4.c, (TypeEvaluator) new y43(new Rect()), (Object[]) new Rect[]{rect, rect2});
            if (z) {
                objectAnimator.addListener(new a(this, kb4Var2.b));
            }
        }
        return objectAnimator;
    }

    public final void s0(kb4 kb4Var) {
        View view = kb4Var.b;
        if (view.getVisibility() == 8) {
            return;
        }
        Rect w = ei4.w(view);
        kb4Var.a.put("android:clipBounds:clip", w);
        if (w == null) {
            kb4Var.a.put("android:clipBounds:bounds", new Rect(0, 0, view.getWidth(), view.getHeight()));
        }
    }

    public ChangeClipBounds(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
