package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.InflateException;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes.dex */
public abstract class Transition implements Cloneable {
    public static final int[] K0 = {2, 1, 3, 4};
    public static final PathMotion L0 = new a();
    public static ThreadLocal<rh<Animator, d>> M0 = new ThreadLocal<>();
    public jb4 G0;
    public e H0;
    public rh<String, String> I0;
    public ArrayList<kb4> x0;
    public ArrayList<kb4> y0;
    public String a = getClass().getName();
    public long f0 = -1;
    public long g0 = -1;
    public TimeInterpolator h0 = null;
    public ArrayList<Integer> i0 = new ArrayList<>();
    public ArrayList<View> j0 = new ArrayList<>();
    public ArrayList<String> k0 = null;
    public ArrayList<Class<?>> l0 = null;
    public ArrayList<Integer> m0 = null;
    public ArrayList<View> n0 = null;
    public ArrayList<Class<?>> o0 = null;
    public ArrayList<String> p0 = null;
    public ArrayList<Integer> q0 = null;
    public ArrayList<View> r0 = null;
    public ArrayList<Class<?>> s0 = null;
    public lb4 t0 = new lb4();
    public lb4 u0 = new lb4();
    public TransitionSet v0 = null;
    public int[] w0 = K0;
    public boolean z0 = false;
    public ArrayList<Animator> A0 = new ArrayList<>();
    public int B0 = 0;
    public boolean C0 = false;
    public boolean D0 = false;
    public ArrayList<f> E0 = null;
    public ArrayList<Animator> F0 = new ArrayList<>();
    public PathMotion J0 = L0;

    /* loaded from: classes.dex */
    public static class a extends PathMotion {
        @Override // androidx.transition.PathMotion
        public Path a(float f, float f2, float f3, float f4) {
            Path path = new Path();
            path.moveTo(f, f2);
            path.lineTo(f3, f4);
            return path;
        }
    }

    /* loaded from: classes.dex */
    public class b extends AnimatorListenerAdapter {
        public final /* synthetic */ rh a;

        public b(rh rhVar) {
            this.a = rhVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.a.remove(animator);
            Transition.this.A0.remove(animator);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            Transition.this.A0.add(animator);
        }
    }

    /* loaded from: classes.dex */
    public class c extends AnimatorListenerAdapter {
        public c() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            Transition.this.u();
            animator.removeListener(this);
        }
    }

    /* loaded from: classes.dex */
    public static class d {
        public View a;
        public String b;
        public kb4 c;
        public ip4 d;
        public Transition e;

        public d(View view, String str, Transition transition, ip4 ip4Var, kb4 kb4Var) {
            this.a = view;
            this.b = str;
            this.c = kb4Var;
            this.d = ip4Var;
            this.e = transition;
        }
    }

    /* loaded from: classes.dex */
    public static abstract class e {
        public abstract Rect a(Transition transition);
    }

    /* loaded from: classes.dex */
    public interface f {
        void onTransitionCancel(Transition transition);

        void onTransitionEnd(Transition transition);

        void onTransitionPause(Transition transition);

        void onTransitionResume(Transition transition);

        void onTransitionStart(Transition transition);
    }

    public Transition() {
    }

    public static rh<Animator, d> H() {
        rh<Animator, d> rhVar = M0.get();
        if (rhVar == null) {
            rh<Animator, d> rhVar2 = new rh<>();
            M0.set(rhVar2);
            return rhVar2;
        }
        return rhVar;
    }

    public static boolean Q(int i) {
        return i >= 1 && i <= 4;
    }

    public static boolean S(kb4 kb4Var, kb4 kb4Var2, String str) {
        Object obj = kb4Var.a.get(str);
        Object obj2 = kb4Var2.a.get(str);
        if (obj == null && obj2 == null) {
            return false;
        }
        if (obj == null || obj2 == null) {
            return true;
        }
        return true ^ obj.equals(obj2);
    }

    public static int[] Y(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
        int[] iArr = new int[stringTokenizer.countTokens()];
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            String trim = stringTokenizer.nextToken().trim();
            if ("id".equalsIgnoreCase(trim)) {
                iArr[i] = 3;
            } else if ("instance".equalsIgnoreCase(trim)) {
                iArr[i] = 1;
            } else if (PublicResolver.FUNC_NAME.equalsIgnoreCase(trim)) {
                iArr[i] = 2;
            } else if ("itemId".equalsIgnoreCase(trim)) {
                iArr[i] = 4;
            } else if (trim.isEmpty()) {
                int[] iArr2 = new int[iArr.length - 1];
                System.arraycopy(iArr, 0, iArr2, 0, i);
                i--;
                iArr = iArr2;
            } else {
                throw new InflateException("Unknown match type in matchOrder: '" + trim + "'");
            }
            i++;
        }
        return iArr;
    }

    public static void e(lb4 lb4Var, View view, kb4 kb4Var) {
        lb4Var.a.put(view, kb4Var);
        int id = view.getId();
        if (id >= 0) {
            if (lb4Var.b.indexOfKey(id) >= 0) {
                lb4Var.b.put(id, null);
            } else {
                lb4Var.b.put(id, view);
            }
        }
        String N = ei4.N(view);
        if (N != null) {
            if (lb4Var.d.containsKey(N)) {
                lb4Var.d.put(N, null);
            } else {
                lb4Var.d.put(N, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            ListView listView = (ListView) view.getParent();
            if (listView.getAdapter().hasStableIds()) {
                long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                if (lb4Var.c.j(itemIdAtPosition) >= 0) {
                    View g = lb4Var.c.g(itemIdAtPosition);
                    if (g != null) {
                        ei4.C0(g, false);
                        lb4Var.c.o(itemIdAtPosition, null);
                        return;
                    }
                    return;
                }
                ei4.C0(view, true);
                lb4Var.c.o(itemIdAtPosition, view);
            }
        }
    }

    public static boolean f(int[] iArr, int i) {
        int i2 = iArr[i];
        for (int i3 = 0; i3 < i; i3++) {
            if (iArr[i3] == i2) {
                return true;
            }
        }
        return false;
    }

    public e A() {
        return this.H0;
    }

    public TimeInterpolator B() {
        return this.h0;
    }

    public kb4 C(View view, boolean z) {
        TransitionSet transitionSet = this.v0;
        if (transitionSet != null) {
            return transitionSet.C(view, z);
        }
        ArrayList<kb4> arrayList = z ? this.x0 : this.y0;
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        int i = -1;
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                break;
            }
            kb4 kb4Var = arrayList.get(i2);
            if (kb4Var == null) {
                return null;
            }
            if (kb4Var.b == view) {
                i = i2;
                break;
            }
            i2++;
        }
        if (i >= 0) {
            return (z ? this.y0 : this.x0).get(i);
        }
        return null;
    }

    public String D() {
        return this.a;
    }

    public PathMotion E() {
        return this.J0;
    }

    public jb4 G() {
        return this.G0;
    }

    public long I() {
        return this.f0;
    }

    public List<Integer> J() {
        return this.i0;
    }

    public List<String> K() {
        return this.k0;
    }

    public List<Class<?>> L() {
        return this.l0;
    }

    public List<View> M() {
        return this.j0;
    }

    public String[] N() {
        return null;
    }

    public kb4 O(View view, boolean z) {
        TransitionSet transitionSet = this.v0;
        if (transitionSet != null) {
            return transitionSet.O(view, z);
        }
        return (z ? this.t0 : this.u0).a.get(view);
    }

    public boolean P(kb4 kb4Var, kb4 kb4Var2) {
        if (kb4Var == null || kb4Var2 == null) {
            return false;
        }
        String[] N = N();
        if (N != null) {
            for (String str : N) {
                if (!S(kb4Var, kb4Var2, str)) {
                }
            }
            return false;
        }
        for (String str2 : kb4Var.a.keySet()) {
            if (S(kb4Var, kb4Var2, str2)) {
            }
        }
        return false;
        return true;
    }

    public boolean R(View view) {
        ArrayList<Class<?>> arrayList;
        ArrayList<String> arrayList2;
        int id = view.getId();
        ArrayList<Integer> arrayList3 = this.m0;
        if (arrayList3 == null || !arrayList3.contains(Integer.valueOf(id))) {
            ArrayList<View> arrayList4 = this.n0;
            if (arrayList4 == null || !arrayList4.contains(view)) {
                ArrayList<Class<?>> arrayList5 = this.o0;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    for (int i = 0; i < size; i++) {
                        if (this.o0.get(i).isInstance(view)) {
                            return false;
                        }
                    }
                }
                if (this.p0 == null || ei4.N(view) == null || !this.p0.contains(ei4.N(view))) {
                    if ((this.i0.size() == 0 && this.j0.size() == 0 && (((arrayList = this.l0) == null || arrayList.isEmpty()) && ((arrayList2 = this.k0) == null || arrayList2.isEmpty()))) || this.i0.contains(Integer.valueOf(id)) || this.j0.contains(view)) {
                        return true;
                    }
                    ArrayList<String> arrayList6 = this.k0;
                    if (arrayList6 == null || !arrayList6.contains(ei4.N(view))) {
                        if (this.l0 != null) {
                            for (int i2 = 0; i2 < this.l0.size(); i2++) {
                                if (this.l0.get(i2).isInstance(view)) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public final void T(rh<View, kb4> rhVar, rh<View, kb4> rhVar2, SparseArray<View> sparseArray, SparseArray<View> sparseArray2) {
        View view;
        int size = sparseArray.size();
        for (int i = 0; i < size; i++) {
            View valueAt = sparseArray.valueAt(i);
            if (valueAt != null && R(valueAt) && (view = sparseArray2.get(sparseArray.keyAt(i))) != null && R(view)) {
                kb4 kb4Var = rhVar.get(valueAt);
                kb4 kb4Var2 = rhVar2.get(view);
                if (kb4Var != null && kb4Var2 != null) {
                    this.x0.add(kb4Var);
                    this.y0.add(kb4Var2);
                    rhVar.remove(valueAt);
                    rhVar2.remove(view);
                }
            }
        }
    }

    public final void U(rh<View, kb4> rhVar, rh<View, kb4> rhVar2) {
        kb4 remove;
        for (int size = rhVar.size() - 1; size >= 0; size--) {
            View i = rhVar.i(size);
            if (i != null && R(i) && (remove = rhVar2.remove(i)) != null && R(remove.b)) {
                this.x0.add(rhVar.k(size));
                this.y0.add(remove);
            }
        }
    }

    public final void V(rh<View, kb4> rhVar, rh<View, kb4> rhVar2, i22<View> i22Var, i22<View> i22Var2) {
        View g;
        int t = i22Var.t();
        for (int i = 0; i < t; i++) {
            View u = i22Var.u(i);
            if (u != null && R(u) && (g = i22Var2.g(i22Var.l(i))) != null && R(g)) {
                kb4 kb4Var = rhVar.get(u);
                kb4 kb4Var2 = rhVar2.get(g);
                if (kb4Var != null && kb4Var2 != null) {
                    this.x0.add(kb4Var);
                    this.y0.add(kb4Var2);
                    rhVar.remove(u);
                    rhVar2.remove(g);
                }
            }
        }
    }

    public final void W(rh<View, kb4> rhVar, rh<View, kb4> rhVar2, rh<String, View> rhVar3, rh<String, View> rhVar4) {
        View view;
        int size = rhVar3.size();
        for (int i = 0; i < size; i++) {
            View m = rhVar3.m(i);
            if (m != null && R(m) && (view = rhVar4.get(rhVar3.i(i))) != null && R(view)) {
                kb4 kb4Var = rhVar.get(m);
                kb4 kb4Var2 = rhVar2.get(view);
                if (kb4Var != null && kb4Var2 != null) {
                    this.x0.add(kb4Var);
                    this.y0.add(kb4Var2);
                    rhVar.remove(m);
                    rhVar2.remove(view);
                }
            }
        }
    }

    public final void X(lb4 lb4Var, lb4 lb4Var2) {
        rh<View, kb4> rhVar = new rh<>(lb4Var.a);
        rh<View, kb4> rhVar2 = new rh<>(lb4Var2.a);
        int i = 0;
        while (true) {
            int[] iArr = this.w0;
            if (i < iArr.length) {
                int i2 = iArr[i];
                if (i2 == 1) {
                    U(rhVar, rhVar2);
                } else if (i2 == 2) {
                    W(rhVar, rhVar2, lb4Var.d, lb4Var2.d);
                } else if (i2 == 3) {
                    T(rhVar, rhVar2, lb4Var.b, lb4Var2.b);
                } else if (i2 == 4) {
                    V(rhVar, rhVar2, lb4Var.c, lb4Var2.c);
                }
                i++;
            } else {
                d(rhVar, rhVar2);
                return;
            }
        }
    }

    public void Z(View view) {
        if (this.D0) {
            return;
        }
        rh<Animator, d> H = H();
        int size = H.size();
        ip4 d2 = nk4.d(view);
        for (int i = size - 1; i >= 0; i--) {
            d m = H.m(i);
            if (m.a != null && d2.equals(m.d)) {
                androidx.transition.a.b(H.i(i));
            }
        }
        ArrayList<f> arrayList = this.E0;
        if (arrayList != null && arrayList.size() > 0) {
            ArrayList arrayList2 = (ArrayList) this.E0.clone();
            int size2 = arrayList2.size();
            for (int i2 = 0; i2 < size2; i2++) {
                ((f) arrayList2.get(i2)).onTransitionPause(this);
            }
        }
        this.C0 = true;
    }

    public Transition a(f fVar) {
        if (this.E0 == null) {
            this.E0 = new ArrayList<>();
        }
        this.E0.add(fVar);
        return this;
    }

    public void a0(ViewGroup viewGroup) {
        d dVar;
        this.x0 = new ArrayList<>();
        this.y0 = new ArrayList<>();
        X(this.t0, this.u0);
        rh<Animator, d> H = H();
        int size = H.size();
        ip4 d2 = nk4.d(viewGroup);
        for (int i = size - 1; i >= 0; i--) {
            Animator i2 = H.i(i);
            if (i2 != null && (dVar = H.get(i2)) != null && dVar.a != null && d2.equals(dVar.d)) {
                kb4 kb4Var = dVar.c;
                View view = dVar.a;
                kb4 O = O(view, true);
                kb4 C = C(view, true);
                if (O == null && C == null) {
                    C = this.u0.a.get(view);
                }
                if (!(O == null && C == null) && dVar.e.P(kb4Var, C)) {
                    if (!i2.isRunning() && !i2.isStarted()) {
                        H.remove(i2);
                    } else {
                        i2.cancel();
                    }
                }
            }
        }
        t(viewGroup, this.t0, this.u0, this.x0, this.y0);
        f0();
    }

    public Transition b(View view) {
        this.j0.add(view);
        return this;
    }

    public Transition b0(f fVar) {
        ArrayList<f> arrayList = this.E0;
        if (arrayList == null) {
            return this;
        }
        arrayList.remove(fVar);
        if (this.E0.size() == 0) {
            this.E0 = null;
        }
        return this;
    }

    public Transition c0(View view) {
        this.j0.remove(view);
        return this;
    }

    public void cancel() {
        for (int size = this.A0.size() - 1; size >= 0; size--) {
            this.A0.get(size).cancel();
        }
        ArrayList<f> arrayList = this.E0;
        if (arrayList == null || arrayList.size() <= 0) {
            return;
        }
        ArrayList arrayList2 = (ArrayList) this.E0.clone();
        int size2 = arrayList2.size();
        for (int i = 0; i < size2; i++) {
            ((f) arrayList2.get(i)).onTransitionCancel(this);
        }
    }

    public final void d(rh<View, kb4> rhVar, rh<View, kb4> rhVar2) {
        for (int i = 0; i < rhVar.size(); i++) {
            kb4 m = rhVar.m(i);
            if (R(m.b)) {
                this.x0.add(m);
                this.y0.add(null);
            }
        }
        for (int i2 = 0; i2 < rhVar2.size(); i2++) {
            kb4 m2 = rhVar2.m(i2);
            if (R(m2.b)) {
                this.y0.add(m2);
                this.x0.add(null);
            }
        }
    }

    public void d0(View view) {
        if (this.C0) {
            if (!this.D0) {
                rh<Animator, d> H = H();
                int size = H.size();
                ip4 d2 = nk4.d(view);
                for (int i = size - 1; i >= 0; i--) {
                    d m = H.m(i);
                    if (m.a != null && d2.equals(m.d)) {
                        androidx.transition.a.c(H.i(i));
                    }
                }
                ArrayList<f> arrayList = this.E0;
                if (arrayList != null && arrayList.size() > 0) {
                    ArrayList arrayList2 = (ArrayList) this.E0.clone();
                    int size2 = arrayList2.size();
                    for (int i2 = 0; i2 < size2; i2++) {
                        ((f) arrayList2.get(i2)).onTransitionResume(this);
                    }
                }
            }
            this.C0 = false;
        }
    }

    public final void e0(Animator animator, rh<Animator, d> rhVar) {
        if (animator != null) {
            animator.addListener(new b(rhVar));
            g(animator);
        }
    }

    public void f0() {
        q0();
        rh<Animator, d> H = H();
        Iterator<Animator> it = this.F0.iterator();
        while (it.hasNext()) {
            Animator next = it.next();
            if (H.containsKey(next)) {
                q0();
                e0(next, H);
            }
        }
        this.F0.clear();
        u();
    }

    public void g(Animator animator) {
        if (animator == null) {
            u();
            return;
        }
        if (x() >= 0) {
            animator.setDuration(x());
        }
        if (I() >= 0) {
            animator.setStartDelay(I() + animator.getStartDelay());
        }
        if (B() != null) {
            animator.setInterpolator(B());
        }
        animator.addListener(new c());
        animator.start();
    }

    public void g0(boolean z) {
        this.z0 = z;
    }

    public abstract void h(kb4 kb4Var);

    public Transition h0(long j) {
        this.g0 = j;
        return this;
    }

    public final void j(View view, boolean z) {
        if (view == null) {
            return;
        }
        int id = view.getId();
        ArrayList<Integer> arrayList = this.m0;
        if (arrayList == null || !arrayList.contains(Integer.valueOf(id))) {
            ArrayList<View> arrayList2 = this.n0;
            if (arrayList2 == null || !arrayList2.contains(view)) {
                ArrayList<Class<?>> arrayList3 = this.o0;
                if (arrayList3 != null) {
                    int size = arrayList3.size();
                    for (int i = 0; i < size; i++) {
                        if (this.o0.get(i).isInstance(view)) {
                            return;
                        }
                    }
                }
                if (view.getParent() instanceof ViewGroup) {
                    kb4 kb4Var = new kb4(view);
                    if (z) {
                        l(kb4Var);
                    } else {
                        h(kb4Var);
                    }
                    kb4Var.c.add(this);
                    k(kb4Var);
                    if (z) {
                        e(this.t0, view, kb4Var);
                    } else {
                        e(this.u0, view, kb4Var);
                    }
                }
                if (view instanceof ViewGroup) {
                    ArrayList<Integer> arrayList4 = this.q0;
                    if (arrayList4 == null || !arrayList4.contains(Integer.valueOf(id))) {
                        ArrayList<View> arrayList5 = this.r0;
                        if (arrayList5 == null || !arrayList5.contains(view)) {
                            ArrayList<Class<?>> arrayList6 = this.s0;
                            if (arrayList6 != null) {
                                int size2 = arrayList6.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    if (this.s0.get(i2).isInstance(view)) {
                                        return;
                                    }
                                }
                            }
                            ViewGroup viewGroup = (ViewGroup) view;
                            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                                j(viewGroup.getChildAt(i3), z);
                            }
                        }
                    }
                }
            }
        }
    }

    public void j0(e eVar) {
        this.H0 = eVar;
    }

    public void k(kb4 kb4Var) {
        String[] b2;
        if (this.G0 == null || kb4Var.a.isEmpty() || (b2 = this.G0.b()) == null) {
            return;
        }
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= b2.length) {
                z = true;
                break;
            } else if (!kb4Var.a.containsKey(b2[i])) {
                break;
            } else {
                i++;
            }
        }
        if (z) {
            return;
        }
        this.G0.a(kb4Var);
    }

    public Transition k0(TimeInterpolator timeInterpolator) {
        this.h0 = timeInterpolator;
        return this;
    }

    public abstract void l(kb4 kb4Var);

    public void l0(int... iArr) {
        if (iArr != null && iArr.length != 0) {
            for (int i = 0; i < iArr.length; i++) {
                if (Q(iArr[i])) {
                    if (f(iArr, i)) {
                        throw new IllegalArgumentException("matches contains a duplicate value");
                    }
                } else {
                    throw new IllegalArgumentException("matches contains invalid value");
                }
            }
            this.w0 = (int[]) iArr.clone();
            return;
        }
        this.w0 = K0;
    }

    public void m0(PathMotion pathMotion) {
        if (pathMotion == null) {
            this.J0 = L0;
        } else {
            this.J0 = pathMotion;
        }
    }

    public void n0(jb4 jb4Var) {
        this.G0 = jb4Var;
    }

    public void o(ViewGroup viewGroup, boolean z) {
        ArrayList<String> arrayList;
        ArrayList<Class<?>> arrayList2;
        rh<String, String> rhVar;
        p(z);
        if ((this.i0.size() <= 0 && this.j0.size() <= 0) || (((arrayList = this.k0) != null && !arrayList.isEmpty()) || ((arrayList2 = this.l0) != null && !arrayList2.isEmpty()))) {
            j(viewGroup, z);
        } else {
            for (int i = 0; i < this.i0.size(); i++) {
                View findViewById = viewGroup.findViewById(this.i0.get(i).intValue());
                if (findViewById != null) {
                    kb4 kb4Var = new kb4(findViewById);
                    if (z) {
                        l(kb4Var);
                    } else {
                        h(kb4Var);
                    }
                    kb4Var.c.add(this);
                    k(kb4Var);
                    if (z) {
                        e(this.t0, findViewById, kb4Var);
                    } else {
                        e(this.u0, findViewById, kb4Var);
                    }
                }
            }
            for (int i2 = 0; i2 < this.j0.size(); i2++) {
                View view = this.j0.get(i2);
                kb4 kb4Var2 = new kb4(view);
                if (z) {
                    l(kb4Var2);
                } else {
                    h(kb4Var2);
                }
                kb4Var2.c.add(this);
                k(kb4Var2);
                if (z) {
                    e(this.t0, view, kb4Var2);
                } else {
                    e(this.u0, view, kb4Var2);
                }
            }
        }
        if (z || (rhVar = this.I0) == null) {
            return;
        }
        int size = rhVar.size();
        ArrayList arrayList3 = new ArrayList(size);
        for (int i3 = 0; i3 < size; i3++) {
            arrayList3.add(this.t0.d.remove(this.I0.i(i3)));
        }
        for (int i4 = 0; i4 < size; i4++) {
            View view2 = (View) arrayList3.get(i4);
            if (view2 != null) {
                this.t0.d.put(this.I0.m(i4), view2);
            }
        }
    }

    public Transition o0(ViewGroup viewGroup) {
        return this;
    }

    public void p(boolean z) {
        if (z) {
            this.t0.a.clear();
            this.t0.b.clear();
            this.t0.c.b();
            return;
        }
        this.u0.a.clear();
        this.u0.b.clear();
        this.u0.c.b();
    }

    public Transition p0(long j) {
        this.f0 = j;
        return this;
    }

    public void q0() {
        if (this.B0 == 0) {
            ArrayList<f> arrayList = this.E0;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.E0.clone();
                int size = arrayList2.size();
                for (int i = 0; i < size; i++) {
                    ((f) arrayList2.get(i)).onTransitionStart(this);
                }
            }
            this.D0 = false;
        }
        this.B0++;
    }

    @Override // 
    /* renamed from: r */
    public Transition clone() {
        try {
            Transition transition = (Transition) super.clone();
            transition.F0 = new ArrayList<>();
            transition.t0 = new lb4();
            transition.u0 = new lb4();
            transition.x0 = null;
            transition.y0 = null;
            return transition;
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }

    public String r0(String str) {
        String str2 = str + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ": ";
        if (this.g0 != -1) {
            str2 = str2 + "dur(" + this.g0 + ") ";
        }
        if (this.f0 != -1) {
            str2 = str2 + "dly(" + this.f0 + ") ";
        }
        if (this.h0 != null) {
            str2 = str2 + "interp(" + this.h0 + ") ";
        }
        if (this.i0.size() > 0 || this.j0.size() > 0) {
            String str3 = str2 + "tgts(";
            if (this.i0.size() > 0) {
                for (int i = 0; i < this.i0.size(); i++) {
                    if (i > 0) {
                        str3 = str3 + ", ";
                    }
                    str3 = str3 + this.i0.get(i);
                }
            }
            if (this.j0.size() > 0) {
                for (int i2 = 0; i2 < this.j0.size(); i2++) {
                    if (i2 > 0) {
                        str3 = str3 + ", ";
                    }
                    str3 = str3 + this.j0.get(i2);
                }
            }
            return str3 + ")";
        }
        return str2;
    }

    public Animator s(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        return null;
    }

    public void t(ViewGroup viewGroup, lb4 lb4Var, lb4 lb4Var2, ArrayList<kb4> arrayList, ArrayList<kb4> arrayList2) {
        Animator s;
        int i;
        int i2;
        View view;
        Animator animator;
        kb4 kb4Var;
        Animator animator2;
        kb4 kb4Var2;
        rh<Animator, d> H = H();
        SparseIntArray sparseIntArray = new SparseIntArray();
        int size = arrayList.size();
        long j = Long.MAX_VALUE;
        int i3 = 0;
        while (i3 < size) {
            kb4 kb4Var3 = arrayList.get(i3);
            kb4 kb4Var4 = arrayList2.get(i3);
            if (kb4Var3 != null && !kb4Var3.c.contains(this)) {
                kb4Var3 = null;
            }
            if (kb4Var4 != null && !kb4Var4.c.contains(this)) {
                kb4Var4 = null;
            }
            if (kb4Var3 != null || kb4Var4 != null) {
                if ((kb4Var3 == null || kb4Var4 == null || P(kb4Var3, kb4Var4)) && (s = s(viewGroup, kb4Var3, kb4Var4)) != null) {
                    if (kb4Var4 != null) {
                        view = kb4Var4.b;
                        String[] N = N();
                        if (N != null && N.length > 0) {
                            kb4Var2 = new kb4(view);
                            i = size;
                            kb4 kb4Var5 = lb4Var2.a.get(view);
                            if (kb4Var5 != null) {
                                int i4 = 0;
                                while (i4 < N.length) {
                                    kb4Var2.a.put(N[i4], kb4Var5.a.get(N[i4]));
                                    i4++;
                                    i3 = i3;
                                    kb4Var5 = kb4Var5;
                                }
                            }
                            i2 = i3;
                            int size2 = H.size();
                            int i5 = 0;
                            while (true) {
                                if (i5 >= size2) {
                                    animator2 = s;
                                    break;
                                }
                                d dVar = H.get(H.i(i5));
                                if (dVar.c != null && dVar.a == view && dVar.b.equals(D()) && dVar.c.equals(kb4Var2)) {
                                    animator2 = null;
                                    break;
                                }
                                i5++;
                            }
                        } else {
                            i = size;
                            i2 = i3;
                            animator2 = s;
                            kb4Var2 = null;
                        }
                        animator = animator2;
                        kb4Var = kb4Var2;
                    } else {
                        i = size;
                        i2 = i3;
                        view = kb4Var3.b;
                        animator = s;
                        kb4Var = null;
                    }
                    if (animator != null) {
                        jb4 jb4Var = this.G0;
                        if (jb4Var != null) {
                            long c2 = jb4Var.c(viewGroup, this, kb4Var3, kb4Var4);
                            sparseIntArray.put(this.F0.size(), (int) c2);
                            j = Math.min(c2, j);
                        }
                        H.put(animator, new d(view, D(), this, nk4.d(viewGroup), kb4Var));
                        this.F0.add(animator);
                        j = j;
                    }
                    i3 = i2 + 1;
                    size = i;
                }
            }
            i = size;
            i2 = i3;
            i3 = i2 + 1;
            size = i;
        }
        if (sparseIntArray.size() != 0) {
            for (int i6 = 0; i6 < sparseIntArray.size(); i6++) {
                Animator animator3 = this.F0.get(sparseIntArray.keyAt(i6));
                animator3.setStartDelay((sparseIntArray.valueAt(i6) - j) + animator3.getStartDelay());
            }
        }
    }

    public String toString() {
        return r0("");
    }

    public void u() {
        int i = this.B0 - 1;
        this.B0 = i;
        if (i == 0) {
            ArrayList<f> arrayList = this.E0;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.E0.clone();
                int size = arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((f) arrayList2.get(i2)).onTransitionEnd(this);
                }
            }
            for (int i3 = 0; i3 < this.t0.c.t(); i3++) {
                View u = this.t0.c.u(i3);
                if (u != null) {
                    ei4.C0(u, false);
                }
            }
            for (int i4 = 0; i4 < this.u0.c.t(); i4++) {
                View u2 = this.u0.c.u(i4);
                if (u2 != null) {
                    ei4.C0(u2, false);
                }
            }
            this.D0 = true;
        }
    }

    public void v(ViewGroup viewGroup) {
        rh<Animator, d> H = H();
        int size = H.size();
        if (viewGroup == null || size == 0) {
            return;
        }
        ip4 d2 = nk4.d(viewGroup);
        rh rhVar = new rh(H);
        H.clear();
        for (int i = size - 1; i >= 0; i--) {
            d dVar = (d) rhVar.m(i);
            if (dVar.a != null && d2 != null && d2.equals(dVar.d)) {
                ((Animator) rhVar.i(i)).end();
            }
        }
    }

    public long x() {
        return this.g0;
    }

    public Rect y() {
        e eVar = this.H0;
        if (eVar == null) {
            return null;
        }
        return eVar.a(this);
    }

    @SuppressLint({"RestrictedApi"})
    public Transition(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hv3.a);
        XmlResourceParser xmlResourceParser = (XmlResourceParser) attributeSet;
        long g = xd4.g(obtainStyledAttributes, xmlResourceParser, "duration", 1, -1);
        if (g >= 0) {
            h0(g);
        }
        long g2 = xd4.g(obtainStyledAttributes, xmlResourceParser, "startDelay", 2, -1);
        if (g2 > 0) {
            p0(g2);
        }
        int h = xd4.h(obtainStyledAttributes, xmlResourceParser, "interpolator", 0, 0);
        if (h > 0) {
            k0(AnimationUtils.loadInterpolator(context, h));
        }
        String i = xd4.i(obtainStyledAttributes, xmlResourceParser, "matchOrder", 3);
        if (i != null) {
            l0(Y(i));
        }
        obtainStyledAttributes.recycle();
    }
}
