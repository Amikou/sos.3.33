package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;
import java.util.Map;

/* loaded from: classes.dex */
public class ChangeBounds extends Transition {
    public static final String[] Q0 = {"android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY"};
    public static final Property<Drawable, PointF> R0 = new b(PointF.class, "boundsOrigin");
    public static final Property<k, PointF> S0 = new c(PointF.class, "topLeft");
    public static final Property<k, PointF> T0 = new d(PointF.class, "bottomRight");
    public static final Property<View, PointF> U0 = new e(PointF.class, "bottomRight");
    public static final Property<View, PointF> V0 = new f(PointF.class, "topLeft");
    public static final Property<View, PointF> W0 = new g(PointF.class, "position");
    public static y43 X0 = new y43();
    public int[] N0;
    public boolean O0;
    public boolean P0;

    /* loaded from: classes.dex */
    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ BitmapDrawable f0;
        public final /* synthetic */ View g0;
        public final /* synthetic */ float h0;

        public a(ChangeBounds changeBounds, ViewGroup viewGroup, BitmapDrawable bitmapDrawable, View view, float f) {
            this.a = viewGroup;
            this.f0 = bitmapDrawable;
            this.g0 = view;
            this.h0 = f;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            nk4.b(this.a).b(this.f0);
            nk4.h(this.g0, this.h0);
        }
    }

    /* loaded from: classes.dex */
    public static class b extends Property<Drawable, PointF> {
        public Rect a;

        public b(Class cls, String str) {
            super(cls, str);
            this.a = new Rect();
        }

        @Override // android.util.Property
        /* renamed from: a */
        public PointF get(Drawable drawable) {
            drawable.copyBounds(this.a);
            Rect rect = this.a;
            return new PointF(rect.left, rect.top);
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(Drawable drawable, PointF pointF) {
            drawable.copyBounds(this.a);
            this.a.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
            drawable.setBounds(this.a);
        }
    }

    /* loaded from: classes.dex */
    public static class c extends Property<k, PointF> {
        public c(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public PointF get(k kVar) {
            return null;
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(k kVar, PointF pointF) {
            kVar.c(pointF);
        }
    }

    /* loaded from: classes.dex */
    public static class d extends Property<k, PointF> {
        public d(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public PointF get(k kVar) {
            return null;
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(k kVar, PointF pointF) {
            kVar.a(pointF);
        }
    }

    /* loaded from: classes.dex */
    public static class e extends Property<View, PointF> {
        public e(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, PointF pointF) {
            nk4.g(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
        }
    }

    /* loaded from: classes.dex */
    public static class f extends Property<View, PointF> {
        public f(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, PointF pointF) {
            nk4.g(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
        }
    }

    /* loaded from: classes.dex */
    public static class g extends Property<View, PointF> {
        public g(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, PointF pointF) {
            int round = Math.round(pointF.x);
            int round2 = Math.round(pointF.y);
            nk4.g(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
        }
    }

    /* loaded from: classes.dex */
    public class h extends AnimatorListenerAdapter {
        public final /* synthetic */ k a;
        private k mViewBounds;

        public h(ChangeBounds changeBounds, k kVar) {
            this.a = kVar;
            this.mViewBounds = kVar;
        }
    }

    /* loaded from: classes.dex */
    public class i extends AnimatorListenerAdapter {
        public boolean a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ Rect g0;
        public final /* synthetic */ int h0;
        public final /* synthetic */ int i0;
        public final /* synthetic */ int j0;
        public final /* synthetic */ int k0;

        public i(ChangeBounds changeBounds, View view, Rect rect, int i, int i2, int i3, int i4) {
            this.f0 = view;
            this.g0 = rect;
            this.h0 = i;
            this.i0 = i2;
            this.j0 = i3;
            this.k0 = i4;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (this.a) {
                return;
            }
            ei4.z0(this.f0, this.g0);
            nk4.g(this.f0, this.h0, this.i0, this.j0, this.k0);
        }
    }

    /* loaded from: classes.dex */
    public class j extends androidx.transition.c {
        public boolean a = false;
        public final /* synthetic */ ViewGroup f0;

        public j(ChangeBounds changeBounds, ViewGroup viewGroup) {
            this.f0 = viewGroup;
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionCancel(Transition transition) {
            qi4.d(this.f0, false);
            this.a = true;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            if (!this.a) {
                qi4.d(this.f0, false);
            }
            transition.b0(this);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionPause(Transition transition) {
            qi4.d(this.f0, false);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionResume(Transition transition) {
            qi4.d(this.f0, true);
        }
    }

    /* loaded from: classes.dex */
    public static class k {
        public int a;
        public int b;
        public int c;
        public int d;
        public View e;
        public int f;
        public int g;

        public k(View view) {
            this.e = view;
        }

        public void a(PointF pointF) {
            this.c = Math.round(pointF.x);
            this.d = Math.round(pointF.y);
            int i = this.g + 1;
            this.g = i;
            if (this.f == i) {
                b();
            }
        }

        public final void b() {
            nk4.g(this.e, this.a, this.b, this.c, this.d);
            this.f = 0;
            this.g = 0;
        }

        public void c(PointF pointF) {
            this.a = Math.round(pointF.x);
            this.b = Math.round(pointF.y);
            int i = this.f + 1;
            this.f = i;
            if (i == this.g) {
                b();
            }
        }
    }

    public ChangeBounds() {
        this.N0 = new int[2];
        this.O0 = false;
        this.P0 = false;
    }

    @Override // androidx.transition.Transition
    public String[] N() {
        return Q0;
    }

    @Override // androidx.transition.Transition
    public void h(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public void l(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public Animator s(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        int i2;
        View view;
        int i3;
        Rect rect;
        ObjectAnimator objectAnimator;
        Animator c2;
        if (kb4Var == null || kb4Var2 == null) {
            return null;
        }
        Map<String, Object> map = kb4Var.a;
        Map<String, Object> map2 = kb4Var2.a;
        ViewGroup viewGroup2 = (ViewGroup) map.get("android:changeBounds:parent");
        ViewGroup viewGroup3 = (ViewGroup) map2.get("android:changeBounds:parent");
        if (viewGroup2 == null || viewGroup3 == null) {
            return null;
        }
        View view2 = kb4Var2.b;
        if (t0(viewGroup2, viewGroup3)) {
            Rect rect2 = (Rect) kb4Var.a.get("android:changeBounds:bounds");
            Rect rect3 = (Rect) kb4Var2.a.get("android:changeBounds:bounds");
            int i4 = rect2.left;
            int i5 = rect3.left;
            int i6 = rect2.top;
            int i7 = rect3.top;
            int i8 = rect2.right;
            int i9 = rect3.right;
            int i10 = rect2.bottom;
            int i11 = rect3.bottom;
            int i12 = i8 - i4;
            int i13 = i10 - i6;
            int i14 = i9 - i5;
            int i15 = i11 - i7;
            Rect rect4 = (Rect) kb4Var.a.get("android:changeBounds:clip");
            Rect rect5 = (Rect) kb4Var2.a.get("android:changeBounds:clip");
            if ((i12 == 0 || i13 == 0) && (i14 == 0 || i15 == 0)) {
                i2 = 0;
            } else {
                i2 = (i4 == i5 && i6 == i7) ? 0 : 1;
                if (i8 != i9 || i10 != i11) {
                    i2++;
                }
            }
            if ((rect4 != null && !rect4.equals(rect5)) || (rect4 == null && rect5 != null)) {
                i2++;
            }
            if (i2 > 0) {
                if (!this.O0) {
                    view = view2;
                    nk4.g(view, i4, i6, i8, i10);
                    if (i2 == 2) {
                        if (i12 == i14 && i13 == i15) {
                            c2 = el2.a(view, W0, E().a(i4, i6, i5, i7));
                        } else {
                            k kVar = new k(view);
                            ObjectAnimator a2 = el2.a(kVar, S0, E().a(i4, i6, i5, i7));
                            ObjectAnimator a3 = el2.a(kVar, T0, E().a(i8, i10, i9, i11));
                            AnimatorSet animatorSet = new AnimatorSet();
                            animatorSet.playTogether(a2, a3);
                            animatorSet.addListener(new h(this, kVar));
                            c2 = animatorSet;
                        }
                    } else if (i4 == i5 && i6 == i7) {
                        c2 = el2.a(view, U0, E().a(i8, i10, i9, i11));
                    } else {
                        c2 = el2.a(view, V0, E().a(i4, i6, i5, i7));
                    }
                } else {
                    view = view2;
                    nk4.g(view, i4, i6, Math.max(i12, i14) + i4, Math.max(i13, i15) + i6);
                    ObjectAnimator a4 = (i4 == i5 && i6 == i7) ? null : el2.a(view, W0, E().a(i4, i6, i5, i7));
                    if (rect4 == null) {
                        i3 = 0;
                        rect = new Rect(0, 0, i12, i13);
                    } else {
                        i3 = 0;
                        rect = rect4;
                    }
                    Rect rect6 = rect5 == null ? new Rect(i3, i3, i14, i15) : rect5;
                    if (rect.equals(rect6)) {
                        objectAnimator = null;
                    } else {
                        ei4.z0(view, rect);
                        y43 y43Var = X0;
                        Object[] objArr = new Object[2];
                        objArr[i3] = rect;
                        objArr[1] = rect6;
                        ObjectAnimator ofObject = ObjectAnimator.ofObject(view, "clipBounds", y43Var, objArr);
                        ofObject.addListener(new i(this, view, rect5, i5, i7, i9, i11));
                        objectAnimator = ofObject;
                    }
                    c2 = androidx.transition.e.c(a4, objectAnimator);
                }
                if (view.getParent() instanceof ViewGroup) {
                    ViewGroup viewGroup4 = (ViewGroup) view.getParent();
                    qi4.d(viewGroup4, true);
                    a(new j(this, viewGroup4));
                }
                return c2;
            }
            return null;
        }
        int intValue = ((Integer) kb4Var.a.get("android:changeBounds:windowX")).intValue();
        int intValue2 = ((Integer) kb4Var.a.get("android:changeBounds:windowY")).intValue();
        int intValue3 = ((Integer) kb4Var2.a.get("android:changeBounds:windowX")).intValue();
        int intValue4 = ((Integer) kb4Var2.a.get("android:changeBounds:windowY")).intValue();
        if (intValue == intValue3 && intValue2 == intValue4) {
            return null;
        }
        viewGroup.getLocationInWindow(this.N0);
        Bitmap createBitmap = Bitmap.createBitmap(view2.getWidth(), view2.getHeight(), Bitmap.Config.ARGB_8888);
        view2.draw(new Canvas(createBitmap));
        BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
        float c3 = nk4.c(view2);
        nk4.h(view2, Utils.FLOAT_EPSILON);
        nk4.b(viewGroup).a(bitmapDrawable);
        PathMotion E = E();
        int[] iArr = this.N0;
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(bitmapDrawable, bw2.a(R0, E.a(intValue - iArr[0], intValue2 - iArr[1], intValue3 - iArr[0], intValue4 - iArr[1])));
        ofPropertyValuesHolder.addListener(new a(this, viewGroup, bitmapDrawable, view2, c3));
        return ofPropertyValuesHolder;
    }

    public final void s0(kb4 kb4Var) {
        View view = kb4Var.b;
        if (!ei4.W(view) && view.getWidth() == 0 && view.getHeight() == 0) {
            return;
        }
        kb4Var.a.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
        kb4Var.a.put("android:changeBounds:parent", kb4Var.b.getParent());
        if (this.P0) {
            kb4Var.b.getLocationInWindow(this.N0);
            kb4Var.a.put("android:changeBounds:windowX", Integer.valueOf(this.N0[0]));
            kb4Var.a.put("android:changeBounds:windowY", Integer.valueOf(this.N0[1]));
        }
        if (this.O0) {
            kb4Var.a.put("android:changeBounds:clip", ei4.w(view));
        }
    }

    public final boolean t0(View view, View view2) {
        if (this.P0) {
            kb4 C = C(view, true);
            if (C == null) {
                if (view == view2) {
                    return true;
                }
            } else if (view2 == C.b) {
                return true;
            }
            return false;
        }
        return true;
    }

    public void u0(boolean z) {
        this.O0 = z;
    }

    @SuppressLint({"RestrictedApi"})
    public ChangeBounds(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.N0 = new int[2];
        this.O0 = false;
        this.P0 = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hv3.b);
        boolean a2 = xd4.a(obtainStyledAttributes, (XmlResourceParser) attributeSet, "resizeClip", 0, false);
        obtainStyledAttributes.recycle();
        u0(a2);
    }
}
