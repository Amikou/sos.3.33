package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;
import org.xmlpull.v1.XmlPullParser;

/* loaded from: classes.dex */
public class ChangeTransform extends Transition {
    public static final String[] Q0 = {"android:changeTransform:matrix", "android:changeTransform:transforms", "android:changeTransform:parentMatrix"};
    public static final Property<e, float[]> R0 = new a(float[].class, "nonTranslations");
    public static final Property<e, PointF> S0 = new b(PointF.class, "translations");
    public static final boolean T0;
    public boolean N0;
    public boolean O0;
    public Matrix P0;

    /* loaded from: classes.dex */
    public static class a extends Property<e, float[]> {
        public a(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public float[] get(e eVar) {
            return null;
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(e eVar, float[] fArr) {
            eVar.d(fArr);
        }
    }

    /* loaded from: classes.dex */
    public static class b extends Property<e, PointF> {
        public b(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public PointF get(e eVar) {
            return null;
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(e eVar, PointF pointF) {
            eVar.c(pointF);
        }
    }

    /* loaded from: classes.dex */
    public class c extends AnimatorListenerAdapter {
        public boolean a;
        public Matrix f0 = new Matrix();
        public final /* synthetic */ boolean g0;
        public final /* synthetic */ Matrix h0;
        public final /* synthetic */ View i0;
        public final /* synthetic */ f j0;
        public final /* synthetic */ e k0;

        public c(boolean z, Matrix matrix, View view, f fVar, e eVar) {
            this.g0 = z;
            this.h0 = matrix;
            this.i0 = view;
            this.j0 = fVar;
            this.k0 = eVar;
        }

        public final void a(Matrix matrix) {
            this.f0.set(matrix);
            this.i0.setTag(zz2.transition_transform, this.f0);
            this.j0.a(this.i0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (!this.a) {
                if (this.g0 && ChangeTransform.this.N0) {
                    a(this.h0);
                } else {
                    this.i0.setTag(zz2.transition_transform, null);
                    this.i0.setTag(zz2.parent_matrix, null);
                }
            }
            nk4.f(this.i0, null);
            this.j0.a(this.i0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener
        public void onAnimationPause(Animator animator) {
            a(this.k0.a());
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener
        public void onAnimationResume(Animator animator) {
            ChangeTransform.w0(this.i0);
        }
    }

    /* loaded from: classes.dex */
    public static class d extends androidx.transition.c {
        public View a;
        public nf1 f0;

        public d(View view, nf1 nf1Var) {
            this.a = view;
            this.f0 = nf1Var;
        }

        @Override // androidx.transition.Transition.f
        public void onTransitionEnd(Transition transition) {
            transition.b0(this);
            rf1.b(this.a);
            this.a.setTag(zz2.transition_transform, null);
            this.a.setTag(zz2.parent_matrix, null);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionPause(Transition transition) {
            this.f0.setVisibility(4);
        }

        @Override // androidx.transition.c, androidx.transition.Transition.f
        public void onTransitionResume(Transition transition) {
            this.f0.setVisibility(0);
        }
    }

    /* loaded from: classes.dex */
    public static class e {
        public final Matrix a = new Matrix();
        public final View b;
        public final float[] c;
        public float d;
        public float e;

        public e(View view, float[] fArr) {
            this.b = view;
            float[] fArr2 = (float[]) fArr.clone();
            this.c = fArr2;
            this.d = fArr2[2];
            this.e = fArr2[5];
            b();
        }

        public Matrix a() {
            return this.a;
        }

        public final void b() {
            float[] fArr = this.c;
            fArr[2] = this.d;
            fArr[5] = this.e;
            this.a.setValues(fArr);
            nk4.f(this.b, this.a);
        }

        public void c(PointF pointF) {
            this.d = pointF.x;
            this.e = pointF.y;
            b();
        }

        public void d(float[] fArr) {
            System.arraycopy(fArr, 0, this.c, 0, fArr.length);
            b();
        }
    }

    /* loaded from: classes.dex */
    public static class f {
        public final float a;
        public final float b;
        public final float c;
        public final float d;
        public final float e;
        public final float f;
        public final float g;
        public final float h;

        public f(View view) {
            this.a = view.getTranslationX();
            this.b = view.getTranslationY();
            this.c = ei4.O(view);
            this.d = view.getScaleX();
            this.e = view.getScaleY();
            this.f = view.getRotationX();
            this.g = view.getRotationY();
            this.h = view.getRotation();
        }

        public void a(View view) {
            ChangeTransform.z0(view, this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
        }

        public boolean equals(Object obj) {
            if (obj instanceof f) {
                f fVar = (f) obj;
                return fVar.a == this.a && fVar.b == this.b && fVar.c == this.c && fVar.d == this.d && fVar.e == this.e && fVar.f == this.f && fVar.g == this.g && fVar.h == this.h;
            }
            return false;
        }

        public int hashCode() {
            float f = this.a;
            int floatToIntBits = (f != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f) : 0) * 31;
            float f2 = this.b;
            int floatToIntBits2 = (floatToIntBits + (f2 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f2) : 0)) * 31;
            float f3 = this.c;
            int floatToIntBits3 = (floatToIntBits2 + (f3 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f3) : 0)) * 31;
            float f4 = this.d;
            int floatToIntBits4 = (floatToIntBits3 + (f4 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f4) : 0)) * 31;
            float f5 = this.e;
            int floatToIntBits5 = (floatToIntBits4 + (f5 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f5) : 0)) * 31;
            float f6 = this.f;
            int floatToIntBits6 = (floatToIntBits5 + (f6 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f6) : 0)) * 31;
            float f7 = this.g;
            int floatToIntBits7 = (floatToIntBits6 + (f7 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f7) : 0)) * 31;
            float f8 = this.h;
            return floatToIntBits7 + (f8 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f8) : 0);
        }
    }

    static {
        T0 = Build.VERSION.SDK_INT >= 21;
    }

    public ChangeTransform() {
        this.N0 = true;
        this.O0 = true;
        this.P0 = new Matrix();
    }

    public static void w0(View view) {
        z0(view, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 1.0f, 1.0f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
    }

    public static void z0(View view, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        view.setTranslationX(f2);
        view.setTranslationY(f3);
        ei4.M0(view, f4);
        view.setScaleX(f5);
        view.setScaleY(f6);
        view.setRotationX(f7);
        view.setRotationY(f8);
        view.setRotation(f9);
    }

    @Override // androidx.transition.Transition
    public String[] N() {
        return Q0;
    }

    @Override // androidx.transition.Transition
    public void h(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public void l(kb4 kb4Var) {
        s0(kb4Var);
        if (T0) {
            return;
        }
        ((ViewGroup) kb4Var.b.getParent()).startViewTransition(kb4Var.b);
    }

    @Override // androidx.transition.Transition
    public Animator s(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        if (kb4Var == null || kb4Var2 == null || !kb4Var.a.containsKey("android:changeTransform:parent") || !kb4Var2.a.containsKey("android:changeTransform:parent")) {
            return null;
        }
        ViewGroup viewGroup2 = (ViewGroup) kb4Var.a.get("android:changeTransform:parent");
        boolean z = this.O0 && !v0(viewGroup2, (ViewGroup) kb4Var2.a.get("android:changeTransform:parent"));
        Matrix matrix = (Matrix) kb4Var.a.get("android:changeTransform:intermediateMatrix");
        if (matrix != null) {
            kb4Var.a.put("android:changeTransform:matrix", matrix);
        }
        Matrix matrix2 = (Matrix) kb4Var.a.get("android:changeTransform:intermediateParentMatrix");
        if (matrix2 != null) {
            kb4Var.a.put("android:changeTransform:parentMatrix", matrix2);
        }
        if (z) {
            x0(kb4Var, kb4Var2);
        }
        ObjectAnimator u0 = u0(kb4Var, kb4Var2, z);
        if (z && u0 != null && this.N0) {
            t0(viewGroup, kb4Var, kb4Var2);
        } else if (!T0) {
            viewGroup2.endViewTransition(kb4Var.b);
        }
        return u0;
    }

    public final void s0(kb4 kb4Var) {
        View view = kb4Var.b;
        if (view.getVisibility() == 8) {
            return;
        }
        kb4Var.a.put("android:changeTransform:parent", view.getParent());
        kb4Var.a.put("android:changeTransform:transforms", new f(view));
        Matrix matrix = view.getMatrix();
        kb4Var.a.put("android:changeTransform:matrix", (matrix == null || matrix.isIdentity()) ? null : new Matrix(matrix));
        if (this.O0) {
            Matrix matrix2 = new Matrix();
            ViewGroup viewGroup = (ViewGroup) view.getParent();
            nk4.j(viewGroup, matrix2);
            matrix2.preTranslate(-viewGroup.getScrollX(), -viewGroup.getScrollY());
            kb4Var.a.put("android:changeTransform:parentMatrix", matrix2);
            kb4Var.a.put("android:changeTransform:intermediateMatrix", view.getTag(zz2.transition_transform));
            kb4Var.a.put("android:changeTransform:intermediateParentMatrix", view.getTag(zz2.parent_matrix));
        }
    }

    public final void t0(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        View view = kb4Var2.b;
        Matrix matrix = new Matrix((Matrix) kb4Var2.a.get("android:changeTransform:parentMatrix"));
        nk4.k(viewGroup, matrix);
        nf1 a2 = rf1.a(view, viewGroup, matrix);
        if (a2 == null) {
            return;
        }
        a2.a((ViewGroup) kb4Var.a.get("android:changeTransform:parent"), kb4Var.b);
        Transition transition = this;
        while (true) {
            Transition transition2 = transition.v0;
            if (transition2 == null) {
                break;
            }
            transition = transition2;
        }
        transition.a(new d(view, a2));
        if (T0) {
            View view2 = kb4Var.b;
            if (view2 != kb4Var2.b) {
                nk4.h(view2, Utils.FLOAT_EPSILON);
            }
            nk4.h(view, 1.0f);
        }
    }

    public final ObjectAnimator u0(kb4 kb4Var, kb4 kb4Var2, boolean z) {
        Matrix matrix = (Matrix) kb4Var.a.get("android:changeTransform:matrix");
        Matrix matrix2 = (Matrix) kb4Var2.a.get("android:changeTransform:matrix");
        if (matrix == null) {
            matrix = b52.a;
        }
        if (matrix2 == null) {
            matrix2 = b52.a;
        }
        Matrix matrix3 = matrix2;
        if (matrix.equals(matrix3)) {
            return null;
        }
        f fVar = (f) kb4Var2.a.get("android:changeTransform:transforms");
        View view = kb4Var2.b;
        w0(view);
        float[] fArr = new float[9];
        matrix.getValues(fArr);
        float[] fArr2 = new float[9];
        matrix3.getValues(fArr2);
        e eVar = new e(view, fArr);
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(eVar, PropertyValuesHolder.ofObject(R0, new b71(new float[9]), fArr, fArr2), bw2.a(S0, E().a(fArr[2], fArr[5], fArr2[2], fArr2[5])));
        c cVar = new c(z, matrix3, view, fVar, eVar);
        ofPropertyValuesHolder.addListener(cVar);
        androidx.transition.a.a(ofPropertyValuesHolder, cVar);
        return ofPropertyValuesHolder;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0017, code lost:
        if (r5 == r4.b) goto L10;
     */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x001a, code lost:
        if (r4 == r5) goto L10;
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x001d, code lost:
        r1 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x001f, code lost:
        return r1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean v0(android.view.ViewGroup r4, android.view.ViewGroup r5) {
        /*
            r3 = this;
            boolean r0 = r3.R(r4)
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L1a
            boolean r0 = r3.R(r5)
            if (r0 != 0) goto Lf
            goto L1a
        Lf:
            kb4 r4 = r3.C(r4, r1)
            if (r4 == 0) goto L1f
            android.view.View r4 = r4.b
            if (r5 != r4) goto L1d
            goto L1e
        L1a:
            if (r4 != r5) goto L1d
            goto L1e
        L1d:
            r1 = r2
        L1e:
            r2 = r1
        L1f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.transition.ChangeTransform.v0(android.view.ViewGroup, android.view.ViewGroup):boolean");
    }

    public final void x0(kb4 kb4Var, kb4 kb4Var2) {
        Matrix matrix = (Matrix) kb4Var2.a.get("android:changeTransform:parentMatrix");
        kb4Var2.b.setTag(zz2.parent_matrix, matrix);
        Matrix matrix2 = this.P0;
        matrix2.reset();
        matrix.invert(matrix2);
        Matrix matrix3 = (Matrix) kb4Var.a.get("android:changeTransform:matrix");
        if (matrix3 == null) {
            matrix3 = new Matrix();
            kb4Var.a.put("android:changeTransform:matrix", matrix3);
        }
        matrix3.postConcat((Matrix) kb4Var.a.get("android:changeTransform:parentMatrix"));
        matrix3.postConcat(matrix2);
    }

    @SuppressLint({"RestrictedApi"})
    public ChangeTransform(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.N0 = true;
        this.O0 = true;
        this.P0 = new Matrix();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, hv3.e);
        XmlPullParser xmlPullParser = (XmlPullParser) attributeSet;
        this.N0 = xd4.a(obtainStyledAttributes, xmlPullParser, "reparentWithOverlay", 1, true);
        this.O0 = xd4.a(obtainStyledAttributes, xmlPullParser, "reparent", 0, true);
        obtainStyledAttributes.recycle();
    }
}
