package org.bouncycastle.jcajce.provider.asymmetric.ec;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPrivateKeySpec;
import java.util.Enumeration;
import org.bouncycastle.asn1.a0;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.i;
import org.bouncycastle.asn1.k;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/* loaded from: classes2.dex */
public class BCECPrivateKey implements ECPrivateKey, PrivateKey {
    public static final long serialVersionUID = 994553197664784084L;
    public transient BigInteger a;
    private String algorithm;
    public transient ECParameterSpec f0;
    public transient gw2 g0;
    public transient a0 h0;
    public transient ko2 i0;
    private boolean withCompression;

    public BCECPrivateKey() {
        this.algorithm = "EC";
        this.i0 = new ko2();
    }

    public BCECPrivateKey(String str, ECPrivateKeySpec eCPrivateKeySpec, gw2 gw2Var) {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.algorithm = str;
        this.a = eCPrivateKeySpec.getS();
        this.f0 = eCPrivateKeySpec.getParams();
        this.g0 = gw2Var;
    }

    public BCECPrivateKey(String str, BCECPrivateKey bCECPrivateKey) {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.algorithm = str;
        this.a = bCECPrivateKey.a;
        this.f0 = bCECPrivateKey.f0;
        this.withCompression = bCECPrivateKey.withCompression;
        this.i0 = bCECPrivateKey.i0;
        this.h0 = bCECPrivateKey.h0;
        this.g0 = bCECPrivateKey.g0;
    }

    public BCECPrivateKey(String str, st0 st0Var, gw2 gw2Var) {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.algorithm = str;
        this.a = st0Var.b();
        this.f0 = null;
        this.g0 = gw2Var;
    }

    public BCECPrivateKey(String str, st0 st0Var, BCECPublicKey bCECPublicKey, ECParameterSpec eCParameterSpec, gw2 gw2Var) {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.algorithm = str;
        this.a = st0Var.b();
        this.g0 = gw2Var;
        if (eCParameterSpec == null) {
            at0 a = st0Var.a();
            eCParameterSpec = new ECParameterSpec(us0.b(a.a(), a.e()), us0.f(a.b()), a.d(), a.c().intValue());
        }
        this.f0 = eCParameterSpec;
        this.h0 = a(bCECPublicKey);
    }

    public BCECPrivateKey(String str, st0 st0Var, BCECPublicKey bCECPublicKey, ot0 ot0Var, gw2 gw2Var) {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.algorithm = str;
        this.a = st0Var.b();
        this.g0 = gw2Var;
        if (ot0Var == null) {
            at0 a = st0Var.a();
            this.f0 = new ECParameterSpec(us0.b(a.a(), a.e()), us0.f(a.b()), a.d(), a.c().intValue());
        } else {
            this.f0 = us0.h(us0.b(ot0Var.a(), ot0Var.e()), ot0Var);
        }
        try {
            this.h0 = a(bCECPublicKey);
        } catch (Exception unused) {
            this.h0 = null;
        }
    }

    public BCECPrivateKey(String str, tt0 tt0Var, gw2 gw2Var) {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.algorithm = str;
        throw null;
    }

    public BCECPrivateKey(String str, zu2 zu2Var, gw2 gw2Var) throws IOException {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.algorithm = str;
        this.g0 = gw2Var;
        b(zu2Var);
    }

    public BCECPrivateKey(ECPrivateKey eCPrivateKey, gw2 gw2Var) {
        this.algorithm = "EC";
        this.i0 = new ko2();
        this.a = eCPrivateKey.getS();
        this.algorithm = eCPrivateKey.getAlgorithm();
        this.f0 = eCPrivateKey.getParams();
        this.g0 = gw2Var;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.g0 = BouncyCastleProvider.CONFIGURATION;
        b(zu2.o(k.s((byte[]) objectInputStream.readObject())));
        this.i0 = new ko2();
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(getEncoded());
    }

    public final a0 a(BCECPublicKey bCECPublicKey) {
        try {
            return jv3.p(k.s(bCECPublicKey.getEncoded())).q();
        } catch (IOException unused) {
            return null;
        }
    }

    public final void b(zu2 zu2Var) throws IOException {
        nr4 o = nr4.o(zu2Var.p().q());
        this.f0 = us0.i(o, us0.j(this.g0, o));
        c4 q = zu2Var.q();
        if (q instanceof g) {
            this.a = g.z(q).B();
            return;
        }
        rt0 o2 = rt0.o(q);
        this.a = o2.p();
        this.h0 = o2.s();
    }

    public ot0 engineGetSpec() {
        ECParameterSpec eCParameterSpec = this.f0;
        return eCParameterSpec != null ? us0.g(eCParameterSpec, this.withCompression) : this.g0.b();
    }

    public boolean equals(Object obj) {
        if (obj instanceof BCECPrivateKey) {
            BCECPrivateKey bCECPrivateKey = (BCECPrivateKey) obj;
            return getD().equals(bCECPrivateKey.getD()) && engineGetSpec().equals(bCECPrivateKey.engineGetSpec());
        }
        return false;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return this.algorithm;
    }

    public c4 getBagAttribute(i iVar) {
        return this.i0.a(iVar);
    }

    public Enumeration getBagAttributeKeys() {
        return this.i0.b();
    }

    public BigInteger getD() {
        return this.a;
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        nr4 a = xt0.a(this.f0, this.withCompression);
        ECParameterSpec eCParameterSpec = this.f0;
        int i = eCParameterSpec == null ? wt0.i(this.g0, null, getS()) : wt0.i(this.g0, eCParameterSpec.getOrder(), getS());
        try {
            return new zu2(new va(vr4.o, a), this.h0 != null ? new rt0(i, getS(), this.h0, a) : new rt0(i, getS(), a)).n("DER");
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "PKCS#8";
    }

    public ot0 getParameters() {
        ECParameterSpec eCParameterSpec = this.f0;
        if (eCParameterSpec == null) {
            return null;
        }
        return us0.g(eCParameterSpec, this.withCompression);
    }

    @Override // java.security.interfaces.ECKey
    public ECParameterSpec getParams() {
        return this.f0;
    }

    @Override // java.security.interfaces.ECPrivateKey
    public BigInteger getS() {
        return this.a;
    }

    public int hashCode() {
        return getD().hashCode() ^ engineGetSpec().hashCode();
    }

    public void setBagAttribute(i iVar, c4 c4Var) {
        this.i0.c(iVar, c4Var);
    }

    public void setPointFormat(String str) {
        this.withCompression = !"UNCOMPRESSED".equalsIgnoreCase(str);
    }

    public String toString() {
        return wt0.j("EC", this.a, engineGetSpec());
    }
}
