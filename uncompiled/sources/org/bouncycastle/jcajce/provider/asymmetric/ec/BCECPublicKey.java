package org.bouncycastle.jcajce.provider.asymmetric.ec;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.EllipticCurve;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/* loaded from: classes2.dex */
public class BCECPublicKey implements ECPublicKey, PublicKey {
    public static final long serialVersionUID = 2422789860422731812L;
    public transient ut0 a;
    private String algorithm;
    public transient ECParameterSpec f0;
    public transient gw2 g0;
    private boolean withCompression;

    public BCECPublicKey(String str, ECPublicKeySpec eCPublicKeySpec, gw2 gw2Var) {
        this.algorithm = "EC";
        this.algorithm = str;
        ECParameterSpec params = eCPublicKeySpec.getParams();
        this.f0 = params;
        this.a = new ut0(us0.e(params, eCPublicKeySpec.getW(), false), us0.k(gw2Var, eCPublicKeySpec.getParams()));
        this.g0 = gw2Var;
    }

    public BCECPublicKey(String str, jv3 jv3Var, gw2 gw2Var) {
        this.algorithm = "EC";
        this.algorithm = str;
        this.g0 = gw2Var;
        b(jv3Var);
    }

    public BCECPublicKey(String str, BCECPublicKey bCECPublicKey) {
        this.algorithm = "EC";
        this.algorithm = str;
        this.a = bCECPublicKey.a;
        this.f0 = bCECPublicKey.f0;
        this.withCompression = bCECPublicKey.withCompression;
        this.g0 = bCECPublicKey.g0;
    }

    public BCECPublicKey(String str, ut0 ut0Var, gw2 gw2Var) {
        this.algorithm = "EC";
        this.algorithm = str;
        this.a = ut0Var;
        this.f0 = null;
        this.g0 = gw2Var;
    }

    public BCECPublicKey(String str, ut0 ut0Var, ECParameterSpec eCParameterSpec, gw2 gw2Var) {
        this.algorithm = "EC";
        at0 a = ut0Var.a();
        this.algorithm = str;
        this.a = ut0Var;
        if (eCParameterSpec == null) {
            this.f0 = a(us0.b(a.a(), a.e()), a);
        } else {
            this.f0 = eCParameterSpec;
        }
        this.g0 = gw2Var;
    }

    public BCECPublicKey(String str, ut0 ut0Var, ot0 ot0Var, gw2 gw2Var) {
        this.algorithm = "EC";
        at0 a = ut0Var.a();
        this.algorithm = str;
        this.f0 = ot0Var == null ? a(us0.b(a.a(), a.e()), a) : us0.h(us0.b(ot0Var.a(), ot0Var.e()), ot0Var);
        this.a = ut0Var;
        this.g0 = gw2Var;
    }

    public BCECPublicKey(String str, vt0 vt0Var, gw2 gw2Var) {
        this.algorithm = "EC";
        this.algorithm = str;
        throw null;
    }

    public BCECPublicKey(ECPublicKey eCPublicKey, gw2 gw2Var) {
        this.algorithm = "EC";
        this.algorithm = eCPublicKey.getAlgorithm();
        ECParameterSpec params = eCPublicKey.getParams();
        this.f0 = params;
        this.a = new ut0(us0.e(params, eCPublicKey.getW(), false), us0.k(gw2Var, eCPublicKey.getParams()));
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.g0 = BouncyCastleProvider.CONFIGURATION;
        b(jv3.p(k.s((byte[]) objectInputStream.readObject())));
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(getEncoded());
    }

    public final ECParameterSpec a(EllipticCurve ellipticCurve, at0 at0Var) {
        return new ECParameterSpec(ellipticCurve, us0.f(at0Var.b()), at0Var.d(), at0Var.c().intValue());
    }

    public final void b(jv3 jv3Var) {
        nr4 o = nr4.o(jv3Var.o().q());
        xs0 j = us0.j(this.g0, o);
        this.f0 = us0.i(o, j);
        byte[] D = jv3Var.q().D();
        f4 j0Var = new j0(D);
        if (D[0] == 4 && D[1] == D.length - 2 && ((D[2] == 2 || D[2] == 3) && new ur4().a(j) >= D.length - 3)) {
            try {
                j0Var = (f4) k.s(D);
            } catch (IOException unused) {
                throw new IllegalArgumentException("error recovering public key");
            }
        }
        this.a = new ut0(new rr4(j, j0Var).o(), wt0.f(this.g0, o));
    }

    public ut0 engineGetKeyParameters() {
        return this.a;
    }

    public ot0 engineGetSpec() {
        ECParameterSpec eCParameterSpec = this.f0;
        return eCParameterSpec != null ? us0.g(eCParameterSpec, this.withCompression) : this.g0.b();
    }

    public boolean equals(Object obj) {
        if (obj instanceof BCECPublicKey) {
            BCECPublicKey bCECPublicKey = (BCECPublicKey) obj;
            return this.a.b().e(bCECPublicKey.a.b()) && engineGetSpec().equals(bCECPublicKey.engineGetSpec());
        }
        return false;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return this.algorithm;
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        return ox1.a(new jv3(new va(vr4.o, xt0.a(this.f0, this.withCompression)), f4.B(new rr4(this.a.b(), this.withCompression).i()).D()));
    }

    @Override // java.security.Key
    public String getFormat() {
        return "X.509";
    }

    public ot0 getParameters() {
        ECParameterSpec eCParameterSpec = this.f0;
        if (eCParameterSpec == null) {
            return null;
        }
        return us0.g(eCParameterSpec, this.withCompression);
    }

    @Override // java.security.interfaces.ECKey
    public ECParameterSpec getParams() {
        return this.f0;
    }

    public pt0 getQ() {
        pt0 b = this.a.b();
        return this.f0 == null ? b.k() : b;
    }

    @Override // java.security.interfaces.ECPublicKey
    public ECPoint getW() {
        return us0.f(this.a.b());
    }

    public int hashCode() {
        return this.a.b().hashCode() ^ engineGetSpec().hashCode();
    }

    public void setPointFormat(String str) {
        this.withCompression = !"UNCOMPRESSED".equalsIgnoreCase(str);
    }

    public String toString() {
        return wt0.k("EC", this.a.b(), engineGetSpec());
    }
}
