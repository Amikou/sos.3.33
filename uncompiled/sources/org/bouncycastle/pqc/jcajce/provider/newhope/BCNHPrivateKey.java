package org.bouncycastle.pqc.jcajce.provider.newhope;

import java.io.IOException;
import java.security.Key;
import java.security.PrivateKey;
import org.bouncycastle.asn1.j0;

/* loaded from: classes2.dex */
public class BCNHPrivateKey implements Key, PrivateKey {
    private static final long serialVersionUID = 1;
    private final qc2 params;

    public BCNHPrivateKey(qc2 qc2Var) {
        this.params = qc2Var;
    }

    public BCNHPrivateKey(zu2 zu2Var) throws IOException {
        this.params = new qc2(a(f4.B(zu2Var.q()).D()));
    }

    public static short[] a(byte[] bArr) {
        int length = bArr.length / 2;
        short[] sArr = new short[length];
        for (int i = 0; i != length; i++) {
            sArr[i] = ro2.j(bArr, i * 2);
        }
        return sArr;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BCNHPrivateKey)) {
            return false;
        }
        return wh.d(this.params.a(), ((BCNHPrivateKey) obj).params.a());
    }

    @Override // java.security.Key
    public final String getAlgorithm() {
        return "NH";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            va vaVar = new va(po2.f);
            short[] a = this.params.a();
            byte[] bArr = new byte[a.length * 2];
            for (int i = 0; i != a.length; i++) {
                ro2.o(a[i], bArr, i * 2);
            }
            return new zu2(vaVar, new j0(bArr)).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "PKCS#8";
    }

    public ty getKeyParams() {
        return this.params;
    }

    public short[] getSecretData() {
        return this.params.a();
    }

    public int hashCode() {
        return wh.w(this.params.a());
    }
}
