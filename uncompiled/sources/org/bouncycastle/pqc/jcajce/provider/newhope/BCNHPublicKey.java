package org.bouncycastle.pqc.jcajce.provider.newhope;

import java.io.IOException;
import java.security.Key;
import java.security.PublicKey;

/* loaded from: classes2.dex */
public class BCNHPublicKey implements Key, PublicKey {
    private static final long serialVersionUID = 1;
    private final rc2 params;

    public BCNHPublicKey(jv3 jv3Var) {
        this.params = new rc2(jv3Var.q().D());
    }

    public BCNHPublicKey(rc2 rc2Var) {
        this.params = rc2Var;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BCNHPublicKey)) {
            return false;
        }
        return wh.a(this.params.a(), ((BCNHPublicKey) obj).params.a());
    }

    @Override // java.security.Key
    public final String getAlgorithm() {
        return "NH";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new jv3(new va(po2.f), this.params.a()).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "X.509";
    }

    public ty getKeyParams() {
        return this.params;
    }

    public byte[] getPublicData() {
        return this.params.a();
    }

    public int hashCode() {
        return wh.r(this.params.a());
    }
}
