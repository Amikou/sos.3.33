package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import java.security.PrivateKey;

/* loaded from: classes2.dex */
public class BCMcElieceCCA2PrivateKey implements PrivateKey {
    private static final long serialVersionUID = 1;
    private h52 params;

    public BCMcElieceCCA2PrivateKey(h52 h52Var) {
        this.params = h52Var;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BCMcElieceCCA2PrivateKey)) {
            return false;
        }
        BCMcElieceCCA2PrivateKey bCMcElieceCCA2PrivateKey = (BCMcElieceCCA2PrivateKey) obj;
        return getN() == bCMcElieceCCA2PrivateKey.getN() && getK() == bCMcElieceCCA2PrivateKey.getK() && getField().equals(bCMcElieceCCA2PrivateKey.getField()) && getGoppaPoly().equals(bCMcElieceCCA2PrivateKey.getGoppaPoly()) && getP().equals(bCMcElieceCCA2PrivateKey.getP()) && getH().equals(bCMcElieceCCA2PrivateKey.getH());
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return "McEliece-CCA2";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new zu2(new va(po2.d), new g52(getN(), getK(), getField(), getGoppaPoly(), getP(), sg4.a(this.params.a()))).m();
        } catch (IOException unused) {
            return null;
        }
    }

    public je1 getField() {
        return this.params.b();
    }

    @Override // java.security.Key
    public String getFormat() {
        return "PKCS#8";
    }

    public rs2 getGoppaPoly() {
        return this.params.c();
    }

    public he1 getH() {
        return this.params.d();
    }

    public int getK() {
        return this.params.e();
    }

    public pi getKeyParams() {
        return this.params;
    }

    public int getN() {
        return this.params.f();
    }

    public mq2 getP() {
        return this.params.g();
    }

    public rs2[] getQInv() {
        return this.params.h();
    }

    public int getT() {
        return this.params.c().g();
    }

    public int hashCode() {
        return (((((((((this.params.e() * 37) + this.params.f()) * 37) + this.params.b().hashCode()) * 37) + this.params.c().hashCode()) * 37) + this.params.g().hashCode()) * 37) + this.params.d().hashCode();
    }
}
