package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactorySpi;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.asn1.k;

/* loaded from: classes2.dex */
public class a extends KeyFactorySpi implements oi {
    @Override // defpackage.oi
    public PublicKey a(jv3 jv3Var) throws IOException {
        i52 q = i52.q(jv3Var.s());
        return new BCMcElieceCCA2PublicKey(new j52(q.s(), q.t(), q.p(), sg4.b(q.o()).g()));
    }

    @Override // defpackage.oi
    public PrivateKey b(zu2 zu2Var) throws IOException {
        g52 s = g52.s(zu2Var.q().i());
        return new BCMcElieceCCA2PrivateKey(new h52(s.w(), s.t(), s.p(), s.q(), s.y(), null));
    }

    @Override // java.security.KeyFactorySpi
    public PrivateKey engineGeneratePrivate(KeySpec keySpec) throws InvalidKeySpecException {
        if (!(keySpec instanceof PKCS8EncodedKeySpec)) {
            throw new InvalidKeySpecException("Unsupported key specification: " + keySpec.getClass() + ".");
        }
        try {
            zu2 o = zu2.o(k.s(((PKCS8EncodedKeySpec) keySpec).getEncoded()));
            try {
                if (po2.d.equals(o.p().o())) {
                    g52 s = g52.s(o.q());
                    return new BCMcElieceCCA2PrivateKey(new h52(s.w(), s.t(), s.p(), s.q(), s.y(), sg4.b(s.o()).g()));
                }
                throw new InvalidKeySpecException("Unable to recognise OID in McEliece public key");
            } catch (IOException unused) {
                throw new InvalidKeySpecException("Unable to decode PKCS8EncodedKeySpec.");
            }
        } catch (IOException e) {
            throw new InvalidKeySpecException("Unable to decode PKCS8EncodedKeySpec: " + e);
        }
    }

    @Override // java.security.KeyFactorySpi
    public PublicKey engineGeneratePublic(KeySpec keySpec) throws InvalidKeySpecException {
        if (!(keySpec instanceof X509EncodedKeySpec)) {
            throw new InvalidKeySpecException("Unsupported key specification: " + keySpec.getClass() + ".");
        }
        try {
            jv3 p = jv3.p(k.s(((X509EncodedKeySpec) keySpec).getEncoded()));
            try {
                if (po2.d.equals(p.o().o())) {
                    i52 q = i52.q(p.s());
                    return new BCMcElieceCCA2PublicKey(new j52(q.s(), q.t(), q.p(), sg4.b(q.o()).g()));
                }
                throw new InvalidKeySpecException("Unable to recognise OID in McEliece private key");
            } catch (IOException e) {
                throw new InvalidKeySpecException("Unable to decode X509EncodedKeySpec: " + e.getMessage());
            }
        } catch (IOException e2) {
            throw new InvalidKeySpecException(e2.toString());
        }
    }

    @Override // java.security.KeyFactorySpi
    public KeySpec engineGetKeySpec(Key key, Class cls) throws InvalidKeySpecException {
        return null;
    }

    @Override // java.security.KeyFactorySpi
    public Key engineTranslateKey(Key key) throws InvalidKeyException {
        return null;
    }
}
