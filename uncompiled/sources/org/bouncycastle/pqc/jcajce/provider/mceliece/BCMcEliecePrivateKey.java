package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import java.security.PrivateKey;

/* loaded from: classes2.dex */
public class BCMcEliecePrivateKey implements ty, PrivateKey {
    private static final long serialVersionUID = 1;
    private n52 params;

    public BCMcEliecePrivateKey(n52 n52Var) {
        this.params = n52Var;
    }

    public boolean equals(Object obj) {
        if (obj instanceof BCMcEliecePrivateKey) {
            BCMcEliecePrivateKey bCMcEliecePrivateKey = (BCMcEliecePrivateKey) obj;
            return getN() == bCMcEliecePrivateKey.getN() && getK() == bCMcEliecePrivateKey.getK() && getField().equals(bCMcEliecePrivateKey.getField()) && getGoppaPoly().equals(bCMcEliecePrivateKey.getGoppaPoly()) && getSInv().equals(bCMcEliecePrivateKey.getSInv()) && getP1().equals(bCMcEliecePrivateKey.getP1()) && getP2().equals(bCMcEliecePrivateKey.getP2());
        }
        return false;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return "McEliece";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new zu2(new va(po2.c), new m52(this.params.e(), this.params.d(), this.params.a(), this.params.b(), this.params.f(), this.params.g(), this.params.i())).m();
        } catch (IOException unused) {
            return null;
        }
    }

    public je1 getField() {
        return this.params.a();
    }

    @Override // java.security.Key
    public String getFormat() {
        return "PKCS#8";
    }

    public rs2 getGoppaPoly() {
        return this.params.b();
    }

    public he1 getH() {
        return this.params.c();
    }

    public int getK() {
        return this.params.d();
    }

    public pi getKeyParams() {
        return this.params;
    }

    public int getN() {
        return this.params.e();
    }

    public mq2 getP1() {
        return this.params.f();
    }

    public mq2 getP2() {
        return this.params.g();
    }

    public rs2[] getQInv() {
        return this.params.h();
    }

    public he1 getSInv() {
        return this.params.i();
    }

    public int hashCode() {
        return (((((((((((this.params.d() * 37) + this.params.e()) * 37) + this.params.a().hashCode()) * 37) + this.params.b().hashCode()) * 37) + this.params.f().hashCode()) * 37) + this.params.g().hashCode()) * 37) + this.params.i().hashCode();
    }
}
