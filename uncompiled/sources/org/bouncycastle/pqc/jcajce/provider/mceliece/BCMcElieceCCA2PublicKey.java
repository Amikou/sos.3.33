package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import java.security.PublicKey;

/* loaded from: classes2.dex */
public class BCMcElieceCCA2PublicKey implements ty, PublicKey {
    private static final long serialVersionUID = 1;
    private j52 params;

    public BCMcElieceCCA2PublicKey(j52 j52Var) {
        this.params = j52Var;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BCMcElieceCCA2PublicKey)) {
            return false;
        }
        BCMcElieceCCA2PublicKey bCMcElieceCCA2PublicKey = (BCMcElieceCCA2PublicKey) obj;
        return this.params.d() == bCMcElieceCCA2PublicKey.getN() && this.params.e() == bCMcElieceCCA2PublicKey.getT() && this.params.b().equals(bCMcElieceCCA2PublicKey.getG());
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return "McEliece-CCA2";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new jv3(new va(po2.d), new i52(this.params.d(), this.params.e(), this.params.b(), sg4.a(this.params.a()))).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "X.509";
    }

    public he1 getG() {
        return this.params.b();
    }

    public int getK() {
        return this.params.c();
    }

    public pi getKeyParams() {
        return this.params;
    }

    public int getN() {
        return this.params.d();
    }

    public int getT() {
        return this.params.e();
    }

    public int hashCode() {
        return ((this.params.d() + (this.params.e() * 37)) * 37) + this.params.b().hashCode();
    }

    public String toString() {
        return (("McEliecePublicKey:\n length of the code         : " + this.params.d() + "\n") + " error correction capability: " + this.params.e() + "\n") + " generator matrix           : " + this.params.b().toString();
    }
}
