package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import java.security.PublicKey;

/* loaded from: classes2.dex */
public class BCMcEliecePublicKey implements PublicKey {
    private static final long serialVersionUID = 1;
    private p52 params;

    public BCMcEliecePublicKey(p52 p52Var) {
        this.params = p52Var;
    }

    public boolean equals(Object obj) {
        if (obj instanceof BCMcEliecePublicKey) {
            BCMcEliecePublicKey bCMcEliecePublicKey = (BCMcEliecePublicKey) obj;
            return this.params.c() == bCMcEliecePublicKey.getN() && this.params.d() == bCMcEliecePublicKey.getT() && this.params.a().equals(bCMcEliecePublicKey.getG());
        }
        return false;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return "McEliece";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new jv3(new va(po2.c), new o52(this.params.c(), this.params.d(), this.params.a())).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "X.509";
    }

    public he1 getG() {
        return this.params.a();
    }

    public int getK() {
        return this.params.b();
    }

    public pi getKeyParams() {
        return this.params;
    }

    public int getN() {
        return this.params.c();
    }

    public int getT() {
        return this.params.d();
    }

    public int hashCode() {
        return ((this.params.c() + (this.params.d() * 37)) * 37) + this.params.a().hashCode();
    }

    public String toString() {
        return (("McEliecePublicKey:\n length of the code         : " + this.params.c() + "\n") + " error correction capability: " + this.params.d() + "\n") + " generator matrix           : " + this.params.a();
    }
}
