package org.bouncycastle.pqc.jcajce.provider;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.IOException;
import java.security.AccessController;
import java.security.PrivateKey;
import java.security.PrivilegedAction;
import java.security.Provider;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import org.bouncycastle.asn1.i;

/* loaded from: classes2.dex */
public class BouncyCastlePQCProvider extends Provider implements x40 {
    public static final gw2 CONFIGURATION = null;
    public static String PROVIDER_NAME = "BCPQC";
    public static String a = "BouncyCastle Post-Quantum Security Provider v1.60";
    public static final Map f0 = new HashMap();
    public static final String[] g0 = {"Rainbow", "McEliece", "SPHINCS", "NH", "XMSS"};

    /* loaded from: classes2.dex */
    public class a implements PrivilegedAction {
        public a() {
        }

        @Override // java.security.PrivilegedAction
        public Object run() {
            BouncyCastlePQCProvider.this.d();
            return null;
        }
    }

    /* loaded from: classes2.dex */
    public static class b implements PrivilegedAction {
        public final /* synthetic */ String a;

        public b(String str) {
            this.a = str;
        }

        @Override // java.security.PrivilegedAction
        public Object run() {
            try {
                return Class.forName(this.a);
            } catch (Exception unused) {
                return null;
            }
        }
    }

    public BouncyCastlePQCProvider() {
        super(PROVIDER_NAME, 1.6d, a);
        AccessController.doPrivileged(new a());
    }

    public static oi a(i iVar) {
        oi oiVar;
        Map map = f0;
        synchronized (map) {
            oiVar = (oi) map.get(iVar);
        }
        return oiVar;
    }

    public static PrivateKey getPrivateKey(zu2 zu2Var) throws IOException {
        oi a2 = a(zu2Var.p().o());
        if (a2 == null) {
            return null;
        }
        return a2.b(zu2Var);
    }

    public static PublicKey getPublicKey(jv3 jv3Var) throws IOException {
        oi a2 = a(jv3Var.o().o());
        if (a2 == null) {
            return null;
        }
        return a2.a(jv3Var);
    }

    public static Class loadClass(Class cls, String str) {
        try {
            ClassLoader classLoader = cls.getClassLoader();
            return classLoader != null ? classLoader.loadClass(str) : (Class) AccessController.doPrivileged(new b(str));
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    public void addAlgorithm(String str, String str2) {
        if (!containsKey(str)) {
            put(str, str2);
            return;
        }
        throw new IllegalStateException("duplicate provider key (" + str + ") found");
    }

    public void addAlgorithm(String str, i iVar, String str2) {
        if (!containsKey(str + "." + str2)) {
            throw new IllegalStateException("primary key (" + str + "." + str2 + ") not found");
        }
        addAlgorithm(str + "." + iVar, str2);
        addAlgorithm(str + ".OID." + iVar, str2);
    }

    public void addAttributes(String str, Map<String, String> map) {
        for (String str2 : map.keySet()) {
            String str3 = str + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + str2;
            if (containsKey(str3)) {
                throw new IllegalStateException("duplicate provider attribute key (" + str3 + ") found");
            }
            put(str3, map.get(str2));
        }
    }

    public void addKeyInfoConverter(i iVar, oi oiVar) {
        Map map = f0;
        synchronized (map) {
            map.put(iVar, oiVar);
        }
    }

    public final void b(String str, String[] strArr) {
        for (int i = 0; i != strArr.length; i++) {
            Class loadClass = loadClass(BouncyCastlePQCProvider.class, str + strArr[i] + "$Mappings");
            if (loadClass != null) {
                try {
                    ((wa) loadClass.newInstance()).a(this);
                } catch (Exception e) {
                    throw new InternalError("cannot create instance of " + str + strArr[i] + "$Mappings : " + e);
                }
            }
        }
    }

    public final void d() {
        b("org.bouncycastle.pqc.jcajce.provider.", g0);
    }

    public boolean hasAlgorithm(String str, String str2) {
        if (!containsKey(str + "." + str2)) {
            if (!containsKey("Alg.Alias." + str + "." + str2)) {
                return false;
            }
        }
        return true;
    }

    public void setParameter(String str, Object obj) {
        synchronized (CONFIGURATION) {
        }
    }
}
