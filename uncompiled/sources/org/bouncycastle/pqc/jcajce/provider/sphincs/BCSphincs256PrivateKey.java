package org.bouncycastle.pqc.jcajce.provider.sphincs;

import java.io.IOException;
import java.security.Key;
import java.security.PrivateKey;
import org.bouncycastle.asn1.i;
import org.bouncycastle.asn1.j0;

/* loaded from: classes2.dex */
public class BCSphincs256PrivateKey implements PrivateKey, Key {
    private static final long serialVersionUID = 1;
    private final va3 params;
    private final i treeDigest;

    public BCSphincs256PrivateKey(i iVar, va3 va3Var) {
        this.treeDigest = iVar;
        this.params = va3Var;
    }

    public BCSphincs256PrivateKey(zu2 zu2Var) throws IOException {
        this.treeDigest = ua3.o(zu2Var.p().q()).p().o();
        this.params = new va3(f4.B(zu2Var.q()).D());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof BCSphincs256PrivateKey) {
            BCSphincs256PrivateKey bCSphincs256PrivateKey = (BCSphincs256PrivateKey) obj;
            return this.treeDigest.equals(bCSphincs256PrivateKey.treeDigest) && wh.a(this.params.a(), bCSphincs256PrivateKey.params.a());
        }
        return false;
    }

    @Override // java.security.Key
    public final String getAlgorithm() {
        return "SPHINCS-256";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new zu2(new va(po2.e, new ua3(new va(this.treeDigest))), new j0(this.params.a())).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "PKCS#8";
    }

    public byte[] getKeyData() {
        return this.params.a();
    }

    public ty getKeyParams() {
        return this.params;
    }

    public int hashCode() {
        return this.treeDigest.hashCode() + (wh.r(this.params.a()) * 37);
    }
}
