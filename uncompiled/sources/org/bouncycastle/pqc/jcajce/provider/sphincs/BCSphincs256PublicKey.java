package org.bouncycastle.pqc.jcajce.provider.sphincs;

import java.io.IOException;
import java.security.Key;
import java.security.PublicKey;
import org.bouncycastle.asn1.i;

/* loaded from: classes2.dex */
public class BCSphincs256PublicKey implements PublicKey, Key {
    private static final long serialVersionUID = 1;
    private final wa3 params;
    private final i treeDigest;

    public BCSphincs256PublicKey(jv3 jv3Var) {
        this.treeDigest = ua3.o(jv3Var.o().q()).p().o();
        this.params = new wa3(jv3Var.q().D());
    }

    public BCSphincs256PublicKey(i iVar, wa3 wa3Var) {
        this.treeDigest = iVar;
        this.params = wa3Var;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof BCSphincs256PublicKey) {
            BCSphincs256PublicKey bCSphincs256PublicKey = (BCSphincs256PublicKey) obj;
            return this.treeDigest.equals(bCSphincs256PublicKey.treeDigest) && wh.a(this.params.a(), bCSphincs256PublicKey.params.a());
        }
        return false;
    }

    @Override // java.security.Key
    public final String getAlgorithm() {
        return "SPHINCS-256";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new jv3(new va(po2.e, new ua3(new va(this.treeDigest))), this.params.a()).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "X.509";
    }

    public byte[] getKeyData() {
        return this.params.a();
    }

    public ty getKeyParams() {
        return this.params;
    }

    public int hashCode() {
        return this.treeDigest.hashCode() + (wh.r(this.params.a()) * 37);
    }
}
