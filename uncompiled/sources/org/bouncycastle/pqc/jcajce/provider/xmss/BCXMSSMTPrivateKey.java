package org.bouncycastle.pqc.jcajce.provider.xmss;

import defpackage.cs4;
import java.io.IOException;
import java.security.PrivateKey;
import org.bouncycastle.asn1.i;
import org.bouncycastle.pqc.crypto.xmss.BDSStateMap;
import org.bouncycastle.pqc.crypto.xmss.f;

/* loaded from: classes2.dex */
public class BCXMSSMTPrivateKey implements PrivateKey {
    private final cs4 keyParams;
    private final i treeDigest;

    public BCXMSSMTPrivateKey(i iVar, cs4 cs4Var) {
        this.treeDigest = iVar;
        this.keyParams = cs4Var;
    }

    public BCXMSSMTPrivateKey(zu2 zu2Var) throws IOException {
        as4 p = as4.p(zu2Var.p().q());
        i o = p.s().o();
        this.treeDigest = o;
        gs4 q = gs4.q(zu2Var.q());
        try {
            cs4.b n = new cs4.b(new f(p.o(), p.q(), so0.a(o))).l(q.p()).p(q.y()).o(q.w()).m(q.s()).n(q.t());
            if (q.o() != null) {
                n.k((BDSStateMap) org.bouncycastle.pqc.crypto.xmss.i.f(q.o(), BDSStateMap.class));
            }
            this.keyParams = n.j();
        } catch (ClassNotFoundException e) {
            throw new IOException("ClassNotFoundException processing BDS state: " + e.getMessage());
        }
    }

    public final bs4 a() {
        byte[] b = this.keyParams.b();
        int b2 = this.keyParams.a().b();
        int c = this.keyParams.a().c();
        int i = (c + 7) / 8;
        int a = (int) org.bouncycastle.pqc.crypto.xmss.i.a(b, 0, i);
        if (org.bouncycastle.pqc.crypto.xmss.i.l(c, a)) {
            int i2 = i + 0;
            byte[] g = org.bouncycastle.pqc.crypto.xmss.i.g(b, i2, b2);
            int i3 = i2 + b2;
            byte[] g2 = org.bouncycastle.pqc.crypto.xmss.i.g(b, i3, b2);
            int i4 = i3 + b2;
            byte[] g3 = org.bouncycastle.pqc.crypto.xmss.i.g(b, i4, b2);
            int i5 = i4 + b2;
            byte[] g4 = org.bouncycastle.pqc.crypto.xmss.i.g(b, i5, b2);
            int i6 = i5 + b2;
            return new bs4(a, g, g2, g3, g4, org.bouncycastle.pqc.crypto.xmss.i.g(b, i6, b.length - i6));
        }
        throw new IllegalArgumentException("index out of bounds");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof BCXMSSMTPrivateKey) {
            BCXMSSMTPrivateKey bCXMSSMTPrivateKey = (BCXMSSMTPrivateKey) obj;
            return this.treeDigest.equals(bCXMSSMTPrivateKey.treeDigest) && wh.a(this.keyParams.b(), bCXMSSMTPrivateKey.keyParams.b());
        }
        return false;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return "XMSSMT";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new zu2(new va(po2.h, new as4(this.keyParams.a().c(), this.keyParams.a().d(), new va(this.treeDigest))), a()).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "PKCS#8";
    }

    public int getHeight() {
        return this.keyParams.a().c();
    }

    public ty getKeyParams() {
        return this.keyParams;
    }

    public int getLayers() {
        return this.keyParams.a().d();
    }

    public String getTreeDigest() {
        return so0.b(this.treeDigest);
    }

    public i getTreeDigestOID() {
        return this.treeDigest;
    }

    public int hashCode() {
        return this.treeDigest.hashCode() + (wh.r(this.keyParams.b()) * 37);
    }
}
