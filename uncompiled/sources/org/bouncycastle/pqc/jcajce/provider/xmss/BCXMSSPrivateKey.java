package org.bouncycastle.pqc.jcajce.provider.xmss;

import java.io.IOException;
import java.security.PrivateKey;
import org.bouncycastle.asn1.i;
import org.bouncycastle.pqc.crypto.xmss.BDS;
import org.bouncycastle.pqc.crypto.xmss.h;

/* loaded from: classes2.dex */
public class BCXMSSPrivateKey implements PrivateKey {
    private final h keyParams;
    private final i treeDigest;

    public BCXMSSPrivateKey(i iVar, h hVar) {
        this.treeDigest = iVar;
        this.keyParams = hVar;
    }

    public BCXMSSPrivateKey(zu2 zu2Var) throws IOException {
        zr4 p = zr4.p(zu2Var.p().q());
        i o = p.q().o();
        this.treeDigest = o;
        gs4 q = gs4.q(zu2Var.q());
        try {
            h.b n = new h.b(new fs4(p.o(), so0.a(o))).l(q.p()).p(q.y()).o(q.w()).m(q.s()).n(q.t());
            if (q.o() != null) {
                n.k((BDS) org.bouncycastle.pqc.crypto.xmss.i.f(q.o(), BDS.class));
            }
            this.keyParams = n.j();
        } catch (ClassNotFoundException e) {
            throw new IOException("ClassNotFoundException processing BDS state: " + e.getMessage());
        }
    }

    public final gs4 a() {
        byte[] b = this.keyParams.b();
        int c = this.keyParams.a().c();
        int d = this.keyParams.a().d();
        int a = (int) org.bouncycastle.pqc.crypto.xmss.i.a(b, 0, 4);
        if (org.bouncycastle.pqc.crypto.xmss.i.l(d, a)) {
            byte[] g = org.bouncycastle.pqc.crypto.xmss.i.g(b, 4, c);
            int i = 4 + c;
            byte[] g2 = org.bouncycastle.pqc.crypto.xmss.i.g(b, i, c);
            int i2 = i + c;
            byte[] g3 = org.bouncycastle.pqc.crypto.xmss.i.g(b, i2, c);
            int i3 = i2 + c;
            byte[] g4 = org.bouncycastle.pqc.crypto.xmss.i.g(b, i3, c);
            int i4 = i3 + c;
            return new gs4(a, g, g2, g3, g4, org.bouncycastle.pqc.crypto.xmss.i.g(b, i4, b.length - i4));
        }
        throw new IllegalArgumentException("index out of bounds");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof BCXMSSPrivateKey) {
            BCXMSSPrivateKey bCXMSSPrivateKey = (BCXMSSPrivateKey) obj;
            return this.treeDigest.equals(bCXMSSPrivateKey.treeDigest) && wh.a(this.keyParams.b(), bCXMSSPrivateKey.keyParams.b());
        }
        return false;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return "XMSS";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new zu2(new va(po2.g, new zr4(this.keyParams.a().d(), new va(this.treeDigest))), a()).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "PKCS#8";
    }

    public int getHeight() {
        return this.keyParams.a().d();
    }

    public ty getKeyParams() {
        return this.keyParams;
    }

    public String getTreeDigest() {
        return so0.b(this.treeDigest);
    }

    public i getTreeDigestOID() {
        return this.treeDigest;
    }

    public int hashCode() {
        return this.treeDigest.hashCode() + (wh.r(this.keyParams.b()) * 37);
    }
}
