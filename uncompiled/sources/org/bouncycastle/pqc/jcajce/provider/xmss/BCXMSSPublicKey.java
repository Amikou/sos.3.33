package org.bouncycastle.pqc.jcajce.provider.xmss;

import defpackage.is4;
import java.io.IOException;
import java.security.PublicKey;
import org.bouncycastle.asn1.i;

/* loaded from: classes2.dex */
public class BCXMSSPublicKey implements PublicKey {
    private final is4 keyParams;
    private final i treeDigest;

    public BCXMSSPublicKey(jv3 jv3Var) throws IOException {
        zr4 p = zr4.p(jv3Var.o().q());
        i o = p.q().o();
        this.treeDigest = o;
        hs4 o2 = hs4.o(jv3Var.s());
        this.keyParams = new is4.b(new fs4(p.o(), so0.a(o))).f(o2.p()).g(o2.q()).e();
    }

    public BCXMSSPublicKey(i iVar, is4 is4Var) {
        this.treeDigest = iVar;
        this.keyParams = is4Var;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof BCXMSSPublicKey) {
            BCXMSSPublicKey bCXMSSPublicKey = (BCXMSSPublicKey) obj;
            return this.treeDigest.equals(bCXMSSPublicKey.treeDigest) && wh.a(this.keyParams.d(), bCXMSSPublicKey.keyParams.d());
        }
        return false;
    }

    @Override // java.security.Key
    public final String getAlgorithm() {
        return "XMSS";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new jv3(new va(po2.g, new zr4(this.keyParams.a().d(), new va(this.treeDigest))), new hs4(this.keyParams.b(), this.keyParams.c())).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "X.509";
    }

    public int getHeight() {
        return this.keyParams.a().d();
    }

    public ty getKeyParams() {
        return this.keyParams;
    }

    public String getTreeDigest() {
        return so0.b(this.treeDigest);
    }

    public int hashCode() {
        return this.treeDigest.hashCode() + (wh.r(this.keyParams.d()) * 37);
    }
}
