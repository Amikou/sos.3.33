package org.bouncycastle.pqc.jcajce.provider.xmss;

import defpackage.ds4;
import java.io.IOException;
import java.security.PublicKey;
import org.bouncycastle.asn1.i;
import org.bouncycastle.pqc.crypto.xmss.f;

/* loaded from: classes2.dex */
public class BCXMSSMTPublicKey implements PublicKey {
    private final ds4 keyParams;
    private final i treeDigest;

    public BCXMSSMTPublicKey(jv3 jv3Var) throws IOException {
        as4 p = as4.p(jv3Var.o().q());
        i o = p.s().o();
        this.treeDigest = o;
        hs4 o2 = hs4.o(jv3Var.s());
        this.keyParams = new ds4.b(new f(p.o(), p.q(), so0.a(o))).f(o2.p()).g(o2.q()).e();
    }

    public BCXMSSMTPublicKey(i iVar, ds4 ds4Var) {
        this.treeDigest = iVar;
        this.keyParams = ds4Var;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof BCXMSSMTPublicKey) {
            BCXMSSMTPublicKey bCXMSSMTPublicKey = (BCXMSSMTPublicKey) obj;
            return this.treeDigest.equals(bCXMSSMTPublicKey.treeDigest) && wh.a(this.keyParams.d(), bCXMSSMTPublicKey.keyParams.d());
        }
        return false;
    }

    @Override // java.security.Key
    public final String getAlgorithm() {
        return "XMSSMT";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        try {
            return new jv3(new va(po2.h, new as4(this.keyParams.a().c(), this.keyParams.a().d(), new va(this.treeDigest))), new hs4(this.keyParams.b(), this.keyParams.c())).m();
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.Key
    public String getFormat() {
        return "X.509";
    }

    public int getHeight() {
        return this.keyParams.a().c();
    }

    public ty getKeyParams() {
        return this.keyParams;
    }

    public int getLayers() {
        return this.keyParams.a().d();
    }

    public String getTreeDigest() {
        return so0.b(this.treeDigest);
    }

    public int hashCode() {
        return this.treeDigest.hashCode() + (wh.r(this.keyParams.d()) * 37);
    }
}
