package org.bouncycastle.pqc.crypto.xmss;

/* loaded from: classes2.dex */
public final class f {
    public final fs4 a;
    public final int b;
    public final int c;

    public f(int i, int i2, qo0 qo0Var) {
        this.b = i;
        this.c = i2;
        this.a = new fs4(h(i, i2), qo0Var);
        pl0.b(a().g(), b(), f(), e(), c(), i2);
    }

    public static int h(int i, int i2) throws IllegalArgumentException {
        if (i >= 2) {
            if (i % i2 == 0) {
                int i3 = i / i2;
                if (i3 != 1) {
                    return i3;
                }
                throw new IllegalArgumentException("height / layers must be greater than 1");
            }
            throw new IllegalArgumentException("layers must divide totalHeight without remainder");
        }
        throw new IllegalArgumentException("totalHeight must be > 1");
    }

    public qo0 a() {
        return this.a.b();
    }

    public int b() {
        return this.a.c();
    }

    public int c() {
        return this.b;
    }

    public int d() {
        return this.c;
    }

    public int e() {
        return this.a.f().d().c();
    }

    public int f() {
        return this.a.g();
    }

    public fs4 g() {
        return this.a;
    }
}
