package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.pqc.crypto.xmss.e;

/* loaded from: classes2.dex */
public final class b extends e {
    public final int e;
    public final int f;
    public final int g;

    /* renamed from: org.bouncycastle.pqc.crypto.xmss.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static class C0258b extends e.a<C0258b> {
        public int e;
        public int f;
        public int g;

        public C0258b() {
            super(1);
            this.e = 0;
            this.f = 0;
            this.g = 0;
        }

        public e l() {
            return new b(this);
        }

        @Override // org.bouncycastle.pqc.crypto.xmss.e.a
        /* renamed from: m */
        public C0258b e() {
            return this;
        }

        public C0258b n(int i) {
            this.e = i;
            return this;
        }

        public C0258b o(int i) {
            this.f = i;
            return this;
        }

        public C0258b p(int i) {
            this.g = i;
            return this;
        }
    }

    public b(C0258b c0258b) {
        super(c0258b);
        this.e = c0258b.e;
        this.f = c0258b.f;
        this.g = c0258b.g;
    }

    @Override // org.bouncycastle.pqc.crypto.xmss.e
    public byte[] d() {
        byte[] d = super.d();
        ro2.c(this.e, d, 16);
        ro2.c(this.f, d, 20);
        ro2.c(this.g, d, 24);
        return d;
    }

    public int e() {
        return this.e;
    }

    public int f() {
        return this.f;
    }

    public int g() {
        return this.g;
    }
}
