package org.bouncycastle.pqc.crypto.xmss;

import java.io.IOException;
import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.c;

/* loaded from: classes2.dex */
public final class h extends pi {
    public final fs4 a;
    public final byte[] f0;
    public final byte[] g0;
    public final byte[] h0;
    public final byte[] i0;
    public final BDS j0;

    /* loaded from: classes2.dex */
    public static class b {
        public final fs4 a;
        public int b = 0;
        public byte[] c = null;
        public byte[] d = null;
        public byte[] e = null;
        public byte[] f = null;
        public BDS g = null;
        public byte[] h = null;
        public fs4 i = null;

        public b(fs4 fs4Var) {
            this.a = fs4Var;
        }

        public h j() {
            return new h(this);
        }

        public b k(BDS bds) {
            this.g = bds;
            return this;
        }

        public b l(int i) {
            this.b = i;
            return this;
        }

        public b m(byte[] bArr) {
            this.e = i.c(bArr);
            return this;
        }

        public b n(byte[] bArr) {
            this.f = i.c(bArr);
            return this;
        }

        public b o(byte[] bArr) {
            this.d = i.c(bArr);
            return this;
        }

        public b p(byte[] bArr) {
            this.c = i.c(bArr);
            return this;
        }
    }

    public h(b bVar) {
        super(true);
        fs4 fs4Var = bVar.a;
        this.a = fs4Var;
        Objects.requireNonNull(fs4Var, "params == null");
        int c = fs4Var.c();
        byte[] bArr = bVar.h;
        if (bArr != null) {
            Objects.requireNonNull(bVar.i, "xmss == null");
            int d = fs4Var.d();
            int a2 = ro2.a(bArr, 0);
            if (!i.l(d, a2)) {
                throw new IllegalArgumentException("index out of bounds");
            }
            this.f0 = i.g(bArr, 4, c);
            int i = 4 + c;
            this.g0 = i.g(bArr, i, c);
            int i2 = i + c;
            this.h0 = i.g(bArr, i2, c);
            int i3 = i2 + c;
            this.i0 = i.g(bArr, i3, c);
            int i4 = i3 + c;
            try {
                BDS bds = (BDS) i.f(i.g(bArr, i4, bArr.length - i4), BDS.class);
                bds.setXMSS(bVar.i);
                bds.validate();
                if (bds.getIndex() != a2) {
                    throw new IllegalStateException("serialized BDS has wrong index");
                }
                this.j0 = bds;
                return;
            } catch (IOException e) {
                throw new IllegalArgumentException(e.getMessage(), e);
            } catch (ClassNotFoundException e2) {
                throw new IllegalArgumentException(e2.getMessage(), e2);
            }
        }
        byte[] bArr2 = bVar.c;
        if (bArr2 == null) {
            this.f0 = new byte[c];
        } else if (bArr2.length != c) {
            throw new IllegalArgumentException("size of secretKeySeed needs to be equal size of digest");
        } else {
            this.f0 = bArr2;
        }
        byte[] bArr3 = bVar.d;
        if (bArr3 == null) {
            this.g0 = new byte[c];
        } else if (bArr3.length != c) {
            throw new IllegalArgumentException("size of secretKeyPRF needs to be equal size of digest");
        } else {
            this.g0 = bArr3;
        }
        byte[] bArr4 = bVar.e;
        if (bArr4 == null) {
            this.h0 = new byte[c];
        } else if (bArr4.length != c) {
            throw new IllegalArgumentException("size of publicSeed needs to be equal size of digest");
        } else {
            this.h0 = bArr4;
        }
        byte[] bArr5 = bVar.f;
        if (bArr5 == null) {
            this.i0 = new byte[c];
        } else if (bArr5.length != c) {
            throw new IllegalArgumentException("size of root needs to be equal size of digest");
        } else {
            this.i0 = bArr5;
        }
        BDS bds2 = bVar.g;
        if (bds2 != null) {
            this.j0 = bds2;
        } else {
            this.j0 = (bVar.b >= (1 << fs4Var.d()) + (-2) || bArr4 == null || bArr2 == null) ? new BDS(fs4Var, bVar.b) : new BDS(fs4Var, bArr4, bArr2, (c) new c.b().l(), bVar.b);
        }
    }

    public fs4 a() {
        return this.a;
    }

    public byte[] b() {
        int c = this.a.c();
        byte[] bArr = new byte[c + 4 + c + c + c];
        ro2.c(this.j0.getIndex(), bArr, 0);
        i.e(bArr, this.f0, 4);
        int i = 4 + c;
        i.e(bArr, this.g0, i);
        int i2 = i + c;
        i.e(bArr, this.h0, i2);
        i.e(bArr, this.i0, i2 + c);
        try {
            return wh.i(bArr, i.o(this.j0));
        } catch (IOException e) {
            throw new RuntimeException("error serializing bds state: " + e.getMessage());
        }
    }
}
