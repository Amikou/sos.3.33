package org.bouncycastle.pqc.crypto.xmss;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.a;
import org.bouncycastle.pqc.crypto.xmss.b;

/* loaded from: classes2.dex */
public class g {
    public static XMSSNode a(d dVar, kl4 kl4Var, b bVar) {
        double d;
        Objects.requireNonNull(kl4Var, "publicKey == null");
        Objects.requireNonNull(bVar, "address == null");
        int c = dVar.d().c();
        byte[][] a = kl4Var.a();
        XMSSNode[] xMSSNodeArr = new XMSSNode[a.length];
        for (int i = 0; i < a.length; i++) {
            xMSSNodeArr[i] = new XMSSNode(0, a[i]);
        }
        b.C0258b f = new b.C0258b().g(bVar.b()).h(bVar.c()).n(bVar.e()).o(0).p(bVar.g()).f(bVar.a());
        while (true) {
            b bVar2 = (b) f.l();
            if (c <= 1) {
                return xMSSNodeArr[0];
            }
            int i2 = 0;
            while (true) {
                d = c / 2;
                if (i2 >= ((int) Math.floor(d))) {
                    break;
                }
                bVar2 = (b) new b.C0258b().g(bVar2.b()).h(bVar2.c()).n(bVar2.e()).o(bVar2.f()).p(i2).f(bVar2.a()).l();
                int i3 = i2 * 2;
                xMSSNodeArr[i2] = b(dVar, xMSSNodeArr[i3], xMSSNodeArr[i3 + 1], bVar2);
                i2++;
            }
            if (c % 2 == 1) {
                xMSSNodeArr[(int) Math.floor(d)] = xMSSNodeArr[c - 1];
            }
            c = (int) Math.ceil(c / 2.0d);
            f = new b.C0258b().g(bVar2.b()).h(bVar2.c()).n(bVar2.e()).o(bVar2.f() + 1).p(bVar2.g()).f(bVar2.a());
        }
    }

    public static XMSSNode b(d dVar, XMSSNode xMSSNode, XMSSNode xMSSNode2, e eVar) {
        Objects.requireNonNull(xMSSNode, "left == null");
        Objects.requireNonNull(xMSSNode2, "right == null");
        if (xMSSNode.getHeight() == xMSSNode2.getHeight()) {
            Objects.requireNonNull(eVar, "address == null");
            byte[] f = dVar.f();
            if (eVar instanceof b) {
                b bVar = (b) eVar;
                eVar = (b) new b.C0258b().g(bVar.b()).h(bVar.c()).n(bVar.e()).o(bVar.f()).p(bVar.g()).f(0).l();
            } else if (eVar instanceof a) {
                a aVar = (a) eVar;
                eVar = (a) new a.b().g(aVar.b()).h(aVar.c()).m(aVar.e()).n(aVar.f()).f(0).k();
            }
            byte[] c = dVar.c().c(f, eVar.d());
            if (eVar instanceof b) {
                b bVar2 = (b) eVar;
                eVar = (b) new b.C0258b().g(bVar2.b()).h(bVar2.c()).n(bVar2.e()).o(bVar2.f()).p(bVar2.g()).f(1).l();
            } else if (eVar instanceof a) {
                a aVar2 = (a) eVar;
                eVar = (a) new a.b().g(aVar2.b()).h(aVar2.c()).m(aVar2.e()).n(aVar2.f()).f(1).k();
            }
            byte[] c2 = dVar.c().c(f, eVar.d());
            if (eVar instanceof b) {
                b bVar3 = (b) eVar;
                eVar = (b) new b.C0258b().g(bVar3.b()).h(bVar3.c()).n(bVar3.e()).o(bVar3.f()).p(bVar3.g()).f(2).l();
            } else if (eVar instanceof a) {
                a aVar3 = (a) eVar;
                eVar = (a) new a.b().g(aVar3.b()).h(aVar3.c()).m(aVar3.e()).n(aVar3.f()).f(2).k();
            }
            byte[] c3 = dVar.c().c(f, eVar.d());
            int b = dVar.d().b();
            byte[] bArr = new byte[b * 2];
            for (int i = 0; i < b; i++) {
                bArr[i] = (byte) (xMSSNode.getValue()[i] ^ c2[i]);
            }
            for (int i2 = 0; i2 < b; i2++) {
                bArr[i2 + b] = (byte) (xMSSNode2.getValue()[i2] ^ c3[i2]);
            }
            return new XMSSNode(xMSSNode.getHeight(), dVar.c().b(c, bArr));
        }
        throw new IllegalStateException("height of both nodes must be equal");
    }
}
