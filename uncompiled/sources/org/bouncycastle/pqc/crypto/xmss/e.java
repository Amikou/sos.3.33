package org.bouncycastle.pqc.crypto.xmss;

/* loaded from: classes2.dex */
public abstract class e {
    public final int a;
    public final long b;
    public final int c;
    public final int d;

    /* loaded from: classes2.dex */
    public static abstract class a<T extends a> {
        public final int a;
        public int b = 0;
        public long c = 0;
        public int d = 0;

        public a(int i) {
            this.a = i;
        }

        public abstract T e();

        public T f(int i) {
            this.d = i;
            return e();
        }

        public T g(int i) {
            this.b = i;
            return e();
        }

        public T h(long j) {
            this.c = j;
            return e();
        }
    }

    public e(a aVar) {
        this.a = aVar.b;
        this.b = aVar.c;
        this.c = aVar.a;
        this.d = aVar.d;
    }

    public final int a() {
        return this.d;
    }

    public final int b() {
        return this.a;
    }

    public final long c() {
        return this.b;
    }

    public byte[] d() {
        byte[] bArr = new byte[32];
        ro2.c(this.a, bArr, 0);
        ro2.k(this.b, bArr, 4);
        ro2.c(this.c, bArr, 12);
        ro2.c(this.d, bArr, 28);
        return bArr;
    }
}
