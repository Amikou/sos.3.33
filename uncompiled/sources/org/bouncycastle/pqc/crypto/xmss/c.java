package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.pqc.crypto.xmss.e;

/* loaded from: classes2.dex */
public final class c extends e {
    public final int e;
    public final int f;
    public final int g;

    /* loaded from: classes2.dex */
    public static class b extends e.a<b> {
        public int e;
        public int f;
        public int g;

        public b() {
            super(0);
            this.e = 0;
            this.f = 0;
            this.g = 0;
        }

        public e l() {
            return new c(this);
        }

        @Override // org.bouncycastle.pqc.crypto.xmss.e.a
        /* renamed from: m */
        public b e() {
            return this;
        }

        public b n(int i) {
            this.f = i;
            return this;
        }

        public b o(int i) {
            this.g = i;
            return this;
        }

        public b p(int i) {
            this.e = i;
            return this;
        }
    }

    public c(b bVar) {
        super(bVar);
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
    }

    @Override // org.bouncycastle.pqc.crypto.xmss.e
    public byte[] d() {
        byte[] d = super.d();
        ro2.c(this.e, d, 16);
        ro2.c(this.f, d, 20);
        ro2.c(this.g, d, 24);
        return d;
    }

    public int e() {
        return this.f;
    }

    public int f() {
        return this.g;
    }

    public int g() {
        return this.e;
    }
}
