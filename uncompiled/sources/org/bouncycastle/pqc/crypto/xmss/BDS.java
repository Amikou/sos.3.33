package org.bouncycastle.pqc.crypto.xmss;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.TreeMap;
import org.bouncycastle.pqc.crypto.xmss.a;
import org.bouncycastle.pqc.crypto.xmss.b;
import org.bouncycastle.pqc.crypto.xmss.c;

/* loaded from: classes2.dex */
public final class BDS implements Serializable {
    private static final long serialVersionUID = 1;
    public transient d a;
    private List<XMSSNode> authenticationPath;
    private int index;
    private int k;
    private Map<Integer, XMSSNode> keep;
    private Map<Integer, LinkedList<XMSSNode>> retain;
    private XMSSNode root;
    private Stack<XMSSNode> stack;
    private final List<BDSTreeHash> treeHashInstances;
    private final int treeHeight;
    private boolean used;

    public BDS(fs4 fs4Var, int i) {
        this(fs4Var.f(), fs4Var.d(), fs4Var.e());
        this.index = i;
        this.used = true;
    }

    public BDS(fs4 fs4Var, byte[] bArr, byte[] bArr2, c cVar) {
        this(fs4Var.f(), fs4Var.d(), fs4Var.e());
        b(bArr, bArr2, cVar);
    }

    public BDS(fs4 fs4Var, byte[] bArr, byte[] bArr2, c cVar, int i) {
        this(fs4Var.f(), fs4Var.d(), fs4Var.e());
        b(bArr, bArr2, cVar);
        while (this.index < i) {
            c(bArr, bArr2, cVar);
            this.used = false;
        }
    }

    public BDS(BDS bds, byte[] bArr, byte[] bArr2, c cVar) {
        this.a = bds.a;
        this.treeHeight = bds.treeHeight;
        this.k = bds.k;
        this.root = bds.root;
        this.authenticationPath = new ArrayList(bds.authenticationPath);
        this.retain = bds.retain;
        this.stack = (Stack) bds.stack.clone();
        this.treeHashInstances = bds.treeHashInstances;
        this.keep = new TreeMap(bds.keep);
        this.index = bds.index;
        c(bArr, bArr2, cVar);
        bds.used = true;
    }

    public BDS(d dVar, int i, int i2) {
        this.a = dVar;
        this.treeHeight = i;
        this.k = i2;
        if (i2 <= i && i2 >= 2) {
            int i3 = i - i2;
            if (i3 % 2 == 0) {
                this.authenticationPath = new ArrayList();
                this.retain = new TreeMap();
                this.stack = new Stack<>();
                this.treeHashInstances = new ArrayList();
                for (int i4 = 0; i4 < i3; i4++) {
                    this.treeHashInstances.add(new BDSTreeHash(i4));
                }
                this.keep = new TreeMap();
                this.index = 0;
                this.used = false;
                return;
            }
        }
        throw new IllegalArgumentException("illegal value for BDS parameter k");
    }

    public final BDSTreeHash a() {
        BDSTreeHash bDSTreeHash = null;
        for (BDSTreeHash bDSTreeHash2 : this.treeHashInstances) {
            if (!bDSTreeHash2.isFinished() && bDSTreeHash2.isInitialized() && (bDSTreeHash == null || bDSTreeHash2.getHeight() < bDSTreeHash.getHeight() || (bDSTreeHash2.getHeight() == bDSTreeHash.getHeight() && bDSTreeHash2.getIndexLeaf() < bDSTreeHash.getIndexLeaf()))) {
                bDSTreeHash = bDSTreeHash2;
            }
        }
        return bDSTreeHash;
    }

    public final void b(byte[] bArr, byte[] bArr2, c cVar) {
        Objects.requireNonNull(cVar, "otsHashAddress == null");
        b bVar = (b) new b.C0258b().g(cVar.b()).h(cVar.c()).l();
        a aVar = (a) new a.b().g(cVar.b()).h(cVar.c()).k();
        for (int i = 0; i < (1 << this.treeHeight); i++) {
            cVar = (c) new c.b().g(cVar.b()).h(cVar.c()).p(i).n(cVar.e()).o(cVar.f()).f(cVar.a()).l();
            d dVar = this.a;
            dVar.h(dVar.g(bArr2, cVar), bArr);
            kl4 e = this.a.e(cVar);
            bVar = (b) new b.C0258b().g(bVar.b()).h(bVar.c()).n(i).o(bVar.f()).p(bVar.g()).f(bVar.a()).l();
            XMSSNode a = g.a(this.a, e, bVar);
            aVar = (a) new a.b().g(aVar.b()).h(aVar.c()).n(i).f(aVar.a()).k();
            while (!this.stack.isEmpty() && this.stack.peek().getHeight() == a.getHeight()) {
                int floor = (int) Math.floor(i / (1 << a.getHeight()));
                if (floor == 1) {
                    this.authenticationPath.add(a.clone());
                }
                if (floor == 3 && a.getHeight() < this.treeHeight - this.k) {
                    this.treeHashInstances.get(a.getHeight()).setNode(a.clone());
                }
                if (floor >= 3 && (floor & 1) == 1 && a.getHeight() >= this.treeHeight - this.k && a.getHeight() <= this.treeHeight - 2) {
                    if (this.retain.get(Integer.valueOf(a.getHeight())) == null) {
                        LinkedList<XMSSNode> linkedList = new LinkedList<>();
                        linkedList.add(a.clone());
                        this.retain.put(Integer.valueOf(a.getHeight()), linkedList);
                    } else {
                        this.retain.get(Integer.valueOf(a.getHeight())).add(a.clone());
                    }
                }
                a aVar2 = (a) new a.b().g(aVar.b()).h(aVar.c()).m(aVar.e()).n((aVar.f() - 1) / 2).f(aVar.a()).k();
                XMSSNode b = g.b(this.a, this.stack.pop(), a, aVar2);
                XMSSNode xMSSNode = new XMSSNode(b.getHeight() + 1, b.getValue());
                aVar = (a) new a.b().g(aVar2.b()).h(aVar2.c()).m(aVar2.e() + 1).n(aVar2.f()).f(aVar2.a()).k();
                a = xMSSNode;
            }
            this.stack.push(a);
        }
        this.root = this.stack.pop();
    }

    public final void c(byte[] bArr, byte[] bArr2, c cVar) {
        List<XMSSNode> list;
        XMSSNode removeFirst;
        Objects.requireNonNull(cVar, "otsHashAddress == null");
        if (this.used) {
            throw new IllegalStateException("index already used");
        }
        if (this.index > (1 << this.treeHeight) - 2) {
            throw new IllegalStateException("index out of bounds");
        }
        b bVar = (b) new b.C0258b().g(cVar.b()).h(cVar.c()).l();
        a aVar = (a) new a.b().g(cVar.b()).h(cVar.c()).k();
        int b = i.b(this.index, this.treeHeight);
        if (((this.index >> (b + 1)) & 1) == 0 && b < this.treeHeight - 1) {
            this.keep.put(Integer.valueOf(b), this.authenticationPath.get(b).clone());
        }
        if (b == 0) {
            cVar = (c) new c.b().g(cVar.b()).h(cVar.c()).p(this.index).n(cVar.e()).o(cVar.f()).f(cVar.a()).l();
            d dVar = this.a;
            dVar.h(dVar.g(bArr2, cVar), bArr);
            this.authenticationPath.set(0, g.a(this.a, this.a.e(cVar), (b) new b.C0258b().g(bVar.b()).h(bVar.c()).n(this.index).o(bVar.f()).p(bVar.g()).f(bVar.a()).l()));
        } else {
            int i = b - 1;
            XMSSNode b2 = g.b(this.a, this.authenticationPath.get(i), this.keep.get(Integer.valueOf(i)), (a) new a.b().g(aVar.b()).h(aVar.c()).m(i).n(this.index >> b).f(aVar.a()).k());
            this.authenticationPath.set(b, new XMSSNode(b2.getHeight() + 1, b2.getValue()));
            this.keep.remove(Integer.valueOf(i));
            for (int i2 = 0; i2 < b; i2++) {
                if (i2 < this.treeHeight - this.k) {
                    list = this.authenticationPath;
                    removeFirst = this.treeHashInstances.get(i2).getTailNode();
                } else {
                    list = this.authenticationPath;
                    removeFirst = this.retain.get(Integer.valueOf(i2)).removeFirst();
                }
                list.set(i2, removeFirst);
            }
            int min = Math.min(b, this.treeHeight - this.k);
            for (int i3 = 0; i3 < min; i3++) {
                int i4 = this.index + 1 + ((1 << i3) * 3);
                if (i4 < (1 << this.treeHeight)) {
                    this.treeHashInstances.get(i3).initialize(i4);
                }
            }
        }
        for (int i5 = 0; i5 < ((this.treeHeight - this.k) >> 1); i5++) {
            BDSTreeHash a = a();
            if (a != null) {
                a.update(this.stack, this.a, bArr, bArr2, cVar);
            }
        }
        this.index++;
    }

    public List<XMSSNode> getAuthenticationPath() {
        ArrayList arrayList = new ArrayList();
        for (XMSSNode xMSSNode : this.authenticationPath) {
            arrayList.add(xMSSNode.clone());
        }
        return arrayList;
    }

    public int getIndex() {
        return this.index;
    }

    public BDS getNextState(byte[] bArr, byte[] bArr2, c cVar) {
        return new BDS(this, bArr, bArr2, cVar);
    }

    public XMSSNode getRoot() {
        return this.root.clone();
    }

    public int getTreeHeight() {
        return this.treeHeight;
    }

    public boolean isUsed() {
        return this.used;
    }

    public void setXMSS(fs4 fs4Var) {
        if (this.treeHeight != fs4Var.d()) {
            throw new IllegalStateException("wrong height");
        }
        this.a = fs4Var.f();
    }

    public void validate() {
        if (this.authenticationPath == null) {
            throw new IllegalStateException("authenticationPath == null");
        }
        if (this.retain == null) {
            throw new IllegalStateException("retain == null");
        }
        if (this.stack == null) {
            throw new IllegalStateException("stack == null");
        }
        if (this.treeHashInstances == null) {
            throw new IllegalStateException("treeHashInstances == null");
        }
        if (this.keep == null) {
            throw new IllegalStateException("keep == null");
        }
        if (!i.l(this.treeHeight, this.index)) {
            throw new IllegalStateException("index in BDS state out of bounds");
        }
    }
}
