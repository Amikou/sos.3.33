package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.pqc.crypto.xmss.e;

/* loaded from: classes2.dex */
public final class a extends e {
    public final int e;
    public final int f;
    public final int g;

    /* loaded from: classes2.dex */
    public static class b extends e.a<b> {
        public int e;
        public int f;

        public b() {
            super(2);
            this.e = 0;
            this.f = 0;
        }

        public e k() {
            return new a(this);
        }

        @Override // org.bouncycastle.pqc.crypto.xmss.e.a
        /* renamed from: l */
        public b e() {
            return this;
        }

        public b m(int i) {
            this.e = i;
            return this;
        }

        public b n(int i) {
            this.f = i;
            return this;
        }
    }

    public a(b bVar) {
        super(bVar);
        this.e = 0;
        this.f = bVar.e;
        this.g = bVar.f;
    }

    @Override // org.bouncycastle.pqc.crypto.xmss.e
    public byte[] d() {
        byte[] d = super.d();
        ro2.c(this.e, d, 16);
        ro2.c(this.f, d, 20);
        ro2.c(this.g, d, 24);
        return d;
    }

    public int e() {
        return this.f;
    }

    public int f() {
        return this.g;
    }
}
