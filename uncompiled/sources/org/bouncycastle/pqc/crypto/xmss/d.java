package org.bouncycastle.pqc.crypto.xmss;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.c;

/* loaded from: classes2.dex */
public final class d {
    public final jl4 a;
    public final qx1 b;
    public byte[] c;
    public byte[] d;

    public d(jl4 jl4Var) {
        Objects.requireNonNull(jl4Var, "params == null");
        this.a = jl4Var;
        int b = jl4Var.b();
        this.b = new qx1(jl4Var.a(), b);
        this.c = new byte[b];
        this.d = new byte[b];
    }

    public final byte[] a(byte[] bArr, int i, int i2, c cVar) {
        int b = this.a.b();
        Objects.requireNonNull(bArr, "startHash == null");
        if (bArr.length != b) {
            throw new IllegalArgumentException("startHash needs to be " + b + "bytes");
        }
        Objects.requireNonNull(cVar, "otsHashAddress == null");
        Objects.requireNonNull(cVar.d(), "otsHashAddress byte array == null");
        int i3 = i + i2;
        if (i3 <= this.a.d() - 1) {
            if (i2 == 0) {
                return bArr;
            }
            byte[] a = a(bArr, i, i2 - 1, cVar);
            c cVar2 = (c) new c.b().g(cVar.b()).h(cVar.c()).p(cVar.g()).n(cVar.e()).o(i3 - 1).f(0).l();
            byte[] c = this.b.c(this.d, cVar2.d());
            byte[] c2 = this.b.c(this.d, ((c) new c.b().g(cVar2.b()).h(cVar2.c()).p(cVar2.g()).n(cVar2.e()).o(cVar2.f()).f(1).l()).d());
            byte[] bArr2 = new byte[b];
            for (int i4 = 0; i4 < b; i4++) {
                bArr2[i4] = (byte) (a[i4] ^ c2[i4]);
            }
            return this.b.a(c, bArr2);
        }
        throw new IllegalArgumentException("max chain length must not be greater than w");
    }

    public final byte[] b(int i) {
        if (i < 0 || i >= this.a.c()) {
            throw new IllegalArgumentException("index out of bounds");
        }
        return this.b.c(this.c, i.p(i, 32));
    }

    public qx1 c() {
        return this.b;
    }

    public jl4 d() {
        return this.a;
    }

    public kl4 e(c cVar) {
        Objects.requireNonNull(cVar, "otsHashAddress == null");
        byte[][] bArr = new byte[this.a.c()];
        for (int i = 0; i < this.a.c(); i++) {
            cVar = (c) new c.b().g(cVar.b()).h(cVar.c()).p(cVar.g()).n(i).o(cVar.f()).f(cVar.a()).l();
            bArr[i] = a(b(i), 0, this.a.d() - 1, cVar);
        }
        return new kl4(this.a, bArr);
    }

    public byte[] f() {
        return i.c(this.d);
    }

    public byte[] g(byte[] bArr, c cVar) {
        return this.b.c(bArr, ((c) new c.b().g(cVar.b()).h(cVar.c()).p(cVar.g()).l()).d());
    }

    public void h(byte[] bArr, byte[] bArr2) {
        Objects.requireNonNull(bArr, "secretKeySeed == null");
        if (bArr.length != this.a.b()) {
            throw new IllegalArgumentException("size of secretKeySeed needs to be equal to size of digest");
        }
        Objects.requireNonNull(bArr2, "publicSeed == null");
        if (bArr2.length != this.a.b()) {
            throw new IllegalArgumentException("size of publicSeed needs to be equal to size of digest");
        }
        this.c = bArr;
        this.d = bArr2;
    }
}
