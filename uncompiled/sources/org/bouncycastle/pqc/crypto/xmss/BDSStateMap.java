package org.bouncycastle.pqc.crypto.xmss;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;
import org.bouncycastle.pqc.crypto.xmss.c;

/* loaded from: classes2.dex */
public class BDSStateMap implements Serializable {
    private final Map<Integer, BDS> bdsState = new TreeMap();

    public BDSStateMap() {
    }

    public BDSStateMap(BDSStateMap bDSStateMap, f fVar, long j, byte[] bArr, byte[] bArr2) {
        for (Integer num : bDSStateMap.bdsState.keySet()) {
            this.bdsState.put(num, bDSStateMap.bdsState.get(num));
        }
        a(fVar, j, bArr, bArr2);
    }

    public BDSStateMap(f fVar, long j, byte[] bArr, byte[] bArr2) {
        for (long j2 = 0; j2 < j; j2++) {
            a(fVar, j2, bArr, bArr2);
        }
    }

    public final void a(f fVar, long j, byte[] bArr, byte[] bArr2) {
        fs4 g = fVar.g();
        int d = g.d();
        long j2 = i.j(j, d);
        int i = i.i(j, d);
        c cVar = (c) new c.b().h(j2).p(i).l();
        int i2 = (1 << d) - 1;
        if (i < i2) {
            if (get(0) == null || i == 0) {
                put(0, new BDS(g, bArr, bArr2, cVar));
            }
            update(0, bArr, bArr2, cVar);
        }
        for (int i3 = 1; i3 < fVar.d(); i3++) {
            int i4 = i.i(j2, d);
            j2 = i.j(j2, d);
            c cVar2 = (c) new c.b().g(i3).h(j2).p(i4).l();
            if (i4 < i2 && i.m(j, d, i3)) {
                if (get(i3) == null) {
                    put(i3, new BDS(fVar.g(), bArr, bArr2, cVar2));
                }
                update(i3, bArr, bArr2, cVar2);
            }
        }
    }

    public BDS get(int i) {
        return this.bdsState.get(yr1.b(i));
    }

    public boolean isEmpty() {
        return this.bdsState.isEmpty();
    }

    public void put(int i, BDS bds) {
        this.bdsState.put(yr1.b(i), bds);
    }

    public void setXMSS(fs4 fs4Var) {
        for (Integer num : this.bdsState.keySet()) {
            BDS bds = this.bdsState.get(num);
            bds.setXMSS(fs4Var);
            bds.validate();
        }
    }

    public BDS update(int i, byte[] bArr, byte[] bArr2, c cVar) {
        return this.bdsState.put(yr1.b(i), this.bdsState.get(yr1.b(i)).getNextState(bArr, bArr2, cVar));
    }
}
