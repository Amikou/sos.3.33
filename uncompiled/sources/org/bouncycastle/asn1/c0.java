package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class c0 implements c4, d1 {
    public l a;

    public c0(l lVar) {
        this.a = lVar;
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new ASN1ParsingException("unable to get DER object", e);
        } catch (IllegalArgumentException e2) {
            throw new ASN1ParsingException("unable to get DER object", e2);
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        try {
            return new b0(this.a.d());
        } catch (IllegalArgumentException e) {
            throw new ASN1Exception(e.getMessage(), e);
        }
    }
}
