package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class c extends k {
    public static final byte[] f0 = {-1};
    public static final byte[] g0 = {0};
    public static final c h0 = new c(false);
    public static final c i0 = new c(true);
    public final byte[] a;

    public c(boolean z) {
        this.a = z ? f0 : g0;
    }

    public c(byte[] bArr) {
        if (bArr.length != 1) {
            throw new IllegalArgumentException("byte value should have 1 byte in it");
        }
        if (bArr[0] == 0) {
            this.a = g0;
        } else if ((bArr[0] & 255) == 255) {
            this.a = f0;
        } else {
            this.a = wh.e(bArr);
        }
    }

    public static c z(byte[] bArr) {
        if (bArr.length == 1) {
            return bArr[0] == 0 ? h0 : (bArr[0] & 255) == 255 ? i0 : new c(bArr);
        }
        throw new IllegalArgumentException("BOOLEAN value should have 1 byte in it");
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return this.a[0];
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        return (kVar instanceof c) && this.a[0] == ((c) kVar).a[0];
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(1, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return 3;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return this.a[0] != 0 ? "TRUE" : "FALSE";
    }
}
