package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

/* loaded from: classes2.dex */
public class t extends j4 {
    public t() {
    }

    public t(c4 c4Var) {
        super(c4Var);
    }

    public t(d4 d4Var) {
        super(d4Var, false);
    }

    public t(c4[] c4VarArr) {
        super(c4VarArr, false);
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.c(49);
        jVar.c(128);
        Enumeration F = F();
        while (F.hasMoreElements()) {
            jVar.j((c4) F.nextElement());
        }
        jVar.c(0);
        jVar.c(0);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        Enumeration F = F();
        int i = 0;
        while (F.hasMoreElements()) {
            i += ((c4) F.nextElement()).i().q();
        }
        return i + 2 + 2;
    }
}
