package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public abstract class a extends k {
    public final boolean a;
    public final int f0;
    public final byte[] g0;

    public a(boolean z, int i, byte[] bArr) {
        this.a = z;
        this.f0 = i;
        this.g0 = wh.e(bArr);
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        boolean z = this.a;
        return ((z ? 1 : 0) ^ this.f0) ^ wh.r(this.g0);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof a) {
            a aVar = (a) kVar;
            return this.a == aVar.a && this.f0 == aVar.f0 && wh.a(this.g0, aVar.g0);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.f(this.a ? 96 : 64, this.f0, this.g0);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        return g1.b(this.f0) + g1.a(this.g0.length) + this.g0.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return this.a;
    }

    public int z() {
        return this.f0;
    }
}
