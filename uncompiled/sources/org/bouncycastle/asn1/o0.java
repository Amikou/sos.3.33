package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class o0 implements i4 {
    public l a;

    public o0(l lVar) {
        this.a = lVar;
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        return new n0(this.a.d());
    }
}
