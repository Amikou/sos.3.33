package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class m extends k {
    public byte[] a;

    public m(byte[] bArr) {
        this.a = bArr;
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof m) {
            return wh.a(this.a, ((m) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.c(23);
        int length = this.a.length;
        jVar.i(length);
        for (int i = 0; i != length; i++) {
            jVar.c(this.a[i]);
        }
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        int length = this.a.length;
        return g1.a(length) + 1 + length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return su3.b(this.a);
    }
}
