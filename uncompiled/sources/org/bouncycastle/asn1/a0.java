package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class a0 extends b {
    public a0(c4 c4Var) throws IOException {
        super(c4Var.i().n("DER"), 0);
    }

    public a0(byte[] bArr) {
        this(bArr, 0);
    }

    public a0(byte[] bArr, int i) {
        super(bArr, i);
    }

    public static a0 H(Object obj) {
        if (obj == null || (obj instanceof a0)) {
            return (a0) obj;
        }
        if (obj instanceof x0) {
            x0 x0Var = (x0) obj;
            return new a0(x0Var.a, x0Var.f0);
        } else if (!(obj instanceof byte[])) {
            throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
        } else {
            try {
                return (a0) k.s((byte[]) obj);
            } catch (Exception e) {
                throw new IllegalArgumentException("encoding error in getInstance: " + e.toString());
            }
        }
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        byte[] z = b.z(this.a, this.f0);
        int length = z.length + 1;
        byte[] bArr = new byte[length];
        bArr[0] = (byte) F();
        System.arraycopy(z, 0, bArr, 1, length - 1);
        jVar.g(3, bArr);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length + 1) + 1 + this.a.length + 1;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }
}
