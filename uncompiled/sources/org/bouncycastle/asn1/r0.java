package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class r0 extends k {
    public byte[] a;

    public r0(byte[] bArr) {
        this.a = wh.e(bArr);
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof r0) {
            return wh.a(this.a, ((r0) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(20, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length) + 1 + this.a.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return z();
    }

    public String z() {
        return su3.b(this.a);
    }
}
