package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

/* loaded from: classes2.dex */
public class a1 extends j4 {
    public int g0;

    public a1() {
        this.g0 = -1;
    }

    public a1(c4 c4Var) {
        super(c4Var);
        this.g0 = -1;
    }

    public a1(d4 d4Var) {
        super(d4Var, false);
        this.g0 = -1;
    }

    public a1(c4[] c4VarArr) {
        super(c4VarArr, false);
        this.g0 = -1;
    }

    public final int J() throws IOException {
        if (this.g0 < 0) {
            int i = 0;
            Enumeration F = F();
            while (F.hasMoreElements()) {
                i += ((c4) F.nextElement()).i().y().q();
            }
            this.g0 = i;
        }
        return this.g0;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        j b = jVar.b();
        int J = J();
        jVar.c(49);
        jVar.i(J);
        Enumeration F = F();
        while (F.hasMoreElements()) {
            b.j((c4) F.nextElement());
        }
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        int J = J();
        return g1.a(J) + 1 + J;
    }
}
