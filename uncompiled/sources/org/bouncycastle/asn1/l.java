package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public class l {
    public final InputStream a;
    public final int b;
    public final byte[][] c;

    public l(InputStream inputStream) {
        this(inputStream, g1.c(inputStream));
    }

    public l(InputStream inputStream, int i) {
        this.a = inputStream;
        this.b = i;
        this.c = new byte[11];
    }

    public c4 a(int i) throws IOException {
        if (i != 4) {
            if (i != 8) {
                if (i != 16) {
                    if (i == 17) {
                        return new u(this);
                    }
                    throw new ASN1Exception("unknown BER object encountered: 0x" + Integer.toHexString(i));
                }
                return new s(this);
            }
            return new c0(this);
        }
        return new q(this);
    }

    public c4 b() throws IOException {
        int read = this.a.read();
        if (read == -1) {
            return null;
        }
        e(false);
        int l = f.l(this.a, read);
        boolean z = (read & 32) != 0;
        int i = f.i(this.a, this.b);
        if (i < 0) {
            if (z) {
                l lVar = new l(new e1(this.a, this.b), this.b);
                return (read & 64) != 0 ? new o(l, lVar) : (read & 128) != 0 ? new w(true, l, lVar) : lVar.a(l);
            }
            throw new IOException("indefinite-length primitive encoding encountered");
        }
        c1 c1Var = new c1(this.a, i);
        if ((read & 64) != 0) {
            return new y(z, l, c1Var.c());
        }
        if ((read & 128) != 0) {
            return new w(z, l, new l(c1Var));
        }
        if (!z) {
            if (l != 4) {
                try {
                    return f.d(l, c1Var, this.c);
                } catch (IllegalArgumentException e) {
                    throw new ASN1Exception("corrupted stream detected", e);
                }
            }
            return new k0(c1Var);
        } else if (l != 4) {
            if (l != 8) {
                if (l != 16) {
                    if (l == 17) {
                        return new q0(new l(c1Var));
                    }
                    throw new IOException("unknown tag " + l + " encountered");
                }
                return new o0(new l(c1Var));
            }
            return new c0(new l(c1Var));
        } else {
            return new q(new l(c1Var));
        }
    }

    public k c(boolean z, int i) throws IOException {
        if (z) {
            d4 d = d();
            return this.a instanceof e1 ? d.c() == 1 ? new v(true, i, d.b(0)) : new v(false, i, yl.a(d)) : d.c() == 1 ? new s0(true, i, d.b(0)) : new s0(false, i, ld0.a(d));
        }
        return new s0(false, i, new j0(((c1) this.a).c()));
    }

    public d4 d() throws IOException {
        d4 d4Var = new d4();
        while (true) {
            c4 b = b();
            if (b == null) {
                return d4Var;
            }
            d4Var.a(b instanceof d1 ? ((d1) b).k() : b.i());
        }
    }

    public final void e(boolean z) {
        InputStream inputStream = this.a;
        if (inputStream instanceof e1) {
            ((e1) inputStream).d(z);
        }
    }
}
