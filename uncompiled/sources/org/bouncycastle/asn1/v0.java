package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class v0 extends k {
    public final byte[] a;

    public v0(byte[] bArr) {
        this.a = wh.e(bArr);
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof v0) {
            return wh.a(this.a, ((v0) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(21, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length) + 1 + this.a.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }
}
