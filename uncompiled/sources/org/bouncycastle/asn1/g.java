package org.bouncycastle.asn1;

import java.io.IOException;
import java.math.BigInteger;

/* loaded from: classes2.dex */
public class g extends k {
    public final byte[] a;

    public g(long j) {
        this.a = BigInteger.valueOf(j).toByteArray();
    }

    public g(BigInteger bigInteger) {
        this.a = bigInteger.toByteArray();
    }

    public g(byte[] bArr, boolean z) {
        if (!aw2.c("org.bouncycastle.asn1.allow_unsafe_integer") && D(bArr)) {
            throw new IllegalArgumentException("malformed integer");
        }
        this.a = z ? wh.e(bArr) : bArr;
    }

    public static boolean D(byte[] bArr) {
        if (bArr.length > 1) {
            if (bArr[0] == 0 && (bArr[1] & 128) == 0) {
                return true;
            }
            if (bArr[0] == -1 && (bArr[1] & 128) != 0) {
                return true;
            }
        }
        return false;
    }

    public static g z(Object obj) {
        if (obj == null || (obj instanceof g)) {
            return (g) obj;
        }
        if (!(obj instanceof byte[])) {
            throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
        }
        try {
            return (g) k.s((byte[]) obj);
        } catch (Exception e) {
            throw new IllegalArgumentException("encoding error in getInstance: " + e.toString());
        }
    }

    public BigInteger B() {
        return new BigInteger(this.a);
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        int i = 0;
        int i2 = 0;
        while (true) {
            byte[] bArr = this.a;
            if (i == bArr.length) {
                return i2;
            }
            i2 ^= (bArr[i] & 255) << (i % 4);
            i++;
        }
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof g) {
            return wh.a(this.a, ((g) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(2, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length) + 1 + this.a.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return B().toString();
    }
}
