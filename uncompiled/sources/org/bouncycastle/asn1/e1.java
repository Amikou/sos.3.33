package org.bouncycastle.asn1;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public class e1 extends uz1 {
    public int g0;
    public int h0;
    public boolean i0;
    public boolean j0;

    public e1(InputStream inputStream, int i) throws IOException {
        super(inputStream, i);
        this.i0 = false;
        this.j0 = true;
        this.g0 = inputStream.read();
        int read = inputStream.read();
        this.h0 = read;
        if (read < 0) {
            throw new EOFException();
        }
        c();
    }

    public final boolean c() {
        if (!this.i0 && this.j0 && this.g0 == 0 && this.h0 == 0) {
            this.i0 = true;
            b(true);
        }
        return this.i0;
    }

    public void d(boolean z) {
        this.j0 = z;
        c();
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        if (c()) {
            return -1;
        }
        int read = this.a.read();
        if (read >= 0) {
            int i = this.g0;
            this.g0 = this.h0;
            this.h0 = read;
            return i;
        }
        throw new EOFException();
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (this.j0 || i2 < 3) {
            return super.read(bArr, i, i2);
        }
        if (this.i0) {
            return -1;
        }
        int read = this.a.read(bArr, i + 2, i2 - 2);
        if (read >= 0) {
            bArr[i] = (byte) this.g0;
            bArr[i + 1] = (byte) this.h0;
            this.g0 = this.a.read();
            int read2 = this.a.read();
            this.h0 = read2;
            if (read2 >= 0) {
                return read + 2;
            }
            throw new EOFException();
        }
        throw new EOFException();
    }
}
