package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class x0 extends b {
    public x0(byte[] bArr, int i) {
        super(bArr, i);
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        byte[] bArr = this.a;
        int length = bArr.length + 1;
        byte[] bArr2 = new byte[length];
        bArr2[0] = (byte) F();
        System.arraycopy(bArr, 0, bArr2, 1, length - 1);
        jVar.g(3, bArr2);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length + 1) + 1 + this.a.length + 1;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }
}
