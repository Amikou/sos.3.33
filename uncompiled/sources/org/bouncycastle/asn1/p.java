package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

/* loaded from: classes2.dex */
public class p extends f4 {
    public final int f0;
    public final f4[] g0;

    /* loaded from: classes2.dex */
    public class a implements Enumeration {
        public int a = 0;

        public a() {
        }

        @Override // java.util.Enumeration
        public boolean hasMoreElements() {
            return this.a < p.this.g0.length;
        }

        @Override // java.util.Enumeration
        public Object nextElement() {
            f4[] f4VarArr = p.this.g0;
            int i = this.a;
            this.a = i + 1;
            return f4VarArr[i];
        }
    }

    public p(byte[] bArr) {
        this(bArr, 1000);
    }

    public p(byte[] bArr, int i) {
        this(bArr, null, i);
    }

    public p(byte[] bArr, f4[] f4VarArr, int i) {
        super(bArr);
        this.g0 = f4VarArr;
        this.f0 = i;
    }

    public p(f4[] f4VarArr) {
        this(f4VarArr, 1000);
    }

    public p(f4[] f4VarArr, int i) {
        this(I(f4VarArr), f4VarArr, i);
    }

    public static p F(h4 h4Var) {
        f4[] f4VarArr = new f4[h4Var.size()];
        Enumeration E = h4Var.E();
        int i = 0;
        while (E.hasMoreElements()) {
            f4VarArr[i] = (f4) E.nextElement();
            i++;
        }
        return new p(f4VarArr);
    }

    public static byte[] I(f4[] f4VarArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i != f4VarArr.length; i++) {
            try {
                byteArrayOutputStream.write(((j0) f4VarArr[i]).D());
            } catch (IOException e) {
                throw new IllegalArgumentException("exception converting octets " + e.toString());
            } catch (ClassCastException unused) {
                throw new IllegalArgumentException(f4VarArr[i].getClass().getName() + " found in input should only contain DEROctetString");
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Override // defpackage.f4
    public byte[] D() {
        return this.a;
    }

    public final Vector G() {
        Vector vector = new Vector();
        int i = 0;
        while (true) {
            byte[] bArr = this.a;
            if (i >= bArr.length) {
                return vector;
            }
            int i2 = this.f0;
            int length = (i + i2 > bArr.length ? bArr.length : i2 + i) - i;
            byte[] bArr2 = new byte[length];
            System.arraycopy(bArr, i, bArr2, 0, length);
            vector.addElement(new j0(bArr2));
            i += this.f0;
        }
    }

    public Enumeration H() {
        return this.g0 == null ? G().elements() : new a();
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.c(36);
        jVar.c(128);
        Enumeration H = H();
        while (H.hasMoreElements()) {
            jVar.j((c4) H.nextElement());
        }
        jVar.c(0);
        jVar.c(0);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        Enumeration H = H();
        int i = 0;
        while (H.hasMoreElements()) {
            i += ((c4) H.nextElement()).i().q();
        }
        return i + 2 + 2;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return true;
    }
}
