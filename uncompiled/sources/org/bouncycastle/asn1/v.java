package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

/* loaded from: classes2.dex */
public class v extends k4 {
    public v(boolean z, int i, c4 c4Var) {
        super(z, i, c4Var);
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        Enumeration F;
        jVar.k(160, this.a);
        jVar.c(128);
        if (!this.f0) {
            if (this.g0) {
                jVar.j(this.h0);
            } else {
                c4 c4Var = this.h0;
                if (c4Var instanceof f4) {
                    F = c4Var instanceof p ? ((p) c4Var).H() : new p(((f4) c4Var).D()).H();
                } else if (c4Var instanceof h4) {
                    F = ((h4) c4Var).E();
                } else if (!(c4Var instanceof j4)) {
                    throw new ASN1Exception("not implemented: " + this.h0.getClass().getName());
                } else {
                    F = ((j4) c4Var).F();
                }
                while (F.hasMoreElements()) {
                    jVar.j((c4) F.nextElement());
                }
            }
        }
        jVar.c(0);
        jVar.c(0);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        int b;
        if (this.f0) {
            return g1.b(this.a) + 1;
        }
        int q = this.h0.i().q();
        if (this.g0) {
            b = g1.b(this.a) + g1.a(q);
        } else {
            q--;
            b = g1.b(this.a);
        }
        return b + q;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        if (this.f0 || this.g0) {
            return true;
        }
        return this.h0.i().w().t();
    }
}
