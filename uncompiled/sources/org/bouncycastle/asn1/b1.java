package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class b1 extends k4 {
    public static final byte[] i0 = new byte[0];

    public b1(boolean z, int i, c4 c4Var) {
        super(z, i, c4Var);
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        if (this.f0) {
            jVar.f(160, this.a, i0);
            return;
        }
        k y = this.h0.i().y();
        if (!this.g0) {
            jVar.k(y.t() ? 160 : 128, this.a);
            jVar.h(y);
            return;
        }
        jVar.k(160, this.a);
        jVar.i(y.q());
        jVar.j(y);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        int b;
        if (this.f0) {
            return g1.b(this.a) + 1;
        }
        int q = this.h0.i().y().q();
        if (this.g0) {
            b = g1.b(this.a) + g1.a(q);
        } else {
            q--;
            b = g1.b(this.a);
        }
        return b + q;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        if (this.f0 || this.g0) {
            return true;
        }
        return this.h0.i().y().t();
    }
}
