package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class w implements c4, d1 {
    public boolean a;
    public int f0;
    public l g0;

    public w(boolean z, int i, l lVar) {
        this.a = z;
        this.f0 = i;
        this.g0 = lVar;
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new ASN1ParsingException(e.getMessage());
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        return this.g0.c(this.a, this.f0);
    }
}
