package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class d extends k {
    public static d[] f0 = new d[12];
    public final byte[] a;

    public d(byte[] bArr) {
        if (!aw2.c("org.bouncycastle.asn1.allow_unsafe_integer") && g.D(bArr)) {
            throw new IllegalArgumentException("malformed enumerated");
        }
        this.a = wh.e(bArr);
    }

    public static d z(byte[] bArr) {
        if (bArr.length > 1) {
            return new d(bArr);
        }
        if (bArr.length != 0) {
            int i = bArr[0] & 255;
            d[] dVarArr = f0;
            if (i >= dVarArr.length) {
                return new d(wh.e(bArr));
            }
            d dVar = dVarArr[i];
            if (dVar == null) {
                d dVar2 = new d(wh.e(bArr));
                dVarArr[i] = dVar2;
                return dVar2;
            }
            return dVar;
        }
        throw new IllegalArgumentException("ENUMERATED has zero length");
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof d) {
            return wh.a(this.a, ((d) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(10, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length) + 1 + this.a.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }
}
