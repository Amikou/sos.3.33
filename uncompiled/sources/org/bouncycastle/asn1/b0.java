package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* loaded from: classes2.dex */
public class b0 extends k {
    public i a;
    public g f0;
    public k g0;
    public int h0;
    public k i0;

    public b0(d4 d4Var) {
        int i = 0;
        k z = z(d4Var, 0);
        if (z instanceof i) {
            this.a = (i) z;
            z = z(d4Var, 1);
            i = 1;
        }
        if (z instanceof g) {
            this.f0 = (g) z;
            i++;
            z = z(d4Var, i);
        }
        if (!(z instanceof k4)) {
            this.g0 = z;
            i++;
            z = z(d4Var, i);
        }
        if (d4Var.c() != i + 1) {
            throw new IllegalArgumentException("input vector too large");
        }
        if (!(z instanceof k4)) {
            throw new IllegalArgumentException("No tagged object found in vector. Structure doesn't seem to be of type External");
        }
        k4 k4Var = (k4) z;
        B(k4Var.D());
        this.i0 = k4Var.B();
    }

    public final void B(int i) {
        if (i >= 0 && i <= 2) {
            this.h0 = i;
            return;
        }
        throw new IllegalArgumentException("invalid encoding value: " + i);
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        i iVar = this.a;
        int hashCode = iVar != null ? iVar.hashCode() : 0;
        g gVar = this.f0;
        if (gVar != null) {
            hashCode ^= gVar.hashCode();
        }
        k kVar = this.g0;
        if (kVar != null) {
            hashCode ^= kVar.hashCode();
        }
        return hashCode ^ this.i0.hashCode();
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        k kVar2;
        g gVar;
        i iVar;
        if (kVar instanceof b0) {
            if (this == kVar) {
                return true;
            }
            b0 b0Var = (b0) kVar;
            i iVar2 = this.a;
            if (iVar2 == null || ((iVar = b0Var.a) != null && iVar.equals(iVar2))) {
                g gVar2 = this.f0;
                if (gVar2 == null || ((gVar = b0Var.f0) != null && gVar.equals(gVar2))) {
                    k kVar3 = this.g0;
                    if (kVar3 == null || ((kVar2 = b0Var.g0) != null && kVar2.equals(kVar3))) {
                        return this.i0.equals(b0Var.i0);
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        i iVar = this.a;
        if (iVar != null) {
            byteArrayOutputStream.write(iVar.n("DER"));
        }
        g gVar = this.f0;
        if (gVar != null) {
            byteArrayOutputStream.write(gVar.n("DER"));
        }
        k kVar = this.g0;
        if (kVar != null) {
            byteArrayOutputStream.write(kVar.n("DER"));
        }
        byteArrayOutputStream.write(new s0(true, this.h0, this.i0).n("DER"));
        jVar.f(32, 8, byteArrayOutputStream.toByteArray());
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        return m().length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return true;
    }

    public final k z(d4 d4Var, int i) {
        if (d4Var.c() > i) {
            return d4Var.b(i).i();
        }
        throw new IllegalArgumentException("too few objects in input vector");
    }
}
