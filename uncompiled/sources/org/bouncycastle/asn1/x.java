package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public class x extends InputStream {
    public final l a;
    public boolean f0 = true;
    public InputStream g0;

    public x(l lVar) {
        this.a = lVar;
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        g4 g4Var;
        if (this.g0 == null) {
            if (!this.f0 || (g4Var = (g4) this.a.b()) == null) {
                return -1;
            }
            this.f0 = false;
            this.g0 = g4Var.e();
        }
        while (true) {
            int read = this.g0.read();
            if (read >= 0) {
                return read;
            }
            g4 g4Var2 = (g4) this.a.b();
            if (g4Var2 == null) {
                this.g0 = null;
                return -1;
            }
            this.g0 = g4Var2.e();
        }
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        g4 g4Var;
        int i3 = 0;
        if (this.g0 == null) {
            if (!this.f0 || (g4Var = (g4) this.a.b()) == null) {
                return -1;
            }
            this.f0 = false;
            this.g0 = g4Var.e();
        }
        while (true) {
            int read = this.g0.read(bArr, i + i3, i2 - i3);
            if (read >= 0) {
                i3 += read;
                if (i3 == i2) {
                    return i3;
                }
            } else {
                g4 g4Var2 = (g4) this.a.b();
                if (g4Var2 == null) {
                    this.g0 = null;
                    if (i3 < 1) {
                        return -1;
                    }
                    return i3;
                }
                this.g0 = g4Var2.e();
            }
        }
    }
}
