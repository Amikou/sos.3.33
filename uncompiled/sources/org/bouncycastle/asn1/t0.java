package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class t0 extends k {
    public final byte[] a;

    public t0(byte[] bArr) {
        this.a = bArr;
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof t0) {
            return wh.a(this.a, ((t0) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(12, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        return g1.a(this.a.length) + 1 + this.a.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return z();
    }

    public String z() {
        return su3.c(this.a);
    }
}
