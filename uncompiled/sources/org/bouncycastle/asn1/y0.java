package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.OutputStream;

/* loaded from: classes2.dex */
public class y0 extends j {
    public y0(OutputStream outputStream) {
        super(outputStream);
    }

    @Override // org.bouncycastle.asn1.j
    public void j(c4 c4Var) throws IOException {
        if (c4Var == null) {
            throw new IOException("null object detected");
        }
        c4Var.i().y().p(this);
    }
}
