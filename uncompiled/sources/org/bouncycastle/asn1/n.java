package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* loaded from: classes2.dex */
public class n extends a {
    public n(int i, d4 d4Var) {
        super(true, i, B(d4Var));
    }

    public static byte[] B(d4 d4Var) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i != d4Var.c(); i++) {
            try {
                byteArrayOutputStream.write(((h) d4Var.b(i)).n("BER"));
            } catch (IOException e) {
                throw new ASN1ParsingException("malformed object: " + e, e);
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Override // org.bouncycastle.asn1.a, org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.k(this.a ? 96 : 64, this.f0);
        jVar.c(128);
        jVar.d(this.g0);
        jVar.c(0);
        jVar.c(0);
    }
}
