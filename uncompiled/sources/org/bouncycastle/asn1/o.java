package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class o implements c4, d1 {
    public final int a;
    public final l f0;

    public o(int i, l lVar) {
        this.a = i;
        this.f0 = lVar;
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new ASN1ParsingException(e.getMessage(), e);
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        return new n(this.a, this.f0.d());
    }
}
