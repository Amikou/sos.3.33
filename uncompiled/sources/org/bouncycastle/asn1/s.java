package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class s implements i4 {
    public l a;

    public s(l lVar) {
        this.a = lVar;
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        return new r(this.a.d());
    }
}
