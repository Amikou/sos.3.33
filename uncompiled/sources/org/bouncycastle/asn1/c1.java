package org.bouncycastle.asn1;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public class c1 extends uz1 {
    public static final byte[] i0 = new byte[0];
    public final int g0;
    public int h0;

    public c1(InputStream inputStream, int i) {
        super(inputStream, i);
        if (i < 0) {
            throw new IllegalArgumentException("negative lengths not allowed");
        }
        this.g0 = i;
        this.h0 = i;
        if (i == 0) {
            b(true);
        }
    }

    @Override // defpackage.uz1
    public int a() {
        return this.h0;
    }

    public byte[] c() throws IOException {
        int i = this.h0;
        if (i == 0) {
            return i0;
        }
        byte[] bArr = new byte[i];
        int c = i - iu3.c(this.a, bArr);
        this.h0 = c;
        if (c == 0) {
            b(true);
            return bArr;
        }
        throw new EOFException("DEF length " + this.g0 + " object truncated by " + this.h0);
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        if (this.h0 == 0) {
            return -1;
        }
        int read = this.a.read();
        if (read >= 0) {
            int i = this.h0 - 1;
            this.h0 = i;
            if (i == 0) {
                b(true);
            }
            return read;
        }
        throw new EOFException("DEF length " + this.g0 + " object truncated by " + this.h0);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int i3 = this.h0;
        if (i3 == 0) {
            return -1;
        }
        int read = this.a.read(bArr, i, Math.min(i2, i3));
        if (read >= 0) {
            int i4 = this.h0 - read;
            this.h0 = i4;
            if (i4 == 0) {
                b(true);
            }
            return read;
        }
        throw new EOFException("DEF length " + this.g0 + " object truncated by " + this.h0);
    }
}
