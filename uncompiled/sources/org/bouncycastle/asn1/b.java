package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/* loaded from: classes2.dex */
public abstract class b extends k {
    public static final char[] g0 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    public final byte[] a;
    public final int f0;

    public b(byte[] bArr, int i) {
        Objects.requireNonNull(bArr, "data cannot be null");
        if (bArr.length == 0 && i != 0) {
            throw new IllegalArgumentException("zero length data with non-zero pad bits");
        }
        if (i > 7 || i < 0) {
            throw new IllegalArgumentException("pad bits cannot be greater than 7 or less than 0");
        }
        this.a = wh.e(bArr);
        this.f0 = i;
    }

    public static b B(int i, InputStream inputStream) throws IOException {
        if (i >= 1) {
            int read = inputStream.read();
            int i2 = i - 1;
            byte[] bArr = new byte[i2];
            if (i2 != 0) {
                if (iu3.c(inputStream, bArr) != i2) {
                    throw new EOFException("EOF encountered in middle of BIT STRING");
                }
                if (read > 0 && read < 8) {
                    int i3 = i2 - 1;
                    if (bArr[i3] != ((byte) (bArr[i3] & (255 << read)))) {
                        return new x0(bArr, read);
                    }
                }
            }
            return new a0(bArr, read);
        }
        throw new IllegalArgumentException("truncated BIT STRING detected");
    }

    public static byte[] z(byte[] bArr, int i) {
        byte[] e = wh.e(bArr);
        if (i > 0) {
            int length = bArr.length - 1;
            e[length] = (byte) ((255 << i) & e[length]);
        }
        return e;
    }

    public byte[] D() {
        return z(this.a, this.f0);
    }

    public byte[] E() {
        if (this.f0 == 0) {
            return wh.e(this.a);
        }
        throw new IllegalStateException("attempt to get non-octet aligned data from BIT STRING");
    }

    public int F() {
        return this.f0;
    }

    public String G() {
        StringBuffer stringBuffer = new StringBuffer("#");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            new j(byteArrayOutputStream).j(this);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            for (int i = 0; i != byteArray.length; i++) {
                char[] cArr = g0;
                stringBuffer.append(cArr[(byteArray[i] >>> 4) & 15]);
                stringBuffer.append(cArr[byteArray[i] & 15]);
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            throw new ASN1ParsingException("Internal error encoding BitString: " + e.getMessage(), e);
        }
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return this.f0 ^ wh.r(D());
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof b) {
            b bVar = (b) kVar;
            return this.f0 == bVar.f0 && wh.a(D(), bVar.D());
        }
        return false;
    }

    public String toString() {
        return G();
    }

    @Override // org.bouncycastle.asn1.k
    public k w() {
        return new a0(this.a, this.f0);
    }

    @Override // org.bouncycastle.asn1.k
    public k y() {
        return new x0(this.a, this.f0);
    }
}
