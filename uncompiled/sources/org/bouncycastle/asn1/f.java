package org.bouncycastle.asn1;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public class f extends FilterInputStream {
    public final int a;
    public final boolean f0;
    public final byte[][] g0;

    public f(InputStream inputStream) {
        this(inputStream, g1.c(inputStream));
    }

    public f(InputStream inputStream, int i) {
        this(inputStream, i, false);
    }

    public f(InputStream inputStream, int i, boolean z) {
        super(inputStream);
        this.a = i;
        this.f0 = z;
        this.g0 = new byte[11];
    }

    public f(byte[] bArr) {
        this(new ByteArrayInputStream(bArr), bArr.length);
    }

    public f(byte[] bArr, boolean z) {
        this(new ByteArrayInputStream(bArr), bArr.length, z);
    }

    public static k d(int i, c1 c1Var, byte[][] bArr) throws IOException {
        if (i != 10) {
            if (i != 12) {
                if (i != 30) {
                    switch (i) {
                        case 1:
                            return c.z(f(c1Var, bArr));
                        case 2:
                            return new g(c1Var.c(), false);
                        case 3:
                            return b.B(c1Var.a(), c1Var);
                        case 4:
                            return new j0(c1Var.c());
                        case 5:
                            return h0.a;
                        case 6:
                            return i.D(f(c1Var, bArr));
                        default:
                            switch (i) {
                                case 18:
                                    return new i0(c1Var.c());
                                case 19:
                                    return new m0(c1Var.c());
                                case 20:
                                    return new r0(c1Var.c());
                                case 21:
                                    return new v0(c1Var.c());
                                case 22:
                                    return new g0(c1Var.c());
                                case 23:
                                    return new m(c1Var.c());
                                case 24:
                                    return new e(c1Var.c());
                                case 25:
                                    return new f0(c1Var.c());
                                case 26:
                                    return new w0(c1Var.c());
                                case 27:
                                    return new d0(c1Var.c());
                                case 28:
                                    return new u0(c1Var.c());
                                default:
                                    throw new IOException("unknown tag " + i + " encountered");
                            }
                    }
                }
                return new z(e(c1Var));
            }
            return new t0(c1Var.c());
        }
        return d.z(f(c1Var, bArr));
    }

    public static char[] e(c1 c1Var) throws IOException {
        int read;
        int a = c1Var.a() / 2;
        char[] cArr = new char[a];
        for (int i = 0; i < a; i++) {
            int read2 = c1Var.read();
            if (read2 < 0 || (read = c1Var.read()) < 0) {
                break;
            }
            cArr[i] = (char) ((read2 << 8) | (read & 255));
        }
        return cArr;
    }

    public static byte[] f(c1 c1Var, byte[][] bArr) throws IOException {
        int a = c1Var.a();
        if (c1Var.a() < bArr.length) {
            byte[] bArr2 = bArr[a];
            if (bArr2 == null) {
                bArr2 = new byte[a];
                bArr[a] = bArr2;
            }
            iu3.c(c1Var, bArr2);
            return bArr2;
        }
        return c1Var.c();
    }

    public static int i(InputStream inputStream, int i) throws IOException {
        int read = inputStream.read();
        if (read >= 0) {
            if (read == 128) {
                return -1;
            }
            if (read > 127) {
                int i2 = read & 127;
                if (i2 > 4) {
                    throw new IOException("DER length more than 4 bytes: " + i2);
                }
                int i3 = 0;
                for (int i4 = 0; i4 < i2; i4++) {
                    int read2 = inputStream.read();
                    if (read2 < 0) {
                        throw new EOFException("EOF found reading length");
                    }
                    i3 = (i3 << 8) + read2;
                }
                if (i3 >= 0) {
                    if (i3 < i) {
                        return i3;
                    }
                    throw new IOException("corrupted stream - out of bounds length found");
                }
                throw new IOException("corrupted stream - negative length found");
            }
            return read;
        }
        throw new EOFException("EOF found when length expected");
    }

    public static int l(InputStream inputStream, int i) throws IOException {
        int i2 = i & 31;
        if (i2 == 31) {
            int i3 = 0;
            int read = inputStream.read();
            if ((read & 127) != 0) {
                while (read >= 0 && (read & 128) != 0) {
                    i3 = (i3 | (read & 127)) << 7;
                    read = inputStream.read();
                }
                if (read >= 0) {
                    return i3 | (read & 127);
                }
                throw new EOFException("EOF found inside tag value.");
            }
            throw new IOException("corrupted stream - invalid high tag number found");
        }
        return i2;
    }

    public d4 a(c1 c1Var) throws IOException {
        return new f(c1Var).b();
    }

    public d4 b() throws IOException {
        d4 d4Var = new d4();
        while (true) {
            k j = j();
            if (j == null) {
                return d4Var;
            }
            d4Var.a(j);
        }
    }

    public k c(int i, int i2, int i3) throws IOException {
        boolean z = (i & 32) != 0;
        c1 c1Var = new c1(this, i3);
        if ((i & 64) != 0) {
            return new y(z, i2, c1Var.c());
        }
        if ((i & 128) != 0) {
            return new l(c1Var).c(z, i2);
        }
        if (z) {
            if (i2 == 4) {
                d4 a = a(c1Var);
                int c = a.c();
                f4[] f4VarArr = new f4[c];
                for (int i4 = 0; i4 != c; i4++) {
                    f4VarArr[i4] = (f4) a.b(i4);
                }
                return new p(f4VarArr);
            } else if (i2 != 8) {
                if (i2 == 16) {
                    return this.f0 ? new f1(c1Var.c()) : ld0.a(a(c1Var));
                } else if (i2 == 17) {
                    return ld0.b(a(c1Var));
                } else {
                    throw new IOException("unknown tag " + i2 + " encountered");
                }
            } else {
                return new b0(a(c1Var));
            }
        }
        return d(i2, c1Var, this.g0);
    }

    public int g() {
        return this.a;
    }

    public int h() throws IOException {
        return i(this, this.a);
    }

    public k j() throws IOException {
        int read = read();
        if (read <= 0) {
            if (read != 0) {
                return null;
            }
            throw new IOException("unexpected end-of-contents marker");
        }
        int l = l(this, read);
        boolean z = (read & 32) != 0;
        int h = h();
        if (h >= 0) {
            try {
                return c(read, l, h);
            } catch (IllegalArgumentException e) {
                throw new ASN1Exception("corrupted stream detected", e);
            }
        } else if (z) {
            l lVar = new l(new e1(this, this.a), this.a);
            if ((read & 64) != 0) {
                return new o(l, lVar).k();
            }
            if ((read & 128) != 0) {
                return new w(true, l, lVar).k();
            }
            if (l != 4) {
                if (l != 8) {
                    if (l != 16) {
                        if (l == 17) {
                            return new u(lVar).k();
                        }
                        throw new IOException("unknown BER object encountered");
                    }
                    return new s(lVar).k();
                }
                return new c0(lVar).k();
            }
            return new q(lVar).k();
        } else {
            throw new IOException("indefinite-length primitive encoding encountered");
        }
    }
}
