package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public class k0 implements g4 {
    public c1 a;

    public k0(c1 c1Var) {
        this.a = c1Var;
    }

    @Override // defpackage.g4
    public InputStream e() {
        return this.a;
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new ASN1ParsingException("IOException converting stream to byte array: " + e.getMessage(), e);
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        return new j0(this.a.c());
    }
}
