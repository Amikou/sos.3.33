package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* loaded from: classes2.dex */
public class i extends k {
    public static final ConcurrentMap<a, i> g0 = new ConcurrentHashMap();
    public final String a;
    public byte[] f0;

    /* loaded from: classes2.dex */
    public static class a {
        public final int a;
        public final byte[] b;

        public a(byte[] bArr) {
            this.a = wh.r(bArr);
            this.b = bArr;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                return wh.a(this.b, ((a) obj).b);
            }
            return false;
        }

        public int hashCode() {
            return this.a;
        }
    }

    public i(String str) {
        if (str == null) {
            throw new IllegalArgumentException("'identifier' cannot be null");
        }
        if (I(str)) {
            this.a = str;
            return;
        }
        throw new IllegalArgumentException("string " + str + " not an OID");
    }

    public i(i iVar, String str) {
        if (!H(str, 0)) {
            throw new IllegalArgumentException("string " + str + " not a valid OID branch");
        }
        this.a = iVar.F() + "." + str;
    }

    public i(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = true;
        long j = 0;
        BigInteger bigInteger = null;
        for (int i = 0; i != bArr.length; i++) {
            int i2 = bArr[i] & 255;
            if (j <= 72057594037927808L) {
                long j2 = j + (i2 & 127);
                if ((i2 & 128) == 0) {
                    if (z) {
                        if (j2 < 40) {
                            stringBuffer.append('0');
                        } else if (j2 < 80) {
                            stringBuffer.append('1');
                            j2 -= 40;
                        } else {
                            stringBuffer.append('2');
                            j2 -= 80;
                        }
                        z = false;
                    }
                    stringBuffer.append('.');
                    stringBuffer.append(j2);
                    j = 0;
                } else {
                    j = j2 << 7;
                }
            } else {
                BigInteger or = (bigInteger == null ? BigInteger.valueOf(j) : bigInteger).or(BigInteger.valueOf(i2 & 127));
                if ((i2 & 128) == 0) {
                    if (z) {
                        stringBuffer.append('2');
                        or = or.subtract(BigInteger.valueOf(80L));
                        z = false;
                    }
                    stringBuffer.append('.');
                    stringBuffer.append(or);
                    j = 0;
                    bigInteger = null;
                } else {
                    bigInteger = or.shiftLeft(7);
                }
            }
        }
        this.a = stringBuffer.toString();
        this.f0 = wh.e(bArr);
    }

    public static i D(byte[] bArr) {
        i iVar = g0.get(new a(bArr));
        return iVar == null ? new i(bArr) : iVar;
    }

    public static i G(Object obj) {
        if (obj == null || (obj instanceof i)) {
            return (i) obj;
        }
        if (obj instanceof c4) {
            c4 c4Var = (c4) obj;
            if (c4Var.i() instanceof i) {
                return (i) c4Var.i();
            }
        }
        if (!(obj instanceof byte[])) {
            throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
        }
        try {
            return (i) k.s((byte[]) obj);
        } catch (IOException e) {
            throw new IllegalArgumentException("failed to construct object identifier from byte[]: " + e.getMessage());
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x001a, code lost:
        if (r3 != '.') goto L17;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean H(java.lang.String r5, int r6) {
        /*
            int r0 = r5.length()
            r1 = 0
        L5:
            r2 = r1
        L6:
            int r0 = r0 + (-1)
            if (r0 < r6) goto L1f
            char r3 = r5.charAt(r0)
            r4 = 48
            if (r4 > r3) goto L18
            r4 = 57
            if (r3 > r4) goto L18
            r2 = 1
            goto L6
        L18:
            r4 = 46
            if (r3 != r4) goto L1e
            if (r2 != 0) goto L5
        L1e:
            return r1
        L1f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.asn1.i.H(java.lang.String, int):boolean");
    }

    public static boolean I(String str) {
        char charAt;
        if (str.length() < 3 || str.charAt(1) != '.' || (charAt = str.charAt(0)) < '0' || charAt > '2') {
            return false;
        }
        return H(str, 2);
    }

    public final void B(ByteArrayOutputStream byteArrayOutputStream) {
        gj2 gj2Var = new gj2(this.a);
        int parseInt = Integer.parseInt(gj2Var.b()) * 40;
        String b = gj2Var.b();
        if (b.length() <= 18) {
            J(byteArrayOutputStream, parseInt + Long.parseLong(b));
        } else {
            K(byteArrayOutputStream, new BigInteger(b).add(BigInteger.valueOf(parseInt)));
        }
        while (gj2Var.a()) {
            String b2 = gj2Var.b();
            if (b2.length() <= 18) {
                J(byteArrayOutputStream, Long.parseLong(b2));
            } else {
                K(byteArrayOutputStream, new BigInteger(b2));
            }
        }
    }

    public final synchronized byte[] E() {
        if (this.f0 == null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            B(byteArrayOutputStream);
            this.f0 = byteArrayOutputStream.toByteArray();
        }
        return this.f0;
    }

    public String F() {
        return this.a;
    }

    public final void J(ByteArrayOutputStream byteArrayOutputStream, long j) {
        byte[] bArr = new byte[9];
        int i = 8;
        bArr[8] = (byte) (((int) j) & 127);
        while (j >= 128) {
            j >>= 7;
            i--;
            bArr[i] = (byte) ((((int) j) & 127) | 128);
        }
        byteArrayOutputStream.write(bArr, i, 9 - i);
    }

    public final void K(ByteArrayOutputStream byteArrayOutputStream, BigInteger bigInteger) {
        int bitLength = (bigInteger.bitLength() + 6) / 7;
        if (bitLength == 0) {
            byteArrayOutputStream.write(0);
            return;
        }
        byte[] bArr = new byte[bitLength];
        int i = bitLength - 1;
        for (int i2 = i; i2 >= 0; i2--) {
            bArr[i2] = (byte) ((bigInteger.intValue() & 127) | 128);
            bigInteger = bigInteger.shiftRight(7);
        }
        bArr[i] = (byte) (bArr[i] & Byte.MAX_VALUE);
        byteArrayOutputStream.write(bArr, 0, bitLength);
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar == this) {
            return true;
        }
        if (kVar instanceof i) {
            return this.a.equals(((i) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        byte[] E = E();
        jVar.c(6);
        jVar.i(E.length);
        jVar.d(E);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        int length = E().length;
        return g1.a(length) + 1 + length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return F();
    }

    public i z(String str) {
        return new i(this, str);
    }
}
