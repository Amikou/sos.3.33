package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public abstract class k extends h {
    public static k s(byte[] bArr) throws IOException {
        f fVar = new f(bArr);
        try {
            k j = fVar.j();
            if (fVar.available() == 0) {
                return j;
            }
            throw new IOException("Extra data detected in stream");
        } catch (ClassCastException unused) {
            throw new IOException("cannot recognise object in stream");
        }
    }

    @Override // org.bouncycastle.asn1.h
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof c4) && o(((c4) obj).i());
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        return this;
    }

    public abstract boolean o(k kVar);

    public abstract void p(j jVar) throws IOException;

    public abstract int q() throws IOException;

    public abstract boolean t();

    public k w() {
        return this;
    }

    public k y() {
        return this;
    }
}
