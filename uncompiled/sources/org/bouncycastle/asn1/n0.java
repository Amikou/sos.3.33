package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

/* loaded from: classes2.dex */
public class n0 extends h4 {
    public int f0;

    public n0() {
        this.f0 = -1;
    }

    public n0(d4 d4Var) {
        super(d4Var);
        this.f0 = -1;
    }

    public final int G() throws IOException {
        if (this.f0 < 0) {
            int i = 0;
            Enumeration E = E();
            while (E.hasMoreElements()) {
                i += ((c4) E.nextElement()).i().w().q();
            }
            this.f0 = i;
        }
        return this.f0;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        j a = jVar.a();
        int G = G();
        jVar.c(48);
        jVar.i(G);
        Enumeration E = E();
        while (E.hasMoreElements()) {
            a.j((c4) E.nextElement());
        }
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        int G = G();
        return g1.a(G) + 1 + G;
    }
}
