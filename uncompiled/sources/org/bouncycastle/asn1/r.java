package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

/* loaded from: classes2.dex */
public class r extends h4 {
    public r() {
    }

    public r(d4 d4Var) {
        super(d4Var);
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.c(48);
        jVar.c(128);
        Enumeration E = E();
        while (E.hasMoreElements()) {
            jVar.j((c4) E.nextElement());
        }
        jVar.c(0);
        jVar.c(0);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        Enumeration E = E();
        int i = 0;
        while (E.hasMoreElements()) {
            i += ((c4) E.nextElement()).i().q();
        }
        return i + 2 + 2;
    }
}
