package org.bouncycastle.asn1;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.IOException;

/* loaded from: classes2.dex */
public class y extends a {
    public y(boolean z, int i, byte[] bArr) {
        super(z, i, bArr);
    }

    @Override // org.bouncycastle.asn1.a, org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.f(this.a ? 96 : 64, this.f0, this.g0);
    }

    public String toString() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        if (t()) {
            stringBuffer.append("CONSTRUCTED ");
        }
        stringBuffer.append("APPLICATION ");
        stringBuffer.append(Integer.toString(z()));
        stringBuffer.append("]");
        if (this.g0 != null) {
            stringBuffer.append(" #");
            str = pk1.d(this.g0);
        } else {
            str = " #null";
        }
        stringBuffer.append(str);
        stringBuffer.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        return stringBuffer.toString();
    }
}
