package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

/* loaded from: classes2.dex */
public class p0 extends j4 {
    public int g0;

    public p0() {
        this.g0 = -1;
    }

    public p0(d4 d4Var, boolean z) {
        super(d4Var, z);
        this.g0 = -1;
    }

    public final int J() throws IOException {
        if (this.g0 < 0) {
            int i = 0;
            Enumeration F = F();
            while (F.hasMoreElements()) {
                i += ((c4) F.nextElement()).i().w().q();
            }
            this.g0 = i;
        }
        return this.g0;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        j a = jVar.a();
        int J = J();
        jVar.c(49);
        jVar.i(J);
        Enumeration F = F();
        while (F.hasMoreElements()) {
            a.j((c4) F.nextElement());
        }
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        int J = J();
        return g1.a(J) + 1 + J;
    }
}
