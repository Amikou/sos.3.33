package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public class q implements g4 {
    public l a;

    public q(l lVar) {
        this.a = lVar;
    }

    @Override // defpackage.g4
    public InputStream e() {
        return new x(this.a);
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new ASN1ParsingException("IOException converting stream to byte array: " + e.getMessage(), e);
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        return new p(iu3.b(e()));
    }
}
