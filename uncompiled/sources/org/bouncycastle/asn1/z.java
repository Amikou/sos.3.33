package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class z extends k {
    public final char[] a;

    public z(char[] cArr) {
        this.a = cArr;
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.s(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof z) {
            return wh.b(this.a, ((z) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.c(30);
        jVar.i(this.a.length * 2);
        int i = 0;
        while (true) {
            char[] cArr = this.a;
            if (i == cArr.length) {
                return;
            }
            char c = cArr[i];
            jVar.c((byte) (c >> '\b'));
            jVar.c((byte) c);
            i++;
        }
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length * 2) + 1 + (this.a.length * 2);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return z();
    }

    public String z() {
        return new String(this.a);
    }
}
