package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class h0 extends e4 {
    public static final h0 a = new h0();
    public static final byte[] f0 = new byte[0];

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(5, f0);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return 2;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }
}
