package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class j0 extends f4 {
    public j0(byte[] bArr) {
        super(bArr);
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(4, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length) + 1 + this.a.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }
}
