package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* loaded from: classes2.dex */
public abstract class h implements c4 {
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof c4) {
            return i().equals(((c4) obj).i());
        }
        return false;
    }

    public int hashCode() {
        return i().hashCode();
    }

    @Override // defpackage.c4
    public abstract k i();

    public byte[] m() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        new j(byteArrayOutputStream).j(this);
        return byteArrayOutputStream.toByteArray();
    }

    public byte[] n(String str) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream;
        if (str.equals("DER")) {
            byteArrayOutputStream = new ByteArrayOutputStream();
            new l0(byteArrayOutputStream).j(this);
        } else if (!str.equals("DL")) {
            return m();
        } else {
            byteArrayOutputStream = new ByteArrayOutputStream();
            new y0(byteArrayOutputStream).j(this);
        }
        return byteArrayOutputStream.toByteArray();
    }
}
