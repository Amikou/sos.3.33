package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

/* loaded from: classes2.dex */
public class f1 extends h4 {
    public byte[] f0;

    public f1(byte[] bArr) throws IOException {
        this.f0 = bArr;
    }

    @Override // defpackage.h4
    public synchronized c4 D(int i) {
        if (this.f0 != null) {
            G();
        }
        return super.D(i);
    }

    @Override // defpackage.h4
    public synchronized Enumeration E() {
        byte[] bArr = this.f0;
        if (bArr == null) {
            return super.E();
        }
        return new vy1(bArr);
    }

    public final void G() {
        vy1 vy1Var = new vy1(this.f0);
        while (vy1Var.hasMoreElements()) {
            this.a.addElement(vy1Var.nextElement());
        }
        this.f0 = null;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        byte[] bArr = this.f0;
        if (bArr != null) {
            jVar.g(48, bArr);
        } else {
            super.y().p(jVar);
        }
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        byte[] bArr = this.f0;
        return bArr != null ? g1.a(bArr.length) + 1 + this.f0.length : super.y().q();
    }

    @Override // defpackage.h4
    public synchronized int size() {
        if (this.f0 != null) {
            G();
        }
        return super.size();
    }

    @Override // defpackage.h4, org.bouncycastle.asn1.k
    public k w() {
        if (this.f0 != null) {
            G();
        }
        return super.w();
    }

    @Override // defpackage.h4, org.bouncycastle.asn1.k
    public k y() {
        if (this.f0 != null) {
            G();
        }
        return super.y();
    }
}
