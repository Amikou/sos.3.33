package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* loaded from: classes2.dex */
public class u0 extends k {
    public static final char[] f0 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    public final byte[] a;

    public u0(byte[] bArr) {
        this.a = wh.e(bArr);
    }

    public String B() {
        StringBuffer stringBuffer = new StringBuffer("#");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            new j(byteArrayOutputStream).j(this);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            for (int i = 0; i != byteArray.length; i++) {
                char[] cArr = f0;
                stringBuffer.append(cArr[(byteArray[i] >>> 4) & 15]);
                stringBuffer.append(cArr[byteArray[i] & 15]);
            }
            return stringBuffer.toString();
        } catch (IOException unused) {
            throw new ASN1ParsingException("internal error encoding UniversalString");
        }
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof u0) {
            return wh.a(this.a, ((u0) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(28, z());
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        return g1.a(this.a.length) + 1 + this.a.length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    public String toString() {
        return B();
    }

    public byte[] z() {
        return wh.e(this.a);
    }
}
