package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.OutputStream;

/* loaded from: classes2.dex */
public class j {
    public OutputStream a;

    /* loaded from: classes2.dex */
    public class a extends j {
        public boolean b;

        public a(j jVar, OutputStream outputStream) {
            super(outputStream);
            this.b = true;
        }

        @Override // org.bouncycastle.asn1.j
        public void c(int i) throws IOException {
            if (this.b) {
                this.b = false;
            } else {
                super.c(i);
            }
        }
    }

    public j(OutputStream outputStream) {
        this.a = outputStream;
    }

    public j a() {
        return new l0(this.a);
    }

    public j b() {
        return new y0(this.a);
    }

    public void c(int i) throws IOException {
        this.a.write(i);
    }

    public void d(byte[] bArr) throws IOException {
        this.a.write(bArr);
    }

    public void e(byte[] bArr, int i, int i2) throws IOException {
        this.a.write(bArr, i, i2);
    }

    public void f(int i, int i2, byte[] bArr) throws IOException {
        k(i, i2);
        i(bArr.length);
        d(bArr);
    }

    public void g(int i, byte[] bArr) throws IOException {
        c(i);
        i(bArr.length);
        d(bArr);
    }

    public void h(k kVar) throws IOException {
        if (kVar == null) {
            throw new IOException("null object detected");
        }
        kVar.p(new a(this, this.a));
    }

    public void i(int i) throws IOException {
        if (i <= 127) {
            c((byte) i);
            return;
        }
        int i2 = i;
        int i3 = 1;
        while (true) {
            i2 >>>= 8;
            if (i2 == 0) {
                break;
            }
            i3++;
        }
        c((byte) (i3 | 128));
        for (int i4 = (i3 - 1) * 8; i4 >= 0; i4 -= 8) {
            c((byte) (i >> i4));
        }
    }

    public void j(c4 c4Var) throws IOException {
        if (c4Var == null) {
            throw new IOException("null object detected");
        }
        c4Var.i().p(this);
    }

    public void k(int i, int i2) throws IOException {
        if (i2 < 31) {
            c(i | i2);
            return;
        }
        c(i | 31);
        if (i2 < 128) {
            c(i2);
            return;
        }
        byte[] bArr = new byte[5];
        int i3 = 4;
        bArr[4] = (byte) (i2 & 127);
        do {
            i2 >>= 7;
            i3--;
            bArr[i3] = (byte) ((i2 & 127) | 128);
        } while (i2 > 127);
        e(bArr, i3, 5 - i3);
    }
}
