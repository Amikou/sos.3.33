package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class u implements c4, d1 {
    public l a;

    public u(l lVar) {
        this.a = lVar;
    }

    @Override // defpackage.c4
    public k i() {
        try {
            return k();
        } catch (IOException e) {
            throw new ASN1ParsingException(e.getMessage(), e);
        }
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() throws IOException {
        return new t(this.a.d());
    }
}
