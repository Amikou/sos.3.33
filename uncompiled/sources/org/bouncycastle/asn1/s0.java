package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class s0 extends k4 {
    public static final byte[] i0 = new byte[0];

    public s0(boolean z, int i, c4 c4Var) {
        super(z, i, c4Var);
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        if (this.f0) {
            jVar.f(160, this.a, i0);
            return;
        }
        k w = this.h0.i().w();
        if (!this.g0) {
            jVar.k(w.t() ? 160 : 128, this.a);
            jVar.h(w);
            return;
        }
        jVar.k(160, this.a);
        jVar.i(w.q());
        jVar.j(w);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() throws IOException {
        int b;
        if (this.f0) {
            return g1.b(this.a) + 1;
        }
        int q = this.h0.i().w().q();
        if (this.g0) {
            b = g1.b(this.a) + g1.a(q);
        } else {
            q--;
            b = g1.b(this.a);
        }
        return b + q;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        if (this.f0 || this.g0) {
            return true;
        }
        return this.h0.i().w().t();
    }
}
