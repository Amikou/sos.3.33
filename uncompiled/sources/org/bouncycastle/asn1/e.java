package org.bouncycastle.asn1;

import java.io.IOException;

/* loaded from: classes2.dex */
public class e extends k {
    public byte[] a;

    public e(byte[] bArr) {
        this.a = bArr;
    }

    public boolean B() {
        return E(10) && E(11);
    }

    public boolean D() {
        return E(12) && E(13);
    }

    public final boolean E(int i) {
        byte[] bArr = this.a;
        return bArr.length > i && bArr[i] >= 48 && bArr[i] <= 57;
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof e) {
            return wh.a(this.a, ((e) kVar).a);
        }
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public void p(j jVar) throws IOException {
        jVar.g(24, this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public int q() {
        int length = this.a.length;
        return g1.a(length) + 1 + length;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return false;
    }

    @Override // org.bouncycastle.asn1.k
    public k w() {
        return new e0(this.a);
    }

    public boolean z() {
        int i = 0;
        while (true) {
            byte[] bArr = this.a;
            if (i == bArr.length) {
                return false;
            }
            if (bArr[i] == 46 && i == 14) {
                return true;
            }
            i++;
        }
    }
}
