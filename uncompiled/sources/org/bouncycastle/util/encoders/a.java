package org.bouncycastle.util.encoders;

import java.io.IOException;
import java.io.OutputStream;

/* loaded from: classes2.dex */
public interface a {
    int a(String str, OutputStream outputStream) throws IOException;

    int b(byte[] bArr, int i, int i2, OutputStream outputStream) throws IOException;
}
