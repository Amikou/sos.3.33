package org.bouncycastle.jce.provider;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.IOException;
import java.security.AccessController;
import java.security.PrivateKey;
import java.security.PrivilegedAction;
import java.security.Provider;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import org.bouncycastle.asn1.i;
import org.bouncycastle.pqc.jcajce.provider.xmss.b;

/* loaded from: classes2.dex */
public final class BouncyCastleProvider extends Provider implements x40 {
    public static String a = "BouncyCastle Security Provider v1.60";
    public static final gw2 CONFIGURATION = new fr();
    public static final Map f0 = new HashMap();
    public static final String[] g0 = {"PBEPBKDF1", "PBEPBKDF2", "PBEPKCS12", "TLSKDF", "SCRYPT"};
    public static final String[] h0 = {"SipHash", "Poly1305"};
    public static final String[] i0 = {"AES", "ARC4", "ARIA", "Blowfish", "Camellia", "CAST5", "CAST6", "ChaCha", "DES", "DESede", "GOST28147", "Grainv1", "Grain128", "HC128", "HC256", "IDEA", "Noekeon", "RC2", "RC5", "RC6", "Rijndael", "Salsa20", "SEED", "Serpent", "Shacal2", "Skipjack", "SM4", "TEA", "Twofish", "Threefish", "VMPC", "VMPCKSA3", "XTEA", "XSalsa20", "OpenSSLPBKDF", "DSTU7624", "GOST3412_2015"};
    public static final String[] j0 = {"X509", "IES"};
    public static final String[] k0 = {"DSA", "DH", "EC", "RSA", "GOST", "ECGOST", "ElGamal", "DSTU4145", "GM"};
    public static final String[] l0 = {"GOST3411", "Keccak", "MD2", "MD4", "MD5", "SHA1", "RIPEMD128", "RIPEMD160", "RIPEMD256", "RIPEMD320", "SHA224", "SHA256", "SHA384", "SHA512", "SHA3", "Skein", "SM3", "Tiger", "Whirlpool", "Blake2b", "Blake2s", "DSTU7564"};
    public static final String PROVIDER_NAME = "BC";
    public static final String[] m0 = {PROVIDER_NAME, "BCFKS", "PKCS12"};
    public static final String[] n0 = {"DRBG"};

    /* loaded from: classes2.dex */
    public class a implements PrivilegedAction {
        public a() {
        }

        @Override // java.security.PrivilegedAction
        public Object run() {
            BouncyCastleProvider.this.e();
            return null;
        }
    }

    public BouncyCastleProvider() {
        super(PROVIDER_NAME, 1.6d, a);
        AccessController.doPrivileged(new a());
    }

    public static oi a(i iVar) {
        oi oiVar;
        Map map = f0;
        synchronized (map) {
            oiVar = (oi) map.get(iVar);
        }
        return oiVar;
    }

    public static PrivateKey getPrivateKey(zu2 zu2Var) throws IOException {
        oi a2 = a(zu2Var.p().o());
        if (a2 == null) {
            return null;
        }
        return a2.b(zu2Var);
    }

    public static PublicKey getPublicKey(jv3 jv3Var) throws IOException {
        oi a2 = a(jv3Var.o().o());
        if (a2 == null) {
            return null;
        }
        return a2.a(jv3Var);
    }

    public void addAlgorithm(String str, String str2) {
        if (!containsKey(str)) {
            put(str, str2);
            return;
        }
        throw new IllegalStateException("duplicate provider key (" + str + ") found");
    }

    public void addAlgorithm(String str, i iVar, String str2) {
        addAlgorithm(str + "." + iVar, str2);
        addAlgorithm(str + ".OID." + iVar, str2);
    }

    public void addAttributes(String str, Map<String, String> map) {
        for (String str2 : map.keySet()) {
            String str3 = str + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + str2;
            if (containsKey(str3)) {
                throw new IllegalStateException("duplicate provider attribute key (" + str3 + ") found");
            }
            put(str3, map.get(str2));
        }
    }

    public void addKeyInfoConverter(i iVar, oi oiVar) {
        Map map = f0;
        synchronized (map) {
            map.put(iVar, oiVar);
        }
    }

    public final void b(String str, String[] strArr) {
        for (int i = 0; i != strArr.length; i++) {
            Class a2 = dz.a(BouncyCastleProvider.class, str + strArr[i] + "$Mappings");
            if (a2 != null) {
                try {
                    ((wa) a2.newInstance()).a(this);
                } catch (Exception e) {
                    throw new InternalError("cannot create instance of " + str + strArr[i] + "$Mappings : " + e);
                }
            }
        }
    }

    public final void d() {
        addKeyInfoConverter(po2.e, new org.bouncycastle.pqc.jcajce.provider.sphincs.a());
        addKeyInfoConverter(po2.f, new org.bouncycastle.pqc.jcajce.provider.newhope.a());
        addKeyInfoConverter(po2.g, new org.bouncycastle.pqc.jcajce.provider.xmss.a());
        addKeyInfoConverter(po2.h, new b());
        addKeyInfoConverter(po2.c, new org.bouncycastle.pqc.jcajce.provider.mceliece.b());
        addKeyInfoConverter(po2.d, new org.bouncycastle.pqc.jcajce.provider.mceliece.a());
        addKeyInfoConverter(po2.a, new org.bouncycastle.pqc.jcajce.provider.rainbow.a());
    }

    public final void e() {
        b("org.bouncycastle.jcajce.provider.digest.", l0);
        b("org.bouncycastle.jcajce.provider.symmetric.", g0);
        b("org.bouncycastle.jcajce.provider.symmetric.", h0);
        b("org.bouncycastle.jcajce.provider.symmetric.", i0);
        b("org.bouncycastle.jcajce.provider.asymmetric.", j0);
        b("org.bouncycastle.jcajce.provider.asymmetric.", k0);
        b("org.bouncycastle.jcajce.provider.keystore.", m0);
        b("org.bouncycastle.jcajce.provider.drbg.", n0);
        d();
        put("X509Store.CERTIFICATE/COLLECTION", "org.bouncycastle.jce.provider.X509StoreCertCollection");
        put("X509Store.ATTRIBUTECERTIFICATE/COLLECTION", "org.bouncycastle.jce.provider.X509StoreAttrCertCollection");
        put("X509Store.CRL/COLLECTION", "org.bouncycastle.jce.provider.X509StoreCRLCollection");
        put("X509Store.CERTIFICATEPAIR/COLLECTION", "org.bouncycastle.jce.provider.X509StoreCertPairCollection");
        put("X509Store.CERTIFICATE/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPCerts");
        put("X509Store.CRL/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPCRLs");
        put("X509Store.ATTRIBUTECERTIFICATE/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPAttrCerts");
        put("X509Store.CERTIFICATEPAIR/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPCertPairs");
        put("X509StreamParser.CERTIFICATE", "org.bouncycastle.jce.provider.X509CertParser");
        put("X509StreamParser.ATTRIBUTECERTIFICATE", "org.bouncycastle.jce.provider.X509AttrCertParser");
        put("X509StreamParser.CRL", "org.bouncycastle.jce.provider.X509CRLParser");
        put("X509StreamParser.CERTIFICATEPAIR", "org.bouncycastle.jce.provider.X509CertPairParser");
        put("Cipher.BROKENPBEWITHMD5ANDDES", "org.bouncycastle.jce.provider.BrokenJCEBlockCipher$BrokePBEWithMD5AndDES");
        put("Cipher.BROKENPBEWITHSHA1ANDDES", "org.bouncycastle.jce.provider.BrokenJCEBlockCipher$BrokePBEWithSHA1AndDES");
        put("Cipher.OLDPBEWITHSHAANDTWOFISH-CBC", "org.bouncycastle.jce.provider.BrokenJCEBlockCipher$OldPBEWithSHAAndTwofish");
        put("CertPathValidator.RFC3281", "org.bouncycastle.jce.provider.PKIXAttrCertPathValidatorSpi");
        put("CertPathBuilder.RFC3281", "org.bouncycastle.jce.provider.PKIXAttrCertPathBuilderSpi");
        put("CertPathValidator.RFC3280", "org.bouncycastle.jce.provider.PKIXCertPathValidatorSpi");
        put("CertPathBuilder.RFC3280", "org.bouncycastle.jce.provider.PKIXCertPathBuilderSpi");
        put("CertPathValidator.PKIX", "org.bouncycastle.jce.provider.PKIXCertPathValidatorSpi");
        put("CertPathBuilder.PKIX", "org.bouncycastle.jce.provider.PKIXCertPathBuilderSpi");
        put("CertStore.Collection", "org.bouncycastle.jce.provider.CertStoreCollectionSpi");
        put("CertStore.LDAP", "org.bouncycastle.jce.provider.X509LDAPCertStoreSpi");
        put("CertStore.Multi", "org.bouncycastle.jce.provider.MultiCertStoreSpi");
        put("Alg.Alias.CertStore.X509LDAP", "LDAP");
    }

    public boolean hasAlgorithm(String str, String str2) {
        if (!containsKey(str + "." + str2)) {
            if (!containsKey("Alg.Alias." + str + "." + str2)) {
                return false;
            }
        }
        return true;
    }

    public void setParameter(String str, Object obj) {
        gw2 gw2Var = CONFIGURATION;
        synchronized (gw2Var) {
            ((fr) gw2Var).d(str, obj);
        }
    }
}
