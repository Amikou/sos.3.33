package org.bouncycastle.crypto;

/* loaded from: classes2.dex */
public interface a {
    int a(byte[] bArr, int i) throws DataLengthException, IllegalStateException;

    void b(byte[] bArr, int i, int i2) throws DataLengthException, IllegalStateException;

    int c();

    void d(ty tyVar) throws IllegalArgumentException;
}
