package org.java_websocket.drafts;

import androidx.media3.common.PlaybackException;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.java_websocket.WebSocket;
import org.java_websocket.b;
import org.java_websocket.exceptions.IncompleteHandshakeException;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.exceptions.LimitExedeedException;
import org.java_websocket.framing.Framedata;

/* loaded from: classes2.dex */
public abstract class Draft {
    public WebSocket.Role a = null;

    /* loaded from: classes2.dex */
    public enum CloseHandshakeType {
        NONE,
        ONEWAY,
        TWOWAY
    }

    /* loaded from: classes2.dex */
    public enum HandshakeState {
        MATCHED,
        NOT_MATCHED
    }

    public static ByteBuffer n(ByteBuffer byteBuffer) {
        ByteBuffer allocate = ByteBuffer.allocate(byteBuffer.remaining());
        byte b = 48;
        while (byteBuffer.hasRemaining()) {
            byte b2 = byteBuffer.get();
            allocate.put(b2);
            if (b == 13 && b2 == 10) {
                allocate.limit(allocate.position() - 2);
                allocate.position(0);
                return allocate;
            }
            b = b2;
        }
        byteBuffer.position(byteBuffer.position() - allocate.position());
        return null;
    }

    public static String o(ByteBuffer byteBuffer) {
        ByteBuffer n = n(byteBuffer);
        if (n == null) {
            return null;
        }
        return ay.d(n.array(), 0, n.limit());
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r10v21, types: [cm3, sj1] */
    public static qj1 u(ByteBuffer byteBuffer, WebSocket.Role role) throws InvalidHandshakeException, IncompleteHandshakeException {
        rj1 rj1Var;
        String o = o(byteBuffer);
        if (o != null) {
            String[] split = o.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, 3);
            if (split.length == 3) {
                if (role == WebSocket.Role.CLIENT) {
                    if ("101".equals(split[1])) {
                        if ("HTTP/1.1".equalsIgnoreCase(split[0])) {
                            ?? sj1Var = new sj1();
                            sj1Var.e(Short.parseShort(split[1]));
                            sj1Var.g(split[2]);
                            rj1Var = sj1Var;
                        } else {
                            throw new InvalidHandshakeException("Invalid status line received: " + split[0] + " Status line: " + o);
                        }
                    } else {
                        throw new InvalidHandshakeException("Invalid status code received: " + split[1] + " Status line: " + o);
                    }
                } else if ("GET".equalsIgnoreCase(split[0])) {
                    if ("HTTP/1.1".equalsIgnoreCase(split[2])) {
                        rj1 rj1Var2 = new rj1();
                        rj1Var2.f(split[1]);
                        rj1Var = rj1Var2;
                    } else {
                        throw new InvalidHandshakeException("Invalid status line received: " + split[2] + " Status line: " + o);
                    }
                } else {
                    throw new InvalidHandshakeException("Invalid request method received: " + split[0] + " Status line: " + o);
                }
                String o2 = o(byteBuffer);
                while (o2 != null && o2.length() > 0) {
                    String[] split2 = o2.split(":", 2);
                    if (split2.length == 2) {
                        if (rj1Var.d(split2[0])) {
                            String str = split2[0];
                            rj1Var.put(str, rj1Var.h(split2[0]) + "; " + split2[1].replaceFirst("^ +", ""));
                        } else {
                            rj1Var.put(split2[0], split2[1].replaceFirst("^ +", ""));
                        }
                        o2 = o(byteBuffer);
                    } else {
                        throw new InvalidHandshakeException("not an http header");
                    }
                }
                if (o2 != null) {
                    return rj1Var;
                }
                throw new IncompleteHandshakeException();
            }
            throw new InvalidHandshakeException();
        }
        throw new IncompleteHandshakeException(byteBuffer.capacity() + 128);
    }

    public abstract HandshakeState a(hz hzVar, bm3 bm3Var) throws InvalidHandshakeException;

    public abstract HandshakeState b(hz hzVar) throws InvalidHandshakeException;

    public boolean c(tj1 tj1Var) {
        return tj1Var.h("Upgrade").equalsIgnoreCase("websocket") && tj1Var.h("Connection").toLowerCase(Locale.ENGLISH).contains("upgrade");
    }

    public int d(int i) throws LimitExedeedException, InvalidDataException {
        if (i >= 0) {
            return i;
        }
        throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "Negative count");
    }

    public abstract Draft e();

    public abstract ByteBuffer f(Framedata framedata);

    public abstract List<Framedata> g(String str, boolean z);

    public List<ByteBuffer> h(tj1 tj1Var, WebSocket.Role role) {
        return i(tj1Var, role, true);
    }

    public List<ByteBuffer> i(tj1 tj1Var, WebSocket.Role role, boolean z) {
        StringBuilder sb = new StringBuilder(100);
        if (tj1Var instanceof hz) {
            sb.append("GET ");
            sb.append(((hz) tj1Var).a());
            sb.append(" HTTP/1.1");
        } else if (tj1Var instanceof bm3) {
            sb.append("HTTP/1.1 101 ");
            sb.append(((bm3) tj1Var).c());
        } else {
            throw new IllegalArgumentException("unknown role");
        }
        sb.append("\r\n");
        Iterator<String> b = tj1Var.b();
        while (b.hasNext()) {
            String next = b.next();
            String h = tj1Var.h(next);
            sb.append(next);
            sb.append(": ");
            sb.append(h);
            sb.append("\r\n");
        }
        sb.append("\r\n");
        byte[] a = ay.a(sb.toString());
        byte[] content = z ? tj1Var.getContent() : null;
        ByteBuffer allocate = ByteBuffer.allocate((content == null ? 0 : content.length) + a.length);
        allocate.put(a);
        if (content != null) {
            allocate.put(content);
        }
        allocate.flip();
        return Collections.singletonList(allocate);
    }

    public abstract CloseHandshakeType j();

    public abstract iz k(iz izVar) throws InvalidHandshakeException;

    public abstract qj1 l(hz hzVar, cm3 cm3Var) throws InvalidHandshakeException;

    public abstract void m(b bVar, Framedata framedata) throws InvalidDataException;

    public int p(tj1 tj1Var) {
        String h = tj1Var.h("Sec-WebSocket-Version");
        if (h.length() > 0) {
            try {
                return new Integer(h.trim()).intValue();
            } catch (NumberFormatException unused) {
            }
        }
        return -1;
    }

    public abstract void q();

    public void r(WebSocket.Role role) {
        this.a = role;
    }

    public abstract List<Framedata> s(ByteBuffer byteBuffer) throws InvalidDataException;

    public tj1 t(ByteBuffer byteBuffer) throws InvalidHandshakeException {
        return u(byteBuffer, this.a);
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}
