package org.java_websocket.drafts;

import androidx.media3.common.PlaybackException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import okhttp3.internal.ws.WebSocketProtocol;
import org.java_websocket.WebSocket;
import org.java_websocket.b;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.IncompleteException;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidFrameException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.exceptions.LimitExedeedException;
import org.java_websocket.exceptions.NotSendableException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.f;
import org.java_websocket.framing.i;

/* compiled from: Draft_6455.java */
/* loaded from: classes2.dex */
public class a extends Draft {
    public xl1 b;
    public List<xl1> c;
    public om1 d;
    public List<om1> e;
    public Framedata f;
    public List<ByteBuffer> g;
    public ByteBuffer h;
    public final Random i;

    public a() {
        this(Collections.emptyList());
    }

    public List<om1> A() {
        return this.e;
    }

    public final ByteBuffer B() throws LimitExedeedException {
        long j = 0;
        for (ByteBuffer byteBuffer : this.g) {
            j += byteBuffer.limit();
        }
        if (j <= 2147483647L) {
            ByteBuffer allocate = ByteBuffer.allocate((int) j);
            for (ByteBuffer byteBuffer2 : this.g) {
                allocate.put(byteBuffer2);
            }
            allocate.flip();
            return allocate;
        }
        throw new LimitExedeedException("Payloadsize is to big...");
    }

    public om1 C() {
        return this.d;
    }

    public final String D() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(calendar.getTime());
    }

    public final byte[] E(long j, int i) {
        byte[] bArr = new byte[i];
        int i2 = (i * 8) - 8;
        for (int i3 = 0; i3 < i; i3++) {
            bArr[i3] = (byte) (j >>> (i2 - (i3 * 8)));
        }
        return bArr;
    }

    public final Framedata.Opcode F(byte b) throws InvalidFrameException {
        if (b != 0) {
            if (b != 1) {
                if (b != 2) {
                    switch (b) {
                        case 8:
                            return Framedata.Opcode.CLOSING;
                        case 9:
                            return Framedata.Opcode.PING;
                        case 10:
                            return Framedata.Opcode.PONG;
                        default:
                            throw new InvalidFrameException("Unknown opcode " + ((int) b));
                    }
                }
                return Framedata.Opcode.BINARY;
            }
            return Framedata.Opcode.TEXT;
        }
        return Framedata.Opcode.CONTINUOUS;
    }

    public Framedata G(ByteBuffer byteBuffer) throws IncompleteException, InvalidDataException {
        boolean z;
        int i;
        int remaining = byteBuffer.remaining();
        int i2 = 2;
        if (remaining >= 2) {
            byte b = byteBuffer.get();
            boolean z2 = (b >> 8) != 0;
            boolean z3 = (b & 64) != 0;
            boolean z4 = (b & 32) != 0;
            boolean z5 = (b & 16) != 0;
            byte b2 = byteBuffer.get();
            boolean z6 = (b2 & Byte.MIN_VALUE) != 0;
            byte b3 = (byte) (b2 & Byte.MAX_VALUE);
            Framedata.Opcode F = F((byte) (b & 15));
            if (b3 >= 0 && b3 <= 125) {
                z = z3;
                i = b3;
            } else if (F == Framedata.Opcode.PING || F == Framedata.Opcode.PONG || F == Framedata.Opcode.CLOSING) {
                throw new InvalidFrameException("more than 125 octets");
            } else {
                if (b3 != 126) {
                    i2 = 10;
                    if (remaining >= 10) {
                        byte[] bArr = new byte[8];
                        for (int i3 = 0; i3 < 8; i3++) {
                            bArr[i3] = byteBuffer.get();
                        }
                        z = z3;
                        long longValue = new BigInteger(bArr).longValue();
                        if (longValue > 2147483647L) {
                            throw new LimitExedeedException("Payloadsize is to big...");
                        }
                        i = (int) longValue;
                    } else {
                        throw new IncompleteException(10);
                    }
                } else if (remaining >= 4) {
                    z = z3;
                    i = new BigInteger(new byte[]{0, byteBuffer.get(), byteBuffer.get()}).intValue();
                    i2 = 4;
                } else {
                    throw new IncompleteException(4);
                }
            }
            int i4 = i2 + (z6 ? 4 : 0) + i;
            if (remaining >= i4) {
                ByteBuffer allocate = ByteBuffer.allocate(d(i));
                if (z6) {
                    byte[] bArr2 = new byte[4];
                    byteBuffer.get(bArr2);
                    for (int i5 = 0; i5 < i; i5++) {
                        allocate.put((byte) (byteBuffer.get() ^ bArr2[i5 % 4]));
                    }
                } else {
                    allocate.put(byteBuffer.array(), byteBuffer.position(), allocate.limit());
                    byteBuffer.position(byteBuffer.position() + allocate.limit());
                }
                f g = f.g(F);
                g.i(z2);
                g.k(z);
                g.l(z4);
                g.m(z5);
                allocate.flip();
                g.j(allocate);
                y().f(g);
                y().c(g);
                if (b.w0) {
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append("afterDecoding(");
                    sb.append(g.f().remaining());
                    sb.append("): {");
                    sb.append(g.f().remaining() > 1000 ? "too big to display" : new String(g.f().array()));
                    sb.append('}');
                    printStream.println(sb.toString());
                }
                g.h();
                return g;
            }
            throw new IncompleteException(i4);
        }
        throw new IncompleteException(2);
    }

    @Override // org.java_websocket.drafts.Draft
    public Draft.HandshakeState a(hz hzVar, bm3 bm3Var) throws InvalidHandshakeException {
        if (!c(bm3Var)) {
            return Draft.HandshakeState.NOT_MATCHED;
        }
        if (hzVar.d("Sec-WebSocket-Key") && bm3Var.d("Sec-WebSocket-Accept")) {
            if (!x(hzVar.h("Sec-WebSocket-Key")).equals(bm3Var.h("Sec-WebSocket-Accept"))) {
                return Draft.HandshakeState.NOT_MATCHED;
            }
            Draft.HandshakeState handshakeState = Draft.HandshakeState.NOT_MATCHED;
            String h = bm3Var.h("Sec-WebSocket-Extensions");
            Iterator<xl1> it = this.c.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                xl1 next = it.next();
                if (next.d(h)) {
                    this.b = next;
                    handshakeState = Draft.HandshakeState.MATCHED;
                    break;
                }
            }
            Draft.HandshakeState handshakeState2 = Draft.HandshakeState.NOT_MATCHED;
            String h2 = bm3Var.h("Sec-WebSocket-Protocol");
            Iterator<om1> it2 = this.e.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                om1 next2 = it2.next();
                if (next2.c(h2)) {
                    this.d = next2;
                    handshakeState2 = Draft.HandshakeState.MATCHED;
                    break;
                }
            }
            Draft.HandshakeState handshakeState3 = Draft.HandshakeState.MATCHED;
            return (handshakeState2 == handshakeState3 && handshakeState == handshakeState3) ? handshakeState3 : Draft.HandshakeState.NOT_MATCHED;
        }
        return Draft.HandshakeState.NOT_MATCHED;
    }

    @Override // org.java_websocket.drafts.Draft
    public Draft.HandshakeState b(hz hzVar) throws InvalidHandshakeException {
        if (p(hzVar) != 13) {
            return Draft.HandshakeState.NOT_MATCHED;
        }
        Draft.HandshakeState handshakeState = Draft.HandshakeState.NOT_MATCHED;
        String h = hzVar.h("Sec-WebSocket-Extensions");
        Iterator<xl1> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            xl1 next = it.next();
            if (next.b(h)) {
                this.b = next;
                handshakeState = Draft.HandshakeState.MATCHED;
                break;
            }
        }
        Draft.HandshakeState handshakeState2 = Draft.HandshakeState.NOT_MATCHED;
        String h2 = hzVar.h("Sec-WebSocket-Protocol");
        Iterator<om1> it2 = this.e.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            om1 next2 = it2.next();
            if (next2.c(h2)) {
                this.d = next2;
                handshakeState2 = Draft.HandshakeState.MATCHED;
                break;
            }
        }
        Draft.HandshakeState handshakeState3 = Draft.HandshakeState.MATCHED;
        return (handshakeState2 == handshakeState3 && handshakeState == handshakeState3) ? handshakeState3 : Draft.HandshakeState.NOT_MATCHED;
    }

    @Override // org.java_websocket.drafts.Draft
    public Draft e() {
        ArrayList arrayList = new ArrayList();
        for (xl1 xl1Var : z()) {
            arrayList.add(xl1Var.a());
        }
        ArrayList arrayList2 = new ArrayList();
        for (om1 om1Var : A()) {
            arrayList2.add(om1Var.a());
        }
        return new a(arrayList, arrayList2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || a.class != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        xl1 xl1Var = this.b;
        if (xl1Var == null ? aVar.b == null : xl1Var.equals(aVar.b)) {
            om1 om1Var = this.d;
            om1 om1Var2 = aVar.d;
            return om1Var != null ? om1Var.equals(om1Var2) : om1Var2 == null;
        }
        return false;
    }

    @Override // org.java_websocket.drafts.Draft
    public ByteBuffer f(Framedata framedata) {
        y().e(framedata);
        if (b.w0) {
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("afterEnconding(");
            sb.append(framedata.f().remaining());
            sb.append("): {");
            sb.append(framedata.f().remaining() > 1000 ? "too big to display" : new String(framedata.f().array()));
            sb.append('}');
            printStream.println(sb.toString());
        }
        return v(framedata);
    }

    @Override // org.java_websocket.drafts.Draft
    public List<Framedata> g(String str, boolean z) {
        i iVar = new i();
        iVar.j(ByteBuffer.wrap(ay.f(str)));
        iVar.n(z);
        try {
            iVar.h();
            return Collections.singletonList(iVar);
        } catch (InvalidDataException e) {
            throw new NotSendableException(e);
        }
    }

    public int hashCode() {
        xl1 xl1Var = this.b;
        int hashCode = (xl1Var != null ? xl1Var.hashCode() : 0) * 31;
        om1 om1Var = this.d;
        return hashCode + (om1Var != null ? om1Var.hashCode() : 0);
    }

    @Override // org.java_websocket.drafts.Draft
    public Draft.CloseHandshakeType j() {
        return Draft.CloseHandshakeType.TWOWAY;
    }

    @Override // org.java_websocket.drafts.Draft
    public iz k(iz izVar) {
        izVar.put("Upgrade", "websocket");
        izVar.put("Connection", "Upgrade");
        byte[] bArr = new byte[16];
        this.i.nextBytes(bArr);
        izVar.put("Sec-WebSocket-Key", km.g(bArr));
        izVar.put("Sec-WebSocket-Version", "13");
        StringBuilder sb = new StringBuilder();
        for (xl1 xl1Var : this.c) {
            if (xl1Var.g() != null && xl1Var.g().length() != 0) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(xl1Var.g());
            }
        }
        if (sb.length() != 0) {
            izVar.put("Sec-WebSocket-Extensions", sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        for (om1 om1Var : this.e) {
            if (om1Var.b().length() != 0) {
                if (sb2.length() > 0) {
                    sb2.append(", ");
                }
                sb2.append(om1Var.b());
            }
        }
        if (sb2.length() != 0) {
            izVar.put("Sec-WebSocket-Protocol", sb2.toString());
        }
        return izVar;
    }

    @Override // org.java_websocket.drafts.Draft
    public qj1 l(hz hzVar, cm3 cm3Var) throws InvalidHandshakeException {
        cm3Var.put("Upgrade", "websocket");
        cm3Var.put("Connection", hzVar.h("Connection"));
        String h = hzVar.h("Sec-WebSocket-Key");
        if (h != null) {
            cm3Var.put("Sec-WebSocket-Accept", x(h));
            if (y().h().length() != 0) {
                cm3Var.put("Sec-WebSocket-Extensions", y().h());
            }
            if (C() != null && C().b().length() != 0) {
                cm3Var.put("Sec-WebSocket-Protocol", C().b());
            }
            cm3Var.g("Web Socket Protocol Handshake");
            cm3Var.put("Server", "TooTallNate Java-WebSocket");
            cm3Var.put("Date", D());
            return cm3Var;
        }
        throw new InvalidHandshakeException("missing Sec-WebSocket-Key");
    }

    @Override // org.java_websocket.drafts.Draft
    public void m(b bVar, Framedata framedata) throws InvalidDataException {
        String str;
        Framedata.Opcode c = framedata.c();
        if (c == Framedata.Opcode.CLOSING) {
            int i = WebSocketProtocol.CLOSE_NO_STATUS_CODE;
            if (framedata instanceof org.java_websocket.framing.b) {
                org.java_websocket.framing.b bVar2 = (org.java_websocket.framing.b) framedata;
                i = bVar2.o();
                str = bVar2.p();
            } else {
                str = "";
            }
            if (bVar.q() == WebSocket.READYSTATE.CLOSING) {
                bVar.f(i, str, true);
            } else if (j() == Draft.CloseHandshakeType.TWOWAY) {
                bVar.c(i, str, true);
            } else {
                bVar.n(i, str, false);
            }
        } else if (c == Framedata.Opcode.PING) {
            bVar.r().onWebsocketPing(bVar, framedata);
        } else if (c == Framedata.Opcode.PONG) {
            bVar.B();
            bVar.r().onWebsocketPong(bVar, framedata);
        } else if (framedata.e() && c != Framedata.Opcode.CONTINUOUS) {
            if (this.f == null) {
                if (c == Framedata.Opcode.TEXT) {
                    try {
                        bVar.r().onWebsocketMessage(bVar, ay.e(framedata.f()));
                        return;
                    } catch (RuntimeException e) {
                        bVar.r().onWebsocketError(bVar, e);
                        return;
                    }
                } else if (c == Framedata.Opcode.BINARY) {
                    try {
                        bVar.r().onWebsocketMessage(bVar, framedata.f());
                        return;
                    } catch (RuntimeException e2) {
                        bVar.r().onWebsocketError(bVar, e2);
                        return;
                    }
                } else {
                    throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "non control or continious frame expected");
                }
            }
            throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "Continuous frame sequence not completed.");
        } else {
            if (c != Framedata.Opcode.CONTINUOUS) {
                if (this.f == null) {
                    this.f = framedata;
                    this.g.add(framedata.f());
                } else {
                    throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "Previous continuous frame sequence not completed.");
                }
            } else if (framedata.e()) {
                if (this.f != null) {
                    this.g.add(framedata.f());
                    if (this.f.c() == Framedata.Opcode.TEXT) {
                        ((f) this.f).j(B());
                        ((f) this.f).h();
                        try {
                            bVar.r().onWebsocketMessage(bVar, ay.e(this.f.f()));
                        } catch (RuntimeException e3) {
                            bVar.r().onWebsocketError(bVar, e3);
                        }
                    } else if (this.f.c() == Framedata.Opcode.BINARY) {
                        ((f) this.f).j(B());
                        ((f) this.f).h();
                        try {
                            bVar.r().onWebsocketMessage(bVar, this.f.f());
                        } catch (RuntimeException e4) {
                            bVar.r().onWebsocketError(bVar, e4);
                        }
                    }
                    this.f = null;
                    this.g.clear();
                } else {
                    throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "Continuous frame sequence was not started.");
                }
            } else if (this.f == null) {
                throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "Continuous frame sequence was not started.");
            }
            if (c == Framedata.Opcode.TEXT && !ay.b(framedata.f())) {
                throw new InvalidDataException(1007);
            }
            if (c != Framedata.Opcode.CONTINUOUS || this.f == null) {
                return;
            }
            this.g.add(framedata.f());
        }
    }

    @Override // org.java_websocket.drafts.Draft
    public void q() {
        this.h = null;
        xl1 xl1Var = this.b;
        if (xl1Var != null) {
            xl1Var.reset();
        }
        this.b = new gj0();
        this.d = null;
    }

    @Override // org.java_websocket.drafts.Draft
    public List<Framedata> s(ByteBuffer byteBuffer) throws InvalidDataException {
        LinkedList linkedList;
        while (true) {
            linkedList = new LinkedList();
            if (this.h == null) {
                break;
            }
            try {
                byteBuffer.mark();
                int remaining = byteBuffer.remaining();
                int remaining2 = this.h.remaining();
                if (remaining2 > remaining) {
                    this.h.put(byteBuffer.array(), byteBuffer.position(), remaining);
                    byteBuffer.position(byteBuffer.position() + remaining);
                    return Collections.emptyList();
                }
                this.h.put(byteBuffer.array(), byteBuffer.position(), remaining2);
                byteBuffer.position(byteBuffer.position() + remaining2);
                linkedList.add(G((ByteBuffer) this.h.duplicate().position(0)));
                this.h = null;
            } catch (IncompleteException e) {
                ByteBuffer allocate = ByteBuffer.allocate(d(e.getPreferredSize()));
                this.h.rewind();
                allocate.put(this.h);
                this.h = allocate;
            }
        }
        while (byteBuffer.hasRemaining()) {
            byteBuffer.mark();
            try {
                linkedList.add(G(byteBuffer));
            } catch (IncompleteException e2) {
                byteBuffer.reset();
                ByteBuffer allocate2 = ByteBuffer.allocate(d(e2.getPreferredSize()));
                this.h = allocate2;
                allocate2.put(byteBuffer);
            }
        }
        return linkedList;
    }

    @Override // org.java_websocket.drafts.Draft
    public String toString() {
        String draft = super.toString();
        if (y() != null) {
            draft = draft + " extension: " + y().toString();
        }
        if (C() != null) {
            return draft + " protocol: " + C().toString();
        }
        return draft;
    }

    public final ByteBuffer v(Framedata framedata) {
        ByteBuffer f = framedata.f();
        int i = 0;
        boolean z = this.a == WebSocket.Role.CLIENT;
        int i2 = f.remaining() <= 125 ? 1 : f.remaining() <= 65535 ? 2 : 8;
        ByteBuffer allocate = ByteBuffer.allocate((i2 > 1 ? i2 + 1 : i2) + 1 + (z ? 4 : 0) + f.remaining());
        allocate.put((byte) (((byte) (framedata.e() ? -128 : 0)) | w(framedata.c())));
        byte[] E = E(f.remaining(), i2);
        if (i2 == 1) {
            allocate.put((byte) (E[0] | (z ? Byte.MIN_VALUE : (byte) 0)));
        } else if (i2 == 2) {
            allocate.put((byte) ((z ? Byte.MIN_VALUE : (byte) 0) | 126));
            allocate.put(E);
        } else if (i2 == 8) {
            allocate.put((byte) ((z ? Byte.MIN_VALUE : (byte) 0) | Byte.MAX_VALUE));
            allocate.put(E);
        } else {
            throw new RuntimeException("Size representation not supported/specified");
        }
        if (z) {
            ByteBuffer allocate2 = ByteBuffer.allocate(4);
            allocate2.putInt(this.i.nextInt());
            allocate.put(allocate2.array());
            while (f.hasRemaining()) {
                allocate.put((byte) (f.get() ^ allocate2.get(i % 4)));
                i++;
            }
        } else {
            allocate.put(f);
            f.flip();
        }
        allocate.flip();
        return allocate;
    }

    public final byte w(Framedata.Opcode opcode) {
        if (opcode == Framedata.Opcode.CONTINUOUS) {
            return (byte) 0;
        }
        if (opcode == Framedata.Opcode.TEXT) {
            return (byte) 1;
        }
        if (opcode == Framedata.Opcode.BINARY) {
            return (byte) 2;
        }
        if (opcode == Framedata.Opcode.CLOSING) {
            return (byte) 8;
        }
        if (opcode == Framedata.Opcode.PING) {
            return (byte) 9;
        }
        if (opcode == Framedata.Opcode.PONG) {
            return (byte) 10;
        }
        throw new IllegalArgumentException("Don't know how to handle " + opcode.toString());
    }

    public final String x(String str) {
        try {
            return km.g(MessageDigest.getInstance("SHA1").digest((str.trim() + WebSocketProtocol.ACCEPT_MAGIC).getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }

    public xl1 y() {
        return this.b;
    }

    public List<xl1> z() {
        return this.c;
    }

    public a(List<xl1> list) {
        this(list, Collections.singletonList(new cw2("")));
    }

    public a(List<xl1> list, List<om1> list2) {
        this.b = new gj0();
        this.i = new Random();
        if (list != null && list2 != null) {
            this.c = new ArrayList(list.size());
            this.e = new ArrayList(list2.size());
            boolean z = false;
            this.g = new ArrayList();
            for (xl1 xl1Var : list) {
                if (xl1Var.getClass().equals(gj0.class)) {
                    z = true;
                }
            }
            this.c.addAll(list);
            if (!z) {
                List<xl1> list3 = this.c;
                list3.add(list3.size(), this.b);
            }
            this.e.addAll(list2);
            return;
        }
        throw new IllegalArgumentException();
    }
}
