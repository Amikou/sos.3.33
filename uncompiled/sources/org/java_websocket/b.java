package org.java_websocket;

import androidx.media3.common.PlaybackException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.IncompleteHandshakeException;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.g;

/* compiled from: WebSocketImpl.java */
/* loaded from: classes2.dex */
public class b implements WebSocket {
    public static int v0 = 16384;
    public static boolean w0 = false;
    public static final Object x0 = new Object();
    public final BlockingQueue<ByteBuffer> a;
    public final c f0;
    public SelectionKey g0;
    public ByteChannel h0;
    public List<Draft> k0;
    public Draft l0;
    public WebSocket.Role m0;
    public g u0;
    public volatile boolean i0 = false;
    public WebSocket.READYSTATE j0 = WebSocket.READYSTATE.NOT_YET_CONNECTED;
    public ByteBuffer n0 = ByteBuffer.allocate(0);
    public hz o0 = null;
    public String p0 = null;
    public Integer q0 = null;
    public Boolean r0 = null;
    public String s0 = null;
    public long t0 = System.currentTimeMillis();

    public b(c cVar, Draft draft) {
        this.l0 = null;
        if (cVar != null && (draft != null || this.m0 != WebSocket.Role.SERVER)) {
            this.a = new LinkedBlockingQueue();
            new LinkedBlockingQueue();
            this.f0 = cVar;
            this.m0 = WebSocket.Role.CLIENT;
            if (draft != null) {
                this.l0 = draft.e();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("parameters must not be null");
    }

    public void A(iz izVar) throws InvalidHandshakeException {
        this.o0 = this.l0.k(izVar);
        this.s0 = izVar.a();
        try {
            this.f0.onWebsocketHandshakeSentAsClient(this, this.o0);
            D(this.l0.h(this.o0, this.m0));
        } catch (RuntimeException e) {
            this.f0.onWebsocketError(this, e);
            throw new InvalidHandshakeException("rejected because of" + e);
        } catch (InvalidDataException unused) {
            throw new InvalidHandshakeException("Handshake data rejected by client.");
        }
    }

    public void B() {
        this.t0 = System.currentTimeMillis();
    }

    public final void C(ByteBuffer byteBuffer) {
        if (w0) {
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("write(");
            sb.append(byteBuffer.remaining());
            sb.append("): {");
            sb.append(byteBuffer.remaining() > 1000 ? "too big to display" : new String(byteBuffer.array()));
            sb.append('}');
            printStream.println(sb.toString());
        }
        this.a.add(byteBuffer);
        this.f0.onWriteDemand(this);
    }

    public final void D(List<ByteBuffer> list) {
        synchronized (x0) {
            for (ByteBuffer byteBuffer : list) {
                C(byteBuffer);
            }
        }
    }

    public void a(int i) {
        c(i, "", false);
    }

    public void b(int i, String str) {
        c(i, str, false);
    }

    public synchronized void c(int i, String str, boolean z) {
        WebSocket.READYSTATE q = q();
        WebSocket.READYSTATE readystate = WebSocket.READYSTATE.CLOSING;
        if (q == readystate || this.j0 == WebSocket.READYSTATE.CLOSED) {
            return;
        }
        if (q() == WebSocket.READYSTATE.OPEN) {
            if (i == 1006) {
                z(readystate);
                n(i, str, false);
                return;
            }
            if (this.l0.j() != Draft.CloseHandshakeType.NONE) {
                if (!z) {
                    try {
                        try {
                            this.f0.onWebsocketCloseInitiated(this, i, str);
                        } catch (InvalidDataException e) {
                            this.f0.onWebsocketError(this, e);
                            n(1006, "generated frame is invalid", false);
                        }
                    } catch (RuntimeException e2) {
                        this.f0.onWebsocketError(this, e2);
                    }
                }
                if (u()) {
                    org.java_websocket.framing.b bVar = new org.java_websocket.framing.b();
                    bVar.r(str);
                    bVar.q(i);
                    bVar.h();
                    sendFrame(bVar);
                }
            }
            n(i, str, z);
        } else if (i == -3) {
            n(-3, str, true);
        } else if (i == 1002) {
            n(i, str, z);
        } else {
            n(-1, str, false);
        }
        z(WebSocket.READYSTATE.CLOSING);
        this.n0 = null;
    }

    public void d(InvalidDataException invalidDataException) {
        c(invalidDataException.getCloseCode(), invalidDataException.getMessage(), false);
    }

    public void e(int i, String str) {
        f(i, str, false);
    }

    public synchronized void f(int i, String str, boolean z) {
        if (q() == WebSocket.READYSTATE.CLOSED) {
            return;
        }
        if (q() == WebSocket.READYSTATE.OPEN && i == 1006) {
            z(WebSocket.READYSTATE.CLOSING);
        }
        SelectionKey selectionKey = this.g0;
        if (selectionKey != null) {
            selectionKey.cancel();
        }
        ByteChannel byteChannel = this.h0;
        if (byteChannel != null) {
            try {
                byteChannel.close();
            } catch (IOException e) {
                if (e.getMessage().equals("Broken pipe")) {
                    if (w0) {
                        System.out.println("Caught IOException: Broken pipe during closeConnection()");
                    }
                } else {
                    this.f0.onWebsocketError(this, e);
                }
            }
        }
        try {
            this.f0.onWebsocketClose(this, i, str, z);
        } catch (RuntimeException e2) {
            this.f0.onWebsocketError(this, e2);
        }
        Draft draft = this.l0;
        if (draft != null) {
            draft.q();
        }
        this.o0 = null;
        z(WebSocket.READYSTATE.CLOSED);
    }

    public void g(int i, boolean z) {
        f(i, "", z);
    }

    public final void h(RuntimeException runtimeException) {
        C(o(500));
        n(-1, runtimeException.getMessage(), false);
    }

    public int hashCode() {
        return super.hashCode();
    }

    public final void i(InvalidDataException invalidDataException) {
        C(o(404));
        n(invalidDataException.getCloseCode(), invalidDataException.getMessage(), false);
    }

    public void j(ByteBuffer byteBuffer) {
        if (w0) {
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("process(");
            sb.append(byteBuffer.remaining());
            sb.append("): {");
            sb.append(byteBuffer.remaining() > 1000 ? "too big to display" : new String(byteBuffer.array(), byteBuffer.position(), byteBuffer.remaining()));
            sb.append('}');
            printStream.println(sb.toString());
        }
        if (q() != WebSocket.READYSTATE.NOT_YET_CONNECTED) {
            if (q() == WebSocket.READYSTATE.OPEN) {
                k(byteBuffer);
            }
        } else if (!l(byteBuffer) || t() || s()) {
        } else {
            if (byteBuffer.hasRemaining()) {
                k(byteBuffer);
            } else if (this.n0.hasRemaining()) {
                k(this.n0);
            }
        }
    }

    public final void k(ByteBuffer byteBuffer) {
        try {
            for (Framedata framedata : this.l0.s(byteBuffer)) {
                if (w0) {
                    PrintStream printStream = System.out;
                    printStream.println("matched frame: " + framedata);
                }
                this.l0.m(this, framedata);
            }
        } catch (InvalidDataException e) {
            this.f0.onWebsocketError(this, e);
            d(e);
        }
    }

    public final boolean l(ByteBuffer byteBuffer) {
        ByteBuffer byteBuffer2;
        WebSocket.Role role;
        tj1 t;
        if (this.n0.capacity() == 0) {
            byteBuffer2 = byteBuffer;
        } else {
            if (this.n0.remaining() < byteBuffer.remaining()) {
                ByteBuffer allocate = ByteBuffer.allocate(this.n0.capacity() + byteBuffer.remaining());
                this.n0.flip();
                allocate.put(this.n0);
                this.n0 = allocate;
            }
            this.n0.put(byteBuffer);
            this.n0.flip();
            byteBuffer2 = this.n0;
        }
        byteBuffer2.mark();
        try {
            try {
                role = this.m0;
            } catch (InvalidHandshakeException e) {
                d(e);
            }
        } catch (IncompleteHandshakeException e2) {
            if (this.n0.capacity() == 0) {
                byteBuffer2.reset();
                int preferedSize = e2.getPreferedSize();
                if (preferedSize == 0) {
                    preferedSize = byteBuffer2.capacity() + 16;
                }
                ByteBuffer allocate2 = ByteBuffer.allocate(preferedSize);
                this.n0 = allocate2;
                allocate2.put(byteBuffer);
            } else {
                ByteBuffer byteBuffer3 = this.n0;
                byteBuffer3.position(byteBuffer3.limit());
                ByteBuffer byteBuffer4 = this.n0;
                byteBuffer4.limit(byteBuffer4.capacity());
            }
        }
        if (role == WebSocket.Role.SERVER) {
            Draft draft = this.l0;
            if (draft == null) {
                for (Draft draft2 : this.k0) {
                    Draft e3 = draft2.e();
                    try {
                        e3.r(this.m0);
                        byteBuffer2.reset();
                        t = e3.t(byteBuffer2);
                    } catch (InvalidHandshakeException unused) {
                    }
                    if (!(t instanceof hz)) {
                        i(new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "wrong http function"));
                        return false;
                    }
                    hz hzVar = (hz) t;
                    if (e3.b(hzVar) == Draft.HandshakeState.MATCHED) {
                        this.s0 = hzVar.a();
                        try {
                            D(e3.h(e3.l(hzVar, this.f0.onWebsocketHandshakeReceivedAsServer(this, e3, hzVar)), this.m0));
                            this.l0 = e3;
                            v(hzVar);
                            return true;
                        } catch (RuntimeException e4) {
                            this.f0.onWebsocketError(this, e4);
                            h(e4);
                            return false;
                        } catch (InvalidDataException e5) {
                            i(e5);
                            return false;
                        }
                    }
                }
                if (this.l0 == null) {
                    i(new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "no draft matches"));
                }
                return false;
            }
            tj1 t2 = draft.t(byteBuffer2);
            if (!(t2 instanceof hz)) {
                n(PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "wrong http function", false);
                return false;
            }
            hz hzVar2 = (hz) t2;
            if (this.l0.b(hzVar2) == Draft.HandshakeState.MATCHED) {
                v(hzVar2);
                return true;
            }
            b(PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "the handshake did finaly not match");
            return false;
        }
        if (role == WebSocket.Role.CLIENT) {
            this.l0.r(role);
            tj1 t3 = this.l0.t(byteBuffer2);
            if (!(t3 instanceof bm3)) {
                n(PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "wrong http function", false);
                return false;
            }
            bm3 bm3Var = (bm3) t3;
            if (this.l0.a(this.o0, bm3Var) == Draft.HandshakeState.MATCHED) {
                try {
                    this.f0.onWebsocketHandshakeReceivedAsClient(this, this.o0, bm3Var);
                    v(bm3Var);
                    return true;
                } catch (RuntimeException e6) {
                    this.f0.onWebsocketError(this, e6);
                    n(-1, e6.getMessage(), false);
                    return false;
                } catch (InvalidDataException e7) {
                    n(e7.getCloseCode(), e7.getMessage(), false);
                    return false;
                }
            }
            b(PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "draft " + this.l0 + " refuses handshake");
        }
        return false;
    }

    public void m() {
        if (q() == WebSocket.READYSTATE.NOT_YET_CONNECTED) {
            g(-1, true);
        } else if (this.i0) {
            f(this.q0.intValue(), this.p0, this.r0.booleanValue());
        } else if (this.l0.j() == Draft.CloseHandshakeType.NONE) {
            g(1000, true);
        } else if (this.l0.j() == Draft.CloseHandshakeType.ONEWAY) {
            if (this.m0 == WebSocket.Role.SERVER) {
                g(1006, true);
            } else {
                g(1000, true);
            }
        } else {
            g(1006, true);
        }
    }

    public synchronized void n(int i, String str, boolean z) {
        if (this.i0) {
            return;
        }
        this.q0 = Integer.valueOf(i);
        this.p0 = str;
        this.r0 = Boolean.valueOf(z);
        this.i0 = true;
        this.f0.onWriteDemand(this);
        try {
            this.f0.onWebsocketClosing(this, i, str, z);
        } catch (RuntimeException e) {
            this.f0.onWebsocketError(this, e);
        }
        Draft draft = this.l0;
        if (draft != null) {
            draft.q();
        }
        this.o0 = null;
    }

    public final ByteBuffer o(int i) {
        String str = i != 404 ? "500 Internal Server Error" : "404 WebSocket Upgrade Failure";
        return ByteBuffer.wrap(ay.a("HTTP/1.1 " + str + "\r\nContent-Type: text/html\nServer: TooTallNate Java-WebSocket\r\nContent-Length: " + (str.length() + 48) + "\r\n\r\n<html><head></head><body><h1>" + str + "</h1></body></html>"));
    }

    public long p() {
        return this.t0;
    }

    public WebSocket.READYSTATE q() {
        return this.j0;
    }

    public c r() {
        return this.f0;
    }

    public boolean s() {
        return q() == WebSocket.READYSTATE.CLOSED;
    }

    @Override // org.java_websocket.WebSocket
    public void sendFrame(Framedata framedata) {
        x(Collections.singletonList(framedata));
    }

    public boolean t() {
        return q() == WebSocket.READYSTATE.CLOSING;
    }

    public String toString() {
        return super.toString();
    }

    public boolean u() {
        return q() == WebSocket.READYSTATE.OPEN;
    }

    public final void v(tj1 tj1Var) {
        if (w0) {
            PrintStream printStream = System.out;
            printStream.println("open using draft: " + this.l0);
        }
        z(WebSocket.READYSTATE.OPEN);
        try {
            this.f0.onWebsocketOpen(this, tj1Var);
        } catch (RuntimeException e) {
            this.f0.onWebsocketError(this, e);
        }
    }

    public void w(String str) throws WebsocketNotConnectedException {
        if (str == null) {
            throw new IllegalArgumentException("Cannot send 'null' data to a WebSocketImpl.");
        }
        x(this.l0.g(str, this.m0 == WebSocket.Role.CLIENT));
    }

    public final void x(Collection<Framedata> collection) {
        if (!u()) {
            throw new WebsocketNotConnectedException();
        }
        if (collection != null) {
            ArrayList arrayList = new ArrayList();
            for (Framedata framedata : collection) {
                if (w0) {
                    PrintStream printStream = System.out;
                    printStream.println("send frame: " + framedata);
                }
                arrayList.add(this.l0.f(framedata));
            }
            D(arrayList);
            return;
        }
        throw new IllegalArgumentException();
    }

    public void y() throws NotYetConnectedException {
        if (this.u0 == null) {
            this.u0 = new g();
        }
        sendFrame(this.u0);
    }

    public final void z(WebSocket.READYSTATE readystate) {
        this.j0 = readystate;
    }
}
