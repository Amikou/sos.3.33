package org.java_websocket.framing;

import org.java_websocket.framing.Framedata;

/* compiled from: PongFrame.java */
/* loaded from: classes2.dex */
public class h extends d {
    public h() {
        super(Framedata.Opcode.PONG);
    }

    public h(g gVar) {
        super(Framedata.Opcode.PONG);
        j(gVar.f());
    }
}
