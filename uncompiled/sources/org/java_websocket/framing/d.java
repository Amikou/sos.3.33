package org.java_websocket.framing;

import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidFrameException;
import org.java_websocket.framing.Framedata;

/* compiled from: ControlFrame.java */
/* loaded from: classes2.dex */
public abstract class d extends f {
    public d(Framedata.Opcode opcode) {
        super(opcode);
    }

    @Override // org.java_websocket.framing.f
    public void h() throws InvalidDataException {
        if (e()) {
            if (!a()) {
                if (!b()) {
                    if (d()) {
                        throw new InvalidFrameException("Control frame cant have rsv3==true set");
                    }
                    return;
                }
                throw new InvalidFrameException("Control frame cant have rsv2==true set");
            }
            throw new InvalidFrameException("Control frame cant have rsv1==true set");
        }
        throw new InvalidFrameException("Control frame cant have fin==false set");
    }
}
