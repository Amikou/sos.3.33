package org.java_websocket.framing;

import androidx.media3.common.PlaybackException;
import java.nio.ByteBuffer;
import okhttp3.internal.ws.WebSocketProtocol;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidFrameException;
import org.java_websocket.framing.Framedata;

/* compiled from: CloseFrame.java */
/* loaded from: classes2.dex */
public class b extends d {
    public int h;
    public String i;

    public b() {
        super(Framedata.Opcode.CLOSING);
        r("");
        q(1000);
    }

    @Override // org.java_websocket.framing.f, org.java_websocket.framing.Framedata
    public ByteBuffer f() {
        if (this.h == 1005) {
            return us.a();
        }
        return super.f();
    }

    @Override // org.java_websocket.framing.d, org.java_websocket.framing.f
    public void h() throws InvalidDataException {
        super.h();
        int i = this.h;
        if (i == 1007 && this.i == null) {
            throw new InvalidDataException(1007, "Received text is no valid utf8 string!");
        }
        if (i == 1005 && this.i.length() > 0) {
            throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "A close frame must have a closecode if it has a reason");
        }
        int i2 = this.h;
        if (i2 > 1015 && i2 < 3000) {
            throw new InvalidDataException((int) PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, "Trying to send an illegal close code!");
        }
        if (i2 == 1006 || i2 == 1015 || i2 == 1005 || i2 > 4999 || i2 < 1000 || i2 == 1004) {
            throw new InvalidFrameException("closecode must not be sent over the wire: " + this.h);
        }
    }

    @Override // org.java_websocket.framing.f
    public void j(ByteBuffer byteBuffer) {
        this.h = WebSocketProtocol.CLOSE_NO_STATUS_CODE;
        this.i = "";
        byteBuffer.mark();
        if (byteBuffer.remaining() == 0) {
            this.h = 1000;
        } else if (byteBuffer.remaining() == 1) {
            this.h = PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW;
        } else {
            if (byteBuffer.remaining() >= 2) {
                ByteBuffer allocate = ByteBuffer.allocate(4);
                allocate.position(2);
                allocate.putShort(byteBuffer.getShort());
                allocate.position(0);
                this.h = allocate.getInt();
            }
            byteBuffer.reset();
            try {
                int position = byteBuffer.position();
                try {
                    byteBuffer.position(byteBuffer.position() + 2);
                    this.i = ay.e(byteBuffer);
                    byteBuffer.position(position);
                } catch (IllegalArgumentException unused) {
                    throw new InvalidDataException(1007);
                }
            } catch (InvalidDataException unused2) {
                this.h = 1007;
                this.i = null;
            }
        }
    }

    public int o() {
        return this.h;
    }

    public String p() {
        return this.i;
    }

    public void q(int i) {
        this.h = i;
        if (i == 1015) {
            this.h = WebSocketProtocol.CLOSE_NO_STATUS_CODE;
            this.i = "";
        }
        s();
    }

    public void r(String str) {
        if (str == null) {
            str = "";
        }
        this.i = str;
        s();
    }

    public final void s() {
        byte[] f = ay.f(this.i);
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(this.h);
        allocate.position(2);
        ByteBuffer allocate2 = ByteBuffer.allocate(f.length + 2);
        allocate2.put(allocate);
        allocate2.put(f);
        allocate2.rewind();
        super.j(allocate2);
    }

    @Override // org.java_websocket.framing.f
    public String toString() {
        return super.toString() + "code: " + this.h;
    }
}
