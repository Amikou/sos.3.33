package org.java_websocket.framing;

import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.framing.Framedata;

/* compiled from: TextFrame.java */
/* loaded from: classes2.dex */
public class i extends e {
    public i() {
        super(Framedata.Opcode.TEXT);
    }

    @Override // org.java_websocket.framing.e, org.java_websocket.framing.f
    public void h() throws InvalidDataException {
        super.h();
        if (!ay.b(f())) {
            throw new InvalidDataException(1007, "Received text is no valid utf8 string!");
        }
    }
}
