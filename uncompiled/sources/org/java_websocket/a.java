package org.java_websocket;

import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.g;
import org.java_websocket.framing.h;

/* compiled from: WebSocketAdapter.java */
/* loaded from: classes2.dex */
public abstract class a implements c {
    @Override // org.java_websocket.c
    public void onWebsocketHandshakeReceivedAsClient(WebSocket webSocket, hz hzVar, bm3 bm3Var) throws InvalidDataException {
    }

    @Override // org.java_websocket.c
    public cm3 onWebsocketHandshakeReceivedAsServer(WebSocket webSocket, Draft draft, hz hzVar) throws InvalidDataException {
        return new sj1();
    }

    @Override // org.java_websocket.c
    public void onWebsocketHandshakeSentAsClient(WebSocket webSocket, hz hzVar) throws InvalidDataException {
    }

    @Override // org.java_websocket.c
    public void onWebsocketPing(WebSocket webSocket, Framedata framedata) {
        webSocket.sendFrame(new h((g) framedata));
    }

    @Override // org.java_websocket.c
    public void onWebsocketPong(WebSocket webSocket, Framedata framedata) {
    }
}
