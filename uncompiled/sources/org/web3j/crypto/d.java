package org.web3j.crypto;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.web3j.crypto.c;

/* compiled from: TransactionEncoder.java */
/* loaded from: classes3.dex */
public class d {
    private static final int CHAIN_ID_INC = 35;
    private static final int LOWER_REAL_V = 27;

    public static List<g93> asRlpValues(y33 y33Var, c.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(f93.create(y33Var.getNonce()));
        arrayList.add(f93.create(y33Var.getGasPrice()));
        arrayList.add(f93.create(y33Var.getGasLimit()));
        String to = y33Var.getTo();
        if (to != null && to.length() > 0) {
            arrayList.add(f93.create(ej2.hexStringToByteArray(to)));
        } else {
            arrayList.add(f93.create(""));
        }
        arrayList.add(f93.create(y33Var.getValue()));
        arrayList.add(f93.create(ej2.hexStringToByteArray(y33Var.getData())));
        if (y33Var.isEIP1559Transaction()) {
            arrayList.add(f93.create(y33Var.getGasPremium()));
            arrayList.add(f93.create(y33Var.getFeeCap()));
        }
        if (aVar != null) {
            arrayList.add(f93.create(zs.trimLeadingZeroes(aVar.getV())));
            arrayList.add(f93.create(zs.trimLeadingZeroes(aVar.getR())));
            arrayList.add(f93.create(zs.trimLeadingZeroes(aVar.getS())));
        }
        return arrayList;
    }

    public static c.a createEip155SignatureData(c.a aVar, long j) {
        return new c.a(ej2.toBigInt(aVar.getV()).subtract(BigInteger.valueOf(27L)).add(BigInteger.valueOf(j * 2)).add(BigInteger.valueOf(35L)).toByteArray(), aVar.getR(), aVar.getS());
    }

    public static byte[] encode(y33 y33Var) {
        return encode(y33Var, (c.a) null);
    }

    private static byte[] longToBytes(long j) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.putLong(j);
        return allocate.array();
    }

    public static byte[] signMessage(y33 y33Var, ma0 ma0Var) {
        return encode(y33Var, c.signMessage(encode(y33Var), ma0Var.getEcKeyPair()));
    }

    public static byte[] encode(y33 y33Var, long j) {
        return encode(y33Var, new c.a(longToBytes(j), new byte[0], new byte[0]));
    }

    public static byte[] signMessage(y33 y33Var, long j, ma0 ma0Var) {
        return encode(y33Var, createEip155SignatureData(c.signMessage(encode(y33Var, j), ma0Var.getEcKeyPair()), j));
    }

    @Deprecated
    public static byte[] encode(y33 y33Var, byte b) {
        return encode(y33Var, b);
    }

    @Deprecated
    public static c.a createEip155SignatureData(c.a aVar, byte b) {
        return createEip155SignatureData(aVar, b);
    }

    private static byte[] encode(y33 y33Var, c.a aVar) {
        return d93.encode(new e93(asRlpValues(y33Var, aVar)));
    }

    @Deprecated
    public static byte[] signMessage(y33 y33Var, byte b, ma0 ma0Var) {
        return signMessage(y33Var, b, ma0Var);
    }
}
