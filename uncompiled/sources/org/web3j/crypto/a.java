package org.web3j.crypto;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.util.Arrays;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/* compiled from: Keys.java */
/* loaded from: classes3.dex */
public class a {
    public static final int ADDRESS_LENGTH_IN_HEX = 40;
    public static final int ADDRESS_SIZE = 160;
    public static final int PRIVATE_KEY_LENGTH_IN_HEX = 64;
    public static final int PRIVATE_KEY_SIZE = 32;
    public static final int PUBLIC_KEY_LENGTH_IN_HEX = 128;
    public static final int PUBLIC_KEY_SIZE = 64;

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    private a() {
    }

    public static et0 createEcKeyPair() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        return createEcKeyPair(zh3.secureRandom());
    }

    public static KeyPair createSecp256k1KeyPair() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        return createSecp256k1KeyPair(zh3.secureRandom());
    }

    public static et0 deserialize(byte[] bArr) {
        if (bArr.length == 96) {
            return new et0(ej2.toBigInt(bArr, 0, 32), ej2.toBigInt(bArr, 32, 64));
        }
        throw new RuntimeException("Invalid input key size");
    }

    public static String getAddress(et0 et0Var) {
        return getAddress(et0Var.getPublicKey());
    }

    public static byte[] serialize(et0 et0Var) {
        byte[] bytesPadded = ej2.toBytesPadded(et0Var.getPrivateKey(), 32);
        byte[] bytesPadded2 = ej2.toBytesPadded(et0Var.getPublicKey(), 64);
        byte[] copyOf = Arrays.copyOf(bytesPadded, 96);
        System.arraycopy(bytesPadded2, 0, copyOf, 32, 64);
        return copyOf;
    }

    public static String toChecksumAddress(String str) {
        String lowerCase = ej2.cleanHexPrefix(str).toLowerCase();
        String cleanHexPrefix = ej2.cleanHexPrefix(ak1.sha3String(lowerCase));
        StringBuilder sb = new StringBuilder(lowerCase.length() + 2);
        sb.append("0x");
        for (int i = 0; i < lowerCase.length(); i++) {
            if (Integer.parseInt(String.valueOf(cleanHexPrefix.charAt(i)), 16) >= 8) {
                sb.append(String.valueOf(lowerCase.charAt(i)).toUpperCase());
            } else {
                sb.append(lowerCase.charAt(i));
            }
        }
        return sb.toString();
    }

    public static et0 createEcKeyPair(SecureRandom secureRandom) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        return et0.create(createSecp256k1KeyPair(secureRandom));
    }

    public static KeyPair createSecp256k1KeyPair(SecureRandom secureRandom) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("ECDSA", BouncyCastleProvider.PROVIDER_NAME);
        ECGenParameterSpec eCGenParameterSpec = new ECGenParameterSpec("secp256k1");
        if (secureRandom != null) {
            keyPairGenerator.initialize(eCGenParameterSpec, secureRandom);
        } else {
            keyPairGenerator.initialize(eCGenParameterSpec);
        }
        return keyPairGenerator.generateKeyPair();
    }

    public static String getAddress(BigInteger bigInteger) {
        return getAddress(ej2.toHexStringWithPrefixZeroPadded(bigInteger, 128));
    }

    public static String getAddress(String str) {
        String sha3;
        String cleanHexPrefix = ej2.cleanHexPrefix(str);
        if (cleanHexPrefix.length() < 128) {
            cleanHexPrefix = uu3.zeros(128 - cleanHexPrefix.length()) + cleanHexPrefix;
        }
        return ak1.sha3(cleanHexPrefix).substring(sha3.length() - 40);
    }

    public static byte[] getAddress(byte[] bArr) {
        byte[] sha3 = ak1.sha3(bArr);
        return Arrays.copyOfRange(sha3, sha3.length - 20, sha3.length);
    }
}
