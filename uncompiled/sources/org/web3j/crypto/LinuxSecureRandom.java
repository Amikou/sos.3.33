package org.web3j.crypto;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Provider;
import java.security.SecureRandomSpi;
import java.security.Security;

/* loaded from: classes3.dex */
public class LinuxSecureRandom extends SecureRandomSpi {
    private static final x12 log;
    private static final FileInputStream urandom;
    private final DataInputStream dis = new DataInputStream(urandom);

    /* loaded from: classes3.dex */
    public static class LinuxSecureRandomProvider extends Provider {
        public LinuxSecureRandomProvider() {
            super("LinuxSecureRandom", 1.0d, "A Linux specific random number provider that uses /dev/urandom");
            put("SecureRandom.LinuxSecureRandom", LinuxSecureRandom.class.getName());
        }
    }

    static {
        x12 i = org.slf4j.a.i(LinuxSecureRandom.class);
        log = i;
        try {
            File file = new File("/dev/urandom");
            FileInputStream fileInputStream = new FileInputStream(file);
            urandom = fileInputStream;
            if (fileInputStream.read() != -1) {
                if (Security.insertProviderAt(new LinuxSecureRandomProvider(), 1) != -1) {
                    i.info("Secure randomness will be read from {} only.", file);
                    return;
                } else {
                    i.info("Randomness is already secure.");
                    return;
                }
            }
            throw new RuntimeException("/dev/urandom not readable?");
        } catch (FileNotFoundException e) {
            log.error("/dev/urandom does not appear to exist or is not openable");
            throw new RuntimeException(e);
        } catch (IOException e2) {
            log.error("/dev/urandom does not appear to be readable");
            throw new RuntimeException(e2);
        }
    }

    @Override // java.security.SecureRandomSpi
    public byte[] engineGenerateSeed(int i) {
        byte[] bArr = new byte[i];
        engineNextBytes(bArr);
        return bArr;
    }

    @Override // java.security.SecureRandomSpi
    public void engineNextBytes(byte[] bArr) {
        try {
            this.dis.readFully(bArr);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override // java.security.SecureRandomSpi
    public void engineSetSeed(byte[] bArr) {
    }
}
