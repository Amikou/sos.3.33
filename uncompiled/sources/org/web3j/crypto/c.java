package org.web3j.crypto;

import java.math.BigInteger;
import java.security.SignatureException;
import java.util.Arrays;

/* compiled from: Sign.java */
/* loaded from: classes3.dex */
public class c {
    public static final at0 CURVE;
    public static final pr4 CURVE_PARAMS;
    public static final BigInteger HALF_CURVE_ORDER;
    public static final String MESSAGE_PREFIX = "\u0019Ethereum Signed Message:\n";

    /* compiled from: Sign.java */
    /* loaded from: classes3.dex */
    public static class a {
        private final byte[] r;
        private final byte[] s;
        private final byte[] v;

        public a(byte b, byte[] bArr, byte[] bArr2) {
            this(new byte[]{b}, bArr, bArr2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (Arrays.equals(this.v, aVar.v) && Arrays.equals(this.r, aVar.r)) {
                return Arrays.equals(this.s, aVar.s);
            }
            return false;
        }

        public byte[] getR() {
            return this.r;
        }

        public byte[] getS() {
            return this.s;
        }

        public byte[] getV() {
            return this.v;
        }

        public int hashCode() {
            return (((Arrays.hashCode(this.v) * 31) + Arrays.hashCode(this.r)) * 31) + Arrays.hashCode(this.s);
        }

        public a(byte[] bArr, byte[] bArr2, byte[] bArr3) {
            this.v = bArr;
            this.r = bArr2;
            this.s = bArr3;
        }
    }

    static {
        pr4 h = hc0.h("secp256k1");
        CURVE_PARAMS = h;
        CURVE = new at0(h.o(), h.p(), h.t(), h.q());
        HALF_CURVE_ORDER = h.t().shiftRight(1);
    }

    private static pt0 decompressKey(BigInteger bigInteger, boolean z) {
        ur4 ur4Var = new ur4();
        at0 at0Var = CURVE;
        byte[] c = ur4Var.c(bigInteger, ur4Var.a(at0Var.a()) + 1);
        c[0] = (byte) (z ? 3 : 2);
        return at0Var.a().k(c);
    }

    public static byte[] getEthereumMessageHash(byte[] bArr) {
        byte[] ethereumMessagePrefix = getEthereumMessagePrefix(bArr.length);
        byte[] bArr2 = new byte[ethereumMessagePrefix.length + bArr.length];
        System.arraycopy(ethereumMessagePrefix, 0, bArr2, 0, ethereumMessagePrefix.length);
        System.arraycopy(bArr, 0, bArr2, ethereumMessagePrefix.length, bArr.length);
        return ak1.sha3(bArr2);
    }

    public static byte[] getEthereumMessagePrefix(int i) {
        return MESSAGE_PREFIX.concat(String.valueOf(i)).getBytes();
    }

    public static BigInteger publicFromPoint(byte[] bArr) {
        return new BigInteger(1, Arrays.copyOfRange(bArr, 1, bArr.length));
    }

    public static BigInteger publicKeyFromPrivate(BigInteger bigInteger) {
        byte[] l = publicPointFromPrivate(bigInteger).l(false);
        return new BigInteger(1, Arrays.copyOfRange(l, 1, l.length));
    }

    public static pt0 publicPointFromPrivate(BigInteger bigInteger) {
        int bitLength = bigInteger.bitLength();
        at0 at0Var = CURVE;
        if (bitLength > at0Var.d().bitLength()) {
            bigInteger = bigInteger.mod(at0Var.d());
        }
        return new f61().a(at0Var.b(), bigInteger);
    }

    public static BigInteger recoverFromSignature(int i, ys0 ys0Var, byte[] bArr) {
        hi.verifyPrecondition(i >= 0, "recId must be positive");
        hi.verifyPrecondition(ys0Var.r.signum() >= 0, "r must be positive");
        hi.verifyPrecondition(ys0Var.s.signum() >= 0, "s must be positive");
        hi.verifyPrecondition(bArr != null, "message cannot be null");
        at0 at0Var = CURVE;
        BigInteger d = at0Var.d();
        BigInteger add = ys0Var.r.add(BigInteger.valueOf(i / 2).multiply(d));
        if (add.compareTo(ef3.j) >= 0) {
            return null;
        }
        pt0 decompressKey = decompressKey(add, (i & 1) == 1);
        if (decompressKey.y(d).u()) {
            BigInteger mod = BigInteger.ZERO.subtract(new BigInteger(1, bArr)).mod(d);
            BigInteger modInverse = ys0Var.r.modInverse(d);
            byte[] l = vs0.p(at0Var.b(), modInverse.multiply(mod).mod(d), decompressKey, modInverse.multiply(ys0Var.s).mod(d)).l(false);
            return new BigInteger(1, Arrays.copyOfRange(l, 1, l.length));
        }
        return null;
    }

    public static a signMessage(byte[] bArr, et0 et0Var) {
        return signMessage(bArr, et0Var, true);
    }

    public static a signPrefixedMessage(byte[] bArr, et0 et0Var) {
        return signMessage(getEthereumMessageHash(bArr), et0Var, false);
    }

    public static BigInteger signedMessageHashToKey(byte[] bArr, a aVar) throws SignatureException {
        byte[] r = aVar.getR();
        byte[] s = aVar.getS();
        hi.verifyPrecondition(r != null && r.length == 32, "r must be 32 bytes");
        hi.verifyPrecondition(s != null && s.length == 32, "s must be 32 bytes");
        int i = aVar.getV()[0] & 255;
        if (i >= 27 && i <= 34) {
            BigInteger recoverFromSignature = recoverFromSignature(i - 27, new ys0(new BigInteger(1, aVar.getR()), new BigInteger(1, aVar.getS())), bArr);
            if (recoverFromSignature != null) {
                return recoverFromSignature;
            }
            throw new SignatureException("Could not recover public key from signature");
        }
        throw new SignatureException("Header byte out of range: " + i);
    }

    public static BigInteger signedMessageToKey(byte[] bArr, a aVar) throws SignatureException {
        return signedMessageHashToKey(ak1.sha3(bArr), aVar);
    }

    public static BigInteger signedPrefixedMessageToKey(byte[] bArr, a aVar) throws SignatureException {
        return signedMessageHashToKey(getEthereumMessageHash(bArr), aVar);
    }

    public static a signMessage(byte[] bArr, et0 et0Var, boolean z) {
        BigInteger publicKey = et0Var.getPublicKey();
        if (z) {
            bArr = ak1.sha3(bArr);
        }
        ys0 sign = et0Var.sign(bArr);
        int i = 0;
        while (true) {
            if (i >= 4) {
                i = -1;
                break;
            }
            BigInteger recoverFromSignature = recoverFromSignature(i, sign, bArr);
            if (recoverFromSignature != null && recoverFromSignature.equals(publicKey)) {
                break;
            }
            i++;
        }
        if (i != -1) {
            return new a(new byte[]{(byte) (i + 27)}, ej2.toBytesPadded(sign.r, 32), ej2.toBytesPadded(sign.s, 32));
        }
        throw new RuntimeException("Could not construct a recoverable key. Are your credentials valid?");
    }
}
