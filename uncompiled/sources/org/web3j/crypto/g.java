package org.web3j.crypto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/* compiled from: WalletUtils.java */
/* loaded from: classes3.dex */
public class g {
    private static final ObjectMapper objectMapper;
    private static final SecureRandom secureRandom;

    static {
        ObjectMapper objectMapper2 = new ObjectMapper();
        objectMapper = objectMapper2;
        secureRandom = zh3.secureRandom();
        objectMapper2.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper2.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static op generateBip39Wallet(String str, File file) throws CipherException, IOException {
        byte[] bArr = new byte[16];
        secureRandom.nextBytes(bArr);
        String generateMnemonic = b.generateMnemonic(bArr);
        return new op(generateWalletFile(str, et0.create(ak1.sha256(b.generateSeed(generateMnemonic, str))), file, false), generateMnemonic);
    }

    public static op generateBip39WalletFromMnemonic(String str, String str2, File file) throws CipherException, IOException {
        return new op(generateWalletFile(str, et0.create(ak1.sha256(b.generateSeed(str2, str))), file, false), str2);
    }

    public static String generateFullNewWalletFile(String str, File file) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, CipherException, IOException {
        return generateNewWalletFile(str, file, true);
    }

    public static String generateLightNewWalletFile(String str, File file) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, CipherException, IOException {
        return generateNewWalletFile(str, file, false);
    }

    public static String generateNewWalletFile(String str, File file) throws CipherException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
        return generateFullNewWalletFile(str, file);
    }

    public static String generateWalletFile(String str, et0 et0Var, File file, boolean z) throws CipherException, IOException {
        f createLight;
        if (z) {
            createLight = e.createStandard(str, et0Var);
        } else {
            createLight = e.createLight(str, et0Var);
        }
        String walletFileName = getWalletFileName(createLight);
        objectMapper.writeValue(new File(file, walletFileName), createLight);
        return walletFileName;
    }

    public static String getDefaultKeyDirectory() {
        return getDefaultKeyDirectory(System.getProperty("os.name"));
    }

    public static String getMainnetKeyDirectory() {
        return String.format("%s%skeystore", getDefaultKeyDirectory(), File.separator);
    }

    public static String getRinkebyKeyDirectory() {
        String str = File.separator;
        return String.format("%s%srinkeby%skeystore", getDefaultKeyDirectory(), str, str);
    }

    public static String getTestnetKeyDirectory() {
        String str = File.separator;
        return String.format("%s%stestnet%skeystore", getDefaultKeyDirectory(), str, str);
    }

    private static String getWalletFileName(f fVar) {
        return timestamp(new Date()) + fVar.getAddress() + ".json";
    }

    public static boolean isValidAddress(String str) {
        return isValidAddress(str, 40);
    }

    public static boolean isValidPrivateKey(String str) {
        return ej2.cleanHexPrefix(str).length() == 64;
    }

    public static ma0 loadBip39Credentials(String str, String str2) {
        return ma0.create(et0.create(ak1.sha256(b.generateSeed(str2, str))));
    }

    public static ma0 loadCredentials(String str, String str2) throws IOException, CipherException {
        return loadCredentials(str, new File(str2));
    }

    public static ma0 loadJsonCredentials(String str, String str2) throws IOException, CipherException {
        return ma0.create(e.decrypt(str, (f) objectMapper.readValue(str2, f.class)));
    }

    public static String timestamp(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("'UTC--'yyyy-MM-dd'T'HH-mm-ss.S'Z--'");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(date);
    }

    public static String generateNewWalletFile(String str, File file, boolean z) throws CipherException, IOException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        return generateWalletFile(str, a.createEcKeyPair(), file, z);
    }

    public static String getDefaultKeyDirectory(String str) {
        String lowerCase = str.toLowerCase();
        if (!lowerCase.startsWith("mac")) {
            return lowerCase.startsWith("win") ? String.format("%s%sEthereum", System.getenv("APPDATA"), File.separator) : String.format("%s%s.ethereum", System.getProperty("user.home"), File.separator);
        }
        String str2 = File.separator;
        return String.format("%s%sLibrary%sEthereum", System.getProperty("user.home"), str2, str2);
    }

    public static boolean isValidAddress(String str, int i) {
        String cleanHexPrefix = ej2.cleanHexPrefix(str);
        try {
            ej2.toBigIntNoPrefix(cleanHexPrefix);
            return cleanHexPrefix.length() == i;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    public static ma0 loadCredentials(String str, File file) throws IOException, CipherException {
        return ma0.create(e.decrypt(str, (f) objectMapper.readValue(file, f.class)));
    }
}
