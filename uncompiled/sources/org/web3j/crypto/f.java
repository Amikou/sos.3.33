package org.web3j.crypto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.n;
import com.fasterxml.jackson.annotation.o;

/* compiled from: WalletFile.java */
/* loaded from: classes3.dex */
public class f {
    private String address;
    private c crypto;
    private String id;
    private int version;

    /* compiled from: WalletFile.java */
    /* loaded from: classes3.dex */
    public static class a implements d {
        private int c;
        private int dklen;
        private String prf;
        private String salt;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (this.dklen == aVar.dklen && this.c == aVar.c) {
                    if (getPrf() == null ? aVar.getPrf() == null : getPrf().equals(aVar.getPrf())) {
                        return getSalt() != null ? getSalt().equals(aVar.getSalt()) : aVar.getSalt() == null;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public int getC() {
            return this.c;
        }

        @Override // org.web3j.crypto.f.d
        public int getDklen() {
            return this.dklen;
        }

        public String getPrf() {
            return this.prf;
        }

        @Override // org.web3j.crypto.f.d
        public String getSalt() {
            return this.salt;
        }

        public int hashCode() {
            return (((((this.dklen * 31) + this.c) * 31) + (getPrf() != null ? getPrf().hashCode() : 0)) * 31) + (getSalt() != null ? getSalt().hashCode() : 0);
        }

        public void setC(int i) {
            this.c = i;
        }

        public void setDklen(int i) {
            this.dklen = i;
        }

        public void setPrf(String str) {
            this.prf = str;
        }

        public void setSalt(String str) {
            this.salt = str;
        }
    }

    /* compiled from: WalletFile.java */
    /* loaded from: classes3.dex */
    public static class b {
        private String iv;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof b) {
                b bVar = (b) obj;
                return getIv() != null ? getIv().equals(bVar.getIv()) : bVar.getIv() == null;
            }
            return false;
        }

        public String getIv() {
            return this.iv;
        }

        public int hashCode() {
            if (getIv() != null) {
                return getIv().hashCode();
            }
            return 0;
        }

        public void setIv(String str) {
            this.iv = str;
        }
    }

    /* compiled from: WalletFile.java */
    /* loaded from: classes3.dex */
    public static class c {
        private String cipher;
        private b cipherparams;
        private String ciphertext;
        private String kdf;
        private d kdfparams;
        private String mac;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                if (getCipher() == null ? cVar.getCipher() == null : getCipher().equals(cVar.getCipher())) {
                    if (getCiphertext() == null ? cVar.getCiphertext() == null : getCiphertext().equals(cVar.getCiphertext())) {
                        if (getCipherparams() == null ? cVar.getCipherparams() == null : getCipherparams().equals(cVar.getCipherparams())) {
                            if (getKdf() == null ? cVar.getKdf() == null : getKdf().equals(cVar.getKdf())) {
                                if (getKdfparams() == null ? cVar.getKdfparams() == null : getKdfparams().equals(cVar.getKdfparams())) {
                                    return getMac() != null ? getMac().equals(cVar.getMac()) : cVar.getMac() == null;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public String getCipher() {
            return this.cipher;
        }

        public b getCipherparams() {
            return this.cipherparams;
        }

        public String getCiphertext() {
            return this.ciphertext;
        }

        public String getKdf() {
            return this.kdf;
        }

        public d getKdfparams() {
            return this.kdfparams;
        }

        public String getMac() {
            return this.mac;
        }

        public int hashCode() {
            return ((((((((((getCipher() != null ? getCipher().hashCode() : 0) * 31) + (getCiphertext() != null ? getCiphertext().hashCode() : 0)) * 31) + (getCipherparams() != null ? getCipherparams().hashCode() : 0)) * 31) + (getKdf() != null ? getKdf().hashCode() : 0)) * 31) + (getKdfparams() != null ? getKdfparams().hashCode() : 0)) * 31) + (getMac() != null ? getMac().hashCode() : 0);
        }

        public void setCipher(String str) {
            this.cipher = str;
        }

        public void setCipherparams(b bVar) {
            this.cipherparams = bVar;
        }

        public void setCiphertext(String str) {
            this.ciphertext = str;
        }

        public void setKdf(String str) {
            this.kdf = str;
        }

        @JsonTypeInfo(include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "kdf", use = JsonTypeInfo.Id.NAME)
        @o({@o.a(name = org.web3j.crypto.e.AES_128_CTR, value = a.class), @o.a(name = org.web3j.crypto.e.SCRYPT, value = e.class)})
        public void setKdfparams(d dVar) {
            this.kdfparams = dVar;
        }

        public void setMac(String str) {
            this.mac = str;
        }
    }

    /* compiled from: WalletFile.java */
    /* loaded from: classes3.dex */
    public interface d {
        int getDklen();

        String getSalt();
    }

    /* compiled from: WalletFile.java */
    /* loaded from: classes3.dex */
    public static class e implements d {
        private int dklen;
        private int n;
        private int p;
        private int r;
        private String salt;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof e) {
                e eVar = (e) obj;
                if (this.dklen == eVar.dklen && this.n == eVar.n && this.p == eVar.p && this.r == eVar.r) {
                    return getSalt() != null ? getSalt().equals(eVar.getSalt()) : eVar.getSalt() == null;
                }
                return false;
            }
            return false;
        }

        @Override // org.web3j.crypto.f.d
        public int getDklen() {
            return this.dklen;
        }

        public int getN() {
            return this.n;
        }

        public int getP() {
            return this.p;
        }

        public int getR() {
            return this.r;
        }

        @Override // org.web3j.crypto.f.d
        public String getSalt() {
            return this.salt;
        }

        public int hashCode() {
            return (((((((this.dklen * 31) + this.n) * 31) + this.p) * 31) + this.r) * 31) + (getSalt() != null ? getSalt().hashCode() : 0);
        }

        public void setDklen(int i) {
            this.dklen = i;
        }

        public void setN(int i) {
            this.n = i;
        }

        public void setP(int i) {
            this.p = i;
        }

        public void setR(int i) {
            this.r = i;
        }

        public void setSalt(String str) {
            this.salt = str;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof f) {
            f fVar = (f) obj;
            if (getAddress() == null ? fVar.getAddress() == null : getAddress().equals(fVar.getAddress())) {
                if (getCrypto() == null ? fVar.getCrypto() == null : getCrypto().equals(fVar.getCrypto())) {
                    if (getId() == null ? fVar.getId() == null : getId().equals(fVar.getId())) {
                        return this.version == fVar.version;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public String getAddress() {
        return this.address;
    }

    public c getCrypto() {
        return this.crypto;
    }

    public String getId() {
        return this.id;
    }

    public int getVersion() {
        return this.version;
    }

    public int hashCode() {
        return ((((((getAddress() != null ? getAddress().hashCode() : 0) * 31) + (getCrypto() != null ? getCrypto().hashCode() : 0)) * 31) + (getId() != null ? getId().hashCode() : 0)) * 31) + this.version;
    }

    public void setAddress(String str) {
        this.address = str;
    }

    @n("crypto")
    public void setCrypto(c cVar) {
        this.crypto = cVar;
    }

    @n("Crypto")
    public void setCryptoV1(c cVar) {
        setCrypto(cVar);
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setVersion(int i) {
        this.version = i;
    }
}
