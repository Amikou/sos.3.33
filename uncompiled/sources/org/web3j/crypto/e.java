package org.web3j.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.web3j.crypto.f;

/* compiled from: Wallet.java */
/* loaded from: classes3.dex */
public class e {
    public static final String AES_128_CTR = "pbkdf2";
    private static final String CIPHER = "aes-128-ctr";
    private static final int CURRENT_VERSION = 3;
    private static final int DKLEN = 32;
    private static final int N_LIGHT = 4096;
    private static final int N_STANDARD = 262144;
    private static final int P_LIGHT = 6;
    private static final int P_STANDARD = 1;
    private static final int R = 8;
    public static final String SCRYPT = "scrypt";

    public static f create(String str, et0 et0Var, int i, int i2) throws CipherException {
        byte[] generateRandomBytes = generateRandomBytes(32);
        byte[] generateDerivedScryptKey = generateDerivedScryptKey(str.getBytes(m30.UTF_8), generateRandomBytes, i, 8, i2, 32);
        byte[] copyOfRange = Arrays.copyOfRange(generateDerivedScryptKey, 0, 16);
        byte[] generateRandomBytes2 = generateRandomBytes(16);
        byte[] performCipherOperation = performCipherOperation(1, generateRandomBytes2, copyOfRange, ej2.toBytesPadded(et0Var.getPrivateKey(), 32));
        return createWalletFile(et0Var, performCipherOperation, generateRandomBytes2, generateRandomBytes, generateMac(generateDerivedScryptKey, performCipherOperation), i, i2);
    }

    public static f createLight(String str, et0 et0Var) throws CipherException {
        return create(str, et0Var, 4096, 6);
    }

    public static f createStandard(String str, et0 et0Var) throws CipherException {
        return create(str, et0Var, N_STANDARD, 1);
    }

    private static f createWalletFile(et0 et0Var, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, int i, int i2) {
        f fVar = new f();
        fVar.setAddress(a.getAddress(et0Var));
        f.c cVar = new f.c();
        cVar.setCipher(CIPHER);
        cVar.setCiphertext(ej2.toHexStringNoPrefix(bArr));
        f.b bVar = new f.b();
        bVar.setIv(ej2.toHexStringNoPrefix(bArr2));
        cVar.setCipherparams(bVar);
        cVar.setKdf(SCRYPT);
        f.e eVar = new f.e();
        eVar.setDklen(32);
        eVar.setN(i);
        eVar.setP(i2);
        eVar.setR(8);
        eVar.setSalt(ej2.toHexStringNoPrefix(bArr3));
        cVar.setKdfparams(eVar);
        cVar.setMac(ej2.toHexStringNoPrefix(bArr4));
        fVar.setCrypto(cVar);
        fVar.setId(UUID.randomUUID().toString());
        fVar.setVersion(3);
        return fVar;
    }

    public static et0 decrypt(String str, f fVar) throws CipherException {
        byte[] generateAes128CtrDerivedKey;
        validate(fVar);
        f.c crypto = fVar.getCrypto();
        byte[] hexStringToByteArray = ej2.hexStringToByteArray(crypto.getMac());
        byte[] hexStringToByteArray2 = ej2.hexStringToByteArray(crypto.getCipherparams().getIv());
        byte[] hexStringToByteArray3 = ej2.hexStringToByteArray(crypto.getCiphertext());
        f.d kdfparams = crypto.getKdfparams();
        if (kdfparams instanceof f.e) {
            f.e eVar = (f.e) crypto.getKdfparams();
            int dklen = eVar.getDklen();
            int n = eVar.getN();
            int p = eVar.getP();
            int r = eVar.getR();
            generateAes128CtrDerivedKey = generateDerivedScryptKey(str.getBytes(m30.UTF_8), ej2.hexStringToByteArray(eVar.getSalt()), n, r, p, dklen);
        } else if (kdfparams instanceof f.a) {
            f.a aVar = (f.a) crypto.getKdfparams();
            int c = aVar.getC();
            String prf = aVar.getPrf();
            generateAes128CtrDerivedKey = generateAes128CtrDerivedKey(str.getBytes(m30.UTF_8), ej2.hexStringToByteArray(aVar.getSalt()), c, prf);
        } else {
            throw new CipherException("Unable to deserialize params: " + crypto.getKdf());
        }
        if (Arrays.equals(generateMac(generateAes128CtrDerivedKey, hexStringToByteArray3), hexStringToByteArray)) {
            return et0.create(performCipherOperation(2, hexStringToByteArray2, Arrays.copyOfRange(generateAes128CtrDerivedKey, 0, 16), hexStringToByteArray3));
        }
        throw new CipherException("Invalid password provided");
    }

    private static byte[] generateAes128CtrDerivedKey(byte[] bArr, byte[] bArr2, int i, String str) throws CipherException {
        if (str.equals("hmac-sha256")) {
            lo2 lo2Var = new lo2(new ka3());
            lo2Var.b(bArr, bArr2, i);
            return ((kx1) lo2Var.e(256)).a();
        }
        throw new CipherException("Unsupported prf:" + str);
    }

    private static byte[] generateDerivedScryptKey(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4) throws CipherException {
        return ea3.i(bArr, bArr2, i, i2, i3, i4);
    }

    private static byte[] generateMac(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[bArr2.length + 16];
        System.arraycopy(bArr, 16, bArr3, 0, 16);
        System.arraycopy(bArr2, 0, bArr3, 16, bArr2.length);
        return ak1.sha3(bArr3);
    }

    public static byte[] generateRandomBytes(int i) {
        byte[] bArr = new byte[i];
        zh3.secureRandom().nextBytes(bArr);
        return bArr;
    }

    private static byte[] performCipherOperation(int i, byte[] bArr, byte[] bArr2, byte[] bArr3) throws CipherException {
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
            cipher.init(i, new SecretKeySpec(bArr2, "AES"), ivParameterSpec);
            return cipher.doFinal(bArr3);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new CipherException("Error performing cipher operation", e);
        }
    }

    public static void validate(f fVar) throws CipherException {
        f.c crypto = fVar.getCrypto();
        if (fVar.getVersion() == 3) {
            if (crypto.getCipher().equals(CIPHER)) {
                if (!crypto.getKdf().equals(AES_128_CTR) && !crypto.getKdf().equals(SCRYPT)) {
                    throw new CipherException("KDF type is not supported");
                }
                return;
            }
            throw new CipherException("Wallet cipher is not supported");
        }
        throw new CipherException("Wallet version is not supported");
    }
}
