package org.web3j.crypto;

/* loaded from: classes3.dex */
public class CipherException extends Exception {
    public CipherException(String str) {
        super(str);
    }

    public CipherException(Throwable th) {
        super(th);
    }

    public CipherException(String str, Throwable th) {
        super(str, th);
    }
}
