package org.web3j.abi.datatypes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes3.dex */
public class StaticStruct extends StaticArray<zc4> implements fv3 {
    private final List<Class<zc4>> itemTypes;

    /* JADX WARN: Multi-variable type inference failed */
    public StaticStruct(List<zc4> list) {
        super(zc4.class, list.size(), list);
        this.itemTypes = new ArrayList();
        for (zc4 zc4Var : list) {
            this.itemTypes.add(zc4Var.getClass());
        }
    }

    @Override // org.web3j.abi.datatypes.StaticArray, org.web3j.abi.datatypes.Array, defpackage.zc4
    public String getTypeAsString() {
        StringBuilder sb = new StringBuilder("(");
        for (int i = 0; i < this.itemTypes.size(); i++) {
            Class<zc4> cls = this.itemTypes.get(i);
            if (fv3.class.isAssignableFrom(cls)) {
                sb.append(getValue().get(i).getTypeAsString());
            } else {
                sb.append(m4.getTypeAString(cls));
            }
            if (i < this.itemTypes.size() - 1) {
                sb.append(",");
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public StaticStruct(zc4... zc4VarArr) {
        this(Arrays.asList(zc4VarArr));
    }
}
