package org.web3j.abi.datatypes;

import defpackage.zc4;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class Array<T extends zc4> implements zc4<List<T>> {
    private final Class<T> type;
    public final List<T> value;

    @Deprecated
    public Array(String str, T... tArr) {
        this(str, Arrays.asList(tArr));
    }

    private void checkValid(Class<T> cls, List<T> list) {
        m30.requireNonNull(cls);
        m30.requireNonNull(list);
    }

    @Override // defpackage.zc4
    public int bytes32PaddedLength() {
        int i = 0;
        for (int i2 = 0; i2 < this.value.size(); i2++) {
            i += this.value.get(i2).bytes32PaddedLength();
        }
        return i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Array array = (Array) obj;
        if (this.type.equals(array.type)) {
            return m30.equals(this.value, array.value);
        }
        return false;
    }

    public Class<T> getComponentType() {
        return this.type;
    }

    @Override // defpackage.zc4
    public abstract String getTypeAsString();

    public int hashCode() {
        int hashCode = this.type.hashCode() * 31;
        List<T> list = this.value;
        return hashCode + (list != null ? list.hashCode() : 0);
    }

    @Deprecated
    public Array(String str, List<T> list) {
        this(m4.getType(str), list);
    }

    @Override // defpackage.zc4
    public List<T> getValue() {
        return this.value;
    }

    @Deprecated
    public Array(String str) {
        this(str, new ArrayList());
    }

    public Array(Class<T> cls, T... tArr) {
        this(cls, Arrays.asList(tArr));
    }

    public Array(Class<T> cls, List<T> list) {
        checkValid(cls, list);
        this.type = cls;
        this.value = list;
    }
}
