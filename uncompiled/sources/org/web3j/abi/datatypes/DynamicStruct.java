package org.web3j.abi.datatypes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes3.dex */
public class DynamicStruct extends DynamicArray<zc4> implements fv3 {
    private final List<Class<zc4>> itemTypes;

    public DynamicStruct(List<zc4> list) {
        this(zc4.class, list);
    }

    @Override // org.web3j.abi.datatypes.DynamicArray, org.web3j.abi.datatypes.Array, defpackage.zc4
    public int bytes32PaddedLength() {
        return super.bytes32PaddedLength() + 32;
    }

    @Override // org.web3j.abi.datatypes.DynamicArray, org.web3j.abi.datatypes.Array, defpackage.zc4
    public String getTypeAsString() {
        StringBuilder sb = new StringBuilder("(");
        for (int i = 0; i < this.itemTypes.size(); i++) {
            Class<zc4> cls = this.itemTypes.get(i);
            if (fv3.class.isAssignableFrom(cls)) {
                sb.append(((zc4) getValue().get(i)).getTypeAsString());
            } else {
                sb.append(m4.getTypeAString(cls));
            }
            if (i < this.itemTypes.size() - 1) {
                sb.append(",");
            }
        }
        sb.append(")");
        return sb.toString();
    }

    /* JADX WARN: Multi-variable type inference failed */
    private DynamicStruct(Class<zc4> cls, List<zc4> list) {
        super(cls, list);
        this.itemTypes = new ArrayList();
        for (zc4 zc4Var : list) {
            this.itemTypes.add(zc4Var.getClass());
        }
    }

    public DynamicStruct(zc4... zc4VarArr) {
        this(Arrays.asList(zc4VarArr));
    }

    public DynamicStruct(Class<zc4> cls, Type... typeArr) {
        this(cls, Arrays.asList(typeArr));
    }
}
