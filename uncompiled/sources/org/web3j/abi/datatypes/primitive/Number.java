package org.web3j.abi.datatypes.primitive;

import java.lang.Comparable;
import java.lang.Number;
import org.web3j.abi.datatypes.NumericType;

/* loaded from: classes3.dex */
public abstract class Number<T extends java.lang.Number & Comparable<T>> extends PrimitiveType<T> {
    /* JADX WARN: Multi-variable type inference failed */
    public Number(T t) {
        super(t);
    }

    @Override // org.web3j.abi.datatypes.primitive.PrimitiveType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.primitive.PrimitiveType
    public abstract NumericType toSolidityType();
}
