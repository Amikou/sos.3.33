package org.web3j.abi.datatypes.primitive;

import org.web3j.abi.datatypes.generated.Bytes1;

/* loaded from: classes3.dex */
public final class Byte extends PrimitiveType<java.lang.Byte> {
    public Byte(byte b) {
        super(java.lang.Byte.valueOf(b));
    }

    @Override // org.web3j.abi.datatypes.primitive.PrimitiveType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.primitive.PrimitiveType
    public zc4 toSolidityType() {
        return new Bytes1(new byte[]{getValue().byteValue()});
    }
}
