package org.web3j.abi.datatypes.primitive;

import org.web3j.abi.datatypes.NumericType;

/* loaded from: classes3.dex */
public class Float extends Number<java.lang.Float> {
    public Float(float f) {
        super(java.lang.Float.valueOf(f));
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType
    public NumericType toSolidityType() {
        throw new UnsupportedOperationException("Fixed types are not supported");
    }
}
