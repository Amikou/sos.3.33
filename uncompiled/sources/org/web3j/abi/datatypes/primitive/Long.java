package org.web3j.abi.datatypes.primitive;

import org.web3j.abi.datatypes.NumericType;
import org.web3j.abi.datatypes.generated.Int64;

/* loaded from: classes3.dex */
public final class Long extends Number<java.lang.Long> {
    public Long(long j) {
        super(java.lang.Long.valueOf(j));
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType
    public NumericType toSolidityType() {
        return new Int64(((java.lang.Long) getValue()).longValue());
    }
}
