package org.web3j.abi.datatypes.primitive;

import org.web3j.abi.datatypes.Utf8String;

/* loaded from: classes3.dex */
public final class Char extends PrimitiveType<Character> {
    public Char(char c) {
        super(Character.valueOf(c));
    }

    @Override // org.web3j.abi.datatypes.primitive.PrimitiveType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.primitive.PrimitiveType
    public zc4 toSolidityType() {
        return new Utf8String(String.valueOf(getValue()));
    }
}
