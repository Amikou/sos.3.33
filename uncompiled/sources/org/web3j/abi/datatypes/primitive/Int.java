package org.web3j.abi.datatypes.primitive;

import org.web3j.abi.datatypes.NumericType;
import org.web3j.abi.datatypes.generated.Int32;

/* loaded from: classes3.dex */
public final class Int extends Number<Integer> {
    public Int(int i) {
        super(Integer.valueOf(i));
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType
    public NumericType toSolidityType() {
        return new Int32(((Integer) getValue()).intValue());
    }
}
