package org.web3j.abi.datatypes.primitive;

import org.web3j.abi.datatypes.NumericType;
import org.web3j.abi.datatypes.generated.Int16;

/* loaded from: classes3.dex */
public final class Short extends Number<java.lang.Short> {
    public Short(short s) {
        super(java.lang.Short.valueOf(s));
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.primitive.Number, org.web3j.abi.datatypes.primitive.PrimitiveType
    public NumericType toSolidityType() {
        return new Int16(((java.lang.Short) getValue()).shortValue());
    }
}
