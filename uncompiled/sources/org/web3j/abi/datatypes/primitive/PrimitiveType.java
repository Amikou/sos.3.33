package org.web3j.abi.datatypes.primitive;

import java.io.Serializable;
import java.lang.Comparable;

/* loaded from: classes3.dex */
public abstract class PrimitiveType<T extends Serializable & Comparable<T>> implements zc4<T> {
    private final String type = getClass().getSimpleName().toLowerCase();
    private final T value;

    public PrimitiveType(T t) {
        this.value = t;
    }

    @Override // defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PrimitiveType primitiveType = (PrimitiveType) obj;
        return this.type.equals(primitiveType.type) && this.value.equals(primitiveType.value);
    }

    @Override // defpackage.zc4
    public String getTypeAsString() {
        return this.type;
    }

    public int hashCode() {
        return rl2.c(this.type, this.value);
    }

    public abstract zc4 toSolidityType();

    @Override // defpackage.zc4
    public T getValue() {
        return this.value;
    }
}
