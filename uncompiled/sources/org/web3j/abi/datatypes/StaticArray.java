package org.web3j.abi.datatypes;

import defpackage.zc4;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class StaticArray<T extends zc4> extends Array<T> {
    public static final int MAX_SIZE_OF_STATIC_ARRAY = 32;

    @Deprecated
    public StaticArray(T... tArr) {
        this(tArr.length, tArr);
    }

    private void checkValid(int i) {
        if (this.value.size() <= 32) {
            if (this.value.size() == i) {
                return;
            }
            throw new UnsupportedOperationException("Expected array of type [" + getClass().getSimpleName() + "] to have [" + i + "] elements.");
        }
        throw new UnsupportedOperationException("Static arrays with a length greater than 32 are not supported.");
    }

    @Override // org.web3j.abi.datatypes.Array, defpackage.zc4
    public String getTypeAsString() {
        return m4.getTypeAString(getComponentType()) + "[" + this.value.size() + "]";
    }

    @Deprecated
    public StaticArray(int i, T... tArr) {
        this(i, Arrays.asList(tArr));
    }

    @Override // org.web3j.abi.datatypes.Array, defpackage.zc4
    public List<T> getValue() {
        return Collections.unmodifiableList(this.value);
    }

    @Deprecated
    public StaticArray(List<T> list) {
        this(list.size(), list);
    }

    @Deprecated
    public StaticArray(int i, List<T> list) {
        super(m4.getType(list.get(0).getTypeAsString()), list);
        checkValid(i);
    }

    public StaticArray(Class<T> cls, T... tArr) {
        this(cls, Arrays.asList(tArr));
    }

    public StaticArray(Class<T> cls, int i, T... tArr) {
        this(cls, i, Arrays.asList(tArr));
    }

    public StaticArray(Class<T> cls, List<T> list) {
        this(cls, list == null ? 0 : list.size(), list);
    }

    public StaticArray(Class<T> cls, int i, List<T> list) {
        super(cls, list);
        checkValid(i);
    }
}
