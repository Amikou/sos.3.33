package org.web3j.abi.datatypes;

import defpackage.zc4;
import java.util.List;
import okhttp3.HttpUrl;

/* loaded from: classes3.dex */
public class DynamicArray<T extends zc4> extends Array<T> {
    @Deprecated
    public DynamicArray(T... tArr) {
        super(m4.getType(tArr[0].getTypeAsString()), tArr);
    }

    @Deprecated
    public static DynamicArray empty(String str) {
        return new DynamicArray(str);
    }

    @Override // org.web3j.abi.datatypes.Array, defpackage.zc4
    public int bytes32PaddedLength() {
        return super.bytes32PaddedLength() + 32;
    }

    @Override // org.web3j.abi.datatypes.Array, defpackage.zc4
    public String getTypeAsString() {
        return m4.getTypeAString(getComponentType()) + HttpUrl.PATH_SEGMENT_ENCODE_SET_URI;
    }

    @Deprecated
    public DynamicArray(List<T> list) {
        super(m4.getType(list.get(0).getTypeAsString()), list);
    }

    @Deprecated
    private DynamicArray(String str) {
        super(m4.getType(str), new zc4[0]);
    }

    public DynamicArray(Class<T> cls, List<T> list) {
        super(cls, list);
    }

    public DynamicArray(Class<T> cls, T... tArr) {
        super(cls, tArr);
    }
}
