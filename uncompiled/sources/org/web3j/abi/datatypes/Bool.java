package org.web3j.abi.datatypes;

/* loaded from: classes3.dex */
public class Bool implements zc4<Boolean> {
    public static final Bool DEFAULT = new Bool(false);
    public static final String TYPE_NAME = "bool";
    private boolean value;

    public Bool(boolean z) {
        this.value = z;
    }

    @Override // defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && getClass() == obj.getClass() && this.value == ((Bool) obj).value;
    }

    @Override // defpackage.zc4
    public String getTypeAsString() {
        return TYPE_NAME;
    }

    public int hashCode() {
        return this.value ? 1 : 0;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.zc4
    public Boolean getValue() {
        return Boolean.valueOf(this.value);
    }

    public Bool(Boolean bool) {
        this.value = bool.booleanValue();
    }
}
