package org.web3j.abi.datatypes;

import java.math.BigInteger;

/* loaded from: classes3.dex */
public abstract class NumericType implements zc4<BigInteger> {
    private String type;
    public BigInteger value;

    public NumericType(String str, BigInteger bigInteger) {
        this.type = str;
        this.value = bigInteger;
    }

    @Override // defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NumericType numericType = (NumericType) obj;
        if (this.type.equals(numericType.type)) {
            BigInteger bigInteger = this.value;
            BigInteger bigInteger2 = numericType.value;
            return bigInteger != null ? bigInteger.equals(bigInteger2) : bigInteger2 == null;
        }
        return false;
    }

    public abstract int getBitSize();

    @Override // defpackage.zc4
    public String getTypeAsString() {
        return this.type;
    }

    public int hashCode() {
        int hashCode = this.type.hashCode() * 31;
        BigInteger bigInteger = this.value;
        return hashCode + (bigInteger != null ? bigInteger.hashCode() : 0);
    }

    @Override // defpackage.zc4
    public BigInteger getValue() {
        return this.value;
    }
}
