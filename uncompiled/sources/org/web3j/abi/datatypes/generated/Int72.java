package org.web3j.abi.datatypes.generated;

import java.math.BigInteger;
import org.web3j.abi.datatypes.Int;

/* loaded from: classes3.dex */
public class Int72 extends Int {
    public static final Int72 DEFAULT = new Int72(BigInteger.ZERO);

    public Int72(BigInteger bigInteger) {
        super(72, bigInteger);
    }

    @Override // org.web3j.abi.datatypes.Int, org.web3j.abi.datatypes.IntType, org.web3j.abi.datatypes.NumericType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    public Int72(long j) {
        this(BigInteger.valueOf(j));
    }
}
