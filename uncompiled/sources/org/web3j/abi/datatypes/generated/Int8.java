package org.web3j.abi.datatypes.generated;

import java.math.BigInteger;
import org.web3j.abi.datatypes.Int;

/* loaded from: classes3.dex */
public class Int8 extends Int {
    public static final Int8 DEFAULT = new Int8(BigInteger.ZERO);

    public Int8(BigInteger bigInteger) {
        super(8, bigInteger);
    }

    @Override // org.web3j.abi.datatypes.Int, org.web3j.abi.datatypes.IntType, org.web3j.abi.datatypes.NumericType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    public Int8(long j) {
        this(BigInteger.valueOf(j));
    }
}
