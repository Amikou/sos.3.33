package org.web3j.abi.datatypes.generated;

import defpackage.zc4;
import java.util.List;
import org.web3j.abi.datatypes.StaticArray;

/* loaded from: classes3.dex */
public class StaticArray15<T extends zc4> extends StaticArray<T> {
    @Deprecated
    public StaticArray15(List<T> list) {
        super(15, list);
    }

    @Deprecated
    public StaticArray15(T... tArr) {
        super(15, tArr);
    }

    public StaticArray15(Class<T> cls, List<T> list) {
        super(cls, 15, list);
    }

    public StaticArray15(Class<T> cls, T... tArr) {
        super(cls, 15, tArr);
    }
}
