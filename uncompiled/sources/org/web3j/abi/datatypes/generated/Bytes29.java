package org.web3j.abi.datatypes.generated;

import org.web3j.abi.datatypes.Bytes;

/* loaded from: classes3.dex */
public class Bytes29 extends Bytes {
    public static final Bytes29 DEFAULT = new Bytes29(new byte[29]);

    public Bytes29(byte[] bArr) {
        super(29, bArr);
    }
}
