package org.web3j.abi.datatypes.generated;

import org.web3j.abi.datatypes.Bytes;

/* loaded from: classes3.dex */
public class Bytes2 extends Bytes {
    public static final Bytes2 DEFAULT = new Bytes2(new byte[2]);

    public Bytes2(byte[] bArr) {
        super(2, bArr);
    }
}
