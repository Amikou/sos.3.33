package org.web3j.abi.datatypes.generated;

import defpackage.zc4;
import java.util.List;
import org.web3j.abi.datatypes.StaticArray;

/* loaded from: classes3.dex */
public class StaticArray7<T extends zc4> extends StaticArray<T> {
    @Deprecated
    public StaticArray7(List<T> list) {
        super(7, list);
    }

    @Deprecated
    public StaticArray7(T... tArr) {
        super(7, tArr);
    }

    public StaticArray7(Class<T> cls, List<T> list) {
        super(cls, 7, list);
    }

    public StaticArray7(Class<T> cls, T... tArr) {
        super(cls, 7, tArr);
    }
}
