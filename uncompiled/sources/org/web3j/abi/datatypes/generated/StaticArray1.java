package org.web3j.abi.datatypes.generated;

import defpackage.zc4;
import java.util.List;
import org.web3j.abi.datatypes.StaticArray;

/* loaded from: classes3.dex */
public class StaticArray1<T extends zc4> extends StaticArray<T> {
    @Deprecated
    public StaticArray1(List<T> list) {
        super(1, list);
    }

    @Deprecated
    public StaticArray1(T... tArr) {
        super(1, tArr);
    }

    public StaticArray1(Class<T> cls, List<T> list) {
        super(cls, 1, list);
    }

    public StaticArray1(Class<T> cls, T... tArr) {
        super(cls, 1, tArr);
    }
}
