package org.web3j.abi.datatypes.generated;

import org.web3j.abi.datatypes.Bytes;

/* loaded from: classes3.dex */
public class Bytes8 extends Bytes {
    public static final Bytes8 DEFAULT = new Bytes8(new byte[8]);

    public Bytes8(byte[] bArr) {
        super(8, bArr);
    }
}
