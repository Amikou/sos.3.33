package org.web3j.abi.datatypes.generated;

import defpackage.zc4;
import java.util.List;
import org.web3j.abi.datatypes.StaticArray;

/* loaded from: classes3.dex */
public class StaticArray6<T extends zc4> extends StaticArray<T> {
    @Deprecated
    public StaticArray6(List<T> list) {
        super(6, list);
    }

    @Deprecated
    public StaticArray6(T... tArr) {
        super(6, tArr);
    }

    public StaticArray6(Class<T> cls, List<T> list) {
        super(cls, 6, list);
    }

    public StaticArray6(Class<T> cls, T... tArr) {
        super(cls, 6, tArr);
    }
}
