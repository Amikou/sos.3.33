package org.web3j.abi.datatypes.generated;

import java.math.BigInteger;
import org.web3j.abi.datatypes.Uint;

/* loaded from: classes3.dex */
public class Uint80 extends Uint {
    public static final Uint80 DEFAULT = new Uint80(BigInteger.ZERO);

    public Uint80(BigInteger bigInteger) {
        super(80, bigInteger);
    }

    @Override // org.web3j.abi.datatypes.Uint, org.web3j.abi.datatypes.IntType, org.web3j.abi.datatypes.NumericType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    public Uint80(long j) {
        this(BigInteger.valueOf(j));
    }
}
