package org.web3j.abi.datatypes.generated;

import org.web3j.abi.datatypes.Bytes;

/* loaded from: classes3.dex */
public class Bytes31 extends Bytes {
    public static final Bytes31 DEFAULT = new Bytes31(new byte[31]);

    public Bytes31(byte[] bArr) {
        super(31, bArr);
    }
}
