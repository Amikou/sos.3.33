package org.web3j.abi.datatypes.generated;

import org.web3j.abi.datatypes.Bytes;

/* loaded from: classes3.dex */
public class Bytes25 extends Bytes {
    public static final Bytes25 DEFAULT = new Bytes25(new byte[25]);

    public Bytes25(byte[] bArr) {
        super(25, bArr);
    }
}
