package org.web3j.abi.datatypes.generated;

import defpackage.zc4;
import java.util.List;
import org.web3j.abi.datatypes.StaticArray;

/* loaded from: classes3.dex */
public class StaticArray29<T extends zc4> extends StaticArray<T> {
    @Deprecated
    public StaticArray29(List<T> list) {
        super(29, list);
    }

    @Deprecated
    public StaticArray29(T... tArr) {
        super(29, tArr);
    }

    public StaticArray29(Class<T> cls, List<T> list) {
        super(cls, 29, list);
    }

    public StaticArray29(Class<T> cls, T... tArr) {
        super(cls, 29, tArr);
    }
}
