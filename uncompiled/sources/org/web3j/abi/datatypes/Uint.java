package org.web3j.abi.datatypes;

import java.math.BigInteger;

/* loaded from: classes3.dex */
public class Uint extends IntType {
    public static final Uint DEFAULT = new Uint(BigInteger.ZERO);
    public static final String TYPE_NAME = "uint";

    public Uint(int i, BigInteger bigInteger) {
        super(TYPE_NAME, i, bigInteger);
    }

    @Override // org.web3j.abi.datatypes.IntType, org.web3j.abi.datatypes.NumericType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    @Override // org.web3j.abi.datatypes.IntType
    public boolean valid() {
        return super.valid() && this.value.signum() >= 0;
    }

    public Uint(BigInteger bigInteger) {
        this(256, bigInteger);
    }
}
