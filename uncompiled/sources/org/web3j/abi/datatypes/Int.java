package org.web3j.abi.datatypes;

import java.math.BigInteger;

/* loaded from: classes3.dex */
public class Int extends IntType {
    public static final Int DEFAULT = new Int(BigInteger.ZERO);
    public static final String TYPE_NAME = "int";

    public Int(BigInteger bigInteger) {
        this(256, bigInteger);
    }

    @Override // org.web3j.abi.datatypes.IntType, org.web3j.abi.datatypes.NumericType, defpackage.zc4
    public /* bridge */ /* synthetic */ int bytes32PaddedLength() {
        return yc4.a(this);
    }

    public Int(int i, BigInteger bigInteger) {
        super(TYPE_NAME, i, bigInteger);
    }
}
