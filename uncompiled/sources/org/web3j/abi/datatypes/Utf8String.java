package org.web3j.abi.datatypes;

/* loaded from: classes3.dex */
public class Utf8String implements zc4<String> {
    public static final Utf8String DEFAULT = new Utf8String("");
    public static final String TYPE_NAME = "string";
    private String value;

    public Utf8String(String str) {
        this.value = str;
    }

    @Override // defpackage.zc4
    public int bytes32PaddedLength() {
        return 64;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        String str = this.value;
        String str2 = ((Utf8String) obj).value;
        return str != null ? str.equals(str2) : str2 == null;
    }

    @Override // defpackage.zc4
    public String getTypeAsString() {
        return TYPE_NAME;
    }

    public int hashCode() {
        String str = this.value;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return this.value;
    }

    @Override // defpackage.zc4
    public String getValue() {
        return this.value;
    }
}
