package org.web3j.abi;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java8.util.stream.h;
import org.web3j.abi.TypeReference;
import org.web3j.abi.c;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Bytes;
import org.web3j.abi.datatypes.BytesType;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.DynamicBytes;
import org.web3j.abi.datatypes.DynamicStruct;
import org.web3j.abi.datatypes.Fixed;
import org.web3j.abi.datatypes.FixedPointType;
import org.web3j.abi.datatypes.Int;
import org.web3j.abi.datatypes.IntType;
import org.web3j.abi.datatypes.NumericType;
import org.web3j.abi.datatypes.StaticArray;
import org.web3j.abi.datatypes.StaticStruct;
import org.web3j.abi.datatypes.Ufixed;
import org.web3j.abi.datatypes.Uint;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint160;
import org.web3j.abi.datatypes.primitive.Double;
import org.web3j.abi.datatypes.primitive.Float;

/* compiled from: TypeDecoder.java */
/* loaded from: classes2.dex */
public class c {
    public static final int MAX_BYTE_LENGTH_FOR_HEX_STRING = 64;

    public static List arrayToList(Object obj) {
        int length = Array.getLength(obj);
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            arrayList.add(Array.get(obj, i));
        }
        return arrayList;
    }

    public static BigInteger asBigInteger(Object obj) {
        if (obj instanceof BigInteger) {
            return (BigInteger) obj;
        }
        if (obj instanceof BigDecimal) {
            return ((BigDecimal) obj).toBigInteger();
        }
        if (obj instanceof String) {
            return ej2.toBigInt((String) obj);
        }
        if (obj instanceof byte[]) {
            return ej2.toBigInt((byte[]) obj);
        }
        if (!(obj instanceof Double) && !(obj instanceof Float) && !(obj instanceof Double) && !(obj instanceof Float)) {
            if (obj instanceof Number) {
                return BigInteger.valueOf(((Number) obj).longValue());
            }
            return null;
        }
        return BigDecimal.valueOf(((Number) obj).doubleValue()).toBigInteger();
    }

    public static <T extends org.web3j.abi.datatypes.Array> T decode(String str, int i, TypeReference<T> typeReference) {
        Class<?> cls = ((ParameterizedType) typeReference.getType()).getRawType().getClass();
        if (StaticArray.class.isAssignableFrom(cls)) {
            return (T) decodeStaticArray(str, i, typeReference, 1);
        }
        if (DynamicArray.class.isAssignableFrom(cls)) {
            return (T) decodeDynamicArray(str, i, typeReference);
        }
        throw new UnsupportedOperationException("Unsupported TypeReference: " + cls.getName() + ", only Array types can be passed as TypeReferences");
    }

    public static Address decodeAddress(String str) {
        return new Address((Uint) decodeNumeric(str, Uint160.class));
    }

    private static <T extends zc4> T decodeArrayElements(String str, int i, TypeReference<T> typeReference, int i2, fp<List<T>, String, T> fpVar) {
        try {
            Class parameterizedTypeFromArray = d.getParameterizedTypeFromArray(typeReference);
            if (!org.web3j.abi.datatypes.Array.class.isAssignableFrom(parameterizedTypeFromArray)) {
                ArrayList arrayList = new ArrayList(i2);
                int i3 = 0;
                while (i3 < i2) {
                    arrayList.add(decode(str, i, parameterizedTypeFromArray));
                    i3++;
                    i += getSingleElementLength(str, i, parameterizedTypeFromArray) * 64;
                }
                return fpVar.apply(arrayList, d.getSimpleTypeName(parameterizedTypeFromArray));
            }
            throw new UnsupportedOperationException("Arrays of arrays are not currently supported for external functions, seehttp://solidity.readthedocs.io/en/develop/types.html#members");
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException("Unable to access parameterized type " + m30.getTypeName(typeReference.getType()), e);
        }
    }

    public static Bool decodeBool(String str, int i) {
        return new Bool(ej2.toBigInt(str.substring(i, i + 64)).equals(BigInteger.ONE));
    }

    public static <T extends Bytes> T decodeBytes(String str, Class<T> cls) {
        return (T) decodeBytes(str, 0, cls);
    }

    public static <T extends zc4> T decodeDynamicArray(String str, int i, TypeReference<T> typeReference) {
        return (T) decodeArrayElements(str, i + 64, typeReference, decodeUintAsInt(str, i), hd4.a);
    }

    public static DynamicBytes decodeDynamicBytes(String str, int i) {
        int i2 = i + 64;
        return new DynamicBytes(ej2.hexStringToByteArray(str.substring(i2, (decodeUintAsInt(str, i) << 1) + i2)));
    }

    private static <T extends zc4> T decodeDynamicParameterFromStruct(String str, int i, int i2, Class<T> cls) {
        String substring = str.substring(i, i2 + i);
        if (DynamicStruct.class.isAssignableFrom(cls)) {
            return (T) decodeDynamicStruct(substring, 64, TypeReference.create(cls));
        }
        return (T) decode(substring, cls);
    }

    public static <T extends zc4> T decodeDynamicStruct(String str, int i, final TypeReference<T> typeReference) {
        return (T) decodeDynamicStructElements(str, i, typeReference, new fp() { // from class: fd4
            @Override // defpackage.fp
            public final Object apply(Object obj, Object obj2) {
                zc4 lambda$decodeDynamicStruct$9;
                lambda$decodeDynamicStruct$9 = c.lambda$decodeDynamicStruct$9(TypeReference.this, (List) obj, (String) obj2);
                return lambda$decodeDynamicStruct$9;
            }
        });
    }

    private static int decodeDynamicStructDynamicParameterOffset(String str) {
        return (decodeUintAsInt(str, 0) * 2) + 64;
    }

    private static <T extends zc4> T decodeDynamicStructElements(String str, int i, TypeReference<T> typeReference, fp<List<T>, String, T> fpVar) {
        int intValue;
        int intValue2;
        zc4 decode;
        try {
            Class<T> classType = typeReference.getClassType();
            Constructor constructor = (Constructor) h.a(Arrays.asList(classType.getDeclaredConstructors())).b(nd4.a).d().g(dd4.a);
            int length = constructor.getParameterTypes().length;
            HashMap hashMap = new HashMap();
            ArrayList arrayList = new ArrayList();
            int i2 = 0;
            int i3 = 0;
            while (true) {
                boolean z = true;
                if (i2 >= length) {
                    break;
                }
                Class<?> cls = constructor.getParameterTypes()[i2];
                int i4 = i + i3;
                if (isDynamic(cls)) {
                    if (length != 1) {
                        z = false;
                    }
                    arrayList.add(Integer.valueOf(z ? i : decodeDynamicStructDynamicParameterOffset(str.substring(i4, i4 + 64))));
                    i3 += 64;
                } else {
                    if (StaticStruct.class.isAssignableFrom(cls)) {
                        decode = decodeStaticStruct(str.substring(i4), 0, TypeReference.create(cls));
                    } else {
                        decode = decode(str.substring(i4), 0, (Class<zc4>) cls);
                    }
                    hashMap.put(Integer.valueOf(i2), decode);
                    i3 += decode.bytes32PaddedLength() * 2;
                }
                i2++;
            }
            int dynamicStructDynamicParametersCount = getDynamicStructDynamicParametersCount(constructor.getParameterTypes());
            int i5 = 0;
            for (int i6 = 0; i6 < length; i6++) {
                Class<?> cls2 = constructor.getParameterTypes()[i6];
                if (isDynamic(cls2)) {
                    if (i5 == dynamicStructDynamicParametersCount + (-1)) {
                        intValue = str.length();
                        intValue2 = ((Integer) arrayList.get(i5)).intValue();
                    } else {
                        intValue = ((Integer) arrayList.get(i5 + 1)).intValue();
                        intValue2 = ((Integer) arrayList.get(i5)).intValue();
                    }
                    hashMap.put(Integer.valueOf(i6), decodeDynamicParameterFromStruct(str, ((Integer) arrayList.get(i5)).intValue(), intValue - intValue2, cls2));
                    i5++;
                }
            }
            String simpleTypeName = d.getSimpleTypeName(classType);
            ArrayList arrayList2 = new ArrayList();
            for (int i7 = 0; i7 < length; i7++) {
                arrayList2.add(hashMap.get(Integer.valueOf(i7)));
            }
            return fpVar.apply(arrayList2, simpleTypeName);
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException("Unable to access parameterized type " + m30.getTypeName(typeReference.getType()), e);
        }
    }

    public static <T extends NumericType> T decodeNumeric(String str, Class<T> cls) {
        try {
            byte[] hexStringToByteArray = ej2.hexStringToByteArray(str);
            int typeLengthInBytes = getTypeLengthInBytes(cls);
            byte[] bArr = new byte[typeLengthInBytes + 1];
            if (Int.class.isAssignableFrom(cls) || Fixed.class.isAssignableFrom(cls)) {
                bArr[0] = hexStringToByteArray[0];
            }
            System.arraycopy(hexStringToByteArray, 32 - typeLengthInBytes, bArr, 1, typeLengthInBytes);
            return cls.getConstructor(BigInteger.class).newInstance(new BigInteger(bArr));
        } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            throw new UnsupportedOperationException("Unable to create instance of " + cls.getName(), e);
        }
    }

    public static <T extends zc4> T decodeStaticArray(String str, int i, final TypeReference<T> typeReference, final int i2) {
        return (T) decodeArrayElements(str, i, typeReference, i2, new fp() { // from class: gd4
            @Override // defpackage.fp
            public final Object apply(Object obj, Object obj2) {
                zc4 lambda$decodeStaticArray$0;
                lambda$decodeStaticArray$0 = c.lambda$decodeStaticArray$0(TypeReference.this, i2, (List) obj, (String) obj2);
                return lambda$decodeStaticArray$0;
            }
        });
    }

    public static <T extends zc4> T decodeStaticStruct(String str, int i, final TypeReference<T> typeReference) {
        return (T) decodeStaticStructElement(str, i, typeReference, new fp() { // from class: ad4
            @Override // defpackage.fp
            public final Object apply(Object obj, Object obj2) {
                zc4 lambda$decodeStaticStruct$1;
                lambda$decodeStaticStruct$1 = c.lambda$decodeStaticStruct$1(TypeReference.this, (List) obj, (String) obj2);
                return lambda$decodeStaticStruct$1;
            }
        });
    }

    private static <T extends zc4> T decodeStaticStructElement(String str, int i, TypeReference<T> typeReference, fp<List<T>, String, T> fpVar) {
        int i2;
        zc4 decode;
        try {
            Class<T> classType = typeReference.getClassType();
            Constructor constructor = (Constructor) h.a(Arrays.asList(classType.getDeclaredConstructors())).b(md4.a).d().g(ed4.a);
            int length = constructor.getParameterTypes().length;
            ArrayList arrayList = new ArrayList(length);
            int i3 = 0;
            int i4 = 0;
            while (i3 < length) {
                Class<?> cls = constructor.getParameterTypes()[i3];
                System.out.println(i4);
                if (StaticStruct.class.isAssignableFrom(cls)) {
                    i2 = (classType.getDeclaredFields()[i3].getType().getConstructors()[0].getParameterTypes().length * 64) + i4;
                    decode = decodeStaticStruct(str.substring(i4, i2), 0, TypeReference.create(cls));
                } else {
                    i2 = i4 + 64;
                    decode = decode(str.substring(i4, i2), 0, (Class<zc4>) cls);
                }
                arrayList.add(decode);
                i3++;
                i4 = i2;
            }
            return fpVar.apply(arrayList, d.getSimpleTypeName(classType));
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException("Unable to access parameterized type " + m30.getTypeName(typeReference.getType()), e);
        }
    }

    public static int decodeUintAsInt(String str, int i) {
        return ((Uint) decode(str.substring(i, i + 64), 0, Uint.class)).getValue().intValue();
    }

    public static Utf8String decodeUtf8String(String str, int i) {
        return new Utf8String(new String(decodeDynamicBytes(str, i).getValue(), m30.UTF_8));
    }

    private static <T extends zc4> int getDynamicStructDynamicParametersCount(Class<?>[] clsArr) {
        return (int) h.a(Arrays.asList(clsArr)).b(ld4.a).a();
    }

    public static <T extends zc4> int getSingleElementLength(String str, int i, Class<T> cls) {
        if (str.length() == i) {
            return 0;
        }
        if (DynamicBytes.class.isAssignableFrom(cls) || Utf8String.class.isAssignableFrom(cls)) {
            return (decodeUintAsInt(str, i) / 32) + 2;
        }
        return 1;
    }

    public static <T extends NumericType> int getTypeLength(Class<T> cls) {
        if (IntType.class.isAssignableFrom(cls)) {
            String[] split = cls.getSimpleName().split("(" + Uint.class.getSimpleName() + "|" + Int.class.getSimpleName() + ")");
            if (split.length == 2) {
                return Integer.parseInt(split[1]);
            }
            return 256;
        } else if (FixedPointType.class.isAssignableFrom(cls)) {
            String[] split2 = cls.getSimpleName().split("(" + Ufixed.class.getSimpleName() + "|" + Fixed.class.getSimpleName() + ")");
            if (split2.length == 2) {
                String[] split3 = split2[1].split("x");
                return Integer.parseInt(split3[0]) + Integer.parseInt(split3[1]);
            }
            return 256;
        } else {
            return 256;
        }
    }

    public static <T extends NumericType> int getTypeLengthInBytes(Class<T> cls) {
        return getTypeLength(cls) >> 3;
    }

    public static zc4 instantiateArrayType(TypeReference typeReference, Object obj) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        List<Object> arrayToList;
        Constructor<?> constructor;
        if (obj instanceof List) {
            arrayToList = (List) obj;
        } else if (obj.getClass().isArray()) {
            arrayToList = arrayToList(obj);
        } else {
            throw new ClassCastException("Arg of type " + obj.getClass() + " should be a list to instantiate web3j Array");
        }
        int size = typeReference instanceof TypeReference.StaticArrayTypeReference ? ((TypeReference.StaticArrayTypeReference) typeReference).getSize() : -1;
        if (size <= 0) {
            constructor = DynamicArray.class.getConstructor(Class.class, List.class);
        } else {
            constructor = Class.forName("org.web3j.abi.datatypes.generated.StaticArray" + size).getConstructor(Class.class, List.class);
        }
        ArrayList arrayList = new ArrayList(arrayToList.size());
        TypeReference subTypeReference = typeReference.getSubTypeReference();
        for (Object obj2 : arrayToList) {
            arrayList.add(instantiateType(subTypeReference, obj2));
        }
        return (zc4) constructor.newInstance(subTypeReference.getClassType(), arrayList);
    }

    public static zc4 instantiateAtomicType(Class<?> cls, Object obj) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        Object obj2 = null;
        if (NumericType.class.isAssignableFrom(cls)) {
            obj2 = asBigInteger(obj);
        } else if (BytesType.class.isAssignableFrom(cls)) {
            if (!(obj instanceof byte[])) {
                if (obj instanceof BigInteger) {
                    obj2 = ((BigInteger) obj).toByteArray();
                } else if (obj instanceof String) {
                    obj2 = ej2.hexStringToByteArray((String) obj);
                }
            }
            obj2 = obj;
        } else if (Utf8String.class.isAssignableFrom(cls)) {
            obj2 = obj.toString();
        } else if (Address.class.isAssignableFrom(cls)) {
            if (!(obj instanceof BigInteger) && !(obj instanceof Uint160)) {
                obj2 = obj.toString();
            }
            obj2 = obj;
        } else if (Bool.class.isAssignableFrom(cls)) {
            if (!(obj instanceof Boolean)) {
                BigInteger asBigInteger = asBigInteger(obj);
                if (asBigInteger != null) {
                    obj2 = Boolean.valueOf(!asBigInteger.equals(BigInteger.ZERO));
                }
            }
            obj2 = obj;
        }
        if (obj2 != null) {
            return (zc4) cls.getConstructor(obj2.getClass()).newInstance(obj2);
        }
        throw new InstantiationException("Could not create type " + cls + " from arg " + obj.toString() + " of type " + obj.getClass());
    }

    private static <T extends zc4> T instantiateStaticArray(TypeReference<T> typeReference, List<T> list, int i) {
        try {
            return (T) Class.forName("org.web3j.abi.datatypes.generated.StaticArray" + i).getConstructor(List.class).newInstance(list);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    private static <T extends zc4> T instantiateStruct(TypeReference<T> typeReference, List<T> list) {
        try {
            Constructor constructor = (Constructor) h.a(Arrays.asList(typeReference.getClassType().getDeclaredConstructors())).b(bd4.a).d().g(cd4.a);
            constructor.setAccessible(true);
            return (T) constructor.newInstance(list.toArray());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new UnsupportedOperationException("Constructor cannot accept" + Arrays.toString(list.toArray()), e);
        }
    }

    public static zc4 instantiateType(String str, Object obj) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        return instantiateType(TypeReference.makeTypeReference(str), obj);
    }

    public static <T extends zc4> boolean isDynamic(Class<T> cls) {
        return DynamicBytes.class.isAssignableFrom(cls) || Utf8String.class.isAssignableFrom(cls) || DynamicArray.class.isAssignableFrom(cls);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ zc4 lambda$decodeDynamicArray$8(List list, String str) {
        return new DynamicArray(m4.getType(str), list);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ zc4 lambda$decodeDynamicStruct$9(TypeReference typeReference, List list, String str) {
        if (!list.isEmpty()) {
            return instantiateStruct(typeReference, list);
        }
        throw new UnsupportedOperationException("Zero length fixed array is invalid type");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ boolean lambda$decodeDynamicStructElements$11(Constructor constructor) {
        return h.a(Arrays.asList(constructor.getParameterTypes())).e(jd4.a);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ RuntimeException lambda$decodeDynamicStructElements$12() {
        return new RuntimeException("TypeReferenced struct must contain a constructor with types that extend Type");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ zc4 lambda$decodeStaticArray$0(TypeReference typeReference, int i, List list, String str) {
        if (!list.isEmpty()) {
            return instantiateStaticArray(typeReference, list, i);
        }
        throw new UnsupportedOperationException("Zero length fixed array is invalid type");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ zc4 lambda$decodeStaticStruct$1(TypeReference typeReference, List list, String str) {
        if (!list.isEmpty()) {
            return instantiateStruct(typeReference, list);
        }
        throw new UnsupportedOperationException("Zero length fixed array is invalid type");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ boolean lambda$decodeStaticStructElement$3(Constructor constructor) {
        return h.a(Arrays.asList(constructor.getParameterTypes())).e(kd4.a);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ RuntimeException lambda$decodeStaticStructElement$4() {
        return new RuntimeException("TypeReferenced struct must contain a constructor with types that extend Type");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ boolean lambda$instantiateStruct$6(Constructor constructor) {
        return h.a(Arrays.asList(constructor.getParameterTypes())).e(id4.a);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ RuntimeException lambda$instantiateStruct$7() {
        return new RuntimeException("TypeReference struct must contain a constructor with types that extend Type");
    }

    public static <T extends Bytes> T decodeBytes(String str, int i, Class<T> cls) {
        try {
            return cls.getConstructor(byte[].class).newInstance(ej2.hexStringToByteArray(str.substring(i, (Integer.parseInt(cls.getSimpleName().split(Bytes.class.getSimpleName())[1]) << 1) + i)));
        } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            throw new UnsupportedOperationException("Unable to create instance of " + cls.getName(), e);
        }
    }

    public static zc4 instantiateType(TypeReference typeReference, Object obj) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        Class classType = typeReference.getClassType();
        if (org.web3j.abi.datatypes.Array.class.isAssignableFrom(classType)) {
            return instantiateArrayType(typeReference, obj);
        }
        return instantiateAtomicType(classType, obj);
    }

    public static <T extends zc4> T decode(String str, int i, Class<T> cls) {
        if (NumericType.class.isAssignableFrom(cls)) {
            return decodeNumeric(str.substring(i), cls);
        }
        if (Address.class.isAssignableFrom(cls)) {
            return decodeAddress(str.substring(i));
        }
        if (Bool.class.isAssignableFrom(cls)) {
            return decodeBool(str, i);
        }
        if (Bytes.class.isAssignableFrom(cls)) {
            return decodeBytes(str, i, cls);
        }
        if (DynamicBytes.class.isAssignableFrom(cls)) {
            return decodeDynamicBytes(str, i);
        }
        if (Utf8String.class.isAssignableFrom(cls)) {
            return decodeUtf8String(str, i);
        }
        if (org.web3j.abi.datatypes.Array.class.isAssignableFrom(cls)) {
            throw new UnsupportedOperationException("Array types must be wrapped in a TypeReference");
        }
        throw new UnsupportedOperationException("Type cannot be encoded: " + cls.getClass());
    }

    public static <T extends zc4> T decode(String str, Class<T> cls) {
        return (T) decode(str, 0, cls);
    }
}
