package org.web3j.abi;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

/* compiled from: FunctionEncoder.java */
/* loaded from: classes2.dex */
public abstract class b {
    private static b DEFAULT_ENCODER;
    private static final ServiceLoader<xd1> loader = ServiceLoader.load(xd1.class);

    public static String buildMethodId(String str) {
        return ej2.toHexString(ak1.sha3(str.getBytes())).substring(0, 10);
    }

    public static String buildMethodSignature(String str, List<zc4> list) {
        return str + "(" + i10.join(list, ",", wd1.a) + ")";
    }

    private static b defaultEncoder() {
        if (DEFAULT_ENCODER == null) {
            DEFAULT_ENCODER = new nj0();
        }
        return DEFAULT_ENCODER;
    }

    public static String encode(id1 id1Var) {
        return encoder().encodeFunction(id1Var);
    }

    public static String encodeConstructor(List<zc4> list) {
        return encoder().encodeParameters(list);
    }

    private static b encoder() {
        Iterator<xd1> it = loader.iterator();
        return it.hasNext() ? (b) it.next().get() : defaultEncoder();
    }

    public static id1 makeFunction(String str, List<String> list, List<Object> list2, List<String> list3) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        ArrayList arrayList = new ArrayList();
        Iterator<Object> it = list2.iterator();
        for (String str2 : list) {
            arrayList.add(c.instantiateType(str2, it.next()));
        }
        ArrayList arrayList2 = new ArrayList();
        for (String str3 : list3) {
            arrayList2.add(TypeReference.makeTypeReference(str3));
        }
        return new id1(str, arrayList, arrayList2);
    }

    public abstract String encodeFunction(id1 id1Var);

    public abstract String encodeParameters(List<zc4> list);
}
