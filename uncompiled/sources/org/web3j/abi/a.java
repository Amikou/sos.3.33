package org.web3j.abi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Array;
import org.web3j.abi.datatypes.Bytes;
import org.web3j.abi.datatypes.BytesType;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.DynamicBytes;
import org.web3j.abi.datatypes.DynamicStruct;
import org.web3j.abi.datatypes.StaticArray;
import org.web3j.abi.datatypes.StaticStruct;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Bytes32;

/* compiled from: DefaultFunctionReturnDecoder.java */
/* loaded from: classes2.dex */
public class a extends yd1 {
    private static List<zc4> build(String str, List<TypeReference<zc4>> list) {
        zc4 decode;
        int parseInt;
        ArrayList arrayList = new ArrayList(list.size());
        int i = 0;
        for (TypeReference<zc4> typeReference : list) {
            try {
                Class<zc4> classType = typeReference.getClassType();
                int dataOffset = getDataOffset(str, i, classType);
                if (DynamicStruct.class.isAssignableFrom(classType)) {
                    if (list.size() == 1) {
                        decode = c.decodeDynamicStruct(str, dataOffset, typeReference);
                    } else {
                        throw new UnsupportedOperationException("Multiple return objects containing a struct is not supported");
                    }
                } else if (DynamicArray.class.isAssignableFrom(classType)) {
                    decode = c.decodeDynamicArray(str, dataOffset, typeReference);
                } else {
                    if (typeReference instanceof TypeReference.StaticArrayTypeReference) {
                        parseInt = ((TypeReference.StaticArrayTypeReference) typeReference).getSize();
                        decode = c.decodeStaticArray(str, dataOffset, typeReference, parseInt);
                    } else if (StaticStruct.class.isAssignableFrom(classType)) {
                        decode = c.decodeStaticStruct(str, dataOffset, typeReference);
                        parseInt = classType.getDeclaredFields().length;
                    } else if (StaticArray.class.isAssignableFrom(classType)) {
                        parseInt = Integer.parseInt(classType.getSimpleName().substring(StaticArray.class.getSimpleName().length()));
                        decode = c.decodeStaticArray(str, dataOffset, typeReference, parseInt);
                    } else {
                        decode = c.decode(str, dataOffset, classType);
                    }
                    i += parseInt * 64;
                    arrayList.add(decode);
                }
                i += 64;
                arrayList.add(decode);
            } catch (ClassNotFoundException e) {
                throw new UnsupportedOperationException("Invalid class reference provided", e);
            }
        }
        return arrayList;
    }

    private static <T extends zc4> int getDataOffset(String str, int i, Class<T> cls) {
        return (DynamicBytes.class.isAssignableFrom(cls) || Utf8String.class.isAssignableFrom(cls) || DynamicArray.class.isAssignableFrom(cls)) ? c.decodeUintAsInt(str, i) << 1 : i;
    }

    @Override // defpackage.yd1
    public <T extends zc4> zc4 decodeEventParameter(String str, TypeReference<T> typeReference) {
        String cleanHexPrefix = ej2.cleanHexPrefix(str);
        try {
            Class<T> classType = typeReference.getClassType();
            if (Bytes.class.isAssignableFrom(classType)) {
                return c.decodeBytes(cleanHexPrefix, Class.forName(classType.getName()));
            }
            if (!Array.class.isAssignableFrom(classType) && !BytesType.class.isAssignableFrom(classType) && !Utf8String.class.isAssignableFrom(classType)) {
                return c.decode(cleanHexPrefix, classType);
            }
            return c.decodeBytes(cleanHexPrefix, Bytes32.class);
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException("Invalid class reference provided", e);
        }
    }

    @Override // defpackage.yd1
    public List<zc4> decodeFunctionResult(String str, List<TypeReference<zc4>> list) {
        String cleanHexPrefix = ej2.cleanHexPrefix(str);
        if (uu3.isEmpty(cleanHexPrefix)) {
            return Collections.emptyList();
        }
        return build(cleanHexPrefix, list);
    }
}
