package org.web3j.abi;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import okhttp3.HttpUrl;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.DynamicBytes;
import org.web3j.abi.datatypes.Fixed;
import org.web3j.abi.datatypes.Int;
import org.web3j.abi.datatypes.StaticArray;
import org.web3j.abi.datatypes.Ufixed;
import org.web3j.abi.datatypes.Uint;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: Utils.java */
/* loaded from: classes2.dex */
public class d {
    private d() {
    }

    public static List<TypeReference<zc4>> convert(List<TypeReference<?>> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (TypeReference<?> typeReference : list) {
            arrayList.add(typeReference);
        }
        return arrayList;
    }

    public static <T extends zc4> Class<T> getParameterizedTypeFromArray(TypeReference typeReference) throws ClassNotFoundException {
        return (Class<T>) Class.forName(m30.getTypeName(((ParameterizedType) typeReference.getType()).getActualTypeArguments()[0]));
    }

    public static <T extends zc4, U extends zc4> String getParameterizedTypeName(TypeReference<T> typeReference, Class<?> cls) {
        try {
            if (cls.equals(DynamicArray.class)) {
                String simpleTypeName = getSimpleTypeName(getParameterizedTypeFromArray(typeReference));
                return simpleTypeName + HttpUrl.PATH_SEGMENT_ENCODE_SET_URI;
            } else if (cls.equals(StaticArray.class)) {
                String simpleTypeName2 = getSimpleTypeName(getParameterizedTypeFromArray(typeReference));
                return simpleTypeName2 + "[" + ((TypeReference.StaticArrayTypeReference) typeReference).getSize() + "]";
            } else {
                throw new UnsupportedOperationException("Invalid type provided " + cls.getName());
            }
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException("Invalid class reference provided", e);
        }
    }

    public static String getSimpleTypeName(Class<?> cls) {
        String lowerCase = cls.getSimpleName().toLowerCase();
        if (!cls.equals(Uint.class) && !cls.equals(Int.class) && !cls.equals(Ufixed.class) && !cls.equals(Fixed.class)) {
            return cls.equals(Utf8String.class) ? Utf8String.TYPE_NAME : cls.equals(DynamicBytes.class) ? "bytes" : lowerCase;
        }
        return lowerCase + "256";
    }

    public static <T extends zc4> String getTypeName(TypeReference<T> typeReference) {
        try {
            Type type = typeReference.getType();
            if (type instanceof ParameterizedType) {
                return getParameterizedTypeName(typeReference, (Class) ((ParameterizedType) type).getRawType());
            }
            return getSimpleTypeName(Class.forName(m30.getTypeName(type)));
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException("Invalid class reference provided", e);
        }
    }

    public static <T, R extends zc4<T>, E extends zc4<T>> List<E> typeMap(List<List<T>> list, Class<E> cls, Class<R> cls2) {
        ArrayList arrayList = new ArrayList();
        try {
            Constructor<E> declaredConstructor = cls.getDeclaredConstructor(Class.class, List.class);
            Iterator<List<T>> it = list.iterator();
            while (it.hasNext()) {
                arrayList.add(declaredConstructor.newInstance(cls2, typeMap(it.next(), cls2)));
            }
            return arrayList;
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new TypeMappingException(e);
        }
    }

    public static <T, R extends zc4<T>> List<R> typeMap(List<T> list, Class<R> cls) throws TypeMappingException {
        ArrayList arrayList = new ArrayList(list.size());
        if (!list.isEmpty()) {
            try {
                Constructor<R> declaredConstructor = cls.getDeclaredConstructor(list.get(0).getClass());
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    arrayList.add(declaredConstructor.newInstance(it.next()));
                }
            } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                throw new TypeMappingException(e);
            }
        }
        return arrayList;
    }
}
