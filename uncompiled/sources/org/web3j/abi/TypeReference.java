package org.web3j.abi;

import defpackage.zc4;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.StaticArray;

/* loaded from: classes2.dex */
public abstract class TypeReference<T extends zc4> implements Comparable<TypeReference<T>> {
    public static Pattern ARRAY_SUFFIX = Pattern.compile("\\[(\\d*)]");
    private final boolean indexed;
    private final Type type;

    /* loaded from: classes2.dex */
    public static abstract class StaticArrayTypeReference<T extends zc4> extends TypeReference<T> {
        private final int size;

        public StaticArrayTypeReference(int i) {
            this.size = i;
        }

        @Override // org.web3j.abi.TypeReference, java.lang.Comparable
        public /* bridge */ /* synthetic */ int compareTo(Object obj) {
            return super.compareTo((TypeReference) ((TypeReference) obj));
        }

        public int getSize() {
            return this.size;
        }
    }

    public TypeReference() {
        this(false);
    }

    public static <T extends zc4> TypeReference<T> create(Class<T> cls) {
        return create(cls, false);
    }

    public static Class<? extends zc4> getAtomicTypeClass(String str, boolean z) throws ClassNotFoundException {
        if (!ARRAY_SUFFIX.matcher(str).find()) {
            return m4.getType(str, z);
        }
        throw new ClassNotFoundException("getAtomicTypeClass does not work with array types. See makeTypeReference()");
    }

    public static TypeReference makeTypeReference(String str) throws ClassNotFoundException {
        return makeTypeReference(str, false, false);
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return compareTo((TypeReference) ((TypeReference) obj));
    }

    public int compareTo(TypeReference<T> typeReference) {
        return 0;
    }

    public Class<T> getClassType() throws ClassNotFoundException {
        Type type = getType();
        if (type instanceof ParameterizedType) {
            return (Class) ((ParameterizedType) type).getRawType();
        }
        return (Class<T>) Class.forName(m30.getTypeName(type));
    }

    public TypeReference getSubTypeReference() {
        return null;
    }

    public Type getType() {
        return this.type;
    }

    public boolean isIndexed() {
        return this.indexed;
    }

    public TypeReference(boolean z) {
        Type genericSuperclass = getClass().getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            this.type = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
            this.indexed = z;
            return;
        }
        throw new RuntimeException("Missing type parameter.");
    }

    public static <T extends zc4> TypeReference<T> create(final Class<T> cls, boolean z) {
        return (TypeReference<T>) new TypeReference<T>(z) { // from class: org.web3j.abi.TypeReference.1
            @Override // org.web3j.abi.TypeReference, java.lang.Comparable
            public /* bridge */ /* synthetic */ int compareTo(Object obj) {
                return super.compareTo((TypeReference) ((TypeReference) obj));
            }

            @Override // org.web3j.abi.TypeReference
            public Type getType() {
                return cls;
            }
        };
    }

    public static TypeReference makeTypeReference(String str, final boolean z, boolean z2) throws ClassNotFoundException {
        final Class cls;
        Matcher matcher = ARRAY_SUFFIX.matcher(str);
        if (!matcher.find()) {
            return create(getAtomicTypeClass(str, z2), z);
        }
        int start = matcher.start();
        final TypeReference<DynamicArray> create = create(getAtomicTypeClass(str.substring(0, start), z2), z);
        int length = str.length();
        while (start < length) {
            String group = matcher.group(1);
            if (group != null && !group.equals("")) {
                int parseInt = Integer.parseInt(group);
                if (parseInt <= 32) {
                    cls = Class.forName("org.web3j.abi.datatypes.generated.StaticArray" + group);
                } else {
                    cls = StaticArray.class;
                }
                create = new StaticArrayTypeReference<StaticArray>(parseInt) { // from class: org.web3j.abi.TypeReference.3

                    /* renamed from: org.web3j.abi.TypeReference$3$a */
                    /* loaded from: classes2.dex */
                    public class a implements ParameterizedType {
                        public a() {
                        }

                        @Override // java.lang.reflect.ParameterizedType
                        public Type[] getActualTypeArguments() {
                            return new Type[]{create.getType()};
                        }

                        @Override // java.lang.reflect.ParameterizedType
                        public Type getOwnerType() {
                            return Class.class;
                        }

                        @Override // java.lang.reflect.ParameterizedType
                        public Type getRawType() {
                            return cls;
                        }
                    }

                    @Override // org.web3j.abi.TypeReference
                    public TypeReference getSubTypeReference() {
                        return create;
                    }

                    @Override // org.web3j.abi.TypeReference
                    public Type getType() {
                        return new a();
                    }

                    @Override // org.web3j.abi.TypeReference
                    public boolean isIndexed() {
                        return z;
                    }
                };
            } else {
                create = new TypeReference<DynamicArray>(z) { // from class: org.web3j.abi.TypeReference.2

                    /* renamed from: org.web3j.abi.TypeReference$2$a */
                    /* loaded from: classes2.dex */
                    public class a implements ParameterizedType {
                        public a() {
                        }

                        @Override // java.lang.reflect.ParameterizedType
                        public Type[] getActualTypeArguments() {
                            return new Type[]{create.getType()};
                        }

                        @Override // java.lang.reflect.ParameterizedType
                        public Type getOwnerType() {
                            return Class.class;
                        }

                        @Override // java.lang.reflect.ParameterizedType
                        public Type getRawType() {
                            return DynamicArray.class;
                        }
                    }

                    @Override // org.web3j.abi.TypeReference, java.lang.Comparable
                    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
                        return super.compareTo((TypeReference) ((TypeReference) obj));
                    }

                    @Override // org.web3j.abi.TypeReference
                    public TypeReference getSubTypeReference() {
                        return create;
                    }

                    @Override // org.web3j.abi.TypeReference
                    public Type getType() {
                        return new a();
                    }
                };
            }
            start = matcher.end();
            matcher = ARRAY_SUFFIX.matcher(str);
            if (!matcher.find(start) && start != length) {
                throw new ClassNotFoundException("Unable to make TypeReference from " + str);
            }
        }
        return create;
    }
}
