package org.web3j.ens;

import org.web3j.crypto.g;
import org.web3j.ens.contracts.generated.ENS;
import org.web3j.ens.contracts.generated.PublicResolver;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: EnsResolver.java */
/* loaded from: classes3.dex */
public class a {
    public static final long DEFAULT_SYNC_THRESHOLD = 180000;
    public static final String REVERSE_NAME_SUFFIX = ".addr.reverse";
    private final int addressLength;
    private long syncThreshold;
    private final u84 transactionManager;
    private final ko4 web3j;

    public a(ko4 ko4Var, long j, int i) {
        this.web3j = ko4Var;
        this.transactionManager = new lz(ko4Var, null);
        this.syncThreshold = j;
        this.addressLength = i;
    }

    public static boolean isValidEnsName(String str) {
        return isValidEnsName(str, 40);
    }

    private PublicResolver lookupResolver(String str) throws Exception {
        return PublicResolver.load(ENS.load(k80.resolveRegistryContract(this.web3j.netVersion().send().getNetVersion()), this.web3j, this.transactionManager, new oj0()).resolver(xc2.nameHashAsBytes(str)).send(), this.web3j, this.transactionManager, new oj0());
    }

    public long getSyncThreshold() {
        return this.syncThreshold;
    }

    public boolean isSynced() throws Exception {
        if (this.web3j.ethSyncing().send().isSyncing()) {
            return false;
        }
        return System.currentTimeMillis() - this.syncThreshold < m30.longValueExact(this.web3j.ethGetBlockByNumber(DefaultBlockParameterName.LATEST, false).send().getBlock().getTimestamp()) * 1000;
    }

    public PublicResolver obtainPublicResolver(String str) {
        if (isValidEnsName(str, this.addressLength)) {
            try {
                if (isSynced()) {
                    return lookupResolver(str);
                }
                throw new EnsResolutionException("Node is not currently synced");
            } catch (Exception e) {
                throw new EnsResolutionException("Unable to determine sync status of node", e);
            }
        }
        throw new EnsResolutionException("EnsName is invalid: " + str);
    }

    public String resolve(String str) {
        if (isValidEnsName(str, this.addressLength)) {
            try {
                String send = obtainPublicResolver(str).addr(xc2.nameHashAsBytes(str)).send();
                if (g.isValidAddress(send)) {
                    return send;
                }
                throw new RuntimeException("Unable to resolve address for name: " + str);
            } catch (Exception e) {
                throw new RuntimeException("Unable to execute Ethereum request", e);
            }
        }
        return str;
    }

    public String reverseResolve(String str) {
        if (g.isValidAddress(str, this.addressLength)) {
            String str2 = ej2.cleanHexPrefix(str) + REVERSE_NAME_SUFFIX;
            try {
                String send = obtainPublicResolver(str2).name(xc2.nameHashAsBytes(str2)).send();
                if (isValidEnsName(send, this.addressLength)) {
                    return send;
                }
                throw new RuntimeException("Unable to resolve name for address: " + str);
            } catch (Exception e) {
                throw new RuntimeException("Unable to execute Ethereum request", e);
            }
        }
        throw new EnsResolutionException("Address is invalid: " + str);
    }

    public void setSyncThreshold(long j) {
        this.syncThreshold = j;
    }

    public static boolean isValidEnsName(String str, int i) {
        return str != null && (str.contains(".") || !g.isValidAddress(str, i));
    }

    public a(ko4 ko4Var, long j) {
        this(ko4Var, j, 40);
    }

    public a(ko4 ko4Var) {
        this(ko4Var, DEFAULT_SYNC_THRESHOLD);
    }
}
