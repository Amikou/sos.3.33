package org.web3j.ens.contracts.generated;

import defpackage.i80;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.generated.Bytes32;
import org.web3j.abi.datatypes.generated.Uint64;

/* loaded from: classes3.dex */
public class ENS extends i80 {
    private static final String BINARY = "608060405234801561001057600080fd5b5060008080526020527fad3228b676f7d3cd4284a5443f17f1962b36e491b30a40b2405849e597ba5fb58054600160a060020a03191633179055610500806100596000396000f3006080604052600436106100825763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416630178b8bf811461008757806302571be3146100bb57806306ab5923146100d357806314ab9038146100fc57806316a25cbd146101215780631896f70a146101565780635b0fc9c31461017a575b600080fd5b34801561009357600080fd5b5061009f60043561019e565b60408051600160a060020a039092168252519081900360200190f35b3480156100c757600080fd5b5061009f6004356101bc565b3480156100df57600080fd5b506100fa600435602435600160a060020a03604435166101d7565b005b34801561010857600080fd5b506100fa60043567ffffffffffffffff60243516610291565b34801561012d57600080fd5b5061013960043561035a565b6040805167ffffffffffffffff9092168252519081900360200190f35b34801561016257600080fd5b506100fa600435600160a060020a0360243516610391565b34801561018657600080fd5b506100fa600435600160a060020a0360243516610434565b600090815260208190526040902060010154600160a060020a031690565b600090815260208190526040902054600160a060020a031690565b6000838152602081905260408120548490600160a060020a031633146101fc57600080fd5b60408051868152602080820187905282519182900383018220600160a060020a03871683529251929450869288927fce0457fe73731f824cc272376169235128c118b49d344817417c6d108d155e8292908290030190a3506000908152602081905260409020805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03929092169190911790555050565b6000828152602081905260409020548290600160a060020a031633146102b657600080fd5b6040805167ffffffffffffffff84168152905184917f1d4f9bbfc9cab89d66e1a1562f2233ccbf1308cb4f63de2ead5787adddb8fa68919081900360200190a250600091825260208290526040909120600101805467ffffffffffffffff90921674010000000000000000000000000000000000000000027fffffffff0000000000000000ffffffffffffffffffffffffffffffffffffffff909216919091179055565b60009081526020819052604090206001015474010000000000000000000000000000000000000000900467ffffffffffffffff1690565b6000828152602081905260409020548290600160a060020a031633146103b657600080fd5b60408051600160a060020a0384168152905184917f335721b01866dc23fbee8b6b2c7b1e14d6f05c28cd35a2c934239f94095602a0919081900360200190a250600091825260208290526040909120600101805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03909216919091179055565b6000828152602081905260409020548290600160a060020a0316331461045957600080fd5b60408051600160a060020a0384168152905184917fd4735d920b0f87494915f556dd9b54c8f309026070caea5c737245152564d266919081900360200190a250600091825260208290526040909120805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a039092169190911790555600a165627a7a72305820f201bf7d54db31743dfa9e72c9529bab797c8e550d2355afb39d50e2ab48b4fb0029";
    public static final String FUNC_OWNER = "owner";
    public static final String FUNC_RESOLVER = "resolver";
    public static final String FUNC_SETOWNER = "setOwner";
    public static final String FUNC_SETRESOLVER = "setResolver";
    public static final String FUNC_SETSUBNODEOWNER = "setSubnodeOwner";
    public static final String FUNC_SETTTL = "setTTL";
    public static final String FUNC_TTL = "ttl";
    public static final px0 NEWOWNER_EVENT = new px0("NewOwner", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.ENS.1
    }, new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.ENS.2
    }, new TypeReference<Address>() { // from class: org.web3j.ens.contracts.generated.ENS.3
    }));
    public static final px0 TRANSFER_EVENT = new px0("Transfer", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.ENS.4
    }, new TypeReference<Address>() { // from class: org.web3j.ens.contracts.generated.ENS.5
    }));
    public static final px0 NEWRESOLVER_EVENT = new px0("NewResolver", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.ENS.6
    }, new TypeReference<Address>() { // from class: org.web3j.ens.contracts.generated.ENS.7
    }));
    public static final px0 NEWTTL_EVENT = new px0("NewTTL", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.ENS.8
    }, new TypeReference<Uint64>() { // from class: org.web3j.ens.contracts.generated.ENS.9
    }));

    /* loaded from: classes3.dex */
    public class a implements ld1<q12, e> {
        public a() {
        }

        @Override // defpackage.ld1
        public e apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = ENS.this.lambda$extractEventParametersWithLog$12(ENS.NEWOWNER_EVENT, q12Var);
            e eVar = new e();
            eVar.log = q12Var;
            eVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            eVar.label = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(1).getValue();
            eVar.owner = (String) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return eVar;
        }
    }

    /* loaded from: classes3.dex */
    public class b implements ld1<q12, h> {
        public b() {
        }

        @Override // defpackage.ld1
        public h apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = ENS.this.lambda$extractEventParametersWithLog$12(ENS.TRANSFER_EVENT, q12Var);
            h hVar = new h();
            hVar.log = q12Var;
            hVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            hVar.owner = (String) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return hVar;
        }
    }

    /* loaded from: classes3.dex */
    public class c implements ld1<q12, f> {
        public c() {
        }

        @Override // defpackage.ld1
        public f apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = ENS.this.lambda$extractEventParametersWithLog$12(ENS.NEWRESOLVER_EVENT, q12Var);
            f fVar = new f();
            fVar.log = q12Var;
            fVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            fVar.resolver = (String) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return fVar;
        }
    }

    /* loaded from: classes3.dex */
    public class d implements ld1<q12, g> {
        public d() {
        }

        @Override // defpackage.ld1
        public g apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = ENS.this.lambda$extractEventParametersWithLog$12(ENS.NEWTTL_EVENT, q12Var);
            g gVar = new g();
            gVar.log = q12Var;
            gVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            gVar.ttl = (BigInteger) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return gVar;
        }
    }

    /* loaded from: classes3.dex */
    public static class e {
        public byte[] label;
        public q12 log;
        public byte[] node;
        public String owner;
    }

    /* loaded from: classes3.dex */
    public static class f {
        public q12 log;
        public byte[] node;
        public String resolver;
    }

    /* loaded from: classes3.dex */
    public static class g {
        public q12 log;
        public byte[] node;
        public BigInteger ttl;
    }

    /* loaded from: classes3.dex */
    public static class h {
        public q12 log;
        public byte[] node;
        public String owner;
    }

    @Deprecated
    public ENS(String str, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        super(BINARY, str, ko4Var, ma0Var, bigInteger, bigInteger2);
    }

    public static org.web3j.protocol.core.b<ENS> deploy(ko4 ko4Var, ma0 ma0Var, j80 j80Var) {
        return i80.deployRemoteCall(ENS.class, ko4Var, ma0Var, j80Var, BINARY, "");
    }

    @Deprecated
    public static ENS load(String str, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        return new ENS(str, ko4Var, ma0Var, bigInteger, bigInteger2);
    }

    public List<e> getNewOwnerEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(NEWOWNER_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            e eVar = new e();
            eVar.log = bVar.getLog();
            eVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            eVar.label = (byte[]) bVar.getIndexedValues().get(1).getValue();
            eVar.owner = (String) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(eVar);
        }
        return arrayList;
    }

    public List<f> getNewResolverEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(NEWRESOLVER_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            f fVar = new f();
            fVar.log = bVar.getLog();
            fVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            fVar.resolver = (String) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(fVar);
        }
        return arrayList;
    }

    public List<g> getNewTTLEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(NEWTTL_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            g gVar = new g();
            gVar.log = bVar.getLog();
            gVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            gVar.ttl = (BigInteger) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(gVar);
        }
        return arrayList;
    }

    public List<h> getTransferEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(TRANSFER_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            h hVar = new h();
            hVar.log = bVar.getLog();
            hVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            hVar.owner = (String) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(hVar);
        }
        return arrayList;
    }

    public q71<e> newOwnerEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new a());
    }

    public q71<f> newResolverEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new c());
    }

    public q71<g> newTTLEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new d());
    }

    public org.web3j.protocol.core.b<String> owner(byte[] bArr) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_OWNER, Arrays.asList(new Bytes32(bArr)), Arrays.asList(new TypeReference<Address>() { // from class: org.web3j.ens.contracts.generated.ENS.11
        })), String.class);
    }

    public org.web3j.protocol.core.b<String> resolver(byte[] bArr) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_RESOLVER, Arrays.asList(new Bytes32(bArr)), Arrays.asList(new TypeReference<Address>() { // from class: org.web3j.ens.contracts.generated.ENS.10
        })), String.class);
    }

    public org.web3j.protocol.core.b<w84> setOwner(byte[] bArr, String str) {
        return executeRemoteCallTransaction(new id1(FUNC_SETOWNER, Arrays.asList(new Bytes32(bArr), new Address(str)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setResolver(byte[] bArr, String str) {
        return executeRemoteCallTransaction(new id1(FUNC_SETRESOLVER, Arrays.asList(new Bytes32(bArr), new Address(str)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setSubnodeOwner(byte[] bArr, byte[] bArr2, String str) {
        return executeRemoteCallTransaction(new id1(FUNC_SETSUBNODEOWNER, Arrays.asList(new Bytes32(bArr), new Bytes32(bArr2), new Address(str)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setTTL(byte[] bArr, BigInteger bigInteger) {
        return executeRemoteCallTransaction(new id1(FUNC_SETTTL, Arrays.asList(new Bytes32(bArr), new Uint64(bigInteger)), Collections.emptyList()));
    }

    public q71<h> transferEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new b());
    }

    public org.web3j.protocol.core.b<BigInteger> ttl(byte[] bArr) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_TTL, Arrays.asList(new Bytes32(bArr)), Arrays.asList(new TypeReference<Uint64>() { // from class: org.web3j.ens.contracts.generated.ENS.12
        })), BigInteger.class);
    }

    public ENS(String str, ko4 ko4Var, ma0 ma0Var, j80 j80Var) {
        super(BINARY, str, ko4Var, ma0Var, j80Var);
    }

    public static org.web3j.protocol.core.b<ENS> deploy(ko4 ko4Var, u84 u84Var, j80 j80Var) {
        return i80.deployRemoteCall(ENS.class, ko4Var, u84Var, j80Var, BINARY, "");
    }

    @Deprecated
    public static ENS load(String str, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2) {
        return new ENS(str, ko4Var, u84Var, bigInteger, bigInteger2);
    }

    public q71<e> newOwnerEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(NEWOWNER_EVENT));
        return newOwnerEventFlowable(qw0Var);
    }

    public q71<f> newResolverEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(NEWRESOLVER_EVENT));
        return newResolverEventFlowable(qw0Var);
    }

    public q71<g> newTTLEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(NEWTTL_EVENT));
        return newTTLEventFlowable(qw0Var);
    }

    public q71<h> transferEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(TRANSFER_EVENT));
        return transferEventFlowable(qw0Var);
    }

    @Deprecated
    public ENS(String str, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2) {
        super(BINARY, str, ko4Var, u84Var, bigInteger, bigInteger2);
    }

    @Deprecated
    public static org.web3j.protocol.core.b<ENS> deploy(ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        return i80.deployRemoteCall(ENS.class, ko4Var, ma0Var, bigInteger, bigInteger2, BINARY, "");
    }

    public static ENS load(String str, ko4 ko4Var, ma0 ma0Var, j80 j80Var) {
        return new ENS(str, ko4Var, ma0Var, j80Var);
    }

    public ENS(String str, ko4 ko4Var, u84 u84Var, j80 j80Var) {
        super(BINARY, str, ko4Var, u84Var, j80Var);
    }

    @Deprecated
    public static org.web3j.protocol.core.b<ENS> deploy(ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2) {
        return i80.deployRemoteCall(ENS.class, ko4Var, u84Var, bigInteger, bigInteger2, BINARY, "");
    }

    public static ENS load(String str, ko4 ko4Var, u84 u84Var, j80 j80Var) {
        return new ENS(str, ko4Var, u84Var, j80Var);
    }
}
