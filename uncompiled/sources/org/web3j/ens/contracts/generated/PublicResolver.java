package org.web3j.ens.contracts.generated;

import defpackage.i80;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.DynamicBytes;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Bytes32;
import org.web3j.abi.datatypes.generated.Bytes4;
import org.web3j.abi.datatypes.generated.Uint256;

/* loaded from: classes3.dex */
public class PublicResolver extends i80 {
    private static final String BINARY = "608060405234801561001057600080fd5b506040516020806111dd833981016040525160008054600160a060020a03909216600160a060020a031990921691909117905561118b806100526000396000f3006080604052600436106100c45763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166301ffc9a781146100c957806310f13a8c146100ff5780632203ab561461019d57806329cd62ea146102375780632dff6941146102555780633b3b57de1461027f57806359d1d43c146102b3578063623195b014610386578063691f3431146103e657806377372213146103fe578063c3d014d61461045c578063c869023314610477578063d5fa2b00146104a8575b600080fd5b3480156100d557600080fd5b506100eb600160e060020a0319600435166104cc565b604080519115158252519081900360200190f35b34801561010b57600080fd5b5060408051602060046024803582810135601f810185900485028601850190965285855261019b95833595369560449491939091019190819084018382808284375050604080516020601f89358b018035918201839004830284018301909452808352979a9998810197919650918201945092508291508401838280828437509497506106399650505050505050565b005b3480156101a957600080fd5b506101b860043560243561084d565b6040518083815260200180602001828103825283818151815260200191508051906020019080838360005b838110156101fb5781810151838201526020016101e3565b50505050905090810190601f1680156102285780820380516001836020036101000a031916815260200191505b50935050505060405180910390f35b34801561024357600080fd5b5061019b600435602435604435610959565b34801561026157600080fd5b5061026d600435610a5d565b60408051918252519081900360200190f35b34801561028b57600080fd5b50610297600435610a73565b60408051600160a060020a039092168252519081900360200190f35b3480156102bf57600080fd5b5060408051602060046024803582810135601f8101859004850286018501909652858552610311958335953695604494919390910191908190840183828082843750949750610a8e9650505050505050565b6040805160208082528351818301528351919283929083019185019080838360005b8381101561034b578181015183820152602001610333565b50505050905090810190601f1680156103785780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34801561039257600080fd5b50604080516020600460443581810135601f810184900484028501840190955284845261019b948235946024803595369594606494920191908190840183828082843750949750610b979650505050505050565b3480156103f257600080fd5b50610311600435610c9c565b34801561040a57600080fd5b5060408051602060046024803582810135601f810185900485028601850190965285855261019b958335953695604494919390910191908190840183828082843750949750610d409650505050505050565b34801561046857600080fd5b5061019b600435602435610e9a565b34801561048357600080fd5b5061048f600435610f7f565b6040805192835260208301919091528051918290030190f35b3480156104b457600080fd5b5061019b600435600160a060020a0360243516610f9c565b6000600160e060020a031982167f3b3b57de00000000000000000000000000000000000000000000000000000000148061052f5750600160e060020a031982167fd8389dc500000000000000000000000000000000000000000000000000000000145b806105635750600160e060020a031982167f691f343100000000000000000000000000000000000000000000000000000000145b806105975750600160e060020a031982167f2203ab5600000000000000000000000000000000000000000000000000000000145b806105cb5750600160e060020a031982167fc869023300000000000000000000000000000000000000000000000000000000145b806105ff5750600160e060020a031982167f59d1d43c00000000000000000000000000000000000000000000000000000000145b806106335750600160e060020a031982167f01ffc9a700000000000000000000000000000000000000000000000000000000145b92915050565b6000805460408051600080516020611140833981519152815260048101879052905186933393600160a060020a0316926302571be39260248083019360209383900390910190829087803b15801561069057600080fd5b505af11580156106a4573d6000803e3d6000fd5b505050506040513d60208110156106ba57600080fd5b5051600160a060020a0316146106cf57600080fd5b6000848152600160209081526040918290209151855185936005019287929182918401908083835b602083106107165780518252601f1990920191602091820191016106f7565b51815160209384036101000a6000190180199092169116179052920194855250604051938490038101909320845161075795919491909101925090506110a4565b50826040518082805190602001908083835b602083106107885780518252601f199092019160209182019101610769565b51815160209384036101000a60001901801990921691161790526040805192909401829003822081835289518383015289519096508a95507fd8c9334b1a9c2f9da342a0a2b32629c1a229b6445dad78947f674b44444a7550948a94508392908301919085019080838360005b8381101561080d5781810151838201526020016107f5565b50505050905090810190601f16801561083a5780820380516001836020036101000a031916815260200191505b509250505060405180910390a350505050565b60008281526001602081905260409091206060905b83831161094c578284161580159061089b5750600083815260068201602052604081205460026000196101006001841615020190911604115b1561094157600083815260068201602090815260409182902080548351601f6002600019610100600186161502019093169290920491820184900484028101840190945280845290918301828280156109355780601f1061090a57610100808354040283529160200191610935565b820191906000526020600020905b81548152906001019060200180831161091857829003601f168201915b50505050509150610951565b600290920291610862565b600092505b509250929050565b6000805460408051600080516020611140833981519152815260048101879052905186933393600160a060020a0316926302571be39260248083019360209383900390910190829087803b1580156109b057600080fd5b505af11580156109c4573d6000803e3d6000fd5b505050506040513d60208110156109da57600080fd5b5051600160a060020a0316146109ef57600080fd5b604080518082018252848152602080820185815260008881526001835284902092516003840155516004909201919091558151858152908101849052815186927f1d6f5e03d3f63eb58751986629a5439baee5079ff04f345becb66e23eb154e46928290030190a250505050565b6000908152600160208190526040909120015490565b600090815260016020526040902054600160a060020a031690565b600082815260016020908152604091829020915183516060936005019285929182918401908083835b60208310610ad65780518252601f199092019160209182019101610ab7565b518151600019602094850361010090810a820192831692199390931691909117909252949092019687526040805197889003820188208054601f6002600183161590980290950116959095049283018290048202880182019052818752929450925050830182828015610b8a5780601f10610b5f57610100808354040283529160200191610b8a565b820191906000526020600020905b815481529060010190602001808311610b6d57829003601f168201915b5050505050905092915050565b6000805460408051600080516020611140833981519152815260048101879052905186933393600160a060020a0316926302571be39260248083019360209383900390910190829087803b158015610bee57600080fd5b505af1158015610c02573d6000803e3d6000fd5b505050506040513d6020811015610c1857600080fd5b5051600160a060020a031614610c2d57600080fd5b6000198301831615610c3e57600080fd5b600084815260016020908152604080832086845260060182529091208351610c68928501906110a4565b50604051839085907faa121bbeef5f32f5961a2a28966e769023910fc9479059ee3495d4c1a696efe390600090a350505050565b6000818152600160208181526040928390206002908101805485516000199582161561010002959095011691909104601f81018390048302840183019094528383526060939091830182828015610d345780601f10610d0957610100808354040283529160200191610d34565b820191906000526020600020905b815481529060010190602001808311610d1757829003601f168201915b50505050509050919050565b6000805460408051600080516020611140833981519152815260048101869052905185933393600160a060020a0316926302571be39260248083019360209383900390910190829087803b158015610d9757600080fd5b505af1158015610dab573d6000803e3d6000fd5b505050506040513d6020811015610dc157600080fd5b5051600160a060020a031614610dd657600080fd5b60008381526001602090815260409091208351610dfb926002909201918501906110a4565b50604080516020808252845181830152845186937fb7d29e911041e8d9b843369e890bcb72c9388692ba48b65ac54e7214c4c348f79387939092839283019185019080838360005b83811015610e5b578181015183820152602001610e43565b50505050905090810190601f168015610e885780820380516001836020036101000a031916815260200191505b509250505060405180910390a2505050565b6000805460408051600080516020611140833981519152815260048101869052905185933393600160a060020a0316926302571be39260248083019360209383900390910190829087803b158015610ef157600080fd5b505af1158015610f05573d6000803e3d6000fd5b505050506040513d6020811015610f1b57600080fd5b5051600160a060020a031614610f3057600080fd5b6000838152600160208181526040928390209091018490558151848152915185927f0424b6fe0d9c3bdbece0e7879dc241bb0c22e900be8b6c168b4ee08bd9bf83bc92908290030190a2505050565b600090815260016020526040902060038101546004909101549091565b6000805460408051600080516020611140833981519152815260048101869052905185933393600160a060020a0316926302571be39260248083019360209383900390910190829087803b158015610ff357600080fd5b505af1158015611007573d6000803e3d6000fd5b505050506040513d602081101561101d57600080fd5b5051600160a060020a03161461103257600080fd5b600083815260016020908152604091829020805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0386169081179091558251908152915185927f52d7d861f09ab3d26239d492e8968629f95e9e318cf0b73bfddc441522a15fd292908290030190a2505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106110e557805160ff1916838001178555611112565b82800160010185558215611112579182015b828111156111125782518255916020019190600101906110f7565b5061111e929150611122565b5090565b61113c91905b8082111561111e5760008155600101611128565b90560002571be300000000000000000000000000000000000000000000000000000000a165627a7a72305820c27de7e59fab6007ff39ba74a6a8d7e8a5eac02905b0a26fee254f94ff3ae4b60029";
    public static final String FUNC_ABI = "ABI";
    public static final String FUNC_ADDR = "addr";
    public static final String FUNC_CONTENT = "content";
    public static final String FUNC_NAME = "name";
    public static final String FUNC_PUBKEY = "pubkey";
    public static final String FUNC_SETABI = "setABI";
    public static final String FUNC_SETADDR = "setAddr";
    public static final String FUNC_SETCONTENT = "setContent";
    public static final String FUNC_SETNAME = "setName";
    public static final String FUNC_SETPUBKEY = "setPubkey";
    public static final String FUNC_SETTEXT = "setText";
    public static final String FUNC_SUPPORTSINTERFACE = "supportsInterface";
    public static final String FUNC_TEXT = "text";
    public static final px0 ADDRCHANGED_EVENT = new px0("AddrChanged", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.1
    }, new TypeReference<Address>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.2
    }));
    public static final px0 CONTENTCHANGED_EVENT = new px0("ContentChanged", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.3
    }, new TypeReference<Bytes32>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.4
    }));
    public static final px0 NAMECHANGED_EVENT = new px0("NameChanged", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.5
    }, new TypeReference<Utf8String>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.6
    }));
    public static final px0 ABICHANGED_EVENT = new px0("ABIChanged", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.7
    }, new TypeReference<Uint256>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.8
    }));
    public static final px0 PUBKEYCHANGED_EVENT = new px0("PubkeyChanged", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.9
    }, new TypeReference<Bytes32>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.10
    }, new TypeReference<Bytes32>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.11
    }));
    public static final px0 TEXTCHANGED_EVENT = new px0("TextChanged", Arrays.asList(new TypeReference<Bytes32>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.12
    }, new TypeReference<Utf8String>(true) { // from class: org.web3j.ens.contracts.generated.PublicResolver.13
    }, new TypeReference<Utf8String>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.14
    }));

    /* loaded from: classes3.dex */
    public class a implements Callable<oc4<BigInteger, byte[]>> {
        public final /* synthetic */ id1 val$function;

        public a(id1 id1Var) {
            this.val$function = id1Var;
        }

        @Override // java.util.concurrent.Callable
        public oc4<BigInteger, byte[]> call() throws Exception {
            List lambda$executeRemoteCallMultipleValueReturn$2 = PublicResolver.this.lambda$executeRemoteCallMultipleValueReturn$2(this.val$function);
            return new oc4<>((BigInteger) ((zc4) lambda$executeRemoteCallMultipleValueReturn$2.get(0)).getValue(), (byte[]) ((zc4) lambda$executeRemoteCallMultipleValueReturn$2.get(1)).getValue());
        }
    }

    /* loaded from: classes3.dex */
    public class b implements Callable<oc4<byte[], byte[]>> {
        public final /* synthetic */ id1 val$function;

        public b(id1 id1Var) {
            this.val$function = id1Var;
        }

        @Override // java.util.concurrent.Callable
        public oc4<byte[], byte[]> call() throws Exception {
            List lambda$executeRemoteCallMultipleValueReturn$2 = PublicResolver.this.lambda$executeRemoteCallMultipleValueReturn$2(this.val$function);
            return new oc4<>((byte[]) ((zc4) lambda$executeRemoteCallMultipleValueReturn$2.get(0)).getValue(), (byte[]) ((zc4) lambda$executeRemoteCallMultipleValueReturn$2.get(1)).getValue());
        }
    }

    /* loaded from: classes3.dex */
    public class c implements ld1<q12, j> {
        public c() {
        }

        @Override // defpackage.ld1
        public j apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = PublicResolver.this.lambda$extractEventParametersWithLog$12(PublicResolver.ADDRCHANGED_EVENT, q12Var);
            j jVar = new j();
            jVar.log = q12Var;
            jVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            jVar.a = (String) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return jVar;
        }
    }

    /* loaded from: classes3.dex */
    public class d implements ld1<q12, k> {
        public d() {
        }

        @Override // defpackage.ld1
        public k apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = PublicResolver.this.lambda$extractEventParametersWithLog$12(PublicResolver.CONTENTCHANGED_EVENT, q12Var);
            k kVar = new k();
            kVar.log = q12Var;
            kVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            kVar.hash = (byte[]) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return kVar;
        }
    }

    /* loaded from: classes3.dex */
    public class e implements ld1<q12, l> {
        public e() {
        }

        @Override // defpackage.ld1
        public l apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = PublicResolver.this.lambda$extractEventParametersWithLog$12(PublicResolver.NAMECHANGED_EVENT, q12Var);
            l lVar = new l();
            lVar.log = q12Var;
            lVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            lVar.name = (String) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return lVar;
        }
    }

    /* loaded from: classes3.dex */
    public class f implements ld1<q12, i> {
        public f() {
        }

        @Override // defpackage.ld1
        public i apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = PublicResolver.this.lambda$extractEventParametersWithLog$12(PublicResolver.ABICHANGED_EVENT, q12Var);
            i iVar = new i();
            iVar.log = q12Var;
            iVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            iVar.contentType = (BigInteger) lambda$extractEventParametersWithLog$12.getIndexedValues().get(1).getValue();
            return iVar;
        }
    }

    /* loaded from: classes3.dex */
    public class g implements ld1<q12, m> {
        public g() {
        }

        @Override // defpackage.ld1
        public m apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = PublicResolver.this.lambda$extractEventParametersWithLog$12(PublicResolver.PUBKEYCHANGED_EVENT, q12Var);
            m mVar = new m();
            mVar.log = q12Var;
            mVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            mVar.x = (byte[]) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            mVar.y = (byte[]) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(1).getValue();
            return mVar;
        }
    }

    /* loaded from: classes3.dex */
    public class h implements ld1<q12, n> {
        public h() {
        }

        @Override // defpackage.ld1
        public n apply(q12 q12Var) {
            i80.b lambda$extractEventParametersWithLog$12 = PublicResolver.this.lambda$extractEventParametersWithLog$12(PublicResolver.TEXTCHANGED_EVENT, q12Var);
            n nVar = new n();
            nVar.log = q12Var;
            nVar.node = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(0).getValue();
            nVar.indexedKey = (byte[]) lambda$extractEventParametersWithLog$12.getIndexedValues().get(1).getValue();
            nVar.key = (String) lambda$extractEventParametersWithLog$12.getNonIndexedValues().get(0).getValue();
            return nVar;
        }
    }

    /* loaded from: classes3.dex */
    public static class i {
        public BigInteger contentType;
        public q12 log;
        public byte[] node;
    }

    /* loaded from: classes3.dex */
    public static class j {
        public String a;
        public q12 log;
        public byte[] node;
    }

    /* loaded from: classes3.dex */
    public static class k {
        public byte[] hash;
        public q12 log;
        public byte[] node;
    }

    /* loaded from: classes3.dex */
    public static class l {
        public q12 log;
        public String name;
        public byte[] node;
    }

    /* loaded from: classes3.dex */
    public static class m {
        public q12 log;
        public byte[] node;
        public byte[] x;
        public byte[] y;
    }

    /* loaded from: classes3.dex */
    public static class n {
        public byte[] indexedKey;
        public String key;
        public q12 log;
        public byte[] node;
    }

    @Deprecated
    public PublicResolver(String str, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        super(BINARY, str, ko4Var, ma0Var, bigInteger, bigInteger2);
    }

    public static org.web3j.protocol.core.b<PublicResolver> deploy(ko4 ko4Var, ma0 ma0Var, j80 j80Var, String str) {
        return i80.deployRemoteCall(PublicResolver.class, ko4Var, ma0Var, j80Var, BINARY, org.web3j.abi.b.encodeConstructor(Arrays.asList(new Address(str))));
    }

    @Deprecated
    public static PublicResolver load(String str, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        return new PublicResolver(str, ko4Var, ma0Var, bigInteger, bigInteger2);
    }

    public org.web3j.protocol.core.b<oc4<BigInteger, byte[]>> ABI(byte[] bArr, BigInteger bigInteger) {
        return new org.web3j.protocol.core.b<>(new a(new id1(FUNC_ABI, Arrays.asList(new Bytes32(bArr), new Uint256(bigInteger)), Arrays.asList(new TypeReference<Uint256>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.16
        }, new TypeReference<DynamicBytes>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.17
        }))));
    }

    public q71<i> aBIChangedEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new f());
    }

    public org.web3j.protocol.core.b<String> addr(byte[] bArr) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_ADDR, Arrays.asList(new Bytes32(bArr)), Arrays.asList(new TypeReference<Address>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.20
        })), String.class);
    }

    public q71<j> addrChangedEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new c());
    }

    public org.web3j.protocol.core.b<byte[]> content(byte[] bArr) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_CONTENT, Arrays.asList(new Bytes32(bArr)), Arrays.asList(new TypeReference<Bytes32>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.19
        })), byte[].class);
    }

    public q71<k> contentChangedEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new d());
    }

    public List<i> getABIChangedEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(ABICHANGED_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            i iVar = new i();
            iVar.log = bVar.getLog();
            iVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            iVar.contentType = (BigInteger) bVar.getIndexedValues().get(1).getValue();
            arrayList.add(iVar);
        }
        return arrayList;
    }

    public List<j> getAddrChangedEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(ADDRCHANGED_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            j jVar = new j();
            jVar.log = bVar.getLog();
            jVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            jVar.a = (String) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(jVar);
        }
        return arrayList;
    }

    public List<k> getContentChangedEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(CONTENTCHANGED_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            k kVar = new k();
            kVar.log = bVar.getLog();
            kVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            kVar.hash = (byte[]) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(kVar);
        }
        return arrayList;
    }

    public List<l> getNameChangedEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(NAMECHANGED_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            l lVar = new l();
            lVar.log = bVar.getLog();
            lVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            lVar.name = (String) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(lVar);
        }
        return arrayList;
    }

    public List<m> getPubkeyChangedEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(PUBKEYCHANGED_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            m mVar = new m();
            mVar.log = bVar.getLog();
            mVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            mVar.x = (byte[]) bVar.getNonIndexedValues().get(0).getValue();
            mVar.y = (byte[]) bVar.getNonIndexedValues().get(1).getValue();
            arrayList.add(mVar);
        }
        return arrayList;
    }

    public List<n> getTextChangedEvents(w84 w84Var) {
        List<i80.b> extractEventParametersWithLog = extractEventParametersWithLog(TEXTCHANGED_EVENT, w84Var);
        ArrayList arrayList = new ArrayList(extractEventParametersWithLog.size());
        for (i80.b bVar : extractEventParametersWithLog) {
            n nVar = new n();
            nVar.log = bVar.getLog();
            nVar.node = (byte[]) bVar.getIndexedValues().get(0).getValue();
            nVar.indexedKey = (byte[]) bVar.getIndexedValues().get(1).getValue();
            nVar.key = (String) bVar.getNonIndexedValues().get(0).getValue();
            arrayList.add(nVar);
        }
        return arrayList;
    }

    public org.web3j.protocol.core.b<String> name(byte[] bArr) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_NAME, Arrays.asList(new Bytes32(bArr)), Arrays.asList(new TypeReference<Utf8String>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.22
        })), String.class);
    }

    public q71<l> nameChangedEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new e());
    }

    public org.web3j.protocol.core.b<oc4<byte[], byte[]>> pubkey(byte[] bArr) {
        return new org.web3j.protocol.core.b<>(new b(new id1(FUNC_PUBKEY, Arrays.asList(new Bytes32(bArr)), Arrays.asList(new TypeReference<Bytes32>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.23
        }, new TypeReference<Bytes32>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.24
        }))));
    }

    public q71<m> pubkeyChangedEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new g());
    }

    public org.web3j.protocol.core.b<w84> setABI(byte[] bArr, BigInteger bigInteger, byte[] bArr2) {
        return executeRemoteCallTransaction(new id1(FUNC_SETABI, Arrays.asList(new Bytes32(bArr), new Uint256(bigInteger), new DynamicBytes(bArr2)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setAddr(byte[] bArr, String str) {
        return executeRemoteCallTransaction(new id1(FUNC_SETADDR, Arrays.asList(new Bytes32(bArr), new Address(str)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setContent(byte[] bArr, byte[] bArr2) {
        return executeRemoteCallTransaction(new id1(FUNC_SETCONTENT, Arrays.asList(new Bytes32(bArr), new Bytes32(bArr2)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setName(byte[] bArr, String str) {
        return executeRemoteCallTransaction(new id1(FUNC_SETNAME, Arrays.asList(new Bytes32(bArr), new Utf8String(str)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setPubkey(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        return executeRemoteCallTransaction(new id1(FUNC_SETPUBKEY, Arrays.asList(new Bytes32(bArr), new Bytes32(bArr2), new Bytes32(bArr3)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<w84> setText(byte[] bArr, String str, String str2) {
        return executeRemoteCallTransaction(new id1(FUNC_SETTEXT, Arrays.asList(new Bytes32(bArr), new Utf8String(str), new Utf8String(str2)), Collections.emptyList()));
    }

    public org.web3j.protocol.core.b<Boolean> supportsInterface(byte[] bArr) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_SUPPORTSINTERFACE, Arrays.asList(new Bytes4(bArr)), Arrays.asList(new TypeReference<Bool>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.15
        })), Boolean.class);
    }

    public org.web3j.protocol.core.b<String> text(byte[] bArr, String str) {
        return executeRemoteCallSingleValueReturn(new id1(FUNC_TEXT, Arrays.asList(new Bytes32(bArr), new Utf8String(str)), Arrays.asList(new TypeReference<Utf8String>() { // from class: org.web3j.ens.contracts.generated.PublicResolver.21
        })), String.class);
    }

    public q71<n> textChangedEventFlowable(qw0 qw0Var) {
        return this.web3j.ethLogFlowable(qw0Var).p(new h());
    }

    public PublicResolver(String str, ko4 ko4Var, ma0 ma0Var, j80 j80Var) {
        super(BINARY, str, ko4Var, ma0Var, j80Var);
    }

    @Deprecated
    public static PublicResolver load(String str, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2) {
        return new PublicResolver(str, ko4Var, u84Var, bigInteger, bigInteger2);
    }

    public q71<i> aBIChangedEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(ABICHANGED_EVENT));
        return aBIChangedEventFlowable(qw0Var);
    }

    public q71<j> addrChangedEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(ADDRCHANGED_EVENT));
        return addrChangedEventFlowable(qw0Var);
    }

    public q71<k> contentChangedEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(CONTENTCHANGED_EVENT));
        return contentChangedEventFlowable(qw0Var);
    }

    public q71<l> nameChangedEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(NAMECHANGED_EVENT));
        return nameChangedEventFlowable(qw0Var);
    }

    public q71<m> pubkeyChangedEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(PUBKEYCHANGED_EVENT));
        return pubkeyChangedEventFlowable(qw0Var);
    }

    public q71<n> textChangedEventFlowable(gi0 gi0Var, gi0 gi0Var2) {
        qw0 qw0Var = new qw0(gi0Var, gi0Var2, getContractAddress());
        qw0Var.addSingleTopic(ux0.encode(TEXTCHANGED_EVENT));
        return textChangedEventFlowable(qw0Var);
    }

    @Deprecated
    public PublicResolver(String str, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2) {
        super(BINARY, str, ko4Var, u84Var, bigInteger, bigInteger2);
    }

    public static org.web3j.protocol.core.b<PublicResolver> deploy(ko4 ko4Var, u84 u84Var, j80 j80Var, String str) {
        return i80.deployRemoteCall(PublicResolver.class, ko4Var, u84Var, j80Var, BINARY, org.web3j.abi.b.encodeConstructor(Arrays.asList(new Address(str))));
    }

    public static PublicResolver load(String str, ko4 ko4Var, ma0 ma0Var, j80 j80Var) {
        return new PublicResolver(str, ko4Var, ma0Var, j80Var);
    }

    public PublicResolver(String str, ko4 ko4Var, u84 u84Var, j80 j80Var) {
        super(BINARY, str, ko4Var, u84Var, j80Var);
    }

    public static PublicResolver load(String str, ko4 ko4Var, u84 u84Var, j80 j80Var) {
        return new PublicResolver(str, ko4Var, u84Var, j80Var);
    }

    @Deprecated
    public static org.web3j.protocol.core.b<PublicResolver> deploy(ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2, String str) {
        return i80.deployRemoteCall(PublicResolver.class, ko4Var, ma0Var, bigInteger, bigInteger2, BINARY, org.web3j.abi.b.encodeConstructor(Arrays.asList(new Address(str))));
    }

    @Deprecated
    public static org.web3j.protocol.core.b<PublicResolver> deploy(ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2, String str) {
        return i80.deployRemoteCall(PublicResolver.class, ko4Var, u84Var, bigInteger, bigInteger2, BINARY, org.web3j.abi.b.encodeConstructor(Arrays.asList(new Address(str))));
    }
}
