package org.web3j.ens;

/* loaded from: classes3.dex */
public class EnsResolutionException extends RuntimeException {
    public EnsResolutionException(String str) {
        super(str);
    }

    public EnsResolutionException(String str, Throwable th) {
        super(str, th);
    }
}
