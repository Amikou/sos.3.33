package org.web3j.utils;

import io.reactivex.BackpressureStrategy;
import java.math.BigInteger;

/* compiled from: Flowables.java */
/* loaded from: classes3.dex */
public class a {
    private static /* synthetic */ void lambda$range$0(BigInteger bigInteger, BigInteger bigInteger2, u71 u71Var) throws Exception {
        while (bigInteger.compareTo(bigInteger2) < 1 && !u71Var.isCancelled()) {
            u71Var.c(bigInteger);
            bigInteger = bigInteger.add(BigInteger.ONE);
        }
        if (u71Var.isCancelled()) {
            return;
        }
        u71Var.a();
    }

    private static /* synthetic */ void lambda$range$1(BigInteger bigInteger, BigInteger bigInteger2, u71 u71Var) throws Exception {
        while (bigInteger.compareTo(bigInteger2) > -1 && !u71Var.isCancelled()) {
            u71Var.c(bigInteger);
            bigInteger = bigInteger.subtract(BigInteger.ONE);
        }
        if (u71Var.isCancelled()) {
            return;
        }
        u71Var.a();
    }

    public static q71<BigInteger> range(BigInteger bigInteger, BigInteger bigInteger2) {
        return range(bigInteger, bigInteger2, true);
    }

    public static q71<BigInteger> range(final BigInteger bigInteger, final BigInteger bigInteger2, boolean z) {
        if (bigInteger.compareTo(BigInteger.ZERO) != -1) {
            if (bigInteger.compareTo(bigInteger2) <= 0) {
                if (z) {
                    return q71.d(new d81(bigInteger, bigInteger2) { // from class: g81
                    }, BackpressureStrategy.BUFFER);
                }
                return q71.d(new d81(bigInteger2, bigInteger) { // from class: g81
                }, BackpressureStrategy.BUFFER);
            }
            throw new IllegalArgumentException("Negative start index cannot be greater then end index");
        }
        throw new IllegalArgumentException("Negative start index cannot be used");
    }
}
