package org.web3j.utils;

import java.io.IOException;

/* compiled from: RevertReasonExtractor.java */
/* loaded from: classes3.dex */
public class b {
    public static final String MISSING_REASON = "N/A";

    public static String extractRevertReason(w84 w84Var, String str, ko4 ko4Var, Boolean bool) throws IOException {
        String retrieveRevertReason;
        if (w84Var.getRevertReason() != null) {
            return w84Var.getRevertReason();
        }
        if (!bool.booleanValue() || (retrieveRevertReason = retrieveRevertReason(w84Var, str, ko4Var)) == null) {
            return MISSING_REASON;
        }
        w84Var.setRevertReason(retrieveRevertReason);
        return retrieveRevertReason;
    }

    public static String retrieveRevertReason(w84 w84Var, String str, ko4 ko4Var) throws IOException {
        if (w84Var.getBlockNumber() == null) {
            return null;
        }
        return ko4Var.ethCall(p84.createEthCallTransaction(w84Var.getFrom(), w84Var.getTo(), str), fi0.b(w84Var.getBlockNumber())).send().getRevertReason();
    }
}
