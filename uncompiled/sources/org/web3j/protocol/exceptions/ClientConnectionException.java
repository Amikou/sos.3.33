package org.web3j.protocol.exceptions;

/* loaded from: classes3.dex */
public class ClientConnectionException extends RuntimeException {
    public ClientConnectionException(String str) {
        super(str);
    }
}
