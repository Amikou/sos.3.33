package org.web3j.protocol.exceptions;

import java8.util.o;

/* loaded from: classes3.dex */
public class TransactionException extends Exception {
    private o<String> transactionHash;
    private o<w84> transactionReceipt;

    public TransactionException(String str) {
        super(str);
        this.transactionHash = o.a();
        this.transactionReceipt = o.a();
    }

    public o<String> getTransactionHash() {
        return this.transactionHash;
    }

    public o<w84> getTransactionReceipt() {
        return this.transactionReceipt;
    }

    public TransactionException(String str, String str2) {
        super(str);
        this.transactionHash = o.a();
        this.transactionReceipt = o.a();
        this.transactionHash = o.e(str2);
    }

    public TransactionException(String str, w84 w84Var) {
        super(str);
        this.transactionHash = o.a();
        this.transactionReceipt = o.a();
        this.transactionReceipt = o.e(w84Var);
    }

    public TransactionException(Throwable th) {
        super(th);
        this.transactionHash = o.a();
        this.transactionReceipt = o.a();
    }
}
