package org.web3j.protocol.core.filters;

import defpackage.ex0;
import defpackage.i83;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java8.util.o;
import org.web3j.protocol.core.filters.b;

/* compiled from: Filter.java */
/* loaded from: classes3.dex */
public abstract class b<T> {
    private static final x12 log = org.slf4j.a.i(b.class);
    private long blockTime;
    public vu<T> callback;
    private volatile BigInteger filterId;
    public ScheduledFuture<?> schedule;
    private ScheduledExecutorService scheduledExecutorService;
    public final ko4 web3j;

    public b(ko4 ko4Var, vu<T> vuVar) {
        this.web3j = ko4Var;
        this.callback = vuVar;
    }

    private void getInitialFilterLogs() {
        ex0 ex0Var;
        try {
            o<org.web3j.protocol.core.c<?, ex0>> filterLogs = getFilterLogs(this.filterId);
            if (filterLogs.c()) {
                ex0Var = filterLogs.b().send();
            } else {
                ex0Var = new ex0();
                ex0Var.setResult(Collections.emptyList());
            }
            process(ex0Var.getLogs());
        } catch (IOException e) {
            throwException(e);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void lambda$run$0(pw0 pw0Var) {
        try {
            pollFilter(pw0Var);
        } catch (Throwable th) {
            log.error("Error sending request", th);
        }
    }

    private void pollFilter(pw0 pw0Var) {
        ex0 ex0Var;
        try {
            ex0Var = this.web3j.ethGetFilterChanges(this.filterId).send();
        } catch (IOException e) {
            throwException(e);
            ex0Var = null;
        }
        if (ex0Var.hasError()) {
            i83.a error = ex0Var.getError();
            if (error.getCode() != -32000) {
                throwException(error);
                return;
            } else {
                reinstallFilter();
                return;
            }
        }
        process(ex0Var.getLogs());
    }

    private void reinstallFilter() {
        x12 x12Var = log;
        x12Var.warn("The filter has not been found. Filter id: " + this.filterId);
        this.schedule.cancel(true);
        run(this.scheduledExecutorService, this.blockTime);
    }

    public void cancel() {
        this.schedule.cancel(false);
        try {
            ox0 uninstallFilter = uninstallFilter(this.filterId);
            if (uninstallFilter.hasError()) {
                throwException(uninstallFilter.getError());
            }
            if (uninstallFilter.isUninstalled()) {
                return;
            }
            throw new FilterException("Filter with id '" + this.filterId + "' failed to uninstall");
        } catch (IOException e) {
            throwException(e);
        }
    }

    public abstract o<org.web3j.protocol.core.c<?, ex0>> getFilterLogs(BigInteger bigInteger);

    public abstract void process(List<ex0.c> list);

    public void run(ScheduledExecutorService scheduledExecutorService, long j) {
        try {
            final pw0 sendRequest = sendRequest();
            if (sendRequest.hasError()) {
                throwException(sendRequest.getError());
            }
            this.filterId = sendRequest.getFilterId();
            this.scheduledExecutorService = scheduledExecutorService;
            this.blockTime = j;
            getInitialFilterLogs();
            this.schedule = scheduledExecutorService.scheduleAtFixedRate(new Runnable() { // from class: h41
                @Override // java.lang.Runnable
                public final void run() {
                    b.this.lambda$run$0(sendRequest);
                }
            }, 0L, j, TimeUnit.MILLISECONDS);
        } catch (IOException e) {
            throwException(e);
        }
    }

    public abstract pw0 sendRequest() throws IOException;

    public void throwException(i83.a aVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Invalid request: ");
        sb.append(aVar == null ? "Unknown Error" : aVar.getMessage());
        throw new FilterException(sb.toString());
    }

    public ox0 uninstallFilter(BigInteger bigInteger) throws IOException {
        return this.web3j.ethUninstallFilter(bigInteger).send();
    }

    public void throwException(Throwable th) {
        throw new FilterException("Error sending request", th);
    }
}
