package org.web3j.protocol.core.filters;

import defpackage.ex0;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java8.util.o;

/* compiled from: LogFilter.java */
/* loaded from: classes3.dex */
public class c extends b<q12> {
    public final qw0 ethFilter;

    public c(ko4 ko4Var, vu<q12> vuVar, qw0 qw0Var) {
        super(ko4Var, vuVar);
        this.ethFilter = qw0Var;
    }

    @Override // org.web3j.protocol.core.filters.b
    public o<org.web3j.protocol.core.c<?, ex0>> getFilterLogs(BigInteger bigInteger) {
        return o.d(this.web3j.ethGetFilterLogs(bigInteger));
    }

    @Override // org.web3j.protocol.core.filters.b
    public void process(List<ex0.c> list) {
        for (ex0.c cVar : list) {
            if (cVar instanceof ex0.b) {
                this.callback.onEvent(((ex0.b) cVar).get());
            } else {
                throw new FilterException("Unexpected result type: " + cVar.get() + " required LogObject");
            }
        }
    }

    @Override // org.web3j.protocol.core.filters.b
    public pw0 sendRequest() throws IOException {
        return this.web3j.ethNewFilter(this.ethFilter).send();
    }
}
