package org.web3j.protocol.core.filters;

/* loaded from: classes3.dex */
public class FilterException extends RuntimeException {
    public FilterException(String str) {
        super(str);
    }

    public FilterException(String str, Throwable th) {
        super(str, th);
    }
}
