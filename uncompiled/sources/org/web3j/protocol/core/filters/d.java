package org.web3j.protocol.core.filters;

import defpackage.ex0;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java8.util.o;

/* compiled from: PendingTransactionFilter.java */
/* loaded from: classes3.dex */
public class d extends b<String> {
    public d(ko4 ko4Var, vu<String> vuVar) {
        super(ko4Var, vuVar);
    }

    @Override // org.web3j.protocol.core.filters.b
    public o<org.web3j.protocol.core.c<?, ex0>> getFilterLogs(BigInteger bigInteger) {
        return o.a();
    }

    @Override // org.web3j.protocol.core.filters.b
    public void process(List<ex0.c> list) {
        for (ex0.c cVar : list) {
            if (cVar instanceof ex0.a) {
                this.callback.onEvent(((ex0.a) cVar).get());
            } else {
                throw new FilterException("Unexpected result type: " + cVar.get() + ", required Hash");
            }
        }
    }

    @Override // org.web3j.protocol.core.filters.b
    public pw0 sendRequest() throws IOException {
        return this.web3j.ethNewPendingTransactionFilter().send();
    }
}
