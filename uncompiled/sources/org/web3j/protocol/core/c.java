package org.web3j.protocol.core;

import com.fasterxml.jackson.annotation.g;
import com.trustwallet.walletconnect.WCClientKt;
import defpackage.i83;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java8.util.concurrent.CompletableFuture;
import org.web3j.protocol.core.c;

/* compiled from: Request.java */
/* loaded from: classes3.dex */
public class c<S, T extends i83> {
    private static AtomicLong nextId = new AtomicLong(0);
    private long id;
    private String jsonrpc;
    private String method;
    private List<S> params;
    private Class<T> responseType;
    private lo4 web3jService;

    public c() {
        this.jsonrpc = WCClientKt.JSONRPC_VERSION;
    }

    public q71<T> flowable() {
        return new b(new Callable() { // from class: c73
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return c.this.send();
            }
        }).flowable();
    }

    public long getId() {
        return this.id;
    }

    public String getJsonrpc() {
        return this.jsonrpc;
    }

    public String getMethod() {
        return this.method;
    }

    public List<S> getParams() {
        return this.params;
    }

    @g
    public Class<T> getResponseType() {
        return this.responseType;
    }

    public T send() throws IOException {
        return (T) this.web3jService.send(this, this.responseType);
    }

    public CompletableFuture<T> sendAsync() {
        return this.web3jService.sendAsync(this, this.responseType);
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setJsonrpc(String str) {
        this.jsonrpc = str;
    }

    public void setMethod(String str) {
        this.method = str;
    }

    public void setParams(List<S> list) {
        this.params = list;
    }

    public c(String str, List<S> list, lo4 lo4Var, Class<T> cls) {
        this.jsonrpc = WCClientKt.JSONRPC_VERSION;
        this.method = str;
        this.params = list;
        this.id = nextId.getAndIncrement();
        this.web3jService = lo4Var;
        this.responseType = cls;
    }
}
