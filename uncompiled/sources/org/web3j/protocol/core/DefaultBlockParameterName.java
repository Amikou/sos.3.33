package org.web3j.protocol.core;

import com.fasterxml.jackson.annotation.r;

/* loaded from: classes3.dex */
public enum DefaultBlockParameterName implements gi0 {
    EARLIEST("earliest"),
    LATEST("latest"),
    PENDING("pending");
    
    private String name;

    DefaultBlockParameterName(String str) {
        this.name = str;
    }

    public static DefaultBlockParameterName fromString(String str) {
        DefaultBlockParameterName[] values;
        if (str != null) {
            for (DefaultBlockParameterName defaultBlockParameterName : values()) {
                if (str.equalsIgnoreCase(defaultBlockParameterName.name)) {
                    return defaultBlockParameterName;
                }
            }
        }
        return valueOf(str);
    }

    @Override // defpackage.gi0
    @r
    public String getValue() {
        return this.name;
    }
}
