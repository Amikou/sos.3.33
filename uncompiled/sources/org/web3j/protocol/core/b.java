package org.web3j.protocol.core;

import java.util.concurrent.Callable;
import java8.util.concurrent.CompletableFuture;

/* compiled from: RemoteCall.java */
/* loaded from: classes3.dex */
public class b<T> {
    private Callable<T> callable;

    public b(Callable<T> callable) {
        this.callable = callable;
    }

    public q71<T> flowable() {
        return q71.n(new l63(this));
    }

    public T send() throws Exception {
        return this.callable.call();
    }

    public CompletableFuture<T> sendAsync() {
        return ti.run(new l63(this));
    }
}
