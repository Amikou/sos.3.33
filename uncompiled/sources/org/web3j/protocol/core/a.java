package org.web3j.protocol.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java8.util.concurrent.CompletableFuture;

/* compiled from: BatchRequest.java */
/* loaded from: classes3.dex */
public class a {
    private List<c<?, ? extends i83<?>>> requests = new ArrayList();
    private lo4 web3jService;

    public a(lo4 lo4Var) {
        this.web3jService = lo4Var;
    }

    public a add(c<?, ? extends i83<?>> cVar) {
        this.requests.add(cVar);
        return this;
    }

    public List<c<?, ? extends i83<?>>> getRequests() {
        return this.requests;
    }

    public lo send() throws IOException {
        return this.web3jService.sendBatch(this);
    }

    public CompletableFuture<lo> sendAsync() {
        return this.web3jService.sendBatchAsync(this);
    }
}
