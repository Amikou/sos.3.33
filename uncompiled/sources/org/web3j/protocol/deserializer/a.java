package org.web3j.protocol.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.c;
import java.io.IOException;

/* compiled from: KeepAsJsonDeserialzier.java */
/* loaded from: classes3.dex */
public class a extends c<String> {
    @Override // com.fasterxml.jackson.databind.c
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return jsonParser.n().readTree(jsonParser).toString();
    }
}
