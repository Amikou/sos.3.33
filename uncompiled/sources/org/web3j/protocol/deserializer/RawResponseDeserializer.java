package org.web3j.protocol.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.h;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/* loaded from: classes3.dex */
public class RawResponseDeserializer extends StdDeserializer<i83> implements h {
    private final c<?> defaultDeserializer;

    public RawResponseDeserializer(c<?> cVar) {
        super(i83.class);
        this.defaultDeserializer = cVar;
    }

    private String getRawResponse(JsonParser jsonParser) throws IOException {
        InputStream inputStream = (InputStream) jsonParser.C();
        if (inputStream == null) {
            return "";
        }
        inputStream.reset();
        return streamToString(inputStream);
    }

    private String streamToString(InputStream inputStream) throws IOException {
        return new Scanner(inputStream, m30.UTF_8.name()).useDelimiter("\\Z").next();
    }

    @Override // com.fasterxml.jackson.databind.deser.h
    public void resolve(DeserializationContext deserializationContext) throws JsonMappingException {
        ((h) this.defaultDeserializer).resolve(deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.c
    public i83 deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        i83 i83Var = (i83) this.defaultDeserializer.deserialize(jsonParser, deserializationContext);
        i83Var.setRawResponse(getRawResponse(jsonParser));
        return i83Var;
    }
}
