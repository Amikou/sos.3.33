package org.web3j.exceptions;

/* loaded from: classes3.dex */
public class MessageDecodingException extends RuntimeException {
    public MessageDecodingException(String str) {
        super(str);
    }

    public MessageDecodingException(String str, Throwable th) {
        super(str, th);
    }
}
