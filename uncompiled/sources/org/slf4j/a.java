package org.slf4j;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.impl.StaticLoggerBinder;

/* compiled from: LoggerFactory.java */
/* loaded from: classes2.dex */
public final class a {
    public static volatile int a;
    public static final ov3 b = new ov3();
    public static final uc2 c = new uc2();
    public static boolean d = lg4.f("slf4j.detectLoggerNameMismatch");
    public static final String[] e = {"1.6", "1.7"};
    public static String f = "org/slf4j/impl/StaticLoggerBinder.class";

    public static final void a() {
        Set<URL> set = null;
        try {
            if (!l()) {
                set = f();
                s(set);
            }
            StaticLoggerBinder.getSingleton();
            a = 3;
            r(set);
            g();
            p();
            b.b();
        } catch (Exception e2) {
            e(e2);
            throw new IllegalStateException("Unexpected initialization failure", e2);
        } catch (NoClassDefFoundError e3) {
            if (m(e3.getMessage())) {
                a = 4;
                lg4.c("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                lg4.c("Defaulting to no-operation (NOP) logger implementation");
                lg4.c("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
                return;
            }
            e(e3);
            throw e3;
        } catch (NoSuchMethodError e4) {
            String message = e4.getMessage();
            if (message != null && message.contains("org.slf4j.impl.StaticLoggerBinder.getSingleton()")) {
                a = 2;
                lg4.c("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                lg4.c("Your binding is version 1.5.5 or earlier.");
                lg4.c("Upgrade your binding to version 1.6.x.");
            }
            throw e4;
        }
    }

    public static void b(pv3 pv3Var, int i) {
        if (pv3Var.a().c()) {
            c(i);
        } else if (pv3Var.a().d()) {
        } else {
            d();
        }
    }

    public static void c(int i) {
        lg4.c("A number (" + i + ") of logging calls during the initialization phase have been intercepted and are");
        lg4.c("now being replayed. These are subject to the filtering rules of the underlying logging system.");
        lg4.c("See also http://www.slf4j.org/codes.html#replay");
    }

    public static void d() {
        lg4.c("The following set of substitute loggers may have been accessed");
        lg4.c("during the initialization phase. Logging calls during this");
        lg4.c("phase were not honored. However, subsequent logging calls to these");
        lg4.c("loggers will work as normally expected.");
        lg4.c("See also http://www.slf4j.org/codes.html#substituteLogger");
    }

    public static void e(Throwable th) {
        a = 2;
        lg4.d("Failed to instantiate SLF4J LoggerFactory", th);
    }

    public static Set<URL> f() {
        Enumeration<URL> resources;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        try {
            ClassLoader classLoader = a.class.getClassLoader();
            if (classLoader == null) {
                resources = ClassLoader.getSystemResources(f);
            } else {
                resources = classLoader.getResources(f);
            }
            while (resources.hasMoreElements()) {
                linkedHashSet.add(resources.nextElement());
            }
        } catch (IOException e2) {
            lg4.d("Error getting resources from path", e2);
        }
        return linkedHashSet;
    }

    public static void g() {
        ov3 ov3Var = b;
        synchronized (ov3Var) {
            ov3Var.e();
            for (nv3 nv3Var : ov3Var.d()) {
                nv3Var.g(j(nv3Var.getName()));
            }
        }
    }

    public static hm1 h() {
        if (a == 0) {
            synchronized (a.class) {
                if (a == 0) {
                    a = 1;
                    o();
                }
            }
        }
        int i = a;
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        return c;
                    }
                    throw new IllegalStateException("Unreachable code");
                }
                return StaticLoggerBinder.getSingleton().getLoggerFactory();
            }
            throw new IllegalStateException("org.slf4j.LoggerFactory in failed state. Original exception was thrown EARLIER. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
        }
        return b;
    }

    public static x12 i(Class<?> cls) {
        Class<?> a2;
        x12 j = j(cls.getName());
        if (d && (a2 = lg4.a()) != null && n(cls, a2)) {
            lg4.c(String.format("Detected logger name mismatch. Given name: \"%s\"; computed name: \"%s\".", j.getName(), a2.getName()));
            lg4.c("See http://www.slf4j.org/codes.html#loggerNameMismatch for an explanation");
        }
        return j;
    }

    public static x12 j(String str) {
        return h().a(str);
    }

    public static boolean k(Set<URL> set) {
        return set.size() > 1;
    }

    public static boolean l() {
        String g = lg4.g("java.vendor.url");
        if (g == null) {
            return false;
        }
        return g.toLowerCase().contains("android");
    }

    public static boolean m(String str) {
        if (str == null) {
            return false;
        }
        return str.contains("org/slf4j/impl/StaticLoggerBinder") || str.contains("org.slf4j.impl.StaticLoggerBinder");
    }

    public static boolean n(Class<?> cls, Class<?> cls2) {
        return !cls2.isAssignableFrom(cls);
    }

    public static final void o() {
        a();
        if (a == 3) {
            t();
        }
    }

    public static void p() {
        LinkedBlockingQueue<pv3> c2 = b.c();
        int size = c2.size();
        ArrayList<pv3> arrayList = new ArrayList(128);
        int i = 0;
        while (c2.drainTo(arrayList, 128) != 0) {
            for (pv3 pv3Var : arrayList) {
                q(pv3Var);
                int i2 = i + 1;
                if (i == 0) {
                    b(pv3Var, size);
                }
                i = i2;
            }
            arrayList.clear();
        }
    }

    public static void q(pv3 pv3Var) {
        if (pv3Var == null) {
            return;
        }
        nv3 a2 = pv3Var.a();
        String name = a2.getName();
        if (!a2.e()) {
            if (a2.d()) {
                return;
            }
            if (a2.c()) {
                a2.f(pv3Var);
                return;
            } else {
                lg4.c(name);
                return;
            }
        }
        throw new IllegalStateException("Delegate logger cannot be null at this state.");
    }

    public static void r(Set<URL> set) {
        if (set == null || !k(set)) {
            return;
        }
        lg4.c("Actual binding is of type [" + StaticLoggerBinder.getSingleton().getLoggerFactoryClassStr() + "]");
    }

    public static void s(Set<URL> set) {
        if (k(set)) {
            lg4.c("Class path contains multiple SLF4J bindings.");
            Iterator<URL> it = set.iterator();
            while (it.hasNext()) {
                lg4.c("Found binding in [" + it.next() + "]");
            }
            lg4.c("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
        }
    }

    public static final void t() {
        try {
            String str = StaticLoggerBinder.REQUESTED_API_VERSION;
            boolean z = false;
            for (String str2 : e) {
                if (str.startsWith(str2)) {
                    z = true;
                }
            }
            if (z) {
                return;
            }
            lg4.c("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(e).toString());
            lg4.c("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
        } catch (NoSuchFieldError unused) {
        } catch (Throwable th) {
            lg4.d("Unexpected problem occured during version sanity check", th);
        }
    }
}
