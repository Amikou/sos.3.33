package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import okhttp3.ResponseBody;
import retrofit2.e;

/* compiled from: OptionalConverterFactory.java */
/* loaded from: classes3.dex */
public final class i extends e.a {
    public static final e.a a = new i();

    /* compiled from: OptionalConverterFactory.java */
    /* loaded from: classes3.dex */
    public static final class a<T> implements e<ResponseBody, Optional<T>> {
        public final e<ResponseBody, T> a;

        public a(e<ResponseBody, T> eVar) {
            this.a = eVar;
        }

        @Override // retrofit2.e
        /* renamed from: b */
        public Optional<T> a(ResponseBody responseBody) throws IOException {
            return Optional.ofNullable(this.a.a(responseBody));
        }
    }

    @Override // retrofit2.e.a
    public e<ResponseBody, ?> d(Type type, Annotation[] annotationArr, o oVar) {
        if (e.a.b(type) != Optional.class) {
            return null;
        }
        return new a(oVar.i(e.a.a(0, (ParameterizedType) type), annotationArr));
    }
}
