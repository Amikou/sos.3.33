package retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.p;

/* compiled from: HttpServiceMethod.java */
/* loaded from: classes3.dex */
public abstract class g<ResponseT, ReturnT> extends hm3<ReturnT> {
    public final m a;
    public final Call.Factory b;
    public final e<ResponseBody, ResponseT> c;

    /* compiled from: HttpServiceMethod.java */
    /* loaded from: classes3.dex */
    public static final class a<ResponseT, ReturnT> extends g<ResponseT, ReturnT> {
        public final retrofit2.c<ResponseT, ReturnT> d;

        public a(m mVar, Call.Factory factory, e<ResponseBody, ResponseT> eVar, retrofit2.c<ResponseT, ReturnT> cVar) {
            super(mVar, factory, eVar);
            this.d = cVar;
        }

        @Override // retrofit2.g
        public ReturnT c(retrofit2.b<ResponseT> bVar, Object[] objArr) {
            return this.d.b(bVar);
        }
    }

    /* compiled from: HttpServiceMethod.java */
    /* loaded from: classes3.dex */
    public static final class b<ResponseT> extends g<ResponseT, Object> {
        public final retrofit2.c<ResponseT, retrofit2.b<ResponseT>> d;
        public final boolean e;

        public b(m mVar, Call.Factory factory, e<ResponseBody, ResponseT> eVar, retrofit2.c<ResponseT, retrofit2.b<ResponseT>> cVar, boolean z) {
            super(mVar, factory, eVar);
            this.d = cVar;
            this.e = z;
        }

        @Override // retrofit2.g
        public Object c(retrofit2.b<ResponseT> bVar, Object[] objArr) {
            retrofit2.b<ResponseT> b = this.d.b(bVar);
            q70 q70Var = (q70) objArr[objArr.length - 1];
            try {
                if (this.e) {
                    return KotlinExtensions.b(b, q70Var);
                }
                return KotlinExtensions.a(b, q70Var);
            } catch (Exception e) {
                return KotlinExtensions.d(e, q70Var);
            }
        }
    }

    /* compiled from: HttpServiceMethod.java */
    /* loaded from: classes3.dex */
    public static final class c<ResponseT> extends g<ResponseT, Object> {
        public final retrofit2.c<ResponseT, retrofit2.b<ResponseT>> d;

        public c(m mVar, Call.Factory factory, e<ResponseBody, ResponseT> eVar, retrofit2.c<ResponseT, retrofit2.b<ResponseT>> cVar) {
            super(mVar, factory, eVar);
            this.d = cVar;
        }

        @Override // retrofit2.g
        public Object c(retrofit2.b<ResponseT> bVar, Object[] objArr) {
            retrofit2.b<ResponseT> b = this.d.b(bVar);
            q70 q70Var = (q70) objArr[objArr.length - 1];
            try {
                return KotlinExtensions.c(b, q70Var);
            } catch (Exception e) {
                return KotlinExtensions.d(e, q70Var);
            }
        }
    }

    public g(m mVar, Call.Factory factory, e<ResponseBody, ResponseT> eVar) {
        this.a = mVar;
        this.b = factory;
        this.c = eVar;
    }

    public static <ResponseT, ReturnT> retrofit2.c<ResponseT, ReturnT> d(o oVar, Method method, Type type, Annotation[] annotationArr) {
        try {
            return (retrofit2.c<ResponseT, ReturnT>) oVar.a(type, annotationArr);
        } catch (RuntimeException e) {
            throw p.n(method, e, "Unable to create call adapter for %s", type);
        }
    }

    public static <ResponseT> e<ResponseBody, ResponseT> e(o oVar, Method method, Type type) {
        try {
            return oVar.i(type, method.getAnnotations());
        } catch (RuntimeException e) {
            throw p.n(method, e, "Unable to create converter for %s", type);
        }
    }

    public static <ResponseT, ReturnT> g<ResponseT, ReturnT> f(o oVar, Method method, m mVar) {
        Type genericReturnType;
        boolean z;
        boolean z2 = mVar.k;
        Annotation[] annotations = method.getAnnotations();
        if (z2) {
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            Type f = p.f(0, (ParameterizedType) genericParameterTypes[genericParameterTypes.length - 1]);
            if (p.h(f) == n.class && (f instanceof ParameterizedType)) {
                f = p.g(0, (ParameterizedType) f);
                z = true;
            } else {
                z = false;
            }
            genericReturnType = new p.b(null, retrofit2.b.class, f);
            annotations = fq3.a(annotations);
        } else {
            genericReturnType = method.getGenericReturnType();
            z = false;
        }
        retrofit2.c d = d(oVar, method, genericReturnType, annotations);
        Type a2 = d.a();
        if (a2 != Response.class) {
            if (a2 != n.class) {
                if (mVar.c.equals("HEAD") && !Void.class.equals(a2)) {
                    throw p.m(method, "HEAD method must use Void as response type.", new Object[0]);
                }
                e e = e(oVar, method, a2);
                Call.Factory factory = oVar.b;
                if (z2) {
                    if (z) {
                        return new c(mVar, factory, e, d);
                    }
                    return new b(mVar, factory, e, d, false);
                }
                return new a(mVar, factory, e, d);
            }
            throw p.m(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
        }
        throw p.m(method, "'" + p.h(a2).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
    }

    @Override // defpackage.hm3
    public final ReturnT a(Object[] objArr) {
        return c(new h(this.a, objArr, this.b, this.c), objArr);
    }

    public abstract ReturnT c(retrofit2.b<ResponseT> bVar, Object[] objArr);
}
