package retrofit2;

import kotlin.coroutines.jvm.internal.ContinuationImpl;

/* compiled from: KotlinExtensions.kt */
@kotlin.coroutines.jvm.internal.a(c = "retrofit2.KotlinExtensions", f = "KotlinExtensions.kt", l = {113}, m = "suspendAndThrow")
/* loaded from: classes3.dex */
public final class KotlinExtensions$suspendAndThrow$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;

    public KotlinExtensions$suspendAndThrow$1(q70 q70Var) {
        super(q70Var);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return KotlinExtensions.d(null, this);
    }
}
