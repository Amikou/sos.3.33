package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import retrofit2.j;

/* compiled from: RequestFactory.java */
/* loaded from: classes3.dex */
public final class m {
    public final Method a;
    public final HttpUrl b;
    public final String c;
    public final String d;
    public final Headers e;
    public final MediaType f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final j<?>[] j;
    public final boolean k;

    /* compiled from: RequestFactory.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final Pattern x = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
        public static final Pattern y = Pattern.compile("[a-zA-Z][a-zA-Z0-9_-]*");
        public final o a;
        public final Method b;
        public final Annotation[] c;
        public final Annotation[][] d;
        public final Type[] e;
        public boolean f;
        public boolean g;
        public boolean h;
        public boolean i;
        public boolean j;
        public boolean k;
        public boolean l;
        public boolean m;
        public String n;
        public boolean o;
        public boolean p;
        public boolean q;
        public String r;
        public Headers s;
        public MediaType t;
        public Set<String> u;
        public j<?>[] v;
        public boolean w;

        public a(o oVar, Method method) {
            this.a = oVar;
            this.b = method;
            this.c = method.getAnnotations();
            this.e = method.getGenericParameterTypes();
            this.d = method.getParameterAnnotations();
        }

        public static Class<?> a(Class<?> cls) {
            return Boolean.TYPE == cls ? Boolean.class : Byte.TYPE == cls ? Byte.class : Character.TYPE == cls ? Character.class : Double.TYPE == cls ? Double.class : Float.TYPE == cls ? Float.class : Integer.TYPE == cls ? Integer.class : Long.TYPE == cls ? Long.class : Short.TYPE == cls ? Short.class : cls;
        }

        public static Set<String> h(String str) {
            Matcher matcher = x.matcher(str);
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            while (matcher.find()) {
                linkedHashSet.add(matcher.group(1));
            }
            return linkedHashSet;
        }

        public m b() {
            for (Annotation annotation : this.c) {
                e(annotation);
            }
            if (this.n != null) {
                if (!this.o) {
                    if (!this.q) {
                        if (this.p) {
                            throw p.m(this.b, "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                        }
                    } else {
                        throw p.m(this.b, "Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    }
                }
                int length = this.d.length;
                this.v = new j[length];
                int i = length - 1;
                int i2 = 0;
                while (true) {
                    boolean z = true;
                    if (i2 >= length) {
                        break;
                    }
                    j<?>[] jVarArr = this.v;
                    Type type = this.e[i2];
                    Annotation[] annotationArr = this.d[i2];
                    if (i2 != i) {
                        z = false;
                    }
                    jVarArr[i2] = f(i2, type, annotationArr, z);
                    i2++;
                }
                if (this.r != null || this.m) {
                    boolean z2 = this.p;
                    if (!z2 && !this.q && !this.o && this.h) {
                        throw p.m(this.b, "Non-body HTTP method cannot contain @Body.", new Object[0]);
                    }
                    if (z2 && !this.f) {
                        throw p.m(this.b, "Form-encoded method must contain at least one @Field.", new Object[0]);
                    }
                    if (this.q && !this.g) {
                        throw p.m(this.b, "Multipart method must contain at least one @Part.", new Object[0]);
                    }
                    return new m(this);
                }
                throw p.m(this.b, "Missing either @%s URL or @Url parameter.", this.n);
            }
            throw p.m(this.b, "HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
        }

        public final Headers c(String[] strArr) {
            Headers.Builder builder = new Headers.Builder();
            for (String str : strArr) {
                int indexOf = str.indexOf(58);
                if (indexOf == -1 || indexOf == 0 || indexOf == str.length() - 1) {
                    throw p.m(this.b, "@Headers value must be in the form \"Name: Value\". Found: \"%s\"", str);
                }
                String substring = str.substring(0, indexOf);
                String trim = str.substring(indexOf + 1).trim();
                if ("Content-Type".equalsIgnoreCase(substring)) {
                    try {
                        this.t = MediaType.get(trim);
                    } catch (IllegalArgumentException e) {
                        throw p.n(this.b, e, "Malformed content type: %s", trim);
                    }
                } else {
                    builder.add(substring, trim);
                }
            }
            return builder.build();
        }

        public final void d(String str, String str2, boolean z) {
            String str3 = this.n;
            if (str3 != null) {
                throw p.m(this.b, "Only one HTTP method is allowed. Found: %s and %s.", str3, str);
            }
            this.n = str;
            this.o = z;
            if (str2.isEmpty()) {
                return;
            }
            int indexOf = str2.indexOf(63);
            if (indexOf != -1 && indexOf < str2.length() - 1) {
                String substring = str2.substring(indexOf + 1);
                if (x.matcher(substring).find()) {
                    throw p.m(this.b, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                }
            }
            this.r = str2;
            this.u = h(str2);
        }

        public final void e(Annotation annotation) {
            if (annotation instanceof kd0) {
                d("DELETE", ((kd0) annotation).value(), false);
            } else if (annotation instanceof ge1) {
                d("GET", ((ge1) annotation).value(), false);
            } else if (annotation instanceof gj1) {
                d("HEAD", ((gj1) annotation).value(), false);
            } else if (annotation instanceof fo2) {
                d("PATCH", ((fo2) annotation).value(), true);
            } else if (annotation instanceof oo2) {
                d("POST", ((oo2) annotation).value(), true);
            } else if (annotation instanceof qo2) {
                d("PUT", ((qo2) annotation).value(), true);
            } else if (annotation instanceof kj2) {
                d("OPTIONS", ((kj2) annotation).value(), false);
            } else if (annotation instanceof jj1) {
                jj1 jj1Var = (jj1) annotation;
                d(jj1Var.method(), jj1Var.path(), jj1Var.hasBody());
            } else if (annotation instanceof hk1) {
                String[] value = ((hk1) annotation).value();
                if (value.length != 0) {
                    this.s = c(value);
                    return;
                }
                throw p.m(this.b, "@Headers annotation is empty.", new Object[0]);
            } else if (annotation instanceof xa2) {
                if (!this.p) {
                    this.q = true;
                    return;
                }
                throw p.m(this.b, "Only one encoding annotation is allowed.", new Object[0]);
            } else if (annotation instanceof v81) {
                if (!this.q) {
                    this.p = true;
                    return;
                }
                throw p.m(this.b, "Only one encoding annotation is allowed.", new Object[0]);
            }
        }

        public final j<?> f(int i, Type type, Annotation[] annotationArr, boolean z) {
            j<?> jVar;
            if (annotationArr != null) {
                jVar = null;
                for (Annotation annotation : annotationArr) {
                    j<?> g = g(i, type, annotationArr, annotation);
                    if (g != null) {
                        if (jVar != null) {
                            throw p.o(this.b, i, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
                        }
                        jVar = g;
                    }
                }
            } else {
                jVar = null;
            }
            if (jVar == null) {
                if (z) {
                    try {
                        if (p.h(type) == q70.class) {
                            this.w = true;
                            return null;
                        }
                    } catch (NoClassDefFoundError unused) {
                    }
                }
                throw p.o(this.b, i, "No Retrofit annotation found.", new Object[0]);
            }
            return jVar;
        }

        public final j<?> g(int i, Type type, Annotation[] annotationArr, Annotation annotation) {
            if (annotation instanceof sf4) {
                j(i, type);
                if (!this.m) {
                    if (!this.i) {
                        if (!this.j) {
                            if (!this.k) {
                                if (!this.l) {
                                    if (this.r == null) {
                                        this.m = true;
                                        if (type != HttpUrl.class && type != String.class && type != URI.class && (!(type instanceof Class) || !"android.net.Uri".equals(((Class) type).getName()))) {
                                            throw p.o(this.b, i, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
                                        }
                                        return new j.p(this.b, i);
                                    }
                                    throw p.o(this.b, i, "@Url cannot be used with @%s URL", this.n);
                                }
                                throw p.o(this.b, i, "A @Url parameter must not come after a @QueryMap.", new Object[0]);
                            }
                            throw p.o(this.b, i, "A @Url parameter must not come after a @QueryName.", new Object[0]);
                        }
                        throw p.o(this.b, i, "A @Url parameter must not come after a @Query.", new Object[0]);
                    }
                    throw p.o(this.b, i, "@Path parameters may not be used with @Url.", new Object[0]);
                }
                throw p.o(this.b, i, "Multiple @Url method annotations found.", new Object[0]);
            } else if (annotation instanceof vp2) {
                j(i, type);
                if (!this.j) {
                    if (!this.k) {
                        if (!this.l) {
                            if (!this.m) {
                                if (this.r != null) {
                                    this.i = true;
                                    vp2 vp2Var = (vp2) annotation;
                                    String value = vp2Var.value();
                                    i(i, value);
                                    return new j.k(this.b, i, value, this.a.j(type, annotationArr), vp2Var.encoded());
                                }
                                throw p.o(this.b, i, "@Path can only be used with relative url on @%s", this.n);
                            }
                            throw p.o(this.b, i, "@Path parameters may not be used with @Url.", new Object[0]);
                        }
                        throw p.o(this.b, i, "A @Path parameter must not come after a @QueryMap.", new Object[0]);
                    }
                    throw p.o(this.b, i, "A @Path parameter must not come after a @QueryName.", new Object[0]);
                }
                throw p.o(this.b, i, "A @Path parameter must not come after a @Query.", new Object[0]);
            } else if (annotation instanceof yw2) {
                j(i, type);
                yw2 yw2Var = (yw2) annotation;
                String value2 = yw2Var.value();
                boolean encoded = yw2Var.encoded();
                Class<?> h = p.h(type);
                this.j = true;
                if (Iterable.class.isAssignableFrom(h)) {
                    if (type instanceof ParameterizedType) {
                        return new j.l(value2, this.a.j(p.g(0, (ParameterizedType) type), annotationArr), encoded).c();
                    }
                    throw p.o(this.b, i, h.getSimpleName() + " must include generic type (e.g., " + h.getSimpleName() + "<String>)", new Object[0]);
                } else if (h.isArray()) {
                    return new j.l(value2, this.a.j(a(h.getComponentType()), annotationArr), encoded).b();
                } else {
                    return new j.l(value2, this.a.j(type, annotationArr), encoded);
                }
            } else if (annotation instanceof mx2) {
                j(i, type);
                boolean encoded2 = ((mx2) annotation).encoded();
                Class<?> h2 = p.h(type);
                this.k = true;
                if (Iterable.class.isAssignableFrom(h2)) {
                    if (type instanceof ParameterizedType) {
                        return new j.n(this.a.j(p.g(0, (ParameterizedType) type), annotationArr), encoded2).c();
                    }
                    throw p.o(this.b, i, h2.getSimpleName() + " must include generic type (e.g., " + h2.getSimpleName() + "<String>)", new Object[0]);
                } else if (h2.isArray()) {
                    return new j.n(this.a.j(a(h2.getComponentType()), annotationArr), encoded2).b();
                } else {
                    return new j.n(this.a.j(type, annotationArr), encoded2);
                }
            } else if (annotation instanceof lx2) {
                j(i, type);
                Class<?> h3 = p.h(type);
                this.l = true;
                if (Map.class.isAssignableFrom(h3)) {
                    Type i2 = p.i(type, h3, Map.class);
                    if (i2 instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) i2;
                        Type g = p.g(0, parameterizedType);
                        if (String.class == g) {
                            return new j.m(this.b, i, this.a.j(p.g(1, parameterizedType), annotationArr), ((lx2) annotation).encoded());
                        }
                        throw p.o(this.b, i, "@QueryMap keys must be of type String: " + g, new Object[0]);
                    }
                    throw p.o(this.b, i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                throw p.o(this.b, i, "@QueryMap parameter type must be Map.", new Object[0]);
            } else if (annotation instanceof ek1) {
                j(i, type);
                String value3 = ((ek1) annotation).value();
                Class<?> h4 = p.h(type);
                if (Iterable.class.isAssignableFrom(h4)) {
                    if (type instanceof ParameterizedType) {
                        return new j.f(value3, this.a.j(p.g(0, (ParameterizedType) type), annotationArr)).c();
                    }
                    throw p.o(this.b, i, h4.getSimpleName() + " must include generic type (e.g., " + h4.getSimpleName() + "<String>)", new Object[0]);
                } else if (h4.isArray()) {
                    return new j.f(value3, this.a.j(a(h4.getComponentType()), annotationArr)).b();
                } else {
                    return new j.f(value3, this.a.j(type, annotationArr));
                }
            } else if (annotation instanceof fk1) {
                if (type == Headers.class) {
                    return new j.h(this.b, i);
                }
                j(i, type);
                Class<?> h5 = p.h(type);
                if (Map.class.isAssignableFrom(h5)) {
                    Type i3 = p.i(type, h5, Map.class);
                    if (i3 instanceof ParameterizedType) {
                        ParameterizedType parameterizedType2 = (ParameterizedType) i3;
                        Type g2 = p.g(0, parameterizedType2);
                        if (String.class == g2) {
                            return new j.g(this.b, i, this.a.j(p.g(1, parameterizedType2), annotationArr));
                        }
                        throw p.o(this.b, i, "@HeaderMap keys must be of type String: " + g2, new Object[0]);
                    }
                    throw p.o(this.b, i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                throw p.o(this.b, i, "@HeaderMap parameter type must be Map.", new Object[0]);
            } else if (annotation instanceof g31) {
                j(i, type);
                if (this.p) {
                    g31 g31Var = (g31) annotation;
                    String value4 = g31Var.value();
                    boolean encoded3 = g31Var.encoded();
                    this.f = true;
                    Class<?> h6 = p.h(type);
                    if (Iterable.class.isAssignableFrom(h6)) {
                        if (type instanceof ParameterizedType) {
                            return new j.d(value4, this.a.j(p.g(0, (ParameterizedType) type), annotationArr), encoded3).c();
                        }
                        throw p.o(this.b, i, h6.getSimpleName() + " must include generic type (e.g., " + h6.getSimpleName() + "<String>)", new Object[0]);
                    } else if (h6.isArray()) {
                        return new j.d(value4, this.a.j(a(h6.getComponentType()), annotationArr), encoded3).b();
                    } else {
                        return new j.d(value4, this.a.j(type, annotationArr), encoded3);
                    }
                }
                throw p.o(this.b, i, "@Field parameters can only be used with form encoding.", new Object[0]);
            } else if (annotation instanceof i31) {
                j(i, type);
                if (this.p) {
                    Class<?> h7 = p.h(type);
                    if (Map.class.isAssignableFrom(h7)) {
                        Type i4 = p.i(type, h7, Map.class);
                        if (i4 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType3 = (ParameterizedType) i4;
                            Type g3 = p.g(0, parameterizedType3);
                            if (String.class == g3) {
                                e j = this.a.j(p.g(1, parameterizedType3), annotationArr);
                                this.f = true;
                                return new j.e(this.b, i, j, ((i31) annotation).encoded());
                            }
                            throw p.o(this.b, i, "@FieldMap keys must be of type String: " + g3, new Object[0]);
                        }
                        throw p.o(this.b, i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw p.o(this.b, i, "@FieldMap parameter type must be Map.", new Object[0]);
                }
                throw p.o(this.b, i, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
            } else if (annotation instanceof sp2) {
                j(i, type);
                if (this.q) {
                    sp2 sp2Var = (sp2) annotation;
                    this.g = true;
                    String value5 = sp2Var.value();
                    Class<?> h8 = p.h(type);
                    if (value5.isEmpty()) {
                        if (Iterable.class.isAssignableFrom(h8)) {
                            if (type instanceof ParameterizedType) {
                                if (MultipartBody.Part.class.isAssignableFrom(p.h(p.g(0, (ParameterizedType) type)))) {
                                    return j.o.a.c();
                                }
                                throw p.o(this.b, i, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                            }
                            throw p.o(this.b, i, h8.getSimpleName() + " must include generic type (e.g., " + h8.getSimpleName() + "<String>)", new Object[0]);
                        } else if (h8.isArray()) {
                            if (MultipartBody.Part.class.isAssignableFrom(h8.getComponentType())) {
                                return j.o.a.b();
                            }
                            throw p.o(this.b, i, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                        } else if (MultipartBody.Part.class.isAssignableFrom(h8)) {
                            return j.o.a;
                        } else {
                            throw p.o(this.b, i, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                        }
                    }
                    Headers of = Headers.of("Content-Disposition", "form-data; name=\"" + value5 + "\"", "Content-Transfer-Encoding", sp2Var.encoding());
                    if (Iterable.class.isAssignableFrom(h8)) {
                        if (type instanceof ParameterizedType) {
                            Type g4 = p.g(0, (ParameterizedType) type);
                            if (!MultipartBody.Part.class.isAssignableFrom(p.h(g4))) {
                                return new j.i(this.b, i, of, this.a.h(g4, annotationArr, this.c)).c();
                            }
                            throw p.o(this.b, i, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        }
                        throw p.o(this.b, i, h8.getSimpleName() + " must include generic type (e.g., " + h8.getSimpleName() + "<String>)", new Object[0]);
                    } else if (h8.isArray()) {
                        Class<?> a = a(h8.getComponentType());
                        if (!MultipartBody.Part.class.isAssignableFrom(a)) {
                            return new j.i(this.b, i, of, this.a.h(a, annotationArr, this.c)).b();
                        }
                        throw p.o(this.b, i, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                    } else if (!MultipartBody.Part.class.isAssignableFrom(h8)) {
                        return new j.i(this.b, i, of, this.a.h(type, annotationArr, this.c));
                    } else {
                        throw p.o(this.b, i, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                    }
                }
                throw p.o(this.b, i, "@Part parameters can only be used with multipart encoding.", new Object[0]);
            } else if (annotation instanceof tp2) {
                j(i, type);
                if (this.q) {
                    this.g = true;
                    Class<?> h9 = p.h(type);
                    if (Map.class.isAssignableFrom(h9)) {
                        Type i5 = p.i(type, h9, Map.class);
                        if (i5 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType4 = (ParameterizedType) i5;
                            Type g5 = p.g(0, parameterizedType4);
                            if (String.class == g5) {
                                Type g6 = p.g(1, parameterizedType4);
                                if (!MultipartBody.Part.class.isAssignableFrom(p.h(g6))) {
                                    return new j.C0280j(this.b, i, this.a.h(g6, annotationArr, this.c), ((tp2) annotation).encoding());
                                }
                                throw p.o(this.b, i, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                            }
                            throw p.o(this.b, i, "@PartMap keys must be of type String: " + g5, new Object[0]);
                        }
                        throw p.o(this.b, i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw p.o(this.b, i, "@PartMap parameter type must be Map.", new Object[0]);
                }
                throw p.o(this.b, i, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
            } else if (annotation instanceof ar) {
                j(i, type);
                if (!this.p && !this.q) {
                    if (!this.h) {
                        try {
                            e h10 = this.a.h(type, annotationArr, this.c);
                            this.h = true;
                            return new j.c(this.b, i, h10);
                        } catch (RuntimeException e) {
                            throw p.p(this.b, e, i, "Unable to create @Body converter for %s", type);
                        }
                    }
                    throw p.o(this.b, i, "Multiple @Body method annotations found.", new Object[0]);
                }
                throw p.o(this.b, i, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
            } else if (annotation instanceof g34) {
                j(i, type);
                Class<?> h11 = p.h(type);
                for (int i6 = i - 1; i6 >= 0; i6--) {
                    j<?> jVar = this.v[i6];
                    if ((jVar instanceof j.q) && ((j.q) jVar).a.equals(h11)) {
                        throw p.o(this.b, i, "@Tag type " + h11.getName() + " is duplicate of parameter #" + (i6 + 1) + " and would always overwrite its value.", new Object[0]);
                    }
                }
                return new j.q(h11);
            } else {
                return null;
            }
        }

        public final void i(int i, String str) {
            if (y.matcher(str).matches()) {
                if (!this.u.contains(str)) {
                    throw p.o(this.b, i, "URL \"%s\" does not contain \"{%s}\".", this.r, str);
                }
                return;
            }
            throw p.o(this.b, i, "@Path parameter name must match %s. Found: %s", x.pattern(), str);
        }

        public final void j(int i, Type type) {
            if (p.j(type)) {
                throw p.o(this.b, i, "Parameter type must not include a type variable or wildcard: %s", type);
            }
        }
    }

    public m(a aVar) {
        this.a = aVar.b;
        this.b = aVar.a.c;
        this.c = aVar.n;
        this.d = aVar.r;
        this.e = aVar.s;
        this.f = aVar.t;
        this.g = aVar.o;
        this.h = aVar.p;
        this.i = aVar.q;
        this.j = aVar.v;
        this.k = aVar.w;
    }

    public static m b(o oVar, Method method) {
        return new a(oVar, method).b();
    }

    public Request a(Object[] objArr) throws IOException {
        j<?>[] jVarArr = this.j;
        int length = objArr.length;
        if (length == jVarArr.length) {
            l lVar = new l(this.c, this.b, this.d, this.e, this.f, this.g, this.h, this.i);
            if (this.k) {
                length--;
            }
            ArrayList arrayList = new ArrayList(length);
            for (int i = 0; i < length; i++) {
                arrayList.add(objArr[i]);
                jVarArr[i].a(lVar, objArr[i]);
            }
            return lVar.k().tag(ls1.class, new ls1(this.a, arrayList)).build();
        }
        throw new IllegalArgumentException("Argument count (" + length + ") doesn't match expected count (" + jVarArr.length + ")");
    }
}
