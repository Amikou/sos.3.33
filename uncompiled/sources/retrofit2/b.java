package retrofit2;

import java.io.IOException;
import okhttp3.Request;

/* compiled from: Call.java */
/* loaded from: classes3.dex */
public interface b<T> extends Cloneable {
    void cancel();

    b<T> clone();

    n<T> execute() throws IOException;

    boolean isCanceled();

    void n(wu<T> wuVar);

    Request request();
}
