package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.e;

/* compiled from: BuiltInConverters.java */
/* loaded from: classes3.dex */
public final class a extends e.a {
    public boolean a = true;

    /* compiled from: BuiltInConverters.java */
    /* renamed from: retrofit2.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0278a implements retrofit2.e<ResponseBody, ResponseBody> {
        public static final C0278a a = new C0278a();

        @Override // retrofit2.e
        /* renamed from: b */
        public ResponseBody a(ResponseBody responseBody) throws IOException {
            try {
                return p.a(responseBody);
            } finally {
                responseBody.close();
            }
        }
    }

    /* compiled from: BuiltInConverters.java */
    /* loaded from: classes3.dex */
    public static final class b implements retrofit2.e<RequestBody, RequestBody> {
        public static final b a = new b();

        @Override // retrofit2.e
        /* renamed from: b */
        public RequestBody a(RequestBody requestBody) {
            return requestBody;
        }
    }

    /* compiled from: BuiltInConverters.java */
    /* loaded from: classes3.dex */
    public static final class c implements retrofit2.e<ResponseBody, ResponseBody> {
        public static final c a = new c();

        @Override // retrofit2.e
        /* renamed from: b */
        public ResponseBody a(ResponseBody responseBody) {
            return responseBody;
        }
    }

    /* compiled from: BuiltInConverters.java */
    /* loaded from: classes3.dex */
    public static final class d implements retrofit2.e<Object, String> {
        public static final d a = new d();

        @Override // retrofit2.e
        /* renamed from: b */
        public String a(Object obj) {
            return obj.toString();
        }
    }

    /* compiled from: BuiltInConverters.java */
    /* loaded from: classes3.dex */
    public static final class e implements retrofit2.e<ResponseBody, te4> {
        public static final e a = new e();

        @Override // retrofit2.e
        /* renamed from: b */
        public te4 a(ResponseBody responseBody) {
            responseBody.close();
            return te4.a;
        }
    }

    /* compiled from: BuiltInConverters.java */
    /* loaded from: classes3.dex */
    public static final class f implements retrofit2.e<ResponseBody, Void> {
        public static final f a = new f();

        @Override // retrofit2.e
        /* renamed from: b */
        public Void a(ResponseBody responseBody) {
            responseBody.close();
            return null;
        }
    }

    @Override // retrofit2.e.a
    public retrofit2.e<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, o oVar) {
        if (RequestBody.class.isAssignableFrom(p.h(type))) {
            return b.a;
        }
        return null;
    }

    @Override // retrofit2.e.a
    public retrofit2.e<ResponseBody, ?> d(Type type, Annotation[] annotationArr, o oVar) {
        if (type == ResponseBody.class) {
            if (p.l(annotationArr, hu3.class)) {
                return c.a;
            }
            return C0278a.a;
        } else if (type == Void.class) {
            return f.a;
        } else {
            if (this.a && type == te4.class) {
                try {
                    return e.a;
                } catch (NoClassDefFoundError unused) {
                    this.a = false;
                    return null;
                }
            }
            return null;
        }
    }
}
