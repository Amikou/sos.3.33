package retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import retrofit2.c;

/* compiled from: CompletableFutureCallAdapterFactory.java */
/* loaded from: classes3.dex */
public final class d extends c.a {
    public static final c.a a = new d();

    /* compiled from: CompletableFutureCallAdapterFactory.java */
    /* loaded from: classes3.dex */
    public static final class a<R> implements retrofit2.c<R, CompletableFuture<R>> {
        public final Type a;

        /* compiled from: CompletableFutureCallAdapterFactory.java */
        /* renamed from: retrofit2.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0279a implements wu<R> {
            public final CompletableFuture<R> a;

            public C0279a(a aVar, CompletableFuture<R> completableFuture) {
                this.a = completableFuture;
            }

            @Override // defpackage.wu
            public void a(retrofit2.b<R> bVar, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @Override // defpackage.wu
            public void b(retrofit2.b<R> bVar, n<R> nVar) {
                if (nVar.e()) {
                    this.a.complete(nVar.a());
                } else {
                    this.a.completeExceptionally(new HttpException(nVar));
                }
            }
        }

        public a(Type type) {
            this.a = type;
        }

        @Override // retrofit2.c
        public Type a() {
            return this.a;
        }

        @Override // retrofit2.c
        /* renamed from: c */
        public CompletableFuture<R> b(retrofit2.b<R> bVar) {
            b bVar2 = new b(bVar);
            bVar.n(new C0279a(this, bVar2));
            return bVar2;
        }
    }

    /* compiled from: CompletableFutureCallAdapterFactory.java */
    /* loaded from: classes3.dex */
    public static final class b<T> extends CompletableFuture<T> {
        public final retrofit2.b<?> a;

        public b(retrofit2.b<?> bVar) {
            this.a = bVar;
        }

        @Override // java.util.concurrent.CompletableFuture, java.util.concurrent.Future
        public boolean cancel(boolean z) {
            if (z) {
                this.a.cancel();
            }
            return super.cancel(z);
        }
    }

    /* compiled from: CompletableFutureCallAdapterFactory.java */
    /* loaded from: classes3.dex */
    public static final class c<R> implements retrofit2.c<R, CompletableFuture<n<R>>> {
        public final Type a;

        /* compiled from: CompletableFutureCallAdapterFactory.java */
        /* loaded from: classes3.dex */
        public class a implements wu<R> {
            public final CompletableFuture<n<R>> a;

            public a(c cVar, CompletableFuture<n<R>> completableFuture) {
                this.a = completableFuture;
            }

            @Override // defpackage.wu
            public void a(retrofit2.b<R> bVar, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @Override // defpackage.wu
            public void b(retrofit2.b<R> bVar, n<R> nVar) {
                this.a.complete(nVar);
            }
        }

        public c(Type type) {
            this.a = type;
        }

        @Override // retrofit2.c
        public Type a() {
            return this.a;
        }

        @Override // retrofit2.c
        /* renamed from: c */
        public CompletableFuture<n<R>> b(retrofit2.b<R> bVar) {
            b bVar2 = new b(bVar);
            bVar.n(new a(this, bVar2));
            return bVar2;
        }
    }

    @Override // retrofit2.c.a
    public retrofit2.c<?, ?> a(Type type, Annotation[] annotationArr, o oVar) {
        if (c.a.c(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type b2 = c.a.b(0, (ParameterizedType) type);
            if (c.a.c(b2) != n.class) {
                return new a(b2);
            }
            if (b2 instanceof ParameterizedType) {
                return new c(c.a.b(0, (ParameterizedType) b2));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
