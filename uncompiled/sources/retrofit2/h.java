package retrofit2;

import java.io.IOException;
import java.util.Objects;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/* compiled from: OkHttpCall.java */
/* loaded from: classes3.dex */
public final class h<T> implements retrofit2.b<T> {
    public final m a;
    public final Object[] f0;
    public final Call.Factory g0;
    public final e<ResponseBody, T> h0;
    public volatile boolean i0;
    public Call j0;
    public Throwable k0;
    public boolean l0;

    /* compiled from: OkHttpCall.java */
    /* loaded from: classes3.dex */
    public class a implements Callback {
        public final /* synthetic */ wu a;

        public a(wu wuVar) {
            this.a = wuVar;
        }

        public final void a(Throwable th) {
            try {
                this.a.a(h.this, th);
            } catch (Throwable th2) {
                p.s(th2);
                th2.printStackTrace();
            }
        }

        @Override // okhttp3.Callback
        public void onFailure(Call call, IOException iOException) {
            a(iOException);
        }

        @Override // okhttp3.Callback
        public void onResponse(Call call, Response response) {
            try {
                try {
                    this.a.b(h.this, h.this.e(response));
                } catch (Throwable th) {
                    p.s(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                p.s(th2);
                a(th2);
            }
        }
    }

    /* compiled from: OkHttpCall.java */
    /* loaded from: classes3.dex */
    public static final class b extends ResponseBody {
        public final ResponseBody a;
        public final okio.d f0;
        public IOException g0;

        /* compiled from: OkHttpCall.java */
        /* loaded from: classes3.dex */
        public class a extends okio.g {
            public a(okio.n nVar) {
                super(nVar);
            }

            @Override // okio.g, okio.n
            public long read(okio.b bVar, long j) throws IOException {
                try {
                    return super.read(bVar, j);
                } catch (IOException e) {
                    b.this.g0 = e;
                    throw e;
                }
            }
        }

        public b(ResponseBody responseBody) {
            this.a = responseBody;
            this.f0 = okio.k.d(new a(responseBody.source()));
        }

        public void a() throws IOException {
            IOException iOException = this.g0;
            if (iOException != null) {
                throw iOException;
            }
        }

        @Override // okhttp3.ResponseBody, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.a.close();
        }

        @Override // okhttp3.ResponseBody
        public long contentLength() {
            return this.a.contentLength();
        }

        @Override // okhttp3.ResponseBody
        public MediaType contentType() {
            return this.a.contentType();
        }

        @Override // okhttp3.ResponseBody
        public okio.d source() {
            return this.f0;
        }
    }

    /* compiled from: OkHttpCall.java */
    /* loaded from: classes3.dex */
    public static final class c extends ResponseBody {
        public final MediaType a;
        public final long f0;

        public c(MediaType mediaType, long j) {
            this.a = mediaType;
            this.f0 = j;
        }

        @Override // okhttp3.ResponseBody
        public long contentLength() {
            return this.f0;
        }

        @Override // okhttp3.ResponseBody
        public MediaType contentType() {
            return this.a;
        }

        @Override // okhttp3.ResponseBody
        public okio.d source() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    public h(m mVar, Object[] objArr, Call.Factory factory, e<ResponseBody, T> eVar) {
        this.a = mVar;
        this.f0 = objArr;
        this.g0 = factory;
        this.h0 = eVar;
    }

    @Override // retrofit2.b
    /* renamed from: a */
    public h<T> clone() {
        return new h<>(this.a, this.f0, this.g0, this.h0);
    }

    public final Call b() throws IOException {
        Call newCall = this.g0.newCall(this.a.a(this.f0));
        Objects.requireNonNull(newCall, "Call.Factory returned null.");
        return newCall;
    }

    @Override // retrofit2.b
    public void cancel() {
        Call call;
        this.i0 = true;
        synchronized (this) {
            call = this.j0;
        }
        if (call != null) {
            call.cancel();
        }
    }

    public final Call d() throws IOException {
        Call call = this.j0;
        if (call != null) {
            return call;
        }
        Throwable th = this.k0;
        if (th != null) {
            if (!(th instanceof IOException)) {
                if (th instanceof RuntimeException) {
                    throw ((RuntimeException) th);
                }
                throw ((Error) th);
            }
            throw ((IOException) th);
        }
        try {
            Call b2 = b();
            this.j0 = b2;
            return b2;
        } catch (IOException | Error | RuntimeException e) {
            p.s(e);
            this.k0 = e;
            throw e;
        }
    }

    public n<T> e(Response response) throws IOException {
        ResponseBody body = response.body();
        Response build = response.newBuilder().body(new c(body.contentType(), body.contentLength())).build();
        int code = build.code();
        if (code < 200 || code >= 300) {
            try {
                return n.c(p.a(body), build);
            } finally {
                body.close();
            }
        } else if (code != 204 && code != 205) {
            b bVar = new b(body);
            try {
                return n.h(this.h0.a(bVar), build);
            } catch (RuntimeException e) {
                bVar.a();
                throw e;
            }
        } else {
            body.close();
            return n.h(null, build);
        }
    }

    @Override // retrofit2.b
    public n<T> execute() throws IOException {
        Call d;
        synchronized (this) {
            if (!this.l0) {
                this.l0 = true;
                d = d();
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.i0) {
            d.cancel();
        }
        return e(d.execute());
    }

    @Override // retrofit2.b
    public boolean isCanceled() {
        boolean z = true;
        if (this.i0) {
            return true;
        }
        synchronized (this) {
            Call call = this.j0;
            if (call == null || !call.isCanceled()) {
                z = false;
            }
        }
        return z;
    }

    @Override // retrofit2.b
    public void n(wu<T> wuVar) {
        Call call;
        Throwable th;
        Objects.requireNonNull(wuVar, "callback == null");
        synchronized (this) {
            if (!this.l0) {
                this.l0 = true;
                call = this.j0;
                th = this.k0;
                if (call == null && th == null) {
                    Call b2 = b();
                    this.j0 = b2;
                    call = b2;
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            wuVar.a(this, th);
            return;
        }
        if (this.i0) {
            call.cancel();
        }
        call.enqueue(new a(wuVar));
    }

    @Override // retrofit2.b
    public synchronized Request request() {
        try {
        } catch (IOException e) {
            throw new RuntimeException("Unable to create request.", e);
        }
        return d().request();
    }
}
