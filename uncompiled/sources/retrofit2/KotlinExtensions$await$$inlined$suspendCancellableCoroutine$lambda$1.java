package retrofit2;

import kotlin.jvm.internal.Lambda;

/* compiled from: KotlinExtensions.kt */
/* loaded from: classes3.dex */
public final class KotlinExtensions$await$$inlined$suspendCancellableCoroutine$lambda$1 extends Lambda implements tc1<Throwable, te4> {
    public final /* synthetic */ b $this_await$inlined;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public KotlinExtensions$await$$inlined$suspendCancellableCoroutine$lambda$1(b bVar) {
        super(1);
        this.$this_await$inlined = bVar;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        invoke2(th);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        this.$this_await$inlined.cancel();
    }
}
