package retrofit2;

import java.lang.reflect.Method;
import kotlin.KotlinNullPointerException;
import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: KotlinExtensions.kt */
/* loaded from: classes3.dex */
public final class KotlinExtensions {

    /* compiled from: KotlinExtensions.kt */
    /* loaded from: classes3.dex */
    public static final class a implements wu<T> {
        public final /* synthetic */ ov a;

        public a(ov ovVar) {
            this.a = ovVar;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<T> bVar, Throwable th) {
            fs1.g(bVar, "call");
            fs1.g(th, "t");
            ov ovVar = this.a;
            Result.a aVar = Result.Companion;
            ovVar.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<T> bVar, n<T> nVar) {
            fs1.g(bVar, "call");
            fs1.g(nVar, "response");
            if (nVar.e()) {
                Object a = nVar.a();
                if (a == null) {
                    Object tag = bVar.request().tag(ls1.class);
                    if (tag == null) {
                        fs1.n();
                    }
                    fs1.c(tag, "call.request().tag(Invocation::class.java)!!");
                    Method a2 = ((ls1) tag).a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Response from ");
                    fs1.c(a2, "method");
                    Class<?> declaringClass = a2.getDeclaringClass();
                    fs1.c(declaringClass, "method.declaringClass");
                    sb.append(declaringClass.getName());
                    sb.append('.');
                    sb.append(a2.getName());
                    sb.append(" was null but response body type was declared as non-null");
                    KotlinNullPointerException kotlinNullPointerException = new KotlinNullPointerException(sb.toString());
                    ov ovVar = this.a;
                    Result.a aVar = Result.Companion;
                    ovVar.resumeWith(Result.m52constructorimpl(o83.a(kotlinNullPointerException)));
                    return;
                }
                ov ovVar2 = this.a;
                Result.a aVar2 = Result.Companion;
                ovVar2.resumeWith(Result.m52constructorimpl(a));
                return;
            }
            ov ovVar3 = this.a;
            HttpException httpException = new HttpException(nVar);
            Result.a aVar3 = Result.Companion;
            ovVar3.resumeWith(Result.m52constructorimpl(o83.a(httpException)));
        }
    }

    /* compiled from: KotlinExtensions.kt */
    /* loaded from: classes3.dex */
    public static final class b implements wu<T> {
        public final /* synthetic */ ov a;

        public b(ov ovVar) {
            this.a = ovVar;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<T> bVar, Throwable th) {
            fs1.g(bVar, "call");
            fs1.g(th, "t");
            ov ovVar = this.a;
            Result.a aVar = Result.Companion;
            ovVar.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<T> bVar, n<T> nVar) {
            fs1.g(bVar, "call");
            fs1.g(nVar, "response");
            if (nVar.e()) {
                ov ovVar = this.a;
                Object a = nVar.a();
                Result.a aVar = Result.Companion;
                ovVar.resumeWith(Result.m52constructorimpl(a));
                return;
            }
            ov ovVar2 = this.a;
            HttpException httpException = new HttpException(nVar);
            Result.a aVar2 = Result.Companion;
            ovVar2.resumeWith(Result.m52constructorimpl(o83.a(httpException)));
        }
    }

    /* compiled from: KotlinExtensions.kt */
    /* loaded from: classes3.dex */
    public static final class c implements wu<T> {
        public final /* synthetic */ ov a;

        public c(ov ovVar) {
            this.a = ovVar;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<T> bVar, Throwable th) {
            fs1.g(bVar, "call");
            fs1.g(th, "t");
            ov ovVar = this.a;
            Result.a aVar = Result.Companion;
            ovVar.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<T> bVar, n<T> nVar) {
            fs1.g(bVar, "call");
            fs1.g(nVar, "response");
            ov ovVar = this.a;
            Result.a aVar = Result.Companion;
            ovVar.resumeWith(Result.m52constructorimpl(nVar));
        }
    }

    /* compiled from: KotlinExtensions.kt */
    /* loaded from: classes3.dex */
    public static final class d implements Runnable {
        public final /* synthetic */ q70 a;
        public final /* synthetic */ Exception f0;

        public d(q70 q70Var, Exception exc) {
            this.a = q70Var;
            this.f0 = exc;
        }

        @Override // java.lang.Runnable
        public final void run() {
            q70 c = IntrinsicsKt__IntrinsicsJvmKt.c(this.a);
            Exception exc = this.f0;
            Result.a aVar = Result.Companion;
            c.resumeWith(Result.m52constructorimpl(o83.a(exc)));
        }
    }

    public static final <T> Object a(retrofit2.b<T> bVar, q70<? super T> q70Var) {
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.f(new KotlinExtensions$await$$inlined$suspendCancellableCoroutine$lambda$1(bVar));
        bVar.n(new a(pvVar));
        Object x = pvVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x;
    }

    public static final <T> Object b(retrofit2.b<T> bVar, q70<? super T> q70Var) {
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.f(new KotlinExtensions$await$$inlined$suspendCancellableCoroutine$lambda$2(bVar));
        bVar.n(new b(pvVar));
        Object x = pvVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x;
    }

    public static final <T> Object c(retrofit2.b<T> bVar, q70<? super n<T>> q70Var) {
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.f(new KotlinExtensions$awaitResponse$$inlined$suspendCancellableCoroutine$lambda$1(bVar));
        bVar.n(new c(pvVar));
        Object x = pvVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final java.lang.Object d(java.lang.Exception r4, defpackage.q70<?> r5) {
        /*
            boolean r0 = r5 instanceof retrofit2.KotlinExtensions$suspendAndThrow$1
            if (r0 == 0) goto L13
            r0 = r5
            retrofit2.KotlinExtensions$suspendAndThrow$1 r0 = (retrofit2.KotlinExtensions$suspendAndThrow$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            retrofit2.KotlinExtensions$suspendAndThrow$1 r0 = new retrofit2.KotlinExtensions$suspendAndThrow$1
            r0.<init>(r5)
        L18:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            java.lang.Object r4 = r0.L$0
            java.lang.Exception r4 = (java.lang.Exception) r4
            defpackage.o83.b(r5)
            goto L5c
        L2d:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "call to 'resume' before 'invoke' with coroutine"
            r4.<init>(r5)
            throw r4
        L35:
            defpackage.o83.b(r5)
            r0.L$0 = r4
            r0.label = r3
            kotlinx.coroutines.CoroutineDispatcher r5 = defpackage.tp0.a()
            kotlin.coroutines.CoroutineContext r2 = r0.getContext()
            retrofit2.KotlinExtensions$d r3 = new retrofit2.KotlinExtensions$d
            r3.<init>(r0, r4)
            r5.h(r2, r3)
            java.lang.Object r4 = defpackage.gs1.d()
            java.lang.Object r5 = defpackage.gs1.d()
            if (r4 != r5) goto L59
            defpackage.ef0.c(r0)
        L59:
            if (r4 != r1) goto L5c
            return r1
        L5c:
            te4 r4 = defpackage.te4.a
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: retrofit2.KotlinExtensions.d(java.lang.Exception, q70):java.lang.Object");
    }
}
