package retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* compiled from: CallAdapter.java */
/* loaded from: classes3.dex */
public interface c<R, T> {

    /* compiled from: CallAdapter.java */
    /* loaded from: classes3.dex */
    public static abstract class a {
        public static Type b(int i, ParameterizedType parameterizedType) {
            return p.g(i, parameterizedType);
        }

        public static Class<?> c(Type type) {
            return p.h(type);
        }

        public abstract c<?, ?> a(Type type, Annotation[] annotationArr, o oVar);
    }

    Type a();

    T b(b<R> bVar);
}
