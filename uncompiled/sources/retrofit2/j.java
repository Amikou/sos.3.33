package retrofit2;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;
import okhttp3.Headers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/* compiled from: ParameterHandler.java */
/* loaded from: classes3.dex */
public abstract class j<T> {

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public class a extends j<Iterable<T>> {
        public a() {
        }

        @Override // retrofit2.j
        /* renamed from: d */
        public void a(retrofit2.l lVar, Iterable<T> iterable) throws IOException {
            if (iterable == null) {
                return;
            }
            for (T t : iterable) {
                j.this.a(lVar, t);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public class b extends j<Object> {
        public b() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // retrofit2.j
        public void a(retrofit2.l lVar, Object obj) throws IOException {
            if (obj == null) {
                return;
            }
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                j.this.a(lVar, Array.get(obj, i));
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class c<T> extends j<T> {
        public final Method a;
        public final int b;
        public final retrofit2.e<T, RequestBody> c;

        public c(Method method, int i, retrofit2.e<T, RequestBody> eVar) {
            this.a = method;
            this.b = i;
            this.c = eVar;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) {
            if (t != null) {
                try {
                    lVar.l(this.c.a(t));
                    return;
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw retrofit2.p.p(method, e, i, "Unable to convert " + t + " to RequestBody", new Object[0]);
                }
            }
            throw retrofit2.p.o(this.a, this.b, "Body parameter value must not be null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class d<T> extends j<T> {
        public final String a;
        public final retrofit2.e<T, String> b;
        public final boolean c;

        public d(String str, retrofit2.e<T, String> eVar, boolean z) {
            Objects.requireNonNull(str, "name == null");
            this.a = str;
            this.b = eVar;
            this.c = z;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) throws IOException {
            String a;
            if (t == null || (a = this.b.a(t)) == null) {
                return;
            }
            lVar.a(this.a, a, this.c);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class e<T> extends j<Map<String, T>> {
        public final Method a;
        public final int b;
        public final retrofit2.e<T, String> c;
        public final boolean d;

        public e(Method method, int i, retrofit2.e<T, String> eVar, boolean z) {
            this.a = method;
            this.b = i;
            this.c = eVar;
            this.d = z;
        }

        @Override // retrofit2.j
        /* renamed from: d */
        public void a(retrofit2.l lVar, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a = this.c.a(value);
                            if (a != null) {
                                lVar.a(key, a, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw retrofit2.p.o(method, i, "Field map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw retrofit2.p.o(method2, i2, "Field map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw retrofit2.p.o(this.a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw retrofit2.p.o(this.a, this.b, "Field map was null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class f<T> extends j<T> {
        public final String a;
        public final retrofit2.e<T, String> b;

        public f(String str, retrofit2.e<T, String> eVar) {
            Objects.requireNonNull(str, "name == null");
            this.a = str;
            this.b = eVar;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) throws IOException {
            String a;
            if (t == null || (a = this.b.a(t)) == null) {
                return;
            }
            lVar.b(this.a, a);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class g<T> extends j<Map<String, T>> {
        public final Method a;
        public final int b;
        public final retrofit2.e<T, String> c;

        public g(Method method, int i, retrofit2.e<T, String> eVar) {
            this.a = method;
            this.b = i;
            this.c = eVar;
        }

        @Override // retrofit2.j
        /* renamed from: d */
        public void a(retrofit2.l lVar, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            lVar.b(key, this.c.a(value));
                        } else {
                            Method method = this.a;
                            int i = this.b;
                            throw retrofit2.p.o(method, i, "Header map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw retrofit2.p.o(this.a, this.b, "Header map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw retrofit2.p.o(this.a, this.b, "Header map was null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class h extends j<Headers> {
        public final Method a;
        public final int b;

        public h(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @Override // retrofit2.j
        /* renamed from: d */
        public void a(retrofit2.l lVar, Headers headers) {
            if (headers != null) {
                lVar.c(headers);
                return;
            }
            throw retrofit2.p.o(this.a, this.b, "Headers parameter must not be null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class i<T> extends j<T> {
        public final Method a;
        public final int b;
        public final Headers c;
        public final retrofit2.e<T, RequestBody> d;

        public i(Method method, int i, Headers headers, retrofit2.e<T, RequestBody> eVar) {
            this.a = method;
            this.b = i;
            this.c = headers;
            this.d = eVar;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) {
            if (t == null) {
                return;
            }
            try {
                lVar.d(this.c, this.d.a(t));
            } catch (IOException e) {
                Method method = this.a;
                int i = this.b;
                throw retrofit2.p.o(method, i, "Unable to convert " + t + " to RequestBody", e);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* renamed from: retrofit2.j$j  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0280j<T> extends j<Map<String, T>> {
        public final Method a;
        public final int b;
        public final retrofit2.e<T, RequestBody> c;
        public final String d;

        public C0280j(Method method, int i, retrofit2.e<T, RequestBody> eVar, String str) {
            this.a = method;
            this.b = i;
            this.c = eVar;
            this.d = str;
        }

        @Override // retrofit2.j
        /* renamed from: d */
        public void a(retrofit2.l lVar, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            lVar.d(Headers.of("Content-Disposition", "form-data; name=\"" + key + "\"", "Content-Transfer-Encoding", this.d), this.c.a(value));
                        } else {
                            Method method = this.a;
                            int i = this.b;
                            throw retrofit2.p.o(method, i, "Part map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw retrofit2.p.o(this.a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw retrofit2.p.o(this.a, this.b, "Part map was null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class k<T> extends j<T> {
        public final Method a;
        public final int b;
        public final String c;
        public final retrofit2.e<T, String> d;
        public final boolean e;

        public k(Method method, int i, String str, retrofit2.e<T, String> eVar, boolean z) {
            this.a = method;
            this.b = i;
            Objects.requireNonNull(str, "name == null");
            this.c = str;
            this.d = eVar;
            this.e = z;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) throws IOException {
            if (t != null) {
                lVar.f(this.c, this.d.a(t), this.e);
                return;
            }
            Method method = this.a;
            int i = this.b;
            throw retrofit2.p.o(method, i, "Path parameter \"" + this.c + "\" value must not be null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class l<T> extends j<T> {
        public final String a;
        public final retrofit2.e<T, String> b;
        public final boolean c;

        public l(String str, retrofit2.e<T, String> eVar, boolean z) {
            Objects.requireNonNull(str, "name == null");
            this.a = str;
            this.b = eVar;
            this.c = z;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) throws IOException {
            String a;
            if (t == null || (a = this.b.a(t)) == null) {
                return;
            }
            lVar.g(this.a, a, this.c);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class m<T> extends j<Map<String, T>> {
        public final Method a;
        public final int b;
        public final retrofit2.e<T, String> c;
        public final boolean d;

        public m(Method method, int i, retrofit2.e<T, String> eVar, boolean z) {
            this.a = method;
            this.b = i;
            this.c = eVar;
            this.d = z;
        }

        @Override // retrofit2.j
        /* renamed from: d */
        public void a(retrofit2.l lVar, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a = this.c.a(value);
                            if (a != null) {
                                lVar.g(key, a, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw retrofit2.p.o(method, i, "Query map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw retrofit2.p.o(method2, i2, "Query map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw retrofit2.p.o(this.a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw retrofit2.p.o(this.a, this.b, "Query map was null", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class n<T> extends j<T> {
        public final retrofit2.e<T, String> a;
        public final boolean b;

        public n(retrofit2.e<T, String> eVar, boolean z) {
            this.a = eVar;
            this.b = z;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) throws IOException {
            if (t == null) {
                return;
            }
            lVar.g(this.a.a(t), null, this.b);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class o extends j<MultipartBody.Part> {
        public static final o a = new o();

        @Override // retrofit2.j
        /* renamed from: d */
        public void a(retrofit2.l lVar, MultipartBody.Part part) {
            if (part != null) {
                lVar.e(part);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class p extends j<Object> {
        public final Method a;
        public final int b;

        public p(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, Object obj) {
            if (obj != null) {
                lVar.m(obj);
                return;
            }
            throw retrofit2.p.o(this.a, this.b, "@Url parameter is null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class q<T> extends j<T> {
        public final Class<T> a;

        public q(Class<T> cls) {
            this.a = cls;
        }

        @Override // retrofit2.j
        public void a(retrofit2.l lVar, T t) {
            lVar.h(this.a, t);
        }
    }

    public abstract void a(retrofit2.l lVar, T t) throws IOException;

    public final j<Object> b() {
        return new b();
    }

    public final j<Iterable<T>> c() {
        return new a();
    }
}
