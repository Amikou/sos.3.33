package retrofit2;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import retrofit2.c;
import retrofit2.e;

/* compiled from: Platform.java */
/* loaded from: classes3.dex */
public class k {
    public static final k c = f();
    public final boolean a;
    public final Constructor<MethodHandles.Lookup> b;

    /* compiled from: Platform.java */
    /* loaded from: classes3.dex */
    public static final class a extends k {

        /* compiled from: Platform.java */
        /* renamed from: retrofit2.k$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class ExecutorC0281a implements Executor {
            public final Handler a = new Handler(Looper.getMainLooper());

            @Override // java.util.concurrent.Executor
            public void execute(Runnable runnable) {
                this.a.post(runnable);
            }
        }

        public a() {
            super(Build.VERSION.SDK_INT >= 24);
        }

        @Override // retrofit2.k
        public Executor c() {
            return new ExecutorC0281a();
        }

        @Override // retrofit2.k
        public Object h(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
            if (Build.VERSION.SDK_INT >= 26) {
                return super.h(method, cls, obj, objArr);
            }
            throw new UnsupportedOperationException("Calling default methods on API 24 and 25 is not supported");
        }
    }

    public k(boolean z) {
        this.a = z;
        Constructor<MethodHandles.Lookup> constructor = null;
        if (z) {
            try {
                constructor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class, Integer.TYPE);
                constructor.setAccessible(true);
            } catch (NoClassDefFoundError | NoSuchMethodException unused) {
            }
        }
        this.b = constructor;
    }

    public static k f() {
        if ("Dalvik".equals(System.getProperty("java.vm.name"))) {
            return new a();
        }
        return new k(true);
    }

    public static k g() {
        return c;
    }

    public List<? extends c.a> a(Executor executor) {
        f fVar = new f(executor);
        return this.a ? Arrays.asList(d.a, fVar) : Collections.singletonList(fVar);
    }

    public int b() {
        return this.a ? 2 : 1;
    }

    public Executor c() {
        return null;
    }

    public List<? extends e.a> d() {
        return this.a ? Collections.singletonList(i.a) : Collections.emptyList();
    }

    public int e() {
        return this.a ? 1 : 0;
    }

    public Object h(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
        Constructor<MethodHandles.Lookup> constructor = this.b;
        return (constructor != null ? constructor.newInstance(cls, -1) : MethodHandles.lookup()).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
    }

    public boolean i(Method method) {
        return this.a && method.isDefault();
    }
}
