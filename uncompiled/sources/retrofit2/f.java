package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.concurrent.Executor;
import okhttp3.Request;
import retrofit2.c;
import retrofit2.f;

/* compiled from: DefaultCallAdapterFactory.java */
/* loaded from: classes3.dex */
public final class f extends c.a {
    public final Executor a;

    /* compiled from: DefaultCallAdapterFactory.java */
    /* loaded from: classes3.dex */
    public class a implements c<Object, retrofit2.b<?>> {
        public final /* synthetic */ Type a;
        public final /* synthetic */ Executor b;

        public a(f fVar, Type type, Executor executor) {
            this.a = type;
            this.b = executor;
        }

        @Override // retrofit2.c
        public Type a() {
            return this.a;
        }

        @Override // retrofit2.c
        /* renamed from: c */
        public retrofit2.b<Object> b(retrofit2.b<Object> bVar) {
            Executor executor = this.b;
            return executor == null ? bVar : new b(executor, bVar);
        }
    }

    /* compiled from: DefaultCallAdapterFactory.java */
    /* loaded from: classes3.dex */
    public static final class b<T> implements retrofit2.b<T> {
        public final Executor a;
        public final retrofit2.b<T> f0;

        /* compiled from: DefaultCallAdapterFactory.java */
        /* loaded from: classes3.dex */
        public class a implements wu<T> {
            public final /* synthetic */ wu a;

            public a(wu wuVar) {
                this.a = wuVar;
            }

            /* JADX INFO: Access modifiers changed from: private */
            public /* synthetic */ void e(wu wuVar, Throwable th) {
                wuVar.a(b.this, th);
            }

            /* JADX INFO: Access modifiers changed from: private */
            public /* synthetic */ void f(wu wuVar, n nVar) {
                if (b.this.f0.isCanceled()) {
                    wuVar.a(b.this, new IOException("Canceled"));
                } else {
                    wuVar.b(b.this, nVar);
                }
            }

            @Override // defpackage.wu
            public void a(retrofit2.b<T> bVar, final Throwable th) {
                Executor executor = b.this.a;
                final wu wuVar = this.a;
                executor.execute(new Runnable() { // from class: ki0
                    @Override // java.lang.Runnable
                    public final void run() {
                        f.b.a.this.e(wuVar, th);
                    }
                });
            }

            @Override // defpackage.wu
            public void b(retrofit2.b<T> bVar, final n<T> nVar) {
                Executor executor = b.this.a;
                final wu wuVar = this.a;
                executor.execute(new Runnable() { // from class: li0
                    @Override // java.lang.Runnable
                    public final void run() {
                        f.b.a.this.f(wuVar, nVar);
                    }
                });
            }
        }

        public b(Executor executor, retrofit2.b<T> bVar) {
            this.a = executor;
            this.f0 = bVar;
        }

        @Override // retrofit2.b
        public void cancel() {
            this.f0.cancel();
        }

        @Override // retrofit2.b
        public n<T> execute() throws IOException {
            return this.f0.execute();
        }

        @Override // retrofit2.b
        public boolean isCanceled() {
            return this.f0.isCanceled();
        }

        @Override // retrofit2.b
        public void n(wu<T> wuVar) {
            Objects.requireNonNull(wuVar, "callback == null");
            this.f0.n(new a(wuVar));
        }

        @Override // retrofit2.b
        public Request request() {
            return this.f0.request();
        }

        @Override // retrofit2.b
        public retrofit2.b<T> clone() {
            return new b(this.a, this.f0.clone());
        }
    }

    public f(Executor executor) {
        this.a = executor;
    }

    @Override // retrofit2.c.a
    public c<?, ?> a(Type type, Annotation[] annotationArr, o oVar) {
        if (c.a.c(type) != retrofit2.b.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            return new a(this, p.g(0, (ParameterizedType) type), p.l(annotationArr, eq3.class) ? null : this.a);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
