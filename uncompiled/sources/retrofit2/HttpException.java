package retrofit2;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.Objects;

/* loaded from: classes3.dex */
public class HttpException extends RuntimeException {
    public final transient n<?> a;
    private final int code;
    private final String message;

    public HttpException(n<?> nVar) {
        super(a(nVar));
        this.code = nVar.b();
        this.message = nVar.f();
        this.a = nVar;
    }

    public static String a(n<?> nVar) {
        Objects.requireNonNull(nVar, "response == null");
        return "HTTP " + nVar.b() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + nVar.f();
    }

    public int code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    public n<?> response() {
        return this.a;
    }
}
