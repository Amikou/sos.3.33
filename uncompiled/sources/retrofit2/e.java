package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/* compiled from: Converter.java */
/* loaded from: classes3.dex */
public interface e<F, T> {

    /* compiled from: Converter.java */
    /* loaded from: classes3.dex */
    public static abstract class a {
        public static Type a(int i, ParameterizedType parameterizedType) {
            return p.g(i, parameterizedType);
        }

        public static Class<?> b(Type type) {
            return p.h(type);
        }

        public e<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, o oVar) {
            return null;
        }

        public e<ResponseBody, ?> d(Type type, Annotation[] annotationArr, o oVar) {
            return null;
        }

        public e<?, String> e(Type type, Annotation[] annotationArr, o oVar) {
            return null;
        }
    }

    T a(F f) throws IOException;
}
