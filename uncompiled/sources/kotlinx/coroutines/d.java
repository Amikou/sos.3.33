package kotlinx.coroutines;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/* compiled from: DefaultExecutor.kt */
/* loaded from: classes2.dex */
public final class d extends e implements Runnable {
    private static volatile Thread _thread;
    private static volatile int debugStatus;
    public static final d k0;
    public static final long l0;

    static {
        Long l;
        d dVar = new d();
        k0 = dVar;
        xx0.S(dVar, false, 1, null);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        try {
            l = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000L);
        } catch (SecurityException unused) {
            l = 1000L;
        }
        l0 = timeUnit.toNanos(l.longValue());
    }

    public final synchronized void S0() {
        if (U0()) {
            debugStatus = 3;
            H0();
            notifyAll();
        }
    }

    public final synchronized Thread T0() {
        Thread thread;
        thread = _thread;
        if (thread == null) {
            thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
            _thread = thread;
            thread.setDaemon(true);
            thread.start();
        }
        return thread;
    }

    public final boolean U0() {
        int i = debugStatus;
        return i == 2 || i == 3;
    }

    public final synchronized boolean W0() {
        if (U0()) {
            return false;
        }
        debugStatus = 1;
        notifyAll();
        return true;
    }

    @Override // defpackage.yx0
    public Thread f0() {
        Thread thread = _thread;
        return thread == null ? T0() : thread;
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean B0;
        j54.a.d(this);
        n5.a();
        try {
            if (!W0()) {
                if (B0) {
                    return;
                }
                return;
            }
            long j = Long.MAX_VALUE;
            while (true) {
                Thread.interrupted();
                long a0 = a0();
                if (a0 == Long.MAX_VALUE) {
                    n5.a();
                    long nanoTime = System.nanoTime();
                    if (j == Long.MAX_VALUE) {
                        j = l0 + nanoTime;
                    }
                    long j2 = j - nanoTime;
                    if (j2 <= 0) {
                        _thread = null;
                        S0();
                        n5.a();
                        if (B0()) {
                            return;
                        }
                        f0();
                        return;
                    }
                    a0 = u33.e(a0, j2);
                } else {
                    j = Long.MAX_VALUE;
                }
                if (a0 > 0) {
                    if (U0()) {
                        _thread = null;
                        S0();
                        n5.a();
                        if (B0()) {
                            return;
                        }
                        f0();
                        return;
                    }
                    n5.a();
                    LockSupport.parkNanos(this, a0);
                }
            }
        } finally {
            _thread = null;
            S0();
            n5.a();
            if (!B0()) {
                f0();
            }
        }
    }
}
