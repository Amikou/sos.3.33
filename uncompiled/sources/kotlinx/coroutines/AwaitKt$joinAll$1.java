package kotlinx.coroutines;

import kotlin.coroutines.jvm.internal.ContinuationImpl;

/* compiled from: Await.kt */
@kotlin.coroutines.jvm.internal.a(c = "kotlinx.coroutines.AwaitKt", f = "Await.kt", l = {54}, m = "joinAll")
/* loaded from: classes2.dex */
public final class AwaitKt$joinAll$1 extends ContinuationImpl {
    public int I$0;
    public int I$1;
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;

    public AwaitKt$joinAll$1(q70<? super AwaitKt$joinAll$1> q70Var) {
        super(q70Var);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return AwaitKt.a(null, this);
    }
}
