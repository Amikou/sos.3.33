package kotlinx.coroutines;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.c;

/* compiled from: CommonPool.kt */
/* loaded from: classes2.dex */
public final class c extends ExecutorCoroutineDispatcher {
    public static final c f0 = new c();
    public static final int g0;
    public static boolean h0;
    private static volatile Executor pool;

    static {
        String str;
        int intValue;
        try {
            str = System.getProperty("kotlinx.coroutines.default.parallelism");
        } catch (Throwable unused) {
            str = null;
        }
        if (str == null) {
            intValue = -1;
        } else {
            Integer l = cv3.l(str);
            if (l != null && l.intValue() >= 1) {
                intValue = l.intValue();
            } else {
                throw new IllegalStateException(fs1.l("Expected positive number in kotlinx.coroutines.default.parallelism, but has ", str).toString());
            }
        }
        g0 = intValue;
    }

    public static final Thread Q(AtomicInteger atomicInteger, Runnable runnable) {
        Thread thread = new Thread(runnable, fs1.l("CommonPool-worker-", Integer.valueOf(atomicInteger.incrementAndGet())));
        thread.setDaemon(true);
        return thread;
    }

    public static final void a0() {
    }

    public final ExecutorService N() {
        final AtomicInteger atomicInteger = new AtomicInteger();
        return Executors.newFixedThreadPool(W(), new ThreadFactory() { // from class: h30
            @Override // java.util.concurrent.ThreadFactory
            public final Thread newThread(Runnable runnable) {
                Thread Q;
                Q = c.Q(atomicInteger, runnable);
                return Q;
            }
        });
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x003d  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.util.concurrent.ExecutorService R() {
        /*
            r6 = this;
            java.lang.SecurityManager r0 = java.lang.System.getSecurityManager()
            if (r0 == 0) goto Lb
            java.util.concurrent.ExecutorService r0 = r6.N()
            return r0
        Lb:
            r0 = 0
            java.lang.String r1 = "java.util.concurrent.ForkJoinPool"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch: java.lang.Throwable -> L13
            goto L14
        L13:
            r1 = r0
        L14:
            if (r1 != 0) goto L1b
            java.util.concurrent.ExecutorService r0 = r6.N()
            return r0
        L1b:
            boolean r2 = kotlinx.coroutines.c.h0
            r3 = 0
            if (r2 != 0) goto L4b
            int r2 = kotlinx.coroutines.c.g0
            if (r2 >= 0) goto L4b
            java.lang.String r2 = "commonPool"
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch: java.lang.Throwable -> L39
            java.lang.reflect.Method r2 = r1.getMethod(r2, r4)     // Catch: java.lang.Throwable -> L39
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch: java.lang.Throwable -> L39
            java.lang.Object r2 = r2.invoke(r0, r4)     // Catch: java.lang.Throwable -> L39
            boolean r4 = r2 instanceof java.util.concurrent.ExecutorService     // Catch: java.lang.Throwable -> L39
            if (r4 == 0) goto L39
            java.util.concurrent.ExecutorService r2 = (java.util.concurrent.ExecutorService) r2     // Catch: java.lang.Throwable -> L39
            goto L3a
        L39:
            r2 = r0
        L3a:
            if (r2 != 0) goto L3d
            goto L4b
        L3d:
            kotlinx.coroutines.c r4 = kotlinx.coroutines.c.f0
            boolean r4 = r4.X(r1, r2)
            if (r4 == 0) goto L46
            goto L47
        L46:
            r2 = r0
        L47:
            if (r2 != 0) goto L4a
            goto L4b
        L4a:
            return r2
        L4b:
            r2 = 1
            java.lang.Class[] r4 = new java.lang.Class[r2]     // Catch: java.lang.Throwable -> L6f
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch: java.lang.Throwable -> L6f
            r4[r3] = r5     // Catch: java.lang.Throwable -> L6f
            java.lang.reflect.Constructor r1 = r1.getConstructor(r4)     // Catch: java.lang.Throwable -> L6f
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch: java.lang.Throwable -> L6f
            kotlinx.coroutines.c r4 = kotlinx.coroutines.c.f0     // Catch: java.lang.Throwable -> L6f
            int r4 = r4.W()     // Catch: java.lang.Throwable -> L6f
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch: java.lang.Throwable -> L6f
            r2[r3] = r4     // Catch: java.lang.Throwable -> L6f
            java.lang.Object r1 = r1.newInstance(r2)     // Catch: java.lang.Throwable -> L6f
            boolean r2 = r1 instanceof java.util.concurrent.ExecutorService     // Catch: java.lang.Throwable -> L6f
            if (r2 == 0) goto L6f
            java.util.concurrent.ExecutorService r1 = (java.util.concurrent.ExecutorService) r1     // Catch: java.lang.Throwable -> L6f
            r0 = r1
        L6f:
            if (r0 != 0) goto L75
            java.util.concurrent.ExecutorService r0 = r6.N()
        L75:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.c.R():java.util.concurrent.ExecutorService");
    }

    public final synchronized Executor S() {
        Executor executor;
        executor = pool;
        if (executor == null) {
            executor = R();
            pool = executor;
        }
        return executor;
    }

    public final int W() {
        Integer valueOf = Integer.valueOf(g0);
        if (!(valueOf.intValue() > 0)) {
            valueOf = null;
        }
        if (valueOf == null) {
            return u33.b(Runtime.getRuntime().availableProcessors() - 1, 1);
        }
        return valueOf.intValue();
    }

    public final boolean X(Class<?> cls, ExecutorService executorService) {
        executorService.submit(g30.a);
        Integer num = null;
        try {
            Object invoke = cls.getMethod("getPoolSize", new Class[0]).invoke(executorService, new Object[0]);
            if (invoke instanceof Integer) {
                num = (Integer) invoke;
            }
        } catch (Throwable unused) {
        }
        return num != null && num.intValue() >= 1;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on CommonPool".toString());
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void h(CoroutineContext coroutineContext, Runnable runnable) {
        try {
            Executor executor = pool;
            if (executor == null) {
                executor = S();
            }
            n5.a();
            executor.execute(runnable);
        } catch (RejectedExecutionException unused) {
            n5.a();
            d.k0.x0(runnable);
        }
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        return "CommonPool";
    }
}
