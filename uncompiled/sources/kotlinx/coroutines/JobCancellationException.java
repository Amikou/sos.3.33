package kotlinx.coroutines;

import java.util.concurrent.CancellationException;

/* compiled from: Exceptions.kt */
/* loaded from: classes2.dex */
public final class JobCancellationException extends CancellationException implements u80<JobCancellationException> {
    public final st1 job;

    public JobCancellationException(String str, Throwable th, st1 st1Var) {
        super(str);
        this.job = st1Var;
        if (th != null) {
            initCause(th);
        }
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof JobCancellationException) {
                JobCancellationException jobCancellationException = (JobCancellationException) obj;
                if (!fs1.b(jobCancellationException.getMessage(), getMessage()) || !fs1.b(jobCancellationException.job, this.job) || !fs1.b(jobCancellationException.getCause(), getCause())) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Throwable
    public Throwable fillInStackTrace() {
        if (ze0.c()) {
            return super.fillInStackTrace();
        }
        setStackTrace(new StackTraceElement[0]);
        return this;
    }

    public int hashCode() {
        String message = getMessage();
        fs1.d(message);
        int hashCode = ((message.hashCode() * 31) + this.job.hashCode()) * 31;
        Throwable cause = getCause();
        return hashCode + (cause == null ? 0 : cause.hashCode());
    }

    @Override // java.lang.Throwable
    public String toString() {
        return super.toString() + "; job=" + this.job;
    }

    @Override // defpackage.u80
    public JobCancellationException createCopy() {
        if (ze0.c()) {
            String message = getMessage();
            fs1.d(message);
            return new JobCancellationException(message, this, this.job);
        }
        return null;
    }
}
