package kotlinx.coroutines.android;

import android.os.Handler;
import android.os.Looper;
import kotlin.coroutines.CoroutineContext;

/* compiled from: HandlerDispatcher.kt */
/* loaded from: classes2.dex */
public final class HandlerContext extends lj1 {
    private volatile HandlerContext _immediate;
    public final Handler f0;
    public final String g0;
    public final boolean h0;
    public final HandlerContext i0;

    /* compiled from: Runnable.kt */
    /* loaded from: classes2.dex */
    public static final class a implements Runnable {
        public final /* synthetic */ ov a;
        public final /* synthetic */ HandlerContext f0;

        public a(ov ovVar, HandlerContext handlerContext) {
            this.a = ovVar;
            this.f0 = handlerContext;
        }

        @Override // java.lang.Runnable
        public final void run() {
            this.a.k(this.f0, te4.a);
        }
    }

    public HandlerContext(Handler handler, String str, boolean z) {
        super(null);
        this.f0 = handler;
        this.g0 = str;
        this.h0 = z;
        this._immediate = z ? this : null;
        HandlerContext handlerContext = this._immediate;
        if (handlerContext == null) {
            handlerContext = new HandlerContext(handler, str, true);
            this._immediate = handlerContext;
            te4 te4Var = te4.a;
        }
        this.i0 = handlerContext;
    }

    @Override // defpackage.e32
    /* renamed from: N */
    public HandlerContext l() {
        return this.i0;
    }

    public boolean equals(Object obj) {
        return (obj instanceof HandlerContext) && ((HandlerContext) obj).f0 == this.f0;
    }

    @Override // defpackage.wl0
    public void f(long j, ov<? super te4> ovVar) {
        a aVar = new a(ovVar, this);
        this.f0.postDelayed(aVar, u33.e(j, 4611686018427387903L));
        ovVar.f(new HandlerContext$scheduleResumeAfterDelay$1(this, aVar));
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void h(CoroutineContext coroutineContext, Runnable runnable) {
        this.f0.post(runnable);
    }

    public int hashCode() {
        return System.identityHashCode(this.f0);
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public boolean j(CoroutineContext coroutineContext) {
        return (this.h0 && fs1.b(Looper.myLooper(), this.f0.getLooper())) ? false : true;
    }

    @Override // defpackage.e32, kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        String m = m();
        if (m == null) {
            String str = this.g0;
            if (str == null) {
                str = this.f0.toString();
            }
            return this.h0 ? fs1.l(str, ".immediate") : str;
        }
        return m;
    }

    public /* synthetic */ HandlerContext(Handler handler, String str, int i, qi0 qi0Var) {
        this(handler, (i & 2) != 0 ? null : str);
    }

    public HandlerContext(Handler handler, String str) {
        this(handler, str, false);
    }
}
