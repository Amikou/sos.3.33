package kotlinx.coroutines;

import java.util.concurrent.CancellationException;

/* compiled from: Timeout.kt */
/* loaded from: classes2.dex */
public final class TimeoutCancellationException extends CancellationException implements u80<TimeoutCancellationException> {
    public final st1 coroutine;

    public TimeoutCancellationException(String str, st1 st1Var) {
        super(str);
        this.coroutine = st1Var;
    }

    @Override // defpackage.u80
    public TimeoutCancellationException createCopy() {
        String message = getMessage();
        if (message == null) {
            message = "";
        }
        TimeoutCancellationException timeoutCancellationException = new TimeoutCancellationException(message, this.coroutine);
        timeoutCancellationException.initCause(this);
        return timeoutCancellationException;
    }

    public TimeoutCancellationException(String str) {
        this(str, null);
    }
}
