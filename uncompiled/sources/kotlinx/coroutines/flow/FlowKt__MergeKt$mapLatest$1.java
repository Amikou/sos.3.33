package kotlinx.coroutines.flow;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Merge.kt */
@a(c = "kotlinx.coroutines.flow.FlowKt__MergeKt$mapLatest$1", f = "Merge.kt", l = {217, 217}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class FlowKt__MergeKt$mapLatest$1 extends SuspendLambda implements kd1<k71<? super R>, T, q70<? super te4>, Object> {
    public final /* synthetic */ hd1<T, q70<? super R>, Object> $transform;
    private /* synthetic */ Object L$0;
    public /* synthetic */ Object L$1;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public FlowKt__MergeKt$mapLatest$1(hd1<? super T, ? super q70<? super R>, ? extends Object> hd1Var, q70<? super FlowKt__MergeKt$mapLatest$1> q70Var) {
        super(3, q70Var);
        this.$transform = hd1Var;
    }

    @Override // defpackage.kd1
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, q70<? super te4> q70Var) {
        return invoke((k71) obj, (k71) obj2, q70Var);
    }

    public final Object invoke(k71<? super R> k71Var, T t, q70<? super te4> q70Var) {
        FlowKt__MergeKt$mapLatest$1 flowKt__MergeKt$mapLatest$1 = new FlowKt__MergeKt$mapLatest$1(this.$transform, q70Var);
        flowKt__MergeKt$mapLatest$1.L$0 = k71Var;
        flowKt__MergeKt$mapLatest$1.L$1 = t;
        return flowKt__MergeKt$mapLatest$1.invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        k71 k71Var;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            k71Var = (k71) this.L$0;
            Object obj2 = this.L$1;
            hd1<T, q70<? super R>, Object> hd1Var = this.$transform;
            this.L$0 = k71Var;
            this.label = 1;
            obj = hd1Var.invoke(obj2, this);
            if (obj == d) {
                return d;
            }
        } else if (i != 1) {
            if (i == 2) {
                o83.b(obj);
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            k71Var = (k71) this.L$0;
            o83.b(obj);
        }
        this.L$0 = null;
        this.label = 2;
        if (k71Var.emit(obj, this) == d) {
            return d;
        }
        return te4.a;
    }
}
