package kotlinx.coroutines.flow;

import kotlin.jvm.internal.Lambda;

/* compiled from: Distinct.kt */
/* loaded from: classes2.dex */
public final class FlowKt__DistinctKt$defaultAreEquivalent$1 extends Lambda implements hd1<Object, Object, Boolean> {
    public static final FlowKt__DistinctKt$defaultAreEquivalent$1 INSTANCE = new FlowKt__DistinctKt$defaultAreEquivalent$1();

    public FlowKt__DistinctKt$defaultAreEquivalent$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ Boolean invoke(Object obj, Object obj2) {
        return Boolean.valueOf(invoke2(obj, obj2));
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Boolean, boolean] */
    @Override // defpackage.hd1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2(Object obj, Object obj2) {
        return fs1.b(obj, obj2);
    }
}
