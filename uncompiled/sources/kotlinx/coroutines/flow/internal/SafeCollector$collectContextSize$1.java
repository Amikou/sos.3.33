package kotlinx.coroutines.flow.internal;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: SafeCollector.kt */
/* loaded from: classes2.dex */
public final class SafeCollector$collectContextSize$1 extends Lambda implements hd1<Integer, CoroutineContext.a, Integer> {
    public static final SafeCollector$collectContextSize$1 INSTANCE = new SafeCollector$collectContextSize$1();

    public SafeCollector$collectContextSize$1() {
        super(2);
    }

    public final int invoke(int i, CoroutineContext.a aVar) {
        return i + 1;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ Integer invoke(Integer num, CoroutineContext.a aVar) {
        return Integer.valueOf(invoke(num.intValue(), aVar));
    }
}
