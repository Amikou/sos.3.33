package kotlinx.coroutines.flow.internal;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: SafeCollector.common.kt */
/* loaded from: classes2.dex */
public final class SafeCollector_commonKt$checkContext$result$1 extends Lambda implements hd1<Integer, CoroutineContext.a, Integer> {
    public final /* synthetic */ SafeCollector<?> $this_checkContext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SafeCollector_commonKt$checkContext$result$1(SafeCollector<?> safeCollector) {
        super(2);
        this.$this_checkContext = safeCollector;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ Integer invoke(Integer num, CoroutineContext.a aVar) {
        return Integer.valueOf(invoke(num.intValue(), aVar));
    }

    public final int invoke(int i, CoroutineContext.a aVar) {
        CoroutineContext.b<?> key = aVar.getKey();
        CoroutineContext.a aVar2 = this.$this_checkContext.collectContext.get(key);
        if (key != st1.f) {
            if (aVar != aVar2) {
                return Integer.MIN_VALUE;
            }
            return i + 1;
        }
        st1 st1Var = (st1) aVar2;
        st1 b = SafeCollector_commonKt.b((st1) aVar, st1Var);
        if (b == st1Var) {
            return st1Var == null ? i : i + 1;
        }
        throw new IllegalStateException(("Flow invariant is violated:\n\t\tEmission from another coroutine is detected.\n\t\tChild of " + b + ", expected child of " + st1Var + ".\n\t\tFlowCollector is not thread-safe and concurrent emissions are prohibited.\n\t\tTo mitigate this restriction please use 'channelFlow' builder instead of 'flow'").toString());
    }
}
