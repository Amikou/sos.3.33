package kotlinx.coroutines.flow.internal;

import kotlin.coroutines.CoroutineContext;

/* compiled from: SafeCollector.common.kt */
/* loaded from: classes2.dex */
public final class SafeCollector_commonKt {
    public static final void a(SafeCollector<?> safeCollector, CoroutineContext coroutineContext) {
        if (((Number) coroutineContext.fold(0, new SafeCollector_commonKt$checkContext$result$1(safeCollector))).intValue() == safeCollector.collectContextSize) {
            return;
        }
        throw new IllegalStateException(("Flow invariant is violated:\n\t\tFlow was collected in " + safeCollector.collectContext + ",\n\t\tbut emission happened in " + coroutineContext + ".\n\t\tPlease refer to 'flow' documentation or use 'flowOn' instead").toString());
    }

    public static final st1 b(st1 st1Var, st1 st1Var2) {
        while (st1Var != null) {
            if (st1Var == st1Var2 || !(st1Var instanceof vd3)) {
                return st1Var;
            }
            st1Var = ((vd3) st1Var).L0();
        }
        return null;
    }
}
