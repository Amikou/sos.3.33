package kotlinx.coroutines.flow.internal;

import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: ChannelFlow.kt */
/* loaded from: classes2.dex */
public abstract class ChannelFlow<T> implements be1<T> {
    public final CoroutineContext a;
    public final int f0;
    public final BufferOverflow g0;

    public ChannelFlow(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        this.a = coroutineContext;
        this.f0 = i;
        this.g0 = bufferOverflow;
        if (ze0.a()) {
            if (!(i != -1)) {
                throw new AssertionError();
            }
        }
    }

    public static /* synthetic */ Object f(ChannelFlow channelFlow, k71 k71Var, q70 q70Var) {
        Object b = d90.b(new ChannelFlow$collect$2(k71Var, channelFlow, null), q70Var);
        return b == gs1.d() ? b : te4.a;
    }

    @Override // defpackage.j71
    public Object a(k71<? super T> k71Var, q70<? super te4> q70Var) {
        return f(this, k71Var, q70Var);
    }

    @Override // defpackage.be1
    public j71<T> c(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        if (ze0.a()) {
            if (!(i != -1)) {
                throw new AssertionError();
            }
        }
        CoroutineContext plus = coroutineContext.plus(this.a);
        if (bufferOverflow == BufferOverflow.SUSPEND) {
            int i2 = this.f0;
            if (i2 != -3) {
                if (i != -3) {
                    if (i2 != -2) {
                        if (i != -2) {
                            if (ze0.a()) {
                                if (!(this.f0 >= 0)) {
                                    throw new AssertionError();
                                }
                            }
                            if (ze0.a()) {
                                if (!(i >= 0)) {
                                    throw new AssertionError();
                                }
                            }
                            i2 = this.f0 + i;
                            if (i2 < 0) {
                                i = Integer.MAX_VALUE;
                            }
                        }
                    }
                }
                i = i2;
            }
            bufferOverflow = this.g0;
        }
        return (fs1.b(plus, this.a) && i == this.f0 && bufferOverflow == this.g0) ? this : h(plus, i, bufferOverflow);
    }

    public String e() {
        return null;
    }

    public abstract Object g(kv2<? super T> kv2Var, q70<? super te4> q70Var);

    public abstract ChannelFlow<T> h(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow);

    public final hd1<kv2<? super T>, q70<? super te4>, Object> i() {
        return new ChannelFlow$collectToFun$1(this, null);
    }

    public final int j() {
        int i = this.f0;
        if (i == -3) {
            return -2;
        }
        return i;
    }

    public f43<T> k(c90 c90Var) {
        return cv2.b(c90Var, this.a, j(), this.g0, CoroutineStart.ATOMIC, null, i(), 16, null);
    }

    public String toString() {
        ArrayList arrayList = new ArrayList(4);
        String e = e();
        if (e != null) {
            arrayList.add(e);
        }
        CoroutineContext coroutineContext = this.a;
        if (coroutineContext != EmptyCoroutineContext.INSTANCE) {
            arrayList.add(fs1.l("context=", coroutineContext));
        }
        int i = this.f0;
        if (i != -3) {
            arrayList.add(fs1.l("capacity=", Integer.valueOf(i)));
        }
        BufferOverflow bufferOverflow = this.g0;
        if (bufferOverflow != BufferOverflow.SUSPEND) {
            arrayList.add(fs1.l("onBufferOverflow=", bufferOverflow));
        }
        return ff0.a(this) + '[' + j20.S(arrayList, ", ", null, null, 0, null, null, 62, null) + ']';
    }
}
