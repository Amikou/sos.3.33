package kotlinx.coroutines.flow.internal;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.text.StringsKt__IndentKt;

/* compiled from: SafeCollector.kt */
/* loaded from: classes2.dex */
public final class SafeCollector<T> extends ContinuationImpl implements k71<T> {
    public final CoroutineContext collectContext;
    public final int collectContextSize;
    public final k71<T> collector;
    private q70<? super te4> completion;
    private CoroutineContext lastEmissionContext;

    /* JADX WARN: Multi-variable type inference failed */
    public SafeCollector(k71<? super T> k71Var, CoroutineContext coroutineContext) {
        super(ng2.a, EmptyCoroutineContext.INSTANCE);
        this.collector = k71Var;
        this.collectContext = coroutineContext;
        this.collectContextSize = ((Number) coroutineContext.fold(0, SafeCollector$collectContextSize$1.INSTANCE)).intValue();
    }

    public final void a(CoroutineContext coroutineContext, CoroutineContext coroutineContext2, T t) {
        if (coroutineContext2 instanceof nq0) {
            g((nq0) coroutineContext2, t);
        }
        SafeCollector_commonKt.a(this, coroutineContext);
        this.lastEmissionContext = coroutineContext;
    }

    public final Object e(q70<? super te4> q70Var, T t) {
        CoroutineContext context = q70Var.getContext();
        xt1.j(context);
        CoroutineContext coroutineContext = this.lastEmissionContext;
        if (coroutineContext != context) {
            a(context, coroutineContext, t);
        }
        this.completion = q70Var;
        return SafeCollectorKt.a().invoke(this.collector, t, this);
    }

    @Override // defpackage.k71
    public Object emit(T t, q70<? super te4> q70Var) {
        try {
            Object e = e(q70Var, t);
            if (e == gs1.d()) {
                ef0.c(q70Var);
            }
            return e == gs1.d() ? e : te4.a;
        } catch (Throwable th) {
            this.lastEmissionContext = new nq0(th);
            throw th;
        }
    }

    public final void g(nq0 nq0Var, Object obj) {
        throw new IllegalStateException(StringsKt__IndentKt.f("\n            Flow exception transparency is violated:\n                Previous 'emit' call has thrown exception " + nq0Var.a + ", but then emission attempt of value '" + obj + "' has been detected.\n                Emissions from 'catch' blocks are prohibited in order to avoid unspecified behaviour, 'Flow.catch' operator can be used instead.\n                For a more detailed explanation, please refer to Flow documentation.\n            ").toString());
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl, defpackage.e90
    public e90 getCallerFrame() {
        q70<? super te4> q70Var = this.completion;
        if (q70Var instanceof e90) {
            return (e90) q70Var;
        }
        return null;
    }

    @Override // kotlin.coroutines.jvm.internal.ContinuationImpl, kotlin.coroutines.jvm.internal.BaseContinuationImpl, defpackage.q70
    public CoroutineContext getContext() {
        q70<? super te4> q70Var = this.completion;
        CoroutineContext context = q70Var == null ? null : q70Var.getContext();
        return context == null ? EmptyCoroutineContext.INSTANCE : context;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl, defpackage.e90
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public Object invokeSuspend(Object obj) {
        Throwable m55exceptionOrNullimpl = Result.m55exceptionOrNullimpl(obj);
        if (m55exceptionOrNullimpl != null) {
            this.lastEmissionContext = new nq0(m55exceptionOrNullimpl);
        }
        q70<? super te4> q70Var = this.completion;
        if (q70Var != null) {
            q70Var.resumeWith(obj);
        }
        return gs1.d();
    }

    @Override // kotlin.coroutines.jvm.internal.ContinuationImpl, kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public void releaseIntercepted() {
        super.releaseIntercepted();
    }
}
