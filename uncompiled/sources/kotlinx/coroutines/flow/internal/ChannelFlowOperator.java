package kotlinx.coroutines.flow.internal;

import defpackage.r70;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: ChannelFlow.kt */
/* loaded from: classes2.dex */
public abstract class ChannelFlowOperator<S, T> extends ChannelFlow<T> {
    public final j71<S> h0;

    /* JADX WARN: Multi-variable type inference failed */
    public ChannelFlowOperator(j71<? extends S> j71Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        super(coroutineContext, i, bufferOverflow);
        this.h0 = j71Var;
    }

    public static /* synthetic */ Object l(ChannelFlowOperator channelFlowOperator, k71 k71Var, q70 q70Var) {
        if (channelFlowOperator.f0 == -3) {
            CoroutineContext context = q70Var.getContext();
            CoroutineContext plus = context.plus(channelFlowOperator.a);
            if (fs1.b(plus, context)) {
                Object o = channelFlowOperator.o(k71Var, q70Var);
                return o == gs1.d() ? o : te4.a;
            }
            r70.b bVar = r70.d;
            if (fs1.b(plus.get(bVar), context.get(bVar))) {
                Object n = channelFlowOperator.n(k71Var, plus, q70Var);
                return n == gs1.d() ? n : te4.a;
            }
        }
        Object a = super.a(k71Var, q70Var);
        return a == gs1.d() ? a : te4.a;
    }

    public static /* synthetic */ Object m(ChannelFlowOperator channelFlowOperator, kv2 kv2Var, q70 q70Var) {
        Object o = channelFlowOperator.o(new qk3(kv2Var), q70Var);
        return o == gs1.d() ? o : te4.a;
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow, defpackage.j71
    public Object a(k71<? super T> k71Var, q70<? super te4> q70Var) {
        return l(this, k71Var, q70Var);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public Object g(kv2<? super T> kv2Var, q70<? super te4> q70Var) {
        return m(this, kv2Var, q70Var);
    }

    public final Object n(k71<? super T> k71Var, CoroutineContext coroutineContext, q70<? super te4> q70Var) {
        Object c = ox.c(coroutineContext, ox.a(k71Var, q70Var.getContext()), null, new ChannelFlowOperator$collectWithContextUndispatched$2(this, null), q70Var, 4, null);
        return c == gs1.d() ? c : te4.a;
    }

    public abstract Object o(k71<? super T> k71Var, q70<? super te4> q70Var);

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public String toString() {
        return this.h0 + " -> " + super.toString();
    }
}
