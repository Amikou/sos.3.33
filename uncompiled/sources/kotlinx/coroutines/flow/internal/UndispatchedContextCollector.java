package kotlinx.coroutines.flow.internal;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: ChannelFlow.kt */
/* loaded from: classes2.dex */
public final class UndispatchedContextCollector<T> implements k71<T> {
    public final CoroutineContext a;
    public final Object f0;
    public final hd1<T, q70<? super te4>, Object> g0;

    public UndispatchedContextCollector(k71<? super T> k71Var, CoroutineContext coroutineContext) {
        this.a = coroutineContext;
        this.f0 = ThreadContextKt.b(coroutineContext);
        this.g0 = new UndispatchedContextCollector$emitRef$1(k71Var, null);
    }

    @Override // defpackage.k71
    public Object emit(T t, q70<? super te4> q70Var) {
        Object b = ox.b(this.a, t, this.f0, this.g0, q70Var);
        return b == gs1.d() ? b : te4.a;
    }
}
