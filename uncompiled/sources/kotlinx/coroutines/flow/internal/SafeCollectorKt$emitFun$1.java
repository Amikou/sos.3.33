package kotlinx.coroutines.flow.internal;

import kotlin.jvm.internal.FunctionReferenceImpl;

/* compiled from: SafeCollector.kt */
/* loaded from: classes2.dex */
public /* synthetic */ class SafeCollectorKt$emitFun$1 extends FunctionReferenceImpl implements kd1<k71<? super Object>, Object, te4> {
    public static final SafeCollectorKt$emitFun$1 INSTANCE = new SafeCollectorKt$emitFun$1();

    public SafeCollectorKt$emitFun$1() {
        super(3, k71.class, "emit", "emit(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", 0);
    }

    @Override // defpackage.kd1
    public final Object invoke(k71<Object> k71Var, Object obj, q70<? super te4> q70Var) {
        return k71Var.emit(obj, q70Var);
    }
}
