package kotlinx.coroutines.flow.internal;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: Merge.kt */
/* loaded from: classes2.dex */
public final class ChannelFlowTransformLatest<T, R> extends ChannelFlowOperator<T, R> {
    public final kd1<k71<? super R>, T, q70<? super te4>, Object> i0;

    public /* synthetic */ ChannelFlowTransformLatest(kd1 kd1Var, j71 j71Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow, int i2, qi0 qi0Var) {
        this(kd1Var, j71Var, (i2 & 4) != 0 ? EmptyCoroutineContext.INSTANCE : coroutineContext, (i2 & 8) != 0 ? -2 : i, (i2 & 16) != 0 ? BufferOverflow.SUSPEND : bufferOverflow);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public ChannelFlow<R> h(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        return new ChannelFlowTransformLatest(this.i0, this.h0, coroutineContext, i, bufferOverflow);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlowOperator
    public Object o(k71<? super R> k71Var, q70<? super te4> q70Var) {
        if (!ze0.a() || hr.a(k71Var instanceof qk3).booleanValue()) {
            Object a = m71.a(new ChannelFlowTransformLatest$flowCollect$3(this, k71Var, null), q70Var);
            return a == gs1.d() ? a : te4.a;
        }
        throw new AssertionError();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ChannelFlowTransformLatest(kd1<? super k71<? super R>, ? super T, ? super q70<? super te4>, ? extends Object> kd1Var, j71<? extends T> j71Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        super(j71Var, coroutineContext, i, bufferOverflow);
        this.i0 = kd1Var;
    }
}
