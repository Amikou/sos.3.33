package kotlinx.coroutines.flow.internal;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ChannelFlow.kt */
@a(c = "kotlinx.coroutines.flow.internal.UndispatchedContextCollector$emitRef$1", f = "ChannelFlow.kt", l = {212}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class UndispatchedContextCollector$emitRef$1 extends SuspendLambda implements hd1<T, q70<? super te4>, Object> {
    public final /* synthetic */ k71<T> $downstream;
    public /* synthetic */ Object L$0;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public UndispatchedContextCollector$emitRef$1(k71<? super T> k71Var, q70<? super UndispatchedContextCollector$emitRef$1> q70Var) {
        super(2, q70Var);
        this.$downstream = k71Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        UndispatchedContextCollector$emitRef$1 undispatchedContextCollector$emitRef$1 = new UndispatchedContextCollector$emitRef$1(this.$downstream, q70Var);
        undispatchedContextCollector$emitRef$1.L$0 = obj;
        return undispatchedContextCollector$emitRef$1;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ Object invoke(Object obj, q70<? super te4> q70Var) {
        return invoke2((UndispatchedContextCollector$emitRef$1) obj, q70Var);
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Object invoke2(T t, q70<? super te4> q70Var) {
        return ((UndispatchedContextCollector$emitRef$1) create(t, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            Object obj2 = this.L$0;
            k71<T> k71Var = this.$downstream;
            this.label = 1;
            if (k71Var.emit(obj2, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
