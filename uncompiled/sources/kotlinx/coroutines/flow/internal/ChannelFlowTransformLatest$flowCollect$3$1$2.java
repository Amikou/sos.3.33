package kotlinx.coroutines.flow.internal;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Merge.kt */
@a(c = "kotlinx.coroutines.flow.internal.ChannelFlowTransformLatest$flowCollect$3$1$2", f = "Merge.kt", l = {34}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ChannelFlowTransformLatest$flowCollect$3$1$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ k71<R> $collector;
    public final /* synthetic */ T $value;
    public int label;
    public final /* synthetic */ ChannelFlowTransformLatest<T, R> this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public ChannelFlowTransformLatest$flowCollect$3$1$2(ChannelFlowTransformLatest<T, R> channelFlowTransformLatest, k71<? super R> k71Var, T t, q70<? super ChannelFlowTransformLatest$flowCollect$3$1$2> q70Var) {
        super(2, q70Var);
        this.this$0 = channelFlowTransformLatest;
        this.$collector = k71Var;
        this.$value = t;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ChannelFlowTransformLatest$flowCollect$3$1$2(this.this$0, this.$collector, this.$value, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ChannelFlowTransformLatest$flowCollect$3$1$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        kd1 kd1Var;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            kd1Var = this.this$0.i0;
            Object obj2 = this.$collector;
            T t = this.$value;
            this.label = 1;
            if (kd1Var.invoke(obj2, t, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
