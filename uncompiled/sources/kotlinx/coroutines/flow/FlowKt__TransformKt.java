package kotlinx.coroutines.flow;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$IntRef;

/* compiled from: Transform.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class FlowKt__TransformKt {

    /* compiled from: SafeCollector.common.kt */
    /* loaded from: classes2.dex */
    public static final class a implements j71<lq1<? extends T>> {
        public final /* synthetic */ j71 a;

        public a(j71 j71Var) {
            this.a = j71Var;
        }

        @Override // defpackage.j71
        public Object a(k71<? super lq1<? extends T>> k71Var, q70<? super te4> q70Var) {
            Object a = this.a.a(new b(k71Var, new Ref$IntRef()), q70Var);
            return a == gs1.d() ? a : te4.a;
        }
    }

    /* compiled from: Collect.kt */
    /* loaded from: classes2.dex */
    public static final class b implements k71<T> {
        public final /* synthetic */ k71 a;
        public final /* synthetic */ Ref$IntRef f0;

        public b(k71 k71Var, Ref$IntRef ref$IntRef) {
            this.a = k71Var;
            this.f0 = ref$IntRef;
        }

        @Override // defpackage.k71
        public Object emit(T t, q70<? super te4> q70Var) {
            k71 k71Var = this.a;
            Ref$IntRef ref$IntRef = this.f0;
            int i = ref$IntRef.element;
            ref$IntRef.element = i + 1;
            if (i >= 0) {
                Object emit = k71Var.emit(new lq1(i, t), q70Var);
                return emit == gs1.d() ? emit : te4.a;
            }
            throw new ArithmeticException("Index overflow has happened");
        }
    }

    public static final <T> j71<T> a(final j71<? extends T> j71Var) {
        return new j71<T>() { // from class: kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1

            /* compiled from: Collect.kt */
            /* renamed from: kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 implements k71<T> {
                public final /* synthetic */ k71 a;

                @a(c = "kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1$2", f = "Transform.kt", l = {136}, m = "emit")
                /* renamed from: kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1$2$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends ContinuationImpl {
                    public int label;
                    public /* synthetic */ Object result;

                    public AnonymousClass1(q70 q70Var) {
                        super(q70Var);
                    }

                    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                    public final Object invokeSuspend(Object obj) {
                        this.result = obj;
                        this.label |= Integer.MIN_VALUE;
                        return AnonymousClass2.this.emit(null, this);
                    }
                }

                public AnonymousClass2(k71 k71Var) {
                    this.a = k71Var;
                }

                /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
                /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
                @Override // defpackage.k71
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public java.lang.Object emit(java.lang.Object r5, defpackage.q70 r6) {
                    /*
                        r4 = this;
                        boolean r0 = r6 instanceof kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1.AnonymousClass2.AnonymousClass1
                        if (r0 == 0) goto L13
                        r0 = r6
                        kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1$2$1 r0 = (kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1.AnonymousClass2.AnonymousClass1) r0
                        int r1 = r0.label
                        r2 = -2147483648(0xffffffff80000000, float:-0.0)
                        r3 = r1 & r2
                        if (r3 == 0) goto L13
                        int r1 = r1 - r2
                        r0.label = r1
                        goto L18
                    L13:
                        kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1$2$1 r0 = new kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1$2$1
                        r0.<init>(r6)
                    L18:
                        java.lang.Object r6 = r0.result
                        java.lang.Object r1 = defpackage.gs1.d()
                        int r2 = r0.label
                        r3 = 1
                        if (r2 == 0) goto L31
                        if (r2 != r3) goto L29
                        defpackage.o83.b(r6)
                        goto L41
                    L29:
                        java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                        java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
                        r5.<init>(r6)
                        throw r5
                    L31:
                        defpackage.o83.b(r6)
                        k71 r6 = r4.a
                        if (r5 == 0) goto L41
                        r0.label = r3
                        java.lang.Object r5 = r6.emit(r5, r0)
                        if (r5 != r1) goto L41
                        return r1
                    L41:
                        te4 r5 = defpackage.te4.a
                        return r5
                    */
                    throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__TransformKt$filterNotNull$$inlined$unsafeTransform$1.AnonymousClass2.emit(java.lang.Object, q70):java.lang.Object");
                }
            }

            @Override // defpackage.j71
            public Object a(k71 k71Var, q70 q70Var) {
                Object a2 = j71.this.a(new AnonymousClass2(k71Var), q70Var);
                return a2 == gs1.d() ? a2 : te4.a;
            }
        };
    }

    public static final <T> j71<T> b(final j71<? extends T> j71Var, final hd1<? super T, ? super q70<? super te4>, ? extends Object> hd1Var) {
        return new j71<T>() { // from class: kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1

            /* compiled from: Collect.kt */
            /* renamed from: kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 implements k71<T> {
                public final /* synthetic */ k71 a;
                public final /* synthetic */ hd1 f0;

                @a(c = "kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1$2", f = "Transform.kt", l = {136, 137}, m = "emit")
                /* renamed from: kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1$2$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends ContinuationImpl {
                    public Object L$0;
                    public Object L$1;
                    public int label;
                    public /* synthetic */ Object result;

                    public AnonymousClass1(q70 q70Var) {
                        super(q70Var);
                    }

                    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                    public final Object invokeSuspend(Object obj) {
                        this.result = obj;
                        this.label |= Integer.MIN_VALUE;
                        return AnonymousClass2.this.emit(null, this);
                    }
                }

                public AnonymousClass2(k71 k71Var, hd1 hd1Var) {
                    this.a = k71Var;
                    this.f0 = hd1Var;
                }

                /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
                /* JADX WARN: Removed duplicated region for block: B:16:0x003e  */
                /* JADX WARN: Removed duplicated region for block: B:22:0x0069 A[RETURN] */
                @Override // defpackage.k71
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public java.lang.Object emit(java.lang.Object r6, defpackage.q70 r7) {
                    /*
                        r5 = this;
                        boolean r0 = r7 instanceof kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1.AnonymousClass2.AnonymousClass1
                        if (r0 == 0) goto L13
                        r0 = r7
                        kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1$2$1 r0 = (kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1.AnonymousClass2.AnonymousClass1) r0
                        int r1 = r0.label
                        r2 = -2147483648(0xffffffff80000000, float:-0.0)
                        r3 = r1 & r2
                        if (r3 == 0) goto L13
                        int r1 = r1 - r2
                        r0.label = r1
                        goto L18
                    L13:
                        kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1$2$1 r0 = new kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1$2$1
                        r0.<init>(r7)
                    L18:
                        java.lang.Object r7 = r0.result
                        java.lang.Object r1 = defpackage.gs1.d()
                        int r2 = r0.label
                        r3 = 2
                        r4 = 1
                        if (r2 == 0) goto L3e
                        if (r2 == r4) goto L34
                        if (r2 != r3) goto L2c
                        defpackage.o83.b(r7)
                        goto L6a
                    L2c:
                        java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
                        java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
                        r6.<init>(r7)
                        throw r6
                    L34:
                        java.lang.Object r6 = r0.L$1
                        k71 r6 = (defpackage.k71) r6
                        java.lang.Object r2 = r0.L$0
                        defpackage.o83.b(r7)
                        goto L5c
                    L3e:
                        defpackage.o83.b(r7)
                        k71 r7 = r5.a
                        hd1 r2 = r5.f0
                        r0.L$0 = r6
                        r0.L$1 = r7
                        r0.label = r4
                        r4 = 6
                        defpackage.uq1.c(r4)
                        java.lang.Object r2 = r2.invoke(r6, r0)
                        r4 = 7
                        defpackage.uq1.c(r4)
                        if (r2 != r1) goto L5a
                        return r1
                    L5a:
                        r2 = r6
                        r6 = r7
                    L5c:
                        r7 = 0
                        r0.L$0 = r7
                        r0.L$1 = r7
                        r0.label = r3
                        java.lang.Object r6 = r6.emit(r2, r0)
                        if (r6 != r1) goto L6a
                        return r1
                    L6a:
                        te4 r6 = defpackage.te4.a
                        return r6
                    */
                    throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__TransformKt$onEach$$inlined$unsafeTransform$1.AnonymousClass2.emit(java.lang.Object, q70):java.lang.Object");
                }
            }

            @Override // defpackage.j71
            public Object a(k71 k71Var, q70 q70Var) {
                Object a2 = j71.this.a(new AnonymousClass2(k71Var, hd1Var), q70Var);
                return a2 == gs1.d() ? a2 : te4.a;
            }
        };
    }

    public static final <T> j71<lq1<T>> c(j71<? extends T> j71Var) {
        return new a(j71Var);
    }
}
