package kotlinx.coroutines.flow;

/* compiled from: Share.kt */
/* loaded from: classes2.dex */
public final class SubscribedFlowCollector<T> implements k71<T> {
    public final k71<T> a;
    public final hd1<k71<? super T>, q70<? super te4>, Object> f0;

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x006a  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x007d  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object a(defpackage.q70<? super defpackage.te4> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof kotlinx.coroutines.flow.SubscribedFlowCollector$onSubscription$1
            if (r0 == 0) goto L13
            r0 = r7
            kotlinx.coroutines.flow.SubscribedFlowCollector$onSubscription$1 r0 = (kotlinx.coroutines.flow.SubscribedFlowCollector$onSubscription$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            kotlinx.coroutines.flow.SubscribedFlowCollector$onSubscription$1 r0 = new kotlinx.coroutines.flow.SubscribedFlowCollector$onSubscription$1
            r0.<init>(r6, r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L42
            if (r2 == r4) goto L34
            if (r2 != r3) goto L2c
            defpackage.o83.b(r7)
            goto L7a
        L2c:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L34:
            java.lang.Object r2 = r0.L$1
            kotlinx.coroutines.flow.internal.SafeCollector r2 = (kotlinx.coroutines.flow.internal.SafeCollector) r2
            java.lang.Object r4 = r0.L$0
            kotlinx.coroutines.flow.SubscribedFlowCollector r4 = (kotlinx.coroutines.flow.SubscribedFlowCollector) r4
            defpackage.o83.b(r7)     // Catch: java.lang.Throwable -> L40
            goto L61
        L40:
            r7 = move-exception
            goto L82
        L42:
            defpackage.o83.b(r7)
            k71<T> r7 = r6.a
            kotlin.coroutines.CoroutineContext r2 = r0.getContext()
            kotlinx.coroutines.flow.internal.SafeCollector r5 = new kotlinx.coroutines.flow.internal.SafeCollector
            r5.<init>(r7, r2)
            hd1<k71<? super T>, q70<? super te4>, java.lang.Object> r7 = r6.f0     // Catch: java.lang.Throwable -> L80
            r0.L$0 = r6     // Catch: java.lang.Throwable -> L80
            r0.L$1 = r5     // Catch: java.lang.Throwable -> L80
            r0.label = r4     // Catch: java.lang.Throwable -> L80
            java.lang.Object r7 = r7.invoke(r5, r0)     // Catch: java.lang.Throwable -> L80
            if (r7 != r1) goto L5f
            return r1
        L5f:
            r4 = r6
            r2 = r5
        L61:
            r2.releaseIntercepted()
            k71<T> r7 = r4.a
            boolean r2 = r7 instanceof kotlinx.coroutines.flow.SubscribedFlowCollector
            if (r2 == 0) goto L7d
            kotlinx.coroutines.flow.SubscribedFlowCollector r7 = (kotlinx.coroutines.flow.SubscribedFlowCollector) r7
            r2 = 0
            r0.L$0 = r2
            r0.L$1 = r2
            r0.label = r3
            java.lang.Object r7 = r7.a(r0)
            if (r7 != r1) goto L7a
            return r1
        L7a:
            te4 r7 = defpackage.te4.a
            return r7
        L7d:
            te4 r7 = defpackage.te4.a
            return r7
        L80:
            r7 = move-exception
            r2 = r5
        L82:
            r2.releaseIntercepted()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.SubscribedFlowCollector.a(q70):java.lang.Object");
    }

    @Override // defpackage.k71
    public Object emit(T t, q70<? super te4> q70Var) {
        return this.a.emit(t, q70Var);
    }
}
