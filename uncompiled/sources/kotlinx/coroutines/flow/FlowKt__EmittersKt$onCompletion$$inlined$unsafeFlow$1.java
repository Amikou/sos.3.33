package kotlinx.coroutines.flow;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SafeCollector.common.kt */
/* loaded from: classes2.dex */
public final class FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1 implements j71<T> {
    public final /* synthetic */ j71 a;
    public final /* synthetic */ kd1 f0;

    @a(c = "kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1", f = "Emitters.kt", l = {114, 121, 128}, m = "collect")
    /* renamed from: kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends ContinuationImpl {
        public Object L$0;
        public Object L$1;
        public int label;
        public /* synthetic */ Object result;

        public AnonymousClass1(q70 q70Var) {
            super(q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1.this.a(null, this);
        }
    }

    public FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1(j71 j71Var, kd1 kd1Var) {
        this.a = j71Var;
        this.f0 = kd1Var;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0054  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0086 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0087  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00a9 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    @Override // defpackage.j71
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object a(defpackage.k71<? super T> r8, defpackage.q70<? super defpackage.te4> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1.AnonymousClass1
            if (r0 == 0) goto L13
            r0 = r9
            kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1$1 r0 = (kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1.AnonymousClass1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1$1 r0 = new kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1$1
            r0.<init>(r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 1
            r6 = 0
            if (r2 == 0) goto L54
            if (r2 == r5) goto L46
            if (r2 == r4) goto L3e
            if (r2 != r3) goto L36
            java.lang.Object r8 = r0.L$0
            kotlinx.coroutines.flow.internal.SafeCollector r8 = (kotlinx.coroutines.flow.internal.SafeCollector) r8
            defpackage.o83.b(r9)     // Catch: java.lang.Throwable -> L34
            goto L88
        L34:
            r9 = move-exception
            goto L90
        L36:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L3e:
            java.lang.Object r8 = r0.L$0
            java.lang.Throwable r8 = (java.lang.Throwable) r8
            defpackage.o83.b(r9)
            goto Laa
        L46:
            java.lang.Object r8 = r0.L$1
            k71 r8 = (defpackage.k71) r8
            java.lang.Object r2 = r0.L$0
            kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1 r2 = (kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1) r2
            defpackage.o83.b(r9)     // Catch: java.lang.Throwable -> L52
            goto L67
        L52:
            r8 = move-exception
            goto L96
        L54:
            defpackage.o83.b(r9)
            j71 r9 = r7.a     // Catch: java.lang.Throwable -> L94
            r0.L$0 = r7     // Catch: java.lang.Throwable -> L94
            r0.L$1 = r8     // Catch: java.lang.Throwable -> L94
            r0.label = r5     // Catch: java.lang.Throwable -> L94
            java.lang.Object r9 = r9.a(r8, r0)     // Catch: java.lang.Throwable -> L94
            if (r9 != r1) goto L66
            return r1
        L66:
            r2 = r7
        L67:
            kotlin.coroutines.CoroutineContext r9 = r0.getContext()
            kotlinx.coroutines.flow.internal.SafeCollector r4 = new kotlinx.coroutines.flow.internal.SafeCollector
            r4.<init>(r8, r9)
            kd1 r8 = r2.f0     // Catch: java.lang.Throwable -> L8e
            r0.L$0 = r4     // Catch: java.lang.Throwable -> L8e
            r0.L$1 = r6     // Catch: java.lang.Throwable -> L8e
            r0.label = r3     // Catch: java.lang.Throwable -> L8e
            r9 = 6
            defpackage.uq1.c(r9)     // Catch: java.lang.Throwable -> L8e
            java.lang.Object r8 = r8.invoke(r4, r6, r0)     // Catch: java.lang.Throwable -> L8e
            r9 = 7
            defpackage.uq1.c(r9)     // Catch: java.lang.Throwable -> L8e
            if (r8 != r1) goto L87
            return r1
        L87:
            r8 = r4
        L88:
            r8.releaseIntercepted()
            te4 r8 = defpackage.te4.a
            return r8
        L8e:
            r9 = move-exception
            r8 = r4
        L90:
            r8.releaseIntercepted()
            throw r9
        L94:
            r8 = move-exception
            r2 = r7
        L96:
            o54 r9 = new o54
            r9.<init>(r8)
            kd1 r2 = r2.f0
            r0.L$0 = r8
            r0.L$1 = r6
            r0.label = r4
            java.lang.Object r9 = kotlinx.coroutines.flow.FlowKt__EmittersKt.a(r9, r2, r8, r0)
            if (r9 != r1) goto Laa
            return r1
        Laa:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1.a(k71, q70):java.lang.Object");
    }
}
