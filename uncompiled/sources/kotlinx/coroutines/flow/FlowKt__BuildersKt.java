package kotlinx.coroutines.flow;

/* compiled from: Builders.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class FlowKt__BuildersKt {

    /* compiled from: SafeCollector.common.kt */
    /* loaded from: classes2.dex */
    public static final class a implements j71<T> {
        public final /* synthetic */ Object a;

        public a(Object obj) {
            this.a = obj;
        }

        @Override // defpackage.j71
        public Object a(k71<? super T> k71Var, q70<? super te4> q70Var) {
            Object emit = k71Var.emit(this.a, q70Var);
            return emit == gs1.d() ? emit : te4.a;
        }
    }

    public static final <T> j71<T> a(hd1<? super k71<? super T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        return new ub3(hd1Var);
    }

    public static final <T> j71<T> b(T t) {
        return new a(t);
    }

    public static final <T> j71<T> c(T... tArr) {
        return new FlowKt__BuildersKt$flowOf$$inlined$unsafeFlow$1(tArr);
    }
}
