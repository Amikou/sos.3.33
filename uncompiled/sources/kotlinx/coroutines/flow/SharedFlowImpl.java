package kotlinx.coroutines.flow;

import kotlin.Result;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: SharedFlow.kt */
/* loaded from: classes2.dex */
public final class SharedFlowImpl<T> extends i5<wn3> implements ib2<T>, be1<T>, be1 {
    public final int i0;
    public final int j0;
    public final BufferOverflow k0;
    public Object[] l0;
    public long m0;
    public long n0;
    public int o0;
    public int p0;

    /* compiled from: SharedFlow.kt */
    /* loaded from: classes2.dex */
    public static final class a implements yp0 {
        public final SharedFlowImpl<?> a;
        public long f0;
        public final Object g0;
        public final q70<te4> h0;

        /* JADX WARN: Multi-variable type inference failed */
        public a(SharedFlowImpl<?> sharedFlowImpl, long j, Object obj, q70<? super te4> q70Var) {
            this.a = sharedFlowImpl;
            this.f0 = j;
            this.g0 = obj;
            this.h0 = q70Var;
        }

        @Override // defpackage.yp0
        public void a() {
            this.a.x(this);
        }
    }

    /* compiled from: SharedFlow.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[BufferOverflow.valuesCustom().length];
            iArr[BufferOverflow.SUSPEND.ordinal()] = 1;
            iArr[BufferOverflow.DROP_LATEST.ordinal()] = 2;
            iArr[BufferOverflow.DROP_OLDEST.ordinal()] = 3;
            a = iArr;
        }
    }

    public SharedFlowImpl(int i, int i2, BufferOverflow bufferOverflow) {
        this.i0 = i;
        this.j0 = i2;
        this.k0 = bufferOverflow;
    }

    @Override // defpackage.i5
    /* renamed from: A */
    public wn3 h() {
        return new wn3();
    }

    @Override // defpackage.i5
    /* renamed from: B */
    public wn3[] i(int i) {
        return new wn3[i];
    }

    public final void C() {
        Object[] objArr = this.l0;
        fs1.d(objArr);
        vn3.g(objArr, H(), null);
        this.o0--;
        long H = H() + 1;
        if (this.m0 < H) {
            this.m0 = H;
        }
        if (this.n0 < H) {
            z(H);
        }
        if (ze0.a()) {
            if (!(H() == H)) {
                throw new AssertionError();
            }
        }
    }

    public final Object D(T t, q70<? super te4> q70Var) {
        q70[] q70VarArr;
        a aVar;
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.A();
        q70[] q70VarArr2 = j5.a;
        synchronized (this) {
            if (N(t)) {
                te4 te4Var = te4.a;
                Result.a aVar2 = Result.Companion;
                pvVar.resumeWith(Result.m52constructorimpl(te4Var));
                q70VarArr = F(q70VarArr2);
                aVar = null;
            } else {
                a aVar3 = new a(this, L() + H(), t, pvVar);
                E(aVar3);
                this.p0++;
                if (this.j0 == 0) {
                    q70VarArr2 = F(q70VarArr2);
                }
                q70VarArr = q70VarArr2;
                aVar = aVar3;
            }
        }
        if (aVar != null) {
            rv.a(pvVar, aVar);
        }
        int i = 0;
        int length = q70VarArr.length;
        while (i < length) {
            q70 q70Var2 = q70VarArr[i];
            i++;
            if (q70Var2 != null) {
                te4 te4Var2 = te4.a;
                Result.a aVar4 = Result.Companion;
                q70Var2.resumeWith(Result.m52constructorimpl(te4Var2));
            }
        }
        Object x = pvVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x == gs1.d() ? x : te4.a;
    }

    public final void E(Object obj) {
        int L = L();
        Object[] objArr = this.l0;
        if (objArr == null) {
            objArr = M(null, 0, 2);
        } else if (L >= objArr.length) {
            objArr = M(objArr, L, objArr.length * 2);
        }
        vn3.g(objArr, H() + L, obj);
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0008, code lost:
        r1 = r10.a;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v6, types: [java.lang.Object[], java.lang.Object] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final kotlin.coroutines.Continuation<kotlin.Unit>[] F(kotlin.coroutines.Continuation<kotlin.Unit>[] r11) {
        /*
            r10 = this;
            int r0 = r11.length
            int r1 = defpackage.i5.e(r10)
            if (r1 != 0) goto L8
            goto L4a
        L8:
            k5[] r1 = defpackage.i5.f(r10)
            if (r1 != 0) goto Lf
            goto L4a
        Lf:
            int r2 = r1.length
            r3 = 0
        L11:
            if (r3 >= r2) goto L4a
            r4 = r1[r3]
            if (r4 == 0) goto L47
            wn3 r4 = (defpackage.wn3) r4
            q70<? super te4> r5 = r4.b
            if (r5 != 0) goto L1e
            goto L47
        L1e:
            long r6 = r10.P(r4)
            r8 = 0
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 >= 0) goto L29
            goto L47
        L29:
            int r6 = r11.length
            if (r0 < r6) goto L3c
            int r6 = r11.length
            r7 = 2
            int r6 = r6 * r7
            int r6 = java.lang.Math.max(r7, r6)
            java.lang.Object[] r11 = java.util.Arrays.copyOf(r11, r6)
            java.lang.String r6 = "java.util.Arrays.copyOf(this, newSize)"
            defpackage.fs1.e(r11, r6)
        L3c:
            r6 = r11
            q70[] r6 = (defpackage.q70[]) r6
            int r7 = r0 + 1
            r6[r0] = r5
            r0 = 0
            r4.b = r0
            r0 = r7
        L47:
            int r3 = r3 + 1
            goto L11
        L4a:
            q70[] r11 = (defpackage.q70[]) r11
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.SharedFlowImpl.F(q70[]):q70[]");
    }

    public final long G() {
        return H() + this.o0;
    }

    public final long H() {
        return Math.min(this.n0, this.m0);
    }

    public final Object I(long j) {
        Object f;
        Object[] objArr = this.l0;
        fs1.d(objArr);
        f = vn3.f(objArr, j);
        return f instanceof a ? ((a) f).g0 : f;
    }

    public final long J() {
        return H() + this.o0 + this.p0;
    }

    public final int K() {
        return (int) ((H() + this.o0) - this.m0);
    }

    public final int L() {
        return this.o0 + this.p0;
    }

    public final Object[] M(Object[] objArr, int i, int i2) {
        Object f;
        int i3 = 0;
        if (i2 > 0) {
            Object[] objArr2 = new Object[i2];
            this.l0 = objArr2;
            if (objArr == null) {
                return objArr2;
            }
            long H = H();
            if (i > 0) {
                while (true) {
                    int i4 = i3 + 1;
                    long j = i3 + H;
                    f = vn3.f(objArr, j);
                    vn3.g(objArr2, j, f);
                    if (i4 >= i) {
                        break;
                    }
                    i3 = i4;
                }
            }
            return objArr2;
        }
        throw new IllegalStateException("Buffer size overflow".toString());
    }

    public final boolean N(T t) {
        if (k() == 0) {
            return O(t);
        }
        if (this.o0 >= this.j0 && this.n0 <= this.m0) {
            int i = b.a[this.k0.ordinal()];
            if (i == 1) {
                return false;
            }
            if (i == 2) {
                return true;
            }
        }
        E(t);
        int i2 = this.o0 + 1;
        this.o0 = i2;
        if (i2 > this.j0) {
            C();
        }
        if (K() > this.i0) {
            R(this.m0 + 1, this.n0, G(), J());
        }
        return true;
    }

    public final boolean O(T t) {
        if (ze0.a()) {
            if (!(k() == 0)) {
                throw new AssertionError();
            }
        }
        if (this.i0 == 0) {
            return true;
        }
        E(t);
        int i = this.o0 + 1;
        this.o0 = i;
        if (i > this.i0) {
            C();
        }
        this.n0 = H() + this.o0;
        return true;
    }

    public final long P(wn3 wn3Var) {
        long j = wn3Var.a;
        if (j < G()) {
            return j;
        }
        if (this.j0 <= 0 && j <= H() && this.p0 != 0) {
            return j;
        }
        return -1L;
    }

    public final Object Q(wn3 wn3Var) {
        Object obj;
        q70[] q70VarArr = j5.a;
        synchronized (this) {
            long P = P(wn3Var);
            if (P < 0) {
                obj = vn3.a;
            } else {
                long j = wn3Var.a;
                Object I = I(P);
                wn3Var.a = P + 1;
                q70VarArr = S(j);
                obj = I;
            }
        }
        int i = 0;
        int length = q70VarArr.length;
        while (i < length) {
            q70 q70Var = q70VarArr[i];
            i++;
            if (q70Var != null) {
                te4 te4Var = te4.a;
                Result.a aVar = Result.Companion;
                q70Var.resumeWith(Result.m52constructorimpl(te4Var));
            }
        }
        return obj;
    }

    public final void R(long j, long j2, long j3, long j4) {
        long min = Math.min(j2, j);
        if (ze0.a()) {
            if (!(min >= H())) {
                throw new AssertionError();
            }
        }
        long H = H();
        if (H < min) {
            while (true) {
                long j5 = 1 + H;
                Object[] objArr = this.l0;
                fs1.d(objArr);
                vn3.g(objArr, H, null);
                if (j5 >= min) {
                    break;
                }
                H = j5;
            }
        }
        this.m0 = j;
        this.n0 = j2;
        this.o0 = (int) (j3 - min);
        this.p0 = (int) (j4 - j3);
        if (ze0.a()) {
            if (!(this.o0 >= 0)) {
                throw new AssertionError();
            }
        }
        if (ze0.a()) {
            if (!(this.p0 >= 0)) {
                throw new AssertionError();
            }
        }
        if (ze0.a()) {
            if (!(this.m0 <= H() + ((long) this.o0))) {
                throw new AssertionError();
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x003d, code lost:
        r4 = r22.a;
     */
    /* JADX WARN: Removed duplicated region for block: B:73:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x00f3  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x011d  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x012f  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x0131  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x0136  */
    /* JADX WARN: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final kotlin.coroutines.Continuation<kotlin.Unit>[] S(long r23) {
        /*
            Method dump skipped, instructions count: 315
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.SharedFlowImpl.S(long):q70[]");
    }

    public final long T() {
        long j = this.m0;
        if (j < this.n0) {
            this.n0 = j;
        }
        return j;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00bf A[Catch: all -> 0x006f, TRY_LEAVE, TryCatch #1 {all -> 0x006f, blocks: (B:14:0x003b, B:35:0x00a0, B:40:0x00ae, B:39:0x00ab, B:43:0x00bf, B:19:0x0059, B:22:0x006b, B:33:0x0092), top: B:52:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00a8 A[SYNTHETIC] */
    /* JADX WARN: Type inference failed for: r2v0, types: [int] */
    /* JADX WARN: Type inference failed for: r2v1 */
    /* JADX WARN: Type inference failed for: r2v19 */
    /* JADX WARN: Type inference failed for: r2v2, types: [k5] */
    /* JADX WARN: Type inference failed for: r2v20 */
    /* JADX WARN: Type inference failed for: r2v6, types: [java.lang.Object, wn3] */
    /* JADX WARN: Type inference failed for: r6v1, types: [i5] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:41:0x00bc -> B:15:0x003e). Please submit an issue!!! */
    @Override // defpackage.j71
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object a(defpackage.k71<? super T> r9, defpackage.q70<? super defpackage.te4> r10) {
        /*
            Method dump skipped, instructions count: 214
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.SharedFlowImpl.a(k71, q70):java.lang.Object");
    }

    @Override // defpackage.be1
    public j71<T> c(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        return vn3.e(this, coroutineContext, i, bufferOverflow);
    }

    @Override // defpackage.ib2
    public boolean d(T t) {
        int i;
        boolean z;
        Continuation<Unit>[] continuationArr = j5.a;
        synchronized (this) {
            i = 0;
            if (N(t)) {
                continuationArr = F(continuationArr);
                z = true;
            } else {
                z = false;
            }
        }
        int length = continuationArr.length;
        while (i < length) {
            Continuation<Unit> continuation = continuationArr[i];
            i++;
            if (continuation != null) {
                te4 te4Var = te4.a;
                Result.a aVar = Result.Companion;
                continuation.resumeWith(Result.m52constructorimpl(te4Var));
            }
        }
        return z;
    }

    @Override // defpackage.k71
    public Object emit(T t, q70<? super te4> q70Var) {
        Object D;
        return (!d(t) && (D = D(t, q70Var)) == gs1.d()) ? D : te4.a;
    }

    public final Object w(wn3 wn3Var, q70<? super te4> q70Var) {
        te4 te4Var;
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.A();
        synchronized (this) {
            if (P(wn3Var) < 0) {
                wn3Var.b = pvVar;
                wn3Var.b = pvVar;
            } else {
                te4 te4Var2 = te4.a;
                Result.a aVar = Result.Companion;
                pvVar.resumeWith(Result.m52constructorimpl(te4Var2));
            }
            te4Var = te4.a;
        }
        Object x = pvVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x == gs1.d() ? x : te4Var;
    }

    public final void x(a aVar) {
        Object f;
        synchronized (this) {
            if (aVar.f0 < H()) {
                return;
            }
            Object[] objArr = this.l0;
            fs1.d(objArr);
            f = vn3.f(objArr, aVar.f0);
            if (f != aVar) {
                return;
            }
            vn3.g(objArr, aVar.f0, vn3.a);
            y();
            te4 te4Var = te4.a;
        }
    }

    public final void y() {
        Object f;
        if (this.j0 != 0 || this.p0 > 1) {
            Object[] objArr = this.l0;
            fs1.d(objArr);
            while (this.p0 > 0) {
                f = vn3.f(objArr, (H() + L()) - 1);
                if (f != vn3.a) {
                    return;
                }
                this.p0--;
                vn3.g(objArr, H() + L(), null);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0007, code lost:
        r0 = r8.a;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void z(long r9) {
        /*
            r8 = this;
            int r0 = defpackage.i5.e(r8)
            if (r0 != 0) goto L7
            goto L29
        L7:
            k5[] r0 = defpackage.i5.f(r8)
            if (r0 != 0) goto Le
            goto L29
        Le:
            int r1 = r0.length
            r2 = 0
        L10:
            if (r2 >= r1) goto L29
            r3 = r0[r2]
            if (r3 == 0) goto L26
            wn3 r3 = (defpackage.wn3) r3
            long r4 = r3.a
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 < 0) goto L26
            int r4 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r4 >= 0) goto L26
            r3.a = r9
        L26:
            int r2 = r2 + 1
            goto L10
        L29:
            r8.n0 = r9
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.SharedFlowImpl.z(long):void");
    }
}
