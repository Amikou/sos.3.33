package kotlinx.coroutines.flow;

import kotlin.jvm.internal.Ref$ObjectRef;

/* compiled from: Distinct.kt */
/* loaded from: classes2.dex */
public final class DistinctFlowImpl<T> implements j71<T> {
    public final j71<T> a;
    public final tc1<T, Object> f0;
    public final hd1<Object, Object, Boolean> g0;

    /* JADX WARN: Multi-variable type inference failed */
    public DistinctFlowImpl(j71<? extends T> j71Var, tc1<? super T, ? extends Object> tc1Var, hd1<Object, Object, Boolean> hd1Var) {
        this.a = j71Var;
        this.f0 = tc1Var;
        this.g0 = hd1Var;
    }

    @Override // defpackage.j71
    public Object a(k71<? super T> k71Var, q70<? super te4> q70Var) {
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = (T) xi2.a;
        Object a = this.a.a(new DistinctFlowImpl$collect$$inlined$collect$1(this, ref$ObjectRef, k71Var), q70Var);
        return a == gs1.d() ? a : te4.a;
    }
}
