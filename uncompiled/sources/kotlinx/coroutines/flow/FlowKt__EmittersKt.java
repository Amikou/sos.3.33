package kotlinx.coroutines.flow;

/* compiled from: Emitters.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class FlowKt__EmittersKt {
    public static final void b(k71<?> k71Var) {
        if (k71Var instanceof o54) {
            throw ((o54) k71Var).a;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0036  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final <T> java.lang.Object c(defpackage.k71<? super T> r4, defpackage.kd1<? super defpackage.k71<? super T>, ? super java.lang.Throwable, ? super defpackage.q70<? super defpackage.te4>, ? extends java.lang.Object> r5, java.lang.Throwable r6, defpackage.q70<? super defpackage.te4> r7) {
        /*
            boolean r0 = r7 instanceof kotlinx.coroutines.flow.FlowKt__EmittersKt$invokeSafely$1
            if (r0 == 0) goto L13
            r0 = r7
            kotlinx.coroutines.flow.FlowKt__EmittersKt$invokeSafely$1 r0 = (kotlinx.coroutines.flow.FlowKt__EmittersKt$invokeSafely$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            kotlinx.coroutines.flow.FlowKt__EmittersKt$invokeSafely$1 r0 = new kotlinx.coroutines.flow.FlowKt__EmittersKt$invokeSafely$1
            r0.<init>(r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L36
            if (r2 != r3) goto L2e
            java.lang.Object r4 = r0.L$0
            r6 = r4
            java.lang.Throwable r6 = (java.lang.Throwable) r6
            defpackage.o83.b(r7)     // Catch: java.lang.Throwable -> L47
            goto L44
        L2e:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "call to 'resume' before 'invoke' with coroutine"
            r4.<init>(r5)
            throw r4
        L36:
            defpackage.o83.b(r7)
            r0.L$0 = r6     // Catch: java.lang.Throwable -> L47
            r0.label = r3     // Catch: java.lang.Throwable -> L47
            java.lang.Object r4 = r5.invoke(r4, r6, r0)     // Catch: java.lang.Throwable -> L47
            if (r4 != r1) goto L44
            return r1
        L44:
            te4 r4 = defpackage.te4.a
            return r4
        L47:
            r4 = move-exception
            if (r6 == 0) goto L4f
            if (r6 == r4) goto L4f
            defpackage.py0.a(r4, r6)
        L4f:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__EmittersKt.c(k71, kd1, java.lang.Throwable, q70):java.lang.Object");
    }

    public static final <T> j71<T> d(j71<? extends T> j71Var, kd1<? super k71<? super T>, ? super Throwable, ? super q70<? super te4>, ? extends Object> kd1Var) {
        return new FlowKt__EmittersKt$onCompletion$$inlined$unsafeFlow$1(j71Var, kd1Var);
    }

    public static final <T> j71<T> e(j71<? extends T> j71Var, hd1<? super k71<? super T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        return new FlowKt__EmittersKt$onStart$$inlined$unsafeFlow$1(hd1Var, j71Var);
    }
}
