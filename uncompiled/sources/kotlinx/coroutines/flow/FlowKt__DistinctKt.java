package kotlinx.coroutines.flow;

/* compiled from: Distinct.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class FlowKt__DistinctKt {
    public static final tc1<Object, Object> a = FlowKt__DistinctKt$defaultKeySelector$1.INSTANCE;
    public static final hd1<Object, Object, Boolean> b = FlowKt__DistinctKt$defaultAreEquivalent$1.INSTANCE;

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> j71<T> a(j71<? extends T> j71Var) {
        return j71Var instanceof ws3 ? j71Var : b(j71Var, a, b);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> j71<T> b(j71<? extends T> j71Var, tc1<? super T, ? extends Object> tc1Var, hd1<Object, Object, Boolean> hd1Var) {
        if (j71Var instanceof DistinctFlowImpl) {
            DistinctFlowImpl distinctFlowImpl = (DistinctFlowImpl) j71Var;
            if (distinctFlowImpl.f0 == tc1Var && distinctFlowImpl.g0 == hd1Var) {
                return j71Var;
            }
        }
        return new DistinctFlowImpl(j71Var, tc1Var, hd1Var);
    }
}
