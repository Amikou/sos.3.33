package kotlinx.coroutines.flow;

import kotlinx.coroutines.flow.internal.ChannelFlowTransformLatest;

/* compiled from: Merge.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class FlowKt__MergeKt {
    static {
        a34.b("kotlinx.coroutines.flow.defaultConcurrency", 16, 1, Integer.MAX_VALUE);
    }

    public static final <T, R> j71<R> a(j71<? extends T> j71Var, hd1<? super T, ? super q70<? super R>, ? extends Object> hd1Var) {
        return n71.v(j71Var, new FlowKt__MergeKt$mapLatest$1(hd1Var, null));
    }

    public static final <T, R> j71<R> b(j71<? extends T> j71Var, kd1<? super k71<? super R>, ? super T, ? super q70<? super te4>, ? extends Object> kd1Var) {
        return new ChannelFlowTransformLatest(kd1Var, j71Var, null, 0, null, 28, null);
    }
}
