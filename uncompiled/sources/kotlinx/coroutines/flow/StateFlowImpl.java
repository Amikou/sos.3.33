package kotlinx.coroutines.flow;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: StateFlow.kt */
/* loaded from: classes2.dex */
public final class StateFlowImpl<T> extends i5<ys3> implements jb2<T>, be1<T>, be1 {
    private volatile /* synthetic */ Object _state;
    public int i0;

    public StateFlowImpl(Object obj) {
        this._state = obj;
    }

    /* JADX WARN: Code restructure failed: missing block: B:40:0x00b4, code lost:
        if (defpackage.fs1.b(r11, r12) == false) goto L26;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00ab A[Catch: all -> 0x0073, TryCatch #0 {all -> 0x0073, blocks: (B:14:0x003e, B:34:0x00a6, B:39:0x00b0, B:49:0x00d1, B:51:0x00d7, B:41:0x00b6, B:45:0x00bd, B:37:0x00ab, B:19:0x005c, B:22:0x006f, B:33:0x0097), top: B:58:0x0024 }] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00b0 A[Catch: all -> 0x0073, TryCatch #0 {all -> 0x0073, blocks: (B:14:0x003e, B:34:0x00a6, B:39:0x00b0, B:49:0x00d1, B:51:0x00d7, B:41:0x00b6, B:45:0x00bd, B:37:0x00ab, B:19:0x005c, B:22:0x006f, B:33:0x0097), top: B:58:0x0024 }] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00ba  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00bc  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00cf A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00d0  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00d7 A[Catch: all -> 0x0073, TRY_LEAVE, TryCatch #0 {all -> 0x0073, blocks: (B:14:0x003e, B:34:0x00a6, B:39:0x00b0, B:49:0x00d1, B:51:0x00d7, B:41:0x00b6, B:45:0x00bd, B:37:0x00ab, B:19:0x005c, B:22:0x006f, B:33:0x0097), top: B:58:0x0024 }] */
    /* JADX WARN: Type inference failed for: r6v0 */
    /* JADX WARN: Type inference failed for: r6v1 */
    /* JADX WARN: Type inference failed for: r6v14 */
    /* JADX WARN: Type inference failed for: r6v15 */
    /* JADX WARN: Type inference failed for: r6v16 */
    /* JADX WARN: Type inference failed for: r6v17 */
    /* JADX WARN: Type inference failed for: r6v2, types: [k5] */
    /* JADX WARN: Type inference failed for: r6v7, types: [java.lang.Object] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:50:0x00d5 -> B:34:0x00a6). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:52:0x00e7 -> B:34:0x00a6). Please submit an issue!!! */
    @Override // defpackage.j71
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object a(defpackage.k71<? super T> r11, defpackage.q70<? super defpackage.te4> r12) {
        /*
            Method dump skipped, instructions count: 241
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.StateFlowImpl.a(k71, q70):java.lang.Object");
    }

    @Override // defpackage.jb2
    public boolean b(T t, T t2) {
        if (t == null) {
            t = (T) xi2.a;
        }
        if (t2 == null) {
            t2 = (T) xi2.a;
        }
        return o(t, t2);
    }

    @Override // defpackage.be1
    public j71<T> c(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        return xs3.d(this, coroutineContext, i, bufferOverflow);
    }

    @Override // defpackage.ib2
    public boolean d(T t) {
        setValue(t);
        return true;
    }

    @Override // defpackage.k71
    public Object emit(T t, q70<? super te4> q70Var) {
        setValue(t);
        return te4.a;
    }

    @Override // defpackage.jb2, defpackage.ws3
    public T getValue() {
        k24 k24Var = xi2.a;
        T t = (T) this._state;
        if (t == k24Var) {
            return null;
        }
        return t;
    }

    @Override // defpackage.i5
    /* renamed from: m */
    public ys3 h() {
        return new ys3();
    }

    @Override // defpackage.i5
    /* renamed from: n */
    public ys3[] i(int i) {
        return new ys3[i];
    }

    public final boolean o(Object obj, Object obj2) {
        int i;
        ys3[] l;
        l();
        synchronized (this) {
            Object obj3 = this._state;
            if (obj != null && !fs1.b(obj3, obj)) {
                return false;
            }
            if (fs1.b(obj3, obj2)) {
                return true;
            }
            this._state = obj2;
            int i2 = this.i0;
            if ((i2 & 1) == 0) {
                int i3 = i2 + 1;
                this.i0 = i3;
                ys3[] l2 = l();
                te4 te4Var = te4.a;
                while (true) {
                    ys3[] ys3VarArr = l2;
                    if (ys3VarArr != null) {
                        for (ys3 ys3Var : ys3VarArr) {
                            if (ys3Var != null) {
                                ys3Var.f();
                            }
                        }
                    }
                    synchronized (this) {
                        i = this.i0;
                        if (i == i3) {
                            this.i0 = i3 + 1;
                            return true;
                        }
                        l = l();
                        te4 te4Var2 = te4.a;
                    }
                    l2 = l;
                    i3 = i;
                }
            } else {
                this.i0 = i2 + 2;
                return true;
            }
        }
    }

    @Override // defpackage.jb2
    public void setValue(T t) {
        if (t == null) {
            t = (T) xi2.a;
        }
        o(null, t);
    }
}
