package kotlinx.coroutines.flow;

/* compiled from: Collect.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class FlowKt__CollectKt {
    public static final Object a(j71<?> j71Var, q70<? super te4> q70Var) {
        Object a = j71Var.a(xg2.a, q70Var);
        return a == gs1.d() ? a : te4.a;
    }

    public static final <T> Object b(j71<? extends T> j71Var, hd1<? super T, ? super q70<? super te4>, ? extends Object> hd1Var, q70<? super te4> q70Var) {
        j71 b;
        b = o71.b(n71.r(j71Var, hd1Var), 0, null, 2, null);
        Object e = n71.e(b, q70Var);
        return e == gs1.d() ? e : te4.a;
    }

    public static final <T> st1 c(j71<? extends T> j71Var, c90 c90Var) {
        st1 b;
        b = as.b(c90Var, null, null, new FlowKt__CollectKt$launchIn$1(j71Var, null), 3, null);
        return b;
    }
}
