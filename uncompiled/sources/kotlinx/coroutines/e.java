package kotlinx.coroutines;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.CoroutineContext;

/* compiled from: EventLoop.common.kt */
/* loaded from: classes2.dex */
public abstract class e extends yx0 implements wl0 {
    public static final /* synthetic */ AtomicReferenceFieldUpdater i0 = AtomicReferenceFieldUpdater.newUpdater(e.class, Object.class, "_queue");
    public static final /* synthetic */ AtomicReferenceFieldUpdater j0 = AtomicReferenceFieldUpdater.newUpdater(e.class, Object.class, "_delayed");
    private volatile /* synthetic */ Object _queue = null;
    private volatile /* synthetic */ Object _delayed = null;
    private volatile /* synthetic */ int _isCompleted = 0;

    /* compiled from: EventLoop.common.kt */
    /* loaded from: classes2.dex */
    public final class a extends b {
        public final ov<te4> h0;

        /* JADX WARN: Multi-variable type inference failed */
        public a(long j, ov<? super te4> ovVar) {
            super(j);
            this.h0 = ovVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.h0.k(e.this, te4.a);
        }

        @Override // kotlinx.coroutines.e.b
        public String toString() {
            return fs1.l(super.toString(), this.h0);
        }
    }

    /* compiled from: EventLoop.common.kt */
    /* loaded from: classes2.dex */
    public static abstract class b implements Runnable, Comparable<b>, yp0, l54 {
        public long a;
        public Object f0;
        public int g0 = -1;

        public b(long j) {
            this.a = j;
        }

        @Override // defpackage.yp0
        public final synchronized void a() {
            k24 k24Var;
            k24 k24Var2;
            Object obj = this.f0;
            k24Var = ay0.a;
            if (obj == k24Var) {
                return;
            }
            c cVar = obj instanceof c ? (c) obj : null;
            if (cVar != null) {
                cVar.g(this);
            }
            k24Var2 = ay0.a;
            this.f0 = k24Var2;
        }

        @Override // defpackage.l54
        public k54<?> e() {
            Object obj = this.f0;
            if (obj instanceof k54) {
                return (k54) obj;
            }
            return null;
        }

        @Override // defpackage.l54
        public void f(int i) {
            this.g0 = i;
        }

        @Override // defpackage.l54
        public void g(k54<?> k54Var) {
            k24 k24Var;
            Object obj = this.f0;
            k24Var = ay0.a;
            if (obj != k24Var) {
                this.f0 = k54Var;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @Override // defpackage.l54
        public int getIndex() {
            return this.g0;
        }

        @Override // java.lang.Comparable
        /* renamed from: h */
        public int compareTo(b bVar) {
            int i = ((this.a - bVar.a) > 0L ? 1 : ((this.a - bVar.a) == 0L ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        public final synchronized int j(long j, c cVar, e eVar) {
            k24 k24Var;
            Object obj = this.f0;
            k24Var = ay0.a;
            if (obj == k24Var) {
                return 2;
            }
            synchronized (cVar) {
                b b = cVar.b();
                if (eVar.z0()) {
                    return 1;
                }
                if (b == null) {
                    cVar.b = j;
                } else {
                    long j2 = b.a;
                    if (j2 - j < 0) {
                        j = j2;
                    }
                    if (j - cVar.b > 0) {
                        cVar.b = j;
                    }
                }
                long j3 = this.a;
                long j4 = cVar.b;
                if (j3 - j4 < 0) {
                    this.a = j4;
                }
                cVar.a(this);
                return 0;
            }
        }

        public final boolean k(long j) {
            return j - this.a >= 0;
        }

        public String toString() {
            return "Delayed[nanos=" + this.a + ']';
        }
    }

    /* compiled from: EventLoop.common.kt */
    /* loaded from: classes2.dex */
    public static final class c extends k54<b> {
        public long b;

        public c(long j) {
            this.b = j;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    public final boolean z0() {
        return this._isCompleted;
    }

    public boolean B0() {
        k24 k24Var;
        if (X()) {
            c cVar = (c) this._delayed;
            if (cVar == null || cVar.d()) {
                Object obj = this._queue;
                if (obj != null) {
                    if (obj instanceof n12) {
                        return ((n12) obj).g();
                    }
                    k24Var = ay0.b;
                    if (obj != k24Var) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        return false;
    }

    public final void F0() {
        n5.a();
        long nanoTime = System.nanoTime();
        while (true) {
            c cVar = (c) this._delayed;
            b i = cVar == null ? null : cVar.i();
            if (i == null) {
                return;
            }
            g0(nanoTime, i);
        }
    }

    public final void H0() {
        this._queue = null;
        this._delayed = null;
    }

    public final void J0(long j, b bVar) {
        int K0 = K0(j, bVar);
        if (K0 == 0) {
            if (O0(bVar)) {
                i0();
            }
        } else if (K0 == 1) {
            g0(j, bVar);
        } else if (K0 != 2) {
            throw new IllegalStateException("unexpected result".toString());
        }
    }

    public final int K0(long j, b bVar) {
        if (z0()) {
            return 1;
        }
        c cVar = (c) this._delayed;
        if (cVar == null) {
            j0.compareAndSet(this, null, new c(j));
            cVar = (c) this._delayed;
            fs1.d(cVar);
        }
        return bVar.j(j, cVar, this);
    }

    public final void L0(boolean z) {
        this._isCompleted = z ? 1 : 0;
    }

    public final boolean O0(b bVar) {
        c cVar = (c) this._delayed;
        return (cVar == null ? null : cVar.e()) == bVar;
    }

    @Override // defpackage.xx0
    public long Q() {
        k24 k24Var;
        if (super.Q() == 0) {
            return 0L;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (!(obj instanceof n12)) {
                k24Var = ay0.b;
                return obj == k24Var ? Long.MAX_VALUE : 0L;
            } else if (!((n12) obj).g()) {
                return 0L;
            }
        }
        c cVar = (c) this._delayed;
        b e = cVar == null ? null : cVar.e();
        if (e == null) {
            return Long.MAX_VALUE;
        }
        long j = e.a;
        n5.a();
        return u33.c(j - System.nanoTime(), 0L);
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x004b  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x004f  */
    @Override // defpackage.xx0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long a0() {
        /*
            r9 = this;
            boolean r0 = r9.b0()
            r1 = 0
            if (r0 == 0) goto L9
            return r1
        L9:
            java.lang.Object r0 = r9._delayed
            kotlinx.coroutines.e$c r0 = (kotlinx.coroutines.e.c) r0
            if (r0 == 0) goto L45
            boolean r3 = r0.d()
            if (r3 != 0) goto L45
            defpackage.n5.a()
            long r3 = java.lang.System.nanoTime()
        L1c:
            monitor-enter(r0)
            l54 r5 = r0.b()     // Catch: java.lang.Throwable -> L42
            r6 = 0
            if (r5 != 0) goto L26
            monitor-exit(r0)
            goto L3d
        L26:
            kotlinx.coroutines.e$b r5 = (kotlinx.coroutines.e.b) r5     // Catch: java.lang.Throwable -> L42
            boolean r7 = r5.k(r3)     // Catch: java.lang.Throwable -> L42
            r8 = 0
            if (r7 == 0) goto L34
            boolean r5 = r9.y0(r5)     // Catch: java.lang.Throwable -> L42
            goto L35
        L34:
            r5 = r8
        L35:
            if (r5 == 0) goto L3c
            l54 r5 = r0.h(r8)     // Catch: java.lang.Throwable -> L42
            r6 = r5
        L3c:
            monitor-exit(r0)
        L3d:
            kotlinx.coroutines.e$b r6 = (kotlinx.coroutines.e.b) r6
            if (r6 != 0) goto L1c
            goto L45
        L42:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L45:
            java.lang.Runnable r0 = r9.w0()
            if (r0 == 0) goto L4f
            r0.run()
            return r1
        L4f:
            long r0 = r9.Q()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.e.a0():long");
    }

    @Override // defpackage.wl0
    public void f(long j, ov<? super te4> ovVar) {
        long c2 = ay0.c(j);
        if (c2 < 4611686018427387903L) {
            n5.a();
            long nanoTime = System.nanoTime();
            a aVar = new a(c2 + nanoTime, ovVar);
            rv.a(ovVar, aVar);
            J0(nanoTime, aVar);
        }
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public final void h(CoroutineContext coroutineContext, Runnable runnable) {
        x0(runnable);
    }

    public final void r0() {
        k24 k24Var;
        k24 k24Var2;
        if (ze0.a() && !z0()) {
            throw new AssertionError();
        }
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = i0;
                k24Var = ay0.b;
                if (atomicReferenceFieldUpdater.compareAndSet(this, null, k24Var)) {
                    return;
                }
            } else if (!(obj instanceof n12)) {
                k24Var2 = ay0.b;
                if (obj == k24Var2) {
                    return;
                }
                n12 n12Var = new n12(8, true);
                n12Var.a((Runnable) obj);
                if (i0.compareAndSet(this, obj, n12Var)) {
                    return;
                }
            } else {
                ((n12) obj).d();
                return;
            }
        }
    }

    @Override // defpackage.xx0
    public void shutdown() {
        j54.a.c();
        L0(true);
        r0();
        do {
        } while (a0() <= 0);
        F0();
    }

    public final Runnable w0() {
        k24 k24Var;
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (!(obj instanceof n12)) {
                k24Var = ay0.b;
                if (obj == k24Var) {
                    return null;
                }
                if (i0.compareAndSet(this, obj, null)) {
                    return (Runnable) obj;
                }
            } else {
                n12 n12Var = (n12) obj;
                Object j = n12Var.j();
                if (j != n12.h) {
                    return (Runnable) j;
                }
                i0.compareAndSet(this, obj, n12Var.i());
            }
        }
    }

    public final void x0(Runnable runnable) {
        if (y0(runnable)) {
            i0();
        } else {
            d.k0.x0(runnable);
        }
    }

    public final boolean y0(Runnable runnable) {
        k24 k24Var;
        while (true) {
            Object obj = this._queue;
            if (z0()) {
                return false;
            }
            if (obj == null) {
                if (i0.compareAndSet(this, null, runnable)) {
                    return true;
                }
            } else if (!(obj instanceof n12)) {
                k24Var = ay0.b;
                if (obj == k24Var) {
                    return false;
                }
                n12 n12Var = new n12(8, true);
                n12Var.a((Runnable) obj);
                n12Var.a(runnable);
                if (i0.compareAndSet(this, obj, n12Var)) {
                    return true;
                }
            } else {
                n12 n12Var2 = (n12) obj;
                int a2 = n12Var2.a(runnable);
                if (a2 == 0) {
                    return true;
                }
                if (a2 == 1) {
                    i0.compareAndSet(this, obj, n12Var2.i());
                } else if (a2 == 2) {
                    return false;
                }
            }
        }
    }
}
