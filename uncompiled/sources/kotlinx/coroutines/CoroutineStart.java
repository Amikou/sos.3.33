package kotlinx.coroutines;

import java.util.Arrays;
import kotlin.NoWhenBranchMatchedException;

/* compiled from: CoroutineStart.kt */
/* loaded from: classes2.dex */
public enum CoroutineStart {
    DEFAULT,
    LAZY,
    ATOMIC,
    UNDISPATCHED;

    /* compiled from: CoroutineStart.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[CoroutineStart.valuesCustom().length];
            iArr[CoroutineStart.DEFAULT.ordinal()] = 1;
            iArr[CoroutineStart.ATOMIC.ordinal()] = 2;
            iArr[CoroutineStart.UNDISPATCHED.ordinal()] = 3;
            iArr[CoroutineStart.LAZY.ordinal()] = 4;
            a = iArr;
        }
    }

    public static /* synthetic */ void isLazy$annotations() {
    }

    /* renamed from: values  reason: to resolve conflict with enum method */
    public static CoroutineStart[] valuesCustom() {
        CoroutineStart[] valuesCustom = values();
        return (CoroutineStart[]) Arrays.copyOf(valuesCustom, valuesCustom.length);
    }

    public final <T> void invoke(tc1<? super q70<? super T>, ? extends Object> tc1Var, q70<? super T> q70Var) {
        int i = a.a[ordinal()];
        if (i == 1) {
            sv.b(tc1Var, q70Var);
        } else if (i == 2) {
            s70.a(tc1Var, q70Var);
        } else if (i == 3) {
            re4.a(tc1Var, q70Var);
        } else if (i != 4) {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final boolean isLazy() {
        return this == LAZY;
    }

    public final <R, T> void invoke(hd1<? super R, ? super q70<? super T>, ? extends Object> hd1Var, R r, q70<? super T> q70Var) {
        int i = a.a[ordinal()];
        if (i == 1) {
            sv.d(hd1Var, r, q70Var, null, 4, null);
        } else if (i == 2) {
            s70.b(hd1Var, r, q70Var);
        } else if (i == 3) {
            re4.b(hd1Var, r, q70Var);
        } else if (i != 4) {
            throw new NoWhenBranchMatchedException();
        }
    }
}
