package kotlinx.coroutines;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;

/* compiled from: Builders.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class b {
    public static final <T> T a(CoroutineContext coroutineContext, hd1<? super c90, ? super q70<? super T>, ? extends Object> hd1Var) throws InterruptedException {
        xx0 a;
        CoroutineContext c;
        Thread currentThread = Thread.currentThread();
        r70 r70Var = (r70) coroutineContext.get(r70.d);
        if (r70Var == null) {
            a = j54.a.b();
            c = x80.c(qg1.a, coroutineContext.plus(a));
        } else {
            xx0 xx0Var = null;
            xx0 xx0Var2 = r70Var instanceof xx0 ? (xx0) r70Var : null;
            if (xx0Var2 != null && xx0Var2.e0()) {
                xx0Var = xx0Var2;
            }
            a = xx0Var == null ? j54.a.a() : xx0Var;
            c = x80.c(qg1.a, coroutineContext);
        }
        yq yqVar = new yq(c, currentThread, a);
        yqVar.K0(CoroutineStart.DEFAULT, yqVar, hd1Var);
        return (T) yqVar.L0();
    }

    public static /* synthetic */ Object b(CoroutineContext coroutineContext, hd1 hd1Var, int i, Object obj) throws InterruptedException {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        return a.c(coroutineContext, hd1Var);
    }
}
