package kotlinx.coroutines;

import kotlin.coroutines.CoroutineContext;

/* loaded from: classes2.dex */
public final class a {
    public static final st1 a(c90 c90Var, CoroutineContext coroutineContext, CoroutineStart coroutineStart, hd1<? super c90, ? super q70<? super te4>, ? extends Object> hd1Var) {
        return as.a(c90Var, coroutineContext, coroutineStart, hd1Var);
    }

    public static /* synthetic */ st1 b(c90 c90Var, CoroutineContext coroutineContext, CoroutineStart coroutineStart, hd1 hd1Var, int i, Object obj) {
        return as.b(c90Var, coroutineContext, coroutineStart, hd1Var, i, obj);
    }

    public static final <T> T c(CoroutineContext coroutineContext, hd1<? super c90, ? super q70<? super T>, ? extends Object> hd1Var) throws InterruptedException {
        return (T) b.a(coroutineContext, hd1Var);
    }

    public static final <T> Object e(CoroutineContext coroutineContext, hd1<? super c90, ? super q70<? super T>, ? extends Object> hd1Var, q70<? super T> q70Var) {
        return as.c(coroutineContext, hd1Var, q70Var);
    }
}
