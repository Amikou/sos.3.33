package kotlinx.coroutines.sync;

import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.sync.MutexImpl;

/* compiled from: Mutex.kt */
/* loaded from: classes2.dex */
public final class MutexImpl$LockCont$tryResumeLockWaiter$1 extends Lambda implements tc1<Throwable, te4> {
    public final /* synthetic */ MutexImpl this$0;
    public final /* synthetic */ MutexImpl.LockCont this$1;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MutexImpl$LockCont$tryResumeLockWaiter$1(MutexImpl mutexImpl, MutexImpl.LockCont lockCont) {
        super(1);
        this.this$0 = mutexImpl;
        this.this$1 = lockCont;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        invoke2(th);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        this.this$0.a(this.this$1.h0);
    }
}
