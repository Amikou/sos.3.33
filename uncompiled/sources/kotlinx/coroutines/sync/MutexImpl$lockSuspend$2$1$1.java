package kotlinx.coroutines.sync;

import kotlin.jvm.internal.Lambda;

/* compiled from: Mutex.kt */
/* loaded from: classes2.dex */
public final class MutexImpl$lockSuspend$2$1$1 extends Lambda implements tc1<Throwable, te4> {
    public final /* synthetic */ Object $owner;
    public final /* synthetic */ MutexImpl this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MutexImpl$lockSuspend$2$1$1(MutexImpl mutexImpl, Object obj) {
        super(1);
        this.this$0 = mutexImpl;
        this.$owner = obj;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        invoke2(th);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        this.this$0.a(this.$owner);
    }
}
