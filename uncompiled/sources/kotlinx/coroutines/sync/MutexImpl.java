package kotlinx.coroutines.sync;

import defpackage.l12;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Mutex.kt */
/* loaded from: classes2.dex */
public final class MutexImpl implements kb2 {
    public static final /* synthetic */ AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(MutexImpl.class, Object.class, "_state");
    public volatile /* synthetic */ Object _state;

    /* compiled from: Mutex.kt */
    /* loaded from: classes2.dex */
    public final class LockCont extends a {
        public final ov<te4> i0;

        /* JADX WARN: Multi-variable type inference failed */
        public LockCont(Object obj, ov<? super te4> ovVar) {
            super(MutexImpl.this, obj);
            this.i0 = ovVar;
        }

        @Override // defpackage.l12
        public String toString() {
            return "LockCont[" + this.h0 + ", " + this.i0 + "] for " + MutexImpl.this;
        }

        @Override // kotlinx.coroutines.sync.MutexImpl.a
        public void y(Object obj) {
            this.i0.s(obj);
        }

        @Override // kotlinx.coroutines.sync.MutexImpl.a
        public Object z() {
            return this.i0.o(te4.a, null, new MutexImpl$LockCont$tryResumeLockWaiter$1(MutexImpl.this, this));
        }
    }

    /* compiled from: Mutex.kt */
    /* loaded from: classes2.dex */
    public abstract class a extends l12 implements yp0 {
        public final Object h0;

        public a(MutexImpl mutexImpl, Object obj) {
            this.h0 = obj;
        }

        @Override // defpackage.yp0
        public final void a() {
            t();
        }

        public abstract void y(Object obj);

        public abstract Object z();
    }

    /* compiled from: Mutex.kt */
    /* loaded from: classes2.dex */
    public static final class b extends j12 {
        public Object h0;

        public b(Object obj) {
            this.h0 = obj;
        }

        @Override // defpackage.l12
        public String toString() {
            return "LockedQueue[" + this.h0 + ']';
        }
    }

    /* compiled from: Mutex.kt */
    /* loaded from: classes2.dex */
    public static final class c extends cj<MutexImpl> {
        public final b b;

        public c(b bVar) {
            this.b = bVar;
        }

        @Override // defpackage.cj
        /* renamed from: h */
        public void d(MutexImpl mutexImpl, Object obj) {
            MutexImpl.a.compareAndSet(mutexImpl, this, obj == null ? lb2.e : this.b);
        }

        @Override // defpackage.cj
        /* renamed from: i */
        public Object g(MutexImpl mutexImpl) {
            k24 k24Var;
            if (this.b.y()) {
                return null;
            }
            k24Var = lb2.a;
            return k24Var;
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* loaded from: classes2.dex */
    public static final class d extends l12.a {
        public final /* synthetic */ MutexImpl d;
        public final /* synthetic */ Object e;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(l12 l12Var, MutexImpl mutexImpl, Object obj) {
            super(l12Var);
            this.d = mutexImpl;
            this.e = obj;
        }

        @Override // defpackage.cj
        /* renamed from: i */
        public Object g(l12 l12Var) {
            if (this.d._state == this.e) {
                return null;
            }
            return k12.a();
        }
    }

    public MutexImpl(boolean z) {
        this._state = z ? lb2.d : lb2.e;
    }

    @Override // defpackage.kb2
    public void a(Object obj) {
        nu0 nu0Var;
        k24 k24Var;
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof nu0) {
                if (obj == null) {
                    Object obj3 = ((nu0) obj2).a;
                    k24Var = lb2.c;
                    if (!(obj3 != k24Var)) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                } else {
                    nu0 nu0Var2 = (nu0) obj2;
                    if (!(nu0Var2.a == obj)) {
                        throw new IllegalStateException(("Mutex is locked by " + nu0Var2.a + " but expected " + obj).toString());
                    }
                }
                AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
                nu0Var = lb2.e;
                if (atomicReferenceFieldUpdater.compareAndSet(this, obj2, nu0Var)) {
                    return;
                }
            } else if (obj2 instanceof fn2) {
                ((fn2) obj2).c(this);
            } else if (obj2 instanceof b) {
                if (obj != null) {
                    b bVar = (b) obj2;
                    if (!(bVar.h0 == obj)) {
                        throw new IllegalStateException(("Mutex is locked by " + bVar.h0 + " but expected " + obj).toString());
                    }
                }
                b bVar2 = (b) obj2;
                l12 u = bVar2.u();
                if (u == null) {
                    c cVar = new c(bVar2);
                    if (a.compareAndSet(this, obj2, cVar) && cVar.c(this) == null) {
                        return;
                    }
                } else {
                    a aVar = (a) u;
                    Object z = aVar.z();
                    if (z != null) {
                        Object obj4 = aVar.h0;
                        if (obj4 == null) {
                            obj4 = lb2.b;
                        }
                        bVar2.h0 = obj4;
                        aVar.y(z);
                        return;
                    }
                }
            } else {
                throw new IllegalStateException(fs1.l("Illegal state ", obj2).toString());
            }
        }
    }

    @Override // defpackage.kb2
    public Object b(Object obj, q70<? super te4> q70Var) {
        Object c2;
        return (!d(obj) && (c2 = c(obj, q70Var)) == gs1.d()) ? c2 : te4.a;
    }

    public final Object c(Object obj, q70<? super te4> q70Var) {
        k24 k24Var;
        pv b2 = rv.b(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var));
        LockCont lockCont = new LockCont(obj, b2);
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof nu0) {
                nu0 nu0Var = (nu0) obj2;
                Object obj3 = nu0Var.a;
                k24Var = lb2.c;
                if (obj3 != k24Var) {
                    a.compareAndSet(this, obj2, new b(nu0Var.a));
                } else {
                    if (a.compareAndSet(this, obj2, obj == null ? lb2.d : new nu0(obj))) {
                        b2.j(te4.a, new MutexImpl$lockSuspend$2$1$1(this, obj));
                        break;
                    }
                }
            } else if (obj2 instanceof b) {
                boolean z = false;
                if (((b) obj2).h0 != obj) {
                    l12 l12Var = (l12) obj2;
                    d dVar = new d(lockCont, this, obj2);
                    while (true) {
                        int x = l12Var.p().x(lockCont, l12Var, dVar);
                        if (x == 1) {
                            z = true;
                            break;
                        } else if (x == 2) {
                            break;
                        }
                    }
                    if (z) {
                        rv.c(b2, lockCont);
                        break;
                    }
                } else {
                    throw new IllegalStateException(fs1.l("Already locked by ", obj).toString());
                }
            } else if (!(obj2 instanceof fn2)) {
                throw new IllegalStateException(fs1.l("Illegal state ", obj2).toString());
            } else {
                ((fn2) obj2).c(this);
            }
        }
        Object x2 = b2.x();
        if (x2 == gs1.d()) {
            ef0.c(q70Var);
        }
        return x2 == gs1.d() ? x2 : te4.a;
    }

    public boolean d(Object obj) {
        k24 k24Var;
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof nu0) {
                Object obj3 = ((nu0) obj2).a;
                k24Var = lb2.c;
                if (obj3 != k24Var) {
                    return false;
                }
                if (a.compareAndSet(this, obj2, obj == null ? lb2.d : new nu0(obj))) {
                    return true;
                }
            } else if (obj2 instanceof b) {
                if (((b) obj2).h0 != obj) {
                    return false;
                }
                throw new IllegalStateException(fs1.l("Already locked by ", obj).toString());
            } else if (!(obj2 instanceof fn2)) {
                throw new IllegalStateException(fs1.l("Illegal state ", obj2).toString());
            } else {
                ((fn2) obj2).c(this);
            }
        }
    }

    public String toString() {
        while (true) {
            Object obj = this._state;
            if (obj instanceof nu0) {
                return "Mutex[" + ((nu0) obj).a + ']';
            } else if (!(obj instanceof fn2)) {
                if (obj instanceof b) {
                    return "Mutex[" + ((b) obj).h0 + ']';
                }
                throw new IllegalStateException(fs1.l("Illegal state ", obj).toString());
            } else {
                ((fn2) obj).c(this);
            }
        }
    }
}
