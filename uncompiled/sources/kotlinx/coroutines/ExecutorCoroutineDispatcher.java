package kotlinx.coroutines;

import java.io.Closeable;
import java.util.concurrent.Executor;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: Executors.kt */
/* loaded from: classes2.dex */
public abstract class ExecutorCoroutineDispatcher extends CoroutineDispatcher implements Closeable {

    /* compiled from: Executors.kt */
    /* loaded from: classes2.dex */
    public static final class Key extends v4<CoroutineDispatcher, ExecutorCoroutineDispatcher> {

        /* compiled from: Executors.kt */
        /* renamed from: kotlinx.coroutines.ExecutorCoroutineDispatcher$Key$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends Lambda implements tc1<CoroutineContext.a, ExecutorCoroutineDispatcher> {
            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

            public AnonymousClass1() {
                super(1);
            }

            @Override // defpackage.tc1
            public final ExecutorCoroutineDispatcher invoke(CoroutineContext.a aVar) {
                if (aVar instanceof ExecutorCoroutineDispatcher) {
                    return (ExecutorCoroutineDispatcher) aVar;
                }
                return null;
            }
        }

        public Key() {
            super(CoroutineDispatcher.a, AnonymousClass1.INSTANCE);
        }

        public /* synthetic */ Key(qi0 qi0Var) {
            this();
        }
    }

    static {
        new Key(null);
    }

    public abstract Executor l();
}
