package kotlinx.coroutines;

import defpackage.r70;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: CoroutineDispatcher.kt */
/* loaded from: classes2.dex */
public abstract class CoroutineDispatcher extends u4 implements r70 {
    public static final Key a = new Key(null);

    /* compiled from: CoroutineDispatcher.kt */
    /* loaded from: classes2.dex */
    public static final class Key extends v4<r70, CoroutineDispatcher> {

        /* compiled from: CoroutineDispatcher.kt */
        /* renamed from: kotlinx.coroutines.CoroutineDispatcher$Key$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends Lambda implements tc1<CoroutineContext.a, CoroutineDispatcher> {
            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

            public AnonymousClass1() {
                super(1);
            }

            @Override // defpackage.tc1
            public final CoroutineDispatcher invoke(CoroutineContext.a aVar) {
                if (aVar instanceof CoroutineDispatcher) {
                    return (CoroutineDispatcher) aVar;
                }
                return null;
            }
        }

        public Key() {
            super(r70.d, AnonymousClass1.INSTANCE);
        }

        public /* synthetic */ Key(qi0 qi0Var) {
            this();
        }
    }

    public CoroutineDispatcher() {
        super(r70.d);
    }

    @Override // defpackage.r70
    public void A(q70<?> q70Var) {
        ((np0) q70Var).t();
    }

    @Override // defpackage.u4, kotlin.coroutines.CoroutineContext.a, kotlin.coroutines.CoroutineContext
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        return (E) r70.a.a(this, bVar);
    }

    public abstract void h(CoroutineContext coroutineContext, Runnable runnable);

    public void i(CoroutineContext coroutineContext, Runnable runnable) {
        h(coroutineContext, runnable);
    }

    public boolean j(CoroutineContext coroutineContext) {
        return true;
    }

    @Override // defpackage.u4, kotlin.coroutines.CoroutineContext
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        return r70.a.b(this, bVar);
    }

    public String toString() {
        return ff0.a(this) + '@' + ff0.b(this);
    }

    @Override // defpackage.r70
    public final <T> q70<T> v(q70<? super T> q70Var) {
        return new np0(this, q70Var);
    }
}
