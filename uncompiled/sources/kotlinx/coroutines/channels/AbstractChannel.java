package kotlinx.coroutines.channels;

import defpackage.l12;
import defpackage.ov;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;
import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.internal.OnUndeliveredElementKt;

/* compiled from: AbstractChannel.kt */
/* loaded from: classes2.dex */
public abstract class AbstractChannel<E> extends h5<E> implements kx<E> {

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes2.dex */
    public static final class a<E> implements qx<E> {
        public final AbstractChannel<E> a;
        public Object b = q4.d;

        public a(AbstractChannel<E> abstractChannel) {
            this.a = abstractChannel;
        }

        @Override // defpackage.qx
        public Object a(q70<? super Boolean> q70Var) {
            Object b = b();
            k24 k24Var = q4.d;
            if (b != k24Var) {
                return hr.a(c(b()));
            }
            e(this.a.N());
            return b() != k24Var ? hr.a(c(b())) : d(q70Var);
        }

        public final Object b() {
            return this.b;
        }

        public final boolean c(Object obj) {
            if (obj instanceof d00) {
                d00 d00Var = (d00) obj;
                if (d00Var.h0 == null) {
                    return false;
                }
                throw hs3.k(d00Var.F());
            }
            return true;
        }

        public final Object d(q70<? super Boolean> q70Var) {
            pv b = rv.b(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var));
            d dVar = new d(this, b);
            while (true) {
                if (this.a.E(dVar)) {
                    this.a.P(b, dVar);
                    break;
                }
                Object N = this.a.N();
                e(N);
                if (N instanceof d00) {
                    d00 d00Var = (d00) N;
                    if (d00Var.h0 == null) {
                        Boolean a = hr.a(false);
                        Result.a aVar = Result.Companion;
                        b.resumeWith(Result.m52constructorimpl(a));
                    } else {
                        Throwable F = d00Var.F();
                        Result.a aVar2 = Result.Companion;
                        b.resumeWith(Result.m52constructorimpl(o83.a(F)));
                    }
                } else if (N != q4.d) {
                    Boolean a2 = hr.a(true);
                    tc1<E, te4> tc1Var = this.a.a;
                    b.j(a2, tc1Var == null ? null : OnUndeliveredElementKt.a(tc1Var, N, b.getContext()));
                }
            }
            Object x = b.x();
            if (x == gs1.d()) {
                ef0.c(q70Var);
            }
            return x;
        }

        public final void e(Object obj) {
            this.b = obj;
        }

        @Override // defpackage.qx
        public E next() {
            E e = (E) this.b;
            if (!(e instanceof d00)) {
                k24 k24Var = q4.d;
                if (e != k24Var) {
                    this.b = k24Var;
                    return e;
                }
                throw new IllegalStateException("'hasNext' should be called prior to 'next' invocation");
            }
            throw hs3.k(((d00) e).F());
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes2.dex */
    public static class b<E> extends e43<E> {
        public final ov<Object> h0;
        public final int i0;

        public b(ov<Object> ovVar, int i) {
            this.h0 = ovVar;
            this.i0 = i;
        }

        @Override // defpackage.e43
        public void A(d00<?> d00Var) {
            if (this.i0 == 1) {
                ov<Object> ovVar = this.h0;
                tx b = tx.b(tx.b.a(d00Var.h0));
                Result.a aVar = Result.Companion;
                ovVar.resumeWith(Result.m52constructorimpl(b));
                return;
            }
            ov<Object> ovVar2 = this.h0;
            Throwable F = d00Var.F();
            Result.a aVar2 = Result.Companion;
            ovVar2.resumeWith(Result.m52constructorimpl(o83.a(F)));
        }

        public final Object B(E e) {
            return this.i0 == 1 ? tx.b(tx.b.c(e)) : e;
        }

        @Override // defpackage.l43
        public void f(E e) {
            this.h0.s(qv.a);
        }

        @Override // defpackage.l43
        public k24 g(E e, l12.b bVar) {
            Object o = this.h0.o(B(e), null, z(e));
            if (o == null) {
                return null;
            }
            if (ze0.a()) {
                if (!(o == qv.a)) {
                    throw new AssertionError();
                }
            }
            return qv.a;
        }

        @Override // defpackage.l12
        public String toString() {
            return "ReceiveElement@" + ff0.b(this) + "[receiveMode=" + this.i0 + ']';
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes2.dex */
    public static final class c<E> extends b<E> {
        public final tc1<E, te4> j0;

        /* JADX WARN: Multi-variable type inference failed */
        public c(ov<Object> ovVar, int i, tc1<? super E, te4> tc1Var) {
            super(ovVar, i);
            this.j0 = tc1Var;
        }

        @Override // defpackage.e43
        public tc1<Throwable, te4> z(E e) {
            return OnUndeliveredElementKt.a(this.j0, e, this.h0.getContext());
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes2.dex */
    public static class d<E> extends e43<E> {
        public final a<E> h0;
        public final ov<Boolean> i0;

        /* JADX WARN: Multi-variable type inference failed */
        public d(a<E> aVar, ov<? super Boolean> ovVar) {
            this.h0 = aVar;
            this.i0 = ovVar;
        }

        @Override // defpackage.e43
        public void A(d00<?> d00Var) {
            Object i;
            if (d00Var.h0 == null) {
                i = ov.a.a(this.i0, Boolean.FALSE, null, 2, null);
            } else {
                i = this.i0.i(d00Var.F());
            }
            if (i != null) {
                this.h0.e(d00Var);
                this.i0.s(i);
            }
        }

        @Override // defpackage.l43
        public void f(E e) {
            this.h0.e(e);
            this.i0.s(qv.a);
        }

        @Override // defpackage.l43
        public k24 g(E e, l12.b bVar) {
            Object o = this.i0.o(Boolean.TRUE, null, z(e));
            if (o == null) {
                return null;
            }
            if (ze0.a()) {
                if (!(o == qv.a)) {
                    throw new AssertionError();
                }
            }
            return qv.a;
        }

        @Override // defpackage.l12
        public String toString() {
            return fs1.l("ReceiveHasNext@", ff0.b(this));
        }

        @Override // defpackage.e43
        public tc1<Throwable, te4> z(E e) {
            tc1<E, te4> tc1Var = this.h0.a.a;
            if (tc1Var == null) {
                return null;
            }
            return OnUndeliveredElementKt.a(tc1Var, e, this.i0.getContext());
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes2.dex */
    public final class e extends zo {
        public final e43<?> a;

        public e(e43<?> e43Var) {
            this.a = e43Var;
        }

        @Override // defpackage.jv
        public void a(Throwable th) {
            if (this.a.t()) {
                AbstractChannel.this.L();
            }
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
            a(th);
            return te4.a;
        }

        public String toString() {
            return "RemoveReceiveOnCancel[" + this.a + ']';
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* loaded from: classes2.dex */
    public static final class f extends l12.a {
        public final /* synthetic */ AbstractChannel d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(l12 l12Var, AbstractChannel abstractChannel) {
            super(l12Var);
            this.d = abstractChannel;
        }

        @Override // defpackage.cj
        /* renamed from: i */
        public Object g(l12 l12Var) {
            if (this.d.H()) {
                return null;
            }
            return k12.a();
        }
    }

    public AbstractChannel(tc1<? super E, te4> tc1Var) {
        super(tc1Var);
    }

    public final boolean D(Throwable th) {
        boolean l = l(th);
        J(l);
        return l;
    }

    public final boolean E(e43<? super E> e43Var) {
        boolean F = F(e43Var);
        if (F) {
            M();
        }
        return F;
    }

    public boolean F(e43<? super E> e43Var) {
        int x;
        l12 p;
        if (G()) {
            l12 k = k();
            do {
                p = k.p();
                if (!(!(p instanceof tj3))) {
                    return false;
                }
            } while (!p.i(e43Var, k));
        } else {
            l12 k2 = k();
            f fVar = new f(e43Var, this);
            do {
                l12 p2 = k2.p();
                if (!(!(p2 instanceof tj3))) {
                    return false;
                }
                x = p2.x(e43Var, k2, fVar);
                if (x != 1) {
                }
            } while (x != 2);
            return false;
        }
        return true;
    }

    public abstract boolean G();

    public abstract boolean H();

    public boolean I() {
        return i() != null && H();
    }

    public void J(boolean z) {
        d00<?> j = j();
        if (j != null) {
            Object b2 = tq1.b(null, 1, null);
            while (true) {
                l12 p = j.p();
                if (p instanceof j12) {
                    K(b2, j);
                    return;
                } else if (ze0.a() && !(p instanceof tj3)) {
                    throw new AssertionError();
                } else {
                    if (!p.t()) {
                        p.q();
                    } else {
                        b2 = tq1.c(b2, (tj3) p);
                    }
                }
            }
        } else {
            throw new IllegalStateException("Cannot happen".toString());
        }
    }

    public void K(Object obj, d00<?> d00Var) {
        if (obj == null) {
            return;
        }
        if (!(obj instanceof ArrayList)) {
            ((tj3) obj).A(d00Var);
            return;
        }
        ArrayList arrayList = (ArrayList) obj;
        int size = arrayList.size() - 1;
        if (size < 0) {
            return;
        }
        while (true) {
            int i = size - 1;
            ((tj3) arrayList.get(size)).A(d00Var);
            if (i < 0) {
                return;
            }
            size = i;
        }
    }

    public void L() {
    }

    public void M() {
    }

    public Object N() {
        while (true) {
            tj3 A = A();
            if (A == null) {
                return q4.d;
            }
            k24 B = A.B(null);
            if (B != null) {
                if (ze0.a()) {
                    if (!(B == qv.a)) {
                        throw new AssertionError();
                    }
                }
                A.y();
                return A.z();
            }
            A.C();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v3, types: [kotlinx.coroutines.channels.AbstractChannel$b] */
    public final <R> Object O(int i, q70<? super R> q70Var) {
        c cVar;
        pv b2 = rv.b(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var));
        if (this.a == null) {
            cVar = new b(b2, i);
        } else {
            cVar = new c(b2, i, this.a);
        }
        while (true) {
            if (E(cVar)) {
                P(b2, cVar);
                break;
            }
            Object N = N();
            if (N instanceof d00) {
                cVar.A((d00) N);
                break;
            } else if (N != q4.d) {
                b2.j(cVar.B(N), cVar.z(N));
                break;
            }
        }
        Object x = b2.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x;
    }

    public final void P(ov<?> ovVar, e43<?> e43Var) {
        ovVar.f(new e(e43Var));
    }

    @Override // defpackage.f43
    public final void a(CancellationException cancellationException) {
        if (I()) {
            return;
        }
        if (cancellationException == null) {
            cancellationException = new CancellationException(fs1.l(ff0.a(this), " was cancelled"));
        }
        D(cancellationException);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
    @Override // defpackage.f43
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object e(defpackage.q70<? super defpackage.tx<? extends E>> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof kotlinx.coroutines.channels.AbstractChannel$receiveCatching$1
            if (r0 == 0) goto L13
            r0 = r5
            kotlinx.coroutines.channels.AbstractChannel$receiveCatching$1 r0 = (kotlinx.coroutines.channels.AbstractChannel$receiveCatching$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            kotlinx.coroutines.channels.AbstractChannel$receiveCatching$1 r0 = new kotlinx.coroutines.channels.AbstractChannel$receiveCatching$1
            r0.<init>(r4, r5)
        L18:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L31
            if (r2 != r3) goto L29
            defpackage.o83.b(r5)
            goto L5b
        L29:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L31:
            defpackage.o83.b(r5)
            java.lang.Object r5 = r4.N()
            k24 r2 = defpackage.q4.d
            if (r5 == r2) goto L52
            boolean r0 = r5 instanceof defpackage.d00
            if (r0 == 0) goto L4b
            tx$b r0 = defpackage.tx.b
            d00 r5 = (defpackage.d00) r5
            java.lang.Throwable r5 = r5.h0
            java.lang.Object r5 = r0.a(r5)
            goto L51
        L4b:
            tx$b r0 = defpackage.tx.b
            java.lang.Object r5 = r0.c(r5)
        L51:
            return r5
        L52:
            r0.label = r3
            java.lang.Object r5 = r4.O(r3, r0)
            if (r5 != r1) goto L5b
            return r1
        L5b:
            tx r5 = (defpackage.tx) r5
            java.lang.Object r5 = r5.k()
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.AbstractChannel.e(q70):java.lang.Object");
    }

    @Override // defpackage.f43
    public final qx<E> iterator() {
        return new a(this);
    }

    @Override // defpackage.h5
    public l43<E> z() {
        l43<E> z = super.z();
        if (z != null && !(z instanceof d00)) {
            L();
        }
        return z;
    }
}
