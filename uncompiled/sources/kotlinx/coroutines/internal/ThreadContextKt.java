package kotlinx.coroutines.internal;

import java.util.Objects;
import kotlin.coroutines.CoroutineContext;

/* compiled from: ThreadContext.kt */
/* loaded from: classes2.dex */
public final class ThreadContextKt {
    public static final k24 a = new k24("NO_THREAD_ELEMENTS");
    public static final hd1<Object, CoroutineContext.a, Object> b = ThreadContextKt$countAll$1.INSTANCE;
    public static final hd1<g54<?>, CoroutineContext.a, g54<?>> c = ThreadContextKt$findOne$1.INSTANCE;
    public static final hd1<m54, CoroutineContext.a, m54> d = ThreadContextKt$updateState$1.INSTANCE;

    public static final void a(CoroutineContext coroutineContext, Object obj) {
        if (obj == a) {
            return;
        }
        if (obj instanceof m54) {
            ((m54) obj).b(coroutineContext);
            return;
        }
        Object fold = coroutineContext.fold(null, c);
        Objects.requireNonNull(fold, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        ((g54) fold).q(coroutineContext, obj);
    }

    public static final Object b(CoroutineContext coroutineContext) {
        Object fold = coroutineContext.fold(0, b);
        fs1.d(fold);
        return fold;
    }

    public static final Object c(CoroutineContext coroutineContext, Object obj) {
        if (obj == null) {
            obj = b(coroutineContext);
        }
        if (obj == 0) {
            return a;
        }
        if (obj instanceof Integer) {
            return coroutineContext.fold(new m54(coroutineContext, ((Number) obj).intValue()), d);
        }
        return ((g54) obj).C(coroutineContext);
    }
}
