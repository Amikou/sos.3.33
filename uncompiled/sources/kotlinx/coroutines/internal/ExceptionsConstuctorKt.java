package kotlinx.coroutines.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.Iterator;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import kotlin.Result;

/* compiled from: ExceptionsConstuctor.kt */
/* loaded from: classes2.dex */
public final class ExceptionsConstuctorKt {
    public static final int a = d(Throwable.class, -1);
    public static final ReentrantReadWriteLock b = new ReentrantReadWriteLock();
    public static final WeakHashMap<Class<? extends Throwable>, tc1<Throwable, Throwable>> c = new WeakHashMap<>();

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Comparator<T> {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(Integer.valueOf(((Constructor) t2).getParameterTypes().length), Integer.valueOf(((Constructor) t).getParameterTypes().length));
        }
    }

    public static final tc1<Throwable, Throwable> a(Constructor<?> constructor) {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        int length = parameterTypes.length;
        if (length != 0) {
            if (length != 1) {
                if (length == 2 && fs1.b(parameterTypes[0], String.class) && fs1.b(parameterTypes[1], Throwable.class)) {
                    return new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$1(constructor);
                }
                return null;
            }
            Class<?> cls = parameterTypes[0];
            if (fs1.b(cls, Throwable.class)) {
                return new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$2(constructor);
            }
            if (fs1.b(cls, String.class)) {
                return new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$3(constructor);
            }
            return null;
        }
        return new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$4(constructor);
    }

    public static final int b(Class<?> cls, int i) {
        Field[] declaredFields;
        do {
            int length = cls.getDeclaredFields().length;
            int i2 = 0;
            for (int i3 = 0; i3 < length; i3++) {
                if (!Modifier.isStatic(declaredFields[i3].getModifiers())) {
                    i2++;
                }
            }
            i += i2;
            cls = cls.getSuperclass();
        } while (cls != null);
        return i;
    }

    public static /* synthetic */ int c(Class cls, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return b(cls, i);
    }

    public static final int d(Class<?> cls, int i) {
        Integer m52constructorimpl;
        nw1.c(cls);
        try {
            Result.a aVar = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(Integer.valueOf(c(cls, 0, 1, null)));
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(o83.a(th));
        }
        Integer valueOf = Integer.valueOf(i);
        if (Result.m57isFailureimpl(m52constructorimpl)) {
            m52constructorimpl = valueOf;
        }
        return ((Number) m52constructorimpl).intValue();
    }

    public static final <E extends Throwable> E e(E e) {
        Object m52constructorimpl;
        ReentrantReadWriteLock.ReadLock readLock;
        int readHoldCount;
        ReentrantReadWriteLock.WriteLock writeLock;
        if (e instanceof u80) {
            try {
                Result.a aVar = Result.Companion;
                m52constructorimpl = Result.m52constructorimpl(((u80) e).createCopy());
            } catch (Throwable th) {
                Result.a aVar2 = Result.Companion;
                m52constructorimpl = Result.m52constructorimpl(o83.a(th));
            }
            return (E) (Result.m57isFailureimpl(m52constructorimpl) ? null : m52constructorimpl);
        }
        ReentrantReadWriteLock reentrantReadWriteLock = b;
        ReentrantReadWriteLock.ReadLock readLock2 = reentrantReadWriteLock.readLock();
        readLock2.lock();
        try {
            tc1<Throwable, Throwable> tc1Var = c.get(e.getClass());
            if (tc1Var == null) {
                int i = 0;
                if (a != d(e.getClass(), 0)) {
                    readLock = reentrantReadWriteLock.readLock();
                    readHoldCount = reentrantReadWriteLock.getWriteHoldCount() == 0 ? reentrantReadWriteLock.getReadHoldCount() : 0;
                    for (int i2 = 0; i2 < readHoldCount; i2++) {
                        readLock.unlock();
                    }
                    writeLock = reentrantReadWriteLock.writeLock();
                    writeLock.lock();
                    try {
                        c.put(e.getClass(), ExceptionsConstuctorKt$tryCopyException$4$1.INSTANCE);
                        te4 te4Var = te4.a;
                        return null;
                    } finally {
                        while (i < readHoldCount) {
                            readLock.lock();
                            i++;
                        }
                        writeLock.unlock();
                    }
                }
                Iterator it = ai.G(e.getClass().getConstructors(), new a()).iterator();
                tc1<Throwable, Throwable> tc1Var2 = null;
                while (it.hasNext() && (tc1Var2 = a((Constructor) it.next())) == null) {
                }
                ReentrantReadWriteLock reentrantReadWriteLock2 = b;
                readLock = reentrantReadWriteLock2.readLock();
                readHoldCount = reentrantReadWriteLock2.getWriteHoldCount() == 0 ? reentrantReadWriteLock2.getReadHoldCount() : 0;
                for (int i3 = 0; i3 < readHoldCount; i3++) {
                    readLock.unlock();
                }
                writeLock = reentrantReadWriteLock2.writeLock();
                writeLock.lock();
                try {
                    c.put(e.getClass(), tc1Var2 == null ? ExceptionsConstuctorKt$tryCopyException$5$1.INSTANCE : tc1Var2);
                    te4 te4Var2 = te4.a;
                    while (i < readHoldCount) {
                        readLock.lock();
                        i++;
                    }
                    writeLock.unlock();
                    if (tc1Var2 == null) {
                        return null;
                    }
                    return (E) tc1Var2.invoke(e);
                } finally {
                    while (i < readHoldCount) {
                        readLock.lock();
                        i++;
                    }
                    writeLock.unlock();
                }
            }
            return (E) tc1Var.invoke(e);
        } finally {
            readLock2.unlock();
        }
    }
}
