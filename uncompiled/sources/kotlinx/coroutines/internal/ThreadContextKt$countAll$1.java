package kotlinx.coroutines.internal;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: ThreadContext.kt */
/* loaded from: classes2.dex */
public final class ThreadContextKt$countAll$1 extends Lambda implements hd1<Object, CoroutineContext.a, Object> {
    public static final ThreadContextKt$countAll$1 INSTANCE = new ThreadContextKt$countAll$1();

    public ThreadContextKt$countAll$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public final Object invoke(Object obj, CoroutineContext.a aVar) {
        if (aVar instanceof g54) {
            Integer num = obj instanceof Integer ? (Integer) obj : null;
            int intValue = num == null ? 1 : num.intValue();
            return intValue == 0 ? aVar : Integer.valueOf(intValue + 1);
        }
        return obj;
    }
}
