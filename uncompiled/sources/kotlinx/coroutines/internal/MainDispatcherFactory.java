package kotlinx.coroutines.internal;

import java.util.List;

/* compiled from: MainDispatcherFactory.kt */
/* loaded from: classes2.dex */
public interface MainDispatcherFactory {
    e32 createDispatcher(List<? extends MainDispatcherFactory> list);

    int getLoadPriority();

    String hintOnError();
}
