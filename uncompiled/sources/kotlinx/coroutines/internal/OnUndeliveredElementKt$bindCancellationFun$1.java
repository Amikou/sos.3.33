package kotlinx.coroutines.internal;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: OnUndeliveredElement.kt */
/* loaded from: classes2.dex */
public final class OnUndeliveredElementKt$bindCancellationFun$1 extends Lambda implements tc1<Throwable, te4> {
    public final /* synthetic */ CoroutineContext $context;
    public final /* synthetic */ E $element;
    public final /* synthetic */ tc1<E, te4> $this_bindCancellationFun;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public OnUndeliveredElementKt$bindCancellationFun$1(tc1<? super E, te4> tc1Var, E e, CoroutineContext coroutineContext) {
        super(1);
        this.$this_bindCancellationFun = tc1Var;
        this.$element = e;
        this.$context = coroutineContext;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        invoke2(th);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        OnUndeliveredElementKt.b(this.$this_bindCancellationFun, this.$element, this.$context);
    }
}
