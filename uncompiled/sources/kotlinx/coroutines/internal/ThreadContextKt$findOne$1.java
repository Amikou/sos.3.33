package kotlinx.coroutines.internal;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: ThreadContext.kt */
/* loaded from: classes2.dex */
public final class ThreadContextKt$findOne$1 extends Lambda implements hd1<g54<?>, CoroutineContext.a, g54<?>> {
    public static final ThreadContextKt$findOne$1 INSTANCE = new ThreadContextKt$findOne$1();

    public ThreadContextKt$findOne$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public final g54<?> invoke(g54<?> g54Var, CoroutineContext.a aVar) {
        if (g54Var != null) {
            return g54Var;
        }
        if (aVar instanceof g54) {
            return (g54) aVar;
        }
        return null;
    }
}
