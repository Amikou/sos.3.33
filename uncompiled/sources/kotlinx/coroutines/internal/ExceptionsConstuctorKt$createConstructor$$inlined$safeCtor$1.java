package kotlinx.coroutines.internal;

import java.lang.reflect.Constructor;
import kotlin.Result;
import kotlin.jvm.internal.Lambda;

/* compiled from: ExceptionsConstuctor.kt */
/* loaded from: classes2.dex */
public final class ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$1 extends Lambda implements tc1<Throwable, Throwable> {
    public final /* synthetic */ Constructor $constructor$inlined;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$1(Constructor constructor) {
        super(1);
        this.$constructor$inlined = constructor;
    }

    @Override // defpackage.tc1
    public final Throwable invoke(Throwable th) {
        Object m52constructorimpl;
        Object newInstance;
        try {
            Result.a aVar = Result.Companion;
            newInstance = this.$constructor$inlined.newInstance(th.getMessage(), th);
        } catch (Throwable th2) {
            Result.a aVar2 = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(o83.a(th2));
        }
        if (newInstance != null) {
            m52constructorimpl = Result.m52constructorimpl((Throwable) newInstance);
            if (Result.m57isFailureimpl(m52constructorimpl)) {
                m52constructorimpl = null;
            }
            return (Throwable) m52constructorimpl;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Throwable");
    }
}
