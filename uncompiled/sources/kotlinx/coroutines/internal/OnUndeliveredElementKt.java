package kotlinx.coroutines.internal;

import kotlin.coroutines.CoroutineContext;

/* compiled from: OnUndeliveredElement.kt */
/* loaded from: classes2.dex */
public final class OnUndeliveredElementKt {
    public static final <E> tc1<Throwable, te4> a(tc1<? super E, te4> tc1Var, E e, CoroutineContext coroutineContext) {
        return new OnUndeliveredElementKt$bindCancellationFun$1(tc1Var, e, coroutineContext);
    }

    public static final <E> void b(tc1<? super E, te4> tc1Var, E e, CoroutineContext coroutineContext) {
        UndeliveredElementException c = c(tc1Var, e, null);
        if (c == null) {
            return;
        }
        z80.a(coroutineContext, c);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <E> UndeliveredElementException c(tc1<? super E, te4> tc1Var, E e, UndeliveredElementException undeliveredElementException) {
        try {
            tc1Var.invoke(e);
        } catch (Throwable th) {
            if (undeliveredElementException != null && undeliveredElementException.getCause() != th) {
                py0.a(undeliveredElementException, th);
            } else {
                return new UndeliveredElementException(fs1.l("Exception in undelivered element handler for ", e), th);
            }
        }
        return undeliveredElementException;
    }

    public static /* synthetic */ UndeliveredElementException d(tc1 tc1Var, Object obj, UndeliveredElementException undeliveredElementException, int i, Object obj2) {
        if ((i & 2) != 0) {
            undeliveredElementException = null;
        }
        return c(tc1Var, obj, undeliveredElementException);
    }
}
