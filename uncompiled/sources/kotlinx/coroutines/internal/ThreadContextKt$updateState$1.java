package kotlinx.coroutines.internal;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: ThreadContext.kt */
/* loaded from: classes2.dex */
public final class ThreadContextKt$updateState$1 extends Lambda implements hd1<m54, CoroutineContext.a, m54> {
    public static final ThreadContextKt$updateState$1 INSTANCE = new ThreadContextKt$updateState$1();

    public ThreadContextKt$updateState$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public final m54 invoke(m54 m54Var, CoroutineContext.a aVar) {
        if (aVar instanceof g54) {
            g54<?> g54Var = (g54) aVar;
            m54Var.a(g54Var, g54Var.C(m54Var.a));
        }
        return m54Var;
    }
}
