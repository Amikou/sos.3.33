package kotlinx.coroutines;

import kotlin.coroutines.CoroutineContext;

/* compiled from: Unconfined.kt */
/* loaded from: classes2.dex */
public final class f extends CoroutineDispatcher {
    public static final f f0 = new f();

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void h(CoroutineContext coroutineContext, Runnable runnable) {
        ms4 ms4Var = (ms4) coroutineContext.get(ms4.f0);
        if (ms4Var != null) {
            ms4Var.a = true;
            return;
        }
        throw new UnsupportedOperationException("Dispatchers.Unconfined.dispatch function can only be used by the yield function. If you wrap Unconfined dispatcher in your code, make sure you properly delegate isDispatchNeeded and dispatch calls.");
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public boolean j(CoroutineContext coroutineContext) {
        return false;
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        return "Dispatchers.Unconfined";
    }
}
