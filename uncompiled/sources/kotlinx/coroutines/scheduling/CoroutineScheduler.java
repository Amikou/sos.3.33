package kotlinx.coroutines.scheduling;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.LockSupport;
import kotlin.random.Random;

/* compiled from: CoroutineScheduler.kt */
/* loaded from: classes2.dex */
public final class CoroutineScheduler implements Executor, Closeable {
    public static final /* synthetic */ AtomicLongFieldUpdater l0;
    public static final /* synthetic */ AtomicLongFieldUpdater m0;
    public static final /* synthetic */ AtomicIntegerFieldUpdater n0;
    public static final k24 o0;
    private volatile /* synthetic */ int _isTerminated;
    public final int a;
    public volatile /* synthetic */ long controlState;
    public final int f0;
    public final long g0;
    public final String h0;
    public final pg1 i0;
    public final pg1 j0;
    public final AtomicReferenceArray<c> k0;
    private volatile /* synthetic */ long parkedWorkersStack;

    /* compiled from: CoroutineScheduler.kt */
    /* loaded from: classes2.dex */
    public enum WorkerState {
        CPU_ACQUIRED,
        BLOCKING,
        PARKING,
        DORMANT,
        TERMINATED;

        /* renamed from: values  reason: to resolve conflict with enum method */
        public static WorkerState[] valuesCustom() {
            WorkerState[] valuesCustom = values();
            return (WorkerState[]) Arrays.copyOf(valuesCustom, valuesCustom.length);
        }
    }

    /* compiled from: CoroutineScheduler.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: CoroutineScheduler.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WorkerState.valuesCustom().length];
            iArr[WorkerState.PARKING.ordinal()] = 1;
            iArr[WorkerState.BLOCKING.ordinal()] = 2;
            iArr[WorkerState.CPU_ACQUIRED.ordinal()] = 3;
            iArr[WorkerState.DORMANT.ordinal()] = 4;
            iArr[WorkerState.TERMINATED.ordinal()] = 5;
            a = iArr;
        }
    }

    static {
        new a(null);
        o0 = new k24("NOT_IN_STACK");
        l0 = AtomicLongFieldUpdater.newUpdater(CoroutineScheduler.class, "parkedWorkersStack");
        m0 = AtomicLongFieldUpdater.newUpdater(CoroutineScheduler.class, "controlState");
        n0 = AtomicIntegerFieldUpdater.newUpdater(CoroutineScheduler.class, "_isTerminated");
    }

    public CoroutineScheduler(int i, int i2, long j, String str) {
        this.a = i;
        this.f0 = i2;
        this.g0 = j;
        this.h0 = str;
        if (!(i >= 1)) {
            throw new IllegalArgumentException(("Core pool size " + i + " should be at least 1").toString());
        }
        if (!(i2 >= i)) {
            throw new IllegalArgumentException(("Max pool size " + i2 + " should be greater than or equals to core pool size " + i).toString());
        }
        if (!(i2 <= 2097150)) {
            throw new IllegalArgumentException(("Max pool size " + i2 + " should not exceed maximal supported number of threads 2097150").toString());
        }
        if (j > 0) {
            this.i0 = new pg1();
            this.j0 = new pg1();
            this.parkedWorkersStack = 0L;
            this.k0 = new AtomicReferenceArray<>(i2 + 1);
            this.controlState = i << 42;
            this._isTerminated = 0;
            return;
        }
        throw new IllegalArgumentException(("Idle worker keep alive time " + j + " must be positive").toString());
    }

    public static /* synthetic */ void f(CoroutineScheduler coroutineScheduler, Runnable runnable, p34 p34Var, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            p34Var = ug2.a;
        }
        if ((i & 4) != 0) {
            z = false;
        }
        coroutineScheduler.e(runnable, p34Var, z);
    }

    public static /* synthetic */ boolean v(CoroutineScheduler coroutineScheduler, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            j = coroutineScheduler.controlState;
        }
        return coroutineScheduler.u(j);
    }

    public final boolean a(k34 k34Var) {
        if (k34Var.f0.e() == 1) {
            return this.j0.a(k34Var);
        }
        return this.i0.a(k34Var);
    }

    public final int b() {
        synchronized (this.k0) {
            if (isTerminated()) {
                return -1;
            }
            long j = this.controlState;
            int i = (int) (j & 2097151);
            int b2 = u33.b(i - ((int) ((j & 4398044413952L) >> 21)), 0);
            if (b2 >= this.a) {
                return 0;
            }
            if (i >= this.f0) {
                return 0;
            }
            int i2 = ((int) (this.controlState & 2097151)) + 1;
            if (i2 > 0 && this.k0.get(i2) == null) {
                c cVar = new c(i2);
                this.k0.set(i2, cVar);
                if (i2 == ((int) (2097151 & m0.incrementAndGet(this)))) {
                    cVar.start();
                    return b2 + 1;
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    public final k34 c(Runnable runnable, p34 p34Var) {
        long a2 = w34.e.a();
        if (runnable instanceof k34) {
            k34 k34Var = (k34) runnable;
            k34Var.a = a2;
            k34Var.f0 = p34Var;
            return k34Var;
        }
        return new u34(runnable, a2, p34Var);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        m(10000L);
    }

    public final c d() {
        Thread currentThread = Thread.currentThread();
        c cVar = currentThread instanceof c ? (c) currentThread : null;
        if (cVar != null && fs1.b(CoroutineScheduler.this, this)) {
            return cVar;
        }
        return null;
    }

    public final void e(Runnable runnable, p34 p34Var, boolean z) {
        n5.a();
        k34 c2 = c(runnable, p34Var);
        c d = d();
        k34 r = r(d, c2, z);
        if (r != null && !a(r)) {
            throw new RejectedExecutionException(fs1.l(this.h0, " was terminated"));
        }
        boolean z2 = z && d != null;
        if (c2.f0.e() != 0) {
            n(z2);
        } else if (z2) {
        } else {
            q();
        }
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        f(this, runnable, null, false, 6, null);
    }

    public final int g(c cVar) {
        Object g = cVar.g();
        while (g != o0) {
            if (g == null) {
                return 0;
            }
            c cVar2 = (c) g;
            int f = cVar2.f();
            if (f != 0) {
                return f;
            }
            g = cVar2.g();
        }
        return -1;
    }

    public final c h() {
        while (true) {
            long j = this.parkedWorkersStack;
            c cVar = this.k0.get((int) (2097151 & j));
            if (cVar == null) {
                return null;
            }
            long j2 = (2097152 + j) & (-2097152);
            int g = g(cVar);
            if (g >= 0 && l0.compareAndSet(this, j, g | j2)) {
                cVar.o(o0);
                return cVar;
            }
        }
    }

    public final boolean i(c cVar) {
        long j;
        long j2;
        int f;
        if (cVar.g() != o0) {
            return false;
        }
        do {
            j = this.parkedWorkersStack;
            int i = (int) (2097151 & j);
            j2 = (2097152 + j) & (-2097152);
            f = cVar.f();
            if (ze0.a()) {
                if (!(f != 0)) {
                    throw new AssertionError();
                }
            }
            cVar.o(this.k0.get(i));
        } while (!l0.compareAndSet(this, j, f | j2));
        return true;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    public final boolean isTerminated() {
        return this._isTerminated;
    }

    public final void j(c cVar, int i, int i2) {
        while (true) {
            long j = this.parkedWorkersStack;
            int i3 = (int) (2097151 & j);
            long j2 = (2097152 + j) & (-2097152);
            if (i3 == i) {
                i3 = i2 == 0 ? g(cVar) : i2;
            }
            if (i3 >= 0 && l0.compareAndSet(this, j, j2 | i3)) {
                return;
            }
        }
    }

    public final void l(k34 k34Var) {
        try {
            k34Var.run();
        } finally {
            try {
            } finally {
            }
        }
    }

    public final void m(long j) {
        int i;
        if (n0.compareAndSet(this, 0, 1)) {
            c d = d();
            synchronized (this.k0) {
                i = (int) (this.controlState & 2097151);
            }
            if (1 <= i) {
                int i2 = 1;
                while (true) {
                    int i3 = i2 + 1;
                    c cVar = this.k0.get(i2);
                    fs1.d(cVar);
                    if (cVar != d) {
                        while (cVar.isAlive()) {
                            LockSupport.unpark(cVar);
                            cVar.join(j);
                        }
                        WorkerState workerState = cVar.f0;
                        if (ze0.a()) {
                            if (!(workerState == WorkerState.TERMINATED)) {
                                throw new AssertionError();
                            }
                        }
                        cVar.a.g(this.j0);
                    }
                    if (i2 == i) {
                        break;
                    }
                    i2 = i3;
                }
            }
            this.j0.b();
            this.i0.b();
            while (true) {
                k34 e = d == null ? null : d.e(true);
                if (e == null) {
                    e = this.i0.d();
                }
                if (e == null && (e = this.j0.d()) == null) {
                    break;
                }
                l(e);
            }
            if (d != null) {
                d.r(WorkerState.TERMINATED);
            }
            if (ze0.a()) {
                if (!(((int) ((this.controlState & 9223367638808264704L) >> 42)) == this.a)) {
                    throw new AssertionError();
                }
            }
            this.parkedWorkersStack = 0L;
            this.controlState = 0L;
        }
    }

    public final void n(boolean z) {
        long addAndGet = m0.addAndGet(this, 2097152L);
        if (z || w() || u(addAndGet)) {
            return;
        }
        w();
    }

    public final void q() {
        if (w() || v(this, 0L, 1, null)) {
            return;
        }
        w();
    }

    public final k34 r(c cVar, k34 k34Var, boolean z) {
        if (cVar == null || cVar.f0 == WorkerState.TERMINATED) {
            return k34Var;
        }
        if (k34Var.f0.e() == 0 && cVar.f0 == WorkerState.BLOCKING) {
            return k34Var;
        }
        cVar.j0 = true;
        return cVar.a.a(k34Var, z);
    }

    public String toString() {
        int i;
        int i2;
        int i3;
        int i4;
        ArrayList arrayList = new ArrayList();
        int length = this.k0.length();
        int i5 = 0;
        if (1 < length) {
            i2 = 0;
            int i6 = 0;
            i3 = 0;
            i4 = 0;
            int i7 = 1;
            while (true) {
                int i8 = i7 + 1;
                c cVar = this.k0.get(i7);
                if (cVar != null) {
                    int f = cVar.a.f();
                    int i9 = b.a[cVar.f0.ordinal()];
                    if (i9 == 1) {
                        i5++;
                    } else if (i9 == 2) {
                        i2++;
                        StringBuilder sb = new StringBuilder();
                        sb.append(f);
                        sb.append('b');
                        arrayList.add(sb.toString());
                    } else if (i9 == 3) {
                        i6++;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(f);
                        sb2.append('c');
                        arrayList.add(sb2.toString());
                    } else if (i9 == 4) {
                        i3++;
                        if (f > 0) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(f);
                            sb3.append('d');
                            arrayList.add(sb3.toString());
                        }
                    } else if (i9 == 5) {
                        i4++;
                    }
                }
                if (i8 >= length) {
                    break;
                }
                i7 = i8;
            }
            i = i5;
            i5 = i6;
        } else {
            i = 0;
            i2 = 0;
            i3 = 0;
            i4 = 0;
        }
        long j = this.controlState;
        return this.h0 + '@' + ff0.b(this) + "[Pool Size {core = " + this.a + ", max = " + this.f0 + "}, Worker States {CPU = " + i5 + ", blocking = " + i2 + ", parked = " + i + ", dormant = " + i3 + ", terminated = " + i4 + "}, running workers queues = " + arrayList + ", global CPU queue size = " + this.i0.c() + ", global blocking queue size = " + this.j0.c() + ", Control State {created workers= " + ((int) (2097151 & j)) + ", blocking tasks = " + ((int) ((4398044413952L & j) >> 21)) + ", CPUs acquired = " + (this.a - ((int) ((9223367638808264704L & j) >> 42))) + "}]";
    }

    public final boolean u(long j) {
        if (u33.b(((int) (2097151 & j)) - ((int) ((j & 4398044413952L) >> 21)), 0) < this.a) {
            int b2 = b();
            if (b2 == 1 && this.a > 1) {
                b();
            }
            if (b2 > 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean w() {
        c h;
        do {
            h = h();
            if (h == null) {
                return false;
            }
        } while (!c.l0.compareAndSet(h, -1, 0));
        LockSupport.unpark(h);
        return true;
    }

    /* compiled from: CoroutineScheduler.kt */
    /* loaded from: classes2.dex */
    public final class c extends Thread {
        public static final /* synthetic */ AtomicIntegerFieldUpdater l0 = AtomicIntegerFieldUpdater.newUpdater(c.class, "workerCtl");
        public final qq4 a;
        public WorkerState f0;
        public long g0;
        public long h0;
        public int i0;
        private volatile int indexInArray;
        public boolean j0;
        private volatile Object nextParkedWorker;
        public volatile /* synthetic */ int workerCtl;

        public c() {
            setDaemon(true);
            this.a = new qq4();
            this.f0 = WorkerState.DORMANT;
            this.workerCtl = 0;
            this.nextParkedWorker = CoroutineScheduler.o0;
            this.i0 = Random.Default.nextInt();
        }

        public final void a(int i) {
            if (i == 0) {
                return;
            }
            CoroutineScheduler.m0.addAndGet(CoroutineScheduler.this, -2097152L);
            WorkerState workerState = this.f0;
            if (workerState != WorkerState.TERMINATED) {
                if (ze0.a()) {
                    if (!(workerState == WorkerState.BLOCKING)) {
                        throw new AssertionError();
                    }
                }
                this.f0 = WorkerState.DORMANT;
            }
        }

        public final void b(int i) {
            if (i != 0 && r(WorkerState.BLOCKING)) {
                CoroutineScheduler.this.q();
            }
        }

        public final void c(k34 k34Var) {
            int e = k34Var.f0.e();
            h(e);
            b(e);
            CoroutineScheduler.this.l(k34Var);
            a(e);
        }

        public final k34 d(boolean z) {
            k34 l;
            k34 l2;
            if (z) {
                boolean z2 = j(CoroutineScheduler.this.a * 2) == 0;
                if (z2 && (l2 = l()) != null) {
                    return l2;
                }
                k34 h = this.a.h();
                if (h != null) {
                    return h;
                }
                if (!z2 && (l = l()) != null) {
                    return l;
                }
            } else {
                k34 l3 = l();
                if (l3 != null) {
                    return l3;
                }
            }
            return s(false);
        }

        public final k34 e(boolean z) {
            k34 d;
            if (p()) {
                return d(z);
            }
            if (z) {
                d = this.a.h();
                if (d == null) {
                    d = CoroutineScheduler.this.j0.d();
                }
            } else {
                d = CoroutineScheduler.this.j0.d();
            }
            return d == null ? s(true) : d;
        }

        public final int f() {
            return this.indexInArray;
        }

        public final Object g() {
            return this.nextParkedWorker;
        }

        public final void h(int i) {
            this.g0 = 0L;
            if (this.f0 == WorkerState.PARKING) {
                if (ze0.a()) {
                    if (!(i == 1)) {
                        throw new AssertionError();
                    }
                }
                this.f0 = WorkerState.BLOCKING;
            }
        }

        public final boolean i() {
            return this.nextParkedWorker != CoroutineScheduler.o0;
        }

        public final int j(int i) {
            int i2 = this.i0;
            int i3 = i2 ^ (i2 << 13);
            int i4 = i3 ^ (i3 >> 17);
            int i5 = i4 ^ (i4 << 5);
            this.i0 = i5;
            int i6 = i - 1;
            return (i6 & i) == 0 ? i5 & i6 : (i5 & Integer.MAX_VALUE) % i;
        }

        public final void k() {
            if (this.g0 == 0) {
                this.g0 = System.nanoTime() + CoroutineScheduler.this.g0;
            }
            LockSupport.parkNanos(CoroutineScheduler.this.g0);
            if (System.nanoTime() - this.g0 >= 0) {
                this.g0 = 0L;
                t();
            }
        }

        public final k34 l() {
            if (j(2) == 0) {
                k34 d = CoroutineScheduler.this.i0.d();
                return d == null ? CoroutineScheduler.this.j0.d() : d;
            }
            k34 d2 = CoroutineScheduler.this.j0.d();
            return d2 == null ? CoroutineScheduler.this.i0.d() : d2;
        }

        public final void m() {
            loop0: while (true) {
                boolean z = false;
                while (!CoroutineScheduler.this.isTerminated() && this.f0 != WorkerState.TERMINATED) {
                    k34 e = e(this.j0);
                    if (e != null) {
                        this.h0 = 0L;
                        c(e);
                    } else {
                        this.j0 = false;
                        if (this.h0 == 0) {
                            q();
                        } else if (z) {
                            r(WorkerState.PARKING);
                            Thread.interrupted();
                            LockSupport.parkNanos(this.h0);
                            this.h0 = 0L;
                        } else {
                            z = true;
                        }
                    }
                }
            }
            r(WorkerState.TERMINATED);
        }

        public final void n(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append(CoroutineScheduler.this.h0);
            sb.append("-worker-");
            sb.append(i == 0 ? "TERMINATED" : String.valueOf(i));
            setName(sb.toString());
            this.indexInArray = i;
        }

        public final void o(Object obj) {
            this.nextParkedWorker = obj;
        }

        public final boolean p() {
            boolean z;
            if (this.f0 != WorkerState.CPU_ACQUIRED) {
                CoroutineScheduler coroutineScheduler = CoroutineScheduler.this;
                while (true) {
                    long j = coroutineScheduler.controlState;
                    if (((int) ((9223367638808264704L & j) >> 42)) != 0) {
                        if (CoroutineScheduler.m0.compareAndSet(coroutineScheduler, j, j - 4398046511104L)) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    return false;
                }
                this.f0 = WorkerState.CPU_ACQUIRED;
            }
            return true;
        }

        public final void q() {
            if (!i()) {
                CoroutineScheduler.this.i(this);
                return;
            }
            if (ze0.a()) {
                if (!(this.a.f() == 0)) {
                    throw new AssertionError();
                }
            }
            this.workerCtl = -1;
            while (i() && this.workerCtl == -1 && !CoroutineScheduler.this.isTerminated() && this.f0 != WorkerState.TERMINATED) {
                r(WorkerState.PARKING);
                Thread.interrupted();
                k();
            }
        }

        public final boolean r(WorkerState workerState) {
            WorkerState workerState2 = this.f0;
            boolean z = workerState2 == WorkerState.CPU_ACQUIRED;
            if (z) {
                CoroutineScheduler.m0.addAndGet(CoroutineScheduler.this, 4398046511104L);
            }
            if (workerState2 != workerState) {
                this.f0 = workerState;
            }
            return z;
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            m();
        }

        public final k34 s(boolean z) {
            long l;
            if (ze0.a()) {
                if (!(this.a.f() == 0)) {
                    throw new AssertionError();
                }
            }
            int i = (int) (CoroutineScheduler.this.controlState & 2097151);
            if (i < 2) {
                return null;
            }
            int j = j(i);
            CoroutineScheduler coroutineScheduler = CoroutineScheduler.this;
            long j2 = Long.MAX_VALUE;
            for (int i2 = 0; i2 < i; i2++) {
                j++;
                if (j > i) {
                    j = 1;
                }
                c cVar = coroutineScheduler.k0.get(j);
                if (cVar != null && cVar != this) {
                    if (ze0.a()) {
                        if (!(this.a.f() == 0)) {
                            throw new AssertionError();
                        }
                    }
                    if (z) {
                        l = this.a.k(cVar.a);
                    } else {
                        l = this.a.l(cVar.a);
                    }
                    if (l == -1) {
                        return this.a.h();
                    }
                    if (l > 0) {
                        j2 = Math.min(j2, l);
                    }
                }
            }
            if (j2 == Long.MAX_VALUE) {
                j2 = 0;
            }
            this.h0 = j2;
            return null;
        }

        public final void t() {
            CoroutineScheduler coroutineScheduler = CoroutineScheduler.this;
            synchronized (coroutineScheduler.k0) {
                if (coroutineScheduler.isTerminated()) {
                    return;
                }
                if (((int) (coroutineScheduler.controlState & 2097151)) <= coroutineScheduler.a) {
                    return;
                }
                if (l0.compareAndSet(this, -1, 1)) {
                    int f = f();
                    n(0);
                    coroutineScheduler.j(this, f, 0);
                    int andDecrement = (int) (CoroutineScheduler.m0.getAndDecrement(coroutineScheduler) & 2097151);
                    if (andDecrement != f) {
                        c cVar = coroutineScheduler.k0.get(andDecrement);
                        fs1.d(cVar);
                        coroutineScheduler.k0.set(f, cVar);
                        cVar.n(f);
                        coroutineScheduler.j(cVar, andDecrement, f);
                    }
                    coroutineScheduler.k0.set(andDecrement, null);
                    te4 te4Var = te4.a;
                    this.f0 = WorkerState.TERMINATED;
                }
            }
        }

        public c(int i) {
            this();
            n(i);
        }
    }
}
