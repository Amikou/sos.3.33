package wallet.core.jni;

import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes3.dex */
public enum CoinType {
    AETERNITY(457),
    AION(425),
    BINANCE(714),
    BITCOIN(0),
    BITCOINCASH(145),
    BITCOINGOLD(156),
    CALLISTO(820),
    CARDANO(1815),
    COSMOS(118),
    DASH(5),
    DECRED(42),
    DIGIBYTE(20),
    DOGECOIN(3),
    EOS(194),
    ETHEREUM(60),
    ETHEREUMCLASSIC(61),
    FIO(235),
    GOCHAIN(6060),
    GROESTLCOIN(17),
    ICON(74),
    IOTEX(304),
    KAVA(459),
    KIN(2017),
    LITECOIN(2),
    MONACOIN(22),
    NEBULAS(2718),
    NULS(8964),
    NANO(165),
    NEAR(397),
    NIMIQ(242),
    ONTOLOGY(RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE),
    POANETWORK(178),
    QTUM(2301),
    XRP(144),
    SOLANA(501),
    STELLAR(148),
    TON(396),
    TEZOS(1729),
    THETA(500),
    THUNDERTOKEN(1001),
    NEO(888),
    TOMOCHAIN(889),
    TRON(195),
    VECHAIN(818),
    VIACOIN(14),
    WANCHAIN(5718350),
    ZCASH(133),
    ZCOIN(136),
    ZILLIQA(313),
    ZELCASH(19167),
    RAVENCOIN(175),
    WAVES(5741564),
    TERRA(330),
    HARMONY(1023),
    ALGORAND(283),
    KUSAMA(434),
    POLKADOT(354),
    FILECOIN(461),
    ELROND(508),
    BANDCHAIN(494),
    SMARTCHAINLEGACY(10000714),
    SMARTCHAIN(20000714),
    OASIS(474),
    POLYGON(966);
    
    private final int value;

    CoinType(int i) {
        this.value = i;
    }

    public static CoinType createFromValue(int i) {
        switch (i) {
            case 0:
                return BITCOIN;
            case 2:
                return LITECOIN;
            case 3:
                return DOGECOIN;
            case 5:
                return DASH;
            case 14:
                return VIACOIN;
            case 17:
                return GROESTLCOIN;
            case 20:
                return DIGIBYTE;
            case 22:
                return MONACOIN;
            case 42:
                return DECRED;
            case 60:
                return ETHEREUM;
            case 61:
                return ETHEREUMCLASSIC;
            case 74:
                return ICON;
            case 118:
                return COSMOS;
            case 133:
                return ZCASH;
            case 136:
                return ZCOIN;
            case 144:
                return XRP;
            case 145:
                return BITCOINCASH;
            case 148:
                return STELLAR;
            case 156:
                return BITCOINGOLD;
            case 165:
                return NANO;
            case 175:
                return RAVENCOIN;
            case 178:
                return POANETWORK;
            case 194:
                return EOS;
            case 195:
                return TRON;
            case 235:
                return FIO;
            case 242:
                return NIMIQ;
            case 283:
                return ALGORAND;
            case 304:
                return IOTEX;
            case 313:
                return ZILLIQA;
            case 330:
                return TERRA;
            case 354:
                return POLKADOT;
            case 396:
                return TON;
            case 397:
                return NEAR;
            case 425:
                return AION;
            case 434:
                return KUSAMA;
            case 457:
                return AETERNITY;
            case 459:
                return KAVA;
            case 461:
                return FILECOIN;
            case 474:
                return OASIS;
            case 494:
                return BANDCHAIN;
            case 500:
                return THETA;
            case 501:
                return SOLANA;
            case 508:
                return ELROND;
            case 714:
                return BINANCE;
            case 818:
                return VECHAIN;
            case 820:
                return CALLISTO;
            case 888:
                return NEO;
            case 889:
                return TOMOCHAIN;
            case 966:
                return POLYGON;
            case 1001:
                return THUNDERTOKEN;
            case 1023:
                return HARMONY;
            case RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE /* 1024 */:
                return ONTOLOGY;
            case 1729:
                return TEZOS;
            case 1815:
                return CARDANO;
            case 2017:
                return KIN;
            case 2301:
                return QTUM;
            case 2718:
                return NEBULAS;
            case 6060:
                return GOCHAIN;
            case 8964:
                return NULS;
            case 19167:
                return ZELCASH;
            case 5718350:
                return WANCHAIN;
            case 5741564:
                return WAVES;
            case 10000714:
                return SMARTCHAINLEGACY;
            case 20000714:
                return SMARTCHAIN;
            default:
                return null;
        }
    }

    public native Blockchain blockchain();

    public native Curve curve();

    public native String derivationPath();

    public native String deriveAddress(PrivateKey privateKey);

    public native String deriveAddressFromPublicKey(PublicKey publicKey);

    public native HRP hrp();

    public native byte p2pkhPrefix();

    public native byte p2shPrefix();

    public native Purpose purpose();

    public native int slip44Id();

    public native byte staticPrefix();

    public native boolean validate(String str);

    public int value() {
        return this.value;
    }

    public native HDVersion xprvVersion();

    public native HDVersion xpubVersion();
}
