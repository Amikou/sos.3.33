package wallet.core.jni;

/* loaded from: classes3.dex */
public class Mnemonic {
    private long nativeHandle = 0;

    private Mnemonic() {
    }

    public static Mnemonic createFromNative(long j) {
        Mnemonic mnemonic = new Mnemonic();
        mnemonic.nativeHandle = j;
        return mnemonic;
    }

    public static native boolean isValid(String str);

    public static native boolean isValidWord(String str);

    public static native String suggest(String str);
}
