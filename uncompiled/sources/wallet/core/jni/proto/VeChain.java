package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class VeChain {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\rVeChain.proto\u0012\u0010TW.VeChain.Proto\"1\n\u0006Clause\u0012\n\n\u0002to\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\f\u0012\f\n\u0004data\u0018\u0003 \u0001(\f\"Ð\u0001\n\fSigningInput\u0012\u0011\n\tchain_tag\u0018\u0001 \u0001(\r\u0012\u0011\n\tblock_ref\u0018\u0002 \u0001(\u0004\u0012\u0012\n\nexpiration\u0018\u0003 \u0001(\r\u0012)\n\u0007clauses\u0018\u0004 \u0003(\u000b2\u0018.TW.VeChain.Proto.Clause\u0012\u0016\n\u000egas_price_coef\u0018\u0005 \u0001(\r\u0012\u000b\n\u0003gas\u0018\u0006 \u0001(\u0004\u0012\u0012\n\ndepends_on\u0018\u0007 \u0001(\f\u0012\r\n\u0005nonce\u0018\b \u0001(\u0004\u0012\u0013\n\u000bprivate_key\u0018\t \u0001(\f\"3\n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f\u0012\u0011\n\tsignature\u0018\u0002 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_VeChain_Proto_Clause_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_VeChain_Proto_Clause_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_VeChain_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_VeChain_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_VeChain_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_VeChain_Proto_SigningOutput_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class Clause extends GeneratedMessageV3 implements ClauseOrBuilder {
        public static final int DATA_FIELD_NUMBER = 3;
        private static final Clause DEFAULT_INSTANCE = new Clause();
        private static final t0<Clause> PARSER = new c<Clause>() { // from class: wallet.core.jni.proto.VeChain.Clause.1
            @Override // com.google.protobuf.t0
            public Clause parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Clause(jVar, rVar);
            }
        };
        public static final int TO_FIELD_NUMBER = 1;
        public static final int VALUE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString data_;
        private byte memoizedIsInitialized;
        private volatile Object to_;
        private ByteString value_;

        public static Clause getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return VeChain.internal_static_TW_VeChain_Proto_Clause_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Clause parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Clause) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Clause parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Clause> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Clause)) {
                return super.equals(obj);
            }
            Clause clause = (Clause) obj;
            return getTo().equals(clause.getTo()) && getValue().equals(clause.getValue()) && getData().equals(clause.getData()) && this.unknownFields.equals(clause.unknownFields);
        }

        @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
        public ByteString getData() {
            return this.data_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Clause> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getToBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.to_);
            if (!this.value_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(2, this.value_);
            }
            if (!this.data_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.data_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
        public String getTo() {
            Object obj = this.to_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.to_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
        public ByteString getToBytes() {
            Object obj = this.to_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.to_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
        public ByteString getValue() {
            return this.value_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getTo().hashCode()) * 37) + 2) * 53) + getValue().hashCode()) * 37) + 3) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return VeChain.internal_static_TW_VeChain_Proto_Clause_fieldAccessorTable.d(Clause.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Clause();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getToBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.to_);
            }
            if (!this.value_.isEmpty()) {
                codedOutputStream.q0(2, this.value_);
            }
            if (!this.data_.isEmpty()) {
                codedOutputStream.q0(3, this.data_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements ClauseOrBuilder {
            private ByteString data_;
            private Object to_;
            private ByteString value_;

            public static final Descriptors.b getDescriptor() {
                return VeChain.internal_static_TW_VeChain_Proto_Clause_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearData() {
                this.data_ = Clause.getDefaultInstance().getData();
                onChanged();
                return this;
            }

            public Builder clearTo() {
                this.to_ = Clause.getDefaultInstance().getTo();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = Clause.getDefaultInstance().getValue();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return VeChain.internal_static_TW_VeChain_Proto_Clause_descriptor;
            }

            @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.to_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.VeChain.ClauseOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return VeChain.internal_static_TW_VeChain_Proto_Clause_fieldAccessorTable.d(Clause.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setData(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.data_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTo(String str) {
                Objects.requireNonNull(str);
                this.to_ = str;
                onChanged();
                return this;
            }

            public Builder setToBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.to_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.value_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.to_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.value_ = byteString;
                this.data_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Clause build() {
                Clause buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Clause buildPartial() {
                Clause clause = new Clause(this);
                clause.to_ = this.to_;
                clause.value_ = this.value_;
                clause.data_ = this.data_;
                onBuilt();
                return clause;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Clause getDefaultInstanceForType() {
                return Clause.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.to_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.value_ = byteString;
                this.data_ = byteString;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Clause) {
                    return mergeFrom((Clause) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.to_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.value_ = byteString;
                this.data_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(Clause clause) {
                if (clause == Clause.getDefaultInstance()) {
                    return this;
                }
                if (!clause.getTo().isEmpty()) {
                    this.to_ = clause.to_;
                    onChanged();
                }
                ByteString value = clause.getValue();
                ByteString byteString = ByteString.EMPTY;
                if (value != byteString) {
                    setValue(clause.getValue());
                }
                if (clause.getData() != byteString) {
                    setData(clause.getData());
                }
                mergeUnknownFields(clause.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.VeChain.Clause.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.VeChain.Clause.access$1000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.VeChain$Clause r3 = (wallet.core.jni.proto.VeChain.Clause) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.VeChain$Clause r4 = (wallet.core.jni.proto.VeChain.Clause) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.VeChain.Clause.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.VeChain$Clause$Builder");
            }
        }

        public static Builder newBuilder(Clause clause) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(clause);
        }

        public static Clause parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Clause(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Clause parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Clause) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Clause parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Clause getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static Clause parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Clause() {
            this.memoizedIsInitialized = (byte) -1;
            this.to_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.value_ = byteString;
            this.data_ = byteString;
        }

        public static Clause parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static Clause parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Clause parseFrom(InputStream inputStream) throws IOException {
            return (Clause) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static Clause parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Clause) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private Clause(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.to_ = jVar.I();
                            } else if (J == 18) {
                                this.value_ = jVar.q();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.data_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Clause parseFrom(j jVar) throws IOException {
            return (Clause) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Clause parseFrom(j jVar, r rVar) throws IOException {
            return (Clause) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface ClauseOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getData();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getTo();

        ByteString getToBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        ByteString getValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int BLOCK_REF_FIELD_NUMBER = 2;
        public static final int CHAIN_TAG_FIELD_NUMBER = 1;
        public static final int CLAUSES_FIELD_NUMBER = 4;
        public static final int DEPENDS_ON_FIELD_NUMBER = 7;
        public static final int EXPIRATION_FIELD_NUMBER = 3;
        public static final int GAS_FIELD_NUMBER = 6;
        public static final int GAS_PRICE_COEF_FIELD_NUMBER = 5;
        public static final int NONCE_FIELD_NUMBER = 8;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 9;
        private static final long serialVersionUID = 0;
        private long blockRef_;
        private int chainTag_;
        private List<Clause> clauses_;
        private ByteString dependsOn_;
        private int expiration_;
        private int gasPriceCoef_;
        private long gas_;
        private byte memoizedIsInitialized;
        private long nonce_;
        private ByteString privateKey_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.VeChain.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return VeChain.internal_static_TW_VeChain_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            return getChainTag() == signingInput.getChainTag() && getBlockRef() == signingInput.getBlockRef() && getExpiration() == signingInput.getExpiration() && getClausesList().equals(signingInput.getClausesList()) && getGasPriceCoef() == signingInput.getGasPriceCoef() && getGas() == signingInput.getGas() && getDependsOn().equals(signingInput.getDependsOn()) && getNonce() == signingInput.getNonce() && getPrivateKey().equals(signingInput.getPrivateKey()) && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public long getBlockRef() {
            return this.blockRef_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public int getChainTag() {
            return this.chainTag_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public Clause getClauses(int i) {
            return this.clauses_.get(i);
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public int getClausesCount() {
            return this.clauses_.size();
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public List<Clause> getClausesList() {
            return this.clauses_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public ClauseOrBuilder getClausesOrBuilder(int i) {
            return this.clauses_.get(i);
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public List<? extends ClauseOrBuilder> getClausesOrBuilderList() {
            return this.clauses_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public ByteString getDependsOn() {
            return this.dependsOn_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public int getExpiration() {
            return this.expiration_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public long getGas() {
            return this.gas_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public int getGasPriceCoef() {
            return this.gasPriceCoef_;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.chainTag_;
            int Y = i2 != 0 ? CodedOutputStream.Y(1, i2) + 0 : 0;
            long j = this.blockRef_;
            if (j != 0) {
                Y += CodedOutputStream.a0(2, j);
            }
            int i3 = this.expiration_;
            if (i3 != 0) {
                Y += CodedOutputStream.Y(3, i3);
            }
            for (int i4 = 0; i4 < this.clauses_.size(); i4++) {
                Y += CodedOutputStream.G(4, this.clauses_.get(i4));
            }
            int i5 = this.gasPriceCoef_;
            if (i5 != 0) {
                Y += CodedOutputStream.Y(5, i5);
            }
            long j2 = this.gas_;
            if (j2 != 0) {
                Y += CodedOutputStream.a0(6, j2);
            }
            if (!this.dependsOn_.isEmpty()) {
                Y += CodedOutputStream.h(7, this.dependsOn_);
            }
            long j3 = this.nonce_;
            if (j3 != 0) {
                Y += CodedOutputStream.a0(8, j3);
            }
            if (!this.privateKey_.isEmpty()) {
                Y += CodedOutputStream.h(9, this.privateKey_);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getChainTag()) * 37) + 2) * 53) + a0.h(getBlockRef())) * 37) + 3) * 53) + getExpiration();
            if (getClausesCount() > 0) {
                hashCode = (((hashCode * 37) + 4) * 53) + getClausesList().hashCode();
            }
            int gasPriceCoef = (((((((((((((((((((((hashCode * 37) + 5) * 53) + getGasPriceCoef()) * 37) + 6) * 53) + a0.h(getGas())) * 37) + 7) * 53) + getDependsOn().hashCode()) * 37) + 8) * 53) + a0.h(getNonce())) * 37) + 9) * 53) + getPrivateKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = gasPriceCoef;
            return gasPriceCoef;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return VeChain.internal_static_TW_VeChain_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.chainTag_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            long j = this.blockRef_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            int i2 = this.expiration_;
            if (i2 != 0) {
                codedOutputStream.b1(3, i2);
            }
            for (int i3 = 0; i3 < this.clauses_.size(); i3++) {
                codedOutputStream.K0(4, this.clauses_.get(i3));
            }
            int i4 = this.gasPriceCoef_;
            if (i4 != 0) {
                codedOutputStream.b1(5, i4);
            }
            long j2 = this.gas_;
            if (j2 != 0) {
                codedOutputStream.d1(6, j2);
            }
            if (!this.dependsOn_.isEmpty()) {
                codedOutputStream.q0(7, this.dependsOn_);
            }
            long j3 = this.nonce_;
            if (j3 != 0) {
                codedOutputStream.d1(8, j3);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(9, this.privateKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private int bitField0_;
            private long blockRef_;
            private int chainTag_;
            private x0<Clause, Clause.Builder, ClauseOrBuilder> clausesBuilder_;
            private List<Clause> clauses_;
            private ByteString dependsOn_;
            private int expiration_;
            private int gasPriceCoef_;
            private long gas_;
            private long nonce_;
            private ByteString privateKey_;

            private void ensureClausesIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.clauses_ = new ArrayList(this.clauses_);
                    this.bitField0_ |= 1;
                }
            }

            private x0<Clause, Clause.Builder, ClauseOrBuilder> getClausesFieldBuilder() {
                if (this.clausesBuilder_ == null) {
                    this.clausesBuilder_ = new x0<>(this.clauses_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.clauses_ = null;
                }
                return this.clausesBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return VeChain.internal_static_TW_VeChain_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getClausesFieldBuilder();
                }
            }

            public Builder addAllClauses(Iterable<? extends Clause> iterable) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    ensureClausesIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.clauses_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addClauses(Clause clause) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(clause);
                    ensureClausesIsMutable();
                    this.clauses_.add(clause);
                    onChanged();
                } else {
                    x0Var.f(clause);
                }
                return this;
            }

            public Clause.Builder addClausesBuilder() {
                return getClausesFieldBuilder().d(Clause.getDefaultInstance());
            }

            public Builder clearBlockRef() {
                this.blockRef_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearChainTag() {
                this.chainTag_ = 0;
                onChanged();
                return this;
            }

            public Builder clearClauses() {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    this.clauses_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearDependsOn() {
                this.dependsOn_ = SigningInput.getDefaultInstance().getDependsOn();
                onChanged();
                return this;
            }

            public Builder clearExpiration() {
                this.expiration_ = 0;
                onChanged();
                return this;
            }

            public Builder clearGas() {
                this.gas_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasPriceCoef() {
                this.gasPriceCoef_ = 0;
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public long getBlockRef() {
                return this.blockRef_;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public int getChainTag() {
                return this.chainTag_;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public Clause getClauses(int i) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    return this.clauses_.get(i);
                }
                return x0Var.o(i);
            }

            public Clause.Builder getClausesBuilder(int i) {
                return getClausesFieldBuilder().l(i);
            }

            public List<Clause.Builder> getClausesBuilderList() {
                return getClausesFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public int getClausesCount() {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    return this.clauses_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public List<Clause> getClausesList() {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.clauses_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public ClauseOrBuilder getClausesOrBuilder(int i) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    return this.clauses_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public List<? extends ClauseOrBuilder> getClausesOrBuilderList() {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.clauses_);
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public ByteString getDependsOn() {
                return this.dependsOn_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return VeChain.internal_static_TW_VeChain_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public int getExpiration() {
                return this.expiration_;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public long getGas() {
                return this.gas_;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public int getGasPriceCoef() {
                return this.gasPriceCoef_;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return VeChain.internal_static_TW_VeChain_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeClauses(int i) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    ensureClausesIsMutable();
                    this.clauses_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setBlockRef(long j) {
                this.blockRef_ = j;
                onChanged();
                return this;
            }

            public Builder setChainTag(int i) {
                this.chainTag_ = i;
                onChanged();
                return this;
            }

            public Builder setClauses(int i, Clause clause) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(clause);
                    ensureClausesIsMutable();
                    this.clauses_.set(i, clause);
                    onChanged();
                } else {
                    x0Var.x(i, clause);
                }
                return this;
            }

            public Builder setDependsOn(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.dependsOn_ = byteString;
                onChanged();
                return this;
            }

            public Builder setExpiration(int i) {
                this.expiration_ = i;
                onChanged();
                return this;
            }

            public Builder setGas(long j) {
                this.gas_ = j;
                onChanged();
                return this;
            }

            public Builder setGasPriceCoef(int i) {
                this.gasPriceCoef_ = i;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.clauses_ = Collections.emptyList();
                ByteString byteString = ByteString.EMPTY;
                this.dependsOn_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                signingInput.chainTag_ = this.chainTag_;
                signingInput.blockRef_ = this.blockRef_;
                signingInput.expiration_ = this.expiration_;
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var != null) {
                    signingInput.clauses_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.clauses_ = Collections.unmodifiableList(this.clauses_);
                        this.bitField0_ &= -2;
                    }
                    signingInput.clauses_ = this.clauses_;
                }
                signingInput.gasPriceCoef_ = this.gasPriceCoef_;
                signingInput.gas_ = this.gas_;
                signingInput.dependsOn_ = this.dependsOn_;
                signingInput.nonce_ = this.nonce_;
                signingInput.privateKey_ = this.privateKey_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public Clause.Builder addClausesBuilder(int i) {
                return getClausesFieldBuilder().c(i, Clause.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.chainTag_ = 0;
                this.blockRef_ = 0L;
                this.expiration_ = 0;
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    this.clauses_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                this.gasPriceCoef_ = 0;
                this.gas_ = 0L;
                ByteString byteString = ByteString.EMPTY;
                this.dependsOn_ = byteString;
                this.nonce_ = 0L;
                this.privateKey_ = byteString;
                return this;
            }

            public Builder addClauses(int i, Clause clause) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(clause);
                    ensureClausesIsMutable();
                    this.clauses_.add(i, clause);
                    onChanged();
                } else {
                    x0Var.e(i, clause);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setClauses(int i, Clause.Builder builder) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    ensureClausesIsMutable();
                    this.clauses_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.clauses_ = Collections.emptyList();
                ByteString byteString = ByteString.EMPTY;
                this.dependsOn_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getChainTag() != 0) {
                    setChainTag(signingInput.getChainTag());
                }
                if (signingInput.getBlockRef() != 0) {
                    setBlockRef(signingInput.getBlockRef());
                }
                if (signingInput.getExpiration() != 0) {
                    setExpiration(signingInput.getExpiration());
                }
                if (this.clausesBuilder_ == null) {
                    if (!signingInput.clauses_.isEmpty()) {
                        if (this.clauses_.isEmpty()) {
                            this.clauses_ = signingInput.clauses_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureClausesIsMutable();
                            this.clauses_.addAll(signingInput.clauses_);
                        }
                        onChanged();
                    }
                } else if (!signingInput.clauses_.isEmpty()) {
                    if (!this.clausesBuilder_.u()) {
                        this.clausesBuilder_.b(signingInput.clauses_);
                    } else {
                        this.clausesBuilder_.i();
                        this.clausesBuilder_ = null;
                        this.clauses_ = signingInput.clauses_;
                        this.bitField0_ &= -2;
                        this.clausesBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getClausesFieldBuilder() : null;
                    }
                }
                if (signingInput.getGasPriceCoef() != 0) {
                    setGasPriceCoef(signingInput.getGasPriceCoef());
                }
                if (signingInput.getGas() != 0) {
                    setGas(signingInput.getGas());
                }
                ByteString dependsOn = signingInput.getDependsOn();
                ByteString byteString = ByteString.EMPTY;
                if (dependsOn != byteString) {
                    setDependsOn(signingInput.getDependsOn());
                }
                if (signingInput.getNonce() != 0) {
                    setNonce(signingInput.getNonce());
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            public Builder addClauses(Clause.Builder builder) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    ensureClausesIsMutable();
                    this.clauses_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addClauses(int i, Clause.Builder builder) {
                x0<Clause, Clause.Builder, ClauseOrBuilder> x0Var = this.clausesBuilder_;
                if (x0Var == null) {
                    ensureClausesIsMutable();
                    this.clauses_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.VeChain.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.VeChain.SigningInput.access$3000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.VeChain$SigningInput r3 = (wallet.core.jni.proto.VeChain.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.VeChain$SigningInput r4 = (wallet.core.jni.proto.VeChain.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.VeChain.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.VeChain$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.clauses_ = Collections.emptyList();
            ByteString byteString = ByteString.EMPTY;
            this.dependsOn_ = byteString;
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.chainTag_ = jVar.K();
                            } else if (J == 16) {
                                this.blockRef_ = jVar.L();
                            } else if (J == 24) {
                                this.expiration_ = jVar.K();
                            } else if (J == 34) {
                                if (!(z2 & true)) {
                                    this.clauses_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.clauses_.add(jVar.z(Clause.parser(), rVar));
                            } else if (J == 40) {
                                this.gasPriceCoef_ = jVar.K();
                            } else if (J == 48) {
                                this.gas_ = jVar.L();
                            } else if (J == 58) {
                                this.dependsOn_ = jVar.q();
                            } else if (J == 64) {
                                this.nonce_ = jVar.L();
                            } else if (J != 74) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.privateKey_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.clauses_ = Collections.unmodifiableList(this.clauses_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getBlockRef();

        int getChainTag();

        Clause getClauses(int i);

        int getClausesCount();

        List<Clause> getClausesList();

        ClauseOrBuilder getClausesOrBuilder(int i);

        List<? extends ClauseOrBuilder> getClausesOrBuilderList();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        ByteString getDependsOn();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        int getExpiration();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGas();

        int getGasPriceCoef();

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        public static final int SIGNATURE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private ByteString signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.VeChain.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return VeChain.internal_static_TW_VeChain_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && getSignature().equals(signingOutput.getSignature()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.VeChain.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_);
            if (!this.signature_.isEmpty()) {
                h += CodedOutputStream.h(2, this.signature_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.VeChain.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 37) + 2) * 53) + getSignature().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return VeChain.internal_static_TW_VeChain_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(2, this.signature_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;
            private ByteString signature_;

            public static final Descriptors.b getDescriptor() {
                return VeChain.internal_static_TW_VeChain_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return VeChain.internal_static_TW_VeChain_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.VeChain.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return VeChain.internal_static_TW_VeChain_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                signingOutput.encoded_ = this.encoded_;
                signingOutput.signature_ = this.signature_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.signature_ = byteString;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                ByteString encoded = signingOutput.getEncoded();
                ByteString byteString = ByteString.EMPTY;
                if (encoded != byteString) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (signingOutput.getSignature() != byteString) {
                    setSignature(signingOutput.getSignature());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.VeChain.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.VeChain.SigningOutput.access$4100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.VeChain$SigningOutput r3 = (wallet.core.jni.proto.VeChain.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.VeChain$SigningOutput r4 = (wallet.core.jni.proto.VeChain.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.VeChain.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.VeChain$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.encoded_ = byteString;
            this.signature_ = byteString;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.encoded_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.signature_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_VeChain_Proto_Clause_descriptor = bVar;
        internal_static_TW_VeChain_Proto_Clause_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"To", "Value", "Data"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_VeChain_Proto_SigningInput_descriptor = bVar2;
        internal_static_TW_VeChain_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"ChainTag", "BlockRef", "Expiration", "Clauses", "GasPriceCoef", "Gas", "DependsOn", "Nonce", "PrivateKey"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_VeChain_Proto_SigningOutput_descriptor = bVar3;
        internal_static_TW_VeChain_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Encoded", "Signature"});
    }

    private VeChain() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
