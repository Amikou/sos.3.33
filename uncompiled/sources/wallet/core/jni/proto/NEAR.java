package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class NEAR {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\nNEAR.proto\u0012\rTW.NEAR.Proto\"+\n\tPublicKey\u0012\u0010\n\bkey_type\u0018\u0001 \u0001(\r\u0012\f\n\u0004data\u0018\u0002 \u0001(\f\"@\n\u0016FunctionCallPermission\u0012\u0011\n\tallowance\u0018\u0001 \u0001(\f\u0012\u0013\n\u000breceiver_id\u0018\u0002 \u0001(\t\"\u0016\n\u0014FullAccessPermission\"¤\u0001\n\tAccessKey\u0012\r\n\u0005nonce\u0018\u0001 \u0001(\u0004\u0012>\n\rfunction_call\u0018\u0002 \u0001(\u000b2%.TW.NEAR.Proto.FunctionCallPermissionH\u0000\u0012:\n\u000bfull_access\u0018\u0003 \u0001(\u000b2#.TW.NEAR.Proto.FullAccessPermissionH\u0000B\f\n\npermission\"\u000f\n\rCreateAccount\"\u001e\n\u000eDeployContract\u0012\f\n\u0004code\u0018\u0001 \u0001(\f\"O\n\fFunctionCall\u0012\u0013\n\u000bmethod_name\u0018\u0001 \u0001(\f\u0012\f\n\u0004args\u0018\u0002 \u0001(\f\u0012\u000b\n\u0003gas\u0018\u0003 \u0001(\u0004\u0012\u000f\n\u0007deposit\u0018\u0004 \u0001(\f\"\u001b\n\bTransfer\u0012\u000f\n\u0007deposit\u0018\u0001 \u0001(\f\"*\n\u0005Stake\u0012\r\n\u0005stake\u0018\u0001 \u0001(\f\u0012\u0012\n\npublic_key\u0018\u0002 \u0001(\t\"d\n\u0006AddKey\u0012,\n\npublic_key\u0018\u0001 \u0001(\u000b2\u0018.TW.NEAR.Proto.PublicKey\u0012,\n\naccess_key\u0018\u0002 \u0001(\u000b2\u0018.TW.NEAR.Proto.AccessKey\"9\n\tDeleteKey\u0012,\n\npublic_key\u0018\u0001 \u0001(\u000b2\u0018.TW.NEAR.Proto.PublicKey\"'\n\rDeleteAccount\u0012\u0016\n\u000ebeneficiary_id\u0018\u0001 \u0001(\t\"¡\u0003\n\u0006Action\u00126\n\u000ecreate_account\u0018\u0001 \u0001(\u000b2\u001c.TW.NEAR.Proto.CreateAccountH\u0000\u00128\n\u000fdeploy_contract\u0018\u0002 \u0001(\u000b2\u001d.TW.NEAR.Proto.DeployContractH\u0000\u00124\n\rfunction_call\u0018\u0003 \u0001(\u000b2\u001b.TW.NEAR.Proto.FunctionCallH\u0000\u0012+\n\btransfer\u0018\u0004 \u0001(\u000b2\u0017.TW.NEAR.Proto.TransferH\u0000\u0012%\n\u0005stake\u0018\u0005 \u0001(\u000b2\u0014.TW.NEAR.Proto.StakeH\u0000\u0012(\n\u0007add_key\u0018\u0006 \u0001(\u000b2\u0015.TW.NEAR.Proto.AddKeyH\u0000\u0012.\n\ndelete_key\u0018\u0007 \u0001(\u000b2\u0018.TW.NEAR.Proto.DeleteKeyH\u0000\u00126\n\u000edelete_account\u0018\b \u0001(\u000b2\u001c.TW.NEAR.Proto.DeleteAccountH\u0000B\t\n\u0007payload\"\u0096\u0001\n\fSigningInput\u0012\u0011\n\tsigner_id\u0018\u0001 \u0001(\t\u0012\r\n\u0005nonce\u0018\u0002 \u0001(\u0004\u0012\u0013\n\u000breceiver_id\u0018\u0003 \u0001(\t\u0012\u0012\n\nblock_hash\u0018\u0004 \u0001(\f\u0012&\n\u0007actions\u0018\u0005 \u0003(\u000b2\u0015.TW.NEAR.Proto.Action\u0012\u0013\n\u000bprivate_key\u0018\u0006 \u0001(\f\"+\n\rSigningOutput\u0012\u001a\n\u0012signed_transaction\u0018\u0001 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_NEAR_Proto_AccessKey_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_AccessKey_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_Action_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_Action_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_AddKey_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_AddKey_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_CreateAccount_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_CreateAccount_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_DeleteAccount_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_DeleteAccount_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_DeleteKey_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_DeleteKey_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_DeployContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_DeployContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_FullAccessPermission_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_FullAccessPermission_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_FunctionCallPermission_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_FunctionCallPermission_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_FunctionCall_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_FunctionCall_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_PublicKey_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_PublicKey_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_Stake_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_Stake_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEAR_Proto_Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEAR_Proto_Transfer_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.NEAR$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$NEAR$AccessKey$PermissionCase;
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase;

        static {
            int[] iArr = new int[Action.PayloadCase.values().length];
            $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase = iArr;
            try {
                iArr[Action.PayloadCase.CREATE_ACCOUNT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.DEPLOY_CONTRACT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.FUNCTION_CALL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.TRANSFER.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.STAKE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.ADD_KEY.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.DELETE_KEY.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.DELETE_ACCOUNT.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[Action.PayloadCase.PAYLOAD_NOT_SET.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            int[] iArr2 = new int[AccessKey.PermissionCase.values().length];
            $SwitchMap$wallet$core$jni$proto$NEAR$AccessKey$PermissionCase = iArr2;
            try {
                iArr2[AccessKey.PermissionCase.FUNCTION_CALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$AccessKey$PermissionCase[AccessKey.PermissionCase.FULL_ACCESS.ordinal()] = 2;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$NEAR$AccessKey$PermissionCase[AccessKey.PermissionCase.PERMISSION_NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class AccessKey extends GeneratedMessageV3 implements AccessKeyOrBuilder {
        public static final int FULL_ACCESS_FIELD_NUMBER = 3;
        public static final int FUNCTION_CALL_FIELD_NUMBER = 2;
        public static final int NONCE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private long nonce_;
        private int permissionCase_;
        private Object permission_;
        private static final AccessKey DEFAULT_INSTANCE = new AccessKey();
        private static final t0<AccessKey> PARSER = new c<AccessKey>() { // from class: wallet.core.jni.proto.NEAR.AccessKey.1
            @Override // com.google.protobuf.t0
            public AccessKey parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new AccessKey(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements AccessKeyOrBuilder {
            private a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> fullAccessBuilder_;
            private a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> functionCallBuilder_;
            private long nonce_;
            private int permissionCase_;
            private Object permission_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_AccessKey_descriptor;
            }

            private a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> getFullAccessFieldBuilder() {
                if (this.fullAccessBuilder_ == null) {
                    if (this.permissionCase_ != 3) {
                        this.permission_ = FullAccessPermission.getDefaultInstance();
                    }
                    this.fullAccessBuilder_ = new a1<>((FullAccessPermission) this.permission_, getParentForChildren(), isClean());
                    this.permission_ = null;
                }
                this.permissionCase_ = 3;
                onChanged();
                return this.fullAccessBuilder_;
            }

            private a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> getFunctionCallFieldBuilder() {
                if (this.functionCallBuilder_ == null) {
                    if (this.permissionCase_ != 2) {
                        this.permission_ = FunctionCallPermission.getDefaultInstance();
                    }
                    this.functionCallBuilder_ = new a1<>((FunctionCallPermission) this.permission_, getParentForChildren(), isClean());
                    this.permission_ = null;
                }
                this.permissionCase_ = 2;
                onChanged();
                return this.functionCallBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearFullAccess() {
                a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> a1Var = this.fullAccessBuilder_;
                if (a1Var == null) {
                    if (this.permissionCase_ == 3) {
                        this.permissionCase_ = 0;
                        this.permission_ = null;
                        onChanged();
                    }
                } else {
                    if (this.permissionCase_ == 3) {
                        this.permissionCase_ = 0;
                        this.permission_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearFunctionCall() {
                a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    if (this.permissionCase_ == 2) {
                        this.permissionCase_ = 0;
                        this.permission_ = null;
                        onChanged();
                    }
                } else {
                    if (this.permissionCase_ == 2) {
                        this.permissionCase_ = 0;
                        this.permission_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearPermission() {
                this.permissionCase_ = 0;
                this.permission_ = null;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_AccessKey_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public FullAccessPermission getFullAccess() {
                a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> a1Var = this.fullAccessBuilder_;
                if (a1Var == null) {
                    if (this.permissionCase_ == 3) {
                        return (FullAccessPermission) this.permission_;
                    }
                    return FullAccessPermission.getDefaultInstance();
                } else if (this.permissionCase_ == 3) {
                    return a1Var.f();
                } else {
                    return FullAccessPermission.getDefaultInstance();
                }
            }

            public FullAccessPermission.Builder getFullAccessBuilder() {
                return getFullAccessFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public FullAccessPermissionOrBuilder getFullAccessOrBuilder() {
                a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> a1Var;
                int i = this.permissionCase_;
                if (i != 3 || (a1Var = this.fullAccessBuilder_) == null) {
                    if (i == 3) {
                        return (FullAccessPermission) this.permission_;
                    }
                    return FullAccessPermission.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public FunctionCallPermission getFunctionCall() {
                a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    if (this.permissionCase_ == 2) {
                        return (FunctionCallPermission) this.permission_;
                    }
                    return FunctionCallPermission.getDefaultInstance();
                } else if (this.permissionCase_ == 2) {
                    return a1Var.f();
                } else {
                    return FunctionCallPermission.getDefaultInstance();
                }
            }

            public FunctionCallPermission.Builder getFunctionCallBuilder() {
                return getFunctionCallFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public FunctionCallPermissionOrBuilder getFunctionCallOrBuilder() {
                a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> a1Var;
                int i = this.permissionCase_;
                if (i != 2 || (a1Var = this.functionCallBuilder_) == null) {
                    if (i == 2) {
                        return (FunctionCallPermission) this.permission_;
                    }
                    return FunctionCallPermission.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public PermissionCase getPermissionCase() {
                return PermissionCase.forNumber(this.permissionCase_);
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public boolean hasFullAccess() {
                return this.permissionCase_ == 3;
            }

            @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
            public boolean hasFunctionCall() {
                return this.permissionCase_ == 2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_AccessKey_fieldAccessorTable.d(AccessKey.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeFullAccess(FullAccessPermission fullAccessPermission) {
                a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> a1Var = this.fullAccessBuilder_;
                if (a1Var == null) {
                    if (this.permissionCase_ == 3 && this.permission_ != FullAccessPermission.getDefaultInstance()) {
                        this.permission_ = FullAccessPermission.newBuilder((FullAccessPermission) this.permission_).mergeFrom(fullAccessPermission).buildPartial();
                    } else {
                        this.permission_ = fullAccessPermission;
                    }
                    onChanged();
                } else {
                    if (this.permissionCase_ == 3) {
                        a1Var.h(fullAccessPermission);
                    }
                    this.fullAccessBuilder_.j(fullAccessPermission);
                }
                this.permissionCase_ = 3;
                return this;
            }

            public Builder mergeFunctionCall(FunctionCallPermission functionCallPermission) {
                a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    if (this.permissionCase_ == 2 && this.permission_ != FunctionCallPermission.getDefaultInstance()) {
                        this.permission_ = FunctionCallPermission.newBuilder((FunctionCallPermission) this.permission_).mergeFrom(functionCallPermission).buildPartial();
                    } else {
                        this.permission_ = functionCallPermission;
                    }
                    onChanged();
                } else {
                    if (this.permissionCase_ == 2) {
                        a1Var.h(functionCallPermission);
                    }
                    this.functionCallBuilder_.j(functionCallPermission);
                }
                this.permissionCase_ = 2;
                return this;
            }

            public Builder setFullAccess(FullAccessPermission fullAccessPermission) {
                a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> a1Var = this.fullAccessBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(fullAccessPermission);
                    this.permission_ = fullAccessPermission;
                    onChanged();
                } else {
                    a1Var.j(fullAccessPermission);
                }
                this.permissionCase_ = 3;
                return this;
            }

            public Builder setFunctionCall(FunctionCallPermission functionCallPermission) {
                a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(functionCallPermission);
                    this.permission_ = functionCallPermission;
                    onChanged();
                } else {
                    a1Var.j(functionCallPermission);
                }
                this.permissionCase_ = 2;
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.permissionCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public AccessKey build() {
                AccessKey buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public AccessKey buildPartial() {
                AccessKey accessKey = new AccessKey(this, (AnonymousClass1) null);
                accessKey.nonce_ = this.nonce_;
                if (this.permissionCase_ == 2) {
                    a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> a1Var = this.functionCallBuilder_;
                    if (a1Var == null) {
                        accessKey.permission_ = this.permission_;
                    } else {
                        accessKey.permission_ = a1Var.b();
                    }
                }
                if (this.permissionCase_ == 3) {
                    a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> a1Var2 = this.fullAccessBuilder_;
                    if (a1Var2 == null) {
                        accessKey.permission_ = this.permission_;
                    } else {
                        accessKey.permission_ = a1Var2.b();
                    }
                }
                accessKey.permissionCase_ = this.permissionCase_;
                onBuilt();
                return accessKey;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public AccessKey getDefaultInstanceForType() {
                return AccessKey.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.nonce_ = 0L;
                this.permissionCase_ = 0;
                this.permission_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.permissionCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof AccessKey) {
                    return mergeFrom((AccessKey) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setFullAccess(FullAccessPermission.Builder builder) {
                a1<FullAccessPermission, FullAccessPermission.Builder, FullAccessPermissionOrBuilder> a1Var = this.fullAccessBuilder_;
                if (a1Var == null) {
                    this.permission_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.permissionCase_ = 3;
                return this;
            }

            public Builder setFunctionCall(FunctionCallPermission.Builder builder) {
                a1<FunctionCallPermission, FunctionCallPermission.Builder, FunctionCallPermissionOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    this.permission_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.permissionCase_ = 2;
                return this;
            }

            public Builder mergeFrom(AccessKey accessKey) {
                if (accessKey == AccessKey.getDefaultInstance()) {
                    return this;
                }
                if (accessKey.getNonce() != 0) {
                    setNonce(accessKey.getNonce());
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$NEAR$AccessKey$PermissionCase[accessKey.getPermissionCase().ordinal()];
                if (i == 1) {
                    mergeFunctionCall(accessKey.getFunctionCall());
                } else if (i == 2) {
                    mergeFullAccess(accessKey.getFullAccess());
                }
                mergeUnknownFields(accessKey.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.AccessKey.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.AccessKey.access$4200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$AccessKey r3 = (wallet.core.jni.proto.NEAR.AccessKey) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$AccessKey r4 = (wallet.core.jni.proto.NEAR.AccessKey) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.AccessKey.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$AccessKey$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum PermissionCase implements a0.c {
            FUNCTION_CALL(2),
            FULL_ACCESS(3),
            PERMISSION_NOT_SET(0);
            
            private final int value;

            PermissionCase(int i) {
                this.value = i;
            }

            public static PermissionCase forNumber(int i) {
                if (i != 0) {
                    if (i != 2) {
                        if (i != 3) {
                            return null;
                        }
                        return FULL_ACCESS;
                    }
                    return FUNCTION_CALL;
                }
                return PERMISSION_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static PermissionCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ AccessKey(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static AccessKey getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_AccessKey_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static AccessKey parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (AccessKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static AccessKey parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<AccessKey> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof AccessKey)) {
                return super.equals(obj);
            }
            AccessKey accessKey = (AccessKey) obj;
            if (getNonce() == accessKey.getNonce() && getPermissionCase().equals(accessKey.getPermissionCase())) {
                int i = this.permissionCase_;
                if (i != 2) {
                    if (i == 3 && !getFullAccess().equals(accessKey.getFullAccess())) {
                        return false;
                    }
                } else if (!getFunctionCall().equals(accessKey.getFunctionCall())) {
                    return false;
                }
                return this.unknownFields.equals(accessKey.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public FullAccessPermission getFullAccess() {
            if (this.permissionCase_ == 3) {
                return (FullAccessPermission) this.permission_;
            }
            return FullAccessPermission.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public FullAccessPermissionOrBuilder getFullAccessOrBuilder() {
            if (this.permissionCase_ == 3) {
                return (FullAccessPermission) this.permission_;
            }
            return FullAccessPermission.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public FunctionCallPermission getFunctionCall() {
            if (this.permissionCase_ == 2) {
                return (FunctionCallPermission) this.permission_;
            }
            return FunctionCallPermission.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public FunctionCallPermissionOrBuilder getFunctionCallOrBuilder() {
            if (this.permissionCase_ == 2) {
                return (FunctionCallPermission) this.permission_;
            }
            return FunctionCallPermission.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<AccessKey> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public PermissionCase getPermissionCase() {
            return PermissionCase.forNumber(this.permissionCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.nonce_;
            int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
            if (this.permissionCase_ == 2) {
                a0 += CodedOutputStream.G(2, (FunctionCallPermission) this.permission_);
            }
            if (this.permissionCase_ == 3) {
                a0 += CodedOutputStream.G(3, (FullAccessPermission) this.permission_);
            }
            int serializedSize = a0 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public boolean hasFullAccess() {
            return this.permissionCase_ == 3;
        }

        @Override // wallet.core.jni.proto.NEAR.AccessKeyOrBuilder
        public boolean hasFunctionCall() {
            return this.permissionCase_ == 2;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getNonce());
            int i3 = this.permissionCase_;
            if (i3 == 2) {
                i = ((hashCode2 * 37) + 2) * 53;
                hashCode = getFunctionCall().hashCode();
            } else {
                if (i3 == 3) {
                    i = ((hashCode2 * 37) + 3) * 53;
                    hashCode = getFullAccess().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_AccessKey_fieldAccessorTable.d(AccessKey.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new AccessKey();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(1, j);
            }
            if (this.permissionCase_ == 2) {
                codedOutputStream.K0(2, (FunctionCallPermission) this.permission_);
            }
            if (this.permissionCase_ == 3) {
                codedOutputStream.K0(3, (FullAccessPermission) this.permission_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ AccessKey(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(AccessKey accessKey) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(accessKey);
        }

        public static AccessKey parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private AccessKey(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.permissionCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static AccessKey parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (AccessKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static AccessKey parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public AccessKey getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static AccessKey parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static AccessKey parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private AccessKey() {
            this.permissionCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static AccessKey parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static AccessKey parseFrom(InputStream inputStream) throws IOException {
            return (AccessKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private AccessKey(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 8) {
                                if (J == 18) {
                                    FunctionCallPermission.Builder builder = this.permissionCase_ == 2 ? ((FunctionCallPermission) this.permission_).toBuilder() : null;
                                    m0 z2 = jVar.z(FunctionCallPermission.parser(), rVar);
                                    this.permission_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((FunctionCallPermission) z2);
                                        this.permission_ = builder.buildPartial();
                                    }
                                    this.permissionCase_ = 2;
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    FullAccessPermission.Builder builder2 = this.permissionCase_ == 3 ? ((FullAccessPermission) this.permission_).toBuilder() : null;
                                    m0 z3 = jVar.z(FullAccessPermission.parser(), rVar);
                                    this.permission_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((FullAccessPermission) z3);
                                        this.permission_ = builder2.buildPartial();
                                    }
                                    this.permissionCase_ = 3;
                                }
                            } else {
                                this.nonce_ = jVar.L();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static AccessKey parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (AccessKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static AccessKey parseFrom(j jVar) throws IOException {
            return (AccessKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static AccessKey parseFrom(j jVar, r rVar) throws IOException {
            return (AccessKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface AccessKeyOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        FullAccessPermission getFullAccess();

        FullAccessPermissionOrBuilder getFullAccessOrBuilder();

        FunctionCallPermission getFunctionCall();

        FunctionCallPermissionOrBuilder getFunctionCallOrBuilder();

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        AccessKey.PermissionCase getPermissionCase();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasFullAccess();

        boolean hasFunctionCall();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Action extends GeneratedMessageV3 implements ActionOrBuilder {
        public static final int ADD_KEY_FIELD_NUMBER = 6;
        public static final int CREATE_ACCOUNT_FIELD_NUMBER = 1;
        public static final int DELETE_ACCOUNT_FIELD_NUMBER = 8;
        public static final int DELETE_KEY_FIELD_NUMBER = 7;
        public static final int DEPLOY_CONTRACT_FIELD_NUMBER = 2;
        public static final int FUNCTION_CALL_FIELD_NUMBER = 3;
        public static final int STAKE_FIELD_NUMBER = 5;
        public static final int TRANSFER_FIELD_NUMBER = 4;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int payloadCase_;
        private Object payload_;
        private static final Action DEFAULT_INSTANCE = new Action();
        private static final t0<Action> PARSER = new c<Action>() { // from class: wallet.core.jni.proto.NEAR.Action.1
            @Override // com.google.protobuf.t0
            public Action parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Action(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements ActionOrBuilder {
            private a1<AddKey, AddKey.Builder, AddKeyOrBuilder> addKeyBuilder_;
            private a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> createAccountBuilder_;
            private a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> deleteAccountBuilder_;
            private a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> deleteKeyBuilder_;
            private a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> deployContractBuilder_;
            private a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> functionCallBuilder_;
            private int payloadCase_;
            private Object payload_;
            private a1<Stake, Stake.Builder, StakeOrBuilder> stakeBuilder_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<AddKey, AddKey.Builder, AddKeyOrBuilder> getAddKeyFieldBuilder() {
                if (this.addKeyBuilder_ == null) {
                    if (this.payloadCase_ != 6) {
                        this.payload_ = AddKey.getDefaultInstance();
                    }
                    this.addKeyBuilder_ = new a1<>((AddKey) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 6;
                onChanged();
                return this.addKeyBuilder_;
            }

            private a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> getCreateAccountFieldBuilder() {
                if (this.createAccountBuilder_ == null) {
                    if (this.payloadCase_ != 1) {
                        this.payload_ = CreateAccount.getDefaultInstance();
                    }
                    this.createAccountBuilder_ = new a1<>((CreateAccount) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 1;
                onChanged();
                return this.createAccountBuilder_;
            }

            private a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> getDeleteAccountFieldBuilder() {
                if (this.deleteAccountBuilder_ == null) {
                    if (this.payloadCase_ != 8) {
                        this.payload_ = DeleteAccount.getDefaultInstance();
                    }
                    this.deleteAccountBuilder_ = new a1<>((DeleteAccount) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 8;
                onChanged();
                return this.deleteAccountBuilder_;
            }

            private a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> getDeleteKeyFieldBuilder() {
                if (this.deleteKeyBuilder_ == null) {
                    if (this.payloadCase_ != 7) {
                        this.payload_ = DeleteKey.getDefaultInstance();
                    }
                    this.deleteKeyBuilder_ = new a1<>((DeleteKey) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 7;
                onChanged();
                return this.deleteKeyBuilder_;
            }

            private a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> getDeployContractFieldBuilder() {
                if (this.deployContractBuilder_ == null) {
                    if (this.payloadCase_ != 2) {
                        this.payload_ = DeployContract.getDefaultInstance();
                    }
                    this.deployContractBuilder_ = new a1<>((DeployContract) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 2;
                onChanged();
                return this.deployContractBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_Action_descriptor;
            }

            private a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> getFunctionCallFieldBuilder() {
                if (this.functionCallBuilder_ == null) {
                    if (this.payloadCase_ != 3) {
                        this.payload_ = FunctionCall.getDefaultInstance();
                    }
                    this.functionCallBuilder_ = new a1<>((FunctionCall) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 3;
                onChanged();
                return this.functionCallBuilder_;
            }

            private a1<Stake, Stake.Builder, StakeOrBuilder> getStakeFieldBuilder() {
                if (this.stakeBuilder_ == null) {
                    if (this.payloadCase_ != 5) {
                        this.payload_ = Stake.getDefaultInstance();
                    }
                    this.stakeBuilder_ = new a1<>((Stake) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 5;
                onChanged();
                return this.stakeBuilder_;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferFieldBuilder() {
                if (this.transferBuilder_ == null) {
                    if (this.payloadCase_ != 4) {
                        this.payload_ = Transfer.getDefaultInstance();
                    }
                    this.transferBuilder_ = new a1<>((Transfer) this.payload_, getParentForChildren(), isClean());
                    this.payload_ = null;
                }
                this.payloadCase_ = 4;
                onChanged();
                return this.transferBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAddKey() {
                a1<AddKey, AddKey.Builder, AddKeyOrBuilder> a1Var = this.addKeyBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 6) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 6) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearCreateAccount() {
                a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> a1Var = this.createAccountBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 1) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 1) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearDeleteAccount() {
                a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> a1Var = this.deleteAccountBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 8) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 8) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearDeleteKey() {
                a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> a1Var = this.deleteKeyBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 7) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 7) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearDeployContract() {
                a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> a1Var = this.deployContractBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 2) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 2) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearFunctionCall() {
                a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 3) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 3) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearPayload() {
                this.payloadCase_ = 0;
                this.payload_ = null;
                onChanged();
                return this;
            }

            public Builder clearStake() {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 5) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 5) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 4) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                        onChanged();
                    }
                } else {
                    if (this.payloadCase_ == 4) {
                        this.payloadCase_ = 0;
                        this.payload_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public AddKey getAddKey() {
                a1<AddKey, AddKey.Builder, AddKeyOrBuilder> a1Var = this.addKeyBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 6) {
                        return (AddKey) this.payload_;
                    }
                    return AddKey.getDefaultInstance();
                } else if (this.payloadCase_ == 6) {
                    return a1Var.f();
                } else {
                    return AddKey.getDefaultInstance();
                }
            }

            public AddKey.Builder getAddKeyBuilder() {
                return getAddKeyFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public AddKeyOrBuilder getAddKeyOrBuilder() {
                a1<AddKey, AddKey.Builder, AddKeyOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 6 || (a1Var = this.addKeyBuilder_) == null) {
                    if (i == 6) {
                        return (AddKey) this.payload_;
                    }
                    return AddKey.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public CreateAccount getCreateAccount() {
                a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> a1Var = this.createAccountBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 1) {
                        return (CreateAccount) this.payload_;
                    }
                    return CreateAccount.getDefaultInstance();
                } else if (this.payloadCase_ == 1) {
                    return a1Var.f();
                } else {
                    return CreateAccount.getDefaultInstance();
                }
            }

            public CreateAccount.Builder getCreateAccountBuilder() {
                return getCreateAccountFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public CreateAccountOrBuilder getCreateAccountOrBuilder() {
                a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 1 || (a1Var = this.createAccountBuilder_) == null) {
                    if (i == 1) {
                        return (CreateAccount) this.payload_;
                    }
                    return CreateAccount.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public DeleteAccount getDeleteAccount() {
                a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> a1Var = this.deleteAccountBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 8) {
                        return (DeleteAccount) this.payload_;
                    }
                    return DeleteAccount.getDefaultInstance();
                } else if (this.payloadCase_ == 8) {
                    return a1Var.f();
                } else {
                    return DeleteAccount.getDefaultInstance();
                }
            }

            public DeleteAccount.Builder getDeleteAccountBuilder() {
                return getDeleteAccountFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public DeleteAccountOrBuilder getDeleteAccountOrBuilder() {
                a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 8 || (a1Var = this.deleteAccountBuilder_) == null) {
                    if (i == 8) {
                        return (DeleteAccount) this.payload_;
                    }
                    return DeleteAccount.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public DeleteKey getDeleteKey() {
                a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> a1Var = this.deleteKeyBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 7) {
                        return (DeleteKey) this.payload_;
                    }
                    return DeleteKey.getDefaultInstance();
                } else if (this.payloadCase_ == 7) {
                    return a1Var.f();
                } else {
                    return DeleteKey.getDefaultInstance();
                }
            }

            public DeleteKey.Builder getDeleteKeyBuilder() {
                return getDeleteKeyFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public DeleteKeyOrBuilder getDeleteKeyOrBuilder() {
                a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 7 || (a1Var = this.deleteKeyBuilder_) == null) {
                    if (i == 7) {
                        return (DeleteKey) this.payload_;
                    }
                    return DeleteKey.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public DeployContract getDeployContract() {
                a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> a1Var = this.deployContractBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 2) {
                        return (DeployContract) this.payload_;
                    }
                    return DeployContract.getDefaultInstance();
                } else if (this.payloadCase_ == 2) {
                    return a1Var.f();
                } else {
                    return DeployContract.getDefaultInstance();
                }
            }

            public DeployContract.Builder getDeployContractBuilder() {
                return getDeployContractFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public DeployContractOrBuilder getDeployContractOrBuilder() {
                a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 2 || (a1Var = this.deployContractBuilder_) == null) {
                    if (i == 2) {
                        return (DeployContract) this.payload_;
                    }
                    return DeployContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_Action_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public FunctionCall getFunctionCall() {
                a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 3) {
                        return (FunctionCall) this.payload_;
                    }
                    return FunctionCall.getDefaultInstance();
                } else if (this.payloadCase_ == 3) {
                    return a1Var.f();
                } else {
                    return FunctionCall.getDefaultInstance();
                }
            }

            public FunctionCall.Builder getFunctionCallBuilder() {
                return getFunctionCallFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public FunctionCallOrBuilder getFunctionCallOrBuilder() {
                a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 3 || (a1Var = this.functionCallBuilder_) == null) {
                    if (i == 3) {
                        return (FunctionCall) this.payload_;
                    }
                    return FunctionCall.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public PayloadCase getPayloadCase() {
                return PayloadCase.forNumber(this.payloadCase_);
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public Stake getStake() {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 5) {
                        return (Stake) this.payload_;
                    }
                    return Stake.getDefaultInstance();
                } else if (this.payloadCase_ == 5) {
                    return a1Var.f();
                } else {
                    return Stake.getDefaultInstance();
                }
            }

            public Stake.Builder getStakeBuilder() {
                return getStakeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public StakeOrBuilder getStakeOrBuilder() {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 5 || (a1Var = this.stakeBuilder_) == null) {
                    if (i == 5) {
                        return (Stake) this.payload_;
                    }
                    return Stake.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public Transfer getTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 4) {
                        return (Transfer) this.payload_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.payloadCase_ == 4) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferBuilder() {
                return getTransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public TransferOrBuilder getTransferOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.payloadCase_;
                if (i != 4 || (a1Var = this.transferBuilder_) == null) {
                    if (i == 4) {
                        return (Transfer) this.payload_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasAddKey() {
                return this.payloadCase_ == 6;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasCreateAccount() {
                return this.payloadCase_ == 1;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasDeleteAccount() {
                return this.payloadCase_ == 8;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasDeleteKey() {
                return this.payloadCase_ == 7;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasDeployContract() {
                return this.payloadCase_ == 2;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasFunctionCall() {
                return this.payloadCase_ == 3;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasStake() {
                return this.payloadCase_ == 5;
            }

            @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
            public boolean hasTransfer() {
                return this.payloadCase_ == 4;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_Action_fieldAccessorTable.d(Action.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeAddKey(AddKey addKey) {
                a1<AddKey, AddKey.Builder, AddKeyOrBuilder> a1Var = this.addKeyBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 6 && this.payload_ != AddKey.getDefaultInstance()) {
                        this.payload_ = AddKey.newBuilder((AddKey) this.payload_).mergeFrom(addKey).buildPartial();
                    } else {
                        this.payload_ = addKey;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 6) {
                        a1Var.h(addKey);
                    }
                    this.addKeyBuilder_.j(addKey);
                }
                this.payloadCase_ = 6;
                return this;
            }

            public Builder mergeCreateAccount(CreateAccount createAccount) {
                a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> a1Var = this.createAccountBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 1 && this.payload_ != CreateAccount.getDefaultInstance()) {
                        this.payload_ = CreateAccount.newBuilder((CreateAccount) this.payload_).mergeFrom(createAccount).buildPartial();
                    } else {
                        this.payload_ = createAccount;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 1) {
                        a1Var.h(createAccount);
                    }
                    this.createAccountBuilder_.j(createAccount);
                }
                this.payloadCase_ = 1;
                return this;
            }

            public Builder mergeDeleteAccount(DeleteAccount deleteAccount) {
                a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> a1Var = this.deleteAccountBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 8 && this.payload_ != DeleteAccount.getDefaultInstance()) {
                        this.payload_ = DeleteAccount.newBuilder((DeleteAccount) this.payload_).mergeFrom(deleteAccount).buildPartial();
                    } else {
                        this.payload_ = deleteAccount;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 8) {
                        a1Var.h(deleteAccount);
                    }
                    this.deleteAccountBuilder_.j(deleteAccount);
                }
                this.payloadCase_ = 8;
                return this;
            }

            public Builder mergeDeleteKey(DeleteKey deleteKey) {
                a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> a1Var = this.deleteKeyBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 7 && this.payload_ != DeleteKey.getDefaultInstance()) {
                        this.payload_ = DeleteKey.newBuilder((DeleteKey) this.payload_).mergeFrom(deleteKey).buildPartial();
                    } else {
                        this.payload_ = deleteKey;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 7) {
                        a1Var.h(deleteKey);
                    }
                    this.deleteKeyBuilder_.j(deleteKey);
                }
                this.payloadCase_ = 7;
                return this;
            }

            public Builder mergeDeployContract(DeployContract deployContract) {
                a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> a1Var = this.deployContractBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 2 && this.payload_ != DeployContract.getDefaultInstance()) {
                        this.payload_ = DeployContract.newBuilder((DeployContract) this.payload_).mergeFrom(deployContract).buildPartial();
                    } else {
                        this.payload_ = deployContract;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 2) {
                        a1Var.h(deployContract);
                    }
                    this.deployContractBuilder_.j(deployContract);
                }
                this.payloadCase_ = 2;
                return this;
            }

            public Builder mergeFunctionCall(FunctionCall functionCall) {
                a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 3 && this.payload_ != FunctionCall.getDefaultInstance()) {
                        this.payload_ = FunctionCall.newBuilder((FunctionCall) this.payload_).mergeFrom(functionCall).buildPartial();
                    } else {
                        this.payload_ = functionCall;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 3) {
                        a1Var.h(functionCall);
                    }
                    this.functionCallBuilder_.j(functionCall);
                }
                this.payloadCase_ = 3;
                return this;
            }

            public Builder mergeStake(Stake stake) {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 5 && this.payload_ != Stake.getDefaultInstance()) {
                        this.payload_ = Stake.newBuilder((Stake) this.payload_).mergeFrom(stake).buildPartial();
                    } else {
                        this.payload_ = stake;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 5) {
                        a1Var.h(stake);
                    }
                    this.stakeBuilder_.j(stake);
                }
                this.payloadCase_ = 5;
                return this;
            }

            public Builder mergeTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.payloadCase_ == 4 && this.payload_ != Transfer.getDefaultInstance()) {
                        this.payload_ = Transfer.newBuilder((Transfer) this.payload_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.payload_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.payloadCase_ == 4) {
                        a1Var.h(transfer);
                    }
                    this.transferBuilder_.j(transfer);
                }
                this.payloadCase_ = 4;
                return this;
            }

            public Builder setAddKey(AddKey addKey) {
                a1<AddKey, AddKey.Builder, AddKeyOrBuilder> a1Var = this.addKeyBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(addKey);
                    this.payload_ = addKey;
                    onChanged();
                } else {
                    a1Var.j(addKey);
                }
                this.payloadCase_ = 6;
                return this;
            }

            public Builder setCreateAccount(CreateAccount createAccount) {
                a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> a1Var = this.createAccountBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(createAccount);
                    this.payload_ = createAccount;
                    onChanged();
                } else {
                    a1Var.j(createAccount);
                }
                this.payloadCase_ = 1;
                return this;
            }

            public Builder setDeleteAccount(DeleteAccount deleteAccount) {
                a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> a1Var = this.deleteAccountBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(deleteAccount);
                    this.payload_ = deleteAccount;
                    onChanged();
                } else {
                    a1Var.j(deleteAccount);
                }
                this.payloadCase_ = 8;
                return this;
            }

            public Builder setDeleteKey(DeleteKey deleteKey) {
                a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> a1Var = this.deleteKeyBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(deleteKey);
                    this.payload_ = deleteKey;
                    onChanged();
                } else {
                    a1Var.j(deleteKey);
                }
                this.payloadCase_ = 7;
                return this;
            }

            public Builder setDeployContract(DeployContract deployContract) {
                a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> a1Var = this.deployContractBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(deployContract);
                    this.payload_ = deployContract;
                    onChanged();
                } else {
                    a1Var.j(deployContract);
                }
                this.payloadCase_ = 2;
                return this;
            }

            public Builder setFunctionCall(FunctionCall functionCall) {
                a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(functionCall);
                    this.payload_ = functionCall;
                    onChanged();
                } else {
                    a1Var.j(functionCall);
                }
                this.payloadCase_ = 3;
                return this;
            }

            public Builder setStake(Stake stake) {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(stake);
                    this.payload_ = stake;
                    onChanged();
                } else {
                    a1Var.j(stake);
                }
                this.payloadCase_ = 5;
                return this;
            }

            public Builder setTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.payload_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.payloadCase_ = 4;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.payloadCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Action build() {
                Action buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Action buildPartial() {
                Action action = new Action(this, (AnonymousClass1) null);
                if (this.payloadCase_ == 1) {
                    a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> a1Var = this.createAccountBuilder_;
                    if (a1Var == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var.b();
                    }
                }
                if (this.payloadCase_ == 2) {
                    a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> a1Var2 = this.deployContractBuilder_;
                    if (a1Var2 == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var2.b();
                    }
                }
                if (this.payloadCase_ == 3) {
                    a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> a1Var3 = this.functionCallBuilder_;
                    if (a1Var3 == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var3.b();
                    }
                }
                if (this.payloadCase_ == 4) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var4 = this.transferBuilder_;
                    if (a1Var4 == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var4.b();
                    }
                }
                if (this.payloadCase_ == 5) {
                    a1<Stake, Stake.Builder, StakeOrBuilder> a1Var5 = this.stakeBuilder_;
                    if (a1Var5 == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var5.b();
                    }
                }
                if (this.payloadCase_ == 6) {
                    a1<AddKey, AddKey.Builder, AddKeyOrBuilder> a1Var6 = this.addKeyBuilder_;
                    if (a1Var6 == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var6.b();
                    }
                }
                if (this.payloadCase_ == 7) {
                    a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> a1Var7 = this.deleteKeyBuilder_;
                    if (a1Var7 == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var7.b();
                    }
                }
                if (this.payloadCase_ == 8) {
                    a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> a1Var8 = this.deleteAccountBuilder_;
                    if (a1Var8 == null) {
                        action.payload_ = this.payload_;
                    } else {
                        action.payload_ = a1Var8.b();
                    }
                }
                action.payloadCase_ = this.payloadCase_;
                onBuilt();
                return action;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Action getDefaultInstanceForType() {
                return Action.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.payloadCase_ = 0;
                this.payload_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.payloadCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Action) {
                    return mergeFrom((Action) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setAddKey(AddKey.Builder builder) {
                a1<AddKey, AddKey.Builder, AddKeyOrBuilder> a1Var = this.addKeyBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 6;
                return this;
            }

            public Builder setCreateAccount(CreateAccount.Builder builder) {
                a1<CreateAccount, CreateAccount.Builder, CreateAccountOrBuilder> a1Var = this.createAccountBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 1;
                return this;
            }

            public Builder setDeleteAccount(DeleteAccount.Builder builder) {
                a1<DeleteAccount, DeleteAccount.Builder, DeleteAccountOrBuilder> a1Var = this.deleteAccountBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 8;
                return this;
            }

            public Builder setDeleteKey(DeleteKey.Builder builder) {
                a1<DeleteKey, DeleteKey.Builder, DeleteKeyOrBuilder> a1Var = this.deleteKeyBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 7;
                return this;
            }

            public Builder setDeployContract(DeployContract.Builder builder) {
                a1<DeployContract, DeployContract.Builder, DeployContractOrBuilder> a1Var = this.deployContractBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 2;
                return this;
            }

            public Builder setFunctionCall(FunctionCall.Builder builder) {
                a1<FunctionCall, FunctionCall.Builder, FunctionCallOrBuilder> a1Var = this.functionCallBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 3;
                return this;
            }

            public Builder setStake(Stake.Builder builder) {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 5;
                return this;
            }

            public Builder setTransfer(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    this.payload_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.payloadCase_ = 4;
                return this;
            }

            public Builder mergeFrom(Action action) {
                if (action == Action.getDefaultInstance()) {
                    return this;
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$NEAR$Action$PayloadCase[action.getPayloadCase().ordinal()]) {
                    case 1:
                        mergeCreateAccount(action.getCreateAccount());
                        break;
                    case 2:
                        mergeDeployContract(action.getDeployContract());
                        break;
                    case 3:
                        mergeFunctionCall(action.getFunctionCall());
                        break;
                    case 4:
                        mergeTransfer(action.getTransfer());
                        break;
                    case 5:
                        mergeStake(action.getStake());
                        break;
                    case 6:
                        mergeAddKey(action.getAddKey());
                        break;
                    case 7:
                        mergeDeleteKey(action.getDeleteKey());
                        break;
                    case 8:
                        mergeDeleteAccount(action.getDeleteAccount());
                        break;
                }
                mergeUnknownFields(action.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.Action.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.Action.access$13900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$Action r3 = (wallet.core.jni.proto.NEAR.Action) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$Action r4 = (wallet.core.jni.proto.NEAR.Action) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.Action.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$Action$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum PayloadCase implements a0.c {
            CREATE_ACCOUNT(1),
            DEPLOY_CONTRACT(2),
            FUNCTION_CALL(3),
            TRANSFER(4),
            STAKE(5),
            ADD_KEY(6),
            DELETE_KEY(7),
            DELETE_ACCOUNT(8),
            PAYLOAD_NOT_SET(0);
            
            private final int value;

            PayloadCase(int i) {
                this.value = i;
            }

            public static PayloadCase forNumber(int i) {
                switch (i) {
                    case 0:
                        return PAYLOAD_NOT_SET;
                    case 1:
                        return CREATE_ACCOUNT;
                    case 2:
                        return DEPLOY_CONTRACT;
                    case 3:
                        return FUNCTION_CALL;
                    case 4:
                        return TRANSFER;
                    case 5:
                        return STAKE;
                    case 6:
                        return ADD_KEY;
                    case 7:
                        return DELETE_KEY;
                    case 8:
                        return DELETE_ACCOUNT;
                    default:
                        return null;
                }
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static PayloadCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ Action(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Action getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_Action_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Action parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Action) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Action parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Action> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Action)) {
                return super.equals(obj);
            }
            Action action = (Action) obj;
            if (getPayloadCase().equals(action.getPayloadCase())) {
                switch (this.payloadCase_) {
                    case 1:
                        if (!getCreateAccount().equals(action.getCreateAccount())) {
                            return false;
                        }
                        break;
                    case 2:
                        if (!getDeployContract().equals(action.getDeployContract())) {
                            return false;
                        }
                        break;
                    case 3:
                        if (!getFunctionCall().equals(action.getFunctionCall())) {
                            return false;
                        }
                        break;
                    case 4:
                        if (!getTransfer().equals(action.getTransfer())) {
                            return false;
                        }
                        break;
                    case 5:
                        if (!getStake().equals(action.getStake())) {
                            return false;
                        }
                        break;
                    case 6:
                        if (!getAddKey().equals(action.getAddKey())) {
                            return false;
                        }
                        break;
                    case 7:
                        if (!getDeleteKey().equals(action.getDeleteKey())) {
                            return false;
                        }
                        break;
                    case 8:
                        if (!getDeleteAccount().equals(action.getDeleteAccount())) {
                            return false;
                        }
                        break;
                }
                return this.unknownFields.equals(action.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public AddKey getAddKey() {
            if (this.payloadCase_ == 6) {
                return (AddKey) this.payload_;
            }
            return AddKey.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public AddKeyOrBuilder getAddKeyOrBuilder() {
            if (this.payloadCase_ == 6) {
                return (AddKey) this.payload_;
            }
            return AddKey.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public CreateAccount getCreateAccount() {
            if (this.payloadCase_ == 1) {
                return (CreateAccount) this.payload_;
            }
            return CreateAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public CreateAccountOrBuilder getCreateAccountOrBuilder() {
            if (this.payloadCase_ == 1) {
                return (CreateAccount) this.payload_;
            }
            return CreateAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public DeleteAccount getDeleteAccount() {
            if (this.payloadCase_ == 8) {
                return (DeleteAccount) this.payload_;
            }
            return DeleteAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public DeleteAccountOrBuilder getDeleteAccountOrBuilder() {
            if (this.payloadCase_ == 8) {
                return (DeleteAccount) this.payload_;
            }
            return DeleteAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public DeleteKey getDeleteKey() {
            if (this.payloadCase_ == 7) {
                return (DeleteKey) this.payload_;
            }
            return DeleteKey.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public DeleteKeyOrBuilder getDeleteKeyOrBuilder() {
            if (this.payloadCase_ == 7) {
                return (DeleteKey) this.payload_;
            }
            return DeleteKey.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public DeployContract getDeployContract() {
            if (this.payloadCase_ == 2) {
                return (DeployContract) this.payload_;
            }
            return DeployContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public DeployContractOrBuilder getDeployContractOrBuilder() {
            if (this.payloadCase_ == 2) {
                return (DeployContract) this.payload_;
            }
            return DeployContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public FunctionCall getFunctionCall() {
            if (this.payloadCase_ == 3) {
                return (FunctionCall) this.payload_;
            }
            return FunctionCall.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public FunctionCallOrBuilder getFunctionCallOrBuilder() {
            if (this.payloadCase_ == 3) {
                return (FunctionCall) this.payload_;
            }
            return FunctionCall.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Action> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public PayloadCase getPayloadCase() {
            return PayloadCase.forNumber(this.payloadCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.payloadCase_ == 1 ? 0 + CodedOutputStream.G(1, (CreateAccount) this.payload_) : 0;
            if (this.payloadCase_ == 2) {
                G += CodedOutputStream.G(2, (DeployContract) this.payload_);
            }
            if (this.payloadCase_ == 3) {
                G += CodedOutputStream.G(3, (FunctionCall) this.payload_);
            }
            if (this.payloadCase_ == 4) {
                G += CodedOutputStream.G(4, (Transfer) this.payload_);
            }
            if (this.payloadCase_ == 5) {
                G += CodedOutputStream.G(5, (Stake) this.payload_);
            }
            if (this.payloadCase_ == 6) {
                G += CodedOutputStream.G(6, (AddKey) this.payload_);
            }
            if (this.payloadCase_ == 7) {
                G += CodedOutputStream.G(7, (DeleteKey) this.payload_);
            }
            if (this.payloadCase_ == 8) {
                G += CodedOutputStream.G(8, (DeleteAccount) this.payload_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public Stake getStake() {
            if (this.payloadCase_ == 5) {
                return (Stake) this.payload_;
            }
            return Stake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public StakeOrBuilder getStakeOrBuilder() {
            if (this.payloadCase_ == 5) {
                return (Stake) this.payload_;
            }
            return Stake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public Transfer getTransfer() {
            if (this.payloadCase_ == 4) {
                return (Transfer) this.payload_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public TransferOrBuilder getTransferOrBuilder() {
            if (this.payloadCase_ == 4) {
                return (Transfer) this.payload_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasAddKey() {
            return this.payloadCase_ == 6;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasCreateAccount() {
            return this.payloadCase_ == 1;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasDeleteAccount() {
            return this.payloadCase_ == 8;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasDeleteKey() {
            return this.payloadCase_ == 7;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasDeployContract() {
            return this.payloadCase_ == 2;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasFunctionCall() {
            return this.payloadCase_ == 3;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasStake() {
            return this.payloadCase_ == 5;
        }

        @Override // wallet.core.jni.proto.NEAR.ActionOrBuilder
        public boolean hasTransfer() {
            return this.payloadCase_ == 4;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = 779 + getDescriptor().hashCode();
            switch (this.payloadCase_) {
                case 1:
                    i = ((hashCode2 * 37) + 1) * 53;
                    hashCode = getCreateAccount().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3;
                    return hashCode3;
                case 2:
                    i = ((hashCode2 * 37) + 2) * 53;
                    hashCode = getDeployContract().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32;
                    return hashCode32;
                case 3:
                    i = ((hashCode2 * 37) + 3) * 53;
                    hashCode = getFunctionCall().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322;
                    return hashCode322;
                case 4:
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getTransfer().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222;
                    return hashCode3222;
                case 5:
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getStake().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222;
                    return hashCode32222;
                case 6:
                    i = ((hashCode2 * 37) + 6) * 53;
                    hashCode = getAddKey().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222;
                    return hashCode322222;
                case 7:
                    i = ((hashCode2 * 37) + 7) * 53;
                    hashCode = getDeleteKey().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222;
                    return hashCode3222222;
                case 8:
                    i = ((hashCode2 * 37) + 8) * 53;
                    hashCode = getDeleteAccount().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222222;
                    return hashCode32222222;
                default:
                    int hashCode322222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222222;
                    return hashCode322222222;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_Action_fieldAccessorTable.d(Action.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Action();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.payloadCase_ == 1) {
                codedOutputStream.K0(1, (CreateAccount) this.payload_);
            }
            if (this.payloadCase_ == 2) {
                codedOutputStream.K0(2, (DeployContract) this.payload_);
            }
            if (this.payloadCase_ == 3) {
                codedOutputStream.K0(3, (FunctionCall) this.payload_);
            }
            if (this.payloadCase_ == 4) {
                codedOutputStream.K0(4, (Transfer) this.payload_);
            }
            if (this.payloadCase_ == 5) {
                codedOutputStream.K0(5, (Stake) this.payload_);
            }
            if (this.payloadCase_ == 6) {
                codedOutputStream.K0(6, (AddKey) this.payload_);
            }
            if (this.payloadCase_ == 7) {
                codedOutputStream.K0(7, (DeleteKey) this.payload_);
            }
            if (this.payloadCase_ == 8) {
                codedOutputStream.K0(8, (DeleteAccount) this.payload_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Action(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Action action) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(action);
        }

        public static Action parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Action(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.payloadCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Action parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Action parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Action getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Action parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Action parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Action() {
            this.payloadCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Action parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Action parseFrom(InputStream inputStream) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Action(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                CreateAccount.Builder builder = this.payloadCase_ == 1 ? ((CreateAccount) this.payload_).toBuilder() : null;
                                m0 z2 = jVar.z(CreateAccount.parser(), rVar);
                                this.payload_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((CreateAccount) z2);
                                    this.payload_ = builder.buildPartial();
                                }
                                this.payloadCase_ = 1;
                            } else if (J == 18) {
                                DeployContract.Builder builder2 = this.payloadCase_ == 2 ? ((DeployContract) this.payload_).toBuilder() : null;
                                m0 z3 = jVar.z(DeployContract.parser(), rVar);
                                this.payload_ = z3;
                                if (builder2 != null) {
                                    builder2.mergeFrom((DeployContract) z3);
                                    this.payload_ = builder2.buildPartial();
                                }
                                this.payloadCase_ = 2;
                            } else if (J == 26) {
                                FunctionCall.Builder builder3 = this.payloadCase_ == 3 ? ((FunctionCall) this.payload_).toBuilder() : null;
                                m0 z4 = jVar.z(FunctionCall.parser(), rVar);
                                this.payload_ = z4;
                                if (builder3 != null) {
                                    builder3.mergeFrom((FunctionCall) z4);
                                    this.payload_ = builder3.buildPartial();
                                }
                                this.payloadCase_ = 3;
                            } else if (J == 34) {
                                Transfer.Builder builder4 = this.payloadCase_ == 4 ? ((Transfer) this.payload_).toBuilder() : null;
                                m0 z5 = jVar.z(Transfer.parser(), rVar);
                                this.payload_ = z5;
                                if (builder4 != null) {
                                    builder4.mergeFrom((Transfer) z5);
                                    this.payload_ = builder4.buildPartial();
                                }
                                this.payloadCase_ = 4;
                            } else if (J == 42) {
                                Stake.Builder builder5 = this.payloadCase_ == 5 ? ((Stake) this.payload_).toBuilder() : null;
                                m0 z6 = jVar.z(Stake.parser(), rVar);
                                this.payload_ = z6;
                                if (builder5 != null) {
                                    builder5.mergeFrom((Stake) z6);
                                    this.payload_ = builder5.buildPartial();
                                }
                                this.payloadCase_ = 5;
                            } else if (J == 50) {
                                AddKey.Builder builder6 = this.payloadCase_ == 6 ? ((AddKey) this.payload_).toBuilder() : null;
                                m0 z7 = jVar.z(AddKey.parser(), rVar);
                                this.payload_ = z7;
                                if (builder6 != null) {
                                    builder6.mergeFrom((AddKey) z7);
                                    this.payload_ = builder6.buildPartial();
                                }
                                this.payloadCase_ = 6;
                            } else if (J == 58) {
                                DeleteKey.Builder builder7 = this.payloadCase_ == 7 ? ((DeleteKey) this.payload_).toBuilder() : null;
                                m0 z8 = jVar.z(DeleteKey.parser(), rVar);
                                this.payload_ = z8;
                                if (builder7 != null) {
                                    builder7.mergeFrom((DeleteKey) z8);
                                    this.payload_ = builder7.buildPartial();
                                }
                                this.payloadCase_ = 7;
                            } else if (J != 66) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                DeleteAccount.Builder builder8 = this.payloadCase_ == 8 ? ((DeleteAccount) this.payload_).toBuilder() : null;
                                m0 z9 = jVar.z(DeleteAccount.parser(), rVar);
                                this.payload_ = z9;
                                if (builder8 != null) {
                                    builder8.mergeFrom((DeleteAccount) z9);
                                    this.payload_ = builder8.buildPartial();
                                }
                                this.payloadCase_ = 8;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Action parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Action parseFrom(j jVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Action parseFrom(j jVar, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface ActionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        AddKey getAddKey();

        AddKeyOrBuilder getAddKeyOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        CreateAccount getCreateAccount();

        CreateAccountOrBuilder getCreateAccountOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        DeleteAccount getDeleteAccount();

        DeleteAccountOrBuilder getDeleteAccountOrBuilder();

        DeleteKey getDeleteKey();

        DeleteKeyOrBuilder getDeleteKeyOrBuilder();

        DeployContract getDeployContract();

        DeployContractOrBuilder getDeployContractOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        FunctionCall getFunctionCall();

        FunctionCallOrBuilder getFunctionCallOrBuilder();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Action.PayloadCase getPayloadCase();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Stake getStake();

        StakeOrBuilder getStakeOrBuilder();

        Transfer getTransfer();

        TransferOrBuilder getTransferOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasAddKey();

        boolean hasCreateAccount();

        boolean hasDeleteAccount();

        boolean hasDeleteKey();

        boolean hasDeployContract();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasFunctionCall();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasStake();

        boolean hasTransfer();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class AddKey extends GeneratedMessageV3 implements AddKeyOrBuilder {
        public static final int ACCESS_KEY_FIELD_NUMBER = 2;
        private static final AddKey DEFAULT_INSTANCE = new AddKey();
        private static final t0<AddKey> PARSER = new c<AddKey>() { // from class: wallet.core.jni.proto.NEAR.AddKey.1
            @Override // com.google.protobuf.t0
            public AddKey parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new AddKey(jVar, rVar, null);
            }
        };
        public static final int PUBLIC_KEY_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private AccessKey accessKey_;
        private byte memoizedIsInitialized;
        private PublicKey publicKey_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements AddKeyOrBuilder {
            private a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> accessKeyBuilder_;
            private AccessKey accessKey_;
            private a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> publicKeyBuilder_;
            private PublicKey publicKey_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> getAccessKeyFieldBuilder() {
                if (this.accessKeyBuilder_ == null) {
                    this.accessKeyBuilder_ = new a1<>(getAccessKey(), getParentForChildren(), isClean());
                    this.accessKey_ = null;
                }
                return this.accessKeyBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_AddKey_descriptor;
            }

            private a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> getPublicKeyFieldBuilder() {
                if (this.publicKeyBuilder_ == null) {
                    this.publicKeyBuilder_ = new a1<>(getPublicKey(), getParentForChildren(), isClean());
                    this.publicKey_ = null;
                }
                return this.publicKeyBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAccessKey() {
                if (this.accessKeyBuilder_ == null) {
                    this.accessKey_ = null;
                    onChanged();
                } else {
                    this.accessKey_ = null;
                    this.accessKeyBuilder_ = null;
                }
                return this;
            }

            public Builder clearPublicKey() {
                if (this.publicKeyBuilder_ == null) {
                    this.publicKey_ = null;
                    onChanged();
                } else {
                    this.publicKey_ = null;
                    this.publicKeyBuilder_ = null;
                }
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
            public AccessKey getAccessKey() {
                a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> a1Var = this.accessKeyBuilder_;
                if (a1Var == null) {
                    AccessKey accessKey = this.accessKey_;
                    return accessKey == null ? AccessKey.getDefaultInstance() : accessKey;
                }
                return a1Var.f();
            }

            public AccessKey.Builder getAccessKeyBuilder() {
                onChanged();
                return getAccessKeyFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
            public AccessKeyOrBuilder getAccessKeyOrBuilder() {
                a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> a1Var = this.accessKeyBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                AccessKey accessKey = this.accessKey_;
                return accessKey == null ? AccessKey.getDefaultInstance() : accessKey;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_AddKey_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
            public PublicKey getPublicKey() {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    PublicKey publicKey = this.publicKey_;
                    return publicKey == null ? PublicKey.getDefaultInstance() : publicKey;
                }
                return a1Var.f();
            }

            public PublicKey.Builder getPublicKeyBuilder() {
                onChanged();
                return getPublicKeyFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
            public PublicKeyOrBuilder getPublicKeyOrBuilder() {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                PublicKey publicKey = this.publicKey_;
                return publicKey == null ? PublicKey.getDefaultInstance() : publicKey;
            }

            @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
            public boolean hasAccessKey() {
                return (this.accessKeyBuilder_ == null && this.accessKey_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
            public boolean hasPublicKey() {
                return (this.publicKeyBuilder_ == null && this.publicKey_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_AddKey_fieldAccessorTable.d(AddKey.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeAccessKey(AccessKey accessKey) {
                a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> a1Var = this.accessKeyBuilder_;
                if (a1Var == null) {
                    AccessKey accessKey2 = this.accessKey_;
                    if (accessKey2 != null) {
                        this.accessKey_ = AccessKey.newBuilder(accessKey2).mergeFrom(accessKey).buildPartial();
                    } else {
                        this.accessKey_ = accessKey;
                    }
                    onChanged();
                } else {
                    a1Var.h(accessKey);
                }
                return this;
            }

            public Builder mergePublicKey(PublicKey publicKey) {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    PublicKey publicKey2 = this.publicKey_;
                    if (publicKey2 != null) {
                        this.publicKey_ = PublicKey.newBuilder(publicKey2).mergeFrom(publicKey).buildPartial();
                    } else {
                        this.publicKey_ = publicKey;
                    }
                    onChanged();
                } else {
                    a1Var.h(publicKey);
                }
                return this;
            }

            public Builder setAccessKey(AccessKey accessKey) {
                a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> a1Var = this.accessKeyBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(accessKey);
                    this.accessKey_ = accessKey;
                    onChanged();
                } else {
                    a1Var.j(accessKey);
                }
                return this;
            }

            public Builder setPublicKey(PublicKey publicKey) {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(publicKey);
                    this.publicKey_ = publicKey;
                    onChanged();
                } else {
                    a1Var.j(publicKey);
                }
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public AddKey build() {
                AddKey buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public AddKey buildPartial() {
                AddKey addKey = new AddKey(this, (AnonymousClass1) null);
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    addKey.publicKey_ = this.publicKey_;
                } else {
                    addKey.publicKey_ = a1Var.b();
                }
                a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> a1Var2 = this.accessKeyBuilder_;
                if (a1Var2 == null) {
                    addKey.accessKey_ = this.accessKey_;
                } else {
                    addKey.accessKey_ = a1Var2.b();
                }
                onBuilt();
                return addKey;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public AddKey getDefaultInstanceForType() {
                return AddKey.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.publicKeyBuilder_ == null) {
                    this.publicKey_ = null;
                } else {
                    this.publicKey_ = null;
                    this.publicKeyBuilder_ = null;
                }
                if (this.accessKeyBuilder_ == null) {
                    this.accessKey_ = null;
                } else {
                    this.accessKey_ = null;
                    this.accessKeyBuilder_ = null;
                }
                return this;
            }

            public Builder setAccessKey(AccessKey.Builder builder) {
                a1<AccessKey, AccessKey.Builder, AccessKeyOrBuilder> a1Var = this.accessKeyBuilder_;
                if (a1Var == null) {
                    this.accessKey_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setPublicKey(PublicKey.Builder builder) {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    this.publicKey_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof AddKey) {
                    return mergeFrom((AddKey) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(AddKey addKey) {
                if (addKey == AddKey.getDefaultInstance()) {
                    return this;
                }
                if (addKey.hasPublicKey()) {
                    mergePublicKey(addKey.getPublicKey());
                }
                if (addKey.hasAccessKey()) {
                    mergeAccessKey(addKey.getAccessKey());
                }
                mergeUnknownFields(addKey.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.AddKey.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.AddKey.access$10700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$AddKey r3 = (wallet.core.jni.proto.NEAR.AddKey) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$AddKey r4 = (wallet.core.jni.proto.NEAR.AddKey) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.AddKey.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$AddKey$Builder");
            }
        }

        public /* synthetic */ AddKey(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static AddKey getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_AddKey_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static AddKey parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (AddKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static AddKey parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<AddKey> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof AddKey)) {
                return super.equals(obj);
            }
            AddKey addKey = (AddKey) obj;
            if (hasPublicKey() != addKey.hasPublicKey()) {
                return false;
            }
            if ((!hasPublicKey() || getPublicKey().equals(addKey.getPublicKey())) && hasAccessKey() == addKey.hasAccessKey()) {
                return (!hasAccessKey() || getAccessKey().equals(addKey.getAccessKey())) && this.unknownFields.equals(addKey.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
        public AccessKey getAccessKey() {
            AccessKey accessKey = this.accessKey_;
            return accessKey == null ? AccessKey.getDefaultInstance() : accessKey;
        }

        @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
        public AccessKeyOrBuilder getAccessKeyOrBuilder() {
            return getAccessKey();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<AddKey> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
        public PublicKey getPublicKey() {
            PublicKey publicKey = this.publicKey_;
            return publicKey == null ? PublicKey.getDefaultInstance() : publicKey;
        }

        @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
        public PublicKeyOrBuilder getPublicKeyOrBuilder() {
            return getPublicKey();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.publicKey_ != null ? 0 + CodedOutputStream.G(1, getPublicKey()) : 0;
            if (this.accessKey_ != null) {
                G += CodedOutputStream.G(2, getAccessKey());
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
        public boolean hasAccessKey() {
            return this.accessKey_ != null;
        }

        @Override // wallet.core.jni.proto.NEAR.AddKeyOrBuilder
        public boolean hasPublicKey() {
            return this.publicKey_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasPublicKey()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getPublicKey().hashCode();
            }
            if (hasAccessKey()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getAccessKey().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_AddKey_fieldAccessorTable.d(AddKey.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new AddKey();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.publicKey_ != null) {
                codedOutputStream.K0(1, getPublicKey());
            }
            if (this.accessKey_ != null) {
                codedOutputStream.K0(2, getAccessKey());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ AddKey(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(AddKey addKey) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(addKey);
        }

        public static AddKey parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private AddKey(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static AddKey parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (AddKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static AddKey parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public AddKey getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static AddKey parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private AddKey() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static AddKey parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static AddKey parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private AddKey(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                PublicKey publicKey = this.publicKey_;
                                PublicKey.Builder builder = publicKey != null ? publicKey.toBuilder() : null;
                                PublicKey publicKey2 = (PublicKey) jVar.z(PublicKey.parser(), rVar);
                                this.publicKey_ = publicKey2;
                                if (builder != null) {
                                    builder.mergeFrom(publicKey2);
                                    this.publicKey_ = builder.buildPartial();
                                }
                            } else if (J != 18) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                AccessKey accessKey = this.accessKey_;
                                AccessKey.Builder builder2 = accessKey != null ? accessKey.toBuilder() : null;
                                AccessKey accessKey2 = (AccessKey) jVar.z(AccessKey.parser(), rVar);
                                this.accessKey_ = accessKey2;
                                if (builder2 != null) {
                                    builder2.mergeFrom(accessKey2);
                                    this.accessKey_ = builder2.buildPartial();
                                }
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static AddKey parseFrom(InputStream inputStream) throws IOException {
            return (AddKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static AddKey parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (AddKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static AddKey parseFrom(j jVar) throws IOException {
            return (AddKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static AddKey parseFrom(j jVar, r rVar) throws IOException {
            return (AddKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface AddKeyOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        AccessKey getAccessKey();

        AccessKeyOrBuilder getAccessKeyOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        PublicKey getPublicKey();

        PublicKeyOrBuilder getPublicKeyOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasAccessKey();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasPublicKey();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class CreateAccount extends GeneratedMessageV3 implements CreateAccountOrBuilder {
        private static final CreateAccount DEFAULT_INSTANCE = new CreateAccount();
        private static final t0<CreateAccount> PARSER = new c<CreateAccount>() { // from class: wallet.core.jni.proto.NEAR.CreateAccount.1
            @Override // com.google.protobuf.t0
            public CreateAccount parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new CreateAccount(jVar, rVar, null);
            }
        };
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements CreateAccountOrBuilder {
            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_CreateAccount_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_CreateAccount_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_CreateAccount_fieldAccessorTable.d(CreateAccount.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CreateAccount build() {
                CreateAccount buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CreateAccount buildPartial() {
                CreateAccount createAccount = new CreateAccount(this, (AnonymousClass1) null);
                onBuilt();
                return createAccount;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public CreateAccount getDefaultInstanceForType() {
                return CreateAccount.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof CreateAccount) {
                    return mergeFrom((CreateAccount) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(CreateAccount createAccount) {
                if (createAccount == CreateAccount.getDefaultInstance()) {
                    return this;
                }
                mergeUnknownFields(createAccount.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.CreateAccount.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.CreateAccount.access$5100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$CreateAccount r3 = (wallet.core.jni.proto.NEAR.CreateAccount) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$CreateAccount r4 = (wallet.core.jni.proto.NEAR.CreateAccount) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.CreateAccount.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$CreateAccount$Builder");
            }
        }

        public /* synthetic */ CreateAccount(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static CreateAccount getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_CreateAccount_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static CreateAccount parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (CreateAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static CreateAccount parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<CreateAccount> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof CreateAccount) {
                return this.unknownFields.equals(((CreateAccount) obj).unknownFields);
            }
            return super.equals(obj);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<CreateAccount> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int serializedSize = this.unknownFields.getSerializedSize() + 0;
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((779 + getDescriptor().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_CreateAccount_fieldAccessorTable.d(CreateAccount.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new CreateAccount();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ CreateAccount(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(CreateAccount createAccount) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(createAccount);
        }

        public static CreateAccount parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private CreateAccount(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static CreateAccount parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (CreateAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static CreateAccount parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public CreateAccount getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static CreateAccount parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private CreateAccount() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static CreateAccount parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static CreateAccount parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private CreateAccount(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J == 0 || !parseUnknownField(jVar, g, rVar, J)) {
                                z = true;
                            }
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static CreateAccount parseFrom(InputStream inputStream) throws IOException {
            return (CreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static CreateAccount parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (CreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static CreateAccount parseFrom(j jVar) throws IOException {
            return (CreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static CreateAccount parseFrom(j jVar, r rVar) throws IOException {
            return (CreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface CreateAccountOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DeleteAccount extends GeneratedMessageV3 implements DeleteAccountOrBuilder {
        public static final int BENEFICIARY_ID_FIELD_NUMBER = 1;
        private static final DeleteAccount DEFAULT_INSTANCE = new DeleteAccount();
        private static final t0<DeleteAccount> PARSER = new c<DeleteAccount>() { // from class: wallet.core.jni.proto.NEAR.DeleteAccount.1
            @Override // com.google.protobuf.t0
            public DeleteAccount parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DeleteAccount(jVar, rVar, null);
            }
        };
        private static final long serialVersionUID = 0;
        private volatile Object beneficiaryId_;
        private byte memoizedIsInitialized;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DeleteAccountOrBuilder {
            private Object beneficiaryId_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_DeleteAccount_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBeneficiaryId() {
                this.beneficiaryId_ = DeleteAccount.getDefaultInstance().getBeneficiaryId();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.DeleteAccountOrBuilder
            public String getBeneficiaryId() {
                Object obj = this.beneficiaryId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.beneficiaryId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEAR.DeleteAccountOrBuilder
            public ByteString getBeneficiaryIdBytes() {
                Object obj = this.beneficiaryId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.beneficiaryId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_DeleteAccount_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_DeleteAccount_fieldAccessorTable.d(DeleteAccount.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setBeneficiaryId(String str) {
                Objects.requireNonNull(str);
                this.beneficiaryId_ = str;
                onChanged();
                return this;
            }

            public Builder setBeneficiaryIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.beneficiaryId_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.beneficiaryId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeleteAccount build() {
                DeleteAccount buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeleteAccount buildPartial() {
                DeleteAccount deleteAccount = new DeleteAccount(this, (AnonymousClass1) null);
                deleteAccount.beneficiaryId_ = this.beneficiaryId_;
                onBuilt();
                return deleteAccount;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DeleteAccount getDefaultInstanceForType() {
                return DeleteAccount.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.beneficiaryId_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.beneficiaryId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DeleteAccount) {
                    return mergeFrom((DeleteAccount) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DeleteAccount deleteAccount) {
                if (deleteAccount == DeleteAccount.getDefaultInstance()) {
                    return this;
                }
                if (!deleteAccount.getBeneficiaryId().isEmpty()) {
                    this.beneficiaryId_ = deleteAccount.beneficiaryId_;
                    onChanged();
                }
                mergeUnknownFields(deleteAccount.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.DeleteAccount.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.DeleteAccount.access$12700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$DeleteAccount r3 = (wallet.core.jni.proto.NEAR.DeleteAccount) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$DeleteAccount r4 = (wallet.core.jni.proto.NEAR.DeleteAccount) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.DeleteAccount.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$DeleteAccount$Builder");
            }
        }

        public /* synthetic */ DeleteAccount(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DeleteAccount getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_DeleteAccount_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DeleteAccount parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DeleteAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DeleteAccount parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DeleteAccount> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DeleteAccount)) {
                return super.equals(obj);
            }
            DeleteAccount deleteAccount = (DeleteAccount) obj;
            return getBeneficiaryId().equals(deleteAccount.getBeneficiaryId()) && this.unknownFields.equals(deleteAccount.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEAR.DeleteAccountOrBuilder
        public String getBeneficiaryId() {
            Object obj = this.beneficiaryId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.beneficiaryId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEAR.DeleteAccountOrBuilder
        public ByteString getBeneficiaryIdBytes() {
            Object obj = this.beneficiaryId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.beneficiaryId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DeleteAccount> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getBeneficiaryIdBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.beneficiaryId_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getBeneficiaryId().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_DeleteAccount_fieldAccessorTable.d(DeleteAccount.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DeleteAccount();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getBeneficiaryIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.beneficiaryId_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DeleteAccount(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DeleteAccount deleteAccount) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(deleteAccount);
        }

        public static DeleteAccount parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DeleteAccount(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DeleteAccount parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeleteAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DeleteAccount parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DeleteAccount getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DeleteAccount parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DeleteAccount() {
            this.memoizedIsInitialized = (byte) -1;
            this.beneficiaryId_ = "";
        }

        public static DeleteAccount parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DeleteAccount parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DeleteAccount parseFrom(InputStream inputStream) throws IOException {
            return (DeleteAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private DeleteAccount(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.beneficiaryId_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DeleteAccount parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeleteAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DeleteAccount parseFrom(j jVar) throws IOException {
            return (DeleteAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DeleteAccount parseFrom(j jVar, r rVar) throws IOException {
            return (DeleteAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DeleteAccountOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getBeneficiaryId();

        ByteString getBeneficiaryIdBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DeleteKey extends GeneratedMessageV3 implements DeleteKeyOrBuilder {
        private static final DeleteKey DEFAULT_INSTANCE = new DeleteKey();
        private static final t0<DeleteKey> PARSER = new c<DeleteKey>() { // from class: wallet.core.jni.proto.NEAR.DeleteKey.1
            @Override // com.google.protobuf.t0
            public DeleteKey parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DeleteKey(jVar, rVar, null);
            }
        };
        public static final int PUBLIC_KEY_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private PublicKey publicKey_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DeleteKeyOrBuilder {
            private a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> publicKeyBuilder_;
            private PublicKey publicKey_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_DeleteKey_descriptor;
            }

            private a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> getPublicKeyFieldBuilder() {
                if (this.publicKeyBuilder_ == null) {
                    this.publicKeyBuilder_ = new a1<>(getPublicKey(), getParentForChildren(), isClean());
                    this.publicKey_ = null;
                }
                return this.publicKeyBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPublicKey() {
                if (this.publicKeyBuilder_ == null) {
                    this.publicKey_ = null;
                    onChanged();
                } else {
                    this.publicKey_ = null;
                    this.publicKeyBuilder_ = null;
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_DeleteKey_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.DeleteKeyOrBuilder
            public PublicKey getPublicKey() {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    PublicKey publicKey = this.publicKey_;
                    return publicKey == null ? PublicKey.getDefaultInstance() : publicKey;
                }
                return a1Var.f();
            }

            public PublicKey.Builder getPublicKeyBuilder() {
                onChanged();
                return getPublicKeyFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEAR.DeleteKeyOrBuilder
            public PublicKeyOrBuilder getPublicKeyOrBuilder() {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                PublicKey publicKey = this.publicKey_;
                return publicKey == null ? PublicKey.getDefaultInstance() : publicKey;
            }

            @Override // wallet.core.jni.proto.NEAR.DeleteKeyOrBuilder
            public boolean hasPublicKey() {
                return (this.publicKeyBuilder_ == null && this.publicKey_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_DeleteKey_fieldAccessorTable.d(DeleteKey.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergePublicKey(PublicKey publicKey) {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    PublicKey publicKey2 = this.publicKey_;
                    if (publicKey2 != null) {
                        this.publicKey_ = PublicKey.newBuilder(publicKey2).mergeFrom(publicKey).buildPartial();
                    } else {
                        this.publicKey_ = publicKey;
                    }
                    onChanged();
                } else {
                    a1Var.h(publicKey);
                }
                return this;
            }

            public Builder setPublicKey(PublicKey publicKey) {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(publicKey);
                    this.publicKey_ = publicKey;
                    onChanged();
                } else {
                    a1Var.j(publicKey);
                }
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeleteKey build() {
                DeleteKey buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeleteKey buildPartial() {
                DeleteKey deleteKey = new DeleteKey(this, (AnonymousClass1) null);
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    deleteKey.publicKey_ = this.publicKey_;
                } else {
                    deleteKey.publicKey_ = a1Var.b();
                }
                onBuilt();
                return deleteKey;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DeleteKey getDefaultInstanceForType() {
                return DeleteKey.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.publicKeyBuilder_ == null) {
                    this.publicKey_ = null;
                } else {
                    this.publicKey_ = null;
                    this.publicKeyBuilder_ = null;
                }
                return this;
            }

            public Builder setPublicKey(PublicKey.Builder builder) {
                a1<PublicKey, PublicKey.Builder, PublicKeyOrBuilder> a1Var = this.publicKeyBuilder_;
                if (a1Var == null) {
                    this.publicKey_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DeleteKey) {
                    return mergeFrom((DeleteKey) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DeleteKey deleteKey) {
                if (deleteKey == DeleteKey.getDefaultInstance()) {
                    return this;
                }
                if (deleteKey.hasPublicKey()) {
                    mergePublicKey(deleteKey.getPublicKey());
                }
                mergeUnknownFields(deleteKey.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.DeleteKey.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.DeleteKey.access$11700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$DeleteKey r3 = (wallet.core.jni.proto.NEAR.DeleteKey) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$DeleteKey r4 = (wallet.core.jni.proto.NEAR.DeleteKey) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.DeleteKey.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$DeleteKey$Builder");
            }
        }

        public /* synthetic */ DeleteKey(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DeleteKey getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_DeleteKey_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DeleteKey parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DeleteKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DeleteKey parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DeleteKey> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DeleteKey)) {
                return super.equals(obj);
            }
            DeleteKey deleteKey = (DeleteKey) obj;
            if (hasPublicKey() != deleteKey.hasPublicKey()) {
                return false;
            }
            return (!hasPublicKey() || getPublicKey().equals(deleteKey.getPublicKey())) && this.unknownFields.equals(deleteKey.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DeleteKey> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEAR.DeleteKeyOrBuilder
        public PublicKey getPublicKey() {
            PublicKey publicKey = this.publicKey_;
            return publicKey == null ? PublicKey.getDefaultInstance() : publicKey;
        }

        @Override // wallet.core.jni.proto.NEAR.DeleteKeyOrBuilder
        public PublicKeyOrBuilder getPublicKeyOrBuilder() {
            return getPublicKey();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = (this.publicKey_ != null ? 0 + CodedOutputStream.G(1, getPublicKey()) : 0) + this.unknownFields.getSerializedSize();
            this.memoizedSize = G;
            return G;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.NEAR.DeleteKeyOrBuilder
        public boolean hasPublicKey() {
            return this.publicKey_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasPublicKey()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getPublicKey().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_DeleteKey_fieldAccessorTable.d(DeleteKey.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DeleteKey();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.publicKey_ != null) {
                codedOutputStream.K0(1, getPublicKey());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DeleteKey(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DeleteKey deleteKey) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(deleteKey);
        }

        public static DeleteKey parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DeleteKey(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DeleteKey parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeleteKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DeleteKey parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DeleteKey getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DeleteKey parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DeleteKey() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DeleteKey parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DeleteKey parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private DeleteKey(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J != 10) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    PublicKey publicKey = this.publicKey_;
                                    PublicKey.Builder builder = publicKey != null ? publicKey.toBuilder() : null;
                                    PublicKey publicKey2 = (PublicKey) jVar.z(PublicKey.parser(), rVar);
                                    this.publicKey_ = publicKey2;
                                    if (builder != null) {
                                        builder.mergeFrom(publicKey2);
                                        this.publicKey_ = builder.buildPartial();
                                    }
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DeleteKey parseFrom(InputStream inputStream) throws IOException {
            return (DeleteKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static DeleteKey parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeleteKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DeleteKey parseFrom(j jVar) throws IOException {
            return (DeleteKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DeleteKey parseFrom(j jVar, r rVar) throws IOException {
            return (DeleteKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DeleteKeyOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        PublicKey getPublicKey();

        PublicKeyOrBuilder getPublicKeyOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasPublicKey();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DeployContract extends GeneratedMessageV3 implements DeployContractOrBuilder {
        public static final int CODE_FIELD_NUMBER = 1;
        private static final DeployContract DEFAULT_INSTANCE = new DeployContract();
        private static final t0<DeployContract> PARSER = new c<DeployContract>() { // from class: wallet.core.jni.proto.NEAR.DeployContract.1
            @Override // com.google.protobuf.t0
            public DeployContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DeployContract(jVar, rVar, null);
            }
        };
        private static final long serialVersionUID = 0;
        private ByteString code_;
        private byte memoizedIsInitialized;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DeployContractOrBuilder {
            private ByteString code_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_DeployContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCode() {
                this.code_ = DeployContract.getDefaultInstance().getCode();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.DeployContractOrBuilder
            public ByteString getCode() {
                return this.code_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_DeployContract_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_DeployContract_fieldAccessorTable.d(DeployContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setCode(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.code_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.code_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeployContract build() {
                DeployContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeployContract buildPartial() {
                DeployContract deployContract = new DeployContract(this, (AnonymousClass1) null);
                deployContract.code_ = this.code_;
                onBuilt();
                return deployContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DeployContract getDefaultInstanceForType() {
                return DeployContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.code_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.code_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DeployContract) {
                    return mergeFrom((DeployContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DeployContract deployContract) {
                if (deployContract == DeployContract.getDefaultInstance()) {
                    return this;
                }
                if (deployContract.getCode() != ByteString.EMPTY) {
                    setCode(deployContract.getCode());
                }
                mergeUnknownFields(deployContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.DeployContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.DeployContract.access$6100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$DeployContract r3 = (wallet.core.jni.proto.NEAR.DeployContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$DeployContract r4 = (wallet.core.jni.proto.NEAR.DeployContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.DeployContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$DeployContract$Builder");
            }
        }

        public /* synthetic */ DeployContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DeployContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_DeployContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DeployContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DeployContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DeployContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DeployContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DeployContract)) {
                return super.equals(obj);
            }
            DeployContract deployContract = (DeployContract) obj;
            return getCode().equals(deployContract.getCode()) && this.unknownFields.equals(deployContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEAR.DeployContractOrBuilder
        public ByteString getCode() {
            return this.code_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DeployContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.code_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.code_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getCode().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_DeployContract_fieldAccessorTable.d(DeployContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DeployContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.code_.isEmpty()) {
                codedOutputStream.q0(1, this.code_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DeployContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DeployContract deployContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(deployContract);
        }

        public static DeployContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DeployContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DeployContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeployContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DeployContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DeployContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DeployContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DeployContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.code_ = ByteString.EMPTY;
        }

        public static DeployContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DeployContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DeployContract parseFrom(InputStream inputStream) throws IOException {
            return (DeployContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private DeployContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.code_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DeployContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeployContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DeployContract parseFrom(j jVar) throws IOException {
            return (DeployContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DeployContract parseFrom(j jVar, r rVar) throws IOException {
            return (DeployContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DeployContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getCode();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class FullAccessPermission extends GeneratedMessageV3 implements FullAccessPermissionOrBuilder {
        private static final FullAccessPermission DEFAULT_INSTANCE = new FullAccessPermission();
        private static final t0<FullAccessPermission> PARSER = new c<FullAccessPermission>() { // from class: wallet.core.jni.proto.NEAR.FullAccessPermission.1
            @Override // com.google.protobuf.t0
            public FullAccessPermission parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new FullAccessPermission(jVar, rVar, null);
            }
        };
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements FullAccessPermissionOrBuilder {
            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_FullAccessPermission_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_FullAccessPermission_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_FullAccessPermission_fieldAccessorTable.d(FullAccessPermission.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FullAccessPermission build() {
                FullAccessPermission buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FullAccessPermission buildPartial() {
                FullAccessPermission fullAccessPermission = new FullAccessPermission(this, (AnonymousClass1) null);
                onBuilt();
                return fullAccessPermission;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public FullAccessPermission getDefaultInstanceForType() {
                return FullAccessPermission.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof FullAccessPermission) {
                    return mergeFrom((FullAccessPermission) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(FullAccessPermission fullAccessPermission) {
                if (fullAccessPermission == FullAccessPermission.getDefaultInstance()) {
                    return this;
                }
                mergeUnknownFields(fullAccessPermission.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.FullAccessPermission.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.FullAccessPermission.access$3000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$FullAccessPermission r3 = (wallet.core.jni.proto.NEAR.FullAccessPermission) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$FullAccessPermission r4 = (wallet.core.jni.proto.NEAR.FullAccessPermission) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.FullAccessPermission.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$FullAccessPermission$Builder");
            }
        }

        public /* synthetic */ FullAccessPermission(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static FullAccessPermission getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_FullAccessPermission_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static FullAccessPermission parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FullAccessPermission) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FullAccessPermission parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FullAccessPermission> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof FullAccessPermission) {
                return this.unknownFields.equals(((FullAccessPermission) obj).unknownFields);
            }
            return super.equals(obj);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FullAccessPermission> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int serializedSize = this.unknownFields.getSerializedSize() + 0;
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((779 + getDescriptor().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_FullAccessPermission_fieldAccessorTable.d(FullAccessPermission.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FullAccessPermission();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ FullAccessPermission(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(FullAccessPermission fullAccessPermission) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(fullAccessPermission);
        }

        public static FullAccessPermission parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private FullAccessPermission(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FullAccessPermission parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (FullAccessPermission) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FullAccessPermission parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FullAccessPermission getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static FullAccessPermission parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private FullAccessPermission() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FullAccessPermission parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static FullAccessPermission parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private FullAccessPermission(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J == 0 || !parseUnknownField(jVar, g, rVar, J)) {
                                z = true;
                            }
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static FullAccessPermission parseFrom(InputStream inputStream) throws IOException {
            return (FullAccessPermission) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static FullAccessPermission parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (FullAccessPermission) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static FullAccessPermission parseFrom(j jVar) throws IOException {
            return (FullAccessPermission) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FullAccessPermission parseFrom(j jVar, r rVar) throws IOException {
            return (FullAccessPermission) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface FullAccessPermissionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class FunctionCall extends GeneratedMessageV3 implements FunctionCallOrBuilder {
        public static final int ARGS_FIELD_NUMBER = 2;
        public static final int DEPOSIT_FIELD_NUMBER = 4;
        public static final int GAS_FIELD_NUMBER = 3;
        public static final int METHOD_NAME_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString args_;
        private ByteString deposit_;
        private long gas_;
        private byte memoizedIsInitialized;
        private ByteString methodName_;
        private static final FunctionCall DEFAULT_INSTANCE = new FunctionCall();
        private static final t0<FunctionCall> PARSER = new c<FunctionCall>() { // from class: wallet.core.jni.proto.NEAR.FunctionCall.1
            @Override // com.google.protobuf.t0
            public FunctionCall parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new FunctionCall(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements FunctionCallOrBuilder {
            private ByteString args_;
            private ByteString deposit_;
            private long gas_;
            private ByteString methodName_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_FunctionCall_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearArgs() {
                this.args_ = FunctionCall.getDefaultInstance().getArgs();
                onChanged();
                return this;
            }

            public Builder clearDeposit() {
                this.deposit_ = FunctionCall.getDefaultInstance().getDeposit();
                onChanged();
                return this;
            }

            public Builder clearGas() {
                this.gas_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearMethodName() {
                this.methodName_ = FunctionCall.getDefaultInstance().getMethodName();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
            public ByteString getArgs() {
                return this.args_;
            }

            @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
            public ByteString getDeposit() {
                return this.deposit_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_FunctionCall_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
            public long getGas() {
                return this.gas_;
            }

            @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
            public ByteString getMethodName() {
                return this.methodName_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_FunctionCall_fieldAccessorTable.d(FunctionCall.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setArgs(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.args_ = byteString;
                onChanged();
                return this;
            }

            public Builder setDeposit(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.deposit_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGas(long j) {
                this.gas_ = j;
                onChanged();
                return this;
            }

            public Builder setMethodName(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.methodName_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.methodName_ = byteString;
                this.args_ = byteString;
                this.deposit_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FunctionCall build() {
                FunctionCall buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FunctionCall buildPartial() {
                FunctionCall functionCall = new FunctionCall(this, (AnonymousClass1) null);
                functionCall.methodName_ = this.methodName_;
                functionCall.args_ = this.args_;
                functionCall.gas_ = this.gas_;
                functionCall.deposit_ = this.deposit_;
                onBuilt();
                return functionCall;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public FunctionCall getDefaultInstanceForType() {
                return FunctionCall.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.methodName_ = byteString;
                this.args_ = byteString;
                this.gas_ = 0L;
                this.deposit_ = byteString;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof FunctionCall) {
                    return mergeFrom((FunctionCall) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.methodName_ = byteString;
                this.args_ = byteString;
                this.deposit_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(FunctionCall functionCall) {
                if (functionCall == FunctionCall.getDefaultInstance()) {
                    return this;
                }
                ByteString methodName = functionCall.getMethodName();
                ByteString byteString = ByteString.EMPTY;
                if (methodName != byteString) {
                    setMethodName(functionCall.getMethodName());
                }
                if (functionCall.getArgs() != byteString) {
                    setArgs(functionCall.getArgs());
                }
                if (functionCall.getGas() != 0) {
                    setGas(functionCall.getGas());
                }
                if (functionCall.getDeposit() != byteString) {
                    setDeposit(functionCall.getDeposit());
                }
                mergeUnknownFields(functionCall.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.FunctionCall.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.FunctionCall.access$7400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$FunctionCall r3 = (wallet.core.jni.proto.NEAR.FunctionCall) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$FunctionCall r4 = (wallet.core.jni.proto.NEAR.FunctionCall) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.FunctionCall.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$FunctionCall$Builder");
            }
        }

        public /* synthetic */ FunctionCall(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static FunctionCall getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_FunctionCall_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static FunctionCall parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FunctionCall) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FunctionCall parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FunctionCall> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FunctionCall)) {
                return super.equals(obj);
            }
            FunctionCall functionCall = (FunctionCall) obj;
            return getMethodName().equals(functionCall.getMethodName()) && getArgs().equals(functionCall.getArgs()) && getGas() == functionCall.getGas() && getDeposit().equals(functionCall.getDeposit()) && this.unknownFields.equals(functionCall.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
        public ByteString getArgs() {
            return this.args_;
        }

        @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
        public ByteString getDeposit() {
            return this.deposit_;
        }

        @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
        public long getGas() {
            return this.gas_;
        }

        @Override // wallet.core.jni.proto.NEAR.FunctionCallOrBuilder
        public ByteString getMethodName() {
            return this.methodName_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FunctionCall> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.methodName_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.methodName_);
            if (!this.args_.isEmpty()) {
                h += CodedOutputStream.h(2, this.args_);
            }
            long j = this.gas_;
            if (j != 0) {
                h += CodedOutputStream.a0(3, j);
            }
            if (!this.deposit_.isEmpty()) {
                h += CodedOutputStream.h(4, this.deposit_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getMethodName().hashCode()) * 37) + 2) * 53) + getArgs().hashCode()) * 37) + 3) * 53) + a0.h(getGas())) * 37) + 4) * 53) + getDeposit().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_FunctionCall_fieldAccessorTable.d(FunctionCall.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FunctionCall();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.methodName_.isEmpty()) {
                codedOutputStream.q0(1, this.methodName_);
            }
            if (!this.args_.isEmpty()) {
                codedOutputStream.q0(2, this.args_);
            }
            long j = this.gas_;
            if (j != 0) {
                codedOutputStream.d1(3, j);
            }
            if (!this.deposit_.isEmpty()) {
                codedOutputStream.q0(4, this.deposit_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ FunctionCall(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(FunctionCall functionCall) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(functionCall);
        }

        public static FunctionCall parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private FunctionCall(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FunctionCall parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (FunctionCall) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FunctionCall parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FunctionCall getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static FunctionCall parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private FunctionCall() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.methodName_ = byteString;
            this.args_ = byteString;
            this.deposit_ = byteString;
        }

        public static FunctionCall parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static FunctionCall parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static FunctionCall parseFrom(InputStream inputStream) throws IOException {
            return (FunctionCall) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static FunctionCall parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (FunctionCall) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private FunctionCall(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.methodName_ = jVar.q();
                            } else if (J == 18) {
                                this.args_ = jVar.q();
                            } else if (J == 24) {
                                this.gas_ = jVar.L();
                            } else if (J != 34) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.deposit_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static FunctionCall parseFrom(j jVar) throws IOException {
            return (FunctionCall) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FunctionCall parseFrom(j jVar, r rVar) throws IOException {
            return (FunctionCall) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface FunctionCallOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getArgs();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        ByteString getDeposit();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGas();

        /* synthetic */ String getInitializationErrorString();

        ByteString getMethodName();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class FunctionCallPermission extends GeneratedMessageV3 implements FunctionCallPermissionOrBuilder {
        public static final int ALLOWANCE_FIELD_NUMBER = 1;
        private static final FunctionCallPermission DEFAULT_INSTANCE = new FunctionCallPermission();
        private static final t0<FunctionCallPermission> PARSER = new c<FunctionCallPermission>() { // from class: wallet.core.jni.proto.NEAR.FunctionCallPermission.1
            @Override // com.google.protobuf.t0
            public FunctionCallPermission parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new FunctionCallPermission(jVar, rVar, null);
            }
        };
        public static final int RECEIVER_ID_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString allowance_;
        private byte memoizedIsInitialized;
        private volatile Object receiverId_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements FunctionCallPermissionOrBuilder {
            private ByteString allowance_;
            private Object receiverId_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_FunctionCallPermission_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAllowance() {
                this.allowance_ = FunctionCallPermission.getDefaultInstance().getAllowance();
                onChanged();
                return this;
            }

            public Builder clearReceiverId() {
                this.receiverId_ = FunctionCallPermission.getDefaultInstance().getReceiverId();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.FunctionCallPermissionOrBuilder
            public ByteString getAllowance() {
                return this.allowance_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_FunctionCallPermission_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.FunctionCallPermissionOrBuilder
            public String getReceiverId() {
                Object obj = this.receiverId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.receiverId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEAR.FunctionCallPermissionOrBuilder
            public ByteString getReceiverIdBytes() {
                Object obj = this.receiverId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.receiverId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_FunctionCallPermission_fieldAccessorTable.d(FunctionCallPermission.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAllowance(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.allowance_ = byteString;
                onChanged();
                return this;
            }

            public Builder setReceiverId(String str) {
                Objects.requireNonNull(str);
                this.receiverId_ = str;
                onChanged();
                return this;
            }

            public Builder setReceiverIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.receiverId_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.allowance_ = ByteString.EMPTY;
                this.receiverId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FunctionCallPermission build() {
                FunctionCallPermission buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FunctionCallPermission buildPartial() {
                FunctionCallPermission functionCallPermission = new FunctionCallPermission(this, (AnonymousClass1) null);
                functionCallPermission.allowance_ = this.allowance_;
                functionCallPermission.receiverId_ = this.receiverId_;
                onBuilt();
                return functionCallPermission;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public FunctionCallPermission getDefaultInstanceForType() {
                return FunctionCallPermission.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.allowance_ = ByteString.EMPTY;
                this.receiverId_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.allowance_ = ByteString.EMPTY;
                this.receiverId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof FunctionCallPermission) {
                    return mergeFrom((FunctionCallPermission) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(FunctionCallPermission functionCallPermission) {
                if (functionCallPermission == FunctionCallPermission.getDefaultInstance()) {
                    return this;
                }
                if (functionCallPermission.getAllowance() != ByteString.EMPTY) {
                    setAllowance(functionCallPermission.getAllowance());
                }
                if (!functionCallPermission.getReceiverId().isEmpty()) {
                    this.receiverId_ = functionCallPermission.receiverId_;
                    onChanged();
                }
                mergeUnknownFields(functionCallPermission.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.FunctionCallPermission.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.FunctionCallPermission.access$2000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$FunctionCallPermission r3 = (wallet.core.jni.proto.NEAR.FunctionCallPermission) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$FunctionCallPermission r4 = (wallet.core.jni.proto.NEAR.FunctionCallPermission) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.FunctionCallPermission.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$FunctionCallPermission$Builder");
            }
        }

        public /* synthetic */ FunctionCallPermission(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static FunctionCallPermission getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_FunctionCallPermission_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static FunctionCallPermission parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FunctionCallPermission) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FunctionCallPermission parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FunctionCallPermission> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FunctionCallPermission)) {
                return super.equals(obj);
            }
            FunctionCallPermission functionCallPermission = (FunctionCallPermission) obj;
            return getAllowance().equals(functionCallPermission.getAllowance()) && getReceiverId().equals(functionCallPermission.getReceiverId()) && this.unknownFields.equals(functionCallPermission.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEAR.FunctionCallPermissionOrBuilder
        public ByteString getAllowance() {
            return this.allowance_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FunctionCallPermission> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEAR.FunctionCallPermissionOrBuilder
        public String getReceiverId() {
            Object obj = this.receiverId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.receiverId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEAR.FunctionCallPermissionOrBuilder
        public ByteString getReceiverIdBytes() {
            Object obj = this.receiverId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.receiverId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.allowance_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.allowance_);
            if (!getReceiverIdBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(2, this.receiverId_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAllowance().hashCode()) * 37) + 2) * 53) + getReceiverId().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_FunctionCallPermission_fieldAccessorTable.d(FunctionCallPermission.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FunctionCallPermission();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.allowance_.isEmpty()) {
                codedOutputStream.q0(1, this.allowance_);
            }
            if (!getReceiverIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.receiverId_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ FunctionCallPermission(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(FunctionCallPermission functionCallPermission) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(functionCallPermission);
        }

        public static FunctionCallPermission parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private FunctionCallPermission(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FunctionCallPermission parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (FunctionCallPermission) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FunctionCallPermission parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FunctionCallPermission getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static FunctionCallPermission parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private FunctionCallPermission() {
            this.memoizedIsInitialized = (byte) -1;
            this.allowance_ = ByteString.EMPTY;
            this.receiverId_ = "";
        }

        public static FunctionCallPermission parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static FunctionCallPermission parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static FunctionCallPermission parseFrom(InputStream inputStream) throws IOException {
            return (FunctionCallPermission) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private FunctionCallPermission(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.allowance_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.receiverId_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static FunctionCallPermission parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (FunctionCallPermission) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static FunctionCallPermission parseFrom(j jVar) throws IOException {
            return (FunctionCallPermission) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FunctionCallPermission parseFrom(j jVar, r rVar) throws IOException {
            return (FunctionCallPermission) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface FunctionCallPermissionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAllowance();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getReceiverId();

        ByteString getReceiverIdBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class PublicKey extends GeneratedMessageV3 implements PublicKeyOrBuilder {
        public static final int DATA_FIELD_NUMBER = 2;
        public static final int KEY_TYPE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString data_;
        private int keyType_;
        private byte memoizedIsInitialized;
        private static final PublicKey DEFAULT_INSTANCE = new PublicKey();
        private static final t0<PublicKey> PARSER = new c<PublicKey>() { // from class: wallet.core.jni.proto.NEAR.PublicKey.1
            @Override // com.google.protobuf.t0
            public PublicKey parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new PublicKey(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements PublicKeyOrBuilder {
            private ByteString data_;
            private int keyType_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_PublicKey_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearData() {
                this.data_ = PublicKey.getDefaultInstance().getData();
                onChanged();
                return this;
            }

            public Builder clearKeyType() {
                this.keyType_ = 0;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.PublicKeyOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_PublicKey_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.PublicKeyOrBuilder
            public int getKeyType() {
                return this.keyType_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_PublicKey_fieldAccessorTable.d(PublicKey.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setData(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.data_ = byteString;
                onChanged();
                return this;
            }

            public Builder setKeyType(int i) {
                this.keyType_ = i;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.data_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public PublicKey build() {
                PublicKey buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public PublicKey buildPartial() {
                PublicKey publicKey = new PublicKey(this, (AnonymousClass1) null);
                publicKey.keyType_ = this.keyType_;
                publicKey.data_ = this.data_;
                onBuilt();
                return publicKey;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public PublicKey getDefaultInstanceForType() {
                return PublicKey.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.keyType_ = 0;
                this.data_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.data_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof PublicKey) {
                    return mergeFrom((PublicKey) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(PublicKey publicKey) {
                if (publicKey == PublicKey.getDefaultInstance()) {
                    return this;
                }
                if (publicKey.getKeyType() != 0) {
                    setKeyType(publicKey.getKeyType());
                }
                if (publicKey.getData() != ByteString.EMPTY) {
                    setData(publicKey.getData());
                }
                mergeUnknownFields(publicKey.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.PublicKey.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.PublicKey.access$900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$PublicKey r3 = (wallet.core.jni.proto.NEAR.PublicKey) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$PublicKey r4 = (wallet.core.jni.proto.NEAR.PublicKey) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.PublicKey.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$PublicKey$Builder");
            }
        }

        public /* synthetic */ PublicKey(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static PublicKey getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_PublicKey_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static PublicKey parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (PublicKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static PublicKey parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<PublicKey> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof PublicKey)) {
                return super.equals(obj);
            }
            PublicKey publicKey = (PublicKey) obj;
            return getKeyType() == publicKey.getKeyType() && getData().equals(publicKey.getData()) && this.unknownFields.equals(publicKey.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEAR.PublicKeyOrBuilder
        public ByteString getData() {
            return this.data_;
        }

        @Override // wallet.core.jni.proto.NEAR.PublicKeyOrBuilder
        public int getKeyType() {
            return this.keyType_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<PublicKey> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.keyType_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            if (!this.data_.isEmpty()) {
                Y += CodedOutputStream.h(2, this.data_);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getKeyType()) * 37) + 2) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_PublicKey_fieldAccessorTable.d(PublicKey.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new PublicKey();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.keyType_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            if (!this.data_.isEmpty()) {
                codedOutputStream.q0(2, this.data_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ PublicKey(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(PublicKey publicKey) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(publicKey);
        }

        public static PublicKey parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private PublicKey(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static PublicKey parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (PublicKey) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static PublicKey parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public PublicKey getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static PublicKey parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private PublicKey() {
            this.memoizedIsInitialized = (byte) -1;
            this.data_ = ByteString.EMPTY;
        }

        public static PublicKey parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static PublicKey parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static PublicKey parseFrom(InputStream inputStream) throws IOException {
            return (PublicKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private PublicKey(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.keyType_ = jVar.K();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.data_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static PublicKey parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (PublicKey) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static PublicKey parseFrom(j jVar) throws IOException {
            return (PublicKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static PublicKey parseFrom(j jVar, r rVar) throws IOException {
            return (PublicKey) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface PublicKeyOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getData();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        int getKeyType();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int ACTIONS_FIELD_NUMBER = 5;
        public static final int BLOCK_HASH_FIELD_NUMBER = 4;
        public static final int NONCE_FIELD_NUMBER = 2;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 6;
        public static final int RECEIVER_ID_FIELD_NUMBER = 3;
        public static final int SIGNER_ID_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private List<Action> actions_;
        private ByteString blockHash_;
        private byte memoizedIsInitialized;
        private long nonce_;
        private ByteString privateKey_;
        private volatile Object receiverId_;
        private volatile Object signerId_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.NEAR.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private x0<Action, Action.Builder, ActionOrBuilder> actionsBuilder_;
            private List<Action> actions_;
            private int bitField0_;
            private ByteString blockHash_;
            private long nonce_;
            private ByteString privateKey_;
            private Object receiverId_;
            private Object signerId_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private void ensureActionsIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.actions_ = new ArrayList(this.actions_);
                    this.bitField0_ |= 1;
                }
            }

            private x0<Action, Action.Builder, ActionOrBuilder> getActionsFieldBuilder() {
                if (this.actionsBuilder_ == null) {
                    this.actionsBuilder_ = new x0<>(this.actions_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.actions_ = null;
                }
                return this.actionsBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getActionsFieldBuilder();
                }
            }

            public Builder addActions(Action action) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(action);
                    ensureActionsIsMutable();
                    this.actions_.add(action);
                    onChanged();
                } else {
                    x0Var.f(action);
                }
                return this;
            }

            public Action.Builder addActionsBuilder() {
                return getActionsFieldBuilder().d(Action.getDefaultInstance());
            }

            public Builder addAllActions(Iterable<? extends Action> iterable) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    ensureActionsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.actions_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder clearActions() {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    this.actions_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearBlockHash() {
                this.blockHash_ = SigningInput.getDefaultInstance().getBlockHash();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearReceiverId() {
                this.receiverId_ = SigningInput.getDefaultInstance().getReceiverId();
                onChanged();
                return this;
            }

            public Builder clearSignerId() {
                this.signerId_ = SigningInput.getDefaultInstance().getSignerId();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public Action getActions(int i) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    return this.actions_.get(i);
                }
                return x0Var.o(i);
            }

            public Action.Builder getActionsBuilder(int i) {
                return getActionsFieldBuilder().l(i);
            }

            public List<Action.Builder> getActionsBuilderList() {
                return getActionsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public int getActionsCount() {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    return this.actions_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public List<Action> getActionsList() {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.actions_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public ActionOrBuilder getActionsOrBuilder(int i) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    return this.actions_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public List<? extends ActionOrBuilder> getActionsOrBuilderList() {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.actions_);
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public ByteString getBlockHash() {
                return this.blockHash_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public String getReceiverId() {
                Object obj = this.receiverId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.receiverId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public ByteString getReceiverIdBytes() {
                Object obj = this.receiverId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.receiverId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public String getSignerId() {
                Object obj = this.signerId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.signerId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
            public ByteString getSignerIdBytes() {
                Object obj = this.signerId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.signerId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeActions(int i) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    ensureActionsIsMutable();
                    this.actions_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setActions(int i, Action action) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(action);
                    ensureActionsIsMutable();
                    this.actions_.set(i, action);
                    onChanged();
                } else {
                    x0Var.x(i, action);
                }
                return this;
            }

            public Builder setBlockHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.blockHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setReceiverId(String str) {
                Objects.requireNonNull(str);
                this.receiverId_ = str;
                onChanged();
                return this;
            }

            public Builder setReceiverIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.receiverId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignerId(String str) {
                Objects.requireNonNull(str);
                this.signerId_ = str;
                onChanged();
                return this;
            }

            public Builder setSignerIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.signerId_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.signerId_ = "";
                this.receiverId_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.blockHash_ = byteString;
                this.actions_ = Collections.emptyList();
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.signerId_ = this.signerId_;
                signingInput.nonce_ = this.nonce_;
                signingInput.receiverId_ = this.receiverId_;
                signingInput.blockHash_ = this.blockHash_;
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var != null) {
                    signingInput.actions_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.actions_ = Collections.unmodifiableList(this.actions_);
                        this.bitField0_ &= -2;
                    }
                    signingInput.actions_ = this.actions_;
                }
                signingInput.privateKey_ = this.privateKey_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public Action.Builder addActionsBuilder(int i) {
                return getActionsFieldBuilder().c(i, Action.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.signerId_ = "";
                this.nonce_ = 0L;
                this.receiverId_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.blockHash_ = byteString;
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    this.actions_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                this.privateKey_ = byteString;
                return this;
            }

            public Builder addActions(int i, Action action) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(action);
                    ensureActionsIsMutable();
                    this.actions_.add(i, action);
                    onChanged();
                } else {
                    x0Var.e(i, action);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setActions(int i, Action.Builder builder) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    ensureActionsIsMutable();
                    this.actions_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.signerId_ = "";
                this.receiverId_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.blockHash_ = byteString;
                this.actions_ = Collections.emptyList();
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (!signingInput.getSignerId().isEmpty()) {
                    this.signerId_ = signingInput.signerId_;
                    onChanged();
                }
                if (signingInput.getNonce() != 0) {
                    setNonce(signingInput.getNonce());
                }
                if (!signingInput.getReceiverId().isEmpty()) {
                    this.receiverId_ = signingInput.receiverId_;
                    onChanged();
                }
                ByteString blockHash = signingInput.getBlockHash();
                ByteString byteString = ByteString.EMPTY;
                if (blockHash != byteString) {
                    setBlockHash(signingInput.getBlockHash());
                }
                if (this.actionsBuilder_ == null) {
                    if (!signingInput.actions_.isEmpty()) {
                        if (this.actions_.isEmpty()) {
                            this.actions_ = signingInput.actions_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureActionsIsMutable();
                            this.actions_.addAll(signingInput.actions_);
                        }
                        onChanged();
                    }
                } else if (!signingInput.actions_.isEmpty()) {
                    if (!this.actionsBuilder_.u()) {
                        this.actionsBuilder_.b(signingInput.actions_);
                    } else {
                        this.actionsBuilder_.i();
                        this.actionsBuilder_ = null;
                        this.actions_ = signingInput.actions_;
                        this.bitField0_ &= -2;
                        this.actionsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getActionsFieldBuilder() : null;
                    }
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            public Builder addActions(Action.Builder builder) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    ensureActionsIsMutable();
                    this.actions_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addActions(int i, Action.Builder builder) {
                x0<Action, Action.Builder, ActionOrBuilder> x0Var = this.actionsBuilder_;
                if (x0Var == null) {
                    ensureActionsIsMutable();
                    this.actions_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.SigningInput.access$15500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$SigningInput r3 = (wallet.core.jni.proto.NEAR.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$SigningInput r4 = (wallet.core.jni.proto.NEAR.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            return getSignerId().equals(signingInput.getSignerId()) && getNonce() == signingInput.getNonce() && getReceiverId().equals(signingInput.getReceiverId()) && getBlockHash().equals(signingInput.getBlockHash()) && getActionsList().equals(signingInput.getActionsList()) && getPrivateKey().equals(signingInput.getPrivateKey()) && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public Action getActions(int i) {
            return this.actions_.get(i);
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public int getActionsCount() {
            return this.actions_.size();
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public List<Action> getActionsList() {
            return this.actions_;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public ActionOrBuilder getActionsOrBuilder(int i) {
            return this.actions_.get(i);
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public List<? extends ActionOrBuilder> getActionsOrBuilderList() {
            return this.actions_;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public ByteString getBlockHash() {
            return this.blockHash_;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public String getReceiverId() {
            Object obj = this.receiverId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.receiverId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public ByteString getReceiverIdBytes() {
            Object obj = this.receiverId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.receiverId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = !getSignerIdBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.signerId_) + 0 : 0;
            long j = this.nonce_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(2, j);
            }
            if (!getReceiverIdBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.receiverId_);
            }
            if (!this.blockHash_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.blockHash_);
            }
            for (int i2 = 0; i2 < this.actions_.size(); i2++) {
                computeStringSize += CodedOutputStream.G(5, this.actions_.get(i2));
            }
            if (!this.privateKey_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(6, this.privateKey_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public String getSignerId() {
            Object obj = this.signerId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.signerId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningInputOrBuilder
        public ByteString getSignerIdBytes() {
            Object obj = this.signerId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.signerId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSignerId().hashCode()) * 37) + 2) * 53) + a0.h(getNonce())) * 37) + 3) * 53) + getReceiverId().hashCode()) * 37) + 4) * 53) + getBlockHash().hashCode();
            if (getActionsCount() > 0) {
                hashCode = (((hashCode * 37) + 5) * 53) + getActionsList().hashCode();
            }
            int hashCode2 = (((((hashCode * 37) + 6) * 53) + getPrivateKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getSignerIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.signerId_);
            }
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            if (!getReceiverIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.receiverId_);
            }
            if (!this.blockHash_.isEmpty()) {
                codedOutputStream.q0(4, this.blockHash_);
            }
            for (int i = 0; i < this.actions_.size(); i++) {
                codedOutputStream.K0(5, this.actions_.get(i));
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(6, this.privateKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.signerId_ = "";
            this.receiverId_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.blockHash_ = byteString;
            this.actions_ = Collections.emptyList();
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.signerId_ = jVar.I();
                                } else if (J == 16) {
                                    this.nonce_ = jVar.L();
                                } else if (J == 26) {
                                    this.receiverId_ = jVar.I();
                                } else if (J == 34) {
                                    this.blockHash_ = jVar.q();
                                } else if (J == 42) {
                                    if (!(z2 & true)) {
                                        this.actions_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.actions_.add(jVar.z(Action.parser(), rVar));
                                } else if (J != 50) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.privateKey_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.actions_ = Collections.unmodifiableList(this.actions_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        Action getActions(int i);

        int getActionsCount();

        List<Action> getActionsList();

        ActionOrBuilder getActionsOrBuilder(int i);

        List<? extends ActionOrBuilder> getActionsOrBuilderList();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getBlockHash();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        String getReceiverId();

        ByteString getReceiverIdBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSignerId();

        ByteString getSignerIdBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.NEAR.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };
        public static final int SIGNED_TRANSACTION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString signedTransaction_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString signedTransaction_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearSignedTransaction() {
                this.signedTransaction_ = SigningOutput.getDefaultInstance().getSignedTransaction();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.SigningOutputOrBuilder
            public ByteString getSignedTransaction() {
                return this.signedTransaction_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setSignedTransaction(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signedTransaction_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.signedTransaction_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.signedTransaction_ = this.signedTransaction_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.signedTransaction_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.signedTransaction_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getSignedTransaction() != ByteString.EMPTY) {
                    setSignedTransaction(signingOutput.getSignedTransaction());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.SigningOutput.access$16700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$SigningOutput r3 = (wallet.core.jni.proto.NEAR.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$SigningOutput r4 = (wallet.core.jni.proto.NEAR.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getSignedTransaction().equals(signingOutput.getSignedTransaction()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.signedTransaction_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.signedTransaction_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // wallet.core.jni.proto.NEAR.SigningOutputOrBuilder
        public ByteString getSignedTransaction() {
            return this.signedTransaction_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSignedTransaction().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.signedTransaction_.isEmpty()) {
                codedOutputStream.q0(1, this.signedTransaction_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.signedTransaction_ = ByteString.EMPTY;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.signedTransaction_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignedTransaction();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Stake extends GeneratedMessageV3 implements StakeOrBuilder {
        private static final Stake DEFAULT_INSTANCE = new Stake();
        private static final t0<Stake> PARSER = new c<Stake>() { // from class: wallet.core.jni.proto.NEAR.Stake.1
            @Override // com.google.protobuf.t0
            public Stake parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Stake(jVar, rVar, null);
            }
        };
        public static final int PUBLIC_KEY_FIELD_NUMBER = 2;
        public static final int STAKE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object publicKey_;
        private ByteString stake_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements StakeOrBuilder {
            private Object publicKey_;
            private ByteString stake_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_Stake_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPublicKey() {
                this.publicKey_ = Stake.getDefaultInstance().getPublicKey();
                onChanged();
                return this;
            }

            public Builder clearStake() {
                this.stake_ = Stake.getDefaultInstance().getStake();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_Stake_descriptor;
            }

            @Override // wallet.core.jni.proto.NEAR.StakeOrBuilder
            public String getPublicKey() {
                Object obj = this.publicKey_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.publicKey_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEAR.StakeOrBuilder
            public ByteString getPublicKeyBytes() {
                Object obj = this.publicKey_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.publicKey_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NEAR.StakeOrBuilder
            public ByteString getStake() {
                return this.stake_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_Stake_fieldAccessorTable.d(Stake.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setPublicKey(String str) {
                Objects.requireNonNull(str);
                this.publicKey_ = str;
                onChanged();
                return this;
            }

            public Builder setPublicKeyBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.publicKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setStake(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.stake_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.stake_ = ByteString.EMPTY;
                this.publicKey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Stake build() {
                Stake buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Stake buildPartial() {
                Stake stake = new Stake(this, (AnonymousClass1) null);
                stake.stake_ = this.stake_;
                stake.publicKey_ = this.publicKey_;
                onBuilt();
                return stake;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Stake getDefaultInstanceForType() {
                return Stake.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.stake_ = ByteString.EMPTY;
                this.publicKey_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.stake_ = ByteString.EMPTY;
                this.publicKey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Stake) {
                    return mergeFrom((Stake) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Stake stake) {
                if (stake == Stake.getDefaultInstance()) {
                    return this;
                }
                if (stake.getStake() != ByteString.EMPTY) {
                    setStake(stake.getStake());
                }
                if (!stake.getPublicKey().isEmpty()) {
                    this.publicKey_ = stake.publicKey_;
                    onChanged();
                }
                mergeUnknownFields(stake.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.Stake.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.Stake.access$9500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$Stake r3 = (wallet.core.jni.proto.NEAR.Stake) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$Stake r4 = (wallet.core.jni.proto.NEAR.Stake) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.Stake.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$Stake$Builder");
            }
        }

        public /* synthetic */ Stake(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Stake getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_Stake_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Stake parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Stake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Stake parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Stake> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Stake)) {
                return super.equals(obj);
            }
            Stake stake = (Stake) obj;
            return getStake().equals(stake.getStake()) && getPublicKey().equals(stake.getPublicKey()) && this.unknownFields.equals(stake.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Stake> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEAR.StakeOrBuilder
        public String getPublicKey() {
            Object obj = this.publicKey_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.publicKey_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEAR.StakeOrBuilder
        public ByteString getPublicKeyBytes() {
            Object obj = this.publicKey_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.publicKey_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.stake_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.stake_);
            if (!getPublicKeyBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(2, this.publicKey_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NEAR.StakeOrBuilder
        public ByteString getStake() {
            return this.stake_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getStake().hashCode()) * 37) + 2) * 53) + getPublicKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_Stake_fieldAccessorTable.d(Stake.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Stake();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.stake_.isEmpty()) {
                codedOutputStream.q0(1, this.stake_);
            }
            if (!getPublicKeyBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.publicKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Stake(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Stake stake) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(stake);
        }

        public static Stake parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Stake(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Stake parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Stake parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Stake getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Stake parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Stake() {
            this.memoizedIsInitialized = (byte) -1;
            this.stake_ = ByteString.EMPTY;
            this.publicKey_ = "";
        }

        public static Stake parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Stake parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Stake parseFrom(InputStream inputStream) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Stake(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.stake_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.publicKey_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Stake parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Stake parseFrom(j jVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Stake parseFrom(j jVar, r rVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface StakeOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getPublicKey();

        ByteString getPublicKeyBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getStake();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transfer extends GeneratedMessageV3 implements TransferOrBuilder {
        public static final int DEPOSIT_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString deposit_;
        private byte memoizedIsInitialized;
        private static final Transfer DEFAULT_INSTANCE = new Transfer();
        private static final t0<Transfer> PARSER = new c<Transfer>() { // from class: wallet.core.jni.proto.NEAR.Transfer.1
            @Override // com.google.protobuf.t0
            public Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transfer(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOrBuilder {
            private ByteString deposit_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return NEAR.internal_static_TW_NEAR_Proto_Transfer_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearDeposit() {
                this.deposit_ = Transfer.getDefaultInstance().getDeposit();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEAR.TransferOrBuilder
            public ByteString getDeposit() {
                return this.deposit_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEAR.internal_static_TW_NEAR_Proto_Transfer_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEAR.internal_static_TW_NEAR_Proto_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setDeposit(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.deposit_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.deposit_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transfer build() {
                Transfer buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transfer buildPartial() {
                Transfer transfer = new Transfer(this, (AnonymousClass1) null);
                transfer.deposit_ = this.deposit_;
                onBuilt();
                return transfer;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transfer getDefaultInstanceForType() {
                return Transfer.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.deposit_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.deposit_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transfer) {
                    return mergeFrom((Transfer) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Transfer transfer) {
                if (transfer == Transfer.getDefaultInstance()) {
                    return this;
                }
                if (transfer.getDeposit() != ByteString.EMPTY) {
                    setDeposit(transfer.getDeposit());
                }
                mergeUnknownFields(transfer.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEAR.Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEAR.Transfer.access$8400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEAR$Transfer r3 = (wallet.core.jni.proto.NEAR.Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEAR$Transfer r4 = (wallet.core.jni.proto.NEAR.Transfer) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEAR.Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEAR$Transfer$Builder");
            }
        }

        public /* synthetic */ Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Transfer getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEAR.internal_static_TW_NEAR_Proto_Transfer_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transfer> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transfer)) {
                return super.equals(obj);
            }
            Transfer transfer = (Transfer) obj;
            return getDeposit().equals(transfer.getDeposit()) && this.unknownFields.equals(transfer.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEAR.TransferOrBuilder
        public ByteString getDeposit() {
            return this.deposit_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transfer> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.deposit_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.deposit_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDeposit().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEAR.internal_static_TW_NEAR_Proto_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transfer();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.deposit_.isEmpty()) {
                codedOutputStream.q0(1, this.deposit_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Transfer transfer) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transfer);
        }

        public static Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transfer(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transfer getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Transfer() {
            this.memoizedIsInitialized = (byte) -1;
            this.deposit_ = ByteString.EMPTY;
        }

        public static Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transfer parseFrom(InputStream inputStream) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.deposit_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transfer parseFrom(j jVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transfer parseFrom(j jVar, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransferOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        ByteString getDeposit();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_NEAR_Proto_PublicKey_descriptor = bVar;
        internal_static_TW_NEAR_Proto_PublicKey_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"KeyType", "Data"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_NEAR_Proto_FunctionCallPermission_descriptor = bVar2;
        internal_static_TW_NEAR_Proto_FunctionCallPermission_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Allowance", "ReceiverId"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_NEAR_Proto_FullAccessPermission_descriptor = bVar3;
        internal_static_TW_NEAR_Proto_FullAccessPermission_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[0]);
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_NEAR_Proto_AccessKey_descriptor = bVar4;
        internal_static_TW_NEAR_Proto_AccessKey_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Nonce", "FunctionCall", "FullAccess", "Permission"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_NEAR_Proto_CreateAccount_descriptor = bVar5;
        internal_static_TW_NEAR_Proto_CreateAccount_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[0]);
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_NEAR_Proto_DeployContract_descriptor = bVar6;
        internal_static_TW_NEAR_Proto_DeployContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"Code"});
        Descriptors.b bVar7 = getDescriptor().o().get(6);
        internal_static_TW_NEAR_Proto_FunctionCall_descriptor = bVar7;
        internal_static_TW_NEAR_Proto_FunctionCall_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"MethodName", "Args", "Gas", "Deposit"});
        Descriptors.b bVar8 = getDescriptor().o().get(7);
        internal_static_TW_NEAR_Proto_Transfer_descriptor = bVar8;
        internal_static_TW_NEAR_Proto_Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"Deposit"});
        Descriptors.b bVar9 = getDescriptor().o().get(8);
        internal_static_TW_NEAR_Proto_Stake_descriptor = bVar9;
        internal_static_TW_NEAR_Proto_Stake_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"Stake", "PublicKey"});
        Descriptors.b bVar10 = getDescriptor().o().get(9);
        internal_static_TW_NEAR_Proto_AddKey_descriptor = bVar10;
        internal_static_TW_NEAR_Proto_AddKey_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"PublicKey", "AccessKey"});
        Descriptors.b bVar11 = getDescriptor().o().get(10);
        internal_static_TW_NEAR_Proto_DeleteKey_descriptor = bVar11;
        internal_static_TW_NEAR_Proto_DeleteKey_fieldAccessorTable = new GeneratedMessageV3.e(bVar11, new String[]{"PublicKey"});
        Descriptors.b bVar12 = getDescriptor().o().get(11);
        internal_static_TW_NEAR_Proto_DeleteAccount_descriptor = bVar12;
        internal_static_TW_NEAR_Proto_DeleteAccount_fieldAccessorTable = new GeneratedMessageV3.e(bVar12, new String[]{"BeneficiaryId"});
        Descriptors.b bVar13 = getDescriptor().o().get(12);
        internal_static_TW_NEAR_Proto_Action_descriptor = bVar13;
        internal_static_TW_NEAR_Proto_Action_fieldAccessorTable = new GeneratedMessageV3.e(bVar13, new String[]{"CreateAccount", "DeployContract", "FunctionCall", "Transfer", "Stake", "AddKey", "DeleteKey", "DeleteAccount", "Payload"});
        Descriptors.b bVar14 = getDescriptor().o().get(13);
        internal_static_TW_NEAR_Proto_SigningInput_descriptor = bVar14;
        internal_static_TW_NEAR_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar14, new String[]{"SignerId", "Nonce", "ReceiverId", "BlockHash", "Actions", "PrivateKey"});
        Descriptors.b bVar15 = getDescriptor().o().get(14);
        internal_static_TW_NEAR_Proto_SigningOutput_descriptor = bVar15;
        internal_static_TW_NEAR_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar15, new String[]{"SignedTransaction"});
    }

    private NEAR() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
