package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Harmony {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\rHarmony.proto\u0012\u0010TW.Harmony.Proto\"È\u0001\n\fSigningInput\u0012\u0010\n\bchain_id\u0018\u0001 \u0001(\f\u0012\u0013\n\u000bprivate_key\u0018\u0002 \u0001(\f\u0012C\n\u0013transaction_message\u0018\u0003 \u0001(\u000b2$.TW.Harmony.Proto.TransactionMessageH\u0000\u0012;\n\u000fstaking_message\u0018\u0004 \u0001(\u000b2 .TW.Harmony.Proto.StakingMessageH\u0000B\u000f\n\rmessage_oneof\"A\n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f\u0012\t\n\u0001v\u0018\u0002 \u0001(\f\u0012\t\n\u0001r\u0018\u0003 \u0001(\f\u0012\t\n\u0001s\u0018\u0004 \u0001(\f\"ª\u0001\n\u0012TransactionMessage\u0012\r\n\u0005nonce\u0018\u0001 \u0001(\f\u0012\u0011\n\tgas_price\u0018\u0002 \u0001(\f\u0012\u0011\n\tgas_limit\u0018\u0003 \u0001(\f\u0012\u0012\n\nto_address\u0018\u0004 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0005 \u0001(\f\u0012\u000f\n\u0007payload\u0018\u0006 \u0001(\f\u0012\u0015\n\rfrom_shard_id\u0018\u0007 \u0001(\f\u0012\u0013\n\u000bto_shard_id\u0018\b \u0001(\f\"º\u0003\n\u000eStakingMessage\u0012N\n\u0018create_validator_message\u0018\u0001 \u0001(\u000b2*.TW.Harmony.Proto.DirectiveCreateValidatorH\u0000\u0012J\n\u0016edit_validator_message\u0018\u0002 \u0001(\u000b2(.TW.Harmony.Proto.DirectiveEditValidatorH\u0000\u0012?\n\u0010delegate_message\u0018\u0003 \u0001(\u000b2#.TW.Harmony.Proto.DirectiveDelegateH\u0000\u0012C\n\u0012undelegate_message\u0018\u0004 \u0001(\u000b2%.TW.Harmony.Proto.DirectiveUndelegateH\u0000\u0012D\n\u000fcollect_rewards\u0018\u0005 \u0001(\u000b2).TW.Harmony.Proto.DirectiveCollectRewardsH\u0000\u0012\r\n\u0005nonce\u0018\u0006 \u0001(\f\u0012\u0011\n\tgas_price\u0018\u0007 \u0001(\f\u0012\u0011\n\tgas_limit\u0018\b \u0001(\fB\u000b\n\tstake_msg\"i\n\u000bDescription\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u0010\n\bidentity\u0018\u0002 \u0001(\t\u0012\u000f\n\u0007website\u0018\u0003 \u0001(\t\u0012\u0018\n\u0010security_contact\u0018\u0004 \u0001(\t\u0012\u000f\n\u0007details\u0018\u0005 \u0001(\t\"+\n\u0007Decimal\u0012\r\n\u0005value\u0018\u0001 \u0001(\f\u0012\u0011\n\tprecision\u0018\u0002 \u0001(\f\"\u009a\u0001\n\u000eCommissionRate\u0012'\n\u0004rate\u0018\u0001 \u0001(\u000b2\u0019.TW.Harmony.Proto.Decimal\u0012+\n\bmax_rate\u0018\u0002 \u0001(\u000b2\u0019.TW.Harmony.Proto.Decimal\u00122\n\u000fmax_change_rate\u0018\u0003 \u0001(\u000b2\u0019.TW.Harmony.Proto.Decimal\"\u009e\u0002\n\u0018DirectiveCreateValidator\u0012\u0019\n\u0011validator_address\u0018\u0001 \u0001(\t\u00122\n\u000bdescription\u0018\u0002 \u0001(\u000b2\u001d.TW.Harmony.Proto.Description\u0012:\n\u0010commission_rates\u0018\u0003 \u0001(\u000b2 .TW.Harmony.Proto.CommissionRate\u0012\u001b\n\u0013min_self_delegation\u0018\u0004 \u0001(\f\u0012\u001c\n\u0014max_total_delegation\u0018\u0005 \u0001(\f\u0012\u0015\n\rslot_pub_keys\u0018\u0006 \u0003(\f\u0012\u0015\n\rslot_key_sigs\u0018\u0007 \u0003(\f\u0012\u000e\n\u0006amount\u0018\b \u0001(\f\"¸\u0002\n\u0016DirectiveEditValidator\u0012\u0019\n\u0011validator_address\u0018\u0001 \u0001(\t\u00122\n\u000bdescription\u0018\u0002 \u0001(\u000b2\u001d.TW.Harmony.Proto.Description\u00122\n\u000fcommission_rate\u0018\u0003 \u0001(\u000b2\u0019.TW.Harmony.Proto.Decimal\u0012\u001b\n\u0013min_self_delegation\u0018\u0004 \u0001(\f\u0012\u001c\n\u0014max_total_delegation\u0018\u0005 \u0001(\f\u0012\u001a\n\u0012slot_key_to_remove\u0018\u0006 \u0001(\f\u0012\u0017\n\u000fslot_key_to_add\u0018\u0007 \u0001(\f\u0012\u001b\n\u0013slot_key_to_add_sig\u0018\b \u0001(\f\u0012\u000e\n\u0006active\u0018\t \u0001(\f\"Y\n\u0011DirectiveDelegate\u0012\u0019\n\u0011delegator_address\u0018\u0001 \u0001(\t\u0012\u0019\n\u0011validator_address\u0018\u0002 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0003 \u0001(\f\"[\n\u0013DirectiveUndelegate\u0012\u0019\n\u0011delegator_address\u0018\u0001 \u0001(\t\u0012\u0019\n\u0011validator_address\u0018\u0002 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0003 \u0001(\f\"4\n\u0017DirectiveCollectRewards\u0012\u0019\n\u0011delegator_address\u0018\u0001 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Harmony_Proto_CommissionRate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_CommissionRate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_Decimal_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_Decimal_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_Description_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_Description_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_DirectiveCollectRewards_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_DirectiveCollectRewards_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_DirectiveCreateValidator_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_DirectiveCreateValidator_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_DirectiveDelegate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_DirectiveDelegate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_DirectiveEditValidator_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_DirectiveEditValidator_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_DirectiveUndelegate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_DirectiveUndelegate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_StakingMessage_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_StakingMessage_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Harmony_Proto_TransactionMessage_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Harmony_Proto_TransactionMessage_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Harmony$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Harmony$SigningInput$MessageOneofCase;
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase;

        static {
            int[] iArr = new int[StakingMessage.StakeMsgCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase = iArr;
            try {
                iArr[StakingMessage.StakeMsgCase.CREATE_VALIDATOR_MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase[StakingMessage.StakeMsgCase.EDIT_VALIDATOR_MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase[StakingMessage.StakeMsgCase.DELEGATE_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase[StakingMessage.StakeMsgCase.UNDELEGATE_MESSAGE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase[StakingMessage.StakeMsgCase.COLLECT_REWARDS.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase[StakingMessage.StakeMsgCase.STAKEMSG_NOT_SET.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[SigningInput.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Harmony$SigningInput$MessageOneofCase = iArr2;
            try {
                iArr2[SigningInput.MessageOneofCase.TRANSACTION_MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Harmony$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.STAKING_MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Harmony$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class CommissionRate extends GeneratedMessageV3 implements CommissionRateOrBuilder {
        public static final int MAX_CHANGE_RATE_FIELD_NUMBER = 3;
        public static final int MAX_RATE_FIELD_NUMBER = 2;
        public static final int RATE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private Decimal maxChangeRate_;
        private Decimal maxRate_;
        private byte memoizedIsInitialized;
        private Decimal rate_;
        private static final CommissionRate DEFAULT_INSTANCE = new CommissionRate();
        private static final t0<CommissionRate> PARSER = new c<CommissionRate>() { // from class: wallet.core.jni.proto.Harmony.CommissionRate.1
            @Override // com.google.protobuf.t0
            public CommissionRate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new CommissionRate(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements CommissionRateOrBuilder {
            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> maxChangeRateBuilder_;
            private Decimal maxChangeRate_;
            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> maxRateBuilder_;
            private Decimal maxRate_;
            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> rateBuilder_;
            private Decimal rate_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_CommissionRate_descriptor;
            }

            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> getMaxChangeRateFieldBuilder() {
                if (this.maxChangeRateBuilder_ == null) {
                    this.maxChangeRateBuilder_ = new a1<>(getMaxChangeRate(), getParentForChildren(), isClean());
                    this.maxChangeRate_ = null;
                }
                return this.maxChangeRateBuilder_;
            }

            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> getMaxRateFieldBuilder() {
                if (this.maxRateBuilder_ == null) {
                    this.maxRateBuilder_ = new a1<>(getMaxRate(), getParentForChildren(), isClean());
                    this.maxRate_ = null;
                }
                return this.maxRateBuilder_;
            }

            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> getRateFieldBuilder() {
                if (this.rateBuilder_ == null) {
                    this.rateBuilder_ = new a1<>(getRate(), getParentForChildren(), isClean());
                    this.rate_ = null;
                }
                return this.rateBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearMaxChangeRate() {
                if (this.maxChangeRateBuilder_ == null) {
                    this.maxChangeRate_ = null;
                    onChanged();
                } else {
                    this.maxChangeRate_ = null;
                    this.maxChangeRateBuilder_ = null;
                }
                return this;
            }

            public Builder clearMaxRate() {
                if (this.maxRateBuilder_ == null) {
                    this.maxRate_ = null;
                    onChanged();
                } else {
                    this.maxRate_ = null;
                    this.maxRateBuilder_ = null;
                }
                return this;
            }

            public Builder clearRate() {
                if (this.rateBuilder_ == null) {
                    this.rate_ = null;
                    onChanged();
                } else {
                    this.rate_ = null;
                    this.rateBuilder_ = null;
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_CommissionRate_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public Decimal getMaxChangeRate() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxChangeRateBuilder_;
                if (a1Var == null) {
                    Decimal decimal = this.maxChangeRate_;
                    return decimal == null ? Decimal.getDefaultInstance() : decimal;
                }
                return a1Var.f();
            }

            public Decimal.Builder getMaxChangeRateBuilder() {
                onChanged();
                return getMaxChangeRateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public DecimalOrBuilder getMaxChangeRateOrBuilder() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxChangeRateBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Decimal decimal = this.maxChangeRate_;
                return decimal == null ? Decimal.getDefaultInstance() : decimal;
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public Decimal getMaxRate() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxRateBuilder_;
                if (a1Var == null) {
                    Decimal decimal = this.maxRate_;
                    return decimal == null ? Decimal.getDefaultInstance() : decimal;
                }
                return a1Var.f();
            }

            public Decimal.Builder getMaxRateBuilder() {
                onChanged();
                return getMaxRateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public DecimalOrBuilder getMaxRateOrBuilder() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxRateBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Decimal decimal = this.maxRate_;
                return decimal == null ? Decimal.getDefaultInstance() : decimal;
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public Decimal getRate() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.rateBuilder_;
                if (a1Var == null) {
                    Decimal decimal = this.rate_;
                    return decimal == null ? Decimal.getDefaultInstance() : decimal;
                }
                return a1Var.f();
            }

            public Decimal.Builder getRateBuilder() {
                onChanged();
                return getRateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public DecimalOrBuilder getRateOrBuilder() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.rateBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Decimal decimal = this.rate_;
                return decimal == null ? Decimal.getDefaultInstance() : decimal;
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public boolean hasMaxChangeRate() {
                return (this.maxChangeRateBuilder_ == null && this.maxChangeRate_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public boolean hasMaxRate() {
                return (this.maxRateBuilder_ == null && this.maxRate_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
            public boolean hasRate() {
                return (this.rateBuilder_ == null && this.rate_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_CommissionRate_fieldAccessorTable.d(CommissionRate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeMaxChangeRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxChangeRateBuilder_;
                if (a1Var == null) {
                    Decimal decimal2 = this.maxChangeRate_;
                    if (decimal2 != null) {
                        this.maxChangeRate_ = Decimal.newBuilder(decimal2).mergeFrom(decimal).buildPartial();
                    } else {
                        this.maxChangeRate_ = decimal;
                    }
                    onChanged();
                } else {
                    a1Var.h(decimal);
                }
                return this;
            }

            public Builder mergeMaxRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxRateBuilder_;
                if (a1Var == null) {
                    Decimal decimal2 = this.maxRate_;
                    if (decimal2 != null) {
                        this.maxRate_ = Decimal.newBuilder(decimal2).mergeFrom(decimal).buildPartial();
                    } else {
                        this.maxRate_ = decimal;
                    }
                    onChanged();
                } else {
                    a1Var.h(decimal);
                }
                return this;
            }

            public Builder mergeRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.rateBuilder_;
                if (a1Var == null) {
                    Decimal decimal2 = this.rate_;
                    if (decimal2 != null) {
                        this.rate_ = Decimal.newBuilder(decimal2).mergeFrom(decimal).buildPartial();
                    } else {
                        this.rate_ = decimal;
                    }
                    onChanged();
                } else {
                    a1Var.h(decimal);
                }
                return this;
            }

            public Builder setMaxChangeRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxChangeRateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(decimal);
                    this.maxChangeRate_ = decimal;
                    onChanged();
                } else {
                    a1Var.j(decimal);
                }
                return this;
            }

            public Builder setMaxRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxRateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(decimal);
                    this.maxRate_ = decimal;
                    onChanged();
                } else {
                    a1Var.j(decimal);
                }
                return this;
            }

            public Builder setRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.rateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(decimal);
                    this.rate_ = decimal;
                    onChanged();
                } else {
                    a1Var.j(decimal);
                }
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CommissionRate build() {
                CommissionRate buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CommissionRate buildPartial() {
                CommissionRate commissionRate = new CommissionRate(this, (AnonymousClass1) null);
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.rateBuilder_;
                if (a1Var == null) {
                    commissionRate.rate_ = this.rate_;
                } else {
                    commissionRate.rate_ = a1Var.b();
                }
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var2 = this.maxRateBuilder_;
                if (a1Var2 == null) {
                    commissionRate.maxRate_ = this.maxRate_;
                } else {
                    commissionRate.maxRate_ = a1Var2.b();
                }
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var3 = this.maxChangeRateBuilder_;
                if (a1Var3 == null) {
                    commissionRate.maxChangeRate_ = this.maxChangeRate_;
                } else {
                    commissionRate.maxChangeRate_ = a1Var3.b();
                }
                onBuilt();
                return commissionRate;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public CommissionRate getDefaultInstanceForType() {
                return CommissionRate.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.rateBuilder_ == null) {
                    this.rate_ = null;
                } else {
                    this.rate_ = null;
                    this.rateBuilder_ = null;
                }
                if (this.maxRateBuilder_ == null) {
                    this.maxRate_ = null;
                } else {
                    this.maxRate_ = null;
                    this.maxRateBuilder_ = null;
                }
                if (this.maxChangeRateBuilder_ == null) {
                    this.maxChangeRate_ = null;
                } else {
                    this.maxChangeRate_ = null;
                    this.maxChangeRateBuilder_ = null;
                }
                return this;
            }

            public Builder setMaxChangeRate(Decimal.Builder builder) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxChangeRateBuilder_;
                if (a1Var == null) {
                    this.maxChangeRate_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setMaxRate(Decimal.Builder builder) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.maxRateBuilder_;
                if (a1Var == null) {
                    this.maxRate_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setRate(Decimal.Builder builder) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.rateBuilder_;
                if (a1Var == null) {
                    this.rate_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof CommissionRate) {
                    return mergeFrom((CommissionRate) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(CommissionRate commissionRate) {
                if (commissionRate == CommissionRate.getDefaultInstance()) {
                    return this;
                }
                if (commissionRate.hasRate()) {
                    mergeRate(commissionRate.getRate());
                }
                if (commissionRate.hasMaxRate()) {
                    mergeMaxRate(commissionRate.getMaxRate());
                }
                if (commissionRate.hasMaxChangeRate()) {
                    mergeMaxChangeRate(commissionRate.getMaxChangeRate());
                }
                mergeUnknownFields(commissionRate.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.CommissionRate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.CommissionRate.access$9800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$CommissionRate r3 = (wallet.core.jni.proto.Harmony.CommissionRate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$CommissionRate r4 = (wallet.core.jni.proto.Harmony.CommissionRate) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.CommissionRate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$CommissionRate$Builder");
            }
        }

        public /* synthetic */ CommissionRate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static CommissionRate getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_CommissionRate_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static CommissionRate parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (CommissionRate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static CommissionRate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<CommissionRate> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof CommissionRate)) {
                return super.equals(obj);
            }
            CommissionRate commissionRate = (CommissionRate) obj;
            if (hasRate() != commissionRate.hasRate()) {
                return false;
            }
            if ((!hasRate() || getRate().equals(commissionRate.getRate())) && hasMaxRate() == commissionRate.hasMaxRate()) {
                if ((!hasMaxRate() || getMaxRate().equals(commissionRate.getMaxRate())) && hasMaxChangeRate() == commissionRate.hasMaxChangeRate()) {
                    return (!hasMaxChangeRate() || getMaxChangeRate().equals(commissionRate.getMaxChangeRate())) && this.unknownFields.equals(commissionRate.unknownFields);
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public Decimal getMaxChangeRate() {
            Decimal decimal = this.maxChangeRate_;
            return decimal == null ? Decimal.getDefaultInstance() : decimal;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public DecimalOrBuilder getMaxChangeRateOrBuilder() {
            return getMaxChangeRate();
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public Decimal getMaxRate() {
            Decimal decimal = this.maxRate_;
            return decimal == null ? Decimal.getDefaultInstance() : decimal;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public DecimalOrBuilder getMaxRateOrBuilder() {
            return getMaxRate();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<CommissionRate> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public Decimal getRate() {
            Decimal decimal = this.rate_;
            return decimal == null ? Decimal.getDefaultInstance() : decimal;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public DecimalOrBuilder getRateOrBuilder() {
            return getRate();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.rate_ != null ? 0 + CodedOutputStream.G(1, getRate()) : 0;
            if (this.maxRate_ != null) {
                G += CodedOutputStream.G(2, getMaxRate());
            }
            if (this.maxChangeRate_ != null) {
                G += CodedOutputStream.G(3, getMaxChangeRate());
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public boolean hasMaxChangeRate() {
            return this.maxChangeRate_ != null;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public boolean hasMaxRate() {
            return this.maxRate_ != null;
        }

        @Override // wallet.core.jni.proto.Harmony.CommissionRateOrBuilder
        public boolean hasRate() {
            return this.rate_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasRate()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getRate().hashCode();
            }
            if (hasMaxRate()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getMaxRate().hashCode();
            }
            if (hasMaxChangeRate()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getMaxChangeRate().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_CommissionRate_fieldAccessorTable.d(CommissionRate.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new CommissionRate();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.rate_ != null) {
                codedOutputStream.K0(1, getRate());
            }
            if (this.maxRate_ != null) {
                codedOutputStream.K0(2, getMaxRate());
            }
            if (this.maxChangeRate_ != null) {
                codedOutputStream.K0(3, getMaxChangeRate());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ CommissionRate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(CommissionRate commissionRate) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(commissionRate);
        }

        public static CommissionRate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private CommissionRate(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static CommissionRate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (CommissionRate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static CommissionRate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public CommissionRate getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static CommissionRate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private CommissionRate() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static CommissionRate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static CommissionRate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private CommissionRate(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Decimal.Builder builder;
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                Decimal decimal = this.rate_;
                                builder = decimal != null ? decimal.toBuilder() : null;
                                Decimal decimal2 = (Decimal) jVar.z(Decimal.parser(), rVar);
                                this.rate_ = decimal2;
                                if (builder != null) {
                                    builder.mergeFrom(decimal2);
                                    this.rate_ = builder.buildPartial();
                                }
                            } else if (J == 18) {
                                Decimal decimal3 = this.maxRate_;
                                builder = decimal3 != null ? decimal3.toBuilder() : null;
                                Decimal decimal4 = (Decimal) jVar.z(Decimal.parser(), rVar);
                                this.maxRate_ = decimal4;
                                if (builder != null) {
                                    builder.mergeFrom(decimal4);
                                    this.maxRate_ = builder.buildPartial();
                                }
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                Decimal decimal5 = this.maxChangeRate_;
                                builder = decimal5 != null ? decimal5.toBuilder() : null;
                                Decimal decimal6 = (Decimal) jVar.z(Decimal.parser(), rVar);
                                this.maxChangeRate_ = decimal6;
                                if (builder != null) {
                                    builder.mergeFrom(decimal6);
                                    this.maxChangeRate_ = builder.buildPartial();
                                }
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static CommissionRate parseFrom(InputStream inputStream) throws IOException {
            return (CommissionRate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static CommissionRate parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (CommissionRate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static CommissionRate parseFrom(j jVar) throws IOException {
            return (CommissionRate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static CommissionRate parseFrom(j jVar, r rVar) throws IOException {
            return (CommissionRate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface CommissionRateOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        Decimal getMaxChangeRate();

        DecimalOrBuilder getMaxChangeRateOrBuilder();

        Decimal getMaxRate();

        DecimalOrBuilder getMaxRateOrBuilder();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Decimal getRate();

        DecimalOrBuilder getRateOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasMaxChangeRate();

        boolean hasMaxRate();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasRate();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Decimal extends GeneratedMessageV3 implements DecimalOrBuilder {
        private static final Decimal DEFAULT_INSTANCE = new Decimal();
        private static final t0<Decimal> PARSER = new c<Decimal>() { // from class: wallet.core.jni.proto.Harmony.Decimal.1
            @Override // com.google.protobuf.t0
            public Decimal parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Decimal(jVar, rVar, null);
            }
        };
        public static final int PRECISION_FIELD_NUMBER = 2;
        public static final int VALUE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString precision_;
        private ByteString value_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DecimalOrBuilder {
            private ByteString precision_;
            private ByteString value_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_Decimal_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPrecision() {
                this.precision_ = Decimal.getDefaultInstance().getPrecision();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = Decimal.getDefaultInstance().getValue();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_Decimal_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.DecimalOrBuilder
            public ByteString getPrecision() {
                return this.precision_;
            }

            @Override // wallet.core.jni.proto.Harmony.DecimalOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_Decimal_fieldAccessorTable.d(Decimal.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setPrecision(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.precision_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.value_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.value_ = byteString;
                this.precision_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Decimal build() {
                Decimal buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Decimal buildPartial() {
                Decimal decimal = new Decimal(this, (AnonymousClass1) null);
                decimal.value_ = this.value_;
                decimal.precision_ = this.precision_;
                onBuilt();
                return decimal;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Decimal getDefaultInstanceForType() {
                return Decimal.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.value_ = byteString;
                this.precision_ = byteString;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.value_ = byteString;
                this.precision_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Decimal) {
                    return mergeFrom((Decimal) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Decimal decimal) {
                if (decimal == Decimal.getDefaultInstance()) {
                    return this;
                }
                ByteString value = decimal.getValue();
                ByteString byteString = ByteString.EMPTY;
                if (value != byteString) {
                    setValue(decimal.getValue());
                }
                if (decimal.getPrecision() != byteString) {
                    setPrecision(decimal.getPrecision());
                }
                mergeUnknownFields(decimal.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.Decimal.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.Decimal.access$8600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$Decimal r3 = (wallet.core.jni.proto.Harmony.Decimal) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$Decimal r4 = (wallet.core.jni.proto.Harmony.Decimal) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.Decimal.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$Decimal$Builder");
            }
        }

        public /* synthetic */ Decimal(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Decimal getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_Decimal_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Decimal parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Decimal) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Decimal parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Decimal> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Decimal)) {
                return super.equals(obj);
            }
            Decimal decimal = (Decimal) obj;
            return getValue().equals(decimal.getValue()) && getPrecision().equals(decimal.getPrecision()) && this.unknownFields.equals(decimal.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Decimal> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Harmony.DecimalOrBuilder
        public ByteString getPrecision() {
            return this.precision_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.value_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.value_);
            if (!this.precision_.isEmpty()) {
                h += CodedOutputStream.h(2, this.precision_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.DecimalOrBuilder
        public ByteString getValue() {
            return this.value_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValue().hashCode()) * 37) + 2) * 53) + getPrecision().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_Decimal_fieldAccessorTable.d(Decimal.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Decimal();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.value_.isEmpty()) {
                codedOutputStream.q0(1, this.value_);
            }
            if (!this.precision_.isEmpty()) {
                codedOutputStream.q0(2, this.precision_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Decimal(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Decimal decimal) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(decimal);
        }

        public static Decimal parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Decimal(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Decimal parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Decimal) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Decimal parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Decimal getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Decimal parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Decimal() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.value_ = byteString;
            this.precision_ = byteString;
        }

        public static Decimal parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Decimal parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Decimal parseFrom(InputStream inputStream) throws IOException {
            return (Decimal) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Decimal(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.value_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.precision_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Decimal parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Decimal) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Decimal parseFrom(j jVar) throws IOException {
            return (Decimal) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Decimal parseFrom(j jVar, r rVar) throws IOException {
            return (Decimal) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DecimalOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrecision();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        ByteString getValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Description extends GeneratedMessageV3 implements DescriptionOrBuilder {
        public static final int DETAILS_FIELD_NUMBER = 5;
        public static final int IDENTITY_FIELD_NUMBER = 2;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int SECURITY_CONTACT_FIELD_NUMBER = 4;
        public static final int WEBSITE_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private volatile Object details_;
        private volatile Object identity_;
        private byte memoizedIsInitialized;
        private volatile Object name_;
        private volatile Object securityContact_;
        private volatile Object website_;
        private static final Description DEFAULT_INSTANCE = new Description();
        private static final t0<Description> PARSER = new c<Description>() { // from class: wallet.core.jni.proto.Harmony.Description.1
            @Override // com.google.protobuf.t0
            public Description parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Description(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DescriptionOrBuilder {
            private Object details_;
            private Object identity_;
            private Object name_;
            private Object securityContact_;
            private Object website_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_Description_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearDetails() {
                this.details_ = Description.getDefaultInstance().getDetails();
                onChanged();
                return this;
            }

            public Builder clearIdentity() {
                this.identity_ = Description.getDefaultInstance().getIdentity();
                onChanged();
                return this;
            }

            public Builder clearName() {
                this.name_ = Description.getDefaultInstance().getName();
                onChanged();
                return this;
            }

            public Builder clearSecurityContact() {
                this.securityContact_ = Description.getDefaultInstance().getSecurityContact();
                onChanged();
                return this;
            }

            public Builder clearWebsite() {
                this.website_ = Description.getDefaultInstance().getWebsite();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_Description_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public String getDetails() {
                Object obj = this.details_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.details_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public ByteString getDetailsBytes() {
                Object obj = this.details_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.details_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public String getIdentity() {
                Object obj = this.identity_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.identity_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public ByteString getIdentityBytes() {
                Object obj = this.identity_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.identity_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public String getName() {
                Object obj = this.name_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.name_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public ByteString getNameBytes() {
                Object obj = this.name_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.name_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public String getSecurityContact() {
                Object obj = this.securityContact_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.securityContact_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public ByteString getSecurityContactBytes() {
                Object obj = this.securityContact_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.securityContact_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public String getWebsite() {
                Object obj = this.website_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.website_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
            public ByteString getWebsiteBytes() {
                Object obj = this.website_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.website_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_Description_fieldAccessorTable.d(Description.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setDetails(String str) {
                Objects.requireNonNull(str);
                this.details_ = str;
                onChanged();
                return this;
            }

            public Builder setDetailsBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.details_ = byteString;
                onChanged();
                return this;
            }

            public Builder setIdentity(String str) {
                Objects.requireNonNull(str);
                this.identity_ = str;
                onChanged();
                return this;
            }

            public Builder setIdentityBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.identity_ = byteString;
                onChanged();
                return this;
            }

            public Builder setName(String str) {
                Objects.requireNonNull(str);
                this.name_ = str;
                onChanged();
                return this;
            }

            public Builder setNameBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.name_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSecurityContact(String str) {
                Objects.requireNonNull(str);
                this.securityContact_ = str;
                onChanged();
                return this;
            }

            public Builder setSecurityContactBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.securityContact_ = byteString;
                onChanged();
                return this;
            }

            public Builder setWebsite(String str) {
                Objects.requireNonNull(str);
                this.website_ = str;
                onChanged();
                return this;
            }

            public Builder setWebsiteBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.website_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.name_ = "";
                this.identity_ = "";
                this.website_ = "";
                this.securityContact_ = "";
                this.details_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Description build() {
                Description buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Description buildPartial() {
                Description description = new Description(this, (AnonymousClass1) null);
                description.name_ = this.name_;
                description.identity_ = this.identity_;
                description.website_ = this.website_;
                description.securityContact_ = this.securityContact_;
                description.details_ = this.details_;
                onBuilt();
                return description;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Description getDefaultInstanceForType() {
                return Description.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.name_ = "";
                this.identity_ = "";
                this.website_ = "";
                this.securityContact_ = "";
                this.details_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Description) {
                    return mergeFrom((Description) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.name_ = "";
                this.identity_ = "";
                this.website_ = "";
                this.securityContact_ = "";
                this.details_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(Description description) {
                if (description == Description.getDefaultInstance()) {
                    return this;
                }
                if (!description.getName().isEmpty()) {
                    this.name_ = description.name_;
                    onChanged();
                }
                if (!description.getIdentity().isEmpty()) {
                    this.identity_ = description.identity_;
                    onChanged();
                }
                if (!description.getWebsite().isEmpty()) {
                    this.website_ = description.website_;
                    onChanged();
                }
                if (!description.getSecurityContact().isEmpty()) {
                    this.securityContact_ = description.securityContact_;
                    onChanged();
                }
                if (!description.getDetails().isEmpty()) {
                    this.details_ = description.details_;
                    onChanged();
                }
                mergeUnknownFields(description.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.Description.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.Description.access$7000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$Description r3 = (wallet.core.jni.proto.Harmony.Description) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$Description r4 = (wallet.core.jni.proto.Harmony.Description) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.Description.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$Description$Builder");
            }
        }

        public /* synthetic */ Description(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Description getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_Description_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Description parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Description) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Description parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Description> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Description)) {
                return super.equals(obj);
            }
            Description description = (Description) obj;
            return getName().equals(description.getName()) && getIdentity().equals(description.getIdentity()) && getWebsite().equals(description.getWebsite()) && getSecurityContact().equals(description.getSecurityContact()) && getDetails().equals(description.getDetails()) && this.unknownFields.equals(description.unknownFields);
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public String getDetails() {
            Object obj = this.details_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.details_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public ByteString getDetailsBytes() {
            Object obj = this.details_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.details_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public String getIdentity() {
            Object obj = this.identity_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.identity_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public ByteString getIdentityBytes() {
            Object obj = this.identity_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.identity_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public String getName() {
            Object obj = this.name_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.name_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public ByteString getNameBytes() {
            Object obj = this.name_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.name_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Description> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public String getSecurityContact() {
            Object obj = this.securityContact_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.securityContact_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public ByteString getSecurityContactBytes() {
            Object obj = this.securityContact_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.securityContact_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getNameBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.name_);
            if (!getIdentityBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.identity_);
            }
            if (!getWebsiteBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.website_);
            }
            if (!getSecurityContactBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(4, this.securityContact_);
            }
            if (!getDetailsBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(5, this.details_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public String getWebsite() {
            Object obj = this.website_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.website_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DescriptionOrBuilder
        public ByteString getWebsiteBytes() {
            Object obj = this.website_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.website_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getName().hashCode()) * 37) + 2) * 53) + getIdentity().hashCode()) * 37) + 3) * 53) + getWebsite().hashCode()) * 37) + 4) * 53) + getSecurityContact().hashCode()) * 37) + 5) * 53) + getDetails().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_Description_fieldAccessorTable.d(Description.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Description();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getNameBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
            }
            if (!getIdentityBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.identity_);
            }
            if (!getWebsiteBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.website_);
            }
            if (!getSecurityContactBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.securityContact_);
            }
            if (!getDetailsBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.details_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Description(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Description description) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(description);
        }

        public static Description parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Description(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Description parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Description) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Description parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Description getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Description parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Description() {
            this.memoizedIsInitialized = (byte) -1;
            this.name_ = "";
            this.identity_ = "";
            this.website_ = "";
            this.securityContact_ = "";
            this.details_ = "";
        }

        public static Description parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Description parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Description parseFrom(InputStream inputStream) throws IOException {
            return (Description) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static Description parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Description) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Description parseFrom(j jVar) throws IOException {
            return (Description) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private Description(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.name_ = jVar.I();
                                } else if (J == 18) {
                                    this.identity_ = jVar.I();
                                } else if (J == 26) {
                                    this.website_ = jVar.I();
                                } else if (J == 34) {
                                    this.securityContact_ = jVar.I();
                                } else if (J != 42) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.details_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Description parseFrom(j jVar, r rVar) throws IOException {
            return (Description) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DescriptionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        String getDetails();

        ByteString getDetailsBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        String getIdentity();

        ByteString getIdentityBytes();

        /* synthetic */ String getInitializationErrorString();

        String getName();

        ByteString getNameBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSecurityContact();

        ByteString getSecurityContactBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getWebsite();

        ByteString getWebsiteBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DirectiveCollectRewards extends GeneratedMessageV3 implements DirectiveCollectRewardsOrBuilder {
        public static final int DELEGATOR_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object delegatorAddress_;
        private byte memoizedIsInitialized;
        private static final DirectiveCollectRewards DEFAULT_INSTANCE = new DirectiveCollectRewards();
        private static final t0<DirectiveCollectRewards> PARSER = new c<DirectiveCollectRewards>() { // from class: wallet.core.jni.proto.Harmony.DirectiveCollectRewards.1
            @Override // com.google.protobuf.t0
            public DirectiveCollectRewards parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DirectiveCollectRewards(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DirectiveCollectRewardsOrBuilder {
            private Object delegatorAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveCollectRewards_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearDelegatorAddress() {
                this.delegatorAddress_ = DirectiveCollectRewards.getDefaultInstance().getDelegatorAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCollectRewardsOrBuilder
            public String getDelegatorAddress() {
                Object obj = this.delegatorAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.delegatorAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCollectRewardsOrBuilder
            public ByteString getDelegatorAddressBytes() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveCollectRewards_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveCollectRewards_fieldAccessorTable.d(DirectiveCollectRewards.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setDelegatorAddress(String str) {
                Objects.requireNonNull(str);
                this.delegatorAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setDelegatorAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.delegatorAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.delegatorAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveCollectRewards build() {
                DirectiveCollectRewards buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveCollectRewards buildPartial() {
                DirectiveCollectRewards directiveCollectRewards = new DirectiveCollectRewards(this, (AnonymousClass1) null);
                directiveCollectRewards.delegatorAddress_ = this.delegatorAddress_;
                onBuilt();
                return directiveCollectRewards;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DirectiveCollectRewards getDefaultInstanceForType() {
                return DirectiveCollectRewards.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.delegatorAddress_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.delegatorAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DirectiveCollectRewards) {
                    return mergeFrom((DirectiveCollectRewards) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DirectiveCollectRewards directiveCollectRewards) {
                if (directiveCollectRewards == DirectiveCollectRewards.getDefaultInstance()) {
                    return this;
                }
                if (!directiveCollectRewards.getDelegatorAddress().isEmpty()) {
                    this.delegatorAddress_ = directiveCollectRewards.delegatorAddress_;
                    onChanged();
                }
                mergeUnknownFields(directiveCollectRewards.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.DirectiveCollectRewards.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.DirectiveCollectRewards.access$17300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$DirectiveCollectRewards r3 = (wallet.core.jni.proto.Harmony.DirectiveCollectRewards) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$DirectiveCollectRewards r4 = (wallet.core.jni.proto.Harmony.DirectiveCollectRewards) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.DirectiveCollectRewards.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$DirectiveCollectRewards$Builder");
            }
        }

        public /* synthetic */ DirectiveCollectRewards(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DirectiveCollectRewards getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveCollectRewards_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DirectiveCollectRewards parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DirectiveCollectRewards) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DirectiveCollectRewards parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DirectiveCollectRewards> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DirectiveCollectRewards)) {
                return super.equals(obj);
            }
            DirectiveCollectRewards directiveCollectRewards = (DirectiveCollectRewards) obj;
            return getDelegatorAddress().equals(directiveCollectRewards.getDelegatorAddress()) && this.unknownFields.equals(directiveCollectRewards.unknownFields);
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCollectRewardsOrBuilder
        public String getDelegatorAddress() {
            Object obj = this.delegatorAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.delegatorAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCollectRewardsOrBuilder
        public ByteString getDelegatorAddressBytes() {
            Object obj = this.delegatorAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.delegatorAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DirectiveCollectRewards> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getDelegatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegatorAddress_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegatorAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveCollectRewards_fieldAccessorTable.d(DirectiveCollectRewards.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DirectiveCollectRewards();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDelegatorAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegatorAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DirectiveCollectRewards(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DirectiveCollectRewards directiveCollectRewards) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(directiveCollectRewards);
        }

        public static DirectiveCollectRewards parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DirectiveCollectRewards(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DirectiveCollectRewards parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveCollectRewards) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveCollectRewards parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DirectiveCollectRewards getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DirectiveCollectRewards parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DirectiveCollectRewards() {
            this.memoizedIsInitialized = (byte) -1;
            this.delegatorAddress_ = "";
        }

        public static DirectiveCollectRewards parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DirectiveCollectRewards parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DirectiveCollectRewards parseFrom(InputStream inputStream) throws IOException {
            return (DirectiveCollectRewards) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private DirectiveCollectRewards(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.delegatorAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DirectiveCollectRewards parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveCollectRewards) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveCollectRewards parseFrom(j jVar) throws IOException {
            return (DirectiveCollectRewards) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DirectiveCollectRewards parseFrom(j jVar, r rVar) throws IOException {
            return (DirectiveCollectRewards) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DirectiveCollectRewardsOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        String getDelegatorAddress();

        ByteString getDelegatorAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DirectiveCreateValidator extends GeneratedMessageV3 implements DirectiveCreateValidatorOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 8;
        public static final int COMMISSION_RATES_FIELD_NUMBER = 3;
        public static final int DESCRIPTION_FIELD_NUMBER = 2;
        public static final int MAX_TOTAL_DELEGATION_FIELD_NUMBER = 5;
        public static final int MIN_SELF_DELEGATION_FIELD_NUMBER = 4;
        public static final int SLOT_KEY_SIGS_FIELD_NUMBER = 7;
        public static final int SLOT_PUB_KEYS_FIELD_NUMBER = 6;
        public static final int VALIDATOR_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString amount_;
        private CommissionRate commissionRates_;
        private Description description_;
        private ByteString maxTotalDelegation_;
        private byte memoizedIsInitialized;
        private ByteString minSelfDelegation_;
        private List<ByteString> slotKeySigs_;
        private List<ByteString> slotPubKeys_;
        private volatile Object validatorAddress_;
        private static final DirectiveCreateValidator DEFAULT_INSTANCE = new DirectiveCreateValidator();
        private static final t0<DirectiveCreateValidator> PARSER = new c<DirectiveCreateValidator>() { // from class: wallet.core.jni.proto.Harmony.DirectiveCreateValidator.1
            @Override // com.google.protobuf.t0
            public DirectiveCreateValidator parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DirectiveCreateValidator(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DirectiveCreateValidatorOrBuilder {
            private ByteString amount_;
            private int bitField0_;
            private a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> commissionRatesBuilder_;
            private CommissionRate commissionRates_;
            private a1<Description, Description.Builder, DescriptionOrBuilder> descriptionBuilder_;
            private Description description_;
            private ByteString maxTotalDelegation_;
            private ByteString minSelfDelegation_;
            private List<ByteString> slotKeySigs_;
            private List<ByteString> slotPubKeys_;
            private Object validatorAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private void ensureSlotKeySigsIsMutable() {
                if ((this.bitField0_ & 2) == 0) {
                    this.slotKeySigs_ = new ArrayList(this.slotKeySigs_);
                    this.bitField0_ |= 2;
                }
            }

            private void ensureSlotPubKeysIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.slotPubKeys_ = new ArrayList(this.slotPubKeys_);
                    this.bitField0_ |= 1;
                }
            }

            private a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> getCommissionRatesFieldBuilder() {
                if (this.commissionRatesBuilder_ == null) {
                    this.commissionRatesBuilder_ = new a1<>(getCommissionRates(), getParentForChildren(), isClean());
                    this.commissionRates_ = null;
                }
                return this.commissionRatesBuilder_;
            }

            private a1<Description, Description.Builder, DescriptionOrBuilder> getDescriptionFieldBuilder() {
                if (this.descriptionBuilder_ == null) {
                    this.descriptionBuilder_ = new a1<>(getDescription(), getParentForChildren(), isClean());
                    this.description_ = null;
                }
                return this.descriptionBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveCreateValidator_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder addAllSlotKeySigs(Iterable<? extends ByteString> iterable) {
                ensureSlotKeySigsIsMutable();
                b.a.addAll((Iterable) iterable, (List) this.slotKeySigs_);
                onChanged();
                return this;
            }

            public Builder addAllSlotPubKeys(Iterable<? extends ByteString> iterable) {
                ensureSlotPubKeysIsMutable();
                b.a.addAll((Iterable) iterable, (List) this.slotPubKeys_);
                onChanged();
                return this;
            }

            public Builder addSlotKeySigs(ByteString byteString) {
                Objects.requireNonNull(byteString);
                ensureSlotKeySigsIsMutable();
                this.slotKeySigs_.add(byteString);
                onChanged();
                return this;
            }

            public Builder addSlotPubKeys(ByteString byteString) {
                Objects.requireNonNull(byteString);
                ensureSlotPubKeysIsMutable();
                this.slotPubKeys_.add(byteString);
                onChanged();
                return this;
            }

            public Builder clearAmount() {
                this.amount_ = DirectiveCreateValidator.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearCommissionRates() {
                if (this.commissionRatesBuilder_ == null) {
                    this.commissionRates_ = null;
                    onChanged();
                } else {
                    this.commissionRates_ = null;
                    this.commissionRatesBuilder_ = null;
                }
                return this;
            }

            public Builder clearDescription() {
                if (this.descriptionBuilder_ == null) {
                    this.description_ = null;
                    onChanged();
                } else {
                    this.description_ = null;
                    this.descriptionBuilder_ = null;
                }
                return this;
            }

            public Builder clearMaxTotalDelegation() {
                this.maxTotalDelegation_ = DirectiveCreateValidator.getDefaultInstance().getMaxTotalDelegation();
                onChanged();
                return this;
            }

            public Builder clearMinSelfDelegation() {
                this.minSelfDelegation_ = DirectiveCreateValidator.getDefaultInstance().getMinSelfDelegation();
                onChanged();
                return this;
            }

            public Builder clearSlotKeySigs() {
                this.slotKeySigs_ = Collections.emptyList();
                this.bitField0_ &= -3;
                onChanged();
                return this;
            }

            public Builder clearSlotPubKeys() {
                this.slotPubKeys_ = Collections.emptyList();
                this.bitField0_ &= -2;
                onChanged();
                return this;
            }

            public Builder clearValidatorAddress() {
                this.validatorAddress_ = DirectiveCreateValidator.getDefaultInstance().getValidatorAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public CommissionRate getCommissionRates() {
                a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> a1Var = this.commissionRatesBuilder_;
                if (a1Var == null) {
                    CommissionRate commissionRate = this.commissionRates_;
                    return commissionRate == null ? CommissionRate.getDefaultInstance() : commissionRate;
                }
                return a1Var.f();
            }

            public CommissionRate.Builder getCommissionRatesBuilder() {
                onChanged();
                return getCommissionRatesFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public CommissionRateOrBuilder getCommissionRatesOrBuilder() {
                a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> a1Var = this.commissionRatesBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                CommissionRate commissionRate = this.commissionRates_;
                return commissionRate == null ? CommissionRate.getDefaultInstance() : commissionRate;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public Description getDescription() {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    Description description = this.description_;
                    return description == null ? Description.getDefaultInstance() : description;
                }
                return a1Var.f();
            }

            public Description.Builder getDescriptionBuilder() {
                onChanged();
                return getDescriptionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public DescriptionOrBuilder getDescriptionOrBuilder() {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Description description = this.description_;
                return description == null ? Description.getDefaultInstance() : description;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveCreateValidator_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public ByteString getMaxTotalDelegation() {
                return this.maxTotalDelegation_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public ByteString getMinSelfDelegation() {
                return this.minSelfDelegation_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public ByteString getSlotKeySigs(int i) {
                return this.slotKeySigs_.get(i);
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public int getSlotKeySigsCount() {
                return this.slotKeySigs_.size();
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public List<ByteString> getSlotKeySigsList() {
                return (this.bitField0_ & 2) != 0 ? Collections.unmodifiableList(this.slotKeySigs_) : this.slotKeySigs_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public ByteString getSlotPubKeys(int i) {
                return this.slotPubKeys_.get(i);
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public int getSlotPubKeysCount() {
                return this.slotPubKeys_.size();
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public List<ByteString> getSlotPubKeysList() {
                return (this.bitField0_ & 1) != 0 ? Collections.unmodifiableList(this.slotPubKeys_) : this.slotPubKeys_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public String getValidatorAddress() {
                Object obj = this.validatorAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.validatorAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public ByteString getValidatorAddressBytes() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public boolean hasCommissionRates() {
                return (this.commissionRatesBuilder_ == null && this.commissionRates_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
            public boolean hasDescription() {
                return (this.descriptionBuilder_ == null && this.description_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveCreateValidator_fieldAccessorTable.d(DirectiveCreateValidator.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCommissionRates(CommissionRate commissionRate) {
                a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> a1Var = this.commissionRatesBuilder_;
                if (a1Var == null) {
                    CommissionRate commissionRate2 = this.commissionRates_;
                    if (commissionRate2 != null) {
                        this.commissionRates_ = CommissionRate.newBuilder(commissionRate2).mergeFrom(commissionRate).buildPartial();
                    } else {
                        this.commissionRates_ = commissionRate;
                    }
                    onChanged();
                } else {
                    a1Var.h(commissionRate);
                }
                return this;
            }

            public Builder mergeDescription(Description description) {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    Description description2 = this.description_;
                    if (description2 != null) {
                        this.description_ = Description.newBuilder(description2).mergeFrom(description).buildPartial();
                    } else {
                        this.description_ = description;
                    }
                    onChanged();
                } else {
                    a1Var.h(description);
                }
                return this;
            }

            public Builder setAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setCommissionRates(CommissionRate commissionRate) {
                a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> a1Var = this.commissionRatesBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(commissionRate);
                    this.commissionRates_ = commissionRate;
                    onChanged();
                } else {
                    a1Var.j(commissionRate);
                }
                return this;
            }

            public Builder setDescription(Description description) {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(description);
                    this.description_ = description;
                    onChanged();
                } else {
                    a1Var.j(description);
                }
                return this;
            }

            public Builder setMaxTotalDelegation(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.maxTotalDelegation_ = byteString;
                onChanged();
                return this;
            }

            public Builder setMinSelfDelegation(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.minSelfDelegation_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSlotKeySigs(int i, ByteString byteString) {
                Objects.requireNonNull(byteString);
                ensureSlotKeySigsIsMutable();
                this.slotKeySigs_.set(i, byteString);
                onChanged();
                return this;
            }

            public Builder setSlotPubKeys(int i, ByteString byteString) {
                Objects.requireNonNull(byteString);
                ensureSlotPubKeysIsMutable();
                this.slotPubKeys_.set(i, byteString);
                onChanged();
                return this;
            }

            public Builder setValidatorAddress(String str) {
                Objects.requireNonNull(str);
                this.validatorAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setValidatorAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.validatorAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.validatorAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.minSelfDelegation_ = byteString;
                this.maxTotalDelegation_ = byteString;
                this.slotPubKeys_ = Collections.emptyList();
                this.slotKeySigs_ = Collections.emptyList();
                this.amount_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveCreateValidator build() {
                DirectiveCreateValidator buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveCreateValidator buildPartial() {
                DirectiveCreateValidator directiveCreateValidator = new DirectiveCreateValidator(this, (AnonymousClass1) null);
                directiveCreateValidator.validatorAddress_ = this.validatorAddress_;
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    directiveCreateValidator.description_ = this.description_;
                } else {
                    directiveCreateValidator.description_ = a1Var.b();
                }
                a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> a1Var2 = this.commissionRatesBuilder_;
                if (a1Var2 == null) {
                    directiveCreateValidator.commissionRates_ = this.commissionRates_;
                } else {
                    directiveCreateValidator.commissionRates_ = a1Var2.b();
                }
                directiveCreateValidator.minSelfDelegation_ = this.minSelfDelegation_;
                directiveCreateValidator.maxTotalDelegation_ = this.maxTotalDelegation_;
                if ((this.bitField0_ & 1) != 0) {
                    this.slotPubKeys_ = Collections.unmodifiableList(this.slotPubKeys_);
                    this.bitField0_ &= -2;
                }
                directiveCreateValidator.slotPubKeys_ = this.slotPubKeys_;
                if ((this.bitField0_ & 2) != 0) {
                    this.slotKeySigs_ = Collections.unmodifiableList(this.slotKeySigs_);
                    this.bitField0_ &= -3;
                }
                directiveCreateValidator.slotKeySigs_ = this.slotKeySigs_;
                directiveCreateValidator.amount_ = this.amount_;
                onBuilt();
                return directiveCreateValidator;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DirectiveCreateValidator getDefaultInstanceForType() {
                return DirectiveCreateValidator.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.validatorAddress_ = "";
                if (this.descriptionBuilder_ == null) {
                    this.description_ = null;
                } else {
                    this.description_ = null;
                    this.descriptionBuilder_ = null;
                }
                if (this.commissionRatesBuilder_ == null) {
                    this.commissionRates_ = null;
                } else {
                    this.commissionRates_ = null;
                    this.commissionRatesBuilder_ = null;
                }
                ByteString byteString = ByteString.EMPTY;
                this.minSelfDelegation_ = byteString;
                this.maxTotalDelegation_ = byteString;
                this.slotPubKeys_ = Collections.emptyList();
                this.bitField0_ &= -2;
                this.slotKeySigs_ = Collections.emptyList();
                this.bitField0_ &= -3;
                this.amount_ = byteString;
                return this;
            }

            public Builder setCommissionRates(CommissionRate.Builder builder) {
                a1<CommissionRate, CommissionRate.Builder, CommissionRateOrBuilder> a1Var = this.commissionRatesBuilder_;
                if (a1Var == null) {
                    this.commissionRates_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setDescription(Description.Builder builder) {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    this.description_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DirectiveCreateValidator) {
                    return mergeFrom((DirectiveCreateValidator) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DirectiveCreateValidator directiveCreateValidator) {
                if (directiveCreateValidator == DirectiveCreateValidator.getDefaultInstance()) {
                    return this;
                }
                if (!directiveCreateValidator.getValidatorAddress().isEmpty()) {
                    this.validatorAddress_ = directiveCreateValidator.validatorAddress_;
                    onChanged();
                }
                if (directiveCreateValidator.hasDescription()) {
                    mergeDescription(directiveCreateValidator.getDescription());
                }
                if (directiveCreateValidator.hasCommissionRates()) {
                    mergeCommissionRates(directiveCreateValidator.getCommissionRates());
                }
                ByteString minSelfDelegation = directiveCreateValidator.getMinSelfDelegation();
                ByteString byteString = ByteString.EMPTY;
                if (minSelfDelegation != byteString) {
                    setMinSelfDelegation(directiveCreateValidator.getMinSelfDelegation());
                }
                if (directiveCreateValidator.getMaxTotalDelegation() != byteString) {
                    setMaxTotalDelegation(directiveCreateValidator.getMaxTotalDelegation());
                }
                if (!directiveCreateValidator.slotPubKeys_.isEmpty()) {
                    if (this.slotPubKeys_.isEmpty()) {
                        this.slotPubKeys_ = directiveCreateValidator.slotPubKeys_;
                        this.bitField0_ &= -2;
                    } else {
                        ensureSlotPubKeysIsMutable();
                        this.slotPubKeys_.addAll(directiveCreateValidator.slotPubKeys_);
                    }
                    onChanged();
                }
                if (!directiveCreateValidator.slotKeySigs_.isEmpty()) {
                    if (this.slotKeySigs_.isEmpty()) {
                        this.slotKeySigs_ = directiveCreateValidator.slotKeySigs_;
                        this.bitField0_ &= -3;
                    } else {
                        ensureSlotKeySigsIsMutable();
                        this.slotKeySigs_.addAll(directiveCreateValidator.slotKeySigs_);
                    }
                    onChanged();
                }
                if (directiveCreateValidator.getAmount() != byteString) {
                    setAmount(directiveCreateValidator.getAmount());
                }
                mergeUnknownFields(directiveCreateValidator.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.validatorAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.minSelfDelegation_ = byteString;
                this.maxTotalDelegation_ = byteString;
                this.slotPubKeys_ = Collections.emptyList();
                this.slotKeySigs_ = Collections.emptyList();
                this.amount_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.DirectiveCreateValidator.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.DirectiveCreateValidator.access$11500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$DirectiveCreateValidator r3 = (wallet.core.jni.proto.Harmony.DirectiveCreateValidator) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$DirectiveCreateValidator r4 = (wallet.core.jni.proto.Harmony.DirectiveCreateValidator) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.DirectiveCreateValidator.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$DirectiveCreateValidator$Builder");
            }
        }

        public /* synthetic */ DirectiveCreateValidator(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DirectiveCreateValidator getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveCreateValidator_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DirectiveCreateValidator parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DirectiveCreateValidator) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DirectiveCreateValidator parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DirectiveCreateValidator> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DirectiveCreateValidator)) {
                return super.equals(obj);
            }
            DirectiveCreateValidator directiveCreateValidator = (DirectiveCreateValidator) obj;
            if (getValidatorAddress().equals(directiveCreateValidator.getValidatorAddress()) && hasDescription() == directiveCreateValidator.hasDescription()) {
                if ((!hasDescription() || getDescription().equals(directiveCreateValidator.getDescription())) && hasCommissionRates() == directiveCreateValidator.hasCommissionRates()) {
                    return (!hasCommissionRates() || getCommissionRates().equals(directiveCreateValidator.getCommissionRates())) && getMinSelfDelegation().equals(directiveCreateValidator.getMinSelfDelegation()) && getMaxTotalDelegation().equals(directiveCreateValidator.getMaxTotalDelegation()) && getSlotPubKeysList().equals(directiveCreateValidator.getSlotPubKeysList()) && getSlotKeySigsList().equals(directiveCreateValidator.getSlotKeySigsList()) && getAmount().equals(directiveCreateValidator.getAmount()) && this.unknownFields.equals(directiveCreateValidator.unknownFields);
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public ByteString getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public CommissionRate getCommissionRates() {
            CommissionRate commissionRate = this.commissionRates_;
            return commissionRate == null ? CommissionRate.getDefaultInstance() : commissionRate;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public CommissionRateOrBuilder getCommissionRatesOrBuilder() {
            return getCommissionRates();
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public Description getDescription() {
            Description description = this.description_;
            return description == null ? Description.getDefaultInstance() : description;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public DescriptionOrBuilder getDescriptionOrBuilder() {
            return getDescription();
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public ByteString getMaxTotalDelegation() {
            return this.maxTotalDelegation_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public ByteString getMinSelfDelegation() {
            return this.minSelfDelegation_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DirectiveCreateValidator> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = !getValidatorAddressBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.validatorAddress_) + 0 : 0;
            if (this.description_ != null) {
                computeStringSize += CodedOutputStream.G(2, getDescription());
            }
            if (this.commissionRates_ != null) {
                computeStringSize += CodedOutputStream.G(3, getCommissionRates());
            }
            if (!this.minSelfDelegation_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.minSelfDelegation_);
            }
            if (!this.maxTotalDelegation_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(5, this.maxTotalDelegation_);
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.slotPubKeys_.size(); i3++) {
                i2 += CodedOutputStream.i(this.slotPubKeys_.get(i3));
            }
            int size = computeStringSize + i2 + (getSlotPubKeysList().size() * 1);
            int i4 = 0;
            for (int i5 = 0; i5 < this.slotKeySigs_.size(); i5++) {
                i4 += CodedOutputStream.i(this.slotKeySigs_.get(i5));
            }
            int size2 = size + i4 + (getSlotKeySigsList().size() * 1);
            if (!this.amount_.isEmpty()) {
                size2 += CodedOutputStream.h(8, this.amount_);
            }
            int serializedSize = size2 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public ByteString getSlotKeySigs(int i) {
            return this.slotKeySigs_.get(i);
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public int getSlotKeySigsCount() {
            return this.slotKeySigs_.size();
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public List<ByteString> getSlotKeySigsList() {
            return this.slotKeySigs_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public ByteString getSlotPubKeys(int i) {
            return this.slotPubKeys_.get(i);
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public int getSlotPubKeysCount() {
            return this.slotPubKeys_.size();
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public List<ByteString> getSlotPubKeysList() {
            return this.slotPubKeys_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public String getValidatorAddress() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.validatorAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public ByteString getValidatorAddressBytes() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.validatorAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public boolean hasCommissionRates() {
            return this.commissionRates_ != null;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveCreateValidatorOrBuilder
        public boolean hasDescription() {
            return this.description_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValidatorAddress().hashCode();
            if (hasDescription()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getDescription().hashCode();
            }
            if (hasCommissionRates()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getCommissionRates().hashCode();
            }
            int hashCode2 = (((((((hashCode * 37) + 4) * 53) + getMinSelfDelegation().hashCode()) * 37) + 5) * 53) + getMaxTotalDelegation().hashCode();
            if (getSlotPubKeysCount() > 0) {
                hashCode2 = (((hashCode2 * 37) + 6) * 53) + getSlotPubKeysList().hashCode();
            }
            if (getSlotKeySigsCount() > 0) {
                hashCode2 = (((hashCode2 * 37) + 7) * 53) + getSlotKeySigsList().hashCode();
            }
            int hashCode3 = (((((hashCode2 * 37) + 8) * 53) + getAmount().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode3;
            return hashCode3;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveCreateValidator_fieldAccessorTable.d(DirectiveCreateValidator.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DirectiveCreateValidator();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getValidatorAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.validatorAddress_);
            }
            if (this.description_ != null) {
                codedOutputStream.K0(2, getDescription());
            }
            if (this.commissionRates_ != null) {
                codedOutputStream.K0(3, getCommissionRates());
            }
            if (!this.minSelfDelegation_.isEmpty()) {
                codedOutputStream.q0(4, this.minSelfDelegation_);
            }
            if (!this.maxTotalDelegation_.isEmpty()) {
                codedOutputStream.q0(5, this.maxTotalDelegation_);
            }
            for (int i = 0; i < this.slotPubKeys_.size(); i++) {
                codedOutputStream.q0(6, this.slotPubKeys_.get(i));
            }
            for (int i2 = 0; i2 < this.slotKeySigs_.size(); i2++) {
                codedOutputStream.q0(7, this.slotKeySigs_.get(i2));
            }
            if (!this.amount_.isEmpty()) {
                codedOutputStream.q0(8, this.amount_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DirectiveCreateValidator(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DirectiveCreateValidator directiveCreateValidator) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(directiveCreateValidator);
        }

        public static DirectiveCreateValidator parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DirectiveCreateValidator(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DirectiveCreateValidator parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveCreateValidator) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveCreateValidator parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DirectiveCreateValidator getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DirectiveCreateValidator parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DirectiveCreateValidator() {
            this.memoizedIsInitialized = (byte) -1;
            this.validatorAddress_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.minSelfDelegation_ = byteString;
            this.maxTotalDelegation_ = byteString;
            this.slotPubKeys_ = Collections.emptyList();
            this.slotKeySigs_ = Collections.emptyList();
            this.amount_ = byteString;
        }

        public static DirectiveCreateValidator parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DirectiveCreateValidator parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DirectiveCreateValidator parseFrom(InputStream inputStream) throws IOException {
            return (DirectiveCreateValidator) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static DirectiveCreateValidator parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveCreateValidator) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveCreateValidator parseFrom(j jVar) throws IOException {
            return (DirectiveCreateValidator) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private DirectiveCreateValidator(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (J == 18) {
                                    Description description = this.description_;
                                    Description.Builder builder = description != null ? description.toBuilder() : null;
                                    Description description2 = (Description) jVar.z(Description.parser(), rVar);
                                    this.description_ = description2;
                                    if (builder != null) {
                                        builder.mergeFrom(description2);
                                        this.description_ = builder.buildPartial();
                                    }
                                } else if (J == 26) {
                                    CommissionRate commissionRate = this.commissionRates_;
                                    CommissionRate.Builder builder2 = commissionRate != null ? commissionRate.toBuilder() : null;
                                    CommissionRate commissionRate2 = (CommissionRate) jVar.z(CommissionRate.parser(), rVar);
                                    this.commissionRates_ = commissionRate2;
                                    if (builder2 != null) {
                                        builder2.mergeFrom(commissionRate2);
                                        this.commissionRates_ = builder2.buildPartial();
                                    }
                                } else if (J == 34) {
                                    this.minSelfDelegation_ = jVar.q();
                                } else if (J == 42) {
                                    this.maxTotalDelegation_ = jVar.q();
                                } else if (J == 50) {
                                    boolean z3 = (z2 ? 1 : 0) & true;
                                    z2 = z2;
                                    if (!z3) {
                                        this.slotPubKeys_ = new ArrayList();
                                        z2 = (z2 ? 1 : 0) | true;
                                    }
                                    this.slotPubKeys_.add(jVar.q());
                                } else if (J == 58) {
                                    boolean z4 = (z2 ? 1 : 0) & true;
                                    z2 = z2;
                                    if (!z4) {
                                        this.slotKeySigs_ = new ArrayList();
                                        z2 = (z2 ? 1 : 0) | true;
                                    }
                                    this.slotKeySigs_.add(jVar.q());
                                } else if (J != 66) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.amount_ = jVar.q();
                                }
                            } else {
                                this.validatorAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if ((z2 ? 1 : 0) & true) {
                        this.slotPubKeys_ = Collections.unmodifiableList(this.slotPubKeys_);
                    }
                    if ((z2 ? 1 : 0) & true) {
                        this.slotKeySigs_ = Collections.unmodifiableList(this.slotKeySigs_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DirectiveCreateValidator parseFrom(j jVar, r rVar) throws IOException {
            return (DirectiveCreateValidator) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DirectiveCreateValidatorOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAmount();

        CommissionRate getCommissionRates();

        CommissionRateOrBuilder getCommissionRatesOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        Description getDescription();

        DescriptionOrBuilder getDescriptionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        ByteString getMaxTotalDelegation();

        ByteString getMinSelfDelegation();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSlotKeySigs(int i);

        int getSlotKeySigsCount();

        List<ByteString> getSlotKeySigsList();

        ByteString getSlotPubKeys(int i);

        int getSlotPubKeysCount();

        List<ByteString> getSlotPubKeysList();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValidatorAddress();

        ByteString getValidatorAddressBytes();

        boolean hasCommissionRates();

        boolean hasDescription();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DirectiveDelegate extends GeneratedMessageV3 implements DirectiveDelegateOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 3;
        public static final int DELEGATOR_ADDRESS_FIELD_NUMBER = 1;
        public static final int VALIDATOR_ADDRESS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString amount_;
        private volatile Object delegatorAddress_;
        private byte memoizedIsInitialized;
        private volatile Object validatorAddress_;
        private static final DirectiveDelegate DEFAULT_INSTANCE = new DirectiveDelegate();
        private static final t0<DirectiveDelegate> PARSER = new c<DirectiveDelegate>() { // from class: wallet.core.jni.proto.Harmony.DirectiveDelegate.1
            @Override // com.google.protobuf.t0
            public DirectiveDelegate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DirectiveDelegate(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DirectiveDelegateOrBuilder {
            private ByteString amount_;
            private Object delegatorAddress_;
            private Object validatorAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveDelegate_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = DirectiveDelegate.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearDelegatorAddress() {
                this.delegatorAddress_ = DirectiveDelegate.getDefaultInstance().getDelegatorAddress();
                onChanged();
                return this;
            }

            public Builder clearValidatorAddress() {
                this.validatorAddress_ = DirectiveDelegate.getDefaultInstance().getValidatorAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
            public String getDelegatorAddress() {
                Object obj = this.delegatorAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.delegatorAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
            public ByteString getDelegatorAddressBytes() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveDelegate_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
            public String getValidatorAddress() {
                Object obj = this.validatorAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.validatorAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
            public ByteString getValidatorAddressBytes() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveDelegate_fieldAccessorTable.d(DirectiveDelegate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setDelegatorAddress(String str) {
                Objects.requireNonNull(str);
                this.delegatorAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setDelegatorAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.delegatorAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValidatorAddress(String str) {
                Objects.requireNonNull(str);
                this.validatorAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setValidatorAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.validatorAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveDelegate build() {
                DirectiveDelegate buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveDelegate buildPartial() {
                DirectiveDelegate directiveDelegate = new DirectiveDelegate(this, (AnonymousClass1) null);
                directiveDelegate.delegatorAddress_ = this.delegatorAddress_;
                directiveDelegate.validatorAddress_ = this.validatorAddress_;
                directiveDelegate.amount_ = this.amount_;
                onBuilt();
                return directiveDelegate;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DirectiveDelegate getDefaultInstanceForType() {
                return DirectiveDelegate.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DirectiveDelegate) {
                    return mergeFrom((DirectiveDelegate) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(DirectiveDelegate directiveDelegate) {
                if (directiveDelegate == DirectiveDelegate.getDefaultInstance()) {
                    return this;
                }
                if (!directiveDelegate.getDelegatorAddress().isEmpty()) {
                    this.delegatorAddress_ = directiveDelegate.delegatorAddress_;
                    onChanged();
                }
                if (!directiveDelegate.getValidatorAddress().isEmpty()) {
                    this.validatorAddress_ = directiveDelegate.validatorAddress_;
                    onChanged();
                }
                if (directiveDelegate.getAmount() != ByteString.EMPTY) {
                    setAmount(directiveDelegate.getAmount());
                }
                mergeUnknownFields(directiveDelegate.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.DirectiveDelegate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.DirectiveDelegate.access$14700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$DirectiveDelegate r3 = (wallet.core.jni.proto.Harmony.DirectiveDelegate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$DirectiveDelegate r4 = (wallet.core.jni.proto.Harmony.DirectiveDelegate) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.DirectiveDelegate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$DirectiveDelegate$Builder");
            }
        }

        public /* synthetic */ DirectiveDelegate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DirectiveDelegate getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveDelegate_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DirectiveDelegate parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DirectiveDelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DirectiveDelegate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DirectiveDelegate> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DirectiveDelegate)) {
                return super.equals(obj);
            }
            DirectiveDelegate directiveDelegate = (DirectiveDelegate) obj;
            return getDelegatorAddress().equals(directiveDelegate.getDelegatorAddress()) && getValidatorAddress().equals(directiveDelegate.getValidatorAddress()) && getAmount().equals(directiveDelegate.getAmount()) && this.unknownFields.equals(directiveDelegate.unknownFields);
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
        public ByteString getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
        public String getDelegatorAddress() {
            Object obj = this.delegatorAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.delegatorAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
        public ByteString getDelegatorAddressBytes() {
            Object obj = this.delegatorAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.delegatorAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DirectiveDelegate> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getDelegatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegatorAddress_);
            if (!getValidatorAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.validatorAddress_);
            }
            if (!this.amount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.amount_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
        public String getValidatorAddress() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.validatorAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveDelegateOrBuilder
        public ByteString getValidatorAddressBytes() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.validatorAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegatorAddress().hashCode()) * 37) + 2) * 53) + getValidatorAddress().hashCode()) * 37) + 3) * 53) + getAmount().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveDelegate_fieldAccessorTable.d(DirectiveDelegate.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DirectiveDelegate();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDelegatorAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegatorAddress_);
            }
            if (!getValidatorAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.validatorAddress_);
            }
            if (!this.amount_.isEmpty()) {
                codedOutputStream.q0(3, this.amount_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DirectiveDelegate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DirectiveDelegate directiveDelegate) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(directiveDelegate);
        }

        public static DirectiveDelegate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DirectiveDelegate(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DirectiveDelegate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveDelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveDelegate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DirectiveDelegate getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DirectiveDelegate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DirectiveDelegate() {
            this.memoizedIsInitialized = (byte) -1;
            this.delegatorAddress_ = "";
            this.validatorAddress_ = "";
            this.amount_ = ByteString.EMPTY;
        }

        public static DirectiveDelegate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DirectiveDelegate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DirectiveDelegate parseFrom(InputStream inputStream) throws IOException {
            return (DirectiveDelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static DirectiveDelegate parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveDelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private DirectiveDelegate(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.delegatorAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.validatorAddress_ = jVar.I();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.amount_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DirectiveDelegate parseFrom(j jVar) throws IOException {
            return (DirectiveDelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DirectiveDelegate parseFrom(j jVar, r rVar) throws IOException {
            return (DirectiveDelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DirectiveDelegateOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        String getDelegatorAddress();

        ByteString getDelegatorAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValidatorAddress();

        ByteString getValidatorAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DirectiveEditValidator extends GeneratedMessageV3 implements DirectiveEditValidatorOrBuilder {
        public static final int ACTIVE_FIELD_NUMBER = 9;
        public static final int COMMISSION_RATE_FIELD_NUMBER = 3;
        public static final int DESCRIPTION_FIELD_NUMBER = 2;
        public static final int MAX_TOTAL_DELEGATION_FIELD_NUMBER = 5;
        public static final int MIN_SELF_DELEGATION_FIELD_NUMBER = 4;
        public static final int SLOT_KEY_TO_ADD_FIELD_NUMBER = 7;
        public static final int SLOT_KEY_TO_ADD_SIG_FIELD_NUMBER = 8;
        public static final int SLOT_KEY_TO_REMOVE_FIELD_NUMBER = 6;
        public static final int VALIDATOR_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString active_;
        private Decimal commissionRate_;
        private Description description_;
        private ByteString maxTotalDelegation_;
        private byte memoizedIsInitialized;
        private ByteString minSelfDelegation_;
        private ByteString slotKeyToAddSig_;
        private ByteString slotKeyToAdd_;
        private ByteString slotKeyToRemove_;
        private volatile Object validatorAddress_;
        private static final DirectiveEditValidator DEFAULT_INSTANCE = new DirectiveEditValidator();
        private static final t0<DirectiveEditValidator> PARSER = new c<DirectiveEditValidator>() { // from class: wallet.core.jni.proto.Harmony.DirectiveEditValidator.1
            @Override // com.google.protobuf.t0
            public DirectiveEditValidator parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DirectiveEditValidator(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DirectiveEditValidatorOrBuilder {
            private ByteString active_;
            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> commissionRateBuilder_;
            private Decimal commissionRate_;
            private a1<Description, Description.Builder, DescriptionOrBuilder> descriptionBuilder_;
            private Description description_;
            private ByteString maxTotalDelegation_;
            private ByteString minSelfDelegation_;
            private ByteString slotKeyToAddSig_;
            private ByteString slotKeyToAdd_;
            private ByteString slotKeyToRemove_;
            private Object validatorAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<Decimal, Decimal.Builder, DecimalOrBuilder> getCommissionRateFieldBuilder() {
                if (this.commissionRateBuilder_ == null) {
                    this.commissionRateBuilder_ = new a1<>(getCommissionRate(), getParentForChildren(), isClean());
                    this.commissionRate_ = null;
                }
                return this.commissionRateBuilder_;
            }

            private a1<Description, Description.Builder, DescriptionOrBuilder> getDescriptionFieldBuilder() {
                if (this.descriptionBuilder_ == null) {
                    this.descriptionBuilder_ = new a1<>(getDescription(), getParentForChildren(), isClean());
                    this.description_ = null;
                }
                return this.descriptionBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveEditValidator_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearActive() {
                this.active_ = DirectiveEditValidator.getDefaultInstance().getActive();
                onChanged();
                return this;
            }

            public Builder clearCommissionRate() {
                if (this.commissionRateBuilder_ == null) {
                    this.commissionRate_ = null;
                    onChanged();
                } else {
                    this.commissionRate_ = null;
                    this.commissionRateBuilder_ = null;
                }
                return this;
            }

            public Builder clearDescription() {
                if (this.descriptionBuilder_ == null) {
                    this.description_ = null;
                    onChanged();
                } else {
                    this.description_ = null;
                    this.descriptionBuilder_ = null;
                }
                return this;
            }

            public Builder clearMaxTotalDelegation() {
                this.maxTotalDelegation_ = DirectiveEditValidator.getDefaultInstance().getMaxTotalDelegation();
                onChanged();
                return this;
            }

            public Builder clearMinSelfDelegation() {
                this.minSelfDelegation_ = DirectiveEditValidator.getDefaultInstance().getMinSelfDelegation();
                onChanged();
                return this;
            }

            public Builder clearSlotKeyToAdd() {
                this.slotKeyToAdd_ = DirectiveEditValidator.getDefaultInstance().getSlotKeyToAdd();
                onChanged();
                return this;
            }

            public Builder clearSlotKeyToAddSig() {
                this.slotKeyToAddSig_ = DirectiveEditValidator.getDefaultInstance().getSlotKeyToAddSig();
                onChanged();
                return this;
            }

            public Builder clearSlotKeyToRemove() {
                this.slotKeyToRemove_ = DirectiveEditValidator.getDefaultInstance().getSlotKeyToRemove();
                onChanged();
                return this;
            }

            public Builder clearValidatorAddress() {
                this.validatorAddress_ = DirectiveEditValidator.getDefaultInstance().getValidatorAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public ByteString getActive() {
                return this.active_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public Decimal getCommissionRate() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.commissionRateBuilder_;
                if (a1Var == null) {
                    Decimal decimal = this.commissionRate_;
                    return decimal == null ? Decimal.getDefaultInstance() : decimal;
                }
                return a1Var.f();
            }

            public Decimal.Builder getCommissionRateBuilder() {
                onChanged();
                return getCommissionRateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public DecimalOrBuilder getCommissionRateOrBuilder() {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.commissionRateBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Decimal decimal = this.commissionRate_;
                return decimal == null ? Decimal.getDefaultInstance() : decimal;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public Description getDescription() {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    Description description = this.description_;
                    return description == null ? Description.getDefaultInstance() : description;
                }
                return a1Var.f();
            }

            public Description.Builder getDescriptionBuilder() {
                onChanged();
                return getDescriptionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public DescriptionOrBuilder getDescriptionOrBuilder() {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Description description = this.description_;
                return description == null ? Description.getDefaultInstance() : description;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveEditValidator_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public ByteString getMaxTotalDelegation() {
                return this.maxTotalDelegation_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public ByteString getMinSelfDelegation() {
                return this.minSelfDelegation_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public ByteString getSlotKeyToAdd() {
                return this.slotKeyToAdd_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public ByteString getSlotKeyToAddSig() {
                return this.slotKeyToAddSig_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public ByteString getSlotKeyToRemove() {
                return this.slotKeyToRemove_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public String getValidatorAddress() {
                Object obj = this.validatorAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.validatorAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public ByteString getValidatorAddressBytes() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public boolean hasCommissionRate() {
                return (this.commissionRateBuilder_ == null && this.commissionRate_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
            public boolean hasDescription() {
                return (this.descriptionBuilder_ == null && this.description_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveEditValidator_fieldAccessorTable.d(DirectiveEditValidator.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCommissionRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.commissionRateBuilder_;
                if (a1Var == null) {
                    Decimal decimal2 = this.commissionRate_;
                    if (decimal2 != null) {
                        this.commissionRate_ = Decimal.newBuilder(decimal2).mergeFrom(decimal).buildPartial();
                    } else {
                        this.commissionRate_ = decimal;
                    }
                    onChanged();
                } else {
                    a1Var.h(decimal);
                }
                return this;
            }

            public Builder mergeDescription(Description description) {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    Description description2 = this.description_;
                    if (description2 != null) {
                        this.description_ = Description.newBuilder(description2).mergeFrom(description).buildPartial();
                    } else {
                        this.description_ = description;
                    }
                    onChanged();
                } else {
                    a1Var.h(description);
                }
                return this;
            }

            public Builder setActive(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.active_ = byteString;
                onChanged();
                return this;
            }

            public Builder setCommissionRate(Decimal decimal) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.commissionRateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(decimal);
                    this.commissionRate_ = decimal;
                    onChanged();
                } else {
                    a1Var.j(decimal);
                }
                return this;
            }

            public Builder setDescription(Description description) {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(description);
                    this.description_ = description;
                    onChanged();
                } else {
                    a1Var.j(description);
                }
                return this;
            }

            public Builder setMaxTotalDelegation(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.maxTotalDelegation_ = byteString;
                onChanged();
                return this;
            }

            public Builder setMinSelfDelegation(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.minSelfDelegation_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSlotKeyToAdd(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.slotKeyToAdd_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSlotKeyToAddSig(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.slotKeyToAddSig_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSlotKeyToRemove(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.slotKeyToRemove_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValidatorAddress(String str) {
                Objects.requireNonNull(str);
                this.validatorAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setValidatorAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.validatorAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.validatorAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.minSelfDelegation_ = byteString;
                this.maxTotalDelegation_ = byteString;
                this.slotKeyToRemove_ = byteString;
                this.slotKeyToAdd_ = byteString;
                this.slotKeyToAddSig_ = byteString;
                this.active_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveEditValidator build() {
                DirectiveEditValidator buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveEditValidator buildPartial() {
                DirectiveEditValidator directiveEditValidator = new DirectiveEditValidator(this, (AnonymousClass1) null);
                directiveEditValidator.validatorAddress_ = this.validatorAddress_;
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    directiveEditValidator.description_ = this.description_;
                } else {
                    directiveEditValidator.description_ = a1Var.b();
                }
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var2 = this.commissionRateBuilder_;
                if (a1Var2 == null) {
                    directiveEditValidator.commissionRate_ = this.commissionRate_;
                } else {
                    directiveEditValidator.commissionRate_ = a1Var2.b();
                }
                directiveEditValidator.minSelfDelegation_ = this.minSelfDelegation_;
                directiveEditValidator.maxTotalDelegation_ = this.maxTotalDelegation_;
                directiveEditValidator.slotKeyToRemove_ = this.slotKeyToRemove_;
                directiveEditValidator.slotKeyToAdd_ = this.slotKeyToAdd_;
                directiveEditValidator.slotKeyToAddSig_ = this.slotKeyToAddSig_;
                directiveEditValidator.active_ = this.active_;
                onBuilt();
                return directiveEditValidator;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DirectiveEditValidator getDefaultInstanceForType() {
                return DirectiveEditValidator.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.validatorAddress_ = "";
                if (this.descriptionBuilder_ == null) {
                    this.description_ = null;
                } else {
                    this.description_ = null;
                    this.descriptionBuilder_ = null;
                }
                if (this.commissionRateBuilder_ == null) {
                    this.commissionRate_ = null;
                } else {
                    this.commissionRate_ = null;
                    this.commissionRateBuilder_ = null;
                }
                ByteString byteString = ByteString.EMPTY;
                this.minSelfDelegation_ = byteString;
                this.maxTotalDelegation_ = byteString;
                this.slotKeyToRemove_ = byteString;
                this.slotKeyToAdd_ = byteString;
                this.slotKeyToAddSig_ = byteString;
                this.active_ = byteString;
                return this;
            }

            public Builder setCommissionRate(Decimal.Builder builder) {
                a1<Decimal, Decimal.Builder, DecimalOrBuilder> a1Var = this.commissionRateBuilder_;
                if (a1Var == null) {
                    this.commissionRate_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setDescription(Description.Builder builder) {
                a1<Description, Description.Builder, DescriptionOrBuilder> a1Var = this.descriptionBuilder_;
                if (a1Var == null) {
                    this.description_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DirectiveEditValidator) {
                    return mergeFrom((DirectiveEditValidator) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DirectiveEditValidator directiveEditValidator) {
                if (directiveEditValidator == DirectiveEditValidator.getDefaultInstance()) {
                    return this;
                }
                if (!directiveEditValidator.getValidatorAddress().isEmpty()) {
                    this.validatorAddress_ = directiveEditValidator.validatorAddress_;
                    onChanged();
                }
                if (directiveEditValidator.hasDescription()) {
                    mergeDescription(directiveEditValidator.getDescription());
                }
                if (directiveEditValidator.hasCommissionRate()) {
                    mergeCommissionRate(directiveEditValidator.getCommissionRate());
                }
                ByteString minSelfDelegation = directiveEditValidator.getMinSelfDelegation();
                ByteString byteString = ByteString.EMPTY;
                if (minSelfDelegation != byteString) {
                    setMinSelfDelegation(directiveEditValidator.getMinSelfDelegation());
                }
                if (directiveEditValidator.getMaxTotalDelegation() != byteString) {
                    setMaxTotalDelegation(directiveEditValidator.getMaxTotalDelegation());
                }
                if (directiveEditValidator.getSlotKeyToRemove() != byteString) {
                    setSlotKeyToRemove(directiveEditValidator.getSlotKeyToRemove());
                }
                if (directiveEditValidator.getSlotKeyToAdd() != byteString) {
                    setSlotKeyToAdd(directiveEditValidator.getSlotKeyToAdd());
                }
                if (directiveEditValidator.getSlotKeyToAddSig() != byteString) {
                    setSlotKeyToAddSig(directiveEditValidator.getSlotKeyToAddSig());
                }
                if (directiveEditValidator.getActive() != byteString) {
                    setActive(directiveEditValidator.getActive());
                }
                mergeUnknownFields(directiveEditValidator.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.validatorAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.minSelfDelegation_ = byteString;
                this.maxTotalDelegation_ = byteString;
                this.slotKeyToRemove_ = byteString;
                this.slotKeyToAdd_ = byteString;
                this.slotKeyToAddSig_ = byteString;
                this.active_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.DirectiveEditValidator.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.DirectiveEditValidator.access$13400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$DirectiveEditValidator r3 = (wallet.core.jni.proto.Harmony.DirectiveEditValidator) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$DirectiveEditValidator r4 = (wallet.core.jni.proto.Harmony.DirectiveEditValidator) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.DirectiveEditValidator.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$DirectiveEditValidator$Builder");
            }
        }

        public /* synthetic */ DirectiveEditValidator(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DirectiveEditValidator getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveEditValidator_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DirectiveEditValidator parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DirectiveEditValidator) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DirectiveEditValidator parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DirectiveEditValidator> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DirectiveEditValidator)) {
                return super.equals(obj);
            }
            DirectiveEditValidator directiveEditValidator = (DirectiveEditValidator) obj;
            if (getValidatorAddress().equals(directiveEditValidator.getValidatorAddress()) && hasDescription() == directiveEditValidator.hasDescription()) {
                if ((!hasDescription() || getDescription().equals(directiveEditValidator.getDescription())) && hasCommissionRate() == directiveEditValidator.hasCommissionRate()) {
                    return (!hasCommissionRate() || getCommissionRate().equals(directiveEditValidator.getCommissionRate())) && getMinSelfDelegation().equals(directiveEditValidator.getMinSelfDelegation()) && getMaxTotalDelegation().equals(directiveEditValidator.getMaxTotalDelegation()) && getSlotKeyToRemove().equals(directiveEditValidator.getSlotKeyToRemove()) && getSlotKeyToAdd().equals(directiveEditValidator.getSlotKeyToAdd()) && getSlotKeyToAddSig().equals(directiveEditValidator.getSlotKeyToAddSig()) && getActive().equals(directiveEditValidator.getActive()) && this.unknownFields.equals(directiveEditValidator.unknownFields);
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public ByteString getActive() {
            return this.active_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public Decimal getCommissionRate() {
            Decimal decimal = this.commissionRate_;
            return decimal == null ? Decimal.getDefaultInstance() : decimal;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public DecimalOrBuilder getCommissionRateOrBuilder() {
            return getCommissionRate();
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public Description getDescription() {
            Description description = this.description_;
            return description == null ? Description.getDefaultInstance() : description;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public DescriptionOrBuilder getDescriptionOrBuilder() {
            return getDescription();
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public ByteString getMaxTotalDelegation() {
            return this.maxTotalDelegation_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public ByteString getMinSelfDelegation() {
            return this.minSelfDelegation_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DirectiveEditValidator> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getValidatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.validatorAddress_);
            if (this.description_ != null) {
                computeStringSize += CodedOutputStream.G(2, getDescription());
            }
            if (this.commissionRate_ != null) {
                computeStringSize += CodedOutputStream.G(3, getCommissionRate());
            }
            if (!this.minSelfDelegation_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.minSelfDelegation_);
            }
            if (!this.maxTotalDelegation_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(5, this.maxTotalDelegation_);
            }
            if (!this.slotKeyToRemove_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(6, this.slotKeyToRemove_);
            }
            if (!this.slotKeyToAdd_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(7, this.slotKeyToAdd_);
            }
            if (!this.slotKeyToAddSig_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(8, this.slotKeyToAddSig_);
            }
            if (!this.active_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(9, this.active_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public ByteString getSlotKeyToAdd() {
            return this.slotKeyToAdd_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public ByteString getSlotKeyToAddSig() {
            return this.slotKeyToAddSig_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public ByteString getSlotKeyToRemove() {
            return this.slotKeyToRemove_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public String getValidatorAddress() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.validatorAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public ByteString getValidatorAddressBytes() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.validatorAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public boolean hasCommissionRate() {
            return this.commissionRate_ != null;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveEditValidatorOrBuilder
        public boolean hasDescription() {
            return this.description_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValidatorAddress().hashCode();
            if (hasDescription()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getDescription().hashCode();
            }
            if (hasCommissionRate()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getCommissionRate().hashCode();
            }
            int hashCode2 = (((((((((((((((((((((((((hashCode * 37) + 4) * 53) + getMinSelfDelegation().hashCode()) * 37) + 5) * 53) + getMaxTotalDelegation().hashCode()) * 37) + 6) * 53) + getSlotKeyToRemove().hashCode()) * 37) + 7) * 53) + getSlotKeyToAdd().hashCode()) * 37) + 8) * 53) + getSlotKeyToAddSig().hashCode()) * 37) + 9) * 53) + getActive().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveEditValidator_fieldAccessorTable.d(DirectiveEditValidator.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DirectiveEditValidator();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getValidatorAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.validatorAddress_);
            }
            if (this.description_ != null) {
                codedOutputStream.K0(2, getDescription());
            }
            if (this.commissionRate_ != null) {
                codedOutputStream.K0(3, getCommissionRate());
            }
            if (!this.minSelfDelegation_.isEmpty()) {
                codedOutputStream.q0(4, this.minSelfDelegation_);
            }
            if (!this.maxTotalDelegation_.isEmpty()) {
                codedOutputStream.q0(5, this.maxTotalDelegation_);
            }
            if (!this.slotKeyToRemove_.isEmpty()) {
                codedOutputStream.q0(6, this.slotKeyToRemove_);
            }
            if (!this.slotKeyToAdd_.isEmpty()) {
                codedOutputStream.q0(7, this.slotKeyToAdd_);
            }
            if (!this.slotKeyToAddSig_.isEmpty()) {
                codedOutputStream.q0(8, this.slotKeyToAddSig_);
            }
            if (!this.active_.isEmpty()) {
                codedOutputStream.q0(9, this.active_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DirectiveEditValidator(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DirectiveEditValidator directiveEditValidator) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(directiveEditValidator);
        }

        public static DirectiveEditValidator parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DirectiveEditValidator(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DirectiveEditValidator parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveEditValidator) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveEditValidator parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DirectiveEditValidator getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DirectiveEditValidator parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DirectiveEditValidator() {
            this.memoizedIsInitialized = (byte) -1;
            this.validatorAddress_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.minSelfDelegation_ = byteString;
            this.maxTotalDelegation_ = byteString;
            this.slotKeyToRemove_ = byteString;
            this.slotKeyToAdd_ = byteString;
            this.slotKeyToAddSig_ = byteString;
            this.active_ = byteString;
        }

        public static DirectiveEditValidator parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DirectiveEditValidator parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DirectiveEditValidator parseFrom(InputStream inputStream) throws IOException {
            return (DirectiveEditValidator) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static DirectiveEditValidator parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveEditValidator) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveEditValidator parseFrom(j jVar) throws IOException {
            return (DirectiveEditValidator) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DirectiveEditValidator parseFrom(j jVar, r rVar) throws IOException {
            return (DirectiveEditValidator) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private DirectiveEditValidator(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (J == 18) {
                                    Description description = this.description_;
                                    Description.Builder builder = description != null ? description.toBuilder() : null;
                                    Description description2 = (Description) jVar.z(Description.parser(), rVar);
                                    this.description_ = description2;
                                    if (builder != null) {
                                        builder.mergeFrom(description2);
                                        this.description_ = builder.buildPartial();
                                    }
                                } else if (J == 26) {
                                    Decimal decimal = this.commissionRate_;
                                    Decimal.Builder builder2 = decimal != null ? decimal.toBuilder() : null;
                                    Decimal decimal2 = (Decimal) jVar.z(Decimal.parser(), rVar);
                                    this.commissionRate_ = decimal2;
                                    if (builder2 != null) {
                                        builder2.mergeFrom(decimal2);
                                        this.commissionRate_ = builder2.buildPartial();
                                    }
                                } else if (J == 34) {
                                    this.minSelfDelegation_ = jVar.q();
                                } else if (J == 42) {
                                    this.maxTotalDelegation_ = jVar.q();
                                } else if (J == 50) {
                                    this.slotKeyToRemove_ = jVar.q();
                                } else if (J == 58) {
                                    this.slotKeyToAdd_ = jVar.q();
                                } else if (J == 66) {
                                    this.slotKeyToAddSig_ = jVar.q();
                                } else if (J != 74) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.active_ = jVar.q();
                                }
                            } else {
                                this.validatorAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface DirectiveEditValidatorOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        ByteString getActive();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Decimal getCommissionRate();

        DecimalOrBuilder getCommissionRateOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        Description getDescription();

        DescriptionOrBuilder getDescriptionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        ByteString getMaxTotalDelegation();

        ByteString getMinSelfDelegation();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSlotKeyToAdd();

        ByteString getSlotKeyToAddSig();

        ByteString getSlotKeyToRemove();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValidatorAddress();

        ByteString getValidatorAddressBytes();

        boolean hasCommissionRate();

        boolean hasDescription();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DirectiveUndelegate extends GeneratedMessageV3 implements DirectiveUndelegateOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 3;
        public static final int DELEGATOR_ADDRESS_FIELD_NUMBER = 1;
        public static final int VALIDATOR_ADDRESS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString amount_;
        private volatile Object delegatorAddress_;
        private byte memoizedIsInitialized;
        private volatile Object validatorAddress_;
        private static final DirectiveUndelegate DEFAULT_INSTANCE = new DirectiveUndelegate();
        private static final t0<DirectiveUndelegate> PARSER = new c<DirectiveUndelegate>() { // from class: wallet.core.jni.proto.Harmony.DirectiveUndelegate.1
            @Override // com.google.protobuf.t0
            public DirectiveUndelegate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DirectiveUndelegate(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DirectiveUndelegateOrBuilder {
            private ByteString amount_;
            private Object delegatorAddress_;
            private Object validatorAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveUndelegate_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = DirectiveUndelegate.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearDelegatorAddress() {
                this.delegatorAddress_ = DirectiveUndelegate.getDefaultInstance().getDelegatorAddress();
                onChanged();
                return this;
            }

            public Builder clearValidatorAddress() {
                this.validatorAddress_ = DirectiveUndelegate.getDefaultInstance().getValidatorAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
            public String getDelegatorAddress() {
                Object obj = this.delegatorAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.delegatorAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
            public ByteString getDelegatorAddressBytes() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveUndelegate_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
            public String getValidatorAddress() {
                Object obj = this.validatorAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.validatorAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
            public ByteString getValidatorAddressBytes() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_DirectiveUndelegate_fieldAccessorTable.d(DirectiveUndelegate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setDelegatorAddress(String str) {
                Objects.requireNonNull(str);
                this.delegatorAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setDelegatorAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.delegatorAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValidatorAddress(String str) {
                Objects.requireNonNull(str);
                this.validatorAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setValidatorAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.validatorAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveUndelegate build() {
                DirectiveUndelegate buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DirectiveUndelegate buildPartial() {
                DirectiveUndelegate directiveUndelegate = new DirectiveUndelegate(this, (AnonymousClass1) null);
                directiveUndelegate.delegatorAddress_ = this.delegatorAddress_;
                directiveUndelegate.validatorAddress_ = this.validatorAddress_;
                directiveUndelegate.amount_ = this.amount_;
                onBuilt();
                return directiveUndelegate;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DirectiveUndelegate getDefaultInstanceForType() {
                return DirectiveUndelegate.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DirectiveUndelegate) {
                    return mergeFrom((DirectiveUndelegate) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(DirectiveUndelegate directiveUndelegate) {
                if (directiveUndelegate == DirectiveUndelegate.getDefaultInstance()) {
                    return this;
                }
                if (!directiveUndelegate.getDelegatorAddress().isEmpty()) {
                    this.delegatorAddress_ = directiveUndelegate.delegatorAddress_;
                    onChanged();
                }
                if (!directiveUndelegate.getValidatorAddress().isEmpty()) {
                    this.validatorAddress_ = directiveUndelegate.validatorAddress_;
                    onChanged();
                }
                if (directiveUndelegate.getAmount() != ByteString.EMPTY) {
                    setAmount(directiveUndelegate.getAmount());
                }
                mergeUnknownFields(directiveUndelegate.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.DirectiveUndelegate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.DirectiveUndelegate.access$16100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$DirectiveUndelegate r3 = (wallet.core.jni.proto.Harmony.DirectiveUndelegate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$DirectiveUndelegate r4 = (wallet.core.jni.proto.Harmony.DirectiveUndelegate) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.DirectiveUndelegate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$DirectiveUndelegate$Builder");
            }
        }

        public /* synthetic */ DirectiveUndelegate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DirectiveUndelegate getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveUndelegate_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DirectiveUndelegate parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DirectiveUndelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DirectiveUndelegate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DirectiveUndelegate> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DirectiveUndelegate)) {
                return super.equals(obj);
            }
            DirectiveUndelegate directiveUndelegate = (DirectiveUndelegate) obj;
            return getDelegatorAddress().equals(directiveUndelegate.getDelegatorAddress()) && getValidatorAddress().equals(directiveUndelegate.getValidatorAddress()) && getAmount().equals(directiveUndelegate.getAmount()) && this.unknownFields.equals(directiveUndelegate.unknownFields);
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
        public ByteString getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
        public String getDelegatorAddress() {
            Object obj = this.delegatorAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.delegatorAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
        public ByteString getDelegatorAddressBytes() {
            Object obj = this.delegatorAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.delegatorAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DirectiveUndelegate> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getDelegatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegatorAddress_);
            if (!getValidatorAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.validatorAddress_);
            }
            if (!this.amount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.amount_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
        public String getValidatorAddress() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.validatorAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.DirectiveUndelegateOrBuilder
        public ByteString getValidatorAddressBytes() {
            Object obj = this.validatorAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.validatorAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegatorAddress().hashCode()) * 37) + 2) * 53) + getValidatorAddress().hashCode()) * 37) + 3) * 53) + getAmount().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_DirectiveUndelegate_fieldAccessorTable.d(DirectiveUndelegate.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DirectiveUndelegate();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDelegatorAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegatorAddress_);
            }
            if (!getValidatorAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.validatorAddress_);
            }
            if (!this.amount_.isEmpty()) {
                codedOutputStream.q0(3, this.amount_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DirectiveUndelegate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DirectiveUndelegate directiveUndelegate) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(directiveUndelegate);
        }

        public static DirectiveUndelegate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DirectiveUndelegate(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DirectiveUndelegate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveUndelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DirectiveUndelegate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DirectiveUndelegate getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DirectiveUndelegate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DirectiveUndelegate() {
            this.memoizedIsInitialized = (byte) -1;
            this.delegatorAddress_ = "";
            this.validatorAddress_ = "";
            this.amount_ = ByteString.EMPTY;
        }

        public static DirectiveUndelegate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DirectiveUndelegate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DirectiveUndelegate parseFrom(InputStream inputStream) throws IOException {
            return (DirectiveUndelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static DirectiveUndelegate parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DirectiveUndelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private DirectiveUndelegate(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.delegatorAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.validatorAddress_ = jVar.I();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.amount_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DirectiveUndelegate parseFrom(j jVar) throws IOException {
            return (DirectiveUndelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DirectiveUndelegate parseFrom(j jVar, r rVar) throws IOException {
            return (DirectiveUndelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DirectiveUndelegateOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        String getDelegatorAddress();

        ByteString getDelegatorAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValidatorAddress();

        ByteString getValidatorAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int CHAIN_ID_FIELD_NUMBER = 1;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Harmony.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };
        public static final int PRIVATE_KEY_FIELD_NUMBER = 2;
        public static final int STAKING_MESSAGE_FIELD_NUMBER = 4;
        public static final int TRANSACTION_MESSAGE_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ByteString chainId_;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;
        private ByteString privateKey_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private ByteString chainId_;
            private int messageOneofCase_;
            private Object messageOneof_;
            private ByteString privateKey_;
            private a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> stakingMessageBuilder_;
            private a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> transactionMessageBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_SigningInput_descriptor;
            }

            private a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> getStakingMessageFieldBuilder() {
                if (this.stakingMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 4) {
                        this.messageOneof_ = StakingMessage.getDefaultInstance();
                    }
                    this.stakingMessageBuilder_ = new a1<>((StakingMessage) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 4;
                onChanged();
                return this.stakingMessageBuilder_;
            }

            private a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> getTransactionMessageFieldBuilder() {
                if (this.transactionMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 3) {
                        this.messageOneof_ = TransactionMessage.getDefaultInstance();
                    }
                    this.transactionMessageBuilder_ = new a1<>((TransactionMessage) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 3;
                onChanged();
                return this.transactionMessageBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearChainId() {
                this.chainId_ = SigningInput.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearStakingMessage() {
                a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> a1Var = this.stakingMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransactionMessage() {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public ByteString getChainId() {
                return this.chainId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public StakingMessage getStakingMessage() {
                a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> a1Var = this.stakingMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        return (StakingMessage) this.messageOneof_;
                    }
                    return StakingMessage.getDefaultInstance();
                } else if (this.messageOneofCase_ == 4) {
                    return a1Var.f();
                } else {
                    return StakingMessage.getDefaultInstance();
                }
            }

            public StakingMessage.Builder getStakingMessageBuilder() {
                return getStakingMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public StakingMessageOrBuilder getStakingMessageOrBuilder() {
                a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 4 || (a1Var = this.stakingMessageBuilder_) == null) {
                    if (i == 4) {
                        return (StakingMessage) this.messageOneof_;
                    }
                    return StakingMessage.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public TransactionMessage getTransactionMessage() {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        return (TransactionMessage) this.messageOneof_;
                    }
                    return TransactionMessage.getDefaultInstance();
                } else if (this.messageOneofCase_ == 3) {
                    return a1Var.f();
                } else {
                    return TransactionMessage.getDefaultInstance();
                }
            }

            public TransactionMessage.Builder getTransactionMessageBuilder() {
                return getTransactionMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public TransactionMessageOrBuilder getTransactionMessageOrBuilder() {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 3 || (a1Var = this.transactionMessageBuilder_) == null) {
                    if (i == 3) {
                        return (TransactionMessage) this.messageOneof_;
                    }
                    return TransactionMessage.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public boolean hasStakingMessage() {
                return this.messageOneofCase_ == 4;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
            public boolean hasTransactionMessage() {
                return this.messageOneofCase_ == 3;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeStakingMessage(StakingMessage stakingMessage) {
                a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> a1Var = this.stakingMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4 && this.messageOneof_ != StakingMessage.getDefaultInstance()) {
                        this.messageOneof_ = StakingMessage.newBuilder((StakingMessage) this.messageOneof_).mergeFrom(stakingMessage).buildPartial();
                    } else {
                        this.messageOneof_ = stakingMessage;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 4) {
                        a1Var.h(stakingMessage);
                    }
                    this.stakingMessageBuilder_.j(stakingMessage);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder mergeTransactionMessage(TransactionMessage transactionMessage) {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3 && this.messageOneof_ != TransactionMessage.getDefaultInstance()) {
                        this.messageOneof_ = TransactionMessage.newBuilder((TransactionMessage) this.messageOneof_).mergeFrom(transactionMessage).buildPartial();
                    } else {
                        this.messageOneof_ = transactionMessage;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 3) {
                        a1Var.h(transactionMessage);
                    }
                    this.transactionMessageBuilder_.j(transactionMessage);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder setChainId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setStakingMessage(StakingMessage stakingMessage) {
                a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> a1Var = this.stakingMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(stakingMessage);
                    this.messageOneof_ = stakingMessage;
                    onChanged();
                } else {
                    a1Var.j(stakingMessage);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setTransactionMessage(TransactionMessage transactionMessage) {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionMessage);
                    this.messageOneof_ = transactionMessage;
                    onChanged();
                } else {
                    a1Var.j(transactionMessage);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.chainId_ = this.chainId_;
                signingInput.privateKey_ = this.privateKey_;
                if (this.messageOneofCase_ == 3) {
                    a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionMessageBuilder_;
                    if (a1Var == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var.b();
                    }
                }
                if (this.messageOneofCase_ == 4) {
                    a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> a1Var2 = this.stakingMessageBuilder_;
                    if (a1Var2 == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var2.b();
                    }
                }
                signingInput.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.privateKey_ = byteString;
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setStakingMessage(StakingMessage.Builder builder) {
                a1<StakingMessage, StakingMessage.Builder, StakingMessageOrBuilder> a1Var = this.stakingMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setTransactionMessage(TransactionMessage.Builder builder) {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                ByteString chainId = signingInput.getChainId();
                ByteString byteString = ByteString.EMPTY;
                if (chainId != byteString) {
                    setChainId(signingInput.getChainId());
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Harmony$SigningInput$MessageOneofCase[signingInput.getMessageOneofCase().ordinal()];
                if (i == 1) {
                    mergeTransactionMessage(signingInput.getTransactionMessage());
                } else if (i == 2) {
                    mergeStakingMessage(signingInput.getStakingMessage());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.SigningInput.access$1100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$SigningInput r3 = (wallet.core.jni.proto.Harmony.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$SigningInput r4 = (wallet.core.jni.proto.Harmony.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            TRANSACTION_MESSAGE(3),
            STAKING_MESSAGE(4),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 3) {
                        if (i != 4) {
                            return null;
                        }
                        return STAKING_MESSAGE;
                    }
                    return TRANSACTION_MESSAGE;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getChainId().equals(signingInput.getChainId()) && getPrivateKey().equals(signingInput.getPrivateKey()) && getMessageOneofCase().equals(signingInput.getMessageOneofCase())) {
                int i = this.messageOneofCase_;
                if (i != 3) {
                    if (i == 4 && !getStakingMessage().equals(signingInput.getStakingMessage())) {
                        return false;
                    }
                } else if (!getTransactionMessage().equals(signingInput.getTransactionMessage())) {
                    return false;
                }
                return this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public ByteString getChainId() {
            return this.chainId_;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.chainId_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.chainId_);
            if (!this.privateKey_.isEmpty()) {
                h += CodedOutputStream.h(2, this.privateKey_);
            }
            if (this.messageOneofCase_ == 3) {
                h += CodedOutputStream.G(3, (TransactionMessage) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                h += CodedOutputStream.G(4, (StakingMessage) this.messageOneof_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public StakingMessage getStakingMessage() {
            if (this.messageOneofCase_ == 4) {
                return (StakingMessage) this.messageOneof_;
            }
            return StakingMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public StakingMessageOrBuilder getStakingMessageOrBuilder() {
            if (this.messageOneofCase_ == 4) {
                return (StakingMessage) this.messageOneof_;
            }
            return StakingMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public TransactionMessage getTransactionMessage() {
            if (this.messageOneofCase_ == 3) {
                return (TransactionMessage) this.messageOneof_;
            }
            return TransactionMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public TransactionMessageOrBuilder getTransactionMessageOrBuilder() {
            if (this.messageOneofCase_ == 3) {
                return (TransactionMessage) this.messageOneof_;
            }
            return TransactionMessage.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public boolean hasStakingMessage() {
            return this.messageOneofCase_ == 4;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningInputOrBuilder
        public boolean hasTransactionMessage() {
            return this.messageOneofCase_ == 3;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getChainId().hashCode()) * 37) + 2) * 53) + getPrivateKey().hashCode();
            int i3 = this.messageOneofCase_;
            if (i3 == 3) {
                i = ((hashCode2 * 37) + 3) * 53;
                hashCode = getTransactionMessage().hashCode();
            } else {
                if (i3 == 4) {
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getStakingMessage().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.chainId_.isEmpty()) {
                codedOutputStream.q0(1, this.chainId_);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(2, this.privateKey_);
            }
            if (this.messageOneofCase_ == 3) {
                codedOutputStream.K0(3, (TransactionMessage) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                codedOutputStream.K0(4, (StakingMessage) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.chainId_ = byteString;
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.chainId_ = jVar.q();
                            } else if (J != 18) {
                                if (J == 26) {
                                    TransactionMessage.Builder builder = this.messageOneofCase_ == 3 ? ((TransactionMessage) this.messageOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(TransactionMessage.parser(), rVar);
                                    this.messageOneof_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((TransactionMessage) z2);
                                        this.messageOneof_ = builder.buildPartial();
                                    }
                                    this.messageOneofCase_ = 3;
                                } else if (J != 34) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    StakingMessage.Builder builder2 = this.messageOneofCase_ == 4 ? ((StakingMessage) this.messageOneof_).toBuilder() : null;
                                    m0 z3 = jVar.z(StakingMessage.parser(), rVar);
                                    this.messageOneof_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((StakingMessage) z3);
                                        this.messageOneof_ = builder2.buildPartial();
                                    }
                                    this.messageOneofCase_ = 4;
                                }
                            } else {
                                this.privateKey_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getChainId();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        SigningInput.MessageOneofCase getMessageOneofCase();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        StakingMessage getStakingMessage();

        StakingMessageOrBuilder getStakingMessageOrBuilder();

        TransactionMessage getTransactionMessage();

        TransactionMessageOrBuilder getTransactionMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasStakingMessage();

        boolean hasTransactionMessage();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        public static final int R_FIELD_NUMBER = 3;
        public static final int S_FIELD_NUMBER = 4;
        public static final int V_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private ByteString r_;
        private ByteString s_;
        private ByteString v_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Harmony.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;
            private ByteString r_;
            private ByteString s_;
            private ByteString v_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearR() {
                this.r_ = SigningOutput.getDefaultInstance().getR();
                onChanged();
                return this;
            }

            public Builder clearS() {
                this.s_ = SigningOutput.getDefaultInstance().getS();
                onChanged();
                return this;
            }

            public Builder clearV() {
                this.v_ = SigningOutput.getDefaultInstance().getV();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
            public ByteString getR() {
                return this.r_;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
            public ByteString getS() {
                return this.s_;
            }

            @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
            public ByteString getV() {
                return this.v_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setR(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.r_ = byteString;
                onChanged();
                return this;
            }

            public Builder setS(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.s_ = byteString;
                onChanged();
                return this;
            }

            public Builder setV(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.v_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.v_ = byteString;
                this.r_ = byteString;
                this.s_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                signingOutput.v_ = this.v_;
                signingOutput.r_ = this.r_;
                signingOutput.s_ = this.s_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.v_ = byteString;
                this.r_ = byteString;
                this.s_ = byteString;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.v_ = byteString;
                this.r_ = byteString;
                this.s_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                ByteString encoded = signingOutput.getEncoded();
                ByteString byteString = ByteString.EMPTY;
                if (encoded != byteString) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (signingOutput.getV() != byteString) {
                    setV(signingOutput.getV());
                }
                if (signingOutput.getR() != byteString) {
                    setR(signingOutput.getR());
                }
                if (signingOutput.getS() != byteString) {
                    setS(signingOutput.getS());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.SigningOutput.access$2400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$SigningOutput r3 = (wallet.core.jni.proto.Harmony.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$SigningOutput r4 = (wallet.core.jni.proto.Harmony.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && getV().equals(signingOutput.getV()) && getR().equals(signingOutput.getR()) && getS().equals(signingOutput.getS()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
        public ByteString getR() {
            return this.r_;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
        public ByteString getS() {
            return this.s_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_);
            if (!this.v_.isEmpty()) {
                h += CodedOutputStream.h(2, this.v_);
            }
            if (!this.r_.isEmpty()) {
                h += CodedOutputStream.h(3, this.r_);
            }
            if (!this.s_.isEmpty()) {
                h += CodedOutputStream.h(4, this.s_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.SigningOutputOrBuilder
        public ByteString getV() {
            return this.v_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 37) + 2) * 53) + getV().hashCode()) * 37) + 3) * 53) + getR().hashCode()) * 37) + 4) * 53) + getS().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            if (!this.v_.isEmpty()) {
                codedOutputStream.q0(2, this.v_);
            }
            if (!this.r_.isEmpty()) {
                codedOutputStream.q0(3, this.r_);
            }
            if (!this.s_.isEmpty()) {
                codedOutputStream.q0(4, this.s_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.encoded_ = byteString;
            this.v_ = byteString;
            this.r_ = byteString;
            this.s_ = byteString;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.encoded_ = jVar.q();
                            } else if (J == 18) {
                                this.v_ = jVar.q();
                            } else if (J == 26) {
                                this.r_ = jVar.q();
                            } else if (J != 34) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.s_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getR();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getS();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        ByteString getV();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class StakingMessage extends GeneratedMessageV3 implements StakingMessageOrBuilder {
        public static final int COLLECT_REWARDS_FIELD_NUMBER = 5;
        public static final int CREATE_VALIDATOR_MESSAGE_FIELD_NUMBER = 1;
        public static final int DELEGATE_MESSAGE_FIELD_NUMBER = 3;
        public static final int EDIT_VALIDATOR_MESSAGE_FIELD_NUMBER = 2;
        public static final int GAS_LIMIT_FIELD_NUMBER = 8;
        public static final int GAS_PRICE_FIELD_NUMBER = 7;
        public static final int NONCE_FIELD_NUMBER = 6;
        public static final int UNDELEGATE_MESSAGE_FIELD_NUMBER = 4;
        private static final long serialVersionUID = 0;
        private ByteString gasLimit_;
        private ByteString gasPrice_;
        private byte memoizedIsInitialized;
        private ByteString nonce_;
        private int stakeMsgCase_;
        private Object stakeMsg_;
        private static final StakingMessage DEFAULT_INSTANCE = new StakingMessage();
        private static final t0<StakingMessage> PARSER = new c<StakingMessage>() { // from class: wallet.core.jni.proto.Harmony.StakingMessage.1
            @Override // com.google.protobuf.t0
            public StakingMessage parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new StakingMessage(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements StakingMessageOrBuilder {
            private a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> collectRewardsBuilder_;
            private a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> createValidatorMessageBuilder_;
            private a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> delegateMessageBuilder_;
            private a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> editValidatorMessageBuilder_;
            private ByteString gasLimit_;
            private ByteString gasPrice_;
            private ByteString nonce_;
            private int stakeMsgCase_;
            private Object stakeMsg_;
            private a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> undelegateMessageBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> getCollectRewardsFieldBuilder() {
                if (this.collectRewardsBuilder_ == null) {
                    if (this.stakeMsgCase_ != 5) {
                        this.stakeMsg_ = DirectiveCollectRewards.getDefaultInstance();
                    }
                    this.collectRewardsBuilder_ = new a1<>((DirectiveCollectRewards) this.stakeMsg_, getParentForChildren(), isClean());
                    this.stakeMsg_ = null;
                }
                this.stakeMsgCase_ = 5;
                onChanged();
                return this.collectRewardsBuilder_;
            }

            private a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> getCreateValidatorMessageFieldBuilder() {
                if (this.createValidatorMessageBuilder_ == null) {
                    if (this.stakeMsgCase_ != 1) {
                        this.stakeMsg_ = DirectiveCreateValidator.getDefaultInstance();
                    }
                    this.createValidatorMessageBuilder_ = new a1<>((DirectiveCreateValidator) this.stakeMsg_, getParentForChildren(), isClean());
                    this.stakeMsg_ = null;
                }
                this.stakeMsgCase_ = 1;
                onChanged();
                return this.createValidatorMessageBuilder_;
            }

            private a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> getDelegateMessageFieldBuilder() {
                if (this.delegateMessageBuilder_ == null) {
                    if (this.stakeMsgCase_ != 3) {
                        this.stakeMsg_ = DirectiveDelegate.getDefaultInstance();
                    }
                    this.delegateMessageBuilder_ = new a1<>((DirectiveDelegate) this.stakeMsg_, getParentForChildren(), isClean());
                    this.stakeMsg_ = null;
                }
                this.stakeMsgCase_ = 3;
                onChanged();
                return this.delegateMessageBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_StakingMessage_descriptor;
            }

            private a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> getEditValidatorMessageFieldBuilder() {
                if (this.editValidatorMessageBuilder_ == null) {
                    if (this.stakeMsgCase_ != 2) {
                        this.stakeMsg_ = DirectiveEditValidator.getDefaultInstance();
                    }
                    this.editValidatorMessageBuilder_ = new a1<>((DirectiveEditValidator) this.stakeMsg_, getParentForChildren(), isClean());
                    this.stakeMsg_ = null;
                }
                this.stakeMsgCase_ = 2;
                onChanged();
                return this.editValidatorMessageBuilder_;
            }

            private a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> getUndelegateMessageFieldBuilder() {
                if (this.undelegateMessageBuilder_ == null) {
                    if (this.stakeMsgCase_ != 4) {
                        this.stakeMsg_ = DirectiveUndelegate.getDefaultInstance();
                    }
                    this.undelegateMessageBuilder_ = new a1<>((DirectiveUndelegate) this.stakeMsg_, getParentForChildren(), isClean());
                    this.stakeMsg_ = null;
                }
                this.stakeMsgCase_ = 4;
                onChanged();
                return this.undelegateMessageBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCollectRewards() {
                a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> a1Var = this.collectRewardsBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 5) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                        onChanged();
                    }
                } else {
                    if (this.stakeMsgCase_ == 5) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearCreateValidatorMessage() {
                a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> a1Var = this.createValidatorMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 1) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                        onChanged();
                    }
                } else {
                    if (this.stakeMsgCase_ == 1) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearDelegateMessage() {
                a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> a1Var = this.delegateMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 3) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                        onChanged();
                    }
                } else {
                    if (this.stakeMsgCase_ == 3) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearEditValidatorMessage() {
                a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> a1Var = this.editValidatorMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 2) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                        onChanged();
                    }
                } else {
                    if (this.stakeMsgCase_ == 2) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = StakingMessage.getDefaultInstance().getGasLimit();
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = StakingMessage.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = StakingMessage.getDefaultInstance().getNonce();
                onChanged();
                return this;
            }

            public Builder clearStakeMsg() {
                this.stakeMsgCase_ = 0;
                this.stakeMsg_ = null;
                onChanged();
                return this;
            }

            public Builder clearUndelegateMessage() {
                a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> a1Var = this.undelegateMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 4) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                        onChanged();
                    }
                } else {
                    if (this.stakeMsgCase_ == 4) {
                        this.stakeMsgCase_ = 0;
                        this.stakeMsg_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveCollectRewards getCollectRewards() {
                a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> a1Var = this.collectRewardsBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 5) {
                        return (DirectiveCollectRewards) this.stakeMsg_;
                    }
                    return DirectiveCollectRewards.getDefaultInstance();
                } else if (this.stakeMsgCase_ == 5) {
                    return a1Var.f();
                } else {
                    return DirectiveCollectRewards.getDefaultInstance();
                }
            }

            public DirectiveCollectRewards.Builder getCollectRewardsBuilder() {
                return getCollectRewardsFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveCollectRewardsOrBuilder getCollectRewardsOrBuilder() {
                a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> a1Var;
                int i = this.stakeMsgCase_;
                if (i != 5 || (a1Var = this.collectRewardsBuilder_) == null) {
                    if (i == 5) {
                        return (DirectiveCollectRewards) this.stakeMsg_;
                    }
                    return DirectiveCollectRewards.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveCreateValidator getCreateValidatorMessage() {
                a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> a1Var = this.createValidatorMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 1) {
                        return (DirectiveCreateValidator) this.stakeMsg_;
                    }
                    return DirectiveCreateValidator.getDefaultInstance();
                } else if (this.stakeMsgCase_ == 1) {
                    return a1Var.f();
                } else {
                    return DirectiveCreateValidator.getDefaultInstance();
                }
            }

            public DirectiveCreateValidator.Builder getCreateValidatorMessageBuilder() {
                return getCreateValidatorMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveCreateValidatorOrBuilder getCreateValidatorMessageOrBuilder() {
                a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> a1Var;
                int i = this.stakeMsgCase_;
                if (i != 1 || (a1Var = this.createValidatorMessageBuilder_) == null) {
                    if (i == 1) {
                        return (DirectiveCreateValidator) this.stakeMsg_;
                    }
                    return DirectiveCreateValidator.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveDelegate getDelegateMessage() {
                a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> a1Var = this.delegateMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 3) {
                        return (DirectiveDelegate) this.stakeMsg_;
                    }
                    return DirectiveDelegate.getDefaultInstance();
                } else if (this.stakeMsgCase_ == 3) {
                    return a1Var.f();
                } else {
                    return DirectiveDelegate.getDefaultInstance();
                }
            }

            public DirectiveDelegate.Builder getDelegateMessageBuilder() {
                return getDelegateMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveDelegateOrBuilder getDelegateMessageOrBuilder() {
                a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> a1Var;
                int i = this.stakeMsgCase_;
                if (i != 3 || (a1Var = this.delegateMessageBuilder_) == null) {
                    if (i == 3) {
                        return (DirectiveDelegate) this.stakeMsg_;
                    }
                    return DirectiveDelegate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_StakingMessage_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveEditValidator getEditValidatorMessage() {
                a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> a1Var = this.editValidatorMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 2) {
                        return (DirectiveEditValidator) this.stakeMsg_;
                    }
                    return DirectiveEditValidator.getDefaultInstance();
                } else if (this.stakeMsgCase_ == 2) {
                    return a1Var.f();
                } else {
                    return DirectiveEditValidator.getDefaultInstance();
                }
            }

            public DirectiveEditValidator.Builder getEditValidatorMessageBuilder() {
                return getEditValidatorMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveEditValidatorOrBuilder getEditValidatorMessageOrBuilder() {
                a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> a1Var;
                int i = this.stakeMsgCase_;
                if (i != 2 || (a1Var = this.editValidatorMessageBuilder_) == null) {
                    if (i == 2) {
                        return (DirectiveEditValidator) this.stakeMsg_;
                    }
                    return DirectiveEditValidator.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public ByteString getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public ByteString getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public ByteString getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public StakeMsgCase getStakeMsgCase() {
                return StakeMsgCase.forNumber(this.stakeMsgCase_);
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveUndelegate getUndelegateMessage() {
                a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> a1Var = this.undelegateMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 4) {
                        return (DirectiveUndelegate) this.stakeMsg_;
                    }
                    return DirectiveUndelegate.getDefaultInstance();
                } else if (this.stakeMsgCase_ == 4) {
                    return a1Var.f();
                } else {
                    return DirectiveUndelegate.getDefaultInstance();
                }
            }

            public DirectiveUndelegate.Builder getUndelegateMessageBuilder() {
                return getUndelegateMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public DirectiveUndelegateOrBuilder getUndelegateMessageOrBuilder() {
                a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> a1Var;
                int i = this.stakeMsgCase_;
                if (i != 4 || (a1Var = this.undelegateMessageBuilder_) == null) {
                    if (i == 4) {
                        return (DirectiveUndelegate) this.stakeMsg_;
                    }
                    return DirectiveUndelegate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public boolean hasCollectRewards() {
                return this.stakeMsgCase_ == 5;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public boolean hasCreateValidatorMessage() {
                return this.stakeMsgCase_ == 1;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public boolean hasDelegateMessage() {
                return this.stakeMsgCase_ == 3;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public boolean hasEditValidatorMessage() {
                return this.stakeMsgCase_ == 2;
            }

            @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
            public boolean hasUndelegateMessage() {
                return this.stakeMsgCase_ == 4;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_StakingMessage_fieldAccessorTable.d(StakingMessage.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCollectRewards(DirectiveCollectRewards directiveCollectRewards) {
                a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> a1Var = this.collectRewardsBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 5 && this.stakeMsg_ != DirectiveCollectRewards.getDefaultInstance()) {
                        this.stakeMsg_ = DirectiveCollectRewards.newBuilder((DirectiveCollectRewards) this.stakeMsg_).mergeFrom(directiveCollectRewards).buildPartial();
                    } else {
                        this.stakeMsg_ = directiveCollectRewards;
                    }
                    onChanged();
                } else {
                    if (this.stakeMsgCase_ == 5) {
                        a1Var.h(directiveCollectRewards);
                    }
                    this.collectRewardsBuilder_.j(directiveCollectRewards);
                }
                this.stakeMsgCase_ = 5;
                return this;
            }

            public Builder mergeCreateValidatorMessage(DirectiveCreateValidator directiveCreateValidator) {
                a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> a1Var = this.createValidatorMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 1 && this.stakeMsg_ != DirectiveCreateValidator.getDefaultInstance()) {
                        this.stakeMsg_ = DirectiveCreateValidator.newBuilder((DirectiveCreateValidator) this.stakeMsg_).mergeFrom(directiveCreateValidator).buildPartial();
                    } else {
                        this.stakeMsg_ = directiveCreateValidator;
                    }
                    onChanged();
                } else {
                    if (this.stakeMsgCase_ == 1) {
                        a1Var.h(directiveCreateValidator);
                    }
                    this.createValidatorMessageBuilder_.j(directiveCreateValidator);
                }
                this.stakeMsgCase_ = 1;
                return this;
            }

            public Builder mergeDelegateMessage(DirectiveDelegate directiveDelegate) {
                a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> a1Var = this.delegateMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 3 && this.stakeMsg_ != DirectiveDelegate.getDefaultInstance()) {
                        this.stakeMsg_ = DirectiveDelegate.newBuilder((DirectiveDelegate) this.stakeMsg_).mergeFrom(directiveDelegate).buildPartial();
                    } else {
                        this.stakeMsg_ = directiveDelegate;
                    }
                    onChanged();
                } else {
                    if (this.stakeMsgCase_ == 3) {
                        a1Var.h(directiveDelegate);
                    }
                    this.delegateMessageBuilder_.j(directiveDelegate);
                }
                this.stakeMsgCase_ = 3;
                return this;
            }

            public Builder mergeEditValidatorMessage(DirectiveEditValidator directiveEditValidator) {
                a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> a1Var = this.editValidatorMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 2 && this.stakeMsg_ != DirectiveEditValidator.getDefaultInstance()) {
                        this.stakeMsg_ = DirectiveEditValidator.newBuilder((DirectiveEditValidator) this.stakeMsg_).mergeFrom(directiveEditValidator).buildPartial();
                    } else {
                        this.stakeMsg_ = directiveEditValidator;
                    }
                    onChanged();
                } else {
                    if (this.stakeMsgCase_ == 2) {
                        a1Var.h(directiveEditValidator);
                    }
                    this.editValidatorMessageBuilder_.j(directiveEditValidator);
                }
                this.stakeMsgCase_ = 2;
                return this;
            }

            public Builder mergeUndelegateMessage(DirectiveUndelegate directiveUndelegate) {
                a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> a1Var = this.undelegateMessageBuilder_;
                if (a1Var == null) {
                    if (this.stakeMsgCase_ == 4 && this.stakeMsg_ != DirectiveUndelegate.getDefaultInstance()) {
                        this.stakeMsg_ = DirectiveUndelegate.newBuilder((DirectiveUndelegate) this.stakeMsg_).mergeFrom(directiveUndelegate).buildPartial();
                    } else {
                        this.stakeMsg_ = directiveUndelegate;
                    }
                    onChanged();
                } else {
                    if (this.stakeMsgCase_ == 4) {
                        a1Var.h(directiveUndelegate);
                    }
                    this.undelegateMessageBuilder_.j(directiveUndelegate);
                }
                this.stakeMsgCase_ = 4;
                return this;
            }

            public Builder setCollectRewards(DirectiveCollectRewards directiveCollectRewards) {
                a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> a1Var = this.collectRewardsBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(directiveCollectRewards);
                    this.stakeMsg_ = directiveCollectRewards;
                    onChanged();
                } else {
                    a1Var.j(directiveCollectRewards);
                }
                this.stakeMsgCase_ = 5;
                return this;
            }

            public Builder setCreateValidatorMessage(DirectiveCreateValidator directiveCreateValidator) {
                a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> a1Var = this.createValidatorMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(directiveCreateValidator);
                    this.stakeMsg_ = directiveCreateValidator;
                    onChanged();
                } else {
                    a1Var.j(directiveCreateValidator);
                }
                this.stakeMsgCase_ = 1;
                return this;
            }

            public Builder setDelegateMessage(DirectiveDelegate directiveDelegate) {
                a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> a1Var = this.delegateMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(directiveDelegate);
                    this.stakeMsg_ = directiveDelegate;
                    onChanged();
                } else {
                    a1Var.j(directiveDelegate);
                }
                this.stakeMsgCase_ = 3;
                return this;
            }

            public Builder setEditValidatorMessage(DirectiveEditValidator directiveEditValidator) {
                a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> a1Var = this.editValidatorMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(directiveEditValidator);
                    this.stakeMsg_ = directiveEditValidator;
                    onChanged();
                } else {
                    a1Var.j(directiveEditValidator);
                }
                this.stakeMsgCase_ = 2;
                return this;
            }

            public Builder setGasLimit(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasLimit_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasPrice(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.nonce_ = byteString;
                onChanged();
                return this;
            }

            public Builder setUndelegateMessage(DirectiveUndelegate directiveUndelegate) {
                a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> a1Var = this.undelegateMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(directiveUndelegate);
                    this.stakeMsg_ = directiveUndelegate;
                    onChanged();
                } else {
                    a1Var.j(directiveUndelegate);
                }
                this.stakeMsgCase_ = 4;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.stakeMsgCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public StakingMessage build() {
                StakingMessage buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public StakingMessage buildPartial() {
                StakingMessage stakingMessage = new StakingMessage(this, (AnonymousClass1) null);
                if (this.stakeMsgCase_ == 1) {
                    a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> a1Var = this.createValidatorMessageBuilder_;
                    if (a1Var == null) {
                        stakingMessage.stakeMsg_ = this.stakeMsg_;
                    } else {
                        stakingMessage.stakeMsg_ = a1Var.b();
                    }
                }
                if (this.stakeMsgCase_ == 2) {
                    a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> a1Var2 = this.editValidatorMessageBuilder_;
                    if (a1Var2 == null) {
                        stakingMessage.stakeMsg_ = this.stakeMsg_;
                    } else {
                        stakingMessage.stakeMsg_ = a1Var2.b();
                    }
                }
                if (this.stakeMsgCase_ == 3) {
                    a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> a1Var3 = this.delegateMessageBuilder_;
                    if (a1Var3 == null) {
                        stakingMessage.stakeMsg_ = this.stakeMsg_;
                    } else {
                        stakingMessage.stakeMsg_ = a1Var3.b();
                    }
                }
                if (this.stakeMsgCase_ == 4) {
                    a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> a1Var4 = this.undelegateMessageBuilder_;
                    if (a1Var4 == null) {
                        stakingMessage.stakeMsg_ = this.stakeMsg_;
                    } else {
                        stakingMessage.stakeMsg_ = a1Var4.b();
                    }
                }
                if (this.stakeMsgCase_ == 5) {
                    a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> a1Var5 = this.collectRewardsBuilder_;
                    if (a1Var5 == null) {
                        stakingMessage.stakeMsg_ = this.stakeMsg_;
                    } else {
                        stakingMessage.stakeMsg_ = a1Var5.b();
                    }
                }
                stakingMessage.nonce_ = this.nonce_;
                stakingMessage.gasPrice_ = this.gasPrice_;
                stakingMessage.gasLimit_ = this.gasLimit_;
                stakingMessage.stakeMsgCase_ = this.stakeMsgCase_;
                onBuilt();
                return stakingMessage;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public StakingMessage getDefaultInstanceForType() {
                return StakingMessage.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.stakeMsgCase_ = 0;
                this.stakeMsg_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof StakingMessage) {
                    return mergeFrom((StakingMessage) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setCollectRewards(DirectiveCollectRewards.Builder builder) {
                a1<DirectiveCollectRewards, DirectiveCollectRewards.Builder, DirectiveCollectRewardsOrBuilder> a1Var = this.collectRewardsBuilder_;
                if (a1Var == null) {
                    this.stakeMsg_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.stakeMsgCase_ = 5;
                return this;
            }

            public Builder setCreateValidatorMessage(DirectiveCreateValidator.Builder builder) {
                a1<DirectiveCreateValidator, DirectiveCreateValidator.Builder, DirectiveCreateValidatorOrBuilder> a1Var = this.createValidatorMessageBuilder_;
                if (a1Var == null) {
                    this.stakeMsg_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.stakeMsgCase_ = 1;
                return this;
            }

            public Builder setDelegateMessage(DirectiveDelegate.Builder builder) {
                a1<DirectiveDelegate, DirectiveDelegate.Builder, DirectiveDelegateOrBuilder> a1Var = this.delegateMessageBuilder_;
                if (a1Var == null) {
                    this.stakeMsg_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.stakeMsgCase_ = 3;
                return this;
            }

            public Builder setEditValidatorMessage(DirectiveEditValidator.Builder builder) {
                a1<DirectiveEditValidator, DirectiveEditValidator.Builder, DirectiveEditValidatorOrBuilder> a1Var = this.editValidatorMessageBuilder_;
                if (a1Var == null) {
                    this.stakeMsg_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.stakeMsgCase_ = 2;
                return this;
            }

            public Builder setUndelegateMessage(DirectiveUndelegate.Builder builder) {
                a1<DirectiveUndelegate, DirectiveUndelegate.Builder, DirectiveUndelegateOrBuilder> a1Var = this.undelegateMessageBuilder_;
                if (a1Var == null) {
                    this.stakeMsg_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.stakeMsgCase_ = 4;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.stakeMsgCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(StakingMessage stakingMessage) {
                if (stakingMessage == StakingMessage.getDefaultInstance()) {
                    return this;
                }
                ByteString nonce = stakingMessage.getNonce();
                ByteString byteString = ByteString.EMPTY;
                if (nonce != byteString) {
                    setNonce(stakingMessage.getNonce());
                }
                if (stakingMessage.getGasPrice() != byteString) {
                    setGasPrice(stakingMessage.getGasPrice());
                }
                if (stakingMessage.getGasLimit() != byteString) {
                    setGasLimit(stakingMessage.getGasLimit());
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Harmony$StakingMessage$StakeMsgCase[stakingMessage.getStakeMsgCase().ordinal()];
                if (i == 1) {
                    mergeCreateValidatorMessage(stakingMessage.getCreateValidatorMessage());
                } else if (i == 2) {
                    mergeEditValidatorMessage(stakingMessage.getEditValidatorMessage());
                } else if (i == 3) {
                    mergeDelegateMessage(stakingMessage.getDelegateMessage());
                } else if (i == 4) {
                    mergeUndelegateMessage(stakingMessage.getUndelegateMessage());
                } else if (i == 5) {
                    mergeCollectRewards(stakingMessage.getCollectRewards());
                }
                mergeUnknownFields(stakingMessage.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.StakingMessage.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.StakingMessage.access$5600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$StakingMessage r3 = (wallet.core.jni.proto.Harmony.StakingMessage) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$StakingMessage r4 = (wallet.core.jni.proto.Harmony.StakingMessage) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.StakingMessage.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$StakingMessage$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum StakeMsgCase implements a0.c {
            CREATE_VALIDATOR_MESSAGE(1),
            EDIT_VALIDATOR_MESSAGE(2),
            DELEGATE_MESSAGE(3),
            UNDELEGATE_MESSAGE(4),
            COLLECT_REWARDS(5),
            STAKEMSG_NOT_SET(0);
            
            private final int value;

            StakeMsgCase(int i) {
                this.value = i;
            }

            public static StakeMsgCase forNumber(int i) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 3) {
                                if (i != 4) {
                                    if (i != 5) {
                                        return null;
                                    }
                                    return COLLECT_REWARDS;
                                }
                                return UNDELEGATE_MESSAGE;
                            }
                            return DELEGATE_MESSAGE;
                        }
                        return EDIT_VALIDATOR_MESSAGE;
                    }
                    return CREATE_VALIDATOR_MESSAGE;
                }
                return STAKEMSG_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static StakeMsgCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ StakingMessage(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static StakingMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_StakingMessage_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static StakingMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (StakingMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static StakingMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<StakingMessage> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof StakingMessage)) {
                return super.equals(obj);
            }
            StakingMessage stakingMessage = (StakingMessage) obj;
            if (getNonce().equals(stakingMessage.getNonce()) && getGasPrice().equals(stakingMessage.getGasPrice()) && getGasLimit().equals(stakingMessage.getGasLimit()) && getStakeMsgCase().equals(stakingMessage.getStakeMsgCase())) {
                int i = this.stakeMsgCase_;
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                if (i == 5 && !getCollectRewards().equals(stakingMessage.getCollectRewards())) {
                                    return false;
                                }
                            } else if (!getUndelegateMessage().equals(stakingMessage.getUndelegateMessage())) {
                                return false;
                            }
                        } else if (!getDelegateMessage().equals(stakingMessage.getDelegateMessage())) {
                            return false;
                        }
                    } else if (!getEditValidatorMessage().equals(stakingMessage.getEditValidatorMessage())) {
                        return false;
                    }
                } else if (!getCreateValidatorMessage().equals(stakingMessage.getCreateValidatorMessage())) {
                    return false;
                }
                return this.unknownFields.equals(stakingMessage.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveCollectRewards getCollectRewards() {
            if (this.stakeMsgCase_ == 5) {
                return (DirectiveCollectRewards) this.stakeMsg_;
            }
            return DirectiveCollectRewards.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveCollectRewardsOrBuilder getCollectRewardsOrBuilder() {
            if (this.stakeMsgCase_ == 5) {
                return (DirectiveCollectRewards) this.stakeMsg_;
            }
            return DirectiveCollectRewards.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveCreateValidator getCreateValidatorMessage() {
            if (this.stakeMsgCase_ == 1) {
                return (DirectiveCreateValidator) this.stakeMsg_;
            }
            return DirectiveCreateValidator.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveCreateValidatorOrBuilder getCreateValidatorMessageOrBuilder() {
            if (this.stakeMsgCase_ == 1) {
                return (DirectiveCreateValidator) this.stakeMsg_;
            }
            return DirectiveCreateValidator.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveDelegate getDelegateMessage() {
            if (this.stakeMsgCase_ == 3) {
                return (DirectiveDelegate) this.stakeMsg_;
            }
            return DirectiveDelegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveDelegateOrBuilder getDelegateMessageOrBuilder() {
            if (this.stakeMsgCase_ == 3) {
                return (DirectiveDelegate) this.stakeMsg_;
            }
            return DirectiveDelegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveEditValidator getEditValidatorMessage() {
            if (this.stakeMsgCase_ == 2) {
                return (DirectiveEditValidator) this.stakeMsg_;
            }
            return DirectiveEditValidator.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveEditValidatorOrBuilder getEditValidatorMessageOrBuilder() {
            if (this.stakeMsgCase_ == 2) {
                return (DirectiveEditValidator) this.stakeMsg_;
            }
            return DirectiveEditValidator.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public ByteString getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public ByteString getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public ByteString getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<StakingMessage> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.stakeMsgCase_ == 1 ? 0 + CodedOutputStream.G(1, (DirectiveCreateValidator) this.stakeMsg_) : 0;
            if (this.stakeMsgCase_ == 2) {
                G += CodedOutputStream.G(2, (DirectiveEditValidator) this.stakeMsg_);
            }
            if (this.stakeMsgCase_ == 3) {
                G += CodedOutputStream.G(3, (DirectiveDelegate) this.stakeMsg_);
            }
            if (this.stakeMsgCase_ == 4) {
                G += CodedOutputStream.G(4, (DirectiveUndelegate) this.stakeMsg_);
            }
            if (this.stakeMsgCase_ == 5) {
                G += CodedOutputStream.G(5, (DirectiveCollectRewards) this.stakeMsg_);
            }
            if (!this.nonce_.isEmpty()) {
                G += CodedOutputStream.h(6, this.nonce_);
            }
            if (!this.gasPrice_.isEmpty()) {
                G += CodedOutputStream.h(7, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                G += CodedOutputStream.h(8, this.gasLimit_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public StakeMsgCase getStakeMsgCase() {
            return StakeMsgCase.forNumber(this.stakeMsgCase_);
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveUndelegate getUndelegateMessage() {
            if (this.stakeMsgCase_ == 4) {
                return (DirectiveUndelegate) this.stakeMsg_;
            }
            return DirectiveUndelegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public DirectiveUndelegateOrBuilder getUndelegateMessageOrBuilder() {
            if (this.stakeMsgCase_ == 4) {
                return (DirectiveUndelegate) this.stakeMsg_;
            }
            return DirectiveUndelegate.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public boolean hasCollectRewards() {
            return this.stakeMsgCase_ == 5;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public boolean hasCreateValidatorMessage() {
            return this.stakeMsgCase_ == 1;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public boolean hasDelegateMessage() {
            return this.stakeMsgCase_ == 3;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public boolean hasEditValidatorMessage() {
            return this.stakeMsgCase_ == 2;
        }

        @Override // wallet.core.jni.proto.Harmony.StakingMessageOrBuilder
        public boolean hasUndelegateMessage() {
            return this.stakeMsgCase_ == 4;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((((((779 + getDescriptor().hashCode()) * 37) + 6) * 53) + getNonce().hashCode()) * 37) + 7) * 53) + getGasPrice().hashCode()) * 37) + 8) * 53) + getGasLimit().hashCode();
            int i3 = this.stakeMsgCase_;
            if (i3 == 1) {
                i = ((hashCode2 * 37) + 1) * 53;
                hashCode = getCreateValidatorMessage().hashCode();
            } else if (i3 == 2) {
                i = ((hashCode2 * 37) + 2) * 53;
                hashCode = getEditValidatorMessage().hashCode();
            } else if (i3 == 3) {
                i = ((hashCode2 * 37) + 3) * 53;
                hashCode = getDelegateMessage().hashCode();
            } else if (i3 == 4) {
                i = ((hashCode2 * 37) + 4) * 53;
                hashCode = getUndelegateMessage().hashCode();
            } else {
                if (i3 == 5) {
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getCollectRewards().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_StakingMessage_fieldAccessorTable.d(StakingMessage.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new StakingMessage();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.stakeMsgCase_ == 1) {
                codedOutputStream.K0(1, (DirectiveCreateValidator) this.stakeMsg_);
            }
            if (this.stakeMsgCase_ == 2) {
                codedOutputStream.K0(2, (DirectiveEditValidator) this.stakeMsg_);
            }
            if (this.stakeMsgCase_ == 3) {
                codedOutputStream.K0(3, (DirectiveDelegate) this.stakeMsg_);
            }
            if (this.stakeMsgCase_ == 4) {
                codedOutputStream.K0(4, (DirectiveUndelegate) this.stakeMsg_);
            }
            if (this.stakeMsgCase_ == 5) {
                codedOutputStream.K0(5, (DirectiveCollectRewards) this.stakeMsg_);
            }
            if (!this.nonce_.isEmpty()) {
                codedOutputStream.q0(6, this.nonce_);
            }
            if (!this.gasPrice_.isEmpty()) {
                codedOutputStream.q0(7, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                codedOutputStream.q0(8, this.gasLimit_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ StakingMessage(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(StakingMessage stakingMessage) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(stakingMessage);
        }

        public static StakingMessage parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private StakingMessage(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.stakeMsgCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static StakingMessage parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (StakingMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static StakingMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public StakingMessage getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static StakingMessage parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static StakingMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private StakingMessage() {
            this.stakeMsgCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.nonce_ = byteString;
            this.gasPrice_ = byteString;
            this.gasLimit_ = byteString;
        }

        public static StakingMessage parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static StakingMessage parseFrom(InputStream inputStream) throws IOException {
            return (StakingMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static StakingMessage parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (StakingMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static StakingMessage parseFrom(j jVar) throws IOException {
            return (StakingMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private StakingMessage(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                DirectiveCreateValidator.Builder builder = this.stakeMsgCase_ == 1 ? ((DirectiveCreateValidator) this.stakeMsg_).toBuilder() : null;
                                m0 z2 = jVar.z(DirectiveCreateValidator.parser(), rVar);
                                this.stakeMsg_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((DirectiveCreateValidator) z2);
                                    this.stakeMsg_ = builder.buildPartial();
                                }
                                this.stakeMsgCase_ = 1;
                            } else if (J == 18) {
                                DirectiveEditValidator.Builder builder2 = this.stakeMsgCase_ == 2 ? ((DirectiveEditValidator) this.stakeMsg_).toBuilder() : null;
                                m0 z3 = jVar.z(DirectiveEditValidator.parser(), rVar);
                                this.stakeMsg_ = z3;
                                if (builder2 != null) {
                                    builder2.mergeFrom((DirectiveEditValidator) z3);
                                    this.stakeMsg_ = builder2.buildPartial();
                                }
                                this.stakeMsgCase_ = 2;
                            } else if (J == 26) {
                                DirectiveDelegate.Builder builder3 = this.stakeMsgCase_ == 3 ? ((DirectiveDelegate) this.stakeMsg_).toBuilder() : null;
                                m0 z4 = jVar.z(DirectiveDelegate.parser(), rVar);
                                this.stakeMsg_ = z4;
                                if (builder3 != null) {
                                    builder3.mergeFrom((DirectiveDelegate) z4);
                                    this.stakeMsg_ = builder3.buildPartial();
                                }
                                this.stakeMsgCase_ = 3;
                            } else if (J == 34) {
                                DirectiveUndelegate.Builder builder4 = this.stakeMsgCase_ == 4 ? ((DirectiveUndelegate) this.stakeMsg_).toBuilder() : null;
                                m0 z5 = jVar.z(DirectiveUndelegate.parser(), rVar);
                                this.stakeMsg_ = z5;
                                if (builder4 != null) {
                                    builder4.mergeFrom((DirectiveUndelegate) z5);
                                    this.stakeMsg_ = builder4.buildPartial();
                                }
                                this.stakeMsgCase_ = 4;
                            } else if (J == 42) {
                                DirectiveCollectRewards.Builder builder5 = this.stakeMsgCase_ == 5 ? ((DirectiveCollectRewards) this.stakeMsg_).toBuilder() : null;
                                m0 z6 = jVar.z(DirectiveCollectRewards.parser(), rVar);
                                this.stakeMsg_ = z6;
                                if (builder5 != null) {
                                    builder5.mergeFrom((DirectiveCollectRewards) z6);
                                    this.stakeMsg_ = builder5.buildPartial();
                                }
                                this.stakeMsgCase_ = 5;
                            } else if (J == 50) {
                                this.nonce_ = jVar.q();
                            } else if (J == 58) {
                                this.gasPrice_ = jVar.q();
                            } else if (J != 66) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.gasLimit_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static StakingMessage parseFrom(j jVar, r rVar) throws IOException {
            return (StakingMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface StakingMessageOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        DirectiveCollectRewards getCollectRewards();

        DirectiveCollectRewardsOrBuilder getCollectRewardsOrBuilder();

        DirectiveCreateValidator getCreateValidatorMessage();

        DirectiveCreateValidatorOrBuilder getCreateValidatorMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        DirectiveDelegate getDelegateMessage();

        DirectiveDelegateOrBuilder getDelegateMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        DirectiveEditValidator getEditValidatorMessage();

        DirectiveEditValidatorOrBuilder getEditValidatorMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getGasLimit();

        ByteString getGasPrice();

        /* synthetic */ String getInitializationErrorString();

        ByteString getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        StakingMessage.StakeMsgCase getStakeMsgCase();

        DirectiveUndelegate getUndelegateMessage();

        DirectiveUndelegateOrBuilder getUndelegateMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasCollectRewards();

        boolean hasCreateValidatorMessage();

        boolean hasDelegateMessage();

        boolean hasEditValidatorMessage();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasUndelegateMessage();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionMessage extends GeneratedMessageV3 implements TransactionMessageOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 5;
        public static final int FROM_SHARD_ID_FIELD_NUMBER = 7;
        public static final int GAS_LIMIT_FIELD_NUMBER = 3;
        public static final int GAS_PRICE_FIELD_NUMBER = 2;
        public static final int NONCE_FIELD_NUMBER = 1;
        public static final int PAYLOAD_FIELD_NUMBER = 6;
        public static final int TO_ADDRESS_FIELD_NUMBER = 4;
        public static final int TO_SHARD_ID_FIELD_NUMBER = 8;
        private static final long serialVersionUID = 0;
        private ByteString amount_;
        private ByteString fromShardId_;
        private ByteString gasLimit_;
        private ByteString gasPrice_;
        private byte memoizedIsInitialized;
        private ByteString nonce_;
        private ByteString payload_;
        private volatile Object toAddress_;
        private ByteString toShardId_;
        private static final TransactionMessage DEFAULT_INSTANCE = new TransactionMessage();
        private static final t0<TransactionMessage> PARSER = new c<TransactionMessage>() { // from class: wallet.core.jni.proto.Harmony.TransactionMessage.1
            @Override // com.google.protobuf.t0
            public TransactionMessage parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionMessage(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionMessageOrBuilder {
            private ByteString amount_;
            private ByteString fromShardId_;
            private ByteString gasLimit_;
            private ByteString gasPrice_;
            private ByteString nonce_;
            private ByteString payload_;
            private Object toAddress_;
            private ByteString toShardId_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Harmony.internal_static_TW_Harmony_Proto_TransactionMessage_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = TransactionMessage.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearFromShardId() {
                this.fromShardId_ = TransactionMessage.getDefaultInstance().getFromShardId();
                onChanged();
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = TransactionMessage.getDefaultInstance().getGasLimit();
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = TransactionMessage.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = TransactionMessage.getDefaultInstance().getNonce();
                onChanged();
                return this;
            }

            public Builder clearPayload() {
                this.payload_ = TransactionMessage.getDefaultInstance().getPayload();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransactionMessage.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            public Builder clearToShardId() {
                this.toShardId_ = TransactionMessage.getDefaultInstance().getToShardId();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Harmony.internal_static_TW_Harmony_Proto_TransactionMessage_descriptor;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getFromShardId() {
                return this.fromShardId_;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
            public ByteString getToShardId() {
                return this.toShardId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Harmony.internal_static_TW_Harmony_Proto_TransactionMessage_fieldAccessorTable.d(TransactionMessage.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setFromShardId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.fromShardId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasLimit(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasLimit_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasPrice(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.nonce_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPayload(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.payload_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToShardId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.toShardId_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.amount_ = byteString;
                this.payload_ = byteString;
                this.fromShardId_ = byteString;
                this.toShardId_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionMessage build() {
                TransactionMessage buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionMessage buildPartial() {
                TransactionMessage transactionMessage = new TransactionMessage(this, (AnonymousClass1) null);
                transactionMessage.nonce_ = this.nonce_;
                transactionMessage.gasPrice_ = this.gasPrice_;
                transactionMessage.gasLimit_ = this.gasLimit_;
                transactionMessage.toAddress_ = this.toAddress_;
                transactionMessage.amount_ = this.amount_;
                transactionMessage.payload_ = this.payload_;
                transactionMessage.fromShardId_ = this.fromShardId_;
                transactionMessage.toShardId_ = this.toShardId_;
                onBuilt();
                return transactionMessage;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionMessage getDefaultInstanceForType() {
                return TransactionMessage.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.amount_ = byteString;
                this.payload_ = byteString;
                this.fromShardId_ = byteString;
                this.toShardId_ = byteString;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionMessage) {
                    return mergeFrom((TransactionMessage) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionMessage transactionMessage) {
                if (transactionMessage == TransactionMessage.getDefaultInstance()) {
                    return this;
                }
                ByteString nonce = transactionMessage.getNonce();
                ByteString byteString = ByteString.EMPTY;
                if (nonce != byteString) {
                    setNonce(transactionMessage.getNonce());
                }
                if (transactionMessage.getGasPrice() != byteString) {
                    setGasPrice(transactionMessage.getGasPrice());
                }
                if (transactionMessage.getGasLimit() != byteString) {
                    setGasLimit(transactionMessage.getGasLimit());
                }
                if (!transactionMessage.getToAddress().isEmpty()) {
                    this.toAddress_ = transactionMessage.toAddress_;
                    onChanged();
                }
                if (transactionMessage.getAmount() != byteString) {
                    setAmount(transactionMessage.getAmount());
                }
                if (transactionMessage.getPayload() != byteString) {
                    setPayload(transactionMessage.getPayload());
                }
                if (transactionMessage.getFromShardId() != byteString) {
                    setFromShardId(transactionMessage.getFromShardId());
                }
                if (transactionMessage.getToShardId() != byteString) {
                    setToShardId(transactionMessage.getToShardId());
                }
                mergeUnknownFields(transactionMessage.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.amount_ = byteString;
                this.payload_ = byteString;
                this.fromShardId_ = byteString;
                this.toShardId_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Harmony.TransactionMessage.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Harmony.TransactionMessage.access$4100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Harmony$TransactionMessage r3 = (wallet.core.jni.proto.Harmony.TransactionMessage) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Harmony$TransactionMessage r4 = (wallet.core.jni.proto.Harmony.TransactionMessage) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Harmony.TransactionMessage.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Harmony$TransactionMessage$Builder");
            }
        }

        public /* synthetic */ TransactionMessage(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransactionMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Harmony.internal_static_TW_Harmony_Proto_TransactionMessage_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionMessage> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionMessage)) {
                return super.equals(obj);
            }
            TransactionMessage transactionMessage = (TransactionMessage) obj;
            return getNonce().equals(transactionMessage.getNonce()) && getGasPrice().equals(transactionMessage.getGasPrice()) && getGasLimit().equals(transactionMessage.getGasLimit()) && getToAddress().equals(transactionMessage.getToAddress()) && getAmount().equals(transactionMessage.getAmount()) && getPayload().equals(transactionMessage.getPayload()) && getFromShardId().equals(transactionMessage.getFromShardId()) && getToShardId().equals(transactionMessage.getToShardId()) && this.unknownFields.equals(transactionMessage.unknownFields);
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getFromShardId() {
            return this.fromShardId_;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionMessage> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getPayload() {
            return this.payload_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.nonce_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.nonce_);
            if (!this.gasPrice_.isEmpty()) {
                h += CodedOutputStream.h(2, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                h += CodedOutputStream.h(3, this.gasLimit_);
            }
            if (!getToAddressBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(4, this.toAddress_);
            }
            if (!this.amount_.isEmpty()) {
                h += CodedOutputStream.h(5, this.amount_);
            }
            if (!this.payload_.isEmpty()) {
                h += CodedOutputStream.h(6, this.payload_);
            }
            if (!this.fromShardId_.isEmpty()) {
                h += CodedOutputStream.h(7, this.fromShardId_);
            }
            if (!this.toShardId_.isEmpty()) {
                h += CodedOutputStream.h(8, this.toShardId_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Harmony.TransactionMessageOrBuilder
        public ByteString getToShardId() {
            return this.toShardId_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getNonce().hashCode()) * 37) + 2) * 53) + getGasPrice().hashCode()) * 37) + 3) * 53) + getGasLimit().hashCode()) * 37) + 4) * 53) + getToAddress().hashCode()) * 37) + 5) * 53) + getAmount().hashCode()) * 37) + 6) * 53) + getPayload().hashCode()) * 37) + 7) * 53) + getFromShardId().hashCode()) * 37) + 8) * 53) + getToShardId().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Harmony.internal_static_TW_Harmony_Proto_TransactionMessage_fieldAccessorTable.d(TransactionMessage.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionMessage();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.nonce_.isEmpty()) {
                codedOutputStream.q0(1, this.nonce_);
            }
            if (!this.gasPrice_.isEmpty()) {
                codedOutputStream.q0(2, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                codedOutputStream.q0(3, this.gasLimit_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.toAddress_);
            }
            if (!this.amount_.isEmpty()) {
                codedOutputStream.q0(5, this.amount_);
            }
            if (!this.payload_.isEmpty()) {
                codedOutputStream.q0(6, this.payload_);
            }
            if (!this.fromShardId_.isEmpty()) {
                codedOutputStream.q0(7, this.fromShardId_);
            }
            if (!this.toShardId_.isEmpty()) {
                codedOutputStream.q0(8, this.toShardId_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransactionMessage(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransactionMessage transactionMessage) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionMessage);
        }

        public static TransactionMessage parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionMessage(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionMessage parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionMessage getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransactionMessage parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionMessage() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.nonce_ = byteString;
            this.gasPrice_ = byteString;
            this.gasLimit_ = byteString;
            this.toAddress_ = "";
            this.amount_ = byteString;
            this.payload_ = byteString;
            this.fromShardId_ = byteString;
            this.toShardId_ = byteString;
        }

        public static TransactionMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransactionMessage parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionMessage parseFrom(InputStream inputStream) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransactionMessage parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionMessage parseFrom(j jVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionMessage parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private TransactionMessage(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.nonce_ = jVar.q();
                            } else if (J == 18) {
                                this.gasPrice_ = jVar.q();
                            } else if (J == 26) {
                                this.gasLimit_ = jVar.q();
                            } else if (J == 34) {
                                this.toAddress_ = jVar.I();
                            } else if (J == 42) {
                                this.amount_ = jVar.q();
                            } else if (J == 50) {
                                this.payload_ = jVar.q();
                            } else if (J == 58) {
                                this.fromShardId_ = jVar.q();
                            } else if (J != 66) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.toShardId_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionMessageOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getFromShardId();

        ByteString getGasLimit();

        ByteString getGasPrice();

        /* synthetic */ String getInitializationErrorString();

        ByteString getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPayload();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        ByteString getToShardId();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Harmony_Proto_SigningInput_descriptor = bVar;
        internal_static_TW_Harmony_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"ChainId", "PrivateKey", "TransactionMessage", "StakingMessage", "MessageOneof"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Harmony_Proto_SigningOutput_descriptor = bVar2;
        internal_static_TW_Harmony_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Encoded", "V", "R", "S"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Harmony_Proto_TransactionMessage_descriptor = bVar3;
        internal_static_TW_Harmony_Proto_TransactionMessage_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Nonce", "GasPrice", "GasLimit", "ToAddress", "Amount", "Payload", "FromShardId", "ToShardId"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Harmony_Proto_StakingMessage_descriptor = bVar4;
        internal_static_TW_Harmony_Proto_StakingMessage_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"CreateValidatorMessage", "EditValidatorMessage", "DelegateMessage", "UndelegateMessage", "CollectRewards", "Nonce", "GasPrice", "GasLimit", "StakeMsg"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_Harmony_Proto_Description_descriptor = bVar5;
        internal_static_TW_Harmony_Proto_Description_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"Name", "Identity", "Website", "SecurityContact", "Details"});
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_Harmony_Proto_Decimal_descriptor = bVar6;
        internal_static_TW_Harmony_Proto_Decimal_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"Value", "Precision"});
        Descriptors.b bVar7 = getDescriptor().o().get(6);
        internal_static_TW_Harmony_Proto_CommissionRate_descriptor = bVar7;
        internal_static_TW_Harmony_Proto_CommissionRate_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"Rate", "MaxRate", "MaxChangeRate"});
        Descriptors.b bVar8 = getDescriptor().o().get(7);
        internal_static_TW_Harmony_Proto_DirectiveCreateValidator_descriptor = bVar8;
        internal_static_TW_Harmony_Proto_DirectiveCreateValidator_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"ValidatorAddress", "Description", "CommissionRates", "MinSelfDelegation", "MaxTotalDelegation", "SlotPubKeys", "SlotKeySigs", "Amount"});
        Descriptors.b bVar9 = getDescriptor().o().get(8);
        internal_static_TW_Harmony_Proto_DirectiveEditValidator_descriptor = bVar9;
        internal_static_TW_Harmony_Proto_DirectiveEditValidator_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"ValidatorAddress", "Description", "CommissionRate", "MinSelfDelegation", "MaxTotalDelegation", "SlotKeyToRemove", "SlotKeyToAdd", "SlotKeyToAddSig", "Active"});
        Descriptors.b bVar10 = getDescriptor().o().get(9);
        internal_static_TW_Harmony_Proto_DirectiveDelegate_descriptor = bVar10;
        internal_static_TW_Harmony_Proto_DirectiveDelegate_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"DelegatorAddress", "ValidatorAddress", "Amount"});
        Descriptors.b bVar11 = getDescriptor().o().get(10);
        internal_static_TW_Harmony_Proto_DirectiveUndelegate_descriptor = bVar11;
        internal_static_TW_Harmony_Proto_DirectiveUndelegate_fieldAccessorTable = new GeneratedMessageV3.e(bVar11, new String[]{"DelegatorAddress", "ValidatorAddress", "Amount"});
        Descriptors.b bVar12 = getDescriptor().o().get(11);
        internal_static_TW_Harmony_Proto_DirectiveCollectRewards_descriptor = bVar12;
        internal_static_TW_Harmony_Proto_DirectiveCollectRewards_fieldAccessorTable = new GeneratedMessageV3.e(bVar12, new String[]{"DelegatorAddress"});
    }

    private Harmony() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
