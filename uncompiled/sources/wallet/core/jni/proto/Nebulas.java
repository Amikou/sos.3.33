package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Nebulas {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\rNebulas.proto\u0012\u0010TW.Nebulas.Proto\"È\u0001\n\fSigningInput\u0012\u0014\n\ffrom_address\u0018\u0001 \u0001(\t\u0012\u0010\n\bchain_id\u0018\u0002 \u0001(\f\u0012\r\n\u0005nonce\u0018\u0003 \u0001(\f\u0012\u0011\n\tgas_price\u0018\u0004 \u0001(\f\u0012\u0011\n\tgas_limit\u0018\u0005 \u0001(\f\u0012\u0012\n\nto_address\u0018\u0006 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0007 \u0001(\f\u0012\u0011\n\ttimestamp\u0018\b \u0001(\f\u0012\u000f\n\u0007payload\u0018\t \u0001(\t\u0012\u0013\n\u000bprivate_key\u0018\n \u0001(\f\"B\n\rSigningOutput\u0012\u0011\n\talgorithm\u0018\u0001 \u0001(\r\u0012\u0011\n\tsignature\u0018\u0002 \u0001(\f\u0012\u000b\n\u0003raw\u0018\u0003 \u0001(\t\"%\n\u0004Data\u0012\f\n\u0004type\u0018\u0001 \u0001(\t\u0012\u000f\n\u0007payload\u0018\u0002 \u0001(\f\"â\u0001\n\u000eRawTransaction\u0012\f\n\u0004hash\u0018\u0001 \u0001(\f\u0012\f\n\u0004from\u0018\u0002 \u0001(\f\u0012\n\n\u0002to\u0018\u0003 \u0001(\f\u0012\r\n\u0005value\u0018\u0004 \u0001(\f\u0012\r\n\u0005nonce\u0018\u0005 \u0001(\u0004\u0012\u0011\n\ttimestamp\u0018\u0006 \u0001(\u0003\u0012$\n\u0004data\u0018\u0007 \u0001(\u000b2\u0016.TW.Nebulas.Proto.Data\u0012\u0010\n\bchain_id\u0018\b \u0001(\r\u0012\u0011\n\tgas_price\u0018\t \u0001(\f\u0012\u0011\n\tgas_limit\u0018\n \u0001(\f\u0012\u000b\n\u0003alg\u0018\u000b \u0001(\r\u0012\f\n\u0004sign\u0018\f \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Nebulas_Proto_Data_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Nebulas_Proto_Data_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Nebulas_Proto_RawTransaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Nebulas_Proto_RawTransaction_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Nebulas_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Nebulas_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Nebulas_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Nebulas_Proto_SigningOutput_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class Data extends GeneratedMessageV3 implements DataOrBuilder {
        private static final Data DEFAULT_INSTANCE = new Data();
        private static final t0<Data> PARSER = new c<Data>() { // from class: wallet.core.jni.proto.Nebulas.Data.1
            @Override // com.google.protobuf.t0
            public Data parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Data(jVar, rVar);
            }
        };
        public static final int PAYLOAD_FIELD_NUMBER = 2;
        public static final int TYPE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString payload_;
        private volatile Object type_;

        public static Data getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Nebulas.internal_static_TW_Nebulas_Proto_Data_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Data parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Data) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Data parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Data> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Data)) {
                return super.equals(obj);
            }
            Data data = (Data) obj;
            return getType().equals(data.getType()) && getPayload().equals(data.getPayload()) && this.unknownFields.equals(data.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Data> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Nebulas.DataOrBuilder
        public ByteString getPayload() {
            return this.payload_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getTypeBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.type_);
            if (!this.payload_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(2, this.payload_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Nebulas.DataOrBuilder
        public String getType() {
            Object obj = this.type_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.type_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nebulas.DataOrBuilder
        public ByteString getTypeBytes() {
            Object obj = this.type_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.type_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getType().hashCode()) * 37) + 2) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Nebulas.internal_static_TW_Nebulas_Proto_Data_fieldAccessorTable.d(Data.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Data();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getTypeBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.type_);
            }
            if (!this.payload_.isEmpty()) {
                codedOutputStream.q0(2, this.payload_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DataOrBuilder {
            private ByteString payload_;
            private Object type_;

            public static final Descriptors.b getDescriptor() {
                return Nebulas.internal_static_TW_Nebulas_Proto_Data_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPayload() {
                this.payload_ = Data.getDefaultInstance().getPayload();
                onChanged();
                return this;
            }

            public Builder clearType() {
                this.type_ = Data.getDefaultInstance().getType();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Nebulas.internal_static_TW_Nebulas_Proto_Data_descriptor;
            }

            @Override // wallet.core.jni.proto.Nebulas.DataOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // wallet.core.jni.proto.Nebulas.DataOrBuilder
            public String getType() {
                Object obj = this.type_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.type_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.DataOrBuilder
            public ByteString getTypeBytes() {
                Object obj = this.type_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.type_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Nebulas.internal_static_TW_Nebulas_Proto_Data_fieldAccessorTable.d(Data.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setPayload(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.payload_ = byteString;
                onChanged();
                return this;
            }

            public Builder setType(String str) {
                Objects.requireNonNull(str);
                this.type_ = str;
                onChanged();
                return this;
            }

            public Builder setTypeBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.type_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.type_ = "";
                this.payload_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Data build() {
                Data buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Data buildPartial() {
                Data data = new Data(this);
                data.type_ = this.type_;
                data.payload_ = this.payload_;
                onBuilt();
                return data;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Data getDefaultInstanceForType() {
                return Data.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.type_ = "";
                this.payload_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.type_ = "";
                this.payload_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Data) {
                    return mergeFrom((Data) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Data data) {
                if (data == Data.getDefaultInstance()) {
                    return this;
                }
                if (!data.getType().isEmpty()) {
                    this.type_ = data.type_;
                    onChanged();
                }
                if (data.getPayload() != ByteString.EMPTY) {
                    setPayload(data.getPayload());
                }
                mergeUnknownFields(data.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Nebulas.Data.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Nebulas.Data.access$4400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Nebulas$Data r3 = (wallet.core.jni.proto.Nebulas.Data) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Nebulas$Data r4 = (wallet.core.jni.proto.Nebulas.Data) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Nebulas.Data.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Nebulas$Data$Builder");
            }
        }

        public static Builder newBuilder(Data data) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(data);
        }

        public static Data parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Data(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Data parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Data) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Data parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Data getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static Data parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Data() {
            this.memoizedIsInitialized = (byte) -1;
            this.type_ = "";
            this.payload_ = ByteString.EMPTY;
        }

        public static Data parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static Data parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Data parseFrom(InputStream inputStream) throws IOException {
            return (Data) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Data(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.type_ = jVar.I();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.payload_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Data parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Data) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Data parseFrom(j jVar) throws IOException {
            return (Data) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Data parseFrom(j jVar, r rVar) throws IOException {
            return (Data) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DataOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPayload();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getType();

        ByteString getTypeBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class RawTransaction extends GeneratedMessageV3 implements RawTransactionOrBuilder {
        public static final int ALG_FIELD_NUMBER = 11;
        public static final int CHAIN_ID_FIELD_NUMBER = 8;
        public static final int DATA_FIELD_NUMBER = 7;
        public static final int FROM_FIELD_NUMBER = 2;
        public static final int GAS_LIMIT_FIELD_NUMBER = 10;
        public static final int GAS_PRICE_FIELD_NUMBER = 9;
        public static final int HASH_FIELD_NUMBER = 1;
        public static final int NONCE_FIELD_NUMBER = 5;
        public static final int SIGN_FIELD_NUMBER = 12;
        public static final int TIMESTAMP_FIELD_NUMBER = 6;
        public static final int TO_FIELD_NUMBER = 3;
        public static final int VALUE_FIELD_NUMBER = 4;
        private static final long serialVersionUID = 0;
        private int alg_;
        private int chainId_;
        private Data data_;
        private ByteString from_;
        private ByteString gasLimit_;
        private ByteString gasPrice_;
        private ByteString hash_;
        private byte memoizedIsInitialized;
        private long nonce_;
        private ByteString sign_;
        private long timestamp_;
        private ByteString to_;
        private ByteString value_;
        private static final RawTransaction DEFAULT_INSTANCE = new RawTransaction();
        private static final t0<RawTransaction> PARSER = new c<RawTransaction>() { // from class: wallet.core.jni.proto.Nebulas.RawTransaction.1
            @Override // com.google.protobuf.t0
            public RawTransaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new RawTransaction(jVar, rVar);
            }
        };

        public static RawTransaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Nebulas.internal_static_TW_Nebulas_Proto_RawTransaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static RawTransaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (RawTransaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static RawTransaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<RawTransaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof RawTransaction)) {
                return super.equals(obj);
            }
            RawTransaction rawTransaction = (RawTransaction) obj;
            if (getHash().equals(rawTransaction.getHash()) && getFrom().equals(rawTransaction.getFrom()) && getTo().equals(rawTransaction.getTo()) && getValue().equals(rawTransaction.getValue()) && getNonce() == rawTransaction.getNonce() && getTimestamp() == rawTransaction.getTimestamp() && hasData() == rawTransaction.hasData()) {
                return (!hasData() || getData().equals(rawTransaction.getData())) && getChainId() == rawTransaction.getChainId() && getGasPrice().equals(rawTransaction.getGasPrice()) && getGasLimit().equals(rawTransaction.getGasLimit()) && getAlg() == rawTransaction.getAlg() && getSign().equals(rawTransaction.getSign()) && this.unknownFields.equals(rawTransaction.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public int getAlg() {
            return this.alg_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public int getChainId() {
            return this.chainId_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public Data getData() {
            Data data = this.data_;
            return data == null ? Data.getDefaultInstance() : data;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public DataOrBuilder getDataOrBuilder() {
            return getData();
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public ByteString getFrom() {
            return this.from_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public ByteString getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public ByteString getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public ByteString getHash() {
            return this.hash_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<RawTransaction> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.hash_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.hash_);
            if (!this.from_.isEmpty()) {
                h += CodedOutputStream.h(2, this.from_);
            }
            if (!this.to_.isEmpty()) {
                h += CodedOutputStream.h(3, this.to_);
            }
            if (!this.value_.isEmpty()) {
                h += CodedOutputStream.h(4, this.value_);
            }
            long j = this.nonce_;
            if (j != 0) {
                h += CodedOutputStream.a0(5, j);
            }
            long j2 = this.timestamp_;
            if (j2 != 0) {
                h += CodedOutputStream.z(6, j2);
            }
            if (this.data_ != null) {
                h += CodedOutputStream.G(7, getData());
            }
            int i2 = this.chainId_;
            if (i2 != 0) {
                h += CodedOutputStream.Y(8, i2);
            }
            if (!this.gasPrice_.isEmpty()) {
                h += CodedOutputStream.h(9, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                h += CodedOutputStream.h(10, this.gasLimit_);
            }
            int i3 = this.alg_;
            if (i3 != 0) {
                h += CodedOutputStream.Y(11, i3);
            }
            if (!this.sign_.isEmpty()) {
                h += CodedOutputStream.h(12, this.sign_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public ByteString getSign() {
            return this.sign_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public long getTimestamp() {
            return this.timestamp_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public ByteString getTo() {
            return this.to_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public ByteString getValue() {
            return this.value_;
        }

        @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
        public boolean hasData() {
            return this.data_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getHash().hashCode()) * 37) + 2) * 53) + getFrom().hashCode()) * 37) + 3) * 53) + getTo().hashCode()) * 37) + 4) * 53) + getValue().hashCode()) * 37) + 5) * 53) + a0.h(getNonce())) * 37) + 6) * 53) + a0.h(getTimestamp());
            if (hasData()) {
                hashCode = (((hashCode * 37) + 7) * 53) + getData().hashCode();
            }
            int chainId = (((((((((((((((((((((hashCode * 37) + 8) * 53) + getChainId()) * 37) + 9) * 53) + getGasPrice().hashCode()) * 37) + 10) * 53) + getGasLimit().hashCode()) * 37) + 11) * 53) + getAlg()) * 37) + 12) * 53) + getSign().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = chainId;
            return chainId;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Nebulas.internal_static_TW_Nebulas_Proto_RawTransaction_fieldAccessorTable.d(RawTransaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new RawTransaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.hash_.isEmpty()) {
                codedOutputStream.q0(1, this.hash_);
            }
            if (!this.from_.isEmpty()) {
                codedOutputStream.q0(2, this.from_);
            }
            if (!this.to_.isEmpty()) {
                codedOutputStream.q0(3, this.to_);
            }
            if (!this.value_.isEmpty()) {
                codedOutputStream.q0(4, this.value_);
            }
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(5, j);
            }
            long j2 = this.timestamp_;
            if (j2 != 0) {
                codedOutputStream.I0(6, j2);
            }
            if (this.data_ != null) {
                codedOutputStream.K0(7, getData());
            }
            int i = this.chainId_;
            if (i != 0) {
                codedOutputStream.b1(8, i);
            }
            if (!this.gasPrice_.isEmpty()) {
                codedOutputStream.q0(9, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                codedOutputStream.q0(10, this.gasLimit_);
            }
            int i2 = this.alg_;
            if (i2 != 0) {
                codedOutputStream.b1(11, i2);
            }
            if (!this.sign_.isEmpty()) {
                codedOutputStream.q0(12, this.sign_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements RawTransactionOrBuilder {
            private int alg_;
            private int chainId_;
            private a1<Data, Data.Builder, DataOrBuilder> dataBuilder_;
            private Data data_;
            private ByteString from_;
            private ByteString gasLimit_;
            private ByteString gasPrice_;
            private ByteString hash_;
            private long nonce_;
            private ByteString sign_;
            private long timestamp_;
            private ByteString to_;
            private ByteString value_;

            private a1<Data, Data.Builder, DataOrBuilder> getDataFieldBuilder() {
                if (this.dataBuilder_ == null) {
                    this.dataBuilder_ = new a1<>(getData(), getParentForChildren(), isClean());
                    this.data_ = null;
                }
                return this.dataBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Nebulas.internal_static_TW_Nebulas_Proto_RawTransaction_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAlg() {
                this.alg_ = 0;
                onChanged();
                return this;
            }

            public Builder clearChainId() {
                this.chainId_ = 0;
                onChanged();
                return this;
            }

            public Builder clearData() {
                if (this.dataBuilder_ == null) {
                    this.data_ = null;
                    onChanged();
                } else {
                    this.data_ = null;
                    this.dataBuilder_ = null;
                }
                return this;
            }

            public Builder clearFrom() {
                this.from_ = RawTransaction.getDefaultInstance().getFrom();
                onChanged();
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = RawTransaction.getDefaultInstance().getGasLimit();
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = RawTransaction.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearHash() {
                this.hash_ = RawTransaction.getDefaultInstance().getHash();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearSign() {
                this.sign_ = RawTransaction.getDefaultInstance().getSign();
                onChanged();
                return this;
            }

            public Builder clearTimestamp() {
                this.timestamp_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearTo() {
                this.to_ = RawTransaction.getDefaultInstance().getTo();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = RawTransaction.getDefaultInstance().getValue();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public int getAlg() {
                return this.alg_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public int getChainId() {
                return this.chainId_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public Data getData() {
                a1<Data, Data.Builder, DataOrBuilder> a1Var = this.dataBuilder_;
                if (a1Var == null) {
                    Data data = this.data_;
                    return data == null ? Data.getDefaultInstance() : data;
                }
                return a1Var.f();
            }

            public Data.Builder getDataBuilder() {
                onChanged();
                return getDataFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public DataOrBuilder getDataOrBuilder() {
                a1<Data, Data.Builder, DataOrBuilder> a1Var = this.dataBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Data data = this.data_;
                return data == null ? Data.getDefaultInstance() : data;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Nebulas.internal_static_TW_Nebulas_Proto_RawTransaction_descriptor;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public ByteString getFrom() {
                return this.from_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public ByteString getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public ByteString getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public ByteString getHash() {
                return this.hash_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public ByteString getSign() {
                return this.sign_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public long getTimestamp() {
                return this.timestamp_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public ByteString getTo() {
                return this.to_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // wallet.core.jni.proto.Nebulas.RawTransactionOrBuilder
            public boolean hasData() {
                return (this.dataBuilder_ == null && this.data_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Nebulas.internal_static_TW_Nebulas_Proto_RawTransaction_fieldAccessorTable.d(RawTransaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeData(Data data) {
                a1<Data, Data.Builder, DataOrBuilder> a1Var = this.dataBuilder_;
                if (a1Var == null) {
                    Data data2 = this.data_;
                    if (data2 != null) {
                        this.data_ = Data.newBuilder(data2).mergeFrom(data).buildPartial();
                    } else {
                        this.data_ = data;
                    }
                    onChanged();
                } else {
                    a1Var.h(data);
                }
                return this;
            }

            public Builder setAlg(int i) {
                this.alg_ = i;
                onChanged();
                return this;
            }

            public Builder setChainId(int i) {
                this.chainId_ = i;
                onChanged();
                return this;
            }

            public Builder setData(Data data) {
                a1<Data, Data.Builder, DataOrBuilder> a1Var = this.dataBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(data);
                    this.data_ = data;
                    onChanged();
                } else {
                    a1Var.j(data);
                }
                return this;
            }

            public Builder setFrom(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.from_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasLimit(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasLimit_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasPrice(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.hash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setSign(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.sign_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTimestamp(long j) {
                this.timestamp_ = j;
                onChanged();
                return this;
            }

            public Builder setTo(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.to_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.value_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.hash_ = byteString;
                this.from_ = byteString;
                this.to_ = byteString;
                this.value_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.sign_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public RawTransaction build() {
                RawTransaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public RawTransaction buildPartial() {
                RawTransaction rawTransaction = new RawTransaction(this);
                rawTransaction.hash_ = this.hash_;
                rawTransaction.from_ = this.from_;
                rawTransaction.to_ = this.to_;
                rawTransaction.value_ = this.value_;
                rawTransaction.nonce_ = this.nonce_;
                rawTransaction.timestamp_ = this.timestamp_;
                a1<Data, Data.Builder, DataOrBuilder> a1Var = this.dataBuilder_;
                if (a1Var == null) {
                    rawTransaction.data_ = this.data_;
                } else {
                    rawTransaction.data_ = a1Var.b();
                }
                rawTransaction.chainId_ = this.chainId_;
                rawTransaction.gasPrice_ = this.gasPrice_;
                rawTransaction.gasLimit_ = this.gasLimit_;
                rawTransaction.alg_ = this.alg_;
                rawTransaction.sign_ = this.sign_;
                onBuilt();
                return rawTransaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public RawTransaction getDefaultInstanceForType() {
                return RawTransaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.hash_ = byteString;
                this.from_ = byteString;
                this.to_ = byteString;
                this.value_ = byteString;
                this.nonce_ = 0L;
                this.timestamp_ = 0L;
                if (this.dataBuilder_ == null) {
                    this.data_ = null;
                } else {
                    this.data_ = null;
                    this.dataBuilder_ = null;
                }
                this.chainId_ = 0;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.alg_ = 0;
                this.sign_ = byteString;
                return this;
            }

            public Builder setData(Data.Builder builder) {
                a1<Data, Data.Builder, DataOrBuilder> a1Var = this.dataBuilder_;
                if (a1Var == null) {
                    this.data_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof RawTransaction) {
                    return mergeFrom((RawTransaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(RawTransaction rawTransaction) {
                if (rawTransaction == RawTransaction.getDefaultInstance()) {
                    return this;
                }
                ByteString hash = rawTransaction.getHash();
                ByteString byteString = ByteString.EMPTY;
                if (hash != byteString) {
                    setHash(rawTransaction.getHash());
                }
                if (rawTransaction.getFrom() != byteString) {
                    setFrom(rawTransaction.getFrom());
                }
                if (rawTransaction.getTo() != byteString) {
                    setTo(rawTransaction.getTo());
                }
                if (rawTransaction.getValue() != byteString) {
                    setValue(rawTransaction.getValue());
                }
                if (rawTransaction.getNonce() != 0) {
                    setNonce(rawTransaction.getNonce());
                }
                if (rawTransaction.getTimestamp() != 0) {
                    setTimestamp(rawTransaction.getTimestamp());
                }
                if (rawTransaction.hasData()) {
                    mergeData(rawTransaction.getData());
                }
                if (rawTransaction.getChainId() != 0) {
                    setChainId(rawTransaction.getChainId());
                }
                if (rawTransaction.getGasPrice() != byteString) {
                    setGasPrice(rawTransaction.getGasPrice());
                }
                if (rawTransaction.getGasLimit() != byteString) {
                    setGasLimit(rawTransaction.getGasLimit());
                }
                if (rawTransaction.getAlg() != 0) {
                    setAlg(rawTransaction.getAlg());
                }
                if (rawTransaction.getSign() != byteString) {
                    setSign(rawTransaction.getSign());
                }
                mergeUnknownFields(rawTransaction.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.hash_ = byteString;
                this.from_ = byteString;
                this.to_ = byteString;
                this.value_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.sign_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Nebulas.RawTransaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Nebulas.RawTransaction.access$6600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Nebulas$RawTransaction r3 = (wallet.core.jni.proto.Nebulas.RawTransaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Nebulas$RawTransaction r4 = (wallet.core.jni.proto.Nebulas.RawTransaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Nebulas.RawTransaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Nebulas$RawTransaction$Builder");
            }
        }

        public static Builder newBuilder(RawTransaction rawTransaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(rawTransaction);
        }

        public static RawTransaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private RawTransaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static RawTransaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (RawTransaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static RawTransaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public RawTransaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static RawTransaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private RawTransaction() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.hash_ = byteString;
            this.from_ = byteString;
            this.to_ = byteString;
            this.value_ = byteString;
            this.gasPrice_ = byteString;
            this.gasLimit_ = byteString;
            this.sign_ = byteString;
        }

        public static RawTransaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static RawTransaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static RawTransaction parseFrom(InputStream inputStream) throws IOException {
            return (RawTransaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static RawTransaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (RawTransaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static RawTransaction parseFrom(j jVar) throws IOException {
            return (RawTransaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static RawTransaction parseFrom(j jVar, r rVar) throws IOException {
            return (RawTransaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private RawTransaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 10:
                                this.hash_ = jVar.q();
                                continue;
                            case 18:
                                this.from_ = jVar.q();
                                continue;
                            case 26:
                                this.to_ = jVar.q();
                                continue;
                            case 34:
                                this.value_ = jVar.q();
                                continue;
                            case 40:
                                this.nonce_ = jVar.L();
                                continue;
                            case 48:
                                this.timestamp_ = jVar.y();
                                continue;
                            case 58:
                                Data data = this.data_;
                                Data.Builder builder = data != null ? data.toBuilder() : null;
                                Data data2 = (Data) jVar.z(Data.parser(), rVar);
                                this.data_ = data2;
                                if (builder != null) {
                                    builder.mergeFrom(data2);
                                    this.data_ = builder.buildPartial();
                                } else {
                                    continue;
                                }
                            case 64:
                                this.chainId_ = jVar.K();
                                continue;
                            case 74:
                                this.gasPrice_ = jVar.q();
                                continue;
                            case 82:
                                this.gasLimit_ = jVar.q();
                                continue;
                            case 88:
                                this.alg_ = jVar.K();
                                continue;
                            case 98:
                                this.sign_ = jVar.q();
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface RawTransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        int getAlg();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        int getChainId();

        Data getData();

        DataOrBuilder getDataOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getFrom();

        ByteString getGasLimit();

        ByteString getGasPrice();

        ByteString getHash();

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSign();

        long getTimestamp();

        ByteString getTo();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        ByteString getValue();

        boolean hasData();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 7;
        public static final int CHAIN_ID_FIELD_NUMBER = 2;
        public static final int FROM_ADDRESS_FIELD_NUMBER = 1;
        public static final int GAS_LIMIT_FIELD_NUMBER = 5;
        public static final int GAS_PRICE_FIELD_NUMBER = 4;
        public static final int NONCE_FIELD_NUMBER = 3;
        public static final int PAYLOAD_FIELD_NUMBER = 9;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 10;
        public static final int TIMESTAMP_FIELD_NUMBER = 8;
        public static final int TO_ADDRESS_FIELD_NUMBER = 6;
        private static final long serialVersionUID = 0;
        private ByteString amount_;
        private ByteString chainId_;
        private volatile Object fromAddress_;
        private ByteString gasLimit_;
        private ByteString gasPrice_;
        private byte memoizedIsInitialized;
        private ByteString nonce_;
        private volatile Object payload_;
        private ByteString privateKey_;
        private ByteString timestamp_;
        private volatile Object toAddress_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Nebulas.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Nebulas.internal_static_TW_Nebulas_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            return getFromAddress().equals(signingInput.getFromAddress()) && getChainId().equals(signingInput.getChainId()) && getNonce().equals(signingInput.getNonce()) && getGasPrice().equals(signingInput.getGasPrice()) && getGasLimit().equals(signingInput.getGasLimit()) && getToAddress().equals(signingInput.getToAddress()) && getAmount().equals(signingInput.getAmount()) && getTimestamp().equals(signingInput.getTimestamp()) && getPayload().equals(signingInput.getPayload()) && getPrivateKey().equals(signingInput.getPrivateKey()) && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getChainId() {
            return this.chainId_;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public String getFromAddress() {
            Object obj = this.fromAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.fromAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getFromAddressBytes() {
            Object obj = this.fromAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.fromAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public String getPayload() {
            Object obj = this.payload_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.payload_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getPayloadBytes() {
            Object obj = this.payload_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.payload_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getFromAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.fromAddress_);
            if (!this.chainId_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(2, this.chainId_);
            }
            if (!this.nonce_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.nonce_);
            }
            if (!this.gasPrice_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(5, this.gasLimit_);
            }
            if (!getToAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(6, this.toAddress_);
            }
            if (!this.amount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(7, this.amount_);
            }
            if (!this.timestamp_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(8, this.timestamp_);
            }
            if (!getPayloadBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(9, this.payload_);
            }
            if (!this.privateKey_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(10, this.privateKey_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getTimestamp() {
            return this.timestamp_;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFromAddress().hashCode()) * 37) + 2) * 53) + getChainId().hashCode()) * 37) + 3) * 53) + getNonce().hashCode()) * 37) + 4) * 53) + getGasPrice().hashCode()) * 37) + 5) * 53) + getGasLimit().hashCode()) * 37) + 6) * 53) + getToAddress().hashCode()) * 37) + 7) * 53) + getAmount().hashCode()) * 37) + 8) * 53) + getTimestamp().hashCode()) * 37) + 9) * 53) + getPayload().hashCode()) * 37) + 10) * 53) + getPrivateKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Nebulas.internal_static_TW_Nebulas_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getFromAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.fromAddress_);
            }
            if (!this.chainId_.isEmpty()) {
                codedOutputStream.q0(2, this.chainId_);
            }
            if (!this.nonce_.isEmpty()) {
                codedOutputStream.q0(3, this.nonce_);
            }
            if (!this.gasPrice_.isEmpty()) {
                codedOutputStream.q0(4, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                codedOutputStream.q0(5, this.gasLimit_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 6, this.toAddress_);
            }
            if (!this.amount_.isEmpty()) {
                codedOutputStream.q0(7, this.amount_);
            }
            if (!this.timestamp_.isEmpty()) {
                codedOutputStream.q0(8, this.timestamp_);
            }
            if (!getPayloadBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 9, this.payload_);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(10, this.privateKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private ByteString amount_;
            private ByteString chainId_;
            private Object fromAddress_;
            private ByteString gasLimit_;
            private ByteString gasPrice_;
            private ByteString nonce_;
            private Object payload_;
            private ByteString privateKey_;
            private ByteString timestamp_;
            private Object toAddress_;

            public static final Descriptors.b getDescriptor() {
                return Nebulas.internal_static_TW_Nebulas_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = SigningInput.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearChainId() {
                this.chainId_ = SigningInput.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearFromAddress() {
                this.fromAddress_ = SigningInput.getDefaultInstance().getFromAddress();
                onChanged();
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = SigningInput.getDefaultInstance().getGasLimit();
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = SigningInput.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = SigningInput.getDefaultInstance().getNonce();
                onChanged();
                return this;
            }

            public Builder clearPayload() {
                this.payload_ = SigningInput.getDefaultInstance().getPayload();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearTimestamp() {
                this.timestamp_ = SigningInput.getDefaultInstance().getTimestamp();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = SigningInput.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getChainId() {
                return this.chainId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Nebulas.internal_static_TW_Nebulas_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public String getFromAddress() {
                Object obj = this.fromAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.fromAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getFromAddressBytes() {
                Object obj = this.fromAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.fromAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public String getPayload() {
                Object obj = this.payload_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.payload_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getPayloadBytes() {
                Object obj = this.payload_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.payload_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getTimestamp() {
                return this.timestamp_;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningInputOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Nebulas.internal_static_TW_Nebulas_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setChainId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setFromAddress(String str) {
                Objects.requireNonNull(str);
                this.fromAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setFromAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.fromAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasLimit(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasLimit_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasPrice(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.nonce_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPayload(String str) {
                Objects.requireNonNull(str);
                this.payload_ = str;
                onChanged();
                return this;
            }

            public Builder setPayloadBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.payload_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTimestamp(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.timestamp_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.fromAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.amount_ = byteString;
                this.timestamp_ = byteString;
                this.payload_ = "";
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                signingInput.fromAddress_ = this.fromAddress_;
                signingInput.chainId_ = this.chainId_;
                signingInput.nonce_ = this.nonce_;
                signingInput.gasPrice_ = this.gasPrice_;
                signingInput.gasLimit_ = this.gasLimit_;
                signingInput.toAddress_ = this.toAddress_;
                signingInput.amount_ = this.amount_;
                signingInput.timestamp_ = this.timestamp_;
                signingInput.payload_ = this.payload_;
                signingInput.privateKey_ = this.privateKey_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.fromAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.amount_ = byteString;
                this.timestamp_ = byteString;
                this.payload_ = "";
                this.privateKey_ = byteString;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (!signingInput.getFromAddress().isEmpty()) {
                    this.fromAddress_ = signingInput.fromAddress_;
                    onChanged();
                }
                ByteString chainId = signingInput.getChainId();
                ByteString byteString = ByteString.EMPTY;
                if (chainId != byteString) {
                    setChainId(signingInput.getChainId());
                }
                if (signingInput.getNonce() != byteString) {
                    setNonce(signingInput.getNonce());
                }
                if (signingInput.getGasPrice() != byteString) {
                    setGasPrice(signingInput.getGasPrice());
                }
                if (signingInput.getGasLimit() != byteString) {
                    setGasLimit(signingInput.getGasLimit());
                }
                if (!signingInput.getToAddress().isEmpty()) {
                    this.toAddress_ = signingInput.toAddress_;
                    onChanged();
                }
                if (signingInput.getAmount() != byteString) {
                    setAmount(signingInput.getAmount());
                }
                if (signingInput.getTimestamp() != byteString) {
                    setTimestamp(signingInput.getTimestamp());
                }
                if (!signingInput.getPayload().isEmpty()) {
                    this.payload_ = signingInput.payload_;
                    onChanged();
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.fromAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.amount_ = byteString;
                this.timestamp_ = byteString;
                this.payload_ = "";
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Nebulas.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Nebulas.SigningInput.access$1700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Nebulas$SigningInput r3 = (wallet.core.jni.proto.Nebulas.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Nebulas$SigningInput r4 = (wallet.core.jni.proto.Nebulas.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Nebulas.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Nebulas$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.fromAddress_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.chainId_ = byteString;
            this.nonce_ = byteString;
            this.gasPrice_ = byteString;
            this.gasLimit_ = byteString;
            this.toAddress_ = "";
            this.amount_ = byteString;
            this.timestamp_ = byteString;
            this.payload_ = "";
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 10:
                                this.fromAddress_ = jVar.I();
                                continue;
                            case 18:
                                this.chainId_ = jVar.q();
                                continue;
                            case 26:
                                this.nonce_ = jVar.q();
                                continue;
                            case 34:
                                this.gasPrice_ = jVar.q();
                                continue;
                            case 42:
                                this.gasLimit_ = jVar.q();
                                continue;
                            case 50:
                                this.toAddress_ = jVar.I();
                                continue;
                            case 58:
                                this.amount_ = jVar.q();
                                continue;
                            case 66:
                                this.timestamp_ = jVar.q();
                                continue;
                            case 74:
                                this.payload_ = jVar.I();
                                continue;
                            case 82:
                                this.privateKey_ = jVar.q();
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAmount();

        ByteString getChainId();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        String getFromAddress();

        ByteString getFromAddressBytes();

        ByteString getGasLimit();

        ByteString getGasPrice();

        /* synthetic */ String getInitializationErrorString();

        ByteString getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getPayload();

        ByteString getPayloadBytes();

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getTimestamp();

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ALGORITHM_FIELD_NUMBER = 1;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Nebulas.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };
        public static final int RAW_FIELD_NUMBER = 3;
        public static final int SIGNATURE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private int algorithm_;
        private byte memoizedIsInitialized;
        private volatile Object raw_;
        private ByteString signature_;

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Nebulas.internal_static_TW_Nebulas_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getAlgorithm() == signingOutput.getAlgorithm() && getSignature().equals(signingOutput.getSignature()) && getRaw().equals(signingOutput.getRaw()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
        public int getAlgorithm() {
            return this.algorithm_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
        public String getRaw() {
            Object obj = this.raw_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.raw_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
        public ByteString getRawBytes() {
            Object obj = this.raw_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.raw_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.algorithm_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            if (!this.signature_.isEmpty()) {
                Y += CodedOutputStream.h(2, this.signature_);
            }
            if (!getRawBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(3, this.raw_);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAlgorithm()) * 37) + 2) * 53) + getSignature().hashCode()) * 37) + 3) * 53) + getRaw().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Nebulas.internal_static_TW_Nebulas_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.algorithm_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(2, this.signature_);
            }
            if (!getRawBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.raw_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private int algorithm_;
            private Object raw_;
            private ByteString signature_;

            public static final Descriptors.b getDescriptor() {
                return Nebulas.internal_static_TW_Nebulas_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAlgorithm() {
                this.algorithm_ = 0;
                onChanged();
                return this;
            }

            public Builder clearRaw() {
                this.raw_ = SigningOutput.getDefaultInstance().getRaw();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
            public int getAlgorithm() {
                return this.algorithm_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Nebulas.internal_static_TW_Nebulas_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
            public String getRaw() {
                Object obj = this.raw_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.raw_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
            public ByteString getRawBytes() {
                Object obj = this.raw_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.raw_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Nebulas.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Nebulas.internal_static_TW_Nebulas_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAlgorithm(int i) {
                this.algorithm_ = i;
                onChanged();
                return this;
            }

            public Builder setRaw(String str) {
                Objects.requireNonNull(str);
                this.raw_ = str;
                onChanged();
                return this;
            }

            public Builder setRawBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.raw_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.signature_ = ByteString.EMPTY;
                this.raw_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                signingOutput.algorithm_ = this.algorithm_;
                signingOutput.signature_ = this.signature_;
                signingOutput.raw_ = this.raw_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.algorithm_ = 0;
                this.signature_ = ByteString.EMPTY;
                this.raw_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.signature_ = ByteString.EMPTY;
                this.raw_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getAlgorithm() != 0) {
                    setAlgorithm(signingOutput.getAlgorithm());
                }
                if (signingOutput.getSignature() != ByteString.EMPTY) {
                    setSignature(signingOutput.getSignature());
                }
                if (!signingOutput.getRaw().isEmpty()) {
                    this.raw_ = signingOutput.raw_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Nebulas.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Nebulas.SigningOutput.access$3200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Nebulas$SigningOutput r3 = (wallet.core.jni.proto.Nebulas.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Nebulas$SigningOutput r4 = (wallet.core.jni.proto.Nebulas.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Nebulas.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Nebulas$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.signature_ = ByteString.EMPTY;
            this.raw_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.algorithm_ = jVar.K();
                            } else if (J == 18) {
                                this.signature_ = jVar.q();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.raw_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        int getAlgorithm();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getRaw();

        ByteString getRawBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Nebulas_Proto_SigningInput_descriptor = bVar;
        internal_static_TW_Nebulas_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"FromAddress", "ChainId", "Nonce", "GasPrice", "GasLimit", "ToAddress", "Amount", "Timestamp", "Payload", "PrivateKey"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Nebulas_Proto_SigningOutput_descriptor = bVar2;
        internal_static_TW_Nebulas_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Algorithm", "Signature", "Raw"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Nebulas_Proto_Data_descriptor = bVar3;
        internal_static_TW_Nebulas_Proto_Data_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Type", "Payload"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Nebulas_Proto_RawTransaction_descriptor = bVar4;
        internal_static_TW_Nebulas_Proto_RawTransaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Hash", "From", "To", "Value", "Nonce", "Timestamp", "Data", "ChainId", "GasPrice", "GasLimit", "Alg", "Sign"});
    }

    private Nebulas() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
