package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Elrond {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\fElrond.proto\u0012\u000fTW.Elrond.Proto\"«\u0001\n\u0012TransactionMessage\u0012\r\n\u0005nonce\u0018\u0001 \u0001(\u0004\u0012\r\n\u0005value\u0018\u0002 \u0001(\t\u0012\u0010\n\breceiver\u0018\u0003 \u0001(\t\u0012\u000e\n\u0006sender\u0018\u0004 \u0001(\t\u0012\u0011\n\tgas_price\u0018\u0005 \u0001(\u0004\u0012\u0011\n\tgas_limit\u0018\u0006 \u0001(\u0004\u0012\f\n\u0004data\u0018\u0007 \u0001(\t\u0012\u0010\n\bchain_id\u0018\b \u0001(\t\u0012\u000f\n\u0007version\u0018\t \u0001(\r\"p\n\fSigningInput\u0012\u0013\n\u000bprivate_key\u0018\u0001 \u0001(\f\u0012:\n\u000btransaction\u0018\u0002 \u0001(\u000b2#.TW.Elrond.Proto.TransactionMessageH\u0000B\u000f\n\rmessage_oneof\"3\n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\t\u0012\u0011\n\tsignature\u0018\u0002 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Elrond_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Elrond_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Elrond_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Elrond_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Elrond_Proto_TransactionMessage_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Elrond_Proto_TransactionMessage_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Elrond$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Elrond$SigningInput$MessageOneofCase;

        static {
            int[] iArr = new int[SigningInput.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Elrond$SigningInput$MessageOneofCase = iArr;
            try {
                iArr[SigningInput.MessageOneofCase.TRANSACTION.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Elrond$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Elrond.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };
        public static final int PRIVATE_KEY_FIELD_NUMBER = 1;
        public static final int TRANSACTION_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;
        private ByteString privateKey_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private int messageOneofCase_;
            private Object messageOneof_;
            private ByteString privateKey_;
            private a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> transactionBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Elrond.internal_static_TW_Elrond_Proto_SigningInput_descriptor;
            }

            private a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> getTransactionFieldBuilder() {
                if (this.transactionBuilder_ == null) {
                    if (this.messageOneofCase_ != 2) {
                        this.messageOneof_ = TransactionMessage.getDefaultInstance();
                    }
                    this.transactionBuilder_ = new a1<>((TransactionMessage) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 2;
                onChanged();
                return this.transactionBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearTransaction() {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Elrond.internal_static_TW_Elrond_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
            public TransactionMessage getTransaction() {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        return (TransactionMessage) this.messageOneof_;
                    }
                    return TransactionMessage.getDefaultInstance();
                } else if (this.messageOneofCase_ == 2) {
                    return a1Var.f();
                } else {
                    return TransactionMessage.getDefaultInstance();
                }
            }

            public TransactionMessage.Builder getTransactionBuilder() {
                return getTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
            public TransactionMessageOrBuilder getTransactionOrBuilder() {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 2 || (a1Var = this.transactionBuilder_) == null) {
                    if (i == 2) {
                        return (TransactionMessage) this.messageOneof_;
                    }
                    return TransactionMessage.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
            public boolean hasTransaction() {
                return this.messageOneofCase_ == 2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Elrond.internal_static_TW_Elrond_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransaction(TransactionMessage transactionMessage) {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2 && this.messageOneof_ != TransactionMessage.getDefaultInstance()) {
                        this.messageOneof_ = TransactionMessage.newBuilder((TransactionMessage) this.messageOneof_).mergeFrom(transactionMessage).buildPartial();
                    } else {
                        this.messageOneof_ = transactionMessage;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 2) {
                        a1Var.h(transactionMessage);
                    }
                    this.transactionBuilder_.j(transactionMessage);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTransaction(TransactionMessage transactionMessage) {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionMessage);
                    this.messageOneof_ = transactionMessage;
                    onChanged();
                } else {
                    a1Var.j(transactionMessage);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.privateKey_ = this.privateKey_;
                if (this.messageOneofCase_ == 2) {
                    a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionBuilder_;
                    if (a1Var == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var.b();
                    }
                }
                signingInput.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.privateKey_ = ByteString.EMPTY;
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setTransaction(TransactionMessage.Builder builder) {
                a1<TransactionMessage, TransactionMessage.Builder, TransactionMessageOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Elrond$SigningInput$MessageOneofCase[signingInput.getMessageOneofCase().ordinal()] == 1) {
                    mergeTransaction(signingInput.getTransaction());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Elrond.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Elrond.SigningInput.access$3300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Elrond$SigningInput r3 = (wallet.core.jni.proto.Elrond.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Elrond$SigningInput r4 = (wallet.core.jni.proto.Elrond.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Elrond.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Elrond$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            TRANSACTION(2),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 2) {
                        return null;
                    }
                    return TRANSACTION;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Elrond.internal_static_TW_Elrond_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getPrivateKey().equals(signingInput.getPrivateKey()) && getMessageOneofCase().equals(signingInput.getMessageOneofCase())) {
                return (this.messageOneofCase_ != 2 || getTransaction().equals(signingInput.getTransaction())) && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.privateKey_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.privateKey_);
            if (this.messageOneofCase_ == 2) {
                h += CodedOutputStream.G(2, (TransactionMessage) this.messageOneof_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
        public TransactionMessage getTransaction() {
            if (this.messageOneofCase_ == 2) {
                return (TransactionMessage) this.messageOneof_;
            }
            return TransactionMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
        public TransactionMessageOrBuilder getTransactionOrBuilder() {
            if (this.messageOneofCase_ == 2) {
                return (TransactionMessage) this.messageOneof_;
            }
            return TransactionMessage.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Elrond.SigningInputOrBuilder
        public boolean hasTransaction() {
            return this.messageOneofCase_ == 2;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPrivateKey().hashCode();
            if (this.messageOneofCase_ == 2) {
                hashCode = (((hashCode * 37) + 2) * 53) + getTransaction().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Elrond.internal_static_TW_Elrond_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(1, this.privateKey_);
            }
            if (this.messageOneofCase_ == 2) {
                codedOutputStream.K0(2, (TransactionMessage) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.privateKey_ = ByteString.EMPTY;
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.privateKey_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    TransactionMessage.Builder builder = this.messageOneofCase_ == 2 ? ((TransactionMessage) this.messageOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(TransactionMessage.parser(), rVar);
                                    this.messageOneof_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((TransactionMessage) z2);
                                        this.messageOneof_ = builder.buildPartial();
                                    }
                                    this.messageOneofCase_ = 2;
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        SigningInput.MessageOneofCase getMessageOneofCase();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        TransactionMessage getTransaction();

        TransactionMessageOrBuilder getTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransaction();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        public static final int SIGNATURE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private volatile Object encoded_;
        private byte memoizedIsInitialized;
        private volatile Object signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Elrond.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private Object encoded_;
            private Object signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Elrond.internal_static_TW_Elrond_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Elrond.internal_static_TW_Elrond_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
            public String getEncoded() {
                Object obj = this.encoded_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.encoded_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
            public ByteString getEncodedBytes() {
                Object obj = this.encoded_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.encoded_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
            public String getSignature() {
                Object obj = this.signature_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.signature_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
            public ByteString getSignatureBytes() {
                Object obj = this.signature_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.signature_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Elrond.internal_static_TW_Elrond_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(String str) {
                Objects.requireNonNull(str);
                this.encoded_ = str;
                onChanged();
                return this;
            }

            public Builder setEncodedBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(String str) {
                Objects.requireNonNull(str);
                this.signature_ = str;
                onChanged();
                return this;
            }

            public Builder setSignatureBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.encoded_ = "";
                this.signature_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                signingOutput.signature_ = this.signature_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = "";
                this.signature_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = "";
                this.signature_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (!signingOutput.getEncoded().isEmpty()) {
                    this.encoded_ = signingOutput.encoded_;
                    onChanged();
                }
                if (!signingOutput.getSignature().isEmpty()) {
                    this.signature_ = signingOutput.signature_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Elrond.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Elrond.SigningOutput.access$4400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Elrond$SigningOutput r3 = (wallet.core.jni.proto.Elrond.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Elrond$SigningOutput r4 = (wallet.core.jni.proto.Elrond.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Elrond.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Elrond$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Elrond.internal_static_TW_Elrond_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && getSignature().equals(signingOutput.getSignature()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
        public String getEncoded() {
            Object obj = this.encoded_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.encoded_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
        public ByteString getEncodedBytes() {
            Object obj = this.encoded_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.encoded_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getEncodedBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.encoded_);
            if (!getSignatureBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.signature_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
        public String getSignature() {
            Object obj = this.signature_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.signature_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Elrond.SigningOutputOrBuilder
        public ByteString getSignatureBytes() {
            Object obj = this.signature_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.signature_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 37) + 2) * 53) + getSignature().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Elrond.internal_static_TW_Elrond_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getEncodedBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.encoded_);
            }
            if (!getSignatureBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.signature_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = "";
            this.signature_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.encoded_ = jVar.I();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.signature_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        String getEncoded();

        ByteString getEncodedBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSignature();

        ByteString getSignatureBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionMessage extends GeneratedMessageV3 implements TransactionMessageOrBuilder {
        public static final int CHAIN_ID_FIELD_NUMBER = 8;
        public static final int DATA_FIELD_NUMBER = 7;
        public static final int GAS_LIMIT_FIELD_NUMBER = 6;
        public static final int GAS_PRICE_FIELD_NUMBER = 5;
        public static final int NONCE_FIELD_NUMBER = 1;
        public static final int RECEIVER_FIELD_NUMBER = 3;
        public static final int SENDER_FIELD_NUMBER = 4;
        public static final int VALUE_FIELD_NUMBER = 2;
        public static final int VERSION_FIELD_NUMBER = 9;
        private static final long serialVersionUID = 0;
        private volatile Object chainId_;
        private volatile Object data_;
        private long gasLimit_;
        private long gasPrice_;
        private byte memoizedIsInitialized;
        private long nonce_;
        private volatile Object receiver_;
        private volatile Object sender_;
        private volatile Object value_;
        private int version_;
        private static final TransactionMessage DEFAULT_INSTANCE = new TransactionMessage();
        private static final t0<TransactionMessage> PARSER = new c<TransactionMessage>() { // from class: wallet.core.jni.proto.Elrond.TransactionMessage.1
            @Override // com.google.protobuf.t0
            public TransactionMessage parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionMessage(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionMessageOrBuilder {
            private Object chainId_;
            private Object data_;
            private long gasLimit_;
            private long gasPrice_;
            private long nonce_;
            private Object receiver_;
            private Object sender_;
            private Object value_;
            private int version_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Elrond.internal_static_TW_Elrond_Proto_TransactionMessage_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearChainId() {
                this.chainId_ = TransactionMessage.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearData() {
                this.data_ = TransactionMessage.getDefaultInstance().getData();
                onChanged();
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearReceiver() {
                this.receiver_ = TransactionMessage.getDefaultInstance().getReceiver();
                onChanged();
                return this;
            }

            public Builder clearSender() {
                this.sender_ = TransactionMessage.getDefaultInstance().getSender();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = TransactionMessage.getDefaultInstance().getValue();
                onChanged();
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public String getChainId() {
                Object obj = this.chainId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.chainId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public ByteString getChainIdBytes() {
                Object obj = this.chainId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.chainId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public String getData() {
                Object obj = this.data_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.data_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public ByteString getDataBytes() {
                Object obj = this.data_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.data_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Elrond.internal_static_TW_Elrond_Proto_TransactionMessage_descriptor;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public long getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public long getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public String getReceiver() {
                Object obj = this.receiver_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.receiver_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public ByteString getReceiverBytes() {
                Object obj = this.receiver_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.receiver_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public String getSender() {
                Object obj = this.sender_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.sender_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public ByteString getSenderBytes() {
                Object obj = this.sender_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.sender_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public String getValue() {
                Object obj = this.value_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.value_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public ByteString getValueBytes() {
                Object obj = this.value_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.value_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Elrond.internal_static_TW_Elrond_Proto_TransactionMessage_fieldAccessorTable.d(TransactionMessage.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setChainId(String str) {
                Objects.requireNonNull(str);
                this.chainId_ = str;
                onChanged();
                return this;
            }

            public Builder setChainIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setData(String str) {
                Objects.requireNonNull(str);
                this.data_ = str;
                onChanged();
                return this;
            }

            public Builder setDataBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.data_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasLimit(long j) {
                this.gasLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setGasPrice(long j) {
                this.gasPrice_ = j;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setReceiver(String str) {
                Objects.requireNonNull(str);
                this.receiver_ = str;
                onChanged();
                return this;
            }

            public Builder setReceiverBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.receiver_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSender(String str) {
                Objects.requireNonNull(str);
                this.sender_ = str;
                onChanged();
                return this;
            }

            public Builder setSenderBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.sender_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(String str) {
                Objects.requireNonNull(str);
                this.value_ = str;
                onChanged();
                return this;
            }

            public Builder setValueBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.value_ = byteString;
                onChanged();
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.value_ = "";
                this.receiver_ = "";
                this.sender_ = "";
                this.data_ = "";
                this.chainId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionMessage build() {
                TransactionMessage buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionMessage buildPartial() {
                TransactionMessage transactionMessage = new TransactionMessage(this, (AnonymousClass1) null);
                transactionMessage.nonce_ = this.nonce_;
                transactionMessage.value_ = this.value_;
                transactionMessage.receiver_ = this.receiver_;
                transactionMessage.sender_ = this.sender_;
                transactionMessage.gasPrice_ = this.gasPrice_;
                transactionMessage.gasLimit_ = this.gasLimit_;
                transactionMessage.data_ = this.data_;
                transactionMessage.chainId_ = this.chainId_;
                transactionMessage.version_ = this.version_;
                onBuilt();
                return transactionMessage;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionMessage getDefaultInstanceForType() {
                return TransactionMessage.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.nonce_ = 0L;
                this.value_ = "";
                this.receiver_ = "";
                this.sender_ = "";
                this.gasPrice_ = 0L;
                this.gasLimit_ = 0L;
                this.data_ = "";
                this.chainId_ = "";
                this.version_ = 0;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionMessage) {
                    return mergeFrom((TransactionMessage) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.value_ = "";
                this.receiver_ = "";
                this.sender_ = "";
                this.data_ = "";
                this.chainId_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransactionMessage transactionMessage) {
                if (transactionMessage == TransactionMessage.getDefaultInstance()) {
                    return this;
                }
                if (transactionMessage.getNonce() != 0) {
                    setNonce(transactionMessage.getNonce());
                }
                if (!transactionMessage.getValue().isEmpty()) {
                    this.value_ = transactionMessage.value_;
                    onChanged();
                }
                if (!transactionMessage.getReceiver().isEmpty()) {
                    this.receiver_ = transactionMessage.receiver_;
                    onChanged();
                }
                if (!transactionMessage.getSender().isEmpty()) {
                    this.sender_ = transactionMessage.sender_;
                    onChanged();
                }
                if (transactionMessage.getGasPrice() != 0) {
                    setGasPrice(transactionMessage.getGasPrice());
                }
                if (transactionMessage.getGasLimit() != 0) {
                    setGasLimit(transactionMessage.getGasLimit());
                }
                if (!transactionMessage.getData().isEmpty()) {
                    this.data_ = transactionMessage.data_;
                    onChanged();
                }
                if (!transactionMessage.getChainId().isEmpty()) {
                    this.chainId_ = transactionMessage.chainId_;
                    onChanged();
                }
                if (transactionMessage.getVersion() != 0) {
                    setVersion(transactionMessage.getVersion());
                }
                mergeUnknownFields(transactionMessage.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Elrond.TransactionMessage.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Elrond.TransactionMessage.access$1600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Elrond$TransactionMessage r3 = (wallet.core.jni.proto.Elrond.TransactionMessage) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Elrond$TransactionMessage r4 = (wallet.core.jni.proto.Elrond.TransactionMessage) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Elrond.TransactionMessage.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Elrond$TransactionMessage$Builder");
            }
        }

        public /* synthetic */ TransactionMessage(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransactionMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Elrond.internal_static_TW_Elrond_Proto_TransactionMessage_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionMessage> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionMessage)) {
                return super.equals(obj);
            }
            TransactionMessage transactionMessage = (TransactionMessage) obj;
            return getNonce() == transactionMessage.getNonce() && getValue().equals(transactionMessage.getValue()) && getReceiver().equals(transactionMessage.getReceiver()) && getSender().equals(transactionMessage.getSender()) && getGasPrice() == transactionMessage.getGasPrice() && getGasLimit() == transactionMessage.getGasLimit() && getData().equals(transactionMessage.getData()) && getChainId().equals(transactionMessage.getChainId()) && getVersion() == transactionMessage.getVersion() && this.unknownFields.equals(transactionMessage.unknownFields);
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public String getChainId() {
            Object obj = this.chainId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.chainId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public ByteString getChainIdBytes() {
            Object obj = this.chainId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.chainId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public String getData() {
            Object obj = this.data_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.data_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public ByteString getDataBytes() {
            Object obj = this.data_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.data_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public long getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public long getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionMessage> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public String getReceiver() {
            Object obj = this.receiver_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.receiver_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public ByteString getReceiverBytes() {
            Object obj = this.receiver_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.receiver_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public String getSender() {
            Object obj = this.sender_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.sender_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public ByteString getSenderBytes() {
            Object obj = this.sender_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.sender_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.nonce_;
            int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
            if (!getValueBytes().isEmpty()) {
                a0 += GeneratedMessageV3.computeStringSize(2, this.value_);
            }
            if (!getReceiverBytes().isEmpty()) {
                a0 += GeneratedMessageV3.computeStringSize(3, this.receiver_);
            }
            if (!getSenderBytes().isEmpty()) {
                a0 += GeneratedMessageV3.computeStringSize(4, this.sender_);
            }
            long j2 = this.gasPrice_;
            if (j2 != 0) {
                a0 += CodedOutputStream.a0(5, j2);
            }
            long j3 = this.gasLimit_;
            if (j3 != 0) {
                a0 += CodedOutputStream.a0(6, j3);
            }
            if (!getDataBytes().isEmpty()) {
                a0 += GeneratedMessageV3.computeStringSize(7, this.data_);
            }
            if (!getChainIdBytes().isEmpty()) {
                a0 += GeneratedMessageV3.computeStringSize(8, this.chainId_);
            }
            int i2 = this.version_;
            if (i2 != 0) {
                a0 += CodedOutputStream.Y(9, i2);
            }
            int serializedSize = a0 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public String getValue() {
            Object obj = this.value_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.value_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public ByteString getValueBytes() {
            Object obj = this.value_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.value_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Elrond.TransactionMessageOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getNonce())) * 37) + 2) * 53) + getValue().hashCode()) * 37) + 3) * 53) + getReceiver().hashCode()) * 37) + 4) * 53) + getSender().hashCode()) * 37) + 5) * 53) + a0.h(getGasPrice())) * 37) + 6) * 53) + a0.h(getGasLimit())) * 37) + 7) * 53) + getData().hashCode()) * 37) + 8) * 53) + getChainId().hashCode()) * 37) + 9) * 53) + getVersion()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Elrond.internal_static_TW_Elrond_Proto_TransactionMessage_fieldAccessorTable.d(TransactionMessage.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionMessage();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(1, j);
            }
            if (!getValueBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.value_);
            }
            if (!getReceiverBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.receiver_);
            }
            if (!getSenderBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.sender_);
            }
            long j2 = this.gasPrice_;
            if (j2 != 0) {
                codedOutputStream.d1(5, j2);
            }
            long j3 = this.gasLimit_;
            if (j3 != 0) {
                codedOutputStream.d1(6, j3);
            }
            if (!getDataBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 7, this.data_);
            }
            if (!getChainIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 8, this.chainId_);
            }
            int i = this.version_;
            if (i != 0) {
                codedOutputStream.b1(9, i);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransactionMessage(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransactionMessage transactionMessage) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionMessage);
        }

        public static TransactionMessage parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionMessage(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionMessage parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionMessage getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransactionMessage parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionMessage() {
            this.memoizedIsInitialized = (byte) -1;
            this.value_ = "";
            this.receiver_ = "";
            this.sender_ = "";
            this.data_ = "";
            this.chainId_ = "";
        }

        public static TransactionMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransactionMessage parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionMessage parseFrom(InputStream inputStream) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransactionMessage parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionMessage parseFrom(j jVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private TransactionMessage(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.nonce_ = jVar.L();
                                } else if (J == 18) {
                                    this.value_ = jVar.I();
                                } else if (J == 26) {
                                    this.receiver_ = jVar.I();
                                } else if (J == 34) {
                                    this.sender_ = jVar.I();
                                } else if (J == 40) {
                                    this.gasPrice_ = jVar.L();
                                } else if (J == 48) {
                                    this.gasLimit_ = jVar.L();
                                } else if (J == 58) {
                                    this.data_ = jVar.I();
                                } else if (J == 66) {
                                    this.chainId_ = jVar.I();
                                } else if (J != 72) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.version_ = jVar.K();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionMessage parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionMessageOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getChainId();

        ByteString getChainIdBytes();

        String getData();

        ByteString getDataBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGasLimit();

        long getGasPrice();

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getReceiver();

        ByteString getReceiverBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSender();

        ByteString getSenderBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValue();

        ByteString getValueBytes();

        int getVersion();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Elrond_Proto_TransactionMessage_descriptor = bVar;
        internal_static_TW_Elrond_Proto_TransactionMessage_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Nonce", "Value", "Receiver", "Sender", "GasPrice", "GasLimit", "Data", "ChainId", "Version"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Elrond_Proto_SigningInput_descriptor = bVar2;
        internal_static_TW_Elrond_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"PrivateKey", "Transaction", "MessageOneof"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Elrond_Proto_SigningOutput_descriptor = bVar3;
        internal_static_TW_Elrond_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Encoded", "Signature"});
    }

    private Elrond() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
