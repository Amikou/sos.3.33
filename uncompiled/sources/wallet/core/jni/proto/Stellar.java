package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Stellar {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\rStellar.proto\u0012\u0010TW.Stellar.Proto\"*\n\u0005Asset\u0012\u000e\n\u0006issuer\u0018\u0001 \u0001(\t\u0012\u0011\n\talphanum4\u0018\u0002 \u0001(\t\"=\n\u0016OperationCreateAccount\u0012\u0013\n\u000bdestination\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\u0003\"_\n\u0010OperationPayment\u0012\u0013\n\u000bdestination\u0018\u0001 \u0001(\t\u0012&\n\u0005asset\u0018\u0002 \u0001(\u000b2\u0017.TW.Stellar.Proto.Asset\u0012\u000e\n\u0006amount\u0018\u0003 \u0001(\u0003\"T\n\u0014OperationChangeTrust\u0012&\n\u0005asset\u0018\u0001 \u0001(\u000b2\u0017.TW.Stellar.Proto.Asset\u0012\u0014\n\fvalid_before\u0018\u0002 \u0001(\u0003\"\n\n\bMemoVoid\"\u0018\n\bMemoText\u0012\f\n\u0004text\u0018\u0001 \u0001(\t\"\u0014\n\u0006MemoId\u0012\n\n\u0002id\u0018\u0001 \u0001(\u0003\"\u0018\n\bMemoHash\u0012\f\n\u0004hash\u0018\u0001 \u0001(\f\"É\u0004\n\fSigningInput\u0012\u000b\n\u0003fee\u0018\u0001 \u0001(\u0005\u0012\u0010\n\bsequence\u0018\u0002 \u0001(\u0003\u0012\u000f\n\u0007account\u0018\u0003 \u0001(\t\u0012\u0013\n\u000bprivate_key\u0018\u0004 \u0001(\f\u0012\u0012\n\npassphrase\u0018\u0005 \u0001(\t\u0012E\n\u0011op_create_account\u0018\u0006 \u0001(\u000b2(.TW.Stellar.Proto.OperationCreateAccountH\u0000\u00128\n\nop_payment\u0018\u0007 \u0001(\u000b2\".TW.Stellar.Proto.OperationPaymentH\u0000\u0012A\n\u000fop_change_trust\u0018\b \u0001(\u000b2&.TW.Stellar.Proto.OperationChangeTrustH\u0000\u0012/\n\tmemo_void\u0018\t \u0001(\u000b2\u001a.TW.Stellar.Proto.MemoVoidH\u0001\u0012/\n\tmemo_text\u0018\n \u0001(\u000b2\u001a.TW.Stellar.Proto.MemoTextH\u0001\u0012+\n\u0007memo_id\u0018\u000b \u0001(\u000b2\u0018.TW.Stellar.Proto.MemoIdH\u0001\u0012/\n\tmemo_hash\u0018\f \u0001(\u000b2\u001a.TW.Stellar.Proto.MemoHashH\u0001\u00126\n\u0010memo_return_hash\u0018\r \u0001(\u000b2\u001a.TW.Stellar.Proto.MemoHashH\u0001B\u0011\n\u000foperation_oneofB\u0011\n\u000fmemo_type_oneof\"\"\n\rSigningOutput\u0012\u0011\n\tsignature\u0018\u0001 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Stellar_Proto_Asset_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_Asset_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_MemoHash_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_MemoHash_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_MemoId_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_MemoId_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_MemoText_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_MemoText_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_MemoVoid_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_MemoVoid_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_OperationChangeTrust_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_OperationChangeTrust_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_OperationCreateAccount_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_OperationCreateAccount_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_OperationPayment_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_OperationPayment_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Stellar_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Stellar_Proto_SigningOutput_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Stellar$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase;
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$OperationOneofCase;

        static {
            int[] iArr = new int[SigningInput.MemoTypeOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase = iArr;
            try {
                iArr[SigningInput.MemoTypeOneofCase.MEMO_VOID.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase[SigningInput.MemoTypeOneofCase.MEMO_TEXT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase[SigningInput.MemoTypeOneofCase.MEMO_ID.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase[SigningInput.MemoTypeOneofCase.MEMO_HASH.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase[SigningInput.MemoTypeOneofCase.MEMO_RETURN_HASH.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase[SigningInput.MemoTypeOneofCase.MEMOTYPEONEOF_NOT_SET.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[SigningInput.OperationOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$OperationOneofCase = iArr2;
            try {
                iArr2[SigningInput.OperationOneofCase.OP_CREATE_ACCOUNT.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$OperationOneofCase[SigningInput.OperationOneofCase.OP_PAYMENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$OperationOneofCase[SigningInput.OperationOneofCase.OP_CHANGE_TRUST.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$OperationOneofCase[SigningInput.OperationOneofCase.OPERATIONONEOF_NOT_SET.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class Asset extends GeneratedMessageV3 implements AssetOrBuilder {
        public static final int ALPHANUM4_FIELD_NUMBER = 2;
        public static final int ISSUER_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object alphanum4_;
        private volatile Object issuer_;
        private byte memoizedIsInitialized;
        private static final Asset DEFAULT_INSTANCE = new Asset();
        private static final t0<Asset> PARSER = new c<Asset>() { // from class: wallet.core.jni.proto.Stellar.Asset.1
            @Override // com.google.protobuf.t0
            public Asset parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Asset(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements AssetOrBuilder {
            private Object alphanum4_;
            private Object issuer_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_Asset_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAlphanum4() {
                this.alphanum4_ = Asset.getDefaultInstance().getAlphanum4();
                onChanged();
                return this;
            }

            public Builder clearIssuer() {
                this.issuer_ = Asset.getDefaultInstance().getIssuer();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
            public String getAlphanum4() {
                Object obj = this.alphanum4_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.alphanum4_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
            public ByteString getAlphanum4Bytes() {
                Object obj = this.alphanum4_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.alphanum4_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_Asset_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
            public String getIssuer() {
                Object obj = this.issuer_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.issuer_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
            public ByteString getIssuerBytes() {
                Object obj = this.issuer_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.issuer_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_Asset_fieldAccessorTable.d(Asset.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAlphanum4(String str) {
                Objects.requireNonNull(str);
                this.alphanum4_ = str;
                onChanged();
                return this;
            }

            public Builder setAlphanum4Bytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.alphanum4_ = byteString;
                onChanged();
                return this;
            }

            public Builder setIssuer(String str) {
                Objects.requireNonNull(str);
                this.issuer_ = str;
                onChanged();
                return this;
            }

            public Builder setIssuerBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.issuer_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.issuer_ = "";
                this.alphanum4_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Asset build() {
                Asset buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Asset buildPartial() {
                Asset asset = new Asset(this, (AnonymousClass1) null);
                asset.issuer_ = this.issuer_;
                asset.alphanum4_ = this.alphanum4_;
                onBuilt();
                return asset;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Asset getDefaultInstanceForType() {
                return Asset.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.issuer_ = "";
                this.alphanum4_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.issuer_ = "";
                this.alphanum4_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Asset) {
                    return mergeFrom((Asset) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Asset asset) {
                if (asset == Asset.getDefaultInstance()) {
                    return this;
                }
                if (!asset.getIssuer().isEmpty()) {
                    this.issuer_ = asset.issuer_;
                    onChanged();
                }
                if (!asset.getAlphanum4().isEmpty()) {
                    this.alphanum4_ = asset.alphanum4_;
                    onChanged();
                }
                mergeUnknownFields(asset.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.Asset.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.Asset.access$900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$Asset r3 = (wallet.core.jni.proto.Stellar.Asset) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$Asset r4 = (wallet.core.jni.proto.Stellar.Asset) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.Asset.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$Asset$Builder");
            }
        }

        public /* synthetic */ Asset(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Asset getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_Asset_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Asset parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Asset) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Asset parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Asset> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Asset)) {
                return super.equals(obj);
            }
            Asset asset = (Asset) obj;
            return getIssuer().equals(asset.getIssuer()) && getAlphanum4().equals(asset.getAlphanum4()) && this.unknownFields.equals(asset.unknownFields);
        }

        @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
        public String getAlphanum4() {
            Object obj = this.alphanum4_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.alphanum4_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
        public ByteString getAlphanum4Bytes() {
            Object obj = this.alphanum4_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.alphanum4_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
        public String getIssuer() {
            Object obj = this.issuer_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.issuer_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.AssetOrBuilder
        public ByteString getIssuerBytes() {
            Object obj = this.issuer_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.issuer_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Asset> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getIssuerBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.issuer_);
            if (!getAlphanum4Bytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.alphanum4_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getIssuer().hashCode()) * 37) + 2) * 53) + getAlphanum4().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_Asset_fieldAccessorTable.d(Asset.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Asset();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getIssuerBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.issuer_);
            }
            if (!getAlphanum4Bytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.alphanum4_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Asset(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Asset asset) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(asset);
        }

        public static Asset parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Asset(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Asset parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Asset parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Asset getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Asset parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Asset() {
            this.memoizedIsInitialized = (byte) -1;
            this.issuer_ = "";
            this.alphanum4_ = "";
        }

        public static Asset parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Asset parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Asset parseFrom(InputStream inputStream) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Asset(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.issuer_ = jVar.I();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.alphanum4_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Asset parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Asset parseFrom(j jVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Asset parseFrom(j jVar, r rVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface AssetOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getAlphanum4();

        ByteString getAlphanum4Bytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getIssuer();

        ByteString getIssuerBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class MemoHash extends GeneratedMessageV3 implements MemoHashOrBuilder {
        public static final int HASH_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString hash_;
        private byte memoizedIsInitialized;
        private static final MemoHash DEFAULT_INSTANCE = new MemoHash();
        private static final t0<MemoHash> PARSER = new c<MemoHash>() { // from class: wallet.core.jni.proto.Stellar.MemoHash.1
            @Override // com.google.protobuf.t0
            public MemoHash parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new MemoHash(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements MemoHashOrBuilder {
            private ByteString hash_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoHash_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearHash() {
                this.hash_ = MemoHash.getDefaultInstance().getHash();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoHash_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.MemoHashOrBuilder
            public ByteString getHash() {
                return this.hash_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoHash_fieldAccessorTable.d(MemoHash.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.hash_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.hash_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoHash build() {
                MemoHash buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoHash buildPartial() {
                MemoHash memoHash = new MemoHash(this, (AnonymousClass1) null);
                memoHash.hash_ = this.hash_;
                onBuilt();
                return memoHash;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public MemoHash getDefaultInstanceForType() {
                return MemoHash.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.hash_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.hash_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof MemoHash) {
                    return mergeFrom((MemoHash) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(MemoHash memoHash) {
                if (memoHash == MemoHash.getDefaultInstance()) {
                    return this;
                }
                if (memoHash.getHash() != ByteString.EMPTY) {
                    setHash(memoHash.getHash());
                }
                mergeUnknownFields(memoHash.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.MemoHash.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.MemoHash.access$8700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$MemoHash r3 = (wallet.core.jni.proto.Stellar.MemoHash) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$MemoHash r4 = (wallet.core.jni.proto.Stellar.MemoHash) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.MemoHash.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$MemoHash$Builder");
            }
        }

        public /* synthetic */ MemoHash(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static MemoHash getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoHash_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static MemoHash parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MemoHash) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static MemoHash parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<MemoHash> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MemoHash)) {
                return super.equals(obj);
            }
            MemoHash memoHash = (MemoHash) obj;
            return getHash().equals(memoHash.getHash()) && this.unknownFields.equals(memoHash.unknownFields);
        }

        @Override // wallet.core.jni.proto.Stellar.MemoHashOrBuilder
        public ByteString getHash() {
            return this.hash_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<MemoHash> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.hash_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.hash_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getHash().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoHash_fieldAccessorTable.d(MemoHash.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new MemoHash();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.hash_.isEmpty()) {
                codedOutputStream.q0(1, this.hash_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ MemoHash(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(MemoHash memoHash) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(memoHash);
        }

        public static MemoHash parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private MemoHash(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MemoHash parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoHash) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoHash parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public MemoHash getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static MemoHash parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private MemoHash() {
            this.memoizedIsInitialized = (byte) -1;
            this.hash_ = ByteString.EMPTY;
        }

        public static MemoHash parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static MemoHash parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static MemoHash parseFrom(InputStream inputStream) throws IOException {
            return (MemoHash) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private MemoHash(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.hash_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static MemoHash parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoHash) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoHash parseFrom(j jVar) throws IOException {
            return (MemoHash) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static MemoHash parseFrom(j jVar, r rVar) throws IOException {
            return (MemoHash) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface MemoHashOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getHash();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class MemoId extends GeneratedMessageV3 implements MemoIdOrBuilder {
        public static final int ID_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long id_;
        private byte memoizedIsInitialized;
        private static final MemoId DEFAULT_INSTANCE = new MemoId();
        private static final t0<MemoId> PARSER = new c<MemoId>() { // from class: wallet.core.jni.proto.Stellar.MemoId.1
            @Override // com.google.protobuf.t0
            public MemoId parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new MemoId(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements MemoIdOrBuilder {
            private long id_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoId_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearId() {
                this.id_ = 0L;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoId_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.MemoIdOrBuilder
            public long getId() {
                return this.id_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoId_fieldAccessorTable.d(MemoId.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setId(long j) {
                this.id_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoId build() {
                MemoId buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoId buildPartial() {
                MemoId memoId = new MemoId(this, (AnonymousClass1) null);
                memoId.id_ = this.id_;
                onBuilt();
                return memoId;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public MemoId getDefaultInstanceForType() {
                return MemoId.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.id_ = 0L;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof MemoId) {
                    return mergeFrom((MemoId) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(MemoId memoId) {
                if (memoId == MemoId.getDefaultInstance()) {
                    return this;
                }
                if (memoId.getId() != 0) {
                    setId(memoId.getId());
                }
                mergeUnknownFields(memoId.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.MemoId.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.MemoId.access$7700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$MemoId r3 = (wallet.core.jni.proto.Stellar.MemoId) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$MemoId r4 = (wallet.core.jni.proto.Stellar.MemoId) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.MemoId.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$MemoId$Builder");
            }
        }

        public /* synthetic */ MemoId(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static MemoId getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoId_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static MemoId parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MemoId) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static MemoId parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<MemoId> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MemoId)) {
                return super.equals(obj);
            }
            MemoId memoId = (MemoId) obj;
            return getId() == memoId.getId() && this.unknownFields.equals(memoId.unknownFields);
        }

        @Override // wallet.core.jni.proto.Stellar.MemoIdOrBuilder
        public long getId() {
            return this.id_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<MemoId> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.id_;
            int z = (j != 0 ? 0 + CodedOutputStream.z(1, j) : 0) + this.unknownFields.getSerializedSize();
            this.memoizedSize = z;
            return z;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getId())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoId_fieldAccessorTable.d(MemoId.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new MemoId();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.id_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ MemoId(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(MemoId memoId) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(memoId);
        }

        public static MemoId parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private MemoId(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MemoId parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoId) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoId parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public MemoId getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static MemoId parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private MemoId() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MemoId parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static MemoId parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private MemoId(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 8) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.id_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static MemoId parseFrom(InputStream inputStream) throws IOException {
            return (MemoId) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static MemoId parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoId) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoId parseFrom(j jVar) throws IOException {
            return (MemoId) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static MemoId parseFrom(j jVar, r rVar) throws IOException {
            return (MemoId) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface MemoIdOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getId();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class MemoText extends GeneratedMessageV3 implements MemoTextOrBuilder {
        private static final MemoText DEFAULT_INSTANCE = new MemoText();
        private static final t0<MemoText> PARSER = new c<MemoText>() { // from class: wallet.core.jni.proto.Stellar.MemoText.1
            @Override // com.google.protobuf.t0
            public MemoText parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new MemoText(jVar, rVar, null);
            }
        };
        public static final int TEXT_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object text_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements MemoTextOrBuilder {
            private Object text_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoText_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearText() {
                this.text_ = MemoText.getDefaultInstance().getText();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoText_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.MemoTextOrBuilder
            public String getText() {
                Object obj = this.text_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.text_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.MemoTextOrBuilder
            public ByteString getTextBytes() {
                Object obj = this.text_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.text_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoText_fieldAccessorTable.d(MemoText.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setText(String str) {
                Objects.requireNonNull(str);
                this.text_ = str;
                onChanged();
                return this;
            }

            public Builder setTextBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.text_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.text_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoText build() {
                MemoText buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoText buildPartial() {
                MemoText memoText = new MemoText(this, (AnonymousClass1) null);
                memoText.text_ = this.text_;
                onBuilt();
                return memoText;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public MemoText getDefaultInstanceForType() {
                return MemoText.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.text_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.text_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof MemoText) {
                    return mergeFrom((MemoText) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(MemoText memoText) {
                if (memoText == MemoText.getDefaultInstance()) {
                    return this;
                }
                if (!memoText.getText().isEmpty()) {
                    this.text_ = memoText.text_;
                    onChanged();
                }
                mergeUnknownFields(memoText.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.MemoText.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.MemoText.access$6600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$MemoText r3 = (wallet.core.jni.proto.Stellar.MemoText) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$MemoText r4 = (wallet.core.jni.proto.Stellar.MemoText) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.MemoText.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$MemoText$Builder");
            }
        }

        public /* synthetic */ MemoText(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static MemoText getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoText_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static MemoText parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MemoText) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static MemoText parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<MemoText> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MemoText)) {
                return super.equals(obj);
            }
            MemoText memoText = (MemoText) obj;
            return getText().equals(memoText.getText()) && this.unknownFields.equals(memoText.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<MemoText> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getTextBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.text_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // wallet.core.jni.proto.Stellar.MemoTextOrBuilder
        public String getText() {
            Object obj = this.text_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.text_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.MemoTextOrBuilder
        public ByteString getTextBytes() {
            Object obj = this.text_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.text_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getText().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoText_fieldAccessorTable.d(MemoText.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new MemoText();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getTextBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.text_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ MemoText(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(MemoText memoText) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(memoText);
        }

        public static MemoText parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private MemoText(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MemoText parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoText) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoText parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public MemoText getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static MemoText parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private MemoText() {
            this.memoizedIsInitialized = (byte) -1;
            this.text_ = "";
        }

        public static MemoText parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static MemoText parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static MemoText parseFrom(InputStream inputStream) throws IOException {
            return (MemoText) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private MemoText(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.text_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static MemoText parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoText) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoText parseFrom(j jVar) throws IOException {
            return (MemoText) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static MemoText parseFrom(j jVar, r rVar) throws IOException {
            return (MemoText) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface MemoTextOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getText();

        ByteString getTextBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class MemoVoid extends GeneratedMessageV3 implements MemoVoidOrBuilder {
        private static final MemoVoid DEFAULT_INSTANCE = new MemoVoid();
        private static final t0<MemoVoid> PARSER = new c<MemoVoid>() { // from class: wallet.core.jni.proto.Stellar.MemoVoid.1
            @Override // com.google.protobuf.t0
            public MemoVoid parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new MemoVoid(jVar, rVar, null);
            }
        };
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements MemoVoidOrBuilder {
            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoVoid_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoVoid_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_MemoVoid_fieldAccessorTable.d(MemoVoid.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoVoid build() {
                MemoVoid buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public MemoVoid buildPartial() {
                MemoVoid memoVoid = new MemoVoid(this, (AnonymousClass1) null);
                onBuilt();
                return memoVoid;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public MemoVoid getDefaultInstanceForType() {
                return MemoVoid.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof MemoVoid) {
                    return mergeFrom((MemoVoid) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(MemoVoid memoVoid) {
                if (memoVoid == MemoVoid.getDefaultInstance()) {
                    return this;
                }
                mergeUnknownFields(memoVoid.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.MemoVoid.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.MemoVoid.access$5600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$MemoVoid r3 = (wallet.core.jni.proto.Stellar.MemoVoid) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$MemoVoid r4 = (wallet.core.jni.proto.Stellar.MemoVoid) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.MemoVoid.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$MemoVoid$Builder");
            }
        }

        public /* synthetic */ MemoVoid(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static MemoVoid getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoVoid_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static MemoVoid parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MemoVoid) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static MemoVoid parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<MemoVoid> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof MemoVoid) {
                return this.unknownFields.equals(((MemoVoid) obj).unknownFields);
            }
            return super.equals(obj);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<MemoVoid> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int serializedSize = this.unknownFields.getSerializedSize() + 0;
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((779 + getDescriptor().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_MemoVoid_fieldAccessorTable.d(MemoVoid.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new MemoVoid();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ MemoVoid(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(MemoVoid memoVoid) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(memoVoid);
        }

        public static MemoVoid parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private MemoVoid(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MemoVoid parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoVoid) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoVoid parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public MemoVoid getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static MemoVoid parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private MemoVoid() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static MemoVoid parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static MemoVoid parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private MemoVoid(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J == 0 || !parseUnknownField(jVar, g, rVar, J)) {
                                z = true;
                            }
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static MemoVoid parseFrom(InputStream inputStream) throws IOException {
            return (MemoVoid) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static MemoVoid parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (MemoVoid) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static MemoVoid parseFrom(j jVar) throws IOException {
            return (MemoVoid) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static MemoVoid parseFrom(j jVar, r rVar) throws IOException {
            return (MemoVoid) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface MemoVoidOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class OperationChangeTrust extends GeneratedMessageV3 implements OperationChangeTrustOrBuilder {
        public static final int ASSET_FIELD_NUMBER = 1;
        private static final OperationChangeTrust DEFAULT_INSTANCE = new OperationChangeTrust();
        private static final t0<OperationChangeTrust> PARSER = new c<OperationChangeTrust>() { // from class: wallet.core.jni.proto.Stellar.OperationChangeTrust.1
            @Override // com.google.protobuf.t0
            public OperationChangeTrust parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new OperationChangeTrust(jVar, rVar, null);
            }
        };
        public static final int VALID_BEFORE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private Asset asset_;
        private byte memoizedIsInitialized;
        private long validBefore_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements OperationChangeTrustOrBuilder {
            private a1<Asset, Asset.Builder, AssetOrBuilder> assetBuilder_;
            private Asset asset_;
            private long validBefore_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<Asset, Asset.Builder, AssetOrBuilder> getAssetFieldBuilder() {
                if (this.assetBuilder_ == null) {
                    this.assetBuilder_ = new a1<>(getAsset(), getParentForChildren(), isClean());
                    this.asset_ = null;
                }
                return this.assetBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationChangeTrust_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAsset() {
                if (this.assetBuilder_ == null) {
                    this.asset_ = null;
                    onChanged();
                } else {
                    this.asset_ = null;
                    this.assetBuilder_ = null;
                }
                return this;
            }

            public Builder clearValidBefore() {
                this.validBefore_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
            public Asset getAsset() {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Asset asset = this.asset_;
                    return asset == null ? Asset.getDefaultInstance() : asset;
                }
                return a1Var.f();
            }

            public Asset.Builder getAssetBuilder() {
                onChanged();
                return getAssetFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
            public AssetOrBuilder getAssetOrBuilder() {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Asset asset = this.asset_;
                return asset == null ? Asset.getDefaultInstance() : asset;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationChangeTrust_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
            public long getValidBefore() {
                return this.validBefore_;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
            public boolean hasAsset() {
                return (this.assetBuilder_ == null && this.asset_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationChangeTrust_fieldAccessorTable.d(OperationChangeTrust.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeAsset(Asset asset) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Asset asset2 = this.asset_;
                    if (asset2 != null) {
                        this.asset_ = Asset.newBuilder(asset2).mergeFrom(asset).buildPartial();
                    } else {
                        this.asset_ = asset;
                    }
                    onChanged();
                } else {
                    a1Var.h(asset);
                }
                return this;
            }

            public Builder setAsset(Asset asset) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(asset);
                    this.asset_ = asset;
                    onChanged();
                } else {
                    a1Var.j(asset);
                }
                return this;
            }

            public Builder setValidBefore(long j) {
                this.validBefore_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationChangeTrust build() {
                OperationChangeTrust buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationChangeTrust buildPartial() {
                OperationChangeTrust operationChangeTrust = new OperationChangeTrust(this, (AnonymousClass1) null);
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    operationChangeTrust.asset_ = this.asset_;
                } else {
                    operationChangeTrust.asset_ = a1Var.b();
                }
                operationChangeTrust.validBefore_ = this.validBefore_;
                onBuilt();
                return operationChangeTrust;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public OperationChangeTrust getDefaultInstanceForType() {
                return OperationChangeTrust.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.assetBuilder_ == null) {
                    this.asset_ = null;
                } else {
                    this.asset_ = null;
                    this.assetBuilder_ = null;
                }
                this.validBefore_ = 0L;
                return this;
            }

            public Builder setAsset(Asset.Builder builder) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    this.asset_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof OperationChangeTrust) {
                    return mergeFrom((OperationChangeTrust) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(OperationChangeTrust operationChangeTrust) {
                if (operationChangeTrust == OperationChangeTrust.getDefaultInstance()) {
                    return this;
                }
                if (operationChangeTrust.hasAsset()) {
                    mergeAsset(operationChangeTrust.getAsset());
                }
                if (operationChangeTrust.getValidBefore() != 0) {
                    setValidBefore(operationChangeTrust.getValidBefore());
                }
                mergeUnknownFields(operationChangeTrust.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.OperationChangeTrust.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.OperationChangeTrust.access$4700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$OperationChangeTrust r3 = (wallet.core.jni.proto.Stellar.OperationChangeTrust) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$OperationChangeTrust r4 = (wallet.core.jni.proto.Stellar.OperationChangeTrust) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.OperationChangeTrust.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$OperationChangeTrust$Builder");
            }
        }

        public /* synthetic */ OperationChangeTrust(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static OperationChangeTrust getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_OperationChangeTrust_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static OperationChangeTrust parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OperationChangeTrust) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static OperationChangeTrust parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<OperationChangeTrust> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof OperationChangeTrust)) {
                return super.equals(obj);
            }
            OperationChangeTrust operationChangeTrust = (OperationChangeTrust) obj;
            if (hasAsset() != operationChangeTrust.hasAsset()) {
                return false;
            }
            return (!hasAsset() || getAsset().equals(operationChangeTrust.getAsset())) && getValidBefore() == operationChangeTrust.getValidBefore() && this.unknownFields.equals(operationChangeTrust.unknownFields);
        }

        @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
        public Asset getAsset() {
            Asset asset = this.asset_;
            return asset == null ? Asset.getDefaultInstance() : asset;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
        public AssetOrBuilder getAssetOrBuilder() {
            return getAsset();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<OperationChangeTrust> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.asset_ != null ? 0 + CodedOutputStream.G(1, getAsset()) : 0;
            long j = this.validBefore_;
            if (j != 0) {
                G += CodedOutputStream.z(2, j);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
        public long getValidBefore() {
            return this.validBefore_;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationChangeTrustOrBuilder
        public boolean hasAsset() {
            return this.asset_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasAsset()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getAsset().hashCode();
            }
            int h = (((((hashCode * 37) + 2) * 53) + a0.h(getValidBefore())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_OperationChangeTrust_fieldAccessorTable.d(OperationChangeTrust.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new OperationChangeTrust();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.asset_ != null) {
                codedOutputStream.K0(1, getAsset());
            }
            long j = this.validBefore_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ OperationChangeTrust(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(OperationChangeTrust operationChangeTrust) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(operationChangeTrust);
        }

        public static OperationChangeTrust parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private OperationChangeTrust(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OperationChangeTrust parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationChangeTrust) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationChangeTrust parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public OperationChangeTrust getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static OperationChangeTrust parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private OperationChangeTrust() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OperationChangeTrust parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static OperationChangeTrust parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private OperationChangeTrust(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                Asset asset = this.asset_;
                                Asset.Builder builder = asset != null ? asset.toBuilder() : null;
                                Asset asset2 = (Asset) jVar.z(Asset.parser(), rVar);
                                this.asset_ = asset2;
                                if (builder != null) {
                                    builder.mergeFrom(asset2);
                                    this.asset_ = builder.buildPartial();
                                }
                            } else if (J != 16) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.validBefore_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static OperationChangeTrust parseFrom(InputStream inputStream) throws IOException {
            return (OperationChangeTrust) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static OperationChangeTrust parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationChangeTrust) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationChangeTrust parseFrom(j jVar) throws IOException {
            return (OperationChangeTrust) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static OperationChangeTrust parseFrom(j jVar, r rVar) throws IOException {
            return (OperationChangeTrust) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface OperationChangeTrustOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Asset getAsset();

        AssetOrBuilder getAssetOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        long getValidBefore();

        boolean hasAsset();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class OperationCreateAccount extends GeneratedMessageV3 implements OperationCreateAccountOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 2;
        public static final int DESTINATION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object destination_;
        private byte memoizedIsInitialized;
        private static final OperationCreateAccount DEFAULT_INSTANCE = new OperationCreateAccount();
        private static final t0<OperationCreateAccount> PARSER = new c<OperationCreateAccount>() { // from class: wallet.core.jni.proto.Stellar.OperationCreateAccount.1
            @Override // com.google.protobuf.t0
            public OperationCreateAccount parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new OperationCreateAccount(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements OperationCreateAccountOrBuilder {
            private long amount_;
            private Object destination_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationCreateAccount_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearDestination() {
                this.destination_ = OperationCreateAccount.getDefaultInstance().getDestination();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationCreateAccountOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationCreateAccount_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationCreateAccountOrBuilder
            public String getDestination() {
                Object obj = this.destination_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.destination_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationCreateAccountOrBuilder
            public ByteString getDestinationBytes() {
                Object obj = this.destination_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.destination_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationCreateAccount_fieldAccessorTable.d(OperationCreateAccount.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setDestination(String str) {
                Objects.requireNonNull(str);
                this.destination_ = str;
                onChanged();
                return this;
            }

            public Builder setDestinationBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.destination_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.destination_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationCreateAccount build() {
                OperationCreateAccount buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationCreateAccount buildPartial() {
                OperationCreateAccount operationCreateAccount = new OperationCreateAccount(this, (AnonymousClass1) null);
                operationCreateAccount.destination_ = this.destination_;
                operationCreateAccount.amount_ = this.amount_;
                onBuilt();
                return operationCreateAccount;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public OperationCreateAccount getDefaultInstanceForType() {
                return OperationCreateAccount.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.destination_ = "";
                this.amount_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.destination_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof OperationCreateAccount) {
                    return mergeFrom((OperationCreateAccount) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(OperationCreateAccount operationCreateAccount) {
                if (operationCreateAccount == OperationCreateAccount.getDefaultInstance()) {
                    return this;
                }
                if (!operationCreateAccount.getDestination().isEmpty()) {
                    this.destination_ = operationCreateAccount.destination_;
                    onChanged();
                }
                if (operationCreateAccount.getAmount() != 0) {
                    setAmount(operationCreateAccount.getAmount());
                }
                mergeUnknownFields(operationCreateAccount.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.OperationCreateAccount.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.OperationCreateAccount.access$2200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$OperationCreateAccount r3 = (wallet.core.jni.proto.Stellar.OperationCreateAccount) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$OperationCreateAccount r4 = (wallet.core.jni.proto.Stellar.OperationCreateAccount) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.OperationCreateAccount.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$OperationCreateAccount$Builder");
            }
        }

        public /* synthetic */ OperationCreateAccount(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static OperationCreateAccount getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_OperationCreateAccount_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static OperationCreateAccount parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OperationCreateAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static OperationCreateAccount parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<OperationCreateAccount> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof OperationCreateAccount)) {
                return super.equals(obj);
            }
            OperationCreateAccount operationCreateAccount = (OperationCreateAccount) obj;
            return getDestination().equals(operationCreateAccount.getDestination()) && getAmount() == operationCreateAccount.getAmount() && this.unknownFields.equals(operationCreateAccount.unknownFields);
        }

        @Override // wallet.core.jni.proto.Stellar.OperationCreateAccountOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationCreateAccountOrBuilder
        public String getDestination() {
            Object obj = this.destination_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.destination_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationCreateAccountOrBuilder
        public ByteString getDestinationBytes() {
            Object obj = this.destination_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.destination_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<OperationCreateAccount> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getDestinationBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.destination_);
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(2, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDestination().hashCode()) * 37) + 2) * 53) + a0.h(getAmount())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_OperationCreateAccount_fieldAccessorTable.d(OperationCreateAccount.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new OperationCreateAccount();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDestinationBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.destination_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ OperationCreateAccount(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(OperationCreateAccount operationCreateAccount) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(operationCreateAccount);
        }

        public static OperationCreateAccount parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private OperationCreateAccount(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OperationCreateAccount parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationCreateAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationCreateAccount parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public OperationCreateAccount getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static OperationCreateAccount parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private OperationCreateAccount() {
            this.memoizedIsInitialized = (byte) -1;
            this.destination_ = "";
        }

        public static OperationCreateAccount parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static OperationCreateAccount parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static OperationCreateAccount parseFrom(InputStream inputStream) throws IOException {
            return (OperationCreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private OperationCreateAccount(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.destination_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.amount_ = jVar.y();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static OperationCreateAccount parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationCreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationCreateAccount parseFrom(j jVar) throws IOException {
            return (OperationCreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static OperationCreateAccount parseFrom(j jVar, r rVar) throws IOException {
            return (OperationCreateAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface OperationCreateAccountOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        String getDestination();

        ByteString getDestinationBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class OperationPayment extends GeneratedMessageV3 implements OperationPaymentOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 3;
        public static final int ASSET_FIELD_NUMBER = 2;
        public static final int DESTINATION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long amount_;
        private Asset asset_;
        private volatile Object destination_;
        private byte memoizedIsInitialized;
        private static final OperationPayment DEFAULT_INSTANCE = new OperationPayment();
        private static final t0<OperationPayment> PARSER = new c<OperationPayment>() { // from class: wallet.core.jni.proto.Stellar.OperationPayment.1
            @Override // com.google.protobuf.t0
            public OperationPayment parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new OperationPayment(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements OperationPaymentOrBuilder {
            private long amount_;
            private a1<Asset, Asset.Builder, AssetOrBuilder> assetBuilder_;
            private Asset asset_;
            private Object destination_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<Asset, Asset.Builder, AssetOrBuilder> getAssetFieldBuilder() {
                if (this.assetBuilder_ == null) {
                    this.assetBuilder_ = new a1<>(getAsset(), getParentForChildren(), isClean());
                    this.asset_ = null;
                }
                return this.assetBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationPayment_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearAsset() {
                if (this.assetBuilder_ == null) {
                    this.asset_ = null;
                    onChanged();
                } else {
                    this.asset_ = null;
                    this.assetBuilder_ = null;
                }
                return this;
            }

            public Builder clearDestination() {
                this.destination_ = OperationPayment.getDefaultInstance().getDestination();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
            public Asset getAsset() {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Asset asset = this.asset_;
                    return asset == null ? Asset.getDefaultInstance() : asset;
                }
                return a1Var.f();
            }

            public Asset.Builder getAssetBuilder() {
                onChanged();
                return getAssetFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
            public AssetOrBuilder getAssetOrBuilder() {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Asset asset = this.asset_;
                return asset == null ? Asset.getDefaultInstance() : asset;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationPayment_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
            public String getDestination() {
                Object obj = this.destination_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.destination_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
            public ByteString getDestinationBytes() {
                Object obj = this.destination_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.destination_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
            public boolean hasAsset() {
                return (this.assetBuilder_ == null && this.asset_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_OperationPayment_fieldAccessorTable.d(OperationPayment.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeAsset(Asset asset) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Asset asset2 = this.asset_;
                    if (asset2 != null) {
                        this.asset_ = Asset.newBuilder(asset2).mergeFrom(asset).buildPartial();
                    } else {
                        this.asset_ = asset;
                    }
                    onChanged();
                } else {
                    a1Var.h(asset);
                }
                return this;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setAsset(Asset asset) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(asset);
                    this.asset_ = asset;
                    onChanged();
                } else {
                    a1Var.j(asset);
                }
                return this;
            }

            public Builder setDestination(String str) {
                Objects.requireNonNull(str);
                this.destination_ = str;
                onChanged();
                return this;
            }

            public Builder setDestinationBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.destination_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.destination_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationPayment build() {
                OperationPayment buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationPayment buildPartial() {
                OperationPayment operationPayment = new OperationPayment(this, (AnonymousClass1) null);
                operationPayment.destination_ = this.destination_;
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    operationPayment.asset_ = this.asset_;
                } else {
                    operationPayment.asset_ = a1Var.b();
                }
                operationPayment.amount_ = this.amount_;
                onBuilt();
                return operationPayment;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public OperationPayment getDefaultInstanceForType() {
                return OperationPayment.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.destination_ = "";
                if (this.assetBuilder_ == null) {
                    this.asset_ = null;
                } else {
                    this.asset_ = null;
                    this.assetBuilder_ = null;
                }
                this.amount_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.destination_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder setAsset(Asset.Builder builder) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    this.asset_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof OperationPayment) {
                    return mergeFrom((OperationPayment) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(OperationPayment operationPayment) {
                if (operationPayment == OperationPayment.getDefaultInstance()) {
                    return this;
                }
                if (!operationPayment.getDestination().isEmpty()) {
                    this.destination_ = operationPayment.destination_;
                    onChanged();
                }
                if (operationPayment.hasAsset()) {
                    mergeAsset(operationPayment.getAsset());
                }
                if (operationPayment.getAmount() != 0) {
                    setAmount(operationPayment.getAmount());
                }
                mergeUnknownFields(operationPayment.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.OperationPayment.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.OperationPayment.access$3500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$OperationPayment r3 = (wallet.core.jni.proto.Stellar.OperationPayment) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$OperationPayment r4 = (wallet.core.jni.proto.Stellar.OperationPayment) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.OperationPayment.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$OperationPayment$Builder");
            }
        }

        public /* synthetic */ OperationPayment(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static OperationPayment getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_OperationPayment_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static OperationPayment parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OperationPayment) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static OperationPayment parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<OperationPayment> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof OperationPayment)) {
                return super.equals(obj);
            }
            OperationPayment operationPayment = (OperationPayment) obj;
            if (getDestination().equals(operationPayment.getDestination()) && hasAsset() == operationPayment.hasAsset()) {
                return (!hasAsset() || getAsset().equals(operationPayment.getAsset())) && getAmount() == operationPayment.getAmount() && this.unknownFields.equals(operationPayment.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
        public Asset getAsset() {
            Asset asset = this.asset_;
            return asset == null ? Asset.getDefaultInstance() : asset;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
        public AssetOrBuilder getAssetOrBuilder() {
            return getAsset();
        }

        @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
        public String getDestination() {
            Object obj = this.destination_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.destination_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
        public ByteString getDestinationBytes() {
            Object obj = this.destination_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.destination_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<OperationPayment> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getDestinationBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.destination_);
            if (this.asset_ != null) {
                computeStringSize += CodedOutputStream.G(2, getAsset());
            }
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(3, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Stellar.OperationPaymentOrBuilder
        public boolean hasAsset() {
            return this.asset_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDestination().hashCode();
            if (hasAsset()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getAsset().hashCode();
            }
            int h = (((((hashCode * 37) + 3) * 53) + a0.h(getAmount())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_OperationPayment_fieldAccessorTable.d(OperationPayment.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new OperationPayment();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDestinationBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.destination_);
            }
            if (this.asset_ != null) {
                codedOutputStream.K0(2, getAsset());
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(3, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ OperationPayment(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(OperationPayment operationPayment) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(operationPayment);
        }

        public static OperationPayment parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private OperationPayment(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OperationPayment parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationPayment) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationPayment parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public OperationPayment getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static OperationPayment parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private OperationPayment() {
            this.memoizedIsInitialized = (byte) -1;
            this.destination_ = "";
        }

        public static OperationPayment parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static OperationPayment parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static OperationPayment parseFrom(InputStream inputStream) throws IOException {
            return (OperationPayment) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private OperationPayment(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.destination_ = jVar.I();
                            } else if (J == 18) {
                                Asset asset = this.asset_;
                                Asset.Builder builder = asset != null ? asset.toBuilder() : null;
                                Asset asset2 = (Asset) jVar.z(Asset.parser(), rVar);
                                this.asset_ = asset2;
                                if (builder != null) {
                                    builder.mergeFrom(asset2);
                                    this.asset_ = builder.buildPartial();
                                }
                            } else if (J != 24) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.amount_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static OperationPayment parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationPayment) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationPayment parseFrom(j jVar) throws IOException {
            return (OperationPayment) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static OperationPayment parseFrom(j jVar, r rVar) throws IOException {
            return (OperationPayment) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface OperationPaymentOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        Asset getAsset();

        AssetOrBuilder getAssetOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        String getDestination();

        ByteString getDestinationBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasAsset();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int ACCOUNT_FIELD_NUMBER = 3;
        public static final int FEE_FIELD_NUMBER = 1;
        public static final int MEMO_HASH_FIELD_NUMBER = 12;
        public static final int MEMO_ID_FIELD_NUMBER = 11;
        public static final int MEMO_RETURN_HASH_FIELD_NUMBER = 13;
        public static final int MEMO_TEXT_FIELD_NUMBER = 10;
        public static final int MEMO_VOID_FIELD_NUMBER = 9;
        public static final int OP_CHANGE_TRUST_FIELD_NUMBER = 8;
        public static final int OP_CREATE_ACCOUNT_FIELD_NUMBER = 6;
        public static final int OP_PAYMENT_FIELD_NUMBER = 7;
        public static final int PASSPHRASE_FIELD_NUMBER = 5;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 4;
        public static final int SEQUENCE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private volatile Object account_;
        private int fee_;
        private int memoTypeOneofCase_;
        private Object memoTypeOneof_;
        private byte memoizedIsInitialized;
        private int operationOneofCase_;
        private Object operationOneof_;
        private volatile Object passphrase_;
        private ByteString privateKey_;
        private long sequence_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Stellar.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private Object account_;
            private int fee_;
            private a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> memoHashBuilder_;
            private a1<MemoId, MemoId.Builder, MemoIdOrBuilder> memoIdBuilder_;
            private a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> memoReturnHashBuilder_;
            private a1<MemoText, MemoText.Builder, MemoTextOrBuilder> memoTextBuilder_;
            private int memoTypeOneofCase_;
            private Object memoTypeOneof_;
            private a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> memoVoidBuilder_;
            private a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> opChangeTrustBuilder_;
            private a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> opCreateAccountBuilder_;
            private a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> opPaymentBuilder_;
            private int operationOneofCase_;
            private Object operationOneof_;
            private Object passphrase_;
            private ByteString privateKey_;
            private long sequence_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_SigningInput_descriptor;
            }

            private a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> getMemoHashFieldBuilder() {
                if (this.memoHashBuilder_ == null) {
                    if (this.memoTypeOneofCase_ != 12) {
                        this.memoTypeOneof_ = MemoHash.getDefaultInstance();
                    }
                    this.memoHashBuilder_ = new a1<>((MemoHash) this.memoTypeOneof_, getParentForChildren(), isClean());
                    this.memoTypeOneof_ = null;
                }
                this.memoTypeOneofCase_ = 12;
                onChanged();
                return this.memoHashBuilder_;
            }

            private a1<MemoId, MemoId.Builder, MemoIdOrBuilder> getMemoIdFieldBuilder() {
                if (this.memoIdBuilder_ == null) {
                    if (this.memoTypeOneofCase_ != 11) {
                        this.memoTypeOneof_ = MemoId.getDefaultInstance();
                    }
                    this.memoIdBuilder_ = new a1<>((MemoId) this.memoTypeOneof_, getParentForChildren(), isClean());
                    this.memoTypeOneof_ = null;
                }
                this.memoTypeOneofCase_ = 11;
                onChanged();
                return this.memoIdBuilder_;
            }

            private a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> getMemoReturnHashFieldBuilder() {
                if (this.memoReturnHashBuilder_ == null) {
                    if (this.memoTypeOneofCase_ != 13) {
                        this.memoTypeOneof_ = MemoHash.getDefaultInstance();
                    }
                    this.memoReturnHashBuilder_ = new a1<>((MemoHash) this.memoTypeOneof_, getParentForChildren(), isClean());
                    this.memoTypeOneof_ = null;
                }
                this.memoTypeOneofCase_ = 13;
                onChanged();
                return this.memoReturnHashBuilder_;
            }

            private a1<MemoText, MemoText.Builder, MemoTextOrBuilder> getMemoTextFieldBuilder() {
                if (this.memoTextBuilder_ == null) {
                    if (this.memoTypeOneofCase_ != 10) {
                        this.memoTypeOneof_ = MemoText.getDefaultInstance();
                    }
                    this.memoTextBuilder_ = new a1<>((MemoText) this.memoTypeOneof_, getParentForChildren(), isClean());
                    this.memoTypeOneof_ = null;
                }
                this.memoTypeOneofCase_ = 10;
                onChanged();
                return this.memoTextBuilder_;
            }

            private a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> getMemoVoidFieldBuilder() {
                if (this.memoVoidBuilder_ == null) {
                    if (this.memoTypeOneofCase_ != 9) {
                        this.memoTypeOneof_ = MemoVoid.getDefaultInstance();
                    }
                    this.memoVoidBuilder_ = new a1<>((MemoVoid) this.memoTypeOneof_, getParentForChildren(), isClean());
                    this.memoTypeOneof_ = null;
                }
                this.memoTypeOneofCase_ = 9;
                onChanged();
                return this.memoVoidBuilder_;
            }

            private a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> getOpChangeTrustFieldBuilder() {
                if (this.opChangeTrustBuilder_ == null) {
                    if (this.operationOneofCase_ != 8) {
                        this.operationOneof_ = OperationChangeTrust.getDefaultInstance();
                    }
                    this.opChangeTrustBuilder_ = new a1<>((OperationChangeTrust) this.operationOneof_, getParentForChildren(), isClean());
                    this.operationOneof_ = null;
                }
                this.operationOneofCase_ = 8;
                onChanged();
                return this.opChangeTrustBuilder_;
            }

            private a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> getOpCreateAccountFieldBuilder() {
                if (this.opCreateAccountBuilder_ == null) {
                    if (this.operationOneofCase_ != 6) {
                        this.operationOneof_ = OperationCreateAccount.getDefaultInstance();
                    }
                    this.opCreateAccountBuilder_ = new a1<>((OperationCreateAccount) this.operationOneof_, getParentForChildren(), isClean());
                    this.operationOneof_ = null;
                }
                this.operationOneofCase_ = 6;
                onChanged();
                return this.opCreateAccountBuilder_;
            }

            private a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> getOpPaymentFieldBuilder() {
                if (this.opPaymentBuilder_ == null) {
                    if (this.operationOneofCase_ != 7) {
                        this.operationOneof_ = OperationPayment.getDefaultInstance();
                    }
                    this.opPaymentBuilder_ = new a1<>((OperationPayment) this.operationOneof_, getParentForChildren(), isClean());
                    this.operationOneof_ = null;
                }
                this.operationOneofCase_ = 7;
                onChanged();
                return this.opPaymentBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAccount() {
                this.account_ = SigningInput.getDefaultInstance().getAccount();
                onChanged();
                return this;
            }

            public Builder clearFee() {
                this.fee_ = 0;
                onChanged();
                return this;
            }

            public Builder clearMemoHash() {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoHashBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 12) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.memoTypeOneofCase_ == 12) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMemoId() {
                a1<MemoId, MemoId.Builder, MemoIdOrBuilder> a1Var = this.memoIdBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 11) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.memoTypeOneofCase_ == 11) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMemoReturnHash() {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoReturnHashBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 13) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.memoTypeOneofCase_ == 13) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMemoText() {
                a1<MemoText, MemoText.Builder, MemoTextOrBuilder> a1Var = this.memoTextBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 10) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.memoTypeOneofCase_ == 10) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMemoTypeOneof() {
                this.memoTypeOneofCase_ = 0;
                this.memoTypeOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearMemoVoid() {
                a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> a1Var = this.memoVoidBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 9) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.memoTypeOneofCase_ == 9) {
                        this.memoTypeOneofCase_ = 0;
                        this.memoTypeOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearOpChangeTrust() {
                a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> a1Var = this.opChangeTrustBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 8) {
                        this.operationOneofCase_ = 0;
                        this.operationOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.operationOneofCase_ == 8) {
                        this.operationOneofCase_ = 0;
                        this.operationOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearOpCreateAccount() {
                a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> a1Var = this.opCreateAccountBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 6) {
                        this.operationOneofCase_ = 0;
                        this.operationOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.operationOneofCase_ == 6) {
                        this.operationOneofCase_ = 0;
                        this.operationOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearOpPayment() {
                a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> a1Var = this.opPaymentBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 7) {
                        this.operationOneofCase_ = 0;
                        this.operationOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.operationOneofCase_ == 7) {
                        this.operationOneofCase_ = 0;
                        this.operationOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearOperationOneof() {
                this.operationOneofCase_ = 0;
                this.operationOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearPassphrase() {
                this.passphrase_ = SigningInput.getDefaultInstance().getPassphrase();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearSequence() {
                this.sequence_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public String getAccount() {
                Object obj = this.account_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.account_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public ByteString getAccountBytes() {
                Object obj = this.account_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.account_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public int getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoHash getMemoHash() {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoHashBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 12) {
                        return (MemoHash) this.memoTypeOneof_;
                    }
                    return MemoHash.getDefaultInstance();
                } else if (this.memoTypeOneofCase_ == 12) {
                    return a1Var.f();
                } else {
                    return MemoHash.getDefaultInstance();
                }
            }

            public MemoHash.Builder getMemoHashBuilder() {
                return getMemoHashFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoHashOrBuilder getMemoHashOrBuilder() {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var;
                int i = this.memoTypeOneofCase_;
                if (i != 12 || (a1Var = this.memoHashBuilder_) == null) {
                    if (i == 12) {
                        return (MemoHash) this.memoTypeOneof_;
                    }
                    return MemoHash.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoId getMemoId() {
                a1<MemoId, MemoId.Builder, MemoIdOrBuilder> a1Var = this.memoIdBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 11) {
                        return (MemoId) this.memoTypeOneof_;
                    }
                    return MemoId.getDefaultInstance();
                } else if (this.memoTypeOneofCase_ == 11) {
                    return a1Var.f();
                } else {
                    return MemoId.getDefaultInstance();
                }
            }

            public MemoId.Builder getMemoIdBuilder() {
                return getMemoIdFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoIdOrBuilder getMemoIdOrBuilder() {
                a1<MemoId, MemoId.Builder, MemoIdOrBuilder> a1Var;
                int i = this.memoTypeOneofCase_;
                if (i != 11 || (a1Var = this.memoIdBuilder_) == null) {
                    if (i == 11) {
                        return (MemoId) this.memoTypeOneof_;
                    }
                    return MemoId.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoHash getMemoReturnHash() {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoReturnHashBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 13) {
                        return (MemoHash) this.memoTypeOneof_;
                    }
                    return MemoHash.getDefaultInstance();
                } else if (this.memoTypeOneofCase_ == 13) {
                    return a1Var.f();
                } else {
                    return MemoHash.getDefaultInstance();
                }
            }

            public MemoHash.Builder getMemoReturnHashBuilder() {
                return getMemoReturnHashFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoHashOrBuilder getMemoReturnHashOrBuilder() {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var;
                int i = this.memoTypeOneofCase_;
                if (i != 13 || (a1Var = this.memoReturnHashBuilder_) == null) {
                    if (i == 13) {
                        return (MemoHash) this.memoTypeOneof_;
                    }
                    return MemoHash.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoText getMemoText() {
                a1<MemoText, MemoText.Builder, MemoTextOrBuilder> a1Var = this.memoTextBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 10) {
                        return (MemoText) this.memoTypeOneof_;
                    }
                    return MemoText.getDefaultInstance();
                } else if (this.memoTypeOneofCase_ == 10) {
                    return a1Var.f();
                } else {
                    return MemoText.getDefaultInstance();
                }
            }

            public MemoText.Builder getMemoTextBuilder() {
                return getMemoTextFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoTextOrBuilder getMemoTextOrBuilder() {
                a1<MemoText, MemoText.Builder, MemoTextOrBuilder> a1Var;
                int i = this.memoTypeOneofCase_;
                if (i != 10 || (a1Var = this.memoTextBuilder_) == null) {
                    if (i == 10) {
                        return (MemoText) this.memoTypeOneof_;
                    }
                    return MemoText.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoTypeOneofCase getMemoTypeOneofCase() {
                return MemoTypeOneofCase.forNumber(this.memoTypeOneofCase_);
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoVoid getMemoVoid() {
                a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> a1Var = this.memoVoidBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 9) {
                        return (MemoVoid) this.memoTypeOneof_;
                    }
                    return MemoVoid.getDefaultInstance();
                } else if (this.memoTypeOneofCase_ == 9) {
                    return a1Var.f();
                } else {
                    return MemoVoid.getDefaultInstance();
                }
            }

            public MemoVoid.Builder getMemoVoidBuilder() {
                return getMemoVoidFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public MemoVoidOrBuilder getMemoVoidOrBuilder() {
                a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> a1Var;
                int i = this.memoTypeOneofCase_;
                if (i != 9 || (a1Var = this.memoVoidBuilder_) == null) {
                    if (i == 9) {
                        return (MemoVoid) this.memoTypeOneof_;
                    }
                    return MemoVoid.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public OperationChangeTrust getOpChangeTrust() {
                a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> a1Var = this.opChangeTrustBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 8) {
                        return (OperationChangeTrust) this.operationOneof_;
                    }
                    return OperationChangeTrust.getDefaultInstance();
                } else if (this.operationOneofCase_ == 8) {
                    return a1Var.f();
                } else {
                    return OperationChangeTrust.getDefaultInstance();
                }
            }

            public OperationChangeTrust.Builder getOpChangeTrustBuilder() {
                return getOpChangeTrustFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public OperationChangeTrustOrBuilder getOpChangeTrustOrBuilder() {
                a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> a1Var;
                int i = this.operationOneofCase_;
                if (i != 8 || (a1Var = this.opChangeTrustBuilder_) == null) {
                    if (i == 8) {
                        return (OperationChangeTrust) this.operationOneof_;
                    }
                    return OperationChangeTrust.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public OperationCreateAccount getOpCreateAccount() {
                a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> a1Var = this.opCreateAccountBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 6) {
                        return (OperationCreateAccount) this.operationOneof_;
                    }
                    return OperationCreateAccount.getDefaultInstance();
                } else if (this.operationOneofCase_ == 6) {
                    return a1Var.f();
                } else {
                    return OperationCreateAccount.getDefaultInstance();
                }
            }

            public OperationCreateAccount.Builder getOpCreateAccountBuilder() {
                return getOpCreateAccountFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public OperationCreateAccountOrBuilder getOpCreateAccountOrBuilder() {
                a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> a1Var;
                int i = this.operationOneofCase_;
                if (i != 6 || (a1Var = this.opCreateAccountBuilder_) == null) {
                    if (i == 6) {
                        return (OperationCreateAccount) this.operationOneof_;
                    }
                    return OperationCreateAccount.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public OperationPayment getOpPayment() {
                a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> a1Var = this.opPaymentBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 7) {
                        return (OperationPayment) this.operationOneof_;
                    }
                    return OperationPayment.getDefaultInstance();
                } else if (this.operationOneofCase_ == 7) {
                    return a1Var.f();
                } else {
                    return OperationPayment.getDefaultInstance();
                }
            }

            public OperationPayment.Builder getOpPaymentBuilder() {
                return getOpPaymentFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public OperationPaymentOrBuilder getOpPaymentOrBuilder() {
                a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> a1Var;
                int i = this.operationOneofCase_;
                if (i != 7 || (a1Var = this.opPaymentBuilder_) == null) {
                    if (i == 7) {
                        return (OperationPayment) this.operationOneof_;
                    }
                    return OperationPayment.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public OperationOneofCase getOperationOneofCase() {
                return OperationOneofCase.forNumber(this.operationOneofCase_);
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public String getPassphrase() {
                Object obj = this.passphrase_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.passphrase_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public ByteString getPassphraseBytes() {
                Object obj = this.passphrase_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.passphrase_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public long getSequence() {
                return this.sequence_;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasMemoHash() {
                return this.memoTypeOneofCase_ == 12;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasMemoId() {
                return this.memoTypeOneofCase_ == 11;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasMemoReturnHash() {
                return this.memoTypeOneofCase_ == 13;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasMemoText() {
                return this.memoTypeOneofCase_ == 10;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasMemoVoid() {
                return this.memoTypeOneofCase_ == 9;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasOpChangeTrust() {
                return this.operationOneofCase_ == 8;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasOpCreateAccount() {
                return this.operationOneofCase_ == 6;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
            public boolean hasOpPayment() {
                return this.operationOneofCase_ == 7;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeMemoHash(MemoHash memoHash) {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoHashBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 12 && this.memoTypeOneof_ != MemoHash.getDefaultInstance()) {
                        this.memoTypeOneof_ = MemoHash.newBuilder((MemoHash) this.memoTypeOneof_).mergeFrom(memoHash).buildPartial();
                    } else {
                        this.memoTypeOneof_ = memoHash;
                    }
                    onChanged();
                } else {
                    if (this.memoTypeOneofCase_ == 12) {
                        a1Var.h(memoHash);
                    }
                    this.memoHashBuilder_.j(memoHash);
                }
                this.memoTypeOneofCase_ = 12;
                return this;
            }

            public Builder mergeMemoId(MemoId memoId) {
                a1<MemoId, MemoId.Builder, MemoIdOrBuilder> a1Var = this.memoIdBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 11 && this.memoTypeOneof_ != MemoId.getDefaultInstance()) {
                        this.memoTypeOneof_ = MemoId.newBuilder((MemoId) this.memoTypeOneof_).mergeFrom(memoId).buildPartial();
                    } else {
                        this.memoTypeOneof_ = memoId;
                    }
                    onChanged();
                } else {
                    if (this.memoTypeOneofCase_ == 11) {
                        a1Var.h(memoId);
                    }
                    this.memoIdBuilder_.j(memoId);
                }
                this.memoTypeOneofCase_ = 11;
                return this;
            }

            public Builder mergeMemoReturnHash(MemoHash memoHash) {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoReturnHashBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 13 && this.memoTypeOneof_ != MemoHash.getDefaultInstance()) {
                        this.memoTypeOneof_ = MemoHash.newBuilder((MemoHash) this.memoTypeOneof_).mergeFrom(memoHash).buildPartial();
                    } else {
                        this.memoTypeOneof_ = memoHash;
                    }
                    onChanged();
                } else {
                    if (this.memoTypeOneofCase_ == 13) {
                        a1Var.h(memoHash);
                    }
                    this.memoReturnHashBuilder_.j(memoHash);
                }
                this.memoTypeOneofCase_ = 13;
                return this;
            }

            public Builder mergeMemoText(MemoText memoText) {
                a1<MemoText, MemoText.Builder, MemoTextOrBuilder> a1Var = this.memoTextBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 10 && this.memoTypeOneof_ != MemoText.getDefaultInstance()) {
                        this.memoTypeOneof_ = MemoText.newBuilder((MemoText) this.memoTypeOneof_).mergeFrom(memoText).buildPartial();
                    } else {
                        this.memoTypeOneof_ = memoText;
                    }
                    onChanged();
                } else {
                    if (this.memoTypeOneofCase_ == 10) {
                        a1Var.h(memoText);
                    }
                    this.memoTextBuilder_.j(memoText);
                }
                this.memoTypeOneofCase_ = 10;
                return this;
            }

            public Builder mergeMemoVoid(MemoVoid memoVoid) {
                a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> a1Var = this.memoVoidBuilder_;
                if (a1Var == null) {
                    if (this.memoTypeOneofCase_ == 9 && this.memoTypeOneof_ != MemoVoid.getDefaultInstance()) {
                        this.memoTypeOneof_ = MemoVoid.newBuilder((MemoVoid) this.memoTypeOneof_).mergeFrom(memoVoid).buildPartial();
                    } else {
                        this.memoTypeOneof_ = memoVoid;
                    }
                    onChanged();
                } else {
                    if (this.memoTypeOneofCase_ == 9) {
                        a1Var.h(memoVoid);
                    }
                    this.memoVoidBuilder_.j(memoVoid);
                }
                this.memoTypeOneofCase_ = 9;
                return this;
            }

            public Builder mergeOpChangeTrust(OperationChangeTrust operationChangeTrust) {
                a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> a1Var = this.opChangeTrustBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 8 && this.operationOneof_ != OperationChangeTrust.getDefaultInstance()) {
                        this.operationOneof_ = OperationChangeTrust.newBuilder((OperationChangeTrust) this.operationOneof_).mergeFrom(operationChangeTrust).buildPartial();
                    } else {
                        this.operationOneof_ = operationChangeTrust;
                    }
                    onChanged();
                } else {
                    if (this.operationOneofCase_ == 8) {
                        a1Var.h(operationChangeTrust);
                    }
                    this.opChangeTrustBuilder_.j(operationChangeTrust);
                }
                this.operationOneofCase_ = 8;
                return this;
            }

            public Builder mergeOpCreateAccount(OperationCreateAccount operationCreateAccount) {
                a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> a1Var = this.opCreateAccountBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 6 && this.operationOneof_ != OperationCreateAccount.getDefaultInstance()) {
                        this.operationOneof_ = OperationCreateAccount.newBuilder((OperationCreateAccount) this.operationOneof_).mergeFrom(operationCreateAccount).buildPartial();
                    } else {
                        this.operationOneof_ = operationCreateAccount;
                    }
                    onChanged();
                } else {
                    if (this.operationOneofCase_ == 6) {
                        a1Var.h(operationCreateAccount);
                    }
                    this.opCreateAccountBuilder_.j(operationCreateAccount);
                }
                this.operationOneofCase_ = 6;
                return this;
            }

            public Builder mergeOpPayment(OperationPayment operationPayment) {
                a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> a1Var = this.opPaymentBuilder_;
                if (a1Var == null) {
                    if (this.operationOneofCase_ == 7 && this.operationOneof_ != OperationPayment.getDefaultInstance()) {
                        this.operationOneof_ = OperationPayment.newBuilder((OperationPayment) this.operationOneof_).mergeFrom(operationPayment).buildPartial();
                    } else {
                        this.operationOneof_ = operationPayment;
                    }
                    onChanged();
                } else {
                    if (this.operationOneofCase_ == 7) {
                        a1Var.h(operationPayment);
                    }
                    this.opPaymentBuilder_.j(operationPayment);
                }
                this.operationOneofCase_ = 7;
                return this;
            }

            public Builder setAccount(String str) {
                Objects.requireNonNull(str);
                this.account_ = str;
                onChanged();
                return this;
            }

            public Builder setAccountBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.account_ = byteString;
                onChanged();
                return this;
            }

            public Builder setFee(int i) {
                this.fee_ = i;
                onChanged();
                return this;
            }

            public Builder setMemoHash(MemoHash memoHash) {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoHashBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(memoHash);
                    this.memoTypeOneof_ = memoHash;
                    onChanged();
                } else {
                    a1Var.j(memoHash);
                }
                this.memoTypeOneofCase_ = 12;
                return this;
            }

            public Builder setMemoId(MemoId memoId) {
                a1<MemoId, MemoId.Builder, MemoIdOrBuilder> a1Var = this.memoIdBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(memoId);
                    this.memoTypeOneof_ = memoId;
                    onChanged();
                } else {
                    a1Var.j(memoId);
                }
                this.memoTypeOneofCase_ = 11;
                return this;
            }

            public Builder setMemoReturnHash(MemoHash memoHash) {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoReturnHashBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(memoHash);
                    this.memoTypeOneof_ = memoHash;
                    onChanged();
                } else {
                    a1Var.j(memoHash);
                }
                this.memoTypeOneofCase_ = 13;
                return this;
            }

            public Builder setMemoText(MemoText memoText) {
                a1<MemoText, MemoText.Builder, MemoTextOrBuilder> a1Var = this.memoTextBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(memoText);
                    this.memoTypeOneof_ = memoText;
                    onChanged();
                } else {
                    a1Var.j(memoText);
                }
                this.memoTypeOneofCase_ = 10;
                return this;
            }

            public Builder setMemoVoid(MemoVoid memoVoid) {
                a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> a1Var = this.memoVoidBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(memoVoid);
                    this.memoTypeOneof_ = memoVoid;
                    onChanged();
                } else {
                    a1Var.j(memoVoid);
                }
                this.memoTypeOneofCase_ = 9;
                return this;
            }

            public Builder setOpChangeTrust(OperationChangeTrust operationChangeTrust) {
                a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> a1Var = this.opChangeTrustBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(operationChangeTrust);
                    this.operationOneof_ = operationChangeTrust;
                    onChanged();
                } else {
                    a1Var.j(operationChangeTrust);
                }
                this.operationOneofCase_ = 8;
                return this;
            }

            public Builder setOpCreateAccount(OperationCreateAccount operationCreateAccount) {
                a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> a1Var = this.opCreateAccountBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(operationCreateAccount);
                    this.operationOneof_ = operationCreateAccount;
                    onChanged();
                } else {
                    a1Var.j(operationCreateAccount);
                }
                this.operationOneofCase_ = 6;
                return this;
            }

            public Builder setOpPayment(OperationPayment operationPayment) {
                a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> a1Var = this.opPaymentBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(operationPayment);
                    this.operationOneof_ = operationPayment;
                    onChanged();
                } else {
                    a1Var.j(operationPayment);
                }
                this.operationOneofCase_ = 7;
                return this;
            }

            public Builder setPassphrase(String str) {
                Objects.requireNonNull(str);
                this.passphrase_ = str;
                onChanged();
                return this;
            }

            public Builder setPassphraseBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.passphrase_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSequence(long j) {
                this.sequence_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.operationOneofCase_ = 0;
                this.memoTypeOneofCase_ = 0;
                this.account_ = "";
                this.privateKey_ = ByteString.EMPTY;
                this.passphrase_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.fee_ = this.fee_;
                signingInput.sequence_ = this.sequence_;
                signingInput.account_ = this.account_;
                signingInput.privateKey_ = this.privateKey_;
                signingInput.passphrase_ = this.passphrase_;
                if (this.operationOneofCase_ == 6) {
                    a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> a1Var = this.opCreateAccountBuilder_;
                    if (a1Var == null) {
                        signingInput.operationOneof_ = this.operationOneof_;
                    } else {
                        signingInput.operationOneof_ = a1Var.b();
                    }
                }
                if (this.operationOneofCase_ == 7) {
                    a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> a1Var2 = this.opPaymentBuilder_;
                    if (a1Var2 == null) {
                        signingInput.operationOneof_ = this.operationOneof_;
                    } else {
                        signingInput.operationOneof_ = a1Var2.b();
                    }
                }
                if (this.operationOneofCase_ == 8) {
                    a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> a1Var3 = this.opChangeTrustBuilder_;
                    if (a1Var3 == null) {
                        signingInput.operationOneof_ = this.operationOneof_;
                    } else {
                        signingInput.operationOneof_ = a1Var3.b();
                    }
                }
                if (this.memoTypeOneofCase_ == 9) {
                    a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> a1Var4 = this.memoVoidBuilder_;
                    if (a1Var4 == null) {
                        signingInput.memoTypeOneof_ = this.memoTypeOneof_;
                    } else {
                        signingInput.memoTypeOneof_ = a1Var4.b();
                    }
                }
                if (this.memoTypeOneofCase_ == 10) {
                    a1<MemoText, MemoText.Builder, MemoTextOrBuilder> a1Var5 = this.memoTextBuilder_;
                    if (a1Var5 == null) {
                        signingInput.memoTypeOneof_ = this.memoTypeOneof_;
                    } else {
                        signingInput.memoTypeOneof_ = a1Var5.b();
                    }
                }
                if (this.memoTypeOneofCase_ == 11) {
                    a1<MemoId, MemoId.Builder, MemoIdOrBuilder> a1Var6 = this.memoIdBuilder_;
                    if (a1Var6 == null) {
                        signingInput.memoTypeOneof_ = this.memoTypeOneof_;
                    } else {
                        signingInput.memoTypeOneof_ = a1Var6.b();
                    }
                }
                if (this.memoTypeOneofCase_ == 12) {
                    a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var7 = this.memoHashBuilder_;
                    if (a1Var7 == null) {
                        signingInput.memoTypeOneof_ = this.memoTypeOneof_;
                    } else {
                        signingInput.memoTypeOneof_ = a1Var7.b();
                    }
                }
                if (this.memoTypeOneofCase_ == 13) {
                    a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var8 = this.memoReturnHashBuilder_;
                    if (a1Var8 == null) {
                        signingInput.memoTypeOneof_ = this.memoTypeOneof_;
                    } else {
                        signingInput.memoTypeOneof_ = a1Var8.b();
                    }
                }
                signingInput.operationOneofCase_ = this.operationOneofCase_;
                signingInput.memoTypeOneofCase_ = this.memoTypeOneofCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.fee_ = 0;
                this.sequence_ = 0L;
                this.account_ = "";
                this.privateKey_ = ByteString.EMPTY;
                this.passphrase_ = "";
                this.operationOneofCase_ = 0;
                this.operationOneof_ = null;
                this.memoTypeOneofCase_ = 0;
                this.memoTypeOneof_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setMemoHash(MemoHash.Builder builder) {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoHashBuilder_;
                if (a1Var == null) {
                    this.memoTypeOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.memoTypeOneofCase_ = 12;
                return this;
            }

            public Builder setMemoId(MemoId.Builder builder) {
                a1<MemoId, MemoId.Builder, MemoIdOrBuilder> a1Var = this.memoIdBuilder_;
                if (a1Var == null) {
                    this.memoTypeOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.memoTypeOneofCase_ = 11;
                return this;
            }

            public Builder setMemoReturnHash(MemoHash.Builder builder) {
                a1<MemoHash, MemoHash.Builder, MemoHashOrBuilder> a1Var = this.memoReturnHashBuilder_;
                if (a1Var == null) {
                    this.memoTypeOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.memoTypeOneofCase_ = 13;
                return this;
            }

            public Builder setMemoText(MemoText.Builder builder) {
                a1<MemoText, MemoText.Builder, MemoTextOrBuilder> a1Var = this.memoTextBuilder_;
                if (a1Var == null) {
                    this.memoTypeOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.memoTypeOneofCase_ = 10;
                return this;
            }

            public Builder setMemoVoid(MemoVoid.Builder builder) {
                a1<MemoVoid, MemoVoid.Builder, MemoVoidOrBuilder> a1Var = this.memoVoidBuilder_;
                if (a1Var == null) {
                    this.memoTypeOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.memoTypeOneofCase_ = 9;
                return this;
            }

            public Builder setOpChangeTrust(OperationChangeTrust.Builder builder) {
                a1<OperationChangeTrust, OperationChangeTrust.Builder, OperationChangeTrustOrBuilder> a1Var = this.opChangeTrustBuilder_;
                if (a1Var == null) {
                    this.operationOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.operationOneofCase_ = 8;
                return this;
            }

            public Builder setOpCreateAccount(OperationCreateAccount.Builder builder) {
                a1<OperationCreateAccount, OperationCreateAccount.Builder, OperationCreateAccountOrBuilder> a1Var = this.opCreateAccountBuilder_;
                if (a1Var == null) {
                    this.operationOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.operationOneofCase_ = 6;
                return this;
            }

            public Builder setOpPayment(OperationPayment.Builder builder) {
                a1<OperationPayment, OperationPayment.Builder, OperationPaymentOrBuilder> a1Var = this.opPaymentBuilder_;
                if (a1Var == null) {
                    this.operationOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.operationOneofCase_ = 7;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.operationOneofCase_ = 0;
                this.memoTypeOneofCase_ = 0;
                this.account_ = "";
                this.privateKey_ = ByteString.EMPTY;
                this.passphrase_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getFee() != 0) {
                    setFee(signingInput.getFee());
                }
                if (signingInput.getSequence() != 0) {
                    setSequence(signingInput.getSequence());
                }
                if (!signingInput.getAccount().isEmpty()) {
                    this.account_ = signingInput.account_;
                    onChanged();
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (!signingInput.getPassphrase().isEmpty()) {
                    this.passphrase_ = signingInput.passphrase_;
                    onChanged();
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$OperationOneofCase[signingInput.getOperationOneofCase().ordinal()];
                if (i == 1) {
                    mergeOpCreateAccount(signingInput.getOpCreateAccount());
                } else if (i == 2) {
                    mergeOpPayment(signingInput.getOpPayment());
                } else if (i == 3) {
                    mergeOpChangeTrust(signingInput.getOpChangeTrust());
                }
                int i2 = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Stellar$SigningInput$MemoTypeOneofCase[signingInput.getMemoTypeOneofCase().ordinal()];
                if (i2 == 1) {
                    mergeMemoVoid(signingInput.getMemoVoid());
                } else if (i2 == 2) {
                    mergeMemoText(signingInput.getMemoText());
                } else if (i2 == 3) {
                    mergeMemoId(signingInput.getMemoId());
                } else if (i2 == 4) {
                    mergeMemoHash(signingInput.getMemoHash());
                } else if (i2 == 5) {
                    mergeMemoReturnHash(signingInput.getMemoReturnHash());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.SigningInput.access$10500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$SigningInput r3 = (wallet.core.jni.proto.Stellar.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$SigningInput r4 = (wallet.core.jni.proto.Stellar.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MemoTypeOneofCase implements a0.c {
            MEMO_VOID(9),
            MEMO_TEXT(10),
            MEMO_ID(11),
            MEMO_HASH(12),
            MEMO_RETURN_HASH(13),
            MEMOTYPEONEOF_NOT_SET(0);
            
            private final int value;

            MemoTypeOneofCase(int i) {
                this.value = i;
            }

            public static MemoTypeOneofCase forNumber(int i) {
                if (i != 0) {
                    switch (i) {
                        case 9:
                            return MEMO_VOID;
                        case 10:
                            return MEMO_TEXT;
                        case 11:
                            return MEMO_ID;
                        case 12:
                            return MEMO_HASH;
                        case 13:
                            return MEMO_RETURN_HASH;
                        default:
                            return null;
                    }
                }
                return MEMOTYPEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MemoTypeOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public enum OperationOneofCase implements a0.c {
            OP_CREATE_ACCOUNT(6),
            OP_PAYMENT(7),
            OP_CHANGE_TRUST(8),
            OPERATIONONEOF_NOT_SET(0);
            
            private final int value;

            OperationOneofCase(int i) {
                this.value = i;
            }

            public static OperationOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 6) {
                        if (i != 7) {
                            if (i != 8) {
                                return null;
                            }
                            return OP_CHANGE_TRUST;
                        }
                        return OP_PAYMENT;
                    }
                    return OP_CREATE_ACCOUNT;
                }
                return OPERATIONONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static OperationOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getFee() == signingInput.getFee() && getSequence() == signingInput.getSequence() && getAccount().equals(signingInput.getAccount()) && getPrivateKey().equals(signingInput.getPrivateKey()) && getPassphrase().equals(signingInput.getPassphrase()) && getOperationOneofCase().equals(signingInput.getOperationOneofCase())) {
                int i = this.operationOneofCase_;
                if (i != 6) {
                    if (i != 7) {
                        if (i == 8 && !getOpChangeTrust().equals(signingInput.getOpChangeTrust())) {
                            return false;
                        }
                    } else if (!getOpPayment().equals(signingInput.getOpPayment())) {
                        return false;
                    }
                } else if (!getOpCreateAccount().equals(signingInput.getOpCreateAccount())) {
                    return false;
                }
                if (getMemoTypeOneofCase().equals(signingInput.getMemoTypeOneofCase())) {
                    switch (this.memoTypeOneofCase_) {
                        case 9:
                            if (!getMemoVoid().equals(signingInput.getMemoVoid())) {
                                return false;
                            }
                            break;
                        case 10:
                            if (!getMemoText().equals(signingInput.getMemoText())) {
                                return false;
                            }
                            break;
                        case 11:
                            if (!getMemoId().equals(signingInput.getMemoId())) {
                                return false;
                            }
                            break;
                        case 12:
                            if (!getMemoHash().equals(signingInput.getMemoHash())) {
                                return false;
                            }
                            break;
                        case 13:
                            if (!getMemoReturnHash().equals(signingInput.getMemoReturnHash())) {
                                return false;
                            }
                            break;
                    }
                    return this.unknownFields.equals(signingInput.unknownFields);
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public String getAccount() {
            Object obj = this.account_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.account_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public ByteString getAccountBytes() {
            Object obj = this.account_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.account_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public int getFee() {
            return this.fee_;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoHash getMemoHash() {
            if (this.memoTypeOneofCase_ == 12) {
                return (MemoHash) this.memoTypeOneof_;
            }
            return MemoHash.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoHashOrBuilder getMemoHashOrBuilder() {
            if (this.memoTypeOneofCase_ == 12) {
                return (MemoHash) this.memoTypeOneof_;
            }
            return MemoHash.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoId getMemoId() {
            if (this.memoTypeOneofCase_ == 11) {
                return (MemoId) this.memoTypeOneof_;
            }
            return MemoId.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoIdOrBuilder getMemoIdOrBuilder() {
            if (this.memoTypeOneofCase_ == 11) {
                return (MemoId) this.memoTypeOneof_;
            }
            return MemoId.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoHash getMemoReturnHash() {
            if (this.memoTypeOneofCase_ == 13) {
                return (MemoHash) this.memoTypeOneof_;
            }
            return MemoHash.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoHashOrBuilder getMemoReturnHashOrBuilder() {
            if (this.memoTypeOneofCase_ == 13) {
                return (MemoHash) this.memoTypeOneof_;
            }
            return MemoHash.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoText getMemoText() {
            if (this.memoTypeOneofCase_ == 10) {
                return (MemoText) this.memoTypeOneof_;
            }
            return MemoText.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoTextOrBuilder getMemoTextOrBuilder() {
            if (this.memoTypeOneofCase_ == 10) {
                return (MemoText) this.memoTypeOneof_;
            }
            return MemoText.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoTypeOneofCase getMemoTypeOneofCase() {
            return MemoTypeOneofCase.forNumber(this.memoTypeOneofCase_);
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoVoid getMemoVoid() {
            if (this.memoTypeOneofCase_ == 9) {
                return (MemoVoid) this.memoTypeOneof_;
            }
            return MemoVoid.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public MemoVoidOrBuilder getMemoVoidOrBuilder() {
            if (this.memoTypeOneofCase_ == 9) {
                return (MemoVoid) this.memoTypeOneof_;
            }
            return MemoVoid.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public OperationChangeTrust getOpChangeTrust() {
            if (this.operationOneofCase_ == 8) {
                return (OperationChangeTrust) this.operationOneof_;
            }
            return OperationChangeTrust.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public OperationChangeTrustOrBuilder getOpChangeTrustOrBuilder() {
            if (this.operationOneofCase_ == 8) {
                return (OperationChangeTrust) this.operationOneof_;
            }
            return OperationChangeTrust.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public OperationCreateAccount getOpCreateAccount() {
            if (this.operationOneofCase_ == 6) {
                return (OperationCreateAccount) this.operationOneof_;
            }
            return OperationCreateAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public OperationCreateAccountOrBuilder getOpCreateAccountOrBuilder() {
            if (this.operationOneofCase_ == 6) {
                return (OperationCreateAccount) this.operationOneof_;
            }
            return OperationCreateAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public OperationPayment getOpPayment() {
            if (this.operationOneofCase_ == 7) {
                return (OperationPayment) this.operationOneof_;
            }
            return OperationPayment.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public OperationPaymentOrBuilder getOpPaymentOrBuilder() {
            if (this.operationOneofCase_ == 7) {
                return (OperationPayment) this.operationOneof_;
            }
            return OperationPayment.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public OperationOneofCase getOperationOneofCase() {
            return OperationOneofCase.forNumber(this.operationOneofCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public String getPassphrase() {
            Object obj = this.passphrase_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.passphrase_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public ByteString getPassphraseBytes() {
            Object obj = this.passphrase_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.passphrase_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public long getSequence() {
            return this.sequence_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.fee_;
            int x = i2 != 0 ? 0 + CodedOutputStream.x(1, i2) : 0;
            long j = this.sequence_;
            if (j != 0) {
                x += CodedOutputStream.z(2, j);
            }
            if (!getAccountBytes().isEmpty()) {
                x += GeneratedMessageV3.computeStringSize(3, this.account_);
            }
            if (!this.privateKey_.isEmpty()) {
                x += CodedOutputStream.h(4, this.privateKey_);
            }
            if (!getPassphraseBytes().isEmpty()) {
                x += GeneratedMessageV3.computeStringSize(5, this.passphrase_);
            }
            if (this.operationOneofCase_ == 6) {
                x += CodedOutputStream.G(6, (OperationCreateAccount) this.operationOneof_);
            }
            if (this.operationOneofCase_ == 7) {
                x += CodedOutputStream.G(7, (OperationPayment) this.operationOneof_);
            }
            if (this.operationOneofCase_ == 8) {
                x += CodedOutputStream.G(8, (OperationChangeTrust) this.operationOneof_);
            }
            if (this.memoTypeOneofCase_ == 9) {
                x += CodedOutputStream.G(9, (MemoVoid) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 10) {
                x += CodedOutputStream.G(10, (MemoText) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 11) {
                x += CodedOutputStream.G(11, (MemoId) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 12) {
                x += CodedOutputStream.G(12, (MemoHash) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 13) {
                x += CodedOutputStream.G(13, (MemoHash) this.memoTypeOneof_);
            }
            int serializedSize = x + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasMemoHash() {
            return this.memoTypeOneofCase_ == 12;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasMemoId() {
            return this.memoTypeOneofCase_ == 11;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasMemoReturnHash() {
            return this.memoTypeOneofCase_ == 13;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasMemoText() {
            return this.memoTypeOneofCase_ == 10;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasMemoVoid() {
            return this.memoTypeOneofCase_ == 9;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasOpChangeTrust() {
            return this.operationOneofCase_ == 8;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasOpCreateAccount() {
            return this.operationOneofCase_ == 6;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningInputOrBuilder
        public boolean hasOpPayment() {
            return this.operationOneofCase_ == 7;
        }

        /* JADX WARN: Removed duplicated region for block: B:19:0x0094  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x00a3  */
        /* JADX WARN: Removed duplicated region for block: B:21:0x00b2  */
        /* JADX WARN: Removed duplicated region for block: B:22:0x00c1  */
        /* JADX WARN: Removed duplicated region for block: B:23:0x00d0  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x00df A[PHI: r0 
          PHI: (r0v27 int) = (r0v26 int), (r0v43 int) binds: [B:17:0x0090, B:24:0x00de] A[DONT_GENERATE, DONT_INLINE]] */
        @Override // com.google.protobuf.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public int hashCode() {
            /*
                Method dump skipped, instructions count: 250
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.SigningInput.hashCode():int");
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.fee_;
            if (i != 0) {
                codedOutputStream.G0(1, i);
            }
            long j = this.sequence_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            if (!getAccountBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.account_);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(4, this.privateKey_);
            }
            if (!getPassphraseBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.passphrase_);
            }
            if (this.operationOneofCase_ == 6) {
                codedOutputStream.K0(6, (OperationCreateAccount) this.operationOneof_);
            }
            if (this.operationOneofCase_ == 7) {
                codedOutputStream.K0(7, (OperationPayment) this.operationOneof_);
            }
            if (this.operationOneofCase_ == 8) {
                codedOutputStream.K0(8, (OperationChangeTrust) this.operationOneof_);
            }
            if (this.memoTypeOneofCase_ == 9) {
                codedOutputStream.K0(9, (MemoVoid) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 10) {
                codedOutputStream.K0(10, (MemoText) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 11) {
                codedOutputStream.K0(11, (MemoId) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 12) {
                codedOutputStream.K0(12, (MemoHash) this.memoTypeOneof_);
            }
            if (this.memoTypeOneofCase_ == 13) {
                codedOutputStream.K0(13, (MemoHash) this.memoTypeOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.operationOneofCase_ = 0;
            this.memoTypeOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private SigningInput() {
            this.operationOneofCase_ = 0;
            this.memoTypeOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.account_ = "";
            this.privateKey_ = ByteString.EMPTY;
            this.passphrase_ = "";
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 8:
                                this.fee_ = jVar.x();
                                continue;
                            case 16:
                                this.sequence_ = jVar.y();
                                continue;
                            case 26:
                                this.account_ = jVar.I();
                                continue;
                            case 34:
                                this.privateKey_ = jVar.q();
                                continue;
                            case 42:
                                this.passphrase_ = jVar.I();
                                continue;
                            case 50:
                                OperationCreateAccount.Builder builder = this.operationOneofCase_ == 6 ? ((OperationCreateAccount) this.operationOneof_).toBuilder() : null;
                                m0 z2 = jVar.z(OperationCreateAccount.parser(), rVar);
                                this.operationOneof_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((OperationCreateAccount) z2);
                                    this.operationOneof_ = builder.buildPartial();
                                }
                                this.operationOneofCase_ = 6;
                                continue;
                            case 58:
                                OperationPayment.Builder builder2 = this.operationOneofCase_ == 7 ? ((OperationPayment) this.operationOneof_).toBuilder() : null;
                                m0 z3 = jVar.z(OperationPayment.parser(), rVar);
                                this.operationOneof_ = z3;
                                if (builder2 != null) {
                                    builder2.mergeFrom((OperationPayment) z3);
                                    this.operationOneof_ = builder2.buildPartial();
                                }
                                this.operationOneofCase_ = 7;
                                continue;
                            case 66:
                                OperationChangeTrust.Builder builder3 = this.operationOneofCase_ == 8 ? ((OperationChangeTrust) this.operationOneof_).toBuilder() : null;
                                m0 z4 = jVar.z(OperationChangeTrust.parser(), rVar);
                                this.operationOneof_ = z4;
                                if (builder3 != null) {
                                    builder3.mergeFrom((OperationChangeTrust) z4);
                                    this.operationOneof_ = builder3.buildPartial();
                                }
                                this.operationOneofCase_ = 8;
                                continue;
                            case 74:
                                MemoVoid.Builder builder4 = this.memoTypeOneofCase_ == 9 ? ((MemoVoid) this.memoTypeOneof_).toBuilder() : null;
                                m0 z5 = jVar.z(MemoVoid.parser(), rVar);
                                this.memoTypeOneof_ = z5;
                                if (builder4 != null) {
                                    builder4.mergeFrom((MemoVoid) z5);
                                    this.memoTypeOneof_ = builder4.buildPartial();
                                }
                                this.memoTypeOneofCase_ = 9;
                                continue;
                            case 82:
                                MemoText.Builder builder5 = this.memoTypeOneofCase_ == 10 ? ((MemoText) this.memoTypeOneof_).toBuilder() : null;
                                m0 z6 = jVar.z(MemoText.parser(), rVar);
                                this.memoTypeOneof_ = z6;
                                if (builder5 != null) {
                                    builder5.mergeFrom((MemoText) z6);
                                    this.memoTypeOneof_ = builder5.buildPartial();
                                }
                                this.memoTypeOneofCase_ = 10;
                                continue;
                            case 90:
                                MemoId.Builder builder6 = this.memoTypeOneofCase_ == 11 ? ((MemoId) this.memoTypeOneof_).toBuilder() : null;
                                m0 z7 = jVar.z(MemoId.parser(), rVar);
                                this.memoTypeOneof_ = z7;
                                if (builder6 != null) {
                                    builder6.mergeFrom((MemoId) z7);
                                    this.memoTypeOneof_ = builder6.buildPartial();
                                }
                                this.memoTypeOneofCase_ = 11;
                                continue;
                            case 98:
                                MemoHash.Builder builder7 = this.memoTypeOneofCase_ == 12 ? ((MemoHash) this.memoTypeOneof_).toBuilder() : null;
                                m0 z8 = jVar.z(MemoHash.parser(), rVar);
                                this.memoTypeOneof_ = z8;
                                if (builder7 != null) {
                                    builder7.mergeFrom((MemoHash) z8);
                                    this.memoTypeOneof_ = builder7.buildPartial();
                                }
                                this.memoTypeOneofCase_ = 12;
                                continue;
                            case 106:
                                MemoHash.Builder builder8 = this.memoTypeOneofCase_ == 13 ? ((MemoHash) this.memoTypeOneof_).toBuilder() : null;
                                m0 z9 = jVar.z(MemoHash.parser(), rVar);
                                this.memoTypeOneof_ = z9;
                                if (builder8 != null) {
                                    builder8.mergeFrom((MemoHash) z9);
                                    this.memoTypeOneof_ = builder8.buildPartial();
                                }
                                this.memoTypeOneofCase_ = 13;
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        String getAccount();

        ByteString getAccountBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        int getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        MemoHash getMemoHash();

        MemoHashOrBuilder getMemoHashOrBuilder();

        MemoId getMemoId();

        MemoIdOrBuilder getMemoIdOrBuilder();

        MemoHash getMemoReturnHash();

        MemoHashOrBuilder getMemoReturnHashOrBuilder();

        MemoText getMemoText();

        MemoTextOrBuilder getMemoTextOrBuilder();

        SigningInput.MemoTypeOneofCase getMemoTypeOneofCase();

        MemoVoid getMemoVoid();

        MemoVoidOrBuilder getMemoVoidOrBuilder();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        OperationChangeTrust getOpChangeTrust();

        OperationChangeTrustOrBuilder getOpChangeTrustOrBuilder();

        OperationCreateAccount getOpCreateAccount();

        OperationCreateAccountOrBuilder getOpCreateAccountOrBuilder();

        OperationPayment getOpPayment();

        OperationPaymentOrBuilder getOpPaymentOrBuilder();

        SigningInput.OperationOneofCase getOperationOneofCase();

        String getPassphrase();

        ByteString getPassphraseBytes();

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        long getSequence();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasMemoHash();

        boolean hasMemoId();

        boolean hasMemoReturnHash();

        boolean hasMemoText();

        boolean hasMemoVoid();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasOpChangeTrust();

        boolean hasOpCreateAccount();

        boolean hasOpPayment();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Stellar.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };
        public static final int SIGNATURE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object signature_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private Object signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Stellar.internal_static_TW_Stellar_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Stellar.internal_static_TW_Stellar_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningOutputOrBuilder
            public String getSignature() {
                Object obj = this.signature_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.signature_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Stellar.SigningOutputOrBuilder
            public ByteString getSignatureBytes() {
                Object obj = this.signature_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.signature_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Stellar.internal_static_TW_Stellar_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setSignature(String str) {
                Objects.requireNonNull(str);
                this.signature_ = str;
                onChanged();
                return this;
            }

            public Builder setSignatureBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.signature_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.signature_ = this.signature_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.signature_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.signature_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (!signingOutput.getSignature().isEmpty()) {
                    this.signature_ = signingOutput.signature_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Stellar.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Stellar.SigningOutput.access$11700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Stellar$SigningOutput r3 = (wallet.core.jni.proto.Stellar.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Stellar$SigningOutput r4 = (wallet.core.jni.proto.Stellar.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Stellar.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Stellar$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Stellar.internal_static_TW_Stellar_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getSignature().equals(signingOutput.getSignature()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getSignatureBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.signature_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningOutputOrBuilder
        public String getSignature() {
            Object obj = this.signature_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.signature_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Stellar.SigningOutputOrBuilder
        public ByteString getSignatureBytes() {
            Object obj = this.signature_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.signature_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSignature().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Stellar.internal_static_TW_Stellar_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getSignatureBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.signature_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.signature_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.signature_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSignature();

        ByteString getSignatureBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Stellar_Proto_Asset_descriptor = bVar;
        internal_static_TW_Stellar_Proto_Asset_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Issuer", "Alphanum4"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Stellar_Proto_OperationCreateAccount_descriptor = bVar2;
        internal_static_TW_Stellar_Proto_OperationCreateAccount_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Destination", "Amount"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Stellar_Proto_OperationPayment_descriptor = bVar3;
        internal_static_TW_Stellar_Proto_OperationPayment_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Destination", "Asset", "Amount"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Stellar_Proto_OperationChangeTrust_descriptor = bVar4;
        internal_static_TW_Stellar_Proto_OperationChangeTrust_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Asset", "ValidBefore"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_Stellar_Proto_MemoVoid_descriptor = bVar5;
        internal_static_TW_Stellar_Proto_MemoVoid_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[0]);
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_Stellar_Proto_MemoText_descriptor = bVar6;
        internal_static_TW_Stellar_Proto_MemoText_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"Text"});
        Descriptors.b bVar7 = getDescriptor().o().get(6);
        internal_static_TW_Stellar_Proto_MemoId_descriptor = bVar7;
        internal_static_TW_Stellar_Proto_MemoId_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"Id"});
        Descriptors.b bVar8 = getDescriptor().o().get(7);
        internal_static_TW_Stellar_Proto_MemoHash_descriptor = bVar8;
        internal_static_TW_Stellar_Proto_MemoHash_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"Hash"});
        Descriptors.b bVar9 = getDescriptor().o().get(8);
        internal_static_TW_Stellar_Proto_SigningInput_descriptor = bVar9;
        internal_static_TW_Stellar_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"Fee", "Sequence", "Account", "PrivateKey", "Passphrase", "OpCreateAccount", "OpPayment", "OpChangeTrust", "MemoVoid", "MemoText", "MemoId", "MemoHash", "MemoReturnHash", "OperationOneof", "MemoTypeOneof"});
        Descriptors.b bVar10 = getDescriptor().o().get(9);
        internal_static_TW_Stellar_Proto_SigningOutput_descriptor = bVar10;
        internal_static_TW_Stellar_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"Signature"});
    }

    private Stellar() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
