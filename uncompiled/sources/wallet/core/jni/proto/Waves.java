package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Waves {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000bWaves.proto\u0012\u000eTW.Waves.Proto\"p\n\u000fTransferMessage\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\u0003\u0012\r\n\u0005asset\u0018\u0002 \u0001(\t\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0003\u0012\u0011\n\tfee_asset\u0018\u0004 \u0001(\t\u0012\n\n\u0002to\u0018\u0005 \u0001(\t\u0012\u0012\n\nattachment\u0018\u0006 \u0001(\f\"7\n\fLeaseMessage\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\u0003\u0012\n\n\u0002to\u0018\u0002 \u0001(\t\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0003\"3\n\u0012CancelLeaseMessage\u0012\u0010\n\blease_id\u0018\u0001 \u0001(\t\u0012\u000b\n\u0003fee\u0018\u0002 \u0001(\u0003\"ÿ\u0001\n\fSigningInput\u0012\u0011\n\ttimestamp\u0018\u0001 \u0001(\u0003\u0012\u0013\n\u000bprivate_key\u0018\u0002 \u0001(\f\u0012;\n\u0010transfer_message\u0018\u0003 \u0001(\u000b2\u001f.TW.Waves.Proto.TransferMessageH\u0000\u00125\n\rlease_message\u0018\u0004 \u0001(\u000b2\u001c.TW.Waves.Proto.LeaseMessageH\u0000\u0012B\n\u0014cancel_lease_message\u0018\u0005 \u0001(\u000b2\".TW.Waves.Proto.CancelLeaseMessageH\u0000B\u000f\n\rmessage_oneof\"0\n\rSigningOutput\u0012\u0011\n\tsignature\u0018\u0001 \u0001(\f\u0012\f\n\u0004json\u0018\u0002 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Waves_Proto_CancelLeaseMessage_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Waves_Proto_CancelLeaseMessage_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Waves_Proto_LeaseMessage_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Waves_Proto_LeaseMessage_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Waves_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Waves_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Waves_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Waves_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Waves_Proto_TransferMessage_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Waves_Proto_TransferMessage_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Waves$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Waves$SigningInput$MessageOneofCase;

        static {
            int[] iArr = new int[SigningInput.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Waves$SigningInput$MessageOneofCase = iArr;
            try {
                iArr[SigningInput.MessageOneofCase.TRANSFER_MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Waves$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.LEASE_MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Waves$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.CANCEL_LEASE_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Waves$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class CancelLeaseMessage extends GeneratedMessageV3 implements CancelLeaseMessageOrBuilder {
        public static final int FEE_FIELD_NUMBER = 2;
        public static final int LEASE_ID_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long fee_;
        private volatile Object leaseId_;
        private byte memoizedIsInitialized;
        private static final CancelLeaseMessage DEFAULT_INSTANCE = new CancelLeaseMessage();
        private static final t0<CancelLeaseMessage> PARSER = new c<CancelLeaseMessage>() { // from class: wallet.core.jni.proto.Waves.CancelLeaseMessage.1
            @Override // com.google.protobuf.t0
            public CancelLeaseMessage parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new CancelLeaseMessage(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements CancelLeaseMessageOrBuilder {
            private long fee_;
            private Object leaseId_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Waves.internal_static_TW_Waves_Proto_CancelLeaseMessage_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearLeaseId() {
                this.leaseId_ = CancelLeaseMessage.getDefaultInstance().getLeaseId();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Waves.internal_static_TW_Waves_Proto_CancelLeaseMessage_descriptor;
            }

            @Override // wallet.core.jni.proto.Waves.CancelLeaseMessageOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Waves.CancelLeaseMessageOrBuilder
            public String getLeaseId() {
                Object obj = this.leaseId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.leaseId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Waves.CancelLeaseMessageOrBuilder
            public ByteString getLeaseIdBytes() {
                Object obj = this.leaseId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.leaseId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Waves.internal_static_TW_Waves_Proto_CancelLeaseMessage_fieldAccessorTable.d(CancelLeaseMessage.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setLeaseId(String str) {
                Objects.requireNonNull(str);
                this.leaseId_ = str;
                onChanged();
                return this;
            }

            public Builder setLeaseIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.leaseId_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.leaseId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CancelLeaseMessage build() {
                CancelLeaseMessage buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CancelLeaseMessage buildPartial() {
                CancelLeaseMessage cancelLeaseMessage = new CancelLeaseMessage(this, (AnonymousClass1) null);
                cancelLeaseMessage.leaseId_ = this.leaseId_;
                cancelLeaseMessage.fee_ = this.fee_;
                onBuilt();
                return cancelLeaseMessage;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public CancelLeaseMessage getDefaultInstanceForType() {
                return CancelLeaseMessage.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.leaseId_ = "";
                this.fee_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.leaseId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof CancelLeaseMessage) {
                    return mergeFrom((CancelLeaseMessage) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(CancelLeaseMessage cancelLeaseMessage) {
                if (cancelLeaseMessage == CancelLeaseMessage.getDefaultInstance()) {
                    return this;
                }
                if (!cancelLeaseMessage.getLeaseId().isEmpty()) {
                    this.leaseId_ = cancelLeaseMessage.leaseId_;
                    onChanged();
                }
                if (cancelLeaseMessage.getFee() != 0) {
                    setFee(cancelLeaseMessage.getFee());
                }
                mergeUnknownFields(cancelLeaseMessage.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Waves.CancelLeaseMessage.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Waves.CancelLeaseMessage.access$4000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Waves$CancelLeaseMessage r3 = (wallet.core.jni.proto.Waves.CancelLeaseMessage) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Waves$CancelLeaseMessage r4 = (wallet.core.jni.proto.Waves.CancelLeaseMessage) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Waves.CancelLeaseMessage.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Waves$CancelLeaseMessage$Builder");
            }
        }

        public /* synthetic */ CancelLeaseMessage(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static CancelLeaseMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Waves.internal_static_TW_Waves_Proto_CancelLeaseMessage_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static CancelLeaseMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (CancelLeaseMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static CancelLeaseMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<CancelLeaseMessage> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof CancelLeaseMessage)) {
                return super.equals(obj);
            }
            CancelLeaseMessage cancelLeaseMessage = (CancelLeaseMessage) obj;
            return getLeaseId().equals(cancelLeaseMessage.getLeaseId()) && getFee() == cancelLeaseMessage.getFee() && this.unknownFields.equals(cancelLeaseMessage.unknownFields);
        }

        @Override // wallet.core.jni.proto.Waves.CancelLeaseMessageOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // wallet.core.jni.proto.Waves.CancelLeaseMessageOrBuilder
        public String getLeaseId() {
            Object obj = this.leaseId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.leaseId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Waves.CancelLeaseMessageOrBuilder
        public ByteString getLeaseIdBytes() {
            Object obj = this.leaseId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.leaseId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<CancelLeaseMessage> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getLeaseIdBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.leaseId_);
            long j = this.fee_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(2, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getLeaseId().hashCode()) * 37) + 2) * 53) + a0.h(getFee())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Waves.internal_static_TW_Waves_Proto_CancelLeaseMessage_fieldAccessorTable.d(CancelLeaseMessage.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new CancelLeaseMessage();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getLeaseIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.leaseId_);
            }
            long j = this.fee_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ CancelLeaseMessage(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(CancelLeaseMessage cancelLeaseMessage) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(cancelLeaseMessage);
        }

        public static CancelLeaseMessage parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private CancelLeaseMessage(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static CancelLeaseMessage parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (CancelLeaseMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static CancelLeaseMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public CancelLeaseMessage getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static CancelLeaseMessage parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private CancelLeaseMessage() {
            this.memoizedIsInitialized = (byte) -1;
            this.leaseId_ = "";
        }

        public static CancelLeaseMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static CancelLeaseMessage parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static CancelLeaseMessage parseFrom(InputStream inputStream) throws IOException {
            return (CancelLeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private CancelLeaseMessage(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.leaseId_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.fee_ = jVar.y();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static CancelLeaseMessage parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (CancelLeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static CancelLeaseMessage parseFrom(j jVar) throws IOException {
            return (CancelLeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static CancelLeaseMessage parseFrom(j jVar, r rVar) throws IOException {
            return (CancelLeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface CancelLeaseMessageOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        long getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getLeaseId();

        ByteString getLeaseIdBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class LeaseMessage extends GeneratedMessageV3 implements LeaseMessageOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 1;
        public static final int FEE_FIELD_NUMBER = 3;
        public static final int TO_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private long amount_;
        private long fee_;
        private byte memoizedIsInitialized;
        private volatile Object to_;
        private static final LeaseMessage DEFAULT_INSTANCE = new LeaseMessage();
        private static final t0<LeaseMessage> PARSER = new c<LeaseMessage>() { // from class: wallet.core.jni.proto.Waves.LeaseMessage.1
            @Override // com.google.protobuf.t0
            public LeaseMessage parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new LeaseMessage(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements LeaseMessageOrBuilder {
            private long amount_;
            private long fee_;
            private Object to_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Waves.internal_static_TW_Waves_Proto_LeaseMessage_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearTo() {
                this.to_ = LeaseMessage.getDefaultInstance().getTo();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Waves.internal_static_TW_Waves_Proto_LeaseMessage_descriptor;
            }

            @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.to_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Waves.internal_static_TW_Waves_Proto_LeaseMessage_fieldAccessorTable.d(LeaseMessage.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setTo(String str) {
                Objects.requireNonNull(str);
                this.to_ = str;
                onChanged();
                return this;
            }

            public Builder setToBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.to_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.to_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public LeaseMessage build() {
                LeaseMessage buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public LeaseMessage buildPartial() {
                LeaseMessage leaseMessage = new LeaseMessage(this, (AnonymousClass1) null);
                leaseMessage.amount_ = this.amount_;
                leaseMessage.to_ = this.to_;
                leaseMessage.fee_ = this.fee_;
                onBuilt();
                return leaseMessage;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public LeaseMessage getDefaultInstanceForType() {
                return LeaseMessage.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.amount_ = 0L;
                this.to_ = "";
                this.fee_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.to_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof LeaseMessage) {
                    return mergeFrom((LeaseMessage) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(LeaseMessage leaseMessage) {
                if (leaseMessage == LeaseMessage.getDefaultInstance()) {
                    return this;
                }
                if (leaseMessage.getAmount() != 0) {
                    setAmount(leaseMessage.getAmount());
                }
                if (!leaseMessage.getTo().isEmpty()) {
                    this.to_ = leaseMessage.to_;
                    onChanged();
                }
                if (leaseMessage.getFee() != 0) {
                    setFee(leaseMessage.getFee());
                }
                mergeUnknownFields(leaseMessage.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Waves.LeaseMessage.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Waves.LeaseMessage.access$2800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Waves$LeaseMessage r3 = (wallet.core.jni.proto.Waves.LeaseMessage) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Waves$LeaseMessage r4 = (wallet.core.jni.proto.Waves.LeaseMessage) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Waves.LeaseMessage.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Waves$LeaseMessage$Builder");
            }
        }

        public /* synthetic */ LeaseMessage(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static LeaseMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Waves.internal_static_TW_Waves_Proto_LeaseMessage_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static LeaseMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (LeaseMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static LeaseMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<LeaseMessage> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof LeaseMessage)) {
                return super.equals(obj);
            }
            LeaseMessage leaseMessage = (LeaseMessage) obj;
            return getAmount() == leaseMessage.getAmount() && getTo().equals(leaseMessage.getTo()) && getFee() == leaseMessage.getFee() && this.unknownFields.equals(leaseMessage.unknownFields);
        }

        @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<LeaseMessage> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.amount_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            if (!getToBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(2, this.to_);
            }
            long j2 = this.fee_;
            if (j2 != 0) {
                z += CodedOutputStream.z(3, j2);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
        public String getTo() {
            Object obj = this.to_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.to_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Waves.LeaseMessageOrBuilder
        public ByteString getToBytes() {
            Object obj = this.to_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.to_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getAmount())) * 37) + 2) * 53) + getTo().hashCode()) * 37) + 3) * 53) + a0.h(getFee())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Waves.internal_static_TW_Waves_Proto_LeaseMessage_fieldAccessorTable.d(LeaseMessage.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new LeaseMessage();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            if (!getToBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.to_);
            }
            long j2 = this.fee_;
            if (j2 != 0) {
                codedOutputStream.I0(3, j2);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ LeaseMessage(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(LeaseMessage leaseMessage) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(leaseMessage);
        }

        public static LeaseMessage parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private LeaseMessage(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static LeaseMessage parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (LeaseMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static LeaseMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public LeaseMessage getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static LeaseMessage parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private LeaseMessage() {
            this.memoizedIsInitialized = (byte) -1;
            this.to_ = "";
        }

        public static LeaseMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static LeaseMessage parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static LeaseMessage parseFrom(InputStream inputStream) throws IOException {
            return (LeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private LeaseMessage(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.amount_ = jVar.y();
                            } else if (J == 18) {
                                this.to_ = jVar.I();
                            } else if (J != 24) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.fee_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static LeaseMessage parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (LeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static LeaseMessage parseFrom(j jVar) throws IOException {
            return (LeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static LeaseMessage parseFrom(j jVar, r rVar) throws IOException {
            return (LeaseMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface LeaseMessageOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        long getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getTo();

        ByteString getToBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int CANCEL_LEASE_MESSAGE_FIELD_NUMBER = 5;
        public static final int LEASE_MESSAGE_FIELD_NUMBER = 4;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 2;
        public static final int TIMESTAMP_FIELD_NUMBER = 1;
        public static final int TRANSFER_MESSAGE_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;
        private ByteString privateKey_;
        private long timestamp_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Waves.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> cancelLeaseMessageBuilder_;
            private a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> leaseMessageBuilder_;
            private int messageOneofCase_;
            private Object messageOneof_;
            private ByteString privateKey_;
            private long timestamp_;
            private a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> transferMessageBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> getCancelLeaseMessageFieldBuilder() {
                if (this.cancelLeaseMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 5) {
                        this.messageOneof_ = CancelLeaseMessage.getDefaultInstance();
                    }
                    this.cancelLeaseMessageBuilder_ = new a1<>((CancelLeaseMessage) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 5;
                onChanged();
                return this.cancelLeaseMessageBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Waves.internal_static_TW_Waves_Proto_SigningInput_descriptor;
            }

            private a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> getLeaseMessageFieldBuilder() {
                if (this.leaseMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 4) {
                        this.messageOneof_ = LeaseMessage.getDefaultInstance();
                    }
                    this.leaseMessageBuilder_ = new a1<>((LeaseMessage) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 4;
                onChanged();
                return this.leaseMessageBuilder_;
            }

            private a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> getTransferMessageFieldBuilder() {
                if (this.transferMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 3) {
                        this.messageOneof_ = TransferMessage.getDefaultInstance();
                    }
                    this.transferMessageBuilder_ = new a1<>((TransferMessage) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 3;
                onChanged();
                return this.transferMessageBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCancelLeaseMessage() {
                a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> a1Var = this.cancelLeaseMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearLeaseMessage() {
                a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> a1Var = this.leaseMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearTimestamp() {
                this.timestamp_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearTransferMessage() {
                a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public CancelLeaseMessage getCancelLeaseMessage() {
                a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> a1Var = this.cancelLeaseMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        return (CancelLeaseMessage) this.messageOneof_;
                    }
                    return CancelLeaseMessage.getDefaultInstance();
                } else if (this.messageOneofCase_ == 5) {
                    return a1Var.f();
                } else {
                    return CancelLeaseMessage.getDefaultInstance();
                }
            }

            public CancelLeaseMessage.Builder getCancelLeaseMessageBuilder() {
                return getCancelLeaseMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public CancelLeaseMessageOrBuilder getCancelLeaseMessageOrBuilder() {
                a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 5 || (a1Var = this.cancelLeaseMessageBuilder_) == null) {
                    if (i == 5) {
                        return (CancelLeaseMessage) this.messageOneof_;
                    }
                    return CancelLeaseMessage.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Waves.internal_static_TW_Waves_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public LeaseMessage getLeaseMessage() {
                a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> a1Var = this.leaseMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        return (LeaseMessage) this.messageOneof_;
                    }
                    return LeaseMessage.getDefaultInstance();
                } else if (this.messageOneofCase_ == 4) {
                    return a1Var.f();
                } else {
                    return LeaseMessage.getDefaultInstance();
                }
            }

            public LeaseMessage.Builder getLeaseMessageBuilder() {
                return getLeaseMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public LeaseMessageOrBuilder getLeaseMessageOrBuilder() {
                a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 4 || (a1Var = this.leaseMessageBuilder_) == null) {
                    if (i == 4) {
                        return (LeaseMessage) this.messageOneof_;
                    }
                    return LeaseMessage.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public long getTimestamp() {
                return this.timestamp_;
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public TransferMessage getTransferMessage() {
                a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        return (TransferMessage) this.messageOneof_;
                    }
                    return TransferMessage.getDefaultInstance();
                } else if (this.messageOneofCase_ == 3) {
                    return a1Var.f();
                } else {
                    return TransferMessage.getDefaultInstance();
                }
            }

            public TransferMessage.Builder getTransferMessageBuilder() {
                return getTransferMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public TransferMessageOrBuilder getTransferMessageOrBuilder() {
                a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 3 || (a1Var = this.transferMessageBuilder_) == null) {
                    if (i == 3) {
                        return (TransferMessage) this.messageOneof_;
                    }
                    return TransferMessage.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public boolean hasCancelLeaseMessage() {
                return this.messageOneofCase_ == 5;
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public boolean hasLeaseMessage() {
                return this.messageOneofCase_ == 4;
            }

            @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
            public boolean hasTransferMessage() {
                return this.messageOneofCase_ == 3;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Waves.internal_static_TW_Waves_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCancelLeaseMessage(CancelLeaseMessage cancelLeaseMessage) {
                a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> a1Var = this.cancelLeaseMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5 && this.messageOneof_ != CancelLeaseMessage.getDefaultInstance()) {
                        this.messageOneof_ = CancelLeaseMessage.newBuilder((CancelLeaseMessage) this.messageOneof_).mergeFrom(cancelLeaseMessage).buildPartial();
                    } else {
                        this.messageOneof_ = cancelLeaseMessage;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 5) {
                        a1Var.h(cancelLeaseMessage);
                    }
                    this.cancelLeaseMessageBuilder_.j(cancelLeaseMessage);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder mergeLeaseMessage(LeaseMessage leaseMessage) {
                a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> a1Var = this.leaseMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4 && this.messageOneof_ != LeaseMessage.getDefaultInstance()) {
                        this.messageOneof_ = LeaseMessage.newBuilder((LeaseMessage) this.messageOneof_).mergeFrom(leaseMessage).buildPartial();
                    } else {
                        this.messageOneof_ = leaseMessage;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 4) {
                        a1Var.h(leaseMessage);
                    }
                    this.leaseMessageBuilder_.j(leaseMessage);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder mergeTransferMessage(TransferMessage transferMessage) {
                a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3 && this.messageOneof_ != TransferMessage.getDefaultInstance()) {
                        this.messageOneof_ = TransferMessage.newBuilder((TransferMessage) this.messageOneof_).mergeFrom(transferMessage).buildPartial();
                    } else {
                        this.messageOneof_ = transferMessage;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 3) {
                        a1Var.h(transferMessage);
                    }
                    this.transferMessageBuilder_.j(transferMessage);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder setCancelLeaseMessage(CancelLeaseMessage cancelLeaseMessage) {
                a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> a1Var = this.cancelLeaseMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(cancelLeaseMessage);
                    this.messageOneof_ = cancelLeaseMessage;
                    onChanged();
                } else {
                    a1Var.j(cancelLeaseMessage);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder setLeaseMessage(LeaseMessage leaseMessage) {
                a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> a1Var = this.leaseMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(leaseMessage);
                    this.messageOneof_ = leaseMessage;
                    onChanged();
                } else {
                    a1Var.j(leaseMessage);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTimestamp(long j) {
                this.timestamp_ = j;
                onChanged();
                return this;
            }

            public Builder setTransferMessage(TransferMessage transferMessage) {
                a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transferMessage);
                    this.messageOneof_ = transferMessage;
                    onChanged();
                } else {
                    a1Var.j(transferMessage);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.timestamp_ = this.timestamp_;
                signingInput.privateKey_ = this.privateKey_;
                if (this.messageOneofCase_ == 3) {
                    a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> a1Var = this.transferMessageBuilder_;
                    if (a1Var == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var.b();
                    }
                }
                if (this.messageOneofCase_ == 4) {
                    a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> a1Var2 = this.leaseMessageBuilder_;
                    if (a1Var2 == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var2.b();
                    }
                }
                if (this.messageOneofCase_ == 5) {
                    a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> a1Var3 = this.cancelLeaseMessageBuilder_;
                    if (a1Var3 == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var3.b();
                    }
                }
                signingInput.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.timestamp_ = 0L;
                this.privateKey_ = ByteString.EMPTY;
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setCancelLeaseMessage(CancelLeaseMessage.Builder builder) {
                a1<CancelLeaseMessage, CancelLeaseMessage.Builder, CancelLeaseMessageOrBuilder> a1Var = this.cancelLeaseMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder setLeaseMessage(LeaseMessage.Builder builder) {
                a1<LeaseMessage, LeaseMessage.Builder, LeaseMessageOrBuilder> a1Var = this.leaseMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setTransferMessage(TransferMessage.Builder builder) {
                a1<TransferMessage, TransferMessage.Builder, TransferMessageOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getTimestamp() != 0) {
                    setTimestamp(signingInput.getTimestamp());
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Waves$SigningInput$MessageOneofCase[signingInput.getMessageOneofCase().ordinal()];
                if (i == 1) {
                    mergeTransferMessage(signingInput.getTransferMessage());
                } else if (i == 2) {
                    mergeLeaseMessage(signingInput.getLeaseMessage());
                } else if (i == 3) {
                    mergeCancelLeaseMessage(signingInput.getCancelLeaseMessage());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Waves.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Waves.SigningInput.access$5400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Waves$SigningInput r3 = (wallet.core.jni.proto.Waves.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Waves$SigningInput r4 = (wallet.core.jni.proto.Waves.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Waves.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Waves$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            TRANSFER_MESSAGE(3),
            LEASE_MESSAGE(4),
            CANCEL_LEASE_MESSAGE(5),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 3) {
                        if (i != 4) {
                            if (i != 5) {
                                return null;
                            }
                            return CANCEL_LEASE_MESSAGE;
                        }
                        return LEASE_MESSAGE;
                    }
                    return TRANSFER_MESSAGE;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Waves.internal_static_TW_Waves_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getTimestamp() == signingInput.getTimestamp() && getPrivateKey().equals(signingInput.getPrivateKey()) && getMessageOneofCase().equals(signingInput.getMessageOneofCase())) {
                int i = this.messageOneofCase_;
                if (i != 3) {
                    if (i != 4) {
                        if (i == 5 && !getCancelLeaseMessage().equals(signingInput.getCancelLeaseMessage())) {
                            return false;
                        }
                    } else if (!getLeaseMessage().equals(signingInput.getLeaseMessage())) {
                        return false;
                    }
                } else if (!getTransferMessage().equals(signingInput.getTransferMessage())) {
                    return false;
                }
                return this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public CancelLeaseMessage getCancelLeaseMessage() {
            if (this.messageOneofCase_ == 5) {
                return (CancelLeaseMessage) this.messageOneof_;
            }
            return CancelLeaseMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public CancelLeaseMessageOrBuilder getCancelLeaseMessageOrBuilder() {
            if (this.messageOneofCase_ == 5) {
                return (CancelLeaseMessage) this.messageOneof_;
            }
            return CancelLeaseMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public LeaseMessage getLeaseMessage() {
            if (this.messageOneofCase_ == 4) {
                return (LeaseMessage) this.messageOneof_;
            }
            return LeaseMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public LeaseMessageOrBuilder getLeaseMessageOrBuilder() {
            if (this.messageOneofCase_ == 4) {
                return (LeaseMessage) this.messageOneof_;
            }
            return LeaseMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.timestamp_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            if (!this.privateKey_.isEmpty()) {
                z += CodedOutputStream.h(2, this.privateKey_);
            }
            if (this.messageOneofCase_ == 3) {
                z += CodedOutputStream.G(3, (TransferMessage) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                z += CodedOutputStream.G(4, (LeaseMessage) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                z += CodedOutputStream.G(5, (CancelLeaseMessage) this.messageOneof_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public long getTimestamp() {
            return this.timestamp_;
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public TransferMessage getTransferMessage() {
            if (this.messageOneofCase_ == 3) {
                return (TransferMessage) this.messageOneof_;
            }
            return TransferMessage.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public TransferMessageOrBuilder getTransferMessageOrBuilder() {
            if (this.messageOneofCase_ == 3) {
                return (TransferMessage) this.messageOneof_;
            }
            return TransferMessage.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public boolean hasCancelLeaseMessage() {
            return this.messageOneofCase_ == 5;
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public boolean hasLeaseMessage() {
            return this.messageOneofCase_ == 4;
        }

        @Override // wallet.core.jni.proto.Waves.SigningInputOrBuilder
        public boolean hasTransferMessage() {
            return this.messageOneofCase_ == 3;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getTimestamp())) * 37) + 2) * 53) + getPrivateKey().hashCode();
            int i3 = this.messageOneofCase_;
            if (i3 == 3) {
                i = ((hashCode2 * 37) + 3) * 53;
                hashCode = getTransferMessage().hashCode();
            } else if (i3 == 4) {
                i = ((hashCode2 * 37) + 4) * 53;
                hashCode = getLeaseMessage().hashCode();
            } else {
                if (i3 == 5) {
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getCancelLeaseMessage().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Waves.internal_static_TW_Waves_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.timestamp_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(2, this.privateKey_);
            }
            if (this.messageOneofCase_ == 3) {
                codedOutputStream.K0(3, (TransferMessage) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                codedOutputStream.K0(4, (LeaseMessage) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                codedOutputStream.K0(5, (CancelLeaseMessage) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.privateKey_ = ByteString.EMPTY;
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.timestamp_ = jVar.y();
                            } else if (J != 18) {
                                if (J == 26) {
                                    TransferMessage.Builder builder = this.messageOneofCase_ == 3 ? ((TransferMessage) this.messageOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(TransferMessage.parser(), rVar);
                                    this.messageOneof_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((TransferMessage) z2);
                                        this.messageOneof_ = builder.buildPartial();
                                    }
                                    this.messageOneofCase_ = 3;
                                } else if (J == 34) {
                                    LeaseMessage.Builder builder2 = this.messageOneofCase_ == 4 ? ((LeaseMessage) this.messageOneof_).toBuilder() : null;
                                    m0 z3 = jVar.z(LeaseMessage.parser(), rVar);
                                    this.messageOneof_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((LeaseMessage) z3);
                                        this.messageOneof_ = builder2.buildPartial();
                                    }
                                    this.messageOneofCase_ = 4;
                                } else if (J != 42) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    CancelLeaseMessage.Builder builder3 = this.messageOneofCase_ == 5 ? ((CancelLeaseMessage) this.messageOneof_).toBuilder() : null;
                                    m0 z4 = jVar.z(CancelLeaseMessage.parser(), rVar);
                                    this.messageOneof_ = z4;
                                    if (builder3 != null) {
                                        builder3.mergeFrom((CancelLeaseMessage) z4);
                                        this.messageOneof_ = builder3.buildPartial();
                                    }
                                    this.messageOneofCase_ = 5;
                                }
                            } else {
                                this.privateKey_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        CancelLeaseMessage getCancelLeaseMessage();

        CancelLeaseMessageOrBuilder getCancelLeaseMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        LeaseMessage getLeaseMessage();

        LeaseMessageOrBuilder getLeaseMessageOrBuilder();

        SigningInput.MessageOneofCase getMessageOneofCase();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        long getTimestamp();

        TransferMessage getTransferMessage();

        TransferMessageOrBuilder getTransferMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasCancelLeaseMessage();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasLeaseMessage();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransferMessage();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int JSON_FIELD_NUMBER = 2;
        public static final int SIGNATURE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object json_;
        private byte memoizedIsInitialized;
        private ByteString signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Waves.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private Object json_;
            private ByteString signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Waves.internal_static_TW_Waves_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearJson() {
                this.json_ = SigningOutput.getDefaultInstance().getJson();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Waves.internal_static_TW_Waves_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Waves.SigningOutputOrBuilder
            public String getJson() {
                Object obj = this.json_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.json_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Waves.SigningOutputOrBuilder
            public ByteString getJsonBytes() {
                Object obj = this.json_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.json_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Waves.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Waves.internal_static_TW_Waves_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setJson(String str) {
                Objects.requireNonNull(str);
                this.json_ = str;
                onChanged();
                return this;
            }

            public Builder setJsonBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.json_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.signature_ = this.signature_;
                signingOutput.json_ = this.json_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getSignature() != ByteString.EMPTY) {
                    setSignature(signingOutput.getSignature());
                }
                if (!signingOutput.getJson().isEmpty()) {
                    this.json_ = signingOutput.json_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Waves.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Waves.SigningOutput.access$6500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Waves$SigningOutput r3 = (wallet.core.jni.proto.Waves.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Waves$SigningOutput r4 = (wallet.core.jni.proto.Waves.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Waves.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Waves$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Waves.internal_static_TW_Waves_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getSignature().equals(signingOutput.getSignature()) && getJson().equals(signingOutput.getJson()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Waves.SigningOutputOrBuilder
        public String getJson() {
            Object obj = this.json_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.json_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Waves.SigningOutputOrBuilder
        public ByteString getJsonBytes() {
            Object obj = this.json_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.json_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.signature_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.signature_);
            if (!getJsonBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(2, this.json_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Waves.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSignature().hashCode()) * 37) + 2) * 53) + getJson().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Waves.internal_static_TW_Waves_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(1, this.signature_);
            }
            if (!getJsonBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.json_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.signature_ = ByteString.EMPTY;
            this.json_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.signature_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.json_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getJson();

        ByteString getJsonBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransferMessage extends GeneratedMessageV3 implements TransferMessageOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 1;
        public static final int ASSET_FIELD_NUMBER = 2;
        public static final int ATTACHMENT_FIELD_NUMBER = 6;
        public static final int FEE_ASSET_FIELD_NUMBER = 4;
        public static final int FEE_FIELD_NUMBER = 3;
        public static final int TO_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object asset_;
        private ByteString attachment_;
        private volatile Object feeAsset_;
        private long fee_;
        private byte memoizedIsInitialized;
        private volatile Object to_;
        private static final TransferMessage DEFAULT_INSTANCE = new TransferMessage();
        private static final t0<TransferMessage> PARSER = new c<TransferMessage>() { // from class: wallet.core.jni.proto.Waves.TransferMessage.1
            @Override // com.google.protobuf.t0
            public TransferMessage parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransferMessage(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferMessageOrBuilder {
            private long amount_;
            private Object asset_;
            private ByteString attachment_;
            private Object feeAsset_;
            private long fee_;
            private Object to_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Waves.internal_static_TW_Waves_Proto_TransferMessage_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearAsset() {
                this.asset_ = TransferMessage.getDefaultInstance().getAsset();
                onChanged();
                return this;
            }

            public Builder clearAttachment() {
                this.attachment_ = TransferMessage.getDefaultInstance().getAttachment();
                onChanged();
                return this;
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearFeeAsset() {
                this.feeAsset_ = TransferMessage.getDefaultInstance().getFeeAsset();
                onChanged();
                return this;
            }

            public Builder clearTo() {
                this.to_ = TransferMessage.getDefaultInstance().getTo();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public String getAsset() {
                Object obj = this.asset_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.asset_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public ByteString getAssetBytes() {
                Object obj = this.asset_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.asset_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public ByteString getAttachment() {
                return this.attachment_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Waves.internal_static_TW_Waves_Proto_TransferMessage_descriptor;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public String getFeeAsset() {
                Object obj = this.feeAsset_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.feeAsset_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public ByteString getFeeAssetBytes() {
                Object obj = this.feeAsset_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.feeAsset_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.to_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Waves.internal_static_TW_Waves_Proto_TransferMessage_fieldAccessorTable.d(TransferMessage.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setAsset(String str) {
                Objects.requireNonNull(str);
                this.asset_ = str;
                onChanged();
                return this;
            }

            public Builder setAssetBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.asset_ = byteString;
                onChanged();
                return this;
            }

            public Builder setAttachment(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.attachment_ = byteString;
                onChanged();
                return this;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setFeeAsset(String str) {
                Objects.requireNonNull(str);
                this.feeAsset_ = str;
                onChanged();
                return this;
            }

            public Builder setFeeAssetBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.feeAsset_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTo(String str) {
                Objects.requireNonNull(str);
                this.to_ = str;
                onChanged();
                return this;
            }

            public Builder setToBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.to_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.asset_ = "";
                this.feeAsset_ = "";
                this.to_ = "";
                this.attachment_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferMessage build() {
                TransferMessage buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferMessage buildPartial() {
                TransferMessage transferMessage = new TransferMessage(this, (AnonymousClass1) null);
                transferMessage.amount_ = this.amount_;
                transferMessage.asset_ = this.asset_;
                transferMessage.fee_ = this.fee_;
                transferMessage.feeAsset_ = this.feeAsset_;
                transferMessage.to_ = this.to_;
                transferMessage.attachment_ = this.attachment_;
                onBuilt();
                return transferMessage;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransferMessage getDefaultInstanceForType() {
                return TransferMessage.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.amount_ = 0L;
                this.asset_ = "";
                this.fee_ = 0L;
                this.feeAsset_ = "";
                this.to_ = "";
                this.attachment_ = ByteString.EMPTY;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransferMessage) {
                    return mergeFrom((TransferMessage) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.asset_ = "";
                this.feeAsset_ = "";
                this.to_ = "";
                this.attachment_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransferMessage transferMessage) {
                if (transferMessage == TransferMessage.getDefaultInstance()) {
                    return this;
                }
                if (transferMessage.getAmount() != 0) {
                    setAmount(transferMessage.getAmount());
                }
                if (!transferMessage.getAsset().isEmpty()) {
                    this.asset_ = transferMessage.asset_;
                    onChanged();
                }
                if (transferMessage.getFee() != 0) {
                    setFee(transferMessage.getFee());
                }
                if (!transferMessage.getFeeAsset().isEmpty()) {
                    this.feeAsset_ = transferMessage.feeAsset_;
                    onChanged();
                }
                if (!transferMessage.getTo().isEmpty()) {
                    this.to_ = transferMessage.to_;
                    onChanged();
                }
                if (transferMessage.getAttachment() != ByteString.EMPTY) {
                    setAttachment(transferMessage.getAttachment());
                }
                mergeUnknownFields(transferMessage.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Waves.TransferMessage.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Waves.TransferMessage.access$1300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Waves$TransferMessage r3 = (wallet.core.jni.proto.Waves.TransferMessage) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Waves$TransferMessage r4 = (wallet.core.jni.proto.Waves.TransferMessage) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Waves.TransferMessage.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Waves$TransferMessage$Builder");
            }
        }

        public /* synthetic */ TransferMessage(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransferMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Waves.internal_static_TW_Waves_Proto_TransferMessage_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransferMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransferMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransferMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransferMessage> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransferMessage)) {
                return super.equals(obj);
            }
            TransferMessage transferMessage = (TransferMessage) obj;
            return getAmount() == transferMessage.getAmount() && getAsset().equals(transferMessage.getAsset()) && getFee() == transferMessage.getFee() && getFeeAsset().equals(transferMessage.getFeeAsset()) && getTo().equals(transferMessage.getTo()) && getAttachment().equals(transferMessage.getAttachment()) && this.unknownFields.equals(transferMessage.unknownFields);
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public String getAsset() {
            Object obj = this.asset_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.asset_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public ByteString getAssetBytes() {
            Object obj = this.asset_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.asset_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public ByteString getAttachment() {
            return this.attachment_;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public String getFeeAsset() {
            Object obj = this.feeAsset_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.feeAsset_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public ByteString getFeeAssetBytes() {
            Object obj = this.feeAsset_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.feeAsset_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransferMessage> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.amount_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            if (!getAssetBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(2, this.asset_);
            }
            long j2 = this.fee_;
            if (j2 != 0) {
                z += CodedOutputStream.z(3, j2);
            }
            if (!getFeeAssetBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(4, this.feeAsset_);
            }
            if (!getToBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(5, this.to_);
            }
            if (!this.attachment_.isEmpty()) {
                z += CodedOutputStream.h(6, this.attachment_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public String getTo() {
            Object obj = this.to_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.to_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Waves.TransferMessageOrBuilder
        public ByteString getToBytes() {
            Object obj = this.to_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.to_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getAmount())) * 37) + 2) * 53) + getAsset().hashCode()) * 37) + 3) * 53) + a0.h(getFee())) * 37) + 4) * 53) + getFeeAsset().hashCode()) * 37) + 5) * 53) + getTo().hashCode()) * 37) + 6) * 53) + getAttachment().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Waves.internal_static_TW_Waves_Proto_TransferMessage_fieldAccessorTable.d(TransferMessage.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransferMessage();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            if (!getAssetBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.asset_);
            }
            long j2 = this.fee_;
            if (j2 != 0) {
                codedOutputStream.I0(3, j2);
            }
            if (!getFeeAssetBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.feeAsset_);
            }
            if (!getToBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.to_);
            }
            if (!this.attachment_.isEmpty()) {
                codedOutputStream.q0(6, this.attachment_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransferMessage(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransferMessage transferMessage) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transferMessage);
        }

        public static TransferMessage parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransferMessage(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransferMessage parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferMessage) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransferMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransferMessage getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransferMessage parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransferMessage() {
            this.memoizedIsInitialized = (byte) -1;
            this.asset_ = "";
            this.feeAsset_ = "";
            this.to_ = "";
            this.attachment_ = ByteString.EMPTY;
        }

        public static TransferMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransferMessage parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransferMessage parseFrom(InputStream inputStream) throws IOException {
            return (TransferMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransferMessage parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferMessage) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TransferMessage(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.amount_ = jVar.y();
                            } else if (J == 18) {
                                this.asset_ = jVar.I();
                            } else if (J == 24) {
                                this.fee_ = jVar.y();
                            } else if (J == 34) {
                                this.feeAsset_ = jVar.I();
                            } else if (J == 42) {
                                this.to_ = jVar.I();
                            } else if (J != 50) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.attachment_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransferMessage parseFrom(j jVar) throws IOException {
            return (TransferMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransferMessage parseFrom(j jVar, r rVar) throws IOException {
            return (TransferMessage) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransferMessageOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        String getAsset();

        ByteString getAssetBytes();

        ByteString getAttachment();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        long getFee();

        String getFeeAsset();

        ByteString getFeeAssetBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getTo();

        ByteString getToBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Waves_Proto_TransferMessage_descriptor = bVar;
        internal_static_TW_Waves_Proto_TransferMessage_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Amount", "Asset", "Fee", "FeeAsset", "To", "Attachment"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Waves_Proto_LeaseMessage_descriptor = bVar2;
        internal_static_TW_Waves_Proto_LeaseMessage_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Amount", "To", "Fee"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Waves_Proto_CancelLeaseMessage_descriptor = bVar3;
        internal_static_TW_Waves_Proto_CancelLeaseMessage_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"LeaseId", "Fee"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Waves_Proto_SigningInput_descriptor = bVar4;
        internal_static_TW_Waves_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Timestamp", "PrivateKey", "TransferMessage", "LeaseMessage", "CancelLeaseMessage", "MessageOneof"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_Waves_Proto_SigningOutput_descriptor = bVar5;
        internal_static_TW_Waves_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"Signature", "Json"});
    }

    private Waves() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
