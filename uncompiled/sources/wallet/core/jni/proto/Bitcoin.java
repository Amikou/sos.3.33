package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MapField;
import com.google.protobuf.WireFormat;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g0;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import wallet.core.jni.proto.Common;

/* loaded from: classes3.dex */
public final class Bitcoin {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\rBitcoin.proto\u0012\u0010TW.Bitcoin.Proto\u001a\fCommon.proto\"\u009a\u0001\n\u000bTransaction\u0012\u000f\n\u0007version\u0018\u0001 \u0001(\u0011\u0012\u0010\n\blockTime\u0018\u0002 \u0001(\r\u00122\n\u0006inputs\u0018\u0003 \u0003(\u000b2\".TW.Bitcoin.Proto.TransactionInput\u00124\n\u0007outputs\u0018\u0004 \u0003(\u000b2#.TW.Bitcoin.Proto.TransactionOutput\"h\n\u0010TransactionInput\u00122\n\u000epreviousOutput\u0018\u0001 \u0001(\u000b2\u001a.TW.Bitcoin.Proto.OutPoint\u0012\u0010\n\bsequence\u0018\u0002 \u0001(\r\u0012\u000e\n\u0006script\u0018\u0003 \u0001(\f\"9\n\bOutPoint\u0012\f\n\u0004hash\u0018\u0001 \u0001(\f\u0012\r\n\u0005index\u0018\u0002 \u0001(\r\u0012\u0010\n\bsequence\u0018\u0003 \u0001(\r\"2\n\u0011TransactionOutput\u0012\r\n\u0005value\u0018\u0001 \u0001(\u0003\u0012\u000e\n\u0006script\u0018\u0002 \u0001(\f\"c\n\u0012UnspentTransaction\u0012-\n\tout_point\u0018\u0001 \u0001(\u000b2\u001a.TW.Bitcoin.Proto.OutPoint\u0012\u000e\n\u0006script\u0018\u0002 \u0001(\f\u0012\u000e\n\u0006amount\u0018\u0003 \u0001(\u0003\"\u0082\u0003\n\fSigningInput\u0012\u0011\n\thash_type\u0018\u0001 \u0001(\r\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\u0003\u0012\u0010\n\bbyte_fee\u0018\u0003 \u0001(\u0003\u0012\u0012\n\nto_address\u0018\u0004 \u0001(\t\u0012\u0016\n\u000echange_address\u0018\u0005 \u0001(\t\u0012\u0013\n\u000bprivate_key\u0018\u0006 \u0003(\f\u0012<\n\u0007scripts\u0018\u0007 \u0003(\u000b2+.TW.Bitcoin.Proto.SigningInput.ScriptsEntry\u00122\n\u0004utxo\u0018\b \u0003(\u000b2$.TW.Bitcoin.Proto.UnspentTransaction\u0012\u0016\n\u000euse_max_amount\u0018\t \u0001(\b\u0012\u0011\n\tcoin_type\u0018\n \u0001(\r\u0012/\n\u0004plan\u0018\u000b \u0001(\u000b2!.TW.Bitcoin.Proto.TransactionPlan\u001a.\n\fScriptsEntry\u0012\u000b\n\u0003key\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\f:\u00028\u0001\"Î\u0001\n\u000fTransactionPlan\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\u0003\u0012\u0018\n\u0010available_amount\u0018\u0002 \u0001(\u0003\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0003\u0012\u000e\n\u0006change\u0018\u0004 \u0001(\u0003\u00123\n\u0005utxos\u0018\u0005 \u0003(\u000b2$.TW.Bitcoin.Proto.UnspentTransaction\u0012\u0011\n\tbranch_id\u0018\u0006 \u0001(\f\u0012,\n\u0005error\u0018\u0007 \u0001(\u000e2\u001d.TW.Common.Proto.SigningError\"\u009a\u0001\n\rSigningOutput\u00122\n\u000btransaction\u0018\u0001 \u0001(\u000b2\u001d.TW.Bitcoin.Proto.Transaction\u0012\u000f\n\u0007encoded\u0018\u0002 \u0001(\f\u0012\u0016\n\u000etransaction_id\u0018\u0003 \u0001(\t\u0012,\n\u0005error\u0018\u0004 \u0001(\u000e2\u001d.TW.Common.Proto.SigningErrorB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[]{Common.getDescriptor()});
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_OutPoint_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_OutPoint_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_SigningInput_ScriptsEntry_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_SigningInput_ScriptsEntry_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_TransactionInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_TransactionInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_TransactionOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_TransactionOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_TransactionPlan_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_TransactionPlan_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_Transaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_Transaction_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Bitcoin_Proto_UnspentTransaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Bitcoin_Proto_UnspentTransaction_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class OutPoint extends GeneratedMessageV3 implements OutPointOrBuilder {
        public static final int HASH_FIELD_NUMBER = 1;
        public static final int INDEX_FIELD_NUMBER = 2;
        public static final int SEQUENCE_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ByteString hash_;
        private int index_;
        private byte memoizedIsInitialized;
        private int sequence_;
        private static final OutPoint DEFAULT_INSTANCE = new OutPoint();
        private static final t0<OutPoint> PARSER = new c<OutPoint>() { // from class: wallet.core.jni.proto.Bitcoin.OutPoint.1
            @Override // com.google.protobuf.t0
            public OutPoint parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new OutPoint(jVar, rVar);
            }
        };

        public static OutPoint getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_OutPoint_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static OutPoint parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OutPoint) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static OutPoint parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<OutPoint> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof OutPoint)) {
                return super.equals(obj);
            }
            OutPoint outPoint = (OutPoint) obj;
            return getHash().equals(outPoint.getHash()) && getIndex() == outPoint.getIndex() && getSequence() == outPoint.getSequence() && this.unknownFields.equals(outPoint.unknownFields);
        }

        @Override // wallet.core.jni.proto.Bitcoin.OutPointOrBuilder
        public ByteString getHash() {
            return this.hash_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.OutPointOrBuilder
        public int getIndex() {
            return this.index_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<OutPoint> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Bitcoin.OutPointOrBuilder
        public int getSequence() {
            return this.sequence_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.hash_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.hash_);
            int i2 = this.index_;
            if (i2 != 0) {
                h += CodedOutputStream.Y(2, i2);
            }
            int i3 = this.sequence_;
            if (i3 != 0) {
                h += CodedOutputStream.Y(3, i3);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getHash().hashCode()) * 37) + 2) * 53) + getIndex()) * 37) + 3) * 53) + getSequence()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_OutPoint_fieldAccessorTable.d(OutPoint.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new OutPoint();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.hash_.isEmpty()) {
                codedOutputStream.q0(1, this.hash_);
            }
            int i = this.index_;
            if (i != 0) {
                codedOutputStream.b1(2, i);
            }
            int i2 = this.sequence_;
            if (i2 != 0) {
                codedOutputStream.b1(3, i2);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements OutPointOrBuilder {
            private ByteString hash_;
            private int index_;
            private int sequence_;

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_OutPoint_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearHash() {
                this.hash_ = OutPoint.getDefaultInstance().getHash();
                onChanged();
                return this;
            }

            public Builder clearIndex() {
                this.index_ = 0;
                onChanged();
                return this;
            }

            public Builder clearSequence() {
                this.sequence_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_OutPoint_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.OutPointOrBuilder
            public ByteString getHash() {
                return this.hash_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.OutPointOrBuilder
            public int getIndex() {
                return this.index_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.OutPointOrBuilder
            public int getSequence() {
                return this.sequence_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_OutPoint_fieldAccessorTable.d(OutPoint.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.hash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setIndex(int i) {
                this.index_ = i;
                onChanged();
                return this;
            }

            public Builder setSequence(int i) {
                this.sequence_ = i;
                onChanged();
                return this;
            }

            private Builder() {
                this.hash_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OutPoint build() {
                OutPoint buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OutPoint buildPartial() {
                OutPoint outPoint = new OutPoint(this);
                outPoint.hash_ = this.hash_;
                outPoint.index_ = this.index_;
                outPoint.sequence_ = this.sequence_;
                onBuilt();
                return outPoint;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public OutPoint getDefaultInstanceForType() {
                return OutPoint.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.hash_ = ByteString.EMPTY;
                this.index_ = 0;
                this.sequence_ = 0;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.hash_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof OutPoint) {
                    return mergeFrom((OutPoint) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(OutPoint outPoint) {
                if (outPoint == OutPoint.getDefaultInstance()) {
                    return this;
                }
                if (outPoint.getHash() != ByteString.EMPTY) {
                    setHash(outPoint.getHash());
                }
                if (outPoint.getIndex() != 0) {
                    setIndex(outPoint.getIndex());
                }
                if (outPoint.getSequence() != 0) {
                    setSequence(outPoint.getSequence());
                }
                mergeUnknownFields(outPoint.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.OutPoint.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.OutPoint.access$3700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$OutPoint r3 = (wallet.core.jni.proto.Bitcoin.OutPoint) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$OutPoint r4 = (wallet.core.jni.proto.Bitcoin.OutPoint) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.OutPoint.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$OutPoint$Builder");
            }
        }

        public static Builder newBuilder(OutPoint outPoint) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(outPoint);
        }

        public static OutPoint parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private OutPoint(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OutPoint parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (OutPoint) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static OutPoint parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public OutPoint getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static OutPoint parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private OutPoint() {
            this.memoizedIsInitialized = (byte) -1;
            this.hash_ = ByteString.EMPTY;
        }

        public static OutPoint parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static OutPoint parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static OutPoint parseFrom(InputStream inputStream) throws IOException {
            return (OutPoint) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private OutPoint(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.hash_ = jVar.q();
                            } else if (J == 16) {
                                this.index_ = jVar.K();
                            } else if (J != 24) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.sequence_ = jVar.K();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static OutPoint parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (OutPoint) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static OutPoint parseFrom(j jVar) throws IOException {
            return (OutPoint) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static OutPoint parseFrom(j jVar, r rVar) throws IOException {
            return (OutPoint) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface OutPointOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getHash();

        int getIndex();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        int getSequence();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 2;
        public static final int BYTE_FEE_FIELD_NUMBER = 3;
        public static final int CHANGE_ADDRESS_FIELD_NUMBER = 5;
        public static final int COIN_TYPE_FIELD_NUMBER = 10;
        public static final int HASH_TYPE_FIELD_NUMBER = 1;
        public static final int PLAN_FIELD_NUMBER = 11;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 6;
        public static final int SCRIPTS_FIELD_NUMBER = 7;
        public static final int TO_ADDRESS_FIELD_NUMBER = 4;
        public static final int USE_MAX_AMOUNT_FIELD_NUMBER = 9;
        public static final int UTXO_FIELD_NUMBER = 8;
        private static final long serialVersionUID = 0;
        private long amount_;
        private long byteFee_;
        private volatile Object changeAddress_;
        private int coinType_;
        private int hashType_;
        private byte memoizedIsInitialized;
        private TransactionPlan plan_;
        private List<ByteString> privateKey_;
        private MapField<String, ByteString> scripts_;
        private volatile Object toAddress_;
        private boolean useMaxAmount_;
        private List<UnspentTransaction> utxo_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Bitcoin.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        /* loaded from: classes3.dex */
        public static final class ScriptsDefaultEntryHolder {
            public static final g0<String, ByteString> defaultEntry = g0.k(Bitcoin.internal_static_TW_Bitcoin_Proto_SigningInput_ScriptsEntry_descriptor, WireFormat.FieldType.STRING, "", WireFormat.FieldType.BYTES, ByteString.EMPTY);

            private ScriptsDefaultEntryHolder() {
            }
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningInput_descriptor;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public MapField<String, ByteString> internalGetScripts() {
            MapField<String, ByteString> mapField = this.scripts_;
            return mapField == null ? MapField.h(ScriptsDefaultEntryHolder.defaultEntry) : mapField;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public boolean containsScripts(String str) {
            Objects.requireNonNull(str);
            return internalGetScripts().j().containsKey(str);
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getHashType() == signingInput.getHashType() && getAmount() == signingInput.getAmount() && getByteFee() == signingInput.getByteFee() && getToAddress().equals(signingInput.getToAddress()) && getChangeAddress().equals(signingInput.getChangeAddress()) && getPrivateKeyList().equals(signingInput.getPrivateKeyList()) && internalGetScripts().equals(signingInput.internalGetScripts()) && getUtxoList().equals(signingInput.getUtxoList()) && getUseMaxAmount() == signingInput.getUseMaxAmount() && getCoinType() == signingInput.getCoinType() && hasPlan() == signingInput.hasPlan()) {
                return (!hasPlan() || getPlan().equals(signingInput.getPlan())) && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public long getByteFee() {
            return this.byteFee_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public String getChangeAddress() {
            Object obj = this.changeAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.changeAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public ByteString getChangeAddressBytes() {
            Object obj = this.changeAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.changeAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public int getCoinType() {
            return this.coinType_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public int getHashType() {
            return this.hashType_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public TransactionPlan getPlan() {
            TransactionPlan transactionPlan = this.plan_;
            return transactionPlan == null ? TransactionPlan.getDefaultInstance() : transactionPlan;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public TransactionPlanOrBuilder getPlanOrBuilder() {
            return getPlan();
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public ByteString getPrivateKey(int i) {
            return this.privateKey_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public int getPrivateKeyCount() {
            return this.privateKey_.size();
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public List<ByteString> getPrivateKeyList() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        @Deprecated
        public Map<String, ByteString> getScripts() {
            return getScriptsMap();
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public int getScriptsCount() {
            return internalGetScripts().j().size();
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public Map<String, ByteString> getScriptsMap() {
            return internalGetScripts().j();
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public ByteString getScriptsOrDefault(String str, ByteString byteString) {
            Objects.requireNonNull(str);
            Map<String, ByteString> j = internalGetScripts().j();
            return j.containsKey(str) ? j.get(str) : byteString;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public ByteString getScriptsOrThrow(String str) {
            Objects.requireNonNull(str);
            Map<String, ByteString> j = internalGetScripts().j();
            if (j.containsKey(str)) {
                return j.get(str);
            }
            throw new IllegalArgumentException();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.hashType_;
            int Y = i2 != 0 ? CodedOutputStream.Y(1, i2) + 0 : 0;
            long j = this.amount_;
            if (j != 0) {
                Y += CodedOutputStream.z(2, j);
            }
            long j2 = this.byteFee_;
            if (j2 != 0) {
                Y += CodedOutputStream.z(3, j2);
            }
            if (!getToAddressBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(4, this.toAddress_);
            }
            if (!getChangeAddressBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(5, this.changeAddress_);
            }
            int i3 = 0;
            for (int i4 = 0; i4 < this.privateKey_.size(); i4++) {
                i3 += CodedOutputStream.i(this.privateKey_.get(i4));
            }
            int size = Y + i3 + (getPrivateKeyList().size() * 1);
            for (Map.Entry<String, ByteString> entry : internalGetScripts().j().entrySet()) {
                size += CodedOutputStream.G(7, ScriptsDefaultEntryHolder.defaultEntry.newBuilderForType().r(entry.getKey()).t(entry.getValue()).build());
            }
            for (int i5 = 0; i5 < this.utxo_.size(); i5++) {
                size += CodedOutputStream.G(8, this.utxo_.get(i5));
            }
            boolean z = this.useMaxAmount_;
            if (z) {
                size += CodedOutputStream.e(9, z);
            }
            int i6 = this.coinType_;
            if (i6 != 0) {
                size += CodedOutputStream.Y(10, i6);
            }
            if (this.plan_ != null) {
                size += CodedOutputStream.G(11, getPlan());
            }
            int serializedSize = size + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public boolean getUseMaxAmount() {
            return this.useMaxAmount_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public UnspentTransaction getUtxo(int i) {
            return this.utxo_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public int getUtxoCount() {
            return this.utxo_.size();
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public List<UnspentTransaction> getUtxoList() {
            return this.utxo_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public UnspentTransactionOrBuilder getUtxoOrBuilder(int i) {
            return this.utxo_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public List<? extends UnspentTransactionOrBuilder> getUtxoOrBuilderList() {
            return this.utxo_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
        public boolean hasPlan() {
            return this.plan_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getHashType()) * 37) + 2) * 53) + a0.h(getAmount())) * 37) + 3) * 53) + a0.h(getByteFee())) * 37) + 4) * 53) + getToAddress().hashCode()) * 37) + 5) * 53) + getChangeAddress().hashCode();
            if (getPrivateKeyCount() > 0) {
                hashCode = (((hashCode * 37) + 6) * 53) + getPrivateKeyList().hashCode();
            }
            if (!internalGetScripts().j().isEmpty()) {
                hashCode = (((hashCode * 37) + 7) * 53) + internalGetScripts().hashCode();
            }
            if (getUtxoCount() > 0) {
                hashCode = (((hashCode * 37) + 8) * 53) + getUtxoList().hashCode();
            }
            int c = (((((((hashCode * 37) + 9) * 53) + a0.c(getUseMaxAmount())) * 37) + 10) * 53) + getCoinType();
            if (hasPlan()) {
                c = (((c * 37) + 11) * 53) + getPlan().hashCode();
            }
            int hashCode2 = (c * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public MapField internalGetMapField(int i) {
            if (i == 7) {
                return internalGetScripts();
            }
            throw new RuntimeException("Invalid map field number: " + i);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.hashType_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            long j2 = this.byteFee_;
            if (j2 != 0) {
                codedOutputStream.I0(3, j2);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.toAddress_);
            }
            if (!getChangeAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.changeAddress_);
            }
            for (int i2 = 0; i2 < this.privateKey_.size(); i2++) {
                codedOutputStream.q0(6, this.privateKey_.get(i2));
            }
            GeneratedMessageV3.serializeStringMapTo(codedOutputStream, internalGetScripts(), ScriptsDefaultEntryHolder.defaultEntry, 7);
            for (int i3 = 0; i3 < this.utxo_.size(); i3++) {
                codedOutputStream.K0(8, this.utxo_.get(i3));
            }
            boolean z = this.useMaxAmount_;
            if (z) {
                codedOutputStream.m0(9, z);
            }
            int i4 = this.coinType_;
            if (i4 != 0) {
                codedOutputStream.b1(10, i4);
            }
            if (this.plan_ != null) {
                codedOutputStream.K0(11, getPlan());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private long amount_;
            private int bitField0_;
            private long byteFee_;
            private Object changeAddress_;
            private int coinType_;
            private int hashType_;
            private a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> planBuilder_;
            private TransactionPlan plan_;
            private List<ByteString> privateKey_;
            private MapField<String, ByteString> scripts_;
            private Object toAddress_;
            private boolean useMaxAmount_;
            private x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> utxoBuilder_;
            private List<UnspentTransaction> utxo_;

            private void ensurePrivateKeyIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.privateKey_ = new ArrayList(this.privateKey_);
                    this.bitField0_ |= 1;
                }
            }

            private void ensureUtxoIsMutable() {
                if ((this.bitField0_ & 4) == 0) {
                    this.utxo_ = new ArrayList(this.utxo_);
                    this.bitField0_ |= 4;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningInput_descriptor;
            }

            private a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> getPlanFieldBuilder() {
                if (this.planBuilder_ == null) {
                    this.planBuilder_ = new a1<>(getPlan(), getParentForChildren(), isClean());
                    this.plan_ = null;
                }
                return this.planBuilder_;
            }

            private x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> getUtxoFieldBuilder() {
                if (this.utxoBuilder_ == null) {
                    this.utxoBuilder_ = new x0<>(this.utxo_, (this.bitField0_ & 4) != 0, getParentForChildren(), isClean());
                    this.utxo_ = null;
                }
                return this.utxoBuilder_;
            }

            private MapField<String, ByteString> internalGetMutableScripts() {
                onChanged();
                if (this.scripts_ == null) {
                    this.scripts_ = MapField.q(ScriptsDefaultEntryHolder.defaultEntry);
                }
                if (!this.scripts_.n()) {
                    this.scripts_ = this.scripts_.g();
                }
                return this.scripts_;
            }

            private MapField<String, ByteString> internalGetScripts() {
                MapField<String, ByteString> mapField = this.scripts_;
                return mapField == null ? MapField.h(ScriptsDefaultEntryHolder.defaultEntry) : mapField;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getUtxoFieldBuilder();
                }
            }

            public Builder addAllPrivateKey(Iterable<? extends ByteString> iterable) {
                ensurePrivateKeyIsMutable();
                b.a.addAll((Iterable) iterable, (List) this.privateKey_);
                onChanged();
                return this;
            }

            public Builder addAllUtxo(Iterable<? extends UnspentTransaction> iterable) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    ensureUtxoIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.utxo_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                ensurePrivateKeyIsMutable();
                this.privateKey_.add(byteString);
                onChanged();
                return this;
            }

            public Builder addUtxo(UnspentTransaction unspentTransaction) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(unspentTransaction);
                    ensureUtxoIsMutable();
                    this.utxo_.add(unspentTransaction);
                    onChanged();
                } else {
                    x0Var.f(unspentTransaction);
                }
                return this;
            }

            public UnspentTransaction.Builder addUtxoBuilder() {
                return getUtxoFieldBuilder().d(UnspentTransaction.getDefaultInstance());
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearByteFee() {
                this.byteFee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearChangeAddress() {
                this.changeAddress_ = SigningInput.getDefaultInstance().getChangeAddress();
                onChanged();
                return this;
            }

            public Builder clearCoinType() {
                this.coinType_ = 0;
                onChanged();
                return this;
            }

            public Builder clearHashType() {
                this.hashType_ = 0;
                onChanged();
                return this;
            }

            public Builder clearPlan() {
                if (this.planBuilder_ == null) {
                    this.plan_ = null;
                    onChanged();
                } else {
                    this.plan_ = null;
                    this.planBuilder_ = null;
                }
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = Collections.emptyList();
                this.bitField0_ &= -2;
                onChanged();
                return this;
            }

            public Builder clearScripts() {
                internalGetMutableScripts().m().clear();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = SigningInput.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            public Builder clearUseMaxAmount() {
                this.useMaxAmount_ = false;
                onChanged();
                return this;
            }

            public Builder clearUtxo() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    this.utxo_ = Collections.emptyList();
                    this.bitField0_ &= -5;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public boolean containsScripts(String str) {
                Objects.requireNonNull(str);
                return internalGetScripts().j().containsKey(str);
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public long getByteFee() {
                return this.byteFee_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public String getChangeAddress() {
                Object obj = this.changeAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.changeAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public ByteString getChangeAddressBytes() {
                Object obj = this.changeAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.changeAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public int getCoinType() {
                return this.coinType_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public int getHashType() {
                return this.hashType_;
            }

            @Deprecated
            public Map<String, ByteString> getMutableScripts() {
                return internalGetMutableScripts().m();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public TransactionPlan getPlan() {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    TransactionPlan transactionPlan = this.plan_;
                    return transactionPlan == null ? TransactionPlan.getDefaultInstance() : transactionPlan;
                }
                return a1Var.f();
            }

            public TransactionPlan.Builder getPlanBuilder() {
                onChanged();
                return getPlanFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public TransactionPlanOrBuilder getPlanOrBuilder() {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                TransactionPlan transactionPlan = this.plan_;
                return transactionPlan == null ? TransactionPlan.getDefaultInstance() : transactionPlan;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public ByteString getPrivateKey(int i) {
                return this.privateKey_.get(i);
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public int getPrivateKeyCount() {
                return this.privateKey_.size();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public List<ByteString> getPrivateKeyList() {
                return (this.bitField0_ & 1) != 0 ? Collections.unmodifiableList(this.privateKey_) : this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            @Deprecated
            public Map<String, ByteString> getScripts() {
                return getScriptsMap();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public int getScriptsCount() {
                return internalGetScripts().j().size();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public Map<String, ByteString> getScriptsMap() {
                return internalGetScripts().j();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public ByteString getScriptsOrDefault(String str, ByteString byteString) {
                Objects.requireNonNull(str);
                Map<String, ByteString> j = internalGetScripts().j();
                return j.containsKey(str) ? j.get(str) : byteString;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public ByteString getScriptsOrThrow(String str) {
                Objects.requireNonNull(str);
                Map<String, ByteString> j = internalGetScripts().j();
                if (j.containsKey(str)) {
                    return j.get(str);
                }
                throw new IllegalArgumentException();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public boolean getUseMaxAmount() {
                return this.useMaxAmount_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public UnspentTransaction getUtxo(int i) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    return this.utxo_.get(i);
                }
                return x0Var.o(i);
            }

            public UnspentTransaction.Builder getUtxoBuilder(int i) {
                return getUtxoFieldBuilder().l(i);
            }

            public List<UnspentTransaction.Builder> getUtxoBuilderList() {
                return getUtxoFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public int getUtxoCount() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    return this.utxo_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public List<UnspentTransaction> getUtxoList() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.utxo_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public UnspentTransactionOrBuilder getUtxoOrBuilder(int i) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    return this.utxo_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public List<? extends UnspentTransactionOrBuilder> getUtxoOrBuilderList() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.utxo_);
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningInputOrBuilder
            public boolean hasPlan() {
                return (this.planBuilder_ == null && this.plan_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public MapField internalGetMapField(int i) {
                if (i == 7) {
                    return internalGetScripts();
                }
                throw new RuntimeException("Invalid map field number: " + i);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public MapField internalGetMutableMapField(int i) {
                if (i == 7) {
                    return internalGetMutableScripts();
                }
                throw new RuntimeException("Invalid map field number: " + i);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergePlan(TransactionPlan transactionPlan) {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    TransactionPlan transactionPlan2 = this.plan_;
                    if (transactionPlan2 != null) {
                        this.plan_ = TransactionPlan.newBuilder(transactionPlan2).mergeFrom(transactionPlan).buildPartial();
                    } else {
                        this.plan_ = transactionPlan;
                    }
                    onChanged();
                } else {
                    a1Var.h(transactionPlan);
                }
                return this;
            }

            public Builder putAllScripts(Map<String, ByteString> map) {
                internalGetMutableScripts().m().putAll(map);
                return this;
            }

            public Builder putScripts(String str, ByteString byteString) {
                Objects.requireNonNull(str);
                Objects.requireNonNull(byteString);
                internalGetMutableScripts().m().put(str, byteString);
                return this;
            }

            public Builder removeScripts(String str) {
                Objects.requireNonNull(str);
                internalGetMutableScripts().m().remove(str);
                return this;
            }

            public Builder removeUtxo(int i) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    ensureUtxoIsMutable();
                    this.utxo_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setByteFee(long j) {
                this.byteFee_ = j;
                onChanged();
                return this;
            }

            public Builder setChangeAddress(String str) {
                Objects.requireNonNull(str);
                this.changeAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setChangeAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.changeAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setCoinType(int i) {
                this.coinType_ = i;
                onChanged();
                return this;
            }

            public Builder setHashType(int i) {
                this.hashType_ = i;
                onChanged();
                return this;
            }

            public Builder setPlan(TransactionPlan transactionPlan) {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionPlan);
                    this.plan_ = transactionPlan;
                    onChanged();
                } else {
                    a1Var.j(transactionPlan);
                }
                return this;
            }

            public Builder setPrivateKey(int i, ByteString byteString) {
                Objects.requireNonNull(byteString);
                ensurePrivateKeyIsMutable();
                this.privateKey_.set(i, byteString);
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setUseMaxAmount(boolean z) {
                this.useMaxAmount_ = z;
                onChanged();
                return this;
            }

            public Builder setUtxo(int i, UnspentTransaction unspentTransaction) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(unspentTransaction);
                    ensureUtxoIsMutable();
                    this.utxo_.set(i, unspentTransaction);
                    onChanged();
                } else {
                    x0Var.x(i, unspentTransaction);
                }
                return this;
            }

            private Builder() {
                this.toAddress_ = "";
                this.changeAddress_ = "";
                this.privateKey_ = Collections.emptyList();
                this.utxo_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                signingInput.hashType_ = this.hashType_;
                signingInput.amount_ = this.amount_;
                signingInput.byteFee_ = this.byteFee_;
                signingInput.toAddress_ = this.toAddress_;
                signingInput.changeAddress_ = this.changeAddress_;
                if ((this.bitField0_ & 1) != 0) {
                    this.privateKey_ = Collections.unmodifiableList(this.privateKey_);
                    this.bitField0_ &= -2;
                }
                signingInput.privateKey_ = this.privateKey_;
                signingInput.scripts_ = internalGetScripts();
                signingInput.scripts_.o();
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var != null) {
                    signingInput.utxo_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 4) != 0) {
                        this.utxo_ = Collections.unmodifiableList(this.utxo_);
                        this.bitField0_ &= -5;
                    }
                    signingInput.utxo_ = this.utxo_;
                }
                signingInput.useMaxAmount_ = this.useMaxAmount_;
                signingInput.coinType_ = this.coinType_;
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    signingInput.plan_ = this.plan_;
                } else {
                    signingInput.plan_ = a1Var.b();
                }
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public UnspentTransaction.Builder addUtxoBuilder(int i) {
                return getUtxoFieldBuilder().c(i, UnspentTransaction.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.hashType_ = 0;
                this.amount_ = 0L;
                this.byteFee_ = 0L;
                this.toAddress_ = "";
                this.changeAddress_ = "";
                this.privateKey_ = Collections.emptyList();
                this.bitField0_ &= -2;
                internalGetMutableScripts().b();
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    this.utxo_ = Collections.emptyList();
                    this.bitField0_ &= -5;
                } else {
                    x0Var.h();
                }
                this.useMaxAmount_ = false;
                this.coinType_ = 0;
                if (this.planBuilder_ == null) {
                    this.plan_ = null;
                } else {
                    this.plan_ = null;
                    this.planBuilder_ = null;
                }
                return this;
            }

            public Builder setPlan(TransactionPlan.Builder builder) {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    this.plan_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder addUtxo(int i, UnspentTransaction unspentTransaction) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(unspentTransaction);
                    ensureUtxoIsMutable();
                    this.utxo_.add(i, unspentTransaction);
                    onChanged();
                } else {
                    x0Var.e(i, unspentTransaction);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setUtxo(int i, UnspentTransaction.Builder builder) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    ensureUtxoIsMutable();
                    this.utxo_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.toAddress_ = "";
                this.changeAddress_ = "";
                this.privateKey_ = Collections.emptyList();
                this.utxo_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getHashType() != 0) {
                    setHashType(signingInput.getHashType());
                }
                if (signingInput.getAmount() != 0) {
                    setAmount(signingInput.getAmount());
                }
                if (signingInput.getByteFee() != 0) {
                    setByteFee(signingInput.getByteFee());
                }
                if (!signingInput.getToAddress().isEmpty()) {
                    this.toAddress_ = signingInput.toAddress_;
                    onChanged();
                }
                if (!signingInput.getChangeAddress().isEmpty()) {
                    this.changeAddress_ = signingInput.changeAddress_;
                    onChanged();
                }
                if (!signingInput.privateKey_.isEmpty()) {
                    if (this.privateKey_.isEmpty()) {
                        this.privateKey_ = signingInput.privateKey_;
                        this.bitField0_ &= -2;
                    } else {
                        ensurePrivateKeyIsMutable();
                        this.privateKey_.addAll(signingInput.privateKey_);
                    }
                    onChanged();
                }
                internalGetMutableScripts().p(signingInput.internalGetScripts());
                if (this.utxoBuilder_ == null) {
                    if (!signingInput.utxo_.isEmpty()) {
                        if (this.utxo_.isEmpty()) {
                            this.utxo_ = signingInput.utxo_;
                            this.bitField0_ &= -5;
                        } else {
                            ensureUtxoIsMutable();
                            this.utxo_.addAll(signingInput.utxo_);
                        }
                        onChanged();
                    }
                } else if (!signingInput.utxo_.isEmpty()) {
                    if (!this.utxoBuilder_.u()) {
                        this.utxoBuilder_.b(signingInput.utxo_);
                    } else {
                        this.utxoBuilder_.i();
                        this.utxoBuilder_ = null;
                        this.utxo_ = signingInput.utxo_;
                        this.bitField0_ &= -5;
                        this.utxoBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getUtxoFieldBuilder() : null;
                    }
                }
                if (signingInput.getUseMaxAmount()) {
                    setUseMaxAmount(signingInput.getUseMaxAmount());
                }
                if (signingInput.getCoinType() != 0) {
                    setCoinType(signingInput.getCoinType());
                }
                if (signingInput.hasPlan()) {
                    mergePlan(signingInput.getPlan());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            public Builder addUtxo(UnspentTransaction.Builder builder) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    ensureUtxoIsMutable();
                    this.utxo_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addUtxo(int i, UnspentTransaction.Builder builder) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxoBuilder_;
                if (x0Var == null) {
                    ensureUtxoIsMutable();
                    this.utxo_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.SigningInput.access$8300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$SigningInput r3 = (wallet.core.jni.proto.Bitcoin.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$SigningInput r4 = (wallet.core.jni.proto.Bitcoin.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.toAddress_ = "";
            this.changeAddress_ = "";
            this.privateKey_ = Collections.emptyList();
            this.utxo_ = Collections.emptyList();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v15, types: [java.lang.Object] */
        /* JADX WARN: Type inference failed for: r5v0, types: [java.lang.Object] */
        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 8:
                                this.hashType_ = jVar.K();
                                continue;
                            case 16:
                                this.amount_ = jVar.y();
                                continue;
                            case 24:
                                this.byteFee_ = jVar.y();
                                continue;
                            case 34:
                                this.toAddress_ = jVar.I();
                                continue;
                            case 42:
                                this.changeAddress_ = jVar.I();
                                continue;
                            case 50:
                                if (!(z2 & true)) {
                                    this.privateKey_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.privateKey_.add(jVar.q());
                                continue;
                            case 58:
                                if (!(z2 & true)) {
                                    this.scripts_ = MapField.q(ScriptsDefaultEntryHolder.defaultEntry);
                                    z2 |= true;
                                }
                                g0 g0Var = (g0) jVar.z(ScriptsDefaultEntryHolder.defaultEntry.getParserForType(), rVar);
                                this.scripts_.m().put(g0Var.f(), g0Var.h());
                                continue;
                            case 66:
                                if (!(z2 & true)) {
                                    this.utxo_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.utxo_.add(jVar.z(UnspentTransaction.parser(), rVar));
                                continue;
                            case 72:
                                this.useMaxAmount_ = jVar.p();
                                continue;
                            case 80:
                                this.coinType_ = jVar.K();
                                continue;
                            case 90:
                                TransactionPlan transactionPlan = this.plan_;
                                TransactionPlan.Builder builder = transactionPlan != null ? transactionPlan.toBuilder() : null;
                                TransactionPlan transactionPlan2 = (TransactionPlan) jVar.z(TransactionPlan.parser(), rVar);
                                this.plan_ = transactionPlan2;
                                if (builder != null) {
                                    builder.mergeFrom(transactionPlan2);
                                    this.plan_ = builder.buildPartial();
                                } else {
                                    continue;
                                }
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.privateKey_ = Collections.unmodifiableList(this.privateKey_);
                    }
                    if (z2 & true) {
                        this.utxo_ = Collections.unmodifiableList(this.utxo_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        boolean containsScripts(String str);

        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        long getByteFee();

        String getChangeAddress();

        ByteString getChangeAddressBytes();

        int getCoinType();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        int getHashType();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        TransactionPlan getPlan();

        TransactionPlanOrBuilder getPlanOrBuilder();

        ByteString getPrivateKey(int i);

        int getPrivateKeyCount();

        List<ByteString> getPrivateKeyList();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Deprecated
        Map<String, ByteString> getScripts();

        int getScriptsCount();

        Map<String, ByteString> getScriptsMap();

        ByteString getScriptsOrDefault(String str, ByteString byteString);

        ByteString getScriptsOrThrow(String str);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean getUseMaxAmount();

        UnspentTransaction getUtxo(int i);

        int getUtxoCount();

        List<UnspentTransaction> getUtxoList();

        UnspentTransactionOrBuilder getUtxoOrBuilder(int i);

        List<? extends UnspentTransactionOrBuilder> getUtxoOrBuilderList();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasPlan();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 2;
        public static final int ERROR_FIELD_NUMBER = 4;
        public static final int TRANSACTION_FIELD_NUMBER = 1;
        public static final int TRANSACTION_ID_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private int error_;
        private byte memoizedIsInitialized;
        private volatile Object transactionId_;
        private Transaction transaction_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Bitcoin.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            if (hasTransaction() != signingOutput.hasTransaction()) {
                return false;
            }
            return (!hasTransaction() || getTransaction().equals(signingOutput.getTransaction())) && getEncoded().equals(signingOutput.getEncoded()) && getTransactionId().equals(signingOutput.getTransactionId()) && this.error_ == signingOutput.error_ && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public Common.SigningError getError() {
            Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
            return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public int getErrorValue() {
            return this.error_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.transaction_ != null ? 0 + CodedOutputStream.G(1, getTransaction()) : 0;
            if (!this.encoded_.isEmpty()) {
                G += CodedOutputStream.h(2, this.encoded_);
            }
            if (!getTransactionIdBytes().isEmpty()) {
                G += GeneratedMessageV3.computeStringSize(3, this.transactionId_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                G += CodedOutputStream.l(4, this.error_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public Transaction getTransaction() {
            Transaction transaction = this.transaction_;
            return transaction == null ? Transaction.getDefaultInstance() : transaction;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public String getTransactionId() {
            Object obj = this.transactionId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.transactionId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public ByteString getTransactionIdBytes() {
            Object obj = this.transactionId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.transactionId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public TransactionOrBuilder getTransactionOrBuilder() {
            return getTransaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
        public boolean hasTransaction() {
            return this.transaction_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasTransaction()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getTransaction().hashCode();
            }
            int hashCode2 = (((((((((((((hashCode * 37) + 2) * 53) + getEncoded().hashCode()) * 37) + 3) * 53) + getTransactionId().hashCode()) * 37) + 4) * 53) + this.error_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.transaction_ != null) {
                codedOutputStream.K0(1, getTransaction());
            }
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(2, this.encoded_);
            }
            if (!getTransactionIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.transactionId_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                codedOutputStream.u0(4, this.error_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;
            private int error_;
            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> transactionBuilder_;
            private Object transactionId_;
            private Transaction transaction_;

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningOutput_descriptor;
            }

            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> getTransactionFieldBuilder() {
                if (this.transactionBuilder_ == null) {
                    this.transactionBuilder_ = new a1<>(getTransaction(), getParentForChildren(), isClean());
                    this.transaction_ = null;
                }
                return this.transactionBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearError() {
                this.error_ = 0;
                onChanged();
                return this;
            }

            public Builder clearTransaction() {
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                    onChanged();
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                return this;
            }

            public Builder clearTransactionId() {
                this.transactionId_ = SigningOutput.getDefaultInstance().getTransactionId();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public Common.SigningError getError() {
                Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
                return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public int getErrorValue() {
                return this.error_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public Transaction getTransaction() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction = this.transaction_;
                    return transaction == null ? Transaction.getDefaultInstance() : transaction;
                }
                return a1Var.f();
            }

            public Transaction.Builder getTransactionBuilder() {
                onChanged();
                return getTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public String getTransactionId() {
                Object obj = this.transactionId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.transactionId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public ByteString getTransactionIdBytes() {
                Object obj = this.transactionId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.transactionId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public TransactionOrBuilder getTransactionOrBuilder() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Transaction transaction = this.transaction_;
                return transaction == null ? Transaction.getDefaultInstance() : transaction;
            }

            @Override // wallet.core.jni.proto.Bitcoin.SigningOutputOrBuilder
            public boolean hasTransaction() {
                return (this.transactionBuilder_ == null && this.transaction_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction2 = this.transaction_;
                    if (transaction2 != null) {
                        this.transaction_ = Transaction.newBuilder(transaction2).mergeFrom(transaction).buildPartial();
                    } else {
                        this.transaction_ = transaction;
                    }
                    onChanged();
                } else {
                    a1Var.h(transaction);
                }
                return this;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setError(Common.SigningError signingError) {
                Objects.requireNonNull(signingError);
                this.error_ = signingError.getNumber();
                onChanged();
                return this;
            }

            public Builder setErrorValue(int i) {
                this.error_ = i;
                onChanged();
                return this;
            }

            public Builder setTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transaction);
                    this.transaction_ = transaction;
                    onChanged();
                } else {
                    a1Var.j(transaction);
                }
                return this;
            }

            public Builder setTransactionId(String str) {
                Objects.requireNonNull(str);
                this.transactionId_ = str;
                onChanged();
                return this;
            }

            public Builder setTransactionIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.transactionId_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                this.transactionId_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    signingOutput.transaction_ = this.transaction_;
                } else {
                    signingOutput.transaction_ = a1Var.b();
                }
                signingOutput.encoded_ = this.encoded_;
                signingOutput.transactionId_ = this.transactionId_;
                signingOutput.error_ = this.error_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                this.encoded_ = ByteString.EMPTY;
                this.transactionId_ = "";
                this.error_ = 0;
                return this;
            }

            public Builder setTransaction(Transaction.Builder builder) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    this.transaction_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                this.transactionId_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.hasTransaction()) {
                    mergeTransaction(signingOutput.getTransaction());
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (!signingOutput.getTransactionId().isEmpty()) {
                    this.transactionId_ = signingOutput.transactionId_;
                    onChanged();
                }
                if (signingOutput.error_ != 0) {
                    setErrorValue(signingOutput.getErrorValue());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.SigningOutput.access$11500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$SigningOutput r3 = (wallet.core.jni.proto.Bitcoin.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$SigningOutput r4 = (wallet.core.jni.proto.Bitcoin.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
            this.transactionId_ = "";
            this.error_ = 0;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    Transaction transaction = this.transaction_;
                                    Transaction.Builder builder = transaction != null ? transaction.toBuilder() : null;
                                    Transaction transaction2 = (Transaction) jVar.z(Transaction.parser(), rVar);
                                    this.transaction_ = transaction2;
                                    if (builder != null) {
                                        builder.mergeFrom(transaction2);
                                        this.transaction_ = builder.buildPartial();
                                    }
                                } else if (J == 18) {
                                    this.encoded_ = jVar.q();
                                } else if (J == 26) {
                                    this.transactionId_ = jVar.I();
                                } else if (J != 32) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.error_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        Common.SigningError getError();

        int getErrorValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Transaction getTransaction();

        String getTransactionId();

        ByteString getTransactionIdBytes();

        TransactionOrBuilder getTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransaction();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transaction extends GeneratedMessageV3 implements TransactionOrBuilder {
        public static final int INPUTS_FIELD_NUMBER = 3;
        public static final int LOCKTIME_FIELD_NUMBER = 2;
        public static final int OUTPUTS_FIELD_NUMBER = 4;
        public static final int VERSION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private List<TransactionInput> inputs_;
        private int lockTime_;
        private byte memoizedIsInitialized;
        private List<TransactionOutput> outputs_;
        private int version_;
        private static final Transaction DEFAULT_INSTANCE = new Transaction();
        private static final t0<Transaction> PARSER = new c<Transaction>() { // from class: wallet.core.jni.proto.Bitcoin.Transaction.1
            @Override // com.google.protobuf.t0
            public Transaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transaction(jVar, rVar);
            }
        };

        public static Transaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_Transaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transaction)) {
                return super.equals(obj);
            }
            Transaction transaction = (Transaction) obj;
            return getVersion() == transaction.getVersion() && getLockTime() == transaction.getLockTime() && getInputsList().equals(transaction.getInputsList()) && getOutputsList().equals(transaction.getOutputsList()) && this.unknownFields.equals(transaction.unknownFields);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public TransactionInput getInputs(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public int getInputsCount() {
            return this.inputs_.size();
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public List<TransactionInput> getInputsList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public TransactionInputOrBuilder getInputsOrBuilder(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public int getLockTime() {
            return this.lockTime_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public TransactionOutput getOutputs(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public int getOutputsCount() {
            return this.outputs_.size();
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public List<TransactionOutput> getOutputsList() {
            return this.outputs_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public TransactionOutputOrBuilder getOutputsOrBuilder(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList() {
            return this.outputs_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transaction> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.version_;
            int R = i2 != 0 ? CodedOutputStream.R(1, i2) + 0 : 0;
            int i3 = this.lockTime_;
            if (i3 != 0) {
                R += CodedOutputStream.Y(2, i3);
            }
            for (int i4 = 0; i4 < this.inputs_.size(); i4++) {
                R += CodedOutputStream.G(3, this.inputs_.get(i4));
            }
            for (int i5 = 0; i5 < this.outputs_.size(); i5++) {
                R += CodedOutputStream.G(4, this.outputs_.get(i5));
            }
            int serializedSize = R + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getVersion()) * 37) + 2) * 53) + getLockTime();
            if (getInputsCount() > 0) {
                hashCode = (((hashCode * 37) + 3) * 53) + getInputsList().hashCode();
            }
            if (getOutputsCount() > 0) {
                hashCode = (((hashCode * 37) + 4) * 53) + getOutputsList().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.version_;
            if (i != 0) {
                codedOutputStream.U0(1, i);
            }
            int i2 = this.lockTime_;
            if (i2 != 0) {
                codedOutputStream.b1(2, i2);
            }
            for (int i3 = 0; i3 < this.inputs_.size(); i3++) {
                codedOutputStream.K0(3, this.inputs_.get(i3));
            }
            for (int i4 = 0; i4 < this.outputs_.size(); i4++) {
                codedOutputStream.K0(4, this.outputs_.get(i4));
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOrBuilder {
            private int bitField0_;
            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> inputsBuilder_;
            private List<TransactionInput> inputs_;
            private int lockTime_;
            private x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> outputsBuilder_;
            private List<TransactionOutput> outputs_;
            private int version_;

            private void ensureInputsIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.inputs_ = new ArrayList(this.inputs_);
                    this.bitField0_ |= 1;
                }
            }

            private void ensureOutputsIsMutable() {
                if ((this.bitField0_ & 2) == 0) {
                    this.outputs_ = new ArrayList(this.outputs_);
                    this.bitField0_ |= 2;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_Transaction_descriptor;
            }

            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> getInputsFieldBuilder() {
                if (this.inputsBuilder_ == null) {
                    this.inputsBuilder_ = new x0<>(this.inputs_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.inputs_ = null;
                }
                return this.inputsBuilder_;
            }

            private x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> getOutputsFieldBuilder() {
                if (this.outputsBuilder_ == null) {
                    this.outputsBuilder_ = new x0<>(this.outputs_, (this.bitField0_ & 2) != 0, getParentForChildren(), isClean());
                    this.outputs_ = null;
                }
                return this.outputsBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getInputsFieldBuilder();
                    getOutputsFieldBuilder();
                }
            }

            public Builder addAllInputs(Iterable<? extends TransactionInput> iterable) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.inputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addAllOutputs(Iterable<? extends TransactionOutput> iterable) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.outputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addInputs(TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(transactionInput);
                    onChanged();
                } else {
                    x0Var.f(transactionInput);
                }
                return this;
            }

            public TransactionInput.Builder addInputsBuilder() {
                return getInputsFieldBuilder().d(TransactionInput.getDefaultInstance());
            }

            public Builder addOutputs(TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.add(transactionOutput);
                    onChanged();
                } else {
                    x0Var.f(transactionOutput);
                }
                return this;
            }

            public TransactionOutput.Builder addOutputsBuilder() {
                return getOutputsFieldBuilder().d(TransactionOutput.getDefaultInstance());
            }

            public Builder clearInputs() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearLockTime() {
                this.lockTime_ = 0;
                onChanged();
                return this;
            }

            public Builder clearOutputs() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_Transaction_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public TransactionInput getInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionInput.Builder getInputsBuilder(int i) {
                return getInputsFieldBuilder().l(i);
            }

            public List<TransactionInput.Builder> getInputsBuilderList() {
                return getInputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public int getInputsCount() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public List<TransactionInput> getInputsList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.inputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public TransactionInputOrBuilder getInputsOrBuilder(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.inputs_);
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public int getLockTime() {
                return this.lockTime_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public TransactionOutput getOutputs(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionOutput.Builder getOutputsBuilder(int i) {
                return getOutputsFieldBuilder().l(i);
            }

            public List<TransactionOutput.Builder> getOutputsBuilderList() {
                return getOutputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public int getOutputsCount() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public List<TransactionOutput> getOutputsList() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.outputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public TransactionOutputOrBuilder getOutputsOrBuilder(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.outputs_);
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder removeOutputs(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.set(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.x(i, transactionInput);
                }
                return this;
            }

            public Builder setLockTime(int i) {
                this.lockTime_ = i;
                onChanged();
                return this;
            }

            public Builder setOutputs(int i, TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, transactionOutput);
                    onChanged();
                } else {
                    x0Var.x(i, transactionOutput);
                }
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            private Builder() {
                this.inputs_ = Collections.emptyList();
                this.outputs_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction build() {
                Transaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction buildPartial() {
                Transaction transaction = new Transaction(this);
                transaction.version_ = this.version_;
                transaction.lockTime_ = this.lockTime_;
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var != null) {
                    transaction.inputs_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                        this.bitField0_ &= -2;
                    }
                    transaction.inputs_ = this.inputs_;
                }
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var2 = this.outputsBuilder_;
                if (x0Var2 != null) {
                    transaction.outputs_ = x0Var2.g();
                } else {
                    if ((this.bitField0_ & 2) != 0) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                        this.bitField0_ &= -3;
                    }
                    transaction.outputs_ = this.outputs_;
                }
                onBuilt();
                return transaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transaction getDefaultInstanceForType() {
                return Transaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public TransactionInput.Builder addInputsBuilder(int i) {
                return getInputsFieldBuilder().c(i, TransactionInput.getDefaultInstance());
            }

            public TransactionOutput.Builder addOutputsBuilder(int i) {
                return getOutputsFieldBuilder().c(i, TransactionOutput.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.version_ = 0;
                this.lockTime_ = 0;
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var2 = this.outputsBuilder_;
                if (x0Var2 == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                } else {
                    x0Var2.h();
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.inputs_ = Collections.emptyList();
                this.outputs_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public Builder addInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.e(i, transactionInput);
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, transactionOutput);
                    onChanged();
                } else {
                    x0Var.e(i, transactionOutput);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transaction) {
                    return mergeFrom((Transaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder setOutputs(int i, TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder mergeFrom(Transaction transaction) {
                if (transaction == Transaction.getDefaultInstance()) {
                    return this;
                }
                if (transaction.getVersion() != 0) {
                    setVersion(transaction.getVersion());
                }
                if (transaction.getLockTime() != 0) {
                    setLockTime(transaction.getLockTime());
                }
                if (this.inputsBuilder_ == null) {
                    if (!transaction.inputs_.isEmpty()) {
                        if (this.inputs_.isEmpty()) {
                            this.inputs_ = transaction.inputs_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureInputsIsMutable();
                            this.inputs_.addAll(transaction.inputs_);
                        }
                        onChanged();
                    }
                } else if (!transaction.inputs_.isEmpty()) {
                    if (!this.inputsBuilder_.u()) {
                        this.inputsBuilder_.b(transaction.inputs_);
                    } else {
                        this.inputsBuilder_.i();
                        this.inputsBuilder_ = null;
                        this.inputs_ = transaction.inputs_;
                        this.bitField0_ &= -2;
                        this.inputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getInputsFieldBuilder() : null;
                    }
                }
                if (this.outputsBuilder_ == null) {
                    if (!transaction.outputs_.isEmpty()) {
                        if (this.outputs_.isEmpty()) {
                            this.outputs_ = transaction.outputs_;
                            this.bitField0_ &= -3;
                        } else {
                            ensureOutputsIsMutable();
                            this.outputs_.addAll(transaction.outputs_);
                        }
                        onChanged();
                    }
                } else if (!transaction.outputs_.isEmpty()) {
                    if (!this.outputsBuilder_.u()) {
                        this.outputsBuilder_.b(transaction.outputs_);
                    } else {
                        this.outputsBuilder_.i();
                        this.outputsBuilder_ = null;
                        this.outputs_ = transaction.outputs_;
                        this.bitField0_ &= -3;
                        this.outputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getOutputsFieldBuilder() : null;
                    }
                }
                mergeUnknownFields(transaction.unknownFields);
                onChanged();
                return this;
            }

            public Builder addInputs(TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addOutputs(TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.Transaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.Transaction.access$1300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$Transaction r3 = (wallet.core.jni.proto.Bitcoin.Transaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$Transaction r4 = (wallet.core.jni.proto.Bitcoin.Transaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.Transaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$Transaction$Builder");
            }
        }

        public static Builder newBuilder(Transaction transaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transaction);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static Transaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Transaction() {
            this.memoizedIsInitialized = (byte) -1;
            this.inputs_ = Collections.emptyList();
            this.outputs_ = Collections.emptyList();
        }

        public static Transaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static Transaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transaction parseFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private Transaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.version_ = jVar.F();
                                } else if (J == 16) {
                                    this.lockTime_ = jVar.K();
                                } else if (J == 26) {
                                    if (!(z2 & true)) {
                                        this.inputs_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.inputs_.add(jVar.z(TransactionInput.parser(), rVar));
                                } else if (J != 34) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    if (!(z2 & true)) {
                                        this.outputs_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.outputs_.add(jVar.z(TransactionOutput.parser(), rVar));
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                    }
                    if (z2 & true) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(j jVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transaction parseFrom(j jVar, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public static final class TransactionInput extends GeneratedMessageV3 implements TransactionInputOrBuilder {
        private static final TransactionInput DEFAULT_INSTANCE = new TransactionInput();
        private static final t0<TransactionInput> PARSER = new c<TransactionInput>() { // from class: wallet.core.jni.proto.Bitcoin.TransactionInput.1
            @Override // com.google.protobuf.t0
            public TransactionInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionInput(jVar, rVar);
            }
        };
        public static final int PREVIOUSOUTPUT_FIELD_NUMBER = 1;
        public static final int SCRIPT_FIELD_NUMBER = 3;
        public static final int SEQUENCE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private OutPoint previousOutput_;
        private ByteString script_;
        private int sequence_;

        public static TransactionInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionInput)) {
                return super.equals(obj);
            }
            TransactionInput transactionInput = (TransactionInput) obj;
            if (hasPreviousOutput() != transactionInput.hasPreviousOutput()) {
                return false;
            }
            return (!hasPreviousOutput() || getPreviousOutput().equals(transactionInput.getPreviousOutput())) && getSequence() == transactionInput.getSequence() && getScript().equals(transactionInput.getScript()) && this.unknownFields.equals(transactionInput.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
        public OutPoint getPreviousOutput() {
            OutPoint outPoint = this.previousOutput_;
            return outPoint == null ? OutPoint.getDefaultInstance() : outPoint;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
        public OutPointOrBuilder getPreviousOutputOrBuilder() {
            return getPreviousOutput();
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
        public ByteString getScript() {
            return this.script_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
        public int getSequence() {
            return this.sequence_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.previousOutput_ != null ? 0 + CodedOutputStream.G(1, getPreviousOutput()) : 0;
            int i2 = this.sequence_;
            if (i2 != 0) {
                G += CodedOutputStream.Y(2, i2);
            }
            if (!this.script_.isEmpty()) {
                G += CodedOutputStream.h(3, this.script_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
        public boolean hasPreviousOutput() {
            return this.previousOutput_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasPreviousOutput()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getPreviousOutput().hashCode();
            }
            int sequence = (((((((((hashCode * 37) + 2) * 53) + getSequence()) * 37) + 3) * 53) + getScript().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = sequence;
            return sequence;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionInput_fieldAccessorTable.d(TransactionInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.previousOutput_ != null) {
                codedOutputStream.K0(1, getPreviousOutput());
            }
            int i = this.sequence_;
            if (i != 0) {
                codedOutputStream.b1(2, i);
            }
            if (!this.script_.isEmpty()) {
                codedOutputStream.q0(3, this.script_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionInputOrBuilder {
            private a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> previousOutputBuilder_;
            private OutPoint previousOutput_;
            private ByteString script_;
            private int sequence_;

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionInput_descriptor;
            }

            private a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> getPreviousOutputFieldBuilder() {
                if (this.previousOutputBuilder_ == null) {
                    this.previousOutputBuilder_ = new a1<>(getPreviousOutput(), getParentForChildren(), isClean());
                    this.previousOutput_ = null;
                }
                return this.previousOutputBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPreviousOutput() {
                if (this.previousOutputBuilder_ == null) {
                    this.previousOutput_ = null;
                    onChanged();
                } else {
                    this.previousOutput_ = null;
                    this.previousOutputBuilder_ = null;
                }
                return this;
            }

            public Builder clearScript() {
                this.script_ = TransactionInput.getDefaultInstance().getScript();
                onChanged();
                return this;
            }

            public Builder clearSequence() {
                this.sequence_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
            public OutPoint getPreviousOutput() {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    OutPoint outPoint = this.previousOutput_;
                    return outPoint == null ? OutPoint.getDefaultInstance() : outPoint;
                }
                return a1Var.f();
            }

            public OutPoint.Builder getPreviousOutputBuilder() {
                onChanged();
                return getPreviousOutputFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
            public OutPointOrBuilder getPreviousOutputOrBuilder() {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                OutPoint outPoint = this.previousOutput_;
                return outPoint == null ? OutPoint.getDefaultInstance() : outPoint;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
            public ByteString getScript() {
                return this.script_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
            public int getSequence() {
                return this.sequence_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionInputOrBuilder
            public boolean hasPreviousOutput() {
                return (this.previousOutputBuilder_ == null && this.previousOutput_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionInput_fieldAccessorTable.d(TransactionInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergePreviousOutput(OutPoint outPoint) {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    OutPoint outPoint2 = this.previousOutput_;
                    if (outPoint2 != null) {
                        this.previousOutput_ = OutPoint.newBuilder(outPoint2).mergeFrom(outPoint).buildPartial();
                    } else {
                        this.previousOutput_ = outPoint;
                    }
                    onChanged();
                } else {
                    a1Var.h(outPoint);
                }
                return this;
            }

            public Builder setPreviousOutput(OutPoint outPoint) {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(outPoint);
                    this.previousOutput_ = outPoint;
                    onChanged();
                } else {
                    a1Var.j(outPoint);
                }
                return this;
            }

            public Builder setScript(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.script_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSequence(int i) {
                this.sequence_ = i;
                onChanged();
                return this;
            }

            private Builder() {
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionInput build() {
                TransactionInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionInput buildPartial() {
                TransactionInput transactionInput = new TransactionInput(this);
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    transactionInput.previousOutput_ = this.previousOutput_;
                } else {
                    transactionInput.previousOutput_ = a1Var.b();
                }
                transactionInput.sequence_ = this.sequence_;
                transactionInput.script_ = this.script_;
                onBuilt();
                return transactionInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionInput getDefaultInstanceForType() {
                return TransactionInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.previousOutputBuilder_ == null) {
                    this.previousOutput_ = null;
                } else {
                    this.previousOutput_ = null;
                    this.previousOutputBuilder_ = null;
                }
                this.sequence_ = 0;
                this.script_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder setPreviousOutput(OutPoint.Builder builder) {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    this.previousOutput_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionInput) {
                    return mergeFrom((TransactionInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionInput transactionInput) {
                if (transactionInput == TransactionInput.getDefaultInstance()) {
                    return this;
                }
                if (transactionInput.hasPreviousOutput()) {
                    mergePreviousOutput(transactionInput.getPreviousOutput());
                }
                if (transactionInput.getSequence() != 0) {
                    setSequence(transactionInput.getSequence());
                }
                if (transactionInput.getScript() != ByteString.EMPTY) {
                    setScript(transactionInput.getScript());
                }
                mergeUnknownFields(transactionInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.TransactionInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.TransactionInput.access$2500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$TransactionInput r3 = (wallet.core.jni.proto.Bitcoin.TransactionInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$TransactionInput r4 = (wallet.core.jni.proto.Bitcoin.TransactionInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.TransactionInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$TransactionInput$Builder");
            }
        }

        public static Builder newBuilder(TransactionInput transactionInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionInput);
        }

        public static TransactionInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.script_ = ByteString.EMPTY;
        }

        public static TransactionInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionInput parseFrom(InputStream inputStream) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                OutPoint outPoint = this.previousOutput_;
                                OutPoint.Builder builder = outPoint != null ? outPoint.toBuilder() : null;
                                OutPoint outPoint2 = (OutPoint) jVar.z(OutPoint.parser(), rVar);
                                this.previousOutput_ = outPoint2;
                                if (builder != null) {
                                    builder.mergeFrom(outPoint2);
                                    this.previousOutput_ = builder.buildPartial();
                                }
                            } else if (J == 16) {
                                this.sequence_ = jVar.K();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.script_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionInput parseFrom(j jVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionInput parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        OutPoint getPreviousOutput();

        OutPointOrBuilder getPreviousOutputOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getScript();

        int getSequence();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasPreviousOutput();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public interface TransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        TransactionInput getInputs(int i);

        int getInputsCount();

        List<TransactionInput> getInputsList();

        TransactionInputOrBuilder getInputsOrBuilder(int i);

        List<? extends TransactionInputOrBuilder> getInputsOrBuilderList();

        int getLockTime();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        TransactionOutput getOutputs(int i);

        int getOutputsCount();

        List<TransactionOutput> getOutputsList();

        TransactionOutputOrBuilder getOutputsOrBuilder(int i);

        List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        int getVersion();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionOutput extends GeneratedMessageV3 implements TransactionOutputOrBuilder {
        private static final TransactionOutput DEFAULT_INSTANCE = new TransactionOutput();
        private static final t0<TransactionOutput> PARSER = new c<TransactionOutput>() { // from class: wallet.core.jni.proto.Bitcoin.TransactionOutput.1
            @Override // com.google.protobuf.t0
            public TransactionOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionOutput(jVar, rVar);
            }
        };
        public static final int SCRIPT_FIELD_NUMBER = 2;
        public static final int VALUE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString script_;
        private long value_;

        public static TransactionOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionOutput)) {
                return super.equals(obj);
            }
            TransactionOutput transactionOutput = (TransactionOutput) obj;
            return getValue() == transactionOutput.getValue() && getScript().equals(transactionOutput.getScript()) && this.unknownFields.equals(transactionOutput.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionOutput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOutputOrBuilder
        public ByteString getScript() {
            return this.script_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.value_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            if (!this.script_.isEmpty()) {
                z += CodedOutputStream.h(2, this.script_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionOutputOrBuilder
        public long getValue() {
            return this.value_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getValue())) * 37) + 2) * 53) + getScript().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionOutput_fieldAccessorTable.d(TransactionOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.value_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            if (!this.script_.isEmpty()) {
                codedOutputStream.q0(2, this.script_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOutputOrBuilder {
            private ByteString script_;
            private long value_;

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearScript() {
                this.script_ = TransactionOutput.getDefaultInstance().getScript();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = 0L;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOutputOrBuilder
            public ByteString getScript() {
                return this.script_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionOutputOrBuilder
            public long getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionOutput_fieldAccessorTable.d(TransactionOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setScript(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.script_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(long j) {
                this.value_ = j;
                onChanged();
                return this;
            }

            private Builder() {
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutput build() {
                TransactionOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutput buildPartial() {
                TransactionOutput transactionOutput = new TransactionOutput(this);
                transactionOutput.value_ = this.value_;
                transactionOutput.script_ = this.script_;
                onBuilt();
                return transactionOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionOutput getDefaultInstanceForType() {
                return TransactionOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.value_ = 0L;
                this.script_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionOutput) {
                    return mergeFrom((TransactionOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionOutput transactionOutput) {
                if (transactionOutput == TransactionOutput.getDefaultInstance()) {
                    return this;
                }
                if (transactionOutput.getValue() != 0) {
                    setValue(transactionOutput.getValue());
                }
                if (transactionOutput.getScript() != ByteString.EMPTY) {
                    setScript(transactionOutput.getScript());
                }
                mergeUnknownFields(transactionOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.TransactionOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.TransactionOutput.access$4800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$TransactionOutput r3 = (wallet.core.jni.proto.Bitcoin.TransactionOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$TransactionOutput r4 = (wallet.core.jni.proto.Bitcoin.TransactionOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.TransactionOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$TransactionOutput$Builder");
            }
        }

        public static Builder newBuilder(TransactionOutput transactionOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionOutput);
        }

        public static TransactionOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.script_ = ByteString.EMPTY;
        }

        public static TransactionOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionOutput parseFrom(InputStream inputStream) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.value_ = jVar.y();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.script_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOutput parseFrom(j jVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionOutput parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getScript();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        long getValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionPlan extends GeneratedMessageV3 implements TransactionPlanOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 1;
        public static final int AVAILABLE_AMOUNT_FIELD_NUMBER = 2;
        public static final int BRANCH_ID_FIELD_NUMBER = 6;
        public static final int CHANGE_FIELD_NUMBER = 4;
        public static final int ERROR_FIELD_NUMBER = 7;
        public static final int FEE_FIELD_NUMBER = 3;
        public static final int UTXOS_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private long amount_;
        private long availableAmount_;
        private ByteString branchId_;
        private long change_;
        private int error_;
        private long fee_;
        private byte memoizedIsInitialized;
        private List<UnspentTransaction> utxos_;
        private static final TransactionPlan DEFAULT_INSTANCE = new TransactionPlan();
        private static final t0<TransactionPlan> PARSER = new c<TransactionPlan>() { // from class: wallet.core.jni.proto.Bitcoin.TransactionPlan.1
            @Override // com.google.protobuf.t0
            public TransactionPlan parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionPlan(jVar, rVar);
            }
        };

        public static TransactionPlan getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionPlan_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionPlan parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionPlan parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionPlan> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionPlan)) {
                return super.equals(obj);
            }
            TransactionPlan transactionPlan = (TransactionPlan) obj;
            return getAmount() == transactionPlan.getAmount() && getAvailableAmount() == transactionPlan.getAvailableAmount() && getFee() == transactionPlan.getFee() && getChange() == transactionPlan.getChange() && getUtxosList().equals(transactionPlan.getUtxosList()) && getBranchId().equals(transactionPlan.getBranchId()) && this.error_ == transactionPlan.error_ && this.unknownFields.equals(transactionPlan.unknownFields);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public long getAvailableAmount() {
            return this.availableAmount_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public ByteString getBranchId() {
            return this.branchId_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public long getChange() {
            return this.change_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public Common.SigningError getError() {
            Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
            return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public int getErrorValue() {
            return this.error_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionPlan> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.amount_;
            int z = j != 0 ? CodedOutputStream.z(1, j) + 0 : 0;
            long j2 = this.availableAmount_;
            if (j2 != 0) {
                z += CodedOutputStream.z(2, j2);
            }
            long j3 = this.fee_;
            if (j3 != 0) {
                z += CodedOutputStream.z(3, j3);
            }
            long j4 = this.change_;
            if (j4 != 0) {
                z += CodedOutputStream.z(4, j4);
            }
            for (int i2 = 0; i2 < this.utxos_.size(); i2++) {
                z += CodedOutputStream.G(5, this.utxos_.get(i2));
            }
            if (!this.branchId_.isEmpty()) {
                z += CodedOutputStream.h(6, this.branchId_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                z += CodedOutputStream.l(7, this.error_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public UnspentTransaction getUtxos(int i) {
            return this.utxos_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public int getUtxosCount() {
            return this.utxos_.size();
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public List<UnspentTransaction> getUtxosList() {
            return this.utxos_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public UnspentTransactionOrBuilder getUtxosOrBuilder(int i) {
            return this.utxos_.get(i);
        }

        @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
        public List<? extends UnspentTransactionOrBuilder> getUtxosOrBuilderList() {
            return this.utxos_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getAmount())) * 37) + 2) * 53) + a0.h(getAvailableAmount())) * 37) + 3) * 53) + a0.h(getFee())) * 37) + 4) * 53) + a0.h(getChange());
            if (getUtxosCount() > 0) {
                hashCode = (((hashCode * 37) + 5) * 53) + getUtxosList().hashCode();
            }
            int hashCode2 = (((((((((hashCode * 37) + 6) * 53) + getBranchId().hashCode()) * 37) + 7) * 53) + this.error_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionPlan_fieldAccessorTable.d(TransactionPlan.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionPlan();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            long j2 = this.availableAmount_;
            if (j2 != 0) {
                codedOutputStream.I0(2, j2);
            }
            long j3 = this.fee_;
            if (j3 != 0) {
                codedOutputStream.I0(3, j3);
            }
            long j4 = this.change_;
            if (j4 != 0) {
                codedOutputStream.I0(4, j4);
            }
            for (int i = 0; i < this.utxos_.size(); i++) {
                codedOutputStream.K0(5, this.utxos_.get(i));
            }
            if (!this.branchId_.isEmpty()) {
                codedOutputStream.q0(6, this.branchId_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                codedOutputStream.u0(7, this.error_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionPlanOrBuilder {
            private long amount_;
            private long availableAmount_;
            private int bitField0_;
            private ByteString branchId_;
            private long change_;
            private int error_;
            private long fee_;
            private x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> utxosBuilder_;
            private List<UnspentTransaction> utxos_;

            private void ensureUtxosIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.utxos_ = new ArrayList(this.utxos_);
                    this.bitField0_ |= 1;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionPlan_descriptor;
            }

            private x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> getUtxosFieldBuilder() {
                if (this.utxosBuilder_ == null) {
                    this.utxosBuilder_ = new x0<>(this.utxos_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.utxos_ = null;
                }
                return this.utxosBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getUtxosFieldBuilder();
                }
            }

            public Builder addAllUtxos(Iterable<? extends UnspentTransaction> iterable) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    ensureUtxosIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.utxos_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addUtxos(UnspentTransaction unspentTransaction) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(unspentTransaction);
                    ensureUtxosIsMutable();
                    this.utxos_.add(unspentTransaction);
                    onChanged();
                } else {
                    x0Var.f(unspentTransaction);
                }
                return this;
            }

            public UnspentTransaction.Builder addUtxosBuilder() {
                return getUtxosFieldBuilder().d(UnspentTransaction.getDefaultInstance());
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearAvailableAmount() {
                this.availableAmount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearBranchId() {
                this.branchId_ = TransactionPlan.getDefaultInstance().getBranchId();
                onChanged();
                return this;
            }

            public Builder clearChange() {
                this.change_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearError() {
                this.error_ = 0;
                onChanged();
                return this;
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearUtxos() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    this.utxos_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public long getAvailableAmount() {
                return this.availableAmount_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public ByteString getBranchId() {
                return this.branchId_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public long getChange() {
                return this.change_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionPlan_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public Common.SigningError getError() {
                Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
                return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public int getErrorValue() {
                return this.error_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public UnspentTransaction getUtxos(int i) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    return this.utxos_.get(i);
                }
                return x0Var.o(i);
            }

            public UnspentTransaction.Builder getUtxosBuilder(int i) {
                return getUtxosFieldBuilder().l(i);
            }

            public List<UnspentTransaction.Builder> getUtxosBuilderList() {
                return getUtxosFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public int getUtxosCount() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    return this.utxos_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public List<UnspentTransaction> getUtxosList() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.utxos_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public UnspentTransactionOrBuilder getUtxosOrBuilder(int i) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    return this.utxos_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Bitcoin.TransactionPlanOrBuilder
            public List<? extends UnspentTransactionOrBuilder> getUtxosOrBuilderList() {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.utxos_);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_TransactionPlan_fieldAccessorTable.d(TransactionPlan.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeUtxos(int i) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    ensureUtxosIsMutable();
                    this.utxos_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setAvailableAmount(long j) {
                this.availableAmount_ = j;
                onChanged();
                return this;
            }

            public Builder setBranchId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.branchId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setChange(long j) {
                this.change_ = j;
                onChanged();
                return this;
            }

            public Builder setError(Common.SigningError signingError) {
                Objects.requireNonNull(signingError);
                this.error_ = signingError.getNumber();
                onChanged();
                return this;
            }

            public Builder setErrorValue(int i) {
                this.error_ = i;
                onChanged();
                return this;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setUtxos(int i, UnspentTransaction unspentTransaction) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(unspentTransaction);
                    ensureUtxosIsMutable();
                    this.utxos_.set(i, unspentTransaction);
                    onChanged();
                } else {
                    x0Var.x(i, unspentTransaction);
                }
                return this;
            }

            private Builder() {
                this.utxos_ = Collections.emptyList();
                this.branchId_ = ByteString.EMPTY;
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionPlan build() {
                TransactionPlan buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionPlan buildPartial() {
                TransactionPlan transactionPlan = new TransactionPlan(this);
                transactionPlan.amount_ = this.amount_;
                transactionPlan.availableAmount_ = this.availableAmount_;
                transactionPlan.fee_ = this.fee_;
                transactionPlan.change_ = this.change_;
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var != null) {
                    transactionPlan.utxos_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.utxos_ = Collections.unmodifiableList(this.utxos_);
                        this.bitField0_ &= -2;
                    }
                    transactionPlan.utxos_ = this.utxos_;
                }
                transactionPlan.branchId_ = this.branchId_;
                transactionPlan.error_ = this.error_;
                onBuilt();
                return transactionPlan;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionPlan getDefaultInstanceForType() {
                return TransactionPlan.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public UnspentTransaction.Builder addUtxosBuilder(int i) {
                return getUtxosFieldBuilder().c(i, UnspentTransaction.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.amount_ = 0L;
                this.availableAmount_ = 0L;
                this.fee_ = 0L;
                this.change_ = 0L;
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    this.utxos_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                this.branchId_ = ByteString.EMPTY;
                this.error_ = 0;
                return this;
            }

            public Builder addUtxos(int i, UnspentTransaction unspentTransaction) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(unspentTransaction);
                    ensureUtxosIsMutable();
                    this.utxos_.add(i, unspentTransaction);
                    onChanged();
                } else {
                    x0Var.e(i, unspentTransaction);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionPlan) {
                    return mergeFrom((TransactionPlan) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setUtxos(int i, UnspentTransaction.Builder builder) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    ensureUtxosIsMutable();
                    this.utxos_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.utxos_ = Collections.emptyList();
                this.branchId_ = ByteString.EMPTY;
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransactionPlan transactionPlan) {
                if (transactionPlan == TransactionPlan.getDefaultInstance()) {
                    return this;
                }
                if (transactionPlan.getAmount() != 0) {
                    setAmount(transactionPlan.getAmount());
                }
                if (transactionPlan.getAvailableAmount() != 0) {
                    setAvailableAmount(transactionPlan.getAvailableAmount());
                }
                if (transactionPlan.getFee() != 0) {
                    setFee(transactionPlan.getFee());
                }
                if (transactionPlan.getChange() != 0) {
                    setChange(transactionPlan.getChange());
                }
                if (this.utxosBuilder_ == null) {
                    if (!transactionPlan.utxos_.isEmpty()) {
                        if (this.utxos_.isEmpty()) {
                            this.utxos_ = transactionPlan.utxos_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureUtxosIsMutable();
                            this.utxos_.addAll(transactionPlan.utxos_);
                        }
                        onChanged();
                    }
                } else if (!transactionPlan.utxos_.isEmpty()) {
                    if (!this.utxosBuilder_.u()) {
                        this.utxosBuilder_.b(transactionPlan.utxos_);
                    } else {
                        this.utxosBuilder_.i();
                        this.utxosBuilder_ = null;
                        this.utxos_ = transactionPlan.utxos_;
                        this.bitField0_ &= -2;
                        this.utxosBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getUtxosFieldBuilder() : null;
                    }
                }
                if (transactionPlan.getBranchId() != ByteString.EMPTY) {
                    setBranchId(transactionPlan.getBranchId());
                }
                if (transactionPlan.error_ != 0) {
                    setErrorValue(transactionPlan.getErrorValue());
                }
                mergeUnknownFields(transactionPlan.unknownFields);
                onChanged();
                return this;
            }

            public Builder addUtxos(UnspentTransaction.Builder builder) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    ensureUtxosIsMutable();
                    this.utxos_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addUtxos(int i, UnspentTransaction.Builder builder) {
                x0<UnspentTransaction, UnspentTransaction.Builder, UnspentTransactionOrBuilder> x0Var = this.utxosBuilder_;
                if (x0Var == null) {
                    ensureUtxosIsMutable();
                    this.utxos_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.TransactionPlan.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.TransactionPlan.access$10200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$TransactionPlan r3 = (wallet.core.jni.proto.Bitcoin.TransactionPlan) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$TransactionPlan r4 = (wallet.core.jni.proto.Bitcoin.TransactionPlan) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.TransactionPlan.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$TransactionPlan$Builder");
            }
        }

        public static Builder newBuilder(TransactionPlan transactionPlan) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionPlan);
        }

        public static TransactionPlan parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionPlan(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionPlan parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionPlan parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionPlan getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionPlan parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionPlan() {
            this.memoizedIsInitialized = (byte) -1;
            this.utxos_ = Collections.emptyList();
            this.branchId_ = ByteString.EMPTY;
            this.error_ = 0;
        }

        public static TransactionPlan parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionPlan parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionPlan parseFrom(InputStream inputStream) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransactionPlan parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private TransactionPlan(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.amount_ = jVar.y();
                            } else if (J == 16) {
                                this.availableAmount_ = jVar.y();
                            } else if (J == 24) {
                                this.fee_ = jVar.y();
                            } else if (J == 32) {
                                this.change_ = jVar.y();
                            } else if (J == 42) {
                                if (!(z2 & true)) {
                                    this.utxos_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.utxos_.add(jVar.z(UnspentTransaction.parser(), rVar));
                            } else if (J == 50) {
                                this.branchId_ = jVar.q();
                            } else if (J != 56) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.error_ = jVar.s();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.utxos_ = Collections.unmodifiableList(this.utxos_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionPlan parseFrom(j jVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionPlan parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionPlanOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        long getAvailableAmount();

        ByteString getBranchId();

        long getChange();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        Common.SigningError getError();

        int getErrorValue();

        long getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        UnspentTransaction getUtxos(int i);

        int getUtxosCount();

        List<UnspentTransaction> getUtxosList();

        UnspentTransactionOrBuilder getUtxosOrBuilder(int i);

        List<? extends UnspentTransactionOrBuilder> getUtxosOrBuilderList();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class UnspentTransaction extends GeneratedMessageV3 implements UnspentTransactionOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 3;
        public static final int OUT_POINT_FIELD_NUMBER = 1;
        public static final int SCRIPT_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private long amount_;
        private byte memoizedIsInitialized;
        private OutPoint outPoint_;
        private ByteString script_;
        private static final UnspentTransaction DEFAULT_INSTANCE = new UnspentTransaction();
        private static final t0<UnspentTransaction> PARSER = new c<UnspentTransaction>() { // from class: wallet.core.jni.proto.Bitcoin.UnspentTransaction.1
            @Override // com.google.protobuf.t0
            public UnspentTransaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new UnspentTransaction(jVar, rVar);
            }
        };

        public static UnspentTransaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_UnspentTransaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static UnspentTransaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (UnspentTransaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static UnspentTransaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<UnspentTransaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof UnspentTransaction)) {
                return super.equals(obj);
            }
            UnspentTransaction unspentTransaction = (UnspentTransaction) obj;
            if (hasOutPoint() != unspentTransaction.hasOutPoint()) {
                return false;
            }
            return (!hasOutPoint() || getOutPoint().equals(unspentTransaction.getOutPoint())) && getScript().equals(unspentTransaction.getScript()) && getAmount() == unspentTransaction.getAmount() && this.unknownFields.equals(unspentTransaction.unknownFields);
        }

        @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
        public OutPoint getOutPoint() {
            OutPoint outPoint = this.outPoint_;
            return outPoint == null ? OutPoint.getDefaultInstance() : outPoint;
        }

        @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
        public OutPointOrBuilder getOutPointOrBuilder() {
            return getOutPoint();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<UnspentTransaction> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
        public ByteString getScript() {
            return this.script_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.outPoint_ != null ? 0 + CodedOutputStream.G(1, getOutPoint()) : 0;
            if (!this.script_.isEmpty()) {
                G += CodedOutputStream.h(2, this.script_);
            }
            long j = this.amount_;
            if (j != 0) {
                G += CodedOutputStream.z(3, j);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
        public boolean hasOutPoint() {
            return this.outPoint_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasOutPoint()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getOutPoint().hashCode();
            }
            int hashCode2 = (((((((((hashCode * 37) + 2) * 53) + getScript().hashCode()) * 37) + 3) * 53) + a0.h(getAmount())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Bitcoin.internal_static_TW_Bitcoin_Proto_UnspentTransaction_fieldAccessorTable.d(UnspentTransaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new UnspentTransaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.outPoint_ != null) {
                codedOutputStream.K0(1, getOutPoint());
            }
            if (!this.script_.isEmpty()) {
                codedOutputStream.q0(2, this.script_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(3, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements UnspentTransactionOrBuilder {
            private long amount_;
            private a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> outPointBuilder_;
            private OutPoint outPoint_;
            private ByteString script_;

            public static final Descriptors.b getDescriptor() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_UnspentTransaction_descriptor;
            }

            private a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> getOutPointFieldBuilder() {
                if (this.outPointBuilder_ == null) {
                    this.outPointBuilder_ = new a1<>(getOutPoint(), getParentForChildren(), isClean());
                    this.outPoint_ = null;
                }
                return this.outPointBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearOutPoint() {
                if (this.outPointBuilder_ == null) {
                    this.outPoint_ = null;
                    onChanged();
                } else {
                    this.outPoint_ = null;
                    this.outPointBuilder_ = null;
                }
                return this;
            }

            public Builder clearScript() {
                this.script_ = UnspentTransaction.getDefaultInstance().getScript();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_UnspentTransaction_descriptor;
            }

            @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
            public OutPoint getOutPoint() {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.outPointBuilder_;
                if (a1Var == null) {
                    OutPoint outPoint = this.outPoint_;
                    return outPoint == null ? OutPoint.getDefaultInstance() : outPoint;
                }
                return a1Var.f();
            }

            public OutPoint.Builder getOutPointBuilder() {
                onChanged();
                return getOutPointFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
            public OutPointOrBuilder getOutPointOrBuilder() {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.outPointBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                OutPoint outPoint = this.outPoint_;
                return outPoint == null ? OutPoint.getDefaultInstance() : outPoint;
            }

            @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
            public ByteString getScript() {
                return this.script_;
            }

            @Override // wallet.core.jni.proto.Bitcoin.UnspentTransactionOrBuilder
            public boolean hasOutPoint() {
                return (this.outPointBuilder_ == null && this.outPoint_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Bitcoin.internal_static_TW_Bitcoin_Proto_UnspentTransaction_fieldAccessorTable.d(UnspentTransaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeOutPoint(OutPoint outPoint) {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.outPointBuilder_;
                if (a1Var == null) {
                    OutPoint outPoint2 = this.outPoint_;
                    if (outPoint2 != null) {
                        this.outPoint_ = OutPoint.newBuilder(outPoint2).mergeFrom(outPoint).buildPartial();
                    } else {
                        this.outPoint_ = outPoint;
                    }
                    onChanged();
                } else {
                    a1Var.h(outPoint);
                }
                return this;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setOutPoint(OutPoint outPoint) {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.outPointBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(outPoint);
                    this.outPoint_ = outPoint;
                    onChanged();
                } else {
                    a1Var.j(outPoint);
                }
                return this;
            }

            public Builder setScript(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.script_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public UnspentTransaction build() {
                UnspentTransaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public UnspentTransaction buildPartial() {
                UnspentTransaction unspentTransaction = new UnspentTransaction(this);
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.outPointBuilder_;
                if (a1Var == null) {
                    unspentTransaction.outPoint_ = this.outPoint_;
                } else {
                    unspentTransaction.outPoint_ = a1Var.b();
                }
                unspentTransaction.script_ = this.script_;
                unspentTransaction.amount_ = this.amount_;
                onBuilt();
                return unspentTransaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public UnspentTransaction getDefaultInstanceForType() {
                return UnspentTransaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.outPointBuilder_ == null) {
                    this.outPoint_ = null;
                } else {
                    this.outPoint_ = null;
                    this.outPointBuilder_ = null;
                }
                this.script_ = ByteString.EMPTY;
                this.amount_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder setOutPoint(OutPoint.Builder builder) {
                a1<OutPoint, OutPoint.Builder, OutPointOrBuilder> a1Var = this.outPointBuilder_;
                if (a1Var == null) {
                    this.outPoint_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof UnspentTransaction) {
                    return mergeFrom((UnspentTransaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(UnspentTransaction unspentTransaction) {
                if (unspentTransaction == UnspentTransaction.getDefaultInstance()) {
                    return this;
                }
                if (unspentTransaction.hasOutPoint()) {
                    mergeOutPoint(unspentTransaction.getOutPoint());
                }
                if (unspentTransaction.getScript() != ByteString.EMPTY) {
                    setScript(unspentTransaction.getScript());
                }
                if (unspentTransaction.getAmount() != 0) {
                    setAmount(unspentTransaction.getAmount());
                }
                mergeUnknownFields(unspentTransaction.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Bitcoin.UnspentTransaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Bitcoin.UnspentTransaction.access$6000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Bitcoin$UnspentTransaction r3 = (wallet.core.jni.proto.Bitcoin.UnspentTransaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Bitcoin$UnspentTransaction r4 = (wallet.core.jni.proto.Bitcoin.UnspentTransaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Bitcoin.UnspentTransaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Bitcoin$UnspentTransaction$Builder");
            }
        }

        public static Builder newBuilder(UnspentTransaction unspentTransaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(unspentTransaction);
        }

        public static UnspentTransaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private UnspentTransaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static UnspentTransaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (UnspentTransaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static UnspentTransaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public UnspentTransaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static UnspentTransaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private UnspentTransaction() {
            this.memoizedIsInitialized = (byte) -1;
            this.script_ = ByteString.EMPTY;
        }

        public static UnspentTransaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static UnspentTransaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static UnspentTransaction parseFrom(InputStream inputStream) throws IOException {
            return (UnspentTransaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private UnspentTransaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                OutPoint outPoint = this.outPoint_;
                                OutPoint.Builder builder = outPoint != null ? outPoint.toBuilder() : null;
                                OutPoint outPoint2 = (OutPoint) jVar.z(OutPoint.parser(), rVar);
                                this.outPoint_ = outPoint2;
                                if (builder != null) {
                                    builder.mergeFrom(outPoint2);
                                    this.outPoint_ = builder.buildPartial();
                                }
                            } else if (J == 18) {
                                this.script_ = jVar.q();
                            } else if (J != 24) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.amount_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static UnspentTransaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (UnspentTransaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static UnspentTransaction parseFrom(j jVar) throws IOException {
            return (UnspentTransaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static UnspentTransaction parseFrom(j jVar, r rVar) throws IOException {
            return (UnspentTransaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface UnspentTransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        OutPoint getOutPoint();

        OutPointOrBuilder getOutPointOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getScript();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasOutPoint();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Bitcoin_Proto_Transaction_descriptor = bVar;
        internal_static_TW_Bitcoin_Proto_Transaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Version", "LockTime", "Inputs", "Outputs"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Bitcoin_Proto_TransactionInput_descriptor = bVar2;
        internal_static_TW_Bitcoin_Proto_TransactionInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"PreviousOutput", "Sequence", "Script"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Bitcoin_Proto_OutPoint_descriptor = bVar3;
        internal_static_TW_Bitcoin_Proto_OutPoint_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Hash", "Index", "Sequence"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Bitcoin_Proto_TransactionOutput_descriptor = bVar4;
        internal_static_TW_Bitcoin_Proto_TransactionOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Value", "Script"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_Bitcoin_Proto_UnspentTransaction_descriptor = bVar5;
        internal_static_TW_Bitcoin_Proto_UnspentTransaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"OutPoint", "Script", "Amount"});
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_Bitcoin_Proto_SigningInput_descriptor = bVar6;
        internal_static_TW_Bitcoin_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"HashType", "Amount", "ByteFee", "ToAddress", "ChangeAddress", "PrivateKey", "Scripts", "Utxo", "UseMaxAmount", "CoinType", "Plan"});
        Descriptors.b bVar7 = bVar6.r().get(0);
        internal_static_TW_Bitcoin_Proto_SigningInput_ScriptsEntry_descriptor = bVar7;
        internal_static_TW_Bitcoin_Proto_SigningInput_ScriptsEntry_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"Key", "Value"});
        Descriptors.b bVar8 = getDescriptor().o().get(6);
        internal_static_TW_Bitcoin_Proto_TransactionPlan_descriptor = bVar8;
        internal_static_TW_Bitcoin_Proto_TransactionPlan_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"Amount", "AvailableAmount", "Fee", "Change", "Utxos", "BranchId", "Error"});
        Descriptors.b bVar9 = getDescriptor().o().get(7);
        internal_static_TW_Bitcoin_Proto_SigningOutput_descriptor = bVar9;
        internal_static_TW_Bitcoin_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"Transaction", "Encoded", "TransactionId", "Error"});
        Common.getDescriptor();
    }

    private Bitcoin() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
