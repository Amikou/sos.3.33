package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.d0;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Tron {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\nTron.proto\u0012\rTW.Tron.Proto\"M\n\u0010TransferContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\u0012\u0012\n\nto_address\u0018\u0002 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0003 \u0001(\u0003\"f\n\u0015TransferAssetContract\u0012\u0012\n\nasset_name\u0018\u0001 \u0001(\t\u0012\u0015\n\rowner_address\u0018\u0002 \u0001(\t\u0012\u0012\n\nto_address\u0018\u0003 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0004 \u0001(\u0003\"l\n\u0015TransferTRC20Contract\u0012\u0018\n\u0010contract_address\u0018\u0001 \u0001(\t\u0012\u0015\n\rowner_address\u0018\u0002 \u0001(\t\u0012\u0012\n\nto_address\u0018\u0003 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0004 \u0001(\f\"\u008b\u0001\n\u0015FreezeBalanceContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\u0012\u0016\n\u000efrozen_balance\u0018\u0002 \u0001(\u0003\u0012\u0017\n\u000ffrozen_duration\u0018\u0003 \u0001(\u0003\u0012\u0010\n\bresource\u0018\n \u0001(\t\u0012\u0018\n\u0010receiver_address\u0018\u000f \u0001(\t\"\\\n\u0017UnfreezeBalanceContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\u0012\u0010\n\bresource\u0018\n \u0001(\t\u0012\u0018\n\u0010receiver_address\u0018\u000f \u0001(\t\".\n\u0015UnfreezeAssetContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\"`\n\u0011VoteAssetContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\u0012\u0014\n\fvote_address\u0018\u0002 \u0003(\t\u0012\u000f\n\u0007support\u0018\u0003 \u0001(\b\u0012\r\n\u0005count\u0018\u0005 \u0001(\u0005\"§\u0001\n\u0013VoteWitnessContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\u00126\n\u0005votes\u0018\u0002 \u0003(\u000b2'.TW.Tron.Proto.VoteWitnessContract.Vote\u0012\u000f\n\u0007support\u0018\u0003 \u0001(\b\u001a0\n\u0004Vote\u0012\u0014\n\fvote_address\u0018\u0001 \u0001(\t\u0012\u0012\n\nvote_count\u0018\u0002 \u0001(\u0003\"0\n\u0017WithdrawBalanceContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\"\u0095\u0001\n\u0014TriggerSmartContract\u0012\u0015\n\rowner_address\u0018\u0001 \u0001(\t\u0012\u0018\n\u0010contract_address\u0018\u0002 \u0001(\t\u0012\u0012\n\ncall_value\u0018\u0003 \u0001(\u0003\u0012\f\n\u0004data\u0018\u0004 \u0001(\f\u0012\u0018\n\u0010call_token_value\u0018\u0005 \u0001(\u0003\u0012\u0010\n\btoken_id\u0018\u0006 \u0001(\u0003\"\u0085\u0001\n\u000bBlockHeader\u0012\u0011\n\ttimestamp\u0018\u0001 \u0001(\u0003\u0012\u0014\n\ftx_trie_root\u0018\u0002 \u0001(\f\u0012\u0013\n\u000bparent_hash\u0018\u0003 \u0001(\f\u0012\u000e\n\u0006number\u0018\u0007 \u0001(\u0003\u0012\u0017\n\u000fwitness_address\u0018\t \u0001(\f\u0012\u000f\n\u0007version\u0018\n \u0001(\u0005\"\u008c\u0006\n\u000bTransaction\u0012\u0011\n\ttimestamp\u0018\u0001 \u0001(\u0003\u0012\u0012\n\nexpiration\u0018\u0002 \u0001(\u0003\u00120\n\fblock_header\u0018\u0003 \u0001(\u000b2\u001a.TW.Tron.Proto.BlockHeader\u0012\u0011\n\tfee_limit\u0018\u0004 \u0001(\u0003\u00123\n\btransfer\u0018\n \u0001(\u000b2\u001f.TW.Tron.Proto.TransferContractH\u0000\u0012>\n\u000etransfer_asset\u0018\u000b \u0001(\u000b2$.TW.Tron.Proto.TransferAssetContractH\u0000\u0012>\n\u000efreeze_balance\u0018\f \u0001(\u000b2$.TW.Tron.Proto.FreezeBalanceContractH\u0000\u0012B\n\u0010unfreeze_balance\u0018\r \u0001(\u000b2&.TW.Tron.Proto.UnfreezeBalanceContractH\u0000\u0012>\n\u000eunfreeze_asset\u0018\u000e \u0001(\u000b2$.TW.Tron.Proto.UnfreezeAssetContractH\u0000\u0012B\n\u0010withdraw_balance\u0018\u000f \u0001(\u000b2&.TW.Tron.Proto.WithdrawBalanceContractH\u0000\u00126\n\nvote_asset\u0018\u0010 \u0001(\u000b2 .TW.Tron.Proto.VoteAssetContractH\u0000\u0012:\n\fvote_witness\u0018\u0011 \u0001(\u000b2\".TW.Tron.Proto.VoteWitnessContractH\u0000\u0012E\n\u0016trigger_smart_contract\u0018\u0012 \u0001(\u000b2#.TW.Tron.Proto.TriggerSmartContractH\u0000\u0012G\n\u0017transfer_trc20_contract\u0018\u0013 \u0001(\u000b2$.TW.Tron.Proto.TransferTRC20ContractH\u0000B\u0010\n\u000econtract_oneof\"T\n\fSigningInput\u0012/\n\u000btransaction\u0018\u0001 \u0001(\u000b2\u001a.TW.Tron.Proto.Transaction\u0012\u0013\n\u000bprivate_key\u0018\u0002 \u0001(\f\"m\n\rSigningOutput\u0012\n\n\u0002id\u0018\u0001 \u0001(\f\u0012\u0011\n\tsignature\u0018\u0002 \u0001(\f\u0012\u0017\n\u000fref_block_bytes\u0018\u0003 \u0001(\f\u0012\u0016\n\u000eref_block_hash\u0018\u0004 \u0001(\f\u0012\f\n\u0004json\u0018\u0005 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Tron_Proto_BlockHeader_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_BlockHeader_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_FreezeBalanceContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_FreezeBalanceContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_Transaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_Transaction_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_TransferAssetContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_TransferAssetContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_TransferContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_TransferContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_TransferTRC20Contract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_TransferTRC20Contract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_TriggerSmartContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_TriggerSmartContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_UnfreezeAssetContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_UnfreezeAssetContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_UnfreezeBalanceContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_UnfreezeBalanceContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_VoteAssetContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_VoteAssetContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_VoteWitnessContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_VoteWitnessContract_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tron_Proto_WithdrawBalanceContract_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tron_Proto_WithdrawBalanceContract_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Tron$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase;

        static {
            int[] iArr = new int[Transaction.ContractOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase = iArr;
            try {
                iArr[Transaction.ContractOneofCase.TRANSFER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.TRANSFER_ASSET.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.FREEZE_BALANCE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.UNFREEZE_BALANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.UNFREEZE_ASSET.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.WITHDRAW_BALANCE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.VOTE_ASSET.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.VOTE_WITNESS.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.TRIGGER_SMART_CONTRACT.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.TRANSFER_TRC20_CONTRACT.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[Transaction.ContractOneofCase.CONTRACTONEOF_NOT_SET.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class BlockHeader extends GeneratedMessageV3 implements BlockHeaderOrBuilder {
        public static final int NUMBER_FIELD_NUMBER = 7;
        public static final int PARENT_HASH_FIELD_NUMBER = 3;
        public static final int TIMESTAMP_FIELD_NUMBER = 1;
        public static final int TX_TRIE_ROOT_FIELD_NUMBER = 2;
        public static final int VERSION_FIELD_NUMBER = 10;
        public static final int WITNESS_ADDRESS_FIELD_NUMBER = 9;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private long number_;
        private ByteString parentHash_;
        private long timestamp_;
        private ByteString txTrieRoot_;
        private int version_;
        private ByteString witnessAddress_;
        private static final BlockHeader DEFAULT_INSTANCE = new BlockHeader();
        private static final t0<BlockHeader> PARSER = new c<BlockHeader>() { // from class: wallet.core.jni.proto.Tron.BlockHeader.1
            @Override // com.google.protobuf.t0
            public BlockHeader parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new BlockHeader(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements BlockHeaderOrBuilder {
            private long number_;
            private ByteString parentHash_;
            private long timestamp_;
            private ByteString txTrieRoot_;
            private int version_;
            private ByteString witnessAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_BlockHeader_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearNumber() {
                this.number_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearParentHash() {
                this.parentHash_ = BlockHeader.getDefaultInstance().getParentHash();
                onChanged();
                return this;
            }

            public Builder clearTimestamp() {
                this.timestamp_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearTxTrieRoot() {
                this.txTrieRoot_ = BlockHeader.getDefaultInstance().getTxTrieRoot();
                onChanged();
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            public Builder clearWitnessAddress() {
                this.witnessAddress_ = BlockHeader.getDefaultInstance().getWitnessAddress();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_BlockHeader_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
            public long getNumber() {
                return this.number_;
            }

            @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
            public ByteString getParentHash() {
                return this.parentHash_;
            }

            @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
            public long getTimestamp() {
                return this.timestamp_;
            }

            @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
            public ByteString getTxTrieRoot() {
                return this.txTrieRoot_;
            }

            @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
            public ByteString getWitnessAddress() {
                return this.witnessAddress_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_BlockHeader_fieldAccessorTable.d(BlockHeader.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setNumber(long j) {
                this.number_ = j;
                onChanged();
                return this;
            }

            public Builder setParentHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.parentHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTimestamp(long j) {
                this.timestamp_ = j;
                onChanged();
                return this;
            }

            public Builder setTxTrieRoot(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.txTrieRoot_ = byteString;
                onChanged();
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            public Builder setWitnessAddress(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.witnessAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.txTrieRoot_ = byteString;
                this.parentHash_ = byteString;
                this.witnessAddress_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public BlockHeader build() {
                BlockHeader buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public BlockHeader buildPartial() {
                BlockHeader blockHeader = new BlockHeader(this, (AnonymousClass1) null);
                blockHeader.timestamp_ = this.timestamp_;
                blockHeader.txTrieRoot_ = this.txTrieRoot_;
                blockHeader.parentHash_ = this.parentHash_;
                blockHeader.number_ = this.number_;
                blockHeader.witnessAddress_ = this.witnessAddress_;
                blockHeader.version_ = this.version_;
                onBuilt();
                return blockHeader;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public BlockHeader getDefaultInstanceForType() {
                return BlockHeader.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.timestamp_ = 0L;
                ByteString byteString = ByteString.EMPTY;
                this.txTrieRoot_ = byteString;
                this.parentHash_ = byteString;
                this.number_ = 0L;
                this.witnessAddress_ = byteString;
                this.version_ = 0;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof BlockHeader) {
                    return mergeFrom((BlockHeader) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.txTrieRoot_ = byteString;
                this.parentHash_ = byteString;
                this.witnessAddress_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(BlockHeader blockHeader) {
                if (blockHeader == BlockHeader.getDefaultInstance()) {
                    return this;
                }
                if (blockHeader.getTimestamp() != 0) {
                    setTimestamp(blockHeader.getTimestamp());
                }
                ByteString txTrieRoot = blockHeader.getTxTrieRoot();
                ByteString byteString = ByteString.EMPTY;
                if (txTrieRoot != byteString) {
                    setTxTrieRoot(blockHeader.getTxTrieRoot());
                }
                if (blockHeader.getParentHash() != byteString) {
                    setParentHash(blockHeader.getParentHash());
                }
                if (blockHeader.getNumber() != 0) {
                    setNumber(blockHeader.getNumber());
                }
                if (blockHeader.getWitnessAddress() != byteString) {
                    setWitnessAddress(blockHeader.getWitnessAddress());
                }
                if (blockHeader.getVersion() != 0) {
                    setVersion(blockHeader.getVersion());
                }
                mergeUnknownFields(blockHeader.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.BlockHeader.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.BlockHeader.access$17100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$BlockHeader r3 = (wallet.core.jni.proto.Tron.BlockHeader) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$BlockHeader r4 = (wallet.core.jni.proto.Tron.BlockHeader) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.BlockHeader.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$BlockHeader$Builder");
            }
        }

        public /* synthetic */ BlockHeader(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static BlockHeader getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_BlockHeader_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static BlockHeader parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (BlockHeader) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static BlockHeader parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<BlockHeader> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof BlockHeader)) {
                return super.equals(obj);
            }
            BlockHeader blockHeader = (BlockHeader) obj;
            return getTimestamp() == blockHeader.getTimestamp() && getTxTrieRoot().equals(blockHeader.getTxTrieRoot()) && getParentHash().equals(blockHeader.getParentHash()) && getNumber() == blockHeader.getNumber() && getWitnessAddress().equals(blockHeader.getWitnessAddress()) && getVersion() == blockHeader.getVersion() && this.unknownFields.equals(blockHeader.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
        public long getNumber() {
            return this.number_;
        }

        @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
        public ByteString getParentHash() {
            return this.parentHash_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<BlockHeader> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.timestamp_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            if (!this.txTrieRoot_.isEmpty()) {
                z += CodedOutputStream.h(2, this.txTrieRoot_);
            }
            if (!this.parentHash_.isEmpty()) {
                z += CodedOutputStream.h(3, this.parentHash_);
            }
            long j2 = this.number_;
            if (j2 != 0) {
                z += CodedOutputStream.z(7, j2);
            }
            if (!this.witnessAddress_.isEmpty()) {
                z += CodedOutputStream.h(9, this.witnessAddress_);
            }
            int i2 = this.version_;
            if (i2 != 0) {
                z += CodedOutputStream.x(10, i2);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
        public long getTimestamp() {
            return this.timestamp_;
        }

        @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
        public ByteString getTxTrieRoot() {
            return this.txTrieRoot_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // wallet.core.jni.proto.Tron.BlockHeaderOrBuilder
        public ByteString getWitnessAddress() {
            return this.witnessAddress_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getTimestamp())) * 37) + 2) * 53) + getTxTrieRoot().hashCode()) * 37) + 3) * 53) + getParentHash().hashCode()) * 37) + 7) * 53) + a0.h(getNumber())) * 37) + 9) * 53) + getWitnessAddress().hashCode()) * 37) + 10) * 53) + getVersion()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_BlockHeader_fieldAccessorTable.d(BlockHeader.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new BlockHeader();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.timestamp_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            if (!this.txTrieRoot_.isEmpty()) {
                codedOutputStream.q0(2, this.txTrieRoot_);
            }
            if (!this.parentHash_.isEmpty()) {
                codedOutputStream.q0(3, this.parentHash_);
            }
            long j2 = this.number_;
            if (j2 != 0) {
                codedOutputStream.I0(7, j2);
            }
            if (!this.witnessAddress_.isEmpty()) {
                codedOutputStream.q0(9, this.witnessAddress_);
            }
            int i = this.version_;
            if (i != 0) {
                codedOutputStream.G0(10, i);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ BlockHeader(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(BlockHeader blockHeader) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(blockHeader);
        }

        public static BlockHeader parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private BlockHeader(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static BlockHeader parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (BlockHeader) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static BlockHeader parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public BlockHeader getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static BlockHeader parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private BlockHeader() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.txTrieRoot_ = byteString;
            this.parentHash_ = byteString;
            this.witnessAddress_ = byteString;
        }

        public static BlockHeader parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static BlockHeader parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static BlockHeader parseFrom(InputStream inputStream) throws IOException {
            return (BlockHeader) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static BlockHeader parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (BlockHeader) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private BlockHeader(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.timestamp_ = jVar.y();
                            } else if (J == 18) {
                                this.txTrieRoot_ = jVar.q();
                            } else if (J == 26) {
                                this.parentHash_ = jVar.q();
                            } else if (J == 56) {
                                this.number_ = jVar.y();
                            } else if (J == 74) {
                                this.witnessAddress_ = jVar.q();
                            } else if (J != 80) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.version_ = jVar.x();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static BlockHeader parseFrom(j jVar) throws IOException {
            return (BlockHeader) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static BlockHeader parseFrom(j jVar, r rVar) throws IOException {
            return (BlockHeader) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface BlockHeaderOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        long getNumber();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getParentHash();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        long getTimestamp();

        ByteString getTxTrieRoot();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        int getVersion();

        ByteString getWitnessAddress();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class FreezeBalanceContract extends GeneratedMessageV3 implements FreezeBalanceContractOrBuilder {
        public static final int FROZEN_BALANCE_FIELD_NUMBER = 2;
        public static final int FROZEN_DURATION_FIELD_NUMBER = 3;
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        public static final int RECEIVER_ADDRESS_FIELD_NUMBER = 15;
        public static final int RESOURCE_FIELD_NUMBER = 10;
        private static final long serialVersionUID = 0;
        private long frozenBalance_;
        private long frozenDuration_;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private volatile Object receiverAddress_;
        private volatile Object resource_;
        private static final FreezeBalanceContract DEFAULT_INSTANCE = new FreezeBalanceContract();
        private static final t0<FreezeBalanceContract> PARSER = new c<FreezeBalanceContract>() { // from class: wallet.core.jni.proto.Tron.FreezeBalanceContract.1
            @Override // com.google.protobuf.t0
            public FreezeBalanceContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new FreezeBalanceContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements FreezeBalanceContractOrBuilder {
            private long frozenBalance_;
            private long frozenDuration_;
            private Object ownerAddress_;
            private Object receiverAddress_;
            private Object resource_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_FreezeBalanceContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearFrozenBalance() {
                this.frozenBalance_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearFrozenDuration() {
                this.frozenDuration_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = FreezeBalanceContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearReceiverAddress() {
                this.receiverAddress_ = FreezeBalanceContract.getDefaultInstance().getReceiverAddress();
                onChanged();
                return this;
            }

            public Builder clearResource() {
                this.resource_ = FreezeBalanceContract.getDefaultInstance().getResource();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_FreezeBalanceContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public long getFrozenBalance() {
                return this.frozenBalance_;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public long getFrozenDuration() {
                return this.frozenDuration_;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public String getReceiverAddress() {
                Object obj = this.receiverAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.receiverAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public ByteString getReceiverAddressBytes() {
                Object obj = this.receiverAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.receiverAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public String getResource() {
                Object obj = this.resource_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.resource_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
            public ByteString getResourceBytes() {
                Object obj = this.resource_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.resource_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_FreezeBalanceContract_fieldAccessorTable.d(FreezeBalanceContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setFrozenBalance(long j) {
                this.frozenBalance_ = j;
                onChanged();
                return this;
            }

            public Builder setFrozenDuration(long j) {
                this.frozenDuration_ = j;
                onChanged();
                return this;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setReceiverAddress(String str) {
                Objects.requireNonNull(str);
                this.receiverAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setReceiverAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.receiverAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setResource(String str) {
                Objects.requireNonNull(str);
                this.resource_ = str;
                onChanged();
                return this;
            }

            public Builder setResourceBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.resource_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.ownerAddress_ = "";
                this.resource_ = "";
                this.receiverAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FreezeBalanceContract build() {
                FreezeBalanceContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public FreezeBalanceContract buildPartial() {
                FreezeBalanceContract freezeBalanceContract = new FreezeBalanceContract(this, (AnonymousClass1) null);
                freezeBalanceContract.ownerAddress_ = this.ownerAddress_;
                freezeBalanceContract.frozenBalance_ = this.frozenBalance_;
                freezeBalanceContract.frozenDuration_ = this.frozenDuration_;
                freezeBalanceContract.resource_ = this.resource_;
                freezeBalanceContract.receiverAddress_ = this.receiverAddress_;
                onBuilt();
                return freezeBalanceContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public FreezeBalanceContract getDefaultInstanceForType() {
                return FreezeBalanceContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                this.frozenBalance_ = 0L;
                this.frozenDuration_ = 0L;
                this.resource_ = "";
                this.receiverAddress_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof FreezeBalanceContract) {
                    return mergeFrom((FreezeBalanceContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                this.resource_ = "";
                this.receiverAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(FreezeBalanceContract freezeBalanceContract) {
                if (freezeBalanceContract == FreezeBalanceContract.getDefaultInstance()) {
                    return this;
                }
                if (!freezeBalanceContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = freezeBalanceContract.ownerAddress_;
                    onChanged();
                }
                if (freezeBalanceContract.getFrozenBalance() != 0) {
                    setFrozenBalance(freezeBalanceContract.getFrozenBalance());
                }
                if (freezeBalanceContract.getFrozenDuration() != 0) {
                    setFrozenDuration(freezeBalanceContract.getFrozenDuration());
                }
                if (!freezeBalanceContract.getResource().isEmpty()) {
                    this.resource_ = freezeBalanceContract.resource_;
                    onChanged();
                }
                if (!freezeBalanceContract.getReceiverAddress().isEmpty()) {
                    this.receiverAddress_ = freezeBalanceContract.receiverAddress_;
                    onChanged();
                }
                mergeUnknownFields(freezeBalanceContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.FreezeBalanceContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.FreezeBalanceContract.access$5800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$FreezeBalanceContract r3 = (wallet.core.jni.proto.Tron.FreezeBalanceContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$FreezeBalanceContract r4 = (wallet.core.jni.proto.Tron.FreezeBalanceContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.FreezeBalanceContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$FreezeBalanceContract$Builder");
            }
        }

        public /* synthetic */ FreezeBalanceContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static FreezeBalanceContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_FreezeBalanceContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static FreezeBalanceContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (FreezeBalanceContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static FreezeBalanceContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<FreezeBalanceContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FreezeBalanceContract)) {
                return super.equals(obj);
            }
            FreezeBalanceContract freezeBalanceContract = (FreezeBalanceContract) obj;
            return getOwnerAddress().equals(freezeBalanceContract.getOwnerAddress()) && getFrozenBalance() == freezeBalanceContract.getFrozenBalance() && getFrozenDuration() == freezeBalanceContract.getFrozenDuration() && getResource().equals(freezeBalanceContract.getResource()) && getReceiverAddress().equals(freezeBalanceContract.getReceiverAddress()) && this.unknownFields.equals(freezeBalanceContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public long getFrozenBalance() {
            return this.frozenBalance_;
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public long getFrozenDuration() {
            return this.frozenDuration_;
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<FreezeBalanceContract> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public String getReceiverAddress() {
            Object obj = this.receiverAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.receiverAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public ByteString getReceiverAddressBytes() {
            Object obj = this.receiverAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.receiverAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public String getResource() {
            Object obj = this.resource_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.resource_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.FreezeBalanceContractOrBuilder
        public ByteString getResourceBytes() {
            Object obj = this.resource_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.resource_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getOwnerAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.ownerAddress_);
            long j = this.frozenBalance_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(2, j);
            }
            long j2 = this.frozenDuration_;
            if (j2 != 0) {
                computeStringSize += CodedOutputStream.z(3, j2);
            }
            if (!getResourceBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(10, this.resource_);
            }
            if (!getReceiverAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(15, this.receiverAddress_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode()) * 37) + 2) * 53) + a0.h(getFrozenBalance())) * 37) + 3) * 53) + a0.h(getFrozenDuration())) * 37) + 10) * 53) + getResource().hashCode()) * 37) + 15) * 53) + getReceiverAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_FreezeBalanceContract_fieldAccessorTable.d(FreezeBalanceContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new FreezeBalanceContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            long j = this.frozenBalance_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            long j2 = this.frozenDuration_;
            if (j2 != 0) {
                codedOutputStream.I0(3, j2);
            }
            if (!getResourceBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 10, this.resource_);
            }
            if (!getReceiverAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 15, this.receiverAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ FreezeBalanceContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(FreezeBalanceContract freezeBalanceContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(freezeBalanceContract);
        }

        public static FreezeBalanceContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private FreezeBalanceContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static FreezeBalanceContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (FreezeBalanceContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static FreezeBalanceContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public FreezeBalanceContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static FreezeBalanceContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private FreezeBalanceContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
            this.resource_ = "";
            this.receiverAddress_ = "";
        }

        public static FreezeBalanceContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static FreezeBalanceContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static FreezeBalanceContract parseFrom(InputStream inputStream) throws IOException {
            return (FreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static FreezeBalanceContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (FreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private FreezeBalanceContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.ownerAddress_ = jVar.I();
                                } else if (J == 16) {
                                    this.frozenBalance_ = jVar.y();
                                } else if (J == 24) {
                                    this.frozenDuration_ = jVar.y();
                                } else if (J == 82) {
                                    this.resource_ = jVar.I();
                                } else if (J != 122) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.receiverAddress_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static FreezeBalanceContract parseFrom(j jVar) throws IOException {
            return (FreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static FreezeBalanceContract parseFrom(j jVar, r rVar) throws IOException {
            return (FreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface FreezeBalanceContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getFrozenBalance();

        long getFrozenDuration();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        String getReceiverAddress();

        ByteString getReceiverAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getResource();

        ByteString getResourceBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Tron.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };
        public static final int PRIVATE_KEY_FIELD_NUMBER = 2;
        public static final int TRANSACTION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString privateKey_;
        private Transaction transaction_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private ByteString privateKey_;
            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> transactionBuilder_;
            private Transaction transaction_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_SigningInput_descriptor;
            }

            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> getTransactionFieldBuilder() {
                if (this.transactionBuilder_ == null) {
                    this.transactionBuilder_ = new a1<>(getTransaction(), getParentForChildren(), isClean());
                    this.transaction_ = null;
                }
                return this.transactionBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearTransaction() {
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                    onChanged();
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
            public Transaction getTransaction() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction = this.transaction_;
                    return transaction == null ? Transaction.getDefaultInstance() : transaction;
                }
                return a1Var.f();
            }

            public Transaction.Builder getTransactionBuilder() {
                onChanged();
                return getTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
            public TransactionOrBuilder getTransactionOrBuilder() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Transaction transaction = this.transaction_;
                return transaction == null ? Transaction.getDefaultInstance() : transaction;
            }

            @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
            public boolean hasTransaction() {
                return (this.transactionBuilder_ == null && this.transaction_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction2 = this.transaction_;
                    if (transaction2 != null) {
                        this.transaction_ = Transaction.newBuilder(transaction2).mergeFrom(transaction).buildPartial();
                    } else {
                        this.transaction_ = transaction;
                    }
                    onChanged();
                } else {
                    a1Var.h(transaction);
                }
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transaction);
                    this.transaction_ = transaction;
                    onChanged();
                } else {
                    a1Var.j(transaction);
                }
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    signingInput.transaction_ = this.transaction_;
                } else {
                    signingInput.transaction_ = a1Var.b();
                }
                signingInput.privateKey_ = this.privateKey_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                this.privateKey_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder setTransaction(Transaction.Builder builder) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    this.transaction_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.hasTransaction()) {
                    mergeTransaction(signingInput.getTransaction());
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.SigningInput.access$19700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$SigningInput r3 = (wallet.core.jni.proto.Tron.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$SigningInput r4 = (wallet.core.jni.proto.Tron.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (hasTransaction() != signingInput.hasTransaction()) {
                return false;
            }
            return (!hasTransaction() || getTransaction().equals(signingInput.getTransaction())) && getPrivateKey().equals(signingInput.getPrivateKey()) && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.transaction_ != null ? 0 + CodedOutputStream.G(1, getTransaction()) : 0;
            if (!this.privateKey_.isEmpty()) {
                G += CodedOutputStream.h(2, this.privateKey_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
        public Transaction getTransaction() {
            Transaction transaction = this.transaction_;
            return transaction == null ? Transaction.getDefaultInstance() : transaction;
        }

        @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
        public TransactionOrBuilder getTransactionOrBuilder() {
            return getTransaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Tron.SigningInputOrBuilder
        public boolean hasTransaction() {
            return this.transaction_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasTransaction()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getTransaction().hashCode();
            }
            int hashCode2 = (((((hashCode * 37) + 2) * 53) + getPrivateKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.transaction_ != null) {
                codedOutputStream.K0(1, getTransaction());
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(2, this.privateKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.privateKey_ = ByteString.EMPTY;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                Transaction transaction = this.transaction_;
                                Transaction.Builder builder = transaction != null ? transaction.toBuilder() : null;
                                Transaction transaction2 = (Transaction) jVar.z(Transaction.parser(), rVar);
                                this.transaction_ = transaction2;
                                if (builder != null) {
                                    builder.mergeFrom(transaction2);
                                    this.transaction_ = builder.buildPartial();
                                }
                            } else if (J != 18) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.privateKey_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Transaction getTransaction();

        TransactionOrBuilder getTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransaction();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ID_FIELD_NUMBER = 1;
        public static final int JSON_FIELD_NUMBER = 5;
        public static final int REF_BLOCK_BYTES_FIELD_NUMBER = 3;
        public static final int REF_BLOCK_HASH_FIELD_NUMBER = 4;
        public static final int SIGNATURE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString id_;
        private volatile Object json_;
        private byte memoizedIsInitialized;
        private ByteString refBlockBytes_;
        private ByteString refBlockHash_;
        private ByteString signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Tron.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString id_;
            private Object json_;
            private ByteString refBlockBytes_;
            private ByteString refBlockHash_;
            private ByteString signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearId() {
                this.id_ = SigningOutput.getDefaultInstance().getId();
                onChanged();
                return this;
            }

            public Builder clearJson() {
                this.json_ = SigningOutput.getDefaultInstance().getJson();
                onChanged();
                return this;
            }

            public Builder clearRefBlockBytes() {
                this.refBlockBytes_ = SigningOutput.getDefaultInstance().getRefBlockBytes();
                onChanged();
                return this;
            }

            public Builder clearRefBlockHash() {
                this.refBlockHash_ = SigningOutput.getDefaultInstance().getRefBlockHash();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
            public ByteString getId() {
                return this.id_;
            }

            @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
            public String getJson() {
                Object obj = this.json_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.json_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
            public ByteString getJsonBytes() {
                Object obj = this.json_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.json_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
            public ByteString getRefBlockBytes() {
                return this.refBlockBytes_;
            }

            @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
            public ByteString getRefBlockHash() {
                return this.refBlockHash_;
            }

            @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.id_ = byteString;
                onChanged();
                return this;
            }

            public Builder setJson(String str) {
                Objects.requireNonNull(str);
                this.json_ = str;
                onChanged();
                return this;
            }

            public Builder setJsonBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.json_ = byteString;
                onChanged();
                return this;
            }

            public Builder setRefBlockBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.refBlockBytes_ = byteString;
                onChanged();
                return this;
            }

            public Builder setRefBlockHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.refBlockHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.id_ = byteString;
                this.signature_ = byteString;
                this.refBlockBytes_ = byteString;
                this.refBlockHash_ = byteString;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.id_ = this.id_;
                signingOutput.signature_ = this.signature_;
                signingOutput.refBlockBytes_ = this.refBlockBytes_;
                signingOutput.refBlockHash_ = this.refBlockHash_;
                signingOutput.json_ = this.json_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.id_ = byteString;
                this.signature_ = byteString;
                this.refBlockBytes_ = byteString;
                this.refBlockHash_ = byteString;
                this.json_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.id_ = byteString;
                this.signature_ = byteString;
                this.refBlockBytes_ = byteString;
                this.refBlockHash_ = byteString;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                ByteString id = signingOutput.getId();
                ByteString byteString = ByteString.EMPTY;
                if (id != byteString) {
                    setId(signingOutput.getId());
                }
                if (signingOutput.getSignature() != byteString) {
                    setSignature(signingOutput.getSignature());
                }
                if (signingOutput.getRefBlockBytes() != byteString) {
                    setRefBlockBytes(signingOutput.getRefBlockBytes());
                }
                if (signingOutput.getRefBlockHash() != byteString) {
                    setRefBlockHash(signingOutput.getRefBlockHash());
                }
                if (!signingOutput.getJson().isEmpty()) {
                    this.json_ = signingOutput.json_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.SigningOutput.access$21100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$SigningOutput r3 = (wallet.core.jni.proto.Tron.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$SigningOutput r4 = (wallet.core.jni.proto.Tron.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getId().equals(signingOutput.getId()) && getSignature().equals(signingOutput.getSignature()) && getRefBlockBytes().equals(signingOutput.getRefBlockBytes()) && getRefBlockHash().equals(signingOutput.getRefBlockHash()) && getJson().equals(signingOutput.getJson()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
        public ByteString getId() {
            return this.id_;
        }

        @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
        public String getJson() {
            Object obj = this.json_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.json_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
        public ByteString getJsonBytes() {
            Object obj = this.json_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.json_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
        public ByteString getRefBlockBytes() {
            return this.refBlockBytes_;
        }

        @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
        public ByteString getRefBlockHash() {
            return this.refBlockHash_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.id_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.id_);
            if (!this.signature_.isEmpty()) {
                h += CodedOutputStream.h(2, this.signature_);
            }
            if (!this.refBlockBytes_.isEmpty()) {
                h += CodedOutputStream.h(3, this.refBlockBytes_);
            }
            if (!this.refBlockHash_.isEmpty()) {
                h += CodedOutputStream.h(4, this.refBlockHash_);
            }
            if (!getJsonBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(5, this.json_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getId().hashCode()) * 37) + 2) * 53) + getSignature().hashCode()) * 37) + 3) * 53) + getRefBlockBytes().hashCode()) * 37) + 4) * 53) + getRefBlockHash().hashCode()) * 37) + 5) * 53) + getJson().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.id_.isEmpty()) {
                codedOutputStream.q0(1, this.id_);
            }
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(2, this.signature_);
            }
            if (!this.refBlockBytes_.isEmpty()) {
                codedOutputStream.q0(3, this.refBlockBytes_);
            }
            if (!this.refBlockHash_.isEmpty()) {
                codedOutputStream.q0(4, this.refBlockHash_);
            }
            if (!getJsonBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.json_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.id_ = byteString;
            this.signature_ = byteString;
            this.refBlockBytes_ = byteString;
            this.refBlockHash_ = byteString;
            this.json_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.id_ = jVar.q();
                                } else if (J == 18) {
                                    this.signature_ = jVar.q();
                                } else if (J == 26) {
                                    this.refBlockBytes_ = jVar.q();
                                } else if (J == 34) {
                                    this.refBlockHash_ = jVar.q();
                                } else if (J != 42) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.json_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getId();

        /* synthetic */ String getInitializationErrorString();

        String getJson();

        ByteString getJsonBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getRefBlockBytes();

        ByteString getRefBlockHash();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transaction extends GeneratedMessageV3 implements TransactionOrBuilder {
        public static final int BLOCK_HEADER_FIELD_NUMBER = 3;
        public static final int EXPIRATION_FIELD_NUMBER = 2;
        public static final int FEE_LIMIT_FIELD_NUMBER = 4;
        public static final int FREEZE_BALANCE_FIELD_NUMBER = 12;
        public static final int TIMESTAMP_FIELD_NUMBER = 1;
        public static final int TRANSFER_ASSET_FIELD_NUMBER = 11;
        public static final int TRANSFER_FIELD_NUMBER = 10;
        public static final int TRANSFER_TRC20_CONTRACT_FIELD_NUMBER = 19;
        public static final int TRIGGER_SMART_CONTRACT_FIELD_NUMBER = 18;
        public static final int UNFREEZE_ASSET_FIELD_NUMBER = 14;
        public static final int UNFREEZE_BALANCE_FIELD_NUMBER = 13;
        public static final int VOTE_ASSET_FIELD_NUMBER = 16;
        public static final int VOTE_WITNESS_FIELD_NUMBER = 17;
        public static final int WITHDRAW_BALANCE_FIELD_NUMBER = 15;
        private static final long serialVersionUID = 0;
        private BlockHeader blockHeader_;
        private int contractOneofCase_;
        private Object contractOneof_;
        private long expiration_;
        private long feeLimit_;
        private byte memoizedIsInitialized;
        private long timestamp_;
        private static final Transaction DEFAULT_INSTANCE = new Transaction();
        private static final t0<Transaction> PARSER = new c<Transaction>() { // from class: wallet.core.jni.proto.Tron.Transaction.1
            @Override // com.google.protobuf.t0
            public Transaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transaction(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOrBuilder {
            private a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> blockHeaderBuilder_;
            private BlockHeader blockHeader_;
            private int contractOneofCase_;
            private Object contractOneof_;
            private long expiration_;
            private long feeLimit_;
            private a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> freezeBalanceBuilder_;
            private long timestamp_;
            private a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> transferAssetBuilder_;
            private a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> transferBuilder_;
            private a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> transferTrc20ContractBuilder_;
            private a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> triggerSmartContractBuilder_;
            private a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> unfreezeAssetBuilder_;
            private a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> unfreezeBalanceBuilder_;
            private a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> voteAssetBuilder_;
            private a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> voteWitnessBuilder_;
            private a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> withdrawBalanceBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> getBlockHeaderFieldBuilder() {
                if (this.blockHeaderBuilder_ == null) {
                    this.blockHeaderBuilder_ = new a1<>(getBlockHeader(), getParentForChildren(), isClean());
                    this.blockHeader_ = null;
                }
                return this.blockHeaderBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_Transaction_descriptor;
            }

            private a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> getFreezeBalanceFieldBuilder() {
                if (this.freezeBalanceBuilder_ == null) {
                    if (this.contractOneofCase_ != 12) {
                        this.contractOneof_ = FreezeBalanceContract.getDefaultInstance();
                    }
                    this.freezeBalanceBuilder_ = new a1<>((FreezeBalanceContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 12;
                onChanged();
                return this.freezeBalanceBuilder_;
            }

            private a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> getTransferAssetFieldBuilder() {
                if (this.transferAssetBuilder_ == null) {
                    if (this.contractOneofCase_ != 11) {
                        this.contractOneof_ = TransferAssetContract.getDefaultInstance();
                    }
                    this.transferAssetBuilder_ = new a1<>((TransferAssetContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 11;
                onChanged();
                return this.transferAssetBuilder_;
            }

            private a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> getTransferFieldBuilder() {
                if (this.transferBuilder_ == null) {
                    if (this.contractOneofCase_ != 10) {
                        this.contractOneof_ = TransferContract.getDefaultInstance();
                    }
                    this.transferBuilder_ = new a1<>((TransferContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 10;
                onChanged();
                return this.transferBuilder_;
            }

            private a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> getTransferTrc20ContractFieldBuilder() {
                if (this.transferTrc20ContractBuilder_ == null) {
                    if (this.contractOneofCase_ != 19) {
                        this.contractOneof_ = TransferTRC20Contract.getDefaultInstance();
                    }
                    this.transferTrc20ContractBuilder_ = new a1<>((TransferTRC20Contract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 19;
                onChanged();
                return this.transferTrc20ContractBuilder_;
            }

            private a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> getTriggerSmartContractFieldBuilder() {
                if (this.triggerSmartContractBuilder_ == null) {
                    if (this.contractOneofCase_ != 18) {
                        this.contractOneof_ = TriggerSmartContract.getDefaultInstance();
                    }
                    this.triggerSmartContractBuilder_ = new a1<>((TriggerSmartContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 18;
                onChanged();
                return this.triggerSmartContractBuilder_;
            }

            private a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> getUnfreezeAssetFieldBuilder() {
                if (this.unfreezeAssetBuilder_ == null) {
                    if (this.contractOneofCase_ != 14) {
                        this.contractOneof_ = UnfreezeAssetContract.getDefaultInstance();
                    }
                    this.unfreezeAssetBuilder_ = new a1<>((UnfreezeAssetContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 14;
                onChanged();
                return this.unfreezeAssetBuilder_;
            }

            private a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> getUnfreezeBalanceFieldBuilder() {
                if (this.unfreezeBalanceBuilder_ == null) {
                    if (this.contractOneofCase_ != 13) {
                        this.contractOneof_ = UnfreezeBalanceContract.getDefaultInstance();
                    }
                    this.unfreezeBalanceBuilder_ = new a1<>((UnfreezeBalanceContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 13;
                onChanged();
                return this.unfreezeBalanceBuilder_;
            }

            private a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> getVoteAssetFieldBuilder() {
                if (this.voteAssetBuilder_ == null) {
                    if (this.contractOneofCase_ != 16) {
                        this.contractOneof_ = VoteAssetContract.getDefaultInstance();
                    }
                    this.voteAssetBuilder_ = new a1<>((VoteAssetContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 16;
                onChanged();
                return this.voteAssetBuilder_;
            }

            private a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> getVoteWitnessFieldBuilder() {
                if (this.voteWitnessBuilder_ == null) {
                    if (this.contractOneofCase_ != 17) {
                        this.contractOneof_ = VoteWitnessContract.getDefaultInstance();
                    }
                    this.voteWitnessBuilder_ = new a1<>((VoteWitnessContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 17;
                onChanged();
                return this.voteWitnessBuilder_;
            }

            private a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> getWithdrawBalanceFieldBuilder() {
                if (this.withdrawBalanceBuilder_ == null) {
                    if (this.contractOneofCase_ != 15) {
                        this.contractOneof_ = WithdrawBalanceContract.getDefaultInstance();
                    }
                    this.withdrawBalanceBuilder_ = new a1<>((WithdrawBalanceContract) this.contractOneof_, getParentForChildren(), isClean());
                    this.contractOneof_ = null;
                }
                this.contractOneofCase_ = 15;
                onChanged();
                return this.withdrawBalanceBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBlockHeader() {
                if (this.blockHeaderBuilder_ == null) {
                    this.blockHeader_ = null;
                    onChanged();
                } else {
                    this.blockHeader_ = null;
                    this.blockHeaderBuilder_ = null;
                }
                return this;
            }

            public Builder clearContractOneof() {
                this.contractOneofCase_ = 0;
                this.contractOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearExpiration() {
                this.expiration_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearFeeLimit() {
                this.feeLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearFreezeBalance() {
                a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> a1Var = this.freezeBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 12) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 12) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTimestamp() {
                this.timestamp_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearTransfer() {
                a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 10) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 10) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransferAsset() {
                a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> a1Var = this.transferAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 11) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 11) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransferTrc20Contract() {
                a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> a1Var = this.transferTrc20ContractBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 19) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 19) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTriggerSmartContract() {
                a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> a1Var = this.triggerSmartContractBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 18) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 18) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearUnfreezeAsset() {
                a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> a1Var = this.unfreezeAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 14) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 14) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearUnfreezeBalance() {
                a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> a1Var = this.unfreezeBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 13) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 13) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearVoteAsset() {
                a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> a1Var = this.voteAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 16) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 16) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearVoteWitness() {
                a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> a1Var = this.voteWitnessBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 17) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 17) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearWithdrawBalance() {
                a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> a1Var = this.withdrawBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 15) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.contractOneofCase_ == 15) {
                        this.contractOneofCase_ = 0;
                        this.contractOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public BlockHeader getBlockHeader() {
                a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> a1Var = this.blockHeaderBuilder_;
                if (a1Var == null) {
                    BlockHeader blockHeader = this.blockHeader_;
                    return blockHeader == null ? BlockHeader.getDefaultInstance() : blockHeader;
                }
                return a1Var.f();
            }

            public BlockHeader.Builder getBlockHeaderBuilder() {
                onChanged();
                return getBlockHeaderFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public BlockHeaderOrBuilder getBlockHeaderOrBuilder() {
                a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> a1Var = this.blockHeaderBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                BlockHeader blockHeader = this.blockHeader_;
                return blockHeader == null ? BlockHeader.getDefaultInstance() : blockHeader;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public ContractOneofCase getContractOneofCase() {
                return ContractOneofCase.forNumber(this.contractOneofCase_);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_Transaction_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public long getExpiration() {
                return this.expiration_;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public long getFeeLimit() {
                return this.feeLimit_;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public FreezeBalanceContract getFreezeBalance() {
                a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> a1Var = this.freezeBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 12) {
                        return (FreezeBalanceContract) this.contractOneof_;
                    }
                    return FreezeBalanceContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 12) {
                    return a1Var.f();
                } else {
                    return FreezeBalanceContract.getDefaultInstance();
                }
            }

            public FreezeBalanceContract.Builder getFreezeBalanceBuilder() {
                return getFreezeBalanceFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public FreezeBalanceContractOrBuilder getFreezeBalanceOrBuilder() {
                a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 12 || (a1Var = this.freezeBalanceBuilder_) == null) {
                    if (i == 12) {
                        return (FreezeBalanceContract) this.contractOneof_;
                    }
                    return FreezeBalanceContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public long getTimestamp() {
                return this.timestamp_;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TransferContract getTransfer() {
                a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 10) {
                        return (TransferContract) this.contractOneof_;
                    }
                    return TransferContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 10) {
                    return a1Var.f();
                } else {
                    return TransferContract.getDefaultInstance();
                }
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TransferAssetContract getTransferAsset() {
                a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> a1Var = this.transferAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 11) {
                        return (TransferAssetContract) this.contractOneof_;
                    }
                    return TransferAssetContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 11) {
                    return a1Var.f();
                } else {
                    return TransferAssetContract.getDefaultInstance();
                }
            }

            public TransferAssetContract.Builder getTransferAssetBuilder() {
                return getTransferAssetFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TransferAssetContractOrBuilder getTransferAssetOrBuilder() {
                a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 11 || (a1Var = this.transferAssetBuilder_) == null) {
                    if (i == 11) {
                        return (TransferAssetContract) this.contractOneof_;
                    }
                    return TransferAssetContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            public TransferContract.Builder getTransferBuilder() {
                return getTransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TransferContractOrBuilder getTransferOrBuilder() {
                a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 10 || (a1Var = this.transferBuilder_) == null) {
                    if (i == 10) {
                        return (TransferContract) this.contractOneof_;
                    }
                    return TransferContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TransferTRC20Contract getTransferTrc20Contract() {
                a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> a1Var = this.transferTrc20ContractBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 19) {
                        return (TransferTRC20Contract) this.contractOneof_;
                    }
                    return TransferTRC20Contract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 19) {
                    return a1Var.f();
                } else {
                    return TransferTRC20Contract.getDefaultInstance();
                }
            }

            public TransferTRC20Contract.Builder getTransferTrc20ContractBuilder() {
                return getTransferTrc20ContractFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TransferTRC20ContractOrBuilder getTransferTrc20ContractOrBuilder() {
                a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 19 || (a1Var = this.transferTrc20ContractBuilder_) == null) {
                    if (i == 19) {
                        return (TransferTRC20Contract) this.contractOneof_;
                    }
                    return TransferTRC20Contract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TriggerSmartContract getTriggerSmartContract() {
                a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> a1Var = this.triggerSmartContractBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 18) {
                        return (TriggerSmartContract) this.contractOneof_;
                    }
                    return TriggerSmartContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 18) {
                    return a1Var.f();
                } else {
                    return TriggerSmartContract.getDefaultInstance();
                }
            }

            public TriggerSmartContract.Builder getTriggerSmartContractBuilder() {
                return getTriggerSmartContractFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public TriggerSmartContractOrBuilder getTriggerSmartContractOrBuilder() {
                a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 18 || (a1Var = this.triggerSmartContractBuilder_) == null) {
                    if (i == 18) {
                        return (TriggerSmartContract) this.contractOneof_;
                    }
                    return TriggerSmartContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public UnfreezeAssetContract getUnfreezeAsset() {
                a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> a1Var = this.unfreezeAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 14) {
                        return (UnfreezeAssetContract) this.contractOneof_;
                    }
                    return UnfreezeAssetContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 14) {
                    return a1Var.f();
                } else {
                    return UnfreezeAssetContract.getDefaultInstance();
                }
            }

            public UnfreezeAssetContract.Builder getUnfreezeAssetBuilder() {
                return getUnfreezeAssetFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public UnfreezeAssetContractOrBuilder getUnfreezeAssetOrBuilder() {
                a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 14 || (a1Var = this.unfreezeAssetBuilder_) == null) {
                    if (i == 14) {
                        return (UnfreezeAssetContract) this.contractOneof_;
                    }
                    return UnfreezeAssetContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public UnfreezeBalanceContract getUnfreezeBalance() {
                a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> a1Var = this.unfreezeBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 13) {
                        return (UnfreezeBalanceContract) this.contractOneof_;
                    }
                    return UnfreezeBalanceContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 13) {
                    return a1Var.f();
                } else {
                    return UnfreezeBalanceContract.getDefaultInstance();
                }
            }

            public UnfreezeBalanceContract.Builder getUnfreezeBalanceBuilder() {
                return getUnfreezeBalanceFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public UnfreezeBalanceContractOrBuilder getUnfreezeBalanceOrBuilder() {
                a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 13 || (a1Var = this.unfreezeBalanceBuilder_) == null) {
                    if (i == 13) {
                        return (UnfreezeBalanceContract) this.contractOneof_;
                    }
                    return UnfreezeBalanceContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public VoteAssetContract getVoteAsset() {
                a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> a1Var = this.voteAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 16) {
                        return (VoteAssetContract) this.contractOneof_;
                    }
                    return VoteAssetContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 16) {
                    return a1Var.f();
                } else {
                    return VoteAssetContract.getDefaultInstance();
                }
            }

            public VoteAssetContract.Builder getVoteAssetBuilder() {
                return getVoteAssetFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public VoteAssetContractOrBuilder getVoteAssetOrBuilder() {
                a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 16 || (a1Var = this.voteAssetBuilder_) == null) {
                    if (i == 16) {
                        return (VoteAssetContract) this.contractOneof_;
                    }
                    return VoteAssetContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public VoteWitnessContract getVoteWitness() {
                a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> a1Var = this.voteWitnessBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 17) {
                        return (VoteWitnessContract) this.contractOneof_;
                    }
                    return VoteWitnessContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 17) {
                    return a1Var.f();
                } else {
                    return VoteWitnessContract.getDefaultInstance();
                }
            }

            public VoteWitnessContract.Builder getVoteWitnessBuilder() {
                return getVoteWitnessFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public VoteWitnessContractOrBuilder getVoteWitnessOrBuilder() {
                a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 17 || (a1Var = this.voteWitnessBuilder_) == null) {
                    if (i == 17) {
                        return (VoteWitnessContract) this.contractOneof_;
                    }
                    return VoteWitnessContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public WithdrawBalanceContract getWithdrawBalance() {
                a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> a1Var = this.withdrawBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 15) {
                        return (WithdrawBalanceContract) this.contractOneof_;
                    }
                    return WithdrawBalanceContract.getDefaultInstance();
                } else if (this.contractOneofCase_ == 15) {
                    return a1Var.f();
                } else {
                    return WithdrawBalanceContract.getDefaultInstance();
                }
            }

            public WithdrawBalanceContract.Builder getWithdrawBalanceBuilder() {
                return getWithdrawBalanceFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public WithdrawBalanceContractOrBuilder getWithdrawBalanceOrBuilder() {
                a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> a1Var;
                int i = this.contractOneofCase_;
                if (i != 15 || (a1Var = this.withdrawBalanceBuilder_) == null) {
                    if (i == 15) {
                        return (WithdrawBalanceContract) this.contractOneof_;
                    }
                    return WithdrawBalanceContract.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasBlockHeader() {
                return (this.blockHeaderBuilder_ == null && this.blockHeader_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasFreezeBalance() {
                return this.contractOneofCase_ == 12;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasTransfer() {
                return this.contractOneofCase_ == 10;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasTransferAsset() {
                return this.contractOneofCase_ == 11;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasTransferTrc20Contract() {
                return this.contractOneofCase_ == 19;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasTriggerSmartContract() {
                return this.contractOneofCase_ == 18;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasUnfreezeAsset() {
                return this.contractOneofCase_ == 14;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasUnfreezeBalance() {
                return this.contractOneofCase_ == 13;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasVoteAsset() {
                return this.contractOneofCase_ == 16;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasVoteWitness() {
                return this.contractOneofCase_ == 17;
            }

            @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
            public boolean hasWithdrawBalance() {
                return this.contractOneofCase_ == 15;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeBlockHeader(BlockHeader blockHeader) {
                a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> a1Var = this.blockHeaderBuilder_;
                if (a1Var == null) {
                    BlockHeader blockHeader2 = this.blockHeader_;
                    if (blockHeader2 != null) {
                        this.blockHeader_ = BlockHeader.newBuilder(blockHeader2).mergeFrom(blockHeader).buildPartial();
                    } else {
                        this.blockHeader_ = blockHeader;
                    }
                    onChanged();
                } else {
                    a1Var.h(blockHeader);
                }
                return this;
            }

            public Builder mergeFreezeBalance(FreezeBalanceContract freezeBalanceContract) {
                a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> a1Var = this.freezeBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 12 && this.contractOneof_ != FreezeBalanceContract.getDefaultInstance()) {
                        this.contractOneof_ = FreezeBalanceContract.newBuilder((FreezeBalanceContract) this.contractOneof_).mergeFrom(freezeBalanceContract).buildPartial();
                    } else {
                        this.contractOneof_ = freezeBalanceContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 12) {
                        a1Var.h(freezeBalanceContract);
                    }
                    this.freezeBalanceBuilder_.j(freezeBalanceContract);
                }
                this.contractOneofCase_ = 12;
                return this;
            }

            public Builder mergeTransfer(TransferContract transferContract) {
                a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 10 && this.contractOneof_ != TransferContract.getDefaultInstance()) {
                        this.contractOneof_ = TransferContract.newBuilder((TransferContract) this.contractOneof_).mergeFrom(transferContract).buildPartial();
                    } else {
                        this.contractOneof_ = transferContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 10) {
                        a1Var.h(transferContract);
                    }
                    this.transferBuilder_.j(transferContract);
                }
                this.contractOneofCase_ = 10;
                return this;
            }

            public Builder mergeTransferAsset(TransferAssetContract transferAssetContract) {
                a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> a1Var = this.transferAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 11 && this.contractOneof_ != TransferAssetContract.getDefaultInstance()) {
                        this.contractOneof_ = TransferAssetContract.newBuilder((TransferAssetContract) this.contractOneof_).mergeFrom(transferAssetContract).buildPartial();
                    } else {
                        this.contractOneof_ = transferAssetContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 11) {
                        a1Var.h(transferAssetContract);
                    }
                    this.transferAssetBuilder_.j(transferAssetContract);
                }
                this.contractOneofCase_ = 11;
                return this;
            }

            public Builder mergeTransferTrc20Contract(TransferTRC20Contract transferTRC20Contract) {
                a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> a1Var = this.transferTrc20ContractBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 19 && this.contractOneof_ != TransferTRC20Contract.getDefaultInstance()) {
                        this.contractOneof_ = TransferTRC20Contract.newBuilder((TransferTRC20Contract) this.contractOneof_).mergeFrom(transferTRC20Contract).buildPartial();
                    } else {
                        this.contractOneof_ = transferTRC20Contract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 19) {
                        a1Var.h(transferTRC20Contract);
                    }
                    this.transferTrc20ContractBuilder_.j(transferTRC20Contract);
                }
                this.contractOneofCase_ = 19;
                return this;
            }

            public Builder mergeTriggerSmartContract(TriggerSmartContract triggerSmartContract) {
                a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> a1Var = this.triggerSmartContractBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 18 && this.contractOneof_ != TriggerSmartContract.getDefaultInstance()) {
                        this.contractOneof_ = TriggerSmartContract.newBuilder((TriggerSmartContract) this.contractOneof_).mergeFrom(triggerSmartContract).buildPartial();
                    } else {
                        this.contractOneof_ = triggerSmartContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 18) {
                        a1Var.h(triggerSmartContract);
                    }
                    this.triggerSmartContractBuilder_.j(triggerSmartContract);
                }
                this.contractOneofCase_ = 18;
                return this;
            }

            public Builder mergeUnfreezeAsset(UnfreezeAssetContract unfreezeAssetContract) {
                a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> a1Var = this.unfreezeAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 14 && this.contractOneof_ != UnfreezeAssetContract.getDefaultInstance()) {
                        this.contractOneof_ = UnfreezeAssetContract.newBuilder((UnfreezeAssetContract) this.contractOneof_).mergeFrom(unfreezeAssetContract).buildPartial();
                    } else {
                        this.contractOneof_ = unfreezeAssetContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 14) {
                        a1Var.h(unfreezeAssetContract);
                    }
                    this.unfreezeAssetBuilder_.j(unfreezeAssetContract);
                }
                this.contractOneofCase_ = 14;
                return this;
            }

            public Builder mergeUnfreezeBalance(UnfreezeBalanceContract unfreezeBalanceContract) {
                a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> a1Var = this.unfreezeBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 13 && this.contractOneof_ != UnfreezeBalanceContract.getDefaultInstance()) {
                        this.contractOneof_ = UnfreezeBalanceContract.newBuilder((UnfreezeBalanceContract) this.contractOneof_).mergeFrom(unfreezeBalanceContract).buildPartial();
                    } else {
                        this.contractOneof_ = unfreezeBalanceContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 13) {
                        a1Var.h(unfreezeBalanceContract);
                    }
                    this.unfreezeBalanceBuilder_.j(unfreezeBalanceContract);
                }
                this.contractOneofCase_ = 13;
                return this;
            }

            public Builder mergeVoteAsset(VoteAssetContract voteAssetContract) {
                a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> a1Var = this.voteAssetBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 16 && this.contractOneof_ != VoteAssetContract.getDefaultInstance()) {
                        this.contractOneof_ = VoteAssetContract.newBuilder((VoteAssetContract) this.contractOneof_).mergeFrom(voteAssetContract).buildPartial();
                    } else {
                        this.contractOneof_ = voteAssetContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 16) {
                        a1Var.h(voteAssetContract);
                    }
                    this.voteAssetBuilder_.j(voteAssetContract);
                }
                this.contractOneofCase_ = 16;
                return this;
            }

            public Builder mergeVoteWitness(VoteWitnessContract voteWitnessContract) {
                a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> a1Var = this.voteWitnessBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 17 && this.contractOneof_ != VoteWitnessContract.getDefaultInstance()) {
                        this.contractOneof_ = VoteWitnessContract.newBuilder((VoteWitnessContract) this.contractOneof_).mergeFrom(voteWitnessContract).buildPartial();
                    } else {
                        this.contractOneof_ = voteWitnessContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 17) {
                        a1Var.h(voteWitnessContract);
                    }
                    this.voteWitnessBuilder_.j(voteWitnessContract);
                }
                this.contractOneofCase_ = 17;
                return this;
            }

            public Builder mergeWithdrawBalance(WithdrawBalanceContract withdrawBalanceContract) {
                a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> a1Var = this.withdrawBalanceBuilder_;
                if (a1Var == null) {
                    if (this.contractOneofCase_ == 15 && this.contractOneof_ != WithdrawBalanceContract.getDefaultInstance()) {
                        this.contractOneof_ = WithdrawBalanceContract.newBuilder((WithdrawBalanceContract) this.contractOneof_).mergeFrom(withdrawBalanceContract).buildPartial();
                    } else {
                        this.contractOneof_ = withdrawBalanceContract;
                    }
                    onChanged();
                } else {
                    if (this.contractOneofCase_ == 15) {
                        a1Var.h(withdrawBalanceContract);
                    }
                    this.withdrawBalanceBuilder_.j(withdrawBalanceContract);
                }
                this.contractOneofCase_ = 15;
                return this;
            }

            public Builder setBlockHeader(BlockHeader blockHeader) {
                a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> a1Var = this.blockHeaderBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(blockHeader);
                    this.blockHeader_ = blockHeader;
                    onChanged();
                } else {
                    a1Var.j(blockHeader);
                }
                return this;
            }

            public Builder setExpiration(long j) {
                this.expiration_ = j;
                onChanged();
                return this;
            }

            public Builder setFeeLimit(long j) {
                this.feeLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setFreezeBalance(FreezeBalanceContract freezeBalanceContract) {
                a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> a1Var = this.freezeBalanceBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(freezeBalanceContract);
                    this.contractOneof_ = freezeBalanceContract;
                    onChanged();
                } else {
                    a1Var.j(freezeBalanceContract);
                }
                this.contractOneofCase_ = 12;
                return this;
            }

            public Builder setTimestamp(long j) {
                this.timestamp_ = j;
                onChanged();
                return this;
            }

            public Builder setTransfer(TransferContract transferContract) {
                a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transferContract);
                    this.contractOneof_ = transferContract;
                    onChanged();
                } else {
                    a1Var.j(transferContract);
                }
                this.contractOneofCase_ = 10;
                return this;
            }

            public Builder setTransferAsset(TransferAssetContract transferAssetContract) {
                a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> a1Var = this.transferAssetBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transferAssetContract);
                    this.contractOneof_ = transferAssetContract;
                    onChanged();
                } else {
                    a1Var.j(transferAssetContract);
                }
                this.contractOneofCase_ = 11;
                return this;
            }

            public Builder setTransferTrc20Contract(TransferTRC20Contract transferTRC20Contract) {
                a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> a1Var = this.transferTrc20ContractBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transferTRC20Contract);
                    this.contractOneof_ = transferTRC20Contract;
                    onChanged();
                } else {
                    a1Var.j(transferTRC20Contract);
                }
                this.contractOneofCase_ = 19;
                return this;
            }

            public Builder setTriggerSmartContract(TriggerSmartContract triggerSmartContract) {
                a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> a1Var = this.triggerSmartContractBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(triggerSmartContract);
                    this.contractOneof_ = triggerSmartContract;
                    onChanged();
                } else {
                    a1Var.j(triggerSmartContract);
                }
                this.contractOneofCase_ = 18;
                return this;
            }

            public Builder setUnfreezeAsset(UnfreezeAssetContract unfreezeAssetContract) {
                a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> a1Var = this.unfreezeAssetBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(unfreezeAssetContract);
                    this.contractOneof_ = unfreezeAssetContract;
                    onChanged();
                } else {
                    a1Var.j(unfreezeAssetContract);
                }
                this.contractOneofCase_ = 14;
                return this;
            }

            public Builder setUnfreezeBalance(UnfreezeBalanceContract unfreezeBalanceContract) {
                a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> a1Var = this.unfreezeBalanceBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(unfreezeBalanceContract);
                    this.contractOneof_ = unfreezeBalanceContract;
                    onChanged();
                } else {
                    a1Var.j(unfreezeBalanceContract);
                }
                this.contractOneofCase_ = 13;
                return this;
            }

            public Builder setVoteAsset(VoteAssetContract voteAssetContract) {
                a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> a1Var = this.voteAssetBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(voteAssetContract);
                    this.contractOneof_ = voteAssetContract;
                    onChanged();
                } else {
                    a1Var.j(voteAssetContract);
                }
                this.contractOneofCase_ = 16;
                return this;
            }

            public Builder setVoteWitness(VoteWitnessContract voteWitnessContract) {
                a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> a1Var = this.voteWitnessBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(voteWitnessContract);
                    this.contractOneof_ = voteWitnessContract;
                    onChanged();
                } else {
                    a1Var.j(voteWitnessContract);
                }
                this.contractOneofCase_ = 17;
                return this;
            }

            public Builder setWithdrawBalance(WithdrawBalanceContract withdrawBalanceContract) {
                a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> a1Var = this.withdrawBalanceBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(withdrawBalanceContract);
                    this.contractOneof_ = withdrawBalanceContract;
                    onChanged();
                } else {
                    a1Var.j(withdrawBalanceContract);
                }
                this.contractOneofCase_ = 15;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.contractOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction build() {
                Transaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction buildPartial() {
                Transaction transaction = new Transaction(this, (AnonymousClass1) null);
                transaction.timestamp_ = this.timestamp_;
                transaction.expiration_ = this.expiration_;
                a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> a1Var = this.blockHeaderBuilder_;
                if (a1Var == null) {
                    transaction.blockHeader_ = this.blockHeader_;
                } else {
                    transaction.blockHeader_ = a1Var.b();
                }
                transaction.feeLimit_ = this.feeLimit_;
                if (this.contractOneofCase_ == 10) {
                    a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> a1Var2 = this.transferBuilder_;
                    if (a1Var2 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var2.b();
                    }
                }
                if (this.contractOneofCase_ == 11) {
                    a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> a1Var3 = this.transferAssetBuilder_;
                    if (a1Var3 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var3.b();
                    }
                }
                if (this.contractOneofCase_ == 12) {
                    a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> a1Var4 = this.freezeBalanceBuilder_;
                    if (a1Var4 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var4.b();
                    }
                }
                if (this.contractOneofCase_ == 13) {
                    a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> a1Var5 = this.unfreezeBalanceBuilder_;
                    if (a1Var5 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var5.b();
                    }
                }
                if (this.contractOneofCase_ == 14) {
                    a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> a1Var6 = this.unfreezeAssetBuilder_;
                    if (a1Var6 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var6.b();
                    }
                }
                if (this.contractOneofCase_ == 15) {
                    a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> a1Var7 = this.withdrawBalanceBuilder_;
                    if (a1Var7 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var7.b();
                    }
                }
                if (this.contractOneofCase_ == 16) {
                    a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> a1Var8 = this.voteAssetBuilder_;
                    if (a1Var8 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var8.b();
                    }
                }
                if (this.contractOneofCase_ == 17) {
                    a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> a1Var9 = this.voteWitnessBuilder_;
                    if (a1Var9 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var9.b();
                    }
                }
                if (this.contractOneofCase_ == 18) {
                    a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> a1Var10 = this.triggerSmartContractBuilder_;
                    if (a1Var10 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var10.b();
                    }
                }
                if (this.contractOneofCase_ == 19) {
                    a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> a1Var11 = this.transferTrc20ContractBuilder_;
                    if (a1Var11 == null) {
                        transaction.contractOneof_ = this.contractOneof_;
                    } else {
                        transaction.contractOneof_ = a1Var11.b();
                    }
                }
                transaction.contractOneofCase_ = this.contractOneofCase_;
                onBuilt();
                return transaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transaction getDefaultInstanceForType() {
                return Transaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.timestamp_ = 0L;
                this.expiration_ = 0L;
                if (this.blockHeaderBuilder_ == null) {
                    this.blockHeader_ = null;
                } else {
                    this.blockHeader_ = null;
                    this.blockHeaderBuilder_ = null;
                }
                this.feeLimit_ = 0L;
                this.contractOneofCase_ = 0;
                this.contractOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.contractOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            public Builder setBlockHeader(BlockHeader.Builder builder) {
                a1<BlockHeader, BlockHeader.Builder, BlockHeaderOrBuilder> a1Var = this.blockHeaderBuilder_;
                if (a1Var == null) {
                    this.blockHeader_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transaction) {
                    return mergeFrom((Transaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setFreezeBalance(FreezeBalanceContract.Builder builder) {
                a1<FreezeBalanceContract, FreezeBalanceContract.Builder, FreezeBalanceContractOrBuilder> a1Var = this.freezeBalanceBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 12;
                return this;
            }

            public Builder setTransfer(TransferContract.Builder builder) {
                a1<TransferContract, TransferContract.Builder, TransferContractOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 10;
                return this;
            }

            public Builder setTransferAsset(TransferAssetContract.Builder builder) {
                a1<TransferAssetContract, TransferAssetContract.Builder, TransferAssetContractOrBuilder> a1Var = this.transferAssetBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 11;
                return this;
            }

            public Builder setTransferTrc20Contract(TransferTRC20Contract.Builder builder) {
                a1<TransferTRC20Contract, TransferTRC20Contract.Builder, TransferTRC20ContractOrBuilder> a1Var = this.transferTrc20ContractBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 19;
                return this;
            }

            public Builder setTriggerSmartContract(TriggerSmartContract.Builder builder) {
                a1<TriggerSmartContract, TriggerSmartContract.Builder, TriggerSmartContractOrBuilder> a1Var = this.triggerSmartContractBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 18;
                return this;
            }

            public Builder setUnfreezeAsset(UnfreezeAssetContract.Builder builder) {
                a1<UnfreezeAssetContract, UnfreezeAssetContract.Builder, UnfreezeAssetContractOrBuilder> a1Var = this.unfreezeAssetBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 14;
                return this;
            }

            public Builder setUnfreezeBalance(UnfreezeBalanceContract.Builder builder) {
                a1<UnfreezeBalanceContract, UnfreezeBalanceContract.Builder, UnfreezeBalanceContractOrBuilder> a1Var = this.unfreezeBalanceBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 13;
                return this;
            }

            public Builder setVoteAsset(VoteAssetContract.Builder builder) {
                a1<VoteAssetContract, VoteAssetContract.Builder, VoteAssetContractOrBuilder> a1Var = this.voteAssetBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 16;
                return this;
            }

            public Builder setVoteWitness(VoteWitnessContract.Builder builder) {
                a1<VoteWitnessContract, VoteWitnessContract.Builder, VoteWitnessContractOrBuilder> a1Var = this.voteWitnessBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 17;
                return this;
            }

            public Builder setWithdrawBalance(WithdrawBalanceContract.Builder builder) {
                a1<WithdrawBalanceContract, WithdrawBalanceContract.Builder, WithdrawBalanceContractOrBuilder> a1Var = this.withdrawBalanceBuilder_;
                if (a1Var == null) {
                    this.contractOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.contractOneofCase_ = 15;
                return this;
            }

            public Builder mergeFrom(Transaction transaction) {
                if (transaction == Transaction.getDefaultInstance()) {
                    return this;
                }
                if (transaction.getTimestamp() != 0) {
                    setTimestamp(transaction.getTimestamp());
                }
                if (transaction.getExpiration() != 0) {
                    setExpiration(transaction.getExpiration());
                }
                if (transaction.hasBlockHeader()) {
                    mergeBlockHeader(transaction.getBlockHeader());
                }
                if (transaction.getFeeLimit() != 0) {
                    setFeeLimit(transaction.getFeeLimit());
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Tron$Transaction$ContractOneofCase[transaction.getContractOneofCase().ordinal()]) {
                    case 1:
                        mergeTransfer(transaction.getTransfer());
                        break;
                    case 2:
                        mergeTransferAsset(transaction.getTransferAsset());
                        break;
                    case 3:
                        mergeFreezeBalance(transaction.getFreezeBalance());
                        break;
                    case 4:
                        mergeUnfreezeBalance(transaction.getUnfreezeBalance());
                        break;
                    case 5:
                        mergeUnfreezeAsset(transaction.getUnfreezeAsset());
                        break;
                    case 6:
                        mergeWithdrawBalance(transaction.getWithdrawBalance());
                        break;
                    case 7:
                        mergeVoteAsset(transaction.getVoteAsset());
                        break;
                    case 8:
                        mergeVoteWitness(transaction.getVoteWitness());
                        break;
                    case 9:
                        mergeTriggerSmartContract(transaction.getTriggerSmartContract());
                        break;
                    case 10:
                        mergeTransferTrc20Contract(transaction.getTransferTrc20Contract());
                        break;
                }
                mergeUnknownFields(transaction.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.Transaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.Transaction.access$18600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$Transaction r3 = (wallet.core.jni.proto.Tron.Transaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$Transaction r4 = (wallet.core.jni.proto.Tron.Transaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.Transaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$Transaction$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum ContractOneofCase implements a0.c {
            TRANSFER(10),
            TRANSFER_ASSET(11),
            FREEZE_BALANCE(12),
            UNFREEZE_BALANCE(13),
            UNFREEZE_ASSET(14),
            WITHDRAW_BALANCE(15),
            VOTE_ASSET(16),
            VOTE_WITNESS(17),
            TRIGGER_SMART_CONTRACT(18),
            TRANSFER_TRC20_CONTRACT(19),
            CONTRACTONEOF_NOT_SET(0);
            
            private final int value;

            ContractOneofCase(int i) {
                this.value = i;
            }

            public static ContractOneofCase forNumber(int i) {
                if (i != 0) {
                    switch (i) {
                        case 10:
                            return TRANSFER;
                        case 11:
                            return TRANSFER_ASSET;
                        case 12:
                            return FREEZE_BALANCE;
                        case 13:
                            return UNFREEZE_BALANCE;
                        case 14:
                            return UNFREEZE_ASSET;
                        case 15:
                            return WITHDRAW_BALANCE;
                        case 16:
                            return VOTE_ASSET;
                        case 17:
                            return VOTE_WITNESS;
                        case 18:
                            return TRIGGER_SMART_CONTRACT;
                        case 19:
                            return TRANSFER_TRC20_CONTRACT;
                        default:
                            return null;
                    }
                }
                return CONTRACTONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static ContractOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ Transaction(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Transaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_Transaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transaction)) {
                return super.equals(obj);
            }
            Transaction transaction = (Transaction) obj;
            if (getTimestamp() == transaction.getTimestamp() && getExpiration() == transaction.getExpiration() && hasBlockHeader() == transaction.hasBlockHeader()) {
                if ((!hasBlockHeader() || getBlockHeader().equals(transaction.getBlockHeader())) && getFeeLimit() == transaction.getFeeLimit() && getContractOneofCase().equals(transaction.getContractOneofCase())) {
                    switch (this.contractOneofCase_) {
                        case 10:
                            if (!getTransfer().equals(transaction.getTransfer())) {
                                return false;
                            }
                            break;
                        case 11:
                            if (!getTransferAsset().equals(transaction.getTransferAsset())) {
                                return false;
                            }
                            break;
                        case 12:
                            if (!getFreezeBalance().equals(transaction.getFreezeBalance())) {
                                return false;
                            }
                            break;
                        case 13:
                            if (!getUnfreezeBalance().equals(transaction.getUnfreezeBalance())) {
                                return false;
                            }
                            break;
                        case 14:
                            if (!getUnfreezeAsset().equals(transaction.getUnfreezeAsset())) {
                                return false;
                            }
                            break;
                        case 15:
                            if (!getWithdrawBalance().equals(transaction.getWithdrawBalance())) {
                                return false;
                            }
                            break;
                        case 16:
                            if (!getVoteAsset().equals(transaction.getVoteAsset())) {
                                return false;
                            }
                            break;
                        case 17:
                            if (!getVoteWitness().equals(transaction.getVoteWitness())) {
                                return false;
                            }
                            break;
                        case 18:
                            if (!getTriggerSmartContract().equals(transaction.getTriggerSmartContract())) {
                                return false;
                            }
                            break;
                        case 19:
                            if (!getTransferTrc20Contract().equals(transaction.getTransferTrc20Contract())) {
                                return false;
                            }
                            break;
                    }
                    return this.unknownFields.equals(transaction.unknownFields);
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public BlockHeader getBlockHeader() {
            BlockHeader blockHeader = this.blockHeader_;
            return blockHeader == null ? BlockHeader.getDefaultInstance() : blockHeader;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public BlockHeaderOrBuilder getBlockHeaderOrBuilder() {
            return getBlockHeader();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public ContractOneofCase getContractOneofCase() {
            return ContractOneofCase.forNumber(this.contractOneofCase_);
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public long getExpiration() {
            return this.expiration_;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public long getFeeLimit() {
            return this.feeLimit_;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public FreezeBalanceContract getFreezeBalance() {
            if (this.contractOneofCase_ == 12) {
                return (FreezeBalanceContract) this.contractOneof_;
            }
            return FreezeBalanceContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public FreezeBalanceContractOrBuilder getFreezeBalanceOrBuilder() {
            if (this.contractOneofCase_ == 12) {
                return (FreezeBalanceContract) this.contractOneof_;
            }
            return FreezeBalanceContract.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transaction> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.timestamp_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            long j2 = this.expiration_;
            if (j2 != 0) {
                z += CodedOutputStream.z(2, j2);
            }
            if (this.blockHeader_ != null) {
                z += CodedOutputStream.G(3, getBlockHeader());
            }
            long j3 = this.feeLimit_;
            if (j3 != 0) {
                z += CodedOutputStream.z(4, j3);
            }
            if (this.contractOneofCase_ == 10) {
                z += CodedOutputStream.G(10, (TransferContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 11) {
                z += CodedOutputStream.G(11, (TransferAssetContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 12) {
                z += CodedOutputStream.G(12, (FreezeBalanceContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 13) {
                z += CodedOutputStream.G(13, (UnfreezeBalanceContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 14) {
                z += CodedOutputStream.G(14, (UnfreezeAssetContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 15) {
                z += CodedOutputStream.G(15, (WithdrawBalanceContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 16) {
                z += CodedOutputStream.G(16, (VoteAssetContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 17) {
                z += CodedOutputStream.G(17, (VoteWitnessContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 18) {
                z += CodedOutputStream.G(18, (TriggerSmartContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 19) {
                z += CodedOutputStream.G(19, (TransferTRC20Contract) this.contractOneof_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public long getTimestamp() {
            return this.timestamp_;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TransferContract getTransfer() {
            if (this.contractOneofCase_ == 10) {
                return (TransferContract) this.contractOneof_;
            }
            return TransferContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TransferAssetContract getTransferAsset() {
            if (this.contractOneofCase_ == 11) {
                return (TransferAssetContract) this.contractOneof_;
            }
            return TransferAssetContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TransferAssetContractOrBuilder getTransferAssetOrBuilder() {
            if (this.contractOneofCase_ == 11) {
                return (TransferAssetContract) this.contractOneof_;
            }
            return TransferAssetContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TransferContractOrBuilder getTransferOrBuilder() {
            if (this.contractOneofCase_ == 10) {
                return (TransferContract) this.contractOneof_;
            }
            return TransferContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TransferTRC20Contract getTransferTrc20Contract() {
            if (this.contractOneofCase_ == 19) {
                return (TransferTRC20Contract) this.contractOneof_;
            }
            return TransferTRC20Contract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TransferTRC20ContractOrBuilder getTransferTrc20ContractOrBuilder() {
            if (this.contractOneofCase_ == 19) {
                return (TransferTRC20Contract) this.contractOneof_;
            }
            return TransferTRC20Contract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TriggerSmartContract getTriggerSmartContract() {
            if (this.contractOneofCase_ == 18) {
                return (TriggerSmartContract) this.contractOneof_;
            }
            return TriggerSmartContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public TriggerSmartContractOrBuilder getTriggerSmartContractOrBuilder() {
            if (this.contractOneofCase_ == 18) {
                return (TriggerSmartContract) this.contractOneof_;
            }
            return TriggerSmartContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public UnfreezeAssetContract getUnfreezeAsset() {
            if (this.contractOneofCase_ == 14) {
                return (UnfreezeAssetContract) this.contractOneof_;
            }
            return UnfreezeAssetContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public UnfreezeAssetContractOrBuilder getUnfreezeAssetOrBuilder() {
            if (this.contractOneofCase_ == 14) {
                return (UnfreezeAssetContract) this.contractOneof_;
            }
            return UnfreezeAssetContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public UnfreezeBalanceContract getUnfreezeBalance() {
            if (this.contractOneofCase_ == 13) {
                return (UnfreezeBalanceContract) this.contractOneof_;
            }
            return UnfreezeBalanceContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public UnfreezeBalanceContractOrBuilder getUnfreezeBalanceOrBuilder() {
            if (this.contractOneofCase_ == 13) {
                return (UnfreezeBalanceContract) this.contractOneof_;
            }
            return UnfreezeBalanceContract.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public VoteAssetContract getVoteAsset() {
            if (this.contractOneofCase_ == 16) {
                return (VoteAssetContract) this.contractOneof_;
            }
            return VoteAssetContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public VoteAssetContractOrBuilder getVoteAssetOrBuilder() {
            if (this.contractOneofCase_ == 16) {
                return (VoteAssetContract) this.contractOneof_;
            }
            return VoteAssetContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public VoteWitnessContract getVoteWitness() {
            if (this.contractOneofCase_ == 17) {
                return (VoteWitnessContract) this.contractOneof_;
            }
            return VoteWitnessContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public VoteWitnessContractOrBuilder getVoteWitnessOrBuilder() {
            if (this.contractOneofCase_ == 17) {
                return (VoteWitnessContract) this.contractOneof_;
            }
            return VoteWitnessContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public WithdrawBalanceContract getWithdrawBalance() {
            if (this.contractOneofCase_ == 15) {
                return (WithdrawBalanceContract) this.contractOneof_;
            }
            return WithdrawBalanceContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public WithdrawBalanceContractOrBuilder getWithdrawBalanceOrBuilder() {
            if (this.contractOneofCase_ == 15) {
                return (WithdrawBalanceContract) this.contractOneof_;
            }
            return WithdrawBalanceContract.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasBlockHeader() {
            return this.blockHeader_ != null;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasFreezeBalance() {
            return this.contractOneofCase_ == 12;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasTransfer() {
            return this.contractOneofCase_ == 10;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasTransferAsset() {
            return this.contractOneofCase_ == 11;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasTransferTrc20Contract() {
            return this.contractOneofCase_ == 19;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasTriggerSmartContract() {
            return this.contractOneofCase_ == 18;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasUnfreezeAsset() {
            return this.contractOneofCase_ == 14;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasUnfreezeBalance() {
            return this.contractOneofCase_ == 13;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasVoteAsset() {
            return this.contractOneofCase_ == 16;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasVoteWitness() {
            return this.contractOneofCase_ == 17;
        }

        @Override // wallet.core.jni.proto.Tron.TransactionOrBuilder
        public boolean hasWithdrawBalance() {
            return this.contractOneofCase_ == 15;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getTimestamp())) * 37) + 2) * 53) + a0.h(getExpiration());
            if (hasBlockHeader()) {
                hashCode2 = (((hashCode2 * 37) + 3) * 53) + getBlockHeader().hashCode();
            }
            int h = (((hashCode2 * 37) + 4) * 53) + a0.h(getFeeLimit());
            switch (this.contractOneofCase_) {
                case 10:
                    i = ((h * 37) + 10) * 53;
                    hashCode = getTransfer().hashCode();
                    h = i + hashCode;
                    int hashCode3 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3;
                    return hashCode3;
                case 11:
                    i = ((h * 37) + 11) * 53;
                    hashCode = getTransferAsset().hashCode();
                    h = i + hashCode;
                    int hashCode32 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32;
                    return hashCode32;
                case 12:
                    i = ((h * 37) + 12) * 53;
                    hashCode = getFreezeBalance().hashCode();
                    h = i + hashCode;
                    int hashCode322 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322;
                    return hashCode322;
                case 13:
                    i = ((h * 37) + 13) * 53;
                    hashCode = getUnfreezeBalance().hashCode();
                    h = i + hashCode;
                    int hashCode3222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222;
                    return hashCode3222;
                case 14:
                    i = ((h * 37) + 14) * 53;
                    hashCode = getUnfreezeAsset().hashCode();
                    h = i + hashCode;
                    int hashCode32222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222;
                    return hashCode32222;
                case 15:
                    i = ((h * 37) + 15) * 53;
                    hashCode = getWithdrawBalance().hashCode();
                    h = i + hashCode;
                    int hashCode322222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222;
                    return hashCode322222;
                case 16:
                    i = ((h * 37) + 16) * 53;
                    hashCode = getVoteAsset().hashCode();
                    h = i + hashCode;
                    int hashCode3222222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222;
                    return hashCode3222222;
                case 17:
                    i = ((h * 37) + 17) * 53;
                    hashCode = getVoteWitness().hashCode();
                    h = i + hashCode;
                    int hashCode32222222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222222;
                    return hashCode32222222;
                case 18:
                    i = ((h * 37) + 18) * 53;
                    hashCode = getTriggerSmartContract().hashCode();
                    h = i + hashCode;
                    int hashCode322222222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222222;
                    return hashCode322222222;
                case 19:
                    i = ((h * 37) + 19) * 53;
                    hashCode = getTransferTrc20Contract().hashCode();
                    h = i + hashCode;
                    int hashCode3222222222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222222;
                    return hashCode3222222222;
                default:
                    int hashCode32222222222 = (h * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222222222;
                    return hashCode32222222222;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.timestamp_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            long j2 = this.expiration_;
            if (j2 != 0) {
                codedOutputStream.I0(2, j2);
            }
            if (this.blockHeader_ != null) {
                codedOutputStream.K0(3, getBlockHeader());
            }
            long j3 = this.feeLimit_;
            if (j3 != 0) {
                codedOutputStream.I0(4, j3);
            }
            if (this.contractOneofCase_ == 10) {
                codedOutputStream.K0(10, (TransferContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 11) {
                codedOutputStream.K0(11, (TransferAssetContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 12) {
                codedOutputStream.K0(12, (FreezeBalanceContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 13) {
                codedOutputStream.K0(13, (UnfreezeBalanceContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 14) {
                codedOutputStream.K0(14, (UnfreezeAssetContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 15) {
                codedOutputStream.K0(15, (WithdrawBalanceContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 16) {
                codedOutputStream.K0(16, (VoteAssetContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 17) {
                codedOutputStream.K0(17, (VoteWitnessContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 18) {
                codedOutputStream.K0(18, (TriggerSmartContract) this.contractOneof_);
            }
            if (this.contractOneofCase_ == 19) {
                codedOutputStream.K0(19, (TransferTRC20Contract) this.contractOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Transaction(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Transaction transaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transaction);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.contractOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Transaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Transaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Transaction() {
            this.contractOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transaction parseFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Transaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            switch (J) {
                                case 0:
                                    break;
                                case 8:
                                    this.timestamp_ = jVar.y();
                                    continue;
                                case 16:
                                    this.expiration_ = jVar.y();
                                    continue;
                                case 26:
                                    BlockHeader blockHeader = this.blockHeader_;
                                    BlockHeader.Builder builder = blockHeader != null ? blockHeader.toBuilder() : null;
                                    BlockHeader blockHeader2 = (BlockHeader) jVar.z(BlockHeader.parser(), rVar);
                                    this.blockHeader_ = blockHeader2;
                                    if (builder != null) {
                                        builder.mergeFrom(blockHeader2);
                                        this.blockHeader_ = builder.buildPartial();
                                    } else {
                                        continue;
                                    }
                                case 32:
                                    this.feeLimit_ = jVar.y();
                                    continue;
                                case 82:
                                    TransferContract.Builder builder2 = this.contractOneofCase_ == 10 ? ((TransferContract) this.contractOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(TransferContract.parser(), rVar);
                                    this.contractOneof_ = z2;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((TransferContract) z2);
                                        this.contractOneof_ = builder2.buildPartial();
                                    }
                                    this.contractOneofCase_ = 10;
                                    continue;
                                case 90:
                                    TransferAssetContract.Builder builder3 = this.contractOneofCase_ == 11 ? ((TransferAssetContract) this.contractOneof_).toBuilder() : null;
                                    m0 z3 = jVar.z(TransferAssetContract.parser(), rVar);
                                    this.contractOneof_ = z3;
                                    if (builder3 != null) {
                                        builder3.mergeFrom((TransferAssetContract) z3);
                                        this.contractOneof_ = builder3.buildPartial();
                                    }
                                    this.contractOneofCase_ = 11;
                                    continue;
                                case 98:
                                    FreezeBalanceContract.Builder builder4 = this.contractOneofCase_ == 12 ? ((FreezeBalanceContract) this.contractOneof_).toBuilder() : null;
                                    m0 z4 = jVar.z(FreezeBalanceContract.parser(), rVar);
                                    this.contractOneof_ = z4;
                                    if (builder4 != null) {
                                        builder4.mergeFrom((FreezeBalanceContract) z4);
                                        this.contractOneof_ = builder4.buildPartial();
                                    }
                                    this.contractOneofCase_ = 12;
                                    continue;
                                case 106:
                                    UnfreezeBalanceContract.Builder builder5 = this.contractOneofCase_ == 13 ? ((UnfreezeBalanceContract) this.contractOneof_).toBuilder() : null;
                                    m0 z5 = jVar.z(UnfreezeBalanceContract.parser(), rVar);
                                    this.contractOneof_ = z5;
                                    if (builder5 != null) {
                                        builder5.mergeFrom((UnfreezeBalanceContract) z5);
                                        this.contractOneof_ = builder5.buildPartial();
                                    }
                                    this.contractOneofCase_ = 13;
                                    continue;
                                case 114:
                                    UnfreezeAssetContract.Builder builder6 = this.contractOneofCase_ == 14 ? ((UnfreezeAssetContract) this.contractOneof_).toBuilder() : null;
                                    m0 z6 = jVar.z(UnfreezeAssetContract.parser(), rVar);
                                    this.contractOneof_ = z6;
                                    if (builder6 != null) {
                                        builder6.mergeFrom((UnfreezeAssetContract) z6);
                                        this.contractOneof_ = builder6.buildPartial();
                                    }
                                    this.contractOneofCase_ = 14;
                                    continue;
                                case 122:
                                    WithdrawBalanceContract.Builder builder7 = this.contractOneofCase_ == 15 ? ((WithdrawBalanceContract) this.contractOneof_).toBuilder() : null;
                                    m0 z7 = jVar.z(WithdrawBalanceContract.parser(), rVar);
                                    this.contractOneof_ = z7;
                                    if (builder7 != null) {
                                        builder7.mergeFrom((WithdrawBalanceContract) z7);
                                        this.contractOneof_ = builder7.buildPartial();
                                    }
                                    this.contractOneofCase_ = 15;
                                    continue;
                                case 130:
                                    VoteAssetContract.Builder builder8 = this.contractOneofCase_ == 16 ? ((VoteAssetContract) this.contractOneof_).toBuilder() : null;
                                    m0 z8 = jVar.z(VoteAssetContract.parser(), rVar);
                                    this.contractOneof_ = z8;
                                    if (builder8 != null) {
                                        builder8.mergeFrom((VoteAssetContract) z8);
                                        this.contractOneof_ = builder8.buildPartial();
                                    }
                                    this.contractOneofCase_ = 16;
                                    continue;
                                case 138:
                                    VoteWitnessContract.Builder builder9 = this.contractOneofCase_ == 17 ? ((VoteWitnessContract) this.contractOneof_).toBuilder() : null;
                                    m0 z9 = jVar.z(VoteWitnessContract.parser(), rVar);
                                    this.contractOneof_ = z9;
                                    if (builder9 != null) {
                                        builder9.mergeFrom((VoteWitnessContract) z9);
                                        this.contractOneof_ = builder9.buildPartial();
                                    }
                                    this.contractOneofCase_ = 17;
                                    continue;
                                case 146:
                                    TriggerSmartContract.Builder builder10 = this.contractOneofCase_ == 18 ? ((TriggerSmartContract) this.contractOneof_).toBuilder() : null;
                                    m0 z10 = jVar.z(TriggerSmartContract.parser(), rVar);
                                    this.contractOneof_ = z10;
                                    if (builder10 != null) {
                                        builder10.mergeFrom((TriggerSmartContract) z10);
                                        this.contractOneof_ = builder10.buildPartial();
                                    }
                                    this.contractOneofCase_ = 18;
                                    continue;
                                case 154:
                                    TransferTRC20Contract.Builder builder11 = this.contractOneofCase_ == 19 ? ((TransferTRC20Contract) this.contractOneof_).toBuilder() : null;
                                    m0 z11 = jVar.z(TransferTRC20Contract.parser(), rVar);
                                    this.contractOneof_ = z11;
                                    if (builder11 != null) {
                                        builder11.mergeFrom((TransferTRC20Contract) z11);
                                        this.contractOneof_ = builder11.buildPartial();
                                    }
                                    this.contractOneofCase_ = 19;
                                    continue;
                                default:
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(j jVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transaction parseFrom(j jVar, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        BlockHeader getBlockHeader();

        BlockHeaderOrBuilder getBlockHeaderOrBuilder();

        Transaction.ContractOneofCase getContractOneofCase();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        long getExpiration();

        long getFeeLimit();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        FreezeBalanceContract getFreezeBalance();

        FreezeBalanceContractOrBuilder getFreezeBalanceOrBuilder();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        long getTimestamp();

        TransferContract getTransfer();

        TransferAssetContract getTransferAsset();

        TransferAssetContractOrBuilder getTransferAssetOrBuilder();

        TransferContractOrBuilder getTransferOrBuilder();

        TransferTRC20Contract getTransferTrc20Contract();

        TransferTRC20ContractOrBuilder getTransferTrc20ContractOrBuilder();

        TriggerSmartContract getTriggerSmartContract();

        TriggerSmartContractOrBuilder getTriggerSmartContractOrBuilder();

        UnfreezeAssetContract getUnfreezeAsset();

        UnfreezeAssetContractOrBuilder getUnfreezeAssetOrBuilder();

        UnfreezeBalanceContract getUnfreezeBalance();

        UnfreezeBalanceContractOrBuilder getUnfreezeBalanceOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        VoteAssetContract getVoteAsset();

        VoteAssetContractOrBuilder getVoteAssetOrBuilder();

        VoteWitnessContract getVoteWitness();

        VoteWitnessContractOrBuilder getVoteWitnessOrBuilder();

        WithdrawBalanceContract getWithdrawBalance();

        WithdrawBalanceContractOrBuilder getWithdrawBalanceOrBuilder();

        boolean hasBlockHeader();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasFreezeBalance();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransfer();

        boolean hasTransferAsset();

        boolean hasTransferTrc20Contract();

        boolean hasTriggerSmartContract();

        boolean hasUnfreezeAsset();

        boolean hasUnfreezeBalance();

        boolean hasVoteAsset();

        boolean hasVoteWitness();

        boolean hasWithdrawBalance();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransferAssetContract extends GeneratedMessageV3 implements TransferAssetContractOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 4;
        public static final int ASSET_NAME_FIELD_NUMBER = 1;
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 2;
        public static final int TO_ADDRESS_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object assetName_;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private volatile Object toAddress_;
        private static final TransferAssetContract DEFAULT_INSTANCE = new TransferAssetContract();
        private static final t0<TransferAssetContract> PARSER = new c<TransferAssetContract>() { // from class: wallet.core.jni.proto.Tron.TransferAssetContract.1
            @Override // com.google.protobuf.t0
            public TransferAssetContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransferAssetContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferAssetContractOrBuilder {
            private long amount_;
            private Object assetName_;
            private Object ownerAddress_;
            private Object toAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_TransferAssetContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearAssetName() {
                this.assetName_ = TransferAssetContract.getDefaultInstance().getAssetName();
                onChanged();
                return this;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = TransferAssetContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransferAssetContract.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
            public String getAssetName() {
                Object obj = this.assetName_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.assetName_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
            public ByteString getAssetNameBytes() {
                Object obj = this.assetName_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.assetName_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_TransferAssetContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_TransferAssetContract_fieldAccessorTable.d(TransferAssetContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setAssetName(String str) {
                Objects.requireNonNull(str);
                this.assetName_ = str;
                onChanged();
                return this;
            }

            public Builder setAssetNameBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.assetName_ = byteString;
                onChanged();
                return this;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.assetName_ = "";
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferAssetContract build() {
                TransferAssetContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferAssetContract buildPartial() {
                TransferAssetContract transferAssetContract = new TransferAssetContract(this, (AnonymousClass1) null);
                transferAssetContract.assetName_ = this.assetName_;
                transferAssetContract.ownerAddress_ = this.ownerAddress_;
                transferAssetContract.toAddress_ = this.toAddress_;
                transferAssetContract.amount_ = this.amount_;
                onBuilt();
                return transferAssetContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransferAssetContract getDefaultInstanceForType() {
                return TransferAssetContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.assetName_ = "";
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                this.amount_ = 0L;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransferAssetContract) {
                    return mergeFrom((TransferAssetContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.assetName_ = "";
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransferAssetContract transferAssetContract) {
                if (transferAssetContract == TransferAssetContract.getDefaultInstance()) {
                    return this;
                }
                if (!transferAssetContract.getAssetName().isEmpty()) {
                    this.assetName_ = transferAssetContract.assetName_;
                    onChanged();
                }
                if (!transferAssetContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = transferAssetContract.ownerAddress_;
                    onChanged();
                }
                if (!transferAssetContract.getToAddress().isEmpty()) {
                    this.toAddress_ = transferAssetContract.toAddress_;
                    onChanged();
                }
                if (transferAssetContract.getAmount() != 0) {
                    setAmount(transferAssetContract.getAmount());
                }
                mergeUnknownFields(transferAssetContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.TransferAssetContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.TransferAssetContract.access$2500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$TransferAssetContract r3 = (wallet.core.jni.proto.Tron.TransferAssetContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$TransferAssetContract r4 = (wallet.core.jni.proto.Tron.TransferAssetContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.TransferAssetContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$TransferAssetContract$Builder");
            }
        }

        public /* synthetic */ TransferAssetContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransferAssetContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_TransferAssetContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransferAssetContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransferAssetContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransferAssetContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransferAssetContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransferAssetContract)) {
                return super.equals(obj);
            }
            TransferAssetContract transferAssetContract = (TransferAssetContract) obj;
            return getAssetName().equals(transferAssetContract.getAssetName()) && getOwnerAddress().equals(transferAssetContract.getOwnerAddress()) && getToAddress().equals(transferAssetContract.getToAddress()) && getAmount() == transferAssetContract.getAmount() && this.unknownFields.equals(transferAssetContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
        public String getAssetName() {
            Object obj = this.assetName_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.assetName_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
        public ByteString getAssetNameBytes() {
            Object obj = this.assetName_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.assetName_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransferAssetContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getAssetNameBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.assetName_);
            if (!getOwnerAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.ownerAddress_);
            }
            if (!getToAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.toAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(4, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferAssetContractOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAssetName().hashCode()) * 37) + 2) * 53) + getOwnerAddress().hashCode()) * 37) + 3) * 53) + getToAddress().hashCode()) * 37) + 4) * 53) + a0.h(getAmount())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_TransferAssetContract_fieldAccessorTable.d(TransferAssetContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransferAssetContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getAssetNameBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.assetName_);
            }
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.ownerAddress_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.toAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(4, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransferAssetContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransferAssetContract transferAssetContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transferAssetContract);
        }

        public static TransferAssetContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransferAssetContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransferAssetContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferAssetContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransferAssetContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransferAssetContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransferAssetContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransferAssetContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.assetName_ = "";
            this.ownerAddress_ = "";
            this.toAddress_ = "";
        }

        public static TransferAssetContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransferAssetContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransferAssetContract parseFrom(InputStream inputStream) throws IOException {
            return (TransferAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransferAssetContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TransferAssetContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.assetName_ = jVar.I();
                            } else if (J == 18) {
                                this.ownerAddress_ = jVar.I();
                            } else if (J == 26) {
                                this.toAddress_ = jVar.I();
                            } else if (J != 32) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.amount_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransferAssetContract parseFrom(j jVar) throws IOException {
            return (TransferAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransferAssetContract parseFrom(j jVar, r rVar) throws IOException {
            return (TransferAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransferAssetContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        String getAssetName();

        ByteString getAssetNameBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransferContract extends GeneratedMessageV3 implements TransferContractOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 3;
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        public static final int TO_ADDRESS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private long amount_;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private volatile Object toAddress_;
        private static final TransferContract DEFAULT_INSTANCE = new TransferContract();
        private static final t0<TransferContract> PARSER = new c<TransferContract>() { // from class: wallet.core.jni.proto.Tron.TransferContract.1
            @Override // com.google.protobuf.t0
            public TransferContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransferContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferContractOrBuilder {
            private long amount_;
            private Object ownerAddress_;
            private Object toAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_TransferContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = TransferContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransferContract.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_TransferContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_TransferContract_fieldAccessorTable.d(TransferContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferContract build() {
                TransferContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferContract buildPartial() {
                TransferContract transferContract = new TransferContract(this, (AnonymousClass1) null);
                transferContract.ownerAddress_ = this.ownerAddress_;
                transferContract.toAddress_ = this.toAddress_;
                transferContract.amount_ = this.amount_;
                onBuilt();
                return transferContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransferContract getDefaultInstanceForType() {
                return TransferContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                this.amount_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransferContract) {
                    return mergeFrom((TransferContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransferContract transferContract) {
                if (transferContract == TransferContract.getDefaultInstance()) {
                    return this;
                }
                if (!transferContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = transferContract.ownerAddress_;
                    onChanged();
                }
                if (!transferContract.getToAddress().isEmpty()) {
                    this.toAddress_ = transferContract.toAddress_;
                    onChanged();
                }
                if (transferContract.getAmount() != 0) {
                    setAmount(transferContract.getAmount());
                }
                mergeUnknownFields(transferContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.TransferContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.TransferContract.access$1000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$TransferContract r3 = (wallet.core.jni.proto.Tron.TransferContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$TransferContract r4 = (wallet.core.jni.proto.Tron.TransferContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.TransferContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$TransferContract$Builder");
            }
        }

        public /* synthetic */ TransferContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransferContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_TransferContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransferContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransferContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransferContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransferContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransferContract)) {
                return super.equals(obj);
            }
            TransferContract transferContract = (TransferContract) obj;
            return getOwnerAddress().equals(transferContract.getOwnerAddress()) && getToAddress().equals(transferContract.getToAddress()) && getAmount() == transferContract.getAmount() && this.unknownFields.equals(transferContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransferContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getOwnerAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.ownerAddress_);
            if (!getToAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.toAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(3, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferContractOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode()) * 37) + 2) * 53) + getToAddress().hashCode()) * 37) + 3) * 53) + a0.h(getAmount())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_TransferContract_fieldAccessorTable.d(TransferContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransferContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.toAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(3, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransferContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransferContract transferContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transferContract);
        }

        public static TransferContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransferContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransferContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransferContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransferContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransferContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransferContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
            this.toAddress_ = "";
        }

        public static TransferContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransferContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransferContract parseFrom(InputStream inputStream) throws IOException {
            return (TransferContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransferContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.ownerAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.toAddress_ = jVar.I();
                            } else if (J != 24) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.amount_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransferContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransferContract parseFrom(j jVar) throws IOException {
            return (TransferContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransferContract parseFrom(j jVar, r rVar) throws IOException {
            return (TransferContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransferContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransferTRC20Contract extends GeneratedMessageV3 implements TransferTRC20ContractOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 4;
        public static final int CONTRACT_ADDRESS_FIELD_NUMBER = 1;
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 2;
        public static final int TO_ADDRESS_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ByteString amount_;
        private volatile Object contractAddress_;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private volatile Object toAddress_;
        private static final TransferTRC20Contract DEFAULT_INSTANCE = new TransferTRC20Contract();
        private static final t0<TransferTRC20Contract> PARSER = new c<TransferTRC20Contract>() { // from class: wallet.core.jni.proto.Tron.TransferTRC20Contract.1
            @Override // com.google.protobuf.t0
            public TransferTRC20Contract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransferTRC20Contract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferTRC20ContractOrBuilder {
            private ByteString amount_;
            private Object contractAddress_;
            private Object ownerAddress_;
            private Object toAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_TransferTRC20Contract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = TransferTRC20Contract.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearContractAddress() {
                this.contractAddress_ = TransferTRC20Contract.getDefaultInstance().getContractAddress();
                onChanged();
                return this;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = TransferTRC20Contract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransferTRC20Contract.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
            public String getContractAddress() {
                Object obj = this.contractAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.contractAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
            public ByteString getContractAddressBytes() {
                Object obj = this.contractAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.contractAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_TransferTRC20Contract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_TransferTRC20Contract_fieldAccessorTable.d(TransferTRC20Contract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setContractAddress(String str) {
                Objects.requireNonNull(str);
                this.contractAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setContractAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.contractAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.contractAddress_ = "";
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferTRC20Contract build() {
                TransferTRC20Contract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransferTRC20Contract buildPartial() {
                TransferTRC20Contract transferTRC20Contract = new TransferTRC20Contract(this, (AnonymousClass1) null);
                transferTRC20Contract.contractAddress_ = this.contractAddress_;
                transferTRC20Contract.ownerAddress_ = this.ownerAddress_;
                transferTRC20Contract.toAddress_ = this.toAddress_;
                transferTRC20Contract.amount_ = this.amount_;
                onBuilt();
                return transferTRC20Contract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransferTRC20Contract getDefaultInstanceForType() {
                return TransferTRC20Contract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.contractAddress_ = "";
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransferTRC20Contract) {
                    return mergeFrom((TransferTRC20Contract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.contractAddress_ = "";
                this.ownerAddress_ = "";
                this.toAddress_ = "";
                this.amount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransferTRC20Contract transferTRC20Contract) {
                if (transferTRC20Contract == TransferTRC20Contract.getDefaultInstance()) {
                    return this;
                }
                if (!transferTRC20Contract.getContractAddress().isEmpty()) {
                    this.contractAddress_ = transferTRC20Contract.contractAddress_;
                    onChanged();
                }
                if (!transferTRC20Contract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = transferTRC20Contract.ownerAddress_;
                    onChanged();
                }
                if (!transferTRC20Contract.getToAddress().isEmpty()) {
                    this.toAddress_ = transferTRC20Contract.toAddress_;
                    onChanged();
                }
                if (transferTRC20Contract.getAmount() != ByteString.EMPTY) {
                    setAmount(transferTRC20Contract.getAmount());
                }
                mergeUnknownFields(transferTRC20Contract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.TransferTRC20Contract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.TransferTRC20Contract.access$4100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$TransferTRC20Contract r3 = (wallet.core.jni.proto.Tron.TransferTRC20Contract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$TransferTRC20Contract r4 = (wallet.core.jni.proto.Tron.TransferTRC20Contract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.TransferTRC20Contract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$TransferTRC20Contract$Builder");
            }
        }

        public /* synthetic */ TransferTRC20Contract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransferTRC20Contract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_TransferTRC20Contract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransferTRC20Contract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransferTRC20Contract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransferTRC20Contract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransferTRC20Contract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransferTRC20Contract)) {
                return super.equals(obj);
            }
            TransferTRC20Contract transferTRC20Contract = (TransferTRC20Contract) obj;
            return getContractAddress().equals(transferTRC20Contract.getContractAddress()) && getOwnerAddress().equals(transferTRC20Contract.getOwnerAddress()) && getToAddress().equals(transferTRC20Contract.getToAddress()) && getAmount().equals(transferTRC20Contract.getAmount()) && this.unknownFields.equals(transferTRC20Contract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
        public ByteString getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
        public String getContractAddress() {
            Object obj = this.contractAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.contractAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
        public ByteString getContractAddressBytes() {
            Object obj = this.contractAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.contractAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransferTRC20Contract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getContractAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.contractAddress_);
            if (!getOwnerAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.ownerAddress_);
            }
            if (!getToAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.toAddress_);
            }
            if (!this.amount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.amount_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TransferTRC20ContractOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getContractAddress().hashCode()) * 37) + 2) * 53) + getOwnerAddress().hashCode()) * 37) + 3) * 53) + getToAddress().hashCode()) * 37) + 4) * 53) + getAmount().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_TransferTRC20Contract_fieldAccessorTable.d(TransferTRC20Contract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransferTRC20Contract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getContractAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.contractAddress_);
            }
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.ownerAddress_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.toAddress_);
            }
            if (!this.amount_.isEmpty()) {
                codedOutputStream.q0(4, this.amount_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransferTRC20Contract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransferTRC20Contract transferTRC20Contract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transferTRC20Contract);
        }

        public static TransferTRC20Contract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransferTRC20Contract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransferTRC20Contract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferTRC20Contract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransferTRC20Contract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransferTRC20Contract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransferTRC20Contract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransferTRC20Contract() {
            this.memoizedIsInitialized = (byte) -1;
            this.contractAddress_ = "";
            this.ownerAddress_ = "";
            this.toAddress_ = "";
            this.amount_ = ByteString.EMPTY;
        }

        public static TransferTRC20Contract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransferTRC20Contract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransferTRC20Contract parseFrom(InputStream inputStream) throws IOException {
            return (TransferTRC20Contract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransferTRC20Contract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransferTRC20Contract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TransferTRC20Contract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.contractAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.ownerAddress_ = jVar.I();
                            } else if (J == 26) {
                                this.toAddress_ = jVar.I();
                            } else if (J != 34) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.amount_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransferTRC20Contract parseFrom(j jVar) throws IOException {
            return (TransferTRC20Contract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransferTRC20Contract parseFrom(j jVar, r rVar) throws IOException {
            return (TransferTRC20Contract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransferTRC20ContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAmount();

        String getContractAddress();

        ByteString getContractAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TriggerSmartContract extends GeneratedMessageV3 implements TriggerSmartContractOrBuilder {
        public static final int CALL_TOKEN_VALUE_FIELD_NUMBER = 5;
        public static final int CALL_VALUE_FIELD_NUMBER = 3;
        public static final int CONTRACT_ADDRESS_FIELD_NUMBER = 2;
        public static final int DATA_FIELD_NUMBER = 4;
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        public static final int TOKEN_ID_FIELD_NUMBER = 6;
        private static final long serialVersionUID = 0;
        private long callTokenValue_;
        private long callValue_;
        private volatile Object contractAddress_;
        private ByteString data_;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private long tokenId_;
        private static final TriggerSmartContract DEFAULT_INSTANCE = new TriggerSmartContract();
        private static final t0<TriggerSmartContract> PARSER = new c<TriggerSmartContract>() { // from class: wallet.core.jni.proto.Tron.TriggerSmartContract.1
            @Override // com.google.protobuf.t0
            public TriggerSmartContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TriggerSmartContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TriggerSmartContractOrBuilder {
            private long callTokenValue_;
            private long callValue_;
            private Object contractAddress_;
            private ByteString data_;
            private Object ownerAddress_;
            private long tokenId_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_TriggerSmartContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCallTokenValue() {
                this.callTokenValue_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearCallValue() {
                this.callValue_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearContractAddress() {
                this.contractAddress_ = TriggerSmartContract.getDefaultInstance().getContractAddress();
                onChanged();
                return this;
            }

            public Builder clearData() {
                this.data_ = TriggerSmartContract.getDefaultInstance().getData();
                onChanged();
                return this;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = TriggerSmartContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearTokenId() {
                this.tokenId_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public long getCallTokenValue() {
                return this.callTokenValue_;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public long getCallValue() {
                return this.callValue_;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public String getContractAddress() {
                Object obj = this.contractAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.contractAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public ByteString getContractAddressBytes() {
                Object obj = this.contractAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.contractAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_TriggerSmartContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
            public long getTokenId() {
                return this.tokenId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_TriggerSmartContract_fieldAccessorTable.d(TriggerSmartContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setCallTokenValue(long j) {
                this.callTokenValue_ = j;
                onChanged();
                return this;
            }

            public Builder setCallValue(long j) {
                this.callValue_ = j;
                onChanged();
                return this;
            }

            public Builder setContractAddress(String str) {
                Objects.requireNonNull(str);
                this.contractAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setContractAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.contractAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setData(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.data_ = byteString;
                onChanged();
                return this;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTokenId(long j) {
                this.tokenId_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.ownerAddress_ = "";
                this.contractAddress_ = "";
                this.data_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TriggerSmartContract build() {
                TriggerSmartContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TriggerSmartContract buildPartial() {
                TriggerSmartContract triggerSmartContract = new TriggerSmartContract(this, (AnonymousClass1) null);
                triggerSmartContract.ownerAddress_ = this.ownerAddress_;
                triggerSmartContract.contractAddress_ = this.contractAddress_;
                triggerSmartContract.callValue_ = this.callValue_;
                triggerSmartContract.data_ = this.data_;
                triggerSmartContract.callTokenValue_ = this.callTokenValue_;
                triggerSmartContract.tokenId_ = this.tokenId_;
                onBuilt();
                return triggerSmartContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TriggerSmartContract getDefaultInstanceForType() {
                return TriggerSmartContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                this.contractAddress_ = "";
                this.callValue_ = 0L;
                this.data_ = ByteString.EMPTY;
                this.callTokenValue_ = 0L;
                this.tokenId_ = 0L;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TriggerSmartContract) {
                    return mergeFrom((TriggerSmartContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                this.contractAddress_ = "";
                this.data_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TriggerSmartContract triggerSmartContract) {
                if (triggerSmartContract == TriggerSmartContract.getDefaultInstance()) {
                    return this;
                }
                if (!triggerSmartContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = triggerSmartContract.ownerAddress_;
                    onChanged();
                }
                if (!triggerSmartContract.getContractAddress().isEmpty()) {
                    this.contractAddress_ = triggerSmartContract.contractAddress_;
                    onChanged();
                }
                if (triggerSmartContract.getCallValue() != 0) {
                    setCallValue(triggerSmartContract.getCallValue());
                }
                if (triggerSmartContract.getData() != ByteString.EMPTY) {
                    setData(triggerSmartContract.getData());
                }
                if (triggerSmartContract.getCallTokenValue() != 0) {
                    setCallTokenValue(triggerSmartContract.getCallTokenValue());
                }
                if (triggerSmartContract.getTokenId() != 0) {
                    setTokenId(triggerSmartContract.getTokenId());
                }
                mergeUnknownFields(triggerSmartContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.TriggerSmartContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.TriggerSmartContract.access$15400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$TriggerSmartContract r3 = (wallet.core.jni.proto.Tron.TriggerSmartContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$TriggerSmartContract r4 = (wallet.core.jni.proto.Tron.TriggerSmartContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.TriggerSmartContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$TriggerSmartContract$Builder");
            }
        }

        public /* synthetic */ TriggerSmartContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TriggerSmartContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_TriggerSmartContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TriggerSmartContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TriggerSmartContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TriggerSmartContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TriggerSmartContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TriggerSmartContract)) {
                return super.equals(obj);
            }
            TriggerSmartContract triggerSmartContract = (TriggerSmartContract) obj;
            return getOwnerAddress().equals(triggerSmartContract.getOwnerAddress()) && getContractAddress().equals(triggerSmartContract.getContractAddress()) && getCallValue() == triggerSmartContract.getCallValue() && getData().equals(triggerSmartContract.getData()) && getCallTokenValue() == triggerSmartContract.getCallTokenValue() && getTokenId() == triggerSmartContract.getTokenId() && this.unknownFields.equals(triggerSmartContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public long getCallTokenValue() {
            return this.callTokenValue_;
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public long getCallValue() {
            return this.callValue_;
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public String getContractAddress() {
            Object obj = this.contractAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.contractAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public ByteString getContractAddressBytes() {
            Object obj = this.contractAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.contractAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public ByteString getData() {
            return this.data_;
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TriggerSmartContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getOwnerAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.ownerAddress_);
            if (!getContractAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.contractAddress_);
            }
            long j = this.callValue_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(3, j);
            }
            if (!this.data_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.data_);
            }
            long j2 = this.callTokenValue_;
            if (j2 != 0) {
                computeStringSize += CodedOutputStream.z(5, j2);
            }
            long j3 = this.tokenId_;
            if (j3 != 0) {
                computeStringSize += CodedOutputStream.z(6, j3);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.TriggerSmartContractOrBuilder
        public long getTokenId() {
            return this.tokenId_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode()) * 37) + 2) * 53) + getContractAddress().hashCode()) * 37) + 3) * 53) + a0.h(getCallValue())) * 37) + 4) * 53) + getData().hashCode()) * 37) + 5) * 53) + a0.h(getCallTokenValue())) * 37) + 6) * 53) + a0.h(getTokenId())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_TriggerSmartContract_fieldAccessorTable.d(TriggerSmartContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TriggerSmartContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            if (!getContractAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.contractAddress_);
            }
            long j = this.callValue_;
            if (j != 0) {
                codedOutputStream.I0(3, j);
            }
            if (!this.data_.isEmpty()) {
                codedOutputStream.q0(4, this.data_);
            }
            long j2 = this.callTokenValue_;
            if (j2 != 0) {
                codedOutputStream.I0(5, j2);
            }
            long j3 = this.tokenId_;
            if (j3 != 0) {
                codedOutputStream.I0(6, j3);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TriggerSmartContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TriggerSmartContract triggerSmartContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(triggerSmartContract);
        }

        public static TriggerSmartContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TriggerSmartContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TriggerSmartContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TriggerSmartContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TriggerSmartContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TriggerSmartContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TriggerSmartContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TriggerSmartContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
            this.contractAddress_ = "";
            this.data_ = ByteString.EMPTY;
        }

        public static TriggerSmartContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TriggerSmartContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TriggerSmartContract parseFrom(InputStream inputStream) throws IOException {
            return (TriggerSmartContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TriggerSmartContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TriggerSmartContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TriggerSmartContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.ownerAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.contractAddress_ = jVar.I();
                            } else if (J == 24) {
                                this.callValue_ = jVar.y();
                            } else if (J == 34) {
                                this.data_ = jVar.q();
                            } else if (J == 40) {
                                this.callTokenValue_ = jVar.y();
                            } else if (J != 48) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.tokenId_ = jVar.y();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TriggerSmartContract parseFrom(j jVar) throws IOException {
            return (TriggerSmartContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TriggerSmartContract parseFrom(j jVar, r rVar) throws IOException {
            return (TriggerSmartContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TriggerSmartContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getCallTokenValue();

        long getCallValue();

        String getContractAddress();

        ByteString getContractAddressBytes();

        ByteString getData();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        long getTokenId();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class UnfreezeAssetContract extends GeneratedMessageV3 implements UnfreezeAssetContractOrBuilder {
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private static final UnfreezeAssetContract DEFAULT_INSTANCE = new UnfreezeAssetContract();
        private static final t0<UnfreezeAssetContract> PARSER = new c<UnfreezeAssetContract>() { // from class: wallet.core.jni.proto.Tron.UnfreezeAssetContract.1
            @Override // com.google.protobuf.t0
            public UnfreezeAssetContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new UnfreezeAssetContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements UnfreezeAssetContractOrBuilder {
            private Object ownerAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_UnfreezeAssetContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = UnfreezeAssetContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_UnfreezeAssetContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeAssetContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeAssetContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_UnfreezeAssetContract_fieldAccessorTable.d(UnfreezeAssetContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.ownerAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public UnfreezeAssetContract build() {
                UnfreezeAssetContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public UnfreezeAssetContract buildPartial() {
                UnfreezeAssetContract unfreezeAssetContract = new UnfreezeAssetContract(this, (AnonymousClass1) null);
                unfreezeAssetContract.ownerAddress_ = this.ownerAddress_;
                onBuilt();
                return unfreezeAssetContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public UnfreezeAssetContract getDefaultInstanceForType() {
                return UnfreezeAssetContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof UnfreezeAssetContract) {
                    return mergeFrom((UnfreezeAssetContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(UnfreezeAssetContract unfreezeAssetContract) {
                if (unfreezeAssetContract == UnfreezeAssetContract.getDefaultInstance()) {
                    return this;
                }
                if (!unfreezeAssetContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = unfreezeAssetContract.ownerAddress_;
                    onChanged();
                }
                mergeUnknownFields(unfreezeAssetContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.UnfreezeAssetContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.UnfreezeAssetContract.access$8600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$UnfreezeAssetContract r3 = (wallet.core.jni.proto.Tron.UnfreezeAssetContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$UnfreezeAssetContract r4 = (wallet.core.jni.proto.Tron.UnfreezeAssetContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.UnfreezeAssetContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$UnfreezeAssetContract$Builder");
            }
        }

        public /* synthetic */ UnfreezeAssetContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static UnfreezeAssetContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_UnfreezeAssetContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static UnfreezeAssetContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (UnfreezeAssetContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static UnfreezeAssetContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<UnfreezeAssetContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof UnfreezeAssetContract)) {
                return super.equals(obj);
            }
            UnfreezeAssetContract unfreezeAssetContract = (UnfreezeAssetContract) obj;
            return getOwnerAddress().equals(unfreezeAssetContract.getOwnerAddress()) && this.unknownFields.equals(unfreezeAssetContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeAssetContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeAssetContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<UnfreezeAssetContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getOwnerAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.ownerAddress_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_UnfreezeAssetContract_fieldAccessorTable.d(UnfreezeAssetContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new UnfreezeAssetContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ UnfreezeAssetContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(UnfreezeAssetContract unfreezeAssetContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(unfreezeAssetContract);
        }

        public static UnfreezeAssetContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private UnfreezeAssetContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static UnfreezeAssetContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (UnfreezeAssetContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static UnfreezeAssetContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public UnfreezeAssetContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static UnfreezeAssetContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private UnfreezeAssetContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
        }

        public static UnfreezeAssetContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static UnfreezeAssetContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static UnfreezeAssetContract parseFrom(InputStream inputStream) throws IOException {
            return (UnfreezeAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private UnfreezeAssetContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.ownerAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static UnfreezeAssetContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (UnfreezeAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static UnfreezeAssetContract parseFrom(j jVar) throws IOException {
            return (UnfreezeAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static UnfreezeAssetContract parseFrom(j jVar, r rVar) throws IOException {
            return (UnfreezeAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface UnfreezeAssetContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class UnfreezeBalanceContract extends GeneratedMessageV3 implements UnfreezeBalanceContractOrBuilder {
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        public static final int RECEIVER_ADDRESS_FIELD_NUMBER = 15;
        public static final int RESOURCE_FIELD_NUMBER = 10;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private volatile Object receiverAddress_;
        private volatile Object resource_;
        private static final UnfreezeBalanceContract DEFAULT_INSTANCE = new UnfreezeBalanceContract();
        private static final t0<UnfreezeBalanceContract> PARSER = new c<UnfreezeBalanceContract>() { // from class: wallet.core.jni.proto.Tron.UnfreezeBalanceContract.1
            @Override // com.google.protobuf.t0
            public UnfreezeBalanceContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new UnfreezeBalanceContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements UnfreezeBalanceContractOrBuilder {
            private Object ownerAddress_;
            private Object receiverAddress_;
            private Object resource_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_UnfreezeBalanceContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = UnfreezeBalanceContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearReceiverAddress() {
                this.receiverAddress_ = UnfreezeBalanceContract.getDefaultInstance().getReceiverAddress();
                onChanged();
                return this;
            }

            public Builder clearResource() {
                this.resource_ = UnfreezeBalanceContract.getDefaultInstance().getResource();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_UnfreezeBalanceContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
            public String getReceiverAddress() {
                Object obj = this.receiverAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.receiverAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
            public ByteString getReceiverAddressBytes() {
                Object obj = this.receiverAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.receiverAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
            public String getResource() {
                Object obj = this.resource_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.resource_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
            public ByteString getResourceBytes() {
                Object obj = this.resource_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.resource_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_UnfreezeBalanceContract_fieldAccessorTable.d(UnfreezeBalanceContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setReceiverAddress(String str) {
                Objects.requireNonNull(str);
                this.receiverAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setReceiverAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.receiverAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setResource(String str) {
                Objects.requireNonNull(str);
                this.resource_ = str;
                onChanged();
                return this;
            }

            public Builder setResourceBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.resource_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.ownerAddress_ = "";
                this.resource_ = "";
                this.receiverAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public UnfreezeBalanceContract build() {
                UnfreezeBalanceContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public UnfreezeBalanceContract buildPartial() {
                UnfreezeBalanceContract unfreezeBalanceContract = new UnfreezeBalanceContract(this, (AnonymousClass1) null);
                unfreezeBalanceContract.ownerAddress_ = this.ownerAddress_;
                unfreezeBalanceContract.resource_ = this.resource_;
                unfreezeBalanceContract.receiverAddress_ = this.receiverAddress_;
                onBuilt();
                return unfreezeBalanceContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public UnfreezeBalanceContract getDefaultInstanceForType() {
                return UnfreezeBalanceContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                this.resource_ = "";
                this.receiverAddress_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof UnfreezeBalanceContract) {
                    return mergeFrom((UnfreezeBalanceContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                this.resource_ = "";
                this.receiverAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(UnfreezeBalanceContract unfreezeBalanceContract) {
                if (unfreezeBalanceContract == UnfreezeBalanceContract.getDefaultInstance()) {
                    return this;
                }
                if (!unfreezeBalanceContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = unfreezeBalanceContract.ownerAddress_;
                    onChanged();
                }
                if (!unfreezeBalanceContract.getResource().isEmpty()) {
                    this.resource_ = unfreezeBalanceContract.resource_;
                    onChanged();
                }
                if (!unfreezeBalanceContract.getReceiverAddress().isEmpty()) {
                    this.receiverAddress_ = unfreezeBalanceContract.receiverAddress_;
                    onChanged();
                }
                mergeUnknownFields(unfreezeBalanceContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.UnfreezeBalanceContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.UnfreezeBalanceContract.access$7300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$UnfreezeBalanceContract r3 = (wallet.core.jni.proto.Tron.UnfreezeBalanceContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$UnfreezeBalanceContract r4 = (wallet.core.jni.proto.Tron.UnfreezeBalanceContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.UnfreezeBalanceContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$UnfreezeBalanceContract$Builder");
            }
        }

        public /* synthetic */ UnfreezeBalanceContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static UnfreezeBalanceContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_UnfreezeBalanceContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static UnfreezeBalanceContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (UnfreezeBalanceContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static UnfreezeBalanceContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<UnfreezeBalanceContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof UnfreezeBalanceContract)) {
                return super.equals(obj);
            }
            UnfreezeBalanceContract unfreezeBalanceContract = (UnfreezeBalanceContract) obj;
            return getOwnerAddress().equals(unfreezeBalanceContract.getOwnerAddress()) && getResource().equals(unfreezeBalanceContract.getResource()) && getReceiverAddress().equals(unfreezeBalanceContract.getReceiverAddress()) && this.unknownFields.equals(unfreezeBalanceContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<UnfreezeBalanceContract> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
        public String getReceiverAddress() {
            Object obj = this.receiverAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.receiverAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
        public ByteString getReceiverAddressBytes() {
            Object obj = this.receiverAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.receiverAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
        public String getResource() {
            Object obj = this.resource_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.resource_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.UnfreezeBalanceContractOrBuilder
        public ByteString getResourceBytes() {
            Object obj = this.resource_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.resource_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getOwnerAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.ownerAddress_);
            if (!getResourceBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(10, this.resource_);
            }
            if (!getReceiverAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(15, this.receiverAddress_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode()) * 37) + 10) * 53) + getResource().hashCode()) * 37) + 15) * 53) + getReceiverAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_UnfreezeBalanceContract_fieldAccessorTable.d(UnfreezeBalanceContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new UnfreezeBalanceContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            if (!getResourceBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 10, this.resource_);
            }
            if (!getReceiverAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 15, this.receiverAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ UnfreezeBalanceContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(UnfreezeBalanceContract unfreezeBalanceContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(unfreezeBalanceContract);
        }

        public static UnfreezeBalanceContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private UnfreezeBalanceContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static UnfreezeBalanceContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (UnfreezeBalanceContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static UnfreezeBalanceContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public UnfreezeBalanceContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static UnfreezeBalanceContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private UnfreezeBalanceContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
            this.resource_ = "";
            this.receiverAddress_ = "";
        }

        public static UnfreezeBalanceContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static UnfreezeBalanceContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static UnfreezeBalanceContract parseFrom(InputStream inputStream) throws IOException {
            return (UnfreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static UnfreezeBalanceContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (UnfreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private UnfreezeBalanceContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.ownerAddress_ = jVar.I();
                            } else if (J == 82) {
                                this.resource_ = jVar.I();
                            } else if (J != 122) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.receiverAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static UnfreezeBalanceContract parseFrom(j jVar) throws IOException {
            return (UnfreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static UnfreezeBalanceContract parseFrom(j jVar, r rVar) throws IOException {
            return (UnfreezeBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface UnfreezeBalanceContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        String getReceiverAddress();

        ByteString getReceiverAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getResource();

        ByteString getResourceBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class VoteAssetContract extends GeneratedMessageV3 implements VoteAssetContractOrBuilder {
        public static final int COUNT_FIELD_NUMBER = 5;
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        public static final int SUPPORT_FIELD_NUMBER = 3;
        public static final int VOTE_ADDRESS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private int count_;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private boolean support_;
        private cz1 voteAddress_;
        private static final VoteAssetContract DEFAULT_INSTANCE = new VoteAssetContract();
        private static final t0<VoteAssetContract> PARSER = new c<VoteAssetContract>() { // from class: wallet.core.jni.proto.Tron.VoteAssetContract.1
            @Override // com.google.protobuf.t0
            public VoteAssetContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new VoteAssetContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements VoteAssetContractOrBuilder {
            private int bitField0_;
            private int count_;
            private Object ownerAddress_;
            private boolean support_;
            private cz1 voteAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private void ensureVoteAddressIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.voteAddress_ = new d0(this.voteAddress_);
                    this.bitField0_ |= 1;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_VoteAssetContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder addAllVoteAddress(Iterable<String> iterable) {
                ensureVoteAddressIsMutable();
                b.a.addAll((Iterable) iterable, (List) this.voteAddress_);
                onChanged();
                return this;
            }

            public Builder addVoteAddress(String str) {
                Objects.requireNonNull(str);
                ensureVoteAddressIsMutable();
                this.voteAddress_.add(str);
                onChanged();
                return this;
            }

            public Builder addVoteAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                ensureVoteAddressIsMutable();
                this.voteAddress_.W(byteString);
                onChanged();
                return this;
            }

            public Builder clearCount() {
                this.count_ = 0;
                onChanged();
                return this;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = VoteAssetContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearSupport() {
                this.support_ = false;
                onChanged();
                return this;
            }

            public Builder clearVoteAddress() {
                this.voteAddress_ = d0.h0;
                this.bitField0_ &= -2;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public int getCount() {
                return this.count_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_VoteAssetContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public boolean getSupport() {
                return this.support_;
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public String getVoteAddress(int i) {
                return this.voteAddress_.get(i);
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public ByteString getVoteAddressBytes(int i) {
                return this.voteAddress_.e1(i);
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public int getVoteAddressCount() {
                return this.voteAddress_.size();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_VoteAssetContract_fieldAccessorTable.d(VoteAssetContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setCount(int i) {
                this.count_ = i;
                onChanged();
                return this;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSupport(boolean z) {
                this.support_ = z;
                onChanged();
                return this;
            }

            public Builder setVoteAddress(int i, String str) {
                Objects.requireNonNull(str);
                ensureVoteAddressIsMutable();
                this.voteAddress_.set(i, str);
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
            public dw2 getVoteAddressList() {
                return this.voteAddress_.x();
            }

            private Builder() {
                this.ownerAddress_ = "";
                this.voteAddress_ = d0.h0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public VoteAssetContract build() {
                VoteAssetContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public VoteAssetContract buildPartial() {
                VoteAssetContract voteAssetContract = new VoteAssetContract(this, (AnonymousClass1) null);
                voteAssetContract.ownerAddress_ = this.ownerAddress_;
                if ((this.bitField0_ & 1) != 0) {
                    this.voteAddress_ = this.voteAddress_.x();
                    this.bitField0_ &= -2;
                }
                voteAssetContract.voteAddress_ = this.voteAddress_;
                voteAssetContract.support_ = this.support_;
                voteAssetContract.count_ = this.count_;
                onBuilt();
                return voteAssetContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public VoteAssetContract getDefaultInstanceForType() {
                return VoteAssetContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                this.voteAddress_ = d0.h0;
                this.bitField0_ &= -2;
                this.support_ = false;
                this.count_ = 0;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                this.voteAddress_ = d0.h0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof VoteAssetContract) {
                    return mergeFrom((VoteAssetContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(VoteAssetContract voteAssetContract) {
                if (voteAssetContract == VoteAssetContract.getDefaultInstance()) {
                    return this;
                }
                if (!voteAssetContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = voteAssetContract.ownerAddress_;
                    onChanged();
                }
                if (!voteAssetContract.voteAddress_.isEmpty()) {
                    if (this.voteAddress_.isEmpty()) {
                        this.voteAddress_ = voteAssetContract.voteAddress_;
                        this.bitField0_ &= -2;
                    } else {
                        ensureVoteAddressIsMutable();
                        this.voteAddress_.addAll(voteAssetContract.voteAddress_);
                    }
                    onChanged();
                }
                if (voteAssetContract.getSupport()) {
                    setSupport(voteAssetContract.getSupport());
                }
                if (voteAssetContract.getCount() != 0) {
                    setCount(voteAssetContract.getCount());
                }
                mergeUnknownFields(voteAssetContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.VoteAssetContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.VoteAssetContract.access$10000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$VoteAssetContract r3 = (wallet.core.jni.proto.Tron.VoteAssetContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$VoteAssetContract r4 = (wallet.core.jni.proto.Tron.VoteAssetContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.VoteAssetContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$VoteAssetContract$Builder");
            }
        }

        public /* synthetic */ VoteAssetContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static VoteAssetContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_VoteAssetContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static VoteAssetContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (VoteAssetContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static VoteAssetContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<VoteAssetContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof VoteAssetContract)) {
                return super.equals(obj);
            }
            VoteAssetContract voteAssetContract = (VoteAssetContract) obj;
            return getOwnerAddress().equals(voteAssetContract.getOwnerAddress()) && getVoteAddressList().equals(voteAssetContract.getVoteAddressList()) && getSupport() == voteAssetContract.getSupport() && getCount() == voteAssetContract.getCount() && this.unknownFields.equals(voteAssetContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public int getCount() {
            return this.count_;
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<VoteAssetContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = !getOwnerAddressBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.ownerAddress_) + 0 : 0;
            int i2 = 0;
            for (int i3 = 0; i3 < this.voteAddress_.size(); i3++) {
                i2 += GeneratedMessageV3.computeStringSizeNoTag(this.voteAddress_.j(i3));
            }
            int size = computeStringSize + i2 + (getVoteAddressList().size() * 1);
            boolean z = this.support_;
            if (z) {
                size += CodedOutputStream.e(3, z);
            }
            int i4 = this.count_;
            if (i4 != 0) {
                size += CodedOutputStream.x(5, i4);
            }
            int serializedSize = size + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public boolean getSupport() {
            return this.support_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public String getVoteAddress(int i) {
            return this.voteAddress_.get(i);
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public ByteString getVoteAddressBytes(int i) {
            return this.voteAddress_.e1(i);
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public int getVoteAddressCount() {
            return this.voteAddress_.size();
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode();
            if (getVoteAddressCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getVoteAddressList().hashCode();
            }
            int c = (((((((((hashCode * 37) + 3) * 53) + a0.c(getSupport())) * 37) + 5) * 53) + getCount()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = c;
            return c;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_VoteAssetContract_fieldAccessorTable.d(VoteAssetContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new VoteAssetContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            for (int i = 0; i < this.voteAddress_.size(); i++) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.voteAddress_.j(i));
            }
            boolean z = this.support_;
            if (z) {
                codedOutputStream.m0(3, z);
            }
            int i2 = this.count_;
            if (i2 != 0) {
                codedOutputStream.G0(5, i2);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ VoteAssetContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(VoteAssetContract voteAssetContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(voteAssetContract);
        }

        public static VoteAssetContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        @Override // wallet.core.jni.proto.Tron.VoteAssetContractOrBuilder
        public dw2 getVoteAddressList() {
            return this.voteAddress_;
        }

        private VoteAssetContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static VoteAssetContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (VoteAssetContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static VoteAssetContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public VoteAssetContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static VoteAssetContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private VoteAssetContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
            this.voteAddress_ = d0.h0;
        }

        public static VoteAssetContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static VoteAssetContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static VoteAssetContract parseFrom(InputStream inputStream) throws IOException {
            return (VoteAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private VoteAssetContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.ownerAddress_ = jVar.I();
                            } else if (J == 18) {
                                String I = jVar.I();
                                if (!(z2 & true)) {
                                    this.voteAddress_ = new d0();
                                    z2 |= true;
                                }
                                this.voteAddress_.add(I);
                            } else if (J == 24) {
                                this.support_ = jVar.p();
                            } else if (J != 40) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.count_ = jVar.x();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.voteAddress_ = this.voteAddress_.x();
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static VoteAssetContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (VoteAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static VoteAssetContract parseFrom(j jVar) throws IOException {
            return (VoteAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static VoteAssetContract parseFrom(j jVar, r rVar) throws IOException {
            return (VoteAssetContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface VoteAssetContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        int getCount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        boolean getSupport();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getVoteAddress(int i);

        ByteString getVoteAddressBytes(int i);

        int getVoteAddressCount();

        List<String> getVoteAddressList();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class VoteWitnessContract extends GeneratedMessageV3 implements VoteWitnessContractOrBuilder {
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        public static final int SUPPORT_FIELD_NUMBER = 3;
        public static final int VOTES_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private boolean support_;
        private List<Vote> votes_;
        private static final VoteWitnessContract DEFAULT_INSTANCE = new VoteWitnessContract();
        private static final t0<VoteWitnessContract> PARSER = new c<VoteWitnessContract>() { // from class: wallet.core.jni.proto.Tron.VoteWitnessContract.1
            @Override // com.google.protobuf.t0
            public VoteWitnessContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new VoteWitnessContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements VoteWitnessContractOrBuilder {
            private int bitField0_;
            private Object ownerAddress_;
            private boolean support_;
            private x0<Vote, Vote.Builder, VoteOrBuilder> votesBuilder_;
            private List<Vote> votes_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private void ensureVotesIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.votes_ = new ArrayList(this.votes_);
                    this.bitField0_ |= 1;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_descriptor;
            }

            private x0<Vote, Vote.Builder, VoteOrBuilder> getVotesFieldBuilder() {
                if (this.votesBuilder_ == null) {
                    this.votesBuilder_ = new x0<>(this.votes_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.votes_ = null;
                }
                return this.votesBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getVotesFieldBuilder();
                }
            }

            public Builder addAllVotes(Iterable<? extends Vote> iterable) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    ensureVotesIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.votes_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addVotes(Vote vote) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(vote);
                    ensureVotesIsMutable();
                    this.votes_.add(vote);
                    onChanged();
                } else {
                    x0Var.f(vote);
                }
                return this;
            }

            public Vote.Builder addVotesBuilder() {
                return getVotesFieldBuilder().d(Vote.getDefaultInstance());
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = VoteWitnessContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            public Builder clearSupport() {
                this.support_ = false;
                onChanged();
                return this;
            }

            public Builder clearVotes() {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    this.votes_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public boolean getSupport() {
                return this.support_;
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public Vote getVotes(int i) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    return this.votes_.get(i);
                }
                return x0Var.o(i);
            }

            public Vote.Builder getVotesBuilder(int i) {
                return getVotesFieldBuilder().l(i);
            }

            public List<Vote.Builder> getVotesBuilderList() {
                return getVotesFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public int getVotesCount() {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    return this.votes_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public List<Vote> getVotesList() {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.votes_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public VoteOrBuilder getVotesOrBuilder(int i) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    return this.votes_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
            public List<? extends VoteOrBuilder> getVotesOrBuilderList() {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.votes_);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_fieldAccessorTable.d(VoteWitnessContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeVotes(int i) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    ensureVotesIsMutable();
                    this.votes_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSupport(boolean z) {
                this.support_ = z;
                onChanged();
                return this;
            }

            public Builder setVotes(int i, Vote vote) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(vote);
                    ensureVotesIsMutable();
                    this.votes_.set(i, vote);
                    onChanged();
                } else {
                    x0Var.x(i, vote);
                }
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.ownerAddress_ = "";
                this.votes_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public VoteWitnessContract build() {
                VoteWitnessContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public VoteWitnessContract buildPartial() {
                VoteWitnessContract voteWitnessContract = new VoteWitnessContract(this, (AnonymousClass1) null);
                voteWitnessContract.ownerAddress_ = this.ownerAddress_;
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var != null) {
                    voteWitnessContract.votes_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.votes_ = Collections.unmodifiableList(this.votes_);
                        this.bitField0_ &= -2;
                    }
                    voteWitnessContract.votes_ = this.votes_;
                }
                voteWitnessContract.support_ = this.support_;
                onBuilt();
                return voteWitnessContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public VoteWitnessContract getDefaultInstanceForType() {
                return VoteWitnessContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public Vote.Builder addVotesBuilder(int i) {
                return getVotesFieldBuilder().c(i, Vote.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    this.votes_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                this.support_ = false;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                this.votes_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public Builder addVotes(int i, Vote vote) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(vote);
                    ensureVotesIsMutable();
                    this.votes_.add(i, vote);
                    onChanged();
                } else {
                    x0Var.e(i, vote);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof VoteWitnessContract) {
                    return mergeFrom((VoteWitnessContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setVotes(int i, Vote.Builder builder) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    ensureVotesIsMutable();
                    this.votes_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder mergeFrom(VoteWitnessContract voteWitnessContract) {
                if (voteWitnessContract == VoteWitnessContract.getDefaultInstance()) {
                    return this;
                }
                if (!voteWitnessContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = voteWitnessContract.ownerAddress_;
                    onChanged();
                }
                if (this.votesBuilder_ == null) {
                    if (!voteWitnessContract.votes_.isEmpty()) {
                        if (this.votes_.isEmpty()) {
                            this.votes_ = voteWitnessContract.votes_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureVotesIsMutable();
                            this.votes_.addAll(voteWitnessContract.votes_);
                        }
                        onChanged();
                    }
                } else if (!voteWitnessContract.votes_.isEmpty()) {
                    if (!this.votesBuilder_.u()) {
                        this.votesBuilder_.b(voteWitnessContract.votes_);
                    } else {
                        this.votesBuilder_.i();
                        this.votesBuilder_ = null;
                        this.votes_ = voteWitnessContract.votes_;
                        this.bitField0_ &= -2;
                        this.votesBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getVotesFieldBuilder() : null;
                    }
                }
                if (voteWitnessContract.getSupport()) {
                    setSupport(voteWitnessContract.getSupport());
                }
                mergeUnknownFields(voteWitnessContract.unknownFields);
                onChanged();
                return this;
            }

            public Builder addVotes(Vote.Builder builder) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    ensureVotesIsMutable();
                    this.votes_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addVotes(int i, Vote.Builder builder) {
                x0<Vote, Vote.Builder, VoteOrBuilder> x0Var = this.votesBuilder_;
                if (x0Var == null) {
                    ensureVotesIsMutable();
                    this.votes_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.VoteWitnessContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.VoteWitnessContract.access$12700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$VoteWitnessContract r3 = (wallet.core.jni.proto.Tron.VoteWitnessContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$VoteWitnessContract r4 = (wallet.core.jni.proto.Tron.VoteWitnessContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.VoteWitnessContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$VoteWitnessContract$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public static final class Vote extends GeneratedMessageV3 implements VoteOrBuilder {
            private static final Vote DEFAULT_INSTANCE = new Vote();
            private static final t0<Vote> PARSER = new c<Vote>() { // from class: wallet.core.jni.proto.Tron.VoteWitnessContract.Vote.1
                @Override // com.google.protobuf.t0
                public Vote parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Vote(jVar, rVar, null);
                }
            };
            public static final int VOTE_ADDRESS_FIELD_NUMBER = 1;
            public static final int VOTE_COUNT_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private volatile Object voteAddress_;
            private long voteCount_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements VoteOrBuilder {
                private Object voteAddress_;
                private long voteCount_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearVoteAddress() {
                    this.voteAddress_ = Vote.getDefaultInstance().getVoteAddress();
                    onChanged();
                    return this;
                }

                public Builder clearVoteCount() {
                    this.voteCount_ = 0L;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_descriptor;
                }

                @Override // wallet.core.jni.proto.Tron.VoteWitnessContract.VoteOrBuilder
                public String getVoteAddress() {
                    Object obj = this.voteAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.voteAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Tron.VoteWitnessContract.VoteOrBuilder
                public ByteString getVoteAddressBytes() {
                    Object obj = this.voteAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.voteAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Tron.VoteWitnessContract.VoteOrBuilder
                public long getVoteCount() {
                    return this.voteCount_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_fieldAccessorTable.d(Vote.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setVoteAddress(String str) {
                    Objects.requireNonNull(str);
                    this.voteAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setVoteAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.voteAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setVoteCount(long j) {
                    this.voteCount_ = j;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.voteAddress_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Vote build() {
                    Vote buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Vote buildPartial() {
                    Vote vote = new Vote(this, (AnonymousClass1) null);
                    vote.voteAddress_ = this.voteAddress_;
                    vote.voteCount_ = this.voteCount_;
                    onBuilt();
                    return vote;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Vote getDefaultInstanceForType() {
                    return Vote.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.voteAddress_ = "";
                    this.voteCount_ = 0L;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.voteAddress_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Vote) {
                        return mergeFrom((Vote) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Vote vote) {
                    if (vote == Vote.getDefaultInstance()) {
                        return this;
                    }
                    if (!vote.getVoteAddress().isEmpty()) {
                        this.voteAddress_ = vote.voteAddress_;
                        onChanged();
                    }
                    if (vote.getVoteCount() != 0) {
                        setVoteCount(vote.getVoteCount());
                    }
                    mergeUnknownFields(vote.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Tron.VoteWitnessContract.Vote.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.VoteWitnessContract.Vote.access$11500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Tron$VoteWitnessContract$Vote r3 = (wallet.core.jni.proto.Tron.VoteWitnessContract.Vote) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Tron$VoteWitnessContract$Vote r4 = (wallet.core.jni.proto.Tron.VoteWitnessContract.Vote) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.VoteWitnessContract.Vote.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$VoteWitnessContract$Vote$Builder");
                }
            }

            public /* synthetic */ Vote(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Vote getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Vote parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Vote) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Vote parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Vote> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Vote)) {
                    return super.equals(obj);
                }
                Vote vote = (Vote) obj;
                return getVoteAddress().equals(vote.getVoteAddress()) && getVoteCount() == vote.getVoteCount() && this.unknownFields.equals(vote.unknownFields);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Vote> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getVoteAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.voteAddress_);
                long j = this.voteCount_;
                if (j != 0) {
                    computeStringSize += CodedOutputStream.z(2, j);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContract.VoteOrBuilder
            public String getVoteAddress() {
                Object obj = this.voteAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.voteAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContract.VoteOrBuilder
            public ByteString getVoteAddressBytes() {
                Object obj = this.voteAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.voteAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tron.VoteWitnessContract.VoteOrBuilder
            public long getVoteCount() {
                return this.voteCount_;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getVoteAddress().hashCode()) * 37) + 2) * 53) + a0.h(getVoteCount())) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_fieldAccessorTable.d(Vote.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Vote();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getVoteAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.voteAddress_);
                }
                long j = this.voteCount_;
                if (j != 0) {
                    codedOutputStream.I0(2, j);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Vote(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Vote vote) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(vote);
            }

            public static Vote parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Vote(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Vote parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Vote) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Vote parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Vote getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Vote parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Vote() {
                this.memoizedIsInitialized = (byte) -1;
                this.voteAddress_ = "";
            }

            public static Vote parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Vote parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Vote parseFrom(InputStream inputStream) throws IOException {
                return (Vote) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Vote(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.voteAddress_ = jVar.I();
                                    } else if (J != 16) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.voteCount_ = jVar.y();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Vote parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Vote) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Vote parseFrom(j jVar) throws IOException {
                return (Vote) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Vote parseFrom(j jVar, r rVar) throws IOException {
                return (Vote) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface VoteOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            String getVoteAddress();

            ByteString getVoteAddressBytes();

            long getVoteCount();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ VoteWitnessContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static VoteWitnessContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static VoteWitnessContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (VoteWitnessContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static VoteWitnessContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<VoteWitnessContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof VoteWitnessContract)) {
                return super.equals(obj);
            }
            VoteWitnessContract voteWitnessContract = (VoteWitnessContract) obj;
            return getOwnerAddress().equals(voteWitnessContract.getOwnerAddress()) && getVotesList().equals(voteWitnessContract.getVotesList()) && getSupport() == voteWitnessContract.getSupport() && this.unknownFields.equals(voteWitnessContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<VoteWitnessContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = !getOwnerAddressBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.ownerAddress_) + 0 : 0;
            for (int i2 = 0; i2 < this.votes_.size(); i2++) {
                computeStringSize += CodedOutputStream.G(2, this.votes_.get(i2));
            }
            boolean z = this.support_;
            if (z) {
                computeStringSize += CodedOutputStream.e(3, z);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public boolean getSupport() {
            return this.support_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public Vote getVotes(int i) {
            return this.votes_.get(i);
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public int getVotesCount() {
            return this.votes_.size();
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public List<Vote> getVotesList() {
            return this.votes_;
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public VoteOrBuilder getVotesOrBuilder(int i) {
            return this.votes_.get(i);
        }

        @Override // wallet.core.jni.proto.Tron.VoteWitnessContractOrBuilder
        public List<? extends VoteOrBuilder> getVotesOrBuilderList() {
            return this.votes_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode();
            if (getVotesCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getVotesList().hashCode();
            }
            int c = (((((hashCode * 37) + 3) * 53) + a0.c(getSupport())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = c;
            return c;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_VoteWitnessContract_fieldAccessorTable.d(VoteWitnessContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new VoteWitnessContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            for (int i = 0; i < this.votes_.size(); i++) {
                codedOutputStream.K0(2, this.votes_.get(i));
            }
            boolean z = this.support_;
            if (z) {
                codedOutputStream.m0(3, z);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ VoteWitnessContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(VoteWitnessContract voteWitnessContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(voteWitnessContract);
        }

        public static VoteWitnessContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private VoteWitnessContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static VoteWitnessContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (VoteWitnessContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static VoteWitnessContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public VoteWitnessContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static VoteWitnessContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private VoteWitnessContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
            this.votes_ = Collections.emptyList();
        }

        public static VoteWitnessContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static VoteWitnessContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static VoteWitnessContract parseFrom(InputStream inputStream) throws IOException {
            return (VoteWitnessContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private VoteWitnessContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.ownerAddress_ = jVar.I();
                                } else if (J == 18) {
                                    if (!(z2 & true)) {
                                        this.votes_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.votes_.add(jVar.z(Vote.parser(), rVar));
                                } else if (J != 24) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.support_ = jVar.p();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.votes_ = Collections.unmodifiableList(this.votes_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static VoteWitnessContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (VoteWitnessContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static VoteWitnessContract parseFrom(j jVar) throws IOException {
            return (VoteWitnessContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static VoteWitnessContract parseFrom(j jVar, r rVar) throws IOException {
            return (VoteWitnessContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface VoteWitnessContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        boolean getSupport();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        VoteWitnessContract.Vote getVotes(int i);

        int getVotesCount();

        List<VoteWitnessContract.Vote> getVotesList();

        VoteWitnessContract.VoteOrBuilder getVotesOrBuilder(int i);

        List<? extends VoteWitnessContract.VoteOrBuilder> getVotesOrBuilderList();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class WithdrawBalanceContract extends GeneratedMessageV3 implements WithdrawBalanceContractOrBuilder {
        public static final int OWNER_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object ownerAddress_;
        private static final WithdrawBalanceContract DEFAULT_INSTANCE = new WithdrawBalanceContract();
        private static final t0<WithdrawBalanceContract> PARSER = new c<WithdrawBalanceContract>() { // from class: wallet.core.jni.proto.Tron.WithdrawBalanceContract.1
            @Override // com.google.protobuf.t0
            public WithdrawBalanceContract parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new WithdrawBalanceContract(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements WithdrawBalanceContractOrBuilder {
            private Object ownerAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tron.internal_static_TW_Tron_Proto_WithdrawBalanceContract_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearOwnerAddress() {
                this.ownerAddress_ = WithdrawBalanceContract.getDefaultInstance().getOwnerAddress();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tron.internal_static_TW_Tron_Proto_WithdrawBalanceContract_descriptor;
            }

            @Override // wallet.core.jni.proto.Tron.WithdrawBalanceContractOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.ownerAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tron.WithdrawBalanceContractOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tron.internal_static_TW_Tron_Proto_WithdrawBalanceContract_fieldAccessorTable.d(WithdrawBalanceContract.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setOwnerAddress(String str) {
                Objects.requireNonNull(str);
                this.ownerAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setOwnerAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.ownerAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.ownerAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public WithdrawBalanceContract build() {
                WithdrawBalanceContract buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public WithdrawBalanceContract buildPartial() {
                WithdrawBalanceContract withdrawBalanceContract = new WithdrawBalanceContract(this, (AnonymousClass1) null);
                withdrawBalanceContract.ownerAddress_ = this.ownerAddress_;
                onBuilt();
                return withdrawBalanceContract;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public WithdrawBalanceContract getDefaultInstanceForType() {
                return WithdrawBalanceContract.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.ownerAddress_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.ownerAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof WithdrawBalanceContract) {
                    return mergeFrom((WithdrawBalanceContract) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(WithdrawBalanceContract withdrawBalanceContract) {
                if (withdrawBalanceContract == WithdrawBalanceContract.getDefaultInstance()) {
                    return this;
                }
                if (!withdrawBalanceContract.getOwnerAddress().isEmpty()) {
                    this.ownerAddress_ = withdrawBalanceContract.ownerAddress_;
                    onChanged();
                }
                mergeUnknownFields(withdrawBalanceContract.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tron.WithdrawBalanceContract.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tron.WithdrawBalanceContract.access$13800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tron$WithdrawBalanceContract r3 = (wallet.core.jni.proto.Tron.WithdrawBalanceContract) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tron$WithdrawBalanceContract r4 = (wallet.core.jni.proto.Tron.WithdrawBalanceContract) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tron.WithdrawBalanceContract.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tron$WithdrawBalanceContract$Builder");
            }
        }

        public /* synthetic */ WithdrawBalanceContract(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static WithdrawBalanceContract getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tron.internal_static_TW_Tron_Proto_WithdrawBalanceContract_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static WithdrawBalanceContract parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (WithdrawBalanceContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static WithdrawBalanceContract parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<WithdrawBalanceContract> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof WithdrawBalanceContract)) {
                return super.equals(obj);
            }
            WithdrawBalanceContract withdrawBalanceContract = (WithdrawBalanceContract) obj;
            return getOwnerAddress().equals(withdrawBalanceContract.getOwnerAddress()) && this.unknownFields.equals(withdrawBalanceContract.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tron.WithdrawBalanceContractOrBuilder
        public String getOwnerAddress() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ownerAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tron.WithdrawBalanceContractOrBuilder
        public ByteString getOwnerAddressBytes() {
            Object obj = this.ownerAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.ownerAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<WithdrawBalanceContract> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getOwnerAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.ownerAddress_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getOwnerAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tron.internal_static_TW_Tron_Proto_WithdrawBalanceContract_fieldAccessorTable.d(WithdrawBalanceContract.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new WithdrawBalanceContract();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getOwnerAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.ownerAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ WithdrawBalanceContract(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(WithdrawBalanceContract withdrawBalanceContract) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(withdrawBalanceContract);
        }

        public static WithdrawBalanceContract parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private WithdrawBalanceContract(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static WithdrawBalanceContract parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (WithdrawBalanceContract) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static WithdrawBalanceContract parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public WithdrawBalanceContract getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static WithdrawBalanceContract parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private WithdrawBalanceContract() {
            this.memoizedIsInitialized = (byte) -1;
            this.ownerAddress_ = "";
        }

        public static WithdrawBalanceContract parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static WithdrawBalanceContract parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static WithdrawBalanceContract parseFrom(InputStream inputStream) throws IOException {
            return (WithdrawBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private WithdrawBalanceContract(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.ownerAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static WithdrawBalanceContract parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (WithdrawBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static WithdrawBalanceContract parseFrom(j jVar) throws IOException {
            return (WithdrawBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static WithdrawBalanceContract parseFrom(j jVar, r rVar) throws IOException {
            return (WithdrawBalanceContract) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface WithdrawBalanceContractOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getOwnerAddress();

        ByteString getOwnerAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Tron_Proto_TransferContract_descriptor = bVar;
        internal_static_TW_Tron_Proto_TransferContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"OwnerAddress", "ToAddress", "Amount"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Tron_Proto_TransferAssetContract_descriptor = bVar2;
        internal_static_TW_Tron_Proto_TransferAssetContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"AssetName", "OwnerAddress", "ToAddress", "Amount"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Tron_Proto_TransferTRC20Contract_descriptor = bVar3;
        internal_static_TW_Tron_Proto_TransferTRC20Contract_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"ContractAddress", "OwnerAddress", "ToAddress", "Amount"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Tron_Proto_FreezeBalanceContract_descriptor = bVar4;
        internal_static_TW_Tron_Proto_FreezeBalanceContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"OwnerAddress", "FrozenBalance", "FrozenDuration", "Resource", "ReceiverAddress"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_Tron_Proto_UnfreezeBalanceContract_descriptor = bVar5;
        internal_static_TW_Tron_Proto_UnfreezeBalanceContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"OwnerAddress", "Resource", "ReceiverAddress"});
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_Tron_Proto_UnfreezeAssetContract_descriptor = bVar6;
        internal_static_TW_Tron_Proto_UnfreezeAssetContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"OwnerAddress"});
        Descriptors.b bVar7 = getDescriptor().o().get(6);
        internal_static_TW_Tron_Proto_VoteAssetContract_descriptor = bVar7;
        internal_static_TW_Tron_Proto_VoteAssetContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"OwnerAddress", "VoteAddress", "Support", "Count"});
        Descriptors.b bVar8 = getDescriptor().o().get(7);
        internal_static_TW_Tron_Proto_VoteWitnessContract_descriptor = bVar8;
        internal_static_TW_Tron_Proto_VoteWitnessContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"OwnerAddress", "Votes", "Support"});
        Descriptors.b bVar9 = bVar8.r().get(0);
        internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_descriptor = bVar9;
        internal_static_TW_Tron_Proto_VoteWitnessContract_Vote_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"VoteAddress", "VoteCount"});
        Descriptors.b bVar10 = getDescriptor().o().get(8);
        internal_static_TW_Tron_Proto_WithdrawBalanceContract_descriptor = bVar10;
        internal_static_TW_Tron_Proto_WithdrawBalanceContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"OwnerAddress"});
        Descriptors.b bVar11 = getDescriptor().o().get(9);
        internal_static_TW_Tron_Proto_TriggerSmartContract_descriptor = bVar11;
        internal_static_TW_Tron_Proto_TriggerSmartContract_fieldAccessorTable = new GeneratedMessageV3.e(bVar11, new String[]{"OwnerAddress", "ContractAddress", "CallValue", "Data", "CallTokenValue", "TokenId"});
        Descriptors.b bVar12 = getDescriptor().o().get(10);
        internal_static_TW_Tron_Proto_BlockHeader_descriptor = bVar12;
        internal_static_TW_Tron_Proto_BlockHeader_fieldAccessorTable = new GeneratedMessageV3.e(bVar12, new String[]{"Timestamp", "TxTrieRoot", "ParentHash", "Number", "WitnessAddress", "Version"});
        Descriptors.b bVar13 = getDescriptor().o().get(11);
        internal_static_TW_Tron_Proto_Transaction_descriptor = bVar13;
        internal_static_TW_Tron_Proto_Transaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar13, new String[]{"Timestamp", "Expiration", "BlockHeader", "FeeLimit", "Transfer", "TransferAsset", "FreezeBalance", "UnfreezeBalance", "UnfreezeAsset", "WithdrawBalance", "VoteAsset", "VoteWitness", "TriggerSmartContract", "TransferTrc20Contract", "ContractOneof"});
        Descriptors.b bVar14 = getDescriptor().o().get(12);
        internal_static_TW_Tron_Proto_SigningInput_descriptor = bVar14;
        internal_static_TW_Tron_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar14, new String[]{"Transaction", "PrivateKey"});
        Descriptors.b bVar15 = getDescriptor().o().get(13);
        internal_static_TW_Tron_Proto_SigningOutput_descriptor = bVar15;
        internal_static_TW_Tron_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar15, new String[]{"Id", "Signature", "RefBlockBytes", "RefBlockHash", "Json"});
    }

    private Tron() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
