package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Solana {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\fSolana.proto\u0012\u000fTW.Solana.Proto\",\n\bTransfer\u0012\u0011\n\trecipient\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\u0004\"0\n\u0005Stake\u0012\u0018\n\u0010validator_pubkey\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\u0004\"+\n\u000fDeactivateStake\u0012\u0018\n\u0010validator_pubkey\u0018\u0001 \u0001(\t\"8\n\rWithdrawStake\u0012\u0018\n\u0010validator_pubkey\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\u0004\"]\n\u0012CreateTokenAccount\u0012\u0014\n\fmain_address\u0018\u0001 \u0001(\t\u0012\u001a\n\u0012token_mint_address\u0018\u0002 \u0001(\t\u0012\u0015\n\rtoken_address\u0018\u0003 \u0001(\t\"\u008c\u0001\n\rTokenTransfer\u0012\u001a\n\u0012token_mint_address\u0018\u0001 \u0001(\t\u0012\u001c\n\u0014sender_token_address\u0018\u0002 \u0001(\t\u0012\u001f\n\u0017recipient_token_address\u0018\u0003 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0004 \u0001(\u0004\u0012\u0010\n\bdecimals\u0018\u0005 \u0001(\r\"µ\u0001\n\u0016CreateAndTransferToken\u0012\u001e\n\u0016recipient_main_address\u0018\u0001 \u0001(\t\u0012\u001a\n\u0012token_mint_address\u0018\u0002 \u0001(\t\u0012\u001f\n\u0017recipient_token_address\u0018\u0003 \u0001(\t\u0012\u001c\n\u0014sender_token_address\u0018\u0004 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0005 \u0001(\u0004\u0012\u0010\n\bdecimals\u0018\u0006 \u0001(\r\"¼\u0004\n\fSigningInput\u0012\u0013\n\u000bprivate_key\u0018\u0001 \u0001(\f\u0012\u0018\n\u0010recent_blockhash\u0018\u0002 \u0001(\t\u00129\n\u0014transfer_transaction\u0018\u0003 \u0001(\u000b2\u0019.TW.Solana.Proto.TransferH\u0000\u00123\n\u0011stake_transaction\u0018\u0004 \u0001(\u000b2\u0016.TW.Solana.Proto.StakeH\u0000\u0012H\n\u001cdeactivate_stake_transaction\u0018\u0005 \u0001(\u000b2 .TW.Solana.Proto.DeactivateStakeH\u0000\u0012>\n\u0014withdraw_transaction\u0018\u0006 \u0001(\u000b2\u001e.TW.Solana.Proto.WithdrawStakeH\u0000\u0012O\n create_token_account_transaction\u0018\u0007 \u0001(\u000b2#.TW.Solana.Proto.CreateTokenAccountH\u0000\u0012D\n\u001atoken_transfer_transaction\u0018\b \u0001(\u000b2\u001e.TW.Solana.Proto.TokenTransferH\u0000\u0012X\n%create_and_transfer_token_transaction\u0018\t \u0001(\u000b2'.TW.Solana.Proto.CreateAndTransferTokenH\u0000B\u0012\n\u0010transaction_type\" \n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Solana_Proto_CreateAndTransferToken_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_CreateAndTransferToken_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_CreateTokenAccount_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_CreateTokenAccount_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_DeactivateStake_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_DeactivateStake_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_Stake_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_Stake_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_TokenTransfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_TokenTransfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Solana_Proto_WithdrawStake_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Solana_Proto_WithdrawStake_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Solana$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase;

        static {
            int[] iArr = new int[SigningInput.TransactionTypeCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase = iArr;
            try {
                iArr[SigningInput.TransactionTypeCase.TRANSFER_TRANSACTION.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[SigningInput.TransactionTypeCase.STAKE_TRANSACTION.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[SigningInput.TransactionTypeCase.DEACTIVATE_STAKE_TRANSACTION.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[SigningInput.TransactionTypeCase.WITHDRAW_TRANSACTION.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[SigningInput.TransactionTypeCase.CREATE_TOKEN_ACCOUNT_TRANSACTION.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[SigningInput.TransactionTypeCase.TOKEN_TRANSFER_TRANSACTION.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[SigningInput.TransactionTypeCase.CREATE_AND_TRANSFER_TOKEN_TRANSACTION.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[SigningInput.TransactionTypeCase.TRANSACTIONTYPE_NOT_SET.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class CreateAndTransferToken extends GeneratedMessageV3 implements CreateAndTransferTokenOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 5;
        public static final int DECIMALS_FIELD_NUMBER = 6;
        private static final CreateAndTransferToken DEFAULT_INSTANCE = new CreateAndTransferToken();
        private static final t0<CreateAndTransferToken> PARSER = new c<CreateAndTransferToken>() { // from class: wallet.core.jni.proto.Solana.CreateAndTransferToken.1
            @Override // com.google.protobuf.t0
            public CreateAndTransferToken parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new CreateAndTransferToken(jVar, rVar, null);
            }
        };
        public static final int RECIPIENT_MAIN_ADDRESS_FIELD_NUMBER = 1;
        public static final int RECIPIENT_TOKEN_ADDRESS_FIELD_NUMBER = 3;
        public static final int SENDER_TOKEN_ADDRESS_FIELD_NUMBER = 4;
        public static final int TOKEN_MINT_ADDRESS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private long amount_;
        private int decimals_;
        private byte memoizedIsInitialized;
        private volatile Object recipientMainAddress_;
        private volatile Object recipientTokenAddress_;
        private volatile Object senderTokenAddress_;
        private volatile Object tokenMintAddress_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements CreateAndTransferTokenOrBuilder {
            private long amount_;
            private int decimals_;
            private Object recipientMainAddress_;
            private Object recipientTokenAddress_;
            private Object senderTokenAddress_;
            private Object tokenMintAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_CreateAndTransferToken_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearDecimals() {
                this.decimals_ = 0;
                onChanged();
                return this;
            }

            public Builder clearRecipientMainAddress() {
                this.recipientMainAddress_ = CreateAndTransferToken.getDefaultInstance().getRecipientMainAddress();
                onChanged();
                return this;
            }

            public Builder clearRecipientTokenAddress() {
                this.recipientTokenAddress_ = CreateAndTransferToken.getDefaultInstance().getRecipientTokenAddress();
                onChanged();
                return this;
            }

            public Builder clearSenderTokenAddress() {
                this.senderTokenAddress_ = CreateAndTransferToken.getDefaultInstance().getSenderTokenAddress();
                onChanged();
                return this;
            }

            public Builder clearTokenMintAddress() {
                this.tokenMintAddress_ = CreateAndTransferToken.getDefaultInstance().getTokenMintAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public int getDecimals() {
                return this.decimals_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_CreateAndTransferToken_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public String getRecipientMainAddress() {
                Object obj = this.recipientMainAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.recipientMainAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public ByteString getRecipientMainAddressBytes() {
                Object obj = this.recipientMainAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.recipientMainAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public String getRecipientTokenAddress() {
                Object obj = this.recipientTokenAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.recipientTokenAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public ByteString getRecipientTokenAddressBytes() {
                Object obj = this.recipientTokenAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.recipientTokenAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public String getSenderTokenAddress() {
                Object obj = this.senderTokenAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.senderTokenAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public ByteString getSenderTokenAddressBytes() {
                Object obj = this.senderTokenAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.senderTokenAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public String getTokenMintAddress() {
                Object obj = this.tokenMintAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.tokenMintAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
            public ByteString getTokenMintAddressBytes() {
                Object obj = this.tokenMintAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.tokenMintAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_CreateAndTransferToken_fieldAccessorTable.d(CreateAndTransferToken.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setDecimals(int i) {
                this.decimals_ = i;
                onChanged();
                return this;
            }

            public Builder setRecipientMainAddress(String str) {
                Objects.requireNonNull(str);
                this.recipientMainAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setRecipientMainAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.recipientMainAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setRecipientTokenAddress(String str) {
                Objects.requireNonNull(str);
                this.recipientTokenAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setRecipientTokenAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.recipientTokenAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSenderTokenAddress(String str) {
                Objects.requireNonNull(str);
                this.senderTokenAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setSenderTokenAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.senderTokenAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTokenMintAddress(String str) {
                Objects.requireNonNull(str);
                this.tokenMintAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setTokenMintAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.tokenMintAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.recipientMainAddress_ = "";
                this.tokenMintAddress_ = "";
                this.recipientTokenAddress_ = "";
                this.senderTokenAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CreateAndTransferToken build() {
                CreateAndTransferToken buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CreateAndTransferToken buildPartial() {
                CreateAndTransferToken createAndTransferToken = new CreateAndTransferToken(this, (AnonymousClass1) null);
                createAndTransferToken.recipientMainAddress_ = this.recipientMainAddress_;
                createAndTransferToken.tokenMintAddress_ = this.tokenMintAddress_;
                createAndTransferToken.recipientTokenAddress_ = this.recipientTokenAddress_;
                createAndTransferToken.senderTokenAddress_ = this.senderTokenAddress_;
                createAndTransferToken.amount_ = this.amount_;
                createAndTransferToken.decimals_ = this.decimals_;
                onBuilt();
                return createAndTransferToken;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public CreateAndTransferToken getDefaultInstanceForType() {
                return CreateAndTransferToken.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.recipientMainAddress_ = "";
                this.tokenMintAddress_ = "";
                this.recipientTokenAddress_ = "";
                this.senderTokenAddress_ = "";
                this.amount_ = 0L;
                this.decimals_ = 0;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof CreateAndTransferToken) {
                    return mergeFrom((CreateAndTransferToken) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.recipientMainAddress_ = "";
                this.tokenMintAddress_ = "";
                this.recipientTokenAddress_ = "";
                this.senderTokenAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(CreateAndTransferToken createAndTransferToken) {
                if (createAndTransferToken == CreateAndTransferToken.getDefaultInstance()) {
                    return this;
                }
                if (!createAndTransferToken.getRecipientMainAddress().isEmpty()) {
                    this.recipientMainAddress_ = createAndTransferToken.recipientMainAddress_;
                    onChanged();
                }
                if (!createAndTransferToken.getTokenMintAddress().isEmpty()) {
                    this.tokenMintAddress_ = createAndTransferToken.tokenMintAddress_;
                    onChanged();
                }
                if (!createAndTransferToken.getRecipientTokenAddress().isEmpty()) {
                    this.recipientTokenAddress_ = createAndTransferToken.recipientTokenAddress_;
                    onChanged();
                }
                if (!createAndTransferToken.getSenderTokenAddress().isEmpty()) {
                    this.senderTokenAddress_ = createAndTransferToken.senderTokenAddress_;
                    onChanged();
                }
                if (createAndTransferToken.getAmount() != 0) {
                    setAmount(createAndTransferToken.getAmount());
                }
                if (createAndTransferToken.getDecimals() != 0) {
                    setDecimals(createAndTransferToken.getDecimals());
                }
                mergeUnknownFields(createAndTransferToken.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.CreateAndTransferToken.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.CreateAndTransferToken.access$9200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$CreateAndTransferToken r3 = (wallet.core.jni.proto.Solana.CreateAndTransferToken) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$CreateAndTransferToken r4 = (wallet.core.jni.proto.Solana.CreateAndTransferToken) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.CreateAndTransferToken.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$CreateAndTransferToken$Builder");
            }
        }

        public /* synthetic */ CreateAndTransferToken(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static CreateAndTransferToken getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_CreateAndTransferToken_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static CreateAndTransferToken parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (CreateAndTransferToken) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static CreateAndTransferToken parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<CreateAndTransferToken> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof CreateAndTransferToken)) {
                return super.equals(obj);
            }
            CreateAndTransferToken createAndTransferToken = (CreateAndTransferToken) obj;
            return getRecipientMainAddress().equals(createAndTransferToken.getRecipientMainAddress()) && getTokenMintAddress().equals(createAndTransferToken.getTokenMintAddress()) && getRecipientTokenAddress().equals(createAndTransferToken.getRecipientTokenAddress()) && getSenderTokenAddress().equals(createAndTransferToken.getSenderTokenAddress()) && getAmount() == createAndTransferToken.getAmount() && getDecimals() == createAndTransferToken.getDecimals() && this.unknownFields.equals(createAndTransferToken.unknownFields);
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public int getDecimals() {
            return this.decimals_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<CreateAndTransferToken> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public String getRecipientMainAddress() {
            Object obj = this.recipientMainAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.recipientMainAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public ByteString getRecipientMainAddressBytes() {
            Object obj = this.recipientMainAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.recipientMainAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public String getRecipientTokenAddress() {
            Object obj = this.recipientTokenAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.recipientTokenAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public ByteString getRecipientTokenAddressBytes() {
            Object obj = this.recipientTokenAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.recipientTokenAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public String getSenderTokenAddress() {
            Object obj = this.senderTokenAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.senderTokenAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public ByteString getSenderTokenAddressBytes() {
            Object obj = this.senderTokenAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.senderTokenAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getRecipientMainAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.recipientMainAddress_);
            if (!getTokenMintAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.tokenMintAddress_);
            }
            if (!getRecipientTokenAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.recipientTokenAddress_);
            }
            if (!getSenderTokenAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(4, this.senderTokenAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(5, j);
            }
            int i2 = this.decimals_;
            if (i2 != 0) {
                computeStringSize += CodedOutputStream.Y(6, i2);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public String getTokenMintAddress() {
            Object obj = this.tokenMintAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.tokenMintAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.CreateAndTransferTokenOrBuilder
        public ByteString getTokenMintAddressBytes() {
            Object obj = this.tokenMintAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.tokenMintAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getRecipientMainAddress().hashCode()) * 37) + 2) * 53) + getTokenMintAddress().hashCode()) * 37) + 3) * 53) + getRecipientTokenAddress().hashCode()) * 37) + 4) * 53) + getSenderTokenAddress().hashCode()) * 37) + 5) * 53) + a0.h(getAmount())) * 37) + 6) * 53) + getDecimals()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_CreateAndTransferToken_fieldAccessorTable.d(CreateAndTransferToken.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new CreateAndTransferToken();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getRecipientMainAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.recipientMainAddress_);
            }
            if (!getTokenMintAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.tokenMintAddress_);
            }
            if (!getRecipientTokenAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.recipientTokenAddress_);
            }
            if (!getSenderTokenAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.senderTokenAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.d1(5, j);
            }
            int i = this.decimals_;
            if (i != 0) {
                codedOutputStream.b1(6, i);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ CreateAndTransferToken(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(CreateAndTransferToken createAndTransferToken) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(createAndTransferToken);
        }

        public static CreateAndTransferToken parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private CreateAndTransferToken(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static CreateAndTransferToken parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (CreateAndTransferToken) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static CreateAndTransferToken parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public CreateAndTransferToken getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static CreateAndTransferToken parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private CreateAndTransferToken() {
            this.memoizedIsInitialized = (byte) -1;
            this.recipientMainAddress_ = "";
            this.tokenMintAddress_ = "";
            this.recipientTokenAddress_ = "";
            this.senderTokenAddress_ = "";
        }

        public static CreateAndTransferToken parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static CreateAndTransferToken parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static CreateAndTransferToken parseFrom(InputStream inputStream) throws IOException {
            return (CreateAndTransferToken) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static CreateAndTransferToken parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (CreateAndTransferToken) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private CreateAndTransferToken(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.recipientMainAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.tokenMintAddress_ = jVar.I();
                            } else if (J == 26) {
                                this.recipientTokenAddress_ = jVar.I();
                            } else if (J == 34) {
                                this.senderTokenAddress_ = jVar.I();
                            } else if (J == 40) {
                                this.amount_ = jVar.L();
                            } else if (J != 48) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.decimals_ = jVar.K();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static CreateAndTransferToken parseFrom(j jVar) throws IOException {
            return (CreateAndTransferToken) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static CreateAndTransferToken parseFrom(j jVar, r rVar) throws IOException {
            return (CreateAndTransferToken) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface CreateAndTransferTokenOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        int getDecimals();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getRecipientMainAddress();

        ByteString getRecipientMainAddressBytes();

        String getRecipientTokenAddress();

        ByteString getRecipientTokenAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSenderTokenAddress();

        ByteString getSenderTokenAddressBytes();

        String getTokenMintAddress();

        ByteString getTokenMintAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class CreateTokenAccount extends GeneratedMessageV3 implements CreateTokenAccountOrBuilder {
        public static final int MAIN_ADDRESS_FIELD_NUMBER = 1;
        public static final int TOKEN_ADDRESS_FIELD_NUMBER = 3;
        public static final int TOKEN_MINT_ADDRESS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private volatile Object mainAddress_;
        private byte memoizedIsInitialized;
        private volatile Object tokenAddress_;
        private volatile Object tokenMintAddress_;
        private static final CreateTokenAccount DEFAULT_INSTANCE = new CreateTokenAccount();
        private static final t0<CreateTokenAccount> PARSER = new c<CreateTokenAccount>() { // from class: wallet.core.jni.proto.Solana.CreateTokenAccount.1
            @Override // com.google.protobuf.t0
            public CreateTokenAccount parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new CreateTokenAccount(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements CreateTokenAccountOrBuilder {
            private Object mainAddress_;
            private Object tokenAddress_;
            private Object tokenMintAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_CreateTokenAccount_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearMainAddress() {
                this.mainAddress_ = CreateTokenAccount.getDefaultInstance().getMainAddress();
                onChanged();
                return this;
            }

            public Builder clearTokenAddress() {
                this.tokenAddress_ = CreateTokenAccount.getDefaultInstance().getTokenAddress();
                onChanged();
                return this;
            }

            public Builder clearTokenMintAddress() {
                this.tokenMintAddress_ = CreateTokenAccount.getDefaultInstance().getTokenMintAddress();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_CreateTokenAccount_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
            public String getMainAddress() {
                Object obj = this.mainAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.mainAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
            public ByteString getMainAddressBytes() {
                Object obj = this.mainAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.mainAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
            public String getTokenAddress() {
                Object obj = this.tokenAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.tokenAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
            public ByteString getTokenAddressBytes() {
                Object obj = this.tokenAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.tokenAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
            public String getTokenMintAddress() {
                Object obj = this.tokenMintAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.tokenMintAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
            public ByteString getTokenMintAddressBytes() {
                Object obj = this.tokenMintAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.tokenMintAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_CreateTokenAccount_fieldAccessorTable.d(CreateTokenAccount.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setMainAddress(String str) {
                Objects.requireNonNull(str);
                this.mainAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setMainAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.mainAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTokenAddress(String str) {
                Objects.requireNonNull(str);
                this.tokenAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setTokenAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.tokenAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTokenMintAddress(String str) {
                Objects.requireNonNull(str);
                this.tokenMintAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setTokenMintAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.tokenMintAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.mainAddress_ = "";
                this.tokenMintAddress_ = "";
                this.tokenAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CreateTokenAccount build() {
                CreateTokenAccount buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public CreateTokenAccount buildPartial() {
                CreateTokenAccount createTokenAccount = new CreateTokenAccount(this, (AnonymousClass1) null);
                createTokenAccount.mainAddress_ = this.mainAddress_;
                createTokenAccount.tokenMintAddress_ = this.tokenMintAddress_;
                createTokenAccount.tokenAddress_ = this.tokenAddress_;
                onBuilt();
                return createTokenAccount;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public CreateTokenAccount getDefaultInstanceForType() {
                return CreateTokenAccount.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.mainAddress_ = "";
                this.tokenMintAddress_ = "";
                this.tokenAddress_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof CreateTokenAccount) {
                    return mergeFrom((CreateTokenAccount) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.mainAddress_ = "";
                this.tokenMintAddress_ = "";
                this.tokenAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(CreateTokenAccount createTokenAccount) {
                if (createTokenAccount == CreateTokenAccount.getDefaultInstance()) {
                    return this;
                }
                if (!createTokenAccount.getMainAddress().isEmpty()) {
                    this.mainAddress_ = createTokenAccount.mainAddress_;
                    onChanged();
                }
                if (!createTokenAccount.getTokenMintAddress().isEmpty()) {
                    this.tokenMintAddress_ = createTokenAccount.tokenMintAddress_;
                    onChanged();
                }
                if (!createTokenAccount.getTokenAddress().isEmpty()) {
                    this.tokenAddress_ = createTokenAccount.tokenAddress_;
                    onChanged();
                }
                mergeUnknownFields(createTokenAccount.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.CreateTokenAccount.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.CreateTokenAccount.access$5700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$CreateTokenAccount r3 = (wallet.core.jni.proto.Solana.CreateTokenAccount) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$CreateTokenAccount r4 = (wallet.core.jni.proto.Solana.CreateTokenAccount) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.CreateTokenAccount.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$CreateTokenAccount$Builder");
            }
        }

        public /* synthetic */ CreateTokenAccount(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static CreateTokenAccount getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_CreateTokenAccount_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static CreateTokenAccount parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (CreateTokenAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static CreateTokenAccount parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<CreateTokenAccount> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof CreateTokenAccount)) {
                return super.equals(obj);
            }
            CreateTokenAccount createTokenAccount = (CreateTokenAccount) obj;
            return getMainAddress().equals(createTokenAccount.getMainAddress()) && getTokenMintAddress().equals(createTokenAccount.getTokenMintAddress()) && getTokenAddress().equals(createTokenAccount.getTokenAddress()) && this.unknownFields.equals(createTokenAccount.unknownFields);
        }

        @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
        public String getMainAddress() {
            Object obj = this.mainAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.mainAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
        public ByteString getMainAddressBytes() {
            Object obj = this.mainAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.mainAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<CreateTokenAccount> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getMainAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.mainAddress_);
            if (!getTokenMintAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.tokenMintAddress_);
            }
            if (!getTokenAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.tokenAddress_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
        public String getTokenAddress() {
            Object obj = this.tokenAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.tokenAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
        public ByteString getTokenAddressBytes() {
            Object obj = this.tokenAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.tokenAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
        public String getTokenMintAddress() {
            Object obj = this.tokenMintAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.tokenMintAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.CreateTokenAccountOrBuilder
        public ByteString getTokenMintAddressBytes() {
            Object obj = this.tokenMintAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.tokenMintAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getMainAddress().hashCode()) * 37) + 2) * 53) + getTokenMintAddress().hashCode()) * 37) + 3) * 53) + getTokenAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_CreateTokenAccount_fieldAccessorTable.d(CreateTokenAccount.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new CreateTokenAccount();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getMainAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.mainAddress_);
            }
            if (!getTokenMintAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.tokenMintAddress_);
            }
            if (!getTokenAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.tokenAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ CreateTokenAccount(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(CreateTokenAccount createTokenAccount) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(createTokenAccount);
        }

        public static CreateTokenAccount parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private CreateTokenAccount(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static CreateTokenAccount parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (CreateTokenAccount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static CreateTokenAccount parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public CreateTokenAccount getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static CreateTokenAccount parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private CreateTokenAccount() {
            this.memoizedIsInitialized = (byte) -1;
            this.mainAddress_ = "";
            this.tokenMintAddress_ = "";
            this.tokenAddress_ = "";
        }

        public static CreateTokenAccount parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static CreateTokenAccount parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static CreateTokenAccount parseFrom(InputStream inputStream) throws IOException {
            return (CreateTokenAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static CreateTokenAccount parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (CreateTokenAccount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private CreateTokenAccount(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.mainAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.tokenMintAddress_ = jVar.I();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.tokenAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static CreateTokenAccount parseFrom(j jVar) throws IOException {
            return (CreateTokenAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static CreateTokenAccount parseFrom(j jVar, r rVar) throws IOException {
            return (CreateTokenAccount) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface CreateTokenAccountOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getMainAddress();

        ByteString getMainAddressBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getTokenAddress();

        ByteString getTokenAddressBytes();

        String getTokenMintAddress();

        ByteString getTokenMintAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class DeactivateStake extends GeneratedMessageV3 implements DeactivateStakeOrBuilder {
        private static final DeactivateStake DEFAULT_INSTANCE = new DeactivateStake();
        private static final t0<DeactivateStake> PARSER = new c<DeactivateStake>() { // from class: wallet.core.jni.proto.Solana.DeactivateStake.1
            @Override // com.google.protobuf.t0
            public DeactivateStake parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DeactivateStake(jVar, rVar, null);
            }
        };
        public static final int VALIDATOR_PUBKEY_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object validatorPubkey_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DeactivateStakeOrBuilder {
            private Object validatorPubkey_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_DeactivateStake_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearValidatorPubkey() {
                this.validatorPubkey_ = DeactivateStake.getDefaultInstance().getValidatorPubkey();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_DeactivateStake_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.DeactivateStakeOrBuilder
            public String getValidatorPubkey() {
                Object obj = this.validatorPubkey_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.validatorPubkey_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.DeactivateStakeOrBuilder
            public ByteString getValidatorPubkeyBytes() {
                Object obj = this.validatorPubkey_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorPubkey_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_DeactivateStake_fieldAccessorTable.d(DeactivateStake.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setValidatorPubkey(String str) {
                Objects.requireNonNull(str);
                this.validatorPubkey_ = str;
                onChanged();
                return this;
            }

            public Builder setValidatorPubkeyBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.validatorPubkey_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.validatorPubkey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeactivateStake build() {
                DeactivateStake buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DeactivateStake buildPartial() {
                DeactivateStake deactivateStake = new DeactivateStake(this, (AnonymousClass1) null);
                deactivateStake.validatorPubkey_ = this.validatorPubkey_;
                onBuilt();
                return deactivateStake;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DeactivateStake getDefaultInstanceForType() {
                return DeactivateStake.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.validatorPubkey_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.validatorPubkey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DeactivateStake) {
                    return mergeFrom((DeactivateStake) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DeactivateStake deactivateStake) {
                if (deactivateStake == DeactivateStake.getDefaultInstance()) {
                    return this;
                }
                if (!deactivateStake.getValidatorPubkey().isEmpty()) {
                    this.validatorPubkey_ = deactivateStake.validatorPubkey_;
                    onChanged();
                }
                mergeUnknownFields(deactivateStake.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.DeactivateStake.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.DeactivateStake.access$3200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$DeactivateStake r3 = (wallet.core.jni.proto.Solana.DeactivateStake) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$DeactivateStake r4 = (wallet.core.jni.proto.Solana.DeactivateStake) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.DeactivateStake.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$DeactivateStake$Builder");
            }
        }

        public /* synthetic */ DeactivateStake(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DeactivateStake getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_DeactivateStake_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DeactivateStake parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DeactivateStake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DeactivateStake parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DeactivateStake> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DeactivateStake)) {
                return super.equals(obj);
            }
            DeactivateStake deactivateStake = (DeactivateStake) obj;
            return getValidatorPubkey().equals(deactivateStake.getValidatorPubkey()) && this.unknownFields.equals(deactivateStake.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DeactivateStake> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getValidatorPubkeyBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.validatorPubkey_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Solana.DeactivateStakeOrBuilder
        public String getValidatorPubkey() {
            Object obj = this.validatorPubkey_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.validatorPubkey_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.DeactivateStakeOrBuilder
        public ByteString getValidatorPubkeyBytes() {
            Object obj = this.validatorPubkey_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.validatorPubkey_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValidatorPubkey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_DeactivateStake_fieldAccessorTable.d(DeactivateStake.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DeactivateStake();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getValidatorPubkeyBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.validatorPubkey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DeactivateStake(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DeactivateStake deactivateStake) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(deactivateStake);
        }

        public static DeactivateStake parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DeactivateStake(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DeactivateStake parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeactivateStake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DeactivateStake parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DeactivateStake getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DeactivateStake parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DeactivateStake() {
            this.memoizedIsInitialized = (byte) -1;
            this.validatorPubkey_ = "";
        }

        public static DeactivateStake parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DeactivateStake parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DeactivateStake parseFrom(InputStream inputStream) throws IOException {
            return (DeactivateStake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private DeactivateStake(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.validatorPubkey_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DeactivateStake parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DeactivateStake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DeactivateStake parseFrom(j jVar) throws IOException {
            return (DeactivateStake) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DeactivateStake parseFrom(j jVar, r rVar) throws IOException {
            return (DeactivateStake) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DeactivateStakeOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValidatorPubkey();

        ByteString getValidatorPubkeyBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int CREATE_AND_TRANSFER_TOKEN_TRANSACTION_FIELD_NUMBER = 9;
        public static final int CREATE_TOKEN_ACCOUNT_TRANSACTION_FIELD_NUMBER = 7;
        public static final int DEACTIVATE_STAKE_TRANSACTION_FIELD_NUMBER = 5;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Solana.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };
        public static final int PRIVATE_KEY_FIELD_NUMBER = 1;
        public static final int RECENT_BLOCKHASH_FIELD_NUMBER = 2;
        public static final int STAKE_TRANSACTION_FIELD_NUMBER = 4;
        public static final int TOKEN_TRANSFER_TRANSACTION_FIELD_NUMBER = 8;
        public static final int TRANSFER_TRANSACTION_FIELD_NUMBER = 3;
        public static final int WITHDRAW_TRANSACTION_FIELD_NUMBER = 6;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString privateKey_;
        private volatile Object recentBlockhash_;
        private int transactionTypeCase_;
        private Object transactionType_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> createAndTransferTokenTransactionBuilder_;
            private a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> createTokenAccountTransactionBuilder_;
            private a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> deactivateStakeTransactionBuilder_;
            private ByteString privateKey_;
            private Object recentBlockhash_;
            private a1<Stake, Stake.Builder, StakeOrBuilder> stakeTransactionBuilder_;
            private a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> tokenTransferTransactionBuilder_;
            private int transactionTypeCase_;
            private Object transactionType_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferTransactionBuilder_;
            private a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> withdrawTransactionBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> getCreateAndTransferTokenTransactionFieldBuilder() {
                if (this.createAndTransferTokenTransactionBuilder_ == null) {
                    if (this.transactionTypeCase_ != 9) {
                        this.transactionType_ = CreateAndTransferToken.getDefaultInstance();
                    }
                    this.createAndTransferTokenTransactionBuilder_ = new a1<>((CreateAndTransferToken) this.transactionType_, getParentForChildren(), isClean());
                    this.transactionType_ = null;
                }
                this.transactionTypeCase_ = 9;
                onChanged();
                return this.createAndTransferTokenTransactionBuilder_;
            }

            private a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> getCreateTokenAccountTransactionFieldBuilder() {
                if (this.createTokenAccountTransactionBuilder_ == null) {
                    if (this.transactionTypeCase_ != 7) {
                        this.transactionType_ = CreateTokenAccount.getDefaultInstance();
                    }
                    this.createTokenAccountTransactionBuilder_ = new a1<>((CreateTokenAccount) this.transactionType_, getParentForChildren(), isClean());
                    this.transactionType_ = null;
                }
                this.transactionTypeCase_ = 7;
                onChanged();
                return this.createTokenAccountTransactionBuilder_;
            }

            private a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> getDeactivateStakeTransactionFieldBuilder() {
                if (this.deactivateStakeTransactionBuilder_ == null) {
                    if (this.transactionTypeCase_ != 5) {
                        this.transactionType_ = DeactivateStake.getDefaultInstance();
                    }
                    this.deactivateStakeTransactionBuilder_ = new a1<>((DeactivateStake) this.transactionType_, getParentForChildren(), isClean());
                    this.transactionType_ = null;
                }
                this.transactionTypeCase_ = 5;
                onChanged();
                return this.deactivateStakeTransactionBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_SigningInput_descriptor;
            }

            private a1<Stake, Stake.Builder, StakeOrBuilder> getStakeTransactionFieldBuilder() {
                if (this.stakeTransactionBuilder_ == null) {
                    if (this.transactionTypeCase_ != 4) {
                        this.transactionType_ = Stake.getDefaultInstance();
                    }
                    this.stakeTransactionBuilder_ = new a1<>((Stake) this.transactionType_, getParentForChildren(), isClean());
                    this.transactionType_ = null;
                }
                this.transactionTypeCase_ = 4;
                onChanged();
                return this.stakeTransactionBuilder_;
            }

            private a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> getTokenTransferTransactionFieldBuilder() {
                if (this.tokenTransferTransactionBuilder_ == null) {
                    if (this.transactionTypeCase_ != 8) {
                        this.transactionType_ = TokenTransfer.getDefaultInstance();
                    }
                    this.tokenTransferTransactionBuilder_ = new a1<>((TokenTransfer) this.transactionType_, getParentForChildren(), isClean());
                    this.transactionType_ = null;
                }
                this.transactionTypeCase_ = 8;
                onChanged();
                return this.tokenTransferTransactionBuilder_;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferTransactionFieldBuilder() {
                if (this.transferTransactionBuilder_ == null) {
                    if (this.transactionTypeCase_ != 3) {
                        this.transactionType_ = Transfer.getDefaultInstance();
                    }
                    this.transferTransactionBuilder_ = new a1<>((Transfer) this.transactionType_, getParentForChildren(), isClean());
                    this.transactionType_ = null;
                }
                this.transactionTypeCase_ = 3;
                onChanged();
                return this.transferTransactionBuilder_;
            }

            private a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> getWithdrawTransactionFieldBuilder() {
                if (this.withdrawTransactionBuilder_ == null) {
                    if (this.transactionTypeCase_ != 6) {
                        this.transactionType_ = WithdrawStake.getDefaultInstance();
                    }
                    this.withdrawTransactionBuilder_ = new a1<>((WithdrawStake) this.transactionType_, getParentForChildren(), isClean());
                    this.transactionType_ = null;
                }
                this.transactionTypeCase_ = 6;
                onChanged();
                return this.withdrawTransactionBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCreateAndTransferTokenTransaction() {
                a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> a1Var = this.createAndTransferTokenTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 9) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionTypeCase_ == 9) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearCreateTokenAccountTransaction() {
                a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> a1Var = this.createTokenAccountTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 7) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionTypeCase_ == 7) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearDeactivateStakeTransaction() {
                a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> a1Var = this.deactivateStakeTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 5) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionTypeCase_ == 5) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearRecentBlockhash() {
                this.recentBlockhash_ = SigningInput.getDefaultInstance().getRecentBlockhash();
                onChanged();
                return this;
            }

            public Builder clearStakeTransaction() {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 4) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionTypeCase_ == 4) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTokenTransferTransaction() {
                a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> a1Var = this.tokenTransferTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 8) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionTypeCase_ == 8) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransactionType() {
                this.transactionTypeCase_ = 0;
                this.transactionType_ = null;
                onChanged();
                return this;
            }

            public Builder clearTransferTransaction() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 3) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionTypeCase_ == 3) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearWithdrawTransaction() {
                a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> a1Var = this.withdrawTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 6) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionTypeCase_ == 6) {
                        this.transactionTypeCase_ = 0;
                        this.transactionType_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public CreateAndTransferToken getCreateAndTransferTokenTransaction() {
                a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> a1Var = this.createAndTransferTokenTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 9) {
                        return (CreateAndTransferToken) this.transactionType_;
                    }
                    return CreateAndTransferToken.getDefaultInstance();
                } else if (this.transactionTypeCase_ == 9) {
                    return a1Var.f();
                } else {
                    return CreateAndTransferToken.getDefaultInstance();
                }
            }

            public CreateAndTransferToken.Builder getCreateAndTransferTokenTransactionBuilder() {
                return getCreateAndTransferTokenTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public CreateAndTransferTokenOrBuilder getCreateAndTransferTokenTransactionOrBuilder() {
                a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> a1Var;
                int i = this.transactionTypeCase_;
                if (i != 9 || (a1Var = this.createAndTransferTokenTransactionBuilder_) == null) {
                    if (i == 9) {
                        return (CreateAndTransferToken) this.transactionType_;
                    }
                    return CreateAndTransferToken.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public CreateTokenAccount getCreateTokenAccountTransaction() {
                a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> a1Var = this.createTokenAccountTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 7) {
                        return (CreateTokenAccount) this.transactionType_;
                    }
                    return CreateTokenAccount.getDefaultInstance();
                } else if (this.transactionTypeCase_ == 7) {
                    return a1Var.f();
                } else {
                    return CreateTokenAccount.getDefaultInstance();
                }
            }

            public CreateTokenAccount.Builder getCreateTokenAccountTransactionBuilder() {
                return getCreateTokenAccountTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public CreateTokenAccountOrBuilder getCreateTokenAccountTransactionOrBuilder() {
                a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> a1Var;
                int i = this.transactionTypeCase_;
                if (i != 7 || (a1Var = this.createTokenAccountTransactionBuilder_) == null) {
                    if (i == 7) {
                        return (CreateTokenAccount) this.transactionType_;
                    }
                    return CreateTokenAccount.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public DeactivateStake getDeactivateStakeTransaction() {
                a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> a1Var = this.deactivateStakeTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 5) {
                        return (DeactivateStake) this.transactionType_;
                    }
                    return DeactivateStake.getDefaultInstance();
                } else if (this.transactionTypeCase_ == 5) {
                    return a1Var.f();
                } else {
                    return DeactivateStake.getDefaultInstance();
                }
            }

            public DeactivateStake.Builder getDeactivateStakeTransactionBuilder() {
                return getDeactivateStakeTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public DeactivateStakeOrBuilder getDeactivateStakeTransactionOrBuilder() {
                a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> a1Var;
                int i = this.transactionTypeCase_;
                if (i != 5 || (a1Var = this.deactivateStakeTransactionBuilder_) == null) {
                    if (i == 5) {
                        return (DeactivateStake) this.transactionType_;
                    }
                    return DeactivateStake.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public String getRecentBlockhash() {
                Object obj = this.recentBlockhash_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.recentBlockhash_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public ByteString getRecentBlockhashBytes() {
                Object obj = this.recentBlockhash_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.recentBlockhash_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public Stake getStakeTransaction() {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 4) {
                        return (Stake) this.transactionType_;
                    }
                    return Stake.getDefaultInstance();
                } else if (this.transactionTypeCase_ == 4) {
                    return a1Var.f();
                } else {
                    return Stake.getDefaultInstance();
                }
            }

            public Stake.Builder getStakeTransactionBuilder() {
                return getStakeTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public StakeOrBuilder getStakeTransactionOrBuilder() {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var;
                int i = this.transactionTypeCase_;
                if (i != 4 || (a1Var = this.stakeTransactionBuilder_) == null) {
                    if (i == 4) {
                        return (Stake) this.transactionType_;
                    }
                    return Stake.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public TokenTransfer getTokenTransferTransaction() {
                a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> a1Var = this.tokenTransferTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 8) {
                        return (TokenTransfer) this.transactionType_;
                    }
                    return TokenTransfer.getDefaultInstance();
                } else if (this.transactionTypeCase_ == 8) {
                    return a1Var.f();
                } else {
                    return TokenTransfer.getDefaultInstance();
                }
            }

            public TokenTransfer.Builder getTokenTransferTransactionBuilder() {
                return getTokenTransferTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public TokenTransferOrBuilder getTokenTransferTransactionOrBuilder() {
                a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> a1Var;
                int i = this.transactionTypeCase_;
                if (i != 8 || (a1Var = this.tokenTransferTransactionBuilder_) == null) {
                    if (i == 8) {
                        return (TokenTransfer) this.transactionType_;
                    }
                    return TokenTransfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public TransactionTypeCase getTransactionTypeCase() {
                return TransactionTypeCase.forNumber(this.transactionTypeCase_);
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public Transfer getTransferTransaction() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 3) {
                        return (Transfer) this.transactionType_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.transactionTypeCase_ == 3) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferTransactionBuilder() {
                return getTransferTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public TransferOrBuilder getTransferTransactionOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.transactionTypeCase_;
                if (i != 3 || (a1Var = this.transferTransactionBuilder_) == null) {
                    if (i == 3) {
                        return (Transfer) this.transactionType_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public WithdrawStake getWithdrawTransaction() {
                a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> a1Var = this.withdrawTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 6) {
                        return (WithdrawStake) this.transactionType_;
                    }
                    return WithdrawStake.getDefaultInstance();
                } else if (this.transactionTypeCase_ == 6) {
                    return a1Var.f();
                } else {
                    return WithdrawStake.getDefaultInstance();
                }
            }

            public WithdrawStake.Builder getWithdrawTransactionBuilder() {
                return getWithdrawTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public WithdrawStakeOrBuilder getWithdrawTransactionOrBuilder() {
                a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> a1Var;
                int i = this.transactionTypeCase_;
                if (i != 6 || (a1Var = this.withdrawTransactionBuilder_) == null) {
                    if (i == 6) {
                        return (WithdrawStake) this.transactionType_;
                    }
                    return WithdrawStake.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public boolean hasCreateAndTransferTokenTransaction() {
                return this.transactionTypeCase_ == 9;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public boolean hasCreateTokenAccountTransaction() {
                return this.transactionTypeCase_ == 7;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public boolean hasDeactivateStakeTransaction() {
                return this.transactionTypeCase_ == 5;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public boolean hasStakeTransaction() {
                return this.transactionTypeCase_ == 4;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public boolean hasTokenTransferTransaction() {
                return this.transactionTypeCase_ == 8;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public boolean hasTransferTransaction() {
                return this.transactionTypeCase_ == 3;
            }

            @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
            public boolean hasWithdrawTransaction() {
                return this.transactionTypeCase_ == 6;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCreateAndTransferTokenTransaction(CreateAndTransferToken createAndTransferToken) {
                a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> a1Var = this.createAndTransferTokenTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 9 && this.transactionType_ != CreateAndTransferToken.getDefaultInstance()) {
                        this.transactionType_ = CreateAndTransferToken.newBuilder((CreateAndTransferToken) this.transactionType_).mergeFrom(createAndTransferToken).buildPartial();
                    } else {
                        this.transactionType_ = createAndTransferToken;
                    }
                    onChanged();
                } else {
                    if (this.transactionTypeCase_ == 9) {
                        a1Var.h(createAndTransferToken);
                    }
                    this.createAndTransferTokenTransactionBuilder_.j(createAndTransferToken);
                }
                this.transactionTypeCase_ = 9;
                return this;
            }

            public Builder mergeCreateTokenAccountTransaction(CreateTokenAccount createTokenAccount) {
                a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> a1Var = this.createTokenAccountTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 7 && this.transactionType_ != CreateTokenAccount.getDefaultInstance()) {
                        this.transactionType_ = CreateTokenAccount.newBuilder((CreateTokenAccount) this.transactionType_).mergeFrom(createTokenAccount).buildPartial();
                    } else {
                        this.transactionType_ = createTokenAccount;
                    }
                    onChanged();
                } else {
                    if (this.transactionTypeCase_ == 7) {
                        a1Var.h(createTokenAccount);
                    }
                    this.createTokenAccountTransactionBuilder_.j(createTokenAccount);
                }
                this.transactionTypeCase_ = 7;
                return this;
            }

            public Builder mergeDeactivateStakeTransaction(DeactivateStake deactivateStake) {
                a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> a1Var = this.deactivateStakeTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 5 && this.transactionType_ != DeactivateStake.getDefaultInstance()) {
                        this.transactionType_ = DeactivateStake.newBuilder((DeactivateStake) this.transactionType_).mergeFrom(deactivateStake).buildPartial();
                    } else {
                        this.transactionType_ = deactivateStake;
                    }
                    onChanged();
                } else {
                    if (this.transactionTypeCase_ == 5) {
                        a1Var.h(deactivateStake);
                    }
                    this.deactivateStakeTransactionBuilder_.j(deactivateStake);
                }
                this.transactionTypeCase_ = 5;
                return this;
            }

            public Builder mergeStakeTransaction(Stake stake) {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 4 && this.transactionType_ != Stake.getDefaultInstance()) {
                        this.transactionType_ = Stake.newBuilder((Stake) this.transactionType_).mergeFrom(stake).buildPartial();
                    } else {
                        this.transactionType_ = stake;
                    }
                    onChanged();
                } else {
                    if (this.transactionTypeCase_ == 4) {
                        a1Var.h(stake);
                    }
                    this.stakeTransactionBuilder_.j(stake);
                }
                this.transactionTypeCase_ = 4;
                return this;
            }

            public Builder mergeTokenTransferTransaction(TokenTransfer tokenTransfer) {
                a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> a1Var = this.tokenTransferTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 8 && this.transactionType_ != TokenTransfer.getDefaultInstance()) {
                        this.transactionType_ = TokenTransfer.newBuilder((TokenTransfer) this.transactionType_).mergeFrom(tokenTransfer).buildPartial();
                    } else {
                        this.transactionType_ = tokenTransfer;
                    }
                    onChanged();
                } else {
                    if (this.transactionTypeCase_ == 8) {
                        a1Var.h(tokenTransfer);
                    }
                    this.tokenTransferTransactionBuilder_.j(tokenTransfer);
                }
                this.transactionTypeCase_ = 8;
                return this;
            }

            public Builder mergeTransferTransaction(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 3 && this.transactionType_ != Transfer.getDefaultInstance()) {
                        this.transactionType_ = Transfer.newBuilder((Transfer) this.transactionType_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.transactionType_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.transactionTypeCase_ == 3) {
                        a1Var.h(transfer);
                    }
                    this.transferTransactionBuilder_.j(transfer);
                }
                this.transactionTypeCase_ = 3;
                return this;
            }

            public Builder mergeWithdrawTransaction(WithdrawStake withdrawStake) {
                a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> a1Var = this.withdrawTransactionBuilder_;
                if (a1Var == null) {
                    if (this.transactionTypeCase_ == 6 && this.transactionType_ != WithdrawStake.getDefaultInstance()) {
                        this.transactionType_ = WithdrawStake.newBuilder((WithdrawStake) this.transactionType_).mergeFrom(withdrawStake).buildPartial();
                    } else {
                        this.transactionType_ = withdrawStake;
                    }
                    onChanged();
                } else {
                    if (this.transactionTypeCase_ == 6) {
                        a1Var.h(withdrawStake);
                    }
                    this.withdrawTransactionBuilder_.j(withdrawStake);
                }
                this.transactionTypeCase_ = 6;
                return this;
            }

            public Builder setCreateAndTransferTokenTransaction(CreateAndTransferToken createAndTransferToken) {
                a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> a1Var = this.createAndTransferTokenTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(createAndTransferToken);
                    this.transactionType_ = createAndTransferToken;
                    onChanged();
                } else {
                    a1Var.j(createAndTransferToken);
                }
                this.transactionTypeCase_ = 9;
                return this;
            }

            public Builder setCreateTokenAccountTransaction(CreateTokenAccount createTokenAccount) {
                a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> a1Var = this.createTokenAccountTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(createTokenAccount);
                    this.transactionType_ = createTokenAccount;
                    onChanged();
                } else {
                    a1Var.j(createTokenAccount);
                }
                this.transactionTypeCase_ = 7;
                return this;
            }

            public Builder setDeactivateStakeTransaction(DeactivateStake deactivateStake) {
                a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> a1Var = this.deactivateStakeTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(deactivateStake);
                    this.transactionType_ = deactivateStake;
                    onChanged();
                } else {
                    a1Var.j(deactivateStake);
                }
                this.transactionTypeCase_ = 5;
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setRecentBlockhash(String str) {
                Objects.requireNonNull(str);
                this.recentBlockhash_ = str;
                onChanged();
                return this;
            }

            public Builder setRecentBlockhashBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.recentBlockhash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setStakeTransaction(Stake stake) {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(stake);
                    this.transactionType_ = stake;
                    onChanged();
                } else {
                    a1Var.j(stake);
                }
                this.transactionTypeCase_ = 4;
                return this;
            }

            public Builder setTokenTransferTransaction(TokenTransfer tokenTransfer) {
                a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> a1Var = this.tokenTransferTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(tokenTransfer);
                    this.transactionType_ = tokenTransfer;
                    onChanged();
                } else {
                    a1Var.j(tokenTransfer);
                }
                this.transactionTypeCase_ = 8;
                return this;
            }

            public Builder setTransferTransaction(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.transactionType_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.transactionTypeCase_ = 3;
                return this;
            }

            public Builder setWithdrawTransaction(WithdrawStake withdrawStake) {
                a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> a1Var = this.withdrawTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(withdrawStake);
                    this.transactionType_ = withdrawStake;
                    onChanged();
                } else {
                    a1Var.j(withdrawStake);
                }
                this.transactionTypeCase_ = 6;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.transactionTypeCase_ = 0;
                this.privateKey_ = ByteString.EMPTY;
                this.recentBlockhash_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.privateKey_ = this.privateKey_;
                signingInput.recentBlockhash_ = this.recentBlockhash_;
                if (this.transactionTypeCase_ == 3) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferTransactionBuilder_;
                    if (a1Var == null) {
                        signingInput.transactionType_ = this.transactionType_;
                    } else {
                        signingInput.transactionType_ = a1Var.b();
                    }
                }
                if (this.transactionTypeCase_ == 4) {
                    a1<Stake, Stake.Builder, StakeOrBuilder> a1Var2 = this.stakeTransactionBuilder_;
                    if (a1Var2 == null) {
                        signingInput.transactionType_ = this.transactionType_;
                    } else {
                        signingInput.transactionType_ = a1Var2.b();
                    }
                }
                if (this.transactionTypeCase_ == 5) {
                    a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> a1Var3 = this.deactivateStakeTransactionBuilder_;
                    if (a1Var3 == null) {
                        signingInput.transactionType_ = this.transactionType_;
                    } else {
                        signingInput.transactionType_ = a1Var3.b();
                    }
                }
                if (this.transactionTypeCase_ == 6) {
                    a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> a1Var4 = this.withdrawTransactionBuilder_;
                    if (a1Var4 == null) {
                        signingInput.transactionType_ = this.transactionType_;
                    } else {
                        signingInput.transactionType_ = a1Var4.b();
                    }
                }
                if (this.transactionTypeCase_ == 7) {
                    a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> a1Var5 = this.createTokenAccountTransactionBuilder_;
                    if (a1Var5 == null) {
                        signingInput.transactionType_ = this.transactionType_;
                    } else {
                        signingInput.transactionType_ = a1Var5.b();
                    }
                }
                if (this.transactionTypeCase_ == 8) {
                    a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> a1Var6 = this.tokenTransferTransactionBuilder_;
                    if (a1Var6 == null) {
                        signingInput.transactionType_ = this.transactionType_;
                    } else {
                        signingInput.transactionType_ = a1Var6.b();
                    }
                }
                if (this.transactionTypeCase_ == 9) {
                    a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> a1Var7 = this.createAndTransferTokenTransactionBuilder_;
                    if (a1Var7 == null) {
                        signingInput.transactionType_ = this.transactionType_;
                    } else {
                        signingInput.transactionType_ = a1Var7.b();
                    }
                }
                signingInput.transactionTypeCase_ = this.transactionTypeCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.privateKey_ = ByteString.EMPTY;
                this.recentBlockhash_ = "";
                this.transactionTypeCase_ = 0;
                this.transactionType_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setCreateAndTransferTokenTransaction(CreateAndTransferToken.Builder builder) {
                a1<CreateAndTransferToken, CreateAndTransferToken.Builder, CreateAndTransferTokenOrBuilder> a1Var = this.createAndTransferTokenTransactionBuilder_;
                if (a1Var == null) {
                    this.transactionType_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionTypeCase_ = 9;
                return this;
            }

            public Builder setCreateTokenAccountTransaction(CreateTokenAccount.Builder builder) {
                a1<CreateTokenAccount, CreateTokenAccount.Builder, CreateTokenAccountOrBuilder> a1Var = this.createTokenAccountTransactionBuilder_;
                if (a1Var == null) {
                    this.transactionType_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionTypeCase_ = 7;
                return this;
            }

            public Builder setDeactivateStakeTransaction(DeactivateStake.Builder builder) {
                a1<DeactivateStake, DeactivateStake.Builder, DeactivateStakeOrBuilder> a1Var = this.deactivateStakeTransactionBuilder_;
                if (a1Var == null) {
                    this.transactionType_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionTypeCase_ = 5;
                return this;
            }

            public Builder setStakeTransaction(Stake.Builder builder) {
                a1<Stake, Stake.Builder, StakeOrBuilder> a1Var = this.stakeTransactionBuilder_;
                if (a1Var == null) {
                    this.transactionType_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionTypeCase_ = 4;
                return this;
            }

            public Builder setTokenTransferTransaction(TokenTransfer.Builder builder) {
                a1<TokenTransfer, TokenTransfer.Builder, TokenTransferOrBuilder> a1Var = this.tokenTransferTransactionBuilder_;
                if (a1Var == null) {
                    this.transactionType_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionTypeCase_ = 8;
                return this;
            }

            public Builder setTransferTransaction(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferTransactionBuilder_;
                if (a1Var == null) {
                    this.transactionType_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionTypeCase_ = 3;
                return this;
            }

            public Builder setWithdrawTransaction(WithdrawStake.Builder builder) {
                a1<WithdrawStake, WithdrawStake.Builder, WithdrawStakeOrBuilder> a1Var = this.withdrawTransactionBuilder_;
                if (a1Var == null) {
                    this.transactionType_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionTypeCase_ = 6;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.transactionTypeCase_ = 0;
                this.privateKey_ = ByteString.EMPTY;
                this.recentBlockhash_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (!signingInput.getRecentBlockhash().isEmpty()) {
                    this.recentBlockhash_ = signingInput.recentBlockhash_;
                    onChanged();
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Solana$SigningInput$TransactionTypeCase[signingInput.getTransactionTypeCase().ordinal()]) {
                    case 1:
                        mergeTransferTransaction(signingInput.getTransferTransaction());
                        break;
                    case 2:
                        mergeStakeTransaction(signingInput.getStakeTransaction());
                        break;
                    case 3:
                        mergeDeactivateStakeTransaction(signingInput.getDeactivateStakeTransaction());
                        break;
                    case 4:
                        mergeWithdrawTransaction(signingInput.getWithdrawTransaction());
                        break;
                    case 5:
                        mergeCreateTokenAccountTransaction(signingInput.getCreateTokenAccountTransaction());
                        break;
                    case 6:
                        mergeTokenTransferTransaction(signingInput.getTokenTransferTransaction());
                        break;
                    case 7:
                        mergeCreateAndTransferTokenTransaction(signingInput.getCreateAndTransferTokenTransaction());
                        break;
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.SigningInput.access$10900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$SigningInput r3 = (wallet.core.jni.proto.Solana.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$SigningInput r4 = (wallet.core.jni.proto.Solana.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum TransactionTypeCase implements a0.c {
            TRANSFER_TRANSACTION(3),
            STAKE_TRANSACTION(4),
            DEACTIVATE_STAKE_TRANSACTION(5),
            WITHDRAW_TRANSACTION(6),
            CREATE_TOKEN_ACCOUNT_TRANSACTION(7),
            TOKEN_TRANSFER_TRANSACTION(8),
            CREATE_AND_TRANSFER_TOKEN_TRANSACTION(9),
            TRANSACTIONTYPE_NOT_SET(0);
            
            private final int value;

            TransactionTypeCase(int i) {
                this.value = i;
            }

            public static TransactionTypeCase forNumber(int i) {
                if (i != 0) {
                    switch (i) {
                        case 3:
                            return TRANSFER_TRANSACTION;
                        case 4:
                            return STAKE_TRANSACTION;
                        case 5:
                            return DEACTIVATE_STAKE_TRANSACTION;
                        case 6:
                            return WITHDRAW_TRANSACTION;
                        case 7:
                            return CREATE_TOKEN_ACCOUNT_TRANSACTION;
                        case 8:
                            return TOKEN_TRANSFER_TRANSACTION;
                        case 9:
                            return CREATE_AND_TRANSFER_TOKEN_TRANSACTION;
                        default:
                            return null;
                    }
                }
                return TRANSACTIONTYPE_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static TransactionTypeCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getPrivateKey().equals(signingInput.getPrivateKey()) && getRecentBlockhash().equals(signingInput.getRecentBlockhash()) && getTransactionTypeCase().equals(signingInput.getTransactionTypeCase())) {
                switch (this.transactionTypeCase_) {
                    case 3:
                        if (!getTransferTransaction().equals(signingInput.getTransferTransaction())) {
                            return false;
                        }
                        break;
                    case 4:
                        if (!getStakeTransaction().equals(signingInput.getStakeTransaction())) {
                            return false;
                        }
                        break;
                    case 5:
                        if (!getDeactivateStakeTransaction().equals(signingInput.getDeactivateStakeTransaction())) {
                            return false;
                        }
                        break;
                    case 6:
                        if (!getWithdrawTransaction().equals(signingInput.getWithdrawTransaction())) {
                            return false;
                        }
                        break;
                    case 7:
                        if (!getCreateTokenAccountTransaction().equals(signingInput.getCreateTokenAccountTransaction())) {
                            return false;
                        }
                        break;
                    case 8:
                        if (!getTokenTransferTransaction().equals(signingInput.getTokenTransferTransaction())) {
                            return false;
                        }
                        break;
                    case 9:
                        if (!getCreateAndTransferTokenTransaction().equals(signingInput.getCreateAndTransferTokenTransaction())) {
                            return false;
                        }
                        break;
                }
                return this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public CreateAndTransferToken getCreateAndTransferTokenTransaction() {
            if (this.transactionTypeCase_ == 9) {
                return (CreateAndTransferToken) this.transactionType_;
            }
            return CreateAndTransferToken.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public CreateAndTransferTokenOrBuilder getCreateAndTransferTokenTransactionOrBuilder() {
            if (this.transactionTypeCase_ == 9) {
                return (CreateAndTransferToken) this.transactionType_;
            }
            return CreateAndTransferToken.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public CreateTokenAccount getCreateTokenAccountTransaction() {
            if (this.transactionTypeCase_ == 7) {
                return (CreateTokenAccount) this.transactionType_;
            }
            return CreateTokenAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public CreateTokenAccountOrBuilder getCreateTokenAccountTransactionOrBuilder() {
            if (this.transactionTypeCase_ == 7) {
                return (CreateTokenAccount) this.transactionType_;
            }
            return CreateTokenAccount.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public DeactivateStake getDeactivateStakeTransaction() {
            if (this.transactionTypeCase_ == 5) {
                return (DeactivateStake) this.transactionType_;
            }
            return DeactivateStake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public DeactivateStakeOrBuilder getDeactivateStakeTransactionOrBuilder() {
            if (this.transactionTypeCase_ == 5) {
                return (DeactivateStake) this.transactionType_;
            }
            return DeactivateStake.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public String getRecentBlockhash() {
            Object obj = this.recentBlockhash_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.recentBlockhash_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public ByteString getRecentBlockhashBytes() {
            Object obj = this.recentBlockhash_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.recentBlockhash_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.privateKey_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.privateKey_);
            if (!getRecentBlockhashBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(2, this.recentBlockhash_);
            }
            if (this.transactionTypeCase_ == 3) {
                h += CodedOutputStream.G(3, (Transfer) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 4) {
                h += CodedOutputStream.G(4, (Stake) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 5) {
                h += CodedOutputStream.G(5, (DeactivateStake) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 6) {
                h += CodedOutputStream.G(6, (WithdrawStake) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 7) {
                h += CodedOutputStream.G(7, (CreateTokenAccount) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 8) {
                h += CodedOutputStream.G(8, (TokenTransfer) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 9) {
                h += CodedOutputStream.G(9, (CreateAndTransferToken) this.transactionType_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public Stake getStakeTransaction() {
            if (this.transactionTypeCase_ == 4) {
                return (Stake) this.transactionType_;
            }
            return Stake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public StakeOrBuilder getStakeTransactionOrBuilder() {
            if (this.transactionTypeCase_ == 4) {
                return (Stake) this.transactionType_;
            }
            return Stake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public TokenTransfer getTokenTransferTransaction() {
            if (this.transactionTypeCase_ == 8) {
                return (TokenTransfer) this.transactionType_;
            }
            return TokenTransfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public TokenTransferOrBuilder getTokenTransferTransactionOrBuilder() {
            if (this.transactionTypeCase_ == 8) {
                return (TokenTransfer) this.transactionType_;
            }
            return TokenTransfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public TransactionTypeCase getTransactionTypeCase() {
            return TransactionTypeCase.forNumber(this.transactionTypeCase_);
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public Transfer getTransferTransaction() {
            if (this.transactionTypeCase_ == 3) {
                return (Transfer) this.transactionType_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public TransferOrBuilder getTransferTransactionOrBuilder() {
            if (this.transactionTypeCase_ == 3) {
                return (Transfer) this.transactionType_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public WithdrawStake getWithdrawTransaction() {
            if (this.transactionTypeCase_ == 6) {
                return (WithdrawStake) this.transactionType_;
            }
            return WithdrawStake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public WithdrawStakeOrBuilder getWithdrawTransactionOrBuilder() {
            if (this.transactionTypeCase_ == 6) {
                return (WithdrawStake) this.transactionType_;
            }
            return WithdrawStake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public boolean hasCreateAndTransferTokenTransaction() {
            return this.transactionTypeCase_ == 9;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public boolean hasCreateTokenAccountTransaction() {
            return this.transactionTypeCase_ == 7;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public boolean hasDeactivateStakeTransaction() {
            return this.transactionTypeCase_ == 5;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public boolean hasStakeTransaction() {
            return this.transactionTypeCase_ == 4;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public boolean hasTokenTransferTransaction() {
            return this.transactionTypeCase_ == 8;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public boolean hasTransferTransaction() {
            return this.transactionTypeCase_ == 3;
        }

        @Override // wallet.core.jni.proto.Solana.SigningInputOrBuilder
        public boolean hasWithdrawTransaction() {
            return this.transactionTypeCase_ == 6;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPrivateKey().hashCode()) * 37) + 2) * 53) + getRecentBlockhash().hashCode();
            switch (this.transactionTypeCase_) {
                case 3:
                    i = ((hashCode2 * 37) + 3) * 53;
                    hashCode = getTransferTransaction().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3;
                    return hashCode3;
                case 4:
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getStakeTransaction().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32;
                    return hashCode32;
                case 5:
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getDeactivateStakeTransaction().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322;
                    return hashCode322;
                case 6:
                    i = ((hashCode2 * 37) + 6) * 53;
                    hashCode = getWithdrawTransaction().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222;
                    return hashCode3222;
                case 7:
                    i = ((hashCode2 * 37) + 7) * 53;
                    hashCode = getCreateTokenAccountTransaction().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222;
                    return hashCode32222;
                case 8:
                    i = ((hashCode2 * 37) + 8) * 53;
                    hashCode = getTokenTransferTransaction().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222;
                    return hashCode322222;
                case 9:
                    i = ((hashCode2 * 37) + 9) * 53;
                    hashCode = getCreateAndTransferTokenTransaction().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222;
                    return hashCode3222222;
                default:
                    int hashCode32222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222222;
                    return hashCode32222222;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(1, this.privateKey_);
            }
            if (!getRecentBlockhashBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.recentBlockhash_);
            }
            if (this.transactionTypeCase_ == 3) {
                codedOutputStream.K0(3, (Transfer) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 4) {
                codedOutputStream.K0(4, (Stake) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 5) {
                codedOutputStream.K0(5, (DeactivateStake) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 6) {
                codedOutputStream.K0(6, (WithdrawStake) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 7) {
                codedOutputStream.K0(7, (CreateTokenAccount) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 8) {
                codedOutputStream.K0(8, (TokenTransfer) this.transactionType_);
            }
            if (this.transactionTypeCase_ == 9) {
                codedOutputStream.K0(9, (CreateAndTransferToken) this.transactionType_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.transactionTypeCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.transactionTypeCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.privateKey_ = ByteString.EMPTY;
            this.recentBlockhash_ = "";
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.privateKey_ = jVar.q();
                            } else if (J != 18) {
                                if (J == 26) {
                                    Transfer.Builder builder = this.transactionTypeCase_ == 3 ? ((Transfer) this.transactionType_).toBuilder() : null;
                                    m0 z2 = jVar.z(Transfer.parser(), rVar);
                                    this.transactionType_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((Transfer) z2);
                                        this.transactionType_ = builder.buildPartial();
                                    }
                                    this.transactionTypeCase_ = 3;
                                } else if (J == 34) {
                                    Stake.Builder builder2 = this.transactionTypeCase_ == 4 ? ((Stake) this.transactionType_).toBuilder() : null;
                                    m0 z3 = jVar.z(Stake.parser(), rVar);
                                    this.transactionType_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((Stake) z3);
                                        this.transactionType_ = builder2.buildPartial();
                                    }
                                    this.transactionTypeCase_ = 4;
                                } else if (J == 42) {
                                    DeactivateStake.Builder builder3 = this.transactionTypeCase_ == 5 ? ((DeactivateStake) this.transactionType_).toBuilder() : null;
                                    m0 z4 = jVar.z(DeactivateStake.parser(), rVar);
                                    this.transactionType_ = z4;
                                    if (builder3 != null) {
                                        builder3.mergeFrom((DeactivateStake) z4);
                                        this.transactionType_ = builder3.buildPartial();
                                    }
                                    this.transactionTypeCase_ = 5;
                                } else if (J == 50) {
                                    WithdrawStake.Builder builder4 = this.transactionTypeCase_ == 6 ? ((WithdrawStake) this.transactionType_).toBuilder() : null;
                                    m0 z5 = jVar.z(WithdrawStake.parser(), rVar);
                                    this.transactionType_ = z5;
                                    if (builder4 != null) {
                                        builder4.mergeFrom((WithdrawStake) z5);
                                        this.transactionType_ = builder4.buildPartial();
                                    }
                                    this.transactionTypeCase_ = 6;
                                } else if (J == 58) {
                                    CreateTokenAccount.Builder builder5 = this.transactionTypeCase_ == 7 ? ((CreateTokenAccount) this.transactionType_).toBuilder() : null;
                                    m0 z6 = jVar.z(CreateTokenAccount.parser(), rVar);
                                    this.transactionType_ = z6;
                                    if (builder5 != null) {
                                        builder5.mergeFrom((CreateTokenAccount) z6);
                                        this.transactionType_ = builder5.buildPartial();
                                    }
                                    this.transactionTypeCase_ = 7;
                                } else if (J == 66) {
                                    TokenTransfer.Builder builder6 = this.transactionTypeCase_ == 8 ? ((TokenTransfer) this.transactionType_).toBuilder() : null;
                                    m0 z7 = jVar.z(TokenTransfer.parser(), rVar);
                                    this.transactionType_ = z7;
                                    if (builder6 != null) {
                                        builder6.mergeFrom((TokenTransfer) z7);
                                        this.transactionType_ = builder6.buildPartial();
                                    }
                                    this.transactionTypeCase_ = 8;
                                } else if (J != 74) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    CreateAndTransferToken.Builder builder7 = this.transactionTypeCase_ == 9 ? ((CreateAndTransferToken) this.transactionType_).toBuilder() : null;
                                    m0 z8 = jVar.z(CreateAndTransferToken.parser(), rVar);
                                    this.transactionType_ = z8;
                                    if (builder7 != null) {
                                        builder7.mergeFrom((CreateAndTransferToken) z8);
                                        this.transactionType_ = builder7.buildPartial();
                                    }
                                    this.transactionTypeCase_ = 9;
                                }
                            } else {
                                this.recentBlockhash_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        CreateAndTransferToken getCreateAndTransferTokenTransaction();

        CreateAndTransferTokenOrBuilder getCreateAndTransferTokenTransactionOrBuilder();

        CreateTokenAccount getCreateTokenAccountTransaction();

        CreateTokenAccountOrBuilder getCreateTokenAccountTransactionOrBuilder();

        DeactivateStake getDeactivateStakeTransaction();

        DeactivateStakeOrBuilder getDeactivateStakeTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        String getRecentBlockhash();

        ByteString getRecentBlockhashBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Stake getStakeTransaction();

        StakeOrBuilder getStakeTransactionOrBuilder();

        TokenTransfer getTokenTransferTransaction();

        TokenTransferOrBuilder getTokenTransferTransactionOrBuilder();

        SigningInput.TransactionTypeCase getTransactionTypeCase();

        Transfer getTransferTransaction();

        TransferOrBuilder getTransferTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        WithdrawStake getWithdrawTransaction();

        WithdrawStakeOrBuilder getWithdrawTransactionOrBuilder();

        boolean hasCreateAndTransferTokenTransaction();

        boolean hasCreateTokenAccountTransaction();

        boolean hasDeactivateStakeTransaction();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasStakeTransaction();

        boolean hasTokenTransferTransaction();

        boolean hasTransferTransaction();

        boolean hasWithdrawTransaction();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object encoded_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Solana.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private Object encoded_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.SigningOutputOrBuilder
            public String getEncoded() {
                Object obj = this.encoded_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.encoded_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.SigningOutputOrBuilder
            public ByteString getEncodedBytes() {
                Object obj = this.encoded_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.encoded_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(String str) {
                Objects.requireNonNull(str);
                this.encoded_ = str;
                onChanged();
                return this;
            }

            public Builder setEncodedBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.encoded_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (!signingOutput.getEncoded().isEmpty()) {
                    this.encoded_ = signingOutput.encoded_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.SigningOutput.access$12000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$SigningOutput r3 = (wallet.core.jni.proto.Solana.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$SigningOutput r4 = (wallet.core.jni.proto.Solana.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Solana.SigningOutputOrBuilder
        public String getEncoded() {
            Object obj = this.encoded_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.encoded_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.SigningOutputOrBuilder
        public ByteString getEncodedBytes() {
            Object obj = this.encoded_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.encoded_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getEncodedBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.encoded_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getEncodedBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.encoded_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.encoded_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        String getEncoded();

        ByteString getEncodedBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Stake extends GeneratedMessageV3 implements StakeOrBuilder {
        private static final Stake DEFAULT_INSTANCE = new Stake();
        private static final t0<Stake> PARSER = new c<Stake>() { // from class: wallet.core.jni.proto.Solana.Stake.1
            @Override // com.google.protobuf.t0
            public Stake parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Stake(jVar, rVar, null);
            }
        };
        public static final int VALIDATOR_PUBKEY_FIELD_NUMBER = 1;
        public static final int VALUE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object validatorPubkey_;
        private long value_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements StakeOrBuilder {
            private Object validatorPubkey_;
            private long value_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_Stake_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearValidatorPubkey() {
                this.validatorPubkey_ = Stake.getDefaultInstance().getValidatorPubkey();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = 0L;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_Stake_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.StakeOrBuilder
            public String getValidatorPubkey() {
                Object obj = this.validatorPubkey_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.validatorPubkey_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.StakeOrBuilder
            public ByteString getValidatorPubkeyBytes() {
                Object obj = this.validatorPubkey_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorPubkey_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.StakeOrBuilder
            public long getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_Stake_fieldAccessorTable.d(Stake.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setValidatorPubkey(String str) {
                Objects.requireNonNull(str);
                this.validatorPubkey_ = str;
                onChanged();
                return this;
            }

            public Builder setValidatorPubkeyBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.validatorPubkey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(long j) {
                this.value_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.validatorPubkey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Stake build() {
                Stake buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Stake buildPartial() {
                Stake stake = new Stake(this, (AnonymousClass1) null);
                stake.validatorPubkey_ = this.validatorPubkey_;
                stake.value_ = this.value_;
                onBuilt();
                return stake;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Stake getDefaultInstanceForType() {
                return Stake.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.validatorPubkey_ = "";
                this.value_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.validatorPubkey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Stake) {
                    return mergeFrom((Stake) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Stake stake) {
                if (stake == Stake.getDefaultInstance()) {
                    return this;
                }
                if (!stake.getValidatorPubkey().isEmpty()) {
                    this.validatorPubkey_ = stake.validatorPubkey_;
                    onChanged();
                }
                if (stake.getValue() != 0) {
                    setValue(stake.getValue());
                }
                mergeUnknownFields(stake.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.Stake.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.Stake.access$2100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$Stake r3 = (wallet.core.jni.proto.Solana.Stake) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$Stake r4 = (wallet.core.jni.proto.Solana.Stake) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.Stake.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$Stake$Builder");
            }
        }

        public /* synthetic */ Stake(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Stake getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_Stake_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Stake parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Stake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Stake parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Stake> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Stake)) {
                return super.equals(obj);
            }
            Stake stake = (Stake) obj;
            return getValidatorPubkey().equals(stake.getValidatorPubkey()) && getValue() == stake.getValue() && this.unknownFields.equals(stake.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Stake> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getValidatorPubkeyBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.validatorPubkey_);
            long j = this.value_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(2, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Solana.StakeOrBuilder
        public String getValidatorPubkey() {
            Object obj = this.validatorPubkey_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.validatorPubkey_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.StakeOrBuilder
        public ByteString getValidatorPubkeyBytes() {
            Object obj = this.validatorPubkey_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.validatorPubkey_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Solana.StakeOrBuilder
        public long getValue() {
            return this.value_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValidatorPubkey().hashCode()) * 37) + 2) * 53) + a0.h(getValue())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_Stake_fieldAccessorTable.d(Stake.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Stake();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getValidatorPubkeyBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.validatorPubkey_);
            }
            long j = this.value_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Stake(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Stake stake) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(stake);
        }

        public static Stake parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Stake(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Stake parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Stake parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Stake getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Stake parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Stake() {
            this.memoizedIsInitialized = (byte) -1;
            this.validatorPubkey_ = "";
        }

        public static Stake parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Stake parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Stake parseFrom(InputStream inputStream) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Stake(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.validatorPubkey_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.value_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Stake parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Stake parseFrom(j jVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Stake parseFrom(j jVar, r rVar) throws IOException {
            return (Stake) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface StakeOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValidatorPubkey();

        ByteString getValidatorPubkeyBytes();

        long getValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TokenTransfer extends GeneratedMessageV3 implements TokenTransferOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 4;
        public static final int DECIMALS_FIELD_NUMBER = 5;
        private static final TokenTransfer DEFAULT_INSTANCE = new TokenTransfer();
        private static final t0<TokenTransfer> PARSER = new c<TokenTransfer>() { // from class: wallet.core.jni.proto.Solana.TokenTransfer.1
            @Override // com.google.protobuf.t0
            public TokenTransfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TokenTransfer(jVar, rVar, null);
            }
        };
        public static final int RECIPIENT_TOKEN_ADDRESS_FIELD_NUMBER = 3;
        public static final int SENDER_TOKEN_ADDRESS_FIELD_NUMBER = 2;
        public static final int TOKEN_MINT_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long amount_;
        private int decimals_;
        private byte memoizedIsInitialized;
        private volatile Object recipientTokenAddress_;
        private volatile Object senderTokenAddress_;
        private volatile Object tokenMintAddress_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TokenTransferOrBuilder {
            private long amount_;
            private int decimals_;
            private Object recipientTokenAddress_;
            private Object senderTokenAddress_;
            private Object tokenMintAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_TokenTransfer_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearDecimals() {
                this.decimals_ = 0;
                onChanged();
                return this;
            }

            public Builder clearRecipientTokenAddress() {
                this.recipientTokenAddress_ = TokenTransfer.getDefaultInstance().getRecipientTokenAddress();
                onChanged();
                return this;
            }

            public Builder clearSenderTokenAddress() {
                this.senderTokenAddress_ = TokenTransfer.getDefaultInstance().getSenderTokenAddress();
                onChanged();
                return this;
            }

            public Builder clearTokenMintAddress() {
                this.tokenMintAddress_ = TokenTransfer.getDefaultInstance().getTokenMintAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public int getDecimals() {
                return this.decimals_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_TokenTransfer_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public String getRecipientTokenAddress() {
                Object obj = this.recipientTokenAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.recipientTokenAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public ByteString getRecipientTokenAddressBytes() {
                Object obj = this.recipientTokenAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.recipientTokenAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public String getSenderTokenAddress() {
                Object obj = this.senderTokenAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.senderTokenAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public ByteString getSenderTokenAddressBytes() {
                Object obj = this.senderTokenAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.senderTokenAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public String getTokenMintAddress() {
                Object obj = this.tokenMintAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.tokenMintAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
            public ByteString getTokenMintAddressBytes() {
                Object obj = this.tokenMintAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.tokenMintAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_TokenTransfer_fieldAccessorTable.d(TokenTransfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setDecimals(int i) {
                this.decimals_ = i;
                onChanged();
                return this;
            }

            public Builder setRecipientTokenAddress(String str) {
                Objects.requireNonNull(str);
                this.recipientTokenAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setRecipientTokenAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.recipientTokenAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSenderTokenAddress(String str) {
                Objects.requireNonNull(str);
                this.senderTokenAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setSenderTokenAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.senderTokenAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTokenMintAddress(String str) {
                Objects.requireNonNull(str);
                this.tokenMintAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setTokenMintAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.tokenMintAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.tokenMintAddress_ = "";
                this.senderTokenAddress_ = "";
                this.recipientTokenAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TokenTransfer build() {
                TokenTransfer buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TokenTransfer buildPartial() {
                TokenTransfer tokenTransfer = new TokenTransfer(this, (AnonymousClass1) null);
                tokenTransfer.tokenMintAddress_ = this.tokenMintAddress_;
                tokenTransfer.senderTokenAddress_ = this.senderTokenAddress_;
                tokenTransfer.recipientTokenAddress_ = this.recipientTokenAddress_;
                tokenTransfer.amount_ = this.amount_;
                tokenTransfer.decimals_ = this.decimals_;
                onBuilt();
                return tokenTransfer;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TokenTransfer getDefaultInstanceForType() {
                return TokenTransfer.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.tokenMintAddress_ = "";
                this.senderTokenAddress_ = "";
                this.recipientTokenAddress_ = "";
                this.amount_ = 0L;
                this.decimals_ = 0;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TokenTransfer) {
                    return mergeFrom((TokenTransfer) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.tokenMintAddress_ = "";
                this.senderTokenAddress_ = "";
                this.recipientTokenAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TokenTransfer tokenTransfer) {
                if (tokenTransfer == TokenTransfer.getDefaultInstance()) {
                    return this;
                }
                if (!tokenTransfer.getTokenMintAddress().isEmpty()) {
                    this.tokenMintAddress_ = tokenTransfer.tokenMintAddress_;
                    onChanged();
                }
                if (!tokenTransfer.getSenderTokenAddress().isEmpty()) {
                    this.senderTokenAddress_ = tokenTransfer.senderTokenAddress_;
                    onChanged();
                }
                if (!tokenTransfer.getRecipientTokenAddress().isEmpty()) {
                    this.recipientTokenAddress_ = tokenTransfer.recipientTokenAddress_;
                    onChanged();
                }
                if (tokenTransfer.getAmount() != 0) {
                    setAmount(tokenTransfer.getAmount());
                }
                if (tokenTransfer.getDecimals() != 0) {
                    setDecimals(tokenTransfer.getDecimals());
                }
                mergeUnknownFields(tokenTransfer.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.TokenTransfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.TokenTransfer.access$7400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$TokenTransfer r3 = (wallet.core.jni.proto.Solana.TokenTransfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$TokenTransfer r4 = (wallet.core.jni.proto.Solana.TokenTransfer) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.TokenTransfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$TokenTransfer$Builder");
            }
        }

        public /* synthetic */ TokenTransfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TokenTransfer getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_TokenTransfer_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TokenTransfer parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TokenTransfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TokenTransfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TokenTransfer> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TokenTransfer)) {
                return super.equals(obj);
            }
            TokenTransfer tokenTransfer = (TokenTransfer) obj;
            return getTokenMintAddress().equals(tokenTransfer.getTokenMintAddress()) && getSenderTokenAddress().equals(tokenTransfer.getSenderTokenAddress()) && getRecipientTokenAddress().equals(tokenTransfer.getRecipientTokenAddress()) && getAmount() == tokenTransfer.getAmount() && getDecimals() == tokenTransfer.getDecimals() && this.unknownFields.equals(tokenTransfer.unknownFields);
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public int getDecimals() {
            return this.decimals_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TokenTransfer> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public String getRecipientTokenAddress() {
            Object obj = this.recipientTokenAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.recipientTokenAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public ByteString getRecipientTokenAddressBytes() {
            Object obj = this.recipientTokenAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.recipientTokenAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public String getSenderTokenAddress() {
            Object obj = this.senderTokenAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.senderTokenAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public ByteString getSenderTokenAddressBytes() {
            Object obj = this.senderTokenAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.senderTokenAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getTokenMintAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.tokenMintAddress_);
            if (!getSenderTokenAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.senderTokenAddress_);
            }
            if (!getRecipientTokenAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.recipientTokenAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(4, j);
            }
            int i2 = this.decimals_;
            if (i2 != 0) {
                computeStringSize += CodedOutputStream.Y(5, i2);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public String getTokenMintAddress() {
            Object obj = this.tokenMintAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.tokenMintAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.TokenTransferOrBuilder
        public ByteString getTokenMintAddressBytes() {
            Object obj = this.tokenMintAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.tokenMintAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getTokenMintAddress().hashCode()) * 37) + 2) * 53) + getSenderTokenAddress().hashCode()) * 37) + 3) * 53) + getRecipientTokenAddress().hashCode()) * 37) + 4) * 53) + a0.h(getAmount())) * 37) + 5) * 53) + getDecimals()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_TokenTransfer_fieldAccessorTable.d(TokenTransfer.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TokenTransfer();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getTokenMintAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.tokenMintAddress_);
            }
            if (!getSenderTokenAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.senderTokenAddress_);
            }
            if (!getRecipientTokenAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.recipientTokenAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.d1(4, j);
            }
            int i = this.decimals_;
            if (i != 0) {
                codedOutputStream.b1(5, i);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TokenTransfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TokenTransfer tokenTransfer) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(tokenTransfer);
        }

        public static TokenTransfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TokenTransfer(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TokenTransfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TokenTransfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TokenTransfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TokenTransfer getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TokenTransfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TokenTransfer() {
            this.memoizedIsInitialized = (byte) -1;
            this.tokenMintAddress_ = "";
            this.senderTokenAddress_ = "";
            this.recipientTokenAddress_ = "";
        }

        public static TokenTransfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TokenTransfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TokenTransfer parseFrom(InputStream inputStream) throws IOException {
            return (TokenTransfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TokenTransfer parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TokenTransfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TokenTransfer(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.tokenMintAddress_ = jVar.I();
                                } else if (J == 18) {
                                    this.senderTokenAddress_ = jVar.I();
                                } else if (J == 26) {
                                    this.recipientTokenAddress_ = jVar.I();
                                } else if (J == 32) {
                                    this.amount_ = jVar.L();
                                } else if (J != 40) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.decimals_ = jVar.K();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TokenTransfer parseFrom(j jVar) throws IOException {
            return (TokenTransfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TokenTransfer parseFrom(j jVar, r rVar) throws IOException {
            return (TokenTransfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TokenTransferOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        int getDecimals();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getRecipientTokenAddress();

        ByteString getRecipientTokenAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSenderTokenAddress();

        ByteString getSenderTokenAddressBytes();

        String getTokenMintAddress();

        ByteString getTokenMintAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transfer extends GeneratedMessageV3 implements TransferOrBuilder {
        private static final Transfer DEFAULT_INSTANCE = new Transfer();
        private static final t0<Transfer> PARSER = new c<Transfer>() { // from class: wallet.core.jni.proto.Solana.Transfer.1
            @Override // com.google.protobuf.t0
            public Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transfer(jVar, rVar, null);
            }
        };
        public static final int RECIPIENT_FIELD_NUMBER = 1;
        public static final int VALUE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object recipient_;
        private long value_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOrBuilder {
            private Object recipient_;
            private long value_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_Transfer_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearRecipient() {
                this.recipient_ = Transfer.getDefaultInstance().getRecipient();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = 0L;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_Transfer_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.TransferOrBuilder
            public String getRecipient() {
                Object obj = this.recipient_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.recipient_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.TransferOrBuilder
            public ByteString getRecipientBytes() {
                Object obj = this.recipient_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.recipient_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.TransferOrBuilder
            public long getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setRecipient(String str) {
                Objects.requireNonNull(str);
                this.recipient_ = str;
                onChanged();
                return this;
            }

            public Builder setRecipientBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.recipient_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(long j) {
                this.value_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.recipient_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transfer build() {
                Transfer buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transfer buildPartial() {
                Transfer transfer = new Transfer(this, (AnonymousClass1) null);
                transfer.recipient_ = this.recipient_;
                transfer.value_ = this.value_;
                onBuilt();
                return transfer;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transfer getDefaultInstanceForType() {
                return Transfer.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.recipient_ = "";
                this.value_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.recipient_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transfer) {
                    return mergeFrom((Transfer) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Transfer transfer) {
                if (transfer == Transfer.getDefaultInstance()) {
                    return this;
                }
                if (!transfer.getRecipient().isEmpty()) {
                    this.recipient_ = transfer.recipient_;
                    onChanged();
                }
                if (transfer.getValue() != 0) {
                    setValue(transfer.getValue());
                }
                mergeUnknownFields(transfer.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.Transfer.access$900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$Transfer r3 = (wallet.core.jni.proto.Solana.Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$Transfer r4 = (wallet.core.jni.proto.Solana.Transfer) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$Transfer$Builder");
            }
        }

        public /* synthetic */ Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Transfer getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_Transfer_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transfer> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transfer)) {
                return super.equals(obj);
            }
            Transfer transfer = (Transfer) obj;
            return getRecipient().equals(transfer.getRecipient()) && getValue() == transfer.getValue() && this.unknownFields.equals(transfer.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transfer> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Solana.TransferOrBuilder
        public String getRecipient() {
            Object obj = this.recipient_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.recipient_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.TransferOrBuilder
        public ByteString getRecipientBytes() {
            Object obj = this.recipient_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.recipient_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getRecipientBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.recipient_);
            long j = this.value_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(2, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Solana.TransferOrBuilder
        public long getValue() {
            return this.value_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getRecipient().hashCode()) * 37) + 2) * 53) + a0.h(getValue())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transfer();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getRecipientBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.recipient_);
            }
            long j = this.value_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Transfer transfer) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transfer);
        }

        public static Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transfer(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transfer getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Transfer() {
            this.memoizedIsInitialized = (byte) -1;
            this.recipient_ = "";
        }

        public static Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transfer parseFrom(InputStream inputStream) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.recipient_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.value_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transfer parseFrom(j jVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transfer parseFrom(j jVar, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransferOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getRecipient();

        ByteString getRecipientBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        long getValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class WithdrawStake extends GeneratedMessageV3 implements WithdrawStakeOrBuilder {
        private static final WithdrawStake DEFAULT_INSTANCE = new WithdrawStake();
        private static final t0<WithdrawStake> PARSER = new c<WithdrawStake>() { // from class: wallet.core.jni.proto.Solana.WithdrawStake.1
            @Override // com.google.protobuf.t0
            public WithdrawStake parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new WithdrawStake(jVar, rVar, null);
            }
        };
        public static final int VALIDATOR_PUBKEY_FIELD_NUMBER = 1;
        public static final int VALUE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private volatile Object validatorPubkey_;
        private long value_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements WithdrawStakeOrBuilder {
            private Object validatorPubkey_;
            private long value_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Solana.internal_static_TW_Solana_Proto_WithdrawStake_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearValidatorPubkey() {
                this.validatorPubkey_ = WithdrawStake.getDefaultInstance().getValidatorPubkey();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = 0L;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Solana.internal_static_TW_Solana_Proto_WithdrawStake_descriptor;
            }

            @Override // wallet.core.jni.proto.Solana.WithdrawStakeOrBuilder
            public String getValidatorPubkey() {
                Object obj = this.validatorPubkey_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.validatorPubkey_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Solana.WithdrawStakeOrBuilder
            public ByteString getValidatorPubkeyBytes() {
                Object obj = this.validatorPubkey_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorPubkey_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Solana.WithdrawStakeOrBuilder
            public long getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Solana.internal_static_TW_Solana_Proto_WithdrawStake_fieldAccessorTable.d(WithdrawStake.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setValidatorPubkey(String str) {
                Objects.requireNonNull(str);
                this.validatorPubkey_ = str;
                onChanged();
                return this;
            }

            public Builder setValidatorPubkeyBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.validatorPubkey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(long j) {
                this.value_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.validatorPubkey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public WithdrawStake build() {
                WithdrawStake buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public WithdrawStake buildPartial() {
                WithdrawStake withdrawStake = new WithdrawStake(this, (AnonymousClass1) null);
                withdrawStake.validatorPubkey_ = this.validatorPubkey_;
                withdrawStake.value_ = this.value_;
                onBuilt();
                return withdrawStake;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public WithdrawStake getDefaultInstanceForType() {
                return WithdrawStake.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.validatorPubkey_ = "";
                this.value_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.validatorPubkey_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof WithdrawStake) {
                    return mergeFrom((WithdrawStake) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(WithdrawStake withdrawStake) {
                if (withdrawStake == WithdrawStake.getDefaultInstance()) {
                    return this;
                }
                if (!withdrawStake.getValidatorPubkey().isEmpty()) {
                    this.validatorPubkey_ = withdrawStake.validatorPubkey_;
                    onChanged();
                }
                if (withdrawStake.getValue() != 0) {
                    setValue(withdrawStake.getValue());
                }
                mergeUnknownFields(withdrawStake.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Solana.WithdrawStake.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Solana.WithdrawStake.access$4400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Solana$WithdrawStake r3 = (wallet.core.jni.proto.Solana.WithdrawStake) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Solana$WithdrawStake r4 = (wallet.core.jni.proto.Solana.WithdrawStake) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Solana.WithdrawStake.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Solana$WithdrawStake$Builder");
            }
        }

        public /* synthetic */ WithdrawStake(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static WithdrawStake getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Solana.internal_static_TW_Solana_Proto_WithdrawStake_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static WithdrawStake parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (WithdrawStake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static WithdrawStake parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<WithdrawStake> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof WithdrawStake)) {
                return super.equals(obj);
            }
            WithdrawStake withdrawStake = (WithdrawStake) obj;
            return getValidatorPubkey().equals(withdrawStake.getValidatorPubkey()) && getValue() == withdrawStake.getValue() && this.unknownFields.equals(withdrawStake.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<WithdrawStake> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getValidatorPubkeyBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.validatorPubkey_);
            long j = this.value_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(2, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Solana.WithdrawStakeOrBuilder
        public String getValidatorPubkey() {
            Object obj = this.validatorPubkey_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.validatorPubkey_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Solana.WithdrawStakeOrBuilder
        public ByteString getValidatorPubkeyBytes() {
            Object obj = this.validatorPubkey_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.validatorPubkey_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Solana.WithdrawStakeOrBuilder
        public long getValue() {
            return this.value_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValidatorPubkey().hashCode()) * 37) + 2) * 53) + a0.h(getValue())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Solana.internal_static_TW_Solana_Proto_WithdrawStake_fieldAccessorTable.d(WithdrawStake.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new WithdrawStake();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getValidatorPubkeyBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.validatorPubkey_);
            }
            long j = this.value_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ WithdrawStake(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(WithdrawStake withdrawStake) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(withdrawStake);
        }

        public static WithdrawStake parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private WithdrawStake(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static WithdrawStake parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (WithdrawStake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static WithdrawStake parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public WithdrawStake getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static WithdrawStake parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private WithdrawStake() {
            this.memoizedIsInitialized = (byte) -1;
            this.validatorPubkey_ = "";
        }

        public static WithdrawStake parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static WithdrawStake parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static WithdrawStake parseFrom(InputStream inputStream) throws IOException {
            return (WithdrawStake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private WithdrawStake(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.validatorPubkey_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.value_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static WithdrawStake parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (WithdrawStake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static WithdrawStake parseFrom(j jVar) throws IOException {
            return (WithdrawStake) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static WithdrawStake parseFrom(j jVar, r rVar) throws IOException {
            return (WithdrawStake) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface WithdrawStakeOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getValidatorPubkey();

        ByteString getValidatorPubkeyBytes();

        long getValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Solana_Proto_Transfer_descriptor = bVar;
        internal_static_TW_Solana_Proto_Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Recipient", "Value"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Solana_Proto_Stake_descriptor = bVar2;
        internal_static_TW_Solana_Proto_Stake_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"ValidatorPubkey", "Value"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Solana_Proto_DeactivateStake_descriptor = bVar3;
        internal_static_TW_Solana_Proto_DeactivateStake_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"ValidatorPubkey"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Solana_Proto_WithdrawStake_descriptor = bVar4;
        internal_static_TW_Solana_Proto_WithdrawStake_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"ValidatorPubkey", "Value"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_Solana_Proto_CreateTokenAccount_descriptor = bVar5;
        internal_static_TW_Solana_Proto_CreateTokenAccount_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"MainAddress", "TokenMintAddress", "TokenAddress"});
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_Solana_Proto_TokenTransfer_descriptor = bVar6;
        internal_static_TW_Solana_Proto_TokenTransfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"TokenMintAddress", "SenderTokenAddress", "RecipientTokenAddress", "Amount", "Decimals"});
        Descriptors.b bVar7 = getDescriptor().o().get(6);
        internal_static_TW_Solana_Proto_CreateAndTransferToken_descriptor = bVar7;
        internal_static_TW_Solana_Proto_CreateAndTransferToken_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"RecipientMainAddress", "TokenMintAddress", "RecipientTokenAddress", "SenderTokenAddress", "Amount", "Decimals"});
        Descriptors.b bVar8 = getDescriptor().o().get(7);
        internal_static_TW_Solana_Proto_SigningInput_descriptor = bVar8;
        internal_static_TW_Solana_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"PrivateKey", "RecentBlockhash", "TransferTransaction", "StakeTransaction", "DeactivateStakeTransaction", "WithdrawTransaction", "CreateTokenAccountTransaction", "TokenTransferTransaction", "CreateAndTransferTokenTransaction", "TransactionType"});
        Descriptors.b bVar9 = getDescriptor().o().get(8);
        internal_static_TW_Solana_Proto_SigningOutput_descriptor = bVar9;
        internal_static_TW_Solana_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"Encoded"});
    }

    private Solana() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
