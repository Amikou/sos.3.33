package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.v0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Cosmos {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\fCosmos.proto\u0012\u000fTW.Cosmos.Proto\"'\n\u0006Amount\u0012\r\n\u0005denom\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\u0003\"<\n\u0003Fee\u0012(\n\u0007amounts\u0018\u0001 \u0003(\u000b2\u0017.TW.Cosmos.Proto.Amount\u0012\u000b\n\u0003gas\u0018\u0002 \u0001(\u0004\"à\b\n\u0007Message\u0012;\n\u0012send_coins_message\u0018\u0001 \u0001(\u000b2\u001d.TW.Cosmos.Proto.Message.SendH\u0000\u0012:\n\rstake_message\u0018\u0002 \u0001(\u000b2!.TW.Cosmos.Proto.Message.DelegateH\u0000\u0012>\n\u000funstake_message\u0018\u0003 \u0001(\u000b2#.TW.Cosmos.Proto.Message.UndelegateH\u0000\u0012C\n\u000frestake_message\u0018\u0004 \u0001(\u000b2(.TW.Cosmos.Proto.Message.BeginRedelegateH\u0000\u0012Z\n\u001dwithdraw_stake_reward_message\u0018\u0005 \u0001(\u000b21.TW.Cosmos.Proto.Message.WithdrawDelegationRewardH\u0000\u0012<\n\u0010raw_json_message\u0018\u0006 \u0001(\u000b2 .TW.Cosmos.Proto.Message.RawJSONH\u0000\u001ao\n\u0004Send\u0012\u0014\n\ffrom_address\u0018\u0001 \u0001(\t\u0012\u0012\n\nto_address\u0018\u0002 \u0001(\t\u0012(\n\u0007amounts\u0018\u0003 \u0003(\u000b2\u0017.TW.Cosmos.Proto.Amount\u0012\u0013\n\u000btype_prefix\u0018\u0004 \u0001(\t\u001a~\n\bDelegate\u0012\u0019\n\u0011delegator_address\u0018\u0001 \u0001(\t\u0012\u0019\n\u0011validator_address\u0018\u0002 \u0001(\t\u0012'\n\u0006amount\u0018\u0003 \u0001(\u000b2\u0017.TW.Cosmos.Proto.Amount\u0012\u0013\n\u000btype_prefix\u0018\u0004 \u0001(\t\u001a\u0080\u0001\n\nUndelegate\u0012\u0019\n\u0011delegator_address\u0018\u0001 \u0001(\t\u0012\u0019\n\u0011validator_address\u0018\u0002 \u0001(\t\u0012'\n\u0006amount\u0018\u0003 \u0001(\u000b2\u0017.TW.Cosmos.Proto.Amount\u0012\u0013\n\u000btype_prefix\u0018\u0004 \u0001(\t\u001a¨\u0001\n\u000fBeginRedelegate\u0012\u0019\n\u0011delegator_address\u0018\u0001 \u0001(\t\u0012\u001d\n\u0015validator_src_address\u0018\u0002 \u0001(\t\u0012\u001d\n\u0015validator_dst_address\u0018\u0003 \u0001(\t\u0012'\n\u0006amount\u0018\u0004 \u0001(\u000b2\u0017.TW.Cosmos.Proto.Amount\u0012\u0013\n\u000btype_prefix\u0018\u0005 \u0001(\t\u001ae\n\u0018WithdrawDelegationReward\u0012\u0019\n\u0011delegator_address\u0018\u0001 \u0001(\t\u0012\u0019\n\u0011validator_address\u0018\u0002 \u0001(\t\u0012\u0013\n\u000btype_prefix\u0018\u0003 \u0001(\t\u001a&\n\u0007RawJSON\u0012\f\n\u0004type\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\tB\u000f\n\rmessage_oneof\"ê\u0001\n\fSigningInput\u0012\u0016\n\u000eaccount_number\u0018\u0001 \u0001(\u0004\u0012\u0010\n\bchain_id\u0018\u0002 \u0001(\t\u0012!\n\u0003fee\u0018\u0003 \u0001(\u000b2\u0014.TW.Cosmos.Proto.Fee\u0012\f\n\u0004memo\u0018\u0004 \u0001(\t\u0012\u0010\n\bsequence\u0018\u0005 \u0001(\u0004\u0012\u0013\n\u000bprivate_key\u0018\u0006 \u0001(\f\u0012*\n\bmessages\u0018\u0007 \u0003(\u000b2\u0018.TW.Cosmos.Proto.Message\u0012,\n\u0004mode\u0018\b \u0001(\u000e2\u001e.TW.Cosmos.Proto.BroadcastMode\"0\n\rSigningOutput\u0012\u0011\n\tsignature\u0018\u0001 \u0001(\f\u0012\f\n\u0004json\u0018\u0002 \u0001(\t*/\n\rBroadcastMode\u0012\t\n\u0005BLOCK\u0010\u0000\u0012\b\n\u0004SYNC\u0010\u0001\u0012\t\n\u0005ASYNC\u0010\u0002B\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Amount_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Amount_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Fee_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Fee_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Message_Delegate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Message_Delegate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Message_RawJSON_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Message_RawJSON_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Message_Send_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Message_Send_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Message_Undelegate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Message_Undelegate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_Message_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_Message_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Cosmos_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Cosmos_Proto_SigningOutput_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Cosmos$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase;

        static {
            int[] iArr = new int[Message.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase = iArr;
            try {
                iArr[Message.MessageOneofCase.SEND_COINS_MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase[Message.MessageOneofCase.STAKE_MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase[Message.MessageOneofCase.UNSTAKE_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase[Message.MessageOneofCase.RESTAKE_MESSAGE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase[Message.MessageOneofCase.WITHDRAW_STAKE_REWARD_MESSAGE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase[Message.MessageOneofCase.RAW_JSON_MESSAGE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase[Message.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class Amount extends GeneratedMessageV3 implements AmountOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 2;
        public static final int DENOM_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object denom_;
        private byte memoizedIsInitialized;
        private static final Amount DEFAULT_INSTANCE = new Amount();
        private static final t0<Amount> PARSER = new c<Amount>() { // from class: wallet.core.jni.proto.Cosmos.Amount.1
            @Override // com.google.protobuf.t0
            public Amount parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Amount(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements AmountOrBuilder {
            private long amount_;
            private Object denom_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Amount_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearDenom() {
                this.denom_ = Amount.getDefaultInstance().getDenom();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Cosmos.AmountOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Cosmos.AmountOrBuilder
            public String getDenom() {
                Object obj = this.denom_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.denom_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.AmountOrBuilder
            public ByteString getDenomBytes() {
                Object obj = this.denom_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.denom_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Amount_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Amount_fieldAccessorTable.d(Amount.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setDenom(String str) {
                Objects.requireNonNull(str);
                this.denom_ = str;
                onChanged();
                return this;
            }

            public Builder setDenomBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.denom_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.denom_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Amount build() {
                Amount buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Amount buildPartial() {
                Amount amount = new Amount(this, (AnonymousClass1) null);
                amount.denom_ = this.denom_;
                amount.amount_ = this.amount_;
                onBuilt();
                return amount;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Amount getDefaultInstanceForType() {
                return Amount.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.denom_ = "";
                this.amount_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.denom_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Amount) {
                    return mergeFrom((Amount) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Amount amount) {
                if (amount == Amount.getDefaultInstance()) {
                    return this;
                }
                if (!amount.getDenom().isEmpty()) {
                    this.denom_ = amount.denom_;
                    onChanged();
                }
                if (amount.getAmount() != 0) {
                    setAmount(amount.getAmount());
                }
                mergeUnknownFields(amount.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Cosmos.Amount.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Amount.access$900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Cosmos$Amount r3 = (wallet.core.jni.proto.Cosmos.Amount) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Cosmos$Amount r4 = (wallet.core.jni.proto.Cosmos.Amount) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Amount.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Amount$Builder");
            }
        }

        public /* synthetic */ Amount(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Amount getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Cosmos.internal_static_TW_Cosmos_Proto_Amount_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Amount parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Amount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Amount parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Amount> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Amount)) {
                return super.equals(obj);
            }
            Amount amount = (Amount) obj;
            return getDenom().equals(amount.getDenom()) && getAmount() == amount.getAmount() && this.unknownFields.equals(amount.unknownFields);
        }

        @Override // wallet.core.jni.proto.Cosmos.AmountOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Cosmos.AmountOrBuilder
        public String getDenom() {
            Object obj = this.denom_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.denom_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Cosmos.AmountOrBuilder
        public ByteString getDenomBytes() {
            Object obj = this.denom_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.denom_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Amount> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getDenomBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.denom_);
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(2, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDenom().hashCode()) * 37) + 2) * 53) + a0.h(getAmount())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Cosmos.internal_static_TW_Cosmos_Proto_Amount_fieldAccessorTable.d(Amount.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Amount();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDenomBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.denom_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Amount(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Amount amount) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(amount);
        }

        public static Amount parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Amount(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Amount parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Amount) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Amount parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Amount getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Amount parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Amount() {
            this.memoizedIsInitialized = (byte) -1;
            this.denom_ = "";
        }

        public static Amount parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Amount parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Amount parseFrom(InputStream inputStream) throws IOException {
            return (Amount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Amount(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.denom_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.amount_ = jVar.y();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Amount parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Amount) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Amount parseFrom(j jVar) throws IOException {
            return (Amount) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Amount parseFrom(j jVar, r rVar) throws IOException {
            return (Amount) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface AmountOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        String getDenom();

        ByteString getDenomBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public enum BroadcastMode implements v0 {
        BLOCK(0),
        SYNC(1),
        ASYNC(2),
        UNRECOGNIZED(-1);
        
        public static final int ASYNC_VALUE = 2;
        public static final int BLOCK_VALUE = 0;
        public static final int SYNC_VALUE = 1;
        private final int value;
        private static final a0.d<BroadcastMode> internalValueMap = new a0.d<BroadcastMode>() { // from class: wallet.core.jni.proto.Cosmos.BroadcastMode.1
            @Override // com.google.protobuf.a0.d
            public BroadcastMode findValueByNumber(int i) {
                return BroadcastMode.forNumber(i);
            }
        };
        private static final BroadcastMode[] VALUES = values();

        BroadcastMode(int i) {
            this.value = i;
        }

        public static BroadcastMode forNumber(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        return null;
                    }
                    return ASYNC;
                }
                return SYNC;
            }
            return BLOCK;
        }

        public static final Descriptors.c getDescriptor() {
            return Cosmos.getDescriptor().l().get(0);
        }

        public static a0.d<BroadcastMode> internalGetValueMap() {
            return internalValueMap;
        }

        public final Descriptors.c getDescriptorForType() {
            return getDescriptor();
        }

        @Override // com.google.protobuf.a0.c
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        public final Descriptors.d getValueDescriptor() {
            if (this != UNRECOGNIZED) {
                return getDescriptor().k().get(ordinal());
            }
            throw new IllegalStateException("Can't get the descriptor of an unrecognized enum value.");
        }

        @Deprecated
        public static BroadcastMode valueOf(int i) {
            return forNumber(i);
        }

        public static BroadcastMode valueOf(Descriptors.d dVar) {
            if (dVar.h() == getDescriptor()) {
                if (dVar.g() == -1) {
                    return UNRECOGNIZED;
                }
                return VALUES[dVar.g()];
            }
            throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        }
    }

    /* loaded from: classes3.dex */
    public static final class Fee extends GeneratedMessageV3 implements FeeOrBuilder {
        public static final int AMOUNTS_FIELD_NUMBER = 1;
        public static final int GAS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private List<Amount> amounts_;
        private long gas_;
        private byte memoizedIsInitialized;
        private static final Fee DEFAULT_INSTANCE = new Fee();
        private static final t0<Fee> PARSER = new c<Fee>() { // from class: wallet.core.jni.proto.Cosmos.Fee.1
            @Override // com.google.protobuf.t0
            public Fee parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Fee(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements FeeOrBuilder {
            private x0<Amount, Amount.Builder, AmountOrBuilder> amountsBuilder_;
            private List<Amount> amounts_;
            private int bitField0_;
            private long gas_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private void ensureAmountsIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.amounts_ = new ArrayList(this.amounts_);
                    this.bitField0_ |= 1;
                }
            }

            private x0<Amount, Amount.Builder, AmountOrBuilder> getAmountsFieldBuilder() {
                if (this.amountsBuilder_ == null) {
                    this.amountsBuilder_ = new x0<>(this.amounts_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.amounts_ = null;
                }
                return this.amountsBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Fee_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getAmountsFieldBuilder();
                }
            }

            public Builder addAllAmounts(Iterable<? extends Amount> iterable) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    ensureAmountsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.amounts_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addAmounts(Amount amount) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(amount);
                    ensureAmountsIsMutable();
                    this.amounts_.add(amount);
                    onChanged();
                } else {
                    x0Var.f(amount);
                }
                return this;
            }

            public Amount.Builder addAmountsBuilder() {
                return getAmountsFieldBuilder().d(Amount.getDefaultInstance());
            }

            public Builder clearAmounts() {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    this.amounts_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearGas() {
                this.gas_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
            public Amount getAmounts(int i) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    return this.amounts_.get(i);
                }
                return x0Var.o(i);
            }

            public Amount.Builder getAmountsBuilder(int i) {
                return getAmountsFieldBuilder().l(i);
            }

            public List<Amount.Builder> getAmountsBuilderList() {
                return getAmountsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
            public int getAmountsCount() {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    return this.amounts_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
            public List<Amount> getAmountsList() {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.amounts_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
            public AmountOrBuilder getAmountsOrBuilder(int i) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    return this.amounts_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
            public List<? extends AmountOrBuilder> getAmountsOrBuilderList() {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.amounts_);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Fee_descriptor;
            }

            @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
            public long getGas() {
                return this.gas_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Fee_fieldAccessorTable.d(Fee.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeAmounts(int i) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    ensureAmountsIsMutable();
                    this.amounts_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setAmounts(int i, Amount amount) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(amount);
                    ensureAmountsIsMutable();
                    this.amounts_.set(i, amount);
                    onChanged();
                } else {
                    x0Var.x(i, amount);
                }
                return this;
            }

            public Builder setGas(long j) {
                this.gas_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.amounts_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Fee build() {
                Fee buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Fee buildPartial() {
                Fee fee = new Fee(this, (AnonymousClass1) null);
                int i = this.bitField0_;
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    if ((i & 1) != 0) {
                        this.amounts_ = Collections.unmodifiableList(this.amounts_);
                        this.bitField0_ &= -2;
                    }
                    fee.amounts_ = this.amounts_;
                } else {
                    fee.amounts_ = x0Var.g();
                }
                fee.gas_ = this.gas_;
                onBuilt();
                return fee;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Fee getDefaultInstanceForType() {
                return Fee.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public Amount.Builder addAmountsBuilder(int i) {
                return getAmountsFieldBuilder().c(i, Amount.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    this.amounts_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                this.gas_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.amounts_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public Builder addAmounts(int i, Amount amount) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(amount);
                    ensureAmountsIsMutable();
                    this.amounts_.add(i, amount);
                    onChanged();
                } else {
                    x0Var.e(i, amount);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Fee) {
                    return mergeFrom((Fee) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setAmounts(int i, Amount.Builder builder) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    ensureAmountsIsMutable();
                    this.amounts_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder mergeFrom(Fee fee) {
                if (fee == Fee.getDefaultInstance()) {
                    return this;
                }
                if (this.amountsBuilder_ == null) {
                    if (!fee.amounts_.isEmpty()) {
                        if (this.amounts_.isEmpty()) {
                            this.amounts_ = fee.amounts_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureAmountsIsMutable();
                            this.amounts_.addAll(fee.amounts_);
                        }
                        onChanged();
                    }
                } else if (!fee.amounts_.isEmpty()) {
                    if (!this.amountsBuilder_.u()) {
                        this.amountsBuilder_.b(fee.amounts_);
                    } else {
                        this.amountsBuilder_.i();
                        this.amountsBuilder_ = null;
                        this.amounts_ = fee.amounts_;
                        this.bitField0_ &= -2;
                        this.amountsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getAmountsFieldBuilder() : null;
                    }
                }
                if (fee.getGas() != 0) {
                    setGas(fee.getGas());
                }
                mergeUnknownFields(fee.unknownFields);
                onChanged();
                return this;
            }

            public Builder addAmounts(Amount.Builder builder) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    ensureAmountsIsMutable();
                    this.amounts_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addAmounts(int i, Amount.Builder builder) {
                x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                if (x0Var == null) {
                    ensureAmountsIsMutable();
                    this.amounts_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Cosmos.Fee.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Fee.access$2200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Cosmos$Fee r3 = (wallet.core.jni.proto.Cosmos.Fee) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Cosmos$Fee r4 = (wallet.core.jni.proto.Cosmos.Fee) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Fee.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Fee$Builder");
            }
        }

        public /* synthetic */ Fee(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Fee getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Cosmos.internal_static_TW_Cosmos_Proto_Fee_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Fee parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Fee) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Fee parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Fee> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Fee)) {
                return super.equals(obj);
            }
            Fee fee = (Fee) obj;
            return getAmountsList().equals(fee.getAmountsList()) && getGas() == fee.getGas() && this.unknownFields.equals(fee.unknownFields);
        }

        @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
        public Amount getAmounts(int i) {
            return this.amounts_.get(i);
        }

        @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
        public int getAmountsCount() {
            return this.amounts_.size();
        }

        @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
        public List<Amount> getAmountsList() {
            return this.amounts_;
        }

        @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
        public AmountOrBuilder getAmountsOrBuilder(int i) {
            return this.amounts_.get(i);
        }

        @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
        public List<? extends AmountOrBuilder> getAmountsOrBuilderList() {
            return this.amounts_;
        }

        @Override // wallet.core.jni.proto.Cosmos.FeeOrBuilder
        public long getGas() {
            return this.gas_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Fee> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.amounts_.size(); i3++) {
                i2 += CodedOutputStream.G(1, this.amounts_.get(i3));
            }
            long j = this.gas_;
            if (j != 0) {
                i2 += CodedOutputStream.a0(2, j);
            }
            int serializedSize = i2 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (getAmountsCount() > 0) {
                hashCode = (((hashCode * 37) + 1) * 53) + getAmountsList().hashCode();
            }
            int h = (((((hashCode * 37) + 2) * 53) + a0.h(getGas())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Cosmos.internal_static_TW_Cosmos_Proto_Fee_fieldAccessorTable.d(Fee.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Fee();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            for (int i = 0; i < this.amounts_.size(); i++) {
                codedOutputStream.K0(1, this.amounts_.get(i));
            }
            long j = this.gas_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Fee(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Fee fee) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(fee);
        }

        public static Fee parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Fee(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Fee parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Fee) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Fee parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Fee getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Fee parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Fee() {
            this.memoizedIsInitialized = (byte) -1;
            this.amounts_ = Collections.emptyList();
        }

        public static Fee parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Fee parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Fee parseFrom(InputStream inputStream) throws IOException {
            return (Fee) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private Fee(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                if (!(z2 & true)) {
                                    this.amounts_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.amounts_.add(jVar.z(Amount.parser(), rVar));
                            } else if (J != 16) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.gas_ = jVar.L();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.amounts_ = Collections.unmodifiableList(this.amounts_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Fee parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Fee) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Fee parseFrom(j jVar) throws IOException {
            return (Fee) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Fee parseFrom(j jVar, r rVar) throws IOException {
            return (Fee) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface FeeOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Amount getAmounts(int i);

        int getAmountsCount();

        List<Amount> getAmountsList();

        AmountOrBuilder getAmountsOrBuilder(int i);

        List<? extends AmountOrBuilder> getAmountsOrBuilderList();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGas();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Message extends GeneratedMessageV3 implements MessageOrBuilder {
        private static final Message DEFAULT_INSTANCE = new Message();
        private static final t0<Message> PARSER = new c<Message>() { // from class: wallet.core.jni.proto.Cosmos.Message.1
            @Override // com.google.protobuf.t0
            public Message parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Message(jVar, rVar, null);
            }
        };
        public static final int RAW_JSON_MESSAGE_FIELD_NUMBER = 6;
        public static final int RESTAKE_MESSAGE_FIELD_NUMBER = 4;
        public static final int SEND_COINS_MESSAGE_FIELD_NUMBER = 1;
        public static final int STAKE_MESSAGE_FIELD_NUMBER = 2;
        public static final int UNSTAKE_MESSAGE_FIELD_NUMBER = 3;
        public static final int WITHDRAW_STAKE_REWARD_MESSAGE_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;

        /* loaded from: classes3.dex */
        public static final class BeginRedelegate extends GeneratedMessageV3 implements BeginRedelegateOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 4;
            public static final int DELEGATOR_ADDRESS_FIELD_NUMBER = 1;
            public static final int TYPE_PREFIX_FIELD_NUMBER = 5;
            public static final int VALIDATOR_DST_ADDRESS_FIELD_NUMBER = 3;
            public static final int VALIDATOR_SRC_ADDRESS_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private Amount amount_;
            private volatile Object delegatorAddress_;
            private byte memoizedIsInitialized;
            private volatile Object typePrefix_;
            private volatile Object validatorDstAddress_;
            private volatile Object validatorSrcAddress_;
            private static final BeginRedelegate DEFAULT_INSTANCE = new BeginRedelegate();
            private static final t0<BeginRedelegate> PARSER = new c<BeginRedelegate>() { // from class: wallet.core.jni.proto.Cosmos.Message.BeginRedelegate.1
                @Override // com.google.protobuf.t0
                public BeginRedelegate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new BeginRedelegate(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements BeginRedelegateOrBuilder {
                private a1<Amount, Amount.Builder, AmountOrBuilder> amountBuilder_;
                private Amount amount_;
                private Object delegatorAddress_;
                private Object typePrefix_;
                private Object validatorDstAddress_;
                private Object validatorSrcAddress_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private a1<Amount, Amount.Builder, AmountOrBuilder> getAmountFieldBuilder() {
                    if (this.amountBuilder_ == null) {
                        this.amountBuilder_ = new a1<>(getAmount(), getParentForChildren(), isClean());
                        this.amount_ = null;
                    }
                    return this.amountBuilder_;
                }

                public static final Descriptors.b getDescriptor() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    if (this.amountBuilder_ == null) {
                        this.amount_ = null;
                        onChanged();
                    } else {
                        this.amount_ = null;
                        this.amountBuilder_ = null;
                    }
                    return this;
                }

                public Builder clearDelegatorAddress() {
                    this.delegatorAddress_ = BeginRedelegate.getDefaultInstance().getDelegatorAddress();
                    onChanged();
                    return this;
                }

                public Builder clearTypePrefix() {
                    this.typePrefix_ = BeginRedelegate.getDefaultInstance().getTypePrefix();
                    onChanged();
                    return this;
                }

                public Builder clearValidatorDstAddress() {
                    this.validatorDstAddress_ = BeginRedelegate.getDefaultInstance().getValidatorDstAddress();
                    onChanged();
                    return this;
                }

                public Builder clearValidatorSrcAddress() {
                    this.validatorSrcAddress_ = BeginRedelegate.getDefaultInstance().getValidatorSrcAddress();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public Amount getAmount() {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Amount amount = this.amount_;
                        return amount == null ? Amount.getDefaultInstance() : amount;
                    }
                    return a1Var.f();
                }

                public Amount.Builder getAmountBuilder() {
                    onChanged();
                    return getAmountFieldBuilder().e();
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public AmountOrBuilder getAmountOrBuilder() {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var != null) {
                        return a1Var.g();
                    }
                    Amount amount = this.amount_;
                    return amount == null ? Amount.getDefaultInstance() : amount;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public String getDelegatorAddress() {
                    Object obj = this.delegatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.delegatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public ByteString getDelegatorAddressBytes() {
                    Object obj = this.delegatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.delegatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_descriptor;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public String getTypePrefix() {
                    Object obj = this.typePrefix_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.typePrefix_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public ByteString getTypePrefixBytes() {
                    Object obj = this.typePrefix_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.typePrefix_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public String getValidatorDstAddress() {
                    Object obj = this.validatorDstAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.validatorDstAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public ByteString getValidatorDstAddressBytes() {
                    Object obj = this.validatorDstAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.validatorDstAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public String getValidatorSrcAddress() {
                    Object obj = this.validatorSrcAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.validatorSrcAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public ByteString getValidatorSrcAddressBytes() {
                    Object obj = this.validatorSrcAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.validatorSrcAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
                public boolean hasAmount() {
                    return (this.amountBuilder_ == null && this.amount_ == null) ? false : true;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_fieldAccessorTable.d(BeginRedelegate.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder mergeAmount(Amount amount) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Amount amount2 = this.amount_;
                        if (amount2 != null) {
                            this.amount_ = Amount.newBuilder(amount2).mergeFrom(amount).buildPartial();
                        } else {
                            this.amount_ = amount;
                        }
                        onChanged();
                    } else {
                        a1Var.h(amount);
                    }
                    return this;
                }

                public Builder setAmount(Amount amount) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Objects.requireNonNull(amount);
                        this.amount_ = amount;
                        onChanged();
                    } else {
                        a1Var.j(amount);
                    }
                    return this;
                }

                public Builder setDelegatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.delegatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setDelegatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.delegatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefix(String str) {
                    Objects.requireNonNull(str);
                    this.typePrefix_ = str;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefixBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.typePrefix_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValidatorDstAddress(String str) {
                    Objects.requireNonNull(str);
                    this.validatorDstAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setValidatorDstAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.validatorDstAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValidatorSrcAddress(String str) {
                    Objects.requireNonNull(str);
                    this.validatorSrcAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setValidatorSrcAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.validatorSrcAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.delegatorAddress_ = "";
                    this.validatorSrcAddress_ = "";
                    this.validatorDstAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public BeginRedelegate build() {
                    BeginRedelegate buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public BeginRedelegate buildPartial() {
                    BeginRedelegate beginRedelegate = new BeginRedelegate(this, (AnonymousClass1) null);
                    beginRedelegate.delegatorAddress_ = this.delegatorAddress_;
                    beginRedelegate.validatorSrcAddress_ = this.validatorSrcAddress_;
                    beginRedelegate.validatorDstAddress_ = this.validatorDstAddress_;
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        beginRedelegate.amount_ = this.amount_;
                    } else {
                        beginRedelegate.amount_ = a1Var.b();
                    }
                    beginRedelegate.typePrefix_ = this.typePrefix_;
                    onBuilt();
                    return beginRedelegate;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public BeginRedelegate getDefaultInstanceForType() {
                    return BeginRedelegate.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.delegatorAddress_ = "";
                    this.validatorSrcAddress_ = "";
                    this.validatorDstAddress_ = "";
                    if (this.amountBuilder_ == null) {
                        this.amount_ = null;
                    } else {
                        this.amount_ = null;
                        this.amountBuilder_ = null;
                    }
                    this.typePrefix_ = "";
                    return this;
                }

                public Builder setAmount(Amount.Builder builder) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        this.amount_ = builder.build();
                        onChanged();
                    } else {
                        a1Var.j(builder.build());
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof BeginRedelegate) {
                        return mergeFrom((BeginRedelegate) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.delegatorAddress_ = "";
                    this.validatorSrcAddress_ = "";
                    this.validatorDstAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(BeginRedelegate beginRedelegate) {
                    if (beginRedelegate == BeginRedelegate.getDefaultInstance()) {
                        return this;
                    }
                    if (!beginRedelegate.getDelegatorAddress().isEmpty()) {
                        this.delegatorAddress_ = beginRedelegate.delegatorAddress_;
                        onChanged();
                    }
                    if (!beginRedelegate.getValidatorSrcAddress().isEmpty()) {
                        this.validatorSrcAddress_ = beginRedelegate.validatorSrcAddress_;
                        onChanged();
                    }
                    if (!beginRedelegate.getValidatorDstAddress().isEmpty()) {
                        this.validatorDstAddress_ = beginRedelegate.validatorDstAddress_;
                        onChanged();
                    }
                    if (beginRedelegate.hasAmount()) {
                        mergeAmount(beginRedelegate.getAmount());
                    }
                    if (!beginRedelegate.getTypePrefix().isEmpty()) {
                        this.typePrefix_ = beginRedelegate.typePrefix_;
                        onChanged();
                    }
                    mergeUnknownFields(beginRedelegate.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Cosmos.Message.BeginRedelegate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Message.BeginRedelegate.access$8700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Cosmos$Message$BeginRedelegate r3 = (wallet.core.jni.proto.Cosmos.Message.BeginRedelegate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Cosmos$Message$BeginRedelegate r4 = (wallet.core.jni.proto.Cosmos.Message.BeginRedelegate) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Message.BeginRedelegate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Message$BeginRedelegate$Builder");
                }
            }

            public /* synthetic */ BeginRedelegate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static BeginRedelegate getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static BeginRedelegate parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (BeginRedelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static BeginRedelegate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<BeginRedelegate> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof BeginRedelegate)) {
                    return super.equals(obj);
                }
                BeginRedelegate beginRedelegate = (BeginRedelegate) obj;
                if (getDelegatorAddress().equals(beginRedelegate.getDelegatorAddress()) && getValidatorSrcAddress().equals(beginRedelegate.getValidatorSrcAddress()) && getValidatorDstAddress().equals(beginRedelegate.getValidatorDstAddress()) && hasAmount() == beginRedelegate.hasAmount()) {
                    return (!hasAmount() || getAmount().equals(beginRedelegate.getAmount())) && getTypePrefix().equals(beginRedelegate.getTypePrefix()) && this.unknownFields.equals(beginRedelegate.unknownFields);
                }
                return false;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public Amount getAmount() {
                Amount amount = this.amount_;
                return amount == null ? Amount.getDefaultInstance() : amount;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public AmountOrBuilder getAmountOrBuilder() {
                return getAmount();
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public String getDelegatorAddress() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.delegatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public ByteString getDelegatorAddressBytes() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<BeginRedelegate> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getDelegatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegatorAddress_);
                if (!getValidatorSrcAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.validatorSrcAddress_);
                }
                if (!getValidatorDstAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(3, this.validatorDstAddress_);
                }
                if (this.amount_ != null) {
                    computeStringSize += CodedOutputStream.G(4, getAmount());
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(5, this.typePrefix_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public String getTypePrefix() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.typePrefix_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public ByteString getTypePrefixBytes() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.typePrefix_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public String getValidatorDstAddress() {
                Object obj = this.validatorDstAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.validatorDstAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public ByteString getValidatorDstAddressBytes() {
                Object obj = this.validatorDstAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorDstAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public String getValidatorSrcAddress() {
                Object obj = this.validatorSrcAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.validatorSrcAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public ByteString getValidatorSrcAddressBytes() {
                Object obj = this.validatorSrcAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorSrcAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.BeginRedelegateOrBuilder
            public boolean hasAmount() {
                return this.amount_ != null;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegatorAddress().hashCode()) * 37) + 2) * 53) + getValidatorSrcAddress().hashCode()) * 37) + 3) * 53) + getValidatorDstAddress().hashCode();
                if (hasAmount()) {
                    hashCode = (((hashCode * 37) + 4) * 53) + getAmount().hashCode();
                }
                int hashCode2 = (((((hashCode * 37) + 5) * 53) + getTypePrefix().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_fieldAccessorTable.d(BeginRedelegate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new BeginRedelegate();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getDelegatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegatorAddress_);
                }
                if (!getValidatorSrcAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.validatorSrcAddress_);
                }
                if (!getValidatorDstAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 3, this.validatorDstAddress_);
                }
                if (this.amount_ != null) {
                    codedOutputStream.K0(4, getAmount());
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 5, this.typePrefix_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ BeginRedelegate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(BeginRedelegate beginRedelegate) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(beginRedelegate);
            }

            public static BeginRedelegate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private BeginRedelegate(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static BeginRedelegate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (BeginRedelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static BeginRedelegate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public BeginRedelegate getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static BeginRedelegate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private BeginRedelegate() {
                this.memoizedIsInitialized = (byte) -1;
                this.delegatorAddress_ = "";
                this.validatorSrcAddress_ = "";
                this.validatorDstAddress_ = "";
                this.typePrefix_ = "";
            }

            public static BeginRedelegate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static BeginRedelegate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static BeginRedelegate parseFrom(InputStream inputStream) throws IOException {
                return (BeginRedelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static BeginRedelegate parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (BeginRedelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private BeginRedelegate(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.delegatorAddress_ = jVar.I();
                                } else if (J == 18) {
                                    this.validatorSrcAddress_ = jVar.I();
                                } else if (J == 26) {
                                    this.validatorDstAddress_ = jVar.I();
                                } else if (J == 34) {
                                    Amount amount = this.amount_;
                                    Amount.Builder builder = amount != null ? amount.toBuilder() : null;
                                    Amount amount2 = (Amount) jVar.z(Amount.parser(), rVar);
                                    this.amount_ = amount2;
                                    if (builder != null) {
                                        builder.mergeFrom(amount2);
                                        this.amount_ = builder.buildPartial();
                                    }
                                } else if (J != 42) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.typePrefix_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static BeginRedelegate parseFrom(j jVar) throws IOException {
                return (BeginRedelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static BeginRedelegate parseFrom(j jVar, r rVar) throws IOException {
                return (BeginRedelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface BeginRedelegateOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            Amount getAmount();

            AmountOrBuilder getAmountOrBuilder();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            String getDelegatorAddress();

            ByteString getDelegatorAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getTypePrefix();

            ByteString getTypePrefixBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            String getValidatorDstAddress();

            ByteString getValidatorDstAddressBytes();

            String getValidatorSrcAddress();

            ByteString getValidatorSrcAddressBytes();

            boolean hasAmount();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements MessageOrBuilder {
            private int messageOneofCase_;
            private Object messageOneof_;
            private a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> rawJsonMessageBuilder_;
            private a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> restakeMessageBuilder_;
            private a1<Send, Send.Builder, SendOrBuilder> sendCoinsMessageBuilder_;
            private a1<Delegate, Delegate.Builder, DelegateOrBuilder> stakeMessageBuilder_;
            private a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> unstakeMessageBuilder_;
            private a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> withdrawStakeRewardMessageBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_descriptor;
            }

            private a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> getRawJsonMessageFieldBuilder() {
                if (this.rawJsonMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 6) {
                        this.messageOneof_ = RawJSON.getDefaultInstance();
                    }
                    this.rawJsonMessageBuilder_ = new a1<>((RawJSON) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 6;
                onChanged();
                return this.rawJsonMessageBuilder_;
            }

            private a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> getRestakeMessageFieldBuilder() {
                if (this.restakeMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 4) {
                        this.messageOneof_ = BeginRedelegate.getDefaultInstance();
                    }
                    this.restakeMessageBuilder_ = new a1<>((BeginRedelegate) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 4;
                onChanged();
                return this.restakeMessageBuilder_;
            }

            private a1<Send, Send.Builder, SendOrBuilder> getSendCoinsMessageFieldBuilder() {
                if (this.sendCoinsMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 1) {
                        this.messageOneof_ = Send.getDefaultInstance();
                    }
                    this.sendCoinsMessageBuilder_ = new a1<>((Send) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 1;
                onChanged();
                return this.sendCoinsMessageBuilder_;
            }

            private a1<Delegate, Delegate.Builder, DelegateOrBuilder> getStakeMessageFieldBuilder() {
                if (this.stakeMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 2) {
                        this.messageOneof_ = Delegate.getDefaultInstance();
                    }
                    this.stakeMessageBuilder_ = new a1<>((Delegate) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 2;
                onChanged();
                return this.stakeMessageBuilder_;
            }

            private a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> getUnstakeMessageFieldBuilder() {
                if (this.unstakeMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 3) {
                        this.messageOneof_ = Undelegate.getDefaultInstance();
                    }
                    this.unstakeMessageBuilder_ = new a1<>((Undelegate) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 3;
                onChanged();
                return this.unstakeMessageBuilder_;
            }

            private a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> getWithdrawStakeRewardMessageFieldBuilder() {
                if (this.withdrawStakeRewardMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 5) {
                        this.messageOneof_ = WithdrawDelegationReward.getDefaultInstance();
                    }
                    this.withdrawStakeRewardMessageBuilder_ = new a1<>((WithdrawDelegationReward) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 5;
                onChanged();
                return this.withdrawStakeRewardMessageBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearRawJsonMessage() {
                a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> a1Var = this.rawJsonMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 6) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 6) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearRestakeMessage() {
                a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> a1Var = this.restakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearSendCoinsMessage() {
                a1<Send, Send.Builder, SendOrBuilder> a1Var = this.sendCoinsMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeMessage() {
                a1<Delegate, Delegate.Builder, DelegateOrBuilder> a1Var = this.stakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearUnstakeMessage() {
                a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> a1Var = this.unstakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearWithdrawStakeRewardMessage() {
                a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> a1Var = this.withdrawStakeRewardMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_descriptor;
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public RawJSON getRawJsonMessage() {
                a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> a1Var = this.rawJsonMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 6) {
                        return (RawJSON) this.messageOneof_;
                    }
                    return RawJSON.getDefaultInstance();
                } else if (this.messageOneofCase_ == 6) {
                    return a1Var.f();
                } else {
                    return RawJSON.getDefaultInstance();
                }
            }

            public RawJSON.Builder getRawJsonMessageBuilder() {
                return getRawJsonMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public RawJSONOrBuilder getRawJsonMessageOrBuilder() {
                a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 6 || (a1Var = this.rawJsonMessageBuilder_) == null) {
                    if (i == 6) {
                        return (RawJSON) this.messageOneof_;
                    }
                    return RawJSON.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public BeginRedelegate getRestakeMessage() {
                a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> a1Var = this.restakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        return (BeginRedelegate) this.messageOneof_;
                    }
                    return BeginRedelegate.getDefaultInstance();
                } else if (this.messageOneofCase_ == 4) {
                    return a1Var.f();
                } else {
                    return BeginRedelegate.getDefaultInstance();
                }
            }

            public BeginRedelegate.Builder getRestakeMessageBuilder() {
                return getRestakeMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public BeginRedelegateOrBuilder getRestakeMessageOrBuilder() {
                a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 4 || (a1Var = this.restakeMessageBuilder_) == null) {
                    if (i == 4) {
                        return (BeginRedelegate) this.messageOneof_;
                    }
                    return BeginRedelegate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public Send getSendCoinsMessage() {
                a1<Send, Send.Builder, SendOrBuilder> a1Var = this.sendCoinsMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        return (Send) this.messageOneof_;
                    }
                    return Send.getDefaultInstance();
                } else if (this.messageOneofCase_ == 1) {
                    return a1Var.f();
                } else {
                    return Send.getDefaultInstance();
                }
            }

            public Send.Builder getSendCoinsMessageBuilder() {
                return getSendCoinsMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public SendOrBuilder getSendCoinsMessageOrBuilder() {
                a1<Send, Send.Builder, SendOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 1 || (a1Var = this.sendCoinsMessageBuilder_) == null) {
                    if (i == 1) {
                        return (Send) this.messageOneof_;
                    }
                    return Send.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public Delegate getStakeMessage() {
                a1<Delegate, Delegate.Builder, DelegateOrBuilder> a1Var = this.stakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        return (Delegate) this.messageOneof_;
                    }
                    return Delegate.getDefaultInstance();
                } else if (this.messageOneofCase_ == 2) {
                    return a1Var.f();
                } else {
                    return Delegate.getDefaultInstance();
                }
            }

            public Delegate.Builder getStakeMessageBuilder() {
                return getStakeMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public DelegateOrBuilder getStakeMessageOrBuilder() {
                a1<Delegate, Delegate.Builder, DelegateOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 2 || (a1Var = this.stakeMessageBuilder_) == null) {
                    if (i == 2) {
                        return (Delegate) this.messageOneof_;
                    }
                    return Delegate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public Undelegate getUnstakeMessage() {
                a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> a1Var = this.unstakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        return (Undelegate) this.messageOneof_;
                    }
                    return Undelegate.getDefaultInstance();
                } else if (this.messageOneofCase_ == 3) {
                    return a1Var.f();
                } else {
                    return Undelegate.getDefaultInstance();
                }
            }

            public Undelegate.Builder getUnstakeMessageBuilder() {
                return getUnstakeMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public UndelegateOrBuilder getUnstakeMessageOrBuilder() {
                a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 3 || (a1Var = this.unstakeMessageBuilder_) == null) {
                    if (i == 3) {
                        return (Undelegate) this.messageOneof_;
                    }
                    return Undelegate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public WithdrawDelegationReward getWithdrawStakeRewardMessage() {
                a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> a1Var = this.withdrawStakeRewardMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        return (WithdrawDelegationReward) this.messageOneof_;
                    }
                    return WithdrawDelegationReward.getDefaultInstance();
                } else if (this.messageOneofCase_ == 5) {
                    return a1Var.f();
                } else {
                    return WithdrawDelegationReward.getDefaultInstance();
                }
            }

            public WithdrawDelegationReward.Builder getWithdrawStakeRewardMessageBuilder() {
                return getWithdrawStakeRewardMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public WithdrawDelegationRewardOrBuilder getWithdrawStakeRewardMessageOrBuilder() {
                a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 5 || (a1Var = this.withdrawStakeRewardMessageBuilder_) == null) {
                    if (i == 5) {
                        return (WithdrawDelegationReward) this.messageOneof_;
                    }
                    return WithdrawDelegationReward.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public boolean hasRawJsonMessage() {
                return this.messageOneofCase_ == 6;
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public boolean hasRestakeMessage() {
                return this.messageOneofCase_ == 4;
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public boolean hasSendCoinsMessage() {
                return this.messageOneofCase_ == 1;
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public boolean hasStakeMessage() {
                return this.messageOneofCase_ == 2;
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public boolean hasUnstakeMessage() {
                return this.messageOneofCase_ == 3;
            }

            @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
            public boolean hasWithdrawStakeRewardMessage() {
                return this.messageOneofCase_ == 5;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_fieldAccessorTable.d(Message.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeRawJsonMessage(RawJSON rawJSON) {
                a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> a1Var = this.rawJsonMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 6 && this.messageOneof_ != RawJSON.getDefaultInstance()) {
                        this.messageOneof_ = RawJSON.newBuilder((RawJSON) this.messageOneof_).mergeFrom(rawJSON).buildPartial();
                    } else {
                        this.messageOneof_ = rawJSON;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 6) {
                        a1Var.h(rawJSON);
                    }
                    this.rawJsonMessageBuilder_.j(rawJSON);
                }
                this.messageOneofCase_ = 6;
                return this;
            }

            public Builder mergeRestakeMessage(BeginRedelegate beginRedelegate) {
                a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> a1Var = this.restakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4 && this.messageOneof_ != BeginRedelegate.getDefaultInstance()) {
                        this.messageOneof_ = BeginRedelegate.newBuilder((BeginRedelegate) this.messageOneof_).mergeFrom(beginRedelegate).buildPartial();
                    } else {
                        this.messageOneof_ = beginRedelegate;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 4) {
                        a1Var.h(beginRedelegate);
                    }
                    this.restakeMessageBuilder_.j(beginRedelegate);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder mergeSendCoinsMessage(Send send) {
                a1<Send, Send.Builder, SendOrBuilder> a1Var = this.sendCoinsMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1 && this.messageOneof_ != Send.getDefaultInstance()) {
                        this.messageOneof_ = Send.newBuilder((Send) this.messageOneof_).mergeFrom(send).buildPartial();
                    } else {
                        this.messageOneof_ = send;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 1) {
                        a1Var.h(send);
                    }
                    this.sendCoinsMessageBuilder_.j(send);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder mergeStakeMessage(Delegate delegate) {
                a1<Delegate, Delegate.Builder, DelegateOrBuilder> a1Var = this.stakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2 && this.messageOneof_ != Delegate.getDefaultInstance()) {
                        this.messageOneof_ = Delegate.newBuilder((Delegate) this.messageOneof_).mergeFrom(delegate).buildPartial();
                    } else {
                        this.messageOneof_ = delegate;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 2) {
                        a1Var.h(delegate);
                    }
                    this.stakeMessageBuilder_.j(delegate);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder mergeUnstakeMessage(Undelegate undelegate) {
                a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> a1Var = this.unstakeMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3 && this.messageOneof_ != Undelegate.getDefaultInstance()) {
                        this.messageOneof_ = Undelegate.newBuilder((Undelegate) this.messageOneof_).mergeFrom(undelegate).buildPartial();
                    } else {
                        this.messageOneof_ = undelegate;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 3) {
                        a1Var.h(undelegate);
                    }
                    this.unstakeMessageBuilder_.j(undelegate);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder mergeWithdrawStakeRewardMessage(WithdrawDelegationReward withdrawDelegationReward) {
                a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> a1Var = this.withdrawStakeRewardMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5 && this.messageOneof_ != WithdrawDelegationReward.getDefaultInstance()) {
                        this.messageOneof_ = WithdrawDelegationReward.newBuilder((WithdrawDelegationReward) this.messageOneof_).mergeFrom(withdrawDelegationReward).buildPartial();
                    } else {
                        this.messageOneof_ = withdrawDelegationReward;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 5) {
                        a1Var.h(withdrawDelegationReward);
                    }
                    this.withdrawStakeRewardMessageBuilder_.j(withdrawDelegationReward);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder setRawJsonMessage(RawJSON rawJSON) {
                a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> a1Var = this.rawJsonMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(rawJSON);
                    this.messageOneof_ = rawJSON;
                    onChanged();
                } else {
                    a1Var.j(rawJSON);
                }
                this.messageOneofCase_ = 6;
                return this;
            }

            public Builder setRestakeMessage(BeginRedelegate beginRedelegate) {
                a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> a1Var = this.restakeMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(beginRedelegate);
                    this.messageOneof_ = beginRedelegate;
                    onChanged();
                } else {
                    a1Var.j(beginRedelegate);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setSendCoinsMessage(Send send) {
                a1<Send, Send.Builder, SendOrBuilder> a1Var = this.sendCoinsMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(send);
                    this.messageOneof_ = send;
                    onChanged();
                } else {
                    a1Var.j(send);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setStakeMessage(Delegate delegate) {
                a1<Delegate, Delegate.Builder, DelegateOrBuilder> a1Var = this.stakeMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(delegate);
                    this.messageOneof_ = delegate;
                    onChanged();
                } else {
                    a1Var.j(delegate);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setUnstakeMessage(Undelegate undelegate) {
                a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> a1Var = this.unstakeMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(undelegate);
                    this.messageOneof_ = undelegate;
                    onChanged();
                } else {
                    a1Var.j(undelegate);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder setWithdrawStakeRewardMessage(WithdrawDelegationReward withdrawDelegationReward) {
                a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> a1Var = this.withdrawStakeRewardMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(withdrawDelegationReward);
                    this.messageOneof_ = withdrawDelegationReward;
                    onChanged();
                } else {
                    a1Var.j(withdrawDelegationReward);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Message build() {
                Message buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Message buildPartial() {
                Message message = new Message(this, (AnonymousClass1) null);
                if (this.messageOneofCase_ == 1) {
                    a1<Send, Send.Builder, SendOrBuilder> a1Var = this.sendCoinsMessageBuilder_;
                    if (a1Var == null) {
                        message.messageOneof_ = this.messageOneof_;
                    } else {
                        message.messageOneof_ = a1Var.b();
                    }
                }
                if (this.messageOneofCase_ == 2) {
                    a1<Delegate, Delegate.Builder, DelegateOrBuilder> a1Var2 = this.stakeMessageBuilder_;
                    if (a1Var2 == null) {
                        message.messageOneof_ = this.messageOneof_;
                    } else {
                        message.messageOneof_ = a1Var2.b();
                    }
                }
                if (this.messageOneofCase_ == 3) {
                    a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> a1Var3 = this.unstakeMessageBuilder_;
                    if (a1Var3 == null) {
                        message.messageOneof_ = this.messageOneof_;
                    } else {
                        message.messageOneof_ = a1Var3.b();
                    }
                }
                if (this.messageOneofCase_ == 4) {
                    a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> a1Var4 = this.restakeMessageBuilder_;
                    if (a1Var4 == null) {
                        message.messageOneof_ = this.messageOneof_;
                    } else {
                        message.messageOneof_ = a1Var4.b();
                    }
                }
                if (this.messageOneofCase_ == 5) {
                    a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> a1Var5 = this.withdrawStakeRewardMessageBuilder_;
                    if (a1Var5 == null) {
                        message.messageOneof_ = this.messageOneof_;
                    } else {
                        message.messageOneof_ = a1Var5.b();
                    }
                }
                if (this.messageOneofCase_ == 6) {
                    a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> a1Var6 = this.rawJsonMessageBuilder_;
                    if (a1Var6 == null) {
                        message.messageOneof_ = this.messageOneof_;
                    } else {
                        message.messageOneof_ = a1Var6.b();
                    }
                }
                message.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return message;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Message getDefaultInstanceForType() {
                return Message.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Message) {
                    return mergeFrom((Message) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setRawJsonMessage(RawJSON.Builder builder) {
                a1<RawJSON, RawJSON.Builder, RawJSONOrBuilder> a1Var = this.rawJsonMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 6;
                return this;
            }

            public Builder setRestakeMessage(BeginRedelegate.Builder builder) {
                a1<BeginRedelegate, BeginRedelegate.Builder, BeginRedelegateOrBuilder> a1Var = this.restakeMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setSendCoinsMessage(Send.Builder builder) {
                a1<Send, Send.Builder, SendOrBuilder> a1Var = this.sendCoinsMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setStakeMessage(Delegate.Builder builder) {
                a1<Delegate, Delegate.Builder, DelegateOrBuilder> a1Var = this.stakeMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setUnstakeMessage(Undelegate.Builder builder) {
                a1<Undelegate, Undelegate.Builder, UndelegateOrBuilder> a1Var = this.unstakeMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder setWithdrawStakeRewardMessage(WithdrawDelegationReward.Builder builder) {
                a1<WithdrawDelegationReward, WithdrawDelegationReward.Builder, WithdrawDelegationRewardOrBuilder> a1Var = this.withdrawStakeRewardMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder mergeFrom(Message message) {
                if (message == Message.getDefaultInstance()) {
                    return this;
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Cosmos$Message$MessageOneofCase[message.getMessageOneofCase().ordinal()]) {
                    case 1:
                        mergeSendCoinsMessage(message.getSendCoinsMessage());
                        break;
                    case 2:
                        mergeStakeMessage(message.getStakeMessage());
                        break;
                    case 3:
                        mergeUnstakeMessage(message.getUnstakeMessage());
                        break;
                    case 4:
                        mergeRestakeMessage(message.getRestakeMessage());
                        break;
                    case 5:
                        mergeWithdrawStakeRewardMessage(message.getWithdrawStakeRewardMessage());
                        break;
                    case 6:
                        mergeRawJsonMessage(message.getRawJsonMessage());
                        break;
                }
                mergeUnknownFields(message.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Cosmos.Message.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Message.access$12800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Cosmos$Message r3 = (wallet.core.jni.proto.Cosmos.Message) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Cosmos$Message r4 = (wallet.core.jni.proto.Cosmos.Message) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Message.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Message$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public static final class Delegate extends GeneratedMessageV3 implements DelegateOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 3;
            public static final int DELEGATOR_ADDRESS_FIELD_NUMBER = 1;
            public static final int TYPE_PREFIX_FIELD_NUMBER = 4;
            public static final int VALIDATOR_ADDRESS_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private Amount amount_;
            private volatile Object delegatorAddress_;
            private byte memoizedIsInitialized;
            private volatile Object typePrefix_;
            private volatile Object validatorAddress_;
            private static final Delegate DEFAULT_INSTANCE = new Delegate();
            private static final t0<Delegate> PARSER = new c<Delegate>() { // from class: wallet.core.jni.proto.Cosmos.Message.Delegate.1
                @Override // com.google.protobuf.t0
                public Delegate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Delegate(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements DelegateOrBuilder {
                private a1<Amount, Amount.Builder, AmountOrBuilder> amountBuilder_;
                private Amount amount_;
                private Object delegatorAddress_;
                private Object typePrefix_;
                private Object validatorAddress_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private a1<Amount, Amount.Builder, AmountOrBuilder> getAmountFieldBuilder() {
                    if (this.amountBuilder_ == null) {
                        this.amountBuilder_ = new a1<>(getAmount(), getParentForChildren(), isClean());
                        this.amount_ = null;
                    }
                    return this.amountBuilder_;
                }

                public static final Descriptors.b getDescriptor() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Delegate_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    if (this.amountBuilder_ == null) {
                        this.amount_ = null;
                        onChanged();
                    } else {
                        this.amount_ = null;
                        this.amountBuilder_ = null;
                    }
                    return this;
                }

                public Builder clearDelegatorAddress() {
                    this.delegatorAddress_ = Delegate.getDefaultInstance().getDelegatorAddress();
                    onChanged();
                    return this;
                }

                public Builder clearTypePrefix() {
                    this.typePrefix_ = Delegate.getDefaultInstance().getTypePrefix();
                    onChanged();
                    return this;
                }

                public Builder clearValidatorAddress() {
                    this.validatorAddress_ = Delegate.getDefaultInstance().getValidatorAddress();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public Amount getAmount() {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Amount amount = this.amount_;
                        return amount == null ? Amount.getDefaultInstance() : amount;
                    }
                    return a1Var.f();
                }

                public Amount.Builder getAmountBuilder() {
                    onChanged();
                    return getAmountFieldBuilder().e();
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public AmountOrBuilder getAmountOrBuilder() {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var != null) {
                        return a1Var.g();
                    }
                    Amount amount = this.amount_;
                    return amount == null ? Amount.getDefaultInstance() : amount;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public String getDelegatorAddress() {
                    Object obj = this.delegatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.delegatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public ByteString getDelegatorAddressBytes() {
                    Object obj = this.delegatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.delegatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Delegate_descriptor;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public String getTypePrefix() {
                    Object obj = this.typePrefix_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.typePrefix_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public ByteString getTypePrefixBytes() {
                    Object obj = this.typePrefix_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.typePrefix_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public String getValidatorAddress() {
                    Object obj = this.validatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.validatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public ByteString getValidatorAddressBytes() {
                    Object obj = this.validatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.validatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
                public boolean hasAmount() {
                    return (this.amountBuilder_ == null && this.amount_ == null) ? false : true;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Delegate_fieldAccessorTable.d(Delegate.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder mergeAmount(Amount amount) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Amount amount2 = this.amount_;
                        if (amount2 != null) {
                            this.amount_ = Amount.newBuilder(amount2).mergeFrom(amount).buildPartial();
                        } else {
                            this.amount_ = amount;
                        }
                        onChanged();
                    } else {
                        a1Var.h(amount);
                    }
                    return this;
                }

                public Builder setAmount(Amount amount) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Objects.requireNonNull(amount);
                        this.amount_ = amount;
                        onChanged();
                    } else {
                        a1Var.j(amount);
                    }
                    return this;
                }

                public Builder setDelegatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.delegatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setDelegatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.delegatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefix(String str) {
                    Objects.requireNonNull(str);
                    this.typePrefix_ = str;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefixBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.typePrefix_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValidatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.validatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setValidatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.validatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Delegate build() {
                    Delegate buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Delegate buildPartial() {
                    Delegate delegate = new Delegate(this, (AnonymousClass1) null);
                    delegate.delegatorAddress_ = this.delegatorAddress_;
                    delegate.validatorAddress_ = this.validatorAddress_;
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        delegate.amount_ = this.amount_;
                    } else {
                        delegate.amount_ = a1Var.b();
                    }
                    delegate.typePrefix_ = this.typePrefix_;
                    onBuilt();
                    return delegate;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Delegate getDefaultInstanceForType() {
                    return Delegate.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    if (this.amountBuilder_ == null) {
                        this.amount_ = null;
                    } else {
                        this.amount_ = null;
                        this.amountBuilder_ = null;
                    }
                    this.typePrefix_ = "";
                    return this;
                }

                public Builder setAmount(Amount.Builder builder) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        this.amount_ = builder.build();
                        onChanged();
                    } else {
                        a1Var.j(builder.build());
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Delegate) {
                        return mergeFrom((Delegate) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(Delegate delegate) {
                    if (delegate == Delegate.getDefaultInstance()) {
                        return this;
                    }
                    if (!delegate.getDelegatorAddress().isEmpty()) {
                        this.delegatorAddress_ = delegate.delegatorAddress_;
                        onChanged();
                    }
                    if (!delegate.getValidatorAddress().isEmpty()) {
                        this.validatorAddress_ = delegate.validatorAddress_;
                        onChanged();
                    }
                    if (delegate.hasAmount()) {
                        mergeAmount(delegate.getAmount());
                    }
                    if (!delegate.getTypePrefix().isEmpty()) {
                        this.typePrefix_ = delegate.typePrefix_;
                        onChanged();
                    }
                    mergeUnknownFields(delegate.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Cosmos.Message.Delegate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Message.Delegate.access$5400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Cosmos$Message$Delegate r3 = (wallet.core.jni.proto.Cosmos.Message.Delegate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Cosmos$Message$Delegate r4 = (wallet.core.jni.proto.Cosmos.Message.Delegate) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Message.Delegate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Message$Delegate$Builder");
                }
            }

            public /* synthetic */ Delegate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Delegate getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_Delegate_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Delegate parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Delegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Delegate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Delegate> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Delegate)) {
                    return super.equals(obj);
                }
                Delegate delegate = (Delegate) obj;
                if (getDelegatorAddress().equals(delegate.getDelegatorAddress()) && getValidatorAddress().equals(delegate.getValidatorAddress()) && hasAmount() == delegate.hasAmount()) {
                    return (!hasAmount() || getAmount().equals(delegate.getAmount())) && getTypePrefix().equals(delegate.getTypePrefix()) && this.unknownFields.equals(delegate.unknownFields);
                }
                return false;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public Amount getAmount() {
                Amount amount = this.amount_;
                return amount == null ? Amount.getDefaultInstance() : amount;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public AmountOrBuilder getAmountOrBuilder() {
                return getAmount();
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public String getDelegatorAddress() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.delegatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public ByteString getDelegatorAddressBytes() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Delegate> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getDelegatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegatorAddress_);
                if (!getValidatorAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.validatorAddress_);
                }
                if (this.amount_ != null) {
                    computeStringSize += CodedOutputStream.G(3, getAmount());
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(4, this.typePrefix_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public String getTypePrefix() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.typePrefix_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public ByteString getTypePrefixBytes() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.typePrefix_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public String getValidatorAddress() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.validatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public ByteString getValidatorAddressBytes() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.DelegateOrBuilder
            public boolean hasAmount() {
                return this.amount_ != null;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegatorAddress().hashCode()) * 37) + 2) * 53) + getValidatorAddress().hashCode();
                if (hasAmount()) {
                    hashCode = (((hashCode * 37) + 3) * 53) + getAmount().hashCode();
                }
                int hashCode2 = (((((hashCode * 37) + 4) * 53) + getTypePrefix().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_Delegate_fieldAccessorTable.d(Delegate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Delegate();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getDelegatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegatorAddress_);
                }
                if (!getValidatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.validatorAddress_);
                }
                if (this.amount_ != null) {
                    codedOutputStream.K0(3, getAmount());
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 4, this.typePrefix_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Delegate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Delegate delegate) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(delegate);
            }

            public static Delegate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Delegate(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Delegate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Delegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Delegate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Delegate getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Delegate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Delegate() {
                this.memoizedIsInitialized = (byte) -1;
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.typePrefix_ = "";
            }

            public static Delegate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Delegate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Delegate parseFrom(InputStream inputStream) throws IOException {
                return (Delegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Delegate parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Delegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private Delegate(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.delegatorAddress_ = jVar.I();
                                    } else if (J == 18) {
                                        this.validatorAddress_ = jVar.I();
                                    } else if (J == 26) {
                                        Amount amount = this.amount_;
                                        Amount.Builder builder = amount != null ? amount.toBuilder() : null;
                                        Amount amount2 = (Amount) jVar.z(Amount.parser(), rVar);
                                        this.amount_ = amount2;
                                        if (builder != null) {
                                            builder.mergeFrom(amount2);
                                            this.amount_ = builder.buildPartial();
                                        }
                                    } else if (J != 34) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.typePrefix_ = jVar.I();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Delegate parseFrom(j jVar) throws IOException {
                return (Delegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Delegate parseFrom(j jVar, r rVar) throws IOException {
                return (Delegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface DelegateOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            Amount getAmount();

            AmountOrBuilder getAmountOrBuilder();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            String getDelegatorAddress();

            ByteString getDelegatorAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getTypePrefix();

            ByteString getTypePrefixBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            String getValidatorAddress();

            ByteString getValidatorAddressBytes();

            boolean hasAmount();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            SEND_COINS_MESSAGE(1),
            STAKE_MESSAGE(2),
            UNSTAKE_MESSAGE(3),
            RESTAKE_MESSAGE(4),
            WITHDRAW_STAKE_REWARD_MESSAGE(5),
            RAW_JSON_MESSAGE(6),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                switch (i) {
                    case 0:
                        return MESSAGEONEOF_NOT_SET;
                    case 1:
                        return SEND_COINS_MESSAGE;
                    case 2:
                        return STAKE_MESSAGE;
                    case 3:
                        return UNSTAKE_MESSAGE;
                    case 4:
                        return RESTAKE_MESSAGE;
                    case 5:
                        return WITHDRAW_STAKE_REWARD_MESSAGE;
                    case 6:
                        return RAW_JSON_MESSAGE;
                    default:
                        return null;
                }
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class RawJSON extends GeneratedMessageV3 implements RawJSONOrBuilder {
            private static final RawJSON DEFAULT_INSTANCE = new RawJSON();
            private static final t0<RawJSON> PARSER = new c<RawJSON>() { // from class: wallet.core.jni.proto.Cosmos.Message.RawJSON.1
                @Override // com.google.protobuf.t0
                public RawJSON parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new RawJSON(jVar, rVar, null);
                }
            };
            public static final int TYPE_FIELD_NUMBER = 1;
            public static final int VALUE_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private volatile Object type_;
            private volatile Object value_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements RawJSONOrBuilder {
                private Object type_;
                private Object value_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_RawJSON_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearType() {
                    this.type_ = RawJSON.getDefaultInstance().getType();
                    onChanged();
                    return this;
                }

                public Builder clearValue() {
                    this.value_ = RawJSON.getDefaultInstance().getValue();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_RawJSON_descriptor;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
                public String getType() {
                    Object obj = this.type_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.type_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
                public ByteString getTypeBytes() {
                    Object obj = this.type_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.type_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
                public String getValue() {
                    Object obj = this.value_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.value_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
                public ByteString getValueBytes() {
                    Object obj = this.value_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.value_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_RawJSON_fieldAccessorTable.d(RawJSON.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setType(String str) {
                    Objects.requireNonNull(str);
                    this.type_ = str;
                    onChanged();
                    return this;
                }

                public Builder setTypeBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.type_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValue(String str) {
                    Objects.requireNonNull(str);
                    this.value_ = str;
                    onChanged();
                    return this;
                }

                public Builder setValueBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.value_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.type_ = "";
                    this.value_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public RawJSON build() {
                    RawJSON buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public RawJSON buildPartial() {
                    RawJSON rawJSON = new RawJSON(this, (AnonymousClass1) null);
                    rawJSON.type_ = this.type_;
                    rawJSON.value_ = this.value_;
                    onBuilt();
                    return rawJSON;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public RawJSON getDefaultInstanceForType() {
                    return RawJSON.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.type_ = "";
                    this.value_ = "";
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.type_ = "";
                    this.value_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof RawJSON) {
                        return mergeFrom((RawJSON) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(RawJSON rawJSON) {
                    if (rawJSON == RawJSON.getDefaultInstance()) {
                        return this;
                    }
                    if (!rawJSON.getType().isEmpty()) {
                        this.type_ = rawJSON.type_;
                        onChanged();
                    }
                    if (!rawJSON.getValue().isEmpty()) {
                        this.value_ = rawJSON.value_;
                        onChanged();
                    }
                    mergeUnknownFields(rawJSON.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Cosmos.Message.RawJSON.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Message.RawJSON.access$11700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Cosmos$Message$RawJSON r3 = (wallet.core.jni.proto.Cosmos.Message.RawJSON) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Cosmos$Message$RawJSON r4 = (wallet.core.jni.proto.Cosmos.Message.RawJSON) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Message.RawJSON.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Message$RawJSON$Builder");
                }
            }

            public /* synthetic */ RawJSON(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static RawJSON getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_RawJSON_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static RawJSON parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (RawJSON) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static RawJSON parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<RawJSON> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof RawJSON)) {
                    return super.equals(obj);
                }
                RawJSON rawJSON = (RawJSON) obj;
                return getType().equals(rawJSON.getType()) && getValue().equals(rawJSON.getValue()) && this.unknownFields.equals(rawJSON.unknownFields);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<RawJSON> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getTypeBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.type_);
                if (!getValueBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.value_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
            public String getType() {
                Object obj = this.type_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.type_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
            public ByteString getTypeBytes() {
                Object obj = this.type_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.type_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
            public String getValue() {
                Object obj = this.value_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.value_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.RawJSONOrBuilder
            public ByteString getValueBytes() {
                Object obj = this.value_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.value_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getType().hashCode()) * 37) + 2) * 53) + getValue().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_RawJSON_fieldAccessorTable.d(RawJSON.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new RawJSON();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getTypeBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.type_);
                }
                if (!getValueBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.value_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ RawJSON(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(RawJSON rawJSON) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(rawJSON);
            }

            public static RawJSON parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private RawJSON(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static RawJSON parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (RawJSON) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static RawJSON parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public RawJSON getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static RawJSON parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private RawJSON() {
                this.memoizedIsInitialized = (byte) -1;
                this.type_ = "";
                this.value_ = "";
            }

            public static RawJSON parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static RawJSON parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static RawJSON parseFrom(InputStream inputStream) throws IOException {
                return (RawJSON) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private RawJSON(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.type_ = jVar.I();
                                    } else if (J != 18) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.value_ = jVar.I();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static RawJSON parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (RawJSON) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static RawJSON parseFrom(j jVar) throws IOException {
                return (RawJSON) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static RawJSON parseFrom(j jVar, r rVar) throws IOException {
                return (RawJSON) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface RawJSONOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getType();

            ByteString getTypeBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            String getValue();

            ByteString getValueBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Send extends GeneratedMessageV3 implements SendOrBuilder {
            public static final int AMOUNTS_FIELD_NUMBER = 3;
            public static final int FROM_ADDRESS_FIELD_NUMBER = 1;
            public static final int TO_ADDRESS_FIELD_NUMBER = 2;
            public static final int TYPE_PREFIX_FIELD_NUMBER = 4;
            private static final long serialVersionUID = 0;
            private List<Amount> amounts_;
            private volatile Object fromAddress_;
            private byte memoizedIsInitialized;
            private volatile Object toAddress_;
            private volatile Object typePrefix_;
            private static final Send DEFAULT_INSTANCE = new Send();
            private static final t0<Send> PARSER = new c<Send>() { // from class: wallet.core.jni.proto.Cosmos.Message.Send.1
                @Override // com.google.protobuf.t0
                public Send parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Send(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements SendOrBuilder {
                private x0<Amount, Amount.Builder, AmountOrBuilder> amountsBuilder_;
                private List<Amount> amounts_;
                private int bitField0_;
                private Object fromAddress_;
                private Object toAddress_;
                private Object typePrefix_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private void ensureAmountsIsMutable() {
                    if ((this.bitField0_ & 1) == 0) {
                        this.amounts_ = new ArrayList(this.amounts_);
                        this.bitField0_ |= 1;
                    }
                }

                private x0<Amount, Amount.Builder, AmountOrBuilder> getAmountsFieldBuilder() {
                    if (this.amountsBuilder_ == null) {
                        this.amountsBuilder_ = new x0<>(this.amounts_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                        this.amounts_ = null;
                    }
                    return this.amountsBuilder_;
                }

                public static final Descriptors.b getDescriptor() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Send_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                        getAmountsFieldBuilder();
                    }
                }

                public Builder addAllAmounts(Iterable<? extends Amount> iterable) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        ensureAmountsIsMutable();
                        b.a.addAll((Iterable) iterable, (List) this.amounts_);
                        onChanged();
                    } else {
                        x0Var.b(iterable);
                    }
                    return this;
                }

                public Builder addAmounts(Amount amount) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        Objects.requireNonNull(amount);
                        ensureAmountsIsMutable();
                        this.amounts_.add(amount);
                        onChanged();
                    } else {
                        x0Var.f(amount);
                    }
                    return this;
                }

                public Amount.Builder addAmountsBuilder() {
                    return getAmountsFieldBuilder().d(Amount.getDefaultInstance());
                }

                public Builder clearAmounts() {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        this.amounts_ = Collections.emptyList();
                        this.bitField0_ &= -2;
                        onChanged();
                    } else {
                        x0Var.h();
                    }
                    return this;
                }

                public Builder clearFromAddress() {
                    this.fromAddress_ = Send.getDefaultInstance().getFromAddress();
                    onChanged();
                    return this;
                }

                public Builder clearToAddress() {
                    this.toAddress_ = Send.getDefaultInstance().getToAddress();
                    onChanged();
                    return this;
                }

                public Builder clearTypePrefix() {
                    this.typePrefix_ = Send.getDefaultInstance().getTypePrefix();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public Amount getAmounts(int i) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        return this.amounts_.get(i);
                    }
                    return x0Var.o(i);
                }

                public Amount.Builder getAmountsBuilder(int i) {
                    return getAmountsFieldBuilder().l(i);
                }

                public List<Amount.Builder> getAmountsBuilderList() {
                    return getAmountsFieldBuilder().m();
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public int getAmountsCount() {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        return this.amounts_.size();
                    }
                    return x0Var.n();
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public List<Amount> getAmountsList() {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        return Collections.unmodifiableList(this.amounts_);
                    }
                    return x0Var.q();
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public AmountOrBuilder getAmountsOrBuilder(int i) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        return this.amounts_.get(i);
                    }
                    return x0Var.r(i);
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public List<? extends AmountOrBuilder> getAmountsOrBuilderList() {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var != null) {
                        return x0Var.s();
                    }
                    return Collections.unmodifiableList(this.amounts_);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Send_descriptor;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public String getFromAddress() {
                    Object obj = this.fromAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.fromAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public ByteString getFromAddressBytes() {
                    Object obj = this.fromAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.fromAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public String getToAddress() {
                    Object obj = this.toAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.toAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public ByteString getToAddressBytes() {
                    Object obj = this.toAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.toAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public String getTypePrefix() {
                    Object obj = this.typePrefix_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.typePrefix_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
                public ByteString getTypePrefixBytes() {
                    Object obj = this.typePrefix_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.typePrefix_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Send_fieldAccessorTable.d(Send.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder removeAmounts(int i) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        ensureAmountsIsMutable();
                        this.amounts_.remove(i);
                        onChanged();
                    } else {
                        x0Var.w(i);
                    }
                    return this;
                }

                public Builder setAmounts(int i, Amount amount) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        Objects.requireNonNull(amount);
                        ensureAmountsIsMutable();
                        this.amounts_.set(i, amount);
                        onChanged();
                    } else {
                        x0Var.x(i, amount);
                    }
                    return this;
                }

                public Builder setFromAddress(String str) {
                    Objects.requireNonNull(str);
                    this.fromAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setFromAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.fromAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setToAddress(String str) {
                    Objects.requireNonNull(str);
                    this.toAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setToAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.toAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefix(String str) {
                    Objects.requireNonNull(str);
                    this.typePrefix_ = str;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefixBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.typePrefix_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.fromAddress_ = "";
                    this.toAddress_ = "";
                    this.amounts_ = Collections.emptyList();
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Send build() {
                    Send buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Send buildPartial() {
                    Send send = new Send(this, (AnonymousClass1) null);
                    send.fromAddress_ = this.fromAddress_;
                    send.toAddress_ = this.toAddress_;
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var != null) {
                        send.amounts_ = x0Var.g();
                    } else {
                        if ((this.bitField0_ & 1) != 0) {
                            this.amounts_ = Collections.unmodifiableList(this.amounts_);
                            this.bitField0_ &= -2;
                        }
                        send.amounts_ = this.amounts_;
                    }
                    send.typePrefix_ = this.typePrefix_;
                    onBuilt();
                    return send;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Send getDefaultInstanceForType() {
                    return Send.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                public Amount.Builder addAmountsBuilder(int i) {
                    return getAmountsFieldBuilder().c(i, Amount.getDefaultInstance());
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.fromAddress_ = "";
                    this.toAddress_ = "";
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        this.amounts_ = Collections.emptyList();
                        this.bitField0_ &= -2;
                    } else {
                        x0Var.h();
                    }
                    this.typePrefix_ = "";
                    return this;
                }

                public Builder addAmounts(int i, Amount amount) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        Objects.requireNonNull(amount);
                        ensureAmountsIsMutable();
                        this.amounts_.add(i, amount);
                        onChanged();
                    } else {
                        x0Var.e(i, amount);
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Send) {
                        return mergeFrom((Send) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder setAmounts(int i, Amount.Builder builder) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        ensureAmountsIsMutable();
                        this.amounts_.set(i, builder.build());
                        onChanged();
                    } else {
                        x0Var.x(i, builder.build());
                    }
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.fromAddress_ = "";
                    this.toAddress_ = "";
                    this.amounts_ = Collections.emptyList();
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(Send send) {
                    if (send == Send.getDefaultInstance()) {
                        return this;
                    }
                    if (!send.getFromAddress().isEmpty()) {
                        this.fromAddress_ = send.fromAddress_;
                        onChanged();
                    }
                    if (!send.getToAddress().isEmpty()) {
                        this.toAddress_ = send.toAddress_;
                        onChanged();
                    }
                    if (this.amountsBuilder_ == null) {
                        if (!send.amounts_.isEmpty()) {
                            if (this.amounts_.isEmpty()) {
                                this.amounts_ = send.amounts_;
                                this.bitField0_ &= -2;
                            } else {
                                ensureAmountsIsMutable();
                                this.amounts_.addAll(send.amounts_);
                            }
                            onChanged();
                        }
                    } else if (!send.amounts_.isEmpty()) {
                        if (!this.amountsBuilder_.u()) {
                            this.amountsBuilder_.b(send.amounts_);
                        } else {
                            this.amountsBuilder_.i();
                            this.amountsBuilder_ = null;
                            this.amounts_ = send.amounts_;
                            this.bitField0_ &= -2;
                            this.amountsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getAmountsFieldBuilder() : null;
                        }
                    }
                    if (!send.getTypePrefix().isEmpty()) {
                        this.typePrefix_ = send.typePrefix_;
                        onChanged();
                    }
                    mergeUnknownFields(send.unknownFields);
                    onChanged();
                    return this;
                }

                public Builder addAmounts(Amount.Builder builder) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        ensureAmountsIsMutable();
                        this.amounts_.add(builder.build());
                        onChanged();
                    } else {
                        x0Var.f(builder.build());
                    }
                    return this;
                }

                public Builder addAmounts(int i, Amount.Builder builder) {
                    x0<Amount, Amount.Builder, AmountOrBuilder> x0Var = this.amountsBuilder_;
                    if (x0Var == null) {
                        ensureAmountsIsMutable();
                        this.amounts_.add(i, builder.build());
                        onChanged();
                    } else {
                        x0Var.e(i, builder.build());
                    }
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Cosmos.Message.Send.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Message.Send.access$3800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Cosmos$Message$Send r3 = (wallet.core.jni.proto.Cosmos.Message.Send) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Cosmos$Message$Send r4 = (wallet.core.jni.proto.Cosmos.Message.Send) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Message.Send.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Message$Send$Builder");
                }
            }

            public /* synthetic */ Send(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Send getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_Send_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Send parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Send) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Send parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Send> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Send)) {
                    return super.equals(obj);
                }
                Send send = (Send) obj;
                return getFromAddress().equals(send.getFromAddress()) && getToAddress().equals(send.getToAddress()) && getAmountsList().equals(send.getAmountsList()) && getTypePrefix().equals(send.getTypePrefix()) && this.unknownFields.equals(send.unknownFields);
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public Amount getAmounts(int i) {
                return this.amounts_.get(i);
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public int getAmountsCount() {
                return this.amounts_.size();
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public List<Amount> getAmountsList() {
                return this.amounts_;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public AmountOrBuilder getAmountsOrBuilder(int i) {
                return this.amounts_.get(i);
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public List<? extends AmountOrBuilder> getAmountsOrBuilderList() {
                return this.amounts_;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public String getFromAddress() {
                Object obj = this.fromAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.fromAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public ByteString getFromAddressBytes() {
                Object obj = this.fromAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.fromAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Send> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = !getFromAddressBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.fromAddress_) + 0 : 0;
                if (!getToAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.toAddress_);
                }
                for (int i2 = 0; i2 < this.amounts_.size(); i2++) {
                    computeStringSize += CodedOutputStream.G(3, this.amounts_.get(i2));
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(4, this.typePrefix_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.toAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public String getTypePrefix() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.typePrefix_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.SendOrBuilder
            public ByteString getTypePrefixBytes() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.typePrefix_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFromAddress().hashCode()) * 37) + 2) * 53) + getToAddress().hashCode();
                if (getAmountsCount() > 0) {
                    hashCode = (((hashCode * 37) + 3) * 53) + getAmountsList().hashCode();
                }
                int hashCode2 = (((((hashCode * 37) + 4) * 53) + getTypePrefix().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_Send_fieldAccessorTable.d(Send.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Send();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getFromAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.fromAddress_);
                }
                if (!getToAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.toAddress_);
                }
                for (int i = 0; i < this.amounts_.size(); i++) {
                    codedOutputStream.K0(3, this.amounts_.get(i));
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 4, this.typePrefix_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Send(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Send send) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(send);
            }

            public static Send parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Send(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Send parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Send) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Send parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Send getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Send parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Send() {
                this.memoizedIsInitialized = (byte) -1;
                this.fromAddress_ = "";
                this.toAddress_ = "";
                this.amounts_ = Collections.emptyList();
                this.typePrefix_ = "";
            }

            public static Send parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Send parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Send parseFrom(InputStream inputStream) throws IOException {
                return (Send) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Send parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Send) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            /* JADX WARN: Multi-variable type inference failed */
            private Send(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                boolean z2 = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.fromAddress_ = jVar.I();
                                } else if (J == 18) {
                                    this.toAddress_ = jVar.I();
                                } else if (J == 26) {
                                    if (!(z2 & true)) {
                                        this.amounts_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.amounts_.add(jVar.z(Amount.parser(), rVar));
                                } else if (J != 34) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.typePrefix_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        if (z2 & true) {
                            this.amounts_ = Collections.unmodifiableList(this.amounts_);
                        }
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Send parseFrom(j jVar) throws IOException {
                return (Send) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Send parseFrom(j jVar, r rVar) throws IOException {
                return (Send) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface SendOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            Amount getAmounts(int i);

            int getAmountsCount();

            List<Amount> getAmountsList();

            AmountOrBuilder getAmountsOrBuilder(int i);

            List<? extends AmountOrBuilder> getAmountsOrBuilderList();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            String getFromAddress();

            ByteString getFromAddressBytes();

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getToAddress();

            ByteString getToAddressBytes();

            String getTypePrefix();

            ByteString getTypePrefixBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Undelegate extends GeneratedMessageV3 implements UndelegateOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 3;
            public static final int DELEGATOR_ADDRESS_FIELD_NUMBER = 1;
            public static final int TYPE_PREFIX_FIELD_NUMBER = 4;
            public static final int VALIDATOR_ADDRESS_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private Amount amount_;
            private volatile Object delegatorAddress_;
            private byte memoizedIsInitialized;
            private volatile Object typePrefix_;
            private volatile Object validatorAddress_;
            private static final Undelegate DEFAULT_INSTANCE = new Undelegate();
            private static final t0<Undelegate> PARSER = new c<Undelegate>() { // from class: wallet.core.jni.proto.Cosmos.Message.Undelegate.1
                @Override // com.google.protobuf.t0
                public Undelegate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Undelegate(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements UndelegateOrBuilder {
                private a1<Amount, Amount.Builder, AmountOrBuilder> amountBuilder_;
                private Amount amount_;
                private Object delegatorAddress_;
                private Object typePrefix_;
                private Object validatorAddress_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private a1<Amount, Amount.Builder, AmountOrBuilder> getAmountFieldBuilder() {
                    if (this.amountBuilder_ == null) {
                        this.amountBuilder_ = new a1<>(getAmount(), getParentForChildren(), isClean());
                        this.amount_ = null;
                    }
                    return this.amountBuilder_;
                }

                public static final Descriptors.b getDescriptor() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Undelegate_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    if (this.amountBuilder_ == null) {
                        this.amount_ = null;
                        onChanged();
                    } else {
                        this.amount_ = null;
                        this.amountBuilder_ = null;
                    }
                    return this;
                }

                public Builder clearDelegatorAddress() {
                    this.delegatorAddress_ = Undelegate.getDefaultInstance().getDelegatorAddress();
                    onChanged();
                    return this;
                }

                public Builder clearTypePrefix() {
                    this.typePrefix_ = Undelegate.getDefaultInstance().getTypePrefix();
                    onChanged();
                    return this;
                }

                public Builder clearValidatorAddress() {
                    this.validatorAddress_ = Undelegate.getDefaultInstance().getValidatorAddress();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public Amount getAmount() {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Amount amount = this.amount_;
                        return amount == null ? Amount.getDefaultInstance() : amount;
                    }
                    return a1Var.f();
                }

                public Amount.Builder getAmountBuilder() {
                    onChanged();
                    return getAmountFieldBuilder().e();
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public AmountOrBuilder getAmountOrBuilder() {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var != null) {
                        return a1Var.g();
                    }
                    Amount amount = this.amount_;
                    return amount == null ? Amount.getDefaultInstance() : amount;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public String getDelegatorAddress() {
                    Object obj = this.delegatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.delegatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public ByteString getDelegatorAddressBytes() {
                    Object obj = this.delegatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.delegatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Undelegate_descriptor;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public String getTypePrefix() {
                    Object obj = this.typePrefix_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.typePrefix_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public ByteString getTypePrefixBytes() {
                    Object obj = this.typePrefix_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.typePrefix_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public String getValidatorAddress() {
                    Object obj = this.validatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.validatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public ByteString getValidatorAddressBytes() {
                    Object obj = this.validatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.validatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
                public boolean hasAmount() {
                    return (this.amountBuilder_ == null && this.amount_ == null) ? false : true;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_Undelegate_fieldAccessorTable.d(Undelegate.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder mergeAmount(Amount amount) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Amount amount2 = this.amount_;
                        if (amount2 != null) {
                            this.amount_ = Amount.newBuilder(amount2).mergeFrom(amount).buildPartial();
                        } else {
                            this.amount_ = amount;
                        }
                        onChanged();
                    } else {
                        a1Var.h(amount);
                    }
                    return this;
                }

                public Builder setAmount(Amount amount) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        Objects.requireNonNull(amount);
                        this.amount_ = amount;
                        onChanged();
                    } else {
                        a1Var.j(amount);
                    }
                    return this;
                }

                public Builder setDelegatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.delegatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setDelegatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.delegatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefix(String str) {
                    Objects.requireNonNull(str);
                    this.typePrefix_ = str;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefixBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.typePrefix_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValidatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.validatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setValidatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.validatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Undelegate build() {
                    Undelegate buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Undelegate buildPartial() {
                    Undelegate undelegate = new Undelegate(this, (AnonymousClass1) null);
                    undelegate.delegatorAddress_ = this.delegatorAddress_;
                    undelegate.validatorAddress_ = this.validatorAddress_;
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        undelegate.amount_ = this.amount_;
                    } else {
                        undelegate.amount_ = a1Var.b();
                    }
                    undelegate.typePrefix_ = this.typePrefix_;
                    onBuilt();
                    return undelegate;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Undelegate getDefaultInstanceForType() {
                    return Undelegate.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    if (this.amountBuilder_ == null) {
                        this.amount_ = null;
                    } else {
                        this.amount_ = null;
                        this.amountBuilder_ = null;
                    }
                    this.typePrefix_ = "";
                    return this;
                }

                public Builder setAmount(Amount.Builder builder) {
                    a1<Amount, Amount.Builder, AmountOrBuilder> a1Var = this.amountBuilder_;
                    if (a1Var == null) {
                        this.amount_ = builder.build();
                        onChanged();
                    } else {
                        a1Var.j(builder.build());
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Undelegate) {
                        return mergeFrom((Undelegate) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(Undelegate undelegate) {
                    if (undelegate == Undelegate.getDefaultInstance()) {
                        return this;
                    }
                    if (!undelegate.getDelegatorAddress().isEmpty()) {
                        this.delegatorAddress_ = undelegate.delegatorAddress_;
                        onChanged();
                    }
                    if (!undelegate.getValidatorAddress().isEmpty()) {
                        this.validatorAddress_ = undelegate.validatorAddress_;
                        onChanged();
                    }
                    if (undelegate.hasAmount()) {
                        mergeAmount(undelegate.getAmount());
                    }
                    if (!undelegate.getTypePrefix().isEmpty()) {
                        this.typePrefix_ = undelegate.typePrefix_;
                        onChanged();
                    }
                    mergeUnknownFields(undelegate.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Cosmos.Message.Undelegate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Message.Undelegate.access$7000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Cosmos$Message$Undelegate r3 = (wallet.core.jni.proto.Cosmos.Message.Undelegate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Cosmos$Message$Undelegate r4 = (wallet.core.jni.proto.Cosmos.Message.Undelegate) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Message.Undelegate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Message$Undelegate$Builder");
                }
            }

            public /* synthetic */ Undelegate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Undelegate getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_Undelegate_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Undelegate parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Undelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Undelegate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Undelegate> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Undelegate)) {
                    return super.equals(obj);
                }
                Undelegate undelegate = (Undelegate) obj;
                if (getDelegatorAddress().equals(undelegate.getDelegatorAddress()) && getValidatorAddress().equals(undelegate.getValidatorAddress()) && hasAmount() == undelegate.hasAmount()) {
                    return (!hasAmount() || getAmount().equals(undelegate.getAmount())) && getTypePrefix().equals(undelegate.getTypePrefix()) && this.unknownFields.equals(undelegate.unknownFields);
                }
                return false;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public Amount getAmount() {
                Amount amount = this.amount_;
                return amount == null ? Amount.getDefaultInstance() : amount;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public AmountOrBuilder getAmountOrBuilder() {
                return getAmount();
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public String getDelegatorAddress() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.delegatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public ByteString getDelegatorAddressBytes() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Undelegate> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getDelegatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegatorAddress_);
                if (!getValidatorAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.validatorAddress_);
                }
                if (this.amount_ != null) {
                    computeStringSize += CodedOutputStream.G(3, getAmount());
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(4, this.typePrefix_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public String getTypePrefix() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.typePrefix_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public ByteString getTypePrefixBytes() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.typePrefix_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public String getValidatorAddress() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.validatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public ByteString getValidatorAddressBytes() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.UndelegateOrBuilder
            public boolean hasAmount() {
                return this.amount_ != null;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegatorAddress().hashCode()) * 37) + 2) * 53) + getValidatorAddress().hashCode();
                if (hasAmount()) {
                    hashCode = (((hashCode * 37) + 3) * 53) + getAmount().hashCode();
                }
                int hashCode2 = (((((hashCode * 37) + 4) * 53) + getTypePrefix().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_Undelegate_fieldAccessorTable.d(Undelegate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Undelegate();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getDelegatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegatorAddress_);
                }
                if (!getValidatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.validatorAddress_);
                }
                if (this.amount_ != null) {
                    codedOutputStream.K0(3, getAmount());
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 4, this.typePrefix_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Undelegate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Undelegate undelegate) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(undelegate);
            }

            public static Undelegate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Undelegate(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Undelegate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Undelegate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Undelegate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Undelegate getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Undelegate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Undelegate() {
                this.memoizedIsInitialized = (byte) -1;
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.typePrefix_ = "";
            }

            public static Undelegate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Undelegate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Undelegate parseFrom(InputStream inputStream) throws IOException {
                return (Undelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Undelegate parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Undelegate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private Undelegate(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.delegatorAddress_ = jVar.I();
                                    } else if (J == 18) {
                                        this.validatorAddress_ = jVar.I();
                                    } else if (J == 26) {
                                        Amount amount = this.amount_;
                                        Amount.Builder builder = amount != null ? amount.toBuilder() : null;
                                        Amount amount2 = (Amount) jVar.z(Amount.parser(), rVar);
                                        this.amount_ = amount2;
                                        if (builder != null) {
                                            builder.mergeFrom(amount2);
                                            this.amount_ = builder.buildPartial();
                                        }
                                    } else if (J != 34) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.typePrefix_ = jVar.I();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Undelegate parseFrom(j jVar) throws IOException {
                return (Undelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Undelegate parseFrom(j jVar, r rVar) throws IOException {
                return (Undelegate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface UndelegateOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            Amount getAmount();

            AmountOrBuilder getAmountOrBuilder();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            String getDelegatorAddress();

            ByteString getDelegatorAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getTypePrefix();

            ByteString getTypePrefixBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            String getValidatorAddress();

            ByteString getValidatorAddressBytes();

            boolean hasAmount();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class WithdrawDelegationReward extends GeneratedMessageV3 implements WithdrawDelegationRewardOrBuilder {
            public static final int DELEGATOR_ADDRESS_FIELD_NUMBER = 1;
            public static final int TYPE_PREFIX_FIELD_NUMBER = 3;
            public static final int VALIDATOR_ADDRESS_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private volatile Object delegatorAddress_;
            private byte memoizedIsInitialized;
            private volatile Object typePrefix_;
            private volatile Object validatorAddress_;
            private static final WithdrawDelegationReward DEFAULT_INSTANCE = new WithdrawDelegationReward();
            private static final t0<WithdrawDelegationReward> PARSER = new c<WithdrawDelegationReward>() { // from class: wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationReward.1
                @Override // com.google.protobuf.t0
                public WithdrawDelegationReward parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new WithdrawDelegationReward(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements WithdrawDelegationRewardOrBuilder {
                private Object delegatorAddress_;
                private Object typePrefix_;
                private Object validatorAddress_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearDelegatorAddress() {
                    this.delegatorAddress_ = WithdrawDelegationReward.getDefaultInstance().getDelegatorAddress();
                    onChanged();
                    return this;
                }

                public Builder clearTypePrefix() {
                    this.typePrefix_ = WithdrawDelegationReward.getDefaultInstance().getTypePrefix();
                    onChanged();
                    return this;
                }

                public Builder clearValidatorAddress() {
                    this.validatorAddress_ = WithdrawDelegationReward.getDefaultInstance().getValidatorAddress();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
                public String getDelegatorAddress() {
                    Object obj = this.delegatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.delegatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
                public ByteString getDelegatorAddressBytes() {
                    Object obj = this.delegatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.delegatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_descriptor;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
                public String getTypePrefix() {
                    Object obj = this.typePrefix_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.typePrefix_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
                public ByteString getTypePrefixBytes() {
                    Object obj = this.typePrefix_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.typePrefix_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
                public String getValidatorAddress() {
                    Object obj = this.validatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.validatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
                public ByteString getValidatorAddressBytes() {
                    Object obj = this.validatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.validatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Cosmos.internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_fieldAccessorTable.d(WithdrawDelegationReward.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setDelegatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.delegatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setDelegatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.delegatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefix(String str) {
                    Objects.requireNonNull(str);
                    this.typePrefix_ = str;
                    onChanged();
                    return this;
                }

                public Builder setTypePrefixBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.typePrefix_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValidatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.validatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setValidatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.validatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public WithdrawDelegationReward build() {
                    WithdrawDelegationReward buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public WithdrawDelegationReward buildPartial() {
                    WithdrawDelegationReward withdrawDelegationReward = new WithdrawDelegationReward(this, (AnonymousClass1) null);
                    withdrawDelegationReward.delegatorAddress_ = this.delegatorAddress_;
                    withdrawDelegationReward.validatorAddress_ = this.validatorAddress_;
                    withdrawDelegationReward.typePrefix_ = this.typePrefix_;
                    onBuilt();
                    return withdrawDelegationReward;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public WithdrawDelegationReward getDefaultInstanceForType() {
                    return WithdrawDelegationReward.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    this.typePrefix_ = "";
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof WithdrawDelegationReward) {
                        return mergeFrom((WithdrawDelegationReward) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.delegatorAddress_ = "";
                    this.validatorAddress_ = "";
                    this.typePrefix_ = "";
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(WithdrawDelegationReward withdrawDelegationReward) {
                    if (withdrawDelegationReward == WithdrawDelegationReward.getDefaultInstance()) {
                        return this;
                    }
                    if (!withdrawDelegationReward.getDelegatorAddress().isEmpty()) {
                        this.delegatorAddress_ = withdrawDelegationReward.delegatorAddress_;
                        onChanged();
                    }
                    if (!withdrawDelegationReward.getValidatorAddress().isEmpty()) {
                        this.validatorAddress_ = withdrawDelegationReward.validatorAddress_;
                        onChanged();
                    }
                    if (!withdrawDelegationReward.getTypePrefix().isEmpty()) {
                        this.typePrefix_ = withdrawDelegationReward.typePrefix_;
                        onChanged();
                    }
                    mergeUnknownFields(withdrawDelegationReward.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationReward.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationReward.access$10300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Cosmos$Message$WithdrawDelegationReward r3 = (wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationReward) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Cosmos$Message$WithdrawDelegationReward r4 = (wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationReward) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationReward.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$Message$WithdrawDelegationReward$Builder");
                }
            }

            public /* synthetic */ WithdrawDelegationReward(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static WithdrawDelegationReward getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static WithdrawDelegationReward parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (WithdrawDelegationReward) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static WithdrawDelegationReward parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<WithdrawDelegationReward> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof WithdrawDelegationReward)) {
                    return super.equals(obj);
                }
                WithdrawDelegationReward withdrawDelegationReward = (WithdrawDelegationReward) obj;
                return getDelegatorAddress().equals(withdrawDelegationReward.getDelegatorAddress()) && getValidatorAddress().equals(withdrawDelegationReward.getValidatorAddress()) && getTypePrefix().equals(withdrawDelegationReward.getTypePrefix()) && this.unknownFields.equals(withdrawDelegationReward.unknownFields);
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
            public String getDelegatorAddress() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.delegatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
            public ByteString getDelegatorAddressBytes() {
                Object obj = this.delegatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<WithdrawDelegationReward> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getDelegatorAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegatorAddress_);
                if (!getValidatorAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.validatorAddress_);
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(3, this.typePrefix_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
            public String getTypePrefix() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.typePrefix_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
            public ByteString getTypePrefixBytes() {
                Object obj = this.typePrefix_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.typePrefix_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
            public String getValidatorAddress() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.validatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Cosmos.Message.WithdrawDelegationRewardOrBuilder
            public ByteString getValidatorAddressBytes() {
                Object obj = this.validatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.validatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegatorAddress().hashCode()) * 37) + 2) * 53) + getValidatorAddress().hashCode()) * 37) + 3) * 53) + getTypePrefix().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_fieldAccessorTable.d(WithdrawDelegationReward.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new WithdrawDelegationReward();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getDelegatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegatorAddress_);
                }
                if (!getValidatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.validatorAddress_);
                }
                if (!getTypePrefixBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 3, this.typePrefix_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ WithdrawDelegationReward(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(WithdrawDelegationReward withdrawDelegationReward) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(withdrawDelegationReward);
            }

            public static WithdrawDelegationReward parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private WithdrawDelegationReward(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static WithdrawDelegationReward parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (WithdrawDelegationReward) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static WithdrawDelegationReward parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public WithdrawDelegationReward getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static WithdrawDelegationReward parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private WithdrawDelegationReward() {
                this.memoizedIsInitialized = (byte) -1;
                this.delegatorAddress_ = "";
                this.validatorAddress_ = "";
                this.typePrefix_ = "";
            }

            public static WithdrawDelegationReward parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static WithdrawDelegationReward parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static WithdrawDelegationReward parseFrom(InputStream inputStream) throws IOException {
                return (WithdrawDelegationReward) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static WithdrawDelegationReward parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (WithdrawDelegationReward) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private WithdrawDelegationReward(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.delegatorAddress_ = jVar.I();
                                } else if (J == 18) {
                                    this.validatorAddress_ = jVar.I();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.typePrefix_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static WithdrawDelegationReward parseFrom(j jVar) throws IOException {
                return (WithdrawDelegationReward) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static WithdrawDelegationReward parseFrom(j jVar, r rVar) throws IOException {
                return (WithdrawDelegationReward) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface WithdrawDelegationRewardOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            String getDelegatorAddress();

            ByteString getDelegatorAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getTypePrefix();

            ByteString getTypePrefixBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            String getValidatorAddress();

            ByteString getValidatorAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ Message(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Message getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Cosmos.internal_static_TW_Cosmos_Proto_Message_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Message parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Message) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Message parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Message> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Message)) {
                return super.equals(obj);
            }
            Message message = (Message) obj;
            if (getMessageOneofCase().equals(message.getMessageOneofCase())) {
                switch (this.messageOneofCase_) {
                    case 1:
                        if (!getSendCoinsMessage().equals(message.getSendCoinsMessage())) {
                            return false;
                        }
                        break;
                    case 2:
                        if (!getStakeMessage().equals(message.getStakeMessage())) {
                            return false;
                        }
                        break;
                    case 3:
                        if (!getUnstakeMessage().equals(message.getUnstakeMessage())) {
                            return false;
                        }
                        break;
                    case 4:
                        if (!getRestakeMessage().equals(message.getRestakeMessage())) {
                            return false;
                        }
                        break;
                    case 5:
                        if (!getWithdrawStakeRewardMessage().equals(message.getWithdrawStakeRewardMessage())) {
                            return false;
                        }
                        break;
                    case 6:
                        if (!getRawJsonMessage().equals(message.getRawJsonMessage())) {
                            return false;
                        }
                        break;
                }
                return this.unknownFields.equals(message.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Message> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public RawJSON getRawJsonMessage() {
            if (this.messageOneofCase_ == 6) {
                return (RawJSON) this.messageOneof_;
            }
            return RawJSON.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public RawJSONOrBuilder getRawJsonMessageOrBuilder() {
            if (this.messageOneofCase_ == 6) {
                return (RawJSON) this.messageOneof_;
            }
            return RawJSON.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public BeginRedelegate getRestakeMessage() {
            if (this.messageOneofCase_ == 4) {
                return (BeginRedelegate) this.messageOneof_;
            }
            return BeginRedelegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public BeginRedelegateOrBuilder getRestakeMessageOrBuilder() {
            if (this.messageOneofCase_ == 4) {
                return (BeginRedelegate) this.messageOneof_;
            }
            return BeginRedelegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public Send getSendCoinsMessage() {
            if (this.messageOneofCase_ == 1) {
                return (Send) this.messageOneof_;
            }
            return Send.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public SendOrBuilder getSendCoinsMessageOrBuilder() {
            if (this.messageOneofCase_ == 1) {
                return (Send) this.messageOneof_;
            }
            return Send.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.messageOneofCase_ == 1 ? 0 + CodedOutputStream.G(1, (Send) this.messageOneof_) : 0;
            if (this.messageOneofCase_ == 2) {
                G += CodedOutputStream.G(2, (Delegate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 3) {
                G += CodedOutputStream.G(3, (Undelegate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                G += CodedOutputStream.G(4, (BeginRedelegate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                G += CodedOutputStream.G(5, (WithdrawDelegationReward) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 6) {
                G += CodedOutputStream.G(6, (RawJSON) this.messageOneof_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public Delegate getStakeMessage() {
            if (this.messageOneofCase_ == 2) {
                return (Delegate) this.messageOneof_;
            }
            return Delegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public DelegateOrBuilder getStakeMessageOrBuilder() {
            if (this.messageOneofCase_ == 2) {
                return (Delegate) this.messageOneof_;
            }
            return Delegate.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public Undelegate getUnstakeMessage() {
            if (this.messageOneofCase_ == 3) {
                return (Undelegate) this.messageOneof_;
            }
            return Undelegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public UndelegateOrBuilder getUnstakeMessageOrBuilder() {
            if (this.messageOneofCase_ == 3) {
                return (Undelegate) this.messageOneof_;
            }
            return Undelegate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public WithdrawDelegationReward getWithdrawStakeRewardMessage() {
            if (this.messageOneofCase_ == 5) {
                return (WithdrawDelegationReward) this.messageOneof_;
            }
            return WithdrawDelegationReward.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public WithdrawDelegationRewardOrBuilder getWithdrawStakeRewardMessageOrBuilder() {
            if (this.messageOneofCase_ == 5) {
                return (WithdrawDelegationReward) this.messageOneof_;
            }
            return WithdrawDelegationReward.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public boolean hasRawJsonMessage() {
            return this.messageOneofCase_ == 6;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public boolean hasRestakeMessage() {
            return this.messageOneofCase_ == 4;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public boolean hasSendCoinsMessage() {
            return this.messageOneofCase_ == 1;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public boolean hasStakeMessage() {
            return this.messageOneofCase_ == 2;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public boolean hasUnstakeMessage() {
            return this.messageOneofCase_ == 3;
        }

        @Override // wallet.core.jni.proto.Cosmos.MessageOrBuilder
        public boolean hasWithdrawStakeRewardMessage() {
            return this.messageOneofCase_ == 5;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = 779 + getDescriptor().hashCode();
            switch (this.messageOneofCase_) {
                case 1:
                    i = ((hashCode2 * 37) + 1) * 53;
                    hashCode = getSendCoinsMessage().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3;
                    return hashCode3;
                case 2:
                    i = ((hashCode2 * 37) + 2) * 53;
                    hashCode = getStakeMessage().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32;
                    return hashCode32;
                case 3:
                    i = ((hashCode2 * 37) + 3) * 53;
                    hashCode = getUnstakeMessage().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322;
                    return hashCode322;
                case 4:
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getRestakeMessage().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222;
                    return hashCode3222;
                case 5:
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getWithdrawStakeRewardMessage().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222;
                    return hashCode32222;
                case 6:
                    i = ((hashCode2 * 37) + 6) * 53;
                    hashCode = getRawJsonMessage().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222;
                    return hashCode322222;
                default:
                    int hashCode3222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222;
                    return hashCode3222222;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Cosmos.internal_static_TW_Cosmos_Proto_Message_fieldAccessorTable.d(Message.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Message();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.messageOneofCase_ == 1) {
                codedOutputStream.K0(1, (Send) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 2) {
                codedOutputStream.K0(2, (Delegate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 3) {
                codedOutputStream.K0(3, (Undelegate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                codedOutputStream.K0(4, (BeginRedelegate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                codedOutputStream.K0(5, (WithdrawDelegationReward) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 6) {
                codedOutputStream.K0(6, (RawJSON) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Message(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Message message) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(message);
        }

        public static Message parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Message(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Message parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Message) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Message parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Message getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Message parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Message parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Message() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Message parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Message parseFrom(InputStream inputStream) throws IOException {
            return (Message) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Message(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                Send.Builder builder = this.messageOneofCase_ == 1 ? ((Send) this.messageOneof_).toBuilder() : null;
                                m0 z2 = jVar.z(Send.parser(), rVar);
                                this.messageOneof_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((Send) z2);
                                    this.messageOneof_ = builder.buildPartial();
                                }
                                this.messageOneofCase_ = 1;
                            } else if (J == 18) {
                                Delegate.Builder builder2 = this.messageOneofCase_ == 2 ? ((Delegate) this.messageOneof_).toBuilder() : null;
                                m0 z3 = jVar.z(Delegate.parser(), rVar);
                                this.messageOneof_ = z3;
                                if (builder2 != null) {
                                    builder2.mergeFrom((Delegate) z3);
                                    this.messageOneof_ = builder2.buildPartial();
                                }
                                this.messageOneofCase_ = 2;
                            } else if (J == 26) {
                                Undelegate.Builder builder3 = this.messageOneofCase_ == 3 ? ((Undelegate) this.messageOneof_).toBuilder() : null;
                                m0 z4 = jVar.z(Undelegate.parser(), rVar);
                                this.messageOneof_ = z4;
                                if (builder3 != null) {
                                    builder3.mergeFrom((Undelegate) z4);
                                    this.messageOneof_ = builder3.buildPartial();
                                }
                                this.messageOneofCase_ = 3;
                            } else if (J == 34) {
                                BeginRedelegate.Builder builder4 = this.messageOneofCase_ == 4 ? ((BeginRedelegate) this.messageOneof_).toBuilder() : null;
                                m0 z5 = jVar.z(BeginRedelegate.parser(), rVar);
                                this.messageOneof_ = z5;
                                if (builder4 != null) {
                                    builder4.mergeFrom((BeginRedelegate) z5);
                                    this.messageOneof_ = builder4.buildPartial();
                                }
                                this.messageOneofCase_ = 4;
                            } else if (J == 42) {
                                WithdrawDelegationReward.Builder builder5 = this.messageOneofCase_ == 5 ? ((WithdrawDelegationReward) this.messageOneof_).toBuilder() : null;
                                m0 z6 = jVar.z(WithdrawDelegationReward.parser(), rVar);
                                this.messageOneof_ = z6;
                                if (builder5 != null) {
                                    builder5.mergeFrom((WithdrawDelegationReward) z6);
                                    this.messageOneof_ = builder5.buildPartial();
                                }
                                this.messageOneofCase_ = 5;
                            } else if (J != 50) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                RawJSON.Builder builder6 = this.messageOneofCase_ == 6 ? ((RawJSON) this.messageOneof_).toBuilder() : null;
                                m0 z7 = jVar.z(RawJSON.parser(), rVar);
                                this.messageOneof_ = z7;
                                if (builder6 != null) {
                                    builder6.mergeFrom((RawJSON) z7);
                                    this.messageOneof_ = builder6.buildPartial();
                                }
                                this.messageOneofCase_ = 6;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Message parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Message) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Message parseFrom(j jVar) throws IOException {
            return (Message) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Message parseFrom(j jVar, r rVar) throws IOException {
            return (Message) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface MessageOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        Message.MessageOneofCase getMessageOneofCase();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Message.RawJSON getRawJsonMessage();

        Message.RawJSONOrBuilder getRawJsonMessageOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Message.BeginRedelegate getRestakeMessage();

        Message.BeginRedelegateOrBuilder getRestakeMessageOrBuilder();

        Message.Send getSendCoinsMessage();

        Message.SendOrBuilder getSendCoinsMessageOrBuilder();

        Message.Delegate getStakeMessage();

        Message.DelegateOrBuilder getStakeMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        Message.Undelegate getUnstakeMessage();

        Message.UndelegateOrBuilder getUnstakeMessageOrBuilder();

        Message.WithdrawDelegationReward getWithdrawStakeRewardMessage();

        Message.WithdrawDelegationRewardOrBuilder getWithdrawStakeRewardMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasRawJsonMessage();

        boolean hasRestakeMessage();

        boolean hasSendCoinsMessage();

        boolean hasStakeMessage();

        boolean hasUnstakeMessage();

        boolean hasWithdrawStakeRewardMessage();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int ACCOUNT_NUMBER_FIELD_NUMBER = 1;
        public static final int CHAIN_ID_FIELD_NUMBER = 2;
        public static final int FEE_FIELD_NUMBER = 3;
        public static final int MEMO_FIELD_NUMBER = 4;
        public static final int MESSAGES_FIELD_NUMBER = 7;
        public static final int MODE_FIELD_NUMBER = 8;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 6;
        public static final int SEQUENCE_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private long accountNumber_;
        private volatile Object chainId_;
        private Fee fee_;
        private volatile Object memo_;
        private byte memoizedIsInitialized;
        private List<Message> messages_;
        private int mode_;
        private ByteString privateKey_;
        private long sequence_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Cosmos.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private long accountNumber_;
            private int bitField0_;
            private Object chainId_;
            private a1<Fee, Fee.Builder, FeeOrBuilder> feeBuilder_;
            private Fee fee_;
            private Object memo_;
            private x0<Message, Message.Builder, MessageOrBuilder> messagesBuilder_;
            private List<Message> messages_;
            private int mode_;
            private ByteString privateKey_;
            private long sequence_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private void ensureMessagesIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.messages_ = new ArrayList(this.messages_);
                    this.bitField0_ |= 1;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_SigningInput_descriptor;
            }

            private a1<Fee, Fee.Builder, FeeOrBuilder> getFeeFieldBuilder() {
                if (this.feeBuilder_ == null) {
                    this.feeBuilder_ = new a1<>(getFee(), getParentForChildren(), isClean());
                    this.fee_ = null;
                }
                return this.feeBuilder_;
            }

            private x0<Message, Message.Builder, MessageOrBuilder> getMessagesFieldBuilder() {
                if (this.messagesBuilder_ == null) {
                    this.messagesBuilder_ = new x0<>(this.messages_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.messages_ = null;
                }
                return this.messagesBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getMessagesFieldBuilder();
                }
            }

            public Builder addAllMessages(Iterable<? extends Message> iterable) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    ensureMessagesIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.messages_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addMessages(Message message) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(message);
                    ensureMessagesIsMutable();
                    this.messages_.add(message);
                    onChanged();
                } else {
                    x0Var.f(message);
                }
                return this;
            }

            public Message.Builder addMessagesBuilder() {
                return getMessagesFieldBuilder().d(Message.getDefaultInstance());
            }

            public Builder clearAccountNumber() {
                this.accountNumber_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearChainId() {
                this.chainId_ = SigningInput.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearFee() {
                if (this.feeBuilder_ == null) {
                    this.fee_ = null;
                    onChanged();
                } else {
                    this.fee_ = null;
                    this.feeBuilder_ = null;
                }
                return this;
            }

            public Builder clearMemo() {
                this.memo_ = SigningInput.getDefaultInstance().getMemo();
                onChanged();
                return this;
            }

            public Builder clearMessages() {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    this.messages_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearMode() {
                this.mode_ = 0;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearSequence() {
                this.sequence_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public long getAccountNumber() {
                return this.accountNumber_;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public String getChainId() {
                Object obj = this.chainId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.chainId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public ByteString getChainIdBytes() {
                Object obj = this.chainId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.chainId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Cosmos.internal_static_TW_Cosmos_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public Fee getFee() {
                a1<Fee, Fee.Builder, FeeOrBuilder> a1Var = this.feeBuilder_;
                if (a1Var == null) {
                    Fee fee = this.fee_;
                    return fee == null ? Fee.getDefaultInstance() : fee;
                }
                return a1Var.f();
            }

            public Fee.Builder getFeeBuilder() {
                onChanged();
                return getFeeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public FeeOrBuilder getFeeOrBuilder() {
                a1<Fee, Fee.Builder, FeeOrBuilder> a1Var = this.feeBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Fee fee = this.fee_;
                return fee == null ? Fee.getDefaultInstance() : fee;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public String getMemo() {
                Object obj = this.memo_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.memo_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public ByteString getMemoBytes() {
                Object obj = this.memo_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.memo_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public Message getMessages(int i) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    return this.messages_.get(i);
                }
                return x0Var.o(i);
            }

            public Message.Builder getMessagesBuilder(int i) {
                return getMessagesFieldBuilder().l(i);
            }

            public List<Message.Builder> getMessagesBuilderList() {
                return getMessagesFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public int getMessagesCount() {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    return this.messages_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public List<Message> getMessagesList() {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.messages_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public MessageOrBuilder getMessagesOrBuilder(int i) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    return this.messages_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public List<? extends MessageOrBuilder> getMessagesOrBuilderList() {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.messages_);
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public BroadcastMode getMode() {
                BroadcastMode valueOf = BroadcastMode.valueOf(this.mode_);
                return valueOf == null ? BroadcastMode.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public int getModeValue() {
                return this.mode_;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public long getSequence() {
                return this.sequence_;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
            public boolean hasFee() {
                return (this.feeBuilder_ == null && this.fee_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeFee(Fee fee) {
                a1<Fee, Fee.Builder, FeeOrBuilder> a1Var = this.feeBuilder_;
                if (a1Var == null) {
                    Fee fee2 = this.fee_;
                    if (fee2 != null) {
                        this.fee_ = Fee.newBuilder(fee2).mergeFrom(fee).buildPartial();
                    } else {
                        this.fee_ = fee;
                    }
                    onChanged();
                } else {
                    a1Var.h(fee);
                }
                return this;
            }

            public Builder removeMessages(int i) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    ensureMessagesIsMutable();
                    this.messages_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setAccountNumber(long j) {
                this.accountNumber_ = j;
                onChanged();
                return this;
            }

            public Builder setChainId(String str) {
                Objects.requireNonNull(str);
                this.chainId_ = str;
                onChanged();
                return this;
            }

            public Builder setChainIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setFee(Fee fee) {
                a1<Fee, Fee.Builder, FeeOrBuilder> a1Var = this.feeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(fee);
                    this.fee_ = fee;
                    onChanged();
                } else {
                    a1Var.j(fee);
                }
                return this;
            }

            public Builder setMemo(String str) {
                Objects.requireNonNull(str);
                this.memo_ = str;
                onChanged();
                return this;
            }

            public Builder setMemoBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.memo_ = byteString;
                onChanged();
                return this;
            }

            public Builder setMessages(int i, Message message) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(message);
                    ensureMessagesIsMutable();
                    this.messages_.set(i, message);
                    onChanged();
                } else {
                    x0Var.x(i, message);
                }
                return this;
            }

            public Builder setMode(BroadcastMode broadcastMode) {
                Objects.requireNonNull(broadcastMode);
                this.mode_ = broadcastMode.getNumber();
                onChanged();
                return this;
            }

            public Builder setModeValue(int i) {
                this.mode_ = i;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSequence(long j) {
                this.sequence_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.chainId_ = "";
                this.memo_ = "";
                this.privateKey_ = ByteString.EMPTY;
                this.messages_ = Collections.emptyList();
                this.mode_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.accountNumber_ = this.accountNumber_;
                signingInput.chainId_ = this.chainId_;
                a1<Fee, Fee.Builder, FeeOrBuilder> a1Var = this.feeBuilder_;
                if (a1Var == null) {
                    signingInput.fee_ = this.fee_;
                } else {
                    signingInput.fee_ = a1Var.b();
                }
                signingInput.memo_ = this.memo_;
                signingInput.sequence_ = this.sequence_;
                signingInput.privateKey_ = this.privateKey_;
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var != null) {
                    signingInput.messages_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.messages_ = Collections.unmodifiableList(this.messages_);
                        this.bitField0_ &= -2;
                    }
                    signingInput.messages_ = this.messages_;
                }
                signingInput.mode_ = this.mode_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public Message.Builder addMessagesBuilder(int i) {
                return getMessagesFieldBuilder().c(i, Message.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.accountNumber_ = 0L;
                this.chainId_ = "";
                if (this.feeBuilder_ == null) {
                    this.fee_ = null;
                } else {
                    this.fee_ = null;
                    this.feeBuilder_ = null;
                }
                this.memo_ = "";
                this.sequence_ = 0L;
                this.privateKey_ = ByteString.EMPTY;
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    this.messages_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                this.mode_ = 0;
                return this;
            }

            public Builder setFee(Fee.Builder builder) {
                a1<Fee, Fee.Builder, FeeOrBuilder> a1Var = this.feeBuilder_;
                if (a1Var == null) {
                    this.fee_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder addMessages(int i, Message message) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(message);
                    ensureMessagesIsMutable();
                    this.messages_.add(i, message);
                    onChanged();
                } else {
                    x0Var.e(i, message);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setMessages(int i, Message.Builder builder) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    ensureMessagesIsMutable();
                    this.messages_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.chainId_ = "";
                this.memo_ = "";
                this.privateKey_ = ByteString.EMPTY;
                this.messages_ = Collections.emptyList();
                this.mode_ = 0;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getAccountNumber() != 0) {
                    setAccountNumber(signingInput.getAccountNumber());
                }
                if (!signingInput.getChainId().isEmpty()) {
                    this.chainId_ = signingInput.chainId_;
                    onChanged();
                }
                if (signingInput.hasFee()) {
                    mergeFee(signingInput.getFee());
                }
                if (!signingInput.getMemo().isEmpty()) {
                    this.memo_ = signingInput.memo_;
                    onChanged();
                }
                if (signingInput.getSequence() != 0) {
                    setSequence(signingInput.getSequence());
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (this.messagesBuilder_ == null) {
                    if (!signingInput.messages_.isEmpty()) {
                        if (this.messages_.isEmpty()) {
                            this.messages_ = signingInput.messages_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureMessagesIsMutable();
                            this.messages_.addAll(signingInput.messages_);
                        }
                        onChanged();
                    }
                } else if (!signingInput.messages_.isEmpty()) {
                    if (!this.messagesBuilder_.u()) {
                        this.messagesBuilder_.b(signingInput.messages_);
                    } else {
                        this.messagesBuilder_.i();
                        this.messagesBuilder_ = null;
                        this.messages_ = signingInput.messages_;
                        this.bitField0_ &= -2;
                        this.messagesBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getMessagesFieldBuilder() : null;
                    }
                }
                if (signingInput.mode_ != 0) {
                    setModeValue(signingInput.getModeValue());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            public Builder addMessages(Message.Builder builder) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    ensureMessagesIsMutable();
                    this.messages_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addMessages(int i, Message.Builder builder) {
                x0<Message, Message.Builder, MessageOrBuilder> x0Var = this.messagesBuilder_;
                if (x0Var == null) {
                    ensureMessagesIsMutable();
                    this.messages_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Cosmos.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.SigningInput.access$14600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Cosmos$SigningInput r3 = (wallet.core.jni.proto.Cosmos.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Cosmos$SigningInput r4 = (wallet.core.jni.proto.Cosmos.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Cosmos.internal_static_TW_Cosmos_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getAccountNumber() == signingInput.getAccountNumber() && getChainId().equals(signingInput.getChainId()) && hasFee() == signingInput.hasFee()) {
                return (!hasFee() || getFee().equals(signingInput.getFee())) && getMemo().equals(signingInput.getMemo()) && getSequence() == signingInput.getSequence() && getPrivateKey().equals(signingInput.getPrivateKey()) && getMessagesList().equals(signingInput.getMessagesList()) && this.mode_ == signingInput.mode_ && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public long getAccountNumber() {
            return this.accountNumber_;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public String getChainId() {
            Object obj = this.chainId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.chainId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public ByteString getChainIdBytes() {
            Object obj = this.chainId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.chainId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public Fee getFee() {
            Fee fee = this.fee_;
            return fee == null ? Fee.getDefaultInstance() : fee;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public FeeOrBuilder getFeeOrBuilder() {
            return getFee();
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public String getMemo() {
            Object obj = this.memo_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.memo_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public ByteString getMemoBytes() {
            Object obj = this.memo_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.memo_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public Message getMessages(int i) {
            return this.messages_.get(i);
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public int getMessagesCount() {
            return this.messages_.size();
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public List<Message> getMessagesList() {
            return this.messages_;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public MessageOrBuilder getMessagesOrBuilder(int i) {
            return this.messages_.get(i);
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public List<? extends MessageOrBuilder> getMessagesOrBuilderList() {
            return this.messages_;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public BroadcastMode getMode() {
            BroadcastMode valueOf = BroadcastMode.valueOf(this.mode_);
            return valueOf == null ? BroadcastMode.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public int getModeValue() {
            return this.mode_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public long getSequence() {
            return this.sequence_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.accountNumber_;
            int a0 = j != 0 ? CodedOutputStream.a0(1, j) + 0 : 0;
            if (!getChainIdBytes().isEmpty()) {
                a0 += GeneratedMessageV3.computeStringSize(2, this.chainId_);
            }
            if (this.fee_ != null) {
                a0 += CodedOutputStream.G(3, getFee());
            }
            if (!getMemoBytes().isEmpty()) {
                a0 += GeneratedMessageV3.computeStringSize(4, this.memo_);
            }
            long j2 = this.sequence_;
            if (j2 != 0) {
                a0 += CodedOutputStream.a0(5, j2);
            }
            if (!this.privateKey_.isEmpty()) {
                a0 += CodedOutputStream.h(6, this.privateKey_);
            }
            for (int i2 = 0; i2 < this.messages_.size(); i2++) {
                a0 += CodedOutputStream.G(7, this.messages_.get(i2));
            }
            if (this.mode_ != BroadcastMode.BLOCK.getNumber()) {
                a0 += CodedOutputStream.l(8, this.mode_);
            }
            int serializedSize = a0 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningInputOrBuilder
        public boolean hasFee() {
            return this.fee_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getAccountNumber())) * 37) + 2) * 53) + getChainId().hashCode();
            if (hasFee()) {
                hashCode = (((hashCode * 37) + 3) * 53) + getFee().hashCode();
            }
            int hashCode2 = (((((((((((hashCode * 37) + 4) * 53) + getMemo().hashCode()) * 37) + 5) * 53) + a0.h(getSequence())) * 37) + 6) * 53) + getPrivateKey().hashCode();
            if (getMessagesCount() > 0) {
                hashCode2 = (((hashCode2 * 37) + 7) * 53) + getMessagesList().hashCode();
            }
            int hashCode3 = (((((hashCode2 * 37) + 8) * 53) + this.mode_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode3;
            return hashCode3;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Cosmos.internal_static_TW_Cosmos_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.accountNumber_;
            if (j != 0) {
                codedOutputStream.d1(1, j);
            }
            if (!getChainIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.chainId_);
            }
            if (this.fee_ != null) {
                codedOutputStream.K0(3, getFee());
            }
            if (!getMemoBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.memo_);
            }
            long j2 = this.sequence_;
            if (j2 != 0) {
                codedOutputStream.d1(5, j2);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(6, this.privateKey_);
            }
            for (int i = 0; i < this.messages_.size(); i++) {
                codedOutputStream.K0(7, this.messages_.get(i));
            }
            if (this.mode_ != BroadcastMode.BLOCK.getNumber()) {
                codedOutputStream.u0(8, this.mode_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.chainId_ = "";
            this.memo_ = "";
            this.privateKey_ = ByteString.EMPTY;
            this.messages_ = Collections.emptyList();
            this.mode_ = 0;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.accountNumber_ = jVar.L();
                            } else if (J == 18) {
                                this.chainId_ = jVar.I();
                            } else if (J == 26) {
                                Fee fee = this.fee_;
                                Fee.Builder builder = fee != null ? fee.toBuilder() : null;
                                Fee fee2 = (Fee) jVar.z(Fee.parser(), rVar);
                                this.fee_ = fee2;
                                if (builder != null) {
                                    builder.mergeFrom(fee2);
                                    this.fee_ = builder.buildPartial();
                                }
                            } else if (J == 34) {
                                this.memo_ = jVar.I();
                            } else if (J == 40) {
                                this.sequence_ = jVar.L();
                            } else if (J == 50) {
                                this.privateKey_ = jVar.q();
                            } else if (J == 58) {
                                if (!(z2 & true)) {
                                    this.messages_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.messages_.add(jVar.z(Message.parser(), rVar));
                            } else if (J != 64) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.mode_ = jVar.s();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.messages_ = Collections.unmodifiableList(this.messages_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        long getAccountNumber();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getChainId();

        ByteString getChainIdBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        Fee getFee();

        FeeOrBuilder getFeeOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getMemo();

        ByteString getMemoBytes();

        Message getMessages(int i);

        int getMessagesCount();

        List<Message> getMessagesList();

        MessageOrBuilder getMessagesOrBuilder(int i);

        List<? extends MessageOrBuilder> getMessagesOrBuilderList();

        BroadcastMode getMode();

        int getModeValue();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        long getSequence();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int JSON_FIELD_NUMBER = 2;
        public static final int SIGNATURE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object json_;
        private byte memoizedIsInitialized;
        private ByteString signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Cosmos.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private Object json_;
            private ByteString signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Cosmos.internal_static_TW_Cosmos_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearJson() {
                this.json_ = SigningOutput.getDefaultInstance().getJson();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Cosmos.internal_static_TW_Cosmos_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningOutputOrBuilder
            public String getJson() {
                Object obj = this.json_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.json_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningOutputOrBuilder
            public ByteString getJsonBytes() {
                Object obj = this.json_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.json_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Cosmos.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Cosmos.internal_static_TW_Cosmos_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setJson(String str) {
                Objects.requireNonNull(str);
                this.json_ = str;
                onChanged();
                return this;
            }

            public Builder setJsonBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.json_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.signature_ = this.signature_;
                signingOutput.json_ = this.json_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getSignature() != ByteString.EMPTY) {
                    setSignature(signingOutput.getSignature());
                }
                if (!signingOutput.getJson().isEmpty()) {
                    this.json_ = signingOutput.json_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Cosmos.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Cosmos.SigningOutput.access$15900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Cosmos$SigningOutput r3 = (wallet.core.jni.proto.Cosmos.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Cosmos$SigningOutput r4 = (wallet.core.jni.proto.Cosmos.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Cosmos.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Cosmos$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Cosmos.internal_static_TW_Cosmos_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getSignature().equals(signingOutput.getSignature()) && getJson().equals(signingOutput.getJson()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningOutputOrBuilder
        public String getJson() {
            Object obj = this.json_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.json_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningOutputOrBuilder
        public ByteString getJsonBytes() {
            Object obj = this.json_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.json_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.signature_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.signature_);
            if (!getJsonBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(2, this.json_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Cosmos.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSignature().hashCode()) * 37) + 2) * 53) + getJson().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Cosmos.internal_static_TW_Cosmos_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(1, this.signature_);
            }
            if (!getJsonBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.json_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.signature_ = ByteString.EMPTY;
            this.json_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.signature_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.json_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getJson();

        ByteString getJsonBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Cosmos_Proto_Amount_descriptor = bVar;
        internal_static_TW_Cosmos_Proto_Amount_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Denom", "Amount"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Cosmos_Proto_Fee_descriptor = bVar2;
        internal_static_TW_Cosmos_Proto_Fee_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Amounts", "Gas"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Cosmos_Proto_Message_descriptor = bVar3;
        internal_static_TW_Cosmos_Proto_Message_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"SendCoinsMessage", "StakeMessage", "UnstakeMessage", "RestakeMessage", "WithdrawStakeRewardMessage", "RawJsonMessage", "MessageOneof"});
        Descriptors.b bVar4 = bVar3.r().get(0);
        internal_static_TW_Cosmos_Proto_Message_Send_descriptor = bVar4;
        internal_static_TW_Cosmos_Proto_Message_Send_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"FromAddress", "ToAddress", "Amounts", "TypePrefix"});
        Descriptors.b bVar5 = bVar3.r().get(1);
        internal_static_TW_Cosmos_Proto_Message_Delegate_descriptor = bVar5;
        internal_static_TW_Cosmos_Proto_Message_Delegate_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"DelegatorAddress", "ValidatorAddress", "Amount", "TypePrefix"});
        Descriptors.b bVar6 = bVar3.r().get(2);
        internal_static_TW_Cosmos_Proto_Message_Undelegate_descriptor = bVar6;
        internal_static_TW_Cosmos_Proto_Message_Undelegate_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"DelegatorAddress", "ValidatorAddress", "Amount", "TypePrefix"});
        Descriptors.b bVar7 = bVar3.r().get(3);
        internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_descriptor = bVar7;
        internal_static_TW_Cosmos_Proto_Message_BeginRedelegate_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"DelegatorAddress", "ValidatorSrcAddress", "ValidatorDstAddress", "Amount", "TypePrefix"});
        Descriptors.b bVar8 = bVar3.r().get(4);
        internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_descriptor = bVar8;
        internal_static_TW_Cosmos_Proto_Message_WithdrawDelegationReward_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"DelegatorAddress", "ValidatorAddress", "TypePrefix"});
        Descriptors.b bVar9 = bVar3.r().get(5);
        internal_static_TW_Cosmos_Proto_Message_RawJSON_descriptor = bVar9;
        internal_static_TW_Cosmos_Proto_Message_RawJSON_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"Type", "Value"});
        Descriptors.b bVar10 = getDescriptor().o().get(3);
        internal_static_TW_Cosmos_Proto_SigningInput_descriptor = bVar10;
        internal_static_TW_Cosmos_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"AccountNumber", "ChainId", "Fee", "Memo", "Sequence", "PrivateKey", "Messages", "Mode"});
        Descriptors.b bVar11 = getDescriptor().o().get(4);
        internal_static_TW_Cosmos_Proto_SigningOutput_descriptor = bVar11;
        internal_static_TW_Cosmos_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar11, new String[]{"Signature", "Json"});
    }

    private Cosmos() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
