package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import wallet.core.jni.proto.Bitcoin;
import wallet.core.jni.proto.Common;

/* loaded from: classes3.dex */
public final class Decred {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\fDecred.proto\u0012\u000fTW.Decred.Proto\u001a\rBitcoin.proto\u001a\fCommon.proto\"¿\u0001\n\u000bTransaction\u0012\u0015\n\rserializeType\u0018\u0001 \u0001(\r\u0012\u000f\n\u0007version\u0018\u0002 \u0001(\r\u00121\n\u0006inputs\u0018\u0003 \u0003(\u000b2!.TW.Decred.Proto.TransactionInput\u00123\n\u0007outputs\u0018\u0004 \u0003(\u000b2\".TW.Decred.Proto.TransactionOutput\u0012\u0010\n\blockTime\u0018\u0005 \u0001(\r\u0012\u000e\n\u0006expiry\u0018\u0006 \u0001(\r\"¢\u0001\n\u0010TransactionInput\u00122\n\u000epreviousOutput\u0018\u0001 \u0001(\u000b2\u001a.TW.Bitcoin.Proto.OutPoint\u0012\u0010\n\bsequence\u0018\u0002 \u0001(\r\u0012\u000f\n\u0007valueIn\u0018\u0003 \u0001(\u0003\u0012\u0013\n\u000bblockHeight\u0018\u0004 \u0001(\r\u0012\u0012\n\nblockIndex\u0018\u0005 \u0001(\r\u0012\u000e\n\u0006script\u0018\u0006 \u0001(\f\"C\n\u0011TransactionOutput\u0012\r\n\u0005value\u0018\u0001 \u0001(\u0003\u0012\u000f\n\u0007version\u0018\u0002 \u0001(\r\u0012\u000e\n\u0006script\u0018\u0003 \u0001(\f\"\u0099\u0001\n\rSigningOutput\u00121\n\u000btransaction\u0018\u0001 \u0001(\u000b2\u001c.TW.Decred.Proto.Transaction\u0012\u000f\n\u0007encoded\u0018\u0002 \u0001(\f\u0012\u0016\n\u000etransaction_id\u0018\u0003 \u0001(\t\u0012,\n\u0005error\u0018\u0004 \u0001(\u000e2\u001d.TW.Common.Proto.SigningErrorB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[]{Bitcoin.getDescriptor(), Common.getDescriptor()});
    private static final Descriptors.b internal_static_TW_Decred_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Decred_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Decred_Proto_TransactionInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Decred_Proto_TransactionInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Decred_Proto_TransactionOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Decred_Proto_TransactionOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Decred_Proto_Transaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Decred_Proto_Transaction_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 2;
        public static final int ERROR_FIELD_NUMBER = 4;
        public static final int TRANSACTION_FIELD_NUMBER = 1;
        public static final int TRANSACTION_ID_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private int error_;
        private byte memoizedIsInitialized;
        private volatile Object transactionId_;
        private Transaction transaction_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Decred.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Decred.internal_static_TW_Decred_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            if (hasTransaction() != signingOutput.hasTransaction()) {
                return false;
            }
            return (!hasTransaction() || getTransaction().equals(signingOutput.getTransaction())) && getEncoded().equals(signingOutput.getEncoded()) && getTransactionId().equals(signingOutput.getTransactionId()) && this.error_ == signingOutput.error_ && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public Common.SigningError getError() {
            Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
            return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public int getErrorValue() {
            return this.error_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.transaction_ != null ? 0 + CodedOutputStream.G(1, getTransaction()) : 0;
            if (!this.encoded_.isEmpty()) {
                G += CodedOutputStream.h(2, this.encoded_);
            }
            if (!getTransactionIdBytes().isEmpty()) {
                G += GeneratedMessageV3.computeStringSize(3, this.transactionId_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                G += CodedOutputStream.l(4, this.error_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public Transaction getTransaction() {
            Transaction transaction = this.transaction_;
            return transaction == null ? Transaction.getDefaultInstance() : transaction;
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public String getTransactionId() {
            Object obj = this.transactionId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.transactionId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public ByteString getTransactionIdBytes() {
            Object obj = this.transactionId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.transactionId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public TransactionOrBuilder getTransactionOrBuilder() {
            return getTransaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
        public boolean hasTransaction() {
            return this.transaction_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasTransaction()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getTransaction().hashCode();
            }
            int hashCode2 = (((((((((((((hashCode * 37) + 2) * 53) + getEncoded().hashCode()) * 37) + 3) * 53) + getTransactionId().hashCode()) * 37) + 4) * 53) + this.error_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Decred.internal_static_TW_Decred_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.transaction_ != null) {
                codedOutputStream.K0(1, getTransaction());
            }
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(2, this.encoded_);
            }
            if (!getTransactionIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.transactionId_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                codedOutputStream.u0(4, this.error_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;
            private int error_;
            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> transactionBuilder_;
            private Object transactionId_;
            private Transaction transaction_;

            public static final Descriptors.b getDescriptor() {
                return Decred.internal_static_TW_Decred_Proto_SigningOutput_descriptor;
            }

            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> getTransactionFieldBuilder() {
                if (this.transactionBuilder_ == null) {
                    this.transactionBuilder_ = new a1<>(getTransaction(), getParentForChildren(), isClean());
                    this.transaction_ = null;
                }
                return this.transactionBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearError() {
                this.error_ = 0;
                onChanged();
                return this;
            }

            public Builder clearTransaction() {
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                    onChanged();
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                return this;
            }

            public Builder clearTransactionId() {
                this.transactionId_ = SigningOutput.getDefaultInstance().getTransactionId();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Decred.internal_static_TW_Decred_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public Common.SigningError getError() {
                Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
                return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public int getErrorValue() {
                return this.error_;
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public Transaction getTransaction() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction = this.transaction_;
                    return transaction == null ? Transaction.getDefaultInstance() : transaction;
                }
                return a1Var.f();
            }

            public Transaction.Builder getTransactionBuilder() {
                onChanged();
                return getTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public String getTransactionId() {
                Object obj = this.transactionId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.transactionId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public ByteString getTransactionIdBytes() {
                Object obj = this.transactionId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.transactionId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public TransactionOrBuilder getTransactionOrBuilder() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Transaction transaction = this.transaction_;
                return transaction == null ? Transaction.getDefaultInstance() : transaction;
            }

            @Override // wallet.core.jni.proto.Decred.SigningOutputOrBuilder
            public boolean hasTransaction() {
                return (this.transactionBuilder_ == null && this.transaction_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Decred.internal_static_TW_Decred_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction2 = this.transaction_;
                    if (transaction2 != null) {
                        this.transaction_ = Transaction.newBuilder(transaction2).mergeFrom(transaction).buildPartial();
                    } else {
                        this.transaction_ = transaction;
                    }
                    onChanged();
                } else {
                    a1Var.h(transaction);
                }
                return this;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setError(Common.SigningError signingError) {
                Objects.requireNonNull(signingError);
                this.error_ = signingError.getNumber();
                onChanged();
                return this;
            }

            public Builder setErrorValue(int i) {
                this.error_ = i;
                onChanged();
                return this;
            }

            public Builder setTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transaction);
                    this.transaction_ = transaction;
                    onChanged();
                } else {
                    a1Var.j(transaction);
                }
                return this;
            }

            public Builder setTransactionId(String str) {
                Objects.requireNonNull(str);
                this.transactionId_ = str;
                onChanged();
                return this;
            }

            public Builder setTransactionIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.transactionId_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                this.transactionId_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    signingOutput.transaction_ = this.transaction_;
                } else {
                    signingOutput.transaction_ = a1Var.b();
                }
                signingOutput.encoded_ = this.encoded_;
                signingOutput.transactionId_ = this.transactionId_;
                signingOutput.error_ = this.error_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                this.encoded_ = ByteString.EMPTY;
                this.transactionId_ = "";
                this.error_ = 0;
                return this;
            }

            public Builder setTransaction(Transaction.Builder builder) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    this.transaction_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                this.transactionId_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.hasTransaction()) {
                    mergeTransaction(signingOutput.getTransaction());
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (!signingOutput.getTransactionId().isEmpty()) {
                    this.transactionId_ = signingOutput.transactionId_;
                    onChanged();
                }
                if (signingOutput.error_ != 0) {
                    setErrorValue(signingOutput.getErrorValue());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Decred.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Decred.SigningOutput.access$5500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Decred$SigningOutput r3 = (wallet.core.jni.proto.Decred.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Decred$SigningOutput r4 = (wallet.core.jni.proto.Decred.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Decred.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Decred$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
            this.transactionId_ = "";
            this.error_ = 0;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    Transaction transaction = this.transaction_;
                                    Transaction.Builder builder = transaction != null ? transaction.toBuilder() : null;
                                    Transaction transaction2 = (Transaction) jVar.z(Transaction.parser(), rVar);
                                    this.transaction_ = transaction2;
                                    if (builder != null) {
                                        builder.mergeFrom(transaction2);
                                        this.transaction_ = builder.buildPartial();
                                    }
                                } else if (J == 18) {
                                    this.encoded_ = jVar.q();
                                } else if (J == 26) {
                                    this.transactionId_ = jVar.I();
                                } else if (J != 32) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.error_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        Common.SigningError getError();

        int getErrorValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Transaction getTransaction();

        String getTransactionId();

        ByteString getTransactionIdBytes();

        TransactionOrBuilder getTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransaction();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transaction extends GeneratedMessageV3 implements TransactionOrBuilder {
        public static final int EXPIRY_FIELD_NUMBER = 6;
        public static final int INPUTS_FIELD_NUMBER = 3;
        public static final int LOCKTIME_FIELD_NUMBER = 5;
        public static final int OUTPUTS_FIELD_NUMBER = 4;
        public static final int SERIALIZETYPE_FIELD_NUMBER = 1;
        public static final int VERSION_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private int expiry_;
        private List<TransactionInput> inputs_;
        private int lockTime_;
        private byte memoizedIsInitialized;
        private List<TransactionOutput> outputs_;
        private int serializeType_;
        private int version_;
        private static final Transaction DEFAULT_INSTANCE = new Transaction();
        private static final t0<Transaction> PARSER = new c<Transaction>() { // from class: wallet.core.jni.proto.Decred.Transaction.1
            @Override // com.google.protobuf.t0
            public Transaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transaction(jVar, rVar);
            }
        };

        public static Transaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Decred.internal_static_TW_Decred_Proto_Transaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transaction)) {
                return super.equals(obj);
            }
            Transaction transaction = (Transaction) obj;
            return getSerializeType() == transaction.getSerializeType() && getVersion() == transaction.getVersion() && getInputsList().equals(transaction.getInputsList()) && getOutputsList().equals(transaction.getOutputsList()) && getLockTime() == transaction.getLockTime() && getExpiry() == transaction.getExpiry() && this.unknownFields.equals(transaction.unknownFields);
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public int getExpiry() {
            return this.expiry_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public TransactionInput getInputs(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public int getInputsCount() {
            return this.inputs_.size();
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public List<TransactionInput> getInputsList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public TransactionInputOrBuilder getInputsOrBuilder(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public int getLockTime() {
            return this.lockTime_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public TransactionOutput getOutputs(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public int getOutputsCount() {
            return this.outputs_.size();
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public List<TransactionOutput> getOutputsList() {
            return this.outputs_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public TransactionOutputOrBuilder getOutputsOrBuilder(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList() {
            return this.outputs_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transaction> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public int getSerializeType() {
            return this.serializeType_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.serializeType_;
            int Y = i2 != 0 ? CodedOutputStream.Y(1, i2) + 0 : 0;
            int i3 = this.version_;
            if (i3 != 0) {
                Y += CodedOutputStream.Y(2, i3);
            }
            for (int i4 = 0; i4 < this.inputs_.size(); i4++) {
                Y += CodedOutputStream.G(3, this.inputs_.get(i4));
            }
            for (int i5 = 0; i5 < this.outputs_.size(); i5++) {
                Y += CodedOutputStream.G(4, this.outputs_.get(i5));
            }
            int i6 = this.lockTime_;
            if (i6 != 0) {
                Y += CodedOutputStream.Y(5, i6);
            }
            int i7 = this.expiry_;
            if (i7 != 0) {
                Y += CodedOutputStream.Y(6, i7);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSerializeType()) * 37) + 2) * 53) + getVersion();
            if (getInputsCount() > 0) {
                hashCode = (((hashCode * 37) + 3) * 53) + getInputsList().hashCode();
            }
            if (getOutputsCount() > 0) {
                hashCode = (((hashCode * 37) + 4) * 53) + getOutputsList().hashCode();
            }
            int lockTime = (((((((((hashCode * 37) + 5) * 53) + getLockTime()) * 37) + 6) * 53) + getExpiry()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = lockTime;
            return lockTime;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Decred.internal_static_TW_Decred_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.serializeType_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            int i2 = this.version_;
            if (i2 != 0) {
                codedOutputStream.b1(2, i2);
            }
            for (int i3 = 0; i3 < this.inputs_.size(); i3++) {
                codedOutputStream.K0(3, this.inputs_.get(i3));
            }
            for (int i4 = 0; i4 < this.outputs_.size(); i4++) {
                codedOutputStream.K0(4, this.outputs_.get(i4));
            }
            int i5 = this.lockTime_;
            if (i5 != 0) {
                codedOutputStream.b1(5, i5);
            }
            int i6 = this.expiry_;
            if (i6 != 0) {
                codedOutputStream.b1(6, i6);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOrBuilder {
            private int bitField0_;
            private int expiry_;
            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> inputsBuilder_;
            private List<TransactionInput> inputs_;
            private int lockTime_;
            private x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> outputsBuilder_;
            private List<TransactionOutput> outputs_;
            private int serializeType_;
            private int version_;

            private void ensureInputsIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.inputs_ = new ArrayList(this.inputs_);
                    this.bitField0_ |= 1;
                }
            }

            private void ensureOutputsIsMutable() {
                if ((this.bitField0_ & 2) == 0) {
                    this.outputs_ = new ArrayList(this.outputs_);
                    this.bitField0_ |= 2;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Decred.internal_static_TW_Decred_Proto_Transaction_descriptor;
            }

            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> getInputsFieldBuilder() {
                if (this.inputsBuilder_ == null) {
                    this.inputsBuilder_ = new x0<>(this.inputs_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.inputs_ = null;
                }
                return this.inputsBuilder_;
            }

            private x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> getOutputsFieldBuilder() {
                if (this.outputsBuilder_ == null) {
                    this.outputsBuilder_ = new x0<>(this.outputs_, (this.bitField0_ & 2) != 0, getParentForChildren(), isClean());
                    this.outputs_ = null;
                }
                return this.outputsBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getInputsFieldBuilder();
                    getOutputsFieldBuilder();
                }
            }

            public Builder addAllInputs(Iterable<? extends TransactionInput> iterable) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.inputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addAllOutputs(Iterable<? extends TransactionOutput> iterable) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.outputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addInputs(TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(transactionInput);
                    onChanged();
                } else {
                    x0Var.f(transactionInput);
                }
                return this;
            }

            public TransactionInput.Builder addInputsBuilder() {
                return getInputsFieldBuilder().d(TransactionInput.getDefaultInstance());
            }

            public Builder addOutputs(TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.add(transactionOutput);
                    onChanged();
                } else {
                    x0Var.f(transactionOutput);
                }
                return this;
            }

            public TransactionOutput.Builder addOutputsBuilder() {
                return getOutputsFieldBuilder().d(TransactionOutput.getDefaultInstance());
            }

            public Builder clearExpiry() {
                this.expiry_ = 0;
                onChanged();
                return this;
            }

            public Builder clearInputs() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearLockTime() {
                this.lockTime_ = 0;
                onChanged();
                return this;
            }

            public Builder clearOutputs() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearSerializeType() {
                this.serializeType_ = 0;
                onChanged();
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Decred.internal_static_TW_Decred_Proto_Transaction_descriptor;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public int getExpiry() {
                return this.expiry_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public TransactionInput getInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionInput.Builder getInputsBuilder(int i) {
                return getInputsFieldBuilder().l(i);
            }

            public List<TransactionInput.Builder> getInputsBuilderList() {
                return getInputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public int getInputsCount() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public List<TransactionInput> getInputsList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.inputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public TransactionInputOrBuilder getInputsOrBuilder(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.inputs_);
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public int getLockTime() {
                return this.lockTime_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public TransactionOutput getOutputs(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionOutput.Builder getOutputsBuilder(int i) {
                return getOutputsFieldBuilder().l(i);
            }

            public List<TransactionOutput.Builder> getOutputsBuilderList() {
                return getOutputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public int getOutputsCount() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public List<TransactionOutput> getOutputsList() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.outputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public TransactionOutputOrBuilder getOutputsOrBuilder(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.outputs_);
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public int getSerializeType() {
                return this.serializeType_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Decred.internal_static_TW_Decred_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder removeOutputs(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setExpiry(int i) {
                this.expiry_ = i;
                onChanged();
                return this;
            }

            public Builder setInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.set(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.x(i, transactionInput);
                }
                return this;
            }

            public Builder setLockTime(int i) {
                this.lockTime_ = i;
                onChanged();
                return this;
            }

            public Builder setOutputs(int i, TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, transactionOutput);
                    onChanged();
                } else {
                    x0Var.x(i, transactionOutput);
                }
                return this;
            }

            public Builder setSerializeType(int i) {
                this.serializeType_ = i;
                onChanged();
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            private Builder() {
                this.inputs_ = Collections.emptyList();
                this.outputs_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction build() {
                Transaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction buildPartial() {
                Transaction transaction = new Transaction(this);
                transaction.serializeType_ = this.serializeType_;
                transaction.version_ = this.version_;
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var != null) {
                    transaction.inputs_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                        this.bitField0_ &= -2;
                    }
                    transaction.inputs_ = this.inputs_;
                }
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var2 = this.outputsBuilder_;
                if (x0Var2 != null) {
                    transaction.outputs_ = x0Var2.g();
                } else {
                    if ((this.bitField0_ & 2) != 0) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                        this.bitField0_ &= -3;
                    }
                    transaction.outputs_ = this.outputs_;
                }
                transaction.lockTime_ = this.lockTime_;
                transaction.expiry_ = this.expiry_;
                onBuilt();
                return transaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transaction getDefaultInstanceForType() {
                return Transaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public TransactionInput.Builder addInputsBuilder(int i) {
                return getInputsFieldBuilder().c(i, TransactionInput.getDefaultInstance());
            }

            public TransactionOutput.Builder addOutputsBuilder(int i) {
                return getOutputsFieldBuilder().c(i, TransactionOutput.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.serializeType_ = 0;
                this.version_ = 0;
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var2 = this.outputsBuilder_;
                if (x0Var2 == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                } else {
                    x0Var2.h();
                }
                this.lockTime_ = 0;
                this.expiry_ = 0;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.inputs_ = Collections.emptyList();
                this.outputs_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public Builder addInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.e(i, transactionInput);
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, transactionOutput);
                    onChanged();
                } else {
                    x0Var.e(i, transactionOutput);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transaction) {
                    return mergeFrom((Transaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder setOutputs(int i, TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder mergeFrom(Transaction transaction) {
                if (transaction == Transaction.getDefaultInstance()) {
                    return this;
                }
                if (transaction.getSerializeType() != 0) {
                    setSerializeType(transaction.getSerializeType());
                }
                if (transaction.getVersion() != 0) {
                    setVersion(transaction.getVersion());
                }
                if (this.inputsBuilder_ == null) {
                    if (!transaction.inputs_.isEmpty()) {
                        if (this.inputs_.isEmpty()) {
                            this.inputs_ = transaction.inputs_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureInputsIsMutable();
                            this.inputs_.addAll(transaction.inputs_);
                        }
                        onChanged();
                    }
                } else if (!transaction.inputs_.isEmpty()) {
                    if (!this.inputsBuilder_.u()) {
                        this.inputsBuilder_.b(transaction.inputs_);
                    } else {
                        this.inputsBuilder_.i();
                        this.inputsBuilder_ = null;
                        this.inputs_ = transaction.inputs_;
                        this.bitField0_ &= -2;
                        this.inputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getInputsFieldBuilder() : null;
                    }
                }
                if (this.outputsBuilder_ == null) {
                    if (!transaction.outputs_.isEmpty()) {
                        if (this.outputs_.isEmpty()) {
                            this.outputs_ = transaction.outputs_;
                            this.bitField0_ &= -3;
                        } else {
                            ensureOutputsIsMutable();
                            this.outputs_.addAll(transaction.outputs_);
                        }
                        onChanged();
                    }
                } else if (!transaction.outputs_.isEmpty()) {
                    if (!this.outputsBuilder_.u()) {
                        this.outputsBuilder_.b(transaction.outputs_);
                    } else {
                        this.outputsBuilder_.i();
                        this.outputsBuilder_ = null;
                        this.outputs_ = transaction.outputs_;
                        this.bitField0_ &= -3;
                        this.outputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getOutputsFieldBuilder() : null;
                    }
                }
                if (transaction.getLockTime() != 0) {
                    setLockTime(transaction.getLockTime());
                }
                if (transaction.getExpiry() != 0) {
                    setExpiry(transaction.getExpiry());
                }
                mergeUnknownFields(transaction.unknownFields);
                onChanged();
                return this;
            }

            public Builder addInputs(TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addOutputs(TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Decred.Transaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Decred.Transaction.access$1500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Decred$Transaction r3 = (wallet.core.jni.proto.Decred.Transaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Decred$Transaction r4 = (wallet.core.jni.proto.Decred.Transaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Decred.Transaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Decred$Transaction$Builder");
            }
        }

        public static Builder newBuilder(Transaction transaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transaction);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static Transaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Transaction() {
            this.memoizedIsInitialized = (byte) -1;
            this.inputs_ = Collections.emptyList();
            this.outputs_ = Collections.emptyList();
        }

        public static Transaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static Transaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transaction parseFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private Transaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.serializeType_ = jVar.K();
                            } else if (J == 16) {
                                this.version_ = jVar.K();
                            } else if (J == 26) {
                                if (!(z2 & true)) {
                                    this.inputs_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.inputs_.add(jVar.z(TransactionInput.parser(), rVar));
                            } else if (J == 34) {
                                if (!(z2 & true)) {
                                    this.outputs_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.outputs_.add(jVar.z(TransactionOutput.parser(), rVar));
                            } else if (J == 40) {
                                this.lockTime_ = jVar.K();
                            } else if (J != 48) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.expiry_ = jVar.K();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                    }
                    if (z2 & true) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(j jVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transaction parseFrom(j jVar, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public static final class TransactionInput extends GeneratedMessageV3 implements TransactionInputOrBuilder {
        public static final int BLOCKHEIGHT_FIELD_NUMBER = 4;
        public static final int BLOCKINDEX_FIELD_NUMBER = 5;
        private static final TransactionInput DEFAULT_INSTANCE = new TransactionInput();
        private static final t0<TransactionInput> PARSER = new c<TransactionInput>() { // from class: wallet.core.jni.proto.Decred.TransactionInput.1
            @Override // com.google.protobuf.t0
            public TransactionInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionInput(jVar, rVar);
            }
        };
        public static final int PREVIOUSOUTPUT_FIELD_NUMBER = 1;
        public static final int SCRIPT_FIELD_NUMBER = 6;
        public static final int SEQUENCE_FIELD_NUMBER = 2;
        public static final int VALUEIN_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private int blockHeight_;
        private int blockIndex_;
        private byte memoizedIsInitialized;
        private Bitcoin.OutPoint previousOutput_;
        private ByteString script_;
        private int sequence_;
        private long valueIn_;

        public static TransactionInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Decred.internal_static_TW_Decred_Proto_TransactionInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionInput)) {
                return super.equals(obj);
            }
            TransactionInput transactionInput = (TransactionInput) obj;
            if (hasPreviousOutput() != transactionInput.hasPreviousOutput()) {
                return false;
            }
            return (!hasPreviousOutput() || getPreviousOutput().equals(transactionInput.getPreviousOutput())) && getSequence() == transactionInput.getSequence() && getValueIn() == transactionInput.getValueIn() && getBlockHeight() == transactionInput.getBlockHeight() && getBlockIndex() == transactionInput.getBlockIndex() && getScript().equals(transactionInput.getScript()) && this.unknownFields.equals(transactionInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public int getBlockHeight() {
            return this.blockHeight_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public int getBlockIndex() {
            return this.blockIndex_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public Bitcoin.OutPoint getPreviousOutput() {
            Bitcoin.OutPoint outPoint = this.previousOutput_;
            return outPoint == null ? Bitcoin.OutPoint.getDefaultInstance() : outPoint;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public Bitcoin.OutPointOrBuilder getPreviousOutputOrBuilder() {
            return getPreviousOutput();
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public ByteString getScript() {
            return this.script_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public int getSequence() {
            return this.sequence_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.previousOutput_ != null ? 0 + CodedOutputStream.G(1, getPreviousOutput()) : 0;
            int i2 = this.sequence_;
            if (i2 != 0) {
                G += CodedOutputStream.Y(2, i2);
            }
            long j = this.valueIn_;
            if (j != 0) {
                G += CodedOutputStream.z(3, j);
            }
            int i3 = this.blockHeight_;
            if (i3 != 0) {
                G += CodedOutputStream.Y(4, i3);
            }
            int i4 = this.blockIndex_;
            if (i4 != 0) {
                G += CodedOutputStream.Y(5, i4);
            }
            if (!this.script_.isEmpty()) {
                G += CodedOutputStream.h(6, this.script_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public long getValueIn() {
            return this.valueIn_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
        public boolean hasPreviousOutput() {
            return this.previousOutput_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasPreviousOutput()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getPreviousOutput().hashCode();
            }
            int sequence = (((((((((((((((((((((hashCode * 37) + 2) * 53) + getSequence()) * 37) + 3) * 53) + a0.h(getValueIn())) * 37) + 4) * 53) + getBlockHeight()) * 37) + 5) * 53) + getBlockIndex()) * 37) + 6) * 53) + getScript().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = sequence;
            return sequence;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Decred.internal_static_TW_Decred_Proto_TransactionInput_fieldAccessorTable.d(TransactionInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.previousOutput_ != null) {
                codedOutputStream.K0(1, getPreviousOutput());
            }
            int i = this.sequence_;
            if (i != 0) {
                codedOutputStream.b1(2, i);
            }
            long j = this.valueIn_;
            if (j != 0) {
                codedOutputStream.I0(3, j);
            }
            int i2 = this.blockHeight_;
            if (i2 != 0) {
                codedOutputStream.b1(4, i2);
            }
            int i3 = this.blockIndex_;
            if (i3 != 0) {
                codedOutputStream.b1(5, i3);
            }
            if (!this.script_.isEmpty()) {
                codedOutputStream.q0(6, this.script_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionInputOrBuilder {
            private int blockHeight_;
            private int blockIndex_;
            private a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> previousOutputBuilder_;
            private Bitcoin.OutPoint previousOutput_;
            private ByteString script_;
            private int sequence_;
            private long valueIn_;

            public static final Descriptors.b getDescriptor() {
                return Decred.internal_static_TW_Decred_Proto_TransactionInput_descriptor;
            }

            private a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> getPreviousOutputFieldBuilder() {
                if (this.previousOutputBuilder_ == null) {
                    this.previousOutputBuilder_ = new a1<>(getPreviousOutput(), getParentForChildren(), isClean());
                    this.previousOutput_ = null;
                }
                return this.previousOutputBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBlockHeight() {
                this.blockHeight_ = 0;
                onChanged();
                return this;
            }

            public Builder clearBlockIndex() {
                this.blockIndex_ = 0;
                onChanged();
                return this;
            }

            public Builder clearPreviousOutput() {
                if (this.previousOutputBuilder_ == null) {
                    this.previousOutput_ = null;
                    onChanged();
                } else {
                    this.previousOutput_ = null;
                    this.previousOutputBuilder_ = null;
                }
                return this;
            }

            public Builder clearScript() {
                this.script_ = TransactionInput.getDefaultInstance().getScript();
                onChanged();
                return this;
            }

            public Builder clearSequence() {
                this.sequence_ = 0;
                onChanged();
                return this;
            }

            public Builder clearValueIn() {
                this.valueIn_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public int getBlockHeight() {
                return this.blockHeight_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public int getBlockIndex() {
                return this.blockIndex_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Decred.internal_static_TW_Decred_Proto_TransactionInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public Bitcoin.OutPoint getPreviousOutput() {
                a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    Bitcoin.OutPoint outPoint = this.previousOutput_;
                    return outPoint == null ? Bitcoin.OutPoint.getDefaultInstance() : outPoint;
                }
                return a1Var.f();
            }

            public Bitcoin.OutPoint.Builder getPreviousOutputBuilder() {
                onChanged();
                return getPreviousOutputFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public Bitcoin.OutPointOrBuilder getPreviousOutputOrBuilder() {
                a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Bitcoin.OutPoint outPoint = this.previousOutput_;
                return outPoint == null ? Bitcoin.OutPoint.getDefaultInstance() : outPoint;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public ByteString getScript() {
                return this.script_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public int getSequence() {
                return this.sequence_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public long getValueIn() {
                return this.valueIn_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionInputOrBuilder
            public boolean hasPreviousOutput() {
                return (this.previousOutputBuilder_ == null && this.previousOutput_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Decred.internal_static_TW_Decred_Proto_TransactionInput_fieldAccessorTable.d(TransactionInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergePreviousOutput(Bitcoin.OutPoint outPoint) {
                a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    Bitcoin.OutPoint outPoint2 = this.previousOutput_;
                    if (outPoint2 != null) {
                        this.previousOutput_ = Bitcoin.OutPoint.newBuilder(outPoint2).mergeFrom(outPoint).buildPartial();
                    } else {
                        this.previousOutput_ = outPoint;
                    }
                    onChanged();
                } else {
                    a1Var.h(outPoint);
                }
                return this;
            }

            public Builder setBlockHeight(int i) {
                this.blockHeight_ = i;
                onChanged();
                return this;
            }

            public Builder setBlockIndex(int i) {
                this.blockIndex_ = i;
                onChanged();
                return this;
            }

            public Builder setPreviousOutput(Bitcoin.OutPoint outPoint) {
                a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(outPoint);
                    this.previousOutput_ = outPoint;
                    onChanged();
                } else {
                    a1Var.j(outPoint);
                }
                return this;
            }

            public Builder setScript(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.script_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSequence(int i) {
                this.sequence_ = i;
                onChanged();
                return this;
            }

            public Builder setValueIn(long j) {
                this.valueIn_ = j;
                onChanged();
                return this;
            }

            private Builder() {
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionInput build() {
                TransactionInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionInput buildPartial() {
                TransactionInput transactionInput = new TransactionInput(this);
                a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    transactionInput.previousOutput_ = this.previousOutput_;
                } else {
                    transactionInput.previousOutput_ = a1Var.b();
                }
                transactionInput.sequence_ = this.sequence_;
                transactionInput.valueIn_ = this.valueIn_;
                transactionInput.blockHeight_ = this.blockHeight_;
                transactionInput.blockIndex_ = this.blockIndex_;
                transactionInput.script_ = this.script_;
                onBuilt();
                return transactionInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionInput getDefaultInstanceForType() {
                return TransactionInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.previousOutputBuilder_ == null) {
                    this.previousOutput_ = null;
                } else {
                    this.previousOutput_ = null;
                    this.previousOutputBuilder_ = null;
                }
                this.sequence_ = 0;
                this.valueIn_ = 0L;
                this.blockHeight_ = 0;
                this.blockIndex_ = 0;
                this.script_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder setPreviousOutput(Bitcoin.OutPoint.Builder builder) {
                a1<Bitcoin.OutPoint, Bitcoin.OutPoint.Builder, Bitcoin.OutPointOrBuilder> a1Var = this.previousOutputBuilder_;
                if (a1Var == null) {
                    this.previousOutput_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionInput) {
                    return mergeFrom((TransactionInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionInput transactionInput) {
                if (transactionInput == TransactionInput.getDefaultInstance()) {
                    return this;
                }
                if (transactionInput.hasPreviousOutput()) {
                    mergePreviousOutput(transactionInput.getPreviousOutput());
                }
                if (transactionInput.getSequence() != 0) {
                    setSequence(transactionInput.getSequence());
                }
                if (transactionInput.getValueIn() != 0) {
                    setValueIn(transactionInput.getValueIn());
                }
                if (transactionInput.getBlockHeight() != 0) {
                    setBlockHeight(transactionInput.getBlockHeight());
                }
                if (transactionInput.getBlockIndex() != 0) {
                    setBlockIndex(transactionInput.getBlockIndex());
                }
                if (transactionInput.getScript() != ByteString.EMPTY) {
                    setScript(transactionInput.getScript());
                }
                mergeUnknownFields(transactionInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Decred.TransactionInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Decred.TransactionInput.access$3000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Decred$TransactionInput r3 = (wallet.core.jni.proto.Decred.TransactionInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Decred$TransactionInput r4 = (wallet.core.jni.proto.Decred.TransactionInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Decred.TransactionInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Decred$TransactionInput$Builder");
            }
        }

        public static Builder newBuilder(TransactionInput transactionInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionInput);
        }

        public static TransactionInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.script_ = ByteString.EMPTY;
        }

        public static TransactionInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionInput parseFrom(InputStream inputStream) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                Bitcoin.OutPoint outPoint = this.previousOutput_;
                                Bitcoin.OutPoint.Builder builder = outPoint != null ? outPoint.toBuilder() : null;
                                Bitcoin.OutPoint outPoint2 = (Bitcoin.OutPoint) jVar.z(Bitcoin.OutPoint.parser(), rVar);
                                this.previousOutput_ = outPoint2;
                                if (builder != null) {
                                    builder.mergeFrom(outPoint2);
                                    this.previousOutput_ = builder.buildPartial();
                                }
                            } else if (J == 16) {
                                this.sequence_ = jVar.K();
                            } else if (J == 24) {
                                this.valueIn_ = jVar.y();
                            } else if (J == 32) {
                                this.blockHeight_ = jVar.K();
                            } else if (J == 40) {
                                this.blockIndex_ = jVar.K();
                            } else if (J != 50) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.script_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionInput parseFrom(j jVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionInput parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        int getBlockHeight();

        int getBlockIndex();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Bitcoin.OutPoint getPreviousOutput();

        Bitcoin.OutPointOrBuilder getPreviousOutputOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getScript();

        int getSequence();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        long getValueIn();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasPreviousOutput();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public interface TransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        int getExpiry();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        TransactionInput getInputs(int i);

        int getInputsCount();

        List<TransactionInput> getInputsList();

        TransactionInputOrBuilder getInputsOrBuilder(int i);

        List<? extends TransactionInputOrBuilder> getInputsOrBuilderList();

        int getLockTime();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        TransactionOutput getOutputs(int i);

        int getOutputsCount();

        List<TransactionOutput> getOutputsList();

        TransactionOutputOrBuilder getOutputsOrBuilder(int i);

        List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        int getSerializeType();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        int getVersion();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionOutput extends GeneratedMessageV3 implements TransactionOutputOrBuilder {
        private static final TransactionOutput DEFAULT_INSTANCE = new TransactionOutput();
        private static final t0<TransactionOutput> PARSER = new c<TransactionOutput>() { // from class: wallet.core.jni.proto.Decred.TransactionOutput.1
            @Override // com.google.protobuf.t0
            public TransactionOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionOutput(jVar, rVar);
            }
        };
        public static final int SCRIPT_FIELD_NUMBER = 3;
        public static final int VALUE_FIELD_NUMBER = 1;
        public static final int VERSION_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString script_;
        private long value_;
        private int version_;

        public static TransactionOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Decred.internal_static_TW_Decred_Proto_TransactionOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionOutput)) {
                return super.equals(obj);
            }
            TransactionOutput transactionOutput = (TransactionOutput) obj;
            return getValue() == transactionOutput.getValue() && getVersion() == transactionOutput.getVersion() && getScript().equals(transactionOutput.getScript()) && this.unknownFields.equals(transactionOutput.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionOutput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOutputOrBuilder
        public ByteString getScript() {
            return this.script_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.value_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            int i2 = this.version_;
            if (i2 != 0) {
                z += CodedOutputStream.Y(2, i2);
            }
            if (!this.script_.isEmpty()) {
                z += CodedOutputStream.h(3, this.script_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOutputOrBuilder
        public long getValue() {
            return this.value_;
        }

        @Override // wallet.core.jni.proto.Decred.TransactionOutputOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getValue())) * 37) + 2) * 53) + getVersion()) * 37) + 3) * 53) + getScript().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Decred.internal_static_TW_Decred_Proto_TransactionOutput_fieldAccessorTable.d(TransactionOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.value_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            int i = this.version_;
            if (i != 0) {
                codedOutputStream.b1(2, i);
            }
            if (!this.script_.isEmpty()) {
                codedOutputStream.q0(3, this.script_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOutputOrBuilder {
            private ByteString script_;
            private long value_;
            private int version_;

            public static final Descriptors.b getDescriptor() {
                return Decred.internal_static_TW_Decred_Proto_TransactionOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearScript() {
                this.script_ = TransactionOutput.getDefaultInstance().getScript();
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Decred.internal_static_TW_Decred_Proto_TransactionOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOutputOrBuilder
            public ByteString getScript() {
                return this.script_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOutputOrBuilder
            public long getValue() {
                return this.value_;
            }

            @Override // wallet.core.jni.proto.Decred.TransactionOutputOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Decred.internal_static_TW_Decred_Proto_TransactionOutput_fieldAccessorTable.d(TransactionOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setScript(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.script_ = byteString;
                onChanged();
                return this;
            }

            public Builder setValue(long j) {
                this.value_ = j;
                onChanged();
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            private Builder() {
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutput build() {
                TransactionOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutput buildPartial() {
                TransactionOutput transactionOutput = new TransactionOutput(this);
                transactionOutput.value_ = this.value_;
                transactionOutput.version_ = this.version_;
                transactionOutput.script_ = this.script_;
                onBuilt();
                return transactionOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionOutput getDefaultInstanceForType() {
                return TransactionOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.value_ = 0L;
                this.version_ = 0;
                this.script_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.script_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionOutput) {
                    return mergeFrom((TransactionOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionOutput transactionOutput) {
                if (transactionOutput == TransactionOutput.getDefaultInstance()) {
                    return this;
                }
                if (transactionOutput.getValue() != 0) {
                    setValue(transactionOutput.getValue());
                }
                if (transactionOutput.getVersion() != 0) {
                    setVersion(transactionOutput.getVersion());
                }
                if (transactionOutput.getScript() != ByteString.EMPTY) {
                    setScript(transactionOutput.getScript());
                }
                mergeUnknownFields(transactionOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Decred.TransactionOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Decred.TransactionOutput.access$4200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Decred$TransactionOutput r3 = (wallet.core.jni.proto.Decred.TransactionOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Decred$TransactionOutput r4 = (wallet.core.jni.proto.Decred.TransactionOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Decred.TransactionOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Decred$TransactionOutput$Builder");
            }
        }

        public static Builder newBuilder(TransactionOutput transactionOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionOutput);
        }

        public static TransactionOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.script_ = ByteString.EMPTY;
        }

        public static TransactionOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionOutput parseFrom(InputStream inputStream) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.value_ = jVar.y();
                            } else if (J == 16) {
                                this.version_ = jVar.K();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.script_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOutput parseFrom(j jVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionOutput parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getScript();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        long getValue();

        int getVersion();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Decred_Proto_Transaction_descriptor = bVar;
        internal_static_TW_Decred_Proto_Transaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"SerializeType", "Version", "Inputs", "Outputs", "LockTime", "Expiry"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Decred_Proto_TransactionInput_descriptor = bVar2;
        internal_static_TW_Decred_Proto_TransactionInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"PreviousOutput", "Sequence", "ValueIn", "BlockHeight", "BlockIndex", "Script"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Decred_Proto_TransactionOutput_descriptor = bVar3;
        internal_static_TW_Decred_Proto_TransactionOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Value", "Version", "Script"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Decred_Proto_SigningOutput_descriptor = bVar4;
        internal_static_TW_Decred_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Transaction", "Encoded", "TransactionId", "Error"});
        Bitcoin.getDescriptor();
        Common.getDescriptor();
    }

    private Decred() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
