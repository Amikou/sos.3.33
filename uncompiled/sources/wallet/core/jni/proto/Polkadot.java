package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.d0;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.v0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Polkadot {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000ePolkadot.proto\u0012\u0011TW.Polkadot.Proto\"+\n\u0003Era\u0012\u0014\n\fblock_number\u0018\u0001 \u0001(\u0004\u0012\u000e\n\u0006period\u0018\u0002 \u0001(\u0004\"\u0082\u0001\n\u0007Balance\u00127\n\btransfer\u0018\u0001 \u0001(\u000b2#.TW.Polkadot.Proto.Balance.TransferH\u0000\u001a-\n\bTransfer\u0012\u0012\n\nto_address\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\fB\u000f\n\rmessage_oneof\"¿\u0006\n\u0007Staking\u0012/\n\u0004bond\u0018\u0001 \u0001(\u000b2\u001f.TW.Polkadot.Proto.Staking.BondH\u0000\u0012G\n\u0011bond_and_nominate\u0018\u0002 \u0001(\u000b2*.TW.Polkadot.Proto.Staking.BondAndNominateH\u0000\u0012:\n\nbond_extra\u0018\u0003 \u0001(\u000b2$.TW.Polkadot.Proto.Staking.BondExtraH\u0000\u00123\n\u0006unbond\u0018\u0004 \u0001(\u000b2!.TW.Polkadot.Proto.Staking.UnbondH\u0000\u0012H\n\u0011withdraw_unbonded\u0018\u0005 \u0001(\u000b2+.TW.Polkadot.Proto.Staking.WithdrawUnbondedH\u0000\u00127\n\bnominate\u0018\u0006 \u0001(\u000b2#.TW.Polkadot.Proto.Staking.NominateH\u0000\u00121\n\u0005chill\u0018\u0007 \u0001(\u000b2 .TW.Polkadot.Proto.Staking.ChillH\u0000\u001ak\n\u0004Bond\u0012\u0012\n\ncontroller\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\f\u0012@\n\u0012reward_destination\u0018\u0003 \u0001(\u000e2$.TW.Polkadot.Proto.RewardDestination\u001a\u008a\u0001\n\u000fBondAndNominate\u0012\u0012\n\ncontroller\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\f\u0012@\n\u0012reward_destination\u0018\u0003 \u0001(\u000e2$.TW.Polkadot.Proto.RewardDestination\u0012\u0012\n\nnominators\u0018\u0004 \u0003(\t\u001a\u001a\n\tBondExtra\u0012\r\n\u0005value\u0018\u0001 \u0001(\f\u001a\u0017\n\u0006Unbond\u0012\r\n\u0005value\u0018\u0001 \u0001(\f\u001a*\n\u0010WithdrawUnbonded\u0012\u0016\n\u000eslashing_spans\u0018\u0001 \u0001(\u0005\u001a\u001e\n\bNominate\u0012\u0012\n\nnominators\u0018\u0001 \u0003(\t\u001a\u0007\n\u0005ChillB\u000f\n\rmessage_oneof\"ç\u0002\n\fSigningInput\u0012\u0012\n\nblock_hash\u0018\u0001 \u0001(\f\u0012\u0014\n\fgenesis_hash\u0018\u0002 \u0001(\f\u0012\r\n\u0005nonce\u0018\u0003 \u0001(\u0004\u0012\u0014\n\fspec_version\u0018\u0004 \u0001(\r\u0012\u001b\n\u0013transaction_version\u0018\u0005 \u0001(\r\u0012\u000b\n\u0003tip\u0018\u0006 \u0001(\f\u0012#\n\u0003era\u0018\u0007 \u0001(\u000b2\u0016.TW.Polkadot.Proto.Era\u0012\u0013\n\u000bprivate_key\u0018\b \u0001(\f\u0012+\n\u0007network\u0018\t \u0001(\u000e2\u001a.TW.Polkadot.Proto.Network\u00122\n\fbalance_call\u0018\n \u0001(\u000b2\u001a.TW.Polkadot.Proto.BalanceH\u0000\u00122\n\fstaking_call\u0018\u000b \u0001(\u000b2\u001a.TW.Polkadot.Proto.StakingH\u0000B\u000f\n\rmessage_oneof\" \n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f*#\n\u0007Network\u0012\f\n\bPOLKADOT\u0010\u0000\u0012\n\n\u0006KUSAMA\u0010\u0002*:\n\u0011RewardDestination\u0012\n\n\u0006STAKED\u0010\u0000\u0012\t\n\u0005STASH\u0010\u0001\u0012\u000e\n\nCONTROLLER\u0010\u0002B\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Balance_Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Balance_Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Balance_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Balance_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Era_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Era_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_BondExtra_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_BondExtra_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_Bond_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_Bond_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_Chill_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_Chill_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_Nominate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_Nominate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_Unbond_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_Unbond_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Polkadot_Proto_Staking_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Polkadot_Proto_Staking_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Polkadot$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Polkadot$Balance$MessageOneofCase;
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Polkadot$SigningInput$MessageOneofCase;
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase;

        static {
            int[] iArr = new int[SigningInput.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Polkadot$SigningInput$MessageOneofCase = iArr;
            try {
                iArr[SigningInput.MessageOneofCase.BALANCE_CALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.STAKING_CALL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[Staking.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase = iArr2;
            try {
                iArr2[Staking.MessageOneofCase.BOND.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[Staking.MessageOneofCase.BOND_AND_NOMINATE.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[Staking.MessageOneofCase.BOND_EXTRA.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[Staking.MessageOneofCase.UNBOND.ordinal()] = 4;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[Staking.MessageOneofCase.WITHDRAW_UNBONDED.ordinal()] = 5;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[Staking.MessageOneofCase.NOMINATE.ordinal()] = 6;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[Staking.MessageOneofCase.CHILL.ordinal()] = 7;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[Staking.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 8;
            } catch (NoSuchFieldError unused11) {
            }
            int[] iArr3 = new int[Balance.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Polkadot$Balance$MessageOneofCase = iArr3;
            try {
                iArr3[Balance.MessageOneofCase.TRANSFER.ordinal()] = 1;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Polkadot$Balance$MessageOneofCase[Balance.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 2;
            } catch (NoSuchFieldError unused13) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class Balance extends GeneratedMessageV3 implements BalanceOrBuilder {
        private static final Balance DEFAULT_INSTANCE = new Balance();
        private static final t0<Balance> PARSER = new c<Balance>() { // from class: wallet.core.jni.proto.Polkadot.Balance.1
            @Override // com.google.protobuf.t0
            public Balance parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Balance(jVar, rVar, null);
            }
        };
        public static final int TRANSFER_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements BalanceOrBuilder {
            private int messageOneofCase_;
            private Object messageOneof_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Balance_descriptor;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferFieldBuilder() {
                if (this.transferBuilder_ == null) {
                    if (this.messageOneofCase_ != 1) {
                        this.messageOneof_ = Transfer.getDefaultInstance();
                    }
                    this.transferBuilder_ = new a1<>((Transfer) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 1;
                onChanged();
                return this.transferBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Balance_descriptor;
            }

            @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
            public Transfer getTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        return (Transfer) this.messageOneof_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.messageOneofCase_ == 1) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferBuilder() {
                return getTransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
            public TransferOrBuilder getTransferOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 1 || (a1Var = this.transferBuilder_) == null) {
                    if (i == 1) {
                        return (Transfer) this.messageOneof_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
            public boolean hasTransfer() {
                return this.messageOneofCase_ == 1;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Balance_fieldAccessorTable.d(Balance.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1 && this.messageOneof_ != Transfer.getDefaultInstance()) {
                        this.messageOneof_ = Transfer.newBuilder((Transfer) this.messageOneof_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.messageOneof_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 1) {
                        a1Var.h(transfer);
                    }
                    this.transferBuilder_.j(transfer);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.messageOneof_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Balance build() {
                Balance buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Balance buildPartial() {
                Balance balance = new Balance(this, (AnonymousClass1) null);
                if (this.messageOneofCase_ == 1) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                    if (a1Var == null) {
                        balance.messageOneof_ = this.messageOneof_;
                    } else {
                        balance.messageOneof_ = a1Var.b();
                    }
                }
                balance.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return balance;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Balance getDefaultInstanceForType() {
                return Balance.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Balance) {
                    return mergeFrom((Balance) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setTransfer(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder mergeFrom(Balance balance) {
                if (balance == Balance.getDefaultInstance()) {
                    return this;
                }
                if (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Polkadot$Balance$MessageOneofCase[balance.getMessageOneofCase().ordinal()] == 1) {
                    mergeTransfer(balance.getTransfer());
                }
                mergeUnknownFields(balance.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Polkadot.Balance.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Balance.access$3200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Polkadot$Balance r3 = (wallet.core.jni.proto.Polkadot.Balance) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Polkadot$Balance r4 = (wallet.core.jni.proto.Polkadot.Balance) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Balance.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Balance$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            TRANSFER(1),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 1) {
                        return null;
                    }
                    return TRANSFER;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class Transfer extends GeneratedMessageV3 implements TransferOrBuilder {
            private static final Transfer DEFAULT_INSTANCE = new Transfer();
            private static final t0<Transfer> PARSER = new c<Transfer>() { // from class: wallet.core.jni.proto.Polkadot.Balance.Transfer.1
                @Override // com.google.protobuf.t0
                public Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Transfer(jVar, rVar, null);
                }
            };
            public static final int TO_ADDRESS_FIELD_NUMBER = 1;
            public static final int VALUE_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private volatile Object toAddress_;
            private ByteString value_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOrBuilder {
                private Object toAddress_;
                private ByteString value_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Balance_Transfer_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearToAddress() {
                    this.toAddress_ = Transfer.getDefaultInstance().getToAddress();
                    onChanged();
                    return this;
                }

                public Builder clearValue() {
                    this.value_ = Transfer.getDefaultInstance().getValue();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Balance_Transfer_descriptor;
                }

                @Override // wallet.core.jni.proto.Polkadot.Balance.TransferOrBuilder
                public String getToAddress() {
                    Object obj = this.toAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.toAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Polkadot.Balance.TransferOrBuilder
                public ByteString getToAddressBytes() {
                    Object obj = this.toAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.toAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Polkadot.Balance.TransferOrBuilder
                public ByteString getValue() {
                    return this.value_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Balance_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setToAddress(String str) {
                    Objects.requireNonNull(str);
                    this.toAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setToAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.toAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValue(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.value_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.toAddress_ = "";
                    this.value_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer build() {
                    Transfer buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer buildPartial() {
                    Transfer transfer = new Transfer(this, (AnonymousClass1) null);
                    transfer.toAddress_ = this.toAddress_;
                    transfer.value_ = this.value_;
                    onBuilt();
                    return transfer;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Transfer getDefaultInstanceForType() {
                    return Transfer.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.toAddress_ = "";
                    this.value_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.toAddress_ = "";
                    this.value_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Transfer) {
                        return mergeFrom((Transfer) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Transfer transfer) {
                    if (transfer == Transfer.getDefaultInstance()) {
                        return this;
                    }
                    if (!transfer.getToAddress().isEmpty()) {
                        this.toAddress_ = transfer.toAddress_;
                        onChanged();
                    }
                    if (transfer.getValue() != ByteString.EMPTY) {
                        setValue(transfer.getValue());
                    }
                    mergeUnknownFields(transfer.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Balance.Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Balance.Transfer.access$2200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Balance$Transfer r3 = (wallet.core.jni.proto.Polkadot.Balance.Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Balance$Transfer r4 = (wallet.core.jni.proto.Polkadot.Balance.Transfer) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Balance.Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Balance$Transfer$Builder");
                }
            }

            public /* synthetic */ Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Transfer getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Balance_Transfer_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Transfer> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Transfer)) {
                    return super.equals(obj);
                }
                Transfer transfer = (Transfer) obj;
                return getToAddress().equals(transfer.getToAddress()) && getValue().equals(transfer.getValue()) && this.unknownFields.equals(transfer.unknownFields);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Transfer> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getToAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.toAddress_);
                if (!this.value_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(2, this.value_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Polkadot.Balance.TransferOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.toAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Polkadot.Balance.TransferOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Polkadot.Balance.TransferOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getToAddress().hashCode()) * 37) + 2) * 53) + getValue().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Balance_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Transfer();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getToAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.toAddress_);
                }
                if (!this.value_.isEmpty()) {
                    codedOutputStream.q0(2, this.value_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Transfer transfer) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(transfer);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Transfer(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Transfer getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Transfer() {
                this.memoizedIsInitialized = (byte) -1;
                this.toAddress_ = "";
                this.value_ = ByteString.EMPTY;
            }

            public static Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Transfer parseFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.toAddress_ = jVar.I();
                                    } else if (J != 18) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.value_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(j jVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Transfer parseFrom(j jVar, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface TransferOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getToAddress();

            ByteString getToAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            ByteString getValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ Balance(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Balance getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Polkadot.internal_static_TW_Polkadot_Proto_Balance_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Balance parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Balance) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Balance parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Balance> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Balance)) {
                return super.equals(obj);
            }
            Balance balance = (Balance) obj;
            if (getMessageOneofCase().equals(balance.getMessageOneofCase())) {
                return (this.messageOneofCase_ != 1 || getTransfer().equals(balance.getTransfer())) && this.unknownFields.equals(balance.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Balance> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = (this.messageOneofCase_ == 1 ? 0 + CodedOutputStream.G(1, (Transfer) this.messageOneof_) : 0) + this.unknownFields.getSerializedSize();
            this.memoizedSize = G;
            return G;
        }

        @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
        public Transfer getTransfer() {
            if (this.messageOneofCase_ == 1) {
                return (Transfer) this.messageOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
        public TransferOrBuilder getTransferOrBuilder() {
            if (this.messageOneofCase_ == 1) {
                return (Transfer) this.messageOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Polkadot.BalanceOrBuilder
        public boolean hasTransfer() {
            return this.messageOneofCase_ == 1;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (this.messageOneofCase_ == 1) {
                hashCode = (((hashCode * 37) + 1) * 53) + getTransfer().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Polkadot.internal_static_TW_Polkadot_Proto_Balance_fieldAccessorTable.d(Balance.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Balance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.messageOneofCase_ == 1) {
                codedOutputStream.K0(1, (Transfer) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Balance(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Balance balance) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(balance);
        }

        public static Balance parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Balance(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Balance parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Balance) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Balance parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Balance getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Balance parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Balance parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Balance() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Balance parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Balance parseFrom(InputStream inputStream) throws IOException {
            return (Balance) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Balance(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                Transfer.Builder builder = this.messageOneofCase_ == 1 ? ((Transfer) this.messageOneof_).toBuilder() : null;
                                m0 z2 = jVar.z(Transfer.parser(), rVar);
                                this.messageOneof_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((Transfer) z2);
                                    this.messageOneof_ = builder.buildPartial();
                                }
                                this.messageOneofCase_ = 1;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Balance parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Balance) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Balance parseFrom(j jVar) throws IOException {
            return (Balance) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Balance parseFrom(j jVar, r rVar) throws IOException {
            return (Balance) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface BalanceOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        Balance.MessageOneofCase getMessageOneofCase();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Balance.Transfer getTransfer();

        Balance.TransferOrBuilder getTransferOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransfer();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Era extends GeneratedMessageV3 implements EraOrBuilder {
        public static final int BLOCK_NUMBER_FIELD_NUMBER = 1;
        private static final Era DEFAULT_INSTANCE = new Era();
        private static final t0<Era> PARSER = new c<Era>() { // from class: wallet.core.jni.proto.Polkadot.Era.1
            @Override // com.google.protobuf.t0
            public Era parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Era(jVar, rVar, null);
            }
        };
        public static final int PERIOD_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private long blockNumber_;
        private byte memoizedIsInitialized;
        private long period_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements EraOrBuilder {
            private long blockNumber_;
            private long period_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Era_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBlockNumber() {
                this.blockNumber_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearPeriod() {
                this.period_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Polkadot.EraOrBuilder
            public long getBlockNumber() {
                return this.blockNumber_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Era_descriptor;
            }

            @Override // wallet.core.jni.proto.Polkadot.EraOrBuilder
            public long getPeriod() {
                return this.period_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Era_fieldAccessorTable.d(Era.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setBlockNumber(long j) {
                this.blockNumber_ = j;
                onChanged();
                return this;
            }

            public Builder setPeriod(long j) {
                this.period_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Era build() {
                Era buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Era buildPartial() {
                Era era = new Era(this, (AnonymousClass1) null);
                era.blockNumber_ = this.blockNumber_;
                era.period_ = this.period_;
                onBuilt();
                return era;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Era getDefaultInstanceForType() {
                return Era.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.blockNumber_ = 0L;
                this.period_ = 0L;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Era) {
                    return mergeFrom((Era) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Era era) {
                if (era == Era.getDefaultInstance()) {
                    return this;
                }
                if (era.getBlockNumber() != 0) {
                    setBlockNumber(era.getBlockNumber());
                }
                if (era.getPeriod() != 0) {
                    setPeriod(era.getPeriod());
                }
                mergeUnknownFields(era.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Polkadot.Era.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Era.access$900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Polkadot$Era r3 = (wallet.core.jni.proto.Polkadot.Era) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Polkadot$Era r4 = (wallet.core.jni.proto.Polkadot.Era) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Era.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Era$Builder");
            }
        }

        public /* synthetic */ Era(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Era getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Polkadot.internal_static_TW_Polkadot_Proto_Era_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Era parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Era) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Era parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Era> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Era)) {
                return super.equals(obj);
            }
            Era era = (Era) obj;
            return getBlockNumber() == era.getBlockNumber() && getPeriod() == era.getPeriod() && this.unknownFields.equals(era.unknownFields);
        }

        @Override // wallet.core.jni.proto.Polkadot.EraOrBuilder
        public long getBlockNumber() {
            return this.blockNumber_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Era> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Polkadot.EraOrBuilder
        public long getPeriod() {
            return this.period_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.blockNumber_;
            int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
            long j2 = this.period_;
            if (j2 != 0) {
                a0 += CodedOutputStream.a0(2, j2);
            }
            int serializedSize = a0 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getBlockNumber())) * 37) + 2) * 53) + a0.h(getPeriod())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Polkadot.internal_static_TW_Polkadot_Proto_Era_fieldAccessorTable.d(Era.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Era();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.blockNumber_;
            if (j != 0) {
                codedOutputStream.d1(1, j);
            }
            long j2 = this.period_;
            if (j2 != 0) {
                codedOutputStream.d1(2, j2);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Era(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Era era) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(era);
        }

        public static Era parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Era(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Era parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Era) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Era parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Era getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Era parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Era() {
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Era parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Era parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        private Era(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.blockNumber_ = jVar.L();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.period_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Era parseFrom(InputStream inputStream) throws IOException {
            return (Era) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static Era parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Era) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Era parseFrom(j jVar) throws IOException {
            return (Era) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Era parseFrom(j jVar, r rVar) throws IOException {
            return (Era) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface EraOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getBlockNumber();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        long getPeriod();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public enum Network implements v0 {
        POLKADOT(0),
        KUSAMA(2),
        UNRECOGNIZED(-1);
        
        public static final int KUSAMA_VALUE = 2;
        public static final int POLKADOT_VALUE = 0;
        private final int value;
        private static final a0.d<Network> internalValueMap = new a0.d<Network>() { // from class: wallet.core.jni.proto.Polkadot.Network.1
            @Override // com.google.protobuf.a0.d
            public Network findValueByNumber(int i) {
                return Network.forNumber(i);
            }
        };
        private static final Network[] VALUES = values();

        Network(int i) {
            this.value = i;
        }

        public static Network forNumber(int i) {
            if (i != 0) {
                if (i != 2) {
                    return null;
                }
                return KUSAMA;
            }
            return POLKADOT;
        }

        public static final Descriptors.c getDescriptor() {
            return Polkadot.getDescriptor().l().get(0);
        }

        public static a0.d<Network> internalGetValueMap() {
            return internalValueMap;
        }

        public final Descriptors.c getDescriptorForType() {
            return getDescriptor();
        }

        @Override // com.google.protobuf.a0.c
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        public final Descriptors.d getValueDescriptor() {
            if (this != UNRECOGNIZED) {
                return getDescriptor().k().get(ordinal());
            }
            throw new IllegalStateException("Can't get the descriptor of an unrecognized enum value.");
        }

        @Deprecated
        public static Network valueOf(int i) {
            return forNumber(i);
        }

        public static Network valueOf(Descriptors.d dVar) {
            if (dVar.h() == getDescriptor()) {
                if (dVar.g() == -1) {
                    return UNRECOGNIZED;
                }
                return VALUES[dVar.g()];
            }
            throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        }
    }

    /* loaded from: classes3.dex */
    public enum RewardDestination implements v0 {
        STAKED(0),
        STASH(1),
        CONTROLLER(2),
        UNRECOGNIZED(-1);
        
        public static final int CONTROLLER_VALUE = 2;
        public static final int STAKED_VALUE = 0;
        public static final int STASH_VALUE = 1;
        private final int value;
        private static final a0.d<RewardDestination> internalValueMap = new a0.d<RewardDestination>() { // from class: wallet.core.jni.proto.Polkadot.RewardDestination.1
            @Override // com.google.protobuf.a0.d
            public RewardDestination findValueByNumber(int i) {
                return RewardDestination.forNumber(i);
            }
        };
        private static final RewardDestination[] VALUES = values();

        RewardDestination(int i) {
            this.value = i;
        }

        public static RewardDestination forNumber(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        return null;
                    }
                    return CONTROLLER;
                }
                return STASH;
            }
            return STAKED;
        }

        public static final Descriptors.c getDescriptor() {
            return Polkadot.getDescriptor().l().get(1);
        }

        public static a0.d<RewardDestination> internalGetValueMap() {
            return internalValueMap;
        }

        public final Descriptors.c getDescriptorForType() {
            return getDescriptor();
        }

        @Override // com.google.protobuf.a0.c
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        public final Descriptors.d getValueDescriptor() {
            if (this != UNRECOGNIZED) {
                return getDescriptor().k().get(ordinal());
            }
            throw new IllegalStateException("Can't get the descriptor of an unrecognized enum value.");
        }

        @Deprecated
        public static RewardDestination valueOf(int i) {
            return forNumber(i);
        }

        public static RewardDestination valueOf(Descriptors.d dVar) {
            if (dVar.h() == getDescriptor()) {
                if (dVar.g() == -1) {
                    return UNRECOGNIZED;
                }
                return VALUES[dVar.g()];
            }
            throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        }
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int BALANCE_CALL_FIELD_NUMBER = 10;
        public static final int BLOCK_HASH_FIELD_NUMBER = 1;
        public static final int ERA_FIELD_NUMBER = 7;
        public static final int GENESIS_HASH_FIELD_NUMBER = 2;
        public static final int NETWORK_FIELD_NUMBER = 9;
        public static final int NONCE_FIELD_NUMBER = 3;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 8;
        public static final int SPEC_VERSION_FIELD_NUMBER = 4;
        public static final int STAKING_CALL_FIELD_NUMBER = 11;
        public static final int TIP_FIELD_NUMBER = 6;
        public static final int TRANSACTION_VERSION_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private ByteString blockHash_;
        private Era era_;
        private ByteString genesisHash_;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;
        private int network_;
        private long nonce_;
        private ByteString privateKey_;
        private int specVersion_;
        private ByteString tip_;
        private int transactionVersion_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Polkadot.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private a1<Balance, Balance.Builder, BalanceOrBuilder> balanceCallBuilder_;
            private ByteString blockHash_;
            private a1<Era, Era.Builder, EraOrBuilder> eraBuilder_;
            private Era era_;
            private ByteString genesisHash_;
            private int messageOneofCase_;
            private Object messageOneof_;
            private int network_;
            private long nonce_;
            private ByteString privateKey_;
            private int specVersion_;
            private a1<Staking, Staking.Builder, StakingOrBuilder> stakingCallBuilder_;
            private ByteString tip_;
            private int transactionVersion_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<Balance, Balance.Builder, BalanceOrBuilder> getBalanceCallFieldBuilder() {
                if (this.balanceCallBuilder_ == null) {
                    if (this.messageOneofCase_ != 10) {
                        this.messageOneof_ = Balance.getDefaultInstance();
                    }
                    this.balanceCallBuilder_ = new a1<>((Balance) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 10;
                onChanged();
                return this.balanceCallBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_SigningInput_descriptor;
            }

            private a1<Era, Era.Builder, EraOrBuilder> getEraFieldBuilder() {
                if (this.eraBuilder_ == null) {
                    this.eraBuilder_ = new a1<>(getEra(), getParentForChildren(), isClean());
                    this.era_ = null;
                }
                return this.eraBuilder_;
            }

            private a1<Staking, Staking.Builder, StakingOrBuilder> getStakingCallFieldBuilder() {
                if (this.stakingCallBuilder_ == null) {
                    if (this.messageOneofCase_ != 11) {
                        this.messageOneof_ = Staking.getDefaultInstance();
                    }
                    this.stakingCallBuilder_ = new a1<>((Staking) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 11;
                onChanged();
                return this.stakingCallBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBalanceCall() {
                a1<Balance, Balance.Builder, BalanceOrBuilder> a1Var = this.balanceCallBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 10) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 10) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearBlockHash() {
                this.blockHash_ = SigningInput.getDefaultInstance().getBlockHash();
                onChanged();
                return this;
            }

            public Builder clearEra() {
                if (this.eraBuilder_ == null) {
                    this.era_ = null;
                    onChanged();
                } else {
                    this.era_ = null;
                    this.eraBuilder_ = null;
                }
                return this;
            }

            public Builder clearGenesisHash() {
                this.genesisHash_ = SigningInput.getDefaultInstance().getGenesisHash();
                onChanged();
                return this;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearNetwork() {
                this.network_ = 0;
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearSpecVersion() {
                this.specVersion_ = 0;
                onChanged();
                return this;
            }

            public Builder clearStakingCall() {
                a1<Staking, Staking.Builder, StakingOrBuilder> a1Var = this.stakingCallBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 11) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 11) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTip() {
                this.tip_ = SigningInput.getDefaultInstance().getTip();
                onChanged();
                return this;
            }

            public Builder clearTransactionVersion() {
                this.transactionVersion_ = 0;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public Balance getBalanceCall() {
                a1<Balance, Balance.Builder, BalanceOrBuilder> a1Var = this.balanceCallBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 10) {
                        return (Balance) this.messageOneof_;
                    }
                    return Balance.getDefaultInstance();
                } else if (this.messageOneofCase_ == 10) {
                    return a1Var.f();
                } else {
                    return Balance.getDefaultInstance();
                }
            }

            public Balance.Builder getBalanceCallBuilder() {
                return getBalanceCallFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public BalanceOrBuilder getBalanceCallOrBuilder() {
                a1<Balance, Balance.Builder, BalanceOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 10 || (a1Var = this.balanceCallBuilder_) == null) {
                    if (i == 10) {
                        return (Balance) this.messageOneof_;
                    }
                    return Balance.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public ByteString getBlockHash() {
                return this.blockHash_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Polkadot.internal_static_TW_Polkadot_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public Era getEra() {
                a1<Era, Era.Builder, EraOrBuilder> a1Var = this.eraBuilder_;
                if (a1Var == null) {
                    Era era = this.era_;
                    return era == null ? Era.getDefaultInstance() : era;
                }
                return a1Var.f();
            }

            public Era.Builder getEraBuilder() {
                onChanged();
                return getEraFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public EraOrBuilder getEraOrBuilder() {
                a1<Era, Era.Builder, EraOrBuilder> a1Var = this.eraBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Era era = this.era_;
                return era == null ? Era.getDefaultInstance() : era;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public ByteString getGenesisHash() {
                return this.genesisHash_;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public Network getNetwork() {
                Network valueOf = Network.valueOf(this.network_);
                return valueOf == null ? Network.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public int getNetworkValue() {
                return this.network_;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public int getSpecVersion() {
                return this.specVersion_;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public Staking getStakingCall() {
                a1<Staking, Staking.Builder, StakingOrBuilder> a1Var = this.stakingCallBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 11) {
                        return (Staking) this.messageOneof_;
                    }
                    return Staking.getDefaultInstance();
                } else if (this.messageOneofCase_ == 11) {
                    return a1Var.f();
                } else {
                    return Staking.getDefaultInstance();
                }
            }

            public Staking.Builder getStakingCallBuilder() {
                return getStakingCallFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public StakingOrBuilder getStakingCallOrBuilder() {
                a1<Staking, Staking.Builder, StakingOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 11 || (a1Var = this.stakingCallBuilder_) == null) {
                    if (i == 11) {
                        return (Staking) this.messageOneof_;
                    }
                    return Staking.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public ByteString getTip() {
                return this.tip_;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public int getTransactionVersion() {
                return this.transactionVersion_;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public boolean hasBalanceCall() {
                return this.messageOneofCase_ == 10;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public boolean hasEra() {
                return (this.eraBuilder_ == null && this.era_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
            public boolean hasStakingCall() {
                return this.messageOneofCase_ == 11;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeBalanceCall(Balance balance) {
                a1<Balance, Balance.Builder, BalanceOrBuilder> a1Var = this.balanceCallBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 10 && this.messageOneof_ != Balance.getDefaultInstance()) {
                        this.messageOneof_ = Balance.newBuilder((Balance) this.messageOneof_).mergeFrom(balance).buildPartial();
                    } else {
                        this.messageOneof_ = balance;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 10) {
                        a1Var.h(balance);
                    }
                    this.balanceCallBuilder_.j(balance);
                }
                this.messageOneofCase_ = 10;
                return this;
            }

            public Builder mergeEra(Era era) {
                a1<Era, Era.Builder, EraOrBuilder> a1Var = this.eraBuilder_;
                if (a1Var == null) {
                    Era era2 = this.era_;
                    if (era2 != null) {
                        this.era_ = Era.newBuilder(era2).mergeFrom(era).buildPartial();
                    } else {
                        this.era_ = era;
                    }
                    onChanged();
                } else {
                    a1Var.h(era);
                }
                return this;
            }

            public Builder mergeStakingCall(Staking staking) {
                a1<Staking, Staking.Builder, StakingOrBuilder> a1Var = this.stakingCallBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 11 && this.messageOneof_ != Staking.getDefaultInstance()) {
                        this.messageOneof_ = Staking.newBuilder((Staking) this.messageOneof_).mergeFrom(staking).buildPartial();
                    } else {
                        this.messageOneof_ = staking;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 11) {
                        a1Var.h(staking);
                    }
                    this.stakingCallBuilder_.j(staking);
                }
                this.messageOneofCase_ = 11;
                return this;
            }

            public Builder setBalanceCall(Balance balance) {
                a1<Balance, Balance.Builder, BalanceOrBuilder> a1Var = this.balanceCallBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(balance);
                    this.messageOneof_ = balance;
                    onChanged();
                } else {
                    a1Var.j(balance);
                }
                this.messageOneofCase_ = 10;
                return this;
            }

            public Builder setBlockHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.blockHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setEra(Era era) {
                a1<Era, Era.Builder, EraOrBuilder> a1Var = this.eraBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(era);
                    this.era_ = era;
                    onChanged();
                } else {
                    a1Var.j(era);
                }
                return this;
            }

            public Builder setGenesisHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.genesisHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNetwork(Network network) {
                Objects.requireNonNull(network);
                this.network_ = network.getNumber();
                onChanged();
                return this;
            }

            public Builder setNetworkValue(int i) {
                this.network_ = i;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSpecVersion(int i) {
                this.specVersion_ = i;
                onChanged();
                return this;
            }

            public Builder setStakingCall(Staking staking) {
                a1<Staking, Staking.Builder, StakingOrBuilder> a1Var = this.stakingCallBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(staking);
                    this.messageOneof_ = staking;
                    onChanged();
                } else {
                    a1Var.j(staking);
                }
                this.messageOneofCase_ = 11;
                return this;
            }

            public Builder setTip(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.tip_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTransactionVersion(int i) {
                this.transactionVersion_ = i;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.blockHash_ = byteString;
                this.genesisHash_ = byteString;
                this.tip_ = byteString;
                this.privateKey_ = byteString;
                this.network_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.blockHash_ = this.blockHash_;
                signingInput.genesisHash_ = this.genesisHash_;
                signingInput.nonce_ = this.nonce_;
                signingInput.specVersion_ = this.specVersion_;
                signingInput.transactionVersion_ = this.transactionVersion_;
                signingInput.tip_ = this.tip_;
                a1<Era, Era.Builder, EraOrBuilder> a1Var = this.eraBuilder_;
                if (a1Var == null) {
                    signingInput.era_ = this.era_;
                } else {
                    signingInput.era_ = a1Var.b();
                }
                signingInput.privateKey_ = this.privateKey_;
                signingInput.network_ = this.network_;
                if (this.messageOneofCase_ == 10) {
                    a1<Balance, Balance.Builder, BalanceOrBuilder> a1Var2 = this.balanceCallBuilder_;
                    if (a1Var2 == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var2.b();
                    }
                }
                if (this.messageOneofCase_ == 11) {
                    a1<Staking, Staking.Builder, StakingOrBuilder> a1Var3 = this.stakingCallBuilder_;
                    if (a1Var3 == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var3.b();
                    }
                }
                signingInput.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.blockHash_ = byteString;
                this.genesisHash_ = byteString;
                this.nonce_ = 0L;
                this.specVersion_ = 0;
                this.transactionVersion_ = 0;
                this.tip_ = byteString;
                if (this.eraBuilder_ == null) {
                    this.era_ = null;
                } else {
                    this.era_ = null;
                    this.eraBuilder_ = null;
                }
                this.privateKey_ = byteString;
                this.network_ = 0;
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            public Builder setEra(Era.Builder builder) {
                a1<Era, Era.Builder, EraOrBuilder> a1Var = this.eraBuilder_;
                if (a1Var == null) {
                    this.era_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setBalanceCall(Balance.Builder builder) {
                a1<Balance, Balance.Builder, BalanceOrBuilder> a1Var = this.balanceCallBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 10;
                return this;
            }

            public Builder setStakingCall(Staking.Builder builder) {
                a1<Staking, Staking.Builder, StakingOrBuilder> a1Var = this.stakingCallBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 11;
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                ByteString blockHash = signingInput.getBlockHash();
                ByteString byteString = ByteString.EMPTY;
                if (blockHash != byteString) {
                    setBlockHash(signingInput.getBlockHash());
                }
                if (signingInput.getGenesisHash() != byteString) {
                    setGenesisHash(signingInput.getGenesisHash());
                }
                if (signingInput.getNonce() != 0) {
                    setNonce(signingInput.getNonce());
                }
                if (signingInput.getSpecVersion() != 0) {
                    setSpecVersion(signingInput.getSpecVersion());
                }
                if (signingInput.getTransactionVersion() != 0) {
                    setTransactionVersion(signingInput.getTransactionVersion());
                }
                if (signingInput.getTip() != byteString) {
                    setTip(signingInput.getTip());
                }
                if (signingInput.hasEra()) {
                    mergeEra(signingInput.getEra());
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (signingInput.network_ != 0) {
                    setNetworkValue(signingInput.getNetworkValue());
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Polkadot$SigningInput$MessageOneofCase[signingInput.getMessageOneofCase().ordinal()];
                if (i == 1) {
                    mergeBalanceCall(signingInput.getBalanceCall());
                } else if (i == 2) {
                    mergeStakingCall(signingInput.getStakingCall());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.blockHash_ = byteString;
                this.genesisHash_ = byteString;
                this.tip_ = byteString;
                this.privateKey_ = byteString;
                this.network_ = 0;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Polkadot.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.SigningInput.access$14100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Polkadot$SigningInput r3 = (wallet.core.jni.proto.Polkadot.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Polkadot$SigningInput r4 = (wallet.core.jni.proto.Polkadot.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            BALANCE_CALL(10),
            STAKING_CALL(11),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 10) {
                        if (i != 11) {
                            return null;
                        }
                        return STAKING_CALL;
                    }
                    return BALANCE_CALL;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Polkadot.internal_static_TW_Polkadot_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getBlockHash().equals(signingInput.getBlockHash()) && getGenesisHash().equals(signingInput.getGenesisHash()) && getNonce() == signingInput.getNonce() && getSpecVersion() == signingInput.getSpecVersion() && getTransactionVersion() == signingInput.getTransactionVersion() && getTip().equals(signingInput.getTip()) && hasEra() == signingInput.hasEra()) {
                if ((!hasEra() || getEra().equals(signingInput.getEra())) && getPrivateKey().equals(signingInput.getPrivateKey()) && this.network_ == signingInput.network_ && getMessageOneofCase().equals(signingInput.getMessageOneofCase())) {
                    int i = this.messageOneofCase_;
                    if (i != 10) {
                        if (i == 11 && !getStakingCall().equals(signingInput.getStakingCall())) {
                            return false;
                        }
                    } else if (!getBalanceCall().equals(signingInput.getBalanceCall())) {
                        return false;
                    }
                    return this.unknownFields.equals(signingInput.unknownFields);
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public Balance getBalanceCall() {
            if (this.messageOneofCase_ == 10) {
                return (Balance) this.messageOneof_;
            }
            return Balance.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public BalanceOrBuilder getBalanceCallOrBuilder() {
            if (this.messageOneofCase_ == 10) {
                return (Balance) this.messageOneof_;
            }
            return Balance.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public ByteString getBlockHash() {
            return this.blockHash_;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public Era getEra() {
            Era era = this.era_;
            return era == null ? Era.getDefaultInstance() : era;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public EraOrBuilder getEraOrBuilder() {
            return getEra();
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public ByteString getGenesisHash() {
            return this.genesisHash_;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public Network getNetwork() {
            Network valueOf = Network.valueOf(this.network_);
            return valueOf == null ? Network.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public int getNetworkValue() {
            return this.network_;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.blockHash_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.blockHash_);
            if (!this.genesisHash_.isEmpty()) {
                h += CodedOutputStream.h(2, this.genesisHash_);
            }
            long j = this.nonce_;
            if (j != 0) {
                h += CodedOutputStream.a0(3, j);
            }
            int i2 = this.specVersion_;
            if (i2 != 0) {
                h += CodedOutputStream.Y(4, i2);
            }
            int i3 = this.transactionVersion_;
            if (i3 != 0) {
                h += CodedOutputStream.Y(5, i3);
            }
            if (!this.tip_.isEmpty()) {
                h += CodedOutputStream.h(6, this.tip_);
            }
            if (this.era_ != null) {
                h += CodedOutputStream.G(7, getEra());
            }
            if (!this.privateKey_.isEmpty()) {
                h += CodedOutputStream.h(8, this.privateKey_);
            }
            if (this.network_ != Network.POLKADOT.getNumber()) {
                h += CodedOutputStream.l(9, this.network_);
            }
            if (this.messageOneofCase_ == 10) {
                h += CodedOutputStream.G(10, (Balance) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 11) {
                h += CodedOutputStream.G(11, (Staking) this.messageOneof_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public int getSpecVersion() {
            return this.specVersion_;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public Staking getStakingCall() {
            if (this.messageOneofCase_ == 11) {
                return (Staking) this.messageOneof_;
            }
            return Staking.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public StakingOrBuilder getStakingCallOrBuilder() {
            if (this.messageOneofCase_ == 11) {
                return (Staking) this.messageOneof_;
            }
            return Staking.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public ByteString getTip() {
            return this.tip_;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public int getTransactionVersion() {
            return this.transactionVersion_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public boolean hasBalanceCall() {
            return this.messageOneofCase_ == 10;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public boolean hasEra() {
            return this.era_ != null;
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningInputOrBuilder
        public boolean hasStakingCall() {
            return this.messageOneofCase_ == 11;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getBlockHash().hashCode()) * 37) + 2) * 53) + getGenesisHash().hashCode()) * 37) + 3) * 53) + a0.h(getNonce())) * 37) + 4) * 53) + getSpecVersion()) * 37) + 5) * 53) + getTransactionVersion()) * 37) + 6) * 53) + getTip().hashCode();
            if (hasEra()) {
                hashCode2 = (((hashCode2 * 37) + 7) * 53) + getEra().hashCode();
            }
            int hashCode3 = (((((((hashCode2 * 37) + 8) * 53) + getPrivateKey().hashCode()) * 37) + 9) * 53) + this.network_;
            int i3 = this.messageOneofCase_;
            if (i3 == 10) {
                i = ((hashCode3 * 37) + 10) * 53;
                hashCode = getBalanceCall().hashCode();
            } else {
                if (i3 == 11) {
                    i = ((hashCode3 * 37) + 11) * 53;
                    hashCode = getStakingCall().hashCode();
                }
                int hashCode4 = (hashCode3 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode4;
                return hashCode4;
            }
            hashCode3 = i + hashCode;
            int hashCode42 = (hashCode3 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode42;
            return hashCode42;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Polkadot.internal_static_TW_Polkadot_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.blockHash_.isEmpty()) {
                codedOutputStream.q0(1, this.blockHash_);
            }
            if (!this.genesisHash_.isEmpty()) {
                codedOutputStream.q0(2, this.genesisHash_);
            }
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(3, j);
            }
            int i = this.specVersion_;
            if (i != 0) {
                codedOutputStream.b1(4, i);
            }
            int i2 = this.transactionVersion_;
            if (i2 != 0) {
                codedOutputStream.b1(5, i2);
            }
            if (!this.tip_.isEmpty()) {
                codedOutputStream.q0(6, this.tip_);
            }
            if (this.era_ != null) {
                codedOutputStream.K0(7, getEra());
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(8, this.privateKey_);
            }
            if (this.network_ != Network.POLKADOT.getNumber()) {
                codedOutputStream.u0(9, this.network_);
            }
            if (this.messageOneofCase_ == 10) {
                codedOutputStream.K0(10, (Balance) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 11) {
                codedOutputStream.K0(11, (Staking) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.blockHash_ = byteString;
            this.genesisHash_ = byteString;
            this.tip_ = byteString;
            this.privateKey_ = byteString;
            this.network_ = 0;
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            switch (J) {
                                case 0:
                                    break;
                                case 10:
                                    this.blockHash_ = jVar.q();
                                    continue;
                                case 18:
                                    this.genesisHash_ = jVar.q();
                                    continue;
                                case 24:
                                    this.nonce_ = jVar.L();
                                    continue;
                                case 32:
                                    this.specVersion_ = jVar.K();
                                    continue;
                                case 40:
                                    this.transactionVersion_ = jVar.K();
                                    continue;
                                case 50:
                                    this.tip_ = jVar.q();
                                    continue;
                                case 58:
                                    Era era = this.era_;
                                    Era.Builder builder = era != null ? era.toBuilder() : null;
                                    Era era2 = (Era) jVar.z(Era.parser(), rVar);
                                    this.era_ = era2;
                                    if (builder != null) {
                                        builder.mergeFrom(era2);
                                        this.era_ = builder.buildPartial();
                                    } else {
                                        continue;
                                    }
                                case 66:
                                    this.privateKey_ = jVar.q();
                                    continue;
                                case 72:
                                    this.network_ = jVar.s();
                                    continue;
                                case 82:
                                    Balance.Builder builder2 = this.messageOneofCase_ == 10 ? ((Balance) this.messageOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(Balance.parser(), rVar);
                                    this.messageOneof_ = z2;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((Balance) z2);
                                        this.messageOneof_ = builder2.buildPartial();
                                    }
                                    this.messageOneofCase_ = 10;
                                    continue;
                                case 90:
                                    Staking.Builder builder3 = this.messageOneofCase_ == 11 ? ((Staking) this.messageOneof_).toBuilder() : null;
                                    m0 z3 = jVar.z(Staking.parser(), rVar);
                                    this.messageOneof_ = z3;
                                    if (builder3 != null) {
                                        builder3.mergeFrom((Staking) z3);
                                        this.messageOneof_ = builder3.buildPartial();
                                    }
                                    this.messageOneofCase_ = 11;
                                    continue;
                                default:
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Balance getBalanceCall();

        BalanceOrBuilder getBalanceCallOrBuilder();

        ByteString getBlockHash();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        Era getEra();

        EraOrBuilder getEraOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getGenesisHash();

        /* synthetic */ String getInitializationErrorString();

        SigningInput.MessageOneofCase getMessageOneofCase();

        Network getNetwork();

        int getNetworkValue();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        int getSpecVersion();

        Staking getStakingCall();

        StakingOrBuilder getStakingCallOrBuilder();

        ByteString getTip();

        int getTransactionVersion();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasBalanceCall();

        boolean hasEra();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasStakingCall();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Polkadot.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Polkadot.internal_static_TW_Polkadot_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Polkadot.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Polkadot.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.SigningOutput.access$15100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Polkadot$SigningOutput r3 = (wallet.core.jni.proto.Polkadot.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Polkadot$SigningOutput r4 = (wallet.core.jni.proto.Polkadot.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Polkadot.internal_static_TW_Polkadot_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Polkadot.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Polkadot.internal_static_TW_Polkadot_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.encoded_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Staking extends GeneratedMessageV3 implements StakingOrBuilder {
        public static final int BOND_AND_NOMINATE_FIELD_NUMBER = 2;
        public static final int BOND_EXTRA_FIELD_NUMBER = 3;
        public static final int BOND_FIELD_NUMBER = 1;
        public static final int CHILL_FIELD_NUMBER = 7;
        public static final int NOMINATE_FIELD_NUMBER = 6;
        public static final int UNBOND_FIELD_NUMBER = 4;
        public static final int WITHDRAW_UNBONDED_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;
        private static final Staking DEFAULT_INSTANCE = new Staking();
        private static final t0<Staking> PARSER = new c<Staking>() { // from class: wallet.core.jni.proto.Polkadot.Staking.1
            @Override // com.google.protobuf.t0
            public Staking parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Staking(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Bond extends GeneratedMessageV3 implements BondOrBuilder {
            public static final int CONTROLLER_FIELD_NUMBER = 1;
            private static final Bond DEFAULT_INSTANCE = new Bond();
            private static final t0<Bond> PARSER = new c<Bond>() { // from class: wallet.core.jni.proto.Polkadot.Staking.Bond.1
                @Override // com.google.protobuf.t0
                public Bond parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Bond(jVar, rVar, null);
                }
            };
            public static final int REWARD_DESTINATION_FIELD_NUMBER = 3;
            public static final int VALUE_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private volatile Object controller_;
            private byte memoizedIsInitialized;
            private int rewardDestination_;
            private ByteString value_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements BondOrBuilder {
                private Object controller_;
                private int rewardDestination_;
                private ByteString value_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Bond_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearController() {
                    this.controller_ = Bond.getDefaultInstance().getController();
                    onChanged();
                    return this;
                }

                public Builder clearRewardDestination() {
                    this.rewardDestination_ = 0;
                    onChanged();
                    return this;
                }

                public Builder clearValue() {
                    this.value_ = Bond.getDefaultInstance().getValue();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
                public String getController() {
                    Object obj = this.controller_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.controller_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
                public ByteString getControllerBytes() {
                    Object obj = this.controller_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.controller_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Bond_descriptor;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
                public RewardDestination getRewardDestination() {
                    RewardDestination valueOf = RewardDestination.valueOf(this.rewardDestination_);
                    return valueOf == null ? RewardDestination.UNRECOGNIZED : valueOf;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
                public int getRewardDestinationValue() {
                    return this.rewardDestination_;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
                public ByteString getValue() {
                    return this.value_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Bond_fieldAccessorTable.d(Bond.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setController(String str) {
                    Objects.requireNonNull(str);
                    this.controller_ = str;
                    onChanged();
                    return this;
                }

                public Builder setControllerBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.controller_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setRewardDestination(RewardDestination rewardDestination) {
                    Objects.requireNonNull(rewardDestination);
                    this.rewardDestination_ = rewardDestination.getNumber();
                    onChanged();
                    return this;
                }

                public Builder setRewardDestinationValue(int i) {
                    this.rewardDestination_ = i;
                    onChanged();
                    return this;
                }

                public Builder setValue(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.value_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.controller_ = "";
                    this.value_ = ByteString.EMPTY;
                    this.rewardDestination_ = 0;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Bond build() {
                    Bond buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Bond buildPartial() {
                    Bond bond = new Bond(this, (AnonymousClass1) null);
                    bond.controller_ = this.controller_;
                    bond.value_ = this.value_;
                    bond.rewardDestination_ = this.rewardDestination_;
                    onBuilt();
                    return bond;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Bond getDefaultInstanceForType() {
                    return Bond.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.controller_ = "";
                    this.value_ = ByteString.EMPTY;
                    this.rewardDestination_ = 0;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Bond) {
                        return mergeFrom((Bond) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.controller_ = "";
                    this.value_ = ByteString.EMPTY;
                    this.rewardDestination_ = 0;
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(Bond bond) {
                    if (bond == Bond.getDefaultInstance()) {
                        return this;
                    }
                    if (!bond.getController().isEmpty()) {
                        this.controller_ = bond.controller_;
                        onChanged();
                    }
                    if (bond.getValue() != ByteString.EMPTY) {
                        setValue(bond.getValue());
                    }
                    if (bond.rewardDestination_ != 0) {
                        setRewardDestinationValue(bond.getRewardDestinationValue());
                    }
                    mergeUnknownFields(bond.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Staking.Bond.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.Bond.access$4600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Staking$Bond r3 = (wallet.core.jni.proto.Polkadot.Staking.Bond) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Staking$Bond r4 = (wallet.core.jni.proto.Polkadot.Staking.Bond) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.Bond.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$Bond$Builder");
                }
            }

            public /* synthetic */ Bond(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Bond getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Bond_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Bond parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Bond) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Bond parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Bond> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Bond)) {
                    return super.equals(obj);
                }
                Bond bond = (Bond) obj;
                return getController().equals(bond.getController()) && getValue().equals(bond.getValue()) && this.rewardDestination_ == bond.rewardDestination_ && this.unknownFields.equals(bond.unknownFields);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
            public String getController() {
                Object obj = this.controller_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.controller_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
            public ByteString getControllerBytes() {
                Object obj = this.controller_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.controller_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Bond> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
            public RewardDestination getRewardDestination() {
                RewardDestination valueOf = RewardDestination.valueOf(this.rewardDestination_);
                return valueOf == null ? RewardDestination.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
            public int getRewardDestinationValue() {
                return this.rewardDestination_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getControllerBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.controller_);
                if (!this.value_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(2, this.value_);
                }
                if (this.rewardDestination_ != RewardDestination.STAKED.getNumber()) {
                    computeStringSize += CodedOutputStream.l(3, this.rewardDestination_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getController().hashCode()) * 37) + 2) * 53) + getValue().hashCode()) * 37) + 3) * 53) + this.rewardDestination_) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Bond_fieldAccessorTable.d(Bond.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Bond();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getControllerBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.controller_);
                }
                if (!this.value_.isEmpty()) {
                    codedOutputStream.q0(2, this.value_);
                }
                if (this.rewardDestination_ != RewardDestination.STAKED.getNumber()) {
                    codedOutputStream.u0(3, this.rewardDestination_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Bond(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Bond bond) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(bond);
            }

            public static Bond parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Bond(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Bond parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Bond) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Bond parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Bond getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Bond parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Bond() {
                this.memoizedIsInitialized = (byte) -1;
                this.controller_ = "";
                this.value_ = ByteString.EMPTY;
                this.rewardDestination_ = 0;
            }

            public static Bond parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Bond parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Bond parseFrom(InputStream inputStream) throws IOException {
                return (Bond) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Bond parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Bond) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private Bond(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.controller_ = jVar.I();
                                } else if (J == 18) {
                                    this.value_ = jVar.q();
                                } else if (J != 24) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.rewardDestination_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Bond parseFrom(j jVar) throws IOException {
                return (Bond) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Bond parseFrom(j jVar, r rVar) throws IOException {
                return (Bond) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public static final class BondAndNominate extends GeneratedMessageV3 implements BondAndNominateOrBuilder {
            public static final int CONTROLLER_FIELD_NUMBER = 1;
            public static final int NOMINATORS_FIELD_NUMBER = 4;
            public static final int REWARD_DESTINATION_FIELD_NUMBER = 3;
            public static final int VALUE_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private volatile Object controller_;
            private byte memoizedIsInitialized;
            private cz1 nominators_;
            private int rewardDestination_;
            private ByteString value_;
            private static final BondAndNominate DEFAULT_INSTANCE = new BondAndNominate();
            private static final t0<BondAndNominate> PARSER = new c<BondAndNominate>() { // from class: wallet.core.jni.proto.Polkadot.Staking.BondAndNominate.1
                @Override // com.google.protobuf.t0
                public BondAndNominate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new BondAndNominate(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements BondAndNominateOrBuilder {
                private int bitField0_;
                private Object controller_;
                private cz1 nominators_;
                private int rewardDestination_;
                private ByteString value_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private void ensureNominatorsIsMutable() {
                    if ((this.bitField0_ & 1) == 0) {
                        this.nominators_ = new d0(this.nominators_);
                        this.bitField0_ |= 1;
                    }
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder addAllNominators(Iterable<String> iterable) {
                    ensureNominatorsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.nominators_);
                    onChanged();
                    return this;
                }

                public Builder addNominators(String str) {
                    Objects.requireNonNull(str);
                    ensureNominatorsIsMutable();
                    this.nominators_.add(str);
                    onChanged();
                    return this;
                }

                public Builder addNominatorsBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    ensureNominatorsIsMutable();
                    this.nominators_.W(byteString);
                    onChanged();
                    return this;
                }

                public Builder clearController() {
                    this.controller_ = BondAndNominate.getDefaultInstance().getController();
                    onChanged();
                    return this;
                }

                public Builder clearNominators() {
                    this.nominators_ = d0.h0;
                    this.bitField0_ &= -2;
                    onChanged();
                    return this;
                }

                public Builder clearRewardDestination() {
                    this.rewardDestination_ = 0;
                    onChanged();
                    return this;
                }

                public Builder clearValue() {
                    this.value_ = BondAndNominate.getDefaultInstance().getValue();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public String getController() {
                    Object obj = this.controller_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.controller_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public ByteString getControllerBytes() {
                    Object obj = this.controller_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.controller_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_descriptor;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public String getNominators(int i) {
                    return this.nominators_.get(i);
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public ByteString getNominatorsBytes(int i) {
                    return this.nominators_.e1(i);
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public int getNominatorsCount() {
                    return this.nominators_.size();
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public RewardDestination getRewardDestination() {
                    RewardDestination valueOf = RewardDestination.valueOf(this.rewardDestination_);
                    return valueOf == null ? RewardDestination.UNRECOGNIZED : valueOf;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public int getRewardDestinationValue() {
                    return this.rewardDestination_;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public ByteString getValue() {
                    return this.value_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_fieldAccessorTable.d(BondAndNominate.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setController(String str) {
                    Objects.requireNonNull(str);
                    this.controller_ = str;
                    onChanged();
                    return this;
                }

                public Builder setControllerBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.controller_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setNominators(int i, String str) {
                    Objects.requireNonNull(str);
                    ensureNominatorsIsMutable();
                    this.nominators_.set(i, str);
                    onChanged();
                    return this;
                }

                public Builder setRewardDestination(RewardDestination rewardDestination) {
                    Objects.requireNonNull(rewardDestination);
                    this.rewardDestination_ = rewardDestination.getNumber();
                    onChanged();
                    return this;
                }

                public Builder setRewardDestinationValue(int i) {
                    this.rewardDestination_ = i;
                    onChanged();
                    return this;
                }

                public Builder setValue(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.value_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
                public dw2 getNominatorsList() {
                    return this.nominators_.x();
                }

                private Builder() {
                    this.controller_ = "";
                    this.value_ = ByteString.EMPTY;
                    this.rewardDestination_ = 0;
                    this.nominators_ = d0.h0;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public BondAndNominate build() {
                    BondAndNominate buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public BondAndNominate buildPartial() {
                    BondAndNominate bondAndNominate = new BondAndNominate(this, (AnonymousClass1) null);
                    bondAndNominate.controller_ = this.controller_;
                    bondAndNominate.value_ = this.value_;
                    bondAndNominate.rewardDestination_ = this.rewardDestination_;
                    if ((this.bitField0_ & 1) != 0) {
                        this.nominators_ = this.nominators_.x();
                        this.bitField0_ &= -2;
                    }
                    bondAndNominate.nominators_ = this.nominators_;
                    onBuilt();
                    return bondAndNominate;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public BondAndNominate getDefaultInstanceForType() {
                    return BondAndNominate.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.controller_ = "";
                    this.value_ = ByteString.EMPTY;
                    this.rewardDestination_ = 0;
                    this.nominators_ = d0.h0;
                    this.bitField0_ &= -2;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof BondAndNominate) {
                        return mergeFrom((BondAndNominate) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.controller_ = "";
                    this.value_ = ByteString.EMPTY;
                    this.rewardDestination_ = 0;
                    this.nominators_ = d0.h0;
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(BondAndNominate bondAndNominate) {
                    if (bondAndNominate == BondAndNominate.getDefaultInstance()) {
                        return this;
                    }
                    if (!bondAndNominate.getController().isEmpty()) {
                        this.controller_ = bondAndNominate.controller_;
                        onChanged();
                    }
                    if (bondAndNominate.getValue() != ByteString.EMPTY) {
                        setValue(bondAndNominate.getValue());
                    }
                    if (bondAndNominate.rewardDestination_ != 0) {
                        setRewardDestinationValue(bondAndNominate.getRewardDestinationValue());
                    }
                    if (!bondAndNominate.nominators_.isEmpty()) {
                        if (this.nominators_.isEmpty()) {
                            this.nominators_ = bondAndNominate.nominators_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureNominatorsIsMutable();
                            this.nominators_.addAll(bondAndNominate.nominators_);
                        }
                        onChanged();
                    }
                    mergeUnknownFields(bondAndNominate.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Staking.BondAndNominate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.BondAndNominate.access$6000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Staking$BondAndNominate r3 = (wallet.core.jni.proto.Polkadot.Staking.BondAndNominate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Staking$BondAndNominate r4 = (wallet.core.jni.proto.Polkadot.Staking.BondAndNominate) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.BondAndNominate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$BondAndNominate$Builder");
                }
            }

            public /* synthetic */ BondAndNominate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static BondAndNominate getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static BondAndNominate parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (BondAndNominate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static BondAndNominate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<BondAndNominate> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof BondAndNominate)) {
                    return super.equals(obj);
                }
                BondAndNominate bondAndNominate = (BondAndNominate) obj;
                return getController().equals(bondAndNominate.getController()) && getValue().equals(bondAndNominate.getValue()) && this.rewardDestination_ == bondAndNominate.rewardDestination_ && getNominatorsList().equals(bondAndNominate.getNominatorsList()) && this.unknownFields.equals(bondAndNominate.unknownFields);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public String getController() {
                Object obj = this.controller_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.controller_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public ByteString getControllerBytes() {
                Object obj = this.controller_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.controller_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public String getNominators(int i) {
                return this.nominators_.get(i);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public ByteString getNominatorsBytes(int i) {
                return this.nominators_.e1(i);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public int getNominatorsCount() {
                return this.nominators_.size();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<BondAndNominate> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public RewardDestination getRewardDestination() {
                RewardDestination valueOf = RewardDestination.valueOf(this.rewardDestination_);
                return valueOf == null ? RewardDestination.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public int getRewardDestinationValue() {
                return this.rewardDestination_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = !getControllerBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.controller_) + 0 : 0;
                if (!this.value_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(2, this.value_);
                }
                if (this.rewardDestination_ != RewardDestination.STAKED.getNumber()) {
                    computeStringSize += CodedOutputStream.l(3, this.rewardDestination_);
                }
                int i2 = 0;
                for (int i3 = 0; i3 < this.nominators_.size(); i3++) {
                    i2 += GeneratedMessageV3.computeStringSizeNoTag(this.nominators_.j(i3));
                }
                int size = computeStringSize + i2 + (getNominatorsList().size() * 1) + this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getController().hashCode()) * 37) + 2) * 53) + getValue().hashCode()) * 37) + 3) * 53) + this.rewardDestination_;
                if (getNominatorsCount() > 0) {
                    hashCode = (((hashCode * 37) + 4) * 53) + getNominatorsList().hashCode();
                }
                int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_fieldAccessorTable.d(BondAndNominate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new BondAndNominate();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getControllerBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.controller_);
                }
                if (!this.value_.isEmpty()) {
                    codedOutputStream.q0(2, this.value_);
                }
                if (this.rewardDestination_ != RewardDestination.STAKED.getNumber()) {
                    codedOutputStream.u0(3, this.rewardDestination_);
                }
                for (int i = 0; i < this.nominators_.size(); i++) {
                    GeneratedMessageV3.writeString(codedOutputStream, 4, this.nominators_.j(i));
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ BondAndNominate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(BondAndNominate bondAndNominate) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(bondAndNominate);
            }

            public static BondAndNominate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondAndNominateOrBuilder
            public dw2 getNominatorsList() {
                return this.nominators_;
            }

            private BondAndNominate(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static BondAndNominate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (BondAndNominate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static BondAndNominate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public BondAndNominate getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static BondAndNominate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private BondAndNominate() {
                this.memoizedIsInitialized = (byte) -1;
                this.controller_ = "";
                this.value_ = ByteString.EMPTY;
                this.rewardDestination_ = 0;
                this.nominators_ = d0.h0;
            }

            public static BondAndNominate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static BondAndNominate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static BondAndNominate parseFrom(InputStream inputStream) throws IOException {
                return (BondAndNominate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static BondAndNominate parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (BondAndNominate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private BondAndNominate(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                boolean z2 = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.controller_ = jVar.I();
                                } else if (J == 18) {
                                    this.value_ = jVar.q();
                                } else if (J == 24) {
                                    this.rewardDestination_ = jVar.s();
                                } else if (J != 34) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    String I = jVar.I();
                                    if (!(z2 & true)) {
                                        this.nominators_ = new d0();
                                        z2 |= true;
                                    }
                                    this.nominators_.add(I);
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        if (z2 & true) {
                            this.nominators_ = this.nominators_.x();
                        }
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static BondAndNominate parseFrom(j jVar) throws IOException {
                return (BondAndNominate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static BondAndNominate parseFrom(j jVar, r rVar) throws IOException {
                return (BondAndNominate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface BondAndNominateOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            String getController();

            ByteString getControllerBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            String getNominators(int i);

            ByteString getNominatorsBytes(int i);

            int getNominatorsCount();

            List<String> getNominatorsList();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            RewardDestination getRewardDestination();

            int getRewardDestinationValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            ByteString getValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class BondExtra extends GeneratedMessageV3 implements BondExtraOrBuilder {
            private static final BondExtra DEFAULT_INSTANCE = new BondExtra();
            private static final t0<BondExtra> PARSER = new c<BondExtra>() { // from class: wallet.core.jni.proto.Polkadot.Staking.BondExtra.1
                @Override // com.google.protobuf.t0
                public BondExtra parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new BondExtra(jVar, rVar, null);
                }
            };
            public static final int VALUE_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private ByteString value_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements BondExtraOrBuilder {
                private ByteString value_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondExtra_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearValue() {
                    this.value_ = BondExtra.getDefaultInstance().getValue();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondExtra_descriptor;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.BondExtraOrBuilder
                public ByteString getValue() {
                    return this.value_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondExtra_fieldAccessorTable.d(BondExtra.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setValue(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.value_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.value_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public BondExtra build() {
                    BondExtra buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public BondExtra buildPartial() {
                    BondExtra bondExtra = new BondExtra(this, (AnonymousClass1) null);
                    bondExtra.value_ = this.value_;
                    onBuilt();
                    return bondExtra;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public BondExtra getDefaultInstanceForType() {
                    return BondExtra.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.value_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.value_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof BondExtra) {
                        return mergeFrom((BondExtra) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(BondExtra bondExtra) {
                    if (bondExtra == BondExtra.getDefaultInstance()) {
                        return this;
                    }
                    if (bondExtra.getValue() != ByteString.EMPTY) {
                        setValue(bondExtra.getValue());
                    }
                    mergeUnknownFields(bondExtra.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Staking.BondExtra.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.BondExtra.access$7200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Staking$BondExtra r3 = (wallet.core.jni.proto.Polkadot.Staking.BondExtra) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Staking$BondExtra r4 = (wallet.core.jni.proto.Polkadot.Staking.BondExtra) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.BondExtra.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$BondExtra$Builder");
                }
            }

            public /* synthetic */ BondExtra(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static BondExtra getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondExtra_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static BondExtra parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (BondExtra) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static BondExtra parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<BondExtra> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof BondExtra)) {
                    return super.equals(obj);
                }
                BondExtra bondExtra = (BondExtra) obj;
                return getValue().equals(bondExtra.getValue()) && this.unknownFields.equals(bondExtra.unknownFields);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<BondExtra> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int h = (this.value_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.value_)) + this.unknownFields.getSerializedSize();
                this.memoizedSize = h;
                return h;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.BondExtraOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValue().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_BondExtra_fieldAccessorTable.d(BondExtra.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new BondExtra();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!this.value_.isEmpty()) {
                    codedOutputStream.q0(1, this.value_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ BondExtra(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(BondExtra bondExtra) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(bondExtra);
            }

            public static BondExtra parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private BondExtra(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static BondExtra parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (BondExtra) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static BondExtra parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public BondExtra getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static BondExtra parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private BondExtra() {
                this.memoizedIsInitialized = (byte) -1;
                this.value_ = ByteString.EMPTY;
            }

            public static BondExtra parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static BondExtra parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static BondExtra parseFrom(InputStream inputStream) throws IOException {
                return (BondExtra) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private BondExtra(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J != 10) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.value_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static BondExtra parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (BondExtra) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static BondExtra parseFrom(j jVar) throws IOException {
                return (BondExtra) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static BondExtra parseFrom(j jVar, r rVar) throws IOException {
                return (BondExtra) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface BondExtraOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            ByteString getValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public interface BondOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            String getController();

            ByteString getControllerBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            RewardDestination getRewardDestination();

            int getRewardDestinationValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            ByteString getValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements StakingOrBuilder {
            private a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> bondAndNominateBuilder_;
            private a1<Bond, Bond.Builder, BondOrBuilder> bondBuilder_;
            private a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> bondExtraBuilder_;
            private a1<Chill, Chill.Builder, ChillOrBuilder> chillBuilder_;
            private int messageOneofCase_;
            private Object messageOneof_;
            private a1<Nominate, Nominate.Builder, NominateOrBuilder> nominateBuilder_;
            private a1<Unbond, Unbond.Builder, UnbondOrBuilder> unbondBuilder_;
            private a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> withdrawUnbondedBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> getBondAndNominateFieldBuilder() {
                if (this.bondAndNominateBuilder_ == null) {
                    if (this.messageOneofCase_ != 2) {
                        this.messageOneof_ = BondAndNominate.getDefaultInstance();
                    }
                    this.bondAndNominateBuilder_ = new a1<>((BondAndNominate) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 2;
                onChanged();
                return this.bondAndNominateBuilder_;
            }

            private a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> getBondExtraFieldBuilder() {
                if (this.bondExtraBuilder_ == null) {
                    if (this.messageOneofCase_ != 3) {
                        this.messageOneof_ = BondExtra.getDefaultInstance();
                    }
                    this.bondExtraBuilder_ = new a1<>((BondExtra) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 3;
                onChanged();
                return this.bondExtraBuilder_;
            }

            private a1<Bond, Bond.Builder, BondOrBuilder> getBondFieldBuilder() {
                if (this.bondBuilder_ == null) {
                    if (this.messageOneofCase_ != 1) {
                        this.messageOneof_ = Bond.getDefaultInstance();
                    }
                    this.bondBuilder_ = new a1<>((Bond) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 1;
                onChanged();
                return this.bondBuilder_;
            }

            private a1<Chill, Chill.Builder, ChillOrBuilder> getChillFieldBuilder() {
                if (this.chillBuilder_ == null) {
                    if (this.messageOneofCase_ != 7) {
                        this.messageOneof_ = Chill.getDefaultInstance();
                    }
                    this.chillBuilder_ = new a1<>((Chill) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 7;
                onChanged();
                return this.chillBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_descriptor;
            }

            private a1<Nominate, Nominate.Builder, NominateOrBuilder> getNominateFieldBuilder() {
                if (this.nominateBuilder_ == null) {
                    if (this.messageOneofCase_ != 6) {
                        this.messageOneof_ = Nominate.getDefaultInstance();
                    }
                    this.nominateBuilder_ = new a1<>((Nominate) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 6;
                onChanged();
                return this.nominateBuilder_;
            }

            private a1<Unbond, Unbond.Builder, UnbondOrBuilder> getUnbondFieldBuilder() {
                if (this.unbondBuilder_ == null) {
                    if (this.messageOneofCase_ != 4) {
                        this.messageOneof_ = Unbond.getDefaultInstance();
                    }
                    this.unbondBuilder_ = new a1<>((Unbond) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 4;
                onChanged();
                return this.unbondBuilder_;
            }

            private a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> getWithdrawUnbondedFieldBuilder() {
                if (this.withdrawUnbondedBuilder_ == null) {
                    if (this.messageOneofCase_ != 5) {
                        this.messageOneof_ = WithdrawUnbonded.getDefaultInstance();
                    }
                    this.withdrawUnbondedBuilder_ = new a1<>((WithdrawUnbonded) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 5;
                onChanged();
                return this.withdrawUnbondedBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBond() {
                a1<Bond, Bond.Builder, BondOrBuilder> a1Var = this.bondBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearBondAndNominate() {
                a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> a1Var = this.bondAndNominateBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearBondExtra() {
                a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> a1Var = this.bondExtraBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearChill() {
                a1<Chill, Chill.Builder, ChillOrBuilder> a1Var = this.chillBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 7) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 7) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearNominate() {
                a1<Nominate, Nominate.Builder, NominateOrBuilder> a1Var = this.nominateBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 6) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 6) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearUnbond() {
                a1<Unbond, Unbond.Builder, UnbondOrBuilder> a1Var = this.unbondBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearWithdrawUnbonded() {
                a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> a1Var = this.withdrawUnbondedBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public Bond getBond() {
                a1<Bond, Bond.Builder, BondOrBuilder> a1Var = this.bondBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        return (Bond) this.messageOneof_;
                    }
                    return Bond.getDefaultInstance();
                } else if (this.messageOneofCase_ == 1) {
                    return a1Var.f();
                } else {
                    return Bond.getDefaultInstance();
                }
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public BondAndNominate getBondAndNominate() {
                a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> a1Var = this.bondAndNominateBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        return (BondAndNominate) this.messageOneof_;
                    }
                    return BondAndNominate.getDefaultInstance();
                } else if (this.messageOneofCase_ == 2) {
                    return a1Var.f();
                } else {
                    return BondAndNominate.getDefaultInstance();
                }
            }

            public BondAndNominate.Builder getBondAndNominateBuilder() {
                return getBondAndNominateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public BondAndNominateOrBuilder getBondAndNominateOrBuilder() {
                a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 2 || (a1Var = this.bondAndNominateBuilder_) == null) {
                    if (i == 2) {
                        return (BondAndNominate) this.messageOneof_;
                    }
                    return BondAndNominate.getDefaultInstance();
                }
                return a1Var.g();
            }

            public Bond.Builder getBondBuilder() {
                return getBondFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public BondExtra getBondExtra() {
                a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> a1Var = this.bondExtraBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        return (BondExtra) this.messageOneof_;
                    }
                    return BondExtra.getDefaultInstance();
                } else if (this.messageOneofCase_ == 3) {
                    return a1Var.f();
                } else {
                    return BondExtra.getDefaultInstance();
                }
            }

            public BondExtra.Builder getBondExtraBuilder() {
                return getBondExtraFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public BondExtraOrBuilder getBondExtraOrBuilder() {
                a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 3 || (a1Var = this.bondExtraBuilder_) == null) {
                    if (i == 3) {
                        return (BondExtra) this.messageOneof_;
                    }
                    return BondExtra.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public BondOrBuilder getBondOrBuilder() {
                a1<Bond, Bond.Builder, BondOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 1 || (a1Var = this.bondBuilder_) == null) {
                    if (i == 1) {
                        return (Bond) this.messageOneof_;
                    }
                    return Bond.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public Chill getChill() {
                a1<Chill, Chill.Builder, ChillOrBuilder> a1Var = this.chillBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 7) {
                        return (Chill) this.messageOneof_;
                    }
                    return Chill.getDefaultInstance();
                } else if (this.messageOneofCase_ == 7) {
                    return a1Var.f();
                } else {
                    return Chill.getDefaultInstance();
                }
            }

            public Chill.Builder getChillBuilder() {
                return getChillFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public ChillOrBuilder getChillOrBuilder() {
                a1<Chill, Chill.Builder, ChillOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 7 || (a1Var = this.chillBuilder_) == null) {
                    if (i == 7) {
                        return (Chill) this.messageOneof_;
                    }
                    return Chill.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_descriptor;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public Nominate getNominate() {
                a1<Nominate, Nominate.Builder, NominateOrBuilder> a1Var = this.nominateBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 6) {
                        return (Nominate) this.messageOneof_;
                    }
                    return Nominate.getDefaultInstance();
                } else if (this.messageOneofCase_ == 6) {
                    return a1Var.f();
                } else {
                    return Nominate.getDefaultInstance();
                }
            }

            public Nominate.Builder getNominateBuilder() {
                return getNominateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public NominateOrBuilder getNominateOrBuilder() {
                a1<Nominate, Nominate.Builder, NominateOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 6 || (a1Var = this.nominateBuilder_) == null) {
                    if (i == 6) {
                        return (Nominate) this.messageOneof_;
                    }
                    return Nominate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public Unbond getUnbond() {
                a1<Unbond, Unbond.Builder, UnbondOrBuilder> a1Var = this.unbondBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        return (Unbond) this.messageOneof_;
                    }
                    return Unbond.getDefaultInstance();
                } else if (this.messageOneofCase_ == 4) {
                    return a1Var.f();
                } else {
                    return Unbond.getDefaultInstance();
                }
            }

            public Unbond.Builder getUnbondBuilder() {
                return getUnbondFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public UnbondOrBuilder getUnbondOrBuilder() {
                a1<Unbond, Unbond.Builder, UnbondOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 4 || (a1Var = this.unbondBuilder_) == null) {
                    if (i == 4) {
                        return (Unbond) this.messageOneof_;
                    }
                    return Unbond.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public WithdrawUnbonded getWithdrawUnbonded() {
                a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> a1Var = this.withdrawUnbondedBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        return (WithdrawUnbonded) this.messageOneof_;
                    }
                    return WithdrawUnbonded.getDefaultInstance();
                } else if (this.messageOneofCase_ == 5) {
                    return a1Var.f();
                } else {
                    return WithdrawUnbonded.getDefaultInstance();
                }
            }

            public WithdrawUnbonded.Builder getWithdrawUnbondedBuilder() {
                return getWithdrawUnbondedFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public WithdrawUnbondedOrBuilder getWithdrawUnbondedOrBuilder() {
                a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 5 || (a1Var = this.withdrawUnbondedBuilder_) == null) {
                    if (i == 5) {
                        return (WithdrawUnbonded) this.messageOneof_;
                    }
                    return WithdrawUnbonded.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public boolean hasBond() {
                return this.messageOneofCase_ == 1;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public boolean hasBondAndNominate() {
                return this.messageOneofCase_ == 2;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public boolean hasBondExtra() {
                return this.messageOneofCase_ == 3;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public boolean hasChill() {
                return this.messageOneofCase_ == 7;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public boolean hasNominate() {
                return this.messageOneofCase_ == 6;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public boolean hasUnbond() {
                return this.messageOneofCase_ == 4;
            }

            @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
            public boolean hasWithdrawUnbonded() {
                return this.messageOneofCase_ == 5;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_fieldAccessorTable.d(Staking.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeBond(Bond bond) {
                a1<Bond, Bond.Builder, BondOrBuilder> a1Var = this.bondBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1 && this.messageOneof_ != Bond.getDefaultInstance()) {
                        this.messageOneof_ = Bond.newBuilder((Bond) this.messageOneof_).mergeFrom(bond).buildPartial();
                    } else {
                        this.messageOneof_ = bond;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 1) {
                        a1Var.h(bond);
                    }
                    this.bondBuilder_.j(bond);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder mergeBondAndNominate(BondAndNominate bondAndNominate) {
                a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> a1Var = this.bondAndNominateBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2 && this.messageOneof_ != BondAndNominate.getDefaultInstance()) {
                        this.messageOneof_ = BondAndNominate.newBuilder((BondAndNominate) this.messageOneof_).mergeFrom(bondAndNominate).buildPartial();
                    } else {
                        this.messageOneof_ = bondAndNominate;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 2) {
                        a1Var.h(bondAndNominate);
                    }
                    this.bondAndNominateBuilder_.j(bondAndNominate);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder mergeBondExtra(BondExtra bondExtra) {
                a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> a1Var = this.bondExtraBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3 && this.messageOneof_ != BondExtra.getDefaultInstance()) {
                        this.messageOneof_ = BondExtra.newBuilder((BondExtra) this.messageOneof_).mergeFrom(bondExtra).buildPartial();
                    } else {
                        this.messageOneof_ = bondExtra;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 3) {
                        a1Var.h(bondExtra);
                    }
                    this.bondExtraBuilder_.j(bondExtra);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder mergeChill(Chill chill) {
                a1<Chill, Chill.Builder, ChillOrBuilder> a1Var = this.chillBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 7 && this.messageOneof_ != Chill.getDefaultInstance()) {
                        this.messageOneof_ = Chill.newBuilder((Chill) this.messageOneof_).mergeFrom(chill).buildPartial();
                    } else {
                        this.messageOneof_ = chill;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 7) {
                        a1Var.h(chill);
                    }
                    this.chillBuilder_.j(chill);
                }
                this.messageOneofCase_ = 7;
                return this;
            }

            public Builder mergeNominate(Nominate nominate) {
                a1<Nominate, Nominate.Builder, NominateOrBuilder> a1Var = this.nominateBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 6 && this.messageOneof_ != Nominate.getDefaultInstance()) {
                        this.messageOneof_ = Nominate.newBuilder((Nominate) this.messageOneof_).mergeFrom(nominate).buildPartial();
                    } else {
                        this.messageOneof_ = nominate;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 6) {
                        a1Var.h(nominate);
                    }
                    this.nominateBuilder_.j(nominate);
                }
                this.messageOneofCase_ = 6;
                return this;
            }

            public Builder mergeUnbond(Unbond unbond) {
                a1<Unbond, Unbond.Builder, UnbondOrBuilder> a1Var = this.unbondBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4 && this.messageOneof_ != Unbond.getDefaultInstance()) {
                        this.messageOneof_ = Unbond.newBuilder((Unbond) this.messageOneof_).mergeFrom(unbond).buildPartial();
                    } else {
                        this.messageOneof_ = unbond;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 4) {
                        a1Var.h(unbond);
                    }
                    this.unbondBuilder_.j(unbond);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder mergeWithdrawUnbonded(WithdrawUnbonded withdrawUnbonded) {
                a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> a1Var = this.withdrawUnbondedBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5 && this.messageOneof_ != WithdrawUnbonded.getDefaultInstance()) {
                        this.messageOneof_ = WithdrawUnbonded.newBuilder((WithdrawUnbonded) this.messageOneof_).mergeFrom(withdrawUnbonded).buildPartial();
                    } else {
                        this.messageOneof_ = withdrawUnbonded;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 5) {
                        a1Var.h(withdrawUnbonded);
                    }
                    this.withdrawUnbondedBuilder_.j(withdrawUnbonded);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder setBond(Bond bond) {
                a1<Bond, Bond.Builder, BondOrBuilder> a1Var = this.bondBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(bond);
                    this.messageOneof_ = bond;
                    onChanged();
                } else {
                    a1Var.j(bond);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setBondAndNominate(BondAndNominate bondAndNominate) {
                a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> a1Var = this.bondAndNominateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(bondAndNominate);
                    this.messageOneof_ = bondAndNominate;
                    onChanged();
                } else {
                    a1Var.j(bondAndNominate);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setBondExtra(BondExtra bondExtra) {
                a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> a1Var = this.bondExtraBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(bondExtra);
                    this.messageOneof_ = bondExtra;
                    onChanged();
                } else {
                    a1Var.j(bondExtra);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder setChill(Chill chill) {
                a1<Chill, Chill.Builder, ChillOrBuilder> a1Var = this.chillBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(chill);
                    this.messageOneof_ = chill;
                    onChanged();
                } else {
                    a1Var.j(chill);
                }
                this.messageOneofCase_ = 7;
                return this;
            }

            public Builder setNominate(Nominate nominate) {
                a1<Nominate, Nominate.Builder, NominateOrBuilder> a1Var = this.nominateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(nominate);
                    this.messageOneof_ = nominate;
                    onChanged();
                } else {
                    a1Var.j(nominate);
                }
                this.messageOneofCase_ = 6;
                return this;
            }

            public Builder setUnbond(Unbond unbond) {
                a1<Unbond, Unbond.Builder, UnbondOrBuilder> a1Var = this.unbondBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(unbond);
                    this.messageOneof_ = unbond;
                    onChanged();
                } else {
                    a1Var.j(unbond);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setWithdrawUnbonded(WithdrawUnbonded withdrawUnbonded) {
                a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> a1Var = this.withdrawUnbondedBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(withdrawUnbonded);
                    this.messageOneof_ = withdrawUnbonded;
                    onChanged();
                } else {
                    a1Var.j(withdrawUnbonded);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Staking build() {
                Staking buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Staking buildPartial() {
                Staking staking = new Staking(this, (AnonymousClass1) null);
                if (this.messageOneofCase_ == 1) {
                    a1<Bond, Bond.Builder, BondOrBuilder> a1Var = this.bondBuilder_;
                    if (a1Var == null) {
                        staking.messageOneof_ = this.messageOneof_;
                    } else {
                        staking.messageOneof_ = a1Var.b();
                    }
                }
                if (this.messageOneofCase_ == 2) {
                    a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> a1Var2 = this.bondAndNominateBuilder_;
                    if (a1Var2 == null) {
                        staking.messageOneof_ = this.messageOneof_;
                    } else {
                        staking.messageOneof_ = a1Var2.b();
                    }
                }
                if (this.messageOneofCase_ == 3) {
                    a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> a1Var3 = this.bondExtraBuilder_;
                    if (a1Var3 == null) {
                        staking.messageOneof_ = this.messageOneof_;
                    } else {
                        staking.messageOneof_ = a1Var3.b();
                    }
                }
                if (this.messageOneofCase_ == 4) {
                    a1<Unbond, Unbond.Builder, UnbondOrBuilder> a1Var4 = this.unbondBuilder_;
                    if (a1Var4 == null) {
                        staking.messageOneof_ = this.messageOneof_;
                    } else {
                        staking.messageOneof_ = a1Var4.b();
                    }
                }
                if (this.messageOneofCase_ == 5) {
                    a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> a1Var5 = this.withdrawUnbondedBuilder_;
                    if (a1Var5 == null) {
                        staking.messageOneof_ = this.messageOneof_;
                    } else {
                        staking.messageOneof_ = a1Var5.b();
                    }
                }
                if (this.messageOneofCase_ == 6) {
                    a1<Nominate, Nominate.Builder, NominateOrBuilder> a1Var6 = this.nominateBuilder_;
                    if (a1Var6 == null) {
                        staking.messageOneof_ = this.messageOneof_;
                    } else {
                        staking.messageOneof_ = a1Var6.b();
                    }
                }
                if (this.messageOneofCase_ == 7) {
                    a1<Chill, Chill.Builder, ChillOrBuilder> a1Var7 = this.chillBuilder_;
                    if (a1Var7 == null) {
                        staking.messageOneof_ = this.messageOneof_;
                    } else {
                        staking.messageOneof_ = a1Var7.b();
                    }
                }
                staking.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return staking;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Staking getDefaultInstanceForType() {
                return Staking.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Staking) {
                    return mergeFrom((Staking) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setBond(Bond.Builder builder) {
                a1<Bond, Bond.Builder, BondOrBuilder> a1Var = this.bondBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setBondAndNominate(BondAndNominate.Builder builder) {
                a1<BondAndNominate, BondAndNominate.Builder, BondAndNominateOrBuilder> a1Var = this.bondAndNominateBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setBondExtra(BondExtra.Builder builder) {
                a1<BondExtra, BondExtra.Builder, BondExtraOrBuilder> a1Var = this.bondExtraBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder setChill(Chill.Builder builder) {
                a1<Chill, Chill.Builder, ChillOrBuilder> a1Var = this.chillBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 7;
                return this;
            }

            public Builder setNominate(Nominate.Builder builder) {
                a1<Nominate, Nominate.Builder, NominateOrBuilder> a1Var = this.nominateBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 6;
                return this;
            }

            public Builder setUnbond(Unbond.Builder builder) {
                a1<Unbond, Unbond.Builder, UnbondOrBuilder> a1Var = this.unbondBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setWithdrawUnbonded(WithdrawUnbonded.Builder builder) {
                a1<WithdrawUnbonded, WithdrawUnbonded.Builder, WithdrawUnbondedOrBuilder> a1Var = this.withdrawUnbondedBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder mergeFrom(Staking staking) {
                if (staking == Staking.getDefaultInstance()) {
                    return this;
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Polkadot$Staking$MessageOneofCase[staking.getMessageOneofCase().ordinal()]) {
                    case 1:
                        mergeBond(staking.getBond());
                        break;
                    case 2:
                        mergeBondAndNominate(staking.getBondAndNominate());
                        break;
                    case 3:
                        mergeBondExtra(staking.getBondExtra());
                        break;
                    case 4:
                        mergeUnbond(staking.getUnbond());
                        break;
                    case 5:
                        mergeWithdrawUnbonded(staking.getWithdrawUnbonded());
                        break;
                    case 6:
                        mergeNominate(staking.getNominate());
                        break;
                    case 7:
                        mergeChill(staking.getChill());
                        break;
                }
                mergeUnknownFields(staking.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Polkadot.Staking.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.access$12100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Polkadot$Staking r3 = (wallet.core.jni.proto.Polkadot.Staking) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Polkadot$Staking r4 = (wallet.core.jni.proto.Polkadot.Staking) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public static final class Chill extends GeneratedMessageV3 implements ChillOrBuilder {
            private static final Chill DEFAULT_INSTANCE = new Chill();
            private static final t0<Chill> PARSER = new c<Chill>() { // from class: wallet.core.jni.proto.Polkadot.Staking.Chill.1
                @Override // com.google.protobuf.t0
                public Chill parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Chill(jVar, rVar, null);
                }
            };
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ChillOrBuilder {
                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Chill_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Chill_descriptor;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Chill_fieldAccessorTable.d(Chill.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Chill build() {
                    Chill buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Chill buildPartial() {
                    Chill chill = new Chill(this, (AnonymousClass1) null);
                    onBuilt();
                    return chill;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Chill getDefaultInstanceForType() {
                    return Chill.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Chill) {
                        return mergeFrom((Chill) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Chill chill) {
                    if (chill == Chill.getDefaultInstance()) {
                        return this;
                    }
                    mergeUnknownFields(chill.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Staking.Chill.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.Chill.access$11200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Staking$Chill r3 = (wallet.core.jni.proto.Polkadot.Staking.Chill) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Staking$Chill r4 = (wallet.core.jni.proto.Polkadot.Staking.Chill) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.Chill.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$Chill$Builder");
                }
            }

            public /* synthetic */ Chill(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Chill getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Chill_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Chill parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Chill) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Chill parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Chill> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (obj instanceof Chill) {
                    return this.unknownFields.equals(((Chill) obj).unknownFields);
                }
                return super.equals(obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Chill> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int serializedSize = this.unknownFields.getSerializedSize() + 0;
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((779 + getDescriptor().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Chill_fieldAccessorTable.d(Chill.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Chill();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Chill(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Chill chill) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(chill);
            }

            public static Chill parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Chill(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Chill parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Chill) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Chill parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Chill getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Chill parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Chill() {
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Chill parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Chill parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            private Chill(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J == 0 || !parseUnknownField(jVar, g, rVar, J)) {
                                    z = true;
                                }
                            } catch (IOException e) {
                                throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                            }
                        } catch (InvalidProtocolBufferException e2) {
                            throw e2.setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Chill parseFrom(InputStream inputStream) throws IOException {
                return (Chill) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Chill parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Chill) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Chill parseFrom(j jVar) throws IOException {
                return (Chill) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Chill parseFrom(j jVar, r rVar) throws IOException {
                return (Chill) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ChillOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            BOND(1),
            BOND_AND_NOMINATE(2),
            BOND_EXTRA(3),
            UNBOND(4),
            WITHDRAW_UNBONDED(5),
            NOMINATE(6),
            CHILL(7),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                switch (i) {
                    case 0:
                        return MESSAGEONEOF_NOT_SET;
                    case 1:
                        return BOND;
                    case 2:
                        return BOND_AND_NOMINATE;
                    case 3:
                        return BOND_EXTRA;
                    case 4:
                        return UNBOND;
                    case 5:
                        return WITHDRAW_UNBONDED;
                    case 6:
                        return NOMINATE;
                    case 7:
                        return CHILL;
                    default:
                        return null;
                }
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class Nominate extends GeneratedMessageV3 implements NominateOrBuilder {
            public static final int NOMINATORS_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private cz1 nominators_;
            private static final Nominate DEFAULT_INSTANCE = new Nominate();
            private static final t0<Nominate> PARSER = new c<Nominate>() { // from class: wallet.core.jni.proto.Polkadot.Staking.Nominate.1
                @Override // com.google.protobuf.t0
                public Nominate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Nominate(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements NominateOrBuilder {
                private int bitField0_;
                private cz1 nominators_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private void ensureNominatorsIsMutable() {
                    if ((this.bitField0_ & 1) == 0) {
                        this.nominators_ = new d0(this.nominators_);
                        this.bitField0_ |= 1;
                    }
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Nominate_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder addAllNominators(Iterable<String> iterable) {
                    ensureNominatorsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.nominators_);
                    onChanged();
                    return this;
                }

                public Builder addNominators(String str) {
                    Objects.requireNonNull(str);
                    ensureNominatorsIsMutable();
                    this.nominators_.add(str);
                    onChanged();
                    return this;
                }

                public Builder addNominatorsBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    ensureNominatorsIsMutable();
                    this.nominators_.W(byteString);
                    onChanged();
                    return this;
                }

                public Builder clearNominators() {
                    this.nominators_ = d0.h0;
                    this.bitField0_ &= -2;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Nominate_descriptor;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
                public String getNominators(int i) {
                    return this.nominators_.get(i);
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
                public ByteString getNominatorsBytes(int i) {
                    return this.nominators_.e1(i);
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
                public int getNominatorsCount() {
                    return this.nominators_.size();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Nominate_fieldAccessorTable.d(Nominate.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setNominators(int i, String str) {
                    Objects.requireNonNull(str);
                    ensureNominatorsIsMutable();
                    this.nominators_.set(i, str);
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
                public dw2 getNominatorsList() {
                    return this.nominators_.x();
                }

                private Builder() {
                    this.nominators_ = d0.h0;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Nominate build() {
                    Nominate buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Nominate buildPartial() {
                    Nominate nominate = new Nominate(this, (AnonymousClass1) null);
                    if ((this.bitField0_ & 1) != 0) {
                        this.nominators_ = this.nominators_.x();
                        this.bitField0_ &= -2;
                    }
                    nominate.nominators_ = this.nominators_;
                    onBuilt();
                    return nominate;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Nominate getDefaultInstanceForType() {
                    return Nominate.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.nominators_ = d0.h0;
                    this.bitField0_ &= -2;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.nominators_ = d0.h0;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Nominate) {
                        return mergeFrom((Nominate) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Nominate nominate) {
                    if (nominate == Nominate.getDefaultInstance()) {
                        return this;
                    }
                    if (!nominate.nominators_.isEmpty()) {
                        if (this.nominators_.isEmpty()) {
                            this.nominators_ = nominate.nominators_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureNominatorsIsMutable();
                            this.nominators_.addAll(nominate.nominators_);
                        }
                        onChanged();
                    }
                    mergeUnknownFields(nominate.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Staking.Nominate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.Nominate.access$10200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Staking$Nominate r3 = (wallet.core.jni.proto.Polkadot.Staking.Nominate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Staking$Nominate r4 = (wallet.core.jni.proto.Polkadot.Staking.Nominate) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.Nominate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$Nominate$Builder");
                }
            }

            public /* synthetic */ Nominate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Nominate getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Nominate_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Nominate parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Nominate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Nominate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Nominate> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Nominate)) {
                    return super.equals(obj);
                }
                Nominate nominate = (Nominate) obj;
                return getNominatorsList().equals(nominate.getNominatorsList()) && this.unknownFields.equals(nominate.unknownFields);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
            public String getNominators(int i) {
                return this.nominators_.get(i);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
            public ByteString getNominatorsBytes(int i) {
                return this.nominators_.e1(i);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
            public int getNominatorsCount() {
                return this.nominators_.size();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Nominate> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int i2 = 0;
                for (int i3 = 0; i3 < this.nominators_.size(); i3++) {
                    i2 += GeneratedMessageV3.computeStringSizeNoTag(this.nominators_.j(i3));
                }
                int size = 0 + i2 + (getNominatorsList().size() * 1) + this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = 779 + getDescriptor().hashCode();
                if (getNominatorsCount() > 0) {
                    hashCode = (((hashCode * 37) + 1) * 53) + getNominatorsList().hashCode();
                }
                int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Nominate_fieldAccessorTable.d(Nominate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Nominate();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                for (int i = 0; i < this.nominators_.size(); i++) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.nominators_.j(i));
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Nominate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Nominate nominate) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(nominate);
            }

            public static Nominate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.NominateOrBuilder
            public dw2 getNominatorsList() {
                return this.nominators_;
            }

            private Nominate(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Nominate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Nominate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Nominate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Nominate getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Nominate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Nominate() {
                this.memoizedIsInitialized = (byte) -1;
                this.nominators_ = d0.h0;
            }

            public static Nominate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Nominate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Nominate parseFrom(InputStream inputStream) throws IOException {
                return (Nominate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Nominate(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                boolean z2 = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J != 10) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    String I = jVar.I();
                                    if (!(z2 & true)) {
                                        this.nominators_ = new d0();
                                        z2 |= true;
                                    }
                                    this.nominators_.add(I);
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        if (z2 & true) {
                            this.nominators_ = this.nominators_.x();
                        }
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Nominate parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Nominate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Nominate parseFrom(j jVar) throws IOException {
                return (Nominate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Nominate parseFrom(j jVar, r rVar) throws IOException {
                return (Nominate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface NominateOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            String getNominators(int i);

            ByteString getNominatorsBytes(int i);

            int getNominatorsCount();

            List<String> getNominatorsList();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Unbond extends GeneratedMessageV3 implements UnbondOrBuilder {
            private static final Unbond DEFAULT_INSTANCE = new Unbond();
            private static final t0<Unbond> PARSER = new c<Unbond>() { // from class: wallet.core.jni.proto.Polkadot.Staking.Unbond.1
                @Override // com.google.protobuf.t0
                public Unbond parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Unbond(jVar, rVar, null);
                }
            };
            public static final int VALUE_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private ByteString value_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements UnbondOrBuilder {
                private ByteString value_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Unbond_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearValue() {
                    this.value_ = Unbond.getDefaultInstance().getValue();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Unbond_descriptor;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.UnbondOrBuilder
                public ByteString getValue() {
                    return this.value_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Unbond_fieldAccessorTable.d(Unbond.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setValue(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.value_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.value_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Unbond build() {
                    Unbond buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Unbond buildPartial() {
                    Unbond unbond = new Unbond(this, (AnonymousClass1) null);
                    unbond.value_ = this.value_;
                    onBuilt();
                    return unbond;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Unbond getDefaultInstanceForType() {
                    return Unbond.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.value_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.value_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Unbond) {
                        return mergeFrom((Unbond) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Unbond unbond) {
                    if (unbond == Unbond.getDefaultInstance()) {
                        return this;
                    }
                    if (unbond.getValue() != ByteString.EMPTY) {
                        setValue(unbond.getValue());
                    }
                    mergeUnknownFields(unbond.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Staking.Unbond.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.Unbond.access$8200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Staking$Unbond r3 = (wallet.core.jni.proto.Polkadot.Staking.Unbond) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Staking$Unbond r4 = (wallet.core.jni.proto.Polkadot.Staking.Unbond) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.Unbond.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$Unbond$Builder");
                }
            }

            public /* synthetic */ Unbond(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Unbond getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Unbond_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Unbond parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Unbond) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Unbond parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Unbond> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Unbond)) {
                    return super.equals(obj);
                }
                Unbond unbond = (Unbond) obj;
                return getValue().equals(unbond.getValue()) && this.unknownFields.equals(unbond.unknownFields);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Unbond> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int h = (this.value_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.value_)) + this.unknownFields.getSerializedSize();
                this.memoizedSize = h;
                return h;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.UnbondOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getValue().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_Unbond_fieldAccessorTable.d(Unbond.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Unbond();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!this.value_.isEmpty()) {
                    codedOutputStream.q0(1, this.value_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Unbond(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Unbond unbond) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(unbond);
            }

            public static Unbond parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Unbond(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Unbond parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Unbond) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Unbond parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Unbond getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Unbond parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Unbond() {
                this.memoizedIsInitialized = (byte) -1;
                this.value_ = ByteString.EMPTY;
            }

            public static Unbond parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Unbond parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Unbond parseFrom(InputStream inputStream) throws IOException {
                return (Unbond) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Unbond(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J != 10) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.value_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Unbond parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Unbond) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Unbond parseFrom(j jVar) throws IOException {
                return (Unbond) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Unbond parseFrom(j jVar, r rVar) throws IOException {
                return (Unbond) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface UnbondOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            ByteString getValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class WithdrawUnbonded extends GeneratedMessageV3 implements WithdrawUnbondedOrBuilder {
            private static final WithdrawUnbonded DEFAULT_INSTANCE = new WithdrawUnbonded();
            private static final t0<WithdrawUnbonded> PARSER = new c<WithdrawUnbonded>() { // from class: wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbonded.1
                @Override // com.google.protobuf.t0
                public WithdrawUnbonded parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new WithdrawUnbonded(jVar, rVar, null);
                }
            };
            public static final int SLASHING_SPANS_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private int slashingSpans_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements WithdrawUnbondedOrBuilder {
                private int slashingSpans_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearSlashingSpans() {
                    this.slashingSpans_ = 0;
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_descriptor;
                }

                @Override // wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbondedOrBuilder
                public int getSlashingSpans() {
                    return this.slashingSpans_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Polkadot.internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_fieldAccessorTable.d(WithdrawUnbonded.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setSlashingSpans(int i) {
                    this.slashingSpans_ = i;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public WithdrawUnbonded build() {
                    WithdrawUnbonded buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public WithdrawUnbonded buildPartial() {
                    WithdrawUnbonded withdrawUnbonded = new WithdrawUnbonded(this, (AnonymousClass1) null);
                    withdrawUnbonded.slashingSpans_ = this.slashingSpans_;
                    onBuilt();
                    return withdrawUnbonded;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public WithdrawUnbonded getDefaultInstanceForType() {
                    return WithdrawUnbonded.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.slashingSpans_ = 0;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof WithdrawUnbonded) {
                        return mergeFrom((WithdrawUnbonded) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(WithdrawUnbonded withdrawUnbonded) {
                    if (withdrawUnbonded == WithdrawUnbonded.getDefaultInstance()) {
                        return this;
                    }
                    if (withdrawUnbonded.getSlashingSpans() != 0) {
                        setSlashingSpans(withdrawUnbonded.getSlashingSpans());
                    }
                    mergeUnknownFields(withdrawUnbonded.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbonded.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbonded.access$9200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Polkadot$Staking$WithdrawUnbonded r3 = (wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbonded) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Polkadot$Staking$WithdrawUnbonded r4 = (wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbonded) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbonded.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Polkadot$Staking$WithdrawUnbonded$Builder");
                }
            }

            public /* synthetic */ WithdrawUnbonded(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static WithdrawUnbonded getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static WithdrawUnbonded parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (WithdrawUnbonded) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static WithdrawUnbonded parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<WithdrawUnbonded> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof WithdrawUnbonded)) {
                    return super.equals(obj);
                }
                WithdrawUnbonded withdrawUnbonded = (WithdrawUnbonded) obj;
                return getSlashingSpans() == withdrawUnbonded.getSlashingSpans() && this.unknownFields.equals(withdrawUnbonded.unknownFields);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<WithdrawUnbonded> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int i2 = this.slashingSpans_;
                int x = (i2 != 0 ? 0 + CodedOutputStream.x(1, i2) : 0) + this.unknownFields.getSerializedSize();
                this.memoizedSize = x;
                return x;
            }

            @Override // wallet.core.jni.proto.Polkadot.Staking.WithdrawUnbondedOrBuilder
            public int getSlashingSpans() {
                return this.slashingSpans_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSlashingSpans()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Polkadot.internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_fieldAccessorTable.d(WithdrawUnbonded.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new WithdrawUnbonded();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                int i = this.slashingSpans_;
                if (i != 0) {
                    codedOutputStream.G0(1, i);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ WithdrawUnbonded(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(WithdrawUnbonded withdrawUnbonded) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(withdrawUnbonded);
            }

            public static WithdrawUnbonded parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private WithdrawUnbonded(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static WithdrawUnbonded parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (WithdrawUnbonded) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static WithdrawUnbonded parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public WithdrawUnbonded getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static WithdrawUnbonded parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private WithdrawUnbonded() {
                this.memoizedIsInitialized = (byte) -1;
            }

            public static WithdrawUnbonded parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static WithdrawUnbonded parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            private WithdrawUnbonded(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J != 8) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.slashingSpans_ = jVar.x();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static WithdrawUnbonded parseFrom(InputStream inputStream) throws IOException {
                return (WithdrawUnbonded) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static WithdrawUnbonded parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (WithdrawUnbonded) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static WithdrawUnbonded parseFrom(j jVar) throws IOException {
                return (WithdrawUnbonded) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static WithdrawUnbonded parseFrom(j jVar, r rVar) throws IOException {
                return (WithdrawUnbonded) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface WithdrawUnbondedOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            int getSlashingSpans();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ Staking(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Staking getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Polkadot.internal_static_TW_Polkadot_Proto_Staking_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Staking parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Staking) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Staking parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Staking> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Staking)) {
                return super.equals(obj);
            }
            Staking staking = (Staking) obj;
            if (getMessageOneofCase().equals(staking.getMessageOneofCase())) {
                switch (this.messageOneofCase_) {
                    case 1:
                        if (!getBond().equals(staking.getBond())) {
                            return false;
                        }
                        break;
                    case 2:
                        if (!getBondAndNominate().equals(staking.getBondAndNominate())) {
                            return false;
                        }
                        break;
                    case 3:
                        if (!getBondExtra().equals(staking.getBondExtra())) {
                            return false;
                        }
                        break;
                    case 4:
                        if (!getUnbond().equals(staking.getUnbond())) {
                            return false;
                        }
                        break;
                    case 5:
                        if (!getWithdrawUnbonded().equals(staking.getWithdrawUnbonded())) {
                            return false;
                        }
                        break;
                    case 6:
                        if (!getNominate().equals(staking.getNominate())) {
                            return false;
                        }
                        break;
                    case 7:
                        if (!getChill().equals(staking.getChill())) {
                            return false;
                        }
                        break;
                }
                return this.unknownFields.equals(staking.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public Bond getBond() {
            if (this.messageOneofCase_ == 1) {
                return (Bond) this.messageOneof_;
            }
            return Bond.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public BondAndNominate getBondAndNominate() {
            if (this.messageOneofCase_ == 2) {
                return (BondAndNominate) this.messageOneof_;
            }
            return BondAndNominate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public BondAndNominateOrBuilder getBondAndNominateOrBuilder() {
            if (this.messageOneofCase_ == 2) {
                return (BondAndNominate) this.messageOneof_;
            }
            return BondAndNominate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public BondExtra getBondExtra() {
            if (this.messageOneofCase_ == 3) {
                return (BondExtra) this.messageOneof_;
            }
            return BondExtra.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public BondExtraOrBuilder getBondExtraOrBuilder() {
            if (this.messageOneofCase_ == 3) {
                return (BondExtra) this.messageOneof_;
            }
            return BondExtra.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public BondOrBuilder getBondOrBuilder() {
            if (this.messageOneofCase_ == 1) {
                return (Bond) this.messageOneof_;
            }
            return Bond.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public Chill getChill() {
            if (this.messageOneofCase_ == 7) {
                return (Chill) this.messageOneof_;
            }
            return Chill.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public ChillOrBuilder getChillOrBuilder() {
            if (this.messageOneofCase_ == 7) {
                return (Chill) this.messageOneof_;
            }
            return Chill.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public Nominate getNominate() {
            if (this.messageOneofCase_ == 6) {
                return (Nominate) this.messageOneof_;
            }
            return Nominate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public NominateOrBuilder getNominateOrBuilder() {
            if (this.messageOneofCase_ == 6) {
                return (Nominate) this.messageOneof_;
            }
            return Nominate.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Staking> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.messageOneofCase_ == 1 ? 0 + CodedOutputStream.G(1, (Bond) this.messageOneof_) : 0;
            if (this.messageOneofCase_ == 2) {
                G += CodedOutputStream.G(2, (BondAndNominate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 3) {
                G += CodedOutputStream.G(3, (BondExtra) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                G += CodedOutputStream.G(4, (Unbond) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                G += CodedOutputStream.G(5, (WithdrawUnbonded) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 6) {
                G += CodedOutputStream.G(6, (Nominate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 7) {
                G += CodedOutputStream.G(7, (Chill) this.messageOneof_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public Unbond getUnbond() {
            if (this.messageOneofCase_ == 4) {
                return (Unbond) this.messageOneof_;
            }
            return Unbond.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public UnbondOrBuilder getUnbondOrBuilder() {
            if (this.messageOneofCase_ == 4) {
                return (Unbond) this.messageOneof_;
            }
            return Unbond.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public WithdrawUnbonded getWithdrawUnbonded() {
            if (this.messageOneofCase_ == 5) {
                return (WithdrawUnbonded) this.messageOneof_;
            }
            return WithdrawUnbonded.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public WithdrawUnbondedOrBuilder getWithdrawUnbondedOrBuilder() {
            if (this.messageOneofCase_ == 5) {
                return (WithdrawUnbonded) this.messageOneof_;
            }
            return WithdrawUnbonded.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public boolean hasBond() {
            return this.messageOneofCase_ == 1;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public boolean hasBondAndNominate() {
            return this.messageOneofCase_ == 2;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public boolean hasBondExtra() {
            return this.messageOneofCase_ == 3;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public boolean hasChill() {
            return this.messageOneofCase_ == 7;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public boolean hasNominate() {
            return this.messageOneofCase_ == 6;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public boolean hasUnbond() {
            return this.messageOneofCase_ == 4;
        }

        @Override // wallet.core.jni.proto.Polkadot.StakingOrBuilder
        public boolean hasWithdrawUnbonded() {
            return this.messageOneofCase_ == 5;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = 779 + getDescriptor().hashCode();
            switch (this.messageOneofCase_) {
                case 1:
                    i = ((hashCode2 * 37) + 1) * 53;
                    hashCode = getBond().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3;
                    return hashCode3;
                case 2:
                    i = ((hashCode2 * 37) + 2) * 53;
                    hashCode = getBondAndNominate().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32;
                    return hashCode32;
                case 3:
                    i = ((hashCode2 * 37) + 3) * 53;
                    hashCode = getBondExtra().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322;
                    return hashCode322;
                case 4:
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getUnbond().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222;
                    return hashCode3222;
                case 5:
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getWithdrawUnbonded().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222;
                    return hashCode32222;
                case 6:
                    i = ((hashCode2 * 37) + 6) * 53;
                    hashCode = getNominate().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222;
                    return hashCode322222;
                case 7:
                    i = ((hashCode2 * 37) + 7) * 53;
                    hashCode = getChill().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222;
                    return hashCode3222222;
                default:
                    int hashCode32222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222222;
                    return hashCode32222222;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Polkadot.internal_static_TW_Polkadot_Proto_Staking_fieldAccessorTable.d(Staking.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Staking();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.messageOneofCase_ == 1) {
                codedOutputStream.K0(1, (Bond) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 2) {
                codedOutputStream.K0(2, (BondAndNominate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 3) {
                codedOutputStream.K0(3, (BondExtra) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                codedOutputStream.K0(4, (Unbond) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                codedOutputStream.K0(5, (WithdrawUnbonded) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 6) {
                codedOutputStream.K0(6, (Nominate) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 7) {
                codedOutputStream.K0(7, (Chill) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Staking(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Staking staking) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(staking);
        }

        public static Staking parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Staking(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Staking parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Staking parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Staking getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Staking parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Staking parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Staking() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Staking parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Staking parseFrom(InputStream inputStream) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Staking(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    Bond.Builder builder = this.messageOneofCase_ == 1 ? ((Bond) this.messageOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(Bond.parser(), rVar);
                                    this.messageOneof_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((Bond) z2);
                                        this.messageOneof_ = builder.buildPartial();
                                    }
                                    this.messageOneofCase_ = 1;
                                } else if (J == 18) {
                                    BondAndNominate.Builder builder2 = this.messageOneofCase_ == 2 ? ((BondAndNominate) this.messageOneof_).toBuilder() : null;
                                    m0 z3 = jVar.z(BondAndNominate.parser(), rVar);
                                    this.messageOneof_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((BondAndNominate) z3);
                                        this.messageOneof_ = builder2.buildPartial();
                                    }
                                    this.messageOneofCase_ = 2;
                                } else if (J == 26) {
                                    BondExtra.Builder builder3 = this.messageOneofCase_ == 3 ? ((BondExtra) this.messageOneof_).toBuilder() : null;
                                    m0 z4 = jVar.z(BondExtra.parser(), rVar);
                                    this.messageOneof_ = z4;
                                    if (builder3 != null) {
                                        builder3.mergeFrom((BondExtra) z4);
                                        this.messageOneof_ = builder3.buildPartial();
                                    }
                                    this.messageOneofCase_ = 3;
                                } else if (J == 34) {
                                    Unbond.Builder builder4 = this.messageOneofCase_ == 4 ? ((Unbond) this.messageOneof_).toBuilder() : null;
                                    m0 z5 = jVar.z(Unbond.parser(), rVar);
                                    this.messageOneof_ = z5;
                                    if (builder4 != null) {
                                        builder4.mergeFrom((Unbond) z5);
                                        this.messageOneof_ = builder4.buildPartial();
                                    }
                                    this.messageOneofCase_ = 4;
                                } else if (J == 42) {
                                    WithdrawUnbonded.Builder builder5 = this.messageOneofCase_ == 5 ? ((WithdrawUnbonded) this.messageOneof_).toBuilder() : null;
                                    m0 z6 = jVar.z(WithdrawUnbonded.parser(), rVar);
                                    this.messageOneof_ = z6;
                                    if (builder5 != null) {
                                        builder5.mergeFrom((WithdrawUnbonded) z6);
                                        this.messageOneof_ = builder5.buildPartial();
                                    }
                                    this.messageOneofCase_ = 5;
                                } else if (J == 50) {
                                    Nominate.Builder builder6 = this.messageOneofCase_ == 6 ? ((Nominate) this.messageOneof_).toBuilder() : null;
                                    m0 z7 = jVar.z(Nominate.parser(), rVar);
                                    this.messageOneof_ = z7;
                                    if (builder6 != null) {
                                        builder6.mergeFrom((Nominate) z7);
                                        this.messageOneof_ = builder6.buildPartial();
                                    }
                                    this.messageOneofCase_ = 6;
                                } else if (J != 58) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    Chill.Builder builder7 = this.messageOneofCase_ == 7 ? ((Chill) this.messageOneof_).toBuilder() : null;
                                    m0 z8 = jVar.z(Chill.parser(), rVar);
                                    this.messageOneof_ = z8;
                                    if (builder7 != null) {
                                        builder7.mergeFrom((Chill) z8);
                                        this.messageOneof_ = builder7.buildPartial();
                                    }
                                    this.messageOneofCase_ = 7;
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Staking parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Staking parseFrom(j jVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Staking parseFrom(j jVar, r rVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface StakingOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Staking.Bond getBond();

        Staking.BondAndNominate getBondAndNominate();

        Staking.BondAndNominateOrBuilder getBondAndNominateOrBuilder();

        Staking.BondExtra getBondExtra();

        Staking.BondExtraOrBuilder getBondExtraOrBuilder();

        Staking.BondOrBuilder getBondOrBuilder();

        Staking.Chill getChill();

        Staking.ChillOrBuilder getChillOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        Staking.MessageOneofCase getMessageOneofCase();

        Staking.Nominate getNominate();

        Staking.NominateOrBuilder getNominateOrBuilder();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Staking.Unbond getUnbond();

        Staking.UnbondOrBuilder getUnbondOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        Staking.WithdrawUnbonded getWithdrawUnbonded();

        Staking.WithdrawUnbondedOrBuilder getWithdrawUnbondedOrBuilder();

        boolean hasBond();

        boolean hasBondAndNominate();

        boolean hasBondExtra();

        boolean hasChill();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasNominate();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasUnbond();

        boolean hasWithdrawUnbonded();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Polkadot_Proto_Era_descriptor = bVar;
        internal_static_TW_Polkadot_Proto_Era_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"BlockNumber", "Period"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Polkadot_Proto_Balance_descriptor = bVar2;
        internal_static_TW_Polkadot_Proto_Balance_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Transfer", "MessageOneof"});
        Descriptors.b bVar3 = bVar2.r().get(0);
        internal_static_TW_Polkadot_Proto_Balance_Transfer_descriptor = bVar3;
        internal_static_TW_Polkadot_Proto_Balance_Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"ToAddress", "Value"});
        Descriptors.b bVar4 = getDescriptor().o().get(2);
        internal_static_TW_Polkadot_Proto_Staking_descriptor = bVar4;
        internal_static_TW_Polkadot_Proto_Staking_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Bond", "BondAndNominate", "BondExtra", "Unbond", "WithdrawUnbonded", "Nominate", "Chill", "MessageOneof"});
        Descriptors.b bVar5 = bVar4.r().get(0);
        internal_static_TW_Polkadot_Proto_Staking_Bond_descriptor = bVar5;
        internal_static_TW_Polkadot_Proto_Staking_Bond_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"Controller", "Value", "RewardDestination"});
        Descriptors.b bVar6 = bVar4.r().get(1);
        internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_descriptor = bVar6;
        internal_static_TW_Polkadot_Proto_Staking_BondAndNominate_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"Controller", "Value", "RewardDestination", "Nominators"});
        Descriptors.b bVar7 = bVar4.r().get(2);
        internal_static_TW_Polkadot_Proto_Staking_BondExtra_descriptor = bVar7;
        internal_static_TW_Polkadot_Proto_Staking_BondExtra_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"Value"});
        Descriptors.b bVar8 = bVar4.r().get(3);
        internal_static_TW_Polkadot_Proto_Staking_Unbond_descriptor = bVar8;
        internal_static_TW_Polkadot_Proto_Staking_Unbond_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"Value"});
        Descriptors.b bVar9 = bVar4.r().get(4);
        internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_descriptor = bVar9;
        internal_static_TW_Polkadot_Proto_Staking_WithdrawUnbonded_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"SlashingSpans"});
        Descriptors.b bVar10 = bVar4.r().get(5);
        internal_static_TW_Polkadot_Proto_Staking_Nominate_descriptor = bVar10;
        internal_static_TW_Polkadot_Proto_Staking_Nominate_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"Nominators"});
        Descriptors.b bVar11 = bVar4.r().get(6);
        internal_static_TW_Polkadot_Proto_Staking_Chill_descriptor = bVar11;
        internal_static_TW_Polkadot_Proto_Staking_Chill_fieldAccessorTable = new GeneratedMessageV3.e(bVar11, new String[0]);
        Descriptors.b bVar12 = getDescriptor().o().get(3);
        internal_static_TW_Polkadot_Proto_SigningInput_descriptor = bVar12;
        internal_static_TW_Polkadot_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar12, new String[]{"BlockHash", "GenesisHash", "Nonce", "SpecVersion", "TransactionVersion", "Tip", "Era", "PrivateKey", "Network", "BalanceCall", "StakingCall", "MessageOneof"});
        Descriptors.b bVar13 = getDescriptor().o().get(4);
        internal_static_TW_Polkadot_Proto_SigningOutput_descriptor = bVar13;
        internal_static_TW_Polkadot_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar13, new String[]{"Encoded"});
    }

    private Polkadot() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
