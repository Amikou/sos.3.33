package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Zilliqa {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\rZilliqa.proto\u0012\u0010TW.Zilliqa.Proto\"ç\u0001\n\u000bTransaction\u0012:\n\btransfer\u0018\u0001 \u0001(\u000b2&.TW.Zilliqa.Proto.Transaction.TransferH\u0000\u0012<\n\u000fraw_transaction\u0018\u0002 \u0001(\u000b2!.TW.Zilliqa.Proto.Transaction.RawH\u0000\u001a\u001a\n\bTransfer\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\f\u001a1\n\u0003Raw\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\f\u0012\f\n\u0004code\u0018\u0002 \u0001(\f\u0012\f\n\u0004data\u0018\u0003 \u0001(\fB\u000f\n\rmessage_oneof\"©\u0001\n\fSigningInput\u0012\u000f\n\u0007version\u0018\u0001 \u0001(\r\u0012\r\n\u0005nonce\u0018\u0002 \u0001(\u0004\u0012\n\n\u0002to\u0018\u0003 \u0001(\t\u0012\u0011\n\tgas_price\u0018\u0004 \u0001(\f\u0012\u0011\n\tgas_limit\u0018\u0005 \u0001(\u0004\u0012\u0013\n\u000bprivate_key\u0018\u0006 \u0001(\f\u00122\n\u000btransaction\u0018\u0007 \u0001(\u000b2\u001d.TW.Zilliqa.Proto.Transaction\"0\n\rSigningOutput\u0012\u0011\n\tsignature\u0018\u0001 \u0001(\f\u0012\f\n\u0004json\u0018\u0002 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Zilliqa_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Zilliqa_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Zilliqa_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Zilliqa_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Zilliqa_Proto_Transaction_Raw_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Zilliqa_Proto_Transaction_Raw_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Zilliqa_Proto_Transaction_Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Zilliqa_Proto_Transaction_Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Zilliqa_Proto_Transaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Zilliqa_Proto_Transaction_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Zilliqa$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Zilliqa$Transaction$MessageOneofCase;

        static {
            int[] iArr = new int[Transaction.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Zilliqa$Transaction$MessageOneofCase = iArr;
            try {
                iArr[Transaction.MessageOneofCase.TRANSFER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Zilliqa$Transaction$MessageOneofCase[Transaction.MessageOneofCase.RAW_TRANSACTION.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Zilliqa$Transaction$MessageOneofCase[Transaction.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int GAS_LIMIT_FIELD_NUMBER = 5;
        public static final int GAS_PRICE_FIELD_NUMBER = 4;
        public static final int NONCE_FIELD_NUMBER = 2;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 6;
        public static final int TO_FIELD_NUMBER = 3;
        public static final int TRANSACTION_FIELD_NUMBER = 7;
        public static final int VERSION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long gasLimit_;
        private ByteString gasPrice_;
        private byte memoizedIsInitialized;
        private long nonce_;
        private ByteString privateKey_;
        private volatile Object to_;
        private Transaction transaction_;
        private int version_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Zilliqa.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private long gasLimit_;
            private ByteString gasPrice_;
            private long nonce_;
            private ByteString privateKey_;
            private Object to_;
            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> transactionBuilder_;
            private Transaction transaction_;
            private int version_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningInput_descriptor;
            }

            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> getTransactionFieldBuilder() {
                if (this.transactionBuilder_ == null) {
                    this.transactionBuilder_ = new a1<>(getTransaction(), getParentForChildren(), isClean());
                    this.transaction_ = null;
                }
                return this.transactionBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = SigningInput.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearTo() {
                this.to_ = SigningInput.getDefaultInstance().getTo();
                onChanged();
                return this;
            }

            public Builder clearTransaction() {
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                    onChanged();
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public long getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public ByteString getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.to_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public Transaction getTransaction() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction = this.transaction_;
                    return transaction == null ? Transaction.getDefaultInstance() : transaction;
                }
                return a1Var.f();
            }

            public Transaction.Builder getTransactionBuilder() {
                onChanged();
                return getTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public TransactionOrBuilder getTransactionOrBuilder() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Transaction transaction = this.transaction_;
                return transaction == null ? Transaction.getDefaultInstance() : transaction;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
            public boolean hasTransaction() {
                return (this.transactionBuilder_ == null && this.transaction_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction2 = this.transaction_;
                    if (transaction2 != null) {
                        this.transaction_ = Transaction.newBuilder(transaction2).mergeFrom(transaction).buildPartial();
                    } else {
                        this.transaction_ = transaction;
                    }
                    onChanged();
                } else {
                    a1Var.h(transaction);
                }
                return this;
            }

            public Builder setGasLimit(long j) {
                this.gasLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setGasPrice(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTo(String str) {
                Objects.requireNonNull(str);
                this.to_ = str;
                onChanged();
                return this;
            }

            public Builder setToBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.to_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transaction);
                    this.transaction_ = transaction;
                    onChanged();
                } else {
                    a1Var.j(transaction);
                }
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.to_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.gasPrice_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.version_ = this.version_;
                signingInput.nonce_ = this.nonce_;
                signingInput.to_ = this.to_;
                signingInput.gasPrice_ = this.gasPrice_;
                signingInput.gasLimit_ = this.gasLimit_;
                signingInput.privateKey_ = this.privateKey_;
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    signingInput.transaction_ = this.transaction_;
                } else {
                    signingInput.transaction_ = a1Var.b();
                }
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.version_ = 0;
                this.nonce_ = 0L;
                this.to_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.gasPrice_ = byteString;
                this.gasLimit_ = 0L;
                this.privateKey_ = byteString;
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                return this;
            }

            public Builder setTransaction(Transaction.Builder builder) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    this.transaction_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.to_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.gasPrice_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getVersion() != 0) {
                    setVersion(signingInput.getVersion());
                }
                if (signingInput.getNonce() != 0) {
                    setNonce(signingInput.getNonce());
                }
                if (!signingInput.getTo().isEmpty()) {
                    this.to_ = signingInput.to_;
                    onChanged();
                }
                ByteString gasPrice = signingInput.getGasPrice();
                ByteString byteString = ByteString.EMPTY;
                if (gasPrice != byteString) {
                    setGasPrice(signingInput.getGasPrice());
                }
                if (signingInput.getGasLimit() != 0) {
                    setGasLimit(signingInput.getGasLimit());
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (signingInput.hasTransaction()) {
                    mergeTransaction(signingInput.getTransaction());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Zilliqa.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Zilliqa.SigningInput.access$4700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Zilliqa$SigningInput r3 = (wallet.core.jni.proto.Zilliqa.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Zilliqa$SigningInput r4 = (wallet.core.jni.proto.Zilliqa.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Zilliqa.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Zilliqa$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getVersion() == signingInput.getVersion() && getNonce() == signingInput.getNonce() && getTo().equals(signingInput.getTo()) && getGasPrice().equals(signingInput.getGasPrice()) && getGasLimit() == signingInput.getGasLimit() && getPrivateKey().equals(signingInput.getPrivateKey()) && hasTransaction() == signingInput.hasTransaction()) {
                return (!hasTransaction() || getTransaction().equals(signingInput.getTransaction())) && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public long getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public ByteString getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.version_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            long j = this.nonce_;
            if (j != 0) {
                Y += CodedOutputStream.a0(2, j);
            }
            if (!getToBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(3, this.to_);
            }
            if (!this.gasPrice_.isEmpty()) {
                Y += CodedOutputStream.h(4, this.gasPrice_);
            }
            long j2 = this.gasLimit_;
            if (j2 != 0) {
                Y += CodedOutputStream.a0(5, j2);
            }
            if (!this.privateKey_.isEmpty()) {
                Y += CodedOutputStream.h(6, this.privateKey_);
            }
            if (this.transaction_ != null) {
                Y += CodedOutputStream.G(7, getTransaction());
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public String getTo() {
            Object obj = this.to_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.to_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public ByteString getToBytes() {
            Object obj = this.to_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.to_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public Transaction getTransaction() {
            Transaction transaction = this.transaction_;
            return transaction == null ? Transaction.getDefaultInstance() : transaction;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public TransactionOrBuilder getTransactionOrBuilder() {
            return getTransaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningInputOrBuilder
        public boolean hasTransaction() {
            return this.transaction_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getVersion()) * 37) + 2) * 53) + a0.h(getNonce())) * 37) + 3) * 53) + getTo().hashCode()) * 37) + 4) * 53) + getGasPrice().hashCode()) * 37) + 5) * 53) + a0.h(getGasLimit())) * 37) + 6) * 53) + getPrivateKey().hashCode();
            if (hasTransaction()) {
                hashCode = (((hashCode * 37) + 7) * 53) + getTransaction().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.version_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            if (!getToBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.to_);
            }
            if (!this.gasPrice_.isEmpty()) {
                codedOutputStream.q0(4, this.gasPrice_);
            }
            long j2 = this.gasLimit_;
            if (j2 != 0) {
                codedOutputStream.d1(5, j2);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(6, this.privateKey_);
            }
            if (this.transaction_ != null) {
                codedOutputStream.K0(7, getTransaction());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.to_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.gasPrice_ = byteString;
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.version_ = jVar.K();
                                } else if (J == 16) {
                                    this.nonce_ = jVar.L();
                                } else if (J == 26) {
                                    this.to_ = jVar.I();
                                } else if (J == 34) {
                                    this.gasPrice_ = jVar.q();
                                } else if (J == 40) {
                                    this.gasLimit_ = jVar.L();
                                } else if (J == 50) {
                                    this.privateKey_ = jVar.q();
                                } else if (J != 58) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    Transaction transaction = this.transaction_;
                                    Transaction.Builder builder = transaction != null ? transaction.toBuilder() : null;
                                    Transaction transaction2 = (Transaction) jVar.z(Transaction.parser(), rVar);
                                    this.transaction_ = transaction2;
                                    if (builder != null) {
                                        builder.mergeFrom(transaction2);
                                        this.transaction_ = builder.buildPartial();
                                    }
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGasLimit();

        ByteString getGasPrice();

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getTo();

        ByteString getToBytes();

        Transaction getTransaction();

        TransactionOrBuilder getTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        int getVersion();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransaction();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int JSON_FIELD_NUMBER = 2;
        public static final int SIGNATURE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object json_;
        private byte memoizedIsInitialized;
        private ByteString signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Zilliqa.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private Object json_;
            private ByteString signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearJson() {
                this.json_ = SigningOutput.getDefaultInstance().getJson();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningOutputOrBuilder
            public String getJson() {
                Object obj = this.json_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.json_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningOutputOrBuilder
            public ByteString getJsonBytes() {
                Object obj = this.json_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.json_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Zilliqa.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setJson(String str) {
                Objects.requireNonNull(str);
                this.json_ = str;
                onChanged();
                return this;
            }

            public Builder setJsonBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.json_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.signature_ = this.signature_;
                signingOutput.json_ = this.json_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.signature_ = ByteString.EMPTY;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getSignature() != ByteString.EMPTY) {
                    setSignature(signingOutput.getSignature());
                }
                if (!signingOutput.getJson().isEmpty()) {
                    this.json_ = signingOutput.json_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Zilliqa.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Zilliqa.SigningOutput.access$5900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Zilliqa$SigningOutput r3 = (wallet.core.jni.proto.Zilliqa.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Zilliqa$SigningOutput r4 = (wallet.core.jni.proto.Zilliqa.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Zilliqa.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Zilliqa$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getSignature().equals(signingOutput.getSignature()) && getJson().equals(signingOutput.getJson()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningOutputOrBuilder
        public String getJson() {
            Object obj = this.json_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.json_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningOutputOrBuilder
        public ByteString getJsonBytes() {
            Object obj = this.json_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.json_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.signature_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.signature_);
            if (!getJsonBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(2, this.json_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Zilliqa.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSignature().hashCode()) * 37) + 2) * 53) + getJson().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Zilliqa.internal_static_TW_Zilliqa_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(1, this.signature_);
            }
            if (!getJsonBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.json_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.signature_ = ByteString.EMPTY;
            this.json_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.signature_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.json_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getJson();

        ByteString getJsonBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transaction extends GeneratedMessageV3 implements TransactionOrBuilder {
        private static final Transaction DEFAULT_INSTANCE = new Transaction();
        private static final t0<Transaction> PARSER = new c<Transaction>() { // from class: wallet.core.jni.proto.Zilliqa.Transaction.1
            @Override // com.google.protobuf.t0
            public Transaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transaction(jVar, rVar, null);
            }
        };
        public static final int RAW_TRANSACTION_FIELD_NUMBER = 2;
        public static final int TRANSFER_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOrBuilder {
            private int messageOneofCase_;
            private Object messageOneof_;
            private a1<Raw, Raw.Builder, RawOrBuilder> rawTransactionBuilder_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_descriptor;
            }

            private a1<Raw, Raw.Builder, RawOrBuilder> getRawTransactionFieldBuilder() {
                if (this.rawTransactionBuilder_ == null) {
                    if (this.messageOneofCase_ != 2) {
                        this.messageOneof_ = Raw.getDefaultInstance();
                    }
                    this.rawTransactionBuilder_ = new a1<>((Raw) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 2;
                onChanged();
                return this.rawTransactionBuilder_;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferFieldBuilder() {
                if (this.transferBuilder_ == null) {
                    if (this.messageOneofCase_ != 1) {
                        this.messageOneof_ = Transfer.getDefaultInstance();
                    }
                    this.transferBuilder_ = new a1<>((Transfer) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 1;
                onChanged();
                return this.transferBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearRawTransaction() {
                a1<Raw, Raw.Builder, RawOrBuilder> a1Var = this.rawTransactionBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_descriptor;
            }

            @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
            public Raw getRawTransaction() {
                a1<Raw, Raw.Builder, RawOrBuilder> a1Var = this.rawTransactionBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        return (Raw) this.messageOneof_;
                    }
                    return Raw.getDefaultInstance();
                } else if (this.messageOneofCase_ == 2) {
                    return a1Var.f();
                } else {
                    return Raw.getDefaultInstance();
                }
            }

            public Raw.Builder getRawTransactionBuilder() {
                return getRawTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
            public RawOrBuilder getRawTransactionOrBuilder() {
                a1<Raw, Raw.Builder, RawOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 2 || (a1Var = this.rawTransactionBuilder_) == null) {
                    if (i == 2) {
                        return (Raw) this.messageOneof_;
                    }
                    return Raw.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
            public Transfer getTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        return (Transfer) this.messageOneof_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.messageOneofCase_ == 1) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferBuilder() {
                return getTransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
            public TransferOrBuilder getTransferOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 1 || (a1Var = this.transferBuilder_) == null) {
                    if (i == 1) {
                        return (Transfer) this.messageOneof_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
            public boolean hasRawTransaction() {
                return this.messageOneofCase_ == 2;
            }

            @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
            public boolean hasTransfer() {
                return this.messageOneofCase_ == 1;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeRawTransaction(Raw raw) {
                a1<Raw, Raw.Builder, RawOrBuilder> a1Var = this.rawTransactionBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2 && this.messageOneof_ != Raw.getDefaultInstance()) {
                        this.messageOneof_ = Raw.newBuilder((Raw) this.messageOneof_).mergeFrom(raw).buildPartial();
                    } else {
                        this.messageOneof_ = raw;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 2) {
                        a1Var.h(raw);
                    }
                    this.rawTransactionBuilder_.j(raw);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder mergeTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1 && this.messageOneof_ != Transfer.getDefaultInstance()) {
                        this.messageOneof_ = Transfer.newBuilder((Transfer) this.messageOneof_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.messageOneof_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 1) {
                        a1Var.h(transfer);
                    }
                    this.transferBuilder_.j(transfer);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setRawTransaction(Raw raw) {
                a1<Raw, Raw.Builder, RawOrBuilder> a1Var = this.rawTransactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(raw);
                    this.messageOneof_ = raw;
                    onChanged();
                } else {
                    a1Var.j(raw);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.messageOneof_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction build() {
                Transaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction buildPartial() {
                Transaction transaction = new Transaction(this, (AnonymousClass1) null);
                if (this.messageOneofCase_ == 1) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                    if (a1Var == null) {
                        transaction.messageOneof_ = this.messageOneof_;
                    } else {
                        transaction.messageOneof_ = a1Var.b();
                    }
                }
                if (this.messageOneofCase_ == 2) {
                    a1<Raw, Raw.Builder, RawOrBuilder> a1Var2 = this.rawTransactionBuilder_;
                    if (a1Var2 == null) {
                        transaction.messageOneof_ = this.messageOneof_;
                    } else {
                        transaction.messageOneof_ = a1Var2.b();
                    }
                }
                transaction.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return transaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transaction getDefaultInstanceForType() {
                return Transaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transaction) {
                    return mergeFrom((Transaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setRawTransaction(Raw.Builder builder) {
                a1<Raw, Raw.Builder, RawOrBuilder> a1Var = this.rawTransactionBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setTransfer(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder mergeFrom(Transaction transaction) {
                if (transaction == Transaction.getDefaultInstance()) {
                    return this;
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Zilliqa$Transaction$MessageOneofCase[transaction.getMessageOneofCase().ordinal()];
                if (i == 1) {
                    mergeTransfer(transaction.getTransfer());
                } else if (i == 2) {
                    mergeRawTransaction(transaction.getRawTransaction());
                }
                mergeUnknownFields(transaction.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Zilliqa.Transaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Zilliqa.Transaction.access$3100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Zilliqa$Transaction r3 = (wallet.core.jni.proto.Zilliqa.Transaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Zilliqa$Transaction r4 = (wallet.core.jni.proto.Zilliqa.Transaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Zilliqa.Transaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Zilliqa$Transaction$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            TRANSFER(1),
            RAW_TRANSACTION(2),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            return null;
                        }
                        return RAW_TRANSACTION;
                    }
                    return TRANSFER;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class Raw extends GeneratedMessageV3 implements RawOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 1;
            public static final int CODE_FIELD_NUMBER = 2;
            public static final int DATA_FIELD_NUMBER = 3;
            private static final Raw DEFAULT_INSTANCE = new Raw();
            private static final t0<Raw> PARSER = new c<Raw>() { // from class: wallet.core.jni.proto.Zilliqa.Transaction.Raw.1
                @Override // com.google.protobuf.t0
                public Raw parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Raw(jVar, rVar, null);
                }
            };
            private static final long serialVersionUID = 0;
            private ByteString amount_;
            private ByteString code_;
            private ByteString data_;
            private byte memoizedIsInitialized;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements RawOrBuilder {
                private ByteString amount_;
                private ByteString code_;
                private ByteString data_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Raw_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = Raw.getDefaultInstance().getAmount();
                    onChanged();
                    return this;
                }

                public Builder clearCode() {
                    this.code_ = Raw.getDefaultInstance().getCode();
                    onChanged();
                    return this;
                }

                public Builder clearData() {
                    this.data_ = Raw.getDefaultInstance().getData();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Zilliqa.Transaction.RawOrBuilder
                public ByteString getAmount() {
                    return this.amount_;
                }

                @Override // wallet.core.jni.proto.Zilliqa.Transaction.RawOrBuilder
                public ByteString getCode() {
                    return this.code_;
                }

                @Override // wallet.core.jni.proto.Zilliqa.Transaction.RawOrBuilder
                public ByteString getData() {
                    return this.data_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Raw_descriptor;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Raw_fieldAccessorTable.d(Raw.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.amount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setCode(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.code_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setData(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.data_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.code_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Raw build() {
                    Raw buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Raw buildPartial() {
                    Raw raw = new Raw(this, (AnonymousClass1) null);
                    raw.amount_ = this.amount_;
                    raw.code_ = this.code_;
                    raw.data_ = this.data_;
                    onBuilt();
                    return raw;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Raw getDefaultInstanceForType() {
                    return Raw.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.code_ = byteString;
                    this.data_ = byteString;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Raw) {
                        return mergeFrom((Raw) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.code_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(Raw raw) {
                    if (raw == Raw.getDefaultInstance()) {
                        return this;
                    }
                    ByteString amount = raw.getAmount();
                    ByteString byteString = ByteString.EMPTY;
                    if (amount != byteString) {
                        setAmount(raw.getAmount());
                    }
                    if (raw.getCode() != byteString) {
                        setCode(raw.getCode());
                    }
                    if (raw.getData() != byteString) {
                        setData(raw.getData());
                    }
                    mergeUnknownFields(raw.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Zilliqa.Transaction.Raw.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Zilliqa.Transaction.Raw.access$2200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Zilliqa$Transaction$Raw r3 = (wallet.core.jni.proto.Zilliqa.Transaction.Raw) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Zilliqa$Transaction$Raw r4 = (wallet.core.jni.proto.Zilliqa.Transaction.Raw) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Zilliqa.Transaction.Raw.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Zilliqa$Transaction$Raw$Builder");
                }
            }

            public /* synthetic */ Raw(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Raw getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Raw_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Raw parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Raw) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Raw parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Raw> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Raw)) {
                    return super.equals(obj);
                }
                Raw raw = (Raw) obj;
                return getAmount().equals(raw.getAmount()) && getCode().equals(raw.getCode()) && getData().equals(raw.getData()) && this.unknownFields.equals(raw.unknownFields);
            }

            @Override // wallet.core.jni.proto.Zilliqa.Transaction.RawOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Zilliqa.Transaction.RawOrBuilder
            public ByteString getCode() {
                return this.code_;
            }

            @Override // wallet.core.jni.proto.Zilliqa.Transaction.RawOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Raw> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int h = this.amount_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.amount_);
                if (!this.code_.isEmpty()) {
                    h += CodedOutputStream.h(2, this.code_);
                }
                if (!this.data_.isEmpty()) {
                    h += CodedOutputStream.h(3, this.data_);
                }
                int serializedSize = h + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAmount().hashCode()) * 37) + 2) * 53) + getCode().hashCode()) * 37) + 3) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Raw_fieldAccessorTable.d(Raw.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Raw();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!this.amount_.isEmpty()) {
                    codedOutputStream.q0(1, this.amount_);
                }
                if (!this.code_.isEmpty()) {
                    codedOutputStream.q0(2, this.code_);
                }
                if (!this.data_.isEmpty()) {
                    codedOutputStream.q0(3, this.data_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Raw(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Raw raw) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(raw);
            }

            public static Raw parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Raw(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Raw parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Raw) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Raw parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Raw getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Raw parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Raw() {
                this.memoizedIsInitialized = (byte) -1;
                ByteString byteString = ByteString.EMPTY;
                this.amount_ = byteString;
                this.code_ = byteString;
                this.data_ = byteString;
            }

            public static Raw parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Raw parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Raw parseFrom(InputStream inputStream) throws IOException {
                return (Raw) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Raw parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Raw) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private Raw(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.amount_ = jVar.q();
                                } else if (J == 18) {
                                    this.code_ = jVar.q();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.data_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Raw parseFrom(j jVar) throws IOException {
                return (Raw) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Raw parseFrom(j jVar, r rVar) throws IOException {
                return (Raw) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface RawOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            ByteString getAmount();

            ByteString getCode();

            ByteString getData();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Transfer extends GeneratedMessageV3 implements TransferOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 1;
            private static final Transfer DEFAULT_INSTANCE = new Transfer();
            private static final t0<Transfer> PARSER = new c<Transfer>() { // from class: wallet.core.jni.proto.Zilliqa.Transaction.Transfer.1
                @Override // com.google.protobuf.t0
                public Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Transfer(jVar, rVar, null);
                }
            };
            private static final long serialVersionUID = 0;
            private ByteString amount_;
            private byte memoizedIsInitialized;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOrBuilder {
                private ByteString amount_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Transfer_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = Transfer.getDefaultInstance().getAmount();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Zilliqa.Transaction.TransferOrBuilder
                public ByteString getAmount() {
                    return this.amount_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Transfer_descriptor;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.amount_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.amount_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer build() {
                    Transfer buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer buildPartial() {
                    Transfer transfer = new Transfer(this, (AnonymousClass1) null);
                    transfer.amount_ = this.amount_;
                    onBuilt();
                    return transfer;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Transfer getDefaultInstanceForType() {
                    return Transfer.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.amount_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.amount_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Transfer) {
                        return mergeFrom((Transfer) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Transfer transfer) {
                    if (transfer == Transfer.getDefaultInstance()) {
                        return this;
                    }
                    if (transfer.getAmount() != ByteString.EMPTY) {
                        setAmount(transfer.getAmount());
                    }
                    mergeUnknownFields(transfer.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Zilliqa.Transaction.Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Zilliqa.Transaction.Transfer.access$1000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Zilliqa$Transaction$Transfer r3 = (wallet.core.jni.proto.Zilliqa.Transaction.Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Zilliqa$Transaction$Transfer r4 = (wallet.core.jni.proto.Zilliqa.Transaction.Transfer) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Zilliqa.Transaction.Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Zilliqa$Transaction$Transfer$Builder");
                }
            }

            public /* synthetic */ Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Transfer getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Transfer_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Transfer> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Transfer)) {
                    return super.equals(obj);
                }
                Transfer transfer = (Transfer) obj;
                return getAmount().equals(transfer.getAmount()) && this.unknownFields.equals(transfer.unknownFields);
            }

            @Override // wallet.core.jni.proto.Zilliqa.Transaction.TransferOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Transfer> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int h = (this.amount_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.amount_)) + this.unknownFields.getSerializedSize();
                this.memoizedSize = h;
                return h;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAmount().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Transfer();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!this.amount_.isEmpty()) {
                    codedOutputStream.q0(1, this.amount_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Transfer transfer) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(transfer);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Transfer(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Transfer getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Transfer() {
                this.memoizedIsInitialized = (byte) -1;
                this.amount_ = ByteString.EMPTY;
            }

            public static Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Transfer parseFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J != 10) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.amount_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(j jVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Transfer parseFrom(j jVar, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface TransferOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            ByteString getAmount();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ Transaction(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Transaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transaction)) {
                return super.equals(obj);
            }
            Transaction transaction = (Transaction) obj;
            if (getMessageOneofCase().equals(transaction.getMessageOneofCase())) {
                int i = this.messageOneofCase_;
                if (i != 1) {
                    if (i == 2 && !getRawTransaction().equals(transaction.getRawTransaction())) {
                        return false;
                    }
                } else if (!getTransfer().equals(transaction.getTransfer())) {
                    return false;
                }
                return this.unknownFields.equals(transaction.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transaction> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
        public Raw getRawTransaction() {
            if (this.messageOneofCase_ == 2) {
                return (Raw) this.messageOneof_;
            }
            return Raw.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
        public RawOrBuilder getRawTransactionOrBuilder() {
            if (this.messageOneofCase_ == 2) {
                return (Raw) this.messageOneof_;
            }
            return Raw.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.messageOneofCase_ == 1 ? 0 + CodedOutputStream.G(1, (Transfer) this.messageOneof_) : 0;
            if (this.messageOneofCase_ == 2) {
                G += CodedOutputStream.G(2, (Raw) this.messageOneof_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
        public Transfer getTransfer() {
            if (this.messageOneofCase_ == 1) {
                return (Transfer) this.messageOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
        public TransferOrBuilder getTransferOrBuilder() {
            if (this.messageOneofCase_ == 1) {
                return (Transfer) this.messageOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
        public boolean hasRawTransaction() {
            return this.messageOneofCase_ == 2;
        }

        @Override // wallet.core.jni.proto.Zilliqa.TransactionOrBuilder
        public boolean hasTransfer() {
            return this.messageOneofCase_ == 1;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = 779 + getDescriptor().hashCode();
            int i3 = this.messageOneofCase_;
            if (i3 == 1) {
                i = ((hashCode2 * 37) + 1) * 53;
                hashCode = getTransfer().hashCode();
            } else {
                if (i3 == 2) {
                    i = ((hashCode2 * 37) + 2) * 53;
                    hashCode = getRawTransaction().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Zilliqa.internal_static_TW_Zilliqa_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.messageOneofCase_ == 1) {
                codedOutputStream.K0(1, (Transfer) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 2) {
                codedOutputStream.K0(2, (Raw) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Transaction(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Transaction transaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transaction);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Transaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Transaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Transaction() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transaction parseFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Transaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    Transfer.Builder builder = this.messageOneofCase_ == 1 ? ((Transfer) this.messageOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(Transfer.parser(), rVar);
                                    this.messageOneof_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((Transfer) z2);
                                        this.messageOneof_ = builder.buildPartial();
                                    }
                                    this.messageOneofCase_ = 1;
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    Raw.Builder builder2 = this.messageOneofCase_ == 2 ? ((Raw) this.messageOneof_).toBuilder() : null;
                                    m0 z3 = jVar.z(Raw.parser(), rVar);
                                    this.messageOneof_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((Raw) z3);
                                        this.messageOneof_ = builder2.buildPartial();
                                    }
                                    this.messageOneofCase_ = 2;
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(j jVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transaction parseFrom(j jVar, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        Transaction.MessageOneofCase getMessageOneofCase();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Transaction.Raw getRawTransaction();

        Transaction.RawOrBuilder getRawTransactionOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Transaction.Transfer getTransfer();

        Transaction.TransferOrBuilder getTransferOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasRawTransaction();

        boolean hasTransfer();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Zilliqa_Proto_Transaction_descriptor = bVar;
        internal_static_TW_Zilliqa_Proto_Transaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Transfer", "RawTransaction", "MessageOneof"});
        Descriptors.b bVar2 = bVar.r().get(0);
        internal_static_TW_Zilliqa_Proto_Transaction_Transfer_descriptor = bVar2;
        internal_static_TW_Zilliqa_Proto_Transaction_Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Amount"});
        Descriptors.b bVar3 = bVar.r().get(1);
        internal_static_TW_Zilliqa_Proto_Transaction_Raw_descriptor = bVar3;
        internal_static_TW_Zilliqa_Proto_Transaction_Raw_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Amount", "Code", "Data"});
        Descriptors.b bVar4 = getDescriptor().o().get(1);
        internal_static_TW_Zilliqa_Proto_SigningInput_descriptor = bVar4;
        internal_static_TW_Zilliqa_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Version", "Nonce", "To", "GasPrice", "GasLimit", "PrivateKey", "Transaction"});
        Descriptors.b bVar5 = getDescriptor().o().get(2);
        internal_static_TW_Zilliqa_Proto_SigningOutput_descriptor = bVar5;
        internal_static_TW_Zilliqa_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"Signature", "Json"});
    }

    private Zilliqa() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
