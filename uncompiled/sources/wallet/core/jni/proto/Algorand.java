package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Algorand {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000eAlgorand.proto\u0012\u0011TW.Algorand.Proto\"j\n\u000eTransactionPay\u0012\u0012\n\nto_address\u0018\u0001 \u0001(\t\u0012\u000b\n\u0003fee\u0018\u0002 \u0001(\u0004\u0012\u000e\n\u0006amount\u0018\u0003 \u0001(\u0004\u0012\u0013\n\u000bfirst_round\u0018\u0004 \u0001(\u0004\u0012\u0012\n\nlast_round\u0018\u0005 \u0001(\u0004\"ª\u0001\n\fSigningInput\u0012\u0012\n\ngenesis_id\u0018\u0001 \u0001(\t\u0012\u0014\n\fgenesis_hash\u0018\u0002 \u0001(\f\u0012\f\n\u0004note\u0018\u0003 \u0001(\f\u0012\u0013\n\u000bprivate_key\u0018\u0004 \u0001(\f\u0012<\n\u000ftransaction_pay\u0018\n \u0001(\u000b2!.TW.Algorand.Proto.TransactionPayH\u0000B\u000f\n\rmessage_oneof\" \n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Algorand_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Algorand_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Algorand_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Algorand_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Algorand_Proto_TransactionPay_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Algorand_Proto_TransactionPay_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Algorand$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Algorand$SigningInput$MessageOneofCase;

        static {
            int[] iArr = new int[SigningInput.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Algorand$SigningInput$MessageOneofCase = iArr;
            try {
                iArr[SigningInput.MessageOneofCase.TRANSACTION_PAY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Algorand$SigningInput$MessageOneofCase[SigningInput.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int GENESIS_HASH_FIELD_NUMBER = 2;
        public static final int GENESIS_ID_FIELD_NUMBER = 1;
        public static final int NOTE_FIELD_NUMBER = 3;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 4;
        public static final int TRANSACTION_PAY_FIELD_NUMBER = 10;
        private static final long serialVersionUID = 0;
        private ByteString genesisHash_;
        private volatile Object genesisId_;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;
        private ByteString note_;
        private ByteString privateKey_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Algorand.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private ByteString genesisHash_;
            private Object genesisId_;
            private int messageOneofCase_;
            private Object messageOneof_;
            private ByteString note_;
            private ByteString privateKey_;
            private a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> transactionPayBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Algorand.internal_static_TW_Algorand_Proto_SigningInput_descriptor;
            }

            private a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> getTransactionPayFieldBuilder() {
                if (this.transactionPayBuilder_ == null) {
                    if (this.messageOneofCase_ != 10) {
                        this.messageOneof_ = TransactionPay.getDefaultInstance();
                    }
                    this.transactionPayBuilder_ = new a1<>((TransactionPay) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 10;
                onChanged();
                return this.transactionPayBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearGenesisHash() {
                this.genesisHash_ = SigningInput.getDefaultInstance().getGenesisHash();
                onChanged();
                return this;
            }

            public Builder clearGenesisId() {
                this.genesisId_ = SigningInput.getDefaultInstance().getGenesisId();
                onChanged();
                return this;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearNote() {
                this.note_ = SigningInput.getDefaultInstance().getNote();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearTransactionPay() {
                a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> a1Var = this.transactionPayBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 10) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 10) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Algorand.internal_static_TW_Algorand_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public ByteString getGenesisHash() {
                return this.genesisHash_;
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public String getGenesisId() {
                Object obj = this.genesisId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.genesisId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public ByteString getGenesisIdBytes() {
                Object obj = this.genesisId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.genesisId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public ByteString getNote() {
                return this.note_;
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public TransactionPay getTransactionPay() {
                a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> a1Var = this.transactionPayBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 10) {
                        return (TransactionPay) this.messageOneof_;
                    }
                    return TransactionPay.getDefaultInstance();
                } else if (this.messageOneofCase_ == 10) {
                    return a1Var.f();
                } else {
                    return TransactionPay.getDefaultInstance();
                }
            }

            public TransactionPay.Builder getTransactionPayBuilder() {
                return getTransactionPayFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public TransactionPayOrBuilder getTransactionPayOrBuilder() {
                a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 10 || (a1Var = this.transactionPayBuilder_) == null) {
                    if (i == 10) {
                        return (TransactionPay) this.messageOneof_;
                    }
                    return TransactionPay.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
            public boolean hasTransactionPay() {
                return this.messageOneofCase_ == 10;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Algorand.internal_static_TW_Algorand_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransactionPay(TransactionPay transactionPay) {
                a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> a1Var = this.transactionPayBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 10 && this.messageOneof_ != TransactionPay.getDefaultInstance()) {
                        this.messageOneof_ = TransactionPay.newBuilder((TransactionPay) this.messageOneof_).mergeFrom(transactionPay).buildPartial();
                    } else {
                        this.messageOneof_ = transactionPay;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 10) {
                        a1Var.h(transactionPay);
                    }
                    this.transactionPayBuilder_.j(transactionPay);
                }
                this.messageOneofCase_ = 10;
                return this;
            }

            public Builder setGenesisHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.genesisHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGenesisId(String str) {
                Objects.requireNonNull(str);
                this.genesisId_ = str;
                onChanged();
                return this;
            }

            public Builder setGenesisIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.genesisId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNote(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.note_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTransactionPay(TransactionPay transactionPay) {
                a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> a1Var = this.transactionPayBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionPay);
                    this.messageOneof_ = transactionPay;
                    onChanged();
                } else {
                    a1Var.j(transactionPay);
                }
                this.messageOneofCase_ = 10;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                this.genesisId_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.genesisHash_ = byteString;
                this.note_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.genesisId_ = this.genesisId_;
                signingInput.genesisHash_ = this.genesisHash_;
                signingInput.note_ = this.note_;
                signingInput.privateKey_ = this.privateKey_;
                if (this.messageOneofCase_ == 10) {
                    a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> a1Var = this.transactionPayBuilder_;
                    if (a1Var == null) {
                        signingInput.messageOneof_ = this.messageOneof_;
                    } else {
                        signingInput.messageOneof_ = a1Var.b();
                    }
                }
                signingInput.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.genesisId_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.genesisHash_ = byteString;
                this.note_ = byteString;
                this.privateKey_ = byteString;
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setTransactionPay(TransactionPay.Builder builder) {
                a1<TransactionPay, TransactionPay.Builder, TransactionPayOrBuilder> a1Var = this.transactionPayBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 10;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                this.genesisId_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.genesisHash_ = byteString;
                this.note_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (!signingInput.getGenesisId().isEmpty()) {
                    this.genesisId_ = signingInput.genesisId_;
                    onChanged();
                }
                ByteString genesisHash = signingInput.getGenesisHash();
                ByteString byteString = ByteString.EMPTY;
                if (genesisHash != byteString) {
                    setGenesisHash(signingInput.getGenesisHash());
                }
                if (signingInput.getNote() != byteString) {
                    setNote(signingInput.getNote());
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Algorand$SigningInput$MessageOneofCase[signingInput.getMessageOneofCase().ordinal()] == 1) {
                    mergeTransactionPay(signingInput.getTransactionPay());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Algorand.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Algorand.SigningInput.access$2800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Algorand$SigningInput r3 = (wallet.core.jni.proto.Algorand.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Algorand$SigningInput r4 = (wallet.core.jni.proto.Algorand.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Algorand.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Algorand$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            TRANSACTION_PAY(10),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 10) {
                        return null;
                    }
                    return TRANSACTION_PAY;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Algorand.internal_static_TW_Algorand_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getGenesisId().equals(signingInput.getGenesisId()) && getGenesisHash().equals(signingInput.getGenesisHash()) && getNote().equals(signingInput.getNote()) && getPrivateKey().equals(signingInput.getPrivateKey()) && getMessageOneofCase().equals(signingInput.getMessageOneofCase())) {
                return (this.messageOneofCase_ != 10 || getTransactionPay().equals(signingInput.getTransactionPay())) && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public ByteString getGenesisHash() {
            return this.genesisHash_;
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public String getGenesisId() {
            Object obj = this.genesisId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.genesisId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public ByteString getGenesisIdBytes() {
            Object obj = this.genesisId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.genesisId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public ByteString getNote() {
            return this.note_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getGenesisIdBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.genesisId_);
            if (!this.genesisHash_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(2, this.genesisHash_);
            }
            if (!this.note_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.note_);
            }
            if (!this.privateKey_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.privateKey_);
            }
            if (this.messageOneofCase_ == 10) {
                computeStringSize += CodedOutputStream.G(10, (TransactionPay) this.messageOneof_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public TransactionPay getTransactionPay() {
            if (this.messageOneofCase_ == 10) {
                return (TransactionPay) this.messageOneof_;
            }
            return TransactionPay.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public TransactionPayOrBuilder getTransactionPayOrBuilder() {
            if (this.messageOneofCase_ == 10) {
                return (TransactionPay) this.messageOneof_;
            }
            return TransactionPay.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Algorand.SigningInputOrBuilder
        public boolean hasTransactionPay() {
            return this.messageOneofCase_ == 10;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getGenesisId().hashCode()) * 37) + 2) * 53) + getGenesisHash().hashCode()) * 37) + 3) * 53) + getNote().hashCode()) * 37) + 4) * 53) + getPrivateKey().hashCode();
            if (this.messageOneofCase_ == 10) {
                hashCode = (((hashCode * 37) + 10) * 53) + getTransactionPay().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Algorand.internal_static_TW_Algorand_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getGenesisIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.genesisId_);
            }
            if (!this.genesisHash_.isEmpty()) {
                codedOutputStream.q0(2, this.genesisHash_);
            }
            if (!this.note_.isEmpty()) {
                codedOutputStream.q0(3, this.note_);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(4, this.privateKey_);
            }
            if (this.messageOneofCase_ == 10) {
                codedOutputStream.K0(10, (TransactionPay) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.genesisId_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.genesisHash_ = byteString;
            this.note_ = byteString;
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.genesisId_ = jVar.I();
                                } else if (J == 18) {
                                    this.genesisHash_ = jVar.q();
                                } else if (J == 26) {
                                    this.note_ = jVar.q();
                                } else if (J == 34) {
                                    this.privateKey_ = jVar.q();
                                } else if (J != 82) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    TransactionPay.Builder builder = this.messageOneofCase_ == 10 ? ((TransactionPay) this.messageOneof_).toBuilder() : null;
                                    m0 z2 = jVar.z(TransactionPay.parser(), rVar);
                                    this.messageOneof_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((TransactionPay) z2);
                                        this.messageOneof_ = builder.buildPartial();
                                    }
                                    this.messageOneofCase_ = 10;
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getGenesisHash();

        String getGenesisId();

        ByteString getGenesisIdBytes();

        /* synthetic */ String getInitializationErrorString();

        SigningInput.MessageOneofCase getMessageOneofCase();

        ByteString getNote();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        TransactionPay getTransactionPay();

        TransactionPayOrBuilder getTransactionPayOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransactionPay();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Algorand.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Algorand.internal_static_TW_Algorand_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Algorand.internal_static_TW_Algorand_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Algorand.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Algorand.internal_static_TW_Algorand_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Algorand.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Algorand.SigningOutput.access$3900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Algorand$SigningOutput r3 = (wallet.core.jni.proto.Algorand.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Algorand$SigningOutput r4 = (wallet.core.jni.proto.Algorand.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Algorand.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Algorand$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Algorand.internal_static_TW_Algorand_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Algorand.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Algorand.internal_static_TW_Algorand_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.encoded_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionPay extends GeneratedMessageV3 implements TransactionPayOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 3;
        public static final int FEE_FIELD_NUMBER = 2;
        public static final int FIRST_ROUND_FIELD_NUMBER = 4;
        public static final int LAST_ROUND_FIELD_NUMBER = 5;
        public static final int TO_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long amount_;
        private long fee_;
        private long firstRound_;
        private long lastRound_;
        private byte memoizedIsInitialized;
        private volatile Object toAddress_;
        private static final TransactionPay DEFAULT_INSTANCE = new TransactionPay();
        private static final t0<TransactionPay> PARSER = new c<TransactionPay>() { // from class: wallet.core.jni.proto.Algorand.TransactionPay.1
            @Override // com.google.protobuf.t0
            public TransactionPay parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionPay(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionPayOrBuilder {
            private long amount_;
            private long fee_;
            private long firstRound_;
            private long lastRound_;
            private Object toAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Algorand.internal_static_TW_Algorand_Proto_TransactionPay_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearFirstRound() {
                this.firstRound_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearLastRound() {
                this.lastRound_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransactionPay.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Algorand.internal_static_TW_Algorand_Proto_TransactionPay_descriptor;
            }

            @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
            public long getFirstRound() {
                return this.firstRound_;
            }

            @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
            public long getLastRound() {
                return this.lastRound_;
            }

            @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Algorand.internal_static_TW_Algorand_Proto_TransactionPay_fieldAccessorTable.d(TransactionPay.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setFirstRound(long j) {
                this.firstRound_ = j;
                onChanged();
                return this;
            }

            public Builder setLastRound(long j) {
                this.lastRound_ = j;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.toAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionPay build() {
                TransactionPay buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionPay buildPartial() {
                TransactionPay transactionPay = new TransactionPay(this, (AnonymousClass1) null);
                transactionPay.toAddress_ = this.toAddress_;
                transactionPay.fee_ = this.fee_;
                transactionPay.amount_ = this.amount_;
                transactionPay.firstRound_ = this.firstRound_;
                transactionPay.lastRound_ = this.lastRound_;
                onBuilt();
                return transactionPay;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionPay getDefaultInstanceForType() {
                return TransactionPay.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.toAddress_ = "";
                this.fee_ = 0L;
                this.amount_ = 0L;
                this.firstRound_ = 0L;
                this.lastRound_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.toAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionPay) {
                    return mergeFrom((TransactionPay) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionPay transactionPay) {
                if (transactionPay == TransactionPay.getDefaultInstance()) {
                    return this;
                }
                if (!transactionPay.getToAddress().isEmpty()) {
                    this.toAddress_ = transactionPay.toAddress_;
                    onChanged();
                }
                if (transactionPay.getFee() != 0) {
                    setFee(transactionPay.getFee());
                }
                if (transactionPay.getAmount() != 0) {
                    setAmount(transactionPay.getAmount());
                }
                if (transactionPay.getFirstRound() != 0) {
                    setFirstRound(transactionPay.getFirstRound());
                }
                if (transactionPay.getLastRound() != 0) {
                    setLastRound(transactionPay.getLastRound());
                }
                mergeUnknownFields(transactionPay.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Algorand.TransactionPay.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Algorand.TransactionPay.access$1200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Algorand$TransactionPay r3 = (wallet.core.jni.proto.Algorand.TransactionPay) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Algorand$TransactionPay r4 = (wallet.core.jni.proto.Algorand.TransactionPay) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Algorand.TransactionPay.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Algorand$TransactionPay$Builder");
            }
        }

        public /* synthetic */ TransactionPay(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransactionPay getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Algorand.internal_static_TW_Algorand_Proto_TransactionPay_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionPay parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionPay) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionPay parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionPay> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionPay)) {
                return super.equals(obj);
            }
            TransactionPay transactionPay = (TransactionPay) obj;
            return getToAddress().equals(transactionPay.getToAddress()) && getFee() == transactionPay.getFee() && getAmount() == transactionPay.getAmount() && getFirstRound() == transactionPay.getFirstRound() && getLastRound() == transactionPay.getLastRound() && this.unknownFields.equals(transactionPay.unknownFields);
        }

        @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
        public long getFirstRound() {
            return this.firstRound_;
        }

        @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
        public long getLastRound() {
            return this.lastRound_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionPay> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getToAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.toAddress_);
            long j = this.fee_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(2, j);
            }
            long j2 = this.amount_;
            if (j2 != 0) {
                computeStringSize += CodedOutputStream.a0(3, j2);
            }
            long j3 = this.firstRound_;
            if (j3 != 0) {
                computeStringSize += CodedOutputStream.a0(4, j3);
            }
            long j4 = this.lastRound_;
            if (j4 != 0) {
                computeStringSize += CodedOutputStream.a0(5, j4);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Algorand.TransactionPayOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getToAddress().hashCode()) * 37) + 2) * 53) + a0.h(getFee())) * 37) + 3) * 53) + a0.h(getAmount())) * 37) + 4) * 53) + a0.h(getFirstRound())) * 37) + 5) * 53) + a0.h(getLastRound())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Algorand.internal_static_TW_Algorand_Proto_TransactionPay_fieldAccessorTable.d(TransactionPay.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionPay();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.toAddress_);
            }
            long j = this.fee_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            long j2 = this.amount_;
            if (j2 != 0) {
                codedOutputStream.d1(3, j2);
            }
            long j3 = this.firstRound_;
            if (j3 != 0) {
                codedOutputStream.d1(4, j3);
            }
            long j4 = this.lastRound_;
            if (j4 != 0) {
                codedOutputStream.d1(5, j4);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransactionPay(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransactionPay transactionPay) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionPay);
        }

        public static TransactionPay parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionPay(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionPay parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionPay) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionPay parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionPay getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransactionPay parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionPay() {
            this.memoizedIsInitialized = (byte) -1;
            this.toAddress_ = "";
        }

        public static TransactionPay parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransactionPay parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionPay parseFrom(InputStream inputStream) throws IOException {
            return (TransactionPay) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionPay(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.toAddress_ = jVar.I();
                                } else if (J == 16) {
                                    this.fee_ = jVar.L();
                                } else if (J == 24) {
                                    this.amount_ = jVar.L();
                                } else if (J == 32) {
                                    this.firstRound_ = jVar.L();
                                } else if (J != 40) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.lastRound_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionPay parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionPay) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionPay parseFrom(j jVar) throws IOException {
            return (TransactionPay) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionPay parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionPay) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionPayOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        long getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getFirstRound();

        /* synthetic */ String getInitializationErrorString();

        long getLastRound();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Algorand_Proto_TransactionPay_descriptor = bVar;
        internal_static_TW_Algorand_Proto_TransactionPay_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"ToAddress", "Fee", "Amount", "FirstRound", "LastRound"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Algorand_Proto_SigningInput_descriptor = bVar2;
        internal_static_TW_Algorand_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"GenesisId", "GenesisHash", "Note", "PrivateKey", "TransactionPay", "MessageOneof"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Algorand_Proto_SigningOutput_descriptor = bVar3;
        internal_static_TW_Algorand_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Encoded"});
    }

    private Algorand() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
