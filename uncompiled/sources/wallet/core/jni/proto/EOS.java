package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.v0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import wallet.core.jni.proto.Common;

/* loaded from: classes3.dex */
public final class EOS {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\tEOS.proto\u0012\fTW.EOS.Proto\u001a\fCommon.proto\"9\n\u0005Asset\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\u0003\u0012\u0010\n\bdecimals\u0018\u0002 \u0001(\r\u0012\u000e\n\u0006symbol\u0018\u0003 \u0001(\t\"\u0087\u0002\n\fSigningInput\u0012\u0010\n\bchain_id\u0018\u0001 \u0001(\f\u0012\u001a\n\u0012reference_block_id\u0018\u0002 \u0001(\f\u0012\u001c\n\u0014reference_block_time\u0018\u0003 \u0001(\u000f\u0012\u0010\n\bcurrency\u0018\u0004 \u0001(\t\u0012\u000e\n\u0006sender\u0018\u0005 \u0001(\t\u0012\u0011\n\trecipient\u0018\u0006 \u0001(\t\u0012\f\n\u0004memo\u0018\u0007 \u0001(\t\u0012\"\n\u0005asset\u0018\b \u0001(\u000b2\u0013.TW.EOS.Proto.Asset\u0012\u0013\n\u000bprivate_key\u0018\t \u0001(\f\u0012/\n\u0010private_key_type\u0018\n \u0001(\u000e2\u0015.TW.EOS.Proto.KeyType\"S\n\rSigningOutput\u0012\u0014\n\fjson_encoded\u0018\u0001 \u0001(\t\u0012,\n\u0005error\u0018\u0002 \u0001(\u000e2\u001d.TW.Common.Proto.SigningError*1\n\u0007KeyType\u0012\n\n\u0006LEGACY\u0010\u0000\u0012\f\n\bMODERNK1\u0010\u0001\u0012\f\n\bMODERNR1\u0010\u0002B\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[]{Common.getDescriptor()});
    private static final Descriptors.b internal_static_TW_EOS_Proto_Asset_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_EOS_Proto_Asset_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_EOS_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_EOS_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_EOS_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_EOS_Proto_SigningOutput_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class Asset extends GeneratedMessageV3 implements AssetOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 1;
        public static final int DECIMALS_FIELD_NUMBER = 2;
        private static final Asset DEFAULT_INSTANCE = new Asset();
        private static final t0<Asset> PARSER = new c<Asset>() { // from class: wallet.core.jni.proto.EOS.Asset.1
            @Override // com.google.protobuf.t0
            public Asset parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Asset(jVar, rVar);
            }
        };
        public static final int SYMBOL_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private long amount_;
        private int decimals_;
        private byte memoizedIsInitialized;
        private volatile Object symbol_;

        public static Asset getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return EOS.internal_static_TW_EOS_Proto_Asset_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Asset parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Asset) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Asset parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Asset> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Asset)) {
                return super.equals(obj);
            }
            Asset asset = (Asset) obj;
            return getAmount() == asset.getAmount() && getDecimals() == asset.getDecimals() && getSymbol().equals(asset.getSymbol()) && this.unknownFields.equals(asset.unknownFields);
        }

        @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
        public int getDecimals() {
            return this.decimals_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Asset> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.amount_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            int i2 = this.decimals_;
            if (i2 != 0) {
                z += CodedOutputStream.Y(2, i2);
            }
            if (!getSymbolBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(3, this.symbol_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
        public String getSymbol() {
            Object obj = this.symbol_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.symbol_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
        public ByteString getSymbolBytes() {
            Object obj = this.symbol_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.symbol_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getAmount())) * 37) + 2) * 53) + getDecimals()) * 37) + 3) * 53) + getSymbol().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return EOS.internal_static_TW_EOS_Proto_Asset_fieldAccessorTable.d(Asset.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Asset();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            int i = this.decimals_;
            if (i != 0) {
                codedOutputStream.b1(2, i);
            }
            if (!getSymbolBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.symbol_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements AssetOrBuilder {
            private long amount_;
            private int decimals_;
            private Object symbol_;

            public static final Descriptors.b getDescriptor() {
                return EOS.internal_static_TW_EOS_Proto_Asset_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearDecimals() {
                this.decimals_ = 0;
                onChanged();
                return this;
            }

            public Builder clearSymbol() {
                this.symbol_ = Asset.getDefaultInstance().getSymbol();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
            public int getDecimals() {
                return this.decimals_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return EOS.internal_static_TW_EOS_Proto_Asset_descriptor;
            }

            @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
            public String getSymbol() {
                Object obj = this.symbol_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.symbol_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.EOS.AssetOrBuilder
            public ByteString getSymbolBytes() {
                Object obj = this.symbol_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.symbol_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return EOS.internal_static_TW_EOS_Proto_Asset_fieldAccessorTable.d(Asset.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setDecimals(int i) {
                this.decimals_ = i;
                onChanged();
                return this;
            }

            public Builder setSymbol(String str) {
                Objects.requireNonNull(str);
                this.symbol_ = str;
                onChanged();
                return this;
            }

            public Builder setSymbolBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.symbol_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.symbol_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Asset build() {
                Asset buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Asset buildPartial() {
                Asset asset = new Asset(this);
                asset.amount_ = this.amount_;
                asset.decimals_ = this.decimals_;
                asset.symbol_ = this.symbol_;
                onBuilt();
                return asset;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Asset getDefaultInstanceForType() {
                return Asset.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.amount_ = 0L;
                this.decimals_ = 0;
                this.symbol_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.symbol_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Asset) {
                    return mergeFrom((Asset) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Asset asset) {
                if (asset == Asset.getDefaultInstance()) {
                    return this;
                }
                if (asset.getAmount() != 0) {
                    setAmount(asset.getAmount());
                }
                if (asset.getDecimals() != 0) {
                    setDecimals(asset.getDecimals());
                }
                if (!asset.getSymbol().isEmpty()) {
                    this.symbol_ = asset.symbol_;
                    onChanged();
                }
                mergeUnknownFields(asset.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.EOS.Asset.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.EOS.Asset.access$1000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.EOS$Asset r3 = (wallet.core.jni.proto.EOS.Asset) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.EOS$Asset r4 = (wallet.core.jni.proto.EOS.Asset) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.EOS.Asset.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.EOS$Asset$Builder");
            }
        }

        public static Builder newBuilder(Asset asset) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(asset);
        }

        public static Asset parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Asset(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Asset parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Asset parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Asset getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static Asset parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Asset() {
            this.memoizedIsInitialized = (byte) -1;
            this.symbol_ = "";
        }

        public static Asset parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static Asset parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Asset parseFrom(InputStream inputStream) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Asset(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.amount_ = jVar.y();
                            } else if (J == 16) {
                                this.decimals_ = jVar.K();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.symbol_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Asset parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Asset parseFrom(j jVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Asset parseFrom(j jVar, r rVar) throws IOException {
            return (Asset) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface AssetOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        int getDecimals();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSymbol();

        ByteString getSymbolBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public enum KeyType implements v0 {
        LEGACY(0),
        MODERNK1(1),
        MODERNR1(2),
        UNRECOGNIZED(-1);
        
        public static final int LEGACY_VALUE = 0;
        public static final int MODERNK1_VALUE = 1;
        public static final int MODERNR1_VALUE = 2;
        private final int value;
        private static final a0.d<KeyType> internalValueMap = new a0.d<KeyType>() { // from class: wallet.core.jni.proto.EOS.KeyType.1
            @Override // com.google.protobuf.a0.d
            public KeyType findValueByNumber(int i) {
                return KeyType.forNumber(i);
            }
        };
        private static final KeyType[] VALUES = values();

        KeyType(int i) {
            this.value = i;
        }

        public static KeyType forNumber(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        return null;
                    }
                    return MODERNR1;
                }
                return MODERNK1;
            }
            return LEGACY;
        }

        public static final Descriptors.c getDescriptor() {
            return EOS.getDescriptor().l().get(0);
        }

        public static a0.d<KeyType> internalGetValueMap() {
            return internalValueMap;
        }

        public final Descriptors.c getDescriptorForType() {
            return getDescriptor();
        }

        @Override // com.google.protobuf.a0.c
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        public final Descriptors.d getValueDescriptor() {
            if (this != UNRECOGNIZED) {
                return getDescriptor().k().get(ordinal());
            }
            throw new IllegalStateException("Can't get the descriptor of an unrecognized enum value.");
        }

        @Deprecated
        public static KeyType valueOf(int i) {
            return forNumber(i);
        }

        public static KeyType valueOf(Descriptors.d dVar) {
            if (dVar.h() == getDescriptor()) {
                if (dVar.g() == -1) {
                    return UNRECOGNIZED;
                }
                return VALUES[dVar.g()];
            }
            throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        }
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int ASSET_FIELD_NUMBER = 8;
        public static final int CHAIN_ID_FIELD_NUMBER = 1;
        public static final int CURRENCY_FIELD_NUMBER = 4;
        public static final int MEMO_FIELD_NUMBER = 7;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 9;
        public static final int PRIVATE_KEY_TYPE_FIELD_NUMBER = 10;
        public static final int RECIPIENT_FIELD_NUMBER = 6;
        public static final int REFERENCE_BLOCK_ID_FIELD_NUMBER = 2;
        public static final int REFERENCE_BLOCK_TIME_FIELD_NUMBER = 3;
        public static final int SENDER_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private Asset asset_;
        private ByteString chainId_;
        private volatile Object currency_;
        private volatile Object memo_;
        private byte memoizedIsInitialized;
        private int privateKeyType_;
        private ByteString privateKey_;
        private volatile Object recipient_;
        private ByteString referenceBlockId_;
        private int referenceBlockTime_;
        private volatile Object sender_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.EOS.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return EOS.internal_static_TW_EOS_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getChainId().equals(signingInput.getChainId()) && getReferenceBlockId().equals(signingInput.getReferenceBlockId()) && getReferenceBlockTime() == signingInput.getReferenceBlockTime() && getCurrency().equals(signingInput.getCurrency()) && getSender().equals(signingInput.getSender()) && getRecipient().equals(signingInput.getRecipient()) && getMemo().equals(signingInput.getMemo()) && hasAsset() == signingInput.hasAsset()) {
                return (!hasAsset() || getAsset().equals(signingInput.getAsset())) && getPrivateKey().equals(signingInput.getPrivateKey()) && this.privateKeyType_ == signingInput.privateKeyType_ && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public Asset getAsset() {
            Asset asset = this.asset_;
            return asset == null ? Asset.getDefaultInstance() : asset;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public AssetOrBuilder getAssetOrBuilder() {
            return getAsset();
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public ByteString getChainId() {
            return this.chainId_;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public String getCurrency() {
            Object obj = this.currency_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.currency_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public ByteString getCurrencyBytes() {
            Object obj = this.currency_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.currency_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public String getMemo() {
            Object obj = this.memo_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.memo_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public ByteString getMemoBytes() {
            Object obj = this.memo_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.memo_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public KeyType getPrivateKeyType() {
            KeyType valueOf = KeyType.valueOf(this.privateKeyType_);
            return valueOf == null ? KeyType.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public int getPrivateKeyTypeValue() {
            return this.privateKeyType_;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public String getRecipient() {
            Object obj = this.recipient_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.recipient_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public ByteString getRecipientBytes() {
            Object obj = this.recipient_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.recipient_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public ByteString getReferenceBlockId() {
            return this.referenceBlockId_;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public int getReferenceBlockTime() {
            return this.referenceBlockTime_;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public String getSender() {
            Object obj = this.sender_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.sender_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public ByteString getSenderBytes() {
            Object obj = this.sender_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.sender_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.chainId_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.chainId_);
            if (!this.referenceBlockId_.isEmpty()) {
                h += CodedOutputStream.h(2, this.referenceBlockId_);
            }
            int i2 = this.referenceBlockTime_;
            if (i2 != 0) {
                h += CodedOutputStream.N(3, i2);
            }
            if (!getCurrencyBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(4, this.currency_);
            }
            if (!getSenderBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(5, this.sender_);
            }
            if (!getRecipientBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(6, this.recipient_);
            }
            if (!getMemoBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(7, this.memo_);
            }
            if (this.asset_ != null) {
                h += CodedOutputStream.G(8, getAsset());
            }
            if (!this.privateKey_.isEmpty()) {
                h += CodedOutputStream.h(9, this.privateKey_);
            }
            if (this.privateKeyType_ != KeyType.LEGACY.getNumber()) {
                h += CodedOutputStream.l(10, this.privateKeyType_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
        public boolean hasAsset() {
            return this.asset_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getChainId().hashCode()) * 37) + 2) * 53) + getReferenceBlockId().hashCode()) * 37) + 3) * 53) + getReferenceBlockTime()) * 37) + 4) * 53) + getCurrency().hashCode()) * 37) + 5) * 53) + getSender().hashCode()) * 37) + 6) * 53) + getRecipient().hashCode()) * 37) + 7) * 53) + getMemo().hashCode();
            if (hasAsset()) {
                hashCode = (((hashCode * 37) + 8) * 53) + getAsset().hashCode();
            }
            int hashCode2 = (((((((((hashCode * 37) + 9) * 53) + getPrivateKey().hashCode()) * 37) + 10) * 53) + this.privateKeyType_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return EOS.internal_static_TW_EOS_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.chainId_.isEmpty()) {
                codedOutputStream.q0(1, this.chainId_);
            }
            if (!this.referenceBlockId_.isEmpty()) {
                codedOutputStream.q0(2, this.referenceBlockId_);
            }
            int i = this.referenceBlockTime_;
            if (i != 0) {
                codedOutputStream.Q0(3, i);
            }
            if (!getCurrencyBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.currency_);
            }
            if (!getSenderBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.sender_);
            }
            if (!getRecipientBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 6, this.recipient_);
            }
            if (!getMemoBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 7, this.memo_);
            }
            if (this.asset_ != null) {
                codedOutputStream.K0(8, getAsset());
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(9, this.privateKey_);
            }
            if (this.privateKeyType_ != KeyType.LEGACY.getNumber()) {
                codedOutputStream.u0(10, this.privateKeyType_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private a1<Asset, Asset.Builder, AssetOrBuilder> assetBuilder_;
            private Asset asset_;
            private ByteString chainId_;
            private Object currency_;
            private Object memo_;
            private int privateKeyType_;
            private ByteString privateKey_;
            private Object recipient_;
            private ByteString referenceBlockId_;
            private int referenceBlockTime_;
            private Object sender_;

            private a1<Asset, Asset.Builder, AssetOrBuilder> getAssetFieldBuilder() {
                if (this.assetBuilder_ == null) {
                    this.assetBuilder_ = new a1<>(getAsset(), getParentForChildren(), isClean());
                    this.asset_ = null;
                }
                return this.assetBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return EOS.internal_static_TW_EOS_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAsset() {
                if (this.assetBuilder_ == null) {
                    this.asset_ = null;
                    onChanged();
                } else {
                    this.asset_ = null;
                    this.assetBuilder_ = null;
                }
                return this;
            }

            public Builder clearChainId() {
                this.chainId_ = SigningInput.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearCurrency() {
                this.currency_ = SigningInput.getDefaultInstance().getCurrency();
                onChanged();
                return this;
            }

            public Builder clearMemo() {
                this.memo_ = SigningInput.getDefaultInstance().getMemo();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearPrivateKeyType() {
                this.privateKeyType_ = 0;
                onChanged();
                return this;
            }

            public Builder clearRecipient() {
                this.recipient_ = SigningInput.getDefaultInstance().getRecipient();
                onChanged();
                return this;
            }

            public Builder clearReferenceBlockId() {
                this.referenceBlockId_ = SigningInput.getDefaultInstance().getReferenceBlockId();
                onChanged();
                return this;
            }

            public Builder clearReferenceBlockTime() {
                this.referenceBlockTime_ = 0;
                onChanged();
                return this;
            }

            public Builder clearSender() {
                this.sender_ = SigningInput.getDefaultInstance().getSender();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public Asset getAsset() {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Asset asset = this.asset_;
                    return asset == null ? Asset.getDefaultInstance() : asset;
                }
                return a1Var.f();
            }

            public Asset.Builder getAssetBuilder() {
                onChanged();
                return getAssetFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public AssetOrBuilder getAssetOrBuilder() {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Asset asset = this.asset_;
                return asset == null ? Asset.getDefaultInstance() : asset;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public ByteString getChainId() {
                return this.chainId_;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public String getCurrency() {
                Object obj = this.currency_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.currency_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public ByteString getCurrencyBytes() {
                Object obj = this.currency_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.currency_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return EOS.internal_static_TW_EOS_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public String getMemo() {
                Object obj = this.memo_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.memo_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public ByteString getMemoBytes() {
                Object obj = this.memo_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.memo_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public KeyType getPrivateKeyType() {
                KeyType valueOf = KeyType.valueOf(this.privateKeyType_);
                return valueOf == null ? KeyType.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public int getPrivateKeyTypeValue() {
                return this.privateKeyType_;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public String getRecipient() {
                Object obj = this.recipient_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.recipient_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public ByteString getRecipientBytes() {
                Object obj = this.recipient_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.recipient_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public ByteString getReferenceBlockId() {
                return this.referenceBlockId_;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public int getReferenceBlockTime() {
                return this.referenceBlockTime_;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public String getSender() {
                Object obj = this.sender_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.sender_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public ByteString getSenderBytes() {
                Object obj = this.sender_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.sender_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningInputOrBuilder
            public boolean hasAsset() {
                return (this.assetBuilder_ == null && this.asset_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return EOS.internal_static_TW_EOS_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeAsset(Asset asset) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Asset asset2 = this.asset_;
                    if (asset2 != null) {
                        this.asset_ = Asset.newBuilder(asset2).mergeFrom(asset).buildPartial();
                    } else {
                        this.asset_ = asset;
                    }
                    onChanged();
                } else {
                    a1Var.h(asset);
                }
                return this;
            }

            public Builder setAsset(Asset asset) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(asset);
                    this.asset_ = asset;
                    onChanged();
                } else {
                    a1Var.j(asset);
                }
                return this;
            }

            public Builder setChainId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setCurrency(String str) {
                Objects.requireNonNull(str);
                this.currency_ = str;
                onChanged();
                return this;
            }

            public Builder setCurrencyBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.currency_ = byteString;
                onChanged();
                return this;
            }

            public Builder setMemo(String str) {
                Objects.requireNonNull(str);
                this.memo_ = str;
                onChanged();
                return this;
            }

            public Builder setMemoBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.memo_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKeyType(KeyType keyType) {
                Objects.requireNonNull(keyType);
                this.privateKeyType_ = keyType.getNumber();
                onChanged();
                return this;
            }

            public Builder setPrivateKeyTypeValue(int i) {
                this.privateKeyType_ = i;
                onChanged();
                return this;
            }

            public Builder setRecipient(String str) {
                Objects.requireNonNull(str);
                this.recipient_ = str;
                onChanged();
                return this;
            }

            public Builder setRecipientBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.recipient_ = byteString;
                onChanged();
                return this;
            }

            public Builder setReferenceBlockId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.referenceBlockId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setReferenceBlockTime(int i) {
                this.referenceBlockTime_ = i;
                onChanged();
                return this;
            }

            public Builder setSender(String str) {
                Objects.requireNonNull(str);
                this.sender_ = str;
                onChanged();
                return this;
            }

            public Builder setSenderBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.sender_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.referenceBlockId_ = byteString;
                this.currency_ = "";
                this.sender_ = "";
                this.recipient_ = "";
                this.memo_ = "";
                this.privateKey_ = byteString;
                this.privateKeyType_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                signingInput.chainId_ = this.chainId_;
                signingInput.referenceBlockId_ = this.referenceBlockId_;
                signingInput.referenceBlockTime_ = this.referenceBlockTime_;
                signingInput.currency_ = this.currency_;
                signingInput.sender_ = this.sender_;
                signingInput.recipient_ = this.recipient_;
                signingInput.memo_ = this.memo_;
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    signingInput.asset_ = this.asset_;
                } else {
                    signingInput.asset_ = a1Var.b();
                }
                signingInput.privateKey_ = this.privateKey_;
                signingInput.privateKeyType_ = this.privateKeyType_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.referenceBlockId_ = byteString;
                this.referenceBlockTime_ = 0;
                this.currency_ = "";
                this.sender_ = "";
                this.recipient_ = "";
                this.memo_ = "";
                if (this.assetBuilder_ == null) {
                    this.asset_ = null;
                } else {
                    this.asset_ = null;
                    this.assetBuilder_ = null;
                }
                this.privateKey_ = byteString;
                this.privateKeyType_ = 0;
                return this;
            }

            public Builder setAsset(Asset.Builder builder) {
                a1<Asset, Asset.Builder, AssetOrBuilder> a1Var = this.assetBuilder_;
                if (a1Var == null) {
                    this.asset_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                ByteString chainId = signingInput.getChainId();
                ByteString byteString = ByteString.EMPTY;
                if (chainId != byteString) {
                    setChainId(signingInput.getChainId());
                }
                if (signingInput.getReferenceBlockId() != byteString) {
                    setReferenceBlockId(signingInput.getReferenceBlockId());
                }
                if (signingInput.getReferenceBlockTime() != 0) {
                    setReferenceBlockTime(signingInput.getReferenceBlockTime());
                }
                if (!signingInput.getCurrency().isEmpty()) {
                    this.currency_ = signingInput.currency_;
                    onChanged();
                }
                if (!signingInput.getSender().isEmpty()) {
                    this.sender_ = signingInput.sender_;
                    onChanged();
                }
                if (!signingInput.getRecipient().isEmpty()) {
                    this.recipient_ = signingInput.recipient_;
                    onChanged();
                }
                if (!signingInput.getMemo().isEmpty()) {
                    this.memo_ = signingInput.memo_;
                    onChanged();
                }
                if (signingInput.hasAsset()) {
                    mergeAsset(signingInput.getAsset());
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (signingInput.privateKeyType_ != 0) {
                    setPrivateKeyTypeValue(signingInput.getPrivateKeyTypeValue());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.referenceBlockId_ = byteString;
                this.currency_ = "";
                this.sender_ = "";
                this.recipient_ = "";
                this.memo_ = "";
                this.privateKey_ = byteString;
                this.privateKeyType_ = 0;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.EOS.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.EOS.SigningInput.access$3000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.EOS$SigningInput r3 = (wallet.core.jni.proto.EOS.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.EOS$SigningInput r4 = (wallet.core.jni.proto.EOS.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.EOS.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.EOS$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.chainId_ = byteString;
            this.referenceBlockId_ = byteString;
            this.currency_ = "";
            this.sender_ = "";
            this.recipient_ = "";
            this.memo_ = "";
            this.privateKey_ = byteString;
            this.privateKeyType_ = 0;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 10:
                                this.chainId_ = jVar.q();
                                continue;
                            case 18:
                                this.referenceBlockId_ = jVar.q();
                                continue;
                            case 29:
                                this.referenceBlockTime_ = jVar.D();
                                continue;
                            case 34:
                                this.currency_ = jVar.I();
                                continue;
                            case 42:
                                this.sender_ = jVar.I();
                                continue;
                            case 50:
                                this.recipient_ = jVar.I();
                                continue;
                            case 58:
                                this.memo_ = jVar.I();
                                continue;
                            case 66:
                                Asset asset = this.asset_;
                                Asset.Builder builder = asset != null ? asset.toBuilder() : null;
                                Asset asset2 = (Asset) jVar.z(Asset.parser(), rVar);
                                this.asset_ = asset2;
                                if (builder != null) {
                                    builder.mergeFrom(asset2);
                                    this.asset_ = builder.buildPartial();
                                } else {
                                    continue;
                                }
                            case 74:
                                this.privateKey_ = jVar.q();
                                continue;
                            case 80:
                                this.privateKeyType_ = jVar.s();
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Asset getAsset();

        AssetOrBuilder getAssetOrBuilder();

        ByteString getChainId();

        String getCurrency();

        ByteString getCurrencyBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getMemo();

        ByteString getMemoBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        KeyType getPrivateKeyType();

        int getPrivateKeyTypeValue();

        String getRecipient();

        ByteString getRecipientBytes();

        ByteString getReferenceBlockId();

        int getReferenceBlockTime();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getSender();

        ByteString getSenderBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasAsset();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ERROR_FIELD_NUMBER = 2;
        public static final int JSON_ENCODED_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private int error_;
        private volatile Object jsonEncoded_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.EOS.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return EOS.internal_static_TW_EOS_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getJsonEncoded().equals(signingOutput.getJsonEncoded()) && this.error_ == signingOutput.error_ && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
        public Common.SigningError getError() {
            Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
            return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
        public int getErrorValue() {
            return this.error_;
        }

        @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
        public String getJsonEncoded() {
            Object obj = this.jsonEncoded_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.jsonEncoded_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
        public ByteString getJsonEncodedBytes() {
            Object obj = this.jsonEncoded_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.jsonEncoded_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getJsonEncodedBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.jsonEncoded_);
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                computeStringSize += CodedOutputStream.l(2, this.error_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getJsonEncoded().hashCode()) * 37) + 2) * 53) + this.error_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return EOS.internal_static_TW_EOS_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getJsonEncodedBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.jsonEncoded_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                codedOutputStream.u0(2, this.error_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private int error_;
            private Object jsonEncoded_;

            public static final Descriptors.b getDescriptor() {
                return EOS.internal_static_TW_EOS_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearError() {
                this.error_ = 0;
                onChanged();
                return this;
            }

            public Builder clearJsonEncoded() {
                this.jsonEncoded_ = SigningOutput.getDefaultInstance().getJsonEncoded();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return EOS.internal_static_TW_EOS_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
            public Common.SigningError getError() {
                Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
                return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
            public int getErrorValue() {
                return this.error_;
            }

            @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
            public String getJsonEncoded() {
                Object obj = this.jsonEncoded_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.jsonEncoded_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.EOS.SigningOutputOrBuilder
            public ByteString getJsonEncodedBytes() {
                Object obj = this.jsonEncoded_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.jsonEncoded_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return EOS.internal_static_TW_EOS_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setError(Common.SigningError signingError) {
                Objects.requireNonNull(signingError);
                this.error_ = signingError.getNumber();
                onChanged();
                return this;
            }

            public Builder setErrorValue(int i) {
                this.error_ = i;
                onChanged();
                return this;
            }

            public Builder setJsonEncoded(String str) {
                Objects.requireNonNull(str);
                this.jsonEncoded_ = str;
                onChanged();
                return this;
            }

            public Builder setJsonEncodedBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.jsonEncoded_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.jsonEncoded_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                signingOutput.jsonEncoded_ = this.jsonEncoded_;
                signingOutput.error_ = this.error_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.jsonEncoded_ = "";
                this.error_ = 0;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.jsonEncoded_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (!signingOutput.getJsonEncoded().isEmpty()) {
                    this.jsonEncoded_ = signingOutput.jsonEncoded_;
                    onChanged();
                }
                if (signingOutput.error_ != 0) {
                    setErrorValue(signingOutput.getErrorValue());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.EOS.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.EOS.SigningOutput.access$4500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.EOS$SigningOutput r3 = (wallet.core.jni.proto.EOS.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.EOS$SigningOutput r4 = (wallet.core.jni.proto.EOS.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.EOS.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.EOS$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.jsonEncoded_ = "";
            this.error_ = 0;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.jsonEncoded_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.error_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        Common.SigningError getError();

        int getErrorValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getJsonEncoded();

        ByteString getJsonEncodedBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_EOS_Proto_Asset_descriptor = bVar;
        internal_static_TW_EOS_Proto_Asset_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Amount", "Decimals", "Symbol"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_EOS_Proto_SigningInput_descriptor = bVar2;
        internal_static_TW_EOS_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"ChainId", "ReferenceBlockId", "ReferenceBlockTime", "Currency", "Sender", "Recipient", "Memo", "Asset", "PrivateKey", "PrivateKeyType"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_EOS_Proto_SigningOutput_descriptor = bVar3;
        internal_static_TW_EOS_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"JsonEncoded", "Error"});
        Common.getDescriptor();
    }

    private EOS() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
