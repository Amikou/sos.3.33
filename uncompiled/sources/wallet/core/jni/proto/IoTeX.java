package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import zendesk.core.Constants;

/* loaded from: classes3.dex */
public final class IoTeX {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000bIoTeX.proto\u0012\u000eTW.IoTeX.Proto\">\n\bTransfer\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\t\u0012\u0011\n\trecipient\u0018\u0002 \u0001(\t\u0012\u000f\n\u0007payload\u0018\u0003 \u0001(\f\"Ñ\n\n\u0007Staking\u00125\n\u000bstakeCreate\u0018\u0001 \u0001(\u000b2\u001e.TW.IoTeX.Proto.Staking.CreateH\u0000\u00127\n\fstakeUnstake\u0018\u0002 \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.ReclaimH\u0000\u00128\n\rstakeWithdraw\u0018\u0003 \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.ReclaimH\u0000\u0012=\n\u000fstakeAddDeposit\u0018\u0004 \u0001(\u000b2\".TW.IoTeX.Proto.Staking.AddDepositH\u0000\u00127\n\fstakeRestake\u0018\u0005 \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.RestakeH\u0000\u0012G\n\u0014stakeChangeCandidate\u0018\u0006 \u0001(\u000b2'.TW.IoTeX.Proto.Staking.ChangeCandidateH\u0000\u0012K\n\u0016stakeTransferOwnership\u0018\u0007 \u0001(\u000b2).TW.IoTeX.Proto.Staking.TransferOwnershipH\u0000\u0012F\n\u0011candidateRegister\u0018\b \u0001(\u000b2).TW.IoTeX.Proto.Staking.CandidateRegisterH\u0000\u0012E\n\u000fcandidateUpdate\u0018\t \u0001(\u000b2*.TW.IoTeX.Proto.Staking.CandidateBasicInfoH\u0000\u001aq\n\u0006Create\u0012\u0015\n\rcandidateName\u0018\u0001 \u0001(\t\u0012\u0014\n\fstakedAmount\u0018\u0002 \u0001(\t\u0012\u0016\n\u000estakedDuration\u0018\u0003 \u0001(\r\u0012\u0011\n\tautoStake\u0018\u0004 \u0001(\b\u0012\u000f\n\u0007payload\u0018\u0005 \u0001(\f\u001a/\n\u0007Reclaim\u0012\u0013\n\u000bbucketIndex\u0018\u0001 \u0001(\u0004\u0012\u000f\n\u0007payload\u0018\u0002 \u0001(\f\u001aB\n\nAddDeposit\u0012\u0013\n\u000bbucketIndex\u0018\u0001 \u0001(\u0004\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\t\u0012\u000f\n\u0007payload\u0018\u0003 \u0001(\f\u001aZ\n\u0007Restake\u0012\u0013\n\u000bbucketIndex\u0018\u0001 \u0001(\u0004\u0012\u0016\n\u000estakedDuration\u0018\u0002 \u0001(\r\u0012\u0011\n\tautoStake\u0018\u0003 \u0001(\b\u0012\u000f\n\u0007payload\u0018\u0004 \u0001(\f\u001aN\n\u000fChangeCandidate\u0012\u0013\n\u000bbucketIndex\u0018\u0001 \u0001(\u0004\u0012\u0015\n\rcandidateName\u0018\u0002 \u0001(\t\u0012\u000f\n\u0007payload\u0018\u0003 \u0001(\f\u001aO\n\u0011TransferOwnership\u0012\u0013\n\u000bbucketIndex\u0018\u0001 \u0001(\u0004\u0012\u0014\n\fvoterAddress\u0018\u0002 \u0001(\t\u0012\u000f\n\u0007payload\u0018\u0003 \u0001(\f\u001aR\n\u0012CandidateBasicInfo\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u0017\n\u000foperatorAddress\u0018\u0002 \u0001(\t\u0012\u0015\n\rrewardAddress\u0018\u0003 \u0001(\t\u001aº\u0001\n\u0011CandidateRegister\u0012=\n\tcandidate\u0018\u0001 \u0001(\u000b2*.TW.IoTeX.Proto.Staking.CandidateBasicInfo\u0012\u0014\n\fstakedAmount\u0018\u0002 \u0001(\t\u0012\u0016\n\u000estakedDuration\u0018\u0003 \u0001(\r\u0012\u0011\n\tautoStake\u0018\u0004 \u0001(\b\u0012\u0014\n\fownerAddress\u0018\u0005 \u0001(\t\u0012\u000f\n\u0007payload\u0018\u0006 \u0001(\fB\t\n\u0007message\">\n\fContractCall\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\t\u0012\u0010\n\bcontract\u0018\u0002 \u0001(\t\u0012\f\n\u0004data\u0018\u0003 \u0001(\f\"\u0093\u0006\n\fSigningInput\u0012\u000f\n\u0007version\u0018\u0001 \u0001(\r\u0012\r\n\u0005nonce\u0018\u0002 \u0001(\u0004\u0012\u0010\n\bgasLimit\u0018\u0003 \u0001(\u0004\u0012\u0010\n\bgasPrice\u0018\u0004 \u0001(\t\u0012\u0012\n\nprivateKey\u0018\u0005 \u0001(\f\u0012,\n\btransfer\u0018\n \u0001(\u000b2\u0018.TW.IoTeX.Proto.TransferH\u0000\u0012,\n\u0004call\u0018\f \u0001(\u000b2\u001c.TW.IoTeX.Proto.ContractCallH\u0000\u00125\n\u000bstakeCreate\u0018( \u0001(\u000b2\u001e.TW.IoTeX.Proto.Staking.CreateH\u0000\u00127\n\fstakeUnstake\u0018) \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.ReclaimH\u0000\u00128\n\rstakeWithdraw\u0018* \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.ReclaimH\u0000\u0012=\n\u000fstakeAddDeposit\u0018+ \u0001(\u000b2\".TW.IoTeX.Proto.Staking.AddDepositH\u0000\u00127\n\fstakeRestake\u0018, \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.RestakeH\u0000\u0012G\n\u0014stakeChangeCandidate\u0018- \u0001(\u000b2'.TW.IoTeX.Proto.Staking.ChangeCandidateH\u0000\u0012K\n\u0016stakeTransferOwnership\u0018. \u0001(\u000b2).TW.IoTeX.Proto.Staking.TransferOwnershipH\u0000\u0012F\n\u0011candidateRegister\u0018/ \u0001(\u000b2).TW.IoTeX.Proto.Staking.CandidateRegisterH\u0000\u0012E\n\u000fcandidateUpdate\u00180 \u0001(\u000b2*.TW.IoTeX.Proto.Staking.CandidateBasicInfoH\u0000B\b\n\u0006action\".\n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f\u0012\f\n\u0004hash\u0018\u0002 \u0001(\f\"\u0082\u0006\n\nActionCore\u0012\u000f\n\u0007version\u0018\u0001 \u0001(\r\u0012\r\n\u0005nonce\u0018\u0002 \u0001(\u0004\u0012\u0010\n\bgasLimit\u0018\u0003 \u0001(\u0004\u0012\u0010\n\bgasPrice\u0018\u0004 \u0001(\t\u0012,\n\btransfer\u0018\n \u0001(\u000b2\u0018.TW.IoTeX.Proto.TransferH\u0000\u00121\n\texecution\u0018\f \u0001(\u000b2\u001c.TW.IoTeX.Proto.ContractCallH\u0000\u00125\n\u000bstakeCreate\u0018( \u0001(\u000b2\u001e.TW.IoTeX.Proto.Staking.CreateH\u0000\u00127\n\fstakeUnstake\u0018) \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.ReclaimH\u0000\u00128\n\rstakeWithdraw\u0018* \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.ReclaimH\u0000\u0012=\n\u000fstakeAddDeposit\u0018+ \u0001(\u000b2\".TW.IoTeX.Proto.Staking.AddDepositH\u0000\u00127\n\fstakeRestake\u0018, \u0001(\u000b2\u001f.TW.IoTeX.Proto.Staking.RestakeH\u0000\u0012G\n\u0014stakeChangeCandidate\u0018- \u0001(\u000b2'.TW.IoTeX.Proto.Staking.ChangeCandidateH\u0000\u0012K\n\u0016stakeTransferOwnership\u0018. \u0001(\u000b2).TW.IoTeX.Proto.Staking.TransferOwnershipH\u0000\u0012F\n\u0011candidateRegister\u0018/ \u0001(\u000b2).TW.IoTeX.Proto.Staking.CandidateRegisterH\u0000\u0012E\n\u000fcandidateUpdate\u00180 \u0001(\u000b2*.TW.IoTeX.Proto.Staking.CandidateBasicInfoH\u0000B\b\n\u0006action\"[\n\u0006Action\u0012(\n\u0004core\u0018\u0001 \u0001(\u000b2\u001a.TW.IoTeX.Proto.ActionCore\u0012\u0014\n\fsenderPubKey\u0018\u0002 \u0001(\f\u0012\u0011\n\tsignature\u0018\u0003 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_ActionCore_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_ActionCore_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Action_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Action_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_ContractCall_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_ContractCall_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_AddDeposit_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_AddDeposit_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_Create_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_Create_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_Reclaim_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_Reclaim_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_Restake_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_Restake_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Staking_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Staking_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_IoTeX_Proto_Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_IoTeX_Proto_Transfer_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.IoTeX$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase;
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase;
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase;

        static {
            int[] iArr = new int[ActionCore.ActionCase.values().length];
            $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase = iArr;
            try {
                iArr[ActionCore.ActionCase.TRANSFER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.EXECUTION.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.STAKECREATE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.STAKEUNSTAKE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.STAKEWITHDRAW.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.STAKEADDDEPOSIT.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.STAKERESTAKE.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.STAKECHANGECANDIDATE.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.STAKETRANSFEROWNERSHIP.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.CANDIDATEREGISTER.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.CANDIDATEUPDATE.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[ActionCore.ActionCase.ACTION_NOT_SET.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            int[] iArr2 = new int[SigningInput.ActionCase.values().length];
            $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase = iArr2;
            try {
                iArr2[SigningInput.ActionCase.TRANSFER.ordinal()] = 1;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.CALL.ordinal()] = 2;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.STAKECREATE.ordinal()] = 3;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.STAKEUNSTAKE.ordinal()] = 4;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.STAKEWITHDRAW.ordinal()] = 5;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.STAKEADDDEPOSIT.ordinal()] = 6;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.STAKERESTAKE.ordinal()] = 7;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.STAKECHANGECANDIDATE.ordinal()] = 8;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.STAKETRANSFEROWNERSHIP.ordinal()] = 9;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.CANDIDATEREGISTER.ordinal()] = 10;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.CANDIDATEUPDATE.ordinal()] = 11;
            } catch (NoSuchFieldError unused23) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[SigningInput.ActionCase.ACTION_NOT_SET.ordinal()] = 12;
            } catch (NoSuchFieldError unused24) {
            }
            int[] iArr3 = new int[Staking.MessageCase.values().length];
            $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase = iArr3;
            try {
                iArr3[Staking.MessageCase.STAKECREATE.ordinal()] = 1;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.STAKEUNSTAKE.ordinal()] = 2;
            } catch (NoSuchFieldError unused26) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.STAKEWITHDRAW.ordinal()] = 3;
            } catch (NoSuchFieldError unused27) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.STAKEADDDEPOSIT.ordinal()] = 4;
            } catch (NoSuchFieldError unused28) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.STAKERESTAKE.ordinal()] = 5;
            } catch (NoSuchFieldError unused29) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.STAKECHANGECANDIDATE.ordinal()] = 6;
            } catch (NoSuchFieldError unused30) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.STAKETRANSFEROWNERSHIP.ordinal()] = 7;
            } catch (NoSuchFieldError unused31) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.CANDIDATEREGISTER.ordinal()] = 8;
            } catch (NoSuchFieldError unused32) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.CANDIDATEUPDATE.ordinal()] = 9;
            } catch (NoSuchFieldError unused33) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[Staking.MessageCase.MESSAGE_NOT_SET.ordinal()] = 10;
            } catch (NoSuchFieldError unused34) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class Action extends GeneratedMessageV3 implements ActionOrBuilder {
        public static final int CORE_FIELD_NUMBER = 1;
        private static final Action DEFAULT_INSTANCE = new Action();
        private static final t0<Action> PARSER = new c<Action>() { // from class: wallet.core.jni.proto.IoTeX.Action.1
            @Override // com.google.protobuf.t0
            public Action parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Action(jVar, rVar, null);
            }
        };
        public static final int SENDERPUBKEY_FIELD_NUMBER = 2;
        public static final int SIGNATURE_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ActionCore core_;
        private byte memoizedIsInitialized;
        private ByteString senderPubKey_;
        private ByteString signature_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements ActionOrBuilder {
            private a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> coreBuilder_;
            private ActionCore core_;
            private ByteString senderPubKey_;
            private ByteString signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> getCoreFieldBuilder() {
                if (this.coreBuilder_ == null) {
                    this.coreBuilder_ = new a1<>(getCore(), getParentForChildren(), isClean());
                    this.core_ = null;
                }
                return this.coreBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Action_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCore() {
                if (this.coreBuilder_ == null) {
                    this.core_ = null;
                    onChanged();
                } else {
                    this.core_ = null;
                    this.coreBuilder_ = null;
                }
                return this;
            }

            public Builder clearSenderPubKey() {
                this.senderPubKey_ = Action.getDefaultInstance().getSenderPubKey();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = Action.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
            public ActionCore getCore() {
                a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> a1Var = this.coreBuilder_;
                if (a1Var == null) {
                    ActionCore actionCore = this.core_;
                    return actionCore == null ? ActionCore.getDefaultInstance() : actionCore;
                }
                return a1Var.f();
            }

            public ActionCore.Builder getCoreBuilder() {
                onChanged();
                return getCoreFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
            public ActionCoreOrBuilder getCoreOrBuilder() {
                a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> a1Var = this.coreBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                ActionCore actionCore = this.core_;
                return actionCore == null ? ActionCore.getDefaultInstance() : actionCore;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Action_descriptor;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
            public ByteString getSenderPubKey() {
                return this.senderPubKey_;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
            public boolean hasCore() {
                return (this.coreBuilder_ == null && this.core_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Action_fieldAccessorTable.d(Action.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCore(ActionCore actionCore) {
                a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> a1Var = this.coreBuilder_;
                if (a1Var == null) {
                    ActionCore actionCore2 = this.core_;
                    if (actionCore2 != null) {
                        this.core_ = ActionCore.newBuilder(actionCore2).mergeFrom(actionCore).buildPartial();
                    } else {
                        this.core_ = actionCore;
                    }
                    onChanged();
                } else {
                    a1Var.h(actionCore);
                }
                return this;
            }

            public Builder setCore(ActionCore actionCore) {
                a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> a1Var = this.coreBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(actionCore);
                    this.core_ = actionCore;
                    onChanged();
                } else {
                    a1Var.j(actionCore);
                }
                return this;
            }

            public Builder setSenderPubKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.senderPubKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.senderPubKey_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Action build() {
                Action buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Action buildPartial() {
                Action action = new Action(this, (AnonymousClass1) null);
                a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> a1Var = this.coreBuilder_;
                if (a1Var == null) {
                    action.core_ = this.core_;
                } else {
                    action.core_ = a1Var.b();
                }
                action.senderPubKey_ = this.senderPubKey_;
                action.signature_ = this.signature_;
                onBuilt();
                return action;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Action getDefaultInstanceForType() {
                return Action.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.coreBuilder_ == null) {
                    this.core_ = null;
                } else {
                    this.core_ = null;
                    this.coreBuilder_ = null;
                }
                ByteString byteString = ByteString.EMPTY;
                this.senderPubKey_ = byteString;
                this.signature_ = byteString;
                return this;
            }

            public Builder setCore(ActionCore.Builder builder) {
                a1<ActionCore, ActionCore.Builder, ActionCoreOrBuilder> a1Var = this.coreBuilder_;
                if (a1Var == null) {
                    this.core_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.senderPubKey_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Action) {
                    return mergeFrom((Action) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Action action) {
                if (action == Action.getDefaultInstance()) {
                    return this;
                }
                if (action.hasCore()) {
                    mergeCore(action.getCore());
                }
                ByteString senderPubKey = action.getSenderPubKey();
                ByteString byteString = ByteString.EMPTY;
                if (senderPubKey != byteString) {
                    setSenderPubKey(action.getSenderPubKey());
                }
                if (action.getSignature() != byteString) {
                    setSignature(action.getSignature());
                }
                mergeUnknownFields(action.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.IoTeX.Action.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Action.access$20400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.IoTeX$Action r3 = (wallet.core.jni.proto.IoTeX.Action) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.IoTeX$Action r4 = (wallet.core.jni.proto.IoTeX.Action) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Action.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Action$Builder");
            }
        }

        public /* synthetic */ Action(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Action getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return IoTeX.internal_static_TW_IoTeX_Proto_Action_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Action parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Action) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Action parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Action> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Action)) {
                return super.equals(obj);
            }
            Action action = (Action) obj;
            if (hasCore() != action.hasCore()) {
                return false;
            }
            return (!hasCore() || getCore().equals(action.getCore())) && getSenderPubKey().equals(action.getSenderPubKey()) && getSignature().equals(action.getSignature()) && this.unknownFields.equals(action.unknownFields);
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
        public ActionCore getCore() {
            ActionCore actionCore = this.core_;
            return actionCore == null ? ActionCore.getDefaultInstance() : actionCore;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
        public ActionCoreOrBuilder getCoreOrBuilder() {
            return getCore();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Action> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
        public ByteString getSenderPubKey() {
            return this.senderPubKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.core_ != null ? 0 + CodedOutputStream.G(1, getCore()) : 0;
            if (!this.senderPubKey_.isEmpty()) {
                G += CodedOutputStream.h(2, this.senderPubKey_);
            }
            if (!this.signature_.isEmpty()) {
                G += CodedOutputStream.h(3, this.signature_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionOrBuilder
        public boolean hasCore() {
            return this.core_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasCore()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getCore().hashCode();
            }
            int hashCode2 = (((((((((hashCode * 37) + 2) * 53) + getSenderPubKey().hashCode()) * 37) + 3) * 53) + getSignature().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return IoTeX.internal_static_TW_IoTeX_Proto_Action_fieldAccessorTable.d(Action.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Action();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.core_ != null) {
                codedOutputStream.K0(1, getCore());
            }
            if (!this.senderPubKey_.isEmpty()) {
                codedOutputStream.q0(2, this.senderPubKey_);
            }
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(3, this.signature_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Action(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Action action) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(action);
        }

        public static Action parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Action(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Action parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Action parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Action getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Action parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Action() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.senderPubKey_ = byteString;
            this.signature_ = byteString;
        }

        public static Action parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Action parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Action parseFrom(InputStream inputStream) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Action(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                ActionCore actionCore = this.core_;
                                ActionCore.Builder builder = actionCore != null ? actionCore.toBuilder() : null;
                                ActionCore actionCore2 = (ActionCore) jVar.z(ActionCore.parser(), rVar);
                                this.core_ = actionCore2;
                                if (builder != null) {
                                    builder.mergeFrom(actionCore2);
                                    this.core_ = builder.buildPartial();
                                }
                            } else if (J == 18) {
                                this.senderPubKey_ = jVar.q();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.signature_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Action parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Action parseFrom(j jVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Action parseFrom(j jVar, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public static final class ActionCore extends GeneratedMessageV3 implements ActionCoreOrBuilder {
        public static final int CANDIDATEREGISTER_FIELD_NUMBER = 47;
        public static final int CANDIDATEUPDATE_FIELD_NUMBER = 48;
        public static final int EXECUTION_FIELD_NUMBER = 12;
        public static final int GASLIMIT_FIELD_NUMBER = 3;
        public static final int GASPRICE_FIELD_NUMBER = 4;
        public static final int NONCE_FIELD_NUMBER = 2;
        public static final int STAKEADDDEPOSIT_FIELD_NUMBER = 43;
        public static final int STAKECHANGECANDIDATE_FIELD_NUMBER = 45;
        public static final int STAKECREATE_FIELD_NUMBER = 40;
        public static final int STAKERESTAKE_FIELD_NUMBER = 44;
        public static final int STAKETRANSFEROWNERSHIP_FIELD_NUMBER = 46;
        public static final int STAKEUNSTAKE_FIELD_NUMBER = 41;
        public static final int STAKEWITHDRAW_FIELD_NUMBER = 42;
        public static final int TRANSFER_FIELD_NUMBER = 10;
        public static final int VERSION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private int actionCase_;
        private Object action_;
        private long gasLimit_;
        private volatile Object gasPrice_;
        private byte memoizedIsInitialized;
        private long nonce_;
        private int version_;
        private static final ActionCore DEFAULT_INSTANCE = new ActionCore();
        private static final t0<ActionCore> PARSER = new c<ActionCore>() { // from class: wallet.core.jni.proto.IoTeX.ActionCore.1
            @Override // com.google.protobuf.t0
            public ActionCore parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new ActionCore(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public enum ActionCase implements a0.c {
            TRANSFER(10),
            EXECUTION(12),
            STAKECREATE(40),
            STAKEUNSTAKE(41),
            STAKEWITHDRAW(42),
            STAKEADDDEPOSIT(43),
            STAKERESTAKE(44),
            STAKECHANGECANDIDATE(45),
            STAKETRANSFEROWNERSHIP(46),
            CANDIDATEREGISTER(47),
            CANDIDATEUPDATE(48),
            ACTION_NOT_SET(0);
            
            private final int value;

            ActionCase(int i) {
                this.value = i;
            }

            public static ActionCase forNumber(int i) {
                if (i != 0) {
                    if (i != 10) {
                        if (i != 12) {
                            switch (i) {
                                case 40:
                                    return STAKECREATE;
                                case 41:
                                    return STAKEUNSTAKE;
                                case 42:
                                    return STAKEWITHDRAW;
                                case 43:
                                    return STAKEADDDEPOSIT;
                                case 44:
                                    return STAKERESTAKE;
                                case 45:
                                    return STAKECHANGECANDIDATE;
                                case 46:
                                    return STAKETRANSFEROWNERSHIP;
                                case 47:
                                    return CANDIDATEREGISTER;
                                case 48:
                                    return CANDIDATEUPDATE;
                                default:
                                    return null;
                            }
                        }
                        return EXECUTION;
                    }
                    return TRANSFER;
                }
                return ACTION_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static ActionCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements ActionCoreOrBuilder {
            private int actionCase_;
            private Object action_;
            private a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> candidateRegisterBuilder_;
            private a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> candidateUpdateBuilder_;
            private a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> executionBuilder_;
            private long gasLimit_;
            private Object gasPrice_;
            private long nonce_;
            private a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> stakeAddDepositBuilder_;
            private a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> stakeChangeCandidateBuilder_;
            private a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> stakeCreateBuilder_;
            private a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> stakeRestakeBuilder_;
            private a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> stakeTransferOwnershipBuilder_;
            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> stakeUnstakeBuilder_;
            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> stakeWithdrawBuilder_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferBuilder_;
            private int version_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> getCandidateRegisterFieldBuilder() {
                if (this.candidateRegisterBuilder_ == null) {
                    if (this.actionCase_ != 47) {
                        this.action_ = Staking.CandidateRegister.getDefaultInstance();
                    }
                    this.candidateRegisterBuilder_ = new a1<>((Staking.CandidateRegister) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 47;
                onChanged();
                return this.candidateRegisterBuilder_;
            }

            private a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> getCandidateUpdateFieldBuilder() {
                if (this.candidateUpdateBuilder_ == null) {
                    if (this.actionCase_ != 48) {
                        this.action_ = Staking.CandidateBasicInfo.getDefaultInstance();
                    }
                    this.candidateUpdateBuilder_ = new a1<>((Staking.CandidateBasicInfo) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 48;
                onChanged();
                return this.candidateUpdateBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_ActionCore_descriptor;
            }

            private a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> getExecutionFieldBuilder() {
                if (this.executionBuilder_ == null) {
                    if (this.actionCase_ != 12) {
                        this.action_ = ContractCall.getDefaultInstance();
                    }
                    this.executionBuilder_ = new a1<>((ContractCall) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 12;
                onChanged();
                return this.executionBuilder_;
            }

            private a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> getStakeAddDepositFieldBuilder() {
                if (this.stakeAddDepositBuilder_ == null) {
                    if (this.actionCase_ != 43) {
                        this.action_ = Staking.AddDeposit.getDefaultInstance();
                    }
                    this.stakeAddDepositBuilder_ = new a1<>((Staking.AddDeposit) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 43;
                onChanged();
                return this.stakeAddDepositBuilder_;
            }

            private a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> getStakeChangeCandidateFieldBuilder() {
                if (this.stakeChangeCandidateBuilder_ == null) {
                    if (this.actionCase_ != 45) {
                        this.action_ = Staking.ChangeCandidate.getDefaultInstance();
                    }
                    this.stakeChangeCandidateBuilder_ = new a1<>((Staking.ChangeCandidate) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 45;
                onChanged();
                return this.stakeChangeCandidateBuilder_;
            }

            private a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> getStakeCreateFieldBuilder() {
                if (this.stakeCreateBuilder_ == null) {
                    if (this.actionCase_ != 40) {
                        this.action_ = Staking.Create.getDefaultInstance();
                    }
                    this.stakeCreateBuilder_ = new a1<>((Staking.Create) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 40;
                onChanged();
                return this.stakeCreateBuilder_;
            }

            private a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> getStakeRestakeFieldBuilder() {
                if (this.stakeRestakeBuilder_ == null) {
                    if (this.actionCase_ != 44) {
                        this.action_ = Staking.Restake.getDefaultInstance();
                    }
                    this.stakeRestakeBuilder_ = new a1<>((Staking.Restake) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 44;
                onChanged();
                return this.stakeRestakeBuilder_;
            }

            private a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> getStakeTransferOwnershipFieldBuilder() {
                if (this.stakeTransferOwnershipBuilder_ == null) {
                    if (this.actionCase_ != 46) {
                        this.action_ = Staking.TransferOwnership.getDefaultInstance();
                    }
                    this.stakeTransferOwnershipBuilder_ = new a1<>((Staking.TransferOwnership) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 46;
                onChanged();
                return this.stakeTransferOwnershipBuilder_;
            }

            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> getStakeUnstakeFieldBuilder() {
                if (this.stakeUnstakeBuilder_ == null) {
                    if (this.actionCase_ != 41) {
                        this.action_ = Staking.Reclaim.getDefaultInstance();
                    }
                    this.stakeUnstakeBuilder_ = new a1<>((Staking.Reclaim) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 41;
                onChanged();
                return this.stakeUnstakeBuilder_;
            }

            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> getStakeWithdrawFieldBuilder() {
                if (this.stakeWithdrawBuilder_ == null) {
                    if (this.actionCase_ != 42) {
                        this.action_ = Staking.Reclaim.getDefaultInstance();
                    }
                    this.stakeWithdrawBuilder_ = new a1<>((Staking.Reclaim) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 42;
                onChanged();
                return this.stakeWithdrawBuilder_;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferFieldBuilder() {
                if (this.transferBuilder_ == null) {
                    if (this.actionCase_ != 10) {
                        this.action_ = Transfer.getDefaultInstance();
                    }
                    this.transferBuilder_ = new a1<>((Transfer) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 10;
                onChanged();
                return this.transferBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAction() {
                this.actionCase_ = 0;
                this.action_ = null;
                onChanged();
                return this;
            }

            public Builder clearCandidateRegister() {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 47) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 47) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearCandidateUpdate() {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 48) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 48) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearExecution() {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.executionBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 12) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 12) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = ActionCore.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearStakeAddDeposit() {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 43) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 43) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeChangeCandidate() {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 45) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 45) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeCreate() {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 40) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 40) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeRestake() {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 44) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 44) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeTransferOwnership() {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 46) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 46) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeUnstake() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 41) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 41) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeWithdraw() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 42) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 42) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 10) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 10) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public ActionCase getActionCase() {
                return ActionCase.forNumber(this.actionCase_);
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.CandidateRegister getCandidateRegister() {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 47) {
                        return (Staking.CandidateRegister) this.action_;
                    }
                    return Staking.CandidateRegister.getDefaultInstance();
                } else if (this.actionCase_ == 47) {
                    return a1Var.f();
                } else {
                    return Staking.CandidateRegister.getDefaultInstance();
                }
            }

            public Staking.CandidateRegister.Builder getCandidateRegisterBuilder() {
                return getCandidateRegisterFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.CandidateRegisterOrBuilder getCandidateRegisterOrBuilder() {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 47 || (a1Var = this.candidateRegisterBuilder_) == null) {
                    if (i == 47) {
                        return (Staking.CandidateRegister) this.action_;
                    }
                    return Staking.CandidateRegister.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.CandidateBasicInfo getCandidateUpdate() {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 48) {
                        return (Staking.CandidateBasicInfo) this.action_;
                    }
                    return Staking.CandidateBasicInfo.getDefaultInstance();
                } else if (this.actionCase_ == 48) {
                    return a1Var.f();
                } else {
                    return Staking.CandidateBasicInfo.getDefaultInstance();
                }
            }

            public Staking.CandidateBasicInfo.Builder getCandidateUpdateBuilder() {
                return getCandidateUpdateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder() {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 48 || (a1Var = this.candidateUpdateBuilder_) == null) {
                    if (i == 48) {
                        return (Staking.CandidateBasicInfo) this.action_;
                    }
                    return Staking.CandidateBasicInfo.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return IoTeX.internal_static_TW_IoTeX_Proto_ActionCore_descriptor;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public ContractCall getExecution() {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.executionBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 12) {
                        return (ContractCall) this.action_;
                    }
                    return ContractCall.getDefaultInstance();
                } else if (this.actionCase_ == 12) {
                    return a1Var.f();
                } else {
                    return ContractCall.getDefaultInstance();
                }
            }

            public ContractCall.Builder getExecutionBuilder() {
                return getExecutionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public ContractCallOrBuilder getExecutionOrBuilder() {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 12 || (a1Var = this.executionBuilder_) == null) {
                    if (i == 12) {
                        return (ContractCall) this.action_;
                    }
                    return ContractCall.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public long getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public String getGasPrice() {
                Object obj = this.gasPrice_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.gasPrice_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public ByteString getGasPriceBytes() {
                Object obj = this.gasPrice_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.gasPrice_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.AddDeposit getStakeAddDeposit() {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 43) {
                        return (Staking.AddDeposit) this.action_;
                    }
                    return Staking.AddDeposit.getDefaultInstance();
                } else if (this.actionCase_ == 43) {
                    return a1Var.f();
                } else {
                    return Staking.AddDeposit.getDefaultInstance();
                }
            }

            public Staking.AddDeposit.Builder getStakeAddDepositBuilder() {
                return getStakeAddDepositFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.AddDepositOrBuilder getStakeAddDepositOrBuilder() {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 43 || (a1Var = this.stakeAddDepositBuilder_) == null) {
                    if (i == 43) {
                        return (Staking.AddDeposit) this.action_;
                    }
                    return Staking.AddDeposit.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.ChangeCandidate getStakeChangeCandidate() {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 45) {
                        return (Staking.ChangeCandidate) this.action_;
                    }
                    return Staking.ChangeCandidate.getDefaultInstance();
                } else if (this.actionCase_ == 45) {
                    return a1Var.f();
                } else {
                    return Staking.ChangeCandidate.getDefaultInstance();
                }
            }

            public Staking.ChangeCandidate.Builder getStakeChangeCandidateBuilder() {
                return getStakeChangeCandidateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder() {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 45 || (a1Var = this.stakeChangeCandidateBuilder_) == null) {
                    if (i == 45) {
                        return (Staking.ChangeCandidate) this.action_;
                    }
                    return Staking.ChangeCandidate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.Create getStakeCreate() {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 40) {
                        return (Staking.Create) this.action_;
                    }
                    return Staking.Create.getDefaultInstance();
                } else if (this.actionCase_ == 40) {
                    return a1Var.f();
                } else {
                    return Staking.Create.getDefaultInstance();
                }
            }

            public Staking.Create.Builder getStakeCreateBuilder() {
                return getStakeCreateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.CreateOrBuilder getStakeCreateOrBuilder() {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 40 || (a1Var = this.stakeCreateBuilder_) == null) {
                    if (i == 40) {
                        return (Staking.Create) this.action_;
                    }
                    return Staking.Create.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.Restake getStakeRestake() {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 44) {
                        return (Staking.Restake) this.action_;
                    }
                    return Staking.Restake.getDefaultInstance();
                } else if (this.actionCase_ == 44) {
                    return a1Var.f();
                } else {
                    return Staking.Restake.getDefaultInstance();
                }
            }

            public Staking.Restake.Builder getStakeRestakeBuilder() {
                return getStakeRestakeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.RestakeOrBuilder getStakeRestakeOrBuilder() {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 44 || (a1Var = this.stakeRestakeBuilder_) == null) {
                    if (i == 44) {
                        return (Staking.Restake) this.action_;
                    }
                    return Staking.Restake.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.TransferOwnership getStakeTransferOwnership() {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 46) {
                        return (Staking.TransferOwnership) this.action_;
                    }
                    return Staking.TransferOwnership.getDefaultInstance();
                } else if (this.actionCase_ == 46) {
                    return a1Var.f();
                } else {
                    return Staking.TransferOwnership.getDefaultInstance();
                }
            }

            public Staking.TransferOwnership.Builder getStakeTransferOwnershipBuilder() {
                return getStakeTransferOwnershipFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder() {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 46 || (a1Var = this.stakeTransferOwnershipBuilder_) == null) {
                    if (i == 46) {
                        return (Staking.TransferOwnership) this.action_;
                    }
                    return Staking.TransferOwnership.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.Reclaim getStakeUnstake() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 41) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                } else if (this.actionCase_ == 41) {
                    return a1Var.f();
                } else {
                    return Staking.Reclaim.getDefaultInstance();
                }
            }

            public Staking.Reclaim.Builder getStakeUnstakeBuilder() {
                return getStakeUnstakeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.ReclaimOrBuilder getStakeUnstakeOrBuilder() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 41 || (a1Var = this.stakeUnstakeBuilder_) == null) {
                    if (i == 41) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.Reclaim getStakeWithdraw() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 42) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                } else if (this.actionCase_ == 42) {
                    return a1Var.f();
                } else {
                    return Staking.Reclaim.getDefaultInstance();
                }
            }

            public Staking.Reclaim.Builder getStakeWithdrawBuilder() {
                return getStakeWithdrawFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Staking.ReclaimOrBuilder getStakeWithdrawOrBuilder() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 42 || (a1Var = this.stakeWithdrawBuilder_) == null) {
                    if (i == 42) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public Transfer getTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 10) {
                        return (Transfer) this.action_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.actionCase_ == 10) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferBuilder() {
                return getTransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public TransferOrBuilder getTransferOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 10 || (a1Var = this.transferBuilder_) == null) {
                    if (i == 10) {
                        return (Transfer) this.action_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasCandidateRegister() {
                return this.actionCase_ == 47;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasCandidateUpdate() {
                return this.actionCase_ == 48;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasExecution() {
                return this.actionCase_ == 12;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasStakeAddDeposit() {
                return this.actionCase_ == 43;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasStakeChangeCandidate() {
                return this.actionCase_ == 45;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasStakeCreate() {
                return this.actionCase_ == 40;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasStakeRestake() {
                return this.actionCase_ == 44;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasStakeTransferOwnership() {
                return this.actionCase_ == 46;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasStakeUnstake() {
                return this.actionCase_ == 41;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasStakeWithdraw() {
                return this.actionCase_ == 42;
            }

            @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
            public boolean hasTransfer() {
                return this.actionCase_ == 10;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_ActionCore_fieldAccessorTable.d(ActionCore.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCandidateRegister(Staking.CandidateRegister candidateRegister) {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 47 && this.action_ != Staking.CandidateRegister.getDefaultInstance()) {
                        this.action_ = Staking.CandidateRegister.newBuilder((Staking.CandidateRegister) this.action_).mergeFrom(candidateRegister).buildPartial();
                    } else {
                        this.action_ = candidateRegister;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 47) {
                        a1Var.h(candidateRegister);
                    }
                    this.candidateRegisterBuilder_.j(candidateRegister);
                }
                this.actionCase_ = 47;
                return this;
            }

            public Builder mergeCandidateUpdate(Staking.CandidateBasicInfo candidateBasicInfo) {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 48 && this.action_ != Staking.CandidateBasicInfo.getDefaultInstance()) {
                        this.action_ = Staking.CandidateBasicInfo.newBuilder((Staking.CandidateBasicInfo) this.action_).mergeFrom(candidateBasicInfo).buildPartial();
                    } else {
                        this.action_ = candidateBasicInfo;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 48) {
                        a1Var.h(candidateBasicInfo);
                    }
                    this.candidateUpdateBuilder_.j(candidateBasicInfo);
                }
                this.actionCase_ = 48;
                return this;
            }

            public Builder mergeExecution(ContractCall contractCall) {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.executionBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 12 && this.action_ != ContractCall.getDefaultInstance()) {
                        this.action_ = ContractCall.newBuilder((ContractCall) this.action_).mergeFrom(contractCall).buildPartial();
                    } else {
                        this.action_ = contractCall;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 12) {
                        a1Var.h(contractCall);
                    }
                    this.executionBuilder_.j(contractCall);
                }
                this.actionCase_ = 12;
                return this;
            }

            public Builder mergeStakeAddDeposit(Staking.AddDeposit addDeposit) {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 43 && this.action_ != Staking.AddDeposit.getDefaultInstance()) {
                        this.action_ = Staking.AddDeposit.newBuilder((Staking.AddDeposit) this.action_).mergeFrom(addDeposit).buildPartial();
                    } else {
                        this.action_ = addDeposit;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 43) {
                        a1Var.h(addDeposit);
                    }
                    this.stakeAddDepositBuilder_.j(addDeposit);
                }
                this.actionCase_ = 43;
                return this;
            }

            public Builder mergeStakeChangeCandidate(Staking.ChangeCandidate changeCandidate) {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 45 && this.action_ != Staking.ChangeCandidate.getDefaultInstance()) {
                        this.action_ = Staking.ChangeCandidate.newBuilder((Staking.ChangeCandidate) this.action_).mergeFrom(changeCandidate).buildPartial();
                    } else {
                        this.action_ = changeCandidate;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 45) {
                        a1Var.h(changeCandidate);
                    }
                    this.stakeChangeCandidateBuilder_.j(changeCandidate);
                }
                this.actionCase_ = 45;
                return this;
            }

            public Builder mergeStakeCreate(Staking.Create create) {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 40 && this.action_ != Staking.Create.getDefaultInstance()) {
                        this.action_ = Staking.Create.newBuilder((Staking.Create) this.action_).mergeFrom(create).buildPartial();
                    } else {
                        this.action_ = create;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 40) {
                        a1Var.h(create);
                    }
                    this.stakeCreateBuilder_.j(create);
                }
                this.actionCase_ = 40;
                return this;
            }

            public Builder mergeStakeRestake(Staking.Restake restake) {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 44 && this.action_ != Staking.Restake.getDefaultInstance()) {
                        this.action_ = Staking.Restake.newBuilder((Staking.Restake) this.action_).mergeFrom(restake).buildPartial();
                    } else {
                        this.action_ = restake;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 44) {
                        a1Var.h(restake);
                    }
                    this.stakeRestakeBuilder_.j(restake);
                }
                this.actionCase_ = 44;
                return this;
            }

            public Builder mergeStakeTransferOwnership(Staking.TransferOwnership transferOwnership) {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 46 && this.action_ != Staking.TransferOwnership.getDefaultInstance()) {
                        this.action_ = Staking.TransferOwnership.newBuilder((Staking.TransferOwnership) this.action_).mergeFrom(transferOwnership).buildPartial();
                    } else {
                        this.action_ = transferOwnership;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 46) {
                        a1Var.h(transferOwnership);
                    }
                    this.stakeTransferOwnershipBuilder_.j(transferOwnership);
                }
                this.actionCase_ = 46;
                return this;
            }

            public Builder mergeStakeUnstake(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 41 && this.action_ != Staking.Reclaim.getDefaultInstance()) {
                        this.action_ = Staking.Reclaim.newBuilder((Staking.Reclaim) this.action_).mergeFrom(reclaim).buildPartial();
                    } else {
                        this.action_ = reclaim;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 41) {
                        a1Var.h(reclaim);
                    }
                    this.stakeUnstakeBuilder_.j(reclaim);
                }
                this.actionCase_ = 41;
                return this;
            }

            public Builder mergeStakeWithdraw(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 42 && this.action_ != Staking.Reclaim.getDefaultInstance()) {
                        this.action_ = Staking.Reclaim.newBuilder((Staking.Reclaim) this.action_).mergeFrom(reclaim).buildPartial();
                    } else {
                        this.action_ = reclaim;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 42) {
                        a1Var.h(reclaim);
                    }
                    this.stakeWithdrawBuilder_.j(reclaim);
                }
                this.actionCase_ = 42;
                return this;
            }

            public Builder mergeTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 10 && this.action_ != Transfer.getDefaultInstance()) {
                        this.action_ = Transfer.newBuilder((Transfer) this.action_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.action_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 10) {
                        a1Var.h(transfer);
                    }
                    this.transferBuilder_.j(transfer);
                }
                this.actionCase_ = 10;
                return this;
            }

            public Builder setCandidateRegister(Staking.CandidateRegister candidateRegister) {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(candidateRegister);
                    this.action_ = candidateRegister;
                    onChanged();
                } else {
                    a1Var.j(candidateRegister);
                }
                this.actionCase_ = 47;
                return this;
            }

            public Builder setCandidateUpdate(Staking.CandidateBasicInfo candidateBasicInfo) {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(candidateBasicInfo);
                    this.action_ = candidateBasicInfo;
                    onChanged();
                } else {
                    a1Var.j(candidateBasicInfo);
                }
                this.actionCase_ = 48;
                return this;
            }

            public Builder setExecution(ContractCall contractCall) {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.executionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(contractCall);
                    this.action_ = contractCall;
                    onChanged();
                } else {
                    a1Var.j(contractCall);
                }
                this.actionCase_ = 12;
                return this;
            }

            public Builder setGasLimit(long j) {
                this.gasLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setGasPrice(String str) {
                Objects.requireNonNull(str);
                this.gasPrice_ = str;
                onChanged();
                return this;
            }

            public Builder setGasPriceBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setStakeAddDeposit(Staking.AddDeposit addDeposit) {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(addDeposit);
                    this.action_ = addDeposit;
                    onChanged();
                } else {
                    a1Var.j(addDeposit);
                }
                this.actionCase_ = 43;
                return this;
            }

            public Builder setStakeChangeCandidate(Staking.ChangeCandidate changeCandidate) {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(changeCandidate);
                    this.action_ = changeCandidate;
                    onChanged();
                } else {
                    a1Var.j(changeCandidate);
                }
                this.actionCase_ = 45;
                return this;
            }

            public Builder setStakeCreate(Staking.Create create) {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(create);
                    this.action_ = create;
                    onChanged();
                } else {
                    a1Var.j(create);
                }
                this.actionCase_ = 40;
                return this;
            }

            public Builder setStakeRestake(Staking.Restake restake) {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(restake);
                    this.action_ = restake;
                    onChanged();
                } else {
                    a1Var.j(restake);
                }
                this.actionCase_ = 44;
                return this;
            }

            public Builder setStakeTransferOwnership(Staking.TransferOwnership transferOwnership) {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transferOwnership);
                    this.action_ = transferOwnership;
                    onChanged();
                } else {
                    a1Var.j(transferOwnership);
                }
                this.actionCase_ = 46;
                return this;
            }

            public Builder setStakeUnstake(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(reclaim);
                    this.action_ = reclaim;
                    onChanged();
                } else {
                    a1Var.j(reclaim);
                }
                this.actionCase_ = 41;
                return this;
            }

            public Builder setStakeWithdraw(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(reclaim);
                    this.action_ = reclaim;
                    onChanged();
                } else {
                    a1Var.j(reclaim);
                }
                this.actionCase_ = 42;
                return this;
            }

            public Builder setTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.action_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.actionCase_ = 10;
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.actionCase_ = 0;
                this.gasPrice_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public ActionCore build() {
                ActionCore buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public ActionCore buildPartial() {
                ActionCore actionCore = new ActionCore(this, (AnonymousClass1) null);
                actionCore.version_ = this.version_;
                actionCore.nonce_ = this.nonce_;
                actionCore.gasLimit_ = this.gasLimit_;
                actionCore.gasPrice_ = this.gasPrice_;
                if (this.actionCase_ == 10) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                    if (a1Var == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var.b();
                    }
                }
                if (this.actionCase_ == 12) {
                    a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var2 = this.executionBuilder_;
                    if (a1Var2 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var2.b();
                    }
                }
                if (this.actionCase_ == 40) {
                    a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var3 = this.stakeCreateBuilder_;
                    if (a1Var3 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var3.b();
                    }
                }
                if (this.actionCase_ == 41) {
                    a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var4 = this.stakeUnstakeBuilder_;
                    if (a1Var4 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var4.b();
                    }
                }
                if (this.actionCase_ == 42) {
                    a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var5 = this.stakeWithdrawBuilder_;
                    if (a1Var5 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var5.b();
                    }
                }
                if (this.actionCase_ == 43) {
                    a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var6 = this.stakeAddDepositBuilder_;
                    if (a1Var6 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var6.b();
                    }
                }
                if (this.actionCase_ == 44) {
                    a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var7 = this.stakeRestakeBuilder_;
                    if (a1Var7 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var7.b();
                    }
                }
                if (this.actionCase_ == 45) {
                    a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var8 = this.stakeChangeCandidateBuilder_;
                    if (a1Var8 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var8.b();
                    }
                }
                if (this.actionCase_ == 46) {
                    a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var9 = this.stakeTransferOwnershipBuilder_;
                    if (a1Var9 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var9.b();
                    }
                }
                if (this.actionCase_ == 47) {
                    a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var10 = this.candidateRegisterBuilder_;
                    if (a1Var10 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var10.b();
                    }
                }
                if (this.actionCase_ == 48) {
                    a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var11 = this.candidateUpdateBuilder_;
                    if (a1Var11 == null) {
                        actionCore.action_ = this.action_;
                    } else {
                        actionCore.action_ = a1Var11.b();
                    }
                }
                actionCore.actionCase_ = this.actionCase_;
                onBuilt();
                return actionCore;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public ActionCore getDefaultInstanceForType() {
                return ActionCore.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.version_ = 0;
                this.nonce_ = 0L;
                this.gasLimit_ = 0L;
                this.gasPrice_ = "";
                this.actionCase_ = 0;
                this.action_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.actionCase_ = 0;
                this.gasPrice_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof ActionCore) {
                    return mergeFrom((ActionCore) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setCandidateRegister(Staking.CandidateRegister.Builder builder) {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 47;
                return this;
            }

            public Builder setCandidateUpdate(Staking.CandidateBasicInfo.Builder builder) {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 48;
                return this;
            }

            public Builder setExecution(ContractCall.Builder builder) {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.executionBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 12;
                return this;
            }

            public Builder setStakeAddDeposit(Staking.AddDeposit.Builder builder) {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 43;
                return this;
            }

            public Builder setStakeChangeCandidate(Staking.ChangeCandidate.Builder builder) {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 45;
                return this;
            }

            public Builder setStakeCreate(Staking.Create.Builder builder) {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 40;
                return this;
            }

            public Builder setStakeRestake(Staking.Restake.Builder builder) {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 44;
                return this;
            }

            public Builder setStakeTransferOwnership(Staking.TransferOwnership.Builder builder) {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 46;
                return this;
            }

            public Builder setStakeUnstake(Staking.Reclaim.Builder builder) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 41;
                return this;
            }

            public Builder setStakeWithdraw(Staking.Reclaim.Builder builder) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 42;
                return this;
            }

            public Builder setTransfer(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 10;
                return this;
            }

            public Builder mergeFrom(ActionCore actionCore) {
                if (actionCore == ActionCore.getDefaultInstance()) {
                    return this;
                }
                if (actionCore.getVersion() != 0) {
                    setVersion(actionCore.getVersion());
                }
                if (actionCore.getNonce() != 0) {
                    setNonce(actionCore.getNonce());
                }
                if (actionCore.getGasLimit() != 0) {
                    setGasLimit(actionCore.getGasLimit());
                }
                if (!actionCore.getGasPrice().isEmpty()) {
                    this.gasPrice_ = actionCore.gasPrice_;
                    onChanged();
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$IoTeX$ActionCore$ActionCase[actionCore.getActionCase().ordinal()]) {
                    case 1:
                        mergeTransfer(actionCore.getTransfer());
                        break;
                    case 2:
                        mergeExecution(actionCore.getExecution());
                        break;
                    case 3:
                        mergeStakeCreate(actionCore.getStakeCreate());
                        break;
                    case 4:
                        mergeStakeUnstake(actionCore.getStakeUnstake());
                        break;
                    case 5:
                        mergeStakeWithdraw(actionCore.getStakeWithdraw());
                        break;
                    case 6:
                        mergeStakeAddDeposit(actionCore.getStakeAddDeposit());
                        break;
                    case 7:
                        mergeStakeRestake(actionCore.getStakeRestake());
                        break;
                    case 8:
                        mergeStakeChangeCandidate(actionCore.getStakeChangeCandidate());
                        break;
                    case 9:
                        mergeStakeTransferOwnership(actionCore.getStakeTransferOwnership());
                        break;
                    case 10:
                        mergeCandidateRegister(actionCore.getCandidateRegister());
                        break;
                    case 11:
                        mergeCandidateUpdate(actionCore.getCandidateUpdate());
                        break;
                }
                mergeUnknownFields(actionCore.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.IoTeX.ActionCore.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.ActionCore.access$19100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.IoTeX$ActionCore r3 = (wallet.core.jni.proto.IoTeX.ActionCore) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.IoTeX$ActionCore r4 = (wallet.core.jni.proto.IoTeX.ActionCore) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.ActionCore.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$ActionCore$Builder");
            }
        }

        public /* synthetic */ ActionCore(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static ActionCore getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return IoTeX.internal_static_TW_IoTeX_Proto_ActionCore_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static ActionCore parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ActionCore) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static ActionCore parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<ActionCore> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ActionCore)) {
                return super.equals(obj);
            }
            ActionCore actionCore = (ActionCore) obj;
            if (getVersion() == actionCore.getVersion() && getNonce() == actionCore.getNonce() && getGasLimit() == actionCore.getGasLimit() && getGasPrice().equals(actionCore.getGasPrice()) && getActionCase().equals(actionCore.getActionCase())) {
                int i = this.actionCase_;
                if (i != 10) {
                    if (i != 12) {
                        switch (i) {
                            case 40:
                                if (!getStakeCreate().equals(actionCore.getStakeCreate())) {
                                    return false;
                                }
                                break;
                            case 41:
                                if (!getStakeUnstake().equals(actionCore.getStakeUnstake())) {
                                    return false;
                                }
                                break;
                            case 42:
                                if (!getStakeWithdraw().equals(actionCore.getStakeWithdraw())) {
                                    return false;
                                }
                                break;
                            case 43:
                                if (!getStakeAddDeposit().equals(actionCore.getStakeAddDeposit())) {
                                    return false;
                                }
                                break;
                            case 44:
                                if (!getStakeRestake().equals(actionCore.getStakeRestake())) {
                                    return false;
                                }
                                break;
                            case 45:
                                if (!getStakeChangeCandidate().equals(actionCore.getStakeChangeCandidate())) {
                                    return false;
                                }
                                break;
                            case 46:
                                if (!getStakeTransferOwnership().equals(actionCore.getStakeTransferOwnership())) {
                                    return false;
                                }
                                break;
                            case 47:
                                if (!getCandidateRegister().equals(actionCore.getCandidateRegister())) {
                                    return false;
                                }
                                break;
                            case 48:
                                if (!getCandidateUpdate().equals(actionCore.getCandidateUpdate())) {
                                    return false;
                                }
                                break;
                        }
                    } else if (!getExecution().equals(actionCore.getExecution())) {
                        return false;
                    }
                } else if (!getTransfer().equals(actionCore.getTransfer())) {
                    return false;
                }
                return this.unknownFields.equals(actionCore.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public ActionCase getActionCase() {
            return ActionCase.forNumber(this.actionCase_);
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.CandidateRegister getCandidateRegister() {
            if (this.actionCase_ == 47) {
                return (Staking.CandidateRegister) this.action_;
            }
            return Staking.CandidateRegister.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.CandidateRegisterOrBuilder getCandidateRegisterOrBuilder() {
            if (this.actionCase_ == 47) {
                return (Staking.CandidateRegister) this.action_;
            }
            return Staking.CandidateRegister.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.CandidateBasicInfo getCandidateUpdate() {
            if (this.actionCase_ == 48) {
                return (Staking.CandidateBasicInfo) this.action_;
            }
            return Staking.CandidateBasicInfo.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder() {
            if (this.actionCase_ == 48) {
                return (Staking.CandidateBasicInfo) this.action_;
            }
            return Staking.CandidateBasicInfo.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public ContractCall getExecution() {
            if (this.actionCase_ == 12) {
                return (ContractCall) this.action_;
            }
            return ContractCall.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public ContractCallOrBuilder getExecutionOrBuilder() {
            if (this.actionCase_ == 12) {
                return (ContractCall) this.action_;
            }
            return ContractCall.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public long getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public String getGasPrice() {
            Object obj = this.gasPrice_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.gasPrice_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public ByteString getGasPriceBytes() {
            Object obj = this.gasPrice_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.gasPrice_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<ActionCore> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.version_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            long j = this.nonce_;
            if (j != 0) {
                Y += CodedOutputStream.a0(2, j);
            }
            long j2 = this.gasLimit_;
            if (j2 != 0) {
                Y += CodedOutputStream.a0(3, j2);
            }
            if (!getGasPriceBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(4, this.gasPrice_);
            }
            if (this.actionCase_ == 10) {
                Y += CodedOutputStream.G(10, (Transfer) this.action_);
            }
            if (this.actionCase_ == 12) {
                Y += CodedOutputStream.G(12, (ContractCall) this.action_);
            }
            if (this.actionCase_ == 40) {
                Y += CodedOutputStream.G(40, (Staking.Create) this.action_);
            }
            if (this.actionCase_ == 41) {
                Y += CodedOutputStream.G(41, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 42) {
                Y += CodedOutputStream.G(42, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 43) {
                Y += CodedOutputStream.G(43, (Staking.AddDeposit) this.action_);
            }
            if (this.actionCase_ == 44) {
                Y += CodedOutputStream.G(44, (Staking.Restake) this.action_);
            }
            if (this.actionCase_ == 45) {
                Y += CodedOutputStream.G(45, (Staking.ChangeCandidate) this.action_);
            }
            if (this.actionCase_ == 46) {
                Y += CodedOutputStream.G(46, (Staking.TransferOwnership) this.action_);
            }
            if (this.actionCase_ == 47) {
                Y += CodedOutputStream.G(47, (Staking.CandidateRegister) this.action_);
            }
            if (this.actionCase_ == 48) {
                Y += CodedOutputStream.G(48, (Staking.CandidateBasicInfo) this.action_);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.AddDeposit getStakeAddDeposit() {
            if (this.actionCase_ == 43) {
                return (Staking.AddDeposit) this.action_;
            }
            return Staking.AddDeposit.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.AddDepositOrBuilder getStakeAddDepositOrBuilder() {
            if (this.actionCase_ == 43) {
                return (Staking.AddDeposit) this.action_;
            }
            return Staking.AddDeposit.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.ChangeCandidate getStakeChangeCandidate() {
            if (this.actionCase_ == 45) {
                return (Staking.ChangeCandidate) this.action_;
            }
            return Staking.ChangeCandidate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder() {
            if (this.actionCase_ == 45) {
                return (Staking.ChangeCandidate) this.action_;
            }
            return Staking.ChangeCandidate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.Create getStakeCreate() {
            if (this.actionCase_ == 40) {
                return (Staking.Create) this.action_;
            }
            return Staking.Create.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.CreateOrBuilder getStakeCreateOrBuilder() {
            if (this.actionCase_ == 40) {
                return (Staking.Create) this.action_;
            }
            return Staking.Create.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.Restake getStakeRestake() {
            if (this.actionCase_ == 44) {
                return (Staking.Restake) this.action_;
            }
            return Staking.Restake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.RestakeOrBuilder getStakeRestakeOrBuilder() {
            if (this.actionCase_ == 44) {
                return (Staking.Restake) this.action_;
            }
            return Staking.Restake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.TransferOwnership getStakeTransferOwnership() {
            if (this.actionCase_ == 46) {
                return (Staking.TransferOwnership) this.action_;
            }
            return Staking.TransferOwnership.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder() {
            if (this.actionCase_ == 46) {
                return (Staking.TransferOwnership) this.action_;
            }
            return Staking.TransferOwnership.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.Reclaim getStakeUnstake() {
            if (this.actionCase_ == 41) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.ReclaimOrBuilder getStakeUnstakeOrBuilder() {
            if (this.actionCase_ == 41) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.Reclaim getStakeWithdraw() {
            if (this.actionCase_ == 42) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Staking.ReclaimOrBuilder getStakeWithdrawOrBuilder() {
            if (this.actionCase_ == 42) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public Transfer getTransfer() {
            if (this.actionCase_ == 10) {
                return (Transfer) this.action_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public TransferOrBuilder getTransferOrBuilder() {
            if (this.actionCase_ == 10) {
                return (Transfer) this.action_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasCandidateRegister() {
            return this.actionCase_ == 47;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasCandidateUpdate() {
            return this.actionCase_ == 48;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasExecution() {
            return this.actionCase_ == 12;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasStakeAddDeposit() {
            return this.actionCase_ == 43;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasStakeChangeCandidate() {
            return this.actionCase_ == 45;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasStakeCreate() {
            return this.actionCase_ == 40;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasStakeRestake() {
            return this.actionCase_ == 44;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasStakeTransferOwnership() {
            return this.actionCase_ == 46;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasStakeUnstake() {
            return this.actionCase_ == 41;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasStakeWithdraw() {
            return this.actionCase_ == 42;
        }

        @Override // wallet.core.jni.proto.IoTeX.ActionCoreOrBuilder
        public boolean hasTransfer() {
            return this.actionCase_ == 10;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getVersion()) * 37) + 2) * 53) + a0.h(getNonce())) * 37) + 3) * 53) + a0.h(getGasLimit())) * 37) + 4) * 53) + getGasPrice().hashCode();
            int i3 = this.actionCase_;
            if (i3 == 10) {
                i = ((hashCode2 * 37) + 10) * 53;
                hashCode = getTransfer().hashCode();
            } else if (i3 != 12) {
                switch (i3) {
                    case 40:
                        i = ((hashCode2 * 37) + 40) * 53;
                        hashCode = getStakeCreate().hashCode();
                        break;
                    case 41:
                        i = ((hashCode2 * 37) + 41) * 53;
                        hashCode = getStakeUnstake().hashCode();
                        break;
                    case 42:
                        i = ((hashCode2 * 37) + 42) * 53;
                        hashCode = getStakeWithdraw().hashCode();
                        break;
                    case 43:
                        i = ((hashCode2 * 37) + 43) * 53;
                        hashCode = getStakeAddDeposit().hashCode();
                        break;
                    case 44:
                        i = ((hashCode2 * 37) + 44) * 53;
                        hashCode = getStakeRestake().hashCode();
                        break;
                    case 45:
                        i = ((hashCode2 * 37) + 45) * 53;
                        hashCode = getStakeChangeCandidate().hashCode();
                        break;
                    case 46:
                        i = ((hashCode2 * 37) + 46) * 53;
                        hashCode = getStakeTransferOwnership().hashCode();
                        break;
                    case 47:
                        i = ((hashCode2 * 37) + 47) * 53;
                        hashCode = getCandidateRegister().hashCode();
                        break;
                    case 48:
                        i = ((hashCode2 * 37) + 48) * 53;
                        hashCode = getCandidateUpdate().hashCode();
                        break;
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            } else {
                i = ((hashCode2 * 37) + 12) * 53;
                hashCode = getExecution().hashCode();
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return IoTeX.internal_static_TW_IoTeX_Proto_ActionCore_fieldAccessorTable.d(ActionCore.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new ActionCore();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.version_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            long j2 = this.gasLimit_;
            if (j2 != 0) {
                codedOutputStream.d1(3, j2);
            }
            if (!getGasPriceBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.gasPrice_);
            }
            if (this.actionCase_ == 10) {
                codedOutputStream.K0(10, (Transfer) this.action_);
            }
            if (this.actionCase_ == 12) {
                codedOutputStream.K0(12, (ContractCall) this.action_);
            }
            if (this.actionCase_ == 40) {
                codedOutputStream.K0(40, (Staking.Create) this.action_);
            }
            if (this.actionCase_ == 41) {
                codedOutputStream.K0(41, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 42) {
                codedOutputStream.K0(42, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 43) {
                codedOutputStream.K0(43, (Staking.AddDeposit) this.action_);
            }
            if (this.actionCase_ == 44) {
                codedOutputStream.K0(44, (Staking.Restake) this.action_);
            }
            if (this.actionCase_ == 45) {
                codedOutputStream.K0(45, (Staking.ChangeCandidate) this.action_);
            }
            if (this.actionCase_ == 46) {
                codedOutputStream.K0(46, (Staking.TransferOwnership) this.action_);
            }
            if (this.actionCase_ == 47) {
                codedOutputStream.K0(47, (Staking.CandidateRegister) this.action_);
            }
            if (this.actionCase_ == 48) {
                codedOutputStream.K0(48, (Staking.CandidateBasicInfo) this.action_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ ActionCore(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(ActionCore actionCore) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(actionCore);
        }

        public static ActionCore parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private ActionCore(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.actionCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static ActionCore parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (ActionCore) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static ActionCore parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public ActionCore getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static ActionCore parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static ActionCore parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private ActionCore() {
            this.actionCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.gasPrice_ = "";
        }

        public static ActionCore parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static ActionCore parseFrom(InputStream inputStream) throws IOException {
            return (ActionCore) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static ActionCore parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (ActionCore) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private ActionCore(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            switch (J) {
                                case 0:
                                    break;
                                case 8:
                                    this.version_ = jVar.K();
                                    continue;
                                case 16:
                                    this.nonce_ = jVar.L();
                                    continue;
                                case 24:
                                    this.gasLimit_ = jVar.L();
                                    continue;
                                case 34:
                                    this.gasPrice_ = jVar.I();
                                    continue;
                                case 82:
                                    Transfer.Builder builder = this.actionCase_ == 10 ? ((Transfer) this.action_).toBuilder() : null;
                                    m0 z2 = jVar.z(Transfer.parser(), rVar);
                                    this.action_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((Transfer) z2);
                                        this.action_ = builder.buildPartial();
                                    }
                                    this.actionCase_ = 10;
                                    continue;
                                case 98:
                                    ContractCall.Builder builder2 = this.actionCase_ == 12 ? ((ContractCall) this.action_).toBuilder() : null;
                                    m0 z3 = jVar.z(ContractCall.parser(), rVar);
                                    this.action_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((ContractCall) z3);
                                        this.action_ = builder2.buildPartial();
                                    }
                                    this.actionCase_ = 12;
                                    continue;
                                case 322:
                                    Staking.Create.Builder builder3 = this.actionCase_ == 40 ? ((Staking.Create) this.action_).toBuilder() : null;
                                    m0 z4 = jVar.z(Staking.Create.parser(), rVar);
                                    this.action_ = z4;
                                    if (builder3 != null) {
                                        builder3.mergeFrom((Staking.Create) z4);
                                        this.action_ = builder3.buildPartial();
                                    }
                                    this.actionCase_ = 40;
                                    continue;
                                case 330:
                                    Staking.Reclaim.Builder builder4 = this.actionCase_ == 41 ? ((Staking.Reclaim) this.action_).toBuilder() : null;
                                    m0 z5 = jVar.z(Staking.Reclaim.parser(), rVar);
                                    this.action_ = z5;
                                    if (builder4 != null) {
                                        builder4.mergeFrom((Staking.Reclaim) z5);
                                        this.action_ = builder4.buildPartial();
                                    }
                                    this.actionCase_ = 41;
                                    continue;
                                case 338:
                                    Staking.Reclaim.Builder builder5 = this.actionCase_ == 42 ? ((Staking.Reclaim) this.action_).toBuilder() : null;
                                    m0 z6 = jVar.z(Staking.Reclaim.parser(), rVar);
                                    this.action_ = z6;
                                    if (builder5 != null) {
                                        builder5.mergeFrom((Staking.Reclaim) z6);
                                        this.action_ = builder5.buildPartial();
                                    }
                                    this.actionCase_ = 42;
                                    continue;
                                case 346:
                                    Staking.AddDeposit.Builder builder6 = this.actionCase_ == 43 ? ((Staking.AddDeposit) this.action_).toBuilder() : null;
                                    m0 z7 = jVar.z(Staking.AddDeposit.parser(), rVar);
                                    this.action_ = z7;
                                    if (builder6 != null) {
                                        builder6.mergeFrom((Staking.AddDeposit) z7);
                                        this.action_ = builder6.buildPartial();
                                    }
                                    this.actionCase_ = 43;
                                    continue;
                                case 354:
                                    Staking.Restake.Builder builder7 = this.actionCase_ == 44 ? ((Staking.Restake) this.action_).toBuilder() : null;
                                    m0 z8 = jVar.z(Staking.Restake.parser(), rVar);
                                    this.action_ = z8;
                                    if (builder7 != null) {
                                        builder7.mergeFrom((Staking.Restake) z8);
                                        this.action_ = builder7.buildPartial();
                                    }
                                    this.actionCase_ = 44;
                                    continue;
                                case 362:
                                    Staking.ChangeCandidate.Builder builder8 = this.actionCase_ == 45 ? ((Staking.ChangeCandidate) this.action_).toBuilder() : null;
                                    m0 z9 = jVar.z(Staking.ChangeCandidate.parser(), rVar);
                                    this.action_ = z9;
                                    if (builder8 != null) {
                                        builder8.mergeFrom((Staking.ChangeCandidate) z9);
                                        this.action_ = builder8.buildPartial();
                                    }
                                    this.actionCase_ = 45;
                                    continue;
                                case 370:
                                    Staking.TransferOwnership.Builder builder9 = this.actionCase_ == 46 ? ((Staking.TransferOwnership) this.action_).toBuilder() : null;
                                    m0 z10 = jVar.z(Staking.TransferOwnership.parser(), rVar);
                                    this.action_ = z10;
                                    if (builder9 != null) {
                                        builder9.mergeFrom((Staking.TransferOwnership) z10);
                                        this.action_ = builder9.buildPartial();
                                    }
                                    this.actionCase_ = 46;
                                    continue;
                                case 378:
                                    Staking.CandidateRegister.Builder builder10 = this.actionCase_ == 47 ? ((Staking.CandidateRegister) this.action_).toBuilder() : null;
                                    m0 z11 = jVar.z(Staking.CandidateRegister.parser(), rVar);
                                    this.action_ = z11;
                                    if (builder10 != null) {
                                        builder10.mergeFrom((Staking.CandidateRegister) z11);
                                        this.action_ = builder10.buildPartial();
                                    }
                                    this.actionCase_ = 47;
                                    continue;
                                case 386:
                                    Staking.CandidateBasicInfo.Builder builder11 = this.actionCase_ == 48 ? ((Staking.CandidateBasicInfo) this.action_).toBuilder() : null;
                                    m0 z12 = jVar.z(Staking.CandidateBasicInfo.parser(), rVar);
                                    this.action_ = z12;
                                    if (builder11 != null) {
                                        builder11.mergeFrom((Staking.CandidateBasicInfo) z12);
                                        this.action_ = builder11.buildPartial();
                                    }
                                    this.actionCase_ = 48;
                                    continue;
                                default:
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static ActionCore parseFrom(j jVar) throws IOException {
            return (ActionCore) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static ActionCore parseFrom(j jVar, r rVar) throws IOException {
            return (ActionCore) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface ActionCoreOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        ActionCore.ActionCase getActionCase();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Staking.CandidateRegister getCandidateRegister();

        Staking.CandidateRegisterOrBuilder getCandidateRegisterOrBuilder();

        Staking.CandidateBasicInfo getCandidateUpdate();

        Staking.CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ContractCall getExecution();

        ContractCallOrBuilder getExecutionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGasLimit();

        String getGasPrice();

        ByteString getGasPriceBytes();

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Staking.AddDeposit getStakeAddDeposit();

        Staking.AddDepositOrBuilder getStakeAddDepositOrBuilder();

        Staking.ChangeCandidate getStakeChangeCandidate();

        Staking.ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder();

        Staking.Create getStakeCreate();

        Staking.CreateOrBuilder getStakeCreateOrBuilder();

        Staking.Restake getStakeRestake();

        Staking.RestakeOrBuilder getStakeRestakeOrBuilder();

        Staking.TransferOwnership getStakeTransferOwnership();

        Staking.TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder();

        Staking.Reclaim getStakeUnstake();

        Staking.ReclaimOrBuilder getStakeUnstakeOrBuilder();

        Staking.Reclaim getStakeWithdraw();

        Staking.ReclaimOrBuilder getStakeWithdrawOrBuilder();

        Transfer getTransfer();

        TransferOrBuilder getTransferOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        int getVersion();

        boolean hasCandidateRegister();

        boolean hasCandidateUpdate();

        boolean hasExecution();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasStakeAddDeposit();

        boolean hasStakeChangeCandidate();

        boolean hasStakeCreate();

        boolean hasStakeRestake();

        boolean hasStakeTransferOwnership();

        boolean hasStakeUnstake();

        boolean hasStakeWithdraw();

        boolean hasTransfer();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public interface ActionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ActionCore getCore();

        ActionCoreOrBuilder getCoreOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSenderPubKey();

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasCore();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class ContractCall extends GeneratedMessageV3 implements ContractCallOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 1;
        public static final int CONTRACT_FIELD_NUMBER = 2;
        public static final int DATA_FIELD_NUMBER = 3;
        private static final ContractCall DEFAULT_INSTANCE = new ContractCall();
        private static final t0<ContractCall> PARSER = new c<ContractCall>() { // from class: wallet.core.jni.proto.IoTeX.ContractCall.1
            @Override // com.google.protobuf.t0
            public ContractCall parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new ContractCall(jVar, rVar, null);
            }
        };
        private static final long serialVersionUID = 0;
        private volatile Object amount_;
        private volatile Object contract_;
        private ByteString data_;
        private byte memoizedIsInitialized;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements ContractCallOrBuilder {
            private Object amount_;
            private Object contract_;
            private ByteString data_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_ContractCall_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = ContractCall.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearContract() {
                this.contract_ = ContractCall.getDefaultInstance().getContract();
                onChanged();
                return this;
            }

            public Builder clearData() {
                this.data_ = ContractCall.getDefaultInstance().getData();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
            public String getAmount() {
                Object obj = this.amount_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.amount_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
            public ByteString getAmountBytes() {
                Object obj = this.amount_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.amount_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
            public String getContract() {
                Object obj = this.contract_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.contract_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
            public ByteString getContractBytes() {
                Object obj = this.contract_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.contract_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return IoTeX.internal_static_TW_IoTeX_Proto_ContractCall_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_ContractCall_fieldAccessorTable.d(ContractCall.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(String str) {
                Objects.requireNonNull(str);
                this.amount_ = str;
                onChanged();
                return this;
            }

            public Builder setAmountBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setContract(String str) {
                Objects.requireNonNull(str);
                this.contract_ = str;
                onChanged();
                return this;
            }

            public Builder setContractBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.contract_ = byteString;
                onChanged();
                return this;
            }

            public Builder setData(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.data_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.amount_ = "";
                this.contract_ = "";
                this.data_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public ContractCall build() {
                ContractCall buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public ContractCall buildPartial() {
                ContractCall contractCall = new ContractCall(this, (AnonymousClass1) null);
                contractCall.amount_ = this.amount_;
                contractCall.contract_ = this.contract_;
                contractCall.data_ = this.data_;
                onBuilt();
                return contractCall;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public ContractCall getDefaultInstanceForType() {
                return ContractCall.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.amount_ = "";
                this.contract_ = "";
                this.data_ = ByteString.EMPTY;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof ContractCall) {
                    return mergeFrom((ContractCall) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.amount_ = "";
                this.contract_ = "";
                this.data_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(ContractCall contractCall) {
                if (contractCall == ContractCall.getDefaultInstance()) {
                    return this;
                }
                if (!contractCall.getAmount().isEmpty()) {
                    this.amount_ = contractCall.amount_;
                    onChanged();
                }
                if (!contractCall.getContract().isEmpty()) {
                    this.contract_ = contractCall.contract_;
                    onChanged();
                }
                if (contractCall.getData() != ByteString.EMPTY) {
                    setData(contractCall.getData());
                }
                mergeUnknownFields(contractCall.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.IoTeX.ContractCall.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.ContractCall.access$14600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.IoTeX$ContractCall r3 = (wallet.core.jni.proto.IoTeX.ContractCall) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.IoTeX$ContractCall r4 = (wallet.core.jni.proto.IoTeX.ContractCall) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.ContractCall.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$ContractCall$Builder");
            }
        }

        public /* synthetic */ ContractCall(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static ContractCall getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return IoTeX.internal_static_TW_IoTeX_Proto_ContractCall_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static ContractCall parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ContractCall) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static ContractCall parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<ContractCall> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ContractCall)) {
                return super.equals(obj);
            }
            ContractCall contractCall = (ContractCall) obj;
            return getAmount().equals(contractCall.getAmount()) && getContract().equals(contractCall.getContract()) && getData().equals(contractCall.getData()) && this.unknownFields.equals(contractCall.unknownFields);
        }

        @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
        public String getAmount() {
            Object obj = this.amount_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.amount_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
        public ByteString getAmountBytes() {
            Object obj = this.amount_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.amount_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
        public String getContract() {
            Object obj = this.contract_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.contract_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
        public ByteString getContractBytes() {
            Object obj = this.contract_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.contract_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.IoTeX.ContractCallOrBuilder
        public ByteString getData() {
            return this.data_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<ContractCall> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getAmountBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.amount_);
            if (!getContractBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.contract_);
            }
            if (!this.data_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.data_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAmount().hashCode()) * 37) + 2) * 53) + getContract().hashCode()) * 37) + 3) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return IoTeX.internal_static_TW_IoTeX_Proto_ContractCall_fieldAccessorTable.d(ContractCall.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new ContractCall();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getAmountBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.amount_);
            }
            if (!getContractBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.contract_);
            }
            if (!this.data_.isEmpty()) {
                codedOutputStream.q0(3, this.data_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ ContractCall(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(ContractCall contractCall) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(contractCall);
        }

        public static ContractCall parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private ContractCall(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static ContractCall parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (ContractCall) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static ContractCall parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public ContractCall getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static ContractCall parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private ContractCall() {
            this.memoizedIsInitialized = (byte) -1;
            this.amount_ = "";
            this.contract_ = "";
            this.data_ = ByteString.EMPTY;
        }

        public static ContractCall parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static ContractCall parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static ContractCall parseFrom(InputStream inputStream) throws IOException {
            return (ContractCall) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static ContractCall parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (ContractCall) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private ContractCall(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.amount_ = jVar.I();
                            } else if (J == 18) {
                                this.contract_ = jVar.I();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.data_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static ContractCall parseFrom(j jVar) throws IOException {
            return (ContractCall) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static ContractCall parseFrom(j jVar, r rVar) throws IOException {
            return (ContractCall) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface ContractCallOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getAmount();

        ByteString getAmountBytes();

        String getContract();

        ByteString getContractBytes();

        ByteString getData();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int CALL_FIELD_NUMBER = 12;
        public static final int CANDIDATEREGISTER_FIELD_NUMBER = 47;
        public static final int CANDIDATEUPDATE_FIELD_NUMBER = 48;
        public static final int GASLIMIT_FIELD_NUMBER = 3;
        public static final int GASPRICE_FIELD_NUMBER = 4;
        public static final int NONCE_FIELD_NUMBER = 2;
        public static final int PRIVATEKEY_FIELD_NUMBER = 5;
        public static final int STAKEADDDEPOSIT_FIELD_NUMBER = 43;
        public static final int STAKECHANGECANDIDATE_FIELD_NUMBER = 45;
        public static final int STAKECREATE_FIELD_NUMBER = 40;
        public static final int STAKERESTAKE_FIELD_NUMBER = 44;
        public static final int STAKETRANSFEROWNERSHIP_FIELD_NUMBER = 46;
        public static final int STAKEUNSTAKE_FIELD_NUMBER = 41;
        public static final int STAKEWITHDRAW_FIELD_NUMBER = 42;
        public static final int TRANSFER_FIELD_NUMBER = 10;
        public static final int VERSION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private int actionCase_;
        private Object action_;
        private long gasLimit_;
        private volatile Object gasPrice_;
        private byte memoizedIsInitialized;
        private long nonce_;
        private ByteString privateKey_;
        private int version_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.IoTeX.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public enum ActionCase implements a0.c {
            TRANSFER(10),
            CALL(12),
            STAKECREATE(40),
            STAKEUNSTAKE(41),
            STAKEWITHDRAW(42),
            STAKEADDDEPOSIT(43),
            STAKERESTAKE(44),
            STAKECHANGECANDIDATE(45),
            STAKETRANSFEROWNERSHIP(46),
            CANDIDATEREGISTER(47),
            CANDIDATEUPDATE(48),
            ACTION_NOT_SET(0);
            
            private final int value;

            ActionCase(int i) {
                this.value = i;
            }

            public static ActionCase forNumber(int i) {
                if (i != 0) {
                    if (i != 10) {
                        if (i != 12) {
                            switch (i) {
                                case 40:
                                    return STAKECREATE;
                                case 41:
                                    return STAKEUNSTAKE;
                                case 42:
                                    return STAKEWITHDRAW;
                                case 43:
                                    return STAKEADDDEPOSIT;
                                case 44:
                                    return STAKERESTAKE;
                                case 45:
                                    return STAKECHANGECANDIDATE;
                                case 46:
                                    return STAKETRANSFEROWNERSHIP;
                                case 47:
                                    return CANDIDATEREGISTER;
                                case 48:
                                    return CANDIDATEUPDATE;
                                default:
                                    return null;
                            }
                        }
                        return CALL;
                    }
                    return TRANSFER;
                }
                return ACTION_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static ActionCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private int actionCase_;
            private Object action_;
            private a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> callBuilder_;
            private a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> candidateRegisterBuilder_;
            private a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> candidateUpdateBuilder_;
            private long gasLimit_;
            private Object gasPrice_;
            private long nonce_;
            private ByteString privateKey_;
            private a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> stakeAddDepositBuilder_;
            private a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> stakeChangeCandidateBuilder_;
            private a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> stakeCreateBuilder_;
            private a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> stakeRestakeBuilder_;
            private a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> stakeTransferOwnershipBuilder_;
            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> stakeUnstakeBuilder_;
            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> stakeWithdrawBuilder_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferBuilder_;
            private int version_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> getCallFieldBuilder() {
                if (this.callBuilder_ == null) {
                    if (this.actionCase_ != 12) {
                        this.action_ = ContractCall.getDefaultInstance();
                    }
                    this.callBuilder_ = new a1<>((ContractCall) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 12;
                onChanged();
                return this.callBuilder_;
            }

            private a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> getCandidateRegisterFieldBuilder() {
                if (this.candidateRegisterBuilder_ == null) {
                    if (this.actionCase_ != 47) {
                        this.action_ = Staking.CandidateRegister.getDefaultInstance();
                    }
                    this.candidateRegisterBuilder_ = new a1<>((Staking.CandidateRegister) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 47;
                onChanged();
                return this.candidateRegisterBuilder_;
            }

            private a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> getCandidateUpdateFieldBuilder() {
                if (this.candidateUpdateBuilder_ == null) {
                    if (this.actionCase_ != 48) {
                        this.action_ = Staking.CandidateBasicInfo.getDefaultInstance();
                    }
                    this.candidateUpdateBuilder_ = new a1<>((Staking.CandidateBasicInfo) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 48;
                onChanged();
                return this.candidateUpdateBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_SigningInput_descriptor;
            }

            private a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> getStakeAddDepositFieldBuilder() {
                if (this.stakeAddDepositBuilder_ == null) {
                    if (this.actionCase_ != 43) {
                        this.action_ = Staking.AddDeposit.getDefaultInstance();
                    }
                    this.stakeAddDepositBuilder_ = new a1<>((Staking.AddDeposit) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 43;
                onChanged();
                return this.stakeAddDepositBuilder_;
            }

            private a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> getStakeChangeCandidateFieldBuilder() {
                if (this.stakeChangeCandidateBuilder_ == null) {
                    if (this.actionCase_ != 45) {
                        this.action_ = Staking.ChangeCandidate.getDefaultInstance();
                    }
                    this.stakeChangeCandidateBuilder_ = new a1<>((Staking.ChangeCandidate) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 45;
                onChanged();
                return this.stakeChangeCandidateBuilder_;
            }

            private a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> getStakeCreateFieldBuilder() {
                if (this.stakeCreateBuilder_ == null) {
                    if (this.actionCase_ != 40) {
                        this.action_ = Staking.Create.getDefaultInstance();
                    }
                    this.stakeCreateBuilder_ = new a1<>((Staking.Create) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 40;
                onChanged();
                return this.stakeCreateBuilder_;
            }

            private a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> getStakeRestakeFieldBuilder() {
                if (this.stakeRestakeBuilder_ == null) {
                    if (this.actionCase_ != 44) {
                        this.action_ = Staking.Restake.getDefaultInstance();
                    }
                    this.stakeRestakeBuilder_ = new a1<>((Staking.Restake) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 44;
                onChanged();
                return this.stakeRestakeBuilder_;
            }

            private a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> getStakeTransferOwnershipFieldBuilder() {
                if (this.stakeTransferOwnershipBuilder_ == null) {
                    if (this.actionCase_ != 46) {
                        this.action_ = Staking.TransferOwnership.getDefaultInstance();
                    }
                    this.stakeTransferOwnershipBuilder_ = new a1<>((Staking.TransferOwnership) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 46;
                onChanged();
                return this.stakeTransferOwnershipBuilder_;
            }

            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> getStakeUnstakeFieldBuilder() {
                if (this.stakeUnstakeBuilder_ == null) {
                    if (this.actionCase_ != 41) {
                        this.action_ = Staking.Reclaim.getDefaultInstance();
                    }
                    this.stakeUnstakeBuilder_ = new a1<>((Staking.Reclaim) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 41;
                onChanged();
                return this.stakeUnstakeBuilder_;
            }

            private a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> getStakeWithdrawFieldBuilder() {
                if (this.stakeWithdrawBuilder_ == null) {
                    if (this.actionCase_ != 42) {
                        this.action_ = Staking.Reclaim.getDefaultInstance();
                    }
                    this.stakeWithdrawBuilder_ = new a1<>((Staking.Reclaim) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 42;
                onChanged();
                return this.stakeWithdrawBuilder_;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferFieldBuilder() {
                if (this.transferBuilder_ == null) {
                    if (this.actionCase_ != 10) {
                        this.action_ = Transfer.getDefaultInstance();
                    }
                    this.transferBuilder_ = new a1<>((Transfer) this.action_, getParentForChildren(), isClean());
                    this.action_ = null;
                }
                this.actionCase_ = 10;
                onChanged();
                return this.transferBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAction() {
                this.actionCase_ = 0;
                this.action_ = null;
                onChanged();
                return this;
            }

            public Builder clearCall() {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.callBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 12) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 12) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearCandidateRegister() {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 47) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 47) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearCandidateUpdate() {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 48) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 48) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = SigningInput.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearStakeAddDeposit() {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 43) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 43) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeChangeCandidate() {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 45) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 45) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeCreate() {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 40) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 40) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeRestake() {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 44) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 44) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeTransferOwnership() {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 46) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 46) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeUnstake() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 41) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 41) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeWithdraw() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 42) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 42) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 10) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                        onChanged();
                    }
                } else {
                    if (this.actionCase_ == 10) {
                        this.actionCase_ = 0;
                        this.action_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearVersion() {
                this.version_ = 0;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public ActionCase getActionCase() {
                return ActionCase.forNumber(this.actionCase_);
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public ContractCall getCall() {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.callBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 12) {
                        return (ContractCall) this.action_;
                    }
                    return ContractCall.getDefaultInstance();
                } else if (this.actionCase_ == 12) {
                    return a1Var.f();
                } else {
                    return ContractCall.getDefaultInstance();
                }
            }

            public ContractCall.Builder getCallBuilder() {
                return getCallFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public ContractCallOrBuilder getCallOrBuilder() {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 12 || (a1Var = this.callBuilder_) == null) {
                    if (i == 12) {
                        return (ContractCall) this.action_;
                    }
                    return ContractCall.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.CandidateRegister getCandidateRegister() {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 47) {
                        return (Staking.CandidateRegister) this.action_;
                    }
                    return Staking.CandidateRegister.getDefaultInstance();
                } else if (this.actionCase_ == 47) {
                    return a1Var.f();
                } else {
                    return Staking.CandidateRegister.getDefaultInstance();
                }
            }

            public Staking.CandidateRegister.Builder getCandidateRegisterBuilder() {
                return getCandidateRegisterFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.CandidateRegisterOrBuilder getCandidateRegisterOrBuilder() {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 47 || (a1Var = this.candidateRegisterBuilder_) == null) {
                    if (i == 47) {
                        return (Staking.CandidateRegister) this.action_;
                    }
                    return Staking.CandidateRegister.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.CandidateBasicInfo getCandidateUpdate() {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 48) {
                        return (Staking.CandidateBasicInfo) this.action_;
                    }
                    return Staking.CandidateBasicInfo.getDefaultInstance();
                } else if (this.actionCase_ == 48) {
                    return a1Var.f();
                } else {
                    return Staking.CandidateBasicInfo.getDefaultInstance();
                }
            }

            public Staking.CandidateBasicInfo.Builder getCandidateUpdateBuilder() {
                return getCandidateUpdateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder() {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 48 || (a1Var = this.candidateUpdateBuilder_) == null) {
                    if (i == 48) {
                        return (Staking.CandidateBasicInfo) this.action_;
                    }
                    return Staking.CandidateBasicInfo.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return IoTeX.internal_static_TW_IoTeX_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public long getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public String getGasPrice() {
                Object obj = this.gasPrice_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.gasPrice_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public ByteString getGasPriceBytes() {
                Object obj = this.gasPrice_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.gasPrice_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public long getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.AddDeposit getStakeAddDeposit() {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 43) {
                        return (Staking.AddDeposit) this.action_;
                    }
                    return Staking.AddDeposit.getDefaultInstance();
                } else if (this.actionCase_ == 43) {
                    return a1Var.f();
                } else {
                    return Staking.AddDeposit.getDefaultInstance();
                }
            }

            public Staking.AddDeposit.Builder getStakeAddDepositBuilder() {
                return getStakeAddDepositFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.AddDepositOrBuilder getStakeAddDepositOrBuilder() {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 43 || (a1Var = this.stakeAddDepositBuilder_) == null) {
                    if (i == 43) {
                        return (Staking.AddDeposit) this.action_;
                    }
                    return Staking.AddDeposit.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.ChangeCandidate getStakeChangeCandidate() {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 45) {
                        return (Staking.ChangeCandidate) this.action_;
                    }
                    return Staking.ChangeCandidate.getDefaultInstance();
                } else if (this.actionCase_ == 45) {
                    return a1Var.f();
                } else {
                    return Staking.ChangeCandidate.getDefaultInstance();
                }
            }

            public Staking.ChangeCandidate.Builder getStakeChangeCandidateBuilder() {
                return getStakeChangeCandidateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder() {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 45 || (a1Var = this.stakeChangeCandidateBuilder_) == null) {
                    if (i == 45) {
                        return (Staking.ChangeCandidate) this.action_;
                    }
                    return Staking.ChangeCandidate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.Create getStakeCreate() {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 40) {
                        return (Staking.Create) this.action_;
                    }
                    return Staking.Create.getDefaultInstance();
                } else if (this.actionCase_ == 40) {
                    return a1Var.f();
                } else {
                    return Staking.Create.getDefaultInstance();
                }
            }

            public Staking.Create.Builder getStakeCreateBuilder() {
                return getStakeCreateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.CreateOrBuilder getStakeCreateOrBuilder() {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 40 || (a1Var = this.stakeCreateBuilder_) == null) {
                    if (i == 40) {
                        return (Staking.Create) this.action_;
                    }
                    return Staking.Create.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.Restake getStakeRestake() {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 44) {
                        return (Staking.Restake) this.action_;
                    }
                    return Staking.Restake.getDefaultInstance();
                } else if (this.actionCase_ == 44) {
                    return a1Var.f();
                } else {
                    return Staking.Restake.getDefaultInstance();
                }
            }

            public Staking.Restake.Builder getStakeRestakeBuilder() {
                return getStakeRestakeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.RestakeOrBuilder getStakeRestakeOrBuilder() {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 44 || (a1Var = this.stakeRestakeBuilder_) == null) {
                    if (i == 44) {
                        return (Staking.Restake) this.action_;
                    }
                    return Staking.Restake.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.TransferOwnership getStakeTransferOwnership() {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 46) {
                        return (Staking.TransferOwnership) this.action_;
                    }
                    return Staking.TransferOwnership.getDefaultInstance();
                } else if (this.actionCase_ == 46) {
                    return a1Var.f();
                } else {
                    return Staking.TransferOwnership.getDefaultInstance();
                }
            }

            public Staking.TransferOwnership.Builder getStakeTransferOwnershipBuilder() {
                return getStakeTransferOwnershipFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder() {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 46 || (a1Var = this.stakeTransferOwnershipBuilder_) == null) {
                    if (i == 46) {
                        return (Staking.TransferOwnership) this.action_;
                    }
                    return Staking.TransferOwnership.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.Reclaim getStakeUnstake() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 41) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                } else if (this.actionCase_ == 41) {
                    return a1Var.f();
                } else {
                    return Staking.Reclaim.getDefaultInstance();
                }
            }

            public Staking.Reclaim.Builder getStakeUnstakeBuilder() {
                return getStakeUnstakeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.ReclaimOrBuilder getStakeUnstakeOrBuilder() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 41 || (a1Var = this.stakeUnstakeBuilder_) == null) {
                    if (i == 41) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.Reclaim getStakeWithdraw() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 42) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                } else if (this.actionCase_ == 42) {
                    return a1Var.f();
                } else {
                    return Staking.Reclaim.getDefaultInstance();
                }
            }

            public Staking.Reclaim.Builder getStakeWithdrawBuilder() {
                return getStakeWithdrawFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Staking.ReclaimOrBuilder getStakeWithdrawOrBuilder() {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 42 || (a1Var = this.stakeWithdrawBuilder_) == null) {
                    if (i == 42) {
                        return (Staking.Reclaim) this.action_;
                    }
                    return Staking.Reclaim.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public Transfer getTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 10) {
                        return (Transfer) this.action_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.actionCase_ == 10) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferBuilder() {
                return getTransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public TransferOrBuilder getTransferOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.actionCase_;
                if (i != 10 || (a1Var = this.transferBuilder_) == null) {
                    if (i == 10) {
                        return (Transfer) this.action_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public int getVersion() {
                return this.version_;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasCall() {
                return this.actionCase_ == 12;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasCandidateRegister() {
                return this.actionCase_ == 47;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasCandidateUpdate() {
                return this.actionCase_ == 48;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasStakeAddDeposit() {
                return this.actionCase_ == 43;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasStakeChangeCandidate() {
                return this.actionCase_ == 45;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasStakeCreate() {
                return this.actionCase_ == 40;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasStakeRestake() {
                return this.actionCase_ == 44;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasStakeTransferOwnership() {
                return this.actionCase_ == 46;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasStakeUnstake() {
                return this.actionCase_ == 41;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasStakeWithdraw() {
                return this.actionCase_ == 42;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
            public boolean hasTransfer() {
                return this.actionCase_ == 10;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCall(ContractCall contractCall) {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.callBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 12 && this.action_ != ContractCall.getDefaultInstance()) {
                        this.action_ = ContractCall.newBuilder((ContractCall) this.action_).mergeFrom(contractCall).buildPartial();
                    } else {
                        this.action_ = contractCall;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 12) {
                        a1Var.h(contractCall);
                    }
                    this.callBuilder_.j(contractCall);
                }
                this.actionCase_ = 12;
                return this;
            }

            public Builder mergeCandidateRegister(Staking.CandidateRegister candidateRegister) {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 47 && this.action_ != Staking.CandidateRegister.getDefaultInstance()) {
                        this.action_ = Staking.CandidateRegister.newBuilder((Staking.CandidateRegister) this.action_).mergeFrom(candidateRegister).buildPartial();
                    } else {
                        this.action_ = candidateRegister;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 47) {
                        a1Var.h(candidateRegister);
                    }
                    this.candidateRegisterBuilder_.j(candidateRegister);
                }
                this.actionCase_ = 47;
                return this;
            }

            public Builder mergeCandidateUpdate(Staking.CandidateBasicInfo candidateBasicInfo) {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 48 && this.action_ != Staking.CandidateBasicInfo.getDefaultInstance()) {
                        this.action_ = Staking.CandidateBasicInfo.newBuilder((Staking.CandidateBasicInfo) this.action_).mergeFrom(candidateBasicInfo).buildPartial();
                    } else {
                        this.action_ = candidateBasicInfo;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 48) {
                        a1Var.h(candidateBasicInfo);
                    }
                    this.candidateUpdateBuilder_.j(candidateBasicInfo);
                }
                this.actionCase_ = 48;
                return this;
            }

            public Builder mergeStakeAddDeposit(Staking.AddDeposit addDeposit) {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 43 && this.action_ != Staking.AddDeposit.getDefaultInstance()) {
                        this.action_ = Staking.AddDeposit.newBuilder((Staking.AddDeposit) this.action_).mergeFrom(addDeposit).buildPartial();
                    } else {
                        this.action_ = addDeposit;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 43) {
                        a1Var.h(addDeposit);
                    }
                    this.stakeAddDepositBuilder_.j(addDeposit);
                }
                this.actionCase_ = 43;
                return this;
            }

            public Builder mergeStakeChangeCandidate(Staking.ChangeCandidate changeCandidate) {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 45 && this.action_ != Staking.ChangeCandidate.getDefaultInstance()) {
                        this.action_ = Staking.ChangeCandidate.newBuilder((Staking.ChangeCandidate) this.action_).mergeFrom(changeCandidate).buildPartial();
                    } else {
                        this.action_ = changeCandidate;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 45) {
                        a1Var.h(changeCandidate);
                    }
                    this.stakeChangeCandidateBuilder_.j(changeCandidate);
                }
                this.actionCase_ = 45;
                return this;
            }

            public Builder mergeStakeCreate(Staking.Create create) {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 40 && this.action_ != Staking.Create.getDefaultInstance()) {
                        this.action_ = Staking.Create.newBuilder((Staking.Create) this.action_).mergeFrom(create).buildPartial();
                    } else {
                        this.action_ = create;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 40) {
                        a1Var.h(create);
                    }
                    this.stakeCreateBuilder_.j(create);
                }
                this.actionCase_ = 40;
                return this;
            }

            public Builder mergeStakeRestake(Staking.Restake restake) {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 44 && this.action_ != Staking.Restake.getDefaultInstance()) {
                        this.action_ = Staking.Restake.newBuilder((Staking.Restake) this.action_).mergeFrom(restake).buildPartial();
                    } else {
                        this.action_ = restake;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 44) {
                        a1Var.h(restake);
                    }
                    this.stakeRestakeBuilder_.j(restake);
                }
                this.actionCase_ = 44;
                return this;
            }

            public Builder mergeStakeTransferOwnership(Staking.TransferOwnership transferOwnership) {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 46 && this.action_ != Staking.TransferOwnership.getDefaultInstance()) {
                        this.action_ = Staking.TransferOwnership.newBuilder((Staking.TransferOwnership) this.action_).mergeFrom(transferOwnership).buildPartial();
                    } else {
                        this.action_ = transferOwnership;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 46) {
                        a1Var.h(transferOwnership);
                    }
                    this.stakeTransferOwnershipBuilder_.j(transferOwnership);
                }
                this.actionCase_ = 46;
                return this;
            }

            public Builder mergeStakeUnstake(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 41 && this.action_ != Staking.Reclaim.getDefaultInstance()) {
                        this.action_ = Staking.Reclaim.newBuilder((Staking.Reclaim) this.action_).mergeFrom(reclaim).buildPartial();
                    } else {
                        this.action_ = reclaim;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 41) {
                        a1Var.h(reclaim);
                    }
                    this.stakeUnstakeBuilder_.j(reclaim);
                }
                this.actionCase_ = 41;
                return this;
            }

            public Builder mergeStakeWithdraw(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 42 && this.action_ != Staking.Reclaim.getDefaultInstance()) {
                        this.action_ = Staking.Reclaim.newBuilder((Staking.Reclaim) this.action_).mergeFrom(reclaim).buildPartial();
                    } else {
                        this.action_ = reclaim;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 42) {
                        a1Var.h(reclaim);
                    }
                    this.stakeWithdrawBuilder_.j(reclaim);
                }
                this.actionCase_ = 42;
                return this;
            }

            public Builder mergeTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.actionCase_ == 10 && this.action_ != Transfer.getDefaultInstance()) {
                        this.action_ = Transfer.newBuilder((Transfer) this.action_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.action_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.actionCase_ == 10) {
                        a1Var.h(transfer);
                    }
                    this.transferBuilder_.j(transfer);
                }
                this.actionCase_ = 10;
                return this;
            }

            public Builder setCall(ContractCall contractCall) {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.callBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(contractCall);
                    this.action_ = contractCall;
                    onChanged();
                } else {
                    a1Var.j(contractCall);
                }
                this.actionCase_ = 12;
                return this;
            }

            public Builder setCandidateRegister(Staking.CandidateRegister candidateRegister) {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(candidateRegister);
                    this.action_ = candidateRegister;
                    onChanged();
                } else {
                    a1Var.j(candidateRegister);
                }
                this.actionCase_ = 47;
                return this;
            }

            public Builder setCandidateUpdate(Staking.CandidateBasicInfo candidateBasicInfo) {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(candidateBasicInfo);
                    this.action_ = candidateBasicInfo;
                    onChanged();
                } else {
                    a1Var.j(candidateBasicInfo);
                }
                this.actionCase_ = 48;
                return this;
            }

            public Builder setGasLimit(long j) {
                this.gasLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setGasPrice(String str) {
                Objects.requireNonNull(str);
                this.gasPrice_ = str;
                onChanged();
                return this;
            }

            public Builder setGasPriceBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(long j) {
                this.nonce_ = j;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setStakeAddDeposit(Staking.AddDeposit addDeposit) {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(addDeposit);
                    this.action_ = addDeposit;
                    onChanged();
                } else {
                    a1Var.j(addDeposit);
                }
                this.actionCase_ = 43;
                return this;
            }

            public Builder setStakeChangeCandidate(Staking.ChangeCandidate changeCandidate) {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(changeCandidate);
                    this.action_ = changeCandidate;
                    onChanged();
                } else {
                    a1Var.j(changeCandidate);
                }
                this.actionCase_ = 45;
                return this;
            }

            public Builder setStakeCreate(Staking.Create create) {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(create);
                    this.action_ = create;
                    onChanged();
                } else {
                    a1Var.j(create);
                }
                this.actionCase_ = 40;
                return this;
            }

            public Builder setStakeRestake(Staking.Restake restake) {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(restake);
                    this.action_ = restake;
                    onChanged();
                } else {
                    a1Var.j(restake);
                }
                this.actionCase_ = 44;
                return this;
            }

            public Builder setStakeTransferOwnership(Staking.TransferOwnership transferOwnership) {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transferOwnership);
                    this.action_ = transferOwnership;
                    onChanged();
                } else {
                    a1Var.j(transferOwnership);
                }
                this.actionCase_ = 46;
                return this;
            }

            public Builder setStakeUnstake(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(reclaim);
                    this.action_ = reclaim;
                    onChanged();
                } else {
                    a1Var.j(reclaim);
                }
                this.actionCase_ = 41;
                return this;
            }

            public Builder setStakeWithdraw(Staking.Reclaim reclaim) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(reclaim);
                    this.action_ = reclaim;
                    onChanged();
                } else {
                    a1Var.j(reclaim);
                }
                this.actionCase_ = 42;
                return this;
            }

            public Builder setTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.action_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.actionCase_ = 10;
                return this;
            }

            public Builder setVersion(int i) {
                this.version_ = i;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.actionCase_ = 0;
                this.gasPrice_ = "";
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.version_ = this.version_;
                signingInput.nonce_ = this.nonce_;
                signingInput.gasLimit_ = this.gasLimit_;
                signingInput.gasPrice_ = this.gasPrice_;
                signingInput.privateKey_ = this.privateKey_;
                if (this.actionCase_ == 10) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                    if (a1Var == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var.b();
                    }
                }
                if (this.actionCase_ == 12) {
                    a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var2 = this.callBuilder_;
                    if (a1Var2 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var2.b();
                    }
                }
                if (this.actionCase_ == 40) {
                    a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var3 = this.stakeCreateBuilder_;
                    if (a1Var3 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var3.b();
                    }
                }
                if (this.actionCase_ == 41) {
                    a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var4 = this.stakeUnstakeBuilder_;
                    if (a1Var4 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var4.b();
                    }
                }
                if (this.actionCase_ == 42) {
                    a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var5 = this.stakeWithdrawBuilder_;
                    if (a1Var5 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var5.b();
                    }
                }
                if (this.actionCase_ == 43) {
                    a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var6 = this.stakeAddDepositBuilder_;
                    if (a1Var6 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var6.b();
                    }
                }
                if (this.actionCase_ == 44) {
                    a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var7 = this.stakeRestakeBuilder_;
                    if (a1Var7 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var7.b();
                    }
                }
                if (this.actionCase_ == 45) {
                    a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var8 = this.stakeChangeCandidateBuilder_;
                    if (a1Var8 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var8.b();
                    }
                }
                if (this.actionCase_ == 46) {
                    a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var9 = this.stakeTransferOwnershipBuilder_;
                    if (a1Var9 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var9.b();
                    }
                }
                if (this.actionCase_ == 47) {
                    a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var10 = this.candidateRegisterBuilder_;
                    if (a1Var10 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var10.b();
                    }
                }
                if (this.actionCase_ == 48) {
                    a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var11 = this.candidateUpdateBuilder_;
                    if (a1Var11 == null) {
                        signingInput.action_ = this.action_;
                    } else {
                        signingInput.action_ = a1Var11.b();
                    }
                }
                signingInput.actionCase_ = this.actionCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.version_ = 0;
                this.nonce_ = 0L;
                this.gasLimit_ = 0L;
                this.gasPrice_ = "";
                this.privateKey_ = ByteString.EMPTY;
                this.actionCase_ = 0;
                this.action_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setCall(ContractCall.Builder builder) {
                a1<ContractCall, ContractCall.Builder, ContractCallOrBuilder> a1Var = this.callBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 12;
                return this;
            }

            public Builder setCandidateRegister(Staking.CandidateRegister.Builder builder) {
                a1<Staking.CandidateRegister, Staking.CandidateRegister.Builder, Staking.CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 47;
                return this;
            }

            public Builder setCandidateUpdate(Staking.CandidateBasicInfo.Builder builder) {
                a1<Staking.CandidateBasicInfo, Staking.CandidateBasicInfo.Builder, Staking.CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 48;
                return this;
            }

            public Builder setStakeAddDeposit(Staking.AddDeposit.Builder builder) {
                a1<Staking.AddDeposit, Staking.AddDeposit.Builder, Staking.AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 43;
                return this;
            }

            public Builder setStakeChangeCandidate(Staking.ChangeCandidate.Builder builder) {
                a1<Staking.ChangeCandidate, Staking.ChangeCandidate.Builder, Staking.ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 45;
                return this;
            }

            public Builder setStakeCreate(Staking.Create.Builder builder) {
                a1<Staking.Create, Staking.Create.Builder, Staking.CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 40;
                return this;
            }

            public Builder setStakeRestake(Staking.Restake.Builder builder) {
                a1<Staking.Restake, Staking.Restake.Builder, Staking.RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 44;
                return this;
            }

            public Builder setStakeTransferOwnership(Staking.TransferOwnership.Builder builder) {
                a1<Staking.TransferOwnership, Staking.TransferOwnership.Builder, Staking.TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 46;
                return this;
            }

            public Builder setStakeUnstake(Staking.Reclaim.Builder builder) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 41;
                return this;
            }

            public Builder setStakeWithdraw(Staking.Reclaim.Builder builder) {
                a1<Staking.Reclaim, Staking.Reclaim.Builder, Staking.ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 42;
                return this;
            }

            public Builder setTransfer(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.actionCase_ = 10;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.actionCase_ = 0;
                this.gasPrice_ = "";
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getVersion() != 0) {
                    setVersion(signingInput.getVersion());
                }
                if (signingInput.getNonce() != 0) {
                    setNonce(signingInput.getNonce());
                }
                if (signingInput.getGasLimit() != 0) {
                    setGasLimit(signingInput.getGasLimit());
                }
                if (!signingInput.getGasPrice().isEmpty()) {
                    this.gasPrice_ = signingInput.gasPrice_;
                    onChanged();
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$IoTeX$SigningInput$ActionCase[signingInput.getActionCase().ordinal()]) {
                    case 1:
                        mergeTransfer(signingInput.getTransfer());
                        break;
                    case 2:
                        mergeCall(signingInput.getCall());
                        break;
                    case 3:
                        mergeStakeCreate(signingInput.getStakeCreate());
                        break;
                    case 4:
                        mergeStakeUnstake(signingInput.getStakeUnstake());
                        break;
                    case 5:
                        mergeStakeWithdraw(signingInput.getStakeWithdraw());
                        break;
                    case 6:
                        mergeStakeAddDeposit(signingInput.getStakeAddDeposit());
                        break;
                    case 7:
                        mergeStakeRestake(signingInput.getStakeRestake());
                        break;
                    case 8:
                        mergeStakeChangeCandidate(signingInput.getStakeChangeCandidate());
                        break;
                    case 9:
                        mergeStakeTransferOwnership(signingInput.getStakeTransferOwnership());
                        break;
                    case 10:
                        mergeCandidateRegister(signingInput.getCandidateRegister());
                        break;
                    case 11:
                        mergeCandidateUpdate(signingInput.getCandidateUpdate());
                        break;
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.IoTeX.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.SigningInput.access$16400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.IoTeX$SigningInput r3 = (wallet.core.jni.proto.IoTeX.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.IoTeX$SigningInput r4 = (wallet.core.jni.proto.IoTeX.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return IoTeX.internal_static_TW_IoTeX_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getVersion() == signingInput.getVersion() && getNonce() == signingInput.getNonce() && getGasLimit() == signingInput.getGasLimit() && getGasPrice().equals(signingInput.getGasPrice()) && getPrivateKey().equals(signingInput.getPrivateKey()) && getActionCase().equals(signingInput.getActionCase())) {
                int i = this.actionCase_;
                if (i != 10) {
                    if (i != 12) {
                        switch (i) {
                            case 40:
                                if (!getStakeCreate().equals(signingInput.getStakeCreate())) {
                                    return false;
                                }
                                break;
                            case 41:
                                if (!getStakeUnstake().equals(signingInput.getStakeUnstake())) {
                                    return false;
                                }
                                break;
                            case 42:
                                if (!getStakeWithdraw().equals(signingInput.getStakeWithdraw())) {
                                    return false;
                                }
                                break;
                            case 43:
                                if (!getStakeAddDeposit().equals(signingInput.getStakeAddDeposit())) {
                                    return false;
                                }
                                break;
                            case 44:
                                if (!getStakeRestake().equals(signingInput.getStakeRestake())) {
                                    return false;
                                }
                                break;
                            case 45:
                                if (!getStakeChangeCandidate().equals(signingInput.getStakeChangeCandidate())) {
                                    return false;
                                }
                                break;
                            case 46:
                                if (!getStakeTransferOwnership().equals(signingInput.getStakeTransferOwnership())) {
                                    return false;
                                }
                                break;
                            case 47:
                                if (!getCandidateRegister().equals(signingInput.getCandidateRegister())) {
                                    return false;
                                }
                                break;
                            case 48:
                                if (!getCandidateUpdate().equals(signingInput.getCandidateUpdate())) {
                                    return false;
                                }
                                break;
                        }
                    } else if (!getCall().equals(signingInput.getCall())) {
                        return false;
                    }
                } else if (!getTransfer().equals(signingInput.getTransfer())) {
                    return false;
                }
                return this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public ActionCase getActionCase() {
            return ActionCase.forNumber(this.actionCase_);
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public ContractCall getCall() {
            if (this.actionCase_ == 12) {
                return (ContractCall) this.action_;
            }
            return ContractCall.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public ContractCallOrBuilder getCallOrBuilder() {
            if (this.actionCase_ == 12) {
                return (ContractCall) this.action_;
            }
            return ContractCall.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.CandidateRegister getCandidateRegister() {
            if (this.actionCase_ == 47) {
                return (Staking.CandidateRegister) this.action_;
            }
            return Staking.CandidateRegister.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.CandidateRegisterOrBuilder getCandidateRegisterOrBuilder() {
            if (this.actionCase_ == 47) {
                return (Staking.CandidateRegister) this.action_;
            }
            return Staking.CandidateRegister.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.CandidateBasicInfo getCandidateUpdate() {
            if (this.actionCase_ == 48) {
                return (Staking.CandidateBasicInfo) this.action_;
            }
            return Staking.CandidateBasicInfo.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder() {
            if (this.actionCase_ == 48) {
                return (Staking.CandidateBasicInfo) this.action_;
            }
            return Staking.CandidateBasicInfo.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public long getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public String getGasPrice() {
            Object obj = this.gasPrice_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.gasPrice_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public ByteString getGasPriceBytes() {
            Object obj = this.gasPrice_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.gasPrice_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public long getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.version_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            long j = this.nonce_;
            if (j != 0) {
                Y += CodedOutputStream.a0(2, j);
            }
            long j2 = this.gasLimit_;
            if (j2 != 0) {
                Y += CodedOutputStream.a0(3, j2);
            }
            if (!getGasPriceBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(4, this.gasPrice_);
            }
            if (!this.privateKey_.isEmpty()) {
                Y += CodedOutputStream.h(5, this.privateKey_);
            }
            if (this.actionCase_ == 10) {
                Y += CodedOutputStream.G(10, (Transfer) this.action_);
            }
            if (this.actionCase_ == 12) {
                Y += CodedOutputStream.G(12, (ContractCall) this.action_);
            }
            if (this.actionCase_ == 40) {
                Y += CodedOutputStream.G(40, (Staking.Create) this.action_);
            }
            if (this.actionCase_ == 41) {
                Y += CodedOutputStream.G(41, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 42) {
                Y += CodedOutputStream.G(42, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 43) {
                Y += CodedOutputStream.G(43, (Staking.AddDeposit) this.action_);
            }
            if (this.actionCase_ == 44) {
                Y += CodedOutputStream.G(44, (Staking.Restake) this.action_);
            }
            if (this.actionCase_ == 45) {
                Y += CodedOutputStream.G(45, (Staking.ChangeCandidate) this.action_);
            }
            if (this.actionCase_ == 46) {
                Y += CodedOutputStream.G(46, (Staking.TransferOwnership) this.action_);
            }
            if (this.actionCase_ == 47) {
                Y += CodedOutputStream.G(47, (Staking.CandidateRegister) this.action_);
            }
            if (this.actionCase_ == 48) {
                Y += CodedOutputStream.G(48, (Staking.CandidateBasicInfo) this.action_);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.AddDeposit getStakeAddDeposit() {
            if (this.actionCase_ == 43) {
                return (Staking.AddDeposit) this.action_;
            }
            return Staking.AddDeposit.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.AddDepositOrBuilder getStakeAddDepositOrBuilder() {
            if (this.actionCase_ == 43) {
                return (Staking.AddDeposit) this.action_;
            }
            return Staking.AddDeposit.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.ChangeCandidate getStakeChangeCandidate() {
            if (this.actionCase_ == 45) {
                return (Staking.ChangeCandidate) this.action_;
            }
            return Staking.ChangeCandidate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder() {
            if (this.actionCase_ == 45) {
                return (Staking.ChangeCandidate) this.action_;
            }
            return Staking.ChangeCandidate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.Create getStakeCreate() {
            if (this.actionCase_ == 40) {
                return (Staking.Create) this.action_;
            }
            return Staking.Create.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.CreateOrBuilder getStakeCreateOrBuilder() {
            if (this.actionCase_ == 40) {
                return (Staking.Create) this.action_;
            }
            return Staking.Create.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.Restake getStakeRestake() {
            if (this.actionCase_ == 44) {
                return (Staking.Restake) this.action_;
            }
            return Staking.Restake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.RestakeOrBuilder getStakeRestakeOrBuilder() {
            if (this.actionCase_ == 44) {
                return (Staking.Restake) this.action_;
            }
            return Staking.Restake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.TransferOwnership getStakeTransferOwnership() {
            if (this.actionCase_ == 46) {
                return (Staking.TransferOwnership) this.action_;
            }
            return Staking.TransferOwnership.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder() {
            if (this.actionCase_ == 46) {
                return (Staking.TransferOwnership) this.action_;
            }
            return Staking.TransferOwnership.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.Reclaim getStakeUnstake() {
            if (this.actionCase_ == 41) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.ReclaimOrBuilder getStakeUnstakeOrBuilder() {
            if (this.actionCase_ == 41) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.Reclaim getStakeWithdraw() {
            if (this.actionCase_ == 42) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Staking.ReclaimOrBuilder getStakeWithdrawOrBuilder() {
            if (this.actionCase_ == 42) {
                return (Staking.Reclaim) this.action_;
            }
            return Staking.Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public Transfer getTransfer() {
            if (this.actionCase_ == 10) {
                return (Transfer) this.action_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public TransferOrBuilder getTransferOrBuilder() {
            if (this.actionCase_ == 10) {
                return (Transfer) this.action_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public int getVersion() {
            return this.version_;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasCall() {
            return this.actionCase_ == 12;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasCandidateRegister() {
            return this.actionCase_ == 47;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasCandidateUpdate() {
            return this.actionCase_ == 48;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasStakeAddDeposit() {
            return this.actionCase_ == 43;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasStakeChangeCandidate() {
            return this.actionCase_ == 45;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasStakeCreate() {
            return this.actionCase_ == 40;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasStakeRestake() {
            return this.actionCase_ == 44;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasStakeTransferOwnership() {
            return this.actionCase_ == 46;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasStakeUnstake() {
            return this.actionCase_ == 41;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasStakeWithdraw() {
            return this.actionCase_ == 42;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningInputOrBuilder
        public boolean hasTransfer() {
            return this.actionCase_ == 10;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getVersion()) * 37) + 2) * 53) + a0.h(getNonce())) * 37) + 3) * 53) + a0.h(getGasLimit())) * 37) + 4) * 53) + getGasPrice().hashCode()) * 37) + 5) * 53) + getPrivateKey().hashCode();
            int i3 = this.actionCase_;
            if (i3 == 10) {
                i = ((hashCode2 * 37) + 10) * 53;
                hashCode = getTransfer().hashCode();
            } else if (i3 != 12) {
                switch (i3) {
                    case 40:
                        i = ((hashCode2 * 37) + 40) * 53;
                        hashCode = getStakeCreate().hashCode();
                        break;
                    case 41:
                        i = ((hashCode2 * 37) + 41) * 53;
                        hashCode = getStakeUnstake().hashCode();
                        break;
                    case 42:
                        i = ((hashCode2 * 37) + 42) * 53;
                        hashCode = getStakeWithdraw().hashCode();
                        break;
                    case 43:
                        i = ((hashCode2 * 37) + 43) * 53;
                        hashCode = getStakeAddDeposit().hashCode();
                        break;
                    case 44:
                        i = ((hashCode2 * 37) + 44) * 53;
                        hashCode = getStakeRestake().hashCode();
                        break;
                    case 45:
                        i = ((hashCode2 * 37) + 45) * 53;
                        hashCode = getStakeChangeCandidate().hashCode();
                        break;
                    case 46:
                        i = ((hashCode2 * 37) + 46) * 53;
                        hashCode = getStakeTransferOwnership().hashCode();
                        break;
                    case 47:
                        i = ((hashCode2 * 37) + 47) * 53;
                        hashCode = getCandidateRegister().hashCode();
                        break;
                    case 48:
                        i = ((hashCode2 * 37) + 48) * 53;
                        hashCode = getCandidateUpdate().hashCode();
                        break;
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            } else {
                i = ((hashCode2 * 37) + 12) * 53;
                hashCode = getCall().hashCode();
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return IoTeX.internal_static_TW_IoTeX_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.version_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            long j = this.nonce_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            long j2 = this.gasLimit_;
            if (j2 != 0) {
                codedOutputStream.d1(3, j2);
            }
            if (!getGasPriceBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.gasPrice_);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(5, this.privateKey_);
            }
            if (this.actionCase_ == 10) {
                codedOutputStream.K0(10, (Transfer) this.action_);
            }
            if (this.actionCase_ == 12) {
                codedOutputStream.K0(12, (ContractCall) this.action_);
            }
            if (this.actionCase_ == 40) {
                codedOutputStream.K0(40, (Staking.Create) this.action_);
            }
            if (this.actionCase_ == 41) {
                codedOutputStream.K0(41, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 42) {
                codedOutputStream.K0(42, (Staking.Reclaim) this.action_);
            }
            if (this.actionCase_ == 43) {
                codedOutputStream.K0(43, (Staking.AddDeposit) this.action_);
            }
            if (this.actionCase_ == 44) {
                codedOutputStream.K0(44, (Staking.Restake) this.action_);
            }
            if (this.actionCase_ == 45) {
                codedOutputStream.K0(45, (Staking.ChangeCandidate) this.action_);
            }
            if (this.actionCase_ == 46) {
                codedOutputStream.K0(46, (Staking.TransferOwnership) this.action_);
            }
            if (this.actionCase_ == 47) {
                codedOutputStream.K0(47, (Staking.CandidateRegister) this.action_);
            }
            if (this.actionCase_ == 48) {
                codedOutputStream.K0(48, (Staking.CandidateBasicInfo) this.action_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.actionCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.actionCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.gasPrice_ = "";
            this.privateKey_ = ByteString.EMPTY;
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 8:
                                this.version_ = jVar.K();
                                continue;
                            case 16:
                                this.nonce_ = jVar.L();
                                continue;
                            case 24:
                                this.gasLimit_ = jVar.L();
                                continue;
                            case 34:
                                this.gasPrice_ = jVar.I();
                                continue;
                            case 42:
                                this.privateKey_ = jVar.q();
                                continue;
                            case 82:
                                Transfer.Builder builder = this.actionCase_ == 10 ? ((Transfer) this.action_).toBuilder() : null;
                                m0 z2 = jVar.z(Transfer.parser(), rVar);
                                this.action_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((Transfer) z2);
                                    this.action_ = builder.buildPartial();
                                }
                                this.actionCase_ = 10;
                                continue;
                            case 98:
                                ContractCall.Builder builder2 = this.actionCase_ == 12 ? ((ContractCall) this.action_).toBuilder() : null;
                                m0 z3 = jVar.z(ContractCall.parser(), rVar);
                                this.action_ = z3;
                                if (builder2 != null) {
                                    builder2.mergeFrom((ContractCall) z3);
                                    this.action_ = builder2.buildPartial();
                                }
                                this.actionCase_ = 12;
                                continue;
                            case 322:
                                Staking.Create.Builder builder3 = this.actionCase_ == 40 ? ((Staking.Create) this.action_).toBuilder() : null;
                                m0 z4 = jVar.z(Staking.Create.parser(), rVar);
                                this.action_ = z4;
                                if (builder3 != null) {
                                    builder3.mergeFrom((Staking.Create) z4);
                                    this.action_ = builder3.buildPartial();
                                }
                                this.actionCase_ = 40;
                                continue;
                            case 330:
                                Staking.Reclaim.Builder builder4 = this.actionCase_ == 41 ? ((Staking.Reclaim) this.action_).toBuilder() : null;
                                m0 z5 = jVar.z(Staking.Reclaim.parser(), rVar);
                                this.action_ = z5;
                                if (builder4 != null) {
                                    builder4.mergeFrom((Staking.Reclaim) z5);
                                    this.action_ = builder4.buildPartial();
                                }
                                this.actionCase_ = 41;
                                continue;
                            case 338:
                                Staking.Reclaim.Builder builder5 = this.actionCase_ == 42 ? ((Staking.Reclaim) this.action_).toBuilder() : null;
                                m0 z6 = jVar.z(Staking.Reclaim.parser(), rVar);
                                this.action_ = z6;
                                if (builder5 != null) {
                                    builder5.mergeFrom((Staking.Reclaim) z6);
                                    this.action_ = builder5.buildPartial();
                                }
                                this.actionCase_ = 42;
                                continue;
                            case 346:
                                Staking.AddDeposit.Builder builder6 = this.actionCase_ == 43 ? ((Staking.AddDeposit) this.action_).toBuilder() : null;
                                m0 z7 = jVar.z(Staking.AddDeposit.parser(), rVar);
                                this.action_ = z7;
                                if (builder6 != null) {
                                    builder6.mergeFrom((Staking.AddDeposit) z7);
                                    this.action_ = builder6.buildPartial();
                                }
                                this.actionCase_ = 43;
                                continue;
                            case 354:
                                Staking.Restake.Builder builder7 = this.actionCase_ == 44 ? ((Staking.Restake) this.action_).toBuilder() : null;
                                m0 z8 = jVar.z(Staking.Restake.parser(), rVar);
                                this.action_ = z8;
                                if (builder7 != null) {
                                    builder7.mergeFrom((Staking.Restake) z8);
                                    this.action_ = builder7.buildPartial();
                                }
                                this.actionCase_ = 44;
                                continue;
                            case 362:
                                Staking.ChangeCandidate.Builder builder8 = this.actionCase_ == 45 ? ((Staking.ChangeCandidate) this.action_).toBuilder() : null;
                                m0 z9 = jVar.z(Staking.ChangeCandidate.parser(), rVar);
                                this.action_ = z9;
                                if (builder8 != null) {
                                    builder8.mergeFrom((Staking.ChangeCandidate) z9);
                                    this.action_ = builder8.buildPartial();
                                }
                                this.actionCase_ = 45;
                                continue;
                            case 370:
                                Staking.TransferOwnership.Builder builder9 = this.actionCase_ == 46 ? ((Staking.TransferOwnership) this.action_).toBuilder() : null;
                                m0 z10 = jVar.z(Staking.TransferOwnership.parser(), rVar);
                                this.action_ = z10;
                                if (builder9 != null) {
                                    builder9.mergeFrom((Staking.TransferOwnership) z10);
                                    this.action_ = builder9.buildPartial();
                                }
                                this.actionCase_ = 46;
                                continue;
                            case 378:
                                Staking.CandidateRegister.Builder builder10 = this.actionCase_ == 47 ? ((Staking.CandidateRegister) this.action_).toBuilder() : null;
                                m0 z11 = jVar.z(Staking.CandidateRegister.parser(), rVar);
                                this.action_ = z11;
                                if (builder10 != null) {
                                    builder10.mergeFrom((Staking.CandidateRegister) z11);
                                    this.action_ = builder10.buildPartial();
                                }
                                this.actionCase_ = 47;
                                continue;
                            case 386:
                                Staking.CandidateBasicInfo.Builder builder11 = this.actionCase_ == 48 ? ((Staking.CandidateBasicInfo) this.action_).toBuilder() : null;
                                m0 z12 = jVar.z(Staking.CandidateBasicInfo.parser(), rVar);
                                this.action_ = z12;
                                if (builder11 != null) {
                                    builder11.mergeFrom((Staking.CandidateBasicInfo) z12);
                                    this.action_ = builder11.buildPartial();
                                }
                                this.actionCase_ = 48;
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        SigningInput.ActionCase getActionCase();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ContractCall getCall();

        ContractCallOrBuilder getCallOrBuilder();

        Staking.CandidateRegister getCandidateRegister();

        Staking.CandidateRegisterOrBuilder getCandidateRegisterOrBuilder();

        Staking.CandidateBasicInfo getCandidateUpdate();

        Staking.CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGasLimit();

        String getGasPrice();

        ByteString getGasPriceBytes();

        /* synthetic */ String getInitializationErrorString();

        long getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Staking.AddDeposit getStakeAddDeposit();

        Staking.AddDepositOrBuilder getStakeAddDepositOrBuilder();

        Staking.ChangeCandidate getStakeChangeCandidate();

        Staking.ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder();

        Staking.Create getStakeCreate();

        Staking.CreateOrBuilder getStakeCreateOrBuilder();

        Staking.Restake getStakeRestake();

        Staking.RestakeOrBuilder getStakeRestakeOrBuilder();

        Staking.TransferOwnership getStakeTransferOwnership();

        Staking.TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder();

        Staking.Reclaim getStakeUnstake();

        Staking.ReclaimOrBuilder getStakeUnstakeOrBuilder();

        Staking.Reclaim getStakeWithdraw();

        Staking.ReclaimOrBuilder getStakeWithdrawOrBuilder();

        Transfer getTransfer();

        TransferOrBuilder getTransferOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        int getVersion();

        boolean hasCall();

        boolean hasCandidateRegister();

        boolean hasCandidateUpdate();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasStakeAddDeposit();

        boolean hasStakeChangeCandidate();

        boolean hasStakeCreate();

        boolean hasStakeRestake();

        boolean hasStakeTransferOwnership();

        boolean hasStakeUnstake();

        boolean hasStakeWithdraw();

        boolean hasTransfer();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        public static final int HASH_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private ByteString hash_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.IoTeX.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;
            private ByteString hash_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearHash() {
                this.hash_ = SigningOutput.getDefaultInstance().getHash();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return IoTeX.internal_static_TW_IoTeX_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.IoTeX.SigningOutputOrBuilder
            public ByteString getHash() {
                return this.hash_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.hash_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.hash_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                signingOutput.hash_ = this.hash_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.hash_ = byteString;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.hash_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                ByteString encoded = signingOutput.getEncoded();
                ByteString byteString = ByteString.EMPTY;
                if (encoded != byteString) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (signingOutput.getHash() != byteString) {
                    setHash(signingOutput.getHash());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.IoTeX.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.SigningOutput.access$17600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.IoTeX$SigningOutput r3 = (wallet.core.jni.proto.IoTeX.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.IoTeX$SigningOutput r4 = (wallet.core.jni.proto.IoTeX.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return IoTeX.internal_static_TW_IoTeX_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && getHash().equals(signingOutput.getHash()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // wallet.core.jni.proto.IoTeX.SigningOutputOrBuilder
        public ByteString getHash() {
            return this.hash_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_);
            if (!this.hash_.isEmpty()) {
                h += CodedOutputStream.h(2, this.hash_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 37) + 2) * 53) + getHash().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return IoTeX.internal_static_TW_IoTeX_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            if (!this.hash_.isEmpty()) {
                codedOutputStream.q0(2, this.hash_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.encoded_ = byteString;
            this.hash_ = byteString;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.encoded_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.hash_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getHash();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Staking extends GeneratedMessageV3 implements StakingOrBuilder {
        public static final int CANDIDATEREGISTER_FIELD_NUMBER = 8;
        public static final int CANDIDATEUPDATE_FIELD_NUMBER = 9;
        private static final Staking DEFAULT_INSTANCE = new Staking();
        private static final t0<Staking> PARSER = new c<Staking>() { // from class: wallet.core.jni.proto.IoTeX.Staking.1
            @Override // com.google.protobuf.t0
            public Staking parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Staking(jVar, rVar, null);
            }
        };
        public static final int STAKEADDDEPOSIT_FIELD_NUMBER = 4;
        public static final int STAKECHANGECANDIDATE_FIELD_NUMBER = 6;
        public static final int STAKECREATE_FIELD_NUMBER = 1;
        public static final int STAKERESTAKE_FIELD_NUMBER = 5;
        public static final int STAKETRANSFEROWNERSHIP_FIELD_NUMBER = 7;
        public static final int STAKEUNSTAKE_FIELD_NUMBER = 2;
        public static final int STAKEWITHDRAW_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageCase_;
        private Object message_;

        /* loaded from: classes3.dex */
        public static final class AddDeposit extends GeneratedMessageV3 implements AddDepositOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 2;
            public static final int BUCKETINDEX_FIELD_NUMBER = 1;
            private static final AddDeposit DEFAULT_INSTANCE = new AddDeposit();
            private static final t0<AddDeposit> PARSER = new c<AddDeposit>() { // from class: wallet.core.jni.proto.IoTeX.Staking.AddDeposit.1
                @Override // com.google.protobuf.t0
                public AddDeposit parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new AddDeposit(jVar, rVar, null);
                }
            };
            public static final int PAYLOAD_FIELD_NUMBER = 3;
            private static final long serialVersionUID = 0;
            private volatile Object amount_;
            private long bucketIndex_;
            private byte memoizedIsInitialized;
            private ByteString payload_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements AddDepositOrBuilder {
                private Object amount_;
                private long bucketIndex_;
                private ByteString payload_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_AddDeposit_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = AddDeposit.getDefaultInstance().getAmount();
                    onChanged();
                    return this;
                }

                public Builder clearBucketIndex() {
                    this.bucketIndex_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearPayload() {
                    this.payload_ = AddDeposit.getDefaultInstance().getPayload();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
                public String getAmount() {
                    Object obj = this.amount_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.amount_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
                public ByteString getAmountBytes() {
                    Object obj = this.amount_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.amount_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
                public long getBucketIndex() {
                    return this.bucketIndex_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_AddDeposit_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
                public ByteString getPayload() {
                    return this.payload_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_AddDeposit_fieldAccessorTable.d(AddDeposit.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(String str) {
                    Objects.requireNonNull(str);
                    this.amount_ = str;
                    onChanged();
                    return this;
                }

                public Builder setAmountBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.amount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setBucketIndex(long j) {
                    this.bucketIndex_ = j;
                    onChanged();
                    return this;
                }

                public Builder setPayload(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.payload_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.amount_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public AddDeposit build() {
                    AddDeposit buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public AddDeposit buildPartial() {
                    AddDeposit addDeposit = new AddDeposit(this, (AnonymousClass1) null);
                    addDeposit.bucketIndex_ = this.bucketIndex_;
                    addDeposit.amount_ = this.amount_;
                    addDeposit.payload_ = this.payload_;
                    onBuilt();
                    return addDeposit;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public AddDeposit getDefaultInstanceForType() {
                    return AddDeposit.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.bucketIndex_ = 0L;
                    this.amount_ = "";
                    this.payload_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.amount_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof AddDeposit) {
                        return mergeFrom((AddDeposit) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(AddDeposit addDeposit) {
                    if (addDeposit == AddDeposit.getDefaultInstance()) {
                        return this;
                    }
                    if (addDeposit.getBucketIndex() != 0) {
                        setBucketIndex(addDeposit.getBucketIndex());
                    }
                    if (!addDeposit.getAmount().isEmpty()) {
                        this.amount_ = addDeposit.amount_;
                        onChanged();
                    }
                    if (addDeposit.getPayload() != ByteString.EMPTY) {
                        setPayload(addDeposit.getPayload());
                    }
                    mergeUnknownFields(addDeposit.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.AddDeposit.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.AddDeposit.access$5300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$AddDeposit r3 = (wallet.core.jni.proto.IoTeX.Staking.AddDeposit) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$AddDeposit r4 = (wallet.core.jni.proto.IoTeX.Staking.AddDeposit) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.AddDeposit.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$AddDeposit$Builder");
                }
            }

            public /* synthetic */ AddDeposit(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static AddDeposit getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_AddDeposit_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static AddDeposit parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (AddDeposit) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static AddDeposit parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<AddDeposit> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof AddDeposit)) {
                    return super.equals(obj);
                }
                AddDeposit addDeposit = (AddDeposit) obj;
                return getBucketIndex() == addDeposit.getBucketIndex() && getAmount().equals(addDeposit.getAmount()) && getPayload().equals(addDeposit.getPayload()) && this.unknownFields.equals(addDeposit.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
            public String getAmount() {
                Object obj = this.amount_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.amount_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
            public ByteString getAmountBytes() {
                Object obj = this.amount_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.amount_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
            public long getBucketIndex() {
                return this.bucketIndex_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<AddDeposit> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.AddDepositOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                long j = this.bucketIndex_;
                int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
                if (!getAmountBytes().isEmpty()) {
                    a0 += GeneratedMessageV3.computeStringSize(2, this.amount_);
                }
                if (!this.payload_.isEmpty()) {
                    a0 += CodedOutputStream.h(3, this.payload_);
                }
                int serializedSize = a0 + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getBucketIndex())) * 37) + 2) * 53) + getAmount().hashCode()) * 37) + 3) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_AddDeposit_fieldAccessorTable.d(AddDeposit.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new AddDeposit();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                long j = this.bucketIndex_;
                if (j != 0) {
                    codedOutputStream.d1(1, j);
                }
                if (!getAmountBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.amount_);
                }
                if (!this.payload_.isEmpty()) {
                    codedOutputStream.q0(3, this.payload_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ AddDeposit(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(AddDeposit addDeposit) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(addDeposit);
            }

            public static AddDeposit parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private AddDeposit(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static AddDeposit parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (AddDeposit) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static AddDeposit parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public AddDeposit getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static AddDeposit parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private AddDeposit() {
                this.memoizedIsInitialized = (byte) -1;
                this.amount_ = "";
                this.payload_ = ByteString.EMPTY;
            }

            public static AddDeposit parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static AddDeposit parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static AddDeposit parseFrom(InputStream inputStream) throws IOException {
                return (AddDeposit) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private AddDeposit(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.bucketIndex_ = jVar.L();
                                } else if (J == 18) {
                                    this.amount_ = jVar.I();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.payload_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static AddDeposit parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (AddDeposit) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static AddDeposit parseFrom(j jVar) throws IOException {
                return (AddDeposit) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static AddDeposit parseFrom(j jVar, r rVar) throws IOException {
                return (AddDeposit) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface AddDepositOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            String getAmount();

            ByteString getAmountBytes();

            long getBucketIndex();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            ByteString getPayload();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements StakingOrBuilder {
            private a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> candidateRegisterBuilder_;
            private a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> candidateUpdateBuilder_;
            private int messageCase_;
            private Object message_;
            private a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> stakeAddDepositBuilder_;
            private a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> stakeChangeCandidateBuilder_;
            private a1<Create, Create.Builder, CreateOrBuilder> stakeCreateBuilder_;
            private a1<Restake, Restake.Builder, RestakeOrBuilder> stakeRestakeBuilder_;
            private a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> stakeTransferOwnershipBuilder_;
            private a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> stakeUnstakeBuilder_;
            private a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> stakeWithdrawBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> getCandidateRegisterFieldBuilder() {
                if (this.candidateRegisterBuilder_ == null) {
                    if (this.messageCase_ != 8) {
                        this.message_ = CandidateRegister.getDefaultInstance();
                    }
                    this.candidateRegisterBuilder_ = new a1<>((CandidateRegister) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 8;
                onChanged();
                return this.candidateRegisterBuilder_;
            }

            private a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> getCandidateUpdateFieldBuilder() {
                if (this.candidateUpdateBuilder_ == null) {
                    if (this.messageCase_ != 9) {
                        this.message_ = CandidateBasicInfo.getDefaultInstance();
                    }
                    this.candidateUpdateBuilder_ = new a1<>((CandidateBasicInfo) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 9;
                onChanged();
                return this.candidateUpdateBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_descriptor;
            }

            private a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> getStakeAddDepositFieldBuilder() {
                if (this.stakeAddDepositBuilder_ == null) {
                    if (this.messageCase_ != 4) {
                        this.message_ = AddDeposit.getDefaultInstance();
                    }
                    this.stakeAddDepositBuilder_ = new a1<>((AddDeposit) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 4;
                onChanged();
                return this.stakeAddDepositBuilder_;
            }

            private a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> getStakeChangeCandidateFieldBuilder() {
                if (this.stakeChangeCandidateBuilder_ == null) {
                    if (this.messageCase_ != 6) {
                        this.message_ = ChangeCandidate.getDefaultInstance();
                    }
                    this.stakeChangeCandidateBuilder_ = new a1<>((ChangeCandidate) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 6;
                onChanged();
                return this.stakeChangeCandidateBuilder_;
            }

            private a1<Create, Create.Builder, CreateOrBuilder> getStakeCreateFieldBuilder() {
                if (this.stakeCreateBuilder_ == null) {
                    if (this.messageCase_ != 1) {
                        this.message_ = Create.getDefaultInstance();
                    }
                    this.stakeCreateBuilder_ = new a1<>((Create) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 1;
                onChanged();
                return this.stakeCreateBuilder_;
            }

            private a1<Restake, Restake.Builder, RestakeOrBuilder> getStakeRestakeFieldBuilder() {
                if (this.stakeRestakeBuilder_ == null) {
                    if (this.messageCase_ != 5) {
                        this.message_ = Restake.getDefaultInstance();
                    }
                    this.stakeRestakeBuilder_ = new a1<>((Restake) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 5;
                onChanged();
                return this.stakeRestakeBuilder_;
            }

            private a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> getStakeTransferOwnershipFieldBuilder() {
                if (this.stakeTransferOwnershipBuilder_ == null) {
                    if (this.messageCase_ != 7) {
                        this.message_ = TransferOwnership.getDefaultInstance();
                    }
                    this.stakeTransferOwnershipBuilder_ = new a1<>((TransferOwnership) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 7;
                onChanged();
                return this.stakeTransferOwnershipBuilder_;
            }

            private a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> getStakeUnstakeFieldBuilder() {
                if (this.stakeUnstakeBuilder_ == null) {
                    if (this.messageCase_ != 2) {
                        this.message_ = Reclaim.getDefaultInstance();
                    }
                    this.stakeUnstakeBuilder_ = new a1<>((Reclaim) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 2;
                onChanged();
                return this.stakeUnstakeBuilder_;
            }

            private a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> getStakeWithdrawFieldBuilder() {
                if (this.stakeWithdrawBuilder_ == null) {
                    if (this.messageCase_ != 3) {
                        this.message_ = Reclaim.getDefaultInstance();
                    }
                    this.stakeWithdrawBuilder_ = new a1<>((Reclaim) this.message_, getParentForChildren(), isClean());
                    this.message_ = null;
                }
                this.messageCase_ = 3;
                onChanged();
                return this.stakeWithdrawBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCandidateRegister() {
                a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 8) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 8) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearCandidateUpdate() {
                a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 9) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 9) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMessage() {
                this.messageCase_ = 0;
                this.message_ = null;
                onChanged();
                return this;
            }

            public Builder clearStakeAddDeposit() {
                a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 4) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 4) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeChangeCandidate() {
                a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 6) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 6) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeCreate() {
                a1<Create, Create.Builder, CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 1) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 1) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeRestake() {
                a1<Restake, Restake.Builder, RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 5) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 5) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeTransferOwnership() {
                a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 7) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 7) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeUnstake() {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 2) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 2) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearStakeWithdraw() {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 3) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageCase_ == 3) {
                        this.messageCase_ = 0;
                        this.message_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public CandidateRegister getCandidateRegister() {
                a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 8) {
                        return (CandidateRegister) this.message_;
                    }
                    return CandidateRegister.getDefaultInstance();
                } else if (this.messageCase_ == 8) {
                    return a1Var.f();
                } else {
                    return CandidateRegister.getDefaultInstance();
                }
            }

            public CandidateRegister.Builder getCandidateRegisterBuilder() {
                return getCandidateRegisterFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public CandidateRegisterOrBuilder getCandidateRegisterOrBuilder() {
                a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 8 || (a1Var = this.candidateRegisterBuilder_) == null) {
                    if (i == 8) {
                        return (CandidateRegister) this.message_;
                    }
                    return CandidateRegister.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public CandidateBasicInfo getCandidateUpdate() {
                a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 9) {
                        return (CandidateBasicInfo) this.message_;
                    }
                    return CandidateBasicInfo.getDefaultInstance();
                } else if (this.messageCase_ == 9) {
                    return a1Var.f();
                } else {
                    return CandidateBasicInfo.getDefaultInstance();
                }
            }

            public CandidateBasicInfo.Builder getCandidateUpdateBuilder() {
                return getCandidateUpdateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder() {
                a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 9 || (a1Var = this.candidateUpdateBuilder_) == null) {
                    if (i == 9) {
                        return (CandidateBasicInfo) this.message_;
                    }
                    return CandidateBasicInfo.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_descriptor;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public MessageCase getMessageCase() {
                return MessageCase.forNumber(this.messageCase_);
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public AddDeposit getStakeAddDeposit() {
                a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 4) {
                        return (AddDeposit) this.message_;
                    }
                    return AddDeposit.getDefaultInstance();
                } else if (this.messageCase_ == 4) {
                    return a1Var.f();
                } else {
                    return AddDeposit.getDefaultInstance();
                }
            }

            public AddDeposit.Builder getStakeAddDepositBuilder() {
                return getStakeAddDepositFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public AddDepositOrBuilder getStakeAddDepositOrBuilder() {
                a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 4 || (a1Var = this.stakeAddDepositBuilder_) == null) {
                    if (i == 4) {
                        return (AddDeposit) this.message_;
                    }
                    return AddDeposit.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public ChangeCandidate getStakeChangeCandidate() {
                a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 6) {
                        return (ChangeCandidate) this.message_;
                    }
                    return ChangeCandidate.getDefaultInstance();
                } else if (this.messageCase_ == 6) {
                    return a1Var.f();
                } else {
                    return ChangeCandidate.getDefaultInstance();
                }
            }

            public ChangeCandidate.Builder getStakeChangeCandidateBuilder() {
                return getStakeChangeCandidateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder() {
                a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 6 || (a1Var = this.stakeChangeCandidateBuilder_) == null) {
                    if (i == 6) {
                        return (ChangeCandidate) this.message_;
                    }
                    return ChangeCandidate.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public Create getStakeCreate() {
                a1<Create, Create.Builder, CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 1) {
                        return (Create) this.message_;
                    }
                    return Create.getDefaultInstance();
                } else if (this.messageCase_ == 1) {
                    return a1Var.f();
                } else {
                    return Create.getDefaultInstance();
                }
            }

            public Create.Builder getStakeCreateBuilder() {
                return getStakeCreateFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public CreateOrBuilder getStakeCreateOrBuilder() {
                a1<Create, Create.Builder, CreateOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 1 || (a1Var = this.stakeCreateBuilder_) == null) {
                    if (i == 1) {
                        return (Create) this.message_;
                    }
                    return Create.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public Restake getStakeRestake() {
                a1<Restake, Restake.Builder, RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 5) {
                        return (Restake) this.message_;
                    }
                    return Restake.getDefaultInstance();
                } else if (this.messageCase_ == 5) {
                    return a1Var.f();
                } else {
                    return Restake.getDefaultInstance();
                }
            }

            public Restake.Builder getStakeRestakeBuilder() {
                return getStakeRestakeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public RestakeOrBuilder getStakeRestakeOrBuilder() {
                a1<Restake, Restake.Builder, RestakeOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 5 || (a1Var = this.stakeRestakeBuilder_) == null) {
                    if (i == 5) {
                        return (Restake) this.message_;
                    }
                    return Restake.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public TransferOwnership getStakeTransferOwnership() {
                a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 7) {
                        return (TransferOwnership) this.message_;
                    }
                    return TransferOwnership.getDefaultInstance();
                } else if (this.messageCase_ == 7) {
                    return a1Var.f();
                } else {
                    return TransferOwnership.getDefaultInstance();
                }
            }

            public TransferOwnership.Builder getStakeTransferOwnershipBuilder() {
                return getStakeTransferOwnershipFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder() {
                a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 7 || (a1Var = this.stakeTransferOwnershipBuilder_) == null) {
                    if (i == 7) {
                        return (TransferOwnership) this.message_;
                    }
                    return TransferOwnership.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public Reclaim getStakeUnstake() {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 2) {
                        return (Reclaim) this.message_;
                    }
                    return Reclaim.getDefaultInstance();
                } else if (this.messageCase_ == 2) {
                    return a1Var.f();
                } else {
                    return Reclaim.getDefaultInstance();
                }
            }

            public Reclaim.Builder getStakeUnstakeBuilder() {
                return getStakeUnstakeFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public ReclaimOrBuilder getStakeUnstakeOrBuilder() {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 2 || (a1Var = this.stakeUnstakeBuilder_) == null) {
                    if (i == 2) {
                        return (Reclaim) this.message_;
                    }
                    return Reclaim.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public Reclaim getStakeWithdraw() {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 3) {
                        return (Reclaim) this.message_;
                    }
                    return Reclaim.getDefaultInstance();
                } else if (this.messageCase_ == 3) {
                    return a1Var.f();
                } else {
                    return Reclaim.getDefaultInstance();
                }
            }

            public Reclaim.Builder getStakeWithdrawBuilder() {
                return getStakeWithdrawFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public ReclaimOrBuilder getStakeWithdrawOrBuilder() {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var;
                int i = this.messageCase_;
                if (i != 3 || (a1Var = this.stakeWithdrawBuilder_) == null) {
                    if (i == 3) {
                        return (Reclaim) this.message_;
                    }
                    return Reclaim.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasCandidateRegister() {
                return this.messageCase_ == 8;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasCandidateUpdate() {
                return this.messageCase_ == 9;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasStakeAddDeposit() {
                return this.messageCase_ == 4;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasStakeChangeCandidate() {
                return this.messageCase_ == 6;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasStakeCreate() {
                return this.messageCase_ == 1;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasStakeRestake() {
                return this.messageCase_ == 5;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasStakeTransferOwnership() {
                return this.messageCase_ == 7;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasStakeUnstake() {
                return this.messageCase_ == 2;
            }

            @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
            public boolean hasStakeWithdraw() {
                return this.messageCase_ == 3;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_fieldAccessorTable.d(Staking.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeCandidateRegister(CandidateRegister candidateRegister) {
                a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 8 && this.message_ != CandidateRegister.getDefaultInstance()) {
                        this.message_ = CandidateRegister.newBuilder((CandidateRegister) this.message_).mergeFrom(candidateRegister).buildPartial();
                    } else {
                        this.message_ = candidateRegister;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 8) {
                        a1Var.h(candidateRegister);
                    }
                    this.candidateRegisterBuilder_.j(candidateRegister);
                }
                this.messageCase_ = 8;
                return this;
            }

            public Builder mergeCandidateUpdate(CandidateBasicInfo candidateBasicInfo) {
                a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 9 && this.message_ != CandidateBasicInfo.getDefaultInstance()) {
                        this.message_ = CandidateBasicInfo.newBuilder((CandidateBasicInfo) this.message_).mergeFrom(candidateBasicInfo).buildPartial();
                    } else {
                        this.message_ = candidateBasicInfo;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 9) {
                        a1Var.h(candidateBasicInfo);
                    }
                    this.candidateUpdateBuilder_.j(candidateBasicInfo);
                }
                this.messageCase_ = 9;
                return this;
            }

            public Builder mergeStakeAddDeposit(AddDeposit addDeposit) {
                a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 4 && this.message_ != AddDeposit.getDefaultInstance()) {
                        this.message_ = AddDeposit.newBuilder((AddDeposit) this.message_).mergeFrom(addDeposit).buildPartial();
                    } else {
                        this.message_ = addDeposit;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 4) {
                        a1Var.h(addDeposit);
                    }
                    this.stakeAddDepositBuilder_.j(addDeposit);
                }
                this.messageCase_ = 4;
                return this;
            }

            public Builder mergeStakeChangeCandidate(ChangeCandidate changeCandidate) {
                a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 6 && this.message_ != ChangeCandidate.getDefaultInstance()) {
                        this.message_ = ChangeCandidate.newBuilder((ChangeCandidate) this.message_).mergeFrom(changeCandidate).buildPartial();
                    } else {
                        this.message_ = changeCandidate;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 6) {
                        a1Var.h(changeCandidate);
                    }
                    this.stakeChangeCandidateBuilder_.j(changeCandidate);
                }
                this.messageCase_ = 6;
                return this;
            }

            public Builder mergeStakeCreate(Create create) {
                a1<Create, Create.Builder, CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 1 && this.message_ != Create.getDefaultInstance()) {
                        this.message_ = Create.newBuilder((Create) this.message_).mergeFrom(create).buildPartial();
                    } else {
                        this.message_ = create;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 1) {
                        a1Var.h(create);
                    }
                    this.stakeCreateBuilder_.j(create);
                }
                this.messageCase_ = 1;
                return this;
            }

            public Builder mergeStakeRestake(Restake restake) {
                a1<Restake, Restake.Builder, RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 5 && this.message_ != Restake.getDefaultInstance()) {
                        this.message_ = Restake.newBuilder((Restake) this.message_).mergeFrom(restake).buildPartial();
                    } else {
                        this.message_ = restake;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 5) {
                        a1Var.h(restake);
                    }
                    this.stakeRestakeBuilder_.j(restake);
                }
                this.messageCase_ = 5;
                return this;
            }

            public Builder mergeStakeTransferOwnership(TransferOwnership transferOwnership) {
                a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 7 && this.message_ != TransferOwnership.getDefaultInstance()) {
                        this.message_ = TransferOwnership.newBuilder((TransferOwnership) this.message_).mergeFrom(transferOwnership).buildPartial();
                    } else {
                        this.message_ = transferOwnership;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 7) {
                        a1Var.h(transferOwnership);
                    }
                    this.stakeTransferOwnershipBuilder_.j(transferOwnership);
                }
                this.messageCase_ = 7;
                return this;
            }

            public Builder mergeStakeUnstake(Reclaim reclaim) {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 2 && this.message_ != Reclaim.getDefaultInstance()) {
                        this.message_ = Reclaim.newBuilder((Reclaim) this.message_).mergeFrom(reclaim).buildPartial();
                    } else {
                        this.message_ = reclaim;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 2) {
                        a1Var.h(reclaim);
                    }
                    this.stakeUnstakeBuilder_.j(reclaim);
                }
                this.messageCase_ = 2;
                return this;
            }

            public Builder mergeStakeWithdraw(Reclaim reclaim) {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    if (this.messageCase_ == 3 && this.message_ != Reclaim.getDefaultInstance()) {
                        this.message_ = Reclaim.newBuilder((Reclaim) this.message_).mergeFrom(reclaim).buildPartial();
                    } else {
                        this.message_ = reclaim;
                    }
                    onChanged();
                } else {
                    if (this.messageCase_ == 3) {
                        a1Var.h(reclaim);
                    }
                    this.stakeWithdrawBuilder_.j(reclaim);
                }
                this.messageCase_ = 3;
                return this;
            }

            public Builder setCandidateRegister(CandidateRegister candidateRegister) {
                a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(candidateRegister);
                    this.message_ = candidateRegister;
                    onChanged();
                } else {
                    a1Var.j(candidateRegister);
                }
                this.messageCase_ = 8;
                return this;
            }

            public Builder setCandidateUpdate(CandidateBasicInfo candidateBasicInfo) {
                a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(candidateBasicInfo);
                    this.message_ = candidateBasicInfo;
                    onChanged();
                } else {
                    a1Var.j(candidateBasicInfo);
                }
                this.messageCase_ = 9;
                return this;
            }

            public Builder setStakeAddDeposit(AddDeposit addDeposit) {
                a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(addDeposit);
                    this.message_ = addDeposit;
                    onChanged();
                } else {
                    a1Var.j(addDeposit);
                }
                this.messageCase_ = 4;
                return this;
            }

            public Builder setStakeChangeCandidate(ChangeCandidate changeCandidate) {
                a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(changeCandidate);
                    this.message_ = changeCandidate;
                    onChanged();
                } else {
                    a1Var.j(changeCandidate);
                }
                this.messageCase_ = 6;
                return this;
            }

            public Builder setStakeCreate(Create create) {
                a1<Create, Create.Builder, CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(create);
                    this.message_ = create;
                    onChanged();
                } else {
                    a1Var.j(create);
                }
                this.messageCase_ = 1;
                return this;
            }

            public Builder setStakeRestake(Restake restake) {
                a1<Restake, Restake.Builder, RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(restake);
                    this.message_ = restake;
                    onChanged();
                } else {
                    a1Var.j(restake);
                }
                this.messageCase_ = 5;
                return this;
            }

            public Builder setStakeTransferOwnership(TransferOwnership transferOwnership) {
                a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transferOwnership);
                    this.message_ = transferOwnership;
                    onChanged();
                } else {
                    a1Var.j(transferOwnership);
                }
                this.messageCase_ = 7;
                return this;
            }

            public Builder setStakeUnstake(Reclaim reclaim) {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(reclaim);
                    this.message_ = reclaim;
                    onChanged();
                } else {
                    a1Var.j(reclaim);
                }
                this.messageCase_ = 2;
                return this;
            }

            public Builder setStakeWithdraw(Reclaim reclaim) {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(reclaim);
                    this.message_ = reclaim;
                    onChanged();
                } else {
                    a1Var.j(reclaim);
                }
                this.messageCase_ = 3;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Staking build() {
                Staking buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Staking buildPartial() {
                Staking staking = new Staking(this, (AnonymousClass1) null);
                if (this.messageCase_ == 1) {
                    a1<Create, Create.Builder, CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                    if (a1Var == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var.b();
                    }
                }
                if (this.messageCase_ == 2) {
                    a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var2 = this.stakeUnstakeBuilder_;
                    if (a1Var2 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var2.b();
                    }
                }
                if (this.messageCase_ == 3) {
                    a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var3 = this.stakeWithdrawBuilder_;
                    if (a1Var3 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var3.b();
                    }
                }
                if (this.messageCase_ == 4) {
                    a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> a1Var4 = this.stakeAddDepositBuilder_;
                    if (a1Var4 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var4.b();
                    }
                }
                if (this.messageCase_ == 5) {
                    a1<Restake, Restake.Builder, RestakeOrBuilder> a1Var5 = this.stakeRestakeBuilder_;
                    if (a1Var5 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var5.b();
                    }
                }
                if (this.messageCase_ == 6) {
                    a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> a1Var6 = this.stakeChangeCandidateBuilder_;
                    if (a1Var6 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var6.b();
                    }
                }
                if (this.messageCase_ == 7) {
                    a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> a1Var7 = this.stakeTransferOwnershipBuilder_;
                    if (a1Var7 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var7.b();
                    }
                }
                if (this.messageCase_ == 8) {
                    a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> a1Var8 = this.candidateRegisterBuilder_;
                    if (a1Var8 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var8.b();
                    }
                }
                if (this.messageCase_ == 9) {
                    a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var9 = this.candidateUpdateBuilder_;
                    if (a1Var9 == null) {
                        staking.message_ = this.message_;
                    } else {
                        staking.message_ = a1Var9.b();
                    }
                }
                staking.messageCase_ = this.messageCase_;
                onBuilt();
                return staking;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Staking getDefaultInstanceForType() {
                return Staking.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.messageCase_ = 0;
                this.message_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Staking) {
                    return mergeFrom((Staking) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setCandidateRegister(CandidateRegister.Builder builder) {
                a1<CandidateRegister, CandidateRegister.Builder, CandidateRegisterOrBuilder> a1Var = this.candidateRegisterBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 8;
                return this;
            }

            public Builder setCandidateUpdate(CandidateBasicInfo.Builder builder) {
                a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateUpdateBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 9;
                return this;
            }

            public Builder setStakeAddDeposit(AddDeposit.Builder builder) {
                a1<AddDeposit, AddDeposit.Builder, AddDepositOrBuilder> a1Var = this.stakeAddDepositBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 4;
                return this;
            }

            public Builder setStakeChangeCandidate(ChangeCandidate.Builder builder) {
                a1<ChangeCandidate, ChangeCandidate.Builder, ChangeCandidateOrBuilder> a1Var = this.stakeChangeCandidateBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 6;
                return this;
            }

            public Builder setStakeCreate(Create.Builder builder) {
                a1<Create, Create.Builder, CreateOrBuilder> a1Var = this.stakeCreateBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 1;
                return this;
            }

            public Builder setStakeRestake(Restake.Builder builder) {
                a1<Restake, Restake.Builder, RestakeOrBuilder> a1Var = this.stakeRestakeBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 5;
                return this;
            }

            public Builder setStakeTransferOwnership(TransferOwnership.Builder builder) {
                a1<TransferOwnership, TransferOwnership.Builder, TransferOwnershipOrBuilder> a1Var = this.stakeTransferOwnershipBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 7;
                return this;
            }

            public Builder setStakeUnstake(Reclaim.Builder builder) {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeUnstakeBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 2;
                return this;
            }

            public Builder setStakeWithdraw(Reclaim.Builder builder) {
                a1<Reclaim, Reclaim.Builder, ReclaimOrBuilder> a1Var = this.stakeWithdrawBuilder_;
                if (a1Var == null) {
                    this.message_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageCase_ = 3;
                return this;
            }

            public Builder mergeFrom(Staking staking) {
                if (staking == Staking.getDefaultInstance()) {
                    return this;
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$IoTeX$Staking$MessageCase[staking.getMessageCase().ordinal()]) {
                    case 1:
                        mergeStakeCreate(staking.getStakeCreate());
                        break;
                    case 2:
                        mergeStakeUnstake(staking.getStakeUnstake());
                        break;
                    case 3:
                        mergeStakeWithdraw(staking.getStakeWithdraw());
                        break;
                    case 4:
                        mergeStakeAddDeposit(staking.getStakeAddDeposit());
                        break;
                    case 5:
                        mergeStakeRestake(staking.getStakeRestake());
                        break;
                    case 6:
                        mergeStakeChangeCandidate(staking.getStakeChangeCandidate());
                        break;
                    case 7:
                        mergeStakeTransferOwnership(staking.getStakeTransferOwnership());
                        break;
                    case 8:
                        mergeCandidateRegister(staking.getCandidateRegister());
                        break;
                    case 9:
                        mergeCandidateUpdate(staking.getCandidateUpdate());
                        break;
                }
                mergeUnknownFields(staking.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.IoTeX.Staking.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.access$13400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.IoTeX$Staking r3 = (wallet.core.jni.proto.IoTeX.Staking) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.IoTeX$Staking r4 = (wallet.core.jni.proto.IoTeX.Staking) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public static final class CandidateBasicInfo extends GeneratedMessageV3 implements CandidateBasicInfoOrBuilder {
            public static final int NAME_FIELD_NUMBER = 1;
            public static final int OPERATORADDRESS_FIELD_NUMBER = 2;
            public static final int REWARDADDRESS_FIELD_NUMBER = 3;
            private static final long serialVersionUID = 0;
            private byte memoizedIsInitialized;
            private volatile Object name_;
            private volatile Object operatorAddress_;
            private volatile Object rewardAddress_;
            private static final CandidateBasicInfo DEFAULT_INSTANCE = new CandidateBasicInfo();
            private static final t0<CandidateBasicInfo> PARSER = new c<CandidateBasicInfo>() { // from class: wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfo.1
                @Override // com.google.protobuf.t0
                public CandidateBasicInfo parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new CandidateBasicInfo(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements CandidateBasicInfoOrBuilder {
                private Object name_;
                private Object operatorAddress_;
                private Object rewardAddress_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearName() {
                    this.name_ = CandidateBasicInfo.getDefaultInstance().getName();
                    onChanged();
                    return this;
                }

                public Builder clearOperatorAddress() {
                    this.operatorAddress_ = CandidateBasicInfo.getDefaultInstance().getOperatorAddress();
                    onChanged();
                    return this;
                }

                public Builder clearRewardAddress() {
                    this.rewardAddress_ = CandidateBasicInfo.getDefaultInstance().getRewardAddress();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
                public String getName() {
                    Object obj = this.name_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.name_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
                public ByteString getNameBytes() {
                    Object obj = this.name_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.name_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
                public String getOperatorAddress() {
                    Object obj = this.operatorAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.operatorAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
                public ByteString getOperatorAddressBytes() {
                    Object obj = this.operatorAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.operatorAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
                public String getRewardAddress() {
                    Object obj = this.rewardAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.rewardAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
                public ByteString getRewardAddressBytes() {
                    Object obj = this.rewardAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.rewardAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_fieldAccessorTable.d(CandidateBasicInfo.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setName(String str) {
                    Objects.requireNonNull(str);
                    this.name_ = str;
                    onChanged();
                    return this;
                }

                public Builder setNameBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.name_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setOperatorAddress(String str) {
                    Objects.requireNonNull(str);
                    this.operatorAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setOperatorAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.operatorAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setRewardAddress(String str) {
                    Objects.requireNonNull(str);
                    this.rewardAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setRewardAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.rewardAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.name_ = "";
                    this.operatorAddress_ = "";
                    this.rewardAddress_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public CandidateBasicInfo build() {
                    CandidateBasicInfo buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public CandidateBasicInfo buildPartial() {
                    CandidateBasicInfo candidateBasicInfo = new CandidateBasicInfo(this, (AnonymousClass1) null);
                    candidateBasicInfo.name_ = this.name_;
                    candidateBasicInfo.operatorAddress_ = this.operatorAddress_;
                    candidateBasicInfo.rewardAddress_ = this.rewardAddress_;
                    onBuilt();
                    return candidateBasicInfo;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public CandidateBasicInfo getDefaultInstanceForType() {
                    return CandidateBasicInfo.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.name_ = "";
                    this.operatorAddress_ = "";
                    this.rewardAddress_ = "";
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof CandidateBasicInfo) {
                        return mergeFrom((CandidateBasicInfo) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.name_ = "";
                    this.operatorAddress_ = "";
                    this.rewardAddress_ = "";
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(CandidateBasicInfo candidateBasicInfo) {
                    if (candidateBasicInfo == CandidateBasicInfo.getDefaultInstance()) {
                        return this;
                    }
                    if (!candidateBasicInfo.getName().isEmpty()) {
                        this.name_ = candidateBasicInfo.name_;
                        onChanged();
                    }
                    if (!candidateBasicInfo.getOperatorAddress().isEmpty()) {
                        this.operatorAddress_ = candidateBasicInfo.operatorAddress_;
                        onChanged();
                    }
                    if (!candidateBasicInfo.getRewardAddress().isEmpty()) {
                        this.rewardAddress_ = candidateBasicInfo.rewardAddress_;
                        onChanged();
                    }
                    mergeUnknownFields(candidateBasicInfo.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfo.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfo.access$10500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$CandidateBasicInfo r3 = (wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfo) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$CandidateBasicInfo r4 = (wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfo) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfo.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$CandidateBasicInfo$Builder");
                }
            }

            public /* synthetic */ CandidateBasicInfo(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static CandidateBasicInfo getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static CandidateBasicInfo parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (CandidateBasicInfo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static CandidateBasicInfo parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<CandidateBasicInfo> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof CandidateBasicInfo)) {
                    return super.equals(obj);
                }
                CandidateBasicInfo candidateBasicInfo = (CandidateBasicInfo) obj;
                return getName().equals(candidateBasicInfo.getName()) && getOperatorAddress().equals(candidateBasicInfo.getOperatorAddress()) && getRewardAddress().equals(candidateBasicInfo.getRewardAddress()) && this.unknownFields.equals(candidateBasicInfo.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
            public String getName() {
                Object obj = this.name_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.name_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
            public ByteString getNameBytes() {
                Object obj = this.name_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.name_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
            public String getOperatorAddress() {
                Object obj = this.operatorAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.operatorAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
            public ByteString getOperatorAddressBytes() {
                Object obj = this.operatorAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.operatorAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<CandidateBasicInfo> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
            public String getRewardAddress() {
                Object obj = this.rewardAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.rewardAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateBasicInfoOrBuilder
            public ByteString getRewardAddressBytes() {
                Object obj = this.rewardAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.rewardAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getNameBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.name_);
                if (!getOperatorAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.operatorAddress_);
                }
                if (!getRewardAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(3, this.rewardAddress_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getName().hashCode()) * 37) + 2) * 53) + getOperatorAddress().hashCode()) * 37) + 3) * 53) + getRewardAddress().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_fieldAccessorTable.d(CandidateBasicInfo.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new CandidateBasicInfo();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getNameBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.name_);
                }
                if (!getOperatorAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.operatorAddress_);
                }
                if (!getRewardAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 3, this.rewardAddress_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ CandidateBasicInfo(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(CandidateBasicInfo candidateBasicInfo) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(candidateBasicInfo);
            }

            public static CandidateBasicInfo parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private CandidateBasicInfo(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static CandidateBasicInfo parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (CandidateBasicInfo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static CandidateBasicInfo parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public CandidateBasicInfo getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static CandidateBasicInfo parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private CandidateBasicInfo() {
                this.memoizedIsInitialized = (byte) -1;
                this.name_ = "";
                this.operatorAddress_ = "";
                this.rewardAddress_ = "";
            }

            public static CandidateBasicInfo parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static CandidateBasicInfo parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static CandidateBasicInfo parseFrom(InputStream inputStream) throws IOException {
                return (CandidateBasicInfo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static CandidateBasicInfo parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (CandidateBasicInfo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private CandidateBasicInfo(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.name_ = jVar.I();
                                } else if (J == 18) {
                                    this.operatorAddress_ = jVar.I();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.rewardAddress_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static CandidateBasicInfo parseFrom(j jVar) throws IOException {
                return (CandidateBasicInfo) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static CandidateBasicInfo parseFrom(j jVar, r rVar) throws IOException {
                return (CandidateBasicInfo) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface CandidateBasicInfoOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            String getName();

            ByteString getNameBytes();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            String getOperatorAddress();

            ByteString getOperatorAddressBytes();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getRewardAddress();

            ByteString getRewardAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class CandidateRegister extends GeneratedMessageV3 implements CandidateRegisterOrBuilder {
            public static final int AUTOSTAKE_FIELD_NUMBER = 4;
            public static final int CANDIDATE_FIELD_NUMBER = 1;
            public static final int OWNERADDRESS_FIELD_NUMBER = 5;
            public static final int PAYLOAD_FIELD_NUMBER = 6;
            public static final int STAKEDAMOUNT_FIELD_NUMBER = 2;
            public static final int STAKEDDURATION_FIELD_NUMBER = 3;
            private static final long serialVersionUID = 0;
            private boolean autoStake_;
            private CandidateBasicInfo candidate_;
            private byte memoizedIsInitialized;
            private volatile Object ownerAddress_;
            private ByteString payload_;
            private volatile Object stakedAmount_;
            private int stakedDuration_;
            private static final CandidateRegister DEFAULT_INSTANCE = new CandidateRegister();
            private static final t0<CandidateRegister> PARSER = new c<CandidateRegister>() { // from class: wallet.core.jni.proto.IoTeX.Staking.CandidateRegister.1
                @Override // com.google.protobuf.t0
                public CandidateRegister parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new CandidateRegister(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements CandidateRegisterOrBuilder {
                private boolean autoStake_;
                private a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> candidateBuilder_;
                private CandidateBasicInfo candidate_;
                private Object ownerAddress_;
                private ByteString payload_;
                private Object stakedAmount_;
                private int stakedDuration_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> getCandidateFieldBuilder() {
                    if (this.candidateBuilder_ == null) {
                        this.candidateBuilder_ = new a1<>(getCandidate(), getParentForChildren(), isClean());
                        this.candidate_ = null;
                    }
                    return this.candidateBuilder_;
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAutoStake() {
                    this.autoStake_ = false;
                    onChanged();
                    return this;
                }

                public Builder clearCandidate() {
                    if (this.candidateBuilder_ == null) {
                        this.candidate_ = null;
                        onChanged();
                    } else {
                        this.candidate_ = null;
                        this.candidateBuilder_ = null;
                    }
                    return this;
                }

                public Builder clearOwnerAddress() {
                    this.ownerAddress_ = CandidateRegister.getDefaultInstance().getOwnerAddress();
                    onChanged();
                    return this;
                }

                public Builder clearPayload() {
                    this.payload_ = CandidateRegister.getDefaultInstance().getPayload();
                    onChanged();
                    return this;
                }

                public Builder clearStakedAmount() {
                    this.stakedAmount_ = CandidateRegister.getDefaultInstance().getStakedAmount();
                    onChanged();
                    return this;
                }

                public Builder clearStakedDuration() {
                    this.stakedDuration_ = 0;
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public boolean getAutoStake() {
                    return this.autoStake_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public CandidateBasicInfo getCandidate() {
                    a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateBuilder_;
                    if (a1Var == null) {
                        CandidateBasicInfo candidateBasicInfo = this.candidate_;
                        return candidateBasicInfo == null ? CandidateBasicInfo.getDefaultInstance() : candidateBasicInfo;
                    }
                    return a1Var.f();
                }

                public CandidateBasicInfo.Builder getCandidateBuilder() {
                    onChanged();
                    return getCandidateFieldBuilder().e();
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public CandidateBasicInfoOrBuilder getCandidateOrBuilder() {
                    a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateBuilder_;
                    if (a1Var != null) {
                        return a1Var.g();
                    }
                    CandidateBasicInfo candidateBasicInfo = this.candidate_;
                    return candidateBasicInfo == null ? CandidateBasicInfo.getDefaultInstance() : candidateBasicInfo;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public String getOwnerAddress() {
                    Object obj = this.ownerAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.ownerAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public ByteString getOwnerAddressBytes() {
                    Object obj = this.ownerAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.ownerAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public ByteString getPayload() {
                    return this.payload_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public String getStakedAmount() {
                    Object obj = this.stakedAmount_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.stakedAmount_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public ByteString getStakedAmountBytes() {
                    Object obj = this.stakedAmount_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.stakedAmount_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public int getStakedDuration() {
                    return this.stakedDuration_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
                public boolean hasCandidate() {
                    return (this.candidateBuilder_ == null && this.candidate_ == null) ? false : true;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_fieldAccessorTable.d(CandidateRegister.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder mergeCandidate(CandidateBasicInfo candidateBasicInfo) {
                    a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateBuilder_;
                    if (a1Var == null) {
                        CandidateBasicInfo candidateBasicInfo2 = this.candidate_;
                        if (candidateBasicInfo2 != null) {
                            this.candidate_ = CandidateBasicInfo.newBuilder(candidateBasicInfo2).mergeFrom(candidateBasicInfo).buildPartial();
                        } else {
                            this.candidate_ = candidateBasicInfo;
                        }
                        onChanged();
                    } else {
                        a1Var.h(candidateBasicInfo);
                    }
                    return this;
                }

                public Builder setAutoStake(boolean z) {
                    this.autoStake_ = z;
                    onChanged();
                    return this;
                }

                public Builder setCandidate(CandidateBasicInfo candidateBasicInfo) {
                    a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateBuilder_;
                    if (a1Var == null) {
                        Objects.requireNonNull(candidateBasicInfo);
                        this.candidate_ = candidateBasicInfo;
                        onChanged();
                    } else {
                        a1Var.j(candidateBasicInfo);
                    }
                    return this;
                }

                public Builder setOwnerAddress(String str) {
                    Objects.requireNonNull(str);
                    this.ownerAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setOwnerAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.ownerAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setPayload(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.payload_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setStakedAmount(String str) {
                    Objects.requireNonNull(str);
                    this.stakedAmount_ = str;
                    onChanged();
                    return this;
                }

                public Builder setStakedAmountBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.stakedAmount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setStakedDuration(int i) {
                    this.stakedDuration_ = i;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.stakedAmount_ = "";
                    this.ownerAddress_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public CandidateRegister build() {
                    CandidateRegister buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public CandidateRegister buildPartial() {
                    CandidateRegister candidateRegister = new CandidateRegister(this, (AnonymousClass1) null);
                    a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateBuilder_;
                    if (a1Var == null) {
                        candidateRegister.candidate_ = this.candidate_;
                    } else {
                        candidateRegister.candidate_ = a1Var.b();
                    }
                    candidateRegister.stakedAmount_ = this.stakedAmount_;
                    candidateRegister.stakedDuration_ = this.stakedDuration_;
                    candidateRegister.autoStake_ = this.autoStake_;
                    candidateRegister.ownerAddress_ = this.ownerAddress_;
                    candidateRegister.payload_ = this.payload_;
                    onBuilt();
                    return candidateRegister;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public CandidateRegister getDefaultInstanceForType() {
                    return CandidateRegister.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    if (this.candidateBuilder_ == null) {
                        this.candidate_ = null;
                    } else {
                        this.candidate_ = null;
                        this.candidateBuilder_ = null;
                    }
                    this.stakedAmount_ = "";
                    this.stakedDuration_ = 0;
                    this.autoStake_ = false;
                    this.ownerAddress_ = "";
                    this.payload_ = ByteString.EMPTY;
                    return this;
                }

                public Builder setCandidate(CandidateBasicInfo.Builder builder) {
                    a1<CandidateBasicInfo, CandidateBasicInfo.Builder, CandidateBasicInfoOrBuilder> a1Var = this.candidateBuilder_;
                    if (a1Var == null) {
                        this.candidate_ = builder.build();
                        onChanged();
                    } else {
                        a1Var.j(builder.build());
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof CandidateRegister) {
                        return mergeFrom((CandidateRegister) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.stakedAmount_ = "";
                    this.ownerAddress_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(CandidateRegister candidateRegister) {
                    if (candidateRegister == CandidateRegister.getDefaultInstance()) {
                        return this;
                    }
                    if (candidateRegister.hasCandidate()) {
                        mergeCandidate(candidateRegister.getCandidate());
                    }
                    if (!candidateRegister.getStakedAmount().isEmpty()) {
                        this.stakedAmount_ = candidateRegister.stakedAmount_;
                        onChanged();
                    }
                    if (candidateRegister.getStakedDuration() != 0) {
                        setStakedDuration(candidateRegister.getStakedDuration());
                    }
                    if (candidateRegister.getAutoStake()) {
                        setAutoStake(candidateRegister.getAutoStake());
                    }
                    if (!candidateRegister.getOwnerAddress().isEmpty()) {
                        this.ownerAddress_ = candidateRegister.ownerAddress_;
                        onChanged();
                    }
                    if (candidateRegister.getPayload() != ByteString.EMPTY) {
                        setPayload(candidateRegister.getPayload());
                    }
                    mergeUnknownFields(candidateRegister.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.CandidateRegister.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.CandidateRegister.access$12300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$CandidateRegister r3 = (wallet.core.jni.proto.IoTeX.Staking.CandidateRegister) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$CandidateRegister r4 = (wallet.core.jni.proto.IoTeX.Staking.CandidateRegister) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.CandidateRegister.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$CandidateRegister$Builder");
                }
            }

            public /* synthetic */ CandidateRegister(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static CandidateRegister getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static CandidateRegister parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (CandidateRegister) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static CandidateRegister parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<CandidateRegister> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof CandidateRegister)) {
                    return super.equals(obj);
                }
                CandidateRegister candidateRegister = (CandidateRegister) obj;
                if (hasCandidate() != candidateRegister.hasCandidate()) {
                    return false;
                }
                return (!hasCandidate() || getCandidate().equals(candidateRegister.getCandidate())) && getStakedAmount().equals(candidateRegister.getStakedAmount()) && getStakedDuration() == candidateRegister.getStakedDuration() && getAutoStake() == candidateRegister.getAutoStake() && getOwnerAddress().equals(candidateRegister.getOwnerAddress()) && getPayload().equals(candidateRegister.getPayload()) && this.unknownFields.equals(candidateRegister.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public boolean getAutoStake() {
                return this.autoStake_;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public CandidateBasicInfo getCandidate() {
                CandidateBasicInfo candidateBasicInfo = this.candidate_;
                return candidateBasicInfo == null ? CandidateBasicInfo.getDefaultInstance() : candidateBasicInfo;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public CandidateBasicInfoOrBuilder getCandidateOrBuilder() {
                return getCandidate();
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public String getOwnerAddress() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.ownerAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public ByteString getOwnerAddressBytes() {
                Object obj = this.ownerAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<CandidateRegister> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int G = this.candidate_ != null ? 0 + CodedOutputStream.G(1, getCandidate()) : 0;
                if (!getStakedAmountBytes().isEmpty()) {
                    G += GeneratedMessageV3.computeStringSize(2, this.stakedAmount_);
                }
                int i2 = this.stakedDuration_;
                if (i2 != 0) {
                    G += CodedOutputStream.Y(3, i2);
                }
                boolean z = this.autoStake_;
                if (z) {
                    G += CodedOutputStream.e(4, z);
                }
                if (!getOwnerAddressBytes().isEmpty()) {
                    G += GeneratedMessageV3.computeStringSize(5, this.ownerAddress_);
                }
                if (!this.payload_.isEmpty()) {
                    G += CodedOutputStream.h(6, this.payload_);
                }
                int serializedSize = G + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public String getStakedAmount() {
                Object obj = this.stakedAmount_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.stakedAmount_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public ByteString getStakedAmountBytes() {
                Object obj = this.stakedAmount_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.stakedAmount_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public int getStakedDuration() {
                return this.stakedDuration_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CandidateRegisterOrBuilder
            public boolean hasCandidate() {
                return this.candidate_ != null;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = 779 + getDescriptor().hashCode();
                if (hasCandidate()) {
                    hashCode = (((hashCode * 37) + 1) * 53) + getCandidate().hashCode();
                }
                int hashCode2 = (((((((((((((((((((((hashCode * 37) + 2) * 53) + getStakedAmount().hashCode()) * 37) + 3) * 53) + getStakedDuration()) * 37) + 4) * 53) + a0.c(getAutoStake())) * 37) + 5) * 53) + getOwnerAddress().hashCode()) * 37) + 6) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode2;
                return hashCode2;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_fieldAccessorTable.d(CandidateRegister.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new CandidateRegister();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (this.candidate_ != null) {
                    codedOutputStream.K0(1, getCandidate());
                }
                if (!getStakedAmountBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.stakedAmount_);
                }
                int i = this.stakedDuration_;
                if (i != 0) {
                    codedOutputStream.b1(3, i);
                }
                boolean z = this.autoStake_;
                if (z) {
                    codedOutputStream.m0(4, z);
                }
                if (!getOwnerAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 5, this.ownerAddress_);
                }
                if (!this.payload_.isEmpty()) {
                    codedOutputStream.q0(6, this.payload_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ CandidateRegister(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(CandidateRegister candidateRegister) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(candidateRegister);
            }

            public static CandidateRegister parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private CandidateRegister(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static CandidateRegister parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (CandidateRegister) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static CandidateRegister parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public CandidateRegister getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static CandidateRegister parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private CandidateRegister() {
                this.memoizedIsInitialized = (byte) -1;
                this.stakedAmount_ = "";
                this.ownerAddress_ = "";
                this.payload_ = ByteString.EMPTY;
            }

            public static CandidateRegister parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static CandidateRegister parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static CandidateRegister parseFrom(InputStream inputStream) throws IOException {
                return (CandidateRegister) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static CandidateRegister parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (CandidateRegister) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private CandidateRegister(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    CandidateBasicInfo candidateBasicInfo = this.candidate_;
                                    CandidateBasicInfo.Builder builder = candidateBasicInfo != null ? candidateBasicInfo.toBuilder() : null;
                                    CandidateBasicInfo candidateBasicInfo2 = (CandidateBasicInfo) jVar.z(CandidateBasicInfo.parser(), rVar);
                                    this.candidate_ = candidateBasicInfo2;
                                    if (builder != null) {
                                        builder.mergeFrom(candidateBasicInfo2);
                                        this.candidate_ = builder.buildPartial();
                                    }
                                } else if (J == 18) {
                                    this.stakedAmount_ = jVar.I();
                                } else if (J == 24) {
                                    this.stakedDuration_ = jVar.K();
                                } else if (J == 32) {
                                    this.autoStake_ = jVar.p();
                                } else if (J == 42) {
                                    this.ownerAddress_ = jVar.I();
                                } else if (J != 50) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.payload_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static CandidateRegister parseFrom(j jVar) throws IOException {
                return (CandidateRegister) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static CandidateRegister parseFrom(j jVar, r rVar) throws IOException {
                return (CandidateRegister) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface CandidateRegisterOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            boolean getAutoStake();

            CandidateBasicInfo getCandidate();

            CandidateBasicInfoOrBuilder getCandidateOrBuilder();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            String getOwnerAddress();

            ByteString getOwnerAddressBytes();

            ByteString getPayload();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getStakedAmount();

            ByteString getStakedAmountBytes();

            int getStakedDuration();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            boolean hasCandidate();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class ChangeCandidate extends GeneratedMessageV3 implements ChangeCandidateOrBuilder {
            public static final int BUCKETINDEX_FIELD_NUMBER = 1;
            public static final int CANDIDATENAME_FIELD_NUMBER = 2;
            private static final ChangeCandidate DEFAULT_INSTANCE = new ChangeCandidate();
            private static final t0<ChangeCandidate> PARSER = new c<ChangeCandidate>() { // from class: wallet.core.jni.proto.IoTeX.Staking.ChangeCandidate.1
                @Override // com.google.protobuf.t0
                public ChangeCandidate parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new ChangeCandidate(jVar, rVar, null);
                }
            };
            public static final int PAYLOAD_FIELD_NUMBER = 3;
            private static final long serialVersionUID = 0;
            private long bucketIndex_;
            private volatile Object candidateName_;
            private byte memoizedIsInitialized;
            private ByteString payload_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ChangeCandidateOrBuilder {
                private long bucketIndex_;
                private Object candidateName_;
                private ByteString payload_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearBucketIndex() {
                    this.bucketIndex_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearCandidateName() {
                    this.candidateName_ = ChangeCandidate.getDefaultInstance().getCandidateName();
                    onChanged();
                    return this;
                }

                public Builder clearPayload() {
                    this.payload_ = ChangeCandidate.getDefaultInstance().getPayload();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
                public long getBucketIndex() {
                    return this.bucketIndex_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
                public String getCandidateName() {
                    Object obj = this.candidateName_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.candidateName_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
                public ByteString getCandidateNameBytes() {
                    Object obj = this.candidateName_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.candidateName_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
                public ByteString getPayload() {
                    return this.payload_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_fieldAccessorTable.d(ChangeCandidate.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setBucketIndex(long j) {
                    this.bucketIndex_ = j;
                    onChanged();
                    return this;
                }

                public Builder setCandidateName(String str) {
                    Objects.requireNonNull(str);
                    this.candidateName_ = str;
                    onChanged();
                    return this;
                }

                public Builder setCandidateNameBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.candidateName_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setPayload(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.payload_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.candidateName_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ChangeCandidate build() {
                    ChangeCandidate buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ChangeCandidate buildPartial() {
                    ChangeCandidate changeCandidate = new ChangeCandidate(this, (AnonymousClass1) null);
                    changeCandidate.bucketIndex_ = this.bucketIndex_;
                    changeCandidate.candidateName_ = this.candidateName_;
                    changeCandidate.payload_ = this.payload_;
                    onBuilt();
                    return changeCandidate;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public ChangeCandidate getDefaultInstanceForType() {
                    return ChangeCandidate.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.bucketIndex_ = 0L;
                    this.candidateName_ = "";
                    this.payload_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.candidateName_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ChangeCandidate) {
                        return mergeFrom((ChangeCandidate) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(ChangeCandidate changeCandidate) {
                    if (changeCandidate == ChangeCandidate.getDefaultInstance()) {
                        return this;
                    }
                    if (changeCandidate.getBucketIndex() != 0) {
                        setBucketIndex(changeCandidate.getBucketIndex());
                    }
                    if (!changeCandidate.getCandidateName().isEmpty()) {
                        this.candidateName_ = changeCandidate.candidateName_;
                        onChanged();
                    }
                    if (changeCandidate.getPayload() != ByteString.EMPTY) {
                        setPayload(changeCandidate.getPayload());
                    }
                    mergeUnknownFields(changeCandidate.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.ChangeCandidate.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.ChangeCandidate.access$7900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$ChangeCandidate r3 = (wallet.core.jni.proto.IoTeX.Staking.ChangeCandidate) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$ChangeCandidate r4 = (wallet.core.jni.proto.IoTeX.Staking.ChangeCandidate) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.ChangeCandidate.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$ChangeCandidate$Builder");
                }
            }

            public /* synthetic */ ChangeCandidate(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static ChangeCandidate getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static ChangeCandidate parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ChangeCandidate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ChangeCandidate parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ChangeCandidate> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ChangeCandidate)) {
                    return super.equals(obj);
                }
                ChangeCandidate changeCandidate = (ChangeCandidate) obj;
                return getBucketIndex() == changeCandidate.getBucketIndex() && getCandidateName().equals(changeCandidate.getCandidateName()) && getPayload().equals(changeCandidate.getPayload()) && this.unknownFields.equals(changeCandidate.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
            public long getBucketIndex() {
                return this.bucketIndex_;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
            public String getCandidateName() {
                Object obj = this.candidateName_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.candidateName_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
            public ByteString getCandidateNameBytes() {
                Object obj = this.candidateName_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.candidateName_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ChangeCandidate> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.ChangeCandidateOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                long j = this.bucketIndex_;
                int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
                if (!getCandidateNameBytes().isEmpty()) {
                    a0 += GeneratedMessageV3.computeStringSize(2, this.candidateName_);
                }
                if (!this.payload_.isEmpty()) {
                    a0 += CodedOutputStream.h(3, this.payload_);
                }
                int serializedSize = a0 + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getBucketIndex())) * 37) + 2) * 53) + getCandidateName().hashCode()) * 37) + 3) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_fieldAccessorTable.d(ChangeCandidate.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ChangeCandidate();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                long j = this.bucketIndex_;
                if (j != 0) {
                    codedOutputStream.d1(1, j);
                }
                if (!getCandidateNameBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.candidateName_);
                }
                if (!this.payload_.isEmpty()) {
                    codedOutputStream.q0(3, this.payload_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ ChangeCandidate(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(ChangeCandidate changeCandidate) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(changeCandidate);
            }

            public static ChangeCandidate parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private ChangeCandidate(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ChangeCandidate parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (ChangeCandidate) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ChangeCandidate parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ChangeCandidate getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static ChangeCandidate parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private ChangeCandidate() {
                this.memoizedIsInitialized = (byte) -1;
                this.candidateName_ = "";
                this.payload_ = ByteString.EMPTY;
            }

            public static ChangeCandidate parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static ChangeCandidate parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static ChangeCandidate parseFrom(InputStream inputStream) throws IOException {
                return (ChangeCandidate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private ChangeCandidate(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.bucketIndex_ = jVar.L();
                                } else if (J == 18) {
                                    this.candidateName_ = jVar.I();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.payload_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ChangeCandidate parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (ChangeCandidate) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static ChangeCandidate parseFrom(j jVar) throws IOException {
                return (ChangeCandidate) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static ChangeCandidate parseFrom(j jVar, r rVar) throws IOException {
                return (ChangeCandidate) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ChangeCandidateOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            long getBucketIndex();

            String getCandidateName();

            ByteString getCandidateNameBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            ByteString getPayload();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Create extends GeneratedMessageV3 implements CreateOrBuilder {
            public static final int AUTOSTAKE_FIELD_NUMBER = 4;
            public static final int CANDIDATENAME_FIELD_NUMBER = 1;
            private static final Create DEFAULT_INSTANCE = new Create();
            private static final t0<Create> PARSER = new c<Create>() { // from class: wallet.core.jni.proto.IoTeX.Staking.Create.1
                @Override // com.google.protobuf.t0
                public Create parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Create(jVar, rVar, null);
                }
            };
            public static final int PAYLOAD_FIELD_NUMBER = 5;
            public static final int STAKEDAMOUNT_FIELD_NUMBER = 2;
            public static final int STAKEDDURATION_FIELD_NUMBER = 3;
            private static final long serialVersionUID = 0;
            private boolean autoStake_;
            private volatile Object candidateName_;
            private byte memoizedIsInitialized;
            private ByteString payload_;
            private volatile Object stakedAmount_;
            private int stakedDuration_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements CreateOrBuilder {
                private boolean autoStake_;
                private Object candidateName_;
                private ByteString payload_;
                private Object stakedAmount_;
                private int stakedDuration_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Create_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAutoStake() {
                    this.autoStake_ = false;
                    onChanged();
                    return this;
                }

                public Builder clearCandidateName() {
                    this.candidateName_ = Create.getDefaultInstance().getCandidateName();
                    onChanged();
                    return this;
                }

                public Builder clearPayload() {
                    this.payload_ = Create.getDefaultInstance().getPayload();
                    onChanged();
                    return this;
                }

                public Builder clearStakedAmount() {
                    this.stakedAmount_ = Create.getDefaultInstance().getStakedAmount();
                    onChanged();
                    return this;
                }

                public Builder clearStakedDuration() {
                    this.stakedDuration_ = 0;
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
                public boolean getAutoStake() {
                    return this.autoStake_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
                public String getCandidateName() {
                    Object obj = this.candidateName_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.candidateName_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
                public ByteString getCandidateNameBytes() {
                    Object obj = this.candidateName_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.candidateName_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Create_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
                public ByteString getPayload() {
                    return this.payload_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
                public String getStakedAmount() {
                    Object obj = this.stakedAmount_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.stakedAmount_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
                public ByteString getStakedAmountBytes() {
                    Object obj = this.stakedAmount_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.stakedAmount_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
                public int getStakedDuration() {
                    return this.stakedDuration_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Create_fieldAccessorTable.d(Create.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAutoStake(boolean z) {
                    this.autoStake_ = z;
                    onChanged();
                    return this;
                }

                public Builder setCandidateName(String str) {
                    Objects.requireNonNull(str);
                    this.candidateName_ = str;
                    onChanged();
                    return this;
                }

                public Builder setCandidateNameBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.candidateName_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setPayload(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.payload_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setStakedAmount(String str) {
                    Objects.requireNonNull(str);
                    this.stakedAmount_ = str;
                    onChanged();
                    return this;
                }

                public Builder setStakedAmountBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.stakedAmount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setStakedDuration(int i) {
                    this.stakedDuration_ = i;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.candidateName_ = "";
                    this.stakedAmount_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Create build() {
                    Create buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Create buildPartial() {
                    Create create = new Create(this, (AnonymousClass1) null);
                    create.candidateName_ = this.candidateName_;
                    create.stakedAmount_ = this.stakedAmount_;
                    create.stakedDuration_ = this.stakedDuration_;
                    create.autoStake_ = this.autoStake_;
                    create.payload_ = this.payload_;
                    onBuilt();
                    return create;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Create getDefaultInstanceForType() {
                    return Create.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.candidateName_ = "";
                    this.stakedAmount_ = "";
                    this.stakedDuration_ = 0;
                    this.autoStake_ = false;
                    this.payload_ = ByteString.EMPTY;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Create) {
                        return mergeFrom((Create) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.candidateName_ = "";
                    this.stakedAmount_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(Create create) {
                    if (create == Create.getDefaultInstance()) {
                        return this;
                    }
                    if (!create.getCandidateName().isEmpty()) {
                        this.candidateName_ = create.candidateName_;
                        onChanged();
                    }
                    if (!create.getStakedAmount().isEmpty()) {
                        this.stakedAmount_ = create.stakedAmount_;
                        onChanged();
                    }
                    if (create.getStakedDuration() != 0) {
                        setStakedDuration(create.getStakedDuration());
                    }
                    if (create.getAutoStake()) {
                        setAutoStake(create.getAutoStake());
                    }
                    if (create.getPayload() != ByteString.EMPTY) {
                        setPayload(create.getPayload());
                    }
                    mergeUnknownFields(create.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.Create.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.Create.access$2800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$Create r3 = (wallet.core.jni.proto.IoTeX.Staking.Create) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$Create r4 = (wallet.core.jni.proto.IoTeX.Staking.Create) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.Create.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$Create$Builder");
                }
            }

            public /* synthetic */ Create(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Create getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Create_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Create parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Create) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Create parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Create> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Create)) {
                    return super.equals(obj);
                }
                Create create = (Create) obj;
                return getCandidateName().equals(create.getCandidateName()) && getStakedAmount().equals(create.getStakedAmount()) && getStakedDuration() == create.getStakedDuration() && getAutoStake() == create.getAutoStake() && getPayload().equals(create.getPayload()) && this.unknownFields.equals(create.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
            public boolean getAutoStake() {
                return this.autoStake_;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
            public String getCandidateName() {
                Object obj = this.candidateName_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.candidateName_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
            public ByteString getCandidateNameBytes() {
                Object obj = this.candidateName_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.candidateName_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Create> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getCandidateNameBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.candidateName_);
                if (!getStakedAmountBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.stakedAmount_);
                }
                int i2 = this.stakedDuration_;
                if (i2 != 0) {
                    computeStringSize += CodedOutputStream.Y(3, i2);
                }
                boolean z = this.autoStake_;
                if (z) {
                    computeStringSize += CodedOutputStream.e(4, z);
                }
                if (!this.payload_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(5, this.payload_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
            public String getStakedAmount() {
                Object obj = this.stakedAmount_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.stakedAmount_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
            public ByteString getStakedAmountBytes() {
                Object obj = this.stakedAmount_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.stakedAmount_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.CreateOrBuilder
            public int getStakedDuration() {
                return this.stakedDuration_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getCandidateName().hashCode()) * 37) + 2) * 53) + getStakedAmount().hashCode()) * 37) + 3) * 53) + getStakedDuration()) * 37) + 4) * 53) + a0.c(getAutoStake())) * 37) + 5) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Create_fieldAccessorTable.d(Create.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Create();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getCandidateNameBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.candidateName_);
                }
                if (!getStakedAmountBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.stakedAmount_);
                }
                int i = this.stakedDuration_;
                if (i != 0) {
                    codedOutputStream.b1(3, i);
                }
                boolean z = this.autoStake_;
                if (z) {
                    codedOutputStream.m0(4, z);
                }
                if (!this.payload_.isEmpty()) {
                    codedOutputStream.q0(5, this.payload_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Create(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Create create) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(create);
            }

            public static Create parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Create(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Create parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Create) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Create parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Create getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Create parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Create() {
                this.memoizedIsInitialized = (byte) -1;
                this.candidateName_ = "";
                this.stakedAmount_ = "";
                this.payload_ = ByteString.EMPTY;
            }

            public static Create parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Create parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Create parseFrom(InputStream inputStream) throws IOException {
                return (Create) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static Create parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Create) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private Create(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.candidateName_ = jVar.I();
                                    } else if (J == 18) {
                                        this.stakedAmount_ = jVar.I();
                                    } else if (J == 24) {
                                        this.stakedDuration_ = jVar.K();
                                    } else if (J == 32) {
                                        this.autoStake_ = jVar.p();
                                    } else if (J != 42) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.payload_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (IOException e) {
                                throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                            }
                        } catch (InvalidProtocolBufferException e2) {
                            throw e2.setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Create parseFrom(j jVar) throws IOException {
                return (Create) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Create parseFrom(j jVar, r rVar) throws IOException {
                return (Create) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface CreateOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            boolean getAutoStake();

            String getCandidateName();

            ByteString getCandidateNameBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            ByteString getPayload();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getStakedAmount();

            ByteString getStakedAmountBytes();

            int getStakedDuration();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public enum MessageCase implements a0.c {
            STAKECREATE(1),
            STAKEUNSTAKE(2),
            STAKEWITHDRAW(3),
            STAKEADDDEPOSIT(4),
            STAKERESTAKE(5),
            STAKECHANGECANDIDATE(6),
            STAKETRANSFEROWNERSHIP(7),
            CANDIDATEREGISTER(8),
            CANDIDATEUPDATE(9),
            MESSAGE_NOT_SET(0);
            
            private final int value;

            MessageCase(int i) {
                this.value = i;
            }

            public static MessageCase forNumber(int i) {
                switch (i) {
                    case 0:
                        return MESSAGE_NOT_SET;
                    case 1:
                        return STAKECREATE;
                    case 2:
                        return STAKEUNSTAKE;
                    case 3:
                        return STAKEWITHDRAW;
                    case 4:
                        return STAKEADDDEPOSIT;
                    case 5:
                        return STAKERESTAKE;
                    case 6:
                        return STAKECHANGECANDIDATE;
                    case 7:
                        return STAKETRANSFEROWNERSHIP;
                    case 8:
                        return CANDIDATEREGISTER;
                    case 9:
                        return CANDIDATEUPDATE;
                    default:
                        return null;
                }
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class Reclaim extends GeneratedMessageV3 implements ReclaimOrBuilder {
            public static final int BUCKETINDEX_FIELD_NUMBER = 1;
            private static final Reclaim DEFAULT_INSTANCE = new Reclaim();
            private static final t0<Reclaim> PARSER = new c<Reclaim>() { // from class: wallet.core.jni.proto.IoTeX.Staking.Reclaim.1
                @Override // com.google.protobuf.t0
                public Reclaim parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Reclaim(jVar, rVar, null);
                }
            };
            public static final int PAYLOAD_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private long bucketIndex_;
            private byte memoizedIsInitialized;
            private ByteString payload_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ReclaimOrBuilder {
                private long bucketIndex_;
                private ByteString payload_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Reclaim_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearBucketIndex() {
                    this.bucketIndex_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearPayload() {
                    this.payload_ = Reclaim.getDefaultInstance().getPayload();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.ReclaimOrBuilder
                public long getBucketIndex() {
                    return this.bucketIndex_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Reclaim_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.ReclaimOrBuilder
                public ByteString getPayload() {
                    return this.payload_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Reclaim_fieldAccessorTable.d(Reclaim.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setBucketIndex(long j) {
                    this.bucketIndex_ = j;
                    onChanged();
                    return this;
                }

                public Builder setPayload(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.payload_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Reclaim build() {
                    Reclaim buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Reclaim buildPartial() {
                    Reclaim reclaim = new Reclaim(this, (AnonymousClass1) null);
                    reclaim.bucketIndex_ = this.bucketIndex_;
                    reclaim.payload_ = this.payload_;
                    onBuilt();
                    return reclaim;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Reclaim getDefaultInstanceForType() {
                    return Reclaim.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.bucketIndex_ = 0L;
                    this.payload_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Reclaim) {
                        return mergeFrom((Reclaim) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Reclaim reclaim) {
                    if (reclaim == Reclaim.getDefaultInstance()) {
                        return this;
                    }
                    if (reclaim.getBucketIndex() != 0) {
                        setBucketIndex(reclaim.getBucketIndex());
                    }
                    if (reclaim.getPayload() != ByteString.EMPTY) {
                        setPayload(reclaim.getPayload());
                    }
                    mergeUnknownFields(reclaim.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.Reclaim.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.Reclaim.access$4100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$Reclaim r3 = (wallet.core.jni.proto.IoTeX.Staking.Reclaim) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$Reclaim r4 = (wallet.core.jni.proto.IoTeX.Staking.Reclaim) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.Reclaim.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$Reclaim$Builder");
                }
            }

            public /* synthetic */ Reclaim(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Reclaim getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Reclaim_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Reclaim parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Reclaim) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Reclaim parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Reclaim> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Reclaim)) {
                    return super.equals(obj);
                }
                Reclaim reclaim = (Reclaim) obj;
                return getBucketIndex() == reclaim.getBucketIndex() && getPayload().equals(reclaim.getPayload()) && this.unknownFields.equals(reclaim.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.ReclaimOrBuilder
            public long getBucketIndex() {
                return this.bucketIndex_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Reclaim> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.ReclaimOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                long j = this.bucketIndex_;
                int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
                if (!this.payload_.isEmpty()) {
                    a0 += CodedOutputStream.h(2, this.payload_);
                }
                int serializedSize = a0 + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getBucketIndex())) * 37) + 2) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Reclaim_fieldAccessorTable.d(Reclaim.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Reclaim();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                long j = this.bucketIndex_;
                if (j != 0) {
                    codedOutputStream.d1(1, j);
                }
                if (!this.payload_.isEmpty()) {
                    codedOutputStream.q0(2, this.payload_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Reclaim(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Reclaim reclaim) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(reclaim);
            }

            public static Reclaim parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Reclaim(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Reclaim parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Reclaim) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Reclaim parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Reclaim getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Reclaim parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Reclaim() {
                this.memoizedIsInitialized = (byte) -1;
                this.payload_ = ByteString.EMPTY;
            }

            public static Reclaim parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Reclaim parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Reclaim parseFrom(InputStream inputStream) throws IOException {
                return (Reclaim) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Reclaim(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 8) {
                                        this.bucketIndex_ = jVar.L();
                                    } else if (J != 18) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.payload_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Reclaim parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Reclaim) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Reclaim parseFrom(j jVar) throws IOException {
                return (Reclaim) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Reclaim parseFrom(j jVar, r rVar) throws IOException {
                return (Reclaim) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ReclaimOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            long getBucketIndex();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            ByteString getPayload();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Restake extends GeneratedMessageV3 implements RestakeOrBuilder {
            public static final int AUTOSTAKE_FIELD_NUMBER = 3;
            public static final int BUCKETINDEX_FIELD_NUMBER = 1;
            private static final Restake DEFAULT_INSTANCE = new Restake();
            private static final t0<Restake> PARSER = new c<Restake>() { // from class: wallet.core.jni.proto.IoTeX.Staking.Restake.1
                @Override // com.google.protobuf.t0
                public Restake parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Restake(jVar, rVar, null);
                }
            };
            public static final int PAYLOAD_FIELD_NUMBER = 4;
            public static final int STAKEDDURATION_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private boolean autoStake_;
            private long bucketIndex_;
            private byte memoizedIsInitialized;
            private ByteString payload_;
            private int stakedDuration_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements RestakeOrBuilder {
                private boolean autoStake_;
                private long bucketIndex_;
                private ByteString payload_;
                private int stakedDuration_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Restake_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAutoStake() {
                    this.autoStake_ = false;
                    onChanged();
                    return this;
                }

                public Builder clearBucketIndex() {
                    this.bucketIndex_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearPayload() {
                    this.payload_ = Restake.getDefaultInstance().getPayload();
                    onChanged();
                    return this;
                }

                public Builder clearStakedDuration() {
                    this.stakedDuration_ = 0;
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
                public boolean getAutoStake() {
                    return this.autoStake_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
                public long getBucketIndex() {
                    return this.bucketIndex_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Restake_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
                public ByteString getPayload() {
                    return this.payload_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
                public int getStakedDuration() {
                    return this.stakedDuration_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Restake_fieldAccessorTable.d(Restake.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAutoStake(boolean z) {
                    this.autoStake_ = z;
                    onChanged();
                    return this;
                }

                public Builder setBucketIndex(long j) {
                    this.bucketIndex_ = j;
                    onChanged();
                    return this;
                }

                public Builder setPayload(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.payload_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setStakedDuration(int i) {
                    this.stakedDuration_ = i;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Restake build() {
                    Restake buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Restake buildPartial() {
                    Restake restake = new Restake(this, (AnonymousClass1) null);
                    restake.bucketIndex_ = this.bucketIndex_;
                    restake.stakedDuration_ = this.stakedDuration_;
                    restake.autoStake_ = this.autoStake_;
                    restake.payload_ = this.payload_;
                    onBuilt();
                    return restake;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Restake getDefaultInstanceForType() {
                    return Restake.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.bucketIndex_ = 0L;
                    this.stakedDuration_ = 0;
                    this.autoStake_ = false;
                    this.payload_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Restake) {
                        return mergeFrom((Restake) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Restake restake) {
                    if (restake == Restake.getDefaultInstance()) {
                        return this;
                    }
                    if (restake.getBucketIndex() != 0) {
                        setBucketIndex(restake.getBucketIndex());
                    }
                    if (restake.getStakedDuration() != 0) {
                        setStakedDuration(restake.getStakedDuration());
                    }
                    if (restake.getAutoStake()) {
                        setAutoStake(restake.getAutoStake());
                    }
                    if (restake.getPayload() != ByteString.EMPTY) {
                        setPayload(restake.getPayload());
                    }
                    mergeUnknownFields(restake.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.Restake.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.Restake.access$6700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$Restake r3 = (wallet.core.jni.proto.IoTeX.Staking.Restake) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$Restake r4 = (wallet.core.jni.proto.IoTeX.Staking.Restake) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.Restake.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$Restake$Builder");
                }
            }

            public /* synthetic */ Restake(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Restake getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Restake_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Restake parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Restake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Restake parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Restake> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Restake)) {
                    return super.equals(obj);
                }
                Restake restake = (Restake) obj;
                return getBucketIndex() == restake.getBucketIndex() && getStakedDuration() == restake.getStakedDuration() && getAutoStake() == restake.getAutoStake() && getPayload().equals(restake.getPayload()) && this.unknownFields.equals(restake.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
            public boolean getAutoStake() {
                return this.autoStake_;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
            public long getBucketIndex() {
                return this.bucketIndex_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Restake> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                long j = this.bucketIndex_;
                int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
                int i2 = this.stakedDuration_;
                if (i2 != 0) {
                    a0 += CodedOutputStream.Y(2, i2);
                }
                boolean z = this.autoStake_;
                if (z) {
                    a0 += CodedOutputStream.e(3, z);
                }
                if (!this.payload_.isEmpty()) {
                    a0 += CodedOutputStream.h(4, this.payload_);
                }
                int serializedSize = a0 + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.RestakeOrBuilder
            public int getStakedDuration() {
                return this.stakedDuration_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getBucketIndex())) * 37) + 2) * 53) + getStakedDuration()) * 37) + 3) * 53) + a0.c(getAutoStake())) * 37) + 4) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_Restake_fieldAccessorTable.d(Restake.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Restake();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                long j = this.bucketIndex_;
                if (j != 0) {
                    codedOutputStream.d1(1, j);
                }
                int i = this.stakedDuration_;
                if (i != 0) {
                    codedOutputStream.b1(2, i);
                }
                boolean z = this.autoStake_;
                if (z) {
                    codedOutputStream.m0(3, z);
                }
                if (!this.payload_.isEmpty()) {
                    codedOutputStream.q0(4, this.payload_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Restake(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Restake restake) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(restake);
            }

            public static Restake parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Restake(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Restake parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Restake) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Restake parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Restake getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Restake parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Restake() {
                this.memoizedIsInitialized = (byte) -1;
                this.payload_ = ByteString.EMPTY;
            }

            public static Restake parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Restake parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Restake parseFrom(InputStream inputStream) throws IOException {
                return (Restake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Restake(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.bucketIndex_ = jVar.L();
                                } else if (J == 16) {
                                    this.stakedDuration_ = jVar.K();
                                } else if (J == 24) {
                                    this.autoStake_ = jVar.p();
                                } else if (J != 34) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.payload_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Restake parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Restake) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Restake parseFrom(j jVar) throws IOException {
                return (Restake) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Restake parseFrom(j jVar, r rVar) throws IOException {
                return (Restake) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface RestakeOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            boolean getAutoStake();

            long getBucketIndex();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            ByteString getPayload();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            int getStakedDuration();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class TransferOwnership extends GeneratedMessageV3 implements TransferOwnershipOrBuilder {
            public static final int BUCKETINDEX_FIELD_NUMBER = 1;
            private static final TransferOwnership DEFAULT_INSTANCE = new TransferOwnership();
            private static final t0<TransferOwnership> PARSER = new c<TransferOwnership>() { // from class: wallet.core.jni.proto.IoTeX.Staking.TransferOwnership.1
                @Override // com.google.protobuf.t0
                public TransferOwnership parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new TransferOwnership(jVar, rVar, null);
                }
            };
            public static final int PAYLOAD_FIELD_NUMBER = 3;
            public static final int VOTERADDRESS_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private long bucketIndex_;
            private byte memoizedIsInitialized;
            private ByteString payload_;
            private volatile Object voterAddress_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOwnershipOrBuilder {
                private long bucketIndex_;
                private ByteString payload_;
                private Object voterAddress_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearBucketIndex() {
                    this.bucketIndex_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearPayload() {
                    this.payload_ = TransferOwnership.getDefaultInstance().getPayload();
                    onChanged();
                    return this;
                }

                public Builder clearVoterAddress() {
                    this.voterAddress_ = TransferOwnership.getDefaultInstance().getVoterAddress();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
                public long getBucketIndex() {
                    return this.bucketIndex_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_descriptor;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
                public ByteString getPayload() {
                    return this.payload_;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
                public String getVoterAddress() {
                    Object obj = this.voterAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.voterAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
                public ByteString getVoterAddressBytes() {
                    Object obj = this.voterAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.voterAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return IoTeX.internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_fieldAccessorTable.d(TransferOwnership.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setBucketIndex(long j) {
                    this.bucketIndex_ = j;
                    onChanged();
                    return this;
                }

                public Builder setPayload(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.payload_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setVoterAddress(String str) {
                    Objects.requireNonNull(str);
                    this.voterAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setVoterAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.voterAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.voterAddress_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public TransferOwnership build() {
                    TransferOwnership buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public TransferOwnership buildPartial() {
                    TransferOwnership transferOwnership = new TransferOwnership(this, (AnonymousClass1) null);
                    transferOwnership.bucketIndex_ = this.bucketIndex_;
                    transferOwnership.voterAddress_ = this.voterAddress_;
                    transferOwnership.payload_ = this.payload_;
                    onBuilt();
                    return transferOwnership;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public TransferOwnership getDefaultInstanceForType() {
                    return TransferOwnership.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.bucketIndex_ = 0L;
                    this.voterAddress_ = "";
                    this.payload_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.voterAddress_ = "";
                    this.payload_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof TransferOwnership) {
                        return mergeFrom((TransferOwnership) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(TransferOwnership transferOwnership) {
                    if (transferOwnership == TransferOwnership.getDefaultInstance()) {
                        return this;
                    }
                    if (transferOwnership.getBucketIndex() != 0) {
                        setBucketIndex(transferOwnership.getBucketIndex());
                    }
                    if (!transferOwnership.getVoterAddress().isEmpty()) {
                        this.voterAddress_ = transferOwnership.voterAddress_;
                        onChanged();
                    }
                    if (transferOwnership.getPayload() != ByteString.EMPTY) {
                        setPayload(transferOwnership.getPayload());
                    }
                    mergeUnknownFields(transferOwnership.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.IoTeX.Staking.TransferOwnership.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Staking.TransferOwnership.access$9200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.IoTeX$Staking$TransferOwnership r3 = (wallet.core.jni.proto.IoTeX.Staking.TransferOwnership) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.IoTeX$Staking$TransferOwnership r4 = (wallet.core.jni.proto.IoTeX.Staking.TransferOwnership) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Staking.TransferOwnership.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Staking$TransferOwnership$Builder");
                }
            }

            public /* synthetic */ TransferOwnership(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static TransferOwnership getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static TransferOwnership parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (TransferOwnership) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static TransferOwnership parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<TransferOwnership> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof TransferOwnership)) {
                    return super.equals(obj);
                }
                TransferOwnership transferOwnership = (TransferOwnership) obj;
                return getBucketIndex() == transferOwnership.getBucketIndex() && getVoterAddress().equals(transferOwnership.getVoterAddress()) && getPayload().equals(transferOwnership.getPayload()) && this.unknownFields.equals(transferOwnership.unknownFields);
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
            public long getBucketIndex() {
                return this.bucketIndex_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<TransferOwnership> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                long j = this.bucketIndex_;
                int a0 = j != 0 ? 0 + CodedOutputStream.a0(1, j) : 0;
                if (!getVoterAddressBytes().isEmpty()) {
                    a0 += GeneratedMessageV3.computeStringSize(2, this.voterAddress_);
                }
                if (!this.payload_.isEmpty()) {
                    a0 += CodedOutputStream.h(3, this.payload_);
                }
                int serializedSize = a0 + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
            public String getVoterAddress() {
                Object obj = this.voterAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.voterAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.IoTeX.Staking.TransferOwnershipOrBuilder
            public ByteString getVoterAddressBytes() {
                Object obj = this.voterAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.voterAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getBucketIndex())) * 37) + 2) * 53) + getVoterAddress().hashCode()) * 37) + 3) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_fieldAccessorTable.d(TransferOwnership.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new TransferOwnership();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                long j = this.bucketIndex_;
                if (j != 0) {
                    codedOutputStream.d1(1, j);
                }
                if (!getVoterAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.voterAddress_);
                }
                if (!this.payload_.isEmpty()) {
                    codedOutputStream.q0(3, this.payload_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ TransferOwnership(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(TransferOwnership transferOwnership) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(transferOwnership);
            }

            public static TransferOwnership parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private TransferOwnership(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static TransferOwnership parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (TransferOwnership) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static TransferOwnership parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public TransferOwnership getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static TransferOwnership parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private TransferOwnership() {
                this.memoizedIsInitialized = (byte) -1;
                this.voterAddress_ = "";
                this.payload_ = ByteString.EMPTY;
            }

            public static TransferOwnership parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static TransferOwnership parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static TransferOwnership parseFrom(InputStream inputStream) throws IOException {
                return (TransferOwnership) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private TransferOwnership(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.bucketIndex_ = jVar.L();
                                } else if (J == 18) {
                                    this.voterAddress_ = jVar.I();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.payload_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static TransferOwnership parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (TransferOwnership) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static TransferOwnership parseFrom(j jVar) throws IOException {
                return (TransferOwnership) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static TransferOwnership parseFrom(j jVar, r rVar) throws IOException {
                return (TransferOwnership) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface TransferOwnershipOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            long getBucketIndex();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            ByteString getPayload();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            String getVoterAddress();

            ByteString getVoterAddressBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ Staking(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Staking getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return IoTeX.internal_static_TW_IoTeX_Proto_Staking_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Staking parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Staking) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Staking parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Staking> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Staking)) {
                return super.equals(obj);
            }
            Staking staking = (Staking) obj;
            if (getMessageCase().equals(staking.getMessageCase())) {
                switch (this.messageCase_) {
                    case 1:
                        if (!getStakeCreate().equals(staking.getStakeCreate())) {
                            return false;
                        }
                        break;
                    case 2:
                        if (!getStakeUnstake().equals(staking.getStakeUnstake())) {
                            return false;
                        }
                        break;
                    case 3:
                        if (!getStakeWithdraw().equals(staking.getStakeWithdraw())) {
                            return false;
                        }
                        break;
                    case 4:
                        if (!getStakeAddDeposit().equals(staking.getStakeAddDeposit())) {
                            return false;
                        }
                        break;
                    case 5:
                        if (!getStakeRestake().equals(staking.getStakeRestake())) {
                            return false;
                        }
                        break;
                    case 6:
                        if (!getStakeChangeCandidate().equals(staking.getStakeChangeCandidate())) {
                            return false;
                        }
                        break;
                    case 7:
                        if (!getStakeTransferOwnership().equals(staking.getStakeTransferOwnership())) {
                            return false;
                        }
                        break;
                    case 8:
                        if (!getCandidateRegister().equals(staking.getCandidateRegister())) {
                            return false;
                        }
                        break;
                    case 9:
                        if (!getCandidateUpdate().equals(staking.getCandidateUpdate())) {
                            return false;
                        }
                        break;
                }
                return this.unknownFields.equals(staking.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public CandidateRegister getCandidateRegister() {
            if (this.messageCase_ == 8) {
                return (CandidateRegister) this.message_;
            }
            return CandidateRegister.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public CandidateRegisterOrBuilder getCandidateRegisterOrBuilder() {
            if (this.messageCase_ == 8) {
                return (CandidateRegister) this.message_;
            }
            return CandidateRegister.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public CandidateBasicInfo getCandidateUpdate() {
            if (this.messageCase_ == 9) {
                return (CandidateBasicInfo) this.message_;
            }
            return CandidateBasicInfo.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder() {
            if (this.messageCase_ == 9) {
                return (CandidateBasicInfo) this.message_;
            }
            return CandidateBasicInfo.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public MessageCase getMessageCase() {
            return MessageCase.forNumber(this.messageCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Staking> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.messageCase_ == 1 ? 0 + CodedOutputStream.G(1, (Create) this.message_) : 0;
            if (this.messageCase_ == 2) {
                G += CodedOutputStream.G(2, (Reclaim) this.message_);
            }
            if (this.messageCase_ == 3) {
                G += CodedOutputStream.G(3, (Reclaim) this.message_);
            }
            if (this.messageCase_ == 4) {
                G += CodedOutputStream.G(4, (AddDeposit) this.message_);
            }
            if (this.messageCase_ == 5) {
                G += CodedOutputStream.G(5, (Restake) this.message_);
            }
            if (this.messageCase_ == 6) {
                G += CodedOutputStream.G(6, (ChangeCandidate) this.message_);
            }
            if (this.messageCase_ == 7) {
                G += CodedOutputStream.G(7, (TransferOwnership) this.message_);
            }
            if (this.messageCase_ == 8) {
                G += CodedOutputStream.G(8, (CandidateRegister) this.message_);
            }
            if (this.messageCase_ == 9) {
                G += CodedOutputStream.G(9, (CandidateBasicInfo) this.message_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public AddDeposit getStakeAddDeposit() {
            if (this.messageCase_ == 4) {
                return (AddDeposit) this.message_;
            }
            return AddDeposit.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public AddDepositOrBuilder getStakeAddDepositOrBuilder() {
            if (this.messageCase_ == 4) {
                return (AddDeposit) this.message_;
            }
            return AddDeposit.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public ChangeCandidate getStakeChangeCandidate() {
            if (this.messageCase_ == 6) {
                return (ChangeCandidate) this.message_;
            }
            return ChangeCandidate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder() {
            if (this.messageCase_ == 6) {
                return (ChangeCandidate) this.message_;
            }
            return ChangeCandidate.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public Create getStakeCreate() {
            if (this.messageCase_ == 1) {
                return (Create) this.message_;
            }
            return Create.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public CreateOrBuilder getStakeCreateOrBuilder() {
            if (this.messageCase_ == 1) {
                return (Create) this.message_;
            }
            return Create.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public Restake getStakeRestake() {
            if (this.messageCase_ == 5) {
                return (Restake) this.message_;
            }
            return Restake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public RestakeOrBuilder getStakeRestakeOrBuilder() {
            if (this.messageCase_ == 5) {
                return (Restake) this.message_;
            }
            return Restake.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public TransferOwnership getStakeTransferOwnership() {
            if (this.messageCase_ == 7) {
                return (TransferOwnership) this.message_;
            }
            return TransferOwnership.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder() {
            if (this.messageCase_ == 7) {
                return (TransferOwnership) this.message_;
            }
            return TransferOwnership.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public Reclaim getStakeUnstake() {
            if (this.messageCase_ == 2) {
                return (Reclaim) this.message_;
            }
            return Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public ReclaimOrBuilder getStakeUnstakeOrBuilder() {
            if (this.messageCase_ == 2) {
                return (Reclaim) this.message_;
            }
            return Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public Reclaim getStakeWithdraw() {
            if (this.messageCase_ == 3) {
                return (Reclaim) this.message_;
            }
            return Reclaim.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public ReclaimOrBuilder getStakeWithdrawOrBuilder() {
            if (this.messageCase_ == 3) {
                return (Reclaim) this.message_;
            }
            return Reclaim.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasCandidateRegister() {
            return this.messageCase_ == 8;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasCandidateUpdate() {
            return this.messageCase_ == 9;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasStakeAddDeposit() {
            return this.messageCase_ == 4;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasStakeChangeCandidate() {
            return this.messageCase_ == 6;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasStakeCreate() {
            return this.messageCase_ == 1;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasStakeRestake() {
            return this.messageCase_ == 5;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasStakeTransferOwnership() {
            return this.messageCase_ == 7;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasStakeUnstake() {
            return this.messageCase_ == 2;
        }

        @Override // wallet.core.jni.proto.IoTeX.StakingOrBuilder
        public boolean hasStakeWithdraw() {
            return this.messageCase_ == 3;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = 779 + getDescriptor().hashCode();
            switch (this.messageCase_) {
                case 1:
                    i = ((hashCode2 * 37) + 1) * 53;
                    hashCode = getStakeCreate().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3;
                    return hashCode3;
                case 2:
                    i = ((hashCode2 * 37) + 2) * 53;
                    hashCode = getStakeUnstake().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32;
                    return hashCode32;
                case 3:
                    i = ((hashCode2 * 37) + 3) * 53;
                    hashCode = getStakeWithdraw().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322;
                    return hashCode322;
                case 4:
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getStakeAddDeposit().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222;
                    return hashCode3222;
                case 5:
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getStakeRestake().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222;
                    return hashCode32222;
                case 6:
                    i = ((hashCode2 * 37) + 6) * 53;
                    hashCode = getStakeChangeCandidate().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222;
                    return hashCode322222;
                case 7:
                    i = ((hashCode2 * 37) + 7) * 53;
                    hashCode = getStakeTransferOwnership().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222;
                    return hashCode3222222;
                case 8:
                    i = ((hashCode2 * 37) + 8) * 53;
                    hashCode = getCandidateRegister().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222222;
                    return hashCode32222222;
                case 9:
                    i = ((hashCode2 * 37) + 9) * 53;
                    hashCode = getCandidateUpdate().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222222;
                    return hashCode322222222;
                default:
                    int hashCode3222222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222222;
                    return hashCode3222222222;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return IoTeX.internal_static_TW_IoTeX_Proto_Staking_fieldAccessorTable.d(Staking.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Staking();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.messageCase_ == 1) {
                codedOutputStream.K0(1, (Create) this.message_);
            }
            if (this.messageCase_ == 2) {
                codedOutputStream.K0(2, (Reclaim) this.message_);
            }
            if (this.messageCase_ == 3) {
                codedOutputStream.K0(3, (Reclaim) this.message_);
            }
            if (this.messageCase_ == 4) {
                codedOutputStream.K0(4, (AddDeposit) this.message_);
            }
            if (this.messageCase_ == 5) {
                codedOutputStream.K0(5, (Restake) this.message_);
            }
            if (this.messageCase_ == 6) {
                codedOutputStream.K0(6, (ChangeCandidate) this.message_);
            }
            if (this.messageCase_ == 7) {
                codedOutputStream.K0(7, (TransferOwnership) this.message_);
            }
            if (this.messageCase_ == 8) {
                codedOutputStream.K0(8, (CandidateRegister) this.message_);
            }
            if (this.messageCase_ == 9) {
                codedOutputStream.K0(9, (CandidateBasicInfo) this.message_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Staking(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Staking staking) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(staking);
        }

        public static Staking parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Staking(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Staking parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Staking parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Staking getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Staking parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Staking parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Staking() {
            this.messageCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Staking parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Staking parseFrom(InputStream inputStream) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Staking(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    Create.Builder builder = this.messageCase_ == 1 ? ((Create) this.message_).toBuilder() : null;
                                    m0 z2 = jVar.z(Create.parser(), rVar);
                                    this.message_ = z2;
                                    if (builder != null) {
                                        builder.mergeFrom((Create) z2);
                                        this.message_ = builder.buildPartial();
                                    }
                                    this.messageCase_ = 1;
                                } else if (J == 18) {
                                    Reclaim.Builder builder2 = this.messageCase_ == 2 ? ((Reclaim) this.message_).toBuilder() : null;
                                    m0 z3 = jVar.z(Reclaim.parser(), rVar);
                                    this.message_ = z3;
                                    if (builder2 != null) {
                                        builder2.mergeFrom((Reclaim) z3);
                                        this.message_ = builder2.buildPartial();
                                    }
                                    this.messageCase_ = 2;
                                } else if (J == 26) {
                                    Reclaim.Builder builder3 = this.messageCase_ == 3 ? ((Reclaim) this.message_).toBuilder() : null;
                                    m0 z4 = jVar.z(Reclaim.parser(), rVar);
                                    this.message_ = z4;
                                    if (builder3 != null) {
                                        builder3.mergeFrom((Reclaim) z4);
                                        this.message_ = builder3.buildPartial();
                                    }
                                    this.messageCase_ = 3;
                                } else if (J == 34) {
                                    AddDeposit.Builder builder4 = this.messageCase_ == 4 ? ((AddDeposit) this.message_).toBuilder() : null;
                                    m0 z5 = jVar.z(AddDeposit.parser(), rVar);
                                    this.message_ = z5;
                                    if (builder4 != null) {
                                        builder4.mergeFrom((AddDeposit) z5);
                                        this.message_ = builder4.buildPartial();
                                    }
                                    this.messageCase_ = 4;
                                } else if (J == 42) {
                                    Restake.Builder builder5 = this.messageCase_ == 5 ? ((Restake) this.message_).toBuilder() : null;
                                    m0 z6 = jVar.z(Restake.parser(), rVar);
                                    this.message_ = z6;
                                    if (builder5 != null) {
                                        builder5.mergeFrom((Restake) z6);
                                        this.message_ = builder5.buildPartial();
                                    }
                                    this.messageCase_ = 5;
                                } else if (J == 50) {
                                    ChangeCandidate.Builder builder6 = this.messageCase_ == 6 ? ((ChangeCandidate) this.message_).toBuilder() : null;
                                    m0 z7 = jVar.z(ChangeCandidate.parser(), rVar);
                                    this.message_ = z7;
                                    if (builder6 != null) {
                                        builder6.mergeFrom((ChangeCandidate) z7);
                                        this.message_ = builder6.buildPartial();
                                    }
                                    this.messageCase_ = 6;
                                } else if (J == 58) {
                                    TransferOwnership.Builder builder7 = this.messageCase_ == 7 ? ((TransferOwnership) this.message_).toBuilder() : null;
                                    m0 z8 = jVar.z(TransferOwnership.parser(), rVar);
                                    this.message_ = z8;
                                    if (builder7 != null) {
                                        builder7.mergeFrom((TransferOwnership) z8);
                                        this.message_ = builder7.buildPartial();
                                    }
                                    this.messageCase_ = 7;
                                } else if (J == 66) {
                                    CandidateRegister.Builder builder8 = this.messageCase_ == 8 ? ((CandidateRegister) this.message_).toBuilder() : null;
                                    m0 z9 = jVar.z(CandidateRegister.parser(), rVar);
                                    this.message_ = z9;
                                    if (builder8 != null) {
                                        builder8.mergeFrom((CandidateRegister) z9);
                                        this.message_ = builder8.buildPartial();
                                    }
                                    this.messageCase_ = 8;
                                } else if (J != 74) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    CandidateBasicInfo.Builder builder9 = this.messageCase_ == 9 ? ((CandidateBasicInfo) this.message_).toBuilder() : null;
                                    m0 z10 = jVar.z(CandidateBasicInfo.parser(), rVar);
                                    this.message_ = z10;
                                    if (builder9 != null) {
                                        builder9.mergeFrom((CandidateBasicInfo) z10);
                                        this.message_ = builder9.buildPartial();
                                    }
                                    this.messageCase_ = 9;
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Staking parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Staking parseFrom(j jVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Staking parseFrom(j jVar, r rVar) throws IOException {
            return (Staking) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface StakingOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Staking.CandidateRegister getCandidateRegister();

        Staking.CandidateRegisterOrBuilder getCandidateRegisterOrBuilder();

        Staking.CandidateBasicInfo getCandidateUpdate();

        Staking.CandidateBasicInfoOrBuilder getCandidateUpdateOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        Staking.MessageCase getMessageCase();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Staking.AddDeposit getStakeAddDeposit();

        Staking.AddDepositOrBuilder getStakeAddDepositOrBuilder();

        Staking.ChangeCandidate getStakeChangeCandidate();

        Staking.ChangeCandidateOrBuilder getStakeChangeCandidateOrBuilder();

        Staking.Create getStakeCreate();

        Staking.CreateOrBuilder getStakeCreateOrBuilder();

        Staking.Restake getStakeRestake();

        Staking.RestakeOrBuilder getStakeRestakeOrBuilder();

        Staking.TransferOwnership getStakeTransferOwnership();

        Staking.TransferOwnershipOrBuilder getStakeTransferOwnershipOrBuilder();

        Staking.Reclaim getStakeUnstake();

        Staking.ReclaimOrBuilder getStakeUnstakeOrBuilder();

        Staking.Reclaim getStakeWithdraw();

        Staking.ReclaimOrBuilder getStakeWithdrawOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasCandidateRegister();

        boolean hasCandidateUpdate();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasStakeAddDeposit();

        boolean hasStakeChangeCandidate();

        boolean hasStakeCreate();

        boolean hasStakeRestake();

        boolean hasStakeTransferOwnership();

        boolean hasStakeUnstake();

        boolean hasStakeWithdraw();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transfer extends GeneratedMessageV3 implements TransferOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 1;
        private static final Transfer DEFAULT_INSTANCE = new Transfer();
        private static final t0<Transfer> PARSER = new c<Transfer>() { // from class: wallet.core.jni.proto.IoTeX.Transfer.1
            @Override // com.google.protobuf.t0
            public Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transfer(jVar, rVar, null);
            }
        };
        public static final int PAYLOAD_FIELD_NUMBER = 3;
        public static final int RECIPIENT_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private volatile Object amount_;
        private byte memoizedIsInitialized;
        private ByteString payload_;
        private volatile Object recipient_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOrBuilder {
            private Object amount_;
            private ByteString payload_;
            private Object recipient_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Transfer_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = Transfer.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearPayload() {
                this.payload_ = Transfer.getDefaultInstance().getPayload();
                onChanged();
                return this;
            }

            public Builder clearRecipient() {
                this.recipient_ = Transfer.getDefaultInstance().getRecipient();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
            public String getAmount() {
                Object obj = this.amount_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.amount_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
            public ByteString getAmountBytes() {
                Object obj = this.amount_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.amount_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Transfer_descriptor;
            }

            @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
            public ByteString getPayload() {
                return this.payload_;
            }

            @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
            public String getRecipient() {
                Object obj = this.recipient_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.recipient_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
            public ByteString getRecipientBytes() {
                Object obj = this.recipient_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.recipient_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return IoTeX.internal_static_TW_IoTeX_Proto_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(String str) {
                Objects.requireNonNull(str);
                this.amount_ = str;
                onChanged();
                return this;
            }

            public Builder setAmountBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPayload(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.payload_ = byteString;
                onChanged();
                return this;
            }

            public Builder setRecipient(String str) {
                Objects.requireNonNull(str);
                this.recipient_ = str;
                onChanged();
                return this;
            }

            public Builder setRecipientBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.recipient_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.amount_ = "";
                this.recipient_ = "";
                this.payload_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transfer build() {
                Transfer buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transfer buildPartial() {
                Transfer transfer = new Transfer(this, (AnonymousClass1) null);
                transfer.amount_ = this.amount_;
                transfer.recipient_ = this.recipient_;
                transfer.payload_ = this.payload_;
                onBuilt();
                return transfer;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transfer getDefaultInstanceForType() {
                return Transfer.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.amount_ = "";
                this.recipient_ = "";
                this.payload_ = ByteString.EMPTY;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transfer) {
                    return mergeFrom((Transfer) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.amount_ = "";
                this.recipient_ = "";
                this.payload_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(Transfer transfer) {
                if (transfer == Transfer.getDefaultInstance()) {
                    return this;
                }
                if (!transfer.getAmount().isEmpty()) {
                    this.amount_ = transfer.amount_;
                    onChanged();
                }
                if (!transfer.getRecipient().isEmpty()) {
                    this.recipient_ = transfer.recipient_;
                    onChanged();
                }
                if (transfer.getPayload() != ByteString.EMPTY) {
                    setPayload(transfer.getPayload());
                }
                mergeUnknownFields(transfer.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.IoTeX.Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.IoTeX.Transfer.access$1000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.IoTeX$Transfer r3 = (wallet.core.jni.proto.IoTeX.Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.IoTeX$Transfer r4 = (wallet.core.jni.proto.IoTeX.Transfer) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.IoTeX.Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.IoTeX$Transfer$Builder");
            }
        }

        public /* synthetic */ Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Transfer getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return IoTeX.internal_static_TW_IoTeX_Proto_Transfer_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transfer> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transfer)) {
                return super.equals(obj);
            }
            Transfer transfer = (Transfer) obj;
            return getAmount().equals(transfer.getAmount()) && getRecipient().equals(transfer.getRecipient()) && getPayload().equals(transfer.getPayload()) && this.unknownFields.equals(transfer.unknownFields);
        }

        @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
        public String getAmount() {
            Object obj = this.amount_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.amount_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
        public ByteString getAmountBytes() {
            Object obj = this.amount_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.amount_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transfer> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
        public ByteString getPayload() {
            return this.payload_;
        }

        @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
        public String getRecipient() {
            Object obj = this.recipient_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.recipient_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.IoTeX.TransferOrBuilder
        public ByteString getRecipientBytes() {
            Object obj = this.recipient_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.recipient_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getAmountBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.amount_);
            if (!getRecipientBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.recipient_);
            }
            if (!this.payload_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.payload_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAmount().hashCode()) * 37) + 2) * 53) + getRecipient().hashCode()) * 37) + 3) * 53) + getPayload().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return IoTeX.internal_static_TW_IoTeX_Proto_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transfer();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getAmountBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.amount_);
            }
            if (!getRecipientBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.recipient_);
            }
            if (!this.payload_.isEmpty()) {
                codedOutputStream.q0(3, this.payload_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Transfer transfer) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transfer);
        }

        public static Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transfer(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transfer getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Transfer() {
            this.memoizedIsInitialized = (byte) -1;
            this.amount_ = "";
            this.recipient_ = "";
            this.payload_ = ByteString.EMPTY;
        }

        public static Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transfer parseFrom(InputStream inputStream) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.amount_ = jVar.I();
                            } else if (J == 18) {
                                this.recipient_ = jVar.I();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.payload_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transfer parseFrom(j jVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transfer parseFrom(j jVar, r rVar) throws IOException {
            return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransferOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getAmount();

        ByteString getAmountBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPayload();

        String getRecipient();

        ByteString getRecipientBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_IoTeX_Proto_Transfer_descriptor = bVar;
        internal_static_TW_IoTeX_Proto_Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Amount", "Recipient", "Payload"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_IoTeX_Proto_Staking_descriptor = bVar2;
        internal_static_TW_IoTeX_Proto_Staking_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"StakeCreate", "StakeUnstake", "StakeWithdraw", "StakeAddDeposit", "StakeRestake", "StakeChangeCandidate", "StakeTransferOwnership", "CandidateRegister", "CandidateUpdate", "Message"});
        Descriptors.b bVar3 = bVar2.r().get(0);
        internal_static_TW_IoTeX_Proto_Staking_Create_descriptor = bVar3;
        internal_static_TW_IoTeX_Proto_Staking_Create_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"CandidateName", "StakedAmount", "StakedDuration", "AutoStake", "Payload"});
        Descriptors.b bVar4 = bVar2.r().get(1);
        internal_static_TW_IoTeX_Proto_Staking_Reclaim_descriptor = bVar4;
        internal_static_TW_IoTeX_Proto_Staking_Reclaim_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"BucketIndex", "Payload"});
        Descriptors.b bVar5 = bVar2.r().get(2);
        internal_static_TW_IoTeX_Proto_Staking_AddDeposit_descriptor = bVar5;
        internal_static_TW_IoTeX_Proto_Staking_AddDeposit_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"BucketIndex", "Amount", "Payload"});
        Descriptors.b bVar6 = bVar2.r().get(3);
        internal_static_TW_IoTeX_Proto_Staking_Restake_descriptor = bVar6;
        internal_static_TW_IoTeX_Proto_Staking_Restake_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"BucketIndex", "StakedDuration", "AutoStake", "Payload"});
        Descriptors.b bVar7 = bVar2.r().get(4);
        internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_descriptor = bVar7;
        internal_static_TW_IoTeX_Proto_Staking_ChangeCandidate_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"BucketIndex", "CandidateName", "Payload"});
        Descriptors.b bVar8 = bVar2.r().get(5);
        internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_descriptor = bVar8;
        internal_static_TW_IoTeX_Proto_Staking_TransferOwnership_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"BucketIndex", "VoterAddress", "Payload"});
        Descriptors.b bVar9 = bVar2.r().get(6);
        internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_descriptor = bVar9;
        internal_static_TW_IoTeX_Proto_Staking_CandidateBasicInfo_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"Name", "OperatorAddress", "RewardAddress"});
        Descriptors.b bVar10 = bVar2.r().get(7);
        internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_descriptor = bVar10;
        internal_static_TW_IoTeX_Proto_Staking_CandidateRegister_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"Candidate", "StakedAmount", "StakedDuration", "AutoStake", "OwnerAddress", "Payload"});
        Descriptors.b bVar11 = getDescriptor().o().get(2);
        internal_static_TW_IoTeX_Proto_ContractCall_descriptor = bVar11;
        internal_static_TW_IoTeX_Proto_ContractCall_fieldAccessorTable = new GeneratedMessageV3.e(bVar11, new String[]{"Amount", "Contract", "Data"});
        Descriptors.b bVar12 = getDescriptor().o().get(3);
        internal_static_TW_IoTeX_Proto_SigningInput_descriptor = bVar12;
        internal_static_TW_IoTeX_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar12, new String[]{"Version", "Nonce", "GasLimit", "GasPrice", "PrivateKey", "Transfer", "Call", "StakeCreate", "StakeUnstake", "StakeWithdraw", "StakeAddDeposit", "StakeRestake", "StakeChangeCandidate", "StakeTransferOwnership", "CandidateRegister", "CandidateUpdate", "Action"});
        Descriptors.b bVar13 = getDescriptor().o().get(4);
        internal_static_TW_IoTeX_Proto_SigningOutput_descriptor = bVar13;
        internal_static_TW_IoTeX_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar13, new String[]{"Encoded", "Hash"});
        Descriptors.b bVar14 = getDescriptor().o().get(5);
        internal_static_TW_IoTeX_Proto_ActionCore_descriptor = bVar14;
        internal_static_TW_IoTeX_Proto_ActionCore_fieldAccessorTable = new GeneratedMessageV3.e(bVar14, new String[]{"Version", "Nonce", "GasLimit", "GasPrice", "Transfer", "Execution", "StakeCreate", "StakeUnstake", "StakeWithdraw", "StakeAddDeposit", "StakeRestake", "StakeChangeCandidate", "StakeTransferOwnership", "CandidateRegister", "CandidateUpdate", "Action"});
        Descriptors.b bVar15 = getDescriptor().o().get(6);
        internal_static_TW_IoTeX_Proto_Action_descriptor = bVar15;
        internal_static_TW_IoTeX_Proto_Action_fieldAccessorTable = new GeneratedMessageV3.e(bVar15, new String[]{Constants.VARIANT, "SenderPubKey", "Signature"});
    }

    private IoTeX() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
