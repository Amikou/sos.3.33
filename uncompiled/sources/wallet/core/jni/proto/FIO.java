package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import wallet.core.jni.proto.Common;

/* loaded from: classes3.dex */
public final class FIO {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\tFIO.proto\u0012\fTW.FIO.Proto\u001a\fCommon.proto\"5\n\rPublicAddress\u0012\u0013\n\u000bcoin_symbol\u0018\u0001 \u0001(\t\u0012\u000f\n\u0007address\u0018\u0002 \u0001(\t\"\u0085\u0001\n\u000fNewFundsContent\u0012\u001c\n\u0014payee_public_address\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\t\u0012\u0013\n\u000bcoin_symbol\u0018\u0003 \u0001(\t\u0012\f\n\u0004memo\u0018\u0004 \u0001(\t\u0012\f\n\u0004hash\u0018\u0005 \u0001(\t\u0012\u0013\n\u000boffline_url\u0018\u0006 \u0001(\t\"ô\u0006\n\u0006Action\u0012O\n\u001cregister_fio_address_message\u0018\u0001 \u0001(\u000b2'.TW.FIO.Proto.Action.RegisterFioAddressH\u0000\u0012E\n\u0017add_pub_address_message\u0018\u0002 \u0001(\u000b2\".TW.FIO.Proto.Action.AddPubAddressH\u0000\u00129\n\u0010transfer_message\u0018\u0003 \u0001(\u000b2\u001d.TW.FIO.Proto.Action.TransferH\u0000\u0012I\n\u0019renew_fio_address_message\u0018\u0004 \u0001(\u000b2$.TW.FIO.Proto.Action.RenewFioAddressH\u0000\u0012I\n\u0019new_funds_request_message\u0018\u0005 \u0001(\u000b2$.TW.FIO.Proto.Action.NewFundsRequestH\u0000\u001aT\n\u0012RegisterFioAddress\u0012\u0013\n\u000bfio_address\u0018\u0001 \u0001(\t\u0012\u001c\n\u0014owner_fio_public_key\u0018\u0002 \u0001(\t\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0004\u001ah\n\rAddPubAddress\u0012\u0013\n\u000bfio_address\u0018\u0001 \u0001(\t\u00125\n\u0010public_addresses\u0018\u0002 \u0003(\u000b2\u001b.TW.FIO.Proto.PublicAddress\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0004\u001aA\n\bTransfer\u0012\u0018\n\u0010payee_public_key\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\u0004\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0004\u001aQ\n\u000fRenewFioAddress\u0012\u0013\n\u000bfio_address\u0018\u0001 \u0001(\t\u0012\u001c\n\u0014owner_fio_public_key\u0018\u0002 \u0001(\t\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0004\u001a\u0099\u0001\n\u000fNewFundsRequest\u0012\u0016\n\u000epayer_fio_name\u0018\u0001 \u0001(\t\u0012\u0019\n\u0011payer_fio_address\u0018\u0002 \u0001(\t\u0012\u0016\n\u000epayee_fio_name\u0018\u0003 \u0001(\t\u0012.\n\u0007content\u0018\u0004 \u0001(\u000b2\u001d.TW.FIO.Proto.NewFundsContent\u0012\u000b\n\u0003fee\u0018\u0005 \u0001(\u0004B\u000f\n\rmessage_oneof\"T\n\u000bChainParams\u0012\u0010\n\bchain_id\u0018\u0001 \u0001(\f\u0012\u0019\n\u0011head_block_number\u0018\u0002 \u0001(\u0004\u0012\u0018\n\u0010ref_block_prefix\u0018\u0003 \u0001(\u0004\"\u0098\u0001\n\fSigningInput\u0012\u000e\n\u0006expiry\u0018\u0001 \u0001(\r\u0012/\n\fchain_params\u0018\u0002 \u0001(\u000b2\u0019.TW.FIO.Proto.ChainParams\u0012\u0013\n\u000bprivate_key\u0018\u0003 \u0001(\f\u0012\f\n\u0004tpid\u0018\u0004 \u0001(\t\u0012$\n\u0006action\u0018\u0005 \u0001(\u000b2\u0014.TW.FIO.Proto.Action\"K\n\rSigningOutput\u0012\f\n\u0004json\u0018\u0001 \u0001(\t\u0012,\n\u0005error\u0018\u0002 \u0001(\u000e2\u001d.TW.Common.Proto.SigningErrorB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[]{Common.getDescriptor()});
    private static final Descriptors.b internal_static_TW_FIO_Proto_Action_AddPubAddress_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_Action_AddPubAddress_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_Action_NewFundsRequest_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_Action_NewFundsRequest_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_Action_RegisterFioAddress_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_Action_RegisterFioAddress_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_Action_RenewFioAddress_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_Action_RenewFioAddress_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_Action_Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_Action_Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_Action_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_Action_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_ChainParams_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_ChainParams_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_NewFundsContent_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_NewFundsContent_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_PublicAddress_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_PublicAddress_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_FIO_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_FIO_Proto_SigningOutput_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.FIO$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase;

        static {
            int[] iArr = new int[Action.MessageOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase = iArr;
            try {
                iArr[Action.MessageOneofCase.REGISTER_FIO_ADDRESS_MESSAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase[Action.MessageOneofCase.ADD_PUB_ADDRESS_MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase[Action.MessageOneofCase.TRANSFER_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase[Action.MessageOneofCase.RENEW_FIO_ADDRESS_MESSAGE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase[Action.MessageOneofCase.NEW_FUNDS_REQUEST_MESSAGE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase[Action.MessageOneofCase.MESSAGEONEOF_NOT_SET.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class Action extends GeneratedMessageV3 implements ActionOrBuilder {
        public static final int ADD_PUB_ADDRESS_MESSAGE_FIELD_NUMBER = 2;
        public static final int NEW_FUNDS_REQUEST_MESSAGE_FIELD_NUMBER = 5;
        public static final int REGISTER_FIO_ADDRESS_MESSAGE_FIELD_NUMBER = 1;
        public static final int RENEW_FIO_ADDRESS_MESSAGE_FIELD_NUMBER = 4;
        public static final int TRANSFER_MESSAGE_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int messageOneofCase_;
        private Object messageOneof_;
        private static final Action DEFAULT_INSTANCE = new Action();
        private static final t0<Action> PARSER = new c<Action>() { // from class: wallet.core.jni.proto.FIO.Action.1
            @Override // com.google.protobuf.t0
            public Action parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Action(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class AddPubAddress extends GeneratedMessageV3 implements AddPubAddressOrBuilder {
            public static final int FEE_FIELD_NUMBER = 3;
            public static final int FIO_ADDRESS_FIELD_NUMBER = 1;
            public static final int PUBLIC_ADDRESSES_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private long fee_;
            private volatile Object fioAddress_;
            private byte memoizedIsInitialized;
            private List<PublicAddress> publicAddresses_;
            private static final AddPubAddress DEFAULT_INSTANCE = new AddPubAddress();
            private static final t0<AddPubAddress> PARSER = new c<AddPubAddress>() { // from class: wallet.core.jni.proto.FIO.Action.AddPubAddress.1
                @Override // com.google.protobuf.t0
                public AddPubAddress parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new AddPubAddress(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements AddPubAddressOrBuilder {
                private int bitField0_;
                private long fee_;
                private Object fioAddress_;
                private x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> publicAddressesBuilder_;
                private List<PublicAddress> publicAddresses_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private void ensurePublicAddressesIsMutable() {
                    if ((this.bitField0_ & 1) == 0) {
                        this.publicAddresses_ = new ArrayList(this.publicAddresses_);
                        this.bitField0_ |= 1;
                    }
                }

                public static final Descriptors.b getDescriptor() {
                    return FIO.internal_static_TW_FIO_Proto_Action_AddPubAddress_descriptor;
                }

                private x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> getPublicAddressesFieldBuilder() {
                    if (this.publicAddressesBuilder_ == null) {
                        this.publicAddressesBuilder_ = new x0<>(this.publicAddresses_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                        this.publicAddresses_ = null;
                    }
                    return this.publicAddressesBuilder_;
                }

                private void maybeForceBuilderInitialization() {
                    if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                        getPublicAddressesFieldBuilder();
                    }
                }

                public Builder addAllPublicAddresses(Iterable<? extends PublicAddress> iterable) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        ensurePublicAddressesIsMutable();
                        b.a.addAll((Iterable) iterable, (List) this.publicAddresses_);
                        onChanged();
                    } else {
                        x0Var.b(iterable);
                    }
                    return this;
                }

                public Builder addPublicAddresses(PublicAddress publicAddress) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        Objects.requireNonNull(publicAddress);
                        ensurePublicAddressesIsMutable();
                        this.publicAddresses_.add(publicAddress);
                        onChanged();
                    } else {
                        x0Var.f(publicAddress);
                    }
                    return this;
                }

                public PublicAddress.Builder addPublicAddressesBuilder() {
                    return getPublicAddressesFieldBuilder().d(PublicAddress.getDefaultInstance());
                }

                public Builder clearFee() {
                    this.fee_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearFioAddress() {
                    this.fioAddress_ = AddPubAddress.getDefaultInstance().getFioAddress();
                    onChanged();
                    return this;
                }

                public Builder clearPublicAddresses() {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        this.publicAddresses_ = Collections.emptyList();
                        this.bitField0_ &= -2;
                        onChanged();
                    } else {
                        x0Var.h();
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return FIO.internal_static_TW_FIO_Proto_Action_AddPubAddress_descriptor;
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public long getFee() {
                    return this.fee_;
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public String getFioAddress() {
                    Object obj = this.fioAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.fioAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public ByteString getFioAddressBytes() {
                    Object obj = this.fioAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.fioAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public PublicAddress getPublicAddresses(int i) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        return this.publicAddresses_.get(i);
                    }
                    return x0Var.o(i);
                }

                public PublicAddress.Builder getPublicAddressesBuilder(int i) {
                    return getPublicAddressesFieldBuilder().l(i);
                }

                public List<PublicAddress.Builder> getPublicAddressesBuilderList() {
                    return getPublicAddressesFieldBuilder().m();
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public int getPublicAddressesCount() {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        return this.publicAddresses_.size();
                    }
                    return x0Var.n();
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public List<PublicAddress> getPublicAddressesList() {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        return Collections.unmodifiableList(this.publicAddresses_);
                    }
                    return x0Var.q();
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public PublicAddressOrBuilder getPublicAddressesOrBuilder(int i) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        return this.publicAddresses_.get(i);
                    }
                    return x0Var.r(i);
                }

                @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
                public List<? extends PublicAddressOrBuilder> getPublicAddressesOrBuilderList() {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var != null) {
                        return x0Var.s();
                    }
                    return Collections.unmodifiableList(this.publicAddresses_);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return FIO.internal_static_TW_FIO_Proto_Action_AddPubAddress_fieldAccessorTable.d(AddPubAddress.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder removePublicAddresses(int i) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        ensurePublicAddressesIsMutable();
                        this.publicAddresses_.remove(i);
                        onChanged();
                    } else {
                        x0Var.w(i);
                    }
                    return this;
                }

                public Builder setFee(long j) {
                    this.fee_ = j;
                    onChanged();
                    return this;
                }

                public Builder setFioAddress(String str) {
                    Objects.requireNonNull(str);
                    this.fioAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setFioAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.fioAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setPublicAddresses(int i, PublicAddress publicAddress) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        Objects.requireNonNull(publicAddress);
                        ensurePublicAddressesIsMutable();
                        this.publicAddresses_.set(i, publicAddress);
                        onChanged();
                    } else {
                        x0Var.x(i, publicAddress);
                    }
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.fioAddress_ = "";
                    this.publicAddresses_ = Collections.emptyList();
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public AddPubAddress build() {
                    AddPubAddress buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public AddPubAddress buildPartial() {
                    AddPubAddress addPubAddress = new AddPubAddress(this, (AnonymousClass1) null);
                    addPubAddress.fioAddress_ = this.fioAddress_;
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var != null) {
                        addPubAddress.publicAddresses_ = x0Var.g();
                    } else {
                        if ((this.bitField0_ & 1) != 0) {
                            this.publicAddresses_ = Collections.unmodifiableList(this.publicAddresses_);
                            this.bitField0_ &= -2;
                        }
                        addPubAddress.publicAddresses_ = this.publicAddresses_;
                    }
                    addPubAddress.fee_ = this.fee_;
                    onBuilt();
                    return addPubAddress;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public AddPubAddress getDefaultInstanceForType() {
                    return AddPubAddress.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                public PublicAddress.Builder addPublicAddressesBuilder(int i) {
                    return getPublicAddressesFieldBuilder().c(i, PublicAddress.getDefaultInstance());
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.fioAddress_ = "";
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        this.publicAddresses_ = Collections.emptyList();
                        this.bitField0_ &= -2;
                    } else {
                        x0Var.h();
                    }
                    this.fee_ = 0L;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.fioAddress_ = "";
                    this.publicAddresses_ = Collections.emptyList();
                    maybeForceBuilderInitialization();
                }

                public Builder addPublicAddresses(int i, PublicAddress publicAddress) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        Objects.requireNonNull(publicAddress);
                        ensurePublicAddressesIsMutable();
                        this.publicAddresses_.add(i, publicAddress);
                        onChanged();
                    } else {
                        x0Var.e(i, publicAddress);
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof AddPubAddress) {
                        return mergeFrom((AddPubAddress) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder setPublicAddresses(int i, PublicAddress.Builder builder) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        ensurePublicAddressesIsMutable();
                        this.publicAddresses_.set(i, builder.build());
                        onChanged();
                    } else {
                        x0Var.x(i, builder.build());
                    }
                    return this;
                }

                public Builder mergeFrom(AddPubAddress addPubAddress) {
                    if (addPubAddress == AddPubAddress.getDefaultInstance()) {
                        return this;
                    }
                    if (!addPubAddress.getFioAddress().isEmpty()) {
                        this.fioAddress_ = addPubAddress.fioAddress_;
                        onChanged();
                    }
                    if (this.publicAddressesBuilder_ == null) {
                        if (!addPubAddress.publicAddresses_.isEmpty()) {
                            if (this.publicAddresses_.isEmpty()) {
                                this.publicAddresses_ = addPubAddress.publicAddresses_;
                                this.bitField0_ &= -2;
                            } else {
                                ensurePublicAddressesIsMutable();
                                this.publicAddresses_.addAll(addPubAddress.publicAddresses_);
                            }
                            onChanged();
                        }
                    } else if (!addPubAddress.publicAddresses_.isEmpty()) {
                        if (!this.publicAddressesBuilder_.u()) {
                            this.publicAddressesBuilder_.b(addPubAddress.publicAddresses_);
                        } else {
                            this.publicAddressesBuilder_.i();
                            this.publicAddressesBuilder_ = null;
                            this.publicAddresses_ = addPubAddress.publicAddresses_;
                            this.bitField0_ &= -2;
                            this.publicAddressesBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getPublicAddressesFieldBuilder() : null;
                        }
                    }
                    if (addPubAddress.getFee() != 0) {
                        setFee(addPubAddress.getFee());
                    }
                    mergeUnknownFields(addPubAddress.unknownFields);
                    onChanged();
                    return this;
                }

                public Builder addPublicAddresses(PublicAddress.Builder builder) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        ensurePublicAddressesIsMutable();
                        this.publicAddresses_.add(builder.build());
                        onChanged();
                    } else {
                        x0Var.f(builder.build());
                    }
                    return this;
                }

                public Builder addPublicAddresses(int i, PublicAddress.Builder builder) {
                    x0<PublicAddress, PublicAddress.Builder, PublicAddressOrBuilder> x0Var = this.publicAddressesBuilder_;
                    if (x0Var == null) {
                        ensurePublicAddressesIsMutable();
                        this.publicAddresses_.add(i, builder.build());
                        onChanged();
                    } else {
                        x0Var.e(i, builder.build());
                    }
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.FIO.Action.AddPubAddress.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.Action.AddPubAddress.access$6100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.FIO$Action$AddPubAddress r3 = (wallet.core.jni.proto.FIO.Action.AddPubAddress) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.FIO$Action$AddPubAddress r4 = (wallet.core.jni.proto.FIO.Action.AddPubAddress) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.Action.AddPubAddress.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$Action$AddPubAddress$Builder");
                }
            }

            public /* synthetic */ AddPubAddress(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static AddPubAddress getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_Action_AddPubAddress_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static AddPubAddress parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (AddPubAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static AddPubAddress parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<AddPubAddress> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof AddPubAddress)) {
                    return super.equals(obj);
                }
                AddPubAddress addPubAddress = (AddPubAddress) obj;
                return getFioAddress().equals(addPubAddress.getFioAddress()) && getPublicAddressesList().equals(addPubAddress.getPublicAddressesList()) && getFee() == addPubAddress.getFee() && this.unknownFields.equals(addPubAddress.unknownFields);
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public String getFioAddress() {
                Object obj = this.fioAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.fioAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public ByteString getFioAddressBytes() {
                Object obj = this.fioAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.fioAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<AddPubAddress> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public PublicAddress getPublicAddresses(int i) {
                return this.publicAddresses_.get(i);
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public int getPublicAddressesCount() {
                return this.publicAddresses_.size();
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public List<PublicAddress> getPublicAddressesList() {
                return this.publicAddresses_;
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public PublicAddressOrBuilder getPublicAddressesOrBuilder(int i) {
                return this.publicAddresses_.get(i);
            }

            @Override // wallet.core.jni.proto.FIO.Action.AddPubAddressOrBuilder
            public List<? extends PublicAddressOrBuilder> getPublicAddressesOrBuilderList() {
                return this.publicAddresses_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = !getFioAddressBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.fioAddress_) + 0 : 0;
                for (int i2 = 0; i2 < this.publicAddresses_.size(); i2++) {
                    computeStringSize += CodedOutputStream.G(2, this.publicAddresses_.get(i2));
                }
                long j = this.fee_;
                if (j != 0) {
                    computeStringSize += CodedOutputStream.a0(3, j);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFioAddress().hashCode();
                if (getPublicAddressesCount() > 0) {
                    hashCode = (((hashCode * 37) + 2) * 53) + getPublicAddressesList().hashCode();
                }
                int h = (((((hashCode * 37) + 3) * 53) + a0.h(getFee())) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = h;
                return h;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_Action_AddPubAddress_fieldAccessorTable.d(AddPubAddress.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new AddPubAddress();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getFioAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.fioAddress_);
                }
                for (int i = 0; i < this.publicAddresses_.size(); i++) {
                    codedOutputStream.K0(2, this.publicAddresses_.get(i));
                }
                long j = this.fee_;
                if (j != 0) {
                    codedOutputStream.d1(3, j);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ AddPubAddress(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(AddPubAddress addPubAddress) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(addPubAddress);
            }

            public static AddPubAddress parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private AddPubAddress(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static AddPubAddress parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (AddPubAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static AddPubAddress parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public AddPubAddress getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static AddPubAddress parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private AddPubAddress() {
                this.memoizedIsInitialized = (byte) -1;
                this.fioAddress_ = "";
                this.publicAddresses_ = Collections.emptyList();
            }

            public static AddPubAddress parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static AddPubAddress parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static AddPubAddress parseFrom(InputStream inputStream) throws IOException {
                return (AddPubAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            /* JADX WARN: Multi-variable type inference failed */
            private AddPubAddress(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                boolean z2 = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.fioAddress_ = jVar.I();
                                    } else if (J == 18) {
                                        if (!(z2 & true)) {
                                            this.publicAddresses_ = new ArrayList();
                                            z2 |= true;
                                        }
                                        this.publicAddresses_.add(jVar.z(PublicAddress.parser(), rVar));
                                    } else if (J != 24) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.fee_ = jVar.L();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        if (z2 & true) {
                            this.publicAddresses_ = Collections.unmodifiableList(this.publicAddresses_);
                        }
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static AddPubAddress parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (AddPubAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static AddPubAddress parseFrom(j jVar) throws IOException {
                return (AddPubAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static AddPubAddress parseFrom(j jVar, r rVar) throws IOException {
                return (AddPubAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface AddPubAddressOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            long getFee();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            String getFioAddress();

            ByteString getFioAddressBytes();

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            PublicAddress getPublicAddresses(int i);

            int getPublicAddressesCount();

            List<PublicAddress> getPublicAddressesList();

            PublicAddressOrBuilder getPublicAddressesOrBuilder(int i);

            List<? extends PublicAddressOrBuilder> getPublicAddressesOrBuilderList();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements ActionOrBuilder {
            private a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> addPubAddressMessageBuilder_;
            private int messageOneofCase_;
            private Object messageOneof_;
            private a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> newFundsRequestMessageBuilder_;
            private a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> registerFioAddressMessageBuilder_;
            private a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> renewFioAddressMessageBuilder_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferMessageBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> getAddPubAddressMessageFieldBuilder() {
                if (this.addPubAddressMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 2) {
                        this.messageOneof_ = AddPubAddress.getDefaultInstance();
                    }
                    this.addPubAddressMessageBuilder_ = new a1<>((AddPubAddress) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 2;
                onChanged();
                return this.addPubAddressMessageBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_Action_descriptor;
            }

            private a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> getNewFundsRequestMessageFieldBuilder() {
                if (this.newFundsRequestMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 5) {
                        this.messageOneof_ = NewFundsRequest.getDefaultInstance();
                    }
                    this.newFundsRequestMessageBuilder_ = new a1<>((NewFundsRequest) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 5;
                onChanged();
                return this.newFundsRequestMessageBuilder_;
            }

            private a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> getRegisterFioAddressMessageFieldBuilder() {
                if (this.registerFioAddressMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 1) {
                        this.messageOneof_ = RegisterFioAddress.getDefaultInstance();
                    }
                    this.registerFioAddressMessageBuilder_ = new a1<>((RegisterFioAddress) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 1;
                onChanged();
                return this.registerFioAddressMessageBuilder_;
            }

            private a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> getRenewFioAddressMessageFieldBuilder() {
                if (this.renewFioAddressMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 4) {
                        this.messageOneof_ = RenewFioAddress.getDefaultInstance();
                    }
                    this.renewFioAddressMessageBuilder_ = new a1<>((RenewFioAddress) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 4;
                onChanged();
                return this.renewFioAddressMessageBuilder_;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferMessageFieldBuilder() {
                if (this.transferMessageBuilder_ == null) {
                    if (this.messageOneofCase_ != 3) {
                        this.messageOneof_ = Transfer.getDefaultInstance();
                    }
                    this.transferMessageBuilder_ = new a1<>((Transfer) this.messageOneof_, getParentForChildren(), isClean());
                    this.messageOneof_ = null;
                }
                this.messageOneofCase_ = 3;
                onChanged();
                return this.transferMessageBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAddPubAddressMessage() {
                a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> a1Var = this.addPubAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 2) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearMessageOneof() {
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearNewFundsRequestMessage() {
                a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> a1Var = this.newFundsRequestMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 5) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearRegisterFioAddressMessage() {
                a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> a1Var = this.registerFioAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 1) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearRenewFioAddressMessage() {
                a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> a1Var = this.renewFioAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 4) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransferMessage() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.messageOneofCase_ == 3) {
                        this.messageOneofCase_ = 0;
                        this.messageOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public AddPubAddress getAddPubAddressMessage() {
                a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> a1Var = this.addPubAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2) {
                        return (AddPubAddress) this.messageOneof_;
                    }
                    return AddPubAddress.getDefaultInstance();
                } else if (this.messageOneofCase_ == 2) {
                    return a1Var.f();
                } else {
                    return AddPubAddress.getDefaultInstance();
                }
            }

            public AddPubAddress.Builder getAddPubAddressMessageBuilder() {
                return getAddPubAddressMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public AddPubAddressOrBuilder getAddPubAddressMessageOrBuilder() {
                a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 2 || (a1Var = this.addPubAddressMessageBuilder_) == null) {
                    if (i == 2) {
                        return (AddPubAddress) this.messageOneof_;
                    }
                    return AddPubAddress.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return FIO.internal_static_TW_FIO_Proto_Action_descriptor;
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public MessageOneofCase getMessageOneofCase() {
                return MessageOneofCase.forNumber(this.messageOneofCase_);
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public NewFundsRequest getNewFundsRequestMessage() {
                a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> a1Var = this.newFundsRequestMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5) {
                        return (NewFundsRequest) this.messageOneof_;
                    }
                    return NewFundsRequest.getDefaultInstance();
                } else if (this.messageOneofCase_ == 5) {
                    return a1Var.f();
                } else {
                    return NewFundsRequest.getDefaultInstance();
                }
            }

            public NewFundsRequest.Builder getNewFundsRequestMessageBuilder() {
                return getNewFundsRequestMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public NewFundsRequestOrBuilder getNewFundsRequestMessageOrBuilder() {
                a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 5 || (a1Var = this.newFundsRequestMessageBuilder_) == null) {
                    if (i == 5) {
                        return (NewFundsRequest) this.messageOneof_;
                    }
                    return NewFundsRequest.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public RegisterFioAddress getRegisterFioAddressMessage() {
                a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> a1Var = this.registerFioAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1) {
                        return (RegisterFioAddress) this.messageOneof_;
                    }
                    return RegisterFioAddress.getDefaultInstance();
                } else if (this.messageOneofCase_ == 1) {
                    return a1Var.f();
                } else {
                    return RegisterFioAddress.getDefaultInstance();
                }
            }

            public RegisterFioAddress.Builder getRegisterFioAddressMessageBuilder() {
                return getRegisterFioAddressMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public RegisterFioAddressOrBuilder getRegisterFioAddressMessageOrBuilder() {
                a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 1 || (a1Var = this.registerFioAddressMessageBuilder_) == null) {
                    if (i == 1) {
                        return (RegisterFioAddress) this.messageOneof_;
                    }
                    return RegisterFioAddress.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public RenewFioAddress getRenewFioAddressMessage() {
                a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> a1Var = this.renewFioAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4) {
                        return (RenewFioAddress) this.messageOneof_;
                    }
                    return RenewFioAddress.getDefaultInstance();
                } else if (this.messageOneofCase_ == 4) {
                    return a1Var.f();
                } else {
                    return RenewFioAddress.getDefaultInstance();
                }
            }

            public RenewFioAddress.Builder getRenewFioAddressMessageBuilder() {
                return getRenewFioAddressMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public RenewFioAddressOrBuilder getRenewFioAddressMessageOrBuilder() {
                a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 4 || (a1Var = this.renewFioAddressMessageBuilder_) == null) {
                    if (i == 4) {
                        return (RenewFioAddress) this.messageOneof_;
                    }
                    return RenewFioAddress.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public Transfer getTransferMessage() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3) {
                        return (Transfer) this.messageOneof_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.messageOneofCase_ == 3) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferMessageBuilder() {
                return getTransferMessageFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public TransferOrBuilder getTransferMessageOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.messageOneofCase_;
                if (i != 3 || (a1Var = this.transferMessageBuilder_) == null) {
                    if (i == 3) {
                        return (Transfer) this.messageOneof_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public boolean hasAddPubAddressMessage() {
                return this.messageOneofCase_ == 2;
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public boolean hasNewFundsRequestMessage() {
                return this.messageOneofCase_ == 5;
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public boolean hasRegisterFioAddressMessage() {
                return this.messageOneofCase_ == 1;
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public boolean hasRenewFioAddressMessage() {
                return this.messageOneofCase_ == 4;
            }

            @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
            public boolean hasTransferMessage() {
                return this.messageOneofCase_ == 3;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_Action_fieldAccessorTable.d(Action.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeAddPubAddressMessage(AddPubAddress addPubAddress) {
                a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> a1Var = this.addPubAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 2 && this.messageOneof_ != AddPubAddress.getDefaultInstance()) {
                        this.messageOneof_ = AddPubAddress.newBuilder((AddPubAddress) this.messageOneof_).mergeFrom(addPubAddress).buildPartial();
                    } else {
                        this.messageOneof_ = addPubAddress;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 2) {
                        a1Var.h(addPubAddress);
                    }
                    this.addPubAddressMessageBuilder_.j(addPubAddress);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder mergeNewFundsRequestMessage(NewFundsRequest newFundsRequest) {
                a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> a1Var = this.newFundsRequestMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 5 && this.messageOneof_ != NewFundsRequest.getDefaultInstance()) {
                        this.messageOneof_ = NewFundsRequest.newBuilder((NewFundsRequest) this.messageOneof_).mergeFrom(newFundsRequest).buildPartial();
                    } else {
                        this.messageOneof_ = newFundsRequest;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 5) {
                        a1Var.h(newFundsRequest);
                    }
                    this.newFundsRequestMessageBuilder_.j(newFundsRequest);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder mergeRegisterFioAddressMessage(RegisterFioAddress registerFioAddress) {
                a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> a1Var = this.registerFioAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 1 && this.messageOneof_ != RegisterFioAddress.getDefaultInstance()) {
                        this.messageOneof_ = RegisterFioAddress.newBuilder((RegisterFioAddress) this.messageOneof_).mergeFrom(registerFioAddress).buildPartial();
                    } else {
                        this.messageOneof_ = registerFioAddress;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 1) {
                        a1Var.h(registerFioAddress);
                    }
                    this.registerFioAddressMessageBuilder_.j(registerFioAddress);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder mergeRenewFioAddressMessage(RenewFioAddress renewFioAddress) {
                a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> a1Var = this.renewFioAddressMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 4 && this.messageOneof_ != RenewFioAddress.getDefaultInstance()) {
                        this.messageOneof_ = RenewFioAddress.newBuilder((RenewFioAddress) this.messageOneof_).mergeFrom(renewFioAddress).buildPartial();
                    } else {
                        this.messageOneof_ = renewFioAddress;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 4) {
                        a1Var.h(renewFioAddress);
                    }
                    this.renewFioAddressMessageBuilder_.j(renewFioAddress);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder mergeTransferMessage(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    if (this.messageOneofCase_ == 3 && this.messageOneof_ != Transfer.getDefaultInstance()) {
                        this.messageOneof_ = Transfer.newBuilder((Transfer) this.messageOneof_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.messageOneof_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.messageOneofCase_ == 3) {
                        a1Var.h(transfer);
                    }
                    this.transferMessageBuilder_.j(transfer);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder setAddPubAddressMessage(AddPubAddress addPubAddress) {
                a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> a1Var = this.addPubAddressMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(addPubAddress);
                    this.messageOneof_ = addPubAddress;
                    onChanged();
                } else {
                    a1Var.j(addPubAddress);
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setNewFundsRequestMessage(NewFundsRequest newFundsRequest) {
                a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> a1Var = this.newFundsRequestMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(newFundsRequest);
                    this.messageOneof_ = newFundsRequest;
                    onChanged();
                } else {
                    a1Var.j(newFundsRequest);
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder setRegisterFioAddressMessage(RegisterFioAddress registerFioAddress) {
                a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> a1Var = this.registerFioAddressMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(registerFioAddress);
                    this.messageOneof_ = registerFioAddress;
                    onChanged();
                } else {
                    a1Var.j(registerFioAddress);
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setRenewFioAddressMessage(RenewFioAddress renewFioAddress) {
                a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> a1Var = this.renewFioAddressMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(renewFioAddress);
                    this.messageOneof_ = renewFioAddress;
                    onChanged();
                } else {
                    a1Var.j(renewFioAddress);
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setTransferMessage(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.messageOneof_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Action build() {
                Action buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Action buildPartial() {
                Action action = new Action(this, (AnonymousClass1) null);
                if (this.messageOneofCase_ == 1) {
                    a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> a1Var = this.registerFioAddressMessageBuilder_;
                    if (a1Var == null) {
                        action.messageOneof_ = this.messageOneof_;
                    } else {
                        action.messageOneof_ = a1Var.b();
                    }
                }
                if (this.messageOneofCase_ == 2) {
                    a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> a1Var2 = this.addPubAddressMessageBuilder_;
                    if (a1Var2 == null) {
                        action.messageOneof_ = this.messageOneof_;
                    } else {
                        action.messageOneof_ = a1Var2.b();
                    }
                }
                if (this.messageOneofCase_ == 3) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var3 = this.transferMessageBuilder_;
                    if (a1Var3 == null) {
                        action.messageOneof_ = this.messageOneof_;
                    } else {
                        action.messageOneof_ = a1Var3.b();
                    }
                }
                if (this.messageOneofCase_ == 4) {
                    a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> a1Var4 = this.renewFioAddressMessageBuilder_;
                    if (a1Var4 == null) {
                        action.messageOneof_ = this.messageOneof_;
                    } else {
                        action.messageOneof_ = a1Var4.b();
                    }
                }
                if (this.messageOneofCase_ == 5) {
                    a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> a1Var5 = this.newFundsRequestMessageBuilder_;
                    if (a1Var5 == null) {
                        action.messageOneof_ = this.messageOneof_;
                    } else {
                        action.messageOneof_ = a1Var5.b();
                    }
                }
                action.messageOneofCase_ = this.messageOneofCase_;
                onBuilt();
                return action;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Action getDefaultInstanceForType() {
                return Action.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.messageOneofCase_ = 0;
                this.messageOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.messageOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Action) {
                    return mergeFrom((Action) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setAddPubAddressMessage(AddPubAddress.Builder builder) {
                a1<AddPubAddress, AddPubAddress.Builder, AddPubAddressOrBuilder> a1Var = this.addPubAddressMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 2;
                return this;
            }

            public Builder setNewFundsRequestMessage(NewFundsRequest.Builder builder) {
                a1<NewFundsRequest, NewFundsRequest.Builder, NewFundsRequestOrBuilder> a1Var = this.newFundsRequestMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 5;
                return this;
            }

            public Builder setRegisterFioAddressMessage(RegisterFioAddress.Builder builder) {
                a1<RegisterFioAddress, RegisterFioAddress.Builder, RegisterFioAddressOrBuilder> a1Var = this.registerFioAddressMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 1;
                return this;
            }

            public Builder setRenewFioAddressMessage(RenewFioAddress.Builder builder) {
                a1<RenewFioAddress, RenewFioAddress.Builder, RenewFioAddressOrBuilder> a1Var = this.renewFioAddressMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 4;
                return this;
            }

            public Builder setTransferMessage(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferMessageBuilder_;
                if (a1Var == null) {
                    this.messageOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.messageOneofCase_ = 3;
                return this;
            }

            public Builder mergeFrom(Action action) {
                if (action == Action.getDefaultInstance()) {
                    return this;
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$FIO$Action$MessageOneofCase[action.getMessageOneofCase().ordinal()];
                if (i == 1) {
                    mergeRegisterFioAddressMessage(action.getRegisterFioAddressMessage());
                } else if (i == 2) {
                    mergeAddPubAddressMessage(action.getAddPubAddressMessage());
                } else if (i == 3) {
                    mergeTransferMessage(action.getTransferMessage());
                } else if (i == 4) {
                    mergeRenewFioAddressMessage(action.getRenewFioAddressMessage());
                } else if (i == 5) {
                    mergeNewFundsRequestMessage(action.getNewFundsRequestMessage());
                }
                mergeUnknownFields(action.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.FIO.Action.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.Action.access$11500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.FIO$Action r3 = (wallet.core.jni.proto.FIO.Action) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.FIO$Action r4 = (wallet.core.jni.proto.FIO.Action) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.Action.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$Action$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum MessageOneofCase implements a0.c {
            REGISTER_FIO_ADDRESS_MESSAGE(1),
            ADD_PUB_ADDRESS_MESSAGE(2),
            TRANSFER_MESSAGE(3),
            RENEW_FIO_ADDRESS_MESSAGE(4),
            NEW_FUNDS_REQUEST_MESSAGE(5),
            MESSAGEONEOF_NOT_SET(0);
            
            private final int value;

            MessageOneofCase(int i) {
                this.value = i;
            }

            public static MessageOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 3) {
                                if (i != 4) {
                                    if (i != 5) {
                                        return null;
                                    }
                                    return NEW_FUNDS_REQUEST_MESSAGE;
                                }
                                return RENEW_FIO_ADDRESS_MESSAGE;
                            }
                            return TRANSFER_MESSAGE;
                        }
                        return ADD_PUB_ADDRESS_MESSAGE;
                    }
                    return REGISTER_FIO_ADDRESS_MESSAGE;
                }
                return MESSAGEONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static MessageOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class NewFundsRequest extends GeneratedMessageV3 implements NewFundsRequestOrBuilder {
            public static final int CONTENT_FIELD_NUMBER = 4;
            public static final int FEE_FIELD_NUMBER = 5;
            public static final int PAYEE_FIO_NAME_FIELD_NUMBER = 3;
            public static final int PAYER_FIO_ADDRESS_FIELD_NUMBER = 2;
            public static final int PAYER_FIO_NAME_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private NewFundsContent content_;
            private long fee_;
            private byte memoizedIsInitialized;
            private volatile Object payeeFioName_;
            private volatile Object payerFioAddress_;
            private volatile Object payerFioName_;
            private static final NewFundsRequest DEFAULT_INSTANCE = new NewFundsRequest();
            private static final t0<NewFundsRequest> PARSER = new c<NewFundsRequest>() { // from class: wallet.core.jni.proto.FIO.Action.NewFundsRequest.1
                @Override // com.google.protobuf.t0
                public NewFundsRequest parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new NewFundsRequest(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements NewFundsRequestOrBuilder {
                private a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> contentBuilder_;
                private NewFundsContent content_;
                private long fee_;
                private Object payeeFioName_;
                private Object payerFioAddress_;
                private Object payerFioName_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                private a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> getContentFieldBuilder() {
                    if (this.contentBuilder_ == null) {
                        this.contentBuilder_ = new a1<>(getContent(), getParentForChildren(), isClean());
                        this.content_ = null;
                    }
                    return this.contentBuilder_;
                }

                public static final Descriptors.b getDescriptor() {
                    return FIO.internal_static_TW_FIO_Proto_Action_NewFundsRequest_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearContent() {
                    if (this.contentBuilder_ == null) {
                        this.content_ = null;
                        onChanged();
                    } else {
                        this.content_ = null;
                        this.contentBuilder_ = null;
                    }
                    return this;
                }

                public Builder clearFee() {
                    this.fee_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearPayeeFioName() {
                    this.payeeFioName_ = NewFundsRequest.getDefaultInstance().getPayeeFioName();
                    onChanged();
                    return this;
                }

                public Builder clearPayerFioAddress() {
                    this.payerFioAddress_ = NewFundsRequest.getDefaultInstance().getPayerFioAddress();
                    onChanged();
                    return this;
                }

                public Builder clearPayerFioName() {
                    this.payerFioName_ = NewFundsRequest.getDefaultInstance().getPayerFioName();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public NewFundsContent getContent() {
                    a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> a1Var = this.contentBuilder_;
                    if (a1Var == null) {
                        NewFundsContent newFundsContent = this.content_;
                        return newFundsContent == null ? NewFundsContent.getDefaultInstance() : newFundsContent;
                    }
                    return a1Var.f();
                }

                public NewFundsContent.Builder getContentBuilder() {
                    onChanged();
                    return getContentFieldBuilder().e();
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public NewFundsContentOrBuilder getContentOrBuilder() {
                    a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> a1Var = this.contentBuilder_;
                    if (a1Var != null) {
                        return a1Var.g();
                    }
                    NewFundsContent newFundsContent = this.content_;
                    return newFundsContent == null ? NewFundsContent.getDefaultInstance() : newFundsContent;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return FIO.internal_static_TW_FIO_Proto_Action_NewFundsRequest_descriptor;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public long getFee() {
                    return this.fee_;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public String getPayeeFioName() {
                    Object obj = this.payeeFioName_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.payeeFioName_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public ByteString getPayeeFioNameBytes() {
                    Object obj = this.payeeFioName_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.payeeFioName_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public String getPayerFioAddress() {
                    Object obj = this.payerFioAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.payerFioAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public ByteString getPayerFioAddressBytes() {
                    Object obj = this.payerFioAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.payerFioAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public String getPayerFioName() {
                    Object obj = this.payerFioName_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.payerFioName_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public ByteString getPayerFioNameBytes() {
                    Object obj = this.payerFioName_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.payerFioName_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
                public boolean hasContent() {
                    return (this.contentBuilder_ == null && this.content_ == null) ? false : true;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return FIO.internal_static_TW_FIO_Proto_Action_NewFundsRequest_fieldAccessorTable.d(NewFundsRequest.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder mergeContent(NewFundsContent newFundsContent) {
                    a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> a1Var = this.contentBuilder_;
                    if (a1Var == null) {
                        NewFundsContent newFundsContent2 = this.content_;
                        if (newFundsContent2 != null) {
                            this.content_ = NewFundsContent.newBuilder(newFundsContent2).mergeFrom(newFundsContent).buildPartial();
                        } else {
                            this.content_ = newFundsContent;
                        }
                        onChanged();
                    } else {
                        a1Var.h(newFundsContent);
                    }
                    return this;
                }

                public Builder setContent(NewFundsContent newFundsContent) {
                    a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> a1Var = this.contentBuilder_;
                    if (a1Var == null) {
                        Objects.requireNonNull(newFundsContent);
                        this.content_ = newFundsContent;
                        onChanged();
                    } else {
                        a1Var.j(newFundsContent);
                    }
                    return this;
                }

                public Builder setFee(long j) {
                    this.fee_ = j;
                    onChanged();
                    return this;
                }

                public Builder setPayeeFioName(String str) {
                    Objects.requireNonNull(str);
                    this.payeeFioName_ = str;
                    onChanged();
                    return this;
                }

                public Builder setPayeeFioNameBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.payeeFioName_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setPayerFioAddress(String str) {
                    Objects.requireNonNull(str);
                    this.payerFioAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setPayerFioAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.payerFioAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setPayerFioName(String str) {
                    Objects.requireNonNull(str);
                    this.payerFioName_ = str;
                    onChanged();
                    return this;
                }

                public Builder setPayerFioNameBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.payerFioName_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.payerFioName_ = "";
                    this.payerFioAddress_ = "";
                    this.payeeFioName_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public NewFundsRequest build() {
                    NewFundsRequest buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public NewFundsRequest buildPartial() {
                    NewFundsRequest newFundsRequest = new NewFundsRequest(this, (AnonymousClass1) null);
                    newFundsRequest.payerFioName_ = this.payerFioName_;
                    newFundsRequest.payerFioAddress_ = this.payerFioAddress_;
                    newFundsRequest.payeeFioName_ = this.payeeFioName_;
                    a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> a1Var = this.contentBuilder_;
                    if (a1Var == null) {
                        newFundsRequest.content_ = this.content_;
                    } else {
                        newFundsRequest.content_ = a1Var.b();
                    }
                    newFundsRequest.fee_ = this.fee_;
                    onBuilt();
                    return newFundsRequest;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public NewFundsRequest getDefaultInstanceForType() {
                    return NewFundsRequest.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.payerFioName_ = "";
                    this.payerFioAddress_ = "";
                    this.payeeFioName_ = "";
                    if (this.contentBuilder_ == null) {
                        this.content_ = null;
                    } else {
                        this.content_ = null;
                        this.contentBuilder_ = null;
                    }
                    this.fee_ = 0L;
                    return this;
                }

                public Builder setContent(NewFundsContent.Builder builder) {
                    a1<NewFundsContent, NewFundsContent.Builder, NewFundsContentOrBuilder> a1Var = this.contentBuilder_;
                    if (a1Var == null) {
                        this.content_ = builder.build();
                        onChanged();
                    } else {
                        a1Var.j(builder.build());
                    }
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof NewFundsRequest) {
                        return mergeFrom((NewFundsRequest) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.payerFioName_ = "";
                    this.payerFioAddress_ = "";
                    this.payeeFioName_ = "";
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(NewFundsRequest newFundsRequest) {
                    if (newFundsRequest == NewFundsRequest.getDefaultInstance()) {
                        return this;
                    }
                    if (!newFundsRequest.getPayerFioName().isEmpty()) {
                        this.payerFioName_ = newFundsRequest.payerFioName_;
                        onChanged();
                    }
                    if (!newFundsRequest.getPayerFioAddress().isEmpty()) {
                        this.payerFioAddress_ = newFundsRequest.payerFioAddress_;
                        onChanged();
                    }
                    if (!newFundsRequest.getPayeeFioName().isEmpty()) {
                        this.payeeFioName_ = newFundsRequest.payeeFioName_;
                        onChanged();
                    }
                    if (newFundsRequest.hasContent()) {
                        mergeContent(newFundsRequest.getContent());
                    }
                    if (newFundsRequest.getFee() != 0) {
                        setFee(newFundsRequest.getFee());
                    }
                    mergeUnknownFields(newFundsRequest.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.FIO.Action.NewFundsRequest.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.Action.NewFundsRequest.access$10300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.FIO$Action$NewFundsRequest r3 = (wallet.core.jni.proto.FIO.Action.NewFundsRequest) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.FIO$Action$NewFundsRequest r4 = (wallet.core.jni.proto.FIO.Action.NewFundsRequest) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.Action.NewFundsRequest.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$Action$NewFundsRequest$Builder");
                }
            }

            public /* synthetic */ NewFundsRequest(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static NewFundsRequest getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_Action_NewFundsRequest_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static NewFundsRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (NewFundsRequest) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static NewFundsRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<NewFundsRequest> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof NewFundsRequest)) {
                    return super.equals(obj);
                }
                NewFundsRequest newFundsRequest = (NewFundsRequest) obj;
                if (getPayerFioName().equals(newFundsRequest.getPayerFioName()) && getPayerFioAddress().equals(newFundsRequest.getPayerFioAddress()) && getPayeeFioName().equals(newFundsRequest.getPayeeFioName()) && hasContent() == newFundsRequest.hasContent()) {
                    return (!hasContent() || getContent().equals(newFundsRequest.getContent())) && getFee() == newFundsRequest.getFee() && this.unknownFields.equals(newFundsRequest.unknownFields);
                }
                return false;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public NewFundsContent getContent() {
                NewFundsContent newFundsContent = this.content_;
                return newFundsContent == null ? NewFundsContent.getDefaultInstance() : newFundsContent;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public NewFundsContentOrBuilder getContentOrBuilder() {
                return getContent();
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<NewFundsRequest> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public String getPayeeFioName() {
                Object obj = this.payeeFioName_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.payeeFioName_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public ByteString getPayeeFioNameBytes() {
                Object obj = this.payeeFioName_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.payeeFioName_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public String getPayerFioAddress() {
                Object obj = this.payerFioAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.payerFioAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public ByteString getPayerFioAddressBytes() {
                Object obj = this.payerFioAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.payerFioAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public String getPayerFioName() {
                Object obj = this.payerFioName_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.payerFioName_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public ByteString getPayerFioNameBytes() {
                Object obj = this.payerFioName_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.payerFioName_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getPayerFioNameBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.payerFioName_);
                if (!getPayerFioAddressBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.payerFioAddress_);
                }
                if (!getPayeeFioNameBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(3, this.payeeFioName_);
                }
                if (this.content_ != null) {
                    computeStringSize += CodedOutputStream.G(4, getContent());
                }
                long j = this.fee_;
                if (j != 0) {
                    computeStringSize += CodedOutputStream.a0(5, j);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.FIO.Action.NewFundsRequestOrBuilder
            public boolean hasContent() {
                return this.content_ != null;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPayerFioName().hashCode()) * 37) + 2) * 53) + getPayerFioAddress().hashCode()) * 37) + 3) * 53) + getPayeeFioName().hashCode();
                if (hasContent()) {
                    hashCode = (((hashCode * 37) + 4) * 53) + getContent().hashCode();
                }
                int h = (((((hashCode * 37) + 5) * 53) + a0.h(getFee())) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = h;
                return h;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_Action_NewFundsRequest_fieldAccessorTable.d(NewFundsRequest.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new NewFundsRequest();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getPayerFioNameBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.payerFioName_);
                }
                if (!getPayerFioAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.payerFioAddress_);
                }
                if (!getPayeeFioNameBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 3, this.payeeFioName_);
                }
                if (this.content_ != null) {
                    codedOutputStream.K0(4, getContent());
                }
                long j = this.fee_;
                if (j != 0) {
                    codedOutputStream.d1(5, j);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ NewFundsRequest(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(NewFundsRequest newFundsRequest) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(newFundsRequest);
            }

            public static NewFundsRequest parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private NewFundsRequest(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static NewFundsRequest parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (NewFundsRequest) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static NewFundsRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public NewFundsRequest getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static NewFundsRequest parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private NewFundsRequest() {
                this.memoizedIsInitialized = (byte) -1;
                this.payerFioName_ = "";
                this.payerFioAddress_ = "";
                this.payeeFioName_ = "";
            }

            public static NewFundsRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static NewFundsRequest parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static NewFundsRequest parseFrom(InputStream inputStream) throws IOException {
                return (NewFundsRequest) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static NewFundsRequest parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (NewFundsRequest) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private NewFundsRequest(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.payerFioName_ = jVar.I();
                                } else if (J == 18) {
                                    this.payerFioAddress_ = jVar.I();
                                } else if (J == 26) {
                                    this.payeeFioName_ = jVar.I();
                                } else if (J == 34) {
                                    NewFundsContent newFundsContent = this.content_;
                                    NewFundsContent.Builder builder = newFundsContent != null ? newFundsContent.toBuilder() : null;
                                    NewFundsContent newFundsContent2 = (NewFundsContent) jVar.z(NewFundsContent.parser(), rVar);
                                    this.content_ = newFundsContent2;
                                    if (builder != null) {
                                        builder.mergeFrom(newFundsContent2);
                                        this.content_ = builder.buildPartial();
                                    }
                                } else if (J != 40) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.fee_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static NewFundsRequest parseFrom(j jVar) throws IOException {
                return (NewFundsRequest) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static NewFundsRequest parseFrom(j jVar, r rVar) throws IOException {
                return (NewFundsRequest) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface NewFundsRequestOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            NewFundsContent getContent();

            NewFundsContentOrBuilder getContentOrBuilder();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            long getFee();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            String getPayeeFioName();

            ByteString getPayeeFioNameBytes();

            String getPayerFioAddress();

            ByteString getPayerFioAddressBytes();

            String getPayerFioName();

            ByteString getPayerFioNameBytes();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            boolean hasContent();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class RegisterFioAddress extends GeneratedMessageV3 implements RegisterFioAddressOrBuilder {
            public static final int FEE_FIELD_NUMBER = 3;
            public static final int FIO_ADDRESS_FIELD_NUMBER = 1;
            public static final int OWNER_FIO_PUBLIC_KEY_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private long fee_;
            private volatile Object fioAddress_;
            private byte memoizedIsInitialized;
            private volatile Object ownerFioPublicKey_;
            private static final RegisterFioAddress DEFAULT_INSTANCE = new RegisterFioAddress();
            private static final t0<RegisterFioAddress> PARSER = new c<RegisterFioAddress>() { // from class: wallet.core.jni.proto.FIO.Action.RegisterFioAddress.1
                @Override // com.google.protobuf.t0
                public RegisterFioAddress parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new RegisterFioAddress(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements RegisterFioAddressOrBuilder {
                private long fee_;
                private Object fioAddress_;
                private Object ownerFioPublicKey_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return FIO.internal_static_TW_FIO_Proto_Action_RegisterFioAddress_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearFee() {
                    this.fee_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearFioAddress() {
                    this.fioAddress_ = RegisterFioAddress.getDefaultInstance().getFioAddress();
                    onChanged();
                    return this;
                }

                public Builder clearOwnerFioPublicKey() {
                    this.ownerFioPublicKey_ = RegisterFioAddress.getDefaultInstance().getOwnerFioPublicKey();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return FIO.internal_static_TW_FIO_Proto_Action_RegisterFioAddress_descriptor;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
                public long getFee() {
                    return this.fee_;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
                public String getFioAddress() {
                    Object obj = this.fioAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.fioAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
                public ByteString getFioAddressBytes() {
                    Object obj = this.fioAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.fioAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
                public String getOwnerFioPublicKey() {
                    Object obj = this.ownerFioPublicKey_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.ownerFioPublicKey_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
                public ByteString getOwnerFioPublicKeyBytes() {
                    Object obj = this.ownerFioPublicKey_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.ownerFioPublicKey_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return FIO.internal_static_TW_FIO_Proto_Action_RegisterFioAddress_fieldAccessorTable.d(RegisterFioAddress.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setFee(long j) {
                    this.fee_ = j;
                    onChanged();
                    return this;
                }

                public Builder setFioAddress(String str) {
                    Objects.requireNonNull(str);
                    this.fioAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setFioAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.fioAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setOwnerFioPublicKey(String str) {
                    Objects.requireNonNull(str);
                    this.ownerFioPublicKey_ = str;
                    onChanged();
                    return this;
                }

                public Builder setOwnerFioPublicKeyBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.ownerFioPublicKey_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.fioAddress_ = "";
                    this.ownerFioPublicKey_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public RegisterFioAddress build() {
                    RegisterFioAddress buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public RegisterFioAddress buildPartial() {
                    RegisterFioAddress registerFioAddress = new RegisterFioAddress(this, (AnonymousClass1) null);
                    registerFioAddress.fioAddress_ = this.fioAddress_;
                    registerFioAddress.ownerFioPublicKey_ = this.ownerFioPublicKey_;
                    registerFioAddress.fee_ = this.fee_;
                    onBuilt();
                    return registerFioAddress;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public RegisterFioAddress getDefaultInstanceForType() {
                    return RegisterFioAddress.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.fioAddress_ = "";
                    this.ownerFioPublicKey_ = "";
                    this.fee_ = 0L;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.fioAddress_ = "";
                    this.ownerFioPublicKey_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof RegisterFioAddress) {
                        return mergeFrom((RegisterFioAddress) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(RegisterFioAddress registerFioAddress) {
                    if (registerFioAddress == RegisterFioAddress.getDefaultInstance()) {
                        return this;
                    }
                    if (!registerFioAddress.getFioAddress().isEmpty()) {
                        this.fioAddress_ = registerFioAddress.fioAddress_;
                        onChanged();
                    }
                    if (!registerFioAddress.getOwnerFioPublicKey().isEmpty()) {
                        this.ownerFioPublicKey_ = registerFioAddress.ownerFioPublicKey_;
                        onChanged();
                    }
                    if (registerFioAddress.getFee() != 0) {
                        setFee(registerFioAddress.getFee());
                    }
                    mergeUnknownFields(registerFioAddress.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.FIO.Action.RegisterFioAddress.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.Action.RegisterFioAddress.access$4600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.FIO$Action$RegisterFioAddress r3 = (wallet.core.jni.proto.FIO.Action.RegisterFioAddress) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.FIO$Action$RegisterFioAddress r4 = (wallet.core.jni.proto.FIO.Action.RegisterFioAddress) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.Action.RegisterFioAddress.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$Action$RegisterFioAddress$Builder");
                }
            }

            public /* synthetic */ RegisterFioAddress(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static RegisterFioAddress getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_Action_RegisterFioAddress_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static RegisterFioAddress parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (RegisterFioAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static RegisterFioAddress parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<RegisterFioAddress> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof RegisterFioAddress)) {
                    return super.equals(obj);
                }
                RegisterFioAddress registerFioAddress = (RegisterFioAddress) obj;
                return getFioAddress().equals(registerFioAddress.getFioAddress()) && getOwnerFioPublicKey().equals(registerFioAddress.getOwnerFioPublicKey()) && getFee() == registerFioAddress.getFee() && this.unknownFields.equals(registerFioAddress.unknownFields);
            }

            @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
            public String getFioAddress() {
                Object obj = this.fioAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.fioAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
            public ByteString getFioAddressBytes() {
                Object obj = this.fioAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.fioAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
            public String getOwnerFioPublicKey() {
                Object obj = this.ownerFioPublicKey_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.ownerFioPublicKey_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RegisterFioAddressOrBuilder
            public ByteString getOwnerFioPublicKeyBytes() {
                Object obj = this.ownerFioPublicKey_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerFioPublicKey_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<RegisterFioAddress> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getFioAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.fioAddress_);
                if (!getOwnerFioPublicKeyBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.ownerFioPublicKey_);
                }
                long j = this.fee_;
                if (j != 0) {
                    computeStringSize += CodedOutputStream.a0(3, j);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFioAddress().hashCode()) * 37) + 2) * 53) + getOwnerFioPublicKey().hashCode()) * 37) + 3) * 53) + a0.h(getFee())) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_Action_RegisterFioAddress_fieldAccessorTable.d(RegisterFioAddress.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new RegisterFioAddress();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getFioAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.fioAddress_);
                }
                if (!getOwnerFioPublicKeyBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.ownerFioPublicKey_);
                }
                long j = this.fee_;
                if (j != 0) {
                    codedOutputStream.d1(3, j);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ RegisterFioAddress(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(RegisterFioAddress registerFioAddress) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(registerFioAddress);
            }

            public static RegisterFioAddress parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private RegisterFioAddress(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static RegisterFioAddress parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (RegisterFioAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static RegisterFioAddress parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public RegisterFioAddress getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static RegisterFioAddress parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private RegisterFioAddress() {
                this.memoizedIsInitialized = (byte) -1;
                this.fioAddress_ = "";
                this.ownerFioPublicKey_ = "";
            }

            public static RegisterFioAddress parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static RegisterFioAddress parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static RegisterFioAddress parseFrom(InputStream inputStream) throws IOException {
                return (RegisterFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private RegisterFioAddress(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.fioAddress_ = jVar.I();
                                } else if (J == 18) {
                                    this.ownerFioPublicKey_ = jVar.I();
                                } else if (J != 24) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.fee_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static RegisterFioAddress parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (RegisterFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static RegisterFioAddress parseFrom(j jVar) throws IOException {
                return (RegisterFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static RegisterFioAddress parseFrom(j jVar, r rVar) throws IOException {
                return (RegisterFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface RegisterFioAddressOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            long getFee();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            String getFioAddress();

            ByteString getFioAddressBytes();

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            String getOwnerFioPublicKey();

            ByteString getOwnerFioPublicKeyBytes();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class RenewFioAddress extends GeneratedMessageV3 implements RenewFioAddressOrBuilder {
            public static final int FEE_FIELD_NUMBER = 3;
            public static final int FIO_ADDRESS_FIELD_NUMBER = 1;
            public static final int OWNER_FIO_PUBLIC_KEY_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private long fee_;
            private volatile Object fioAddress_;
            private byte memoizedIsInitialized;
            private volatile Object ownerFioPublicKey_;
            private static final RenewFioAddress DEFAULT_INSTANCE = new RenewFioAddress();
            private static final t0<RenewFioAddress> PARSER = new c<RenewFioAddress>() { // from class: wallet.core.jni.proto.FIO.Action.RenewFioAddress.1
                @Override // com.google.protobuf.t0
                public RenewFioAddress parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new RenewFioAddress(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements RenewFioAddressOrBuilder {
                private long fee_;
                private Object fioAddress_;
                private Object ownerFioPublicKey_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return FIO.internal_static_TW_FIO_Proto_Action_RenewFioAddress_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearFee() {
                    this.fee_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearFioAddress() {
                    this.fioAddress_ = RenewFioAddress.getDefaultInstance().getFioAddress();
                    onChanged();
                    return this;
                }

                public Builder clearOwnerFioPublicKey() {
                    this.ownerFioPublicKey_ = RenewFioAddress.getDefaultInstance().getOwnerFioPublicKey();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return FIO.internal_static_TW_FIO_Proto_Action_RenewFioAddress_descriptor;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
                public long getFee() {
                    return this.fee_;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
                public String getFioAddress() {
                    Object obj = this.fioAddress_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.fioAddress_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
                public ByteString getFioAddressBytes() {
                    Object obj = this.fioAddress_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.fioAddress_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
                public String getOwnerFioPublicKey() {
                    Object obj = this.ownerFioPublicKey_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.ownerFioPublicKey_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
                public ByteString getOwnerFioPublicKeyBytes() {
                    Object obj = this.ownerFioPublicKey_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.ownerFioPublicKey_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return FIO.internal_static_TW_FIO_Proto_Action_RenewFioAddress_fieldAccessorTable.d(RenewFioAddress.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setFee(long j) {
                    this.fee_ = j;
                    onChanged();
                    return this;
                }

                public Builder setFioAddress(String str) {
                    Objects.requireNonNull(str);
                    this.fioAddress_ = str;
                    onChanged();
                    return this;
                }

                public Builder setFioAddressBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.fioAddress_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setOwnerFioPublicKey(String str) {
                    Objects.requireNonNull(str);
                    this.ownerFioPublicKey_ = str;
                    onChanged();
                    return this;
                }

                public Builder setOwnerFioPublicKeyBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.ownerFioPublicKey_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.fioAddress_ = "";
                    this.ownerFioPublicKey_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public RenewFioAddress build() {
                    RenewFioAddress buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public RenewFioAddress buildPartial() {
                    RenewFioAddress renewFioAddress = new RenewFioAddress(this, (AnonymousClass1) null);
                    renewFioAddress.fioAddress_ = this.fioAddress_;
                    renewFioAddress.ownerFioPublicKey_ = this.ownerFioPublicKey_;
                    renewFioAddress.fee_ = this.fee_;
                    onBuilt();
                    return renewFioAddress;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public RenewFioAddress getDefaultInstanceForType() {
                    return RenewFioAddress.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.fioAddress_ = "";
                    this.ownerFioPublicKey_ = "";
                    this.fee_ = 0L;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.fioAddress_ = "";
                    this.ownerFioPublicKey_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof RenewFioAddress) {
                        return mergeFrom((RenewFioAddress) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(RenewFioAddress renewFioAddress) {
                    if (renewFioAddress == RenewFioAddress.getDefaultInstance()) {
                        return this;
                    }
                    if (!renewFioAddress.getFioAddress().isEmpty()) {
                        this.fioAddress_ = renewFioAddress.fioAddress_;
                        onChanged();
                    }
                    if (!renewFioAddress.getOwnerFioPublicKey().isEmpty()) {
                        this.ownerFioPublicKey_ = renewFioAddress.ownerFioPublicKey_;
                        onChanged();
                    }
                    if (renewFioAddress.getFee() != 0) {
                        setFee(renewFioAddress.getFee());
                    }
                    mergeUnknownFields(renewFioAddress.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.FIO.Action.RenewFioAddress.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.Action.RenewFioAddress.access$8700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.FIO$Action$RenewFioAddress r3 = (wallet.core.jni.proto.FIO.Action.RenewFioAddress) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.FIO$Action$RenewFioAddress r4 = (wallet.core.jni.proto.FIO.Action.RenewFioAddress) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.Action.RenewFioAddress.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$Action$RenewFioAddress$Builder");
                }
            }

            public /* synthetic */ RenewFioAddress(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static RenewFioAddress getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_Action_RenewFioAddress_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static RenewFioAddress parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (RenewFioAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static RenewFioAddress parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<RenewFioAddress> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof RenewFioAddress)) {
                    return super.equals(obj);
                }
                RenewFioAddress renewFioAddress = (RenewFioAddress) obj;
                return getFioAddress().equals(renewFioAddress.getFioAddress()) && getOwnerFioPublicKey().equals(renewFioAddress.getOwnerFioPublicKey()) && getFee() == renewFioAddress.getFee() && this.unknownFields.equals(renewFioAddress.unknownFields);
            }

            @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
            public String getFioAddress() {
                Object obj = this.fioAddress_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.fioAddress_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
            public ByteString getFioAddressBytes() {
                Object obj = this.fioAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.fioAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
            public String getOwnerFioPublicKey() {
                Object obj = this.ownerFioPublicKey_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.ownerFioPublicKey_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.RenewFioAddressOrBuilder
            public ByteString getOwnerFioPublicKeyBytes() {
                Object obj = this.ownerFioPublicKey_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.ownerFioPublicKey_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<RenewFioAddress> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getFioAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.fioAddress_);
                if (!getOwnerFioPublicKeyBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.ownerFioPublicKey_);
                }
                long j = this.fee_;
                if (j != 0) {
                    computeStringSize += CodedOutputStream.a0(3, j);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFioAddress().hashCode()) * 37) + 2) * 53) + getOwnerFioPublicKey().hashCode()) * 37) + 3) * 53) + a0.h(getFee())) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_Action_RenewFioAddress_fieldAccessorTable.d(RenewFioAddress.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new RenewFioAddress();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getFioAddressBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.fioAddress_);
                }
                if (!getOwnerFioPublicKeyBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.ownerFioPublicKey_);
                }
                long j = this.fee_;
                if (j != 0) {
                    codedOutputStream.d1(3, j);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ RenewFioAddress(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(RenewFioAddress renewFioAddress) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(renewFioAddress);
            }

            public static RenewFioAddress parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private RenewFioAddress(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static RenewFioAddress parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (RenewFioAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static RenewFioAddress parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public RenewFioAddress getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static RenewFioAddress parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private RenewFioAddress() {
                this.memoizedIsInitialized = (byte) -1;
                this.fioAddress_ = "";
                this.ownerFioPublicKey_ = "";
            }

            public static RenewFioAddress parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static RenewFioAddress parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static RenewFioAddress parseFrom(InputStream inputStream) throws IOException {
                return (RenewFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private RenewFioAddress(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.fioAddress_ = jVar.I();
                                } else if (J == 18) {
                                    this.ownerFioPublicKey_ = jVar.I();
                                } else if (J != 24) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.fee_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static RenewFioAddress parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (RenewFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static RenewFioAddress parseFrom(j jVar) throws IOException {
                return (RenewFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static RenewFioAddress parseFrom(j jVar, r rVar) throws IOException {
                return (RenewFioAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface RenewFioAddressOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            long getFee();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            String getFioAddress();

            ByteString getFioAddressBytes();

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            String getOwnerFioPublicKey();

            ByteString getOwnerFioPublicKeyBytes();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class Transfer extends GeneratedMessageV3 implements TransferOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 2;
            public static final int FEE_FIELD_NUMBER = 3;
            public static final int PAYEE_PUBLIC_KEY_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private long amount_;
            private long fee_;
            private byte memoizedIsInitialized;
            private volatile Object payeePublicKey_;
            private static final Transfer DEFAULT_INSTANCE = new Transfer();
            private static final t0<Transfer> PARSER = new c<Transfer>() { // from class: wallet.core.jni.proto.FIO.Action.Transfer.1
                @Override // com.google.protobuf.t0
                public Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Transfer(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOrBuilder {
                private long amount_;
                private long fee_;
                private Object payeePublicKey_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return FIO.internal_static_TW_FIO_Proto_Action_Transfer_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearFee() {
                    this.fee_ = 0L;
                    onChanged();
                    return this;
                }

                public Builder clearPayeePublicKey() {
                    this.payeePublicKey_ = Transfer.getDefaultInstance().getPayeePublicKey();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
                public long getAmount() {
                    return this.amount_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return FIO.internal_static_TW_FIO_Proto_Action_Transfer_descriptor;
                }

                @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
                public long getFee() {
                    return this.fee_;
                }

                @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
                public String getPayeePublicKey() {
                    Object obj = this.payeePublicKey_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.payeePublicKey_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
                public ByteString getPayeePublicKeyBytes() {
                    Object obj = this.payeePublicKey_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.payeePublicKey_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return FIO.internal_static_TW_FIO_Proto_Action_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(long j) {
                    this.amount_ = j;
                    onChanged();
                    return this;
                }

                public Builder setFee(long j) {
                    this.fee_ = j;
                    onChanged();
                    return this;
                }

                public Builder setPayeePublicKey(String str) {
                    Objects.requireNonNull(str);
                    this.payeePublicKey_ = str;
                    onChanged();
                    return this;
                }

                public Builder setPayeePublicKeyBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.payeePublicKey_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.payeePublicKey_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer build() {
                    Transfer buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer buildPartial() {
                    Transfer transfer = new Transfer(this, (AnonymousClass1) null);
                    transfer.payeePublicKey_ = this.payeePublicKey_;
                    transfer.amount_ = this.amount_;
                    transfer.fee_ = this.fee_;
                    onBuilt();
                    return transfer;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Transfer getDefaultInstanceForType() {
                    return Transfer.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.payeePublicKey_ = "";
                    this.amount_ = 0L;
                    this.fee_ = 0L;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.payeePublicKey_ = "";
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Transfer) {
                        return mergeFrom((Transfer) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Transfer transfer) {
                    if (transfer == Transfer.getDefaultInstance()) {
                        return this;
                    }
                    if (!transfer.getPayeePublicKey().isEmpty()) {
                        this.payeePublicKey_ = transfer.payeePublicKey_;
                        onChanged();
                    }
                    if (transfer.getAmount() != 0) {
                        setAmount(transfer.getAmount());
                    }
                    if (transfer.getFee() != 0) {
                        setFee(transfer.getFee());
                    }
                    mergeUnknownFields(transfer.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.FIO.Action.Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.Action.Transfer.access$7400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.FIO$Action$Transfer r3 = (wallet.core.jni.proto.FIO.Action.Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.FIO$Action$Transfer r4 = (wallet.core.jni.proto.FIO.Action.Transfer) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.Action.Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$Action$Transfer$Builder");
                }
            }

            public /* synthetic */ Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Transfer getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_Action_Transfer_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Transfer> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Transfer)) {
                    return super.equals(obj);
                }
                Transfer transfer = (Transfer) obj;
                return getPayeePublicKey().equals(transfer.getPayeePublicKey()) && getAmount() == transfer.getAmount() && getFee() == transfer.getFee() && this.unknownFields.equals(transfer.unknownFields);
            }

            @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Transfer> getParserForType() {
                return PARSER;
            }

            @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
            public String getPayeePublicKey() {
                Object obj = this.payeePublicKey_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.payeePublicKey_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.FIO.Action.TransferOrBuilder
            public ByteString getPayeePublicKeyBytes() {
                Object obj = this.payeePublicKey_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.payeePublicKey_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getPayeePublicKeyBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.payeePublicKey_);
                long j = this.amount_;
                if (j != 0) {
                    computeStringSize += CodedOutputStream.a0(2, j);
                }
                long j2 = this.fee_;
                if (j2 != 0) {
                    computeStringSize += CodedOutputStream.a0(3, j2);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPayeePublicKey().hashCode()) * 37) + 2) * 53) + a0.h(getAmount())) * 37) + 3) * 53) + a0.h(getFee())) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_Action_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Transfer();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getPayeePublicKeyBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.payeePublicKey_);
                }
                long j = this.amount_;
                if (j != 0) {
                    codedOutputStream.d1(2, j);
                }
                long j2 = this.fee_;
                if (j2 != 0) {
                    codedOutputStream.d1(3, j2);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Transfer transfer) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(transfer);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Transfer(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Transfer getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Transfer() {
                this.memoizedIsInitialized = (byte) -1;
                this.payeePublicKey_ = "";
            }

            public static Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Transfer parseFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.payeePublicKey_ = jVar.I();
                                } else if (J == 16) {
                                    this.amount_ = jVar.L();
                                } else if (J != 24) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.fee_ = jVar.L();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(j jVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Transfer parseFrom(j jVar, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface TransferOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            long getAmount();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            long getFee();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            String getPayeePublicKey();

            ByteString getPayeePublicKeyBytes();

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ Action(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Action getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return FIO.internal_static_TW_FIO_Proto_Action_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Action parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Action) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Action parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Action> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Action)) {
                return super.equals(obj);
            }
            Action action = (Action) obj;
            if (getMessageOneofCase().equals(action.getMessageOneofCase())) {
                int i = this.messageOneofCase_;
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                if (i == 5 && !getNewFundsRequestMessage().equals(action.getNewFundsRequestMessage())) {
                                    return false;
                                }
                            } else if (!getRenewFioAddressMessage().equals(action.getRenewFioAddressMessage())) {
                                return false;
                            }
                        } else if (!getTransferMessage().equals(action.getTransferMessage())) {
                            return false;
                        }
                    } else if (!getAddPubAddressMessage().equals(action.getAddPubAddressMessage())) {
                        return false;
                    }
                } else if (!getRegisterFioAddressMessage().equals(action.getRegisterFioAddressMessage())) {
                    return false;
                }
                return this.unknownFields.equals(action.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public AddPubAddress getAddPubAddressMessage() {
            if (this.messageOneofCase_ == 2) {
                return (AddPubAddress) this.messageOneof_;
            }
            return AddPubAddress.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public AddPubAddressOrBuilder getAddPubAddressMessageOrBuilder() {
            if (this.messageOneofCase_ == 2) {
                return (AddPubAddress) this.messageOneof_;
            }
            return AddPubAddress.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public MessageOneofCase getMessageOneofCase() {
            return MessageOneofCase.forNumber(this.messageOneofCase_);
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public NewFundsRequest getNewFundsRequestMessage() {
            if (this.messageOneofCase_ == 5) {
                return (NewFundsRequest) this.messageOneof_;
            }
            return NewFundsRequest.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public NewFundsRequestOrBuilder getNewFundsRequestMessageOrBuilder() {
            if (this.messageOneofCase_ == 5) {
                return (NewFundsRequest) this.messageOneof_;
            }
            return NewFundsRequest.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Action> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public RegisterFioAddress getRegisterFioAddressMessage() {
            if (this.messageOneofCase_ == 1) {
                return (RegisterFioAddress) this.messageOneof_;
            }
            return RegisterFioAddress.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public RegisterFioAddressOrBuilder getRegisterFioAddressMessageOrBuilder() {
            if (this.messageOneofCase_ == 1) {
                return (RegisterFioAddress) this.messageOneof_;
            }
            return RegisterFioAddress.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public RenewFioAddress getRenewFioAddressMessage() {
            if (this.messageOneofCase_ == 4) {
                return (RenewFioAddress) this.messageOneof_;
            }
            return RenewFioAddress.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public RenewFioAddressOrBuilder getRenewFioAddressMessageOrBuilder() {
            if (this.messageOneofCase_ == 4) {
                return (RenewFioAddress) this.messageOneof_;
            }
            return RenewFioAddress.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.messageOneofCase_ == 1 ? 0 + CodedOutputStream.G(1, (RegisterFioAddress) this.messageOneof_) : 0;
            if (this.messageOneofCase_ == 2) {
                G += CodedOutputStream.G(2, (AddPubAddress) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 3) {
                G += CodedOutputStream.G(3, (Transfer) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                G += CodedOutputStream.G(4, (RenewFioAddress) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                G += CodedOutputStream.G(5, (NewFundsRequest) this.messageOneof_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public Transfer getTransferMessage() {
            if (this.messageOneofCase_ == 3) {
                return (Transfer) this.messageOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public TransferOrBuilder getTransferMessageOrBuilder() {
            if (this.messageOneofCase_ == 3) {
                return (Transfer) this.messageOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public boolean hasAddPubAddressMessage() {
            return this.messageOneofCase_ == 2;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public boolean hasNewFundsRequestMessage() {
            return this.messageOneofCase_ == 5;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public boolean hasRegisterFioAddressMessage() {
            return this.messageOneofCase_ == 1;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public boolean hasRenewFioAddressMessage() {
            return this.messageOneofCase_ == 4;
        }

        @Override // wallet.core.jni.proto.FIO.ActionOrBuilder
        public boolean hasTransferMessage() {
            return this.messageOneofCase_ == 3;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = 779 + getDescriptor().hashCode();
            int i3 = this.messageOneofCase_;
            if (i3 == 1) {
                i = ((hashCode2 * 37) + 1) * 53;
                hashCode = getRegisterFioAddressMessage().hashCode();
            } else if (i3 == 2) {
                i = ((hashCode2 * 37) + 2) * 53;
                hashCode = getAddPubAddressMessage().hashCode();
            } else if (i3 == 3) {
                i = ((hashCode2 * 37) + 3) * 53;
                hashCode = getTransferMessage().hashCode();
            } else if (i3 == 4) {
                i = ((hashCode2 * 37) + 4) * 53;
                hashCode = getRenewFioAddressMessage().hashCode();
            } else {
                if (i3 == 5) {
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getNewFundsRequestMessage().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return FIO.internal_static_TW_FIO_Proto_Action_fieldAccessorTable.d(Action.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Action();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.messageOneofCase_ == 1) {
                codedOutputStream.K0(1, (RegisterFioAddress) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 2) {
                codedOutputStream.K0(2, (AddPubAddress) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 3) {
                codedOutputStream.K0(3, (Transfer) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 4) {
                codedOutputStream.K0(4, (RenewFioAddress) this.messageOneof_);
            }
            if (this.messageOneofCase_ == 5) {
                codedOutputStream.K0(5, (NewFundsRequest) this.messageOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Action(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Action action) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(action);
        }

        public static Action parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Action(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Action parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Action parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Action getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Action parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Action parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Action() {
            this.messageOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Action parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Action parseFrom(InputStream inputStream) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Action(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                RegisterFioAddress.Builder builder = this.messageOneofCase_ == 1 ? ((RegisterFioAddress) this.messageOneof_).toBuilder() : null;
                                m0 z2 = jVar.z(RegisterFioAddress.parser(), rVar);
                                this.messageOneof_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((RegisterFioAddress) z2);
                                    this.messageOneof_ = builder.buildPartial();
                                }
                                this.messageOneofCase_ = 1;
                            } else if (J == 18) {
                                AddPubAddress.Builder builder2 = this.messageOneofCase_ == 2 ? ((AddPubAddress) this.messageOneof_).toBuilder() : null;
                                m0 z3 = jVar.z(AddPubAddress.parser(), rVar);
                                this.messageOneof_ = z3;
                                if (builder2 != null) {
                                    builder2.mergeFrom((AddPubAddress) z3);
                                    this.messageOneof_ = builder2.buildPartial();
                                }
                                this.messageOneofCase_ = 2;
                            } else if (J == 26) {
                                Transfer.Builder builder3 = this.messageOneofCase_ == 3 ? ((Transfer) this.messageOneof_).toBuilder() : null;
                                m0 z4 = jVar.z(Transfer.parser(), rVar);
                                this.messageOneof_ = z4;
                                if (builder3 != null) {
                                    builder3.mergeFrom((Transfer) z4);
                                    this.messageOneof_ = builder3.buildPartial();
                                }
                                this.messageOneofCase_ = 3;
                            } else if (J == 34) {
                                RenewFioAddress.Builder builder4 = this.messageOneofCase_ == 4 ? ((RenewFioAddress) this.messageOneof_).toBuilder() : null;
                                m0 z5 = jVar.z(RenewFioAddress.parser(), rVar);
                                this.messageOneof_ = z5;
                                if (builder4 != null) {
                                    builder4.mergeFrom((RenewFioAddress) z5);
                                    this.messageOneof_ = builder4.buildPartial();
                                }
                                this.messageOneofCase_ = 4;
                            } else if (J != 42) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                NewFundsRequest.Builder builder5 = this.messageOneofCase_ == 5 ? ((NewFundsRequest) this.messageOneof_).toBuilder() : null;
                                m0 z6 = jVar.z(NewFundsRequest.parser(), rVar);
                                this.messageOneof_ = z6;
                                if (builder5 != null) {
                                    builder5.mergeFrom((NewFundsRequest) z6);
                                    this.messageOneof_ = builder5.buildPartial();
                                }
                                this.messageOneofCase_ = 5;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Action parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Action parseFrom(j jVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Action parseFrom(j jVar, r rVar) throws IOException {
            return (Action) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface ActionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        Action.AddPubAddress getAddPubAddressMessage();

        Action.AddPubAddressOrBuilder getAddPubAddressMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        Action.MessageOneofCase getMessageOneofCase();

        Action.NewFundsRequest getNewFundsRequestMessage();

        Action.NewFundsRequestOrBuilder getNewFundsRequestMessageOrBuilder();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Action.RegisterFioAddress getRegisterFioAddressMessage();

        Action.RegisterFioAddressOrBuilder getRegisterFioAddressMessageOrBuilder();

        Action.RenewFioAddress getRenewFioAddressMessage();

        Action.RenewFioAddressOrBuilder getRenewFioAddressMessageOrBuilder();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Action.Transfer getTransferMessage();

        Action.TransferOrBuilder getTransferMessageOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasAddPubAddressMessage();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasNewFundsRequestMessage();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasRegisterFioAddressMessage();

        boolean hasRenewFioAddressMessage();

        boolean hasTransferMessage();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class ChainParams extends GeneratedMessageV3 implements ChainParamsOrBuilder {
        public static final int CHAIN_ID_FIELD_NUMBER = 1;
        public static final int HEAD_BLOCK_NUMBER_FIELD_NUMBER = 2;
        public static final int REF_BLOCK_PREFIX_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ByteString chainId_;
        private long headBlockNumber_;
        private byte memoizedIsInitialized;
        private long refBlockPrefix_;
        private static final ChainParams DEFAULT_INSTANCE = new ChainParams();
        private static final t0<ChainParams> PARSER = new c<ChainParams>() { // from class: wallet.core.jni.proto.FIO.ChainParams.1
            @Override // com.google.protobuf.t0
            public ChainParams parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new ChainParams(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements ChainParamsOrBuilder {
            private ByteString chainId_;
            private long headBlockNumber_;
            private long refBlockPrefix_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_ChainParams_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearChainId() {
                this.chainId_ = ChainParams.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearHeadBlockNumber() {
                this.headBlockNumber_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearRefBlockPrefix() {
                this.refBlockPrefix_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.FIO.ChainParamsOrBuilder
            public ByteString getChainId() {
                return this.chainId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return FIO.internal_static_TW_FIO_Proto_ChainParams_descriptor;
            }

            @Override // wallet.core.jni.proto.FIO.ChainParamsOrBuilder
            public long getHeadBlockNumber() {
                return this.headBlockNumber_;
            }

            @Override // wallet.core.jni.proto.FIO.ChainParamsOrBuilder
            public long getRefBlockPrefix() {
                return this.refBlockPrefix_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_ChainParams_fieldAccessorTable.d(ChainParams.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setChainId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setHeadBlockNumber(long j) {
                this.headBlockNumber_ = j;
                onChanged();
                return this;
            }

            public Builder setRefBlockPrefix(long j) {
                this.refBlockPrefix_ = j;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.chainId_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public ChainParams build() {
                ChainParams buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public ChainParams buildPartial() {
                ChainParams chainParams = new ChainParams(this, (AnonymousClass1) null);
                chainParams.chainId_ = this.chainId_;
                chainParams.headBlockNumber_ = this.headBlockNumber_;
                chainParams.refBlockPrefix_ = this.refBlockPrefix_;
                onBuilt();
                return chainParams;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public ChainParams getDefaultInstanceForType() {
                return ChainParams.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.chainId_ = ByteString.EMPTY;
                this.headBlockNumber_ = 0L;
                this.refBlockPrefix_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.chainId_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof ChainParams) {
                    return mergeFrom((ChainParams) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(ChainParams chainParams) {
                if (chainParams == ChainParams.getDefaultInstance()) {
                    return this;
                }
                if (chainParams.getChainId() != ByteString.EMPTY) {
                    setChainId(chainParams.getChainId());
                }
                if (chainParams.getHeadBlockNumber() != 0) {
                    setHeadBlockNumber(chainParams.getHeadBlockNumber());
                }
                if (chainParams.getRefBlockPrefix() != 0) {
                    setRefBlockPrefix(chainParams.getRefBlockPrefix());
                }
                mergeUnknownFields(chainParams.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.FIO.ChainParams.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.ChainParams.access$12700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.FIO$ChainParams r3 = (wallet.core.jni.proto.FIO.ChainParams) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.FIO$ChainParams r4 = (wallet.core.jni.proto.FIO.ChainParams) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.ChainParams.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$ChainParams$Builder");
            }
        }

        public /* synthetic */ ChainParams(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static ChainParams getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return FIO.internal_static_TW_FIO_Proto_ChainParams_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static ChainParams parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ChainParams) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static ChainParams parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<ChainParams> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ChainParams)) {
                return super.equals(obj);
            }
            ChainParams chainParams = (ChainParams) obj;
            return getChainId().equals(chainParams.getChainId()) && getHeadBlockNumber() == chainParams.getHeadBlockNumber() && getRefBlockPrefix() == chainParams.getRefBlockPrefix() && this.unknownFields.equals(chainParams.unknownFields);
        }

        @Override // wallet.core.jni.proto.FIO.ChainParamsOrBuilder
        public ByteString getChainId() {
            return this.chainId_;
        }

        @Override // wallet.core.jni.proto.FIO.ChainParamsOrBuilder
        public long getHeadBlockNumber() {
            return this.headBlockNumber_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<ChainParams> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.FIO.ChainParamsOrBuilder
        public long getRefBlockPrefix() {
            return this.refBlockPrefix_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.chainId_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.chainId_);
            long j = this.headBlockNumber_;
            if (j != 0) {
                h += CodedOutputStream.a0(2, j);
            }
            long j2 = this.refBlockPrefix_;
            if (j2 != 0) {
                h += CodedOutputStream.a0(3, j2);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getChainId().hashCode()) * 37) + 2) * 53) + a0.h(getHeadBlockNumber())) * 37) + 3) * 53) + a0.h(getRefBlockPrefix())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return FIO.internal_static_TW_FIO_Proto_ChainParams_fieldAccessorTable.d(ChainParams.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new ChainParams();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.chainId_.isEmpty()) {
                codedOutputStream.q0(1, this.chainId_);
            }
            long j = this.headBlockNumber_;
            if (j != 0) {
                codedOutputStream.d1(2, j);
            }
            long j2 = this.refBlockPrefix_;
            if (j2 != 0) {
                codedOutputStream.d1(3, j2);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ ChainParams(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(ChainParams chainParams) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(chainParams);
        }

        public static ChainParams parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private ChainParams(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static ChainParams parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (ChainParams) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static ChainParams parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public ChainParams getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static ChainParams parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private ChainParams() {
            this.memoizedIsInitialized = (byte) -1;
            this.chainId_ = ByteString.EMPTY;
        }

        public static ChainParams parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static ChainParams parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static ChainParams parseFrom(InputStream inputStream) throws IOException {
            return (ChainParams) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private ChainParams(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.chainId_ = jVar.q();
                            } else if (J == 16) {
                                this.headBlockNumber_ = jVar.L();
                            } else if (J != 24) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.refBlockPrefix_ = jVar.L();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static ChainParams parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (ChainParams) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static ChainParams parseFrom(j jVar) throws IOException {
            return (ChainParams) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static ChainParams parseFrom(j jVar, r rVar) throws IOException {
            return (ChainParams) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface ChainParamsOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getChainId();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getHeadBlockNumber();

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        long getRefBlockPrefix();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class NewFundsContent extends GeneratedMessageV3 implements NewFundsContentOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 2;
        public static final int COIN_SYMBOL_FIELD_NUMBER = 3;
        public static final int HASH_FIELD_NUMBER = 5;
        public static final int MEMO_FIELD_NUMBER = 4;
        public static final int OFFLINE_URL_FIELD_NUMBER = 6;
        public static final int PAYEE_PUBLIC_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object amount_;
        private volatile Object coinSymbol_;
        private volatile Object hash_;
        private volatile Object memo_;
        private byte memoizedIsInitialized;
        private volatile Object offlineUrl_;
        private volatile Object payeePublicAddress_;
        private static final NewFundsContent DEFAULT_INSTANCE = new NewFundsContent();
        private static final t0<NewFundsContent> PARSER = new c<NewFundsContent>() { // from class: wallet.core.jni.proto.FIO.NewFundsContent.1
            @Override // com.google.protobuf.t0
            public NewFundsContent parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new NewFundsContent(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements NewFundsContentOrBuilder {
            private Object amount_;
            private Object coinSymbol_;
            private Object hash_;
            private Object memo_;
            private Object offlineUrl_;
            private Object payeePublicAddress_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_NewFundsContent_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = NewFundsContent.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearCoinSymbol() {
                this.coinSymbol_ = NewFundsContent.getDefaultInstance().getCoinSymbol();
                onChanged();
                return this;
            }

            public Builder clearHash() {
                this.hash_ = NewFundsContent.getDefaultInstance().getHash();
                onChanged();
                return this;
            }

            public Builder clearMemo() {
                this.memo_ = NewFundsContent.getDefaultInstance().getMemo();
                onChanged();
                return this;
            }

            public Builder clearOfflineUrl() {
                this.offlineUrl_ = NewFundsContent.getDefaultInstance().getOfflineUrl();
                onChanged();
                return this;
            }

            public Builder clearPayeePublicAddress() {
                this.payeePublicAddress_ = NewFundsContent.getDefaultInstance().getPayeePublicAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public String getAmount() {
                Object obj = this.amount_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.amount_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public ByteString getAmountBytes() {
                Object obj = this.amount_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.amount_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public String getCoinSymbol() {
                Object obj = this.coinSymbol_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.coinSymbol_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public ByteString getCoinSymbolBytes() {
                Object obj = this.coinSymbol_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.coinSymbol_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return FIO.internal_static_TW_FIO_Proto_NewFundsContent_descriptor;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public String getHash() {
                Object obj = this.hash_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.hash_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public ByteString getHashBytes() {
                Object obj = this.hash_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.hash_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public String getMemo() {
                Object obj = this.memo_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.memo_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public ByteString getMemoBytes() {
                Object obj = this.memo_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.memo_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public String getOfflineUrl() {
                Object obj = this.offlineUrl_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.offlineUrl_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public ByteString getOfflineUrlBytes() {
                Object obj = this.offlineUrl_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.offlineUrl_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public String getPayeePublicAddress() {
                Object obj = this.payeePublicAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.payeePublicAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
            public ByteString getPayeePublicAddressBytes() {
                Object obj = this.payeePublicAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.payeePublicAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_NewFundsContent_fieldAccessorTable.d(NewFundsContent.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(String str) {
                Objects.requireNonNull(str);
                this.amount_ = str;
                onChanged();
                return this;
            }

            public Builder setAmountBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setCoinSymbol(String str) {
                Objects.requireNonNull(str);
                this.coinSymbol_ = str;
                onChanged();
                return this;
            }

            public Builder setCoinSymbolBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.coinSymbol_ = byteString;
                onChanged();
                return this;
            }

            public Builder setHash(String str) {
                Objects.requireNonNull(str);
                this.hash_ = str;
                onChanged();
                return this;
            }

            public Builder setHashBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.hash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setMemo(String str) {
                Objects.requireNonNull(str);
                this.memo_ = str;
                onChanged();
                return this;
            }

            public Builder setMemoBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.memo_ = byteString;
                onChanged();
                return this;
            }

            public Builder setOfflineUrl(String str) {
                Objects.requireNonNull(str);
                this.offlineUrl_ = str;
                onChanged();
                return this;
            }

            public Builder setOfflineUrlBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.offlineUrl_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPayeePublicAddress(String str) {
                Objects.requireNonNull(str);
                this.payeePublicAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setPayeePublicAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.payeePublicAddress_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.payeePublicAddress_ = "";
                this.amount_ = "";
                this.coinSymbol_ = "";
                this.memo_ = "";
                this.hash_ = "";
                this.offlineUrl_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public NewFundsContent build() {
                NewFundsContent buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public NewFundsContent buildPartial() {
                NewFundsContent newFundsContent = new NewFundsContent(this, (AnonymousClass1) null);
                newFundsContent.payeePublicAddress_ = this.payeePublicAddress_;
                newFundsContent.amount_ = this.amount_;
                newFundsContent.coinSymbol_ = this.coinSymbol_;
                newFundsContent.memo_ = this.memo_;
                newFundsContent.hash_ = this.hash_;
                newFundsContent.offlineUrl_ = this.offlineUrl_;
                onBuilt();
                return newFundsContent;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public NewFundsContent getDefaultInstanceForType() {
                return NewFundsContent.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.payeePublicAddress_ = "";
                this.amount_ = "";
                this.coinSymbol_ = "";
                this.memo_ = "";
                this.hash_ = "";
                this.offlineUrl_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof NewFundsContent) {
                    return mergeFrom((NewFundsContent) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(NewFundsContent newFundsContent) {
                if (newFundsContent == NewFundsContent.getDefaultInstance()) {
                    return this;
                }
                if (!newFundsContent.getPayeePublicAddress().isEmpty()) {
                    this.payeePublicAddress_ = newFundsContent.payeePublicAddress_;
                    onChanged();
                }
                if (!newFundsContent.getAmount().isEmpty()) {
                    this.amount_ = newFundsContent.amount_;
                    onChanged();
                }
                if (!newFundsContent.getCoinSymbol().isEmpty()) {
                    this.coinSymbol_ = newFundsContent.coinSymbol_;
                    onChanged();
                }
                if (!newFundsContent.getMemo().isEmpty()) {
                    this.memo_ = newFundsContent.memo_;
                    onChanged();
                }
                if (!newFundsContent.getHash().isEmpty()) {
                    this.hash_ = newFundsContent.hash_;
                    onChanged();
                }
                if (!newFundsContent.getOfflineUrl().isEmpty()) {
                    this.offlineUrl_ = newFundsContent.offlineUrl_;
                    onChanged();
                }
                mergeUnknownFields(newFundsContent.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.payeePublicAddress_ = "";
                this.amount_ = "";
                this.coinSymbol_ = "";
                this.memo_ = "";
                this.hash_ = "";
                this.offlineUrl_ = "";
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.FIO.NewFundsContent.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.NewFundsContent.access$2600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.FIO$NewFundsContent r3 = (wallet.core.jni.proto.FIO.NewFundsContent) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.FIO$NewFundsContent r4 = (wallet.core.jni.proto.FIO.NewFundsContent) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.NewFundsContent.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$NewFundsContent$Builder");
            }
        }

        public /* synthetic */ NewFundsContent(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static NewFundsContent getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return FIO.internal_static_TW_FIO_Proto_NewFundsContent_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static NewFundsContent parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (NewFundsContent) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static NewFundsContent parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<NewFundsContent> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof NewFundsContent)) {
                return super.equals(obj);
            }
            NewFundsContent newFundsContent = (NewFundsContent) obj;
            return getPayeePublicAddress().equals(newFundsContent.getPayeePublicAddress()) && getAmount().equals(newFundsContent.getAmount()) && getCoinSymbol().equals(newFundsContent.getCoinSymbol()) && getMemo().equals(newFundsContent.getMemo()) && getHash().equals(newFundsContent.getHash()) && getOfflineUrl().equals(newFundsContent.getOfflineUrl()) && this.unknownFields.equals(newFundsContent.unknownFields);
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public String getAmount() {
            Object obj = this.amount_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.amount_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public ByteString getAmountBytes() {
            Object obj = this.amount_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.amount_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public String getCoinSymbol() {
            Object obj = this.coinSymbol_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.coinSymbol_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public ByteString getCoinSymbolBytes() {
            Object obj = this.coinSymbol_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.coinSymbol_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public String getHash() {
            Object obj = this.hash_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.hash_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public ByteString getHashBytes() {
            Object obj = this.hash_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.hash_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public String getMemo() {
            Object obj = this.memo_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.memo_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public ByteString getMemoBytes() {
            Object obj = this.memo_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.memo_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public String getOfflineUrl() {
            Object obj = this.offlineUrl_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.offlineUrl_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public ByteString getOfflineUrlBytes() {
            Object obj = this.offlineUrl_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.offlineUrl_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<NewFundsContent> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public String getPayeePublicAddress() {
            Object obj = this.payeePublicAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.payeePublicAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.NewFundsContentOrBuilder
        public ByteString getPayeePublicAddressBytes() {
            Object obj = this.payeePublicAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.payeePublicAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getPayeePublicAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.payeePublicAddress_);
            if (!getAmountBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.amount_);
            }
            if (!getCoinSymbolBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.coinSymbol_);
            }
            if (!getMemoBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(4, this.memo_);
            }
            if (!getHashBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(5, this.hash_);
            }
            if (!getOfflineUrlBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(6, this.offlineUrl_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPayeePublicAddress().hashCode()) * 37) + 2) * 53) + getAmount().hashCode()) * 37) + 3) * 53) + getCoinSymbol().hashCode()) * 37) + 4) * 53) + getMemo().hashCode()) * 37) + 5) * 53) + getHash().hashCode()) * 37) + 6) * 53) + getOfflineUrl().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return FIO.internal_static_TW_FIO_Proto_NewFundsContent_fieldAccessorTable.d(NewFundsContent.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new NewFundsContent();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getPayeePublicAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.payeePublicAddress_);
            }
            if (!getAmountBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.amount_);
            }
            if (!getCoinSymbolBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.coinSymbol_);
            }
            if (!getMemoBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.memo_);
            }
            if (!getHashBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.hash_);
            }
            if (!getOfflineUrlBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 6, this.offlineUrl_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ NewFundsContent(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(NewFundsContent newFundsContent) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(newFundsContent);
        }

        public static NewFundsContent parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private NewFundsContent(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static NewFundsContent parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (NewFundsContent) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static NewFundsContent parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public NewFundsContent getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static NewFundsContent parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private NewFundsContent() {
            this.memoizedIsInitialized = (byte) -1;
            this.payeePublicAddress_ = "";
            this.amount_ = "";
            this.coinSymbol_ = "";
            this.memo_ = "";
            this.hash_ = "";
            this.offlineUrl_ = "";
        }

        public static NewFundsContent parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static NewFundsContent parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static NewFundsContent parseFrom(InputStream inputStream) throws IOException {
            return (NewFundsContent) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static NewFundsContent parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (NewFundsContent) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static NewFundsContent parseFrom(j jVar) throws IOException {
            return (NewFundsContent) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private NewFundsContent(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.payeePublicAddress_ = jVar.I();
                            } else if (J == 18) {
                                this.amount_ = jVar.I();
                            } else if (J == 26) {
                                this.coinSymbol_ = jVar.I();
                            } else if (J == 34) {
                                this.memo_ = jVar.I();
                            } else if (J == 42) {
                                this.hash_ = jVar.I();
                            } else if (J != 50) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.offlineUrl_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static NewFundsContent parseFrom(j jVar, r rVar) throws IOException {
            return (NewFundsContent) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface NewFundsContentOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getAmount();

        ByteString getAmountBytes();

        String getCoinSymbol();

        ByteString getCoinSymbolBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        String getHash();

        ByteString getHashBytes();

        /* synthetic */ String getInitializationErrorString();

        String getMemo();

        ByteString getMemoBytes();

        String getOfflineUrl();

        ByteString getOfflineUrlBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        String getPayeePublicAddress();

        ByteString getPayeePublicAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class PublicAddress extends GeneratedMessageV3 implements PublicAddressOrBuilder {
        public static final int ADDRESS_FIELD_NUMBER = 2;
        public static final int COIN_SYMBOL_FIELD_NUMBER = 1;
        private static final PublicAddress DEFAULT_INSTANCE = new PublicAddress();
        private static final t0<PublicAddress> PARSER = new c<PublicAddress>() { // from class: wallet.core.jni.proto.FIO.PublicAddress.1
            @Override // com.google.protobuf.t0
            public PublicAddress parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new PublicAddress(jVar, rVar, null);
            }
        };
        private static final long serialVersionUID = 0;
        private volatile Object address_;
        private volatile Object coinSymbol_;
        private byte memoizedIsInitialized;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements PublicAddressOrBuilder {
            private Object address_;
            private Object coinSymbol_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_PublicAddress_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAddress() {
                this.address_ = PublicAddress.getDefaultInstance().getAddress();
                onChanged();
                return this;
            }

            public Builder clearCoinSymbol() {
                this.coinSymbol_ = PublicAddress.getDefaultInstance().getCoinSymbol();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
            public String getAddress() {
                Object obj = this.address_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.address_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
            public ByteString getAddressBytes() {
                Object obj = this.address_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.address_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
            public String getCoinSymbol() {
                Object obj = this.coinSymbol_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.coinSymbol_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
            public ByteString getCoinSymbolBytes() {
                Object obj = this.coinSymbol_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.coinSymbol_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return FIO.internal_static_TW_FIO_Proto_PublicAddress_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_PublicAddress_fieldAccessorTable.d(PublicAddress.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAddress(String str) {
                Objects.requireNonNull(str);
                this.address_ = str;
                onChanged();
                return this;
            }

            public Builder setAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.address_ = byteString;
                onChanged();
                return this;
            }

            public Builder setCoinSymbol(String str) {
                Objects.requireNonNull(str);
                this.coinSymbol_ = str;
                onChanged();
                return this;
            }

            public Builder setCoinSymbolBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.coinSymbol_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.coinSymbol_ = "";
                this.address_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public PublicAddress build() {
                PublicAddress buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public PublicAddress buildPartial() {
                PublicAddress publicAddress = new PublicAddress(this, (AnonymousClass1) null);
                publicAddress.coinSymbol_ = this.coinSymbol_;
                publicAddress.address_ = this.address_;
                onBuilt();
                return publicAddress;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public PublicAddress getDefaultInstanceForType() {
                return PublicAddress.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.coinSymbol_ = "";
                this.address_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.coinSymbol_ = "";
                this.address_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof PublicAddress) {
                    return mergeFrom((PublicAddress) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(PublicAddress publicAddress) {
                if (publicAddress == PublicAddress.getDefaultInstance()) {
                    return this;
                }
                if (!publicAddress.getCoinSymbol().isEmpty()) {
                    this.coinSymbol_ = publicAddress.coinSymbol_;
                    onChanged();
                }
                if (!publicAddress.getAddress().isEmpty()) {
                    this.address_ = publicAddress.address_;
                    onChanged();
                }
                mergeUnknownFields(publicAddress.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.FIO.PublicAddress.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.PublicAddress.access$900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.FIO$PublicAddress r3 = (wallet.core.jni.proto.FIO.PublicAddress) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.FIO$PublicAddress r4 = (wallet.core.jni.proto.FIO.PublicAddress) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.PublicAddress.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$PublicAddress$Builder");
            }
        }

        public /* synthetic */ PublicAddress(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static PublicAddress getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return FIO.internal_static_TW_FIO_Proto_PublicAddress_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static PublicAddress parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (PublicAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static PublicAddress parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<PublicAddress> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof PublicAddress)) {
                return super.equals(obj);
            }
            PublicAddress publicAddress = (PublicAddress) obj;
            return getCoinSymbol().equals(publicAddress.getCoinSymbol()) && getAddress().equals(publicAddress.getAddress()) && this.unknownFields.equals(publicAddress.unknownFields);
        }

        @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
        public String getAddress() {
            Object obj = this.address_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.address_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
        public ByteString getAddressBytes() {
            Object obj = this.address_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.address_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
        public String getCoinSymbol() {
            Object obj = this.coinSymbol_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.coinSymbol_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.PublicAddressOrBuilder
        public ByteString getCoinSymbolBytes() {
            Object obj = this.coinSymbol_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.coinSymbol_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<PublicAddress> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getCoinSymbolBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.coinSymbol_);
            if (!getAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.address_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getCoinSymbol().hashCode()) * 37) + 2) * 53) + getAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return FIO.internal_static_TW_FIO_Proto_PublicAddress_fieldAccessorTable.d(PublicAddress.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new PublicAddress();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getCoinSymbolBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.coinSymbol_);
            }
            if (!getAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.address_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ PublicAddress(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(PublicAddress publicAddress) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(publicAddress);
        }

        public static PublicAddress parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private PublicAddress(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static PublicAddress parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (PublicAddress) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static PublicAddress parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public PublicAddress getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static PublicAddress parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private PublicAddress() {
            this.memoizedIsInitialized = (byte) -1;
            this.coinSymbol_ = "";
            this.address_ = "";
        }

        public static PublicAddress parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static PublicAddress parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static PublicAddress parseFrom(InputStream inputStream) throws IOException {
            return (PublicAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private PublicAddress(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.coinSymbol_ = jVar.I();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.address_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static PublicAddress parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (PublicAddress) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static PublicAddress parseFrom(j jVar) throws IOException {
            return (PublicAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static PublicAddress parseFrom(j jVar, r rVar) throws IOException {
            return (PublicAddress) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface PublicAddressOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        String getAddress();

        ByteString getAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getCoinSymbol();

        ByteString getCoinSymbolBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int ACTION_FIELD_NUMBER = 5;
        public static final int CHAIN_PARAMS_FIELD_NUMBER = 2;
        public static final int EXPIRY_FIELD_NUMBER = 1;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 3;
        public static final int TPID_FIELD_NUMBER = 4;
        private static final long serialVersionUID = 0;
        private Action action_;
        private ChainParams chainParams_;
        private int expiry_;
        private byte memoizedIsInitialized;
        private ByteString privateKey_;
        private volatile Object tpid_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.FIO.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private a1<Action, Action.Builder, ActionOrBuilder> actionBuilder_;
            private Action action_;
            private a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> chainParamsBuilder_;
            private ChainParams chainParams_;
            private int expiry_;
            private ByteString privateKey_;
            private Object tpid_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<Action, Action.Builder, ActionOrBuilder> getActionFieldBuilder() {
                if (this.actionBuilder_ == null) {
                    this.actionBuilder_ = new a1<>(getAction(), getParentForChildren(), isClean());
                    this.action_ = null;
                }
                return this.actionBuilder_;
            }

            private a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> getChainParamsFieldBuilder() {
                if (this.chainParamsBuilder_ == null) {
                    this.chainParamsBuilder_ = new a1<>(getChainParams(), getParentForChildren(), isClean());
                    this.chainParams_ = null;
                }
                return this.chainParamsBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAction() {
                if (this.actionBuilder_ == null) {
                    this.action_ = null;
                    onChanged();
                } else {
                    this.action_ = null;
                    this.actionBuilder_ = null;
                }
                return this;
            }

            public Builder clearChainParams() {
                if (this.chainParamsBuilder_ == null) {
                    this.chainParams_ = null;
                    onChanged();
                } else {
                    this.chainParams_ = null;
                    this.chainParamsBuilder_ = null;
                }
                return this;
            }

            public Builder clearExpiry() {
                this.expiry_ = 0;
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearTpid() {
                this.tpid_ = SigningInput.getDefaultInstance().getTpid();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public Action getAction() {
                a1<Action, Action.Builder, ActionOrBuilder> a1Var = this.actionBuilder_;
                if (a1Var == null) {
                    Action action = this.action_;
                    return action == null ? Action.getDefaultInstance() : action;
                }
                return a1Var.f();
            }

            public Action.Builder getActionBuilder() {
                onChanged();
                return getActionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public ActionOrBuilder getActionOrBuilder() {
                a1<Action, Action.Builder, ActionOrBuilder> a1Var = this.actionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Action action = this.action_;
                return action == null ? Action.getDefaultInstance() : action;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public ChainParams getChainParams() {
                a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> a1Var = this.chainParamsBuilder_;
                if (a1Var == null) {
                    ChainParams chainParams = this.chainParams_;
                    return chainParams == null ? ChainParams.getDefaultInstance() : chainParams;
                }
                return a1Var.f();
            }

            public ChainParams.Builder getChainParamsBuilder() {
                onChanged();
                return getChainParamsFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public ChainParamsOrBuilder getChainParamsOrBuilder() {
                a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> a1Var = this.chainParamsBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                ChainParams chainParams = this.chainParams_;
                return chainParams == null ? ChainParams.getDefaultInstance() : chainParams;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return FIO.internal_static_TW_FIO_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public int getExpiry() {
                return this.expiry_;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public String getTpid() {
                Object obj = this.tpid_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.tpid_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public ByteString getTpidBytes() {
                Object obj = this.tpid_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.tpid_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public boolean hasAction() {
                return (this.actionBuilder_ == null && this.action_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
            public boolean hasChainParams() {
                return (this.chainParamsBuilder_ == null && this.chainParams_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeAction(Action action) {
                a1<Action, Action.Builder, ActionOrBuilder> a1Var = this.actionBuilder_;
                if (a1Var == null) {
                    Action action2 = this.action_;
                    if (action2 != null) {
                        this.action_ = Action.newBuilder(action2).mergeFrom(action).buildPartial();
                    } else {
                        this.action_ = action;
                    }
                    onChanged();
                } else {
                    a1Var.h(action);
                }
                return this;
            }

            public Builder mergeChainParams(ChainParams chainParams) {
                a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> a1Var = this.chainParamsBuilder_;
                if (a1Var == null) {
                    ChainParams chainParams2 = this.chainParams_;
                    if (chainParams2 != null) {
                        this.chainParams_ = ChainParams.newBuilder(chainParams2).mergeFrom(chainParams).buildPartial();
                    } else {
                        this.chainParams_ = chainParams;
                    }
                    onChanged();
                } else {
                    a1Var.h(chainParams);
                }
                return this;
            }

            public Builder setAction(Action action) {
                a1<Action, Action.Builder, ActionOrBuilder> a1Var = this.actionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(action);
                    this.action_ = action;
                    onChanged();
                } else {
                    a1Var.j(action);
                }
                return this;
            }

            public Builder setChainParams(ChainParams chainParams) {
                a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> a1Var = this.chainParamsBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(chainParams);
                    this.chainParams_ = chainParams;
                    onChanged();
                } else {
                    a1Var.j(chainParams);
                }
                return this;
            }

            public Builder setExpiry(int i) {
                this.expiry_ = i;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTpid(String str) {
                Objects.requireNonNull(str);
                this.tpid_ = str;
                onChanged();
                return this;
            }

            public Builder setTpidBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.tpid_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.privateKey_ = ByteString.EMPTY;
                this.tpid_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.expiry_ = this.expiry_;
                a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> a1Var = this.chainParamsBuilder_;
                if (a1Var == null) {
                    signingInput.chainParams_ = this.chainParams_;
                } else {
                    signingInput.chainParams_ = a1Var.b();
                }
                signingInput.privateKey_ = this.privateKey_;
                signingInput.tpid_ = this.tpid_;
                a1<Action, Action.Builder, ActionOrBuilder> a1Var2 = this.actionBuilder_;
                if (a1Var2 == null) {
                    signingInput.action_ = this.action_;
                } else {
                    signingInput.action_ = a1Var2.b();
                }
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.expiry_ = 0;
                if (this.chainParamsBuilder_ == null) {
                    this.chainParams_ = null;
                } else {
                    this.chainParams_ = null;
                    this.chainParamsBuilder_ = null;
                }
                this.privateKey_ = ByteString.EMPTY;
                this.tpid_ = "";
                if (this.actionBuilder_ == null) {
                    this.action_ = null;
                } else {
                    this.action_ = null;
                    this.actionBuilder_ = null;
                }
                return this;
            }

            public Builder setAction(Action.Builder builder) {
                a1<Action, Action.Builder, ActionOrBuilder> a1Var = this.actionBuilder_;
                if (a1Var == null) {
                    this.action_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setChainParams(ChainParams.Builder builder) {
                a1<ChainParams, ChainParams.Builder, ChainParamsOrBuilder> a1Var = this.chainParamsBuilder_;
                if (a1Var == null) {
                    this.chainParams_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.privateKey_ = ByteString.EMPTY;
                this.tpid_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.getExpiry() != 0) {
                    setExpiry(signingInput.getExpiry());
                }
                if (signingInput.hasChainParams()) {
                    mergeChainParams(signingInput.getChainParams());
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (!signingInput.getTpid().isEmpty()) {
                    this.tpid_ = signingInput.tpid_;
                    onChanged();
                }
                if (signingInput.hasAction()) {
                    mergeAction(signingInput.getAction());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.FIO.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.SigningInput.access$14100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.FIO$SigningInput r3 = (wallet.core.jni.proto.FIO.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.FIO$SigningInput r4 = (wallet.core.jni.proto.FIO.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return FIO.internal_static_TW_FIO_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getExpiry() == signingInput.getExpiry() && hasChainParams() == signingInput.hasChainParams()) {
                if ((!hasChainParams() || getChainParams().equals(signingInput.getChainParams())) && getPrivateKey().equals(signingInput.getPrivateKey()) && getTpid().equals(signingInput.getTpid()) && hasAction() == signingInput.hasAction()) {
                    return (!hasAction() || getAction().equals(signingInput.getAction())) && this.unknownFields.equals(signingInput.unknownFields);
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public Action getAction() {
            Action action = this.action_;
            return action == null ? Action.getDefaultInstance() : action;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public ActionOrBuilder getActionOrBuilder() {
            return getAction();
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public ChainParams getChainParams() {
            ChainParams chainParams = this.chainParams_;
            return chainParams == null ? ChainParams.getDefaultInstance() : chainParams;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public ChainParamsOrBuilder getChainParamsOrBuilder() {
            return getChainParams();
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public int getExpiry() {
            return this.expiry_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.expiry_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            if (this.chainParams_ != null) {
                Y += CodedOutputStream.G(2, getChainParams());
            }
            if (!this.privateKey_.isEmpty()) {
                Y += CodedOutputStream.h(3, this.privateKey_);
            }
            if (!getTpidBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(4, this.tpid_);
            }
            if (this.action_ != null) {
                Y += CodedOutputStream.G(5, getAction());
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public String getTpid() {
            Object obj = this.tpid_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.tpid_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public ByteString getTpidBytes() {
            Object obj = this.tpid_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.tpid_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public boolean hasAction() {
            return this.action_ != null;
        }

        @Override // wallet.core.jni.proto.FIO.SigningInputOrBuilder
        public boolean hasChainParams() {
            return this.chainParams_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getExpiry();
            if (hasChainParams()) {
                hashCode = (((hashCode * 37) + 2) * 53) + getChainParams().hashCode();
            }
            int hashCode2 = (((((((hashCode * 37) + 3) * 53) + getPrivateKey().hashCode()) * 37) + 4) * 53) + getTpid().hashCode();
            if (hasAction()) {
                hashCode2 = (((hashCode2 * 37) + 5) * 53) + getAction().hashCode();
            }
            int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode3;
            return hashCode3;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return FIO.internal_static_TW_FIO_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.expiry_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            if (this.chainParams_ != null) {
                codedOutputStream.K0(2, getChainParams());
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(3, this.privateKey_);
            }
            if (!getTpidBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.tpid_);
            }
            if (this.action_ != null) {
                codedOutputStream.K0(5, getAction());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.privateKey_ = ByteString.EMPTY;
            this.tpid_ = "";
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 8) {
                                if (J == 18) {
                                    ChainParams chainParams = this.chainParams_;
                                    ChainParams.Builder builder = chainParams != null ? chainParams.toBuilder() : null;
                                    ChainParams chainParams2 = (ChainParams) jVar.z(ChainParams.parser(), rVar);
                                    this.chainParams_ = chainParams2;
                                    if (builder != null) {
                                        builder.mergeFrom(chainParams2);
                                        this.chainParams_ = builder.buildPartial();
                                    }
                                } else if (J == 26) {
                                    this.privateKey_ = jVar.q();
                                } else if (J == 34) {
                                    this.tpid_ = jVar.I();
                                } else if (J != 42) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    Action action = this.action_;
                                    Action.Builder builder2 = action != null ? action.toBuilder() : null;
                                    Action action2 = (Action) jVar.z(Action.parser(), rVar);
                                    this.action_ = action2;
                                    if (builder2 != null) {
                                        builder2.mergeFrom(action2);
                                        this.action_ = builder2.buildPartial();
                                    }
                                }
                            } else {
                                this.expiry_ = jVar.K();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        Action getAction();

        ActionOrBuilder getActionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ChainParams getChainParams();

        ChainParamsOrBuilder getChainParamsOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        int getExpiry();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getTpid();

        ByteString getTpidBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasAction();

        boolean hasChainParams();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ERROR_FIELD_NUMBER = 2;
        public static final int JSON_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private int error_;
        private volatile Object json_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.FIO.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private int error_;
            private Object json_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return FIO.internal_static_TW_FIO_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearError() {
                this.error_ = 0;
                onChanged();
                return this;
            }

            public Builder clearJson() {
                this.json_ = SigningOutput.getDefaultInstance().getJson();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return FIO.internal_static_TW_FIO_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
            public Common.SigningError getError() {
                Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
                return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
            public int getErrorValue() {
                return this.error_;
            }

            @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
            public String getJson() {
                Object obj = this.json_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.json_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
            public ByteString getJsonBytes() {
                Object obj = this.json_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.json_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return FIO.internal_static_TW_FIO_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setError(Common.SigningError signingError) {
                Objects.requireNonNull(signingError);
                this.error_ = signingError.getNumber();
                onChanged();
                return this;
            }

            public Builder setErrorValue(int i) {
                this.error_ = i;
                onChanged();
                return this;
            }

            public Builder setJson(String str) {
                Objects.requireNonNull(str);
                this.json_ = str;
                onChanged();
                return this;
            }

            public Builder setJsonBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.json_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.json_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.json_ = this.json_;
                signingOutput.error_ = this.error_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.json_ = "";
                this.error_ = 0;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.json_ = "";
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (!signingOutput.getJson().isEmpty()) {
                    this.json_ = signingOutput.json_;
                    onChanged();
                }
                if (signingOutput.error_ != 0) {
                    setErrorValue(signingOutput.getErrorValue());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.FIO.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.FIO.SigningOutput.access$15300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.FIO$SigningOutput r3 = (wallet.core.jni.proto.FIO.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.FIO$SigningOutput r4 = (wallet.core.jni.proto.FIO.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.FIO.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.FIO$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return FIO.internal_static_TW_FIO_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getJson().equals(signingOutput.getJson()) && this.error_ == signingOutput.error_ && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
        public Common.SigningError getError() {
            Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
            return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
        public int getErrorValue() {
            return this.error_;
        }

        @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
        public String getJson() {
            Object obj = this.json_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.json_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.FIO.SigningOutputOrBuilder
        public ByteString getJsonBytes() {
            Object obj = this.json_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.json_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getJsonBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.json_);
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                computeStringSize += CodedOutputStream.l(2, this.error_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getJson().hashCode()) * 37) + 2) * 53) + this.error_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return FIO.internal_static_TW_FIO_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getJsonBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.json_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                codedOutputStream.u0(2, this.error_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.json_ = "";
            this.error_ = 0;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.json_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.error_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        Common.SigningError getError();

        int getErrorValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getJson();

        ByteString getJsonBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_FIO_Proto_PublicAddress_descriptor = bVar;
        internal_static_TW_FIO_Proto_PublicAddress_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"CoinSymbol", "Address"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_FIO_Proto_NewFundsContent_descriptor = bVar2;
        internal_static_TW_FIO_Proto_NewFundsContent_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"PayeePublicAddress", "Amount", "CoinSymbol", "Memo", "Hash", "OfflineUrl"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_FIO_Proto_Action_descriptor = bVar3;
        internal_static_TW_FIO_Proto_Action_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"RegisterFioAddressMessage", "AddPubAddressMessage", "TransferMessage", "RenewFioAddressMessage", "NewFundsRequestMessage", "MessageOneof"});
        Descriptors.b bVar4 = bVar3.r().get(0);
        internal_static_TW_FIO_Proto_Action_RegisterFioAddress_descriptor = bVar4;
        internal_static_TW_FIO_Proto_Action_RegisterFioAddress_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"FioAddress", "OwnerFioPublicKey", "Fee"});
        Descriptors.b bVar5 = bVar3.r().get(1);
        internal_static_TW_FIO_Proto_Action_AddPubAddress_descriptor = bVar5;
        internal_static_TW_FIO_Proto_Action_AddPubAddress_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"FioAddress", "PublicAddresses", "Fee"});
        Descriptors.b bVar6 = bVar3.r().get(2);
        internal_static_TW_FIO_Proto_Action_Transfer_descriptor = bVar6;
        internal_static_TW_FIO_Proto_Action_Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"PayeePublicKey", "Amount", "Fee"});
        Descriptors.b bVar7 = bVar3.r().get(3);
        internal_static_TW_FIO_Proto_Action_RenewFioAddress_descriptor = bVar7;
        internal_static_TW_FIO_Proto_Action_RenewFioAddress_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"FioAddress", "OwnerFioPublicKey", "Fee"});
        Descriptors.b bVar8 = bVar3.r().get(4);
        internal_static_TW_FIO_Proto_Action_NewFundsRequest_descriptor = bVar8;
        internal_static_TW_FIO_Proto_Action_NewFundsRequest_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"PayerFioName", "PayerFioAddress", "PayeeFioName", "Content", "Fee"});
        Descriptors.b bVar9 = getDescriptor().o().get(3);
        internal_static_TW_FIO_Proto_ChainParams_descriptor = bVar9;
        internal_static_TW_FIO_Proto_ChainParams_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"ChainId", "HeadBlockNumber", "RefBlockPrefix"});
        Descriptors.b bVar10 = getDescriptor().o().get(4);
        internal_static_TW_FIO_Proto_SigningInput_descriptor = bVar10;
        internal_static_TW_FIO_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar10, new String[]{"Expiry", "ChainParams", "PrivateKey", "Tpid", "Action"});
        Descriptors.b bVar11 = getDescriptor().o().get(5);
        internal_static_TW_FIO_Proto_SigningOutput_descriptor = bVar11;
        internal_static_TW_FIO_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar11, new String[]{"Json", "Error"});
        Common.getDescriptor();
    }

    private FIO() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
