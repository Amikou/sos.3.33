package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Ontology {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000eOntology.proto\u0012\u0011TW.Ontology.Proto\"Ö\u0001\n\fSigningInput\u0012\u0010\n\bcontract\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006method\u0018\u0002 \u0001(\t\u0012\u0019\n\u0011owner_private_key\u0018\u0003 \u0001(\f\u0012\u0012\n\nto_address\u0018\u0004 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0005 \u0001(\u0004\u0012\u0019\n\u0011payer_private_key\u0018\u0006 \u0001(\f\u0012\u0011\n\tgas_price\u0018\u0007 \u0001(\u0004\u0012\u0011\n\tgas_limit\u0018\b \u0001(\u0004\u0012\u0015\n\rquery_address\u0018\t \u0001(\t\u0012\r\n\u0005nonce\u0018\n \u0001(\r\" \n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Ontology_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ontology_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ontology_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ontology_Proto_SigningOutput_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 5;
        public static final int CONTRACT_FIELD_NUMBER = 1;
        public static final int GAS_LIMIT_FIELD_NUMBER = 8;
        public static final int GAS_PRICE_FIELD_NUMBER = 7;
        public static final int METHOD_FIELD_NUMBER = 2;
        public static final int NONCE_FIELD_NUMBER = 10;
        public static final int OWNER_PRIVATE_KEY_FIELD_NUMBER = 3;
        public static final int PAYER_PRIVATE_KEY_FIELD_NUMBER = 6;
        public static final int QUERY_ADDRESS_FIELD_NUMBER = 9;
        public static final int TO_ADDRESS_FIELD_NUMBER = 4;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object contract_;
        private long gasLimit_;
        private long gasPrice_;
        private byte memoizedIsInitialized;
        private volatile Object method_;
        private int nonce_;
        private ByteString ownerPrivateKey_;
        private ByteString payerPrivateKey_;
        private volatile Object queryAddress_;
        private volatile Object toAddress_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Ontology.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Ontology.internal_static_TW_Ontology_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            return getContract().equals(signingInput.getContract()) && getMethod().equals(signingInput.getMethod()) && getOwnerPrivateKey().equals(signingInput.getOwnerPrivateKey()) && getToAddress().equals(signingInput.getToAddress()) && getAmount() == signingInput.getAmount() && getPayerPrivateKey().equals(signingInput.getPayerPrivateKey()) && getGasPrice() == signingInput.getGasPrice() && getGasLimit() == signingInput.getGasLimit() && getQueryAddress().equals(signingInput.getQueryAddress()) && getNonce() == signingInput.getNonce() && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public String getContract() {
            Object obj = this.contract_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.contract_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public ByteString getContractBytes() {
            Object obj = this.contract_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.contract_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public long getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public long getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public String getMethod() {
            Object obj = this.method_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.method_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public ByteString getMethodBytes() {
            Object obj = this.method_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.method_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public int getNonce() {
            return this.nonce_;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public ByteString getOwnerPrivateKey() {
            return this.ownerPrivateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public ByteString getPayerPrivateKey() {
            return this.payerPrivateKey_;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public String getQueryAddress() {
            Object obj = this.queryAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.queryAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public ByteString getQueryAddressBytes() {
            Object obj = this.queryAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.queryAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getContractBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.contract_);
            if (!getMethodBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.method_);
            }
            if (!this.ownerPrivateKey_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.ownerPrivateKey_);
            }
            if (!getToAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(4, this.toAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(5, j);
            }
            if (!this.payerPrivateKey_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(6, this.payerPrivateKey_);
            }
            long j2 = this.gasPrice_;
            if (j2 != 0) {
                computeStringSize += CodedOutputStream.a0(7, j2);
            }
            long j3 = this.gasLimit_;
            if (j3 != 0) {
                computeStringSize += CodedOutputStream.a0(8, j3);
            }
            if (!getQueryAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(9, this.queryAddress_);
            }
            int i2 = this.nonce_;
            if (i2 != 0) {
                computeStringSize += CodedOutputStream.Y(10, i2);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getContract().hashCode()) * 37) + 2) * 53) + getMethod().hashCode()) * 37) + 3) * 53) + getOwnerPrivateKey().hashCode()) * 37) + 4) * 53) + getToAddress().hashCode()) * 37) + 5) * 53) + a0.h(getAmount())) * 37) + 6) * 53) + getPayerPrivateKey().hashCode()) * 37) + 7) * 53) + a0.h(getGasPrice())) * 37) + 8) * 53) + a0.h(getGasLimit())) * 37) + 9) * 53) + getQueryAddress().hashCode()) * 37) + 10) * 53) + getNonce()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Ontology.internal_static_TW_Ontology_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getContractBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.contract_);
            }
            if (!getMethodBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.method_);
            }
            if (!this.ownerPrivateKey_.isEmpty()) {
                codedOutputStream.q0(3, this.ownerPrivateKey_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.toAddress_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.d1(5, j);
            }
            if (!this.payerPrivateKey_.isEmpty()) {
                codedOutputStream.q0(6, this.payerPrivateKey_);
            }
            long j2 = this.gasPrice_;
            if (j2 != 0) {
                codedOutputStream.d1(7, j2);
            }
            long j3 = this.gasLimit_;
            if (j3 != 0) {
                codedOutputStream.d1(8, j3);
            }
            if (!getQueryAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 9, this.queryAddress_);
            }
            int i = this.nonce_;
            if (i != 0) {
                codedOutputStream.b1(10, i);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private long amount_;
            private Object contract_;
            private long gasLimit_;
            private long gasPrice_;
            private Object method_;
            private int nonce_;
            private ByteString ownerPrivateKey_;
            private ByteString payerPrivateKey_;
            private Object queryAddress_;
            private Object toAddress_;

            public static final Descriptors.b getDescriptor() {
                return Ontology.internal_static_TW_Ontology_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearContract() {
                this.contract_ = SigningInput.getDefaultInstance().getContract();
                onChanged();
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearMethod() {
                this.method_ = SigningInput.getDefaultInstance().getMethod();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = 0;
                onChanged();
                return this;
            }

            public Builder clearOwnerPrivateKey() {
                this.ownerPrivateKey_ = SigningInput.getDefaultInstance().getOwnerPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearPayerPrivateKey() {
                this.payerPrivateKey_ = SigningInput.getDefaultInstance().getPayerPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearQueryAddress() {
                this.queryAddress_ = SigningInput.getDefaultInstance().getQueryAddress();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = SigningInput.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public String getContract() {
                Object obj = this.contract_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.contract_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public ByteString getContractBytes() {
                Object obj = this.contract_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.contract_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Ontology.internal_static_TW_Ontology_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public long getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public long getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public String getMethod() {
                Object obj = this.method_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.method_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public ByteString getMethodBytes() {
                Object obj = this.method_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.method_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public int getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public ByteString getOwnerPrivateKey() {
                return this.ownerPrivateKey_;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public ByteString getPayerPrivateKey() {
                return this.payerPrivateKey_;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public String getQueryAddress() {
                Object obj = this.queryAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.queryAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public ByteString getQueryAddressBytes() {
                Object obj = this.queryAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.queryAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningInputOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ontology.internal_static_TW_Ontology_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setContract(String str) {
                Objects.requireNonNull(str);
                this.contract_ = str;
                onChanged();
                return this;
            }

            public Builder setContractBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.contract_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasLimit(long j) {
                this.gasLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setGasPrice(long j) {
                this.gasPrice_ = j;
                onChanged();
                return this;
            }

            public Builder setMethod(String str) {
                Objects.requireNonNull(str);
                this.method_ = str;
                onChanged();
                return this;
            }

            public Builder setMethodBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.method_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(int i) {
                this.nonce_ = i;
                onChanged();
                return this;
            }

            public Builder setOwnerPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.ownerPrivateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPayerPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.payerPrivateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setQueryAddress(String str) {
                Objects.requireNonNull(str);
                this.queryAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setQueryAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.queryAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.contract_ = "";
                this.method_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.ownerPrivateKey_ = byteString;
                this.toAddress_ = "";
                this.payerPrivateKey_ = byteString;
                this.queryAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                signingInput.contract_ = this.contract_;
                signingInput.method_ = this.method_;
                signingInput.ownerPrivateKey_ = this.ownerPrivateKey_;
                signingInput.toAddress_ = this.toAddress_;
                signingInput.amount_ = this.amount_;
                signingInput.payerPrivateKey_ = this.payerPrivateKey_;
                signingInput.gasPrice_ = this.gasPrice_;
                signingInput.gasLimit_ = this.gasLimit_;
                signingInput.queryAddress_ = this.queryAddress_;
                signingInput.nonce_ = this.nonce_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.contract_ = "";
                this.method_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.ownerPrivateKey_ = byteString;
                this.toAddress_ = "";
                this.amount_ = 0L;
                this.payerPrivateKey_ = byteString;
                this.gasPrice_ = 0L;
                this.gasLimit_ = 0L;
                this.queryAddress_ = "";
                this.nonce_ = 0;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (!signingInput.getContract().isEmpty()) {
                    this.contract_ = signingInput.contract_;
                    onChanged();
                }
                if (!signingInput.getMethod().isEmpty()) {
                    this.method_ = signingInput.method_;
                    onChanged();
                }
                ByteString ownerPrivateKey = signingInput.getOwnerPrivateKey();
                ByteString byteString = ByteString.EMPTY;
                if (ownerPrivateKey != byteString) {
                    setOwnerPrivateKey(signingInput.getOwnerPrivateKey());
                }
                if (!signingInput.getToAddress().isEmpty()) {
                    this.toAddress_ = signingInput.toAddress_;
                    onChanged();
                }
                if (signingInput.getAmount() != 0) {
                    setAmount(signingInput.getAmount());
                }
                if (signingInput.getPayerPrivateKey() != byteString) {
                    setPayerPrivateKey(signingInput.getPayerPrivateKey());
                }
                if (signingInput.getGasPrice() != 0) {
                    setGasPrice(signingInput.getGasPrice());
                }
                if (signingInput.getGasLimit() != 0) {
                    setGasLimit(signingInput.getGasLimit());
                }
                if (!signingInput.getQueryAddress().isEmpty()) {
                    this.queryAddress_ = signingInput.queryAddress_;
                    onChanged();
                }
                if (signingInput.getNonce() != 0) {
                    setNonce(signingInput.getNonce());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.contract_ = "";
                this.method_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.ownerPrivateKey_ = byteString;
                this.toAddress_ = "";
                this.payerPrivateKey_ = byteString;
                this.queryAddress_ = "";
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Ontology.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ontology.SigningInput.access$1700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Ontology$SigningInput r3 = (wallet.core.jni.proto.Ontology.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Ontology$SigningInput r4 = (wallet.core.jni.proto.Ontology.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ontology.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ontology$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.contract_ = "";
            this.method_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.ownerPrivateKey_ = byteString;
            this.toAddress_ = "";
            this.payerPrivateKey_ = byteString;
            this.queryAddress_ = "";
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 10:
                                this.contract_ = jVar.I();
                                continue;
                            case 18:
                                this.method_ = jVar.I();
                                continue;
                            case 26:
                                this.ownerPrivateKey_ = jVar.q();
                                continue;
                            case 34:
                                this.toAddress_ = jVar.I();
                                continue;
                            case 40:
                                this.amount_ = jVar.L();
                                continue;
                            case 50:
                                this.payerPrivateKey_ = jVar.q();
                                continue;
                            case 56:
                                this.gasPrice_ = jVar.L();
                                continue;
                            case 64:
                                this.gasLimit_ = jVar.L();
                                continue;
                            case 74:
                                this.queryAddress_ = jVar.I();
                                continue;
                            case 80:
                                this.nonce_ = jVar.K();
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        String getContract();

        ByteString getContractBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGasLimit();

        long getGasPrice();

        /* synthetic */ String getInitializationErrorString();

        String getMethod();

        ByteString getMethodBytes();

        int getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getOwnerPrivateKey();

        ByteString getPayerPrivateKey();

        String getQueryAddress();

        ByteString getQueryAddressBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Ontology.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Ontology.internal_static_TW_Ontology_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Ontology.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Ontology.internal_static_TW_Ontology_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;

            public static final Descriptors.b getDescriptor() {
                return Ontology.internal_static_TW_Ontology_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Ontology.internal_static_TW_Ontology_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Ontology.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ontology.internal_static_TW_Ontology_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                signingOutput.encoded_ = this.encoded_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Ontology.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ontology.SigningOutput.access$3100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Ontology$SigningOutput r3 = (wallet.core.jni.proto.Ontology.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Ontology$SigningOutput r4 = (wallet.core.jni.proto.Ontology.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ontology.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ontology$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.encoded_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Ontology_Proto_SigningInput_descriptor = bVar;
        internal_static_TW_Ontology_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Contract", "Method", "OwnerPrivateKey", "ToAddress", "Amount", "PayerPrivateKey", "GasPrice", "GasLimit", "QueryAddress", "Nonce"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Ontology_Proto_SigningOutput_descriptor = bVar2;
        internal_static_TW_Ontology_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Encoded"});
    }

    private Ontology() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
