package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class NULS {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\nNULS.proto\u0012\rTW.NULS.Proto\"\u0088\u0001\n\u0013TransactionCoinFrom\u0012\u0014\n\ffrom_address\u0018\u0001 \u0001(\t\u0012\u0016\n\u000eassets_chainid\u0018\u0002 \u0001(\r\u0012\u0011\n\tassets_id\u0018\u0003 \u0001(\r\u0012\u0011\n\tid_amount\u0018\u0004 \u0001(\f\u0012\r\n\u0005nonce\u0018\u0005 \u0001(\f\u0012\u000e\n\u0006locked\u0018\u0006 \u0001(\r\"x\n\u0011TransactionCoinTo\u0012\u0012\n\nto_address\u0018\u0001 \u0001(\t\u0012\u0016\n\u000eassets_chainid\u0018\u0002 \u0001(\r\u0012\u0011\n\tassets_id\u0018\u0003 \u0001(\r\u0012\u0011\n\tid_amount\u0018\u0004 \u0001(\f\u0012\u0011\n\tlock_time\u0018\u0005 \u0001(\r\"U\n\tSignature\u0012\u0010\n\bpkey_len\u0018\u0001 \u0001(\r\u0012\u0012\n\npublic_key\u0018\u0002 \u0001(\f\u0012\u000f\n\u0007sig_len\u0018\u0003 \u0001(\r\u0012\u0011\n\tsignature\u0018\u0004 \u0001(\f\"í\u0001\n\u000bTransaction\u0012\f\n\u0004type\u0018\u0001 \u0001(\r\u0012\u0011\n\ttimestamp\u0018\u0002 \u0001(\r\u0012\u000e\n\u0006remark\u0018\u0003 \u0001(\t\u0012\u000f\n\u0007tx_data\u0018\u0004 \u0001(\f\u00121\n\u0005input\u0018\u0005 \u0001(\u000b2\".TW.NULS.Proto.TransactionCoinFrom\u00120\n\u0006output\u0018\u0006 \u0001(\u000b2 .TW.NULS.Proto.TransactionCoinTo\u0012)\n\u0007tx_sigs\u0018\u0007 \u0001(\u000b2\u0018.TW.NULS.Proto.Signature\u0012\f\n\u0004hash\u0018\b \u0001(\r\"·\u0001\n\fSigningInput\u0012\u0013\n\u000bprivate_key\u0018\u0001 \u0001(\f\u0012\f\n\u0004from\u0018\u0002 \u0001(\t\u0012\n\n\u0002to\u0018\u0003 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0004 \u0001(\f\u0012\u0010\n\bchain_id\u0018\u0005 \u0001(\r\u0012\u0013\n\u000bidassets_id\u0018\u0006 \u0001(\r\u0012\r\n\u0005nonce\u0018\u0007 \u0001(\f\u0012\u000e\n\u0006remark\u0018\b \u0001(\t\u0012\u000f\n\u0007balance\u0018\t \u0001(\f\u0012\u0011\n\ttimestamp\u0018\n \u0001(\r\" \n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_NULS_Proto_Signature_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NULS_Proto_Signature_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NULS_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NULS_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NULS_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NULS_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NULS_Proto_TransactionCoinFrom_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NULS_Proto_TransactionCoinFrom_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NULS_Proto_TransactionCoinTo_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NULS_Proto_TransactionCoinTo_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NULS_Proto_Transaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NULS_Proto_Transaction_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class Signature extends GeneratedMessageV3 implements SignatureOrBuilder {
        private static final Signature DEFAULT_INSTANCE = new Signature();
        private static final t0<Signature> PARSER = new c<Signature>() { // from class: wallet.core.jni.proto.NULS.Signature.1
            @Override // com.google.protobuf.t0
            public Signature parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Signature(jVar, rVar);
            }
        };
        public static final int PKEY_LEN_FIELD_NUMBER = 1;
        public static final int PUBLIC_KEY_FIELD_NUMBER = 2;
        public static final int SIGNATURE_FIELD_NUMBER = 4;
        public static final int SIG_LEN_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int pkeyLen_;
        private ByteString publicKey_;
        private int sigLen_;
        private ByteString signature_;

        public static Signature getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NULS.internal_static_TW_NULS_Proto_Signature_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Signature parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Signature) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Signature parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Signature> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Signature)) {
                return super.equals(obj);
            }
            Signature signature = (Signature) obj;
            return getPkeyLen() == signature.getPkeyLen() && getPublicKey().equals(signature.getPublicKey()) && getSigLen() == signature.getSigLen() && getSignature().equals(signature.getSignature()) && this.unknownFields.equals(signature.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Signature> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
        public int getPkeyLen() {
            return this.pkeyLen_;
        }

        @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
        public ByteString getPublicKey() {
            return this.publicKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.pkeyLen_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            if (!this.publicKey_.isEmpty()) {
                Y += CodedOutputStream.h(2, this.publicKey_);
            }
            int i3 = this.sigLen_;
            if (i3 != 0) {
                Y += CodedOutputStream.Y(3, i3);
            }
            if (!this.signature_.isEmpty()) {
                Y += CodedOutputStream.h(4, this.signature_);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
        public int getSigLen() {
            return this.sigLen_;
        }

        @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPkeyLen()) * 37) + 2) * 53) + getPublicKey().hashCode()) * 37) + 3) * 53) + getSigLen()) * 37) + 4) * 53) + getSignature().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NULS.internal_static_TW_NULS_Proto_Signature_fieldAccessorTable.d(Signature.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Signature();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.pkeyLen_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            if (!this.publicKey_.isEmpty()) {
                codedOutputStream.q0(2, this.publicKey_);
            }
            int i2 = this.sigLen_;
            if (i2 != 0) {
                codedOutputStream.b1(3, i2);
            }
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(4, this.signature_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SignatureOrBuilder {
            private int pkeyLen_;
            private ByteString publicKey_;
            private int sigLen_;
            private ByteString signature_;

            public static final Descriptors.b getDescriptor() {
                return NULS.internal_static_TW_NULS_Proto_Signature_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPkeyLen() {
                this.pkeyLen_ = 0;
                onChanged();
                return this;
            }

            public Builder clearPublicKey() {
                this.publicKey_ = Signature.getDefaultInstance().getPublicKey();
                onChanged();
                return this;
            }

            public Builder clearSigLen() {
                this.sigLen_ = 0;
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = Signature.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NULS.internal_static_TW_NULS_Proto_Signature_descriptor;
            }

            @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
            public int getPkeyLen() {
                return this.pkeyLen_;
            }

            @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
            public ByteString getPublicKey() {
                return this.publicKey_;
            }

            @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
            public int getSigLen() {
                return this.sigLen_;
            }

            @Override // wallet.core.jni.proto.NULS.SignatureOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NULS.internal_static_TW_NULS_Proto_Signature_fieldAccessorTable.d(Signature.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setPkeyLen(int i) {
                this.pkeyLen_ = i;
                onChanged();
                return this;
            }

            public Builder setPublicKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.publicKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSigLen(int i) {
                this.sigLen_ = i;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.publicKey_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Signature build() {
                Signature buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Signature buildPartial() {
                Signature signature = new Signature(this);
                signature.pkeyLen_ = this.pkeyLen_;
                signature.publicKey_ = this.publicKey_;
                signature.sigLen_ = this.sigLen_;
                signature.signature_ = this.signature_;
                onBuilt();
                return signature;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Signature getDefaultInstanceForType() {
                return Signature.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.pkeyLen_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.publicKey_ = byteString;
                this.sigLen_ = 0;
                this.signature_ = byteString;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.publicKey_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Signature) {
                    return mergeFrom((Signature) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Signature signature) {
                if (signature == Signature.getDefaultInstance()) {
                    return this;
                }
                if (signature.getPkeyLen() != 0) {
                    setPkeyLen(signature.getPkeyLen());
                }
                ByteString publicKey = signature.getPublicKey();
                ByteString byteString = ByteString.EMPTY;
                if (publicKey != byteString) {
                    setPublicKey(signature.getPublicKey());
                }
                if (signature.getSigLen() != 0) {
                    setSigLen(signature.getSigLen());
                }
                if (signature.getSignature() != byteString) {
                    setSignature(signature.getSignature());
                }
                mergeUnknownFields(signature.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NULS.Signature.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NULS.Signature.access$4200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NULS$Signature r3 = (wallet.core.jni.proto.NULS.Signature) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NULS$Signature r4 = (wallet.core.jni.proto.NULS.Signature) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NULS.Signature.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NULS$Signature$Builder");
            }
        }

        public static Builder newBuilder(Signature signature) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signature);
        }

        public static Signature parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Signature(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Signature parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Signature) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Signature parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Signature getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static Signature parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Signature() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.publicKey_ = byteString;
            this.signature_ = byteString;
        }

        public static Signature parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static Signature parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Signature parseFrom(InputStream inputStream) throws IOException {
            return (Signature) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Signature(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.pkeyLen_ = jVar.K();
                            } else if (J == 18) {
                                this.publicKey_ = jVar.q();
                            } else if (J == 24) {
                                this.sigLen_ = jVar.K();
                            } else if (J != 34) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.signature_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Signature parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Signature) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Signature parseFrom(j jVar) throws IOException {
            return (Signature) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Signature parseFrom(j jVar, r rVar) throws IOException {
            return (Signature) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SignatureOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        int getPkeyLen();

        ByteString getPublicKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        int getSigLen();

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 4;
        public static final int BALANCE_FIELD_NUMBER = 9;
        public static final int CHAIN_ID_FIELD_NUMBER = 5;
        public static final int FROM_FIELD_NUMBER = 2;
        public static final int IDASSETS_ID_FIELD_NUMBER = 6;
        public static final int NONCE_FIELD_NUMBER = 7;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 1;
        public static final int REMARK_FIELD_NUMBER = 8;
        public static final int TIMESTAMP_FIELD_NUMBER = 10;
        public static final int TO_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private ByteString amount_;
        private ByteString balance_;
        private int chainId_;
        private volatile Object from_;
        private int idassetsId_;
        private byte memoizedIsInitialized;
        private ByteString nonce_;
        private ByteString privateKey_;
        private volatile Object remark_;
        private int timestamp_;
        private volatile Object to_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.NULS.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NULS.internal_static_TW_NULS_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            return getPrivateKey().equals(signingInput.getPrivateKey()) && getFrom().equals(signingInput.getFrom()) && getTo().equals(signingInput.getTo()) && getAmount().equals(signingInput.getAmount()) && getChainId() == signingInput.getChainId() && getIdassetsId() == signingInput.getIdassetsId() && getNonce().equals(signingInput.getNonce()) && getRemark().equals(signingInput.getRemark()) && getBalance().equals(signingInput.getBalance()) && getTimestamp() == signingInput.getTimestamp() && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public ByteString getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public ByteString getBalance() {
            return this.balance_;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public int getChainId() {
            return this.chainId_;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public String getFrom() {
            Object obj = this.from_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.from_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public ByteString getFromBytes() {
            Object obj = this.from_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.from_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public int getIdassetsId() {
            return this.idassetsId_;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public ByteString getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public String getRemark() {
            Object obj = this.remark_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.remark_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public ByteString getRemarkBytes() {
            Object obj = this.remark_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.remark_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.privateKey_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.privateKey_);
            if (!getFromBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(2, this.from_);
            }
            if (!getToBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(3, this.to_);
            }
            if (!this.amount_.isEmpty()) {
                h += CodedOutputStream.h(4, this.amount_);
            }
            int i2 = this.chainId_;
            if (i2 != 0) {
                h += CodedOutputStream.Y(5, i2);
            }
            int i3 = this.idassetsId_;
            if (i3 != 0) {
                h += CodedOutputStream.Y(6, i3);
            }
            if (!this.nonce_.isEmpty()) {
                h += CodedOutputStream.h(7, this.nonce_);
            }
            if (!getRemarkBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(8, this.remark_);
            }
            if (!this.balance_.isEmpty()) {
                h += CodedOutputStream.h(9, this.balance_);
            }
            int i4 = this.timestamp_;
            if (i4 != 0) {
                h += CodedOutputStream.Y(10, i4);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public int getTimestamp() {
            return this.timestamp_;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public String getTo() {
            Object obj = this.to_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.to_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
        public ByteString getToBytes() {
            Object obj = this.to_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.to_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPrivateKey().hashCode()) * 37) + 2) * 53) + getFrom().hashCode()) * 37) + 3) * 53) + getTo().hashCode()) * 37) + 4) * 53) + getAmount().hashCode()) * 37) + 5) * 53) + getChainId()) * 37) + 6) * 53) + getIdassetsId()) * 37) + 7) * 53) + getNonce().hashCode()) * 37) + 8) * 53) + getRemark().hashCode()) * 37) + 9) * 53) + getBalance().hashCode()) * 37) + 10) * 53) + getTimestamp()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NULS.internal_static_TW_NULS_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(1, this.privateKey_);
            }
            if (!getFromBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.from_);
            }
            if (!getToBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.to_);
            }
            if (!this.amount_.isEmpty()) {
                codedOutputStream.q0(4, this.amount_);
            }
            int i = this.chainId_;
            if (i != 0) {
                codedOutputStream.b1(5, i);
            }
            int i2 = this.idassetsId_;
            if (i2 != 0) {
                codedOutputStream.b1(6, i2);
            }
            if (!this.nonce_.isEmpty()) {
                codedOutputStream.q0(7, this.nonce_);
            }
            if (!getRemarkBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 8, this.remark_);
            }
            if (!this.balance_.isEmpty()) {
                codedOutputStream.q0(9, this.balance_);
            }
            int i3 = this.timestamp_;
            if (i3 != 0) {
                codedOutputStream.b1(10, i3);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private ByteString amount_;
            private ByteString balance_;
            private int chainId_;
            private Object from_;
            private int idassetsId_;
            private ByteString nonce_;
            private ByteString privateKey_;
            private Object remark_;
            private int timestamp_;
            private Object to_;

            public static final Descriptors.b getDescriptor() {
                return NULS.internal_static_TW_NULS_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = SigningInput.getDefaultInstance().getAmount();
                onChanged();
                return this;
            }

            public Builder clearBalance() {
                this.balance_ = SigningInput.getDefaultInstance().getBalance();
                onChanged();
                return this;
            }

            public Builder clearChainId() {
                this.chainId_ = 0;
                onChanged();
                return this;
            }

            public Builder clearFrom() {
                this.from_ = SigningInput.getDefaultInstance().getFrom();
                onChanged();
                return this;
            }

            public Builder clearIdassetsId() {
                this.idassetsId_ = 0;
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = SigningInput.getDefaultInstance().getNonce();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearRemark() {
                this.remark_ = SigningInput.getDefaultInstance().getRemark();
                onChanged();
                return this;
            }

            public Builder clearTimestamp() {
                this.timestamp_ = 0;
                onChanged();
                return this;
            }

            public Builder clearTo() {
                this.to_ = SigningInput.getDefaultInstance().getTo();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public ByteString getBalance() {
                return this.balance_;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public int getChainId() {
                return this.chainId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NULS.internal_static_TW_NULS_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public String getFrom() {
                Object obj = this.from_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.from_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public ByteString getFromBytes() {
                Object obj = this.from_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.from_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public int getIdassetsId() {
                return this.idassetsId_;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public ByteString getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public String getRemark() {
                Object obj = this.remark_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.remark_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public ByteString getRemarkBytes() {
                Object obj = this.remark_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.remark_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public int getTimestamp() {
                return this.timestamp_;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.to_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NULS.SigningInputOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NULS.internal_static_TW_NULS_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.amount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setBalance(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.balance_ = byteString;
                onChanged();
                return this;
            }

            public Builder setChainId(int i) {
                this.chainId_ = i;
                onChanged();
                return this;
            }

            public Builder setFrom(String str) {
                Objects.requireNonNull(str);
                this.from_ = str;
                onChanged();
                return this;
            }

            public Builder setFromBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.from_ = byteString;
                onChanged();
                return this;
            }

            public Builder setIdassetsId(int i) {
                this.idassetsId_ = i;
                onChanged();
                return this;
            }

            public Builder setNonce(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.nonce_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setRemark(String str) {
                Objects.requireNonNull(str);
                this.remark_ = str;
                onChanged();
                return this;
            }

            public Builder setRemarkBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.remark_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTimestamp(int i) {
                this.timestamp_ = i;
                onChanged();
                return this;
            }

            public Builder setTo(String str) {
                Objects.requireNonNull(str);
                this.to_ = str;
                onChanged();
                return this;
            }

            public Builder setToBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.to_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.privateKey_ = byteString;
                this.from_ = "";
                this.to_ = "";
                this.amount_ = byteString;
                this.nonce_ = byteString;
                this.remark_ = "";
                this.balance_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                signingInput.privateKey_ = this.privateKey_;
                signingInput.from_ = this.from_;
                signingInput.to_ = this.to_;
                signingInput.amount_ = this.amount_;
                signingInput.chainId_ = this.chainId_;
                signingInput.idassetsId_ = this.idassetsId_;
                signingInput.nonce_ = this.nonce_;
                signingInput.remark_ = this.remark_;
                signingInput.balance_ = this.balance_;
                signingInput.timestamp_ = this.timestamp_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.privateKey_ = byteString;
                this.from_ = "";
                this.to_ = "";
                this.amount_ = byteString;
                this.chainId_ = 0;
                this.idassetsId_ = 0;
                this.nonce_ = byteString;
                this.remark_ = "";
                this.balance_ = byteString;
                this.timestamp_ = 0;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                ByteString privateKey = signingInput.getPrivateKey();
                ByteString byteString = ByteString.EMPTY;
                if (privateKey != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (!signingInput.getFrom().isEmpty()) {
                    this.from_ = signingInput.from_;
                    onChanged();
                }
                if (!signingInput.getTo().isEmpty()) {
                    this.to_ = signingInput.to_;
                    onChanged();
                }
                if (signingInput.getAmount() != byteString) {
                    setAmount(signingInput.getAmount());
                }
                if (signingInput.getChainId() != 0) {
                    setChainId(signingInput.getChainId());
                }
                if (signingInput.getIdassetsId() != 0) {
                    setIdassetsId(signingInput.getIdassetsId());
                }
                if (signingInput.getNonce() != byteString) {
                    setNonce(signingInput.getNonce());
                }
                if (!signingInput.getRemark().isEmpty()) {
                    this.remark_ = signingInput.remark_;
                    onChanged();
                }
                if (signingInput.getBalance() != byteString) {
                    setBalance(signingInput.getBalance());
                }
                if (signingInput.getTimestamp() != 0) {
                    setTimestamp(signingInput.getTimestamp());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.privateKey_ = byteString;
                this.from_ = "";
                this.to_ = "";
                this.amount_ = byteString;
                this.nonce_ = byteString;
                this.remark_ = "";
                this.balance_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NULS.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NULS.SigningInput.access$7900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NULS$SigningInput r3 = (wallet.core.jni.proto.NULS.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NULS$SigningInput r4 = (wallet.core.jni.proto.NULS.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NULS.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NULS$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.privateKey_ = byteString;
            this.from_ = "";
            this.to_ = "";
            this.amount_ = byteString;
            this.nonce_ = byteString;
            this.remark_ = "";
            this.balance_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        switch (J) {
                            case 0:
                                break;
                            case 10:
                                this.privateKey_ = jVar.q();
                                continue;
                            case 18:
                                this.from_ = jVar.I();
                                continue;
                            case 26:
                                this.to_ = jVar.I();
                                continue;
                            case 34:
                                this.amount_ = jVar.q();
                                continue;
                            case 40:
                                this.chainId_ = jVar.K();
                                continue;
                            case 48:
                                this.idassetsId_ = jVar.K();
                                continue;
                            case 58:
                                this.nonce_ = jVar.q();
                                continue;
                            case 66:
                                this.remark_ = jVar.I();
                                continue;
                            case 74:
                                this.balance_ = jVar.q();
                                continue;
                            case 80:
                                this.timestamp_ = jVar.K();
                                continue;
                            default:
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getAmount();

        ByteString getBalance();

        int getChainId();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        String getFrom();

        ByteString getFromBytes();

        int getIdassetsId();

        /* synthetic */ String getInitializationErrorString();

        ByteString getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        String getRemark();

        ByteString getRemarkBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        int getTimestamp();

        String getTo();

        ByteString getToBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.NULS.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NULS.internal_static_TW_NULS_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.NULS.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NULS.internal_static_TW_NULS_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;

            public static final Descriptors.b getDescriptor() {
                return NULS.internal_static_TW_NULS_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NULS.internal_static_TW_NULS_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.NULS.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NULS.internal_static_TW_NULS_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                signingOutput.encoded_ = this.encoded_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NULS.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NULS.SigningOutput.access$9200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NULS$SigningOutput r3 = (wallet.core.jni.proto.NULS.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NULS$SigningOutput r4 = (wallet.core.jni.proto.NULS.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NULS.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NULS$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.encoded_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transaction extends GeneratedMessageV3 implements TransactionOrBuilder {
        public static final int HASH_FIELD_NUMBER = 8;
        public static final int INPUT_FIELD_NUMBER = 5;
        public static final int OUTPUT_FIELD_NUMBER = 6;
        public static final int REMARK_FIELD_NUMBER = 3;
        public static final int TIMESTAMP_FIELD_NUMBER = 2;
        public static final int TX_DATA_FIELD_NUMBER = 4;
        public static final int TX_SIGS_FIELD_NUMBER = 7;
        public static final int TYPE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private int hash_;
        private TransactionCoinFrom input_;
        private byte memoizedIsInitialized;
        private TransactionCoinTo output_;
        private volatile Object remark_;
        private int timestamp_;
        private ByteString txData_;
        private Signature txSigs_;
        private int type_;
        private static final Transaction DEFAULT_INSTANCE = new Transaction();
        private static final t0<Transaction> PARSER = new c<Transaction>() { // from class: wallet.core.jni.proto.NULS.Transaction.1
            @Override // com.google.protobuf.t0
            public Transaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transaction(jVar, rVar);
            }
        };

        public static Transaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NULS.internal_static_TW_NULS_Proto_Transaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transaction)) {
                return super.equals(obj);
            }
            Transaction transaction = (Transaction) obj;
            if (getType() == transaction.getType() && getTimestamp() == transaction.getTimestamp() && getRemark().equals(transaction.getRemark()) && getTxData().equals(transaction.getTxData()) && hasInput() == transaction.hasInput()) {
                if ((!hasInput() || getInput().equals(transaction.getInput())) && hasOutput() == transaction.hasOutput()) {
                    if ((!hasOutput() || getOutput().equals(transaction.getOutput())) && hasTxSigs() == transaction.hasTxSigs()) {
                        return (!hasTxSigs() || getTxSigs().equals(transaction.getTxSigs())) && getHash() == transaction.getHash() && this.unknownFields.equals(transaction.unknownFields);
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public int getHash() {
            return this.hash_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public TransactionCoinFrom getInput() {
            TransactionCoinFrom transactionCoinFrom = this.input_;
            return transactionCoinFrom == null ? TransactionCoinFrom.getDefaultInstance() : transactionCoinFrom;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public TransactionCoinFromOrBuilder getInputOrBuilder() {
            return getInput();
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public TransactionCoinTo getOutput() {
            TransactionCoinTo transactionCoinTo = this.output_;
            return transactionCoinTo == null ? TransactionCoinTo.getDefaultInstance() : transactionCoinTo;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public TransactionCoinToOrBuilder getOutputOrBuilder() {
            return getOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transaction> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public String getRemark() {
            Object obj = this.remark_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.remark_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public ByteString getRemarkBytes() {
            Object obj = this.remark_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.remark_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = this.type_;
            int Y = i2 != 0 ? 0 + CodedOutputStream.Y(1, i2) : 0;
            int i3 = this.timestamp_;
            if (i3 != 0) {
                Y += CodedOutputStream.Y(2, i3);
            }
            if (!getRemarkBytes().isEmpty()) {
                Y += GeneratedMessageV3.computeStringSize(3, this.remark_);
            }
            if (!this.txData_.isEmpty()) {
                Y += CodedOutputStream.h(4, this.txData_);
            }
            if (this.input_ != null) {
                Y += CodedOutputStream.G(5, getInput());
            }
            if (this.output_ != null) {
                Y += CodedOutputStream.G(6, getOutput());
            }
            if (this.txSigs_ != null) {
                Y += CodedOutputStream.G(7, getTxSigs());
            }
            int i4 = this.hash_;
            if (i4 != 0) {
                Y += CodedOutputStream.Y(8, i4);
            }
            int serializedSize = Y + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public int getTimestamp() {
            return this.timestamp_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public ByteString getTxData() {
            return this.txData_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public Signature getTxSigs() {
            Signature signature = this.txSigs_;
            return signature == null ? Signature.getDefaultInstance() : signature;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public SignatureOrBuilder getTxSigsOrBuilder() {
            return getTxSigs();
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public int getType() {
            return this.type_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public boolean hasInput() {
            return this.input_ != null;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public boolean hasOutput() {
            return this.output_ != null;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
        public boolean hasTxSigs() {
            return this.txSigs_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getType()) * 37) + 2) * 53) + getTimestamp()) * 37) + 3) * 53) + getRemark().hashCode()) * 37) + 4) * 53) + getTxData().hashCode();
            if (hasInput()) {
                hashCode = (((hashCode * 37) + 5) * 53) + getInput().hashCode();
            }
            if (hasOutput()) {
                hashCode = (((hashCode * 37) + 6) * 53) + getOutput().hashCode();
            }
            if (hasTxSigs()) {
                hashCode = (((hashCode * 37) + 7) * 53) + getTxSigs().hashCode();
            }
            int hash = (((((hashCode * 37) + 8) * 53) + getHash()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hash;
            return hash;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NULS.internal_static_TW_NULS_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            int i = this.type_;
            if (i != 0) {
                codedOutputStream.b1(1, i);
            }
            int i2 = this.timestamp_;
            if (i2 != 0) {
                codedOutputStream.b1(2, i2);
            }
            if (!getRemarkBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.remark_);
            }
            if (!this.txData_.isEmpty()) {
                codedOutputStream.q0(4, this.txData_);
            }
            if (this.input_ != null) {
                codedOutputStream.K0(5, getInput());
            }
            if (this.output_ != null) {
                codedOutputStream.K0(6, getOutput());
            }
            if (this.txSigs_ != null) {
                codedOutputStream.K0(7, getTxSigs());
            }
            int i3 = this.hash_;
            if (i3 != 0) {
                codedOutputStream.b1(8, i3);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOrBuilder {
            private int hash_;
            private a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> inputBuilder_;
            private TransactionCoinFrom input_;
            private a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> outputBuilder_;
            private TransactionCoinTo output_;
            private Object remark_;
            private int timestamp_;
            private ByteString txData_;
            private a1<Signature, Signature.Builder, SignatureOrBuilder> txSigsBuilder_;
            private Signature txSigs_;
            private int type_;

            public static final Descriptors.b getDescriptor() {
                return NULS.internal_static_TW_NULS_Proto_Transaction_descriptor;
            }

            private a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> getInputFieldBuilder() {
                if (this.inputBuilder_ == null) {
                    this.inputBuilder_ = new a1<>(getInput(), getParentForChildren(), isClean());
                    this.input_ = null;
                }
                return this.inputBuilder_;
            }

            private a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> getOutputFieldBuilder() {
                if (this.outputBuilder_ == null) {
                    this.outputBuilder_ = new a1<>(getOutput(), getParentForChildren(), isClean());
                    this.output_ = null;
                }
                return this.outputBuilder_;
            }

            private a1<Signature, Signature.Builder, SignatureOrBuilder> getTxSigsFieldBuilder() {
                if (this.txSigsBuilder_ == null) {
                    this.txSigsBuilder_ = new a1<>(getTxSigs(), getParentForChildren(), isClean());
                    this.txSigs_ = null;
                }
                return this.txSigsBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearHash() {
                this.hash_ = 0;
                onChanged();
                return this;
            }

            public Builder clearInput() {
                if (this.inputBuilder_ == null) {
                    this.input_ = null;
                    onChanged();
                } else {
                    this.input_ = null;
                    this.inputBuilder_ = null;
                }
                return this;
            }

            public Builder clearOutput() {
                if (this.outputBuilder_ == null) {
                    this.output_ = null;
                    onChanged();
                } else {
                    this.output_ = null;
                    this.outputBuilder_ = null;
                }
                return this;
            }

            public Builder clearRemark() {
                this.remark_ = Transaction.getDefaultInstance().getRemark();
                onChanged();
                return this;
            }

            public Builder clearTimestamp() {
                this.timestamp_ = 0;
                onChanged();
                return this;
            }

            public Builder clearTxData() {
                this.txData_ = Transaction.getDefaultInstance().getTxData();
                onChanged();
                return this;
            }

            public Builder clearTxSigs() {
                if (this.txSigsBuilder_ == null) {
                    this.txSigs_ = null;
                    onChanged();
                } else {
                    this.txSigs_ = null;
                    this.txSigsBuilder_ = null;
                }
                return this;
            }

            public Builder clearType() {
                this.type_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NULS.internal_static_TW_NULS_Proto_Transaction_descriptor;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public int getHash() {
                return this.hash_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public TransactionCoinFrom getInput() {
                a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> a1Var = this.inputBuilder_;
                if (a1Var == null) {
                    TransactionCoinFrom transactionCoinFrom = this.input_;
                    return transactionCoinFrom == null ? TransactionCoinFrom.getDefaultInstance() : transactionCoinFrom;
                }
                return a1Var.f();
            }

            public TransactionCoinFrom.Builder getInputBuilder() {
                onChanged();
                return getInputFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public TransactionCoinFromOrBuilder getInputOrBuilder() {
                a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> a1Var = this.inputBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                TransactionCoinFrom transactionCoinFrom = this.input_;
                return transactionCoinFrom == null ? TransactionCoinFrom.getDefaultInstance() : transactionCoinFrom;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public TransactionCoinTo getOutput() {
                a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> a1Var = this.outputBuilder_;
                if (a1Var == null) {
                    TransactionCoinTo transactionCoinTo = this.output_;
                    return transactionCoinTo == null ? TransactionCoinTo.getDefaultInstance() : transactionCoinTo;
                }
                return a1Var.f();
            }

            public TransactionCoinTo.Builder getOutputBuilder() {
                onChanged();
                return getOutputFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public TransactionCoinToOrBuilder getOutputOrBuilder() {
                a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> a1Var = this.outputBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                TransactionCoinTo transactionCoinTo = this.output_;
                return transactionCoinTo == null ? TransactionCoinTo.getDefaultInstance() : transactionCoinTo;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public String getRemark() {
                Object obj = this.remark_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.remark_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public ByteString getRemarkBytes() {
                Object obj = this.remark_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.remark_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public int getTimestamp() {
                return this.timestamp_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public ByteString getTxData() {
                return this.txData_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public Signature getTxSigs() {
                a1<Signature, Signature.Builder, SignatureOrBuilder> a1Var = this.txSigsBuilder_;
                if (a1Var == null) {
                    Signature signature = this.txSigs_;
                    return signature == null ? Signature.getDefaultInstance() : signature;
                }
                return a1Var.f();
            }

            public Signature.Builder getTxSigsBuilder() {
                onChanged();
                return getTxSigsFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public SignatureOrBuilder getTxSigsOrBuilder() {
                a1<Signature, Signature.Builder, SignatureOrBuilder> a1Var = this.txSigsBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Signature signature = this.txSigs_;
                return signature == null ? Signature.getDefaultInstance() : signature;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public int getType() {
                return this.type_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public boolean hasInput() {
                return (this.inputBuilder_ == null && this.input_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public boolean hasOutput() {
                return (this.outputBuilder_ == null && this.output_ == null) ? false : true;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionOrBuilder
            public boolean hasTxSigs() {
                return (this.txSigsBuilder_ == null && this.txSigs_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NULS.internal_static_TW_NULS_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeInput(TransactionCoinFrom transactionCoinFrom) {
                a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> a1Var = this.inputBuilder_;
                if (a1Var == null) {
                    TransactionCoinFrom transactionCoinFrom2 = this.input_;
                    if (transactionCoinFrom2 != null) {
                        this.input_ = TransactionCoinFrom.newBuilder(transactionCoinFrom2).mergeFrom(transactionCoinFrom).buildPartial();
                    } else {
                        this.input_ = transactionCoinFrom;
                    }
                    onChanged();
                } else {
                    a1Var.h(transactionCoinFrom);
                }
                return this;
            }

            public Builder mergeOutput(TransactionCoinTo transactionCoinTo) {
                a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> a1Var = this.outputBuilder_;
                if (a1Var == null) {
                    TransactionCoinTo transactionCoinTo2 = this.output_;
                    if (transactionCoinTo2 != null) {
                        this.output_ = TransactionCoinTo.newBuilder(transactionCoinTo2).mergeFrom(transactionCoinTo).buildPartial();
                    } else {
                        this.output_ = transactionCoinTo;
                    }
                    onChanged();
                } else {
                    a1Var.h(transactionCoinTo);
                }
                return this;
            }

            public Builder mergeTxSigs(Signature signature) {
                a1<Signature, Signature.Builder, SignatureOrBuilder> a1Var = this.txSigsBuilder_;
                if (a1Var == null) {
                    Signature signature2 = this.txSigs_;
                    if (signature2 != null) {
                        this.txSigs_ = Signature.newBuilder(signature2).mergeFrom(signature).buildPartial();
                    } else {
                        this.txSigs_ = signature;
                    }
                    onChanged();
                } else {
                    a1Var.h(signature);
                }
                return this;
            }

            public Builder setHash(int i) {
                this.hash_ = i;
                onChanged();
                return this;
            }

            public Builder setInput(TransactionCoinFrom transactionCoinFrom) {
                a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> a1Var = this.inputBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionCoinFrom);
                    this.input_ = transactionCoinFrom;
                    onChanged();
                } else {
                    a1Var.j(transactionCoinFrom);
                }
                return this;
            }

            public Builder setOutput(TransactionCoinTo transactionCoinTo) {
                a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> a1Var = this.outputBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionCoinTo);
                    this.output_ = transactionCoinTo;
                    onChanged();
                } else {
                    a1Var.j(transactionCoinTo);
                }
                return this;
            }

            public Builder setRemark(String str) {
                Objects.requireNonNull(str);
                this.remark_ = str;
                onChanged();
                return this;
            }

            public Builder setRemarkBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.remark_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTimestamp(int i) {
                this.timestamp_ = i;
                onChanged();
                return this;
            }

            public Builder setTxData(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.txData_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTxSigs(Signature signature) {
                a1<Signature, Signature.Builder, SignatureOrBuilder> a1Var = this.txSigsBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(signature);
                    this.txSigs_ = signature;
                    onChanged();
                } else {
                    a1Var.j(signature);
                }
                return this;
            }

            public Builder setType(int i) {
                this.type_ = i;
                onChanged();
                return this;
            }

            private Builder() {
                this.remark_ = "";
                this.txData_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction build() {
                Transaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction buildPartial() {
                Transaction transaction = new Transaction(this);
                transaction.type_ = this.type_;
                transaction.timestamp_ = this.timestamp_;
                transaction.remark_ = this.remark_;
                transaction.txData_ = this.txData_;
                a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> a1Var = this.inputBuilder_;
                if (a1Var == null) {
                    transaction.input_ = this.input_;
                } else {
                    transaction.input_ = a1Var.b();
                }
                a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> a1Var2 = this.outputBuilder_;
                if (a1Var2 == null) {
                    transaction.output_ = this.output_;
                } else {
                    transaction.output_ = a1Var2.b();
                }
                a1<Signature, Signature.Builder, SignatureOrBuilder> a1Var3 = this.txSigsBuilder_;
                if (a1Var3 == null) {
                    transaction.txSigs_ = this.txSigs_;
                } else {
                    transaction.txSigs_ = a1Var3.b();
                }
                transaction.hash_ = this.hash_;
                onBuilt();
                return transaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transaction getDefaultInstanceForType() {
                return Transaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.type_ = 0;
                this.timestamp_ = 0;
                this.remark_ = "";
                this.txData_ = ByteString.EMPTY;
                if (this.inputBuilder_ == null) {
                    this.input_ = null;
                } else {
                    this.input_ = null;
                    this.inputBuilder_ = null;
                }
                if (this.outputBuilder_ == null) {
                    this.output_ = null;
                } else {
                    this.output_ = null;
                    this.outputBuilder_ = null;
                }
                if (this.txSigsBuilder_ == null) {
                    this.txSigs_ = null;
                } else {
                    this.txSigs_ = null;
                    this.txSigsBuilder_ = null;
                }
                this.hash_ = 0;
                return this;
            }

            public Builder setInput(TransactionCoinFrom.Builder builder) {
                a1<TransactionCoinFrom, TransactionCoinFrom.Builder, TransactionCoinFromOrBuilder> a1Var = this.inputBuilder_;
                if (a1Var == null) {
                    this.input_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setOutput(TransactionCoinTo.Builder builder) {
                a1<TransactionCoinTo, TransactionCoinTo.Builder, TransactionCoinToOrBuilder> a1Var = this.outputBuilder_;
                if (a1Var == null) {
                    this.output_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder setTxSigs(Signature.Builder builder) {
                a1<Signature, Signature.Builder, SignatureOrBuilder> a1Var = this.txSigsBuilder_;
                if (a1Var == null) {
                    this.txSigs_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.remark_ = "";
                this.txData_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transaction) {
                    return mergeFrom((Transaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(Transaction transaction) {
                if (transaction == Transaction.getDefaultInstance()) {
                    return this;
                }
                if (transaction.getType() != 0) {
                    setType(transaction.getType());
                }
                if (transaction.getTimestamp() != 0) {
                    setTimestamp(transaction.getTimestamp());
                }
                if (!transaction.getRemark().isEmpty()) {
                    this.remark_ = transaction.remark_;
                    onChanged();
                }
                if (transaction.getTxData() != ByteString.EMPTY) {
                    setTxData(transaction.getTxData());
                }
                if (transaction.hasInput()) {
                    mergeInput(transaction.getInput());
                }
                if (transaction.hasOutput()) {
                    mergeOutput(transaction.getOutput());
                }
                if (transaction.hasTxSigs()) {
                    mergeTxSigs(transaction.getTxSigs());
                }
                if (transaction.getHash() != 0) {
                    setHash(transaction.getHash());
                }
                mergeUnknownFields(transaction.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NULS.Transaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NULS.Transaction.access$5900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NULS$Transaction r3 = (wallet.core.jni.proto.NULS.Transaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NULS$Transaction r4 = (wallet.core.jni.proto.NULS.Transaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NULS.Transaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NULS$Transaction$Builder");
            }
        }

        public static Builder newBuilder(Transaction transaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transaction);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static Transaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private Transaction() {
            this.memoizedIsInitialized = (byte) -1;
            this.remark_ = "";
            this.txData_ = ByteString.EMPTY;
        }

        public static Transaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static Transaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transaction parseFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Transaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.type_ = jVar.K();
                            } else if (J == 16) {
                                this.timestamp_ = jVar.K();
                            } else if (J == 26) {
                                this.remark_ = jVar.I();
                            } else if (J != 34) {
                                if (J == 42) {
                                    TransactionCoinFrom transactionCoinFrom = this.input_;
                                    TransactionCoinFrom.Builder builder = transactionCoinFrom != null ? transactionCoinFrom.toBuilder() : null;
                                    TransactionCoinFrom transactionCoinFrom2 = (TransactionCoinFrom) jVar.z(TransactionCoinFrom.parser(), rVar);
                                    this.input_ = transactionCoinFrom2;
                                    if (builder != null) {
                                        builder.mergeFrom(transactionCoinFrom2);
                                        this.input_ = builder.buildPartial();
                                    }
                                } else if (J == 50) {
                                    TransactionCoinTo transactionCoinTo = this.output_;
                                    TransactionCoinTo.Builder builder2 = transactionCoinTo != null ? transactionCoinTo.toBuilder() : null;
                                    TransactionCoinTo transactionCoinTo2 = (TransactionCoinTo) jVar.z(TransactionCoinTo.parser(), rVar);
                                    this.output_ = transactionCoinTo2;
                                    if (builder2 != null) {
                                        builder2.mergeFrom(transactionCoinTo2);
                                        this.output_ = builder2.buildPartial();
                                    }
                                } else if (J == 58) {
                                    Signature signature = this.txSigs_;
                                    Signature.Builder builder3 = signature != null ? signature.toBuilder() : null;
                                    Signature signature2 = (Signature) jVar.z(Signature.parser(), rVar);
                                    this.txSigs_ = signature2;
                                    if (builder3 != null) {
                                        builder3.mergeFrom(signature2);
                                        this.txSigs_ = builder3.buildPartial();
                                    }
                                } else if (J != 64) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.hash_ = jVar.K();
                                }
                            } else {
                                this.txData_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(j jVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transaction parseFrom(j jVar, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public static final class TransactionCoinFrom extends GeneratedMessageV3 implements TransactionCoinFromOrBuilder {
        public static final int ASSETS_CHAINID_FIELD_NUMBER = 2;
        public static final int ASSETS_ID_FIELD_NUMBER = 3;
        public static final int FROM_ADDRESS_FIELD_NUMBER = 1;
        public static final int ID_AMOUNT_FIELD_NUMBER = 4;
        public static final int LOCKED_FIELD_NUMBER = 6;
        public static final int NONCE_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private int assetsChainid_;
        private int assetsId_;
        private volatile Object fromAddress_;
        private ByteString idAmount_;
        private int locked_;
        private byte memoizedIsInitialized;
        private ByteString nonce_;
        private static final TransactionCoinFrom DEFAULT_INSTANCE = new TransactionCoinFrom();
        private static final t0<TransactionCoinFrom> PARSER = new c<TransactionCoinFrom>() { // from class: wallet.core.jni.proto.NULS.TransactionCoinFrom.1
            @Override // com.google.protobuf.t0
            public TransactionCoinFrom parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionCoinFrom(jVar, rVar);
            }
        };

        public static TransactionCoinFrom getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NULS.internal_static_TW_NULS_Proto_TransactionCoinFrom_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionCoinFrom parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionCoinFrom) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionCoinFrom parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionCoinFrom> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionCoinFrom)) {
                return super.equals(obj);
            }
            TransactionCoinFrom transactionCoinFrom = (TransactionCoinFrom) obj;
            return getFromAddress().equals(transactionCoinFrom.getFromAddress()) && getAssetsChainid() == transactionCoinFrom.getAssetsChainid() && getAssetsId() == transactionCoinFrom.getAssetsId() && getIdAmount().equals(transactionCoinFrom.getIdAmount()) && getNonce().equals(transactionCoinFrom.getNonce()) && getLocked() == transactionCoinFrom.getLocked() && this.unknownFields.equals(transactionCoinFrom.unknownFields);
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
        public int getAssetsChainid() {
            return this.assetsChainid_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
        public int getAssetsId() {
            return this.assetsId_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
        public String getFromAddress() {
            Object obj = this.fromAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.fromAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
        public ByteString getFromAddressBytes() {
            Object obj = this.fromAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.fromAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
        public ByteString getIdAmount() {
            return this.idAmount_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
        public int getLocked() {
            return this.locked_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
        public ByteString getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionCoinFrom> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getFromAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.fromAddress_);
            int i2 = this.assetsChainid_;
            if (i2 != 0) {
                computeStringSize += CodedOutputStream.Y(2, i2);
            }
            int i3 = this.assetsId_;
            if (i3 != 0) {
                computeStringSize += CodedOutputStream.Y(3, i3);
            }
            if (!this.idAmount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.idAmount_);
            }
            if (!this.nonce_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(5, this.nonce_);
            }
            int i4 = this.locked_;
            if (i4 != 0) {
                computeStringSize += CodedOutputStream.Y(6, i4);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFromAddress().hashCode()) * 37) + 2) * 53) + getAssetsChainid()) * 37) + 3) * 53) + getAssetsId()) * 37) + 4) * 53) + getIdAmount().hashCode()) * 37) + 5) * 53) + getNonce().hashCode()) * 37) + 6) * 53) + getLocked()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NULS.internal_static_TW_NULS_Proto_TransactionCoinFrom_fieldAccessorTable.d(TransactionCoinFrom.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionCoinFrom();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getFromAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.fromAddress_);
            }
            int i = this.assetsChainid_;
            if (i != 0) {
                codedOutputStream.b1(2, i);
            }
            int i2 = this.assetsId_;
            if (i2 != 0) {
                codedOutputStream.b1(3, i2);
            }
            if (!this.idAmount_.isEmpty()) {
                codedOutputStream.q0(4, this.idAmount_);
            }
            if (!this.nonce_.isEmpty()) {
                codedOutputStream.q0(5, this.nonce_);
            }
            int i3 = this.locked_;
            if (i3 != 0) {
                codedOutputStream.b1(6, i3);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionCoinFromOrBuilder {
            private int assetsChainid_;
            private int assetsId_;
            private Object fromAddress_;
            private ByteString idAmount_;
            private int locked_;
            private ByteString nonce_;

            public static final Descriptors.b getDescriptor() {
                return NULS.internal_static_TW_NULS_Proto_TransactionCoinFrom_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAssetsChainid() {
                this.assetsChainid_ = 0;
                onChanged();
                return this;
            }

            public Builder clearAssetsId() {
                this.assetsId_ = 0;
                onChanged();
                return this;
            }

            public Builder clearFromAddress() {
                this.fromAddress_ = TransactionCoinFrom.getDefaultInstance().getFromAddress();
                onChanged();
                return this;
            }

            public Builder clearIdAmount() {
                this.idAmount_ = TransactionCoinFrom.getDefaultInstance().getIdAmount();
                onChanged();
                return this;
            }

            public Builder clearLocked() {
                this.locked_ = 0;
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = TransactionCoinFrom.getDefaultInstance().getNonce();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
            public int getAssetsChainid() {
                return this.assetsChainid_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
            public int getAssetsId() {
                return this.assetsId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NULS.internal_static_TW_NULS_Proto_TransactionCoinFrom_descriptor;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
            public String getFromAddress() {
                Object obj = this.fromAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.fromAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
            public ByteString getFromAddressBytes() {
                Object obj = this.fromAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.fromAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
            public ByteString getIdAmount() {
                return this.idAmount_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
            public int getLocked() {
                return this.locked_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinFromOrBuilder
            public ByteString getNonce() {
                return this.nonce_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NULS.internal_static_TW_NULS_Proto_TransactionCoinFrom_fieldAccessorTable.d(TransactionCoinFrom.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAssetsChainid(int i) {
                this.assetsChainid_ = i;
                onChanged();
                return this;
            }

            public Builder setAssetsId(int i) {
                this.assetsId_ = i;
                onChanged();
                return this;
            }

            public Builder setFromAddress(String str) {
                Objects.requireNonNull(str);
                this.fromAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setFromAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.fromAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setIdAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.idAmount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setLocked(int i) {
                this.locked_ = i;
                onChanged();
                return this;
            }

            public Builder setNonce(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.nonce_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.fromAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.idAmount_ = byteString;
                this.nonce_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionCoinFrom build() {
                TransactionCoinFrom buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionCoinFrom buildPartial() {
                TransactionCoinFrom transactionCoinFrom = new TransactionCoinFrom(this);
                transactionCoinFrom.fromAddress_ = this.fromAddress_;
                transactionCoinFrom.assetsChainid_ = this.assetsChainid_;
                transactionCoinFrom.assetsId_ = this.assetsId_;
                transactionCoinFrom.idAmount_ = this.idAmount_;
                transactionCoinFrom.nonce_ = this.nonce_;
                transactionCoinFrom.locked_ = this.locked_;
                onBuilt();
                return transactionCoinFrom;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionCoinFrom getDefaultInstanceForType() {
                return TransactionCoinFrom.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.fromAddress_ = "";
                this.assetsChainid_ = 0;
                this.assetsId_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.idAmount_ = byteString;
                this.nonce_ = byteString;
                this.locked_ = 0;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionCoinFrom) {
                    return mergeFrom((TransactionCoinFrom) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.fromAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.idAmount_ = byteString;
                this.nonce_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransactionCoinFrom transactionCoinFrom) {
                if (transactionCoinFrom == TransactionCoinFrom.getDefaultInstance()) {
                    return this;
                }
                if (!transactionCoinFrom.getFromAddress().isEmpty()) {
                    this.fromAddress_ = transactionCoinFrom.fromAddress_;
                    onChanged();
                }
                if (transactionCoinFrom.getAssetsChainid() != 0) {
                    setAssetsChainid(transactionCoinFrom.getAssetsChainid());
                }
                if (transactionCoinFrom.getAssetsId() != 0) {
                    setAssetsId(transactionCoinFrom.getAssetsId());
                }
                ByteString idAmount = transactionCoinFrom.getIdAmount();
                ByteString byteString = ByteString.EMPTY;
                if (idAmount != byteString) {
                    setIdAmount(transactionCoinFrom.getIdAmount());
                }
                if (transactionCoinFrom.getNonce() != byteString) {
                    setNonce(transactionCoinFrom.getNonce());
                }
                if (transactionCoinFrom.getLocked() != 0) {
                    setLocked(transactionCoinFrom.getLocked());
                }
                mergeUnknownFields(transactionCoinFrom.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NULS.TransactionCoinFrom.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NULS.TransactionCoinFrom.access$1300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NULS$TransactionCoinFrom r3 = (wallet.core.jni.proto.NULS.TransactionCoinFrom) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NULS$TransactionCoinFrom r4 = (wallet.core.jni.proto.NULS.TransactionCoinFrom) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NULS.TransactionCoinFrom.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NULS$TransactionCoinFrom$Builder");
            }
        }

        public static Builder newBuilder(TransactionCoinFrom transactionCoinFrom) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionCoinFrom);
        }

        public static TransactionCoinFrom parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionCoinFrom(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionCoinFrom parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionCoinFrom) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionCoinFrom parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionCoinFrom getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionCoinFrom parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionCoinFrom() {
            this.memoizedIsInitialized = (byte) -1;
            this.fromAddress_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.idAmount_ = byteString;
            this.nonce_ = byteString;
        }

        public static TransactionCoinFrom parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionCoinFrom parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionCoinFrom parseFrom(InputStream inputStream) throws IOException {
            return (TransactionCoinFrom) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransactionCoinFrom parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionCoinFrom) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TransactionCoinFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.fromAddress_ = jVar.I();
                            } else if (J == 16) {
                                this.assetsChainid_ = jVar.K();
                            } else if (J == 24) {
                                this.assetsId_ = jVar.K();
                            } else if (J == 34) {
                                this.idAmount_ = jVar.q();
                            } else if (J == 42) {
                                this.nonce_ = jVar.q();
                            } else if (J != 48) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.locked_ = jVar.K();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionCoinFrom parseFrom(j jVar) throws IOException {
            return (TransactionCoinFrom) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionCoinFrom parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionCoinFrom) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionCoinFromOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        int getAssetsChainid();

        int getAssetsId();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        String getFromAddress();

        ByteString getFromAddressBytes();

        ByteString getIdAmount();

        /* synthetic */ String getInitializationErrorString();

        int getLocked();

        ByteString getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionCoinTo extends GeneratedMessageV3 implements TransactionCoinToOrBuilder {
        public static final int ASSETS_CHAINID_FIELD_NUMBER = 2;
        public static final int ASSETS_ID_FIELD_NUMBER = 3;
        public static final int ID_AMOUNT_FIELD_NUMBER = 4;
        public static final int LOCK_TIME_FIELD_NUMBER = 5;
        public static final int TO_ADDRESS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private int assetsChainid_;
        private int assetsId_;
        private ByteString idAmount_;
        private int lockTime_;
        private byte memoizedIsInitialized;
        private volatile Object toAddress_;
        private static final TransactionCoinTo DEFAULT_INSTANCE = new TransactionCoinTo();
        private static final t0<TransactionCoinTo> PARSER = new c<TransactionCoinTo>() { // from class: wallet.core.jni.proto.NULS.TransactionCoinTo.1
            @Override // com.google.protobuf.t0
            public TransactionCoinTo parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionCoinTo(jVar, rVar);
            }
        };

        public static TransactionCoinTo getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NULS.internal_static_TW_NULS_Proto_TransactionCoinTo_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionCoinTo parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionCoinTo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionCoinTo parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionCoinTo> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionCoinTo)) {
                return super.equals(obj);
            }
            TransactionCoinTo transactionCoinTo = (TransactionCoinTo) obj;
            return getToAddress().equals(transactionCoinTo.getToAddress()) && getAssetsChainid() == transactionCoinTo.getAssetsChainid() && getAssetsId() == transactionCoinTo.getAssetsId() && getIdAmount().equals(transactionCoinTo.getIdAmount()) && getLockTime() == transactionCoinTo.getLockTime() && this.unknownFields.equals(transactionCoinTo.unknownFields);
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
        public int getAssetsChainid() {
            return this.assetsChainid_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
        public int getAssetsId() {
            return this.assetsId_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
        public ByteString getIdAmount() {
            return this.idAmount_;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
        public int getLockTime() {
            return this.lockTime_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionCoinTo> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getToAddressBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.toAddress_);
            int i2 = this.assetsChainid_;
            if (i2 != 0) {
                computeStringSize += CodedOutputStream.Y(2, i2);
            }
            int i3 = this.assetsId_;
            if (i3 != 0) {
                computeStringSize += CodedOutputStream.Y(3, i3);
            }
            if (!this.idAmount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.idAmount_);
            }
            int i4 = this.lockTime_;
            if (i4 != 0) {
                computeStringSize += CodedOutputStream.Y(5, i4);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getToAddress().hashCode()) * 37) + 2) * 53) + getAssetsChainid()) * 37) + 3) * 53) + getAssetsId()) * 37) + 4) * 53) + getIdAmount().hashCode()) * 37) + 5) * 53) + getLockTime()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NULS.internal_static_TW_NULS_Proto_TransactionCoinTo_fieldAccessorTable.d(TransactionCoinTo.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionCoinTo();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.toAddress_);
            }
            int i = this.assetsChainid_;
            if (i != 0) {
                codedOutputStream.b1(2, i);
            }
            int i2 = this.assetsId_;
            if (i2 != 0) {
                codedOutputStream.b1(3, i2);
            }
            if (!this.idAmount_.isEmpty()) {
                codedOutputStream.q0(4, this.idAmount_);
            }
            int i3 = this.lockTime_;
            if (i3 != 0) {
                codedOutputStream.b1(5, i3);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionCoinToOrBuilder {
            private int assetsChainid_;
            private int assetsId_;
            private ByteString idAmount_;
            private int lockTime_;
            private Object toAddress_;

            public static final Descriptors.b getDescriptor() {
                return NULS.internal_static_TW_NULS_Proto_TransactionCoinTo_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAssetsChainid() {
                this.assetsChainid_ = 0;
                onChanged();
                return this;
            }

            public Builder clearAssetsId() {
                this.assetsId_ = 0;
                onChanged();
                return this;
            }

            public Builder clearIdAmount() {
                this.idAmount_ = TransactionCoinTo.getDefaultInstance().getIdAmount();
                onChanged();
                return this;
            }

            public Builder clearLockTime() {
                this.lockTime_ = 0;
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransactionCoinTo.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
            public int getAssetsChainid() {
                return this.assetsChainid_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
            public int getAssetsId() {
                return this.assetsId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NULS.internal_static_TW_NULS_Proto_TransactionCoinTo_descriptor;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
            public ByteString getIdAmount() {
                return this.idAmount_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
            public int getLockTime() {
                return this.lockTime_;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NULS.TransactionCoinToOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NULS.internal_static_TW_NULS_Proto_TransactionCoinTo_fieldAccessorTable.d(TransactionCoinTo.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAssetsChainid(int i) {
                this.assetsChainid_ = i;
                onChanged();
                return this;
            }

            public Builder setAssetsId(int i) {
                this.assetsId_ = i;
                onChanged();
                return this;
            }

            public Builder setIdAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.idAmount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setLockTime(int i) {
                this.lockTime_ = i;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.toAddress_ = "";
                this.idAmount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionCoinTo build() {
                TransactionCoinTo buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionCoinTo buildPartial() {
                TransactionCoinTo transactionCoinTo = new TransactionCoinTo(this);
                transactionCoinTo.toAddress_ = this.toAddress_;
                transactionCoinTo.assetsChainid_ = this.assetsChainid_;
                transactionCoinTo.assetsId_ = this.assetsId_;
                transactionCoinTo.idAmount_ = this.idAmount_;
                transactionCoinTo.lockTime_ = this.lockTime_;
                onBuilt();
                return transactionCoinTo;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionCoinTo getDefaultInstanceForType() {
                return TransactionCoinTo.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.toAddress_ = "";
                this.assetsChainid_ = 0;
                this.assetsId_ = 0;
                this.idAmount_ = ByteString.EMPTY;
                this.lockTime_ = 0;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.toAddress_ = "";
                this.idAmount_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionCoinTo) {
                    return mergeFrom((TransactionCoinTo) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionCoinTo transactionCoinTo) {
                if (transactionCoinTo == TransactionCoinTo.getDefaultInstance()) {
                    return this;
                }
                if (!transactionCoinTo.getToAddress().isEmpty()) {
                    this.toAddress_ = transactionCoinTo.toAddress_;
                    onChanged();
                }
                if (transactionCoinTo.getAssetsChainid() != 0) {
                    setAssetsChainid(transactionCoinTo.getAssetsChainid());
                }
                if (transactionCoinTo.getAssetsId() != 0) {
                    setAssetsId(transactionCoinTo.getAssetsId());
                }
                if (transactionCoinTo.getIdAmount() != ByteString.EMPTY) {
                    setIdAmount(transactionCoinTo.getIdAmount());
                }
                if (transactionCoinTo.getLockTime() != 0) {
                    setLockTime(transactionCoinTo.getLockTime());
                }
                mergeUnknownFields(transactionCoinTo.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NULS.TransactionCoinTo.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NULS.TransactionCoinTo.access$2800()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NULS$TransactionCoinTo r3 = (wallet.core.jni.proto.NULS.TransactionCoinTo) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NULS$TransactionCoinTo r4 = (wallet.core.jni.proto.NULS.TransactionCoinTo) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NULS.TransactionCoinTo.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NULS$TransactionCoinTo$Builder");
            }
        }

        public static Builder newBuilder(TransactionCoinTo transactionCoinTo) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionCoinTo);
        }

        public static TransactionCoinTo parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionCoinTo(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionCoinTo parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionCoinTo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionCoinTo parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionCoinTo getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionCoinTo parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionCoinTo() {
            this.memoizedIsInitialized = (byte) -1;
            this.toAddress_ = "";
            this.idAmount_ = ByteString.EMPTY;
        }

        public static TransactionCoinTo parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionCoinTo parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionCoinTo parseFrom(InputStream inputStream) throws IOException {
            return (TransactionCoinTo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionCoinTo(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.toAddress_ = jVar.I();
                                } else if (J == 16) {
                                    this.assetsChainid_ = jVar.K();
                                } else if (J == 24) {
                                    this.assetsId_ = jVar.K();
                                } else if (J == 34) {
                                    this.idAmount_ = jVar.q();
                                } else if (J != 40) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.lockTime_ = jVar.K();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionCoinTo parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionCoinTo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionCoinTo parseFrom(j jVar) throws IOException {
            return (TransactionCoinTo) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionCoinTo parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionCoinTo) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionCoinToOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        int getAssetsChainid();

        int getAssetsId();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getIdAmount();

        /* synthetic */ String getInitializationErrorString();

        int getLockTime();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public interface TransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        int getHash();

        /* synthetic */ String getInitializationErrorString();

        TransactionCoinFrom getInput();

        TransactionCoinFromOrBuilder getInputOrBuilder();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        TransactionCoinTo getOutput();

        TransactionCoinToOrBuilder getOutputOrBuilder();

        String getRemark();

        ByteString getRemarkBytes();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        int getTimestamp();

        ByteString getTxData();

        Signature getTxSigs();

        SignatureOrBuilder getTxSigsOrBuilder();

        int getType();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        boolean hasInput();

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasOutput();

        boolean hasTxSigs();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_NULS_Proto_TransactionCoinFrom_descriptor = bVar;
        internal_static_TW_NULS_Proto_TransactionCoinFrom_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"FromAddress", "AssetsChainid", "AssetsId", "IdAmount", "Nonce", "Locked"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_NULS_Proto_TransactionCoinTo_descriptor = bVar2;
        internal_static_TW_NULS_Proto_TransactionCoinTo_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"ToAddress", "AssetsChainid", "AssetsId", "IdAmount", "LockTime"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_NULS_Proto_Signature_descriptor = bVar3;
        internal_static_TW_NULS_Proto_Signature_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"PkeyLen", "PublicKey", "SigLen", "Signature"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_NULS_Proto_Transaction_descriptor = bVar4;
        internal_static_TW_NULS_Proto_Transaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Type", "Timestamp", "Remark", "TxData", "Input", "Output", "TxSigs", "Hash"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_NULS_Proto_SigningInput_descriptor = bVar5;
        internal_static_TW_NULS_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"PrivateKey", "From", "To", "Amount", "ChainId", "IdassetsId", "Nonce", "Remark", "Balance", "Timestamp"});
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_NULS_Proto_SigningOutput_descriptor = bVar6;
        internal_static_TW_NULS_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"Encoded"});
    }

    private NULS() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
