package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Nano {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\nNano.proto\u0012\rTW.Nano.Proto\"®\u0001\n\fSigningInput\u0012\u0013\n\u000bprivate_key\u0018\u0001 \u0001(\f\u0012\u0014\n\fparent_block\u0018\u0002 \u0001(\f\u0012\u0014\n\nlink_block\u0018\u0003 \u0001(\fH\u0000\u0012\u0018\n\u000elink_recipient\u0018\u0004 \u0001(\tH\u0000\u0012\u0016\n\u000erepresentative\u0018\u0005 \u0001(\t\u0012\u000f\n\u0007balance\u0018\u0006 \u0001(\t\u0012\f\n\u0004work\u0018\u0007 \u0001(\tB\f\n\nlink_oneof\"D\n\rSigningOutput\u0012\u0011\n\tsignature\u0018\u0001 \u0001(\f\u0012\u0012\n\nblock_hash\u0018\u0002 \u0001(\f\u0012\f\n\u0004json\u0018\u0003 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Nano_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Nano_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Nano_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Nano_Proto_SigningOutput_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Nano$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Nano$SigningInput$LinkOneofCase;

        static {
            int[] iArr = new int[SigningInput.LinkOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Nano$SigningInput$LinkOneofCase = iArr;
            try {
                iArr[SigningInput.LinkOneofCase.LINK_BLOCK.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Nano$SigningInput$LinkOneofCase[SigningInput.LinkOneofCase.LINK_RECIPIENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Nano$SigningInput$LinkOneofCase[SigningInput.LinkOneofCase.LINKONEOF_NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int BALANCE_FIELD_NUMBER = 6;
        public static final int LINK_BLOCK_FIELD_NUMBER = 3;
        public static final int LINK_RECIPIENT_FIELD_NUMBER = 4;
        public static final int PARENT_BLOCK_FIELD_NUMBER = 2;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 1;
        public static final int REPRESENTATIVE_FIELD_NUMBER = 5;
        public static final int WORK_FIELD_NUMBER = 7;
        private static final long serialVersionUID = 0;
        private volatile Object balance_;
        private int linkOneofCase_;
        private Object linkOneof_;
        private byte memoizedIsInitialized;
        private ByteString parentBlock_;
        private ByteString privateKey_;
        private volatile Object representative_;
        private volatile Object work_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Nano.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private Object balance_;
            private int linkOneofCase_;
            private Object linkOneof_;
            private ByteString parentBlock_;
            private ByteString privateKey_;
            private Object representative_;
            private Object work_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Nano.internal_static_TW_Nano_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBalance() {
                this.balance_ = SigningInput.getDefaultInstance().getBalance();
                onChanged();
                return this;
            }

            public Builder clearLinkBlock() {
                if (this.linkOneofCase_ == 3) {
                    this.linkOneofCase_ = 0;
                    this.linkOneof_ = null;
                    onChanged();
                }
                return this;
            }

            public Builder clearLinkOneof() {
                this.linkOneofCase_ = 0;
                this.linkOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearLinkRecipient() {
                if (this.linkOneofCase_ == 4) {
                    this.linkOneofCase_ = 0;
                    this.linkOneof_ = null;
                    onChanged();
                }
                return this;
            }

            public Builder clearParentBlock() {
                this.parentBlock_ = SigningInput.getDefaultInstance().getParentBlock();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearRepresentative() {
                this.representative_ = SigningInput.getDefaultInstance().getRepresentative();
                onChanged();
                return this;
            }

            public Builder clearWork() {
                this.work_ = SigningInput.getDefaultInstance().getWork();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public String getBalance() {
                Object obj = this.balance_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.balance_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public ByteString getBalanceBytes() {
                Object obj = this.balance_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.balance_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Nano.internal_static_TW_Nano_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public ByteString getLinkBlock() {
                if (this.linkOneofCase_ == 3) {
                    return (ByteString) this.linkOneof_;
                }
                return ByteString.EMPTY;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public LinkOneofCase getLinkOneofCase() {
                return LinkOneofCase.forNumber(this.linkOneofCase_);
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public String getLinkRecipient() {
                String str = this.linkOneofCase_ == 4 ? this.linkOneof_ : "";
                if (!(str instanceof String)) {
                    String stringUtf8 = ((ByteString) str).toStringUtf8();
                    if (this.linkOneofCase_ == 4) {
                        this.linkOneof_ = stringUtf8;
                    }
                    return stringUtf8;
                }
                return (String) str;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public ByteString getLinkRecipientBytes() {
                String str = this.linkOneofCase_ == 4 ? this.linkOneof_ : "";
                if (str instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) str);
                    if (this.linkOneofCase_ == 4) {
                        this.linkOneof_ = copyFromUtf8;
                    }
                    return copyFromUtf8;
                }
                return (ByteString) str;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public ByteString getParentBlock() {
                return this.parentBlock_;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public String getRepresentative() {
                Object obj = this.representative_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.representative_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public ByteString getRepresentativeBytes() {
                Object obj = this.representative_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.representative_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public String getWork() {
                Object obj = this.work_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.work_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
            public ByteString getWorkBytes() {
                Object obj = this.work_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.work_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Nano.internal_static_TW_Nano_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setBalance(String str) {
                Objects.requireNonNull(str);
                this.balance_ = str;
                onChanged();
                return this;
            }

            public Builder setBalanceBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.balance_ = byteString;
                onChanged();
                return this;
            }

            public Builder setLinkBlock(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.linkOneofCase_ = 3;
                this.linkOneof_ = byteString;
                onChanged();
                return this;
            }

            public Builder setLinkRecipient(String str) {
                Objects.requireNonNull(str);
                this.linkOneofCase_ = 4;
                this.linkOneof_ = str;
                onChanged();
                return this;
            }

            public Builder setLinkRecipientBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.linkOneofCase_ = 4;
                this.linkOneof_ = byteString;
                onChanged();
                return this;
            }

            public Builder setParentBlock(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.parentBlock_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setRepresentative(String str) {
                Objects.requireNonNull(str);
                this.representative_ = str;
                onChanged();
                return this;
            }

            public Builder setRepresentativeBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.representative_ = byteString;
                onChanged();
                return this;
            }

            public Builder setWork(String str) {
                Objects.requireNonNull(str);
                this.work_ = str;
                onChanged();
                return this;
            }

            public Builder setWorkBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.work_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.linkOneofCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.privateKey_ = byteString;
                this.parentBlock_ = byteString;
                this.representative_ = "";
                this.balance_ = "";
                this.work_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.privateKey_ = this.privateKey_;
                signingInput.parentBlock_ = this.parentBlock_;
                if (this.linkOneofCase_ == 3) {
                    signingInput.linkOneof_ = this.linkOneof_;
                }
                if (this.linkOneofCase_ == 4) {
                    signingInput.linkOneof_ = this.linkOneof_;
                }
                signingInput.representative_ = this.representative_;
                signingInput.balance_ = this.balance_;
                signingInput.work_ = this.work_;
                signingInput.linkOneofCase_ = this.linkOneofCase_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.privateKey_ = byteString;
                this.parentBlock_ = byteString;
                this.representative_ = "";
                this.balance_ = "";
                this.work_ = "";
                this.linkOneofCase_ = 0;
                this.linkOneof_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                ByteString privateKey = signingInput.getPrivateKey();
                ByteString byteString = ByteString.EMPTY;
                if (privateKey != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (signingInput.getParentBlock() != byteString) {
                    setParentBlock(signingInput.getParentBlock());
                }
                if (!signingInput.getRepresentative().isEmpty()) {
                    this.representative_ = signingInput.representative_;
                    onChanged();
                }
                if (!signingInput.getBalance().isEmpty()) {
                    this.balance_ = signingInput.balance_;
                    onChanged();
                }
                if (!signingInput.getWork().isEmpty()) {
                    this.work_ = signingInput.work_;
                    onChanged();
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Nano$SigningInput$LinkOneofCase[signingInput.getLinkOneofCase().ordinal()];
                if (i == 1) {
                    setLinkBlock(signingInput.getLinkBlock());
                } else if (i == 2) {
                    this.linkOneofCase_ = 4;
                    this.linkOneof_ = signingInput.linkOneof_;
                    onChanged();
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.linkOneofCase_ = 0;
                ByteString byteString = ByteString.EMPTY;
                this.privateKey_ = byteString;
                this.parentBlock_ = byteString;
                this.representative_ = "";
                this.balance_ = "";
                this.work_ = "";
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Nano.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Nano.SigningInput.access$1400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Nano$SigningInput r3 = (wallet.core.jni.proto.Nano.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Nano$SigningInput r4 = (wallet.core.jni.proto.Nano.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Nano.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Nano$SigningInput$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum LinkOneofCase implements a0.c {
            LINK_BLOCK(3),
            LINK_RECIPIENT(4),
            LINKONEOF_NOT_SET(0);
            
            private final int value;

            LinkOneofCase(int i) {
                this.value = i;
            }

            public static LinkOneofCase forNumber(int i) {
                if (i != 0) {
                    if (i != 3) {
                        if (i != 4) {
                            return null;
                        }
                        return LINK_RECIPIENT;
                    }
                    return LINK_BLOCK;
                }
                return LINKONEOF_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static LinkOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Nano.internal_static_TW_Nano_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getPrivateKey().equals(signingInput.getPrivateKey()) && getParentBlock().equals(signingInput.getParentBlock()) && getRepresentative().equals(signingInput.getRepresentative()) && getBalance().equals(signingInput.getBalance()) && getWork().equals(signingInput.getWork()) && getLinkOneofCase().equals(signingInput.getLinkOneofCase())) {
                int i = this.linkOneofCase_;
                if (i != 3) {
                    if (i == 4 && !getLinkRecipient().equals(signingInput.getLinkRecipient())) {
                        return false;
                    }
                } else if (!getLinkBlock().equals(signingInput.getLinkBlock())) {
                    return false;
                }
                return this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public String getBalance() {
            Object obj = this.balance_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.balance_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public ByteString getBalanceBytes() {
            Object obj = this.balance_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.balance_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public ByteString getLinkBlock() {
            if (this.linkOneofCase_ == 3) {
                return (ByteString) this.linkOneof_;
            }
            return ByteString.EMPTY;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public LinkOneofCase getLinkOneofCase() {
            return LinkOneofCase.forNumber(this.linkOneofCase_);
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public String getLinkRecipient() {
            String str = this.linkOneofCase_ == 4 ? this.linkOneof_ : "";
            if (str instanceof String) {
                return (String) str;
            }
            String stringUtf8 = ((ByteString) str).toStringUtf8();
            if (this.linkOneofCase_ == 4) {
                this.linkOneof_ = stringUtf8;
            }
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public ByteString getLinkRecipientBytes() {
            String str = this.linkOneofCase_ == 4 ? this.linkOneof_ : "";
            if (str instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) str);
                if (this.linkOneofCase_ == 4) {
                    this.linkOneof_ = copyFromUtf8;
                }
                return copyFromUtf8;
            }
            return (ByteString) str;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public ByteString getParentBlock() {
            return this.parentBlock_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public String getRepresentative() {
            Object obj = this.representative_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.representative_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public ByteString getRepresentativeBytes() {
            Object obj = this.representative_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.representative_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.privateKey_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.privateKey_);
            if (!this.parentBlock_.isEmpty()) {
                h += CodedOutputStream.h(2, this.parentBlock_);
            }
            if (this.linkOneofCase_ == 3) {
                h += CodedOutputStream.h(3, (ByteString) this.linkOneof_);
            }
            if (this.linkOneofCase_ == 4) {
                h += GeneratedMessageV3.computeStringSize(4, this.linkOneof_);
            }
            if (!getRepresentativeBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(5, this.representative_);
            }
            if (!getBalanceBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(6, this.balance_);
            }
            if (!getWorkBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(7, this.work_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public String getWork() {
            Object obj = this.work_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.work_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nano.SigningInputOrBuilder
        public ByteString getWorkBytes() {
            Object obj = this.work_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.work_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPrivateKey().hashCode()) * 37) + 2) * 53) + getParentBlock().hashCode()) * 37) + 5) * 53) + getRepresentative().hashCode()) * 37) + 6) * 53) + getBalance().hashCode()) * 37) + 7) * 53) + getWork().hashCode();
            int i3 = this.linkOneofCase_;
            if (i3 == 3) {
                i = ((hashCode2 * 37) + 3) * 53;
                hashCode = getLinkBlock().hashCode();
            } else {
                if (i3 == 4) {
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getLinkRecipient().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Nano.internal_static_TW_Nano_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(1, this.privateKey_);
            }
            if (!this.parentBlock_.isEmpty()) {
                codedOutputStream.q0(2, this.parentBlock_);
            }
            if (this.linkOneofCase_ == 3) {
                codedOutputStream.q0(3, (ByteString) this.linkOneof_);
            }
            if (this.linkOneofCase_ == 4) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.linkOneof_);
            }
            if (!getRepresentativeBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.representative_);
            }
            if (!getBalanceBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 6, this.balance_);
            }
            if (!getWorkBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 7, this.work_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.linkOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private SigningInput() {
            this.linkOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.privateKey_ = byteString;
            this.parentBlock_ = byteString;
            this.representative_ = "";
            this.balance_ = "";
            this.work_ = "";
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.privateKey_ = jVar.q();
                                } else if (J == 18) {
                                    this.parentBlock_ = jVar.q();
                                } else if (J == 26) {
                                    this.linkOneofCase_ = 3;
                                    this.linkOneof_ = jVar.q();
                                } else if (J == 34) {
                                    String I = jVar.I();
                                    this.linkOneofCase_ = 4;
                                    this.linkOneof_ = I;
                                } else if (J == 42) {
                                    this.representative_ = jVar.I();
                                } else if (J == 50) {
                                    this.balance_ = jVar.I();
                                } else if (J != 58) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.work_ = jVar.I();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getBalance();

        ByteString getBalanceBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        ByteString getLinkBlock();

        SigningInput.LinkOneofCase getLinkOneofCase();

        String getLinkRecipient();

        ByteString getLinkRecipientBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getParentBlock();

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getRepresentative();

        ByteString getRepresentativeBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        String getWork();

        ByteString getWorkBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int BLOCK_HASH_FIELD_NUMBER = 2;
        public static final int JSON_FIELD_NUMBER = 3;
        public static final int SIGNATURE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString blockHash_;
        private volatile Object json_;
        private byte memoizedIsInitialized;
        private ByteString signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Nano.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString blockHash_;
            private Object json_;
            private ByteString signature_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Nano.internal_static_TW_Nano_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearBlockHash() {
                this.blockHash_ = SigningOutput.getDefaultInstance().getBlockHash();
                onChanged();
                return this;
            }

            public Builder clearJson() {
                this.json_ = SigningOutput.getDefaultInstance().getJson();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
            public ByteString getBlockHash() {
                return this.blockHash_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Nano.internal_static_TW_Nano_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
            public String getJson() {
                Object obj = this.json_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.json_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
            public ByteString getJsonBytes() {
                Object obj = this.json_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.json_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Nano.internal_static_TW_Nano_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setBlockHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.blockHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setJson(String str) {
                Objects.requireNonNull(str);
                this.json_ = str;
                onChanged();
                return this;
            }

            public Builder setJsonBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.json_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.signature_ = byteString;
                this.blockHash_ = byteString;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.signature_ = this.signature_;
                signingOutput.blockHash_ = this.blockHash_;
                signingOutput.json_ = this.json_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.signature_ = byteString;
                this.blockHash_ = byteString;
                this.json_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.signature_ = byteString;
                this.blockHash_ = byteString;
                this.json_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                ByteString signature = signingOutput.getSignature();
                ByteString byteString = ByteString.EMPTY;
                if (signature != byteString) {
                    setSignature(signingOutput.getSignature());
                }
                if (signingOutput.getBlockHash() != byteString) {
                    setBlockHash(signingOutput.getBlockHash());
                }
                if (!signingOutput.getJson().isEmpty()) {
                    this.json_ = signingOutput.json_;
                    onChanged();
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Nano.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Nano.SigningOutput.access$3000()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Nano$SigningOutput r3 = (wallet.core.jni.proto.Nano.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Nano$SigningOutput r4 = (wallet.core.jni.proto.Nano.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Nano.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Nano$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Nano.internal_static_TW_Nano_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getSignature().equals(signingOutput.getSignature()) && getBlockHash().equals(signingOutput.getBlockHash()) && getJson().equals(signingOutput.getJson()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
        public ByteString getBlockHash() {
            return this.blockHash_;
        }

        @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
        public String getJson() {
            Object obj = this.json_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.json_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
        public ByteString getJsonBytes() {
            Object obj = this.json_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.json_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.signature_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.signature_);
            if (!this.blockHash_.isEmpty()) {
                h += CodedOutputStream.h(2, this.blockHash_);
            }
            if (!getJsonBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(3, this.json_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Nano.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSignature().hashCode()) * 37) + 2) * 53) + getBlockHash().hashCode()) * 37) + 3) * 53) + getJson().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Nano.internal_static_TW_Nano_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(1, this.signature_);
            }
            if (!this.blockHash_.isEmpty()) {
                codedOutputStream.q0(2, this.blockHash_);
            }
            if (!getJsonBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.json_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.signature_ = byteString;
            this.blockHash_ = byteString;
            this.json_ = "";
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.signature_ = jVar.q();
                            } else if (J == 18) {
                                this.blockHash_ = jVar.q();
                            } else if (J != 26) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.json_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getBlockHash();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        String getJson();

        ByteString getJsonBytes();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Nano_Proto_SigningInput_descriptor = bVar;
        internal_static_TW_Nano_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"PrivateKey", "ParentBlock", "LinkBlock", "LinkRecipient", "Representative", "Balance", "Work", "LinkOneof"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Nano_Proto_SigningOutput_descriptor = bVar2;
        internal_static_TW_Nano_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Signature", "BlockHash", "Json"});
    }

    private Nano() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
