package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.v0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Tezos {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000bTezos.proto\u0012\u000eTW.Tezos.Proto\"Z\n\fSigningInput\u00125\n\u000eoperation_list\u0018\u0001 \u0001(\u000b2\u001d.TW.Tezos.Proto.OperationList\u0012\u0013\n\u000bprivate_key\u0018\u0002 \u0001(\f\" \n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f\"N\n\rOperationList\u0012\u000e\n\u0006branch\u0018\u0001 \u0001(\t\u0012-\n\noperations\u0018\u0002 \u0003(\u000b2\u0019.TW.Tezos.Proto.Operation\"ß\u0003\n\tOperation\u0012\u000f\n\u0007counter\u0018\u0001 \u0001(\u0003\u0012\u000e\n\u0006source\u0018\u0002 \u0001(\t\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0003\u0012\u0011\n\tgas_limit\u0018\u0004 \u0001(\u0003\u0012\u0015\n\rstorage_limit\u0018\u0005 \u0001(\u0003\u00125\n\u0004kind\u0018\u0007 \u0001(\u000e2'.TW.Tezos.Proto.Operation.OperationKind\u0012D\n\u0015reveal_operation_data\u0018\b \u0001(\u000b2#.TW.Tezos.Proto.RevealOperationDataH\u0000\u0012N\n\u001atransaction_operation_data\u0018\t \u0001(\u000b2(.TW.Tezos.Proto.TransactionOperationDataH\u0000\u0012L\n\u0019delegation_operation_data\u0018\u000b \u0001(\u000b2'.TW.Tezos.Proto.DelegationOperationDataH\u0000\"M\n\rOperationKind\u0012\u000f\n\u000bENDORSEMENT\u0010\u0000\u0012\n\n\u0006REVEAL\u0010k\u0012\u000f\n\u000bTRANSACTION\u0010l\u0012\u000e\n\nDELEGATION\u0010nB\u0010\n\u000eoperation_data\"?\n\u0018TransactionOperationData\u0012\u0013\n\u000bdestination\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\u0003\")\n\u0013RevealOperationData\u0012\u0012\n\npublic_key\u0018\u0001 \u0001(\f\"+\n\u0017DelegationOperationData\u0012\u0010\n\bdelegate\u0018\u0001 \u0001(\tB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Tezos_Proto_DelegationOperationData_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tezos_Proto_DelegationOperationData_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tezos_Proto_OperationList_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tezos_Proto_OperationList_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tezos_Proto_Operation_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tezos_Proto_Operation_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tezos_Proto_RevealOperationData_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tezos_Proto_RevealOperationData_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tezos_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tezos_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tezos_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tezos_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Tezos_Proto_TransactionOperationData_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Tezos_Proto_TransactionOperationData_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Tezos$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Tezos$Operation$OperationDataCase;

        static {
            int[] iArr = new int[Operation.OperationDataCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Tezos$Operation$OperationDataCase = iArr;
            try {
                iArr[Operation.OperationDataCase.REVEAL_OPERATION_DATA.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tezos$Operation$OperationDataCase[Operation.OperationDataCase.TRANSACTION_OPERATION_DATA.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tezos$Operation$OperationDataCase[Operation.OperationDataCase.DELEGATION_OPERATION_DATA.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Tezos$Operation$OperationDataCase[Operation.OperationDataCase.OPERATIONDATA_NOT_SET.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class DelegationOperationData extends GeneratedMessageV3 implements DelegationOperationDataOrBuilder {
        public static final int DELEGATE_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private volatile Object delegate_;
        private byte memoizedIsInitialized;
        private static final DelegationOperationData DEFAULT_INSTANCE = new DelegationOperationData();
        private static final t0<DelegationOperationData> PARSER = new c<DelegationOperationData>() { // from class: wallet.core.jni.proto.Tezos.DelegationOperationData.1
            @Override // com.google.protobuf.t0
            public DelegationOperationData parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new DelegationOperationData(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements DelegationOperationDataOrBuilder {
            private Object delegate_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tezos.internal_static_TW_Tezos_Proto_DelegationOperationData_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearDelegate() {
                this.delegate_ = DelegationOperationData.getDefaultInstance().getDelegate();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Tezos.DelegationOperationDataOrBuilder
            public String getDelegate() {
                Object obj = this.delegate_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.delegate_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tezos.DelegationOperationDataOrBuilder
            public ByteString getDelegateBytes() {
                Object obj = this.delegate_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.delegate_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tezos.internal_static_TW_Tezos_Proto_DelegationOperationData_descriptor;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tezos.internal_static_TW_Tezos_Proto_DelegationOperationData_fieldAccessorTable.d(DelegationOperationData.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setDelegate(String str) {
                Objects.requireNonNull(str);
                this.delegate_ = str;
                onChanged();
                return this;
            }

            public Builder setDelegateBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.delegate_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.delegate_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DelegationOperationData build() {
                DelegationOperationData buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public DelegationOperationData buildPartial() {
                DelegationOperationData delegationOperationData = new DelegationOperationData(this, (AnonymousClass1) null);
                delegationOperationData.delegate_ = this.delegate_;
                onBuilt();
                return delegationOperationData;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public DelegationOperationData getDefaultInstanceForType() {
                return DelegationOperationData.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.delegate_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.delegate_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof DelegationOperationData) {
                    return mergeFrom((DelegationOperationData) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(DelegationOperationData delegationOperationData) {
                if (delegationOperationData == DelegationOperationData.getDefaultInstance()) {
                    return this;
                }
                if (!delegationOperationData.getDelegate().isEmpty()) {
                    this.delegate_ = delegationOperationData.delegate_;
                    onChanged();
                }
                mergeUnknownFields(delegationOperationData.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tezos.DelegationOperationData.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tezos.DelegationOperationData.access$8200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tezos$DelegationOperationData r3 = (wallet.core.jni.proto.Tezos.DelegationOperationData) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tezos$DelegationOperationData r4 = (wallet.core.jni.proto.Tezos.DelegationOperationData) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tezos.DelegationOperationData.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tezos$DelegationOperationData$Builder");
            }
        }

        public /* synthetic */ DelegationOperationData(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static DelegationOperationData getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tezos.internal_static_TW_Tezos_Proto_DelegationOperationData_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static DelegationOperationData parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DelegationOperationData) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static DelegationOperationData parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<DelegationOperationData> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof DelegationOperationData)) {
                return super.equals(obj);
            }
            DelegationOperationData delegationOperationData = (DelegationOperationData) obj;
            return getDelegate().equals(delegationOperationData.getDelegate()) && this.unknownFields.equals(delegationOperationData.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tezos.DelegationOperationDataOrBuilder
        public String getDelegate() {
            Object obj = this.delegate_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.delegate_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tezos.DelegationOperationDataOrBuilder
        public ByteString getDelegateBytes() {
            Object obj = this.delegate_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.delegate_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<DelegationOperationData> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = (getDelegateBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.delegate_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = computeStringSize;
            return computeStringSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDelegate().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tezos.internal_static_TW_Tezos_Proto_DelegationOperationData_fieldAccessorTable.d(DelegationOperationData.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new DelegationOperationData();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDelegateBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.delegate_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ DelegationOperationData(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(DelegationOperationData delegationOperationData) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(delegationOperationData);
        }

        public static DelegationOperationData parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private DelegationOperationData(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static DelegationOperationData parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (DelegationOperationData) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static DelegationOperationData parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public DelegationOperationData getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static DelegationOperationData parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private DelegationOperationData() {
            this.memoizedIsInitialized = (byte) -1;
            this.delegate_ = "";
        }

        public static DelegationOperationData parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static DelegationOperationData parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static DelegationOperationData parseFrom(InputStream inputStream) throws IOException {
            return (DelegationOperationData) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private DelegationOperationData(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.delegate_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static DelegationOperationData parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (DelegationOperationData) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static DelegationOperationData parseFrom(j jVar) throws IOException {
            return (DelegationOperationData) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static DelegationOperationData parseFrom(j jVar, r rVar) throws IOException {
            return (DelegationOperationData) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface DelegationOperationDataOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        String getDelegate();

        ByteString getDelegateBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Operation extends GeneratedMessageV3 implements OperationOrBuilder {
        public static final int COUNTER_FIELD_NUMBER = 1;
        public static final int DELEGATION_OPERATION_DATA_FIELD_NUMBER = 11;
        public static final int FEE_FIELD_NUMBER = 3;
        public static final int GAS_LIMIT_FIELD_NUMBER = 4;
        public static final int KIND_FIELD_NUMBER = 7;
        public static final int REVEAL_OPERATION_DATA_FIELD_NUMBER = 8;
        public static final int SOURCE_FIELD_NUMBER = 2;
        public static final int STORAGE_LIMIT_FIELD_NUMBER = 5;
        public static final int TRANSACTION_OPERATION_DATA_FIELD_NUMBER = 9;
        private static final long serialVersionUID = 0;
        private long counter_;
        private long fee_;
        private long gasLimit_;
        private int kind_;
        private byte memoizedIsInitialized;
        private int operationDataCase_;
        private Object operationData_;
        private volatile Object source_;
        private long storageLimit_;
        private static final Operation DEFAULT_INSTANCE = new Operation();
        private static final t0<Operation> PARSER = new c<Operation>() { // from class: wallet.core.jni.proto.Tezos.Operation.1
            @Override // com.google.protobuf.t0
            public Operation parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Operation(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements OperationOrBuilder {
            private long counter_;
            private a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> delegationOperationDataBuilder_;
            private long fee_;
            private long gasLimit_;
            private int kind_;
            private int operationDataCase_;
            private Object operationData_;
            private a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> revealOperationDataBuilder_;
            private Object source_;
            private long storageLimit_;
            private a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> transactionOperationDataBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> getDelegationOperationDataFieldBuilder() {
                if (this.delegationOperationDataBuilder_ == null) {
                    if (this.operationDataCase_ != 11) {
                        this.operationData_ = DelegationOperationData.getDefaultInstance();
                    }
                    this.delegationOperationDataBuilder_ = new a1<>((DelegationOperationData) this.operationData_, getParentForChildren(), isClean());
                    this.operationData_ = null;
                }
                this.operationDataCase_ = 11;
                onChanged();
                return this.delegationOperationDataBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Tezos.internal_static_TW_Tezos_Proto_Operation_descriptor;
            }

            private a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> getRevealOperationDataFieldBuilder() {
                if (this.revealOperationDataBuilder_ == null) {
                    if (this.operationDataCase_ != 8) {
                        this.operationData_ = RevealOperationData.getDefaultInstance();
                    }
                    this.revealOperationDataBuilder_ = new a1<>((RevealOperationData) this.operationData_, getParentForChildren(), isClean());
                    this.operationData_ = null;
                }
                this.operationDataCase_ = 8;
                onChanged();
                return this.revealOperationDataBuilder_;
            }

            private a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> getTransactionOperationDataFieldBuilder() {
                if (this.transactionOperationDataBuilder_ == null) {
                    if (this.operationDataCase_ != 9) {
                        this.operationData_ = TransactionOperationData.getDefaultInstance();
                    }
                    this.transactionOperationDataBuilder_ = new a1<>((TransactionOperationData) this.operationData_, getParentForChildren(), isClean());
                    this.operationData_ = null;
                }
                this.operationDataCase_ = 9;
                onChanged();
                return this.transactionOperationDataBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearCounter() {
                this.counter_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearDelegationOperationData() {
                a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> a1Var = this.delegationOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 11) {
                        this.operationDataCase_ = 0;
                        this.operationData_ = null;
                        onChanged();
                    }
                } else {
                    if (this.operationDataCase_ == 11) {
                        this.operationDataCase_ = 0;
                        this.operationData_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearKind() {
                this.kind_ = 0;
                onChanged();
                return this;
            }

            public Builder clearOperationData() {
                this.operationDataCase_ = 0;
                this.operationData_ = null;
                onChanged();
                return this;
            }

            public Builder clearRevealOperationData() {
                a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> a1Var = this.revealOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 8) {
                        this.operationDataCase_ = 0;
                        this.operationData_ = null;
                        onChanged();
                    }
                } else {
                    if (this.operationDataCase_ == 8) {
                        this.operationDataCase_ = 0;
                        this.operationData_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearSource() {
                this.source_ = Operation.getDefaultInstance().getSource();
                onChanged();
                return this;
            }

            public Builder clearStorageLimit() {
                this.storageLimit_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearTransactionOperationData() {
                a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> a1Var = this.transactionOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 9) {
                        this.operationDataCase_ = 0;
                        this.operationData_ = null;
                        onChanged();
                    }
                } else {
                    if (this.operationDataCase_ == 9) {
                        this.operationDataCase_ = 0;
                        this.operationData_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public long getCounter() {
                return this.counter_;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public DelegationOperationData getDelegationOperationData() {
                a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> a1Var = this.delegationOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 11) {
                        return (DelegationOperationData) this.operationData_;
                    }
                    return DelegationOperationData.getDefaultInstance();
                } else if (this.operationDataCase_ == 11) {
                    return a1Var.f();
                } else {
                    return DelegationOperationData.getDefaultInstance();
                }
            }

            public DelegationOperationData.Builder getDelegationOperationDataBuilder() {
                return getDelegationOperationDataFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public DelegationOperationDataOrBuilder getDelegationOperationDataOrBuilder() {
                a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> a1Var;
                int i = this.operationDataCase_;
                if (i != 11 || (a1Var = this.delegationOperationDataBuilder_) == null) {
                    if (i == 11) {
                        return (DelegationOperationData) this.operationData_;
                    }
                    return DelegationOperationData.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tezos.internal_static_TW_Tezos_Proto_Operation_descriptor;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public long getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public OperationKind getKind() {
                OperationKind valueOf = OperationKind.valueOf(this.kind_);
                return valueOf == null ? OperationKind.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public int getKindValue() {
                return this.kind_;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public OperationDataCase getOperationDataCase() {
                return OperationDataCase.forNumber(this.operationDataCase_);
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public RevealOperationData getRevealOperationData() {
                a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> a1Var = this.revealOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 8) {
                        return (RevealOperationData) this.operationData_;
                    }
                    return RevealOperationData.getDefaultInstance();
                } else if (this.operationDataCase_ == 8) {
                    return a1Var.f();
                } else {
                    return RevealOperationData.getDefaultInstance();
                }
            }

            public RevealOperationData.Builder getRevealOperationDataBuilder() {
                return getRevealOperationDataFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public RevealOperationDataOrBuilder getRevealOperationDataOrBuilder() {
                a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> a1Var;
                int i = this.operationDataCase_;
                if (i != 8 || (a1Var = this.revealOperationDataBuilder_) == null) {
                    if (i == 8) {
                        return (RevealOperationData) this.operationData_;
                    }
                    return RevealOperationData.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public String getSource() {
                Object obj = this.source_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.source_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public ByteString getSourceBytes() {
                Object obj = this.source_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.source_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public long getStorageLimit() {
                return this.storageLimit_;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public TransactionOperationData getTransactionOperationData() {
                a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> a1Var = this.transactionOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 9) {
                        return (TransactionOperationData) this.operationData_;
                    }
                    return TransactionOperationData.getDefaultInstance();
                } else if (this.operationDataCase_ == 9) {
                    return a1Var.f();
                } else {
                    return TransactionOperationData.getDefaultInstance();
                }
            }

            public TransactionOperationData.Builder getTransactionOperationDataBuilder() {
                return getTransactionOperationDataFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public TransactionOperationDataOrBuilder getTransactionOperationDataOrBuilder() {
                a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> a1Var;
                int i = this.operationDataCase_;
                if (i != 9 || (a1Var = this.transactionOperationDataBuilder_) == null) {
                    if (i == 9) {
                        return (TransactionOperationData) this.operationData_;
                    }
                    return TransactionOperationData.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public boolean hasDelegationOperationData() {
                return this.operationDataCase_ == 11;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public boolean hasRevealOperationData() {
                return this.operationDataCase_ == 8;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
            public boolean hasTransactionOperationData() {
                return this.operationDataCase_ == 9;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tezos.internal_static_TW_Tezos_Proto_Operation_fieldAccessorTable.d(Operation.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeDelegationOperationData(DelegationOperationData delegationOperationData) {
                a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> a1Var = this.delegationOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 11 && this.operationData_ != DelegationOperationData.getDefaultInstance()) {
                        this.operationData_ = DelegationOperationData.newBuilder((DelegationOperationData) this.operationData_).mergeFrom(delegationOperationData).buildPartial();
                    } else {
                        this.operationData_ = delegationOperationData;
                    }
                    onChanged();
                } else {
                    if (this.operationDataCase_ == 11) {
                        a1Var.h(delegationOperationData);
                    }
                    this.delegationOperationDataBuilder_.j(delegationOperationData);
                }
                this.operationDataCase_ = 11;
                return this;
            }

            public Builder mergeRevealOperationData(RevealOperationData revealOperationData) {
                a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> a1Var = this.revealOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 8 && this.operationData_ != RevealOperationData.getDefaultInstance()) {
                        this.operationData_ = RevealOperationData.newBuilder((RevealOperationData) this.operationData_).mergeFrom(revealOperationData).buildPartial();
                    } else {
                        this.operationData_ = revealOperationData;
                    }
                    onChanged();
                } else {
                    if (this.operationDataCase_ == 8) {
                        a1Var.h(revealOperationData);
                    }
                    this.revealOperationDataBuilder_.j(revealOperationData);
                }
                this.operationDataCase_ = 8;
                return this;
            }

            public Builder mergeTransactionOperationData(TransactionOperationData transactionOperationData) {
                a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> a1Var = this.transactionOperationDataBuilder_;
                if (a1Var == null) {
                    if (this.operationDataCase_ == 9 && this.operationData_ != TransactionOperationData.getDefaultInstance()) {
                        this.operationData_ = TransactionOperationData.newBuilder((TransactionOperationData) this.operationData_).mergeFrom(transactionOperationData).buildPartial();
                    } else {
                        this.operationData_ = transactionOperationData;
                    }
                    onChanged();
                } else {
                    if (this.operationDataCase_ == 9) {
                        a1Var.h(transactionOperationData);
                    }
                    this.transactionOperationDataBuilder_.j(transactionOperationData);
                }
                this.operationDataCase_ = 9;
                return this;
            }

            public Builder setCounter(long j) {
                this.counter_ = j;
                onChanged();
                return this;
            }

            public Builder setDelegationOperationData(DelegationOperationData delegationOperationData) {
                a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> a1Var = this.delegationOperationDataBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(delegationOperationData);
                    this.operationData_ = delegationOperationData;
                    onChanged();
                } else {
                    a1Var.j(delegationOperationData);
                }
                this.operationDataCase_ = 11;
                return this;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setGasLimit(long j) {
                this.gasLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setKind(OperationKind operationKind) {
                Objects.requireNonNull(operationKind);
                this.kind_ = operationKind.getNumber();
                onChanged();
                return this;
            }

            public Builder setKindValue(int i) {
                this.kind_ = i;
                onChanged();
                return this;
            }

            public Builder setRevealOperationData(RevealOperationData revealOperationData) {
                a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> a1Var = this.revealOperationDataBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(revealOperationData);
                    this.operationData_ = revealOperationData;
                    onChanged();
                } else {
                    a1Var.j(revealOperationData);
                }
                this.operationDataCase_ = 8;
                return this;
            }

            public Builder setSource(String str) {
                Objects.requireNonNull(str);
                this.source_ = str;
                onChanged();
                return this;
            }

            public Builder setSourceBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.source_ = byteString;
                onChanged();
                return this;
            }

            public Builder setStorageLimit(long j) {
                this.storageLimit_ = j;
                onChanged();
                return this;
            }

            public Builder setTransactionOperationData(TransactionOperationData transactionOperationData) {
                a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> a1Var = this.transactionOperationDataBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionOperationData);
                    this.operationData_ = transactionOperationData;
                    onChanged();
                } else {
                    a1Var.j(transactionOperationData);
                }
                this.operationDataCase_ = 9;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.operationDataCase_ = 0;
                this.source_ = "";
                this.kind_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Operation build() {
                Operation buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Operation buildPartial() {
                Operation operation = new Operation(this, (AnonymousClass1) null);
                operation.counter_ = this.counter_;
                operation.source_ = this.source_;
                operation.fee_ = this.fee_;
                operation.gasLimit_ = this.gasLimit_;
                operation.storageLimit_ = this.storageLimit_;
                operation.kind_ = this.kind_;
                if (this.operationDataCase_ == 8) {
                    a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> a1Var = this.revealOperationDataBuilder_;
                    if (a1Var == null) {
                        operation.operationData_ = this.operationData_;
                    } else {
                        operation.operationData_ = a1Var.b();
                    }
                }
                if (this.operationDataCase_ == 9) {
                    a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> a1Var2 = this.transactionOperationDataBuilder_;
                    if (a1Var2 == null) {
                        operation.operationData_ = this.operationData_;
                    } else {
                        operation.operationData_ = a1Var2.b();
                    }
                }
                if (this.operationDataCase_ == 11) {
                    a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> a1Var3 = this.delegationOperationDataBuilder_;
                    if (a1Var3 == null) {
                        operation.operationData_ = this.operationData_;
                    } else {
                        operation.operationData_ = a1Var3.b();
                    }
                }
                operation.operationDataCase_ = this.operationDataCase_;
                onBuilt();
                return operation;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Operation getDefaultInstanceForType() {
                return Operation.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.counter_ = 0L;
                this.source_ = "";
                this.fee_ = 0L;
                this.gasLimit_ = 0L;
                this.storageLimit_ = 0L;
                this.kind_ = 0;
                this.operationDataCase_ = 0;
                this.operationData_ = null;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Operation) {
                    return mergeFrom((Operation) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setDelegationOperationData(DelegationOperationData.Builder builder) {
                a1<DelegationOperationData, DelegationOperationData.Builder, DelegationOperationDataOrBuilder> a1Var = this.delegationOperationDataBuilder_;
                if (a1Var == null) {
                    this.operationData_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.operationDataCase_ = 11;
                return this;
            }

            public Builder setRevealOperationData(RevealOperationData.Builder builder) {
                a1<RevealOperationData, RevealOperationData.Builder, RevealOperationDataOrBuilder> a1Var = this.revealOperationDataBuilder_;
                if (a1Var == null) {
                    this.operationData_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.operationDataCase_ = 8;
                return this;
            }

            public Builder setTransactionOperationData(TransactionOperationData.Builder builder) {
                a1<TransactionOperationData, TransactionOperationData.Builder, TransactionOperationDataOrBuilder> a1Var = this.transactionOperationDataBuilder_;
                if (a1Var == null) {
                    this.operationData_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.operationDataCase_ = 9;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.operationDataCase_ = 0;
                this.source_ = "";
                this.kind_ = 0;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(Operation operation) {
                if (operation == Operation.getDefaultInstance()) {
                    return this;
                }
                if (operation.getCounter() != 0) {
                    setCounter(operation.getCounter());
                }
                if (!operation.getSource().isEmpty()) {
                    this.source_ = operation.source_;
                    onChanged();
                }
                if (operation.getFee() != 0) {
                    setFee(operation.getFee());
                }
                if (operation.getGasLimit() != 0) {
                    setGasLimit(operation.getGasLimit());
                }
                if (operation.getStorageLimit() != 0) {
                    setStorageLimit(operation.getStorageLimit());
                }
                if (operation.kind_ != 0) {
                    setKindValue(operation.getKindValue());
                }
                int i = AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Tezos$Operation$OperationDataCase[operation.getOperationDataCase().ordinal()];
                if (i == 1) {
                    mergeRevealOperationData(operation.getRevealOperationData());
                } else if (i == 2) {
                    mergeTransactionOperationData(operation.getTransactionOperationData());
                } else if (i == 3) {
                    mergeDelegationOperationData(operation.getDelegationOperationData());
                }
                mergeUnknownFields(operation.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tezos.Operation.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tezos.Operation.access$4900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tezos$Operation r3 = (wallet.core.jni.proto.Tezos.Operation) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tezos$Operation r4 = (wallet.core.jni.proto.Tezos.Operation) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tezos.Operation.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tezos$Operation$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public enum OperationDataCase implements a0.c {
            REVEAL_OPERATION_DATA(8),
            TRANSACTION_OPERATION_DATA(9),
            DELEGATION_OPERATION_DATA(11),
            OPERATIONDATA_NOT_SET(0);
            
            private final int value;

            OperationDataCase(int i) {
                this.value = i;
            }

            public static OperationDataCase forNumber(int i) {
                if (i != 0) {
                    if (i != 11) {
                        if (i != 8) {
                            if (i != 9) {
                                return null;
                            }
                            return TRANSACTION_OPERATION_DATA;
                        }
                        return REVEAL_OPERATION_DATA;
                    }
                    return DELEGATION_OPERATION_DATA;
                }
                return OPERATIONDATA_NOT_SET;
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static OperationDataCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public enum OperationKind implements v0 {
            ENDORSEMENT(0),
            REVEAL(107),
            TRANSACTION(108),
            DELEGATION(110),
            UNRECOGNIZED(-1);
            
            public static final int DELEGATION_VALUE = 110;
            public static final int ENDORSEMENT_VALUE = 0;
            public static final int REVEAL_VALUE = 107;
            public static final int TRANSACTION_VALUE = 108;
            private final int value;
            private static final a0.d<OperationKind> internalValueMap = new a0.d<OperationKind>() { // from class: wallet.core.jni.proto.Tezos.Operation.OperationKind.1
                @Override // com.google.protobuf.a0.d
                public OperationKind findValueByNumber(int i) {
                    return OperationKind.forNumber(i);
                }
            };
            private static final OperationKind[] VALUES = values();

            OperationKind(int i) {
                this.value = i;
            }

            public static OperationKind forNumber(int i) {
                if (i != 0) {
                    if (i != 110) {
                        if (i != 107) {
                            if (i != 108) {
                                return null;
                            }
                            return TRANSACTION;
                        }
                        return REVEAL;
                    }
                    return DELEGATION;
                }
                return ENDORSEMENT;
            }

            public static final Descriptors.c getDescriptor() {
                return Operation.getDescriptor().l().get(0);
            }

            public static a0.d<OperationKind> internalGetValueMap() {
                return internalValueMap;
            }

            public final Descriptors.c getDescriptorForType() {
                return getDescriptor();
            }

            @Override // com.google.protobuf.a0.c
            public final int getNumber() {
                if (this != UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }

            public final Descriptors.d getValueDescriptor() {
                if (this != UNRECOGNIZED) {
                    return getDescriptor().k().get(ordinal());
                }
                throw new IllegalStateException("Can't get the descriptor of an unrecognized enum value.");
            }

            @Deprecated
            public static OperationKind valueOf(int i) {
                return forNumber(i);
            }

            public static OperationKind valueOf(Descriptors.d dVar) {
                if (dVar.h() == getDescriptor()) {
                    if (dVar.g() == -1) {
                        return UNRECOGNIZED;
                    }
                    return VALUES[dVar.g()];
                }
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            }
        }

        public /* synthetic */ Operation(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Operation getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tezos.internal_static_TW_Tezos_Proto_Operation_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Operation parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Operation) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Operation parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Operation> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Operation)) {
                return super.equals(obj);
            }
            Operation operation = (Operation) obj;
            if (getCounter() == operation.getCounter() && getSource().equals(operation.getSource()) && getFee() == operation.getFee() && getGasLimit() == operation.getGasLimit() && getStorageLimit() == operation.getStorageLimit() && this.kind_ == operation.kind_ && getOperationDataCase().equals(operation.getOperationDataCase())) {
                int i = this.operationDataCase_;
                if (i != 8) {
                    if (i != 9) {
                        if (i == 11 && !getDelegationOperationData().equals(operation.getDelegationOperationData())) {
                            return false;
                        }
                    } else if (!getTransactionOperationData().equals(operation.getTransactionOperationData())) {
                        return false;
                    }
                } else if (!getRevealOperationData().equals(operation.getRevealOperationData())) {
                    return false;
                }
                return this.unknownFields.equals(operation.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public long getCounter() {
            return this.counter_;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public DelegationOperationData getDelegationOperationData() {
            if (this.operationDataCase_ == 11) {
                return (DelegationOperationData) this.operationData_;
            }
            return DelegationOperationData.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public DelegationOperationDataOrBuilder getDelegationOperationDataOrBuilder() {
            if (this.operationDataCase_ == 11) {
                return (DelegationOperationData) this.operationData_;
            }
            return DelegationOperationData.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public long getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public OperationKind getKind() {
            OperationKind valueOf = OperationKind.valueOf(this.kind_);
            return valueOf == null ? OperationKind.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public int getKindValue() {
            return this.kind_;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public OperationDataCase getOperationDataCase() {
            return OperationDataCase.forNumber(this.operationDataCase_);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Operation> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public RevealOperationData getRevealOperationData() {
            if (this.operationDataCase_ == 8) {
                return (RevealOperationData) this.operationData_;
            }
            return RevealOperationData.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public RevealOperationDataOrBuilder getRevealOperationDataOrBuilder() {
            if (this.operationDataCase_ == 8) {
                return (RevealOperationData) this.operationData_;
            }
            return RevealOperationData.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.counter_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            if (!getSourceBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(2, this.source_);
            }
            long j2 = this.fee_;
            if (j2 != 0) {
                z += CodedOutputStream.z(3, j2);
            }
            long j3 = this.gasLimit_;
            if (j3 != 0) {
                z += CodedOutputStream.z(4, j3);
            }
            long j4 = this.storageLimit_;
            if (j4 != 0) {
                z += CodedOutputStream.z(5, j4);
            }
            if (this.kind_ != OperationKind.ENDORSEMENT.getNumber()) {
                z += CodedOutputStream.l(7, this.kind_);
            }
            if (this.operationDataCase_ == 8) {
                z += CodedOutputStream.G(8, (RevealOperationData) this.operationData_);
            }
            if (this.operationDataCase_ == 9) {
                z += CodedOutputStream.G(9, (TransactionOperationData) this.operationData_);
            }
            if (this.operationDataCase_ == 11) {
                z += CodedOutputStream.G(11, (DelegationOperationData) this.operationData_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public String getSource() {
            Object obj = this.source_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.source_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public ByteString getSourceBytes() {
            Object obj = this.source_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.source_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public long getStorageLimit() {
            return this.storageLimit_;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public TransactionOperationData getTransactionOperationData() {
            if (this.operationDataCase_ == 9) {
                return (TransactionOperationData) this.operationData_;
            }
            return TransactionOperationData.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public TransactionOperationDataOrBuilder getTransactionOperationDataOrBuilder() {
            if (this.operationDataCase_ == 9) {
                return (TransactionOperationData) this.operationData_;
            }
            return TransactionOperationData.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public boolean hasDelegationOperationData() {
            return this.operationDataCase_ == 11;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public boolean hasRevealOperationData() {
            return this.operationDataCase_ == 8;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationOrBuilder
        public boolean hasTransactionOperationData() {
            return this.operationDataCase_ == 9;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = ((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getCounter())) * 37) + 2) * 53) + getSource().hashCode()) * 37) + 3) * 53) + a0.h(getFee())) * 37) + 4) * 53) + a0.h(getGasLimit())) * 37) + 5) * 53) + a0.h(getStorageLimit())) * 37) + 7) * 53) + this.kind_;
            int i3 = this.operationDataCase_;
            if (i3 == 8) {
                i = ((hashCode2 * 37) + 8) * 53;
                hashCode = getRevealOperationData().hashCode();
            } else if (i3 == 9) {
                i = ((hashCode2 * 37) + 9) * 53;
                hashCode = getTransactionOperationData().hashCode();
            } else {
                if (i3 == 11) {
                    i = ((hashCode2 * 37) + 11) * 53;
                    hashCode = getDelegationOperationData().hashCode();
                }
                int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode3;
                return hashCode3;
            }
            hashCode2 = i + hashCode;
            int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode32;
            return hashCode32;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tezos.internal_static_TW_Tezos_Proto_Operation_fieldAccessorTable.d(Operation.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Operation();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.counter_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            if (!getSourceBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.source_);
            }
            long j2 = this.fee_;
            if (j2 != 0) {
                codedOutputStream.I0(3, j2);
            }
            long j3 = this.gasLimit_;
            if (j3 != 0) {
                codedOutputStream.I0(4, j3);
            }
            long j4 = this.storageLimit_;
            if (j4 != 0) {
                codedOutputStream.I0(5, j4);
            }
            if (this.kind_ != OperationKind.ENDORSEMENT.getNumber()) {
                codedOutputStream.u0(7, this.kind_);
            }
            if (this.operationDataCase_ == 8) {
                codedOutputStream.K0(8, (RevealOperationData) this.operationData_);
            }
            if (this.operationDataCase_ == 9) {
                codedOutputStream.K0(9, (TransactionOperationData) this.operationData_);
            }
            if (this.operationDataCase_ == 11) {
                codedOutputStream.K0(11, (DelegationOperationData) this.operationData_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Operation(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Operation operation) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(operation);
        }

        public static Operation parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Operation(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.operationDataCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Operation parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Operation) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Operation parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Operation getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Operation parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Operation parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Operation() {
            this.operationDataCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
            this.source_ = "";
            this.kind_ = 0;
        }

        public static Operation parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Operation parseFrom(InputStream inputStream) throws IOException {
            return (Operation) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static Operation parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Operation) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private Operation(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 8) {
                                    this.counter_ = jVar.y();
                                } else if (J == 18) {
                                    this.source_ = jVar.I();
                                } else if (J == 24) {
                                    this.fee_ = jVar.y();
                                } else if (J == 32) {
                                    this.gasLimit_ = jVar.y();
                                } else if (J == 40) {
                                    this.storageLimit_ = jVar.y();
                                } else if (J != 56) {
                                    if (J == 66) {
                                        RevealOperationData.Builder builder = this.operationDataCase_ == 8 ? ((RevealOperationData) this.operationData_).toBuilder() : null;
                                        m0 z2 = jVar.z(RevealOperationData.parser(), rVar);
                                        this.operationData_ = z2;
                                        if (builder != null) {
                                            builder.mergeFrom((RevealOperationData) z2);
                                            this.operationData_ = builder.buildPartial();
                                        }
                                        this.operationDataCase_ = 8;
                                    } else if (J == 74) {
                                        TransactionOperationData.Builder builder2 = this.operationDataCase_ == 9 ? ((TransactionOperationData) this.operationData_).toBuilder() : null;
                                        m0 z3 = jVar.z(TransactionOperationData.parser(), rVar);
                                        this.operationData_ = z3;
                                        if (builder2 != null) {
                                            builder2.mergeFrom((TransactionOperationData) z3);
                                            this.operationData_ = builder2.buildPartial();
                                        }
                                        this.operationDataCase_ = 9;
                                    } else if (J != 90) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        DelegationOperationData.Builder builder3 = this.operationDataCase_ == 11 ? ((DelegationOperationData) this.operationData_).toBuilder() : null;
                                        m0 z4 = jVar.z(DelegationOperationData.parser(), rVar);
                                        this.operationData_ = z4;
                                        if (builder3 != null) {
                                            builder3.mergeFrom((DelegationOperationData) z4);
                                            this.operationData_ = builder3.buildPartial();
                                        }
                                        this.operationDataCase_ = 11;
                                    }
                                } else {
                                    this.kind_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Operation parseFrom(j jVar) throws IOException {
            return (Operation) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Operation parseFrom(j jVar, r rVar) throws IOException {
            return (Operation) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public static final class OperationList extends GeneratedMessageV3 implements OperationListOrBuilder {
        public static final int BRANCH_FIELD_NUMBER = 1;
        public static final int OPERATIONS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private volatile Object branch_;
        private byte memoizedIsInitialized;
        private List<Operation> operations_;
        private static final OperationList DEFAULT_INSTANCE = new OperationList();
        private static final t0<OperationList> PARSER = new c<OperationList>() { // from class: wallet.core.jni.proto.Tezos.OperationList.1
            @Override // com.google.protobuf.t0
            public OperationList parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new OperationList(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements OperationListOrBuilder {
            private int bitField0_;
            private Object branch_;
            private x0<Operation, Operation.Builder, OperationOrBuilder> operationsBuilder_;
            private List<Operation> operations_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private void ensureOperationsIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.operations_ = new ArrayList(this.operations_);
                    this.bitField0_ |= 1;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return Tezos.internal_static_TW_Tezos_Proto_OperationList_descriptor;
            }

            private x0<Operation, Operation.Builder, OperationOrBuilder> getOperationsFieldBuilder() {
                if (this.operationsBuilder_ == null) {
                    this.operationsBuilder_ = new x0<>(this.operations_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.operations_ = null;
                }
                return this.operationsBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getOperationsFieldBuilder();
                }
            }

            public Builder addAllOperations(Iterable<? extends Operation> iterable) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    ensureOperationsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.operations_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addOperations(Operation operation) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(operation);
                    ensureOperationsIsMutable();
                    this.operations_.add(operation);
                    onChanged();
                } else {
                    x0Var.f(operation);
                }
                return this;
            }

            public Operation.Builder addOperationsBuilder() {
                return getOperationsFieldBuilder().d(Operation.getDefaultInstance());
            }

            public Builder clearBranch() {
                this.branch_ = OperationList.getDefaultInstance().getBranch();
                onChanged();
                return this;
            }

            public Builder clearOperations() {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    this.operations_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
            public String getBranch() {
                Object obj = this.branch_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.branch_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
            public ByteString getBranchBytes() {
                Object obj = this.branch_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.branch_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tezos.internal_static_TW_Tezos_Proto_OperationList_descriptor;
            }

            @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
            public Operation getOperations(int i) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    return this.operations_.get(i);
                }
                return x0Var.o(i);
            }

            public Operation.Builder getOperationsBuilder(int i) {
                return getOperationsFieldBuilder().l(i);
            }

            public List<Operation.Builder> getOperationsBuilderList() {
                return getOperationsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
            public int getOperationsCount() {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    return this.operations_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
            public List<Operation> getOperationsList() {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.operations_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
            public OperationOrBuilder getOperationsOrBuilder(int i) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    return this.operations_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
            public List<? extends OperationOrBuilder> getOperationsOrBuilderList() {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.operations_);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tezos.internal_static_TW_Tezos_Proto_OperationList_fieldAccessorTable.d(OperationList.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeOperations(int i) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    ensureOperationsIsMutable();
                    this.operations_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setBranch(String str) {
                Objects.requireNonNull(str);
                this.branch_ = str;
                onChanged();
                return this;
            }

            public Builder setBranchBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.branch_ = byteString;
                onChanged();
                return this;
            }

            public Builder setOperations(int i, Operation operation) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(operation);
                    ensureOperationsIsMutable();
                    this.operations_.set(i, operation);
                    onChanged();
                } else {
                    x0Var.x(i, operation);
                }
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.branch_ = "";
                this.operations_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationList build() {
                OperationList buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public OperationList buildPartial() {
                OperationList operationList = new OperationList(this, (AnonymousClass1) null);
                operationList.branch_ = this.branch_;
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var != null) {
                    operationList.operations_ = x0Var.g();
                } else {
                    if ((this.bitField0_ & 1) != 0) {
                        this.operations_ = Collections.unmodifiableList(this.operations_);
                        this.bitField0_ &= -2;
                    }
                    operationList.operations_ = this.operations_;
                }
                onBuilt();
                return operationList;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public OperationList getDefaultInstanceForType() {
                return OperationList.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public Operation.Builder addOperationsBuilder(int i) {
                return getOperationsFieldBuilder().c(i, Operation.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.branch_ = "";
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    this.operations_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.branch_ = "";
                this.operations_ = Collections.emptyList();
                maybeForceBuilderInitialization();
            }

            public Builder addOperations(int i, Operation operation) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(operation);
                    ensureOperationsIsMutable();
                    this.operations_.add(i, operation);
                    onChanged();
                } else {
                    x0Var.e(i, operation);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof OperationList) {
                    return mergeFrom((OperationList) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setOperations(int i, Operation.Builder builder) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    ensureOperationsIsMutable();
                    this.operations_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder mergeFrom(OperationList operationList) {
                if (operationList == OperationList.getDefaultInstance()) {
                    return this;
                }
                if (!operationList.getBranch().isEmpty()) {
                    this.branch_ = operationList.branch_;
                    onChanged();
                }
                if (this.operationsBuilder_ == null) {
                    if (!operationList.operations_.isEmpty()) {
                        if (this.operations_.isEmpty()) {
                            this.operations_ = operationList.operations_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureOperationsIsMutable();
                            this.operations_.addAll(operationList.operations_);
                        }
                        onChanged();
                    }
                } else if (!operationList.operations_.isEmpty()) {
                    if (!this.operationsBuilder_.u()) {
                        this.operationsBuilder_.b(operationList.operations_);
                    } else {
                        this.operationsBuilder_.i();
                        this.operationsBuilder_ = null;
                        this.operations_ = operationList.operations_;
                        this.bitField0_ &= -2;
                        this.operationsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getOperationsFieldBuilder() : null;
                    }
                }
                mergeUnknownFields(operationList.unknownFields);
                onChanged();
                return this;
            }

            public Builder addOperations(Operation.Builder builder) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    ensureOperationsIsMutable();
                    this.operations_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addOperations(int i, Operation.Builder builder) {
                x0<Operation, Operation.Builder, OperationOrBuilder> x0Var = this.operationsBuilder_;
                if (x0Var == null) {
                    ensureOperationsIsMutable();
                    this.operations_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tezos.OperationList.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tezos.OperationList.access$3100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tezos$OperationList r3 = (wallet.core.jni.proto.Tezos.OperationList) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tezos$OperationList r4 = (wallet.core.jni.proto.Tezos.OperationList) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tezos.OperationList.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tezos$OperationList$Builder");
            }
        }

        public /* synthetic */ OperationList(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static OperationList getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tezos.internal_static_TW_Tezos_Proto_OperationList_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static OperationList parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OperationList) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static OperationList parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<OperationList> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof OperationList)) {
                return super.equals(obj);
            }
            OperationList operationList = (OperationList) obj;
            return getBranch().equals(operationList.getBranch()) && getOperationsList().equals(operationList.getOperationsList()) && this.unknownFields.equals(operationList.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
        public String getBranch() {
            Object obj = this.branch_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.branch_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
        public ByteString getBranchBytes() {
            Object obj = this.branch_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.branch_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
        public Operation getOperations(int i) {
            return this.operations_.get(i);
        }

        @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
        public int getOperationsCount() {
            return this.operations_.size();
        }

        @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
        public List<Operation> getOperationsList() {
            return this.operations_;
        }

        @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
        public OperationOrBuilder getOperationsOrBuilder(int i) {
            return this.operations_.get(i);
        }

        @Override // wallet.core.jni.proto.Tezos.OperationListOrBuilder
        public List<? extends OperationOrBuilder> getOperationsOrBuilderList() {
            return this.operations_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<OperationList> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = !getBranchBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.branch_) + 0 : 0;
            for (int i2 = 0; i2 < this.operations_.size(); i2++) {
                computeStringSize += CodedOutputStream.G(2, this.operations_.get(i2));
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getBranch().hashCode();
            if (getOperationsCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getOperationsList().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tezos.internal_static_TW_Tezos_Proto_OperationList_fieldAccessorTable.d(OperationList.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new OperationList();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getBranchBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.branch_);
            }
            for (int i = 0; i < this.operations_.size(); i++) {
                codedOutputStream.K0(2, this.operations_.get(i));
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ OperationList(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(OperationList operationList) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(operationList);
        }

        public static OperationList parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private OperationList(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static OperationList parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationList) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationList parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public OperationList getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static OperationList parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private OperationList() {
            this.memoizedIsInitialized = (byte) -1;
            this.branch_ = "";
            this.operations_ = Collections.emptyList();
        }

        public static OperationList parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static OperationList parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static OperationList parseFrom(InputStream inputStream) throws IOException {
            return (OperationList) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private OperationList(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.branch_ = jVar.I();
                            } else if (J != 18) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                if (!(z2 & true)) {
                                    this.operations_ = new ArrayList();
                                    z2 |= true;
                                }
                                this.operations_.add(jVar.z(Operation.parser(), rVar));
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.operations_ = Collections.unmodifiableList(this.operations_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static OperationList parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (OperationList) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static OperationList parseFrom(j jVar) throws IOException {
            return (OperationList) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static OperationList parseFrom(j jVar, r rVar) throws IOException {
            return (OperationList) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface OperationListOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getBranch();

        ByteString getBranchBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Operation getOperations(int i);

        int getOperationsCount();

        List<Operation> getOperationsList();

        OperationOrBuilder getOperationsOrBuilder(int i);

        List<? extends OperationOrBuilder> getOperationsOrBuilderList();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public interface OperationOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getCounter();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        DelegationOperationData getDelegationOperationData();

        DelegationOperationDataOrBuilder getDelegationOperationDataOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        long getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        long getGasLimit();

        /* synthetic */ String getInitializationErrorString();

        Operation.OperationKind getKind();

        int getKindValue();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        Operation.OperationDataCase getOperationDataCase();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        RevealOperationData getRevealOperationData();

        RevealOperationDataOrBuilder getRevealOperationDataOrBuilder();

        String getSource();

        ByteString getSourceBytes();

        long getStorageLimit();

        TransactionOperationData getTransactionOperationData();

        TransactionOperationDataOrBuilder getTransactionOperationDataOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasDelegationOperationData();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasRevealOperationData();

        boolean hasTransactionOperationData();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class RevealOperationData extends GeneratedMessageV3 implements RevealOperationDataOrBuilder {
        private static final RevealOperationData DEFAULT_INSTANCE = new RevealOperationData();
        private static final t0<RevealOperationData> PARSER = new c<RevealOperationData>() { // from class: wallet.core.jni.proto.Tezos.RevealOperationData.1
            @Override // com.google.protobuf.t0
            public RevealOperationData parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new RevealOperationData(jVar, rVar, null);
            }
        };
        public static final int PUBLIC_KEY_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private ByteString publicKey_;

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements RevealOperationDataOrBuilder {
            private ByteString publicKey_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tezos.internal_static_TW_Tezos_Proto_RevealOperationData_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearPublicKey() {
                this.publicKey_ = RevealOperationData.getDefaultInstance().getPublicKey();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tezos.internal_static_TW_Tezos_Proto_RevealOperationData_descriptor;
            }

            @Override // wallet.core.jni.proto.Tezos.RevealOperationDataOrBuilder
            public ByteString getPublicKey() {
                return this.publicKey_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tezos.internal_static_TW_Tezos_Proto_RevealOperationData_fieldAccessorTable.d(RevealOperationData.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setPublicKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.publicKey_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.publicKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public RevealOperationData build() {
                RevealOperationData buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public RevealOperationData buildPartial() {
                RevealOperationData revealOperationData = new RevealOperationData(this, (AnonymousClass1) null);
                revealOperationData.publicKey_ = this.publicKey_;
                onBuilt();
                return revealOperationData;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public RevealOperationData getDefaultInstanceForType() {
                return RevealOperationData.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.publicKey_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.publicKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof RevealOperationData) {
                    return mergeFrom((RevealOperationData) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(RevealOperationData revealOperationData) {
                if (revealOperationData == RevealOperationData.getDefaultInstance()) {
                    return this;
                }
                if (revealOperationData.getPublicKey() != ByteString.EMPTY) {
                    setPublicKey(revealOperationData.getPublicKey());
                }
                mergeUnknownFields(revealOperationData.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tezos.RevealOperationData.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tezos.RevealOperationData.access$7200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tezos$RevealOperationData r3 = (wallet.core.jni.proto.Tezos.RevealOperationData) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tezos$RevealOperationData r4 = (wallet.core.jni.proto.Tezos.RevealOperationData) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tezos.RevealOperationData.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tezos$RevealOperationData$Builder");
            }
        }

        public /* synthetic */ RevealOperationData(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static RevealOperationData getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tezos.internal_static_TW_Tezos_Proto_RevealOperationData_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static RevealOperationData parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (RevealOperationData) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static RevealOperationData parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<RevealOperationData> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof RevealOperationData)) {
                return super.equals(obj);
            }
            RevealOperationData revealOperationData = (RevealOperationData) obj;
            return getPublicKey().equals(revealOperationData.getPublicKey()) && this.unknownFields.equals(revealOperationData.unknownFields);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<RevealOperationData> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Tezos.RevealOperationDataOrBuilder
        public ByteString getPublicKey() {
            return this.publicKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.publicKey_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.publicKey_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPublicKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tezos.internal_static_TW_Tezos_Proto_RevealOperationData_fieldAccessorTable.d(RevealOperationData.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new RevealOperationData();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.publicKey_.isEmpty()) {
                codedOutputStream.q0(1, this.publicKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ RevealOperationData(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(RevealOperationData revealOperationData) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(revealOperationData);
        }

        public static RevealOperationData parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private RevealOperationData(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static RevealOperationData parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (RevealOperationData) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static RevealOperationData parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public RevealOperationData getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static RevealOperationData parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private RevealOperationData() {
            this.memoizedIsInitialized = (byte) -1;
            this.publicKey_ = ByteString.EMPTY;
        }

        public static RevealOperationData parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static RevealOperationData parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static RevealOperationData parseFrom(InputStream inputStream) throws IOException {
            return (RevealOperationData) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private RevealOperationData(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.publicKey_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static RevealOperationData parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (RevealOperationData) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static RevealOperationData parseFrom(j jVar) throws IOException {
            return (RevealOperationData) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static RevealOperationData parseFrom(j jVar, r rVar) throws IOException {
            return (RevealOperationData) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface RevealOperationDataOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPublicKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int OPERATION_LIST_FIELD_NUMBER = 1;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private OperationList operationList_;
        private ByteString privateKey_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Tezos.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private a1<OperationList, OperationList.Builder, OperationListOrBuilder> operationListBuilder_;
            private OperationList operationList_;
            private ByteString privateKey_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tezos.internal_static_TW_Tezos_Proto_SigningInput_descriptor;
            }

            private a1<OperationList, OperationList.Builder, OperationListOrBuilder> getOperationListFieldBuilder() {
                if (this.operationListBuilder_ == null) {
                    this.operationListBuilder_ = new a1<>(getOperationList(), getParentForChildren(), isClean());
                    this.operationList_ = null;
                }
                return this.operationListBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearOperationList() {
                if (this.operationListBuilder_ == null) {
                    this.operationList_ = null;
                    onChanged();
                } else {
                    this.operationList_ = null;
                    this.operationListBuilder_ = null;
                }
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tezos.internal_static_TW_Tezos_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
            public OperationList getOperationList() {
                a1<OperationList, OperationList.Builder, OperationListOrBuilder> a1Var = this.operationListBuilder_;
                if (a1Var == null) {
                    OperationList operationList = this.operationList_;
                    return operationList == null ? OperationList.getDefaultInstance() : operationList;
                }
                return a1Var.f();
            }

            public OperationList.Builder getOperationListBuilder() {
                onChanged();
                return getOperationListFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
            public OperationListOrBuilder getOperationListOrBuilder() {
                a1<OperationList, OperationList.Builder, OperationListOrBuilder> a1Var = this.operationListBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                OperationList operationList = this.operationList_;
                return operationList == null ? OperationList.getDefaultInstance() : operationList;
            }

            @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
            public boolean hasOperationList() {
                return (this.operationListBuilder_ == null && this.operationList_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tezos.internal_static_TW_Tezos_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeOperationList(OperationList operationList) {
                a1<OperationList, OperationList.Builder, OperationListOrBuilder> a1Var = this.operationListBuilder_;
                if (a1Var == null) {
                    OperationList operationList2 = this.operationList_;
                    if (operationList2 != null) {
                        this.operationList_ = OperationList.newBuilder(operationList2).mergeFrom(operationList).buildPartial();
                    } else {
                        this.operationList_ = operationList;
                    }
                    onChanged();
                } else {
                    a1Var.h(operationList);
                }
                return this;
            }

            public Builder setOperationList(OperationList operationList) {
                a1<OperationList, OperationList.Builder, OperationListOrBuilder> a1Var = this.operationListBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(operationList);
                    this.operationList_ = operationList;
                    onChanged();
                } else {
                    a1Var.j(operationList);
                }
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                a1<OperationList, OperationList.Builder, OperationListOrBuilder> a1Var = this.operationListBuilder_;
                if (a1Var == null) {
                    signingInput.operationList_ = this.operationList_;
                } else {
                    signingInput.operationList_ = a1Var.b();
                }
                signingInput.privateKey_ = this.privateKey_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                if (this.operationListBuilder_ == null) {
                    this.operationList_ = null;
                } else {
                    this.operationList_ = null;
                    this.operationListBuilder_ = null;
                }
                this.privateKey_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.privateKey_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            public Builder setOperationList(OperationList.Builder builder) {
                a1<OperationList, OperationList.Builder, OperationListOrBuilder> a1Var = this.operationListBuilder_;
                if (a1Var == null) {
                    this.operationList_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (signingInput.hasOperationList()) {
                    mergeOperationList(signingInput.getOperationList());
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tezos.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tezos.SigningInput.access$900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tezos$SigningInput r3 = (wallet.core.jni.proto.Tezos.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tezos$SigningInput r4 = (wallet.core.jni.proto.Tezos.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tezos.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tezos$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tezos.internal_static_TW_Tezos_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (hasOperationList() != signingInput.hasOperationList()) {
                return false;
            }
            return (!hasOperationList() || getOperationList().equals(signingInput.getOperationList())) && getPrivateKey().equals(signingInput.getPrivateKey()) && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
        public OperationList getOperationList() {
            OperationList operationList = this.operationList_;
            return operationList == null ? OperationList.getDefaultInstance() : operationList;
        }

        @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
        public OperationListOrBuilder getOperationListOrBuilder() {
            return getOperationList();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.operationList_ != null ? 0 + CodedOutputStream.G(1, getOperationList()) : 0;
            if (!this.privateKey_.isEmpty()) {
                G += CodedOutputStream.h(2, this.privateKey_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Tezos.SigningInputOrBuilder
        public boolean hasOperationList() {
            return this.operationList_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (hasOperationList()) {
                hashCode = (((hashCode * 37) + 1) * 53) + getOperationList().hashCode();
            }
            int hashCode2 = (((((hashCode * 37) + 2) * 53) + getPrivateKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tezos.internal_static_TW_Tezos_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.operationList_ != null) {
                codedOutputStream.K0(1, getOperationList());
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(2, this.privateKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.privateKey_ = ByteString.EMPTY;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                OperationList operationList = this.operationList_;
                                OperationList.Builder builder = operationList != null ? operationList.toBuilder() : null;
                                OperationList operationList2 = (OperationList) jVar.z(OperationList.parser(), rVar);
                                this.operationList_ = operationList2;
                                if (builder != null) {
                                    builder.mergeFrom(operationList2);
                                    this.operationList_ = builder.buildPartial();
                                }
                            } else if (J != 18) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.privateKey_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        OperationList getOperationList();

        OperationListOrBuilder getOperationListOrBuilder();

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasOperationList();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Tezos.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tezos.internal_static_TW_Tezos_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tezos.internal_static_TW_Tezos_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Tezos.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tezos.internal_static_TW_Tezos_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = ByteString.EMPTY;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tezos.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tezos.SigningOutput.access$1900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tezos$SigningOutput r3 = (wallet.core.jni.proto.Tezos.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tezos$SigningOutput r4 = (wallet.core.jni.proto.Tezos.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tezos.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tezos$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tezos.internal_static_TW_Tezos_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tezos.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = (this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_)) + this.unknownFields.getSerializedSize();
            this.memoizedSize = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tezos.internal_static_TW_Tezos_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J != 10) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.encoded_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionOperationData extends GeneratedMessageV3 implements TransactionOperationDataOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 2;
        public static final int DESTINATION_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object destination_;
        private byte memoizedIsInitialized;
        private static final TransactionOperationData DEFAULT_INSTANCE = new TransactionOperationData();
        private static final t0<TransactionOperationData> PARSER = new c<TransactionOperationData>() { // from class: wallet.core.jni.proto.Tezos.TransactionOperationData.1
            @Override // com.google.protobuf.t0
            public TransactionOperationData parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionOperationData(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOperationDataOrBuilder {
            private long amount_;
            private Object destination_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Tezos.internal_static_TW_Tezos_Proto_TransactionOperationData_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearDestination() {
                this.destination_ = TransactionOperationData.getDefaultInstance().getDestination();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Tezos.TransactionOperationDataOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Tezos.internal_static_TW_Tezos_Proto_TransactionOperationData_descriptor;
            }

            @Override // wallet.core.jni.proto.Tezos.TransactionOperationDataOrBuilder
            public String getDestination() {
                Object obj = this.destination_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.destination_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Tezos.TransactionOperationDataOrBuilder
            public ByteString getDestinationBytes() {
                Object obj = this.destination_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.destination_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Tezos.internal_static_TW_Tezos_Proto_TransactionOperationData_fieldAccessorTable.d(TransactionOperationData.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setDestination(String str) {
                Objects.requireNonNull(str);
                this.destination_ = str;
                onChanged();
                return this;
            }

            public Builder setDestinationBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.destination_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.destination_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOperationData build() {
                TransactionOperationData buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOperationData buildPartial() {
                TransactionOperationData transactionOperationData = new TransactionOperationData(this, (AnonymousClass1) null);
                transactionOperationData.destination_ = this.destination_;
                transactionOperationData.amount_ = this.amount_;
                onBuilt();
                return transactionOperationData;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionOperationData getDefaultInstanceForType() {
                return TransactionOperationData.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.destination_ = "";
                this.amount_ = 0L;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.destination_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionOperationData) {
                    return mergeFrom((TransactionOperationData) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionOperationData transactionOperationData) {
                if (transactionOperationData == TransactionOperationData.getDefaultInstance()) {
                    return this;
                }
                if (!transactionOperationData.getDestination().isEmpty()) {
                    this.destination_ = transactionOperationData.destination_;
                    onChanged();
                }
                if (transactionOperationData.getAmount() != 0) {
                    setAmount(transactionOperationData.getAmount());
                }
                mergeUnknownFields(transactionOperationData.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Tezos.TransactionOperationData.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Tezos.TransactionOperationData.access$6100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Tezos$TransactionOperationData r3 = (wallet.core.jni.proto.Tezos.TransactionOperationData) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Tezos$TransactionOperationData r4 = (wallet.core.jni.proto.Tezos.TransactionOperationData) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Tezos.TransactionOperationData.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Tezos$TransactionOperationData$Builder");
            }
        }

        public /* synthetic */ TransactionOperationData(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static TransactionOperationData getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Tezos.internal_static_TW_Tezos_Proto_TransactionOperationData_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionOperationData parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionOperationData) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionOperationData parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionOperationData> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionOperationData)) {
                return super.equals(obj);
            }
            TransactionOperationData transactionOperationData = (TransactionOperationData) obj;
            return getDestination().equals(transactionOperationData.getDestination()) && getAmount() == transactionOperationData.getAmount() && this.unknownFields.equals(transactionOperationData.unknownFields);
        }

        @Override // wallet.core.jni.proto.Tezos.TransactionOperationDataOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.Tezos.TransactionOperationDataOrBuilder
        public String getDestination() {
            Object obj = this.destination_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.destination_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Tezos.TransactionOperationDataOrBuilder
        public ByteString getDestinationBytes() {
            Object obj = this.destination_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.destination_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionOperationData> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getDestinationBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.destination_);
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.z(2, j);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getDestination().hashCode()) * 37) + 2) * 53) + a0.h(getAmount())) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Tezos.internal_static_TW_Tezos_Proto_TransactionOperationData_fieldAccessorTable.d(TransactionOperationData.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionOperationData();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getDestinationBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.destination_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(2, j);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ TransactionOperationData(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(TransactionOperationData transactionOperationData) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionOperationData);
        }

        public static TransactionOperationData parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionOperationData(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionOperationData parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOperationData) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOperationData parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionOperationData getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static TransactionOperationData parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionOperationData() {
            this.memoizedIsInitialized = (byte) -1;
            this.destination_ = "";
        }

        public static TransactionOperationData parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static TransactionOperationData parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionOperationData parseFrom(InputStream inputStream) throws IOException {
            return (TransactionOperationData) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionOperationData(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.destination_ = jVar.I();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.amount_ = jVar.y();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionOperationData parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOperationData) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOperationData parseFrom(j jVar) throws IOException {
            return (TransactionOperationData) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionOperationData parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionOperationData) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOperationDataOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        String getDestination();

        ByteString getDestinationBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Tezos_Proto_SigningInput_descriptor = bVar;
        internal_static_TW_Tezos_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"OperationList", "PrivateKey"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Tezos_Proto_SigningOutput_descriptor = bVar2;
        internal_static_TW_Tezos_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Encoded"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_Tezos_Proto_OperationList_descriptor = bVar3;
        internal_static_TW_Tezos_Proto_OperationList_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Branch", "Operations"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_Tezos_Proto_Operation_descriptor = bVar4;
        internal_static_TW_Tezos_Proto_Operation_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Counter", "Source", "Fee", "GasLimit", "StorageLimit", "Kind", "RevealOperationData", "TransactionOperationData", "DelegationOperationData", "OperationData"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_Tezos_Proto_TransactionOperationData_descriptor = bVar5;
        internal_static_TW_Tezos_Proto_TransactionOperationData_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"Destination", "Amount"});
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_Tezos_Proto_RevealOperationData_descriptor = bVar6;
        internal_static_TW_Tezos_Proto_RevealOperationData_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"PublicKey"});
        Descriptors.b bVar7 = getDescriptor().o().get(6);
        internal_static_TW_Tezos_Proto_DelegationOperationData_descriptor = bVar7;
        internal_static_TW_Tezos_Proto_DelegationOperationData_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"Delegate"});
    }

    private Tezos() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
