package wallet.core.jni.proto;

import com.google.protobuf.Descriptors;
import com.google.protobuf.a0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.v0;

/* loaded from: classes3.dex */
public final class Common {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\fCommon.proto\u0012\u000fTW.Common.Proto*á\u0002\n\fSigningError\u0012\u0006\n\u0002OK\u0010\u0000\u0012\u0011\n\rError_general\u0010\u0001\u0012\u0012\n\u000eError_internal\u0010\u0002\u0012\u0015\n\u0011Error_low_balance\u0010\u0003\u0012\u001f\n\u001bError_zero_amount_requested\u0010\u0004\u0012\u001d\n\u0019Error_missing_private_key\u0010\u0005\u0012\u0013\n\u000fError_wrong_fee\u0010\u0006\u0012\u0011\n\rError_signing\u0010\u0007\u0012\u0014\n\u0010Error_tx_too_big\u0010\b\u0012\u001d\n\u0019Error_missing_input_utxos\u0010\t\u0012\u001a\n\u0016Error_not_enough_utxos\u0010\n\u0012\u0017\n\u0013Error_script_redeem\u0010\u000b\u0012\u0017\n\u0013Error_script_output\u0010\f\u0012 \n\u001cError_script_witness_program\u0010\rB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);

    /* loaded from: classes3.dex */
    public enum SigningError implements v0 {
        OK(0),
        Error_general(1),
        Error_internal(2),
        Error_low_balance(3),
        Error_zero_amount_requested(4),
        Error_missing_private_key(5),
        Error_wrong_fee(6),
        Error_signing(7),
        Error_tx_too_big(8),
        Error_missing_input_utxos(9),
        Error_not_enough_utxos(10),
        Error_script_redeem(11),
        Error_script_output(12),
        Error_script_witness_program(13),
        UNRECOGNIZED(-1);
        
        public static final int Error_general_VALUE = 1;
        public static final int Error_internal_VALUE = 2;
        public static final int Error_low_balance_VALUE = 3;
        public static final int Error_missing_input_utxos_VALUE = 9;
        public static final int Error_missing_private_key_VALUE = 5;
        public static final int Error_not_enough_utxos_VALUE = 10;
        public static final int Error_script_output_VALUE = 12;
        public static final int Error_script_redeem_VALUE = 11;
        public static final int Error_script_witness_program_VALUE = 13;
        public static final int Error_signing_VALUE = 7;
        public static final int Error_tx_too_big_VALUE = 8;
        public static final int Error_wrong_fee_VALUE = 6;
        public static final int Error_zero_amount_requested_VALUE = 4;
        public static final int OK_VALUE = 0;
        private final int value;
        private static final a0.d<SigningError> internalValueMap = new a0.d<SigningError>() { // from class: wallet.core.jni.proto.Common.SigningError.1
            @Override // com.google.protobuf.a0.d
            public SigningError findValueByNumber(int i) {
                return SigningError.forNumber(i);
            }
        };
        private static final SigningError[] VALUES = values();

        SigningError(int i) {
            this.value = i;
        }

        public static SigningError forNumber(int i) {
            switch (i) {
                case 0:
                    return OK;
                case 1:
                    return Error_general;
                case 2:
                    return Error_internal;
                case 3:
                    return Error_low_balance;
                case 4:
                    return Error_zero_amount_requested;
                case 5:
                    return Error_missing_private_key;
                case 6:
                    return Error_wrong_fee;
                case 7:
                    return Error_signing;
                case 8:
                    return Error_tx_too_big;
                case 9:
                    return Error_missing_input_utxos;
                case 10:
                    return Error_not_enough_utxos;
                case 11:
                    return Error_script_redeem;
                case 12:
                    return Error_script_output;
                case 13:
                    return Error_script_witness_program;
                default:
                    return null;
            }
        }

        public static final Descriptors.c getDescriptor() {
            return Common.getDescriptor().l().get(0);
        }

        public static a0.d<SigningError> internalGetValueMap() {
            return internalValueMap;
        }

        public final Descriptors.c getDescriptorForType() {
            return getDescriptor();
        }

        @Override // com.google.protobuf.a0.c
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        public final Descriptors.d getValueDescriptor() {
            if (this != UNRECOGNIZED) {
                return getDescriptor().k().get(ordinal());
            }
            throw new IllegalStateException("Can't get the descriptor of an unrecognized enum value.");
        }

        @Deprecated
        public static SigningError valueOf(int i) {
            return forNumber(i);
        }

        public static SigningError valueOf(Descriptors.d dVar) {
            if (dVar.h() == getDescriptor()) {
                if (dVar.g() == -1) {
                    return UNRECOGNIZED;
                }
                return VALUES[dVar.g()];
            }
            throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        }
    }

    private Common() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
