package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Ethereum {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000eEthereum.proto\u0012\u0011TW.Ethereum.Proto\"¢\u0006\n\u000bTransaction\u0012;\n\btransfer\u0018\u0001 \u0001(\u000b2'.TW.Ethereum.Proto.Transaction.TransferH\u0000\u0012F\n\u000eerc20_transfer\u0018\u0002 \u0001(\u000b2,.TW.Ethereum.Proto.Transaction.ERC20TransferH\u0000\u0012D\n\rerc20_approve\u0018\u0003 \u0001(\u000b2+.TW.Ethereum.Proto.Transaction.ERC20ApproveH\u0000\u0012H\n\u000ferc721_transfer\u0018\u0004 \u0001(\u000b2-.TW.Ethereum.Proto.Transaction.ERC721TransferH\u0000\u0012J\n\u0010erc1155_transfer\u0018\u0005 \u0001(\u000b2..TW.Ethereum.Proto.Transaction.ERC1155TransferH\u0000\u0012J\n\u0010contract_generic\u0018\u0006 \u0001(\u000b2..TW.Ethereum.Proto.Transaction.ContractGenericH\u0000\u001a(\n\bTransfer\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\f\u0012\f\n\u0004data\u0018\u0002 \u0001(\f\u001a+\n\rERC20Transfer\u0012\n\n\u0002to\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\f\u001a/\n\fERC20Approve\u0012\u000f\n\u0007spender\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\f\u001a<\n\u000eERC721Transfer\u0012\f\n\u0004from\u0018\u0001 \u0001(\t\u0012\n\n\u0002to\u0018\u0002 \u0001(\t\u0012\u0010\n\btoken_id\u0018\u0003 \u0001(\f\u001aZ\n\u000fERC1155Transfer\u0012\f\n\u0004from\u0018\u0001 \u0001(\t\u0012\n\n\u0002to\u0018\u0002 \u0001(\t\u0012\u0010\n\btoken_id\u0018\u0003 \u0001(\f\u0012\r\n\u0005value\u0018\u0004 \u0001(\f\u0012\f\n\u0004data\u0018\u0005 \u0001(\f\u001a/\n\u000fContractGeneric\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\f\u0012\f\n\u0004data\u0018\u0002 \u0001(\fB\u0013\n\u0011transaction_oneof\"³\u0001\n\fSigningInput\u0012\u0010\n\bchain_id\u0018\u0001 \u0001(\f\u0012\r\n\u0005nonce\u0018\u0002 \u0001(\f\u0012\u0011\n\tgas_price\u0018\u0003 \u0001(\f\u0012\u0011\n\tgas_limit\u0018\u0004 \u0001(\f\u0012\u0012\n\nto_address\u0018\u0005 \u0001(\t\u0012\u0013\n\u000bprivate_key\u0018\u0006 \u0001(\f\u00123\n\u000btransaction\u0018\u0007 \u0001(\u000b2\u001e.TW.Ethereum.Proto.Transaction\"O\n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f\u0012\t\n\u0001v\u0018\u0002 \u0001(\f\u0012\t\n\u0001r\u0018\u0003 \u0001(\f\u0012\t\n\u0001s\u0018\u0004 \u0001(\f\u0012\f\n\u0004data\u0018\u0005 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_Transaction_Transfer_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_Transaction_Transfer_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Ethereum_Proto_Transaction_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Ethereum_Proto_Transaction_fieldAccessorTable;

    /* renamed from: wallet.core.jni.proto.Ethereum$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase;

        static {
            int[] iArr = new int[Transaction.TransactionOneofCase.values().length];
            $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase = iArr;
            try {
                iArr[Transaction.TransactionOneofCase.TRANSFER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase[Transaction.TransactionOneofCase.ERC20_TRANSFER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase[Transaction.TransactionOneofCase.ERC20_APPROVE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase[Transaction.TransactionOneofCase.ERC721_TRANSFER.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase[Transaction.TransactionOneofCase.ERC1155_TRANSFER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase[Transaction.TransactionOneofCase.CONTRACT_GENERIC.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase[Transaction.TransactionOneofCase.TRANSACTIONONEOF_NOT_SET.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int CHAIN_ID_FIELD_NUMBER = 1;
        public static final int GAS_LIMIT_FIELD_NUMBER = 4;
        public static final int GAS_PRICE_FIELD_NUMBER = 3;
        public static final int NONCE_FIELD_NUMBER = 2;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 6;
        public static final int TO_ADDRESS_FIELD_NUMBER = 5;
        public static final int TRANSACTION_FIELD_NUMBER = 7;
        private static final long serialVersionUID = 0;
        private ByteString chainId_;
        private ByteString gasLimit_;
        private ByteString gasPrice_;
        private byte memoizedIsInitialized;
        private ByteString nonce_;
        private ByteString privateKey_;
        private volatile Object toAddress_;
        private Transaction transaction_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Ethereum.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private ByteString chainId_;
            private ByteString gasLimit_;
            private ByteString gasPrice_;
            private ByteString nonce_;
            private ByteString privateKey_;
            private Object toAddress_;
            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> transactionBuilder_;
            private Transaction transaction_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_SigningInput_descriptor;
            }

            private a1<Transaction, Transaction.Builder, TransactionOrBuilder> getTransactionFieldBuilder() {
                if (this.transactionBuilder_ == null) {
                    this.transactionBuilder_ = new a1<>(getTransaction(), getParentForChildren(), isClean());
                    this.transaction_ = null;
                }
                return this.transactionBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearChainId() {
                this.chainId_ = SigningInput.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearGasLimit() {
                this.gasLimit_ = SigningInput.getDefaultInstance().getGasLimit();
                onChanged();
                return this;
            }

            public Builder clearGasPrice() {
                this.gasPrice_ = SigningInput.getDefaultInstance().getGasPrice();
                onChanged();
                return this;
            }

            public Builder clearNonce() {
                this.nonce_ = SigningInput.getDefaultInstance().getNonce();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = SigningInput.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            public Builder clearTransaction() {
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                    onChanged();
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public ByteString getChainId() {
                return this.chainId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Ethereum.internal_static_TW_Ethereum_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public ByteString getGasLimit() {
                return this.gasLimit_;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public ByteString getGasPrice() {
                return this.gasPrice_;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public ByteString getNonce() {
                return this.nonce_;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public Transaction getTransaction() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction = this.transaction_;
                    return transaction == null ? Transaction.getDefaultInstance() : transaction;
                }
                return a1Var.f();
            }

            public Transaction.Builder getTransactionBuilder() {
                onChanged();
                return getTransactionFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public TransactionOrBuilder getTransactionOrBuilder() {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                Transaction transaction = this.transaction_;
                return transaction == null ? Transaction.getDefaultInstance() : transaction;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
            public boolean hasTransaction() {
                return (this.transactionBuilder_ == null && this.transaction_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Transaction transaction2 = this.transaction_;
                    if (transaction2 != null) {
                        this.transaction_ = Transaction.newBuilder(transaction2).mergeFrom(transaction).buildPartial();
                    } else {
                        this.transaction_ = transaction;
                    }
                    onChanged();
                } else {
                    a1Var.h(transaction);
                }
                return this;
            }

            public Builder setChainId(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasLimit(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasLimit_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasPrice(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.gasPrice_ = byteString;
                onChanged();
                return this;
            }

            public Builder setNonce(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.nonce_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setTransaction(Transaction transaction) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transaction);
                    this.transaction_ = transaction;
                    onChanged();
                } else {
                    a1Var.j(transaction);
                }
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this, (AnonymousClass1) null);
                signingInput.chainId_ = this.chainId_;
                signingInput.nonce_ = this.nonce_;
                signingInput.gasPrice_ = this.gasPrice_;
                signingInput.gasLimit_ = this.gasLimit_;
                signingInput.toAddress_ = this.toAddress_;
                signingInput.privateKey_ = this.privateKey_;
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    signingInput.transaction_ = this.transaction_;
                } else {
                    signingInput.transaction_ = a1Var.b();
                }
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.privateKey_ = byteString;
                if (this.transactionBuilder_ == null) {
                    this.transaction_ = null;
                } else {
                    this.transaction_ = null;
                    this.transactionBuilder_ = null;
                }
                return this;
            }

            public Builder setTransaction(Transaction.Builder builder) {
                a1<Transaction, Transaction.Builder, TransactionOrBuilder> a1Var = this.transactionBuilder_;
                if (a1Var == null) {
                    this.transaction_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                ByteString chainId = signingInput.getChainId();
                ByteString byteString = ByteString.EMPTY;
                if (chainId != byteString) {
                    setChainId(signingInput.getChainId());
                }
                if (signingInput.getNonce() != byteString) {
                    setNonce(signingInput.getNonce());
                }
                if (signingInput.getGasPrice() != byteString) {
                    setGasPrice(signingInput.getGasPrice());
                }
                if (signingInput.getGasLimit() != byteString) {
                    setGasLimit(signingInput.getGasLimit());
                }
                if (!signingInput.getToAddress().isEmpty()) {
                    this.toAddress_ = signingInput.toAddress_;
                    onChanged();
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (signingInput.hasTransaction()) {
                    mergeTransaction(signingInput.getTransaction());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.chainId_ = byteString;
                this.nonce_ = byteString;
                this.gasPrice_ = byteString;
                this.gasLimit_ = byteString;
                this.toAddress_ = "";
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Ethereum.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.SigningInput.access$10100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Ethereum$SigningInput r3 = (wallet.core.jni.proto.Ethereum.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Ethereum$SigningInput r4 = (wallet.core.jni.proto.Ethereum.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$SigningInput$Builder");
            }
        }

        public /* synthetic */ SigningInput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Ethereum.internal_static_TW_Ethereum_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getChainId().equals(signingInput.getChainId()) && getNonce().equals(signingInput.getNonce()) && getGasPrice().equals(signingInput.getGasPrice()) && getGasLimit().equals(signingInput.getGasLimit()) && getToAddress().equals(signingInput.getToAddress()) && getPrivateKey().equals(signingInput.getPrivateKey()) && hasTransaction() == signingInput.hasTransaction()) {
                return (!hasTransaction() || getTransaction().equals(signingInput.getTransaction())) && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public ByteString getChainId() {
            return this.chainId_;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public ByteString getGasLimit() {
            return this.gasLimit_;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public ByteString getGasPrice() {
            return this.gasPrice_;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public ByteString getNonce() {
            return this.nonce_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.chainId_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.chainId_);
            if (!this.nonce_.isEmpty()) {
                h += CodedOutputStream.h(2, this.nonce_);
            }
            if (!this.gasPrice_.isEmpty()) {
                h += CodedOutputStream.h(3, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                h += CodedOutputStream.h(4, this.gasLimit_);
            }
            if (!getToAddressBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(5, this.toAddress_);
            }
            if (!this.privateKey_.isEmpty()) {
                h += CodedOutputStream.h(6, this.privateKey_);
            }
            if (this.transaction_ != null) {
                h += CodedOutputStream.G(7, getTransaction());
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public Transaction getTransaction() {
            Transaction transaction = this.transaction_;
            return transaction == null ? Transaction.getDefaultInstance() : transaction;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public TransactionOrBuilder getTransactionOrBuilder() {
            return getTransaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningInputOrBuilder
        public boolean hasTransaction() {
            return this.transaction_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getChainId().hashCode()) * 37) + 2) * 53) + getNonce().hashCode()) * 37) + 3) * 53) + getGasPrice().hashCode()) * 37) + 4) * 53) + getGasLimit().hashCode()) * 37) + 5) * 53) + getToAddress().hashCode()) * 37) + 6) * 53) + getPrivateKey().hashCode();
            if (hasTransaction()) {
                hashCode = (((hashCode * 37) + 7) * 53) + getTransaction().hashCode();
            }
            int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode2;
            return hashCode2;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Ethereum.internal_static_TW_Ethereum_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.chainId_.isEmpty()) {
                codedOutputStream.q0(1, this.chainId_);
            }
            if (!this.nonce_.isEmpty()) {
                codedOutputStream.q0(2, this.nonce_);
            }
            if (!this.gasPrice_.isEmpty()) {
                codedOutputStream.q0(3, this.gasPrice_);
            }
            if (!this.gasLimit_.isEmpty()) {
                codedOutputStream.q0(4, this.gasLimit_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.toAddress_);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(6, this.privateKey_);
            }
            if (this.transaction_ != null) {
                codedOutputStream.K0(7, getTransaction());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningInput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.chainId_ = byteString;
            this.nonce_ = byteString;
            this.gasPrice_ = byteString;
            this.gasLimit_ = byteString;
            this.toAddress_ = "";
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.chainId_ = jVar.q();
                                } else if (J == 18) {
                                    this.nonce_ = jVar.q();
                                } else if (J == 26) {
                                    this.gasPrice_ = jVar.q();
                                } else if (J == 34) {
                                    this.gasLimit_ = jVar.q();
                                } else if (J == 42) {
                                    this.toAddress_ = jVar.I();
                                } else if (J == 50) {
                                    this.privateKey_ = jVar.q();
                                } else if (J != 58) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    Transaction transaction = this.transaction_;
                                    Transaction.Builder builder = transaction != null ? transaction.toBuilder() : null;
                                    Transaction transaction2 = (Transaction) jVar.z(Transaction.parser(), rVar);
                                    this.transaction_ = transaction2;
                                    if (builder != null) {
                                        builder.mergeFrom(transaction2);
                                        this.transaction_ = builder.buildPartial();
                                    }
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getChainId();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getGasLimit();

        ByteString getGasPrice();

        /* synthetic */ String getInitializationErrorString();

        ByteString getNonce();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        Transaction getTransaction();

        TransactionOrBuilder getTransactionOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransaction();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int DATA_FIELD_NUMBER = 5;
        public static final int ENCODED_FIELD_NUMBER = 1;
        public static final int R_FIELD_NUMBER = 3;
        public static final int S_FIELD_NUMBER = 4;
        public static final int V_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString data_;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private ByteString r_;
        private ByteString s_;
        private ByteString v_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Ethereum.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString data_;
            private ByteString encoded_;
            private ByteString r_;
            private ByteString s_;
            private ByteString v_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearData() {
                this.data_ = SigningOutput.getDefaultInstance().getData();
                onChanged();
                return this;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearR() {
                this.r_ = SigningOutput.getDefaultInstance().getR();
                onChanged();
                return this;
            }

            public Builder clearS() {
                this.s_ = SigningOutput.getDefaultInstance().getS();
                onChanged();
                return this;
            }

            public Builder clearV() {
                this.v_ = SigningOutput.getDefaultInstance().getV();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Ethereum.internal_static_TW_Ethereum_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
            public ByteString getR() {
                return this.r_;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
            public ByteString getS() {
                return this.s_;
            }

            @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
            public ByteString getV() {
                return this.v_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setData(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.data_ = byteString;
                onChanged();
                return this;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setR(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.r_ = byteString;
                onChanged();
                return this;
            }

            public Builder setS(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.s_ = byteString;
                onChanged();
                return this;
            }

            public Builder setV(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.v_ = byteString;
                onChanged();
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.v_ = byteString;
                this.r_ = byteString;
                this.s_ = byteString;
                this.data_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this, (AnonymousClass1) null);
                signingOutput.encoded_ = this.encoded_;
                signingOutput.v_ = this.v_;
                signingOutput.r_ = this.r_;
                signingOutput.s_ = this.s_;
                signingOutput.data_ = this.data_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.v_ = byteString;
                this.r_ = byteString;
                this.s_ = byteString;
                this.data_ = byteString;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.v_ = byteString;
                this.r_ = byteString;
                this.s_ = byteString;
                this.data_ = byteString;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                ByteString encoded = signingOutput.getEncoded();
                ByteString byteString = ByteString.EMPTY;
                if (encoded != byteString) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (signingOutput.getV() != byteString) {
                    setV(signingOutput.getV());
                }
                if (signingOutput.getR() != byteString) {
                    setR(signingOutput.getR());
                }
                if (signingOutput.getS() != byteString) {
                    setS(signingOutput.getS());
                }
                if (signingOutput.getData() != byteString) {
                    setData(signingOutput.getData());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Ethereum.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.SigningOutput.access$11600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Ethereum$SigningOutput r3 = (wallet.core.jni.proto.Ethereum.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Ethereum$SigningOutput r4 = (wallet.core.jni.proto.Ethereum.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$SigningOutput$Builder");
            }
        }

        public /* synthetic */ SigningOutput(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Ethereum.internal_static_TW_Ethereum_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && getV().equals(signingOutput.getV()) && getR().equals(signingOutput.getR()) && getS().equals(signingOutput.getS()) && getData().equals(signingOutput.getData()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
        public ByteString getData() {
            return this.data_;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
        public ByteString getR() {
            return this.r_;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
        public ByteString getS() {
            return this.s_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_);
            if (!this.v_.isEmpty()) {
                h += CodedOutputStream.h(2, this.v_);
            }
            if (!this.r_.isEmpty()) {
                h += CodedOutputStream.h(3, this.r_);
            }
            if (!this.s_.isEmpty()) {
                h += CodedOutputStream.h(4, this.s_);
            }
            if (!this.data_.isEmpty()) {
                h += CodedOutputStream.h(5, this.data_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Ethereum.SigningOutputOrBuilder
        public ByteString getV() {
            return this.v_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 37) + 2) * 53) + getV().hashCode()) * 37) + 3) * 53) + getR().hashCode()) * 37) + 4) * 53) + getS().hashCode()) * 37) + 5) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Ethereum.internal_static_TW_Ethereum_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            if (!this.v_.isEmpty()) {
                codedOutputStream.q0(2, this.v_);
            }
            if (!this.r_.isEmpty()) {
                codedOutputStream.q0(3, this.r_);
            }
            if (!this.s_.isEmpty()) {
                codedOutputStream.q0(4, this.s_);
            }
            if (!this.data_.isEmpty()) {
                codedOutputStream.q0(5, this.data_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ SigningOutput(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.encoded_ = byteString;
            this.v_ = byteString;
            this.r_ = byteString;
            this.s_ = byteString;
            this.data_ = byteString;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.encoded_ = jVar.q();
                                } else if (J == 18) {
                                    this.v_ = jVar.q();
                                } else if (J == 26) {
                                    this.r_ = jVar.q();
                                } else if (J == 34) {
                                    this.s_ = jVar.q();
                                } else if (J != 42) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.data_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (IOException e) {
                            throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                        }
                    } catch (InvalidProtocolBufferException e2) {
                        throw e2.setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        ByteString getData();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getR();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getS();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        ByteString getV();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class Transaction extends GeneratedMessageV3 implements TransactionOrBuilder {
        public static final int CONTRACT_GENERIC_FIELD_NUMBER = 6;
        public static final int ERC1155_TRANSFER_FIELD_NUMBER = 5;
        public static final int ERC20_APPROVE_FIELD_NUMBER = 3;
        public static final int ERC20_TRANSFER_FIELD_NUMBER = 2;
        public static final int ERC721_TRANSFER_FIELD_NUMBER = 4;
        public static final int TRANSFER_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private byte memoizedIsInitialized;
        private int transactionOneofCase_;
        private Object transactionOneof_;
        private static final Transaction DEFAULT_INSTANCE = new Transaction();
        private static final t0<Transaction> PARSER = new c<Transaction>() { // from class: wallet.core.jni.proto.Ethereum.Transaction.1
            @Override // com.google.protobuf.t0
            public Transaction parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new Transaction(jVar, rVar, null);
            }
        };

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOrBuilder {
            private a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> contractGenericBuilder_;
            private a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> erc1155TransferBuilder_;
            private a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> erc20ApproveBuilder_;
            private a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> erc20TransferBuilder_;
            private a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> erc721TransferBuilder_;
            private int transactionOneofCase_;
            private Object transactionOneof_;
            private a1<Transfer, Transfer.Builder, TransferOrBuilder> transferBuilder_;

            public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                this(cVar);
            }

            private a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> getContractGenericFieldBuilder() {
                if (this.contractGenericBuilder_ == null) {
                    if (this.transactionOneofCase_ != 6) {
                        this.transactionOneof_ = ContractGeneric.getDefaultInstance();
                    }
                    this.contractGenericBuilder_ = new a1<>((ContractGeneric) this.transactionOneof_, getParentForChildren(), isClean());
                    this.transactionOneof_ = null;
                }
                this.transactionOneofCase_ = 6;
                onChanged();
                return this.contractGenericBuilder_;
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_descriptor;
            }

            private a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> getErc1155TransferFieldBuilder() {
                if (this.erc1155TransferBuilder_ == null) {
                    if (this.transactionOneofCase_ != 5) {
                        this.transactionOneof_ = ERC1155Transfer.getDefaultInstance();
                    }
                    this.erc1155TransferBuilder_ = new a1<>((ERC1155Transfer) this.transactionOneof_, getParentForChildren(), isClean());
                    this.transactionOneof_ = null;
                }
                this.transactionOneofCase_ = 5;
                onChanged();
                return this.erc1155TransferBuilder_;
            }

            private a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> getErc20ApproveFieldBuilder() {
                if (this.erc20ApproveBuilder_ == null) {
                    if (this.transactionOneofCase_ != 3) {
                        this.transactionOneof_ = ERC20Approve.getDefaultInstance();
                    }
                    this.erc20ApproveBuilder_ = new a1<>((ERC20Approve) this.transactionOneof_, getParentForChildren(), isClean());
                    this.transactionOneof_ = null;
                }
                this.transactionOneofCase_ = 3;
                onChanged();
                return this.erc20ApproveBuilder_;
            }

            private a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> getErc20TransferFieldBuilder() {
                if (this.erc20TransferBuilder_ == null) {
                    if (this.transactionOneofCase_ != 2) {
                        this.transactionOneof_ = ERC20Transfer.getDefaultInstance();
                    }
                    this.erc20TransferBuilder_ = new a1<>((ERC20Transfer) this.transactionOneof_, getParentForChildren(), isClean());
                    this.transactionOneof_ = null;
                }
                this.transactionOneofCase_ = 2;
                onChanged();
                return this.erc20TransferBuilder_;
            }

            private a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> getErc721TransferFieldBuilder() {
                if (this.erc721TransferBuilder_ == null) {
                    if (this.transactionOneofCase_ != 4) {
                        this.transactionOneof_ = ERC721Transfer.getDefaultInstance();
                    }
                    this.erc721TransferBuilder_ = new a1<>((ERC721Transfer) this.transactionOneof_, getParentForChildren(), isClean());
                    this.transactionOneof_ = null;
                }
                this.transactionOneofCase_ = 4;
                onChanged();
                return this.erc721TransferBuilder_;
            }

            private a1<Transfer, Transfer.Builder, TransferOrBuilder> getTransferFieldBuilder() {
                if (this.transferBuilder_ == null) {
                    if (this.transactionOneofCase_ != 1) {
                        this.transactionOneof_ = Transfer.getDefaultInstance();
                    }
                    this.transferBuilder_ = new a1<>((Transfer) this.transactionOneof_, getParentForChildren(), isClean());
                    this.transactionOneof_ = null;
                }
                this.transactionOneofCase_ = 1;
                onChanged();
                return this.transferBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearContractGeneric() {
                a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> a1Var = this.contractGenericBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 6) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionOneofCase_ == 6) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearErc1155Transfer() {
                a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> a1Var = this.erc1155TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 5) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionOneofCase_ == 5) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearErc20Approve() {
                a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> a1Var = this.erc20ApproveBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 3) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionOneofCase_ == 3) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearErc20Transfer() {
                a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> a1Var = this.erc20TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 2) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionOneofCase_ == 2) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearErc721Transfer() {
                a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> a1Var = this.erc721TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 4) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionOneofCase_ == 4) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            public Builder clearTransactionOneof() {
                this.transactionOneofCase_ = 0;
                this.transactionOneof_ = null;
                onChanged();
                return this;
            }

            public Builder clearTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 1) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                        onChanged();
                    }
                } else {
                    if (this.transactionOneofCase_ == 1) {
                        this.transactionOneofCase_ = 0;
                        this.transactionOneof_ = null;
                    }
                    a1Var.c();
                }
                return this;
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ContractGeneric getContractGeneric() {
                a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> a1Var = this.contractGenericBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 6) {
                        return (ContractGeneric) this.transactionOneof_;
                    }
                    return ContractGeneric.getDefaultInstance();
                } else if (this.transactionOneofCase_ == 6) {
                    return a1Var.f();
                } else {
                    return ContractGeneric.getDefaultInstance();
                }
            }

            public ContractGeneric.Builder getContractGenericBuilder() {
                return getContractGenericFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ContractGenericOrBuilder getContractGenericOrBuilder() {
                a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> a1Var;
                int i = this.transactionOneofCase_;
                if (i != 6 || (a1Var = this.contractGenericBuilder_) == null) {
                    if (i == 6) {
                        return (ContractGeneric) this.transactionOneof_;
                    }
                    return ContractGeneric.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_descriptor;
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC1155Transfer getErc1155Transfer() {
                a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> a1Var = this.erc1155TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 5) {
                        return (ERC1155Transfer) this.transactionOneof_;
                    }
                    return ERC1155Transfer.getDefaultInstance();
                } else if (this.transactionOneofCase_ == 5) {
                    return a1Var.f();
                } else {
                    return ERC1155Transfer.getDefaultInstance();
                }
            }

            public ERC1155Transfer.Builder getErc1155TransferBuilder() {
                return getErc1155TransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC1155TransferOrBuilder getErc1155TransferOrBuilder() {
                a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> a1Var;
                int i = this.transactionOneofCase_;
                if (i != 5 || (a1Var = this.erc1155TransferBuilder_) == null) {
                    if (i == 5) {
                        return (ERC1155Transfer) this.transactionOneof_;
                    }
                    return ERC1155Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC20Approve getErc20Approve() {
                a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> a1Var = this.erc20ApproveBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 3) {
                        return (ERC20Approve) this.transactionOneof_;
                    }
                    return ERC20Approve.getDefaultInstance();
                } else if (this.transactionOneofCase_ == 3) {
                    return a1Var.f();
                } else {
                    return ERC20Approve.getDefaultInstance();
                }
            }

            public ERC20Approve.Builder getErc20ApproveBuilder() {
                return getErc20ApproveFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC20ApproveOrBuilder getErc20ApproveOrBuilder() {
                a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> a1Var;
                int i = this.transactionOneofCase_;
                if (i != 3 || (a1Var = this.erc20ApproveBuilder_) == null) {
                    if (i == 3) {
                        return (ERC20Approve) this.transactionOneof_;
                    }
                    return ERC20Approve.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC20Transfer getErc20Transfer() {
                a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> a1Var = this.erc20TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 2) {
                        return (ERC20Transfer) this.transactionOneof_;
                    }
                    return ERC20Transfer.getDefaultInstance();
                } else if (this.transactionOneofCase_ == 2) {
                    return a1Var.f();
                } else {
                    return ERC20Transfer.getDefaultInstance();
                }
            }

            public ERC20Transfer.Builder getErc20TransferBuilder() {
                return getErc20TransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC20TransferOrBuilder getErc20TransferOrBuilder() {
                a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> a1Var;
                int i = this.transactionOneofCase_;
                if (i != 2 || (a1Var = this.erc20TransferBuilder_) == null) {
                    if (i == 2) {
                        return (ERC20Transfer) this.transactionOneof_;
                    }
                    return ERC20Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC721Transfer getErc721Transfer() {
                a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> a1Var = this.erc721TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 4) {
                        return (ERC721Transfer) this.transactionOneof_;
                    }
                    return ERC721Transfer.getDefaultInstance();
                } else if (this.transactionOneofCase_ == 4) {
                    return a1Var.f();
                } else {
                    return ERC721Transfer.getDefaultInstance();
                }
            }

            public ERC721Transfer.Builder getErc721TransferBuilder() {
                return getErc721TransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public ERC721TransferOrBuilder getErc721TransferOrBuilder() {
                a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> a1Var;
                int i = this.transactionOneofCase_;
                if (i != 4 || (a1Var = this.erc721TransferBuilder_) == null) {
                    if (i == 4) {
                        return (ERC721Transfer) this.transactionOneof_;
                    }
                    return ERC721Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public TransactionOneofCase getTransactionOneofCase() {
                return TransactionOneofCase.forNumber(this.transactionOneofCase_);
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public Transfer getTransfer() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 1) {
                        return (Transfer) this.transactionOneof_;
                    }
                    return Transfer.getDefaultInstance();
                } else if (this.transactionOneofCase_ == 1) {
                    return a1Var.f();
                } else {
                    return Transfer.getDefaultInstance();
                }
            }

            public Transfer.Builder getTransferBuilder() {
                return getTransferFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public TransferOrBuilder getTransferOrBuilder() {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var;
                int i = this.transactionOneofCase_;
                if (i != 1 || (a1Var = this.transferBuilder_) == null) {
                    if (i == 1) {
                        return (Transfer) this.transactionOneof_;
                    }
                    return Transfer.getDefaultInstance();
                }
                return a1Var.g();
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public boolean hasContractGeneric() {
                return this.transactionOneofCase_ == 6;
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public boolean hasErc1155Transfer() {
                return this.transactionOneofCase_ == 5;
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public boolean hasErc20Approve() {
                return this.transactionOneofCase_ == 3;
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public boolean hasErc20Transfer() {
                return this.transactionOneofCase_ == 2;
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public boolean hasErc721Transfer() {
                return this.transactionOneofCase_ == 4;
            }

            @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
            public boolean hasTransfer() {
                return this.transactionOneofCase_ == 1;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeContractGeneric(ContractGeneric contractGeneric) {
                a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> a1Var = this.contractGenericBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 6 && this.transactionOneof_ != ContractGeneric.getDefaultInstance()) {
                        this.transactionOneof_ = ContractGeneric.newBuilder((ContractGeneric) this.transactionOneof_).mergeFrom(contractGeneric).buildPartial();
                    } else {
                        this.transactionOneof_ = contractGeneric;
                    }
                    onChanged();
                } else {
                    if (this.transactionOneofCase_ == 6) {
                        a1Var.h(contractGeneric);
                    }
                    this.contractGenericBuilder_.j(contractGeneric);
                }
                this.transactionOneofCase_ = 6;
                return this;
            }

            public Builder mergeErc1155Transfer(ERC1155Transfer eRC1155Transfer) {
                a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> a1Var = this.erc1155TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 5 && this.transactionOneof_ != ERC1155Transfer.getDefaultInstance()) {
                        this.transactionOneof_ = ERC1155Transfer.newBuilder((ERC1155Transfer) this.transactionOneof_).mergeFrom(eRC1155Transfer).buildPartial();
                    } else {
                        this.transactionOneof_ = eRC1155Transfer;
                    }
                    onChanged();
                } else {
                    if (this.transactionOneofCase_ == 5) {
                        a1Var.h(eRC1155Transfer);
                    }
                    this.erc1155TransferBuilder_.j(eRC1155Transfer);
                }
                this.transactionOneofCase_ = 5;
                return this;
            }

            public Builder mergeErc20Approve(ERC20Approve eRC20Approve) {
                a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> a1Var = this.erc20ApproveBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 3 && this.transactionOneof_ != ERC20Approve.getDefaultInstance()) {
                        this.transactionOneof_ = ERC20Approve.newBuilder((ERC20Approve) this.transactionOneof_).mergeFrom(eRC20Approve).buildPartial();
                    } else {
                        this.transactionOneof_ = eRC20Approve;
                    }
                    onChanged();
                } else {
                    if (this.transactionOneofCase_ == 3) {
                        a1Var.h(eRC20Approve);
                    }
                    this.erc20ApproveBuilder_.j(eRC20Approve);
                }
                this.transactionOneofCase_ = 3;
                return this;
            }

            public Builder mergeErc20Transfer(ERC20Transfer eRC20Transfer) {
                a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> a1Var = this.erc20TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 2 && this.transactionOneof_ != ERC20Transfer.getDefaultInstance()) {
                        this.transactionOneof_ = ERC20Transfer.newBuilder((ERC20Transfer) this.transactionOneof_).mergeFrom(eRC20Transfer).buildPartial();
                    } else {
                        this.transactionOneof_ = eRC20Transfer;
                    }
                    onChanged();
                } else {
                    if (this.transactionOneofCase_ == 2) {
                        a1Var.h(eRC20Transfer);
                    }
                    this.erc20TransferBuilder_.j(eRC20Transfer);
                }
                this.transactionOneofCase_ = 2;
                return this;
            }

            public Builder mergeErc721Transfer(ERC721Transfer eRC721Transfer) {
                a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> a1Var = this.erc721TransferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 4 && this.transactionOneof_ != ERC721Transfer.getDefaultInstance()) {
                        this.transactionOneof_ = ERC721Transfer.newBuilder((ERC721Transfer) this.transactionOneof_).mergeFrom(eRC721Transfer).buildPartial();
                    } else {
                        this.transactionOneof_ = eRC721Transfer;
                    }
                    onChanged();
                } else {
                    if (this.transactionOneofCase_ == 4) {
                        a1Var.h(eRC721Transfer);
                    }
                    this.erc721TransferBuilder_.j(eRC721Transfer);
                }
                this.transactionOneofCase_ = 4;
                return this;
            }

            public Builder mergeTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    if (this.transactionOneofCase_ == 1 && this.transactionOneof_ != Transfer.getDefaultInstance()) {
                        this.transactionOneof_ = Transfer.newBuilder((Transfer) this.transactionOneof_).mergeFrom(transfer).buildPartial();
                    } else {
                        this.transactionOneof_ = transfer;
                    }
                    onChanged();
                } else {
                    if (this.transactionOneofCase_ == 1) {
                        a1Var.h(transfer);
                    }
                    this.transferBuilder_.j(transfer);
                }
                this.transactionOneofCase_ = 1;
                return this;
            }

            public Builder setContractGeneric(ContractGeneric contractGeneric) {
                a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> a1Var = this.contractGenericBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(contractGeneric);
                    this.transactionOneof_ = contractGeneric;
                    onChanged();
                } else {
                    a1Var.j(contractGeneric);
                }
                this.transactionOneofCase_ = 6;
                return this;
            }

            public Builder setErc1155Transfer(ERC1155Transfer eRC1155Transfer) {
                a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> a1Var = this.erc1155TransferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(eRC1155Transfer);
                    this.transactionOneof_ = eRC1155Transfer;
                    onChanged();
                } else {
                    a1Var.j(eRC1155Transfer);
                }
                this.transactionOneofCase_ = 5;
                return this;
            }

            public Builder setErc20Approve(ERC20Approve eRC20Approve) {
                a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> a1Var = this.erc20ApproveBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(eRC20Approve);
                    this.transactionOneof_ = eRC20Approve;
                    onChanged();
                } else {
                    a1Var.j(eRC20Approve);
                }
                this.transactionOneofCase_ = 3;
                return this;
            }

            public Builder setErc20Transfer(ERC20Transfer eRC20Transfer) {
                a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> a1Var = this.erc20TransferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(eRC20Transfer);
                    this.transactionOneof_ = eRC20Transfer;
                    onChanged();
                } else {
                    a1Var.j(eRC20Transfer);
                }
                this.transactionOneofCase_ = 2;
                return this;
            }

            public Builder setErc721Transfer(ERC721Transfer eRC721Transfer) {
                a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> a1Var = this.erc721TransferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(eRC721Transfer);
                    this.transactionOneof_ = eRC721Transfer;
                    onChanged();
                } else {
                    a1Var.j(eRC721Transfer);
                }
                this.transactionOneofCase_ = 4;
                return this;
            }

            public Builder setTransfer(Transfer transfer) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transfer);
                    this.transactionOneof_ = transfer;
                    onChanged();
                } else {
                    a1Var.j(transfer);
                }
                this.transactionOneofCase_ = 1;
                return this;
            }

            public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                this();
            }

            private Builder() {
                this.transactionOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction build() {
                Transaction buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public Transaction buildPartial() {
                Transaction transaction = new Transaction(this, (AnonymousClass1) null);
                if (this.transactionOneofCase_ == 1) {
                    a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                    if (a1Var == null) {
                        transaction.transactionOneof_ = this.transactionOneof_;
                    } else {
                        transaction.transactionOneof_ = a1Var.b();
                    }
                }
                if (this.transactionOneofCase_ == 2) {
                    a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> a1Var2 = this.erc20TransferBuilder_;
                    if (a1Var2 == null) {
                        transaction.transactionOneof_ = this.transactionOneof_;
                    } else {
                        transaction.transactionOneof_ = a1Var2.b();
                    }
                }
                if (this.transactionOneofCase_ == 3) {
                    a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> a1Var3 = this.erc20ApproveBuilder_;
                    if (a1Var3 == null) {
                        transaction.transactionOneof_ = this.transactionOneof_;
                    } else {
                        transaction.transactionOneof_ = a1Var3.b();
                    }
                }
                if (this.transactionOneofCase_ == 4) {
                    a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> a1Var4 = this.erc721TransferBuilder_;
                    if (a1Var4 == null) {
                        transaction.transactionOneof_ = this.transactionOneof_;
                    } else {
                        transaction.transactionOneof_ = a1Var4.b();
                    }
                }
                if (this.transactionOneofCase_ == 5) {
                    a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> a1Var5 = this.erc1155TransferBuilder_;
                    if (a1Var5 == null) {
                        transaction.transactionOneof_ = this.transactionOneof_;
                    } else {
                        transaction.transactionOneof_ = a1Var5.b();
                    }
                }
                if (this.transactionOneofCase_ == 6) {
                    a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> a1Var6 = this.contractGenericBuilder_;
                    if (a1Var6 == null) {
                        transaction.transactionOneof_ = this.transactionOneof_;
                    } else {
                        transaction.transactionOneof_ = a1Var6.b();
                    }
                }
                transaction.transactionOneofCase_ = this.transactionOneofCase_;
                onBuilt();
                return transaction;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public Transaction getDefaultInstanceForType() {
                return Transaction.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.transactionOneofCase_ = 0;
                this.transactionOneof_ = null;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.transactionOneofCase_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof Transaction) {
                    return mergeFrom((Transaction) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setContractGeneric(ContractGeneric.Builder builder) {
                a1<ContractGeneric, ContractGeneric.Builder, ContractGenericOrBuilder> a1Var = this.contractGenericBuilder_;
                if (a1Var == null) {
                    this.transactionOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionOneofCase_ = 6;
                return this;
            }

            public Builder setErc1155Transfer(ERC1155Transfer.Builder builder) {
                a1<ERC1155Transfer, ERC1155Transfer.Builder, ERC1155TransferOrBuilder> a1Var = this.erc1155TransferBuilder_;
                if (a1Var == null) {
                    this.transactionOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionOneofCase_ = 5;
                return this;
            }

            public Builder setErc20Approve(ERC20Approve.Builder builder) {
                a1<ERC20Approve, ERC20Approve.Builder, ERC20ApproveOrBuilder> a1Var = this.erc20ApproveBuilder_;
                if (a1Var == null) {
                    this.transactionOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionOneofCase_ = 3;
                return this;
            }

            public Builder setErc20Transfer(ERC20Transfer.Builder builder) {
                a1<ERC20Transfer, ERC20Transfer.Builder, ERC20TransferOrBuilder> a1Var = this.erc20TransferBuilder_;
                if (a1Var == null) {
                    this.transactionOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionOneofCase_ = 2;
                return this;
            }

            public Builder setErc721Transfer(ERC721Transfer.Builder builder) {
                a1<ERC721Transfer, ERC721Transfer.Builder, ERC721TransferOrBuilder> a1Var = this.erc721TransferBuilder_;
                if (a1Var == null) {
                    this.transactionOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionOneofCase_ = 4;
                return this;
            }

            public Builder setTransfer(Transfer.Builder builder) {
                a1<Transfer, Transfer.Builder, TransferOrBuilder> a1Var = this.transferBuilder_;
                if (a1Var == null) {
                    this.transactionOneof_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                this.transactionOneofCase_ = 1;
                return this;
            }

            public Builder mergeFrom(Transaction transaction) {
                if (transaction == Transaction.getDefaultInstance()) {
                    return this;
                }
                switch (AnonymousClass1.$SwitchMap$wallet$core$jni$proto$Ethereum$Transaction$TransactionOneofCase[transaction.getTransactionOneofCase().ordinal()]) {
                    case 1:
                        mergeTransfer(transaction.getTransfer());
                        break;
                    case 2:
                        mergeErc20Transfer(transaction.getErc20Transfer());
                        break;
                    case 3:
                        mergeErc20Approve(transaction.getErc20Approve());
                        break;
                    case 4:
                        mergeErc721Transfer(transaction.getErc721Transfer());
                        break;
                    case 5:
                        mergeErc1155Transfer(transaction.getErc1155Transfer());
                        break;
                    case 6:
                        mergeContractGeneric(transaction.getContractGeneric());
                        break;
                }
                mergeUnknownFields(transaction.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Ethereum.Transaction.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.Transaction.access$8500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Ethereum$Transaction r3 = (wallet.core.jni.proto.Ethereum.Transaction) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Ethereum$Transaction r4 = (wallet.core.jni.proto.Ethereum.Transaction) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.Transaction.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$Transaction$Builder");
            }
        }

        /* loaded from: classes3.dex */
        public static final class ContractGeneric extends GeneratedMessageV3 implements ContractGenericOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 1;
            public static final int DATA_FIELD_NUMBER = 2;
            private static final ContractGeneric DEFAULT_INSTANCE = new ContractGeneric();
            private static final t0<ContractGeneric> PARSER = new c<ContractGeneric>() { // from class: wallet.core.jni.proto.Ethereum.Transaction.ContractGeneric.1
                @Override // com.google.protobuf.t0
                public ContractGeneric parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new ContractGeneric(jVar, rVar, null);
                }
            };
            private static final long serialVersionUID = 0;
            private ByteString amount_;
            private ByteString data_;
            private byte memoizedIsInitialized;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ContractGenericOrBuilder {
                private ByteString amount_;
                private ByteString data_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = ContractGeneric.getDefaultInstance().getAmount();
                    onChanged();
                    return this;
                }

                public Builder clearData() {
                    this.data_ = ContractGeneric.getDefaultInstance().getData();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ContractGenericOrBuilder
                public ByteString getAmount() {
                    return this.amount_;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ContractGenericOrBuilder
                public ByteString getData() {
                    return this.data_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_descriptor;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_fieldAccessorTable.d(ContractGeneric.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.amount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setData(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.data_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ContractGeneric build() {
                    ContractGeneric buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ContractGeneric buildPartial() {
                    ContractGeneric contractGeneric = new ContractGeneric(this, (AnonymousClass1) null);
                    contractGeneric.amount_ = this.amount_;
                    contractGeneric.data_ = this.data_;
                    onBuilt();
                    return contractGeneric;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public ContractGeneric getDefaultInstanceForType() {
                    return ContractGeneric.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.data_ = byteString;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ContractGeneric) {
                        return mergeFrom((ContractGeneric) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(ContractGeneric contractGeneric) {
                    if (contractGeneric == ContractGeneric.getDefaultInstance()) {
                        return this;
                    }
                    ByteString amount = contractGeneric.getAmount();
                    ByteString byteString = ByteString.EMPTY;
                    if (amount != byteString) {
                        setAmount(contractGeneric.getAmount());
                    }
                    if (contractGeneric.getData() != byteString) {
                        setData(contractGeneric.getData());
                    }
                    mergeUnknownFields(contractGeneric.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Ethereum.Transaction.ContractGeneric.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.Transaction.ContractGeneric.access$7600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Ethereum$Transaction$ContractGeneric r3 = (wallet.core.jni.proto.Ethereum.Transaction.ContractGeneric) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Ethereum$Transaction$ContractGeneric r4 = (wallet.core.jni.proto.Ethereum.Transaction.ContractGeneric) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.Transaction.ContractGeneric.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$Transaction$ContractGeneric$Builder");
                }
            }

            public /* synthetic */ ContractGeneric(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static ContractGeneric getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static ContractGeneric parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ContractGeneric) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ContractGeneric parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ContractGeneric> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ContractGeneric)) {
                    return super.equals(obj);
                }
                ContractGeneric contractGeneric = (ContractGeneric) obj;
                return getAmount().equals(contractGeneric.getAmount()) && getData().equals(contractGeneric.getData()) && this.unknownFields.equals(contractGeneric.unknownFields);
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ContractGenericOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ContractGenericOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ContractGeneric> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int h = this.amount_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.amount_);
                if (!this.data_.isEmpty()) {
                    h += CodedOutputStream.h(2, this.data_);
                }
                int serializedSize = h + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAmount().hashCode()) * 37) + 2) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_fieldAccessorTable.d(ContractGeneric.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ContractGeneric();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!this.amount_.isEmpty()) {
                    codedOutputStream.q0(1, this.amount_);
                }
                if (!this.data_.isEmpty()) {
                    codedOutputStream.q0(2, this.data_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ ContractGeneric(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(ContractGeneric contractGeneric) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(contractGeneric);
            }

            public static ContractGeneric parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private ContractGeneric(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ContractGeneric parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (ContractGeneric) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ContractGeneric parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ContractGeneric getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static ContractGeneric parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private ContractGeneric() {
                this.memoizedIsInitialized = (byte) -1;
                ByteString byteString = ByteString.EMPTY;
                this.amount_ = byteString;
                this.data_ = byteString;
            }

            public static ContractGeneric parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static ContractGeneric parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static ContractGeneric parseFrom(InputStream inputStream) throws IOException {
                return (ContractGeneric) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private ContractGeneric(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.amount_ = jVar.q();
                                    } else if (J != 18) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.data_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ContractGeneric parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (ContractGeneric) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static ContractGeneric parseFrom(j jVar) throws IOException {
                return (ContractGeneric) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static ContractGeneric parseFrom(j jVar, r rVar) throws IOException {
                return (ContractGeneric) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ContractGenericOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            ByteString getAmount();

            ByteString getData();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class ERC1155Transfer extends GeneratedMessageV3 implements ERC1155TransferOrBuilder {
            public static final int DATA_FIELD_NUMBER = 5;
            public static final int FROM_FIELD_NUMBER = 1;
            public static final int TOKEN_ID_FIELD_NUMBER = 3;
            public static final int TO_FIELD_NUMBER = 2;
            public static final int VALUE_FIELD_NUMBER = 4;
            private static final long serialVersionUID = 0;
            private ByteString data_;
            private volatile Object from_;
            private byte memoizedIsInitialized;
            private volatile Object to_;
            private ByteString tokenId_;
            private ByteString value_;
            private static final ERC1155Transfer DEFAULT_INSTANCE = new ERC1155Transfer();
            private static final t0<ERC1155Transfer> PARSER = new c<ERC1155Transfer>() { // from class: wallet.core.jni.proto.Ethereum.Transaction.ERC1155Transfer.1
                @Override // com.google.protobuf.t0
                public ERC1155Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new ERC1155Transfer(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ERC1155TransferOrBuilder {
                private ByteString data_;
                private Object from_;
                private Object to_;
                private ByteString tokenId_;
                private ByteString value_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearData() {
                    this.data_ = ERC1155Transfer.getDefaultInstance().getData();
                    onChanged();
                    return this;
                }

                public Builder clearFrom() {
                    this.from_ = ERC1155Transfer.getDefaultInstance().getFrom();
                    onChanged();
                    return this;
                }

                public Builder clearTo() {
                    this.to_ = ERC1155Transfer.getDefaultInstance().getTo();
                    onChanged();
                    return this;
                }

                public Builder clearTokenId() {
                    this.tokenId_ = ERC1155Transfer.getDefaultInstance().getTokenId();
                    onChanged();
                    return this;
                }

                public Builder clearValue() {
                    this.value_ = ERC1155Transfer.getDefaultInstance().getValue();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
                public ByteString getData() {
                    return this.data_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_descriptor;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
                public String getFrom() {
                    Object obj = this.from_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.from_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
                public ByteString getFromBytes() {
                    Object obj = this.from_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.from_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
                public String getTo() {
                    Object obj = this.to_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.to_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
                public ByteString getToBytes() {
                    Object obj = this.to_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.to_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
                public ByteString getTokenId() {
                    return this.tokenId_;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
                public ByteString getValue() {
                    return this.value_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_fieldAccessorTable.d(ERC1155Transfer.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setData(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.data_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setFrom(String str) {
                    Objects.requireNonNull(str);
                    this.from_ = str;
                    onChanged();
                    return this;
                }

                public Builder setFromBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.from_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTo(String str) {
                    Objects.requireNonNull(str);
                    this.to_ = str;
                    onChanged();
                    return this;
                }

                public Builder setToBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.to_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTokenId(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.tokenId_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setValue(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.value_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.from_ = "";
                    this.to_ = "";
                    ByteString byteString = ByteString.EMPTY;
                    this.tokenId_ = byteString;
                    this.value_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC1155Transfer build() {
                    ERC1155Transfer buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC1155Transfer buildPartial() {
                    ERC1155Transfer eRC1155Transfer = new ERC1155Transfer(this, (AnonymousClass1) null);
                    eRC1155Transfer.from_ = this.from_;
                    eRC1155Transfer.to_ = this.to_;
                    eRC1155Transfer.tokenId_ = this.tokenId_;
                    eRC1155Transfer.value_ = this.value_;
                    eRC1155Transfer.data_ = this.data_;
                    onBuilt();
                    return eRC1155Transfer;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public ERC1155Transfer getDefaultInstanceForType() {
                    return ERC1155Transfer.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.from_ = "";
                    this.to_ = "";
                    ByteString byteString = ByteString.EMPTY;
                    this.tokenId_ = byteString;
                    this.value_ = byteString;
                    this.data_ = byteString;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ERC1155Transfer) {
                        return mergeFrom((ERC1155Transfer) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.from_ = "";
                    this.to_ = "";
                    ByteString byteString = ByteString.EMPTY;
                    this.tokenId_ = byteString;
                    this.value_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(ERC1155Transfer eRC1155Transfer) {
                    if (eRC1155Transfer == ERC1155Transfer.getDefaultInstance()) {
                        return this;
                    }
                    if (!eRC1155Transfer.getFrom().isEmpty()) {
                        this.from_ = eRC1155Transfer.from_;
                        onChanged();
                    }
                    if (!eRC1155Transfer.getTo().isEmpty()) {
                        this.to_ = eRC1155Transfer.to_;
                        onChanged();
                    }
                    ByteString tokenId = eRC1155Transfer.getTokenId();
                    ByteString byteString = ByteString.EMPTY;
                    if (tokenId != byteString) {
                        setTokenId(eRC1155Transfer.getTokenId());
                    }
                    if (eRC1155Transfer.getValue() != byteString) {
                        setValue(eRC1155Transfer.getValue());
                    }
                    if (eRC1155Transfer.getData() != byteString) {
                        setData(eRC1155Transfer.getData());
                    }
                    mergeUnknownFields(eRC1155Transfer.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Ethereum.Transaction.ERC1155Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.Transaction.ERC1155Transfer.access$6300()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Ethereum$Transaction$ERC1155Transfer r3 = (wallet.core.jni.proto.Ethereum.Transaction.ERC1155Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Ethereum$Transaction$ERC1155Transfer r4 = (wallet.core.jni.proto.Ethereum.Transaction.ERC1155Transfer) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.Transaction.ERC1155Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$Transaction$ERC1155Transfer$Builder");
                }
            }

            public /* synthetic */ ERC1155Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static ERC1155Transfer getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static ERC1155Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ERC1155Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ERC1155Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ERC1155Transfer> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ERC1155Transfer)) {
                    return super.equals(obj);
                }
                ERC1155Transfer eRC1155Transfer = (ERC1155Transfer) obj;
                return getFrom().equals(eRC1155Transfer.getFrom()) && getTo().equals(eRC1155Transfer.getTo()) && getTokenId().equals(eRC1155Transfer.getTokenId()) && getValue().equals(eRC1155Transfer.getValue()) && getData().equals(eRC1155Transfer.getData()) && this.unknownFields.equals(eRC1155Transfer.unknownFields);
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
            public String getFrom() {
                Object obj = this.from_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.from_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
            public ByteString getFromBytes() {
                Object obj = this.from_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.from_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ERC1155Transfer> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getFromBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.from_);
                if (!getToBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.to_);
                }
                if (!this.tokenId_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(3, this.tokenId_);
                }
                if (!this.value_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(4, this.value_);
                }
                if (!this.data_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(5, this.data_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.to_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
            public ByteString getTokenId() {
                return this.tokenId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC1155TransferOrBuilder
            public ByteString getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFrom().hashCode()) * 37) + 2) * 53) + getTo().hashCode()) * 37) + 3) * 53) + getTokenId().hashCode()) * 37) + 4) * 53) + getValue().hashCode()) * 37) + 5) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_fieldAccessorTable.d(ERC1155Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ERC1155Transfer();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getFromBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.from_);
                }
                if (!getToBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.to_);
                }
                if (!this.tokenId_.isEmpty()) {
                    codedOutputStream.q0(3, this.tokenId_);
                }
                if (!this.value_.isEmpty()) {
                    codedOutputStream.q0(4, this.value_);
                }
                if (!this.data_.isEmpty()) {
                    codedOutputStream.q0(5, this.data_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ ERC1155Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(ERC1155Transfer eRC1155Transfer) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(eRC1155Transfer);
            }

            public static ERC1155Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private ERC1155Transfer(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ERC1155Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC1155Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ERC1155Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ERC1155Transfer getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static ERC1155Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private ERC1155Transfer() {
                this.memoizedIsInitialized = (byte) -1;
                this.from_ = "";
                this.to_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.tokenId_ = byteString;
                this.value_ = byteString;
                this.data_ = byteString;
            }

            public static ERC1155Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static ERC1155Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static ERC1155Transfer parseFrom(InputStream inputStream) throws IOException {
                return (ERC1155Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static ERC1155Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC1155Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static ERC1155Transfer parseFrom(j jVar) throws IOException {
                return (ERC1155Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            private ERC1155Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.from_ = jVar.I();
                                    } else if (J == 18) {
                                        this.to_ = jVar.I();
                                    } else if (J == 26) {
                                        this.tokenId_ = jVar.q();
                                    } else if (J == 34) {
                                        this.value_ = jVar.q();
                                    } else if (J != 42) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.data_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (IOException e) {
                                throw new InvalidProtocolBufferException(e).setUnfinishedMessage(this);
                            }
                        } catch (InvalidProtocolBufferException e2) {
                            throw e2.setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ERC1155Transfer parseFrom(j jVar, r rVar) throws IOException {
                return (ERC1155Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ERC1155TransferOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            ByteString getData();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            String getFrom();

            ByteString getFromBytes();

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getTo();

            ByteString getToBytes();

            ByteString getTokenId();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            ByteString getValue();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class ERC20Approve extends GeneratedMessageV3 implements ERC20ApproveOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 2;
            private static final ERC20Approve DEFAULT_INSTANCE = new ERC20Approve();
            private static final t0<ERC20Approve> PARSER = new c<ERC20Approve>() { // from class: wallet.core.jni.proto.Ethereum.Transaction.ERC20Approve.1
                @Override // com.google.protobuf.t0
                public ERC20Approve parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new ERC20Approve(jVar, rVar, null);
                }
            };
            public static final int SPENDER_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private ByteString amount_;
            private byte memoizedIsInitialized;
            private volatile Object spender_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ERC20ApproveOrBuilder {
                private ByteString amount_;
                private Object spender_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = ERC20Approve.getDefaultInstance().getAmount();
                    onChanged();
                    return this;
                }

                public Builder clearSpender() {
                    this.spender_ = ERC20Approve.getDefaultInstance().getSpender();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20ApproveOrBuilder
                public ByteString getAmount() {
                    return this.amount_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_descriptor;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20ApproveOrBuilder
                public String getSpender() {
                    Object obj = this.spender_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.spender_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20ApproveOrBuilder
                public ByteString getSpenderBytes() {
                    Object obj = this.spender_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.spender_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_fieldAccessorTable.d(ERC20Approve.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.amount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setSpender(String str) {
                    Objects.requireNonNull(str);
                    this.spender_ = str;
                    onChanged();
                    return this;
                }

                public Builder setSpenderBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.spender_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.spender_ = "";
                    this.amount_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC20Approve build() {
                    ERC20Approve buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC20Approve buildPartial() {
                    ERC20Approve eRC20Approve = new ERC20Approve(this, (AnonymousClass1) null);
                    eRC20Approve.spender_ = this.spender_;
                    eRC20Approve.amount_ = this.amount_;
                    onBuilt();
                    return eRC20Approve;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public ERC20Approve getDefaultInstanceForType() {
                    return ERC20Approve.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.spender_ = "";
                    this.amount_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.spender_ = "";
                    this.amount_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ERC20Approve) {
                        return mergeFrom((ERC20Approve) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(ERC20Approve eRC20Approve) {
                    if (eRC20Approve == ERC20Approve.getDefaultInstance()) {
                        return this;
                    }
                    if (!eRC20Approve.getSpender().isEmpty()) {
                        this.spender_ = eRC20Approve.spender_;
                        onChanged();
                    }
                    if (eRC20Approve.getAmount() != ByteString.EMPTY) {
                        setAmount(eRC20Approve.getAmount());
                    }
                    mergeUnknownFields(eRC20Approve.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Ethereum.Transaction.ERC20Approve.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.Transaction.ERC20Approve.access$3400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Ethereum$Transaction$ERC20Approve r3 = (wallet.core.jni.proto.Ethereum.Transaction.ERC20Approve) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Ethereum$Transaction$ERC20Approve r4 = (wallet.core.jni.proto.Ethereum.Transaction.ERC20Approve) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.Transaction.ERC20Approve.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$Transaction$ERC20Approve$Builder");
                }
            }

            public /* synthetic */ ERC20Approve(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static ERC20Approve getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static ERC20Approve parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ERC20Approve) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ERC20Approve parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ERC20Approve> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ERC20Approve)) {
                    return super.equals(obj);
                }
                ERC20Approve eRC20Approve = (ERC20Approve) obj;
                return getSpender().equals(eRC20Approve.getSpender()) && getAmount().equals(eRC20Approve.getAmount()) && this.unknownFields.equals(eRC20Approve.unknownFields);
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20ApproveOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ERC20Approve> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getSpenderBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.spender_);
                if (!this.amount_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(2, this.amount_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20ApproveOrBuilder
            public String getSpender() {
                Object obj = this.spender_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.spender_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20ApproveOrBuilder
            public ByteString getSpenderBytes() {
                Object obj = this.spender_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.spender_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getSpender().hashCode()) * 37) + 2) * 53) + getAmount().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_fieldAccessorTable.d(ERC20Approve.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ERC20Approve();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getSpenderBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.spender_);
                }
                if (!this.amount_.isEmpty()) {
                    codedOutputStream.q0(2, this.amount_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ ERC20Approve(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(ERC20Approve eRC20Approve) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(eRC20Approve);
            }

            public static ERC20Approve parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private ERC20Approve(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ERC20Approve parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC20Approve) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ERC20Approve parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ERC20Approve getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static ERC20Approve parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private ERC20Approve() {
                this.memoizedIsInitialized = (byte) -1;
                this.spender_ = "";
                this.amount_ = ByteString.EMPTY;
            }

            public static ERC20Approve parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static ERC20Approve parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static ERC20Approve parseFrom(InputStream inputStream) throws IOException {
                return (ERC20Approve) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private ERC20Approve(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.spender_ = jVar.I();
                                    } else if (J != 18) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.amount_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ERC20Approve parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC20Approve) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static ERC20Approve parseFrom(j jVar) throws IOException {
                return (ERC20Approve) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static ERC20Approve parseFrom(j jVar, r rVar) throws IOException {
                return (ERC20Approve) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ERC20ApproveOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            ByteString getAmount();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getSpender();

            ByteString getSpenderBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class ERC20Transfer extends GeneratedMessageV3 implements ERC20TransferOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 2;
            private static final ERC20Transfer DEFAULT_INSTANCE = new ERC20Transfer();
            private static final t0<ERC20Transfer> PARSER = new c<ERC20Transfer>() { // from class: wallet.core.jni.proto.Ethereum.Transaction.ERC20Transfer.1
                @Override // com.google.protobuf.t0
                public ERC20Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new ERC20Transfer(jVar, rVar, null);
                }
            };
            public static final int TO_FIELD_NUMBER = 1;
            private static final long serialVersionUID = 0;
            private ByteString amount_;
            private byte memoizedIsInitialized;
            private volatile Object to_;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ERC20TransferOrBuilder {
                private ByteString amount_;
                private Object to_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = ERC20Transfer.getDefaultInstance().getAmount();
                    onChanged();
                    return this;
                }

                public Builder clearTo() {
                    this.to_ = ERC20Transfer.getDefaultInstance().getTo();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20TransferOrBuilder
                public ByteString getAmount() {
                    return this.amount_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_descriptor;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20TransferOrBuilder
                public String getTo() {
                    Object obj = this.to_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.to_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20TransferOrBuilder
                public ByteString getToBytes() {
                    Object obj = this.to_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.to_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_fieldAccessorTable.d(ERC20Transfer.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.amount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTo(String str) {
                    Objects.requireNonNull(str);
                    this.to_ = str;
                    onChanged();
                    return this;
                }

                public Builder setToBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.to_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.to_ = "";
                    this.amount_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC20Transfer build() {
                    ERC20Transfer buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC20Transfer buildPartial() {
                    ERC20Transfer eRC20Transfer = new ERC20Transfer(this, (AnonymousClass1) null);
                    eRC20Transfer.to_ = this.to_;
                    eRC20Transfer.amount_ = this.amount_;
                    onBuilt();
                    return eRC20Transfer;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public ERC20Transfer getDefaultInstanceForType() {
                    return ERC20Transfer.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.to_ = "";
                    this.amount_ = ByteString.EMPTY;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.to_ = "";
                    this.amount_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ERC20Transfer) {
                        return mergeFrom((ERC20Transfer) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(ERC20Transfer eRC20Transfer) {
                    if (eRC20Transfer == ERC20Transfer.getDefaultInstance()) {
                        return this;
                    }
                    if (!eRC20Transfer.getTo().isEmpty()) {
                        this.to_ = eRC20Transfer.to_;
                        onChanged();
                    }
                    if (eRC20Transfer.getAmount() != ByteString.EMPTY) {
                        setAmount(eRC20Transfer.getAmount());
                    }
                    mergeUnknownFields(eRC20Transfer.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Ethereum.Transaction.ERC20Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.Transaction.ERC20Transfer.access$2200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Ethereum$Transaction$ERC20Transfer r3 = (wallet.core.jni.proto.Ethereum.Transaction.ERC20Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Ethereum$Transaction$ERC20Transfer r4 = (wallet.core.jni.proto.Ethereum.Transaction.ERC20Transfer) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.Transaction.ERC20Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$Transaction$ERC20Transfer$Builder");
                }
            }

            public /* synthetic */ ERC20Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static ERC20Transfer getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static ERC20Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ERC20Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ERC20Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ERC20Transfer> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ERC20Transfer)) {
                    return super.equals(obj);
                }
                ERC20Transfer eRC20Transfer = (ERC20Transfer) obj;
                return getTo().equals(eRC20Transfer.getTo()) && getAmount().equals(eRC20Transfer.getAmount()) && this.unknownFields.equals(eRC20Transfer.unknownFields);
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20TransferOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ERC20Transfer> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getToBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.to_);
                if (!this.amount_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(2, this.amount_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20TransferOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.to_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC20TransferOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getTo().hashCode()) * 37) + 2) * 53) + getAmount().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_fieldAccessorTable.d(ERC20Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ERC20Transfer();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getToBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.to_);
                }
                if (!this.amount_.isEmpty()) {
                    codedOutputStream.q0(2, this.amount_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ ERC20Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(ERC20Transfer eRC20Transfer) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(eRC20Transfer);
            }

            public static ERC20Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private ERC20Transfer(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ERC20Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC20Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ERC20Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ERC20Transfer getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static ERC20Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private ERC20Transfer() {
                this.memoizedIsInitialized = (byte) -1;
                this.to_ = "";
                this.amount_ = ByteString.EMPTY;
            }

            public static ERC20Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static ERC20Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static ERC20Transfer parseFrom(InputStream inputStream) throws IOException {
                return (ERC20Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private ERC20Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.to_ = jVar.I();
                                    } else if (J != 18) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.amount_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ERC20Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC20Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static ERC20Transfer parseFrom(j jVar) throws IOException {
                return (ERC20Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static ERC20Transfer parseFrom(j jVar, r rVar) throws IOException {
                return (ERC20Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ERC20TransferOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            ByteString getAmount();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getTo();

            ByteString getToBytes();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public static final class ERC721Transfer extends GeneratedMessageV3 implements ERC721TransferOrBuilder {
            public static final int FROM_FIELD_NUMBER = 1;
            public static final int TOKEN_ID_FIELD_NUMBER = 3;
            public static final int TO_FIELD_NUMBER = 2;
            private static final long serialVersionUID = 0;
            private volatile Object from_;
            private byte memoizedIsInitialized;
            private volatile Object to_;
            private ByteString tokenId_;
            private static final ERC721Transfer DEFAULT_INSTANCE = new ERC721Transfer();
            private static final t0<ERC721Transfer> PARSER = new c<ERC721Transfer>() { // from class: wallet.core.jni.proto.Ethereum.Transaction.ERC721Transfer.1
                @Override // com.google.protobuf.t0
                public ERC721Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new ERC721Transfer(jVar, rVar, null);
                }
            };

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements ERC721TransferOrBuilder {
                private Object from_;
                private Object to_;
                private ByteString tokenId_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearFrom() {
                    this.from_ = ERC721Transfer.getDefaultInstance().getFrom();
                    onChanged();
                    return this;
                }

                public Builder clearTo() {
                    this.to_ = ERC721Transfer.getDefaultInstance().getTo();
                    onChanged();
                    return this;
                }

                public Builder clearTokenId() {
                    this.tokenId_ = ERC721Transfer.getDefaultInstance().getTokenId();
                    onChanged();
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_descriptor;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
                public String getFrom() {
                    Object obj = this.from_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.from_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
                public ByteString getFromBytes() {
                    Object obj = this.from_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.from_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
                public String getTo() {
                    Object obj = this.to_;
                    if (!(obj instanceof String)) {
                        String stringUtf8 = ((ByteString) obj).toStringUtf8();
                        this.to_ = stringUtf8;
                        return stringUtf8;
                    }
                    return (String) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
                public ByteString getToBytes() {
                    Object obj = this.to_;
                    if (obj instanceof String) {
                        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                        this.to_ = copyFromUtf8;
                        return copyFromUtf8;
                    }
                    return (ByteString) obj;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
                public ByteString getTokenId() {
                    return this.tokenId_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_fieldAccessorTable.d(ERC721Transfer.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setFrom(String str) {
                    Objects.requireNonNull(str);
                    this.from_ = str;
                    onChanged();
                    return this;
                }

                public Builder setFromBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.from_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTo(String str) {
                    Objects.requireNonNull(str);
                    this.to_ = str;
                    onChanged();
                    return this;
                }

                public Builder setToBytes(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    b.checkByteStringIsUtf8(byteString);
                    this.to_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setTokenId(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.tokenId_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    this.from_ = "";
                    this.to_ = "";
                    this.tokenId_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC721Transfer build() {
                    ERC721Transfer buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public ERC721Transfer buildPartial() {
                    ERC721Transfer eRC721Transfer = new ERC721Transfer(this, (AnonymousClass1) null);
                    eRC721Transfer.from_ = this.from_;
                    eRC721Transfer.to_ = this.to_;
                    eRC721Transfer.tokenId_ = this.tokenId_;
                    onBuilt();
                    return eRC721Transfer;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public ERC721Transfer getDefaultInstanceForType() {
                    return ERC721Transfer.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    this.from_ = "";
                    this.to_ = "";
                    this.tokenId_ = ByteString.EMPTY;
                    return this;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof ERC721Transfer) {
                        return mergeFrom((ERC721Transfer) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    this.from_ = "";
                    this.to_ = "";
                    this.tokenId_ = ByteString.EMPTY;
                    maybeForceBuilderInitialization();
                }

                public Builder mergeFrom(ERC721Transfer eRC721Transfer) {
                    if (eRC721Transfer == ERC721Transfer.getDefaultInstance()) {
                        return this;
                    }
                    if (!eRC721Transfer.getFrom().isEmpty()) {
                        this.from_ = eRC721Transfer.from_;
                        onChanged();
                    }
                    if (!eRC721Transfer.getTo().isEmpty()) {
                        this.to_ = eRC721Transfer.to_;
                        onChanged();
                    }
                    if (eRC721Transfer.getTokenId() != ByteString.EMPTY) {
                        setTokenId(eRC721Transfer.getTokenId());
                    }
                    mergeUnknownFields(eRC721Transfer.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Ethereum.Transaction.ERC721Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.Transaction.ERC721Transfer.access$4700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Ethereum$Transaction$ERC721Transfer r3 = (wallet.core.jni.proto.Ethereum.Transaction.ERC721Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Ethereum$Transaction$ERC721Transfer r4 = (wallet.core.jni.proto.Ethereum.Transaction.ERC721Transfer) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.Transaction.ERC721Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$Transaction$ERC721Transfer$Builder");
                }
            }

            public /* synthetic */ ERC721Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static ERC721Transfer getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static ERC721Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ERC721Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static ERC721Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<ERC721Transfer> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof ERC721Transfer)) {
                    return super.equals(obj);
                }
                ERC721Transfer eRC721Transfer = (ERC721Transfer) obj;
                return getFrom().equals(eRC721Transfer.getFrom()) && getTo().equals(eRC721Transfer.getTo()) && getTokenId().equals(eRC721Transfer.getTokenId()) && this.unknownFields.equals(eRC721Transfer.unknownFields);
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
            public String getFrom() {
                Object obj = this.from_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.from_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
            public ByteString getFromBytes() {
                Object obj = this.from_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.from_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<ERC721Transfer> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int computeStringSize = getFromBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.from_);
                if (!getToBytes().isEmpty()) {
                    computeStringSize += GeneratedMessageV3.computeStringSize(2, this.to_);
                }
                if (!this.tokenId_.isEmpty()) {
                    computeStringSize += CodedOutputStream.h(3, this.tokenId_);
                }
                int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
            public String getTo() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.to_ = stringUtf8;
                return stringUtf8;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
            public ByteString getToBytes() {
                Object obj = this.to_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.to_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.ERC721TransferOrBuilder
            public ByteString getTokenId() {
                return this.tokenId_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getFrom().hashCode()) * 37) + 2) * 53) + getTo().hashCode()) * 37) + 3) * 53) + getTokenId().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_fieldAccessorTable.d(ERC721Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new ERC721Transfer();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!getFromBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 1, this.from_);
                }
                if (!getToBytes().isEmpty()) {
                    GeneratedMessageV3.writeString(codedOutputStream, 2, this.to_);
                }
                if (!this.tokenId_.isEmpty()) {
                    codedOutputStream.q0(3, this.tokenId_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ ERC721Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(ERC721Transfer eRC721Transfer) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(eRC721Transfer);
            }

            public static ERC721Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private ERC721Transfer(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static ERC721Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC721Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static ERC721Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public ERC721Transfer getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static ERC721Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private ERC721Transfer() {
                this.memoizedIsInitialized = (byte) -1;
                this.from_ = "";
                this.to_ = "";
                this.tokenId_ = ByteString.EMPTY;
            }

            public static ERC721Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static ERC721Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static ERC721Transfer parseFrom(InputStream inputStream) throws IOException {
                return (ERC721Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            public static ERC721Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (ERC721Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            private ERC721Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.from_ = jVar.I();
                                } else if (J == 18) {
                                    this.to_ = jVar.I();
                                } else if (J != 26) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.tokenId_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static ERC721Transfer parseFrom(j jVar) throws IOException {
                return (ERC721Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static ERC721Transfer parseFrom(j jVar, r rVar) throws IOException {
                return (ERC721Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface ERC721TransferOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            String getFrom();

            ByteString getFromBytes();

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            String getTo();

            ByteString getToBytes();

            ByteString getTokenId();

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        /* loaded from: classes3.dex */
        public enum TransactionOneofCase implements a0.c {
            TRANSFER(1),
            ERC20_TRANSFER(2),
            ERC20_APPROVE(3),
            ERC721_TRANSFER(4),
            ERC1155_TRANSFER(5),
            CONTRACT_GENERIC(6),
            TRANSACTIONONEOF_NOT_SET(0);
            
            private final int value;

            TransactionOneofCase(int i) {
                this.value = i;
            }

            public static TransactionOneofCase forNumber(int i) {
                switch (i) {
                    case 0:
                        return TRANSACTIONONEOF_NOT_SET;
                    case 1:
                        return TRANSFER;
                    case 2:
                        return ERC20_TRANSFER;
                    case 3:
                        return ERC20_APPROVE;
                    case 4:
                        return ERC721_TRANSFER;
                    case 5:
                        return ERC1155_TRANSFER;
                    case 6:
                        return CONTRACT_GENERIC;
                    default:
                        return null;
                }
            }

            @Override // com.google.protobuf.a0.c
            public int getNumber() {
                return this.value;
            }

            @Deprecated
            public static TransactionOneofCase valueOf(int i) {
                return forNumber(i);
            }
        }

        /* loaded from: classes3.dex */
        public static final class Transfer extends GeneratedMessageV3 implements TransferOrBuilder {
            public static final int AMOUNT_FIELD_NUMBER = 1;
            public static final int DATA_FIELD_NUMBER = 2;
            private static final Transfer DEFAULT_INSTANCE = new Transfer();
            private static final t0<Transfer> PARSER = new c<Transfer>() { // from class: wallet.core.jni.proto.Ethereum.Transaction.Transfer.1
                @Override // com.google.protobuf.t0
                public Transfer parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                    return new Transfer(jVar, rVar, null);
                }
            };
            private static final long serialVersionUID = 0;
            private ByteString amount_;
            private ByteString data_;
            private byte memoizedIsInitialized;

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransferOrBuilder {
                private ByteString amount_;
                private ByteString data_;

                public /* synthetic */ Builder(GeneratedMessageV3.c cVar, AnonymousClass1 anonymousClass1) {
                    this(cVar);
                }

                public static final Descriptors.b getDescriptor() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_Transfer_descriptor;
                }

                private void maybeForceBuilderInitialization() {
                    boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
                }

                public Builder clearAmount() {
                    this.amount_ = Transfer.getDefaultInstance().getAmount();
                    onChanged();
                    return this;
                }

                public Builder clearData() {
                    this.data_ = Transfer.getDefaultInstance().getData();
                    onChanged();
                    return this;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.TransferOrBuilder
                public ByteString getAmount() {
                    return this.amount_;
                }

                @Override // wallet.core.jni.proto.Ethereum.Transaction.TransferOrBuilder
                public ByteString getData() {
                    return this.data_;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
                public Descriptors.b getDescriptorForType() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_Transfer_descriptor;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                    return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
                public final boolean isInitialized() {
                    return true;
                }

                public Builder setAmount(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.amount_ = byteString;
                    onChanged();
                    return this;
                }

                public Builder setData(ByteString byteString) {
                    Objects.requireNonNull(byteString);
                    this.data_ = byteString;
                    onChanged();
                    return this;
                }

                public /* synthetic */ Builder(AnonymousClass1 anonymousClass1) {
                    this();
                }

                private Builder() {
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.addRepeatedField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer build() {
                    Transfer buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
                }

                @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
                public Transfer buildPartial() {
                    Transfer transfer = new Transfer(this, (AnonymousClass1) null);
                    transfer.amount_ = this.amount_;
                    transfer.data_ = this.data_;
                    onBuilt();
                    return transfer;
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                    return (Builder) super.clearField(fieldDescriptor);
                }

                @Override // defpackage.d82, com.google.protobuf.o0
                public Transfer getDefaultInstanceForType() {
                    return Transfer.getDefaultInstance();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                    return (Builder) super.setField(fieldDescriptor, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b
                /* renamed from: setRepeatedField */
                public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                    return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
                public final Builder setUnknownFields(g1 g1Var) {
                    return (Builder) super.setUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clearOneof(Descriptors.g gVar) {
                    return (Builder) super.clearOneof(gVar);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public final Builder mergeUnknownFields(g1 g1Var) {
                    return (Builder) super.mergeUnknownFields(g1Var);
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
                public Builder clear() {
                    super.clear();
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.data_ = byteString;
                    return this;
                }

                private Builder(GeneratedMessageV3.c cVar) {
                    super(cVar);
                    ByteString byteString = ByteString.EMPTY;
                    this.amount_ = byteString;
                    this.data_ = byteString;
                    maybeForceBuilderInitialization();
                }

                @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
                public Builder clone() {
                    return (Builder) super.clone();
                }

                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
                public Builder mergeFrom(l0 l0Var) {
                    if (l0Var instanceof Transfer) {
                        return mergeFrom((Transfer) l0Var);
                    }
                    super.mergeFrom(l0Var);
                    return this;
                }

                public Builder mergeFrom(Transfer transfer) {
                    if (transfer == Transfer.getDefaultInstance()) {
                        return this;
                    }
                    ByteString amount = transfer.getAmount();
                    ByteString byteString = ByteString.EMPTY;
                    if (amount != byteString) {
                        setAmount(transfer.getAmount());
                    }
                    if (transfer.getData() != byteString) {
                        setData(transfer.getData());
                    }
                    mergeUnknownFields(transfer.unknownFields);
                    onChanged();
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
                @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct code enable 'Show inconsistent code' option in preferences
                */
                public wallet.core.jni.proto.Ethereum.Transaction.Transfer.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        com.google.protobuf.t0 r1 = wallet.core.jni.proto.Ethereum.Transaction.Transfer.access$1100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        wallet.core.jni.proto.Ethereum$Transaction$Transfer r3 = (wallet.core.jni.proto.Ethereum.Transaction.Transfer) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                        if (r3 == 0) goto L10
                        r2.mergeFrom(r3)
                    L10:
                        return r2
                    L11:
                        r3 = move-exception
                        goto L21
                    L13:
                        r3 = move-exception
                        com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                        wallet.core.jni.proto.Ethereum$Transaction$Transfer r4 = (wallet.core.jni.proto.Ethereum.Transaction.Transfer) r4     // Catch: java.lang.Throwable -> L11
                        java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                        throw r3     // Catch: java.lang.Throwable -> L1f
                    L1f:
                        r3 = move-exception
                        r0 = r4
                    L21:
                        if (r0 == 0) goto L26
                        r2.mergeFrom(r0)
                    L26:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Ethereum.Transaction.Transfer.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Ethereum$Transaction$Transfer$Builder");
                }
            }

            public /* synthetic */ Transfer(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
                this(jVar, rVar);
            }

            public static Transfer getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static final Descriptors.b getDescriptor() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_Transfer_descriptor;
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.toBuilder();
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer);
            }

            public static t0<Transfer> parser() {
                return PARSER;
            }

            @Override // com.google.protobuf.a
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Transfer)) {
                    return super.equals(obj);
                }
                Transfer transfer = (Transfer) obj;
                return getAmount().equals(transfer.getAmount()) && getData().equals(transfer.getData()) && this.unknownFields.equals(transfer.unknownFields);
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.TransferOrBuilder
            public ByteString getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.Ethereum.Transaction.TransferOrBuilder
            public ByteString getData() {
                return this.data_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
            public t0<Transfer> getParserForType() {
                return PARSER;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public int getSerializedSize() {
                int i = this.memoizedSize;
                if (i != -1) {
                    return i;
                }
                int h = this.amount_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.amount_);
                if (!this.data_.isEmpty()) {
                    h += CodedOutputStream.h(2, this.data_);
                }
                int serializedSize = h + this.unknownFields.getSerializedSize();
                this.memoizedSize = serializedSize;
                return serializedSize;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
            public final g1 getUnknownFields() {
                return this.unknownFields;
            }

            @Override // com.google.protobuf.a
            public int hashCode() {
                int i = this.memoizedHashCode;
                if (i != 0) {
                    return i;
                }
                int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAmount().hashCode()) * 37) + 2) * 53) + getData().hashCode()) * 29) + this.unknownFields.hashCode();
                this.memoizedHashCode = hashCode;
                return hashCode;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_Transfer_fieldAccessorTable.d(Transfer.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
            public final boolean isInitialized() {
                byte b = this.memoizedIsInitialized;
                if (b == 1) {
                    return true;
                }
                if (b == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Object newInstance(GeneratedMessageV3.f fVar) {
                return new Transfer();
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                if (!this.amount_.isEmpty()) {
                    codedOutputStream.q0(1, this.amount_);
                }
                if (!this.data_.isEmpty()) {
                    codedOutputStream.q0(2, this.data_);
                }
                this.unknownFields.writeTo(codedOutputStream);
            }

            public /* synthetic */ Transfer(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
                this(bVar);
            }

            public static Builder newBuilder(Transfer transfer) {
                return DEFAULT_INSTANCE.toBuilder().mergeFrom(transfer);
            }

            public static Transfer parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteBuffer, rVar);
            }

            private Transfer(GeneratedMessageV3.b<?> bVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
            }

            public static Transfer parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
            public Transfer getDefaultInstanceForType() {
                return DEFAULT_INSTANCE;
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder toBuilder() {
                return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
            }

            public static Transfer parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(byteString, rVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
            public Builder newBuilderForType() {
                return newBuilder();
            }

            private Transfer() {
                this.memoizedIsInitialized = (byte) -1;
                ByteString byteString = ByteString.EMPTY;
                this.amount_ = byteString;
                this.data_ = byteString;
            }

            public static Transfer parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr);
            }

            @Override // com.google.protobuf.GeneratedMessageV3
            public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
                return new Builder(cVar, null);
            }

            public static Transfer parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
                return PARSER.parseFrom(bArr, rVar);
            }

            public static Transfer parseFrom(InputStream inputStream) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
            }

            private Transfer(j jVar, r rVar) throws InvalidProtocolBufferException {
                this();
                Objects.requireNonNull(rVar);
                g1.b g = g1.g();
                boolean z = false;
                while (!z) {
                    try {
                        try {
                            try {
                                int J = jVar.J();
                                if (J != 0) {
                                    if (J == 10) {
                                        this.amount_ = jVar.q();
                                    } else if (J != 18) {
                                        if (!parseUnknownField(jVar, g, rVar, J)) {
                                        }
                                    } else {
                                        this.data_ = jVar.q();
                                    }
                                }
                                z = true;
                            } catch (InvalidProtocolBufferException e) {
                                throw e.setUnfinishedMessage(this);
                            }
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                        }
                    } finally {
                        this.unknownFields = g.build();
                        makeExtensionsImmutable();
                    }
                }
            }

            public static Transfer parseFrom(InputStream inputStream, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
            }

            public static Transfer parseFrom(j jVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
            }

            public static Transfer parseFrom(j jVar, r rVar) throws IOException {
                return (Transfer) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
            }
        }

        /* loaded from: classes3.dex */
        public interface TransferOrBuilder extends o0 {
            /* synthetic */ List<String> findInitializationErrors();

            @Override // com.google.protobuf.o0
            /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

            ByteString getAmount();

            ByteString getData();

            @Override // com.google.protobuf.o0
            /* synthetic */ l0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ m0 getDefaultInstanceForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Descriptors.b getDescriptorForType();

            @Override // com.google.protobuf.o0
            /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ String getInitializationErrorString();

            /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

            /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

            /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

            @Override // com.google.protobuf.o0
            /* synthetic */ g1 getUnknownFields();

            @Override // com.google.protobuf.o0
            /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

            /* synthetic */ boolean hasOneof(Descriptors.g gVar);

            @Override // defpackage.d82
            /* synthetic */ boolean isInitialized();
        }

        public /* synthetic */ Transaction(j jVar, r rVar, AnonymousClass1 anonymousClass1) throws InvalidProtocolBufferException {
            this(jVar, rVar);
        }

        public static Transaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<Transaction> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Transaction)) {
                return super.equals(obj);
            }
            Transaction transaction = (Transaction) obj;
            if (getTransactionOneofCase().equals(transaction.getTransactionOneofCase())) {
                switch (this.transactionOneofCase_) {
                    case 1:
                        if (!getTransfer().equals(transaction.getTransfer())) {
                            return false;
                        }
                        break;
                    case 2:
                        if (!getErc20Transfer().equals(transaction.getErc20Transfer())) {
                            return false;
                        }
                        break;
                    case 3:
                        if (!getErc20Approve().equals(transaction.getErc20Approve())) {
                            return false;
                        }
                        break;
                    case 4:
                        if (!getErc721Transfer().equals(transaction.getErc721Transfer())) {
                            return false;
                        }
                        break;
                    case 5:
                        if (!getErc1155Transfer().equals(transaction.getErc1155Transfer())) {
                            return false;
                        }
                        break;
                    case 6:
                        if (!getContractGeneric().equals(transaction.getContractGeneric())) {
                            return false;
                        }
                        break;
                }
                return this.unknownFields.equals(transaction.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ContractGeneric getContractGeneric() {
            if (this.transactionOneofCase_ == 6) {
                return (ContractGeneric) this.transactionOneof_;
            }
            return ContractGeneric.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ContractGenericOrBuilder getContractGenericOrBuilder() {
            if (this.transactionOneofCase_ == 6) {
                return (ContractGeneric) this.transactionOneof_;
            }
            return ContractGeneric.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC1155Transfer getErc1155Transfer() {
            if (this.transactionOneofCase_ == 5) {
                return (ERC1155Transfer) this.transactionOneof_;
            }
            return ERC1155Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC1155TransferOrBuilder getErc1155TransferOrBuilder() {
            if (this.transactionOneofCase_ == 5) {
                return (ERC1155Transfer) this.transactionOneof_;
            }
            return ERC1155Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC20Approve getErc20Approve() {
            if (this.transactionOneofCase_ == 3) {
                return (ERC20Approve) this.transactionOneof_;
            }
            return ERC20Approve.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC20ApproveOrBuilder getErc20ApproveOrBuilder() {
            if (this.transactionOneofCase_ == 3) {
                return (ERC20Approve) this.transactionOneof_;
            }
            return ERC20Approve.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC20Transfer getErc20Transfer() {
            if (this.transactionOneofCase_ == 2) {
                return (ERC20Transfer) this.transactionOneof_;
            }
            return ERC20Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC20TransferOrBuilder getErc20TransferOrBuilder() {
            if (this.transactionOneofCase_ == 2) {
                return (ERC20Transfer) this.transactionOneof_;
            }
            return ERC20Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC721Transfer getErc721Transfer() {
            if (this.transactionOneofCase_ == 4) {
                return (ERC721Transfer) this.transactionOneof_;
            }
            return ERC721Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public ERC721TransferOrBuilder getErc721TransferOrBuilder() {
            if (this.transactionOneofCase_ == 4) {
                return (ERC721Transfer) this.transactionOneof_;
            }
            return ERC721Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<Transaction> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int G = this.transactionOneofCase_ == 1 ? 0 + CodedOutputStream.G(1, (Transfer) this.transactionOneof_) : 0;
            if (this.transactionOneofCase_ == 2) {
                G += CodedOutputStream.G(2, (ERC20Transfer) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 3) {
                G += CodedOutputStream.G(3, (ERC20Approve) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 4) {
                G += CodedOutputStream.G(4, (ERC721Transfer) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 5) {
                G += CodedOutputStream.G(5, (ERC1155Transfer) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 6) {
                G += CodedOutputStream.G(6, (ContractGeneric) this.transactionOneof_);
            }
            int serializedSize = G + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public TransactionOneofCase getTransactionOneofCase() {
            return TransactionOneofCase.forNumber(this.transactionOneofCase_);
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public Transfer getTransfer() {
            if (this.transactionOneofCase_ == 1) {
                return (Transfer) this.transactionOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public TransferOrBuilder getTransferOrBuilder() {
            if (this.transactionOneofCase_ == 1) {
                return (Transfer) this.transactionOneof_;
            }
            return Transfer.getDefaultInstance();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public boolean hasContractGeneric() {
            return this.transactionOneofCase_ == 6;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public boolean hasErc1155Transfer() {
            return this.transactionOneofCase_ == 5;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public boolean hasErc20Approve() {
            return this.transactionOneofCase_ == 3;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public boolean hasErc20Transfer() {
            return this.transactionOneofCase_ == 2;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public boolean hasErc721Transfer() {
            return this.transactionOneofCase_ == 4;
        }

        @Override // wallet.core.jni.proto.Ethereum.TransactionOrBuilder
        public boolean hasTransfer() {
            return this.transactionOneofCase_ == 1;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i;
            int hashCode;
            int i2 = this.memoizedHashCode;
            if (i2 != 0) {
                return i2;
            }
            int hashCode2 = 779 + getDescriptor().hashCode();
            switch (this.transactionOneofCase_) {
                case 1:
                    i = ((hashCode2 * 37) + 1) * 53;
                    hashCode = getTransfer().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3;
                    return hashCode3;
                case 2:
                    i = ((hashCode2 * 37) + 2) * 53;
                    hashCode = getErc20Transfer().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32;
                    return hashCode32;
                case 3:
                    i = ((hashCode2 * 37) + 3) * 53;
                    hashCode = getErc20Approve().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322;
                    return hashCode322;
                case 4:
                    i = ((hashCode2 * 37) + 4) * 53;
                    hashCode = getErc721Transfer().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode3222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222;
                    return hashCode3222;
                case 5:
                    i = ((hashCode2 * 37) + 5) * 53;
                    hashCode = getErc1155Transfer().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode32222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode32222;
                    return hashCode32222;
                case 6:
                    i = ((hashCode2 * 37) + 6) * 53;
                    hashCode = getContractGeneric().hashCode();
                    hashCode2 = i + hashCode;
                    int hashCode322222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode322222;
                    return hashCode322222;
                default:
                    int hashCode3222222 = (hashCode2 * 29) + this.unknownFields.hashCode();
                    this.memoizedHashCode = hashCode3222222;
                    return hashCode3222222;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Ethereum.internal_static_TW_Ethereum_Proto_Transaction_fieldAccessorTable.d(Transaction.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new Transaction();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (this.transactionOneofCase_ == 1) {
                codedOutputStream.K0(1, (Transfer) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 2) {
                codedOutputStream.K0(2, (ERC20Transfer) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 3) {
                codedOutputStream.K0(3, (ERC20Approve) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 4) {
                codedOutputStream.K0(4, (ERC721Transfer) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 5) {
                codedOutputStream.K0(5, (ERC1155Transfer) this.transactionOneof_);
            }
            if (this.transactionOneofCase_ == 6) {
                codedOutputStream.K0(6, (ContractGeneric) this.transactionOneof_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public /* synthetic */ Transaction(GeneratedMessageV3.b bVar, AnonymousClass1 anonymousClass1) {
            this(bVar);
        }

        public static Builder newBuilder(Transaction transaction) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transaction);
        }

        public static Transaction parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private Transaction(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.transactionOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public Transaction getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder((AnonymousClass1) null) : new Builder((AnonymousClass1) null).mergeFrom(this);
        }

        public static Transaction parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Transaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar, null);
        }

        private Transaction() {
            this.transactionOneofCase_ = 0;
            this.memoizedIsInitialized = (byte) -1;
        }

        public static Transaction parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static Transaction parseFrom(InputStream inputStream) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private Transaction(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                Transfer.Builder builder = this.transactionOneofCase_ == 1 ? ((Transfer) this.transactionOneof_).toBuilder() : null;
                                m0 z2 = jVar.z(Transfer.parser(), rVar);
                                this.transactionOneof_ = z2;
                                if (builder != null) {
                                    builder.mergeFrom((Transfer) z2);
                                    this.transactionOneof_ = builder.buildPartial();
                                }
                                this.transactionOneofCase_ = 1;
                            } else if (J == 18) {
                                ERC20Transfer.Builder builder2 = this.transactionOneofCase_ == 2 ? ((ERC20Transfer) this.transactionOneof_).toBuilder() : null;
                                m0 z3 = jVar.z(ERC20Transfer.parser(), rVar);
                                this.transactionOneof_ = z3;
                                if (builder2 != null) {
                                    builder2.mergeFrom((ERC20Transfer) z3);
                                    this.transactionOneof_ = builder2.buildPartial();
                                }
                                this.transactionOneofCase_ = 2;
                            } else if (J == 26) {
                                ERC20Approve.Builder builder3 = this.transactionOneofCase_ == 3 ? ((ERC20Approve) this.transactionOneof_).toBuilder() : null;
                                m0 z4 = jVar.z(ERC20Approve.parser(), rVar);
                                this.transactionOneof_ = z4;
                                if (builder3 != null) {
                                    builder3.mergeFrom((ERC20Approve) z4);
                                    this.transactionOneof_ = builder3.buildPartial();
                                }
                                this.transactionOneofCase_ = 3;
                            } else if (J == 34) {
                                ERC721Transfer.Builder builder4 = this.transactionOneofCase_ == 4 ? ((ERC721Transfer) this.transactionOneof_).toBuilder() : null;
                                m0 z5 = jVar.z(ERC721Transfer.parser(), rVar);
                                this.transactionOneof_ = z5;
                                if (builder4 != null) {
                                    builder4.mergeFrom((ERC721Transfer) z5);
                                    this.transactionOneof_ = builder4.buildPartial();
                                }
                                this.transactionOneofCase_ = 4;
                            } else if (J == 42) {
                                ERC1155Transfer.Builder builder5 = this.transactionOneofCase_ == 5 ? ((ERC1155Transfer) this.transactionOneof_).toBuilder() : null;
                                m0 z6 = jVar.z(ERC1155Transfer.parser(), rVar);
                                this.transactionOneof_ = z6;
                                if (builder5 != null) {
                                    builder5.mergeFrom((ERC1155Transfer) z6);
                                    this.transactionOneof_ = builder5.buildPartial();
                                }
                                this.transactionOneofCase_ = 5;
                            } else if (J != 50) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                ContractGeneric.Builder builder6 = this.transactionOneofCase_ == 6 ? ((ContractGeneric) this.transactionOneof_).toBuilder() : null;
                                m0 z7 = jVar.z(ContractGeneric.parser(), rVar);
                                this.transactionOneof_ = z7;
                                if (builder6 != null) {
                                    builder6.mergeFrom((ContractGeneric) z7);
                                    this.transactionOneof_ = builder6.buildPartial();
                                }
                                this.transactionOneofCase_ = 6;
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static Transaction parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static Transaction parseFrom(j jVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static Transaction parseFrom(j jVar, r rVar) throws IOException {
            return (Transaction) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        Transaction.ContractGeneric getContractGeneric();

        Transaction.ContractGenericOrBuilder getContractGenericOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        Transaction.ERC1155Transfer getErc1155Transfer();

        Transaction.ERC1155TransferOrBuilder getErc1155TransferOrBuilder();

        Transaction.ERC20Approve getErc20Approve();

        Transaction.ERC20ApproveOrBuilder getErc20ApproveOrBuilder();

        Transaction.ERC20Transfer getErc20Transfer();

        Transaction.ERC20TransferOrBuilder getErc20TransferOrBuilder();

        Transaction.ERC721Transfer getErc721Transfer();

        Transaction.ERC721TransferOrBuilder getErc721TransferOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        Transaction.TransactionOneofCase getTransactionOneofCase();

        Transaction.Transfer getTransfer();

        Transaction.TransferOrBuilder getTransferOrBuilder();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        boolean hasContractGeneric();

        boolean hasErc1155Transfer();

        boolean hasErc20Approve();

        boolean hasErc20Transfer();

        boolean hasErc721Transfer();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasTransfer();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Ethereum_Proto_Transaction_descriptor = bVar;
        internal_static_TW_Ethereum_Proto_Transaction_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"Transfer", "Erc20Transfer", "Erc20Approve", "Erc721Transfer", "Erc1155Transfer", "ContractGeneric", "TransactionOneof"});
        Descriptors.b bVar2 = bVar.r().get(0);
        internal_static_TW_Ethereum_Proto_Transaction_Transfer_descriptor = bVar2;
        internal_static_TW_Ethereum_Proto_Transaction_Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Amount", "Data"});
        Descriptors.b bVar3 = bVar.r().get(1);
        internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_descriptor = bVar3;
        internal_static_TW_Ethereum_Proto_Transaction_ERC20Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"To", "Amount"});
        Descriptors.b bVar4 = bVar.r().get(2);
        internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_descriptor = bVar4;
        internal_static_TW_Ethereum_Proto_Transaction_ERC20Approve_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Spender", "Amount"});
        Descriptors.b bVar5 = bVar.r().get(3);
        internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_descriptor = bVar5;
        internal_static_TW_Ethereum_Proto_Transaction_ERC721Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"From", "To", "TokenId"});
        Descriptors.b bVar6 = bVar.r().get(4);
        internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_descriptor = bVar6;
        internal_static_TW_Ethereum_Proto_Transaction_ERC1155Transfer_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"From", "To", "TokenId", "Value", "Data"});
        Descriptors.b bVar7 = bVar.r().get(5);
        internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_descriptor = bVar7;
        internal_static_TW_Ethereum_Proto_Transaction_ContractGeneric_fieldAccessorTable = new GeneratedMessageV3.e(bVar7, new String[]{"Amount", "Data"});
        Descriptors.b bVar8 = getDescriptor().o().get(1);
        internal_static_TW_Ethereum_Proto_SigningInput_descriptor = bVar8;
        internal_static_TW_Ethereum_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar8, new String[]{"ChainId", "Nonce", "GasPrice", "GasLimit", "ToAddress", "PrivateKey", "Transaction"});
        Descriptors.b bVar9 = getDescriptor().o().get(2);
        internal_static_TW_Ethereum_Proto_SigningOutput_descriptor = bVar9;
        internal_static_TW_Ethereum_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar9, new String[]{"Encoded", "V", "R", "S", "Data"});
    }

    private Ethereum() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
