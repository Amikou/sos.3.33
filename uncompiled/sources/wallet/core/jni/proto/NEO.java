package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.a1;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import com.google.protobuf.x0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import wallet.core.jni.proto.Common;

/* loaded from: classes3.dex */
public final class NEO {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\tNEO.proto\u0012\fTW.NEO.Proto\u001a\fCommon.proto\"Z\n\u0010TransactionInput\u0012\u0011\n\tprev_hash\u0018\u0001 \u0001(\f\u0012\u0012\n\nprev_index\u0018\u0002 \u0001(\u0007\u0012\r\n\u0005value\u0018\u0003 \u0001(\u0003\u0012\u0010\n\basset_id\u0018\u0004 \u0001(\t\"a\n\u0011TransactionOutput\u0012\u0010\n\basset_id\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006amount\u0018\u0002 \u0001(\u0012\u0012\u0012\n\nto_address\u0018\u0003 \u0001(\t\u0012\u0016\n\u000echange_address\u0018\u0004 \u0001(\t\"ñ\u0001\n\fSigningInput\u0012.\n\u0006inputs\u0018\u0001 \u0003(\u000b2\u001e.TW.NEO.Proto.TransactionInput\u00120\n\u0007outputs\u0018\u0002 \u0003(\u000b2\u001f.TW.NEO.Proto.TransactionOutput\u0012\u0013\n\u000bprivate_key\u0018\u0003 \u0001(\f\u0012\u000b\n\u0003fee\u0018\u0004 \u0001(\u0003\u0012\u0014\n\fgas_asset_id\u0018\u0005 \u0001(\t\u0012\u001a\n\u0012gas_change_address\u0018\u0006 \u0001(\t\u0012+\n\u0004plan\u0018\u0007 \u0001(\u000b2\u001d.TW.NEO.Proto.TransactionPlan\"N\n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f\u0012,\n\u0005error\u0018\u0002 \u0001(\u000e2\u001d.TW.Common.Proto.SigningError\"\u008f\u0001\n\u0015TransactionOutputPlan\u0012\u000e\n\u0006amount\u0018\u0001 \u0001(\u0003\u0012\u0018\n\u0010available_amount\u0018\u0002 \u0001(\u0003\u0012\u000e\n\u0006change\u0018\u0003 \u0001(\u0003\u0012\u0010\n\basset_id\u0018\u0004 \u0001(\t\u0012\u0012\n\nto_address\u0018\u0005 \u0001(\t\u0012\u0016\n\u000echange_address\u0018\u0006 \u0001(\t\"²\u0001\n\u000fTransactionPlan\u00124\n\u0007outputs\u0018\u0001 \u0003(\u000b2#.TW.NEO.Proto.TransactionOutputPlan\u0012.\n\u0006inputs\u0018\u0002 \u0003(\u000b2\u001e.TW.NEO.Proto.TransactionInput\u0012\u000b\n\u0003fee\u0018\u0003 \u0001(\u0003\u0012,\n\u0005error\u0018\u0004 \u0001(\u000e2\u001d.TW.Common.Proto.SigningErrorB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[]{Common.getDescriptor()});
    private static final Descriptors.b internal_static_TW_NEO_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEO_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEO_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEO_Proto_SigningOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEO_Proto_TransactionInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEO_Proto_TransactionInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEO_Proto_TransactionOutputPlan_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEO_Proto_TransactionOutputPlan_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEO_Proto_TransactionOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEO_Proto_TransactionOutput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_NEO_Proto_TransactionPlan_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_NEO_Proto_TransactionPlan_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int FEE_FIELD_NUMBER = 4;
        public static final int GAS_ASSET_ID_FIELD_NUMBER = 5;
        public static final int GAS_CHANGE_ADDRESS_FIELD_NUMBER = 6;
        public static final int INPUTS_FIELD_NUMBER = 1;
        public static final int OUTPUTS_FIELD_NUMBER = 2;
        public static final int PLAN_FIELD_NUMBER = 7;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private long fee_;
        private volatile Object gasAssetId_;
        private volatile Object gasChangeAddress_;
        private List<TransactionInput> inputs_;
        private byte memoizedIsInitialized;
        private List<TransactionOutput> outputs_;
        private TransactionPlan plan_;
        private ByteString privateKey_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.NEO.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEO.internal_static_TW_NEO_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            if (getInputsList().equals(signingInput.getInputsList()) && getOutputsList().equals(signingInput.getOutputsList()) && getPrivateKey().equals(signingInput.getPrivateKey()) && getFee() == signingInput.getFee() && getGasAssetId().equals(signingInput.getGasAssetId()) && getGasChangeAddress().equals(signingInput.getGasChangeAddress()) && hasPlan() == signingInput.hasPlan()) {
                return (!hasPlan() || getPlan().equals(signingInput.getPlan())) && this.unknownFields.equals(signingInput.unknownFields);
            }
            return false;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public String getGasAssetId() {
            Object obj = this.gasAssetId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.gasAssetId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public ByteString getGasAssetIdBytes() {
            Object obj = this.gasAssetId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.gasAssetId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public String getGasChangeAddress() {
            Object obj = this.gasChangeAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.gasChangeAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public ByteString getGasChangeAddressBytes() {
            Object obj = this.gasChangeAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.gasChangeAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public TransactionInput getInputs(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public int getInputsCount() {
            return this.inputs_.size();
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public List<TransactionInput> getInputsList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public TransactionInputOrBuilder getInputsOrBuilder(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public TransactionOutput getOutputs(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public int getOutputsCount() {
            return this.outputs_.size();
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public List<TransactionOutput> getOutputsList() {
            return this.outputs_;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public TransactionOutputOrBuilder getOutputsOrBuilder(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList() {
            return this.outputs_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public TransactionPlan getPlan() {
            TransactionPlan transactionPlan = this.plan_;
            return transactionPlan == null ? TransactionPlan.getDefaultInstance() : transactionPlan;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public TransactionPlanOrBuilder getPlanOrBuilder() {
            return getPlan();
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.inputs_.size(); i3++) {
                i2 += CodedOutputStream.G(1, this.inputs_.get(i3));
            }
            for (int i4 = 0; i4 < this.outputs_.size(); i4++) {
                i2 += CodedOutputStream.G(2, this.outputs_.get(i4));
            }
            if (!this.privateKey_.isEmpty()) {
                i2 += CodedOutputStream.h(3, this.privateKey_);
            }
            long j = this.fee_;
            if (j != 0) {
                i2 += CodedOutputStream.z(4, j);
            }
            if (!getGasAssetIdBytes().isEmpty()) {
                i2 += GeneratedMessageV3.computeStringSize(5, this.gasAssetId_);
            }
            if (!getGasChangeAddressBytes().isEmpty()) {
                i2 += GeneratedMessageV3.computeStringSize(6, this.gasChangeAddress_);
            }
            if (this.plan_ != null) {
                i2 += CodedOutputStream.G(7, getPlan());
            }
            int serializedSize = i2 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
        public boolean hasPlan() {
            return this.plan_ != null;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (getInputsCount() > 0) {
                hashCode = (((hashCode * 37) + 1) * 53) + getInputsList().hashCode();
            }
            if (getOutputsCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getOutputsList().hashCode();
            }
            int hashCode2 = (((((((((((((((hashCode * 37) + 3) * 53) + getPrivateKey().hashCode()) * 37) + 4) * 53) + a0.h(getFee())) * 37) + 5) * 53) + getGasAssetId().hashCode()) * 37) + 6) * 53) + getGasChangeAddress().hashCode();
            if (hasPlan()) {
                hashCode2 = (((hashCode2 * 37) + 7) * 53) + getPlan().hashCode();
            }
            int hashCode3 = (hashCode2 * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode3;
            return hashCode3;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEO.internal_static_TW_NEO_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            for (int i = 0; i < this.inputs_.size(); i++) {
                codedOutputStream.K0(1, this.inputs_.get(i));
            }
            for (int i2 = 0; i2 < this.outputs_.size(); i2++) {
                codedOutputStream.K0(2, this.outputs_.get(i2));
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(3, this.privateKey_);
            }
            long j = this.fee_;
            if (j != 0) {
                codedOutputStream.I0(4, j);
            }
            if (!getGasAssetIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.gasAssetId_);
            }
            if (!getGasChangeAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 6, this.gasChangeAddress_);
            }
            if (this.plan_ != null) {
                codedOutputStream.K0(7, getPlan());
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private int bitField0_;
            private long fee_;
            private Object gasAssetId_;
            private Object gasChangeAddress_;
            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> inputsBuilder_;
            private List<TransactionInput> inputs_;
            private x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> outputsBuilder_;
            private List<TransactionOutput> outputs_;
            private a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> planBuilder_;
            private TransactionPlan plan_;
            private ByteString privateKey_;

            private void ensureInputsIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.inputs_ = new ArrayList(this.inputs_);
                    this.bitField0_ |= 1;
                }
            }

            private void ensureOutputsIsMutable() {
                if ((this.bitField0_ & 2) == 0) {
                    this.outputs_ = new ArrayList(this.outputs_);
                    this.bitField0_ |= 2;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return NEO.internal_static_TW_NEO_Proto_SigningInput_descriptor;
            }

            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> getInputsFieldBuilder() {
                if (this.inputsBuilder_ == null) {
                    this.inputsBuilder_ = new x0<>(this.inputs_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.inputs_ = null;
                }
                return this.inputsBuilder_;
            }

            private x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> getOutputsFieldBuilder() {
                if (this.outputsBuilder_ == null) {
                    this.outputsBuilder_ = new x0<>(this.outputs_, (this.bitField0_ & 2) != 0, getParentForChildren(), isClean());
                    this.outputs_ = null;
                }
                return this.outputsBuilder_;
            }

            private a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> getPlanFieldBuilder() {
                if (this.planBuilder_ == null) {
                    this.planBuilder_ = new a1<>(getPlan(), getParentForChildren(), isClean());
                    this.plan_ = null;
                }
                return this.planBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getInputsFieldBuilder();
                    getOutputsFieldBuilder();
                }
            }

            public Builder addAllInputs(Iterable<? extends TransactionInput> iterable) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.inputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addAllOutputs(Iterable<? extends TransactionOutput> iterable) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.outputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addInputs(TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(transactionInput);
                    onChanged();
                } else {
                    x0Var.f(transactionInput);
                }
                return this;
            }

            public TransactionInput.Builder addInputsBuilder() {
                return getInputsFieldBuilder().d(TransactionInput.getDefaultInstance());
            }

            public Builder addOutputs(TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.add(transactionOutput);
                    onChanged();
                } else {
                    x0Var.f(transactionOutput);
                }
                return this;
            }

            public TransactionOutput.Builder addOutputsBuilder() {
                return getOutputsFieldBuilder().d(TransactionOutput.getDefaultInstance());
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearGasAssetId() {
                this.gasAssetId_ = SigningInput.getDefaultInstance().getGasAssetId();
                onChanged();
                return this;
            }

            public Builder clearGasChangeAddress() {
                this.gasChangeAddress_ = SigningInput.getDefaultInstance().getGasChangeAddress();
                onChanged();
                return this;
            }

            public Builder clearInputs() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearOutputs() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearPlan() {
                if (this.planBuilder_ == null) {
                    this.plan_ = null;
                    onChanged();
                } else {
                    this.plan_ = null;
                    this.planBuilder_ = null;
                }
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEO.internal_static_TW_NEO_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public String getGasAssetId() {
                Object obj = this.gasAssetId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.gasAssetId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public ByteString getGasAssetIdBytes() {
                Object obj = this.gasAssetId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.gasAssetId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public String getGasChangeAddress() {
                Object obj = this.gasChangeAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.gasChangeAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public ByteString getGasChangeAddressBytes() {
                Object obj = this.gasChangeAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.gasChangeAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public TransactionInput getInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionInput.Builder getInputsBuilder(int i) {
                return getInputsFieldBuilder().l(i);
            }

            public List<TransactionInput.Builder> getInputsBuilderList() {
                return getInputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public int getInputsCount() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public List<TransactionInput> getInputsList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.inputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public TransactionInputOrBuilder getInputsOrBuilder(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.inputs_);
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public TransactionOutput getOutputs(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionOutput.Builder getOutputsBuilder(int i) {
                return getOutputsFieldBuilder().l(i);
            }

            public List<TransactionOutput.Builder> getOutputsBuilderList() {
                return getOutputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public int getOutputsCount() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public List<TransactionOutput> getOutputsList() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.outputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public TransactionOutputOrBuilder getOutputsOrBuilder(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList() {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.outputs_);
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public TransactionPlan getPlan() {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    TransactionPlan transactionPlan = this.plan_;
                    return transactionPlan == null ? TransactionPlan.getDefaultInstance() : transactionPlan;
                }
                return a1Var.f();
            }

            public TransactionPlan.Builder getPlanBuilder() {
                onChanged();
                return getPlanFieldBuilder().e();
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public TransactionPlanOrBuilder getPlanOrBuilder() {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var != null) {
                    return a1Var.g();
                }
                TransactionPlan transactionPlan = this.plan_;
                return transactionPlan == null ? TransactionPlan.getDefaultInstance() : transactionPlan;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.NEO.SigningInputOrBuilder
            public boolean hasPlan() {
                return (this.planBuilder_ == null && this.plan_ == null) ? false : true;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEO.internal_static_TW_NEO_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder mergePlan(TransactionPlan transactionPlan) {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    TransactionPlan transactionPlan2 = this.plan_;
                    if (transactionPlan2 != null) {
                        this.plan_ = TransactionPlan.newBuilder(transactionPlan2).mergeFrom(transactionPlan).buildPartial();
                    } else {
                        this.plan_ = transactionPlan;
                    }
                    onChanged();
                } else {
                    a1Var.h(transactionPlan);
                }
                return this;
            }

            public Builder removeInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder removeOutputs(int i) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setGasAssetId(String str) {
                Objects.requireNonNull(str);
                this.gasAssetId_ = str;
                onChanged();
                return this;
            }

            public Builder setGasAssetIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.gasAssetId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setGasChangeAddress(String str) {
                Objects.requireNonNull(str);
                this.gasChangeAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setGasChangeAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.gasChangeAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.set(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.x(i, transactionInput);
                }
                return this;
            }

            public Builder setOutputs(int i, TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, transactionOutput);
                    onChanged();
                } else {
                    x0Var.x(i, transactionOutput);
                }
                return this;
            }

            public Builder setPlan(TransactionPlan transactionPlan) {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    Objects.requireNonNull(transactionPlan);
                    this.plan_ = transactionPlan;
                    onChanged();
                } else {
                    a1Var.j(transactionPlan);
                }
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.inputs_ = Collections.emptyList();
                this.outputs_ = Collections.emptyList();
                this.privateKey_ = ByteString.EMPTY;
                this.gasAssetId_ = "";
                this.gasChangeAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                int i = this.bitField0_;
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    if ((i & 1) != 0) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                        this.bitField0_ &= -2;
                    }
                    signingInput.inputs_ = this.inputs_;
                } else {
                    signingInput.inputs_ = x0Var.g();
                }
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var2 = this.outputsBuilder_;
                if (x0Var2 != null) {
                    signingInput.outputs_ = x0Var2.g();
                } else {
                    if ((this.bitField0_ & 2) != 0) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                        this.bitField0_ &= -3;
                    }
                    signingInput.outputs_ = this.outputs_;
                }
                signingInput.privateKey_ = this.privateKey_;
                signingInput.fee_ = this.fee_;
                signingInput.gasAssetId_ = this.gasAssetId_;
                signingInput.gasChangeAddress_ = this.gasChangeAddress_;
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    signingInput.plan_ = this.plan_;
                } else {
                    signingInput.plan_ = a1Var.b();
                }
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public TransactionInput.Builder addInputsBuilder(int i) {
                return getInputsFieldBuilder().c(i, TransactionInput.getDefaultInstance());
            }

            public TransactionOutput.Builder addOutputsBuilder(int i) {
                return getOutputsFieldBuilder().c(i, TransactionOutput.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var2 = this.outputsBuilder_;
                if (x0Var2 == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                } else {
                    x0Var2.h();
                }
                this.privateKey_ = ByteString.EMPTY;
                this.fee_ = 0L;
                this.gasAssetId_ = "";
                this.gasChangeAddress_ = "";
                if (this.planBuilder_ == null) {
                    this.plan_ = null;
                } else {
                    this.plan_ = null;
                    this.planBuilder_ = null;
                }
                return this;
            }

            public Builder setPlan(TransactionPlan.Builder builder) {
                a1<TransactionPlan, TransactionPlan.Builder, TransactionPlanOrBuilder> a1Var = this.planBuilder_;
                if (a1Var == null) {
                    this.plan_ = builder.build();
                    onChanged();
                } else {
                    a1Var.j(builder.build());
                }
                return this;
            }

            public Builder addInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.e(i, transactionInput);
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutput transactionOutput) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutput);
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, transactionOutput);
                    onChanged();
                } else {
                    x0Var.e(i, transactionOutput);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder setOutputs(int i, TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.inputs_ = Collections.emptyList();
                this.outputs_ = Collections.emptyList();
                this.privateKey_ = ByteString.EMPTY;
                this.gasAssetId_ = "";
                this.gasChangeAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (this.inputsBuilder_ == null) {
                    if (!signingInput.inputs_.isEmpty()) {
                        if (this.inputs_.isEmpty()) {
                            this.inputs_ = signingInput.inputs_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureInputsIsMutable();
                            this.inputs_.addAll(signingInput.inputs_);
                        }
                        onChanged();
                    }
                } else if (!signingInput.inputs_.isEmpty()) {
                    if (!this.inputsBuilder_.u()) {
                        this.inputsBuilder_.b(signingInput.inputs_);
                    } else {
                        this.inputsBuilder_.i();
                        this.inputsBuilder_ = null;
                        this.inputs_ = signingInput.inputs_;
                        this.bitField0_ &= -2;
                        this.inputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getInputsFieldBuilder() : null;
                    }
                }
                if (this.outputsBuilder_ == null) {
                    if (!signingInput.outputs_.isEmpty()) {
                        if (this.outputs_.isEmpty()) {
                            this.outputs_ = signingInput.outputs_;
                            this.bitField0_ &= -3;
                        } else {
                            ensureOutputsIsMutable();
                            this.outputs_.addAll(signingInput.outputs_);
                        }
                        onChanged();
                    }
                } else if (!signingInput.outputs_.isEmpty()) {
                    if (!this.outputsBuilder_.u()) {
                        this.outputsBuilder_.b(signingInput.outputs_);
                    } else {
                        this.outputsBuilder_.i();
                        this.outputsBuilder_ = null;
                        this.outputs_ = signingInput.outputs_;
                        this.bitField0_ &= -3;
                        this.outputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getOutputsFieldBuilder() : null;
                    }
                }
                if (signingInput.getPrivateKey() != ByteString.EMPTY) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                if (signingInput.getFee() != 0) {
                    setFee(signingInput.getFee());
                }
                if (!signingInput.getGasAssetId().isEmpty()) {
                    this.gasAssetId_ = signingInput.gasAssetId_;
                    onChanged();
                }
                if (!signingInput.getGasChangeAddress().isEmpty()) {
                    this.gasChangeAddress_ = signingInput.gasChangeAddress_;
                    onChanged();
                }
                if (signingInput.hasPlan()) {
                    mergePlan(signingInput.getPlan());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            public Builder addInputs(TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addOutputs(TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutput.Builder builder) {
                x0<TransactionOutput, TransactionOutput.Builder, TransactionOutputOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEO.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEO.SigningInput.access$4600()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEO$SigningInput r3 = (wallet.core.jni.proto.NEO.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEO$SigningInput r4 = (wallet.core.jni.proto.NEO.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEO.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEO$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.inputs_ = Collections.emptyList();
            this.outputs_ = Collections.emptyList();
            this.privateKey_ = ByteString.EMPTY;
            this.gasAssetId_ = "";
            this.gasChangeAddress_ = "";
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    if (!(z2 & true)) {
                                        this.inputs_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.inputs_.add(jVar.z(TransactionInput.parser(), rVar));
                                } else if (J == 18) {
                                    if (!(z2 & true)) {
                                        this.outputs_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.outputs_.add(jVar.z(TransactionOutput.parser(), rVar));
                                } else if (J == 26) {
                                    this.privateKey_ = jVar.q();
                                } else if (J == 32) {
                                    this.fee_ = jVar.y();
                                } else if (J == 42) {
                                    this.gasAssetId_ = jVar.I();
                                } else if (J == 50) {
                                    this.gasChangeAddress_ = jVar.I();
                                } else if (J != 58) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    TransactionPlan transactionPlan = this.plan_;
                                    TransactionPlan.Builder builder = transactionPlan != null ? transactionPlan.toBuilder() : null;
                                    TransactionPlan transactionPlan2 = (TransactionPlan) jVar.z(TransactionPlan.parser(), rVar);
                                    this.plan_ = transactionPlan2;
                                    if (builder != null) {
                                        builder.mergeFrom(transactionPlan2);
                                        this.plan_ = builder.buildPartial();
                                    }
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                    }
                    if (z2 & true) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        long getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        String getGasAssetId();

        ByteString getGasAssetIdBytes();

        String getGasChangeAddress();

        ByteString getGasChangeAddressBytes();

        /* synthetic */ String getInitializationErrorString();

        TransactionInput getInputs(int i);

        int getInputsCount();

        List<TransactionInput> getInputsList();

        TransactionInputOrBuilder getInputsOrBuilder(int i);

        List<? extends TransactionInputOrBuilder> getInputsOrBuilderList();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        TransactionOutput getOutputs(int i);

        int getOutputsCount();

        List<TransactionOutput> getOutputsList();

        TransactionOutputOrBuilder getOutputsOrBuilder(int i);

        List<? extends TransactionOutputOrBuilder> getOutputsOrBuilderList();

        TransactionPlan getPlan();

        TransactionPlanOrBuilder getPlanOrBuilder();

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        boolean hasPlan();

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        public static final int ERROR_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private int error_;
        private byte memoizedIsInitialized;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.NEO.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEO.internal_static_TW_NEO_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && this.error_ == signingOutput.error_ && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEO.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // wallet.core.jni.proto.NEO.SigningOutputOrBuilder
        public Common.SigningError getError() {
            Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
            return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.NEO.SigningOutputOrBuilder
        public int getErrorValue() {
            return this.error_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_);
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                h += CodedOutputStream.l(2, this.error_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 37) + 2) * 53) + this.error_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEO.internal_static_TW_NEO_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                codedOutputStream.u0(2, this.error_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;
            private int error_;

            public static final Descriptors.b getDescriptor() {
                return NEO.internal_static_TW_NEO_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearError() {
                this.error_ = 0;
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEO.internal_static_TW_NEO_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.NEO.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.NEO.SigningOutputOrBuilder
            public Common.SigningError getError() {
                Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
                return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.NEO.SigningOutputOrBuilder
            public int getErrorValue() {
                return this.error_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEO.internal_static_TW_NEO_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setError(Common.SigningError signingError) {
                Objects.requireNonNull(signingError);
                this.error_ = signingError.getNumber();
                onChanged();
                return this;
            }

            public Builder setErrorValue(int i) {
                this.error_ = i;
                onChanged();
                return this;
            }

            private Builder() {
                this.encoded_ = ByteString.EMPTY;
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                signingOutput.encoded_ = this.encoded_;
                signingOutput.error_ = this.error_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.encoded_ = ByteString.EMPTY;
                this.error_ = 0;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.encoded_ = ByteString.EMPTY;
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                if (signingOutput.getEncoded() != ByteString.EMPTY) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (signingOutput.error_ != 0) {
                    setErrorValue(signingOutput.getErrorValue());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEO.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEO.SigningOutput.access$5900()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEO$SigningOutput r3 = (wallet.core.jni.proto.NEO.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEO$SigningOutput r4 = (wallet.core.jni.proto.NEO.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEO.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEO$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.encoded_ = ByteString.EMPTY;
            this.error_ = 0;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.encoded_ = jVar.q();
                                } else if (J != 16) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.error_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        Common.SigningError getError();

        int getErrorValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionInput extends GeneratedMessageV3 implements TransactionInputOrBuilder {
        public static final int ASSET_ID_FIELD_NUMBER = 4;
        private static final TransactionInput DEFAULT_INSTANCE = new TransactionInput();
        private static final t0<TransactionInput> PARSER = new c<TransactionInput>() { // from class: wallet.core.jni.proto.NEO.TransactionInput.1
            @Override // com.google.protobuf.t0
            public TransactionInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionInput(jVar, rVar);
            }
        };
        public static final int PREV_HASH_FIELD_NUMBER = 1;
        public static final int PREV_INDEX_FIELD_NUMBER = 2;
        public static final int VALUE_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private volatile Object assetId_;
        private byte memoizedIsInitialized;
        private ByteString prevHash_;
        private int prevIndex_;
        private long value_;

        public static TransactionInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEO.internal_static_TW_NEO_Proto_TransactionInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionInput)) {
                return super.equals(obj);
            }
            TransactionInput transactionInput = (TransactionInput) obj;
            return getPrevHash().equals(transactionInput.getPrevHash()) && getPrevIndex() == transactionInput.getPrevIndex() && getValue() == transactionInput.getValue() && getAssetId().equals(transactionInput.getAssetId()) && this.unknownFields.equals(transactionInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
        public String getAssetId() {
            Object obj = this.assetId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.assetId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
        public ByteString getAssetIdBytes() {
            Object obj = this.assetId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.assetId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
        public ByteString getPrevHash() {
            return this.prevHash_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
        public int getPrevIndex() {
            return this.prevIndex_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.prevHash_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.prevHash_);
            int i2 = this.prevIndex_;
            if (i2 != 0) {
                h += CodedOutputStream.n(2, i2);
            }
            long j = this.value_;
            if (j != 0) {
                h += CodedOutputStream.z(3, j);
            }
            if (!getAssetIdBytes().isEmpty()) {
                h += GeneratedMessageV3.computeStringSize(4, this.assetId_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
        public long getValue() {
            return this.value_;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getPrevHash().hashCode()) * 37) + 2) * 53) + getPrevIndex()) * 37) + 3) * 53) + a0.h(getValue())) * 37) + 4) * 53) + getAssetId().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEO.internal_static_TW_NEO_Proto_TransactionInput_fieldAccessorTable.d(TransactionInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.prevHash_.isEmpty()) {
                codedOutputStream.q0(1, this.prevHash_);
            }
            int i = this.prevIndex_;
            if (i != 0) {
                codedOutputStream.w0(2, i);
            }
            long j = this.value_;
            if (j != 0) {
                codedOutputStream.I0(3, j);
            }
            if (!getAssetIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.assetId_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionInputOrBuilder {
            private Object assetId_;
            private ByteString prevHash_;
            private int prevIndex_;
            private long value_;

            public static final Descriptors.b getDescriptor() {
                return NEO.internal_static_TW_NEO_Proto_TransactionInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAssetId() {
                this.assetId_ = TransactionInput.getDefaultInstance().getAssetId();
                onChanged();
                return this;
            }

            public Builder clearPrevHash() {
                this.prevHash_ = TransactionInput.getDefaultInstance().getPrevHash();
                onChanged();
                return this;
            }

            public Builder clearPrevIndex() {
                this.prevIndex_ = 0;
                onChanged();
                return this;
            }

            public Builder clearValue() {
                this.value_ = 0L;
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
            public String getAssetId() {
                Object obj = this.assetId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.assetId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
            public ByteString getAssetIdBytes() {
                Object obj = this.assetId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.assetId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEO.internal_static_TW_NEO_Proto_TransactionInput_descriptor;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
            public ByteString getPrevHash() {
                return this.prevHash_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
            public int getPrevIndex() {
                return this.prevIndex_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionInputOrBuilder
            public long getValue() {
                return this.value_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEO.internal_static_TW_NEO_Proto_TransactionInput_fieldAccessorTable.d(TransactionInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAssetId(String str) {
                Objects.requireNonNull(str);
                this.assetId_ = str;
                onChanged();
                return this;
            }

            public Builder setAssetIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.assetId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrevHash(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.prevHash_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrevIndex(int i) {
                this.prevIndex_ = i;
                onChanged();
                return this;
            }

            public Builder setValue(long j) {
                this.value_ = j;
                onChanged();
                return this;
            }

            private Builder() {
                this.prevHash_ = ByteString.EMPTY;
                this.assetId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionInput build() {
                TransactionInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionInput buildPartial() {
                TransactionInput transactionInput = new TransactionInput(this);
                transactionInput.prevHash_ = this.prevHash_;
                transactionInput.prevIndex_ = this.prevIndex_;
                transactionInput.value_ = this.value_;
                transactionInput.assetId_ = this.assetId_;
                onBuilt();
                return transactionInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionInput getDefaultInstanceForType() {
                return TransactionInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.prevHash_ = ByteString.EMPTY;
                this.prevIndex_ = 0;
                this.value_ = 0L;
                this.assetId_ = "";
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.prevHash_ = ByteString.EMPTY;
                this.assetId_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionInput) {
                    return mergeFrom((TransactionInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(TransactionInput transactionInput) {
                if (transactionInput == TransactionInput.getDefaultInstance()) {
                    return this;
                }
                if (transactionInput.getPrevHash() != ByteString.EMPTY) {
                    setPrevHash(transactionInput.getPrevHash());
                }
                if (transactionInput.getPrevIndex() != 0) {
                    setPrevIndex(transactionInput.getPrevIndex());
                }
                if (transactionInput.getValue() != 0) {
                    setValue(transactionInput.getValue());
                }
                if (!transactionInput.getAssetId().isEmpty()) {
                    this.assetId_ = transactionInput.assetId_;
                    onChanged();
                }
                mergeUnknownFields(transactionInput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEO.TransactionInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEO.TransactionInput.access$1100()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEO$TransactionInput r3 = (wallet.core.jni.proto.NEO.TransactionInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEO$TransactionInput r4 = (wallet.core.jni.proto.NEO.TransactionInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEO.TransactionInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEO$TransactionInput$Builder");
            }
        }

        public static Builder newBuilder(TransactionInput transactionInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionInput);
        }

        public static TransactionInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.prevHash_ = ByteString.EMPTY;
            this.assetId_ = "";
        }

        public static TransactionInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionInput parseFrom(InputStream inputStream) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private TransactionInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.prevHash_ = jVar.q();
                            } else if (J == 21) {
                                this.prevIndex_ = jVar.t();
                            } else if (J == 24) {
                                this.value_ = jVar.y();
                            } else if (J != 34) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.assetId_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionInput parseFrom(j jVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionInput parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getAssetId();

        ByteString getAssetIdBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrevHash();

        int getPrevIndex();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        long getValue();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionOutput extends GeneratedMessageV3 implements TransactionOutputOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 2;
        public static final int ASSET_ID_FIELD_NUMBER = 1;
        public static final int CHANGE_ADDRESS_FIELD_NUMBER = 4;
        private static final TransactionOutput DEFAULT_INSTANCE = new TransactionOutput();
        private static final t0<TransactionOutput> PARSER = new c<TransactionOutput>() { // from class: wallet.core.jni.proto.NEO.TransactionOutput.1
            @Override // com.google.protobuf.t0
            public TransactionOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionOutput(jVar, rVar);
            }
        };
        public static final int TO_ADDRESS_FIELD_NUMBER = 3;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object assetId_;
        private volatile Object changeAddress_;
        private byte memoizedIsInitialized;
        private volatile Object toAddress_;

        public static TransactionOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEO.internal_static_TW_NEO_Proto_TransactionOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionOutput)) {
                return super.equals(obj);
            }
            TransactionOutput transactionOutput = (TransactionOutput) obj;
            return getAssetId().equals(transactionOutput.getAssetId()) && getAmount() == transactionOutput.getAmount() && getToAddress().equals(transactionOutput.getToAddress()) && getChangeAddress().equals(transactionOutput.getChangeAddress()) && this.unknownFields.equals(transactionOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
        public String getAssetId() {
            Object obj = this.assetId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.assetId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
        public ByteString getAssetIdBytes() {
            Object obj = this.assetId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.assetId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
        public String getChangeAddress() {
            Object obj = this.changeAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.changeAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
        public ByteString getChangeAddressBytes() {
            Object obj = this.changeAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.changeAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getAssetIdBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.assetId_);
            long j = this.amount_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.T(2, j);
            }
            if (!getToAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(3, this.toAddress_);
            }
            if (!getChangeAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(4, this.changeAddress_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getAssetId().hashCode()) * 37) + 2) * 53) + a0.h(getAmount())) * 37) + 3) * 53) + getToAddress().hashCode()) * 37) + 4) * 53) + getChangeAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEO.internal_static_TW_NEO_Proto_TransactionOutput_fieldAccessorTable.d(TransactionOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getAssetIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.assetId_);
            }
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.W0(2, j);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 3, this.toAddress_);
            }
            if (!getChangeAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.changeAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOutputOrBuilder {
            private long amount_;
            private Object assetId_;
            private Object changeAddress_;
            private Object toAddress_;

            public static final Descriptors.b getDescriptor() {
                return NEO.internal_static_TW_NEO_Proto_TransactionOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearAssetId() {
                this.assetId_ = TransactionOutput.getDefaultInstance().getAssetId();
                onChanged();
                return this;
            }

            public Builder clearChangeAddress() {
                this.changeAddress_ = TransactionOutput.getDefaultInstance().getChangeAddress();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransactionOutput.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
            public String getAssetId() {
                Object obj = this.assetId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.assetId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
            public ByteString getAssetIdBytes() {
                Object obj = this.assetId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.assetId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
            public String getChangeAddress() {
                Object obj = this.changeAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.changeAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
            public ByteString getChangeAddressBytes() {
                Object obj = this.changeAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.changeAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEO.internal_static_TW_NEO_Proto_TransactionOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEO.internal_static_TW_NEO_Proto_TransactionOutput_fieldAccessorTable.d(TransactionOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setAssetId(String str) {
                Objects.requireNonNull(str);
                this.assetId_ = str;
                onChanged();
                return this;
            }

            public Builder setAssetIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.assetId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setChangeAddress(String str) {
                Objects.requireNonNull(str);
                this.changeAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setChangeAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.changeAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.assetId_ = "";
                this.toAddress_ = "";
                this.changeAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutput build() {
                TransactionOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutput buildPartial() {
                TransactionOutput transactionOutput = new TransactionOutput(this);
                transactionOutput.assetId_ = this.assetId_;
                transactionOutput.amount_ = this.amount_;
                transactionOutput.toAddress_ = this.toAddress_;
                transactionOutput.changeAddress_ = this.changeAddress_;
                onBuilt();
                return transactionOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionOutput getDefaultInstanceForType() {
                return TransactionOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.assetId_ = "";
                this.amount_ = 0L;
                this.toAddress_ = "";
                this.changeAddress_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionOutput) {
                    return mergeFrom((TransactionOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.assetId_ = "";
                this.toAddress_ = "";
                this.changeAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransactionOutput transactionOutput) {
                if (transactionOutput == TransactionOutput.getDefaultInstance()) {
                    return this;
                }
                if (!transactionOutput.getAssetId().isEmpty()) {
                    this.assetId_ = transactionOutput.assetId_;
                    onChanged();
                }
                if (transactionOutput.getAmount() != 0) {
                    setAmount(transactionOutput.getAmount());
                }
                if (!transactionOutput.getToAddress().isEmpty()) {
                    this.toAddress_ = transactionOutput.toAddress_;
                    onChanged();
                }
                if (!transactionOutput.getChangeAddress().isEmpty()) {
                    this.changeAddress_ = transactionOutput.changeAddress_;
                    onChanged();
                }
                mergeUnknownFields(transactionOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEO.TransactionOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEO.TransactionOutput.access$2500()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEO$TransactionOutput r3 = (wallet.core.jni.proto.NEO.TransactionOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEO$TransactionOutput r4 = (wallet.core.jni.proto.NEO.TransactionOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEO.TransactionOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEO$TransactionOutput$Builder");
            }
        }

        public static Builder newBuilder(TransactionOutput transactionOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionOutput);
        }

        public static TransactionOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionOutput() {
            this.memoizedIsInitialized = (byte) -1;
            this.assetId_ = "";
            this.toAddress_ = "";
            this.changeAddress_ = "";
        }

        public static TransactionOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionOutput parseFrom(InputStream inputStream) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransactionOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TransactionOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.assetId_ = jVar.I();
                            } else if (J == 16) {
                                this.amount_ = jVar.G();
                            } else if (J == 26) {
                                this.toAddress_ = jVar.I();
                            } else if (J != 34) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.changeAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionOutput parseFrom(j jVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionOutput parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        String getAssetId();

        ByteString getAssetIdBytes();

        String getChangeAddress();

        ByteString getChangeAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionOutputPlan extends GeneratedMessageV3 implements TransactionOutputPlanOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER = 1;
        public static final int ASSET_ID_FIELD_NUMBER = 4;
        public static final int AVAILABLE_AMOUNT_FIELD_NUMBER = 2;
        public static final int CHANGE_ADDRESS_FIELD_NUMBER = 6;
        public static final int CHANGE_FIELD_NUMBER = 3;
        private static final TransactionOutputPlan DEFAULT_INSTANCE = new TransactionOutputPlan();
        private static final t0<TransactionOutputPlan> PARSER = new c<TransactionOutputPlan>() { // from class: wallet.core.jni.proto.NEO.TransactionOutputPlan.1
            @Override // com.google.protobuf.t0
            public TransactionOutputPlan parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionOutputPlan(jVar, rVar);
            }
        };
        public static final int TO_ADDRESS_FIELD_NUMBER = 5;
        private static final long serialVersionUID = 0;
        private long amount_;
        private volatile Object assetId_;
        private long availableAmount_;
        private volatile Object changeAddress_;
        private long change_;
        private byte memoizedIsInitialized;
        private volatile Object toAddress_;

        public static TransactionOutputPlan getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEO.internal_static_TW_NEO_Proto_TransactionOutputPlan_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionOutputPlan parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionOutputPlan) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionOutputPlan parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionOutputPlan> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionOutputPlan)) {
                return super.equals(obj);
            }
            TransactionOutputPlan transactionOutputPlan = (TransactionOutputPlan) obj;
            return getAmount() == transactionOutputPlan.getAmount() && getAvailableAmount() == transactionOutputPlan.getAvailableAmount() && getChange() == transactionOutputPlan.getChange() && getAssetId().equals(transactionOutputPlan.getAssetId()) && getToAddress().equals(transactionOutputPlan.getToAddress()) && getChangeAddress().equals(transactionOutputPlan.getChangeAddress()) && this.unknownFields.equals(transactionOutputPlan.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public String getAssetId() {
            Object obj = this.assetId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.assetId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public ByteString getAssetIdBytes() {
            Object obj = this.assetId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.assetId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public long getAvailableAmount() {
            return this.availableAmount_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public long getChange() {
            return this.change_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public String getChangeAddress() {
            Object obj = this.changeAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.changeAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public ByteString getChangeAddressBytes() {
            Object obj = this.changeAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.changeAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionOutputPlan> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            long j = this.amount_;
            int z = j != 0 ? 0 + CodedOutputStream.z(1, j) : 0;
            long j2 = this.availableAmount_;
            if (j2 != 0) {
                z += CodedOutputStream.z(2, j2);
            }
            long j3 = this.change_;
            if (j3 != 0) {
                z += CodedOutputStream.z(3, j3);
            }
            if (!getAssetIdBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(4, this.assetId_);
            }
            if (!getToAddressBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(5, this.toAddress_);
            }
            if (!getChangeAddressBytes().isEmpty()) {
                z += GeneratedMessageV3.computeStringSize(6, this.changeAddress_);
            }
            int serializedSize = z + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + a0.h(getAmount())) * 37) + 2) * 53) + a0.h(getAvailableAmount())) * 37) + 3) * 53) + a0.h(getChange())) * 37) + 4) * 53) + getAssetId().hashCode()) * 37) + 5) * 53) + getToAddress().hashCode()) * 37) + 6) * 53) + getChangeAddress().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEO.internal_static_TW_NEO_Proto_TransactionOutputPlan_fieldAccessorTable.d(TransactionOutputPlan.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionOutputPlan();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            long j = this.amount_;
            if (j != 0) {
                codedOutputStream.I0(1, j);
            }
            long j2 = this.availableAmount_;
            if (j2 != 0) {
                codedOutputStream.I0(2, j2);
            }
            long j3 = this.change_;
            if (j3 != 0) {
                codedOutputStream.I0(3, j3);
            }
            if (!getAssetIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 4, this.assetId_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 5, this.toAddress_);
            }
            if (!getChangeAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 6, this.changeAddress_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionOutputPlanOrBuilder {
            private long amount_;
            private Object assetId_;
            private long availableAmount_;
            private Object changeAddress_;
            private long change_;
            private Object toAddress_;

            public static final Descriptors.b getDescriptor() {
                return NEO.internal_static_TW_NEO_Proto_TransactionOutputPlan_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearAmount() {
                this.amount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearAssetId() {
                this.assetId_ = TransactionOutputPlan.getDefaultInstance().getAssetId();
                onChanged();
                return this;
            }

            public Builder clearAvailableAmount() {
                this.availableAmount_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearChange() {
                this.change_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearChangeAddress() {
                this.changeAddress_ = TransactionOutputPlan.getDefaultInstance().getChangeAddress();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = TransactionOutputPlan.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public long getAmount() {
                return this.amount_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public String getAssetId() {
                Object obj = this.assetId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.assetId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public ByteString getAssetIdBytes() {
                Object obj = this.assetId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.assetId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public long getAvailableAmount() {
                return this.availableAmount_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public long getChange() {
                return this.change_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public String getChangeAddress() {
                Object obj = this.changeAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.changeAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public ByteString getChangeAddressBytes() {
                Object obj = this.changeAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.changeAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEO.internal_static_TW_NEO_Proto_TransactionOutputPlan_descriptor;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionOutputPlanOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEO.internal_static_TW_NEO_Proto_TransactionOutputPlan_fieldAccessorTable.d(TransactionOutputPlan.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setAmount(long j) {
                this.amount_ = j;
                onChanged();
                return this;
            }

            public Builder setAssetId(String str) {
                Objects.requireNonNull(str);
                this.assetId_ = str;
                onChanged();
                return this;
            }

            public Builder setAssetIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.assetId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setAvailableAmount(long j) {
                this.availableAmount_ = j;
                onChanged();
                return this;
            }

            public Builder setChange(long j) {
                this.change_ = j;
                onChanged();
                return this;
            }

            public Builder setChangeAddress(String str) {
                Objects.requireNonNull(str);
                this.changeAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setChangeAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.changeAddress_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.assetId_ = "";
                this.toAddress_ = "";
                this.changeAddress_ = "";
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutputPlan build() {
                TransactionOutputPlan buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionOutputPlan buildPartial() {
                TransactionOutputPlan transactionOutputPlan = new TransactionOutputPlan(this);
                transactionOutputPlan.amount_ = this.amount_;
                transactionOutputPlan.availableAmount_ = this.availableAmount_;
                transactionOutputPlan.change_ = this.change_;
                transactionOutputPlan.assetId_ = this.assetId_;
                transactionOutputPlan.toAddress_ = this.toAddress_;
                transactionOutputPlan.changeAddress_ = this.changeAddress_;
                onBuilt();
                return transactionOutputPlan;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionOutputPlan getDefaultInstanceForType() {
                return TransactionOutputPlan.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.amount_ = 0L;
                this.availableAmount_ = 0L;
                this.change_ = 0L;
                this.assetId_ = "";
                this.toAddress_ = "";
                this.changeAddress_ = "";
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionOutputPlan) {
                    return mergeFrom((TransactionOutputPlan) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.assetId_ = "";
                this.toAddress_ = "";
                this.changeAddress_ = "";
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransactionOutputPlan transactionOutputPlan) {
                if (transactionOutputPlan == TransactionOutputPlan.getDefaultInstance()) {
                    return this;
                }
                if (transactionOutputPlan.getAmount() != 0) {
                    setAmount(transactionOutputPlan.getAmount());
                }
                if (transactionOutputPlan.getAvailableAmount() != 0) {
                    setAvailableAmount(transactionOutputPlan.getAvailableAmount());
                }
                if (transactionOutputPlan.getChange() != 0) {
                    setChange(transactionOutputPlan.getChange());
                }
                if (!transactionOutputPlan.getAssetId().isEmpty()) {
                    this.assetId_ = transactionOutputPlan.assetId_;
                    onChanged();
                }
                if (!transactionOutputPlan.getToAddress().isEmpty()) {
                    this.toAddress_ = transactionOutputPlan.toAddress_;
                    onChanged();
                }
                if (!transactionOutputPlan.getChangeAddress().isEmpty()) {
                    this.changeAddress_ = transactionOutputPlan.changeAddress_;
                    onChanged();
                }
                mergeUnknownFields(transactionOutputPlan.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEO.TransactionOutputPlan.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEO.TransactionOutputPlan.access$7400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEO$TransactionOutputPlan r3 = (wallet.core.jni.proto.NEO.TransactionOutputPlan) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEO$TransactionOutputPlan r4 = (wallet.core.jni.proto.NEO.TransactionOutputPlan) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEO.TransactionOutputPlan.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEO$TransactionOutputPlan$Builder");
            }
        }

        public static Builder newBuilder(TransactionOutputPlan transactionOutputPlan) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionOutputPlan);
        }

        public static TransactionOutputPlan parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionOutputPlan(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionOutputPlan parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutputPlan) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionOutputPlan parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionOutputPlan getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionOutputPlan parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionOutputPlan() {
            this.memoizedIsInitialized = (byte) -1;
            this.assetId_ = "";
            this.toAddress_ = "";
            this.changeAddress_ = "";
        }

        public static TransactionOutputPlan parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionOutputPlan parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionOutputPlan parseFrom(InputStream inputStream) throws IOException {
            return (TransactionOutputPlan) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransactionOutputPlan parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionOutputPlan) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        private TransactionOutputPlan(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 8) {
                                this.amount_ = jVar.y();
                            } else if (J == 16) {
                                this.availableAmount_ = jVar.y();
                            } else if (J == 24) {
                                this.change_ = jVar.y();
                            } else if (J == 34) {
                                this.assetId_ = jVar.I();
                            } else if (J == 42) {
                                this.toAddress_ = jVar.I();
                            } else if (J != 50) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.changeAddress_ = jVar.I();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionOutputPlan parseFrom(j jVar) throws IOException {
            return (TransactionOutputPlan) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionOutputPlan parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionOutputPlan) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionOutputPlanOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        long getAmount();

        String getAssetId();

        ByteString getAssetIdBytes();

        long getAvailableAmount();

        long getChange();

        String getChangeAddress();

        ByteString getChangeAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class TransactionPlan extends GeneratedMessageV3 implements TransactionPlanOrBuilder {
        public static final int ERROR_FIELD_NUMBER = 4;
        public static final int FEE_FIELD_NUMBER = 3;
        public static final int INPUTS_FIELD_NUMBER = 2;
        public static final int OUTPUTS_FIELD_NUMBER = 1;
        private static final long serialVersionUID = 0;
        private int error_;
        private long fee_;
        private List<TransactionInput> inputs_;
        private byte memoizedIsInitialized;
        private List<TransactionOutputPlan> outputs_;
        private static final TransactionPlan DEFAULT_INSTANCE = new TransactionPlan();
        private static final t0<TransactionPlan> PARSER = new c<TransactionPlan>() { // from class: wallet.core.jni.proto.NEO.TransactionPlan.1
            @Override // com.google.protobuf.t0
            public TransactionPlan parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new TransactionPlan(jVar, rVar);
            }
        };

        public static TransactionPlan getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return NEO.internal_static_TW_NEO_Proto_TransactionPlan_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static TransactionPlan parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static TransactionPlan parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<TransactionPlan> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TransactionPlan)) {
                return super.equals(obj);
            }
            TransactionPlan transactionPlan = (TransactionPlan) obj;
            return getOutputsList().equals(transactionPlan.getOutputsList()) && getInputsList().equals(transactionPlan.getInputsList()) && getFee() == transactionPlan.getFee() && this.error_ == transactionPlan.error_ && this.unknownFields.equals(transactionPlan.unknownFields);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public Common.SigningError getError() {
            Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
            return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public int getErrorValue() {
            return this.error_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public long getFee() {
            return this.fee_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public TransactionInput getInputs(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public int getInputsCount() {
            return this.inputs_.size();
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public List<TransactionInput> getInputsList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public TransactionInputOrBuilder getInputsOrBuilder(int i) {
            return this.inputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
            return this.inputs_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public TransactionOutputPlan getOutputs(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public int getOutputsCount() {
            return this.outputs_.size();
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public List<TransactionOutputPlan> getOutputsList() {
            return this.outputs_;
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public TransactionOutputPlanOrBuilder getOutputsOrBuilder(int i) {
            return this.outputs_.get(i);
        }

        @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
        public List<? extends TransactionOutputPlanOrBuilder> getOutputsOrBuilderList() {
            return this.outputs_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<TransactionPlan> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.outputs_.size(); i3++) {
                i2 += CodedOutputStream.G(1, this.outputs_.get(i3));
            }
            for (int i4 = 0; i4 < this.inputs_.size(); i4++) {
                i2 += CodedOutputStream.G(2, this.inputs_.get(i4));
            }
            long j = this.fee_;
            if (j != 0) {
                i2 += CodedOutputStream.z(3, j);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                i2 += CodedOutputStream.l(4, this.error_);
            }
            int serializedSize = i2 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = 779 + getDescriptor().hashCode();
            if (getOutputsCount() > 0) {
                hashCode = (((hashCode * 37) + 1) * 53) + getOutputsList().hashCode();
            }
            if (getInputsCount() > 0) {
                hashCode = (((hashCode * 37) + 2) * 53) + getInputsList().hashCode();
            }
            int h = (((((((((hashCode * 37) + 3) * 53) + a0.h(getFee())) * 37) + 4) * 53) + this.error_) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = h;
            return h;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return NEO.internal_static_TW_NEO_Proto_TransactionPlan_fieldAccessorTable.d(TransactionPlan.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new TransactionPlan();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            for (int i = 0; i < this.outputs_.size(); i++) {
                codedOutputStream.K0(1, this.outputs_.get(i));
            }
            for (int i2 = 0; i2 < this.inputs_.size(); i2++) {
                codedOutputStream.K0(2, this.inputs_.get(i2));
            }
            long j = this.fee_;
            if (j != 0) {
                codedOutputStream.I0(3, j);
            }
            if (this.error_ != Common.SigningError.OK.getNumber()) {
                codedOutputStream.u0(4, this.error_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements TransactionPlanOrBuilder {
            private int bitField0_;
            private int error_;
            private long fee_;
            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> inputsBuilder_;
            private List<TransactionInput> inputs_;
            private x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> outputsBuilder_;
            private List<TransactionOutputPlan> outputs_;

            private void ensureInputsIsMutable() {
                if ((this.bitField0_ & 2) == 0) {
                    this.inputs_ = new ArrayList(this.inputs_);
                    this.bitField0_ |= 2;
                }
            }

            private void ensureOutputsIsMutable() {
                if ((this.bitField0_ & 1) == 0) {
                    this.outputs_ = new ArrayList(this.outputs_);
                    this.bitField0_ |= 1;
                }
            }

            public static final Descriptors.b getDescriptor() {
                return NEO.internal_static_TW_NEO_Proto_TransactionPlan_descriptor;
            }

            private x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> getInputsFieldBuilder() {
                if (this.inputsBuilder_ == null) {
                    this.inputsBuilder_ = new x0<>(this.inputs_, (this.bitField0_ & 2) != 0, getParentForChildren(), isClean());
                    this.inputs_ = null;
                }
                return this.inputsBuilder_;
            }

            private x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> getOutputsFieldBuilder() {
                if (this.outputsBuilder_ == null) {
                    this.outputsBuilder_ = new x0<>(this.outputs_, (this.bitField0_ & 1) != 0, getParentForChildren(), isClean());
                    this.outputs_ = null;
                }
                return this.outputsBuilder_;
            }

            private void maybeForceBuilderInitialization() {
                if (GeneratedMessageV3.alwaysUseFieldBuilders) {
                    getOutputsFieldBuilder();
                    getInputsFieldBuilder();
                }
            }

            public Builder addAllInputs(Iterable<? extends TransactionInput> iterable) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.inputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addAllOutputs(Iterable<? extends TransactionOutputPlan> iterable) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    b.a.addAll((Iterable) iterable, (List) this.outputs_);
                    onChanged();
                } else {
                    x0Var.b(iterable);
                }
                return this;
            }

            public Builder addInputs(TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(transactionInput);
                    onChanged();
                } else {
                    x0Var.f(transactionInput);
                }
                return this;
            }

            public TransactionInput.Builder addInputsBuilder() {
                return getInputsFieldBuilder().d(TransactionInput.getDefaultInstance());
            }

            public Builder addOutputs(TransactionOutputPlan transactionOutputPlan) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutputPlan);
                    ensureOutputsIsMutable();
                    this.outputs_.add(transactionOutputPlan);
                    onChanged();
                } else {
                    x0Var.f(transactionOutputPlan);
                }
                return this;
            }

            public TransactionOutputPlan.Builder addOutputsBuilder() {
                return getOutputsFieldBuilder().d(TransactionOutputPlan.getDefaultInstance());
            }

            public Builder clearError() {
                this.error_ = 0;
                onChanged();
                return this;
            }

            public Builder clearFee() {
                this.fee_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearInputs() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            public Builder clearOutputs() {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                    onChanged();
                } else {
                    x0Var.h();
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return NEO.internal_static_TW_NEO_Proto_TransactionPlan_descriptor;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public Common.SigningError getError() {
                Common.SigningError valueOf = Common.SigningError.valueOf(this.error_);
                return valueOf == null ? Common.SigningError.UNRECOGNIZED : valueOf;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public int getErrorValue() {
                return this.error_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public long getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public TransactionInput getInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionInput.Builder getInputsBuilder(int i) {
                return getInputsFieldBuilder().l(i);
            }

            public List<TransactionInput.Builder> getInputsBuilderList() {
                return getInputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public int getInputsCount() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public List<TransactionInput> getInputsList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.inputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public TransactionInputOrBuilder getInputsOrBuilder(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    return this.inputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public List<? extends TransactionInputOrBuilder> getInputsOrBuilderList() {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.inputs_);
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public TransactionOutputPlan getOutputs(int i) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.o(i);
            }

            public TransactionOutputPlan.Builder getOutputsBuilder(int i) {
                return getOutputsFieldBuilder().l(i);
            }

            public List<TransactionOutputPlan.Builder> getOutputsBuilderList() {
                return getOutputsFieldBuilder().m();
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public int getOutputsCount() {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.size();
                }
                return x0Var.n();
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public List<TransactionOutputPlan> getOutputsList() {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return Collections.unmodifiableList(this.outputs_);
                }
                return x0Var.q();
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public TransactionOutputPlanOrBuilder getOutputsOrBuilder(int i) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    return this.outputs_.get(i);
                }
                return x0Var.r(i);
            }

            @Override // wallet.core.jni.proto.NEO.TransactionPlanOrBuilder
            public List<? extends TransactionOutputPlanOrBuilder> getOutputsOrBuilderList() {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var != null) {
                    return x0Var.s();
                }
                return Collections.unmodifiableList(this.outputs_);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return NEO.internal_static_TW_NEO_Proto_TransactionPlan_fieldAccessorTable.d(TransactionPlan.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder removeInputs(int i) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder removeOutputs(int i) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.remove(i);
                    onChanged();
                } else {
                    x0Var.w(i);
                }
                return this;
            }

            public Builder setError(Common.SigningError signingError) {
                Objects.requireNonNull(signingError);
                this.error_ = signingError.getNumber();
                onChanged();
                return this;
            }

            public Builder setErrorValue(int i) {
                this.error_ = i;
                onChanged();
                return this;
            }

            public Builder setFee(long j) {
                this.fee_ = j;
                onChanged();
                return this;
            }

            public Builder setInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.set(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.x(i, transactionInput);
                }
                return this;
            }

            public Builder setOutputs(int i, TransactionOutputPlan transactionOutputPlan) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutputPlan);
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, transactionOutputPlan);
                    onChanged();
                } else {
                    x0Var.x(i, transactionOutputPlan);
                }
                return this;
            }

            private Builder() {
                this.outputs_ = Collections.emptyList();
                this.inputs_ = Collections.emptyList();
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionPlan build() {
                TransactionPlan buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public TransactionPlan buildPartial() {
                TransactionPlan transactionPlan = new TransactionPlan(this);
                int i = this.bitField0_;
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    if ((i & 1) != 0) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                        this.bitField0_ &= -2;
                    }
                    transactionPlan.outputs_ = this.outputs_;
                } else {
                    transactionPlan.outputs_ = x0Var.g();
                }
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var2 = this.inputsBuilder_;
                if (x0Var2 != null) {
                    transactionPlan.inputs_ = x0Var2.g();
                } else {
                    if ((this.bitField0_ & 2) != 0) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                        this.bitField0_ &= -3;
                    }
                    transactionPlan.inputs_ = this.inputs_;
                }
                transactionPlan.fee_ = this.fee_;
                transactionPlan.error_ = this.error_;
                onBuilt();
                return transactionPlan;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public TransactionPlan getDefaultInstanceForType() {
                return TransactionPlan.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            public TransactionInput.Builder addInputsBuilder(int i) {
                return getInputsFieldBuilder().c(i, TransactionInput.getDefaultInstance());
            }

            public TransactionOutputPlan.Builder addOutputsBuilder(int i) {
                return getOutputsFieldBuilder().c(i, TransactionOutputPlan.getDefaultInstance());
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    this.outputs_ = Collections.emptyList();
                    this.bitField0_ &= -2;
                } else {
                    x0Var.h();
                }
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var2 = this.inputsBuilder_;
                if (x0Var2 == null) {
                    this.inputs_ = Collections.emptyList();
                    this.bitField0_ &= -3;
                } else {
                    x0Var2.h();
                }
                this.fee_ = 0L;
                this.error_ = 0;
                return this;
            }

            public Builder addInputs(int i, TransactionInput transactionInput) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionInput);
                    ensureInputsIsMutable();
                    this.inputs_.add(i, transactionInput);
                    onChanged();
                } else {
                    x0Var.e(i, transactionInput);
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutputPlan transactionOutputPlan) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    Objects.requireNonNull(transactionOutputPlan);
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, transactionOutputPlan);
                    onChanged();
                } else {
                    x0Var.e(i, transactionOutputPlan);
                }
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof TransactionPlan) {
                    return mergeFrom((TransactionPlan) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder setInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            public Builder setOutputs(int i, TransactionOutputPlan.Builder builder) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.set(i, builder.build());
                    onChanged();
                } else {
                    x0Var.x(i, builder.build());
                }
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.outputs_ = Collections.emptyList();
                this.inputs_ = Collections.emptyList();
                this.error_ = 0;
                maybeForceBuilderInitialization();
            }

            public Builder mergeFrom(TransactionPlan transactionPlan) {
                if (transactionPlan == TransactionPlan.getDefaultInstance()) {
                    return this;
                }
                if (this.outputsBuilder_ == null) {
                    if (!transactionPlan.outputs_.isEmpty()) {
                        if (this.outputs_.isEmpty()) {
                            this.outputs_ = transactionPlan.outputs_;
                            this.bitField0_ &= -2;
                        } else {
                            ensureOutputsIsMutable();
                            this.outputs_.addAll(transactionPlan.outputs_);
                        }
                        onChanged();
                    }
                } else if (!transactionPlan.outputs_.isEmpty()) {
                    if (!this.outputsBuilder_.u()) {
                        this.outputsBuilder_.b(transactionPlan.outputs_);
                    } else {
                        this.outputsBuilder_.i();
                        this.outputsBuilder_ = null;
                        this.outputs_ = transactionPlan.outputs_;
                        this.bitField0_ &= -2;
                        this.outputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getOutputsFieldBuilder() : null;
                    }
                }
                if (this.inputsBuilder_ == null) {
                    if (!transactionPlan.inputs_.isEmpty()) {
                        if (this.inputs_.isEmpty()) {
                            this.inputs_ = transactionPlan.inputs_;
                            this.bitField0_ &= -3;
                        } else {
                            ensureInputsIsMutable();
                            this.inputs_.addAll(transactionPlan.inputs_);
                        }
                        onChanged();
                    }
                } else if (!transactionPlan.inputs_.isEmpty()) {
                    if (!this.inputsBuilder_.u()) {
                        this.inputsBuilder_.b(transactionPlan.inputs_);
                    } else {
                        this.inputsBuilder_.i();
                        this.inputsBuilder_ = null;
                        this.inputs_ = transactionPlan.inputs_;
                        this.bitField0_ &= -3;
                        this.inputsBuilder_ = GeneratedMessageV3.alwaysUseFieldBuilders ? getInputsFieldBuilder() : null;
                    }
                }
                if (transactionPlan.getFee() != 0) {
                    setFee(transactionPlan.getFee());
                }
                if (transactionPlan.error_ != 0) {
                    setErrorValue(transactionPlan.getErrorValue());
                }
                mergeUnknownFields(transactionPlan.unknownFields);
                onChanged();
                return this;
            }

            public Builder addInputs(TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addOutputs(TransactionOutputPlan.Builder builder) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(builder.build());
                    onChanged();
                } else {
                    x0Var.f(builder.build());
                }
                return this;
            }

            public Builder addInputs(int i, TransactionInput.Builder builder) {
                x0<TransactionInput, TransactionInput.Builder, TransactionInputOrBuilder> x0Var = this.inputsBuilder_;
                if (x0Var == null) {
                    ensureInputsIsMutable();
                    this.inputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            public Builder addOutputs(int i, TransactionOutputPlan.Builder builder) {
                x0<TransactionOutputPlan, TransactionOutputPlan.Builder, TransactionOutputPlanOrBuilder> x0Var = this.outputsBuilder_;
                if (x0Var == null) {
                    ensureOutputsIsMutable();
                    this.outputs_.add(i, builder.build());
                    onChanged();
                } else {
                    x0Var.e(i, builder.build());
                }
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.NEO.TransactionPlan.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.NEO.TransactionPlan.access$9200()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.NEO$TransactionPlan r3 = (wallet.core.jni.proto.NEO.TransactionPlan) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.NEO$TransactionPlan r4 = (wallet.core.jni.proto.NEO.TransactionPlan) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.NEO.TransactionPlan.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.NEO$TransactionPlan$Builder");
            }
        }

        public static Builder newBuilder(TransactionPlan transactionPlan) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(transactionPlan);
        }

        public static TransactionPlan parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private TransactionPlan(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static TransactionPlan parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static TransactionPlan parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public TransactionPlan getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static TransactionPlan parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private TransactionPlan() {
            this.memoizedIsInitialized = (byte) -1;
            this.outputs_ = Collections.emptyList();
            this.inputs_ = Collections.emptyList();
            this.error_ = 0;
        }

        public static TransactionPlan parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static TransactionPlan parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static TransactionPlan parseFrom(InputStream inputStream) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static TransactionPlan parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private TransactionPlan(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    if (!(z2 & true)) {
                                        this.outputs_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.outputs_.add(jVar.z(TransactionOutputPlan.parser(), rVar));
                                } else if (J == 18) {
                                    if (!(z2 & true)) {
                                        this.inputs_ = new ArrayList();
                                        z2 |= true;
                                    }
                                    this.inputs_.add(jVar.z(TransactionInput.parser(), rVar));
                                } else if (J == 24) {
                                    this.fee_ = jVar.y();
                                } else if (J != 32) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.error_ = jVar.s();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    if (z2 & true) {
                        this.outputs_ = Collections.unmodifiableList(this.outputs_);
                    }
                    if (z2 & true) {
                        this.inputs_ = Collections.unmodifiableList(this.inputs_);
                    }
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static TransactionPlan parseFrom(j jVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static TransactionPlan parseFrom(j jVar, r rVar) throws IOException {
            return (TransactionPlan) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface TransactionPlanOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        Common.SigningError getError();

        int getErrorValue();

        long getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        TransactionInput getInputs(int i);

        int getInputsCount();

        List<TransactionInput> getInputsList();

        TransactionInputOrBuilder getInputsOrBuilder(int i);

        List<? extends TransactionInputOrBuilder> getInputsOrBuilderList();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        TransactionOutputPlan getOutputs(int i);

        int getOutputsCount();

        List<TransactionOutputPlan> getOutputsList();

        TransactionOutputPlanOrBuilder getOutputsOrBuilder(int i);

        List<? extends TransactionOutputPlanOrBuilder> getOutputsOrBuilderList();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_NEO_Proto_TransactionInput_descriptor = bVar;
        internal_static_TW_NEO_Proto_TransactionInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"PrevHash", "PrevIndex", "Value", "AssetId"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_NEO_Proto_TransactionOutput_descriptor = bVar2;
        internal_static_TW_NEO_Proto_TransactionOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"AssetId", "Amount", "ToAddress", "ChangeAddress"});
        Descriptors.b bVar3 = getDescriptor().o().get(2);
        internal_static_TW_NEO_Proto_SigningInput_descriptor = bVar3;
        internal_static_TW_NEO_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar3, new String[]{"Inputs", "Outputs", "PrivateKey", "Fee", "GasAssetId", "GasChangeAddress", "Plan"});
        Descriptors.b bVar4 = getDescriptor().o().get(3);
        internal_static_TW_NEO_Proto_SigningOutput_descriptor = bVar4;
        internal_static_TW_NEO_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar4, new String[]{"Encoded", "Error"});
        Descriptors.b bVar5 = getDescriptor().o().get(4);
        internal_static_TW_NEO_Proto_TransactionOutputPlan_descriptor = bVar5;
        internal_static_TW_NEO_Proto_TransactionOutputPlan_fieldAccessorTable = new GeneratedMessageV3.e(bVar5, new String[]{"Amount", "AvailableAmount", "Change", "AssetId", "ToAddress", "ChangeAddress"});
        Descriptors.b bVar6 = getDescriptor().o().get(5);
        internal_static_TW_NEO_Proto_TransactionPlan_descriptor = bVar6;
        internal_static_TW_NEO_Proto_TransactionPlan_fieldAccessorTable = new GeneratedMessageV3.e(bVar6, new String[]{"Outputs", "Inputs", "Fee", "Error"});
        Common.getDescriptor();
    }

    private NEO() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
