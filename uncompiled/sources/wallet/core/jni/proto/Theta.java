package wallet.core.jni.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.b;
import com.google.protobuf.c;
import com.google.protobuf.g1;
import com.google.protobuf.j;
import com.google.protobuf.l0;
import com.google.protobuf.m0;
import com.google.protobuf.o0;
import com.google.protobuf.q;
import com.google.protobuf.r;
import com.google.protobuf.t0;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class Theta {
    private static Descriptors.FileDescriptor descriptor = Descriptors.FileDescriptor.u(new String[]{"\n\u000bTheta.proto\u0012\u000eTW.Theta.Proto\"\u0094\u0001\n\fSigningInput\u0012\u0010\n\bchain_id\u0018\u0001 \u0001(\t\u0012\u0012\n\nto_address\u0018\u0002 \u0001(\t\u0012\u0014\n\ftheta_amount\u0018\u0003 \u0001(\f\u0012\u0014\n\ftfuel_amount\u0018\u0004 \u0001(\f\u0012\u0010\n\bsequence\u0018\u0005 \u0001(\u0004\u0012\u000b\n\u0003fee\u0018\u0006 \u0001(\f\u0012\u0013\n\u000bprivate_key\u0018\u0007 \u0001(\f\"3\n\rSigningOutput\u0012\u000f\n\u0007encoded\u0018\u0001 \u0001(\f\u0012\u0011\n\tsignature\u0018\u0002 \u0001(\fB\u0017\n\u0015wallet.core.jni.protob\u0006proto3"}, new Descriptors.FileDescriptor[0]);
    private static final Descriptors.b internal_static_TW_Theta_Proto_SigningInput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Theta_Proto_SigningInput_fieldAccessorTable;
    private static final Descriptors.b internal_static_TW_Theta_Proto_SigningOutput_descriptor;
    private static final GeneratedMessageV3.e internal_static_TW_Theta_Proto_SigningOutput_fieldAccessorTable;

    /* loaded from: classes3.dex */
    public static final class SigningInput extends GeneratedMessageV3 implements SigningInputOrBuilder {
        public static final int CHAIN_ID_FIELD_NUMBER = 1;
        public static final int FEE_FIELD_NUMBER = 6;
        public static final int PRIVATE_KEY_FIELD_NUMBER = 7;
        public static final int SEQUENCE_FIELD_NUMBER = 5;
        public static final int TFUEL_AMOUNT_FIELD_NUMBER = 4;
        public static final int THETA_AMOUNT_FIELD_NUMBER = 3;
        public static final int TO_ADDRESS_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private volatile Object chainId_;
        private ByteString fee_;
        private byte memoizedIsInitialized;
        private ByteString privateKey_;
        private long sequence_;
        private ByteString tfuelAmount_;
        private ByteString thetaAmount_;
        private volatile Object toAddress_;
        private static final SigningInput DEFAULT_INSTANCE = new SigningInput();
        private static final t0<SigningInput> PARSER = new c<SigningInput>() { // from class: wallet.core.jni.proto.Theta.SigningInput.1
            @Override // com.google.protobuf.t0
            public SigningInput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningInput(jVar, rVar);
            }
        };

        public static SigningInput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Theta.internal_static_TW_Theta_Proto_SigningInput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningInput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningInput)) {
                return super.equals(obj);
            }
            SigningInput signingInput = (SigningInput) obj;
            return getChainId().equals(signingInput.getChainId()) && getToAddress().equals(signingInput.getToAddress()) && getThetaAmount().equals(signingInput.getThetaAmount()) && getTfuelAmount().equals(signingInput.getTfuelAmount()) && getSequence() == signingInput.getSequence() && getFee().equals(signingInput.getFee()) && getPrivateKey().equals(signingInput.getPrivateKey()) && this.unknownFields.equals(signingInput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public String getChainId() {
            Object obj = this.chainId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.chainId_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public ByteString getChainIdBytes() {
            Object obj = this.chainId_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.chainId_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public ByteString getFee() {
            return this.fee_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningInput> getParserForType() {
            return PARSER;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public ByteString getPrivateKey() {
            return this.privateKey_;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public long getSequence() {
            return this.sequence_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int computeStringSize = getChainIdBytes().isEmpty() ? 0 : 0 + GeneratedMessageV3.computeStringSize(1, this.chainId_);
            if (!getToAddressBytes().isEmpty()) {
                computeStringSize += GeneratedMessageV3.computeStringSize(2, this.toAddress_);
            }
            if (!this.thetaAmount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(3, this.thetaAmount_);
            }
            if (!this.tfuelAmount_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(4, this.tfuelAmount_);
            }
            long j = this.sequence_;
            if (j != 0) {
                computeStringSize += CodedOutputStream.a0(5, j);
            }
            if (!this.fee_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(6, this.fee_);
            }
            if (!this.privateKey_.isEmpty()) {
                computeStringSize += CodedOutputStream.h(7, this.privateKey_);
            }
            int serializedSize = computeStringSize + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public ByteString getTfuelAmount() {
            return this.tfuelAmount_;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public ByteString getThetaAmount() {
            return this.thetaAmount_;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public String getToAddress() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.toAddress_ = stringUtf8;
            return stringUtf8;
        }

        @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
        public ByteString getToAddressBytes() {
            Object obj = this.toAddress_;
            if (obj instanceof String) {
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.toAddress_ = copyFromUtf8;
                return copyFromUtf8;
            }
            return (ByteString) obj;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getChainId().hashCode()) * 37) + 2) * 53) + getToAddress().hashCode()) * 37) + 3) * 53) + getThetaAmount().hashCode()) * 37) + 4) * 53) + getTfuelAmount().hashCode()) * 37) + 5) * 53) + a0.h(getSequence())) * 37) + 6) * 53) + getFee().hashCode()) * 37) + 7) * 53) + getPrivateKey().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Theta.internal_static_TW_Theta_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningInput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getChainIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.chainId_);
            }
            if (!getToAddressBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 2, this.toAddress_);
            }
            if (!this.thetaAmount_.isEmpty()) {
                codedOutputStream.q0(3, this.thetaAmount_);
            }
            if (!this.tfuelAmount_.isEmpty()) {
                codedOutputStream.q0(4, this.tfuelAmount_);
            }
            long j = this.sequence_;
            if (j != 0) {
                codedOutputStream.d1(5, j);
            }
            if (!this.fee_.isEmpty()) {
                codedOutputStream.q0(6, this.fee_);
            }
            if (!this.privateKey_.isEmpty()) {
                codedOutputStream.q0(7, this.privateKey_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningInputOrBuilder {
            private Object chainId_;
            private ByteString fee_;
            private ByteString privateKey_;
            private long sequence_;
            private ByteString tfuelAmount_;
            private ByteString thetaAmount_;
            private Object toAddress_;

            public static final Descriptors.b getDescriptor() {
                return Theta.internal_static_TW_Theta_Proto_SigningInput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearChainId() {
                this.chainId_ = SigningInput.getDefaultInstance().getChainId();
                onChanged();
                return this;
            }

            public Builder clearFee() {
                this.fee_ = SigningInput.getDefaultInstance().getFee();
                onChanged();
                return this;
            }

            public Builder clearPrivateKey() {
                this.privateKey_ = SigningInput.getDefaultInstance().getPrivateKey();
                onChanged();
                return this;
            }

            public Builder clearSequence() {
                this.sequence_ = 0L;
                onChanged();
                return this;
            }

            public Builder clearTfuelAmount() {
                this.tfuelAmount_ = SigningInput.getDefaultInstance().getTfuelAmount();
                onChanged();
                return this;
            }

            public Builder clearThetaAmount() {
                this.thetaAmount_ = SigningInput.getDefaultInstance().getThetaAmount();
                onChanged();
                return this;
            }

            public Builder clearToAddress() {
                this.toAddress_ = SigningInput.getDefaultInstance().getToAddress();
                onChanged();
                return this;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public String getChainId() {
                Object obj = this.chainId_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.chainId_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public ByteString getChainIdBytes() {
                Object obj = this.chainId_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.chainId_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Theta.internal_static_TW_Theta_Proto_SigningInput_descriptor;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public ByteString getFee() {
                return this.fee_;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public ByteString getPrivateKey() {
                return this.privateKey_;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public long getSequence() {
                return this.sequence_;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public ByteString getTfuelAmount() {
                return this.tfuelAmount_;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public ByteString getThetaAmount() {
                return this.thetaAmount_;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public String getToAddress() {
                Object obj = this.toAddress_;
                if (!(obj instanceof String)) {
                    String stringUtf8 = ((ByteString) obj).toStringUtf8();
                    this.toAddress_ = stringUtf8;
                    return stringUtf8;
                }
                return (String) obj;
            }

            @Override // wallet.core.jni.proto.Theta.SigningInputOrBuilder
            public ByteString getToAddressBytes() {
                Object obj = this.toAddress_;
                if (obj instanceof String) {
                    ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                    this.toAddress_ = copyFromUtf8;
                    return copyFromUtf8;
                }
                return (ByteString) obj;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Theta.internal_static_TW_Theta_Proto_SigningInput_fieldAccessorTable.d(SigningInput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setChainId(String str) {
                Objects.requireNonNull(str);
                this.chainId_ = str;
                onChanged();
                return this;
            }

            public Builder setChainIdBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.chainId_ = byteString;
                onChanged();
                return this;
            }

            public Builder setFee(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.fee_ = byteString;
                onChanged();
                return this;
            }

            public Builder setPrivateKey(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.privateKey_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSequence(long j) {
                this.sequence_ = j;
                onChanged();
                return this;
            }

            public Builder setTfuelAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.tfuelAmount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setThetaAmount(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.thetaAmount_ = byteString;
                onChanged();
                return this;
            }

            public Builder setToAddress(String str) {
                Objects.requireNonNull(str);
                this.toAddress_ = str;
                onChanged();
                return this;
            }

            public Builder setToAddressBytes(ByteString byteString) {
                Objects.requireNonNull(byteString);
                b.checkByteStringIsUtf8(byteString);
                this.toAddress_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                this.chainId_ = "";
                this.toAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.thetaAmount_ = byteString;
                this.tfuelAmount_ = byteString;
                this.fee_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput build() {
                SigningInput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningInput buildPartial() {
                SigningInput signingInput = new SigningInput(this);
                signingInput.chainId_ = this.chainId_;
                signingInput.toAddress_ = this.toAddress_;
                signingInput.thetaAmount_ = this.thetaAmount_;
                signingInput.tfuelAmount_ = this.tfuelAmount_;
                signingInput.sequence_ = this.sequence_;
                signingInput.fee_ = this.fee_;
                signingInput.privateKey_ = this.privateKey_;
                onBuilt();
                return signingInput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningInput getDefaultInstanceForType() {
                return SigningInput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                this.chainId_ = "";
                this.toAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.thetaAmount_ = byteString;
                this.tfuelAmount_ = byteString;
                this.sequence_ = 0L;
                this.fee_ = byteString;
                this.privateKey_ = byteString;
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningInput) {
                    return mergeFrom((SigningInput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningInput signingInput) {
                if (signingInput == SigningInput.getDefaultInstance()) {
                    return this;
                }
                if (!signingInput.getChainId().isEmpty()) {
                    this.chainId_ = signingInput.chainId_;
                    onChanged();
                }
                if (!signingInput.getToAddress().isEmpty()) {
                    this.toAddress_ = signingInput.toAddress_;
                    onChanged();
                }
                ByteString thetaAmount = signingInput.getThetaAmount();
                ByteString byteString = ByteString.EMPTY;
                if (thetaAmount != byteString) {
                    setThetaAmount(signingInput.getThetaAmount());
                }
                if (signingInput.getTfuelAmount() != byteString) {
                    setTfuelAmount(signingInput.getTfuelAmount());
                }
                if (signingInput.getSequence() != 0) {
                    setSequence(signingInput.getSequence());
                }
                if (signingInput.getFee() != byteString) {
                    setFee(signingInput.getFee());
                }
                if (signingInput.getPrivateKey() != byteString) {
                    setPrivateKey(signingInput.getPrivateKey());
                }
                mergeUnknownFields(signingInput.unknownFields);
                onChanged();
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                this.chainId_ = "";
                this.toAddress_ = "";
                ByteString byteString = ByteString.EMPTY;
                this.thetaAmount_ = byteString;
                this.tfuelAmount_ = byteString;
                this.fee_ = byteString;
                this.privateKey_ = byteString;
                maybeForceBuilderInitialization();
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Theta.SigningInput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Theta.SigningInput.access$1400()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Theta$SigningInput r3 = (wallet.core.jni.proto.Theta.SigningInput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Theta$SigningInput r4 = (wallet.core.jni.proto.Theta.SigningInput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Theta.SigningInput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Theta$SigningInput$Builder");
            }
        }

        public static Builder newBuilder(SigningInput signingInput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingInput);
        }

        public static SigningInput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningInput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningInput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningInput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningInput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningInput() {
            this.memoizedIsInitialized = (byte) -1;
            this.chainId_ = "";
            this.toAddress_ = "";
            ByteString byteString = ByteString.EMPTY;
            this.thetaAmount_ = byteString;
            this.tfuelAmount_ = byteString;
            this.fee_ = byteString;
            this.privateKey_ = byteString;
        }

        public static SigningInput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningInput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningInput parseFrom(InputStream inputStream) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static SigningInput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningInput parseFrom(j jVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        private SigningInput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        int J = jVar.J();
                        if (J != 0) {
                            if (J == 10) {
                                this.chainId_ = jVar.I();
                            } else if (J == 18) {
                                this.toAddress_ = jVar.I();
                            } else if (J == 26) {
                                this.thetaAmount_ = jVar.q();
                            } else if (J == 34) {
                                this.tfuelAmount_ = jVar.q();
                            } else if (J == 40) {
                                this.sequence_ = jVar.L();
                            } else if (J == 50) {
                                this.fee_ = jVar.q();
                            } else if (J != 58) {
                                if (!parseUnknownField(jVar, g, rVar, J)) {
                                }
                            } else {
                                this.privateKey_ = jVar.q();
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningInput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningInput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningInputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        String getChainId();

        ByteString getChainIdBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getFee();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        ByteString getPrivateKey();

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        long getSequence();

        ByteString getTfuelAmount();

        ByteString getThetaAmount();

        String getToAddress();

        ByteString getToAddressBytes();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes3.dex */
    public static final class SigningOutput extends GeneratedMessageV3 implements SigningOutputOrBuilder {
        public static final int ENCODED_FIELD_NUMBER = 1;
        public static final int SIGNATURE_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        private ByteString encoded_;
        private byte memoizedIsInitialized;
        private ByteString signature_;
        private static final SigningOutput DEFAULT_INSTANCE = new SigningOutput();
        private static final t0<SigningOutput> PARSER = new c<SigningOutput>() { // from class: wallet.core.jni.proto.Theta.SigningOutput.1
            @Override // com.google.protobuf.t0
            public SigningOutput parsePartialFrom(j jVar, r rVar) throws InvalidProtocolBufferException {
                return new SigningOutput(jVar, rVar);
            }
        };

        public static SigningOutput getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static final Descriptors.b getDescriptor() {
            return Theta.internal_static_TW_Theta_Proto_SigningOutput_descriptor;
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer);
        }

        public static t0<SigningOutput> parser() {
            return PARSER;
        }

        @Override // com.google.protobuf.a
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SigningOutput)) {
                return super.equals(obj);
            }
            SigningOutput signingOutput = (SigningOutput) obj;
            return getEncoded().equals(signingOutput.getEncoded()) && getSignature().equals(signingOutput.getSignature()) && this.unknownFields.equals(signingOutput.unknownFields);
        }

        @Override // wallet.core.jni.proto.Theta.SigningOutputOrBuilder
        public ByteString getEncoded() {
            return this.encoded_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0
        public t0<SigningOutput> getParserForType() {
            return PARSER;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int h = this.encoded_.isEmpty() ? 0 : 0 + CodedOutputStream.h(1, this.encoded_);
            if (!this.signature_.isEmpty()) {
                h += CodedOutputStream.h(2, this.signature_);
            }
            int serializedSize = h + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        @Override // wallet.core.jni.proto.Theta.SigningOutputOrBuilder
        public ByteString getSignature() {
            return this.signature_;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.o0
        public final g1 getUnknownFields() {
            return this.unknownFields;
        }

        @Override // com.google.protobuf.a
        public int hashCode() {
            int i = this.memoizedHashCode;
            if (i != 0) {
                return i;
            }
            int hashCode = ((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getEncoded().hashCode()) * 37) + 2) * 53) + getSignature().hashCode()) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public GeneratedMessageV3.e internalGetFieldAccessorTable() {
            return Theta.internal_static_TW_Theta_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, defpackage.d82
        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Object newInstance(GeneratedMessageV3.f fVar) {
            return new SigningOutput();
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.a, com.google.protobuf.m0
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!this.encoded_.isEmpty()) {
                codedOutputStream.q0(1, this.encoded_);
            }
            if (!this.signature_.isEmpty()) {
                codedOutputStream.q0(2, this.signature_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageV3.b<Builder> implements SigningOutputOrBuilder {
            private ByteString encoded_;
            private ByteString signature_;

            public static final Descriptors.b getDescriptor() {
                return Theta.internal_static_TW_Theta_Proto_SigningOutput_descriptor;
            }

            private void maybeForceBuilderInitialization() {
                boolean unused = GeneratedMessageV3.alwaysUseFieldBuilders;
            }

            public Builder clearEncoded() {
                this.encoded_ = SigningOutput.getDefaultInstance().getEncoded();
                onChanged();
                return this;
            }

            public Builder clearSignature() {
                this.signature_ = SigningOutput.getDefaultInstance().getSignature();
                onChanged();
                return this;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a, com.google.protobuf.o0
            public Descriptors.b getDescriptorForType() {
                return Theta.internal_static_TW_Theta_Proto_SigningOutput_descriptor;
            }

            @Override // wallet.core.jni.proto.Theta.SigningOutputOrBuilder
            public ByteString getEncoded() {
                return this.encoded_;
            }

            @Override // wallet.core.jni.proto.Theta.SigningOutputOrBuilder
            public ByteString getSignature() {
                return this.signature_;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            public GeneratedMessageV3.e internalGetFieldAccessorTable() {
                return Theta.internal_static_TW_Theta_Proto_SigningOutput_fieldAccessorTable.d(SigningOutput.class, Builder.class);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, defpackage.d82
            public final boolean isInitialized() {
                return true;
            }

            public Builder setEncoded(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.encoded_ = byteString;
                onChanged();
                return this;
            }

            public Builder setSignature(ByteString byteString) {
                Objects.requireNonNull(byteString);
                this.signature_ = byteString;
                onChanged();
                return this;
            }

            private Builder() {
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder addRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput build() {
                SigningOutput buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw a.AbstractC0151a.newUninitializedMessageException((l0) buildPartial);
            }

            @Override // com.google.protobuf.m0.a, com.google.protobuf.l0.a
            public SigningOutput buildPartial() {
                SigningOutput signingOutput = new SigningOutput(this);
                signingOutput.encoded_ = this.encoded_;
                signingOutput.signature_ = this.signature_;
                onBuilt();
                return signingOutput;
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder clearField(Descriptors.FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            @Override // defpackage.d82, com.google.protobuf.o0
            public SigningOutput getDefaultInstanceForType() {
                return SigningOutput.getDefaultInstance();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public Builder setField(Descriptors.FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b
            /* renamed from: setRepeatedField */
            public Builder setRepeatedField2(Descriptors.FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.m19setRepeatedField(fieldDescriptor, i, obj);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.l0.a
            public final Builder setUnknownFields(g1 g1Var) {
                return (Builder) super.setUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clearOneof(Descriptors.g gVar) {
                return (Builder) super.clearOneof(gVar);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public final Builder mergeUnknownFields(g1 g1Var) {
                return (Builder) super.mergeUnknownFields(g1Var);
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a
            public Builder clear() {
                super.clear();
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.signature_ = byteString;
                return this;
            }

            private Builder(GeneratedMessageV3.c cVar) {
                super(cVar);
                ByteString byteString = ByteString.EMPTY;
                this.encoded_ = byteString;
                this.signature_ = byteString;
                maybeForceBuilderInitialization();
            }

            @Override // com.google.protobuf.GeneratedMessageV3.b, com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a
            public Builder clone() {
                return (Builder) super.clone();
            }

            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.l0.a
            public Builder mergeFrom(l0 l0Var) {
                if (l0Var instanceof SigningOutput) {
                    return mergeFrom((SigningOutput) l0Var);
                }
                super.mergeFrom(l0Var);
                return this;
            }

            public Builder mergeFrom(SigningOutput signingOutput) {
                if (signingOutput == SigningOutput.getDefaultInstance()) {
                    return this;
                }
                ByteString encoded = signingOutput.getEncoded();
                ByteString byteString = ByteString.EMPTY;
                if (encoded != byteString) {
                    setEncoded(signingOutput.getEncoded());
                }
                if (signingOutput.getSignature() != byteString) {
                    setSignature(signingOutput.getSignature());
                }
                mergeUnknownFields(signingOutput.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
            @Override // com.google.protobuf.a.AbstractC0151a, com.google.protobuf.b.a, com.google.protobuf.m0.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public wallet.core.jni.proto.Theta.SigningOutput.Builder mergeFrom(com.google.protobuf.j r3, com.google.protobuf.r r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    com.google.protobuf.t0 r1 = wallet.core.jni.proto.Theta.SigningOutput.access$2700()     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    wallet.core.jni.proto.Theta$SigningOutput r3 = (wallet.core.jni.proto.Theta.SigningOutput) r3     // Catch: java.lang.Throwable -> L11 com.google.protobuf.InvalidProtocolBufferException -> L13
                    if (r3 == 0) goto L10
                    r2.mergeFrom(r3)
                L10:
                    return r2
                L11:
                    r3 = move-exception
                    goto L21
                L13:
                    r3 = move-exception
                    com.google.protobuf.m0 r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> L11
                    wallet.core.jni.proto.Theta$SigningOutput r4 = (wallet.core.jni.proto.Theta.SigningOutput) r4     // Catch: java.lang.Throwable -> L11
                    java.io.IOException r3 = r3.unwrapIOException()     // Catch: java.lang.Throwable -> L1f
                    throw r3     // Catch: java.lang.Throwable -> L1f
                L1f:
                    r3 = move-exception
                    r0 = r4
                L21:
                    if (r0 == 0) goto L26
                    r2.mergeFrom(r0)
                L26:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: wallet.core.jni.proto.Theta.SigningOutput.Builder.mergeFrom(com.google.protobuf.j, com.google.protobuf.r):wallet.core.jni.proto.Theta$SigningOutput$Builder");
            }
        }

        public static Builder newBuilder(SigningOutput signingOutput) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(signingOutput);
        }

        public static SigningOutput parseFrom(ByteBuffer byteBuffer, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteBuffer, rVar);
        }

        private SigningOutput(GeneratedMessageV3.b<?> bVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
        }

        public static SigningOutput parseDelimitedFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, defpackage.d82, com.google.protobuf.o0
        public SigningOutput getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
        }

        public static SigningOutput parseFrom(ByteString byteString, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(byteString, rVar);
        }

        @Override // com.google.protobuf.GeneratedMessageV3, com.google.protobuf.m0, com.google.protobuf.l0
        public Builder newBuilderForType() {
            return newBuilder();
        }

        private SigningOutput() {
            this.memoizedIsInitialized = (byte) -1;
            ByteString byteString = ByteString.EMPTY;
            this.encoded_ = byteString;
            this.signature_ = byteString;
        }

        public static SigningOutput parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr);
        }

        @Override // com.google.protobuf.GeneratedMessageV3
        public Builder newBuilderForType(GeneratedMessageV3.c cVar) {
            return new Builder(cVar);
        }

        public static SigningOutput parseFrom(byte[] bArr, r rVar) throws InvalidProtocolBufferException {
            return PARSER.parseFrom(bArr, rVar);
        }

        public static SigningOutput parseFrom(InputStream inputStream) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        private SigningOutput(j jVar, r rVar) throws InvalidProtocolBufferException {
            this();
            Objects.requireNonNull(rVar);
            g1.b g = g1.g();
            boolean z = false;
            while (!z) {
                try {
                    try {
                        try {
                            int J = jVar.J();
                            if (J != 0) {
                                if (J == 10) {
                                    this.encoded_ = jVar.q();
                                } else if (J != 18) {
                                    if (!parseUnknownField(jVar, g, rVar, J)) {
                                    }
                                } else {
                                    this.signature_ = jVar.q();
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        }
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    }
                } finally {
                    this.unknownFields = g.build();
                    makeExtensionsImmutable();
                }
            }
        }

        public static SigningOutput parseFrom(InputStream inputStream, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, rVar);
        }

        public static SigningOutput parseFrom(j jVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar);
        }

        public static SigningOutput parseFrom(j jVar, r rVar) throws IOException {
            return (SigningOutput) GeneratedMessageV3.parseWithIOException(PARSER, jVar, rVar);
        }
    }

    /* loaded from: classes3.dex */
    public interface SigningOutputOrBuilder extends o0 {
        /* synthetic */ List<String> findInitializationErrors();

        @Override // com.google.protobuf.o0
        /* synthetic */ Map<Descriptors.FieldDescriptor, Object> getAllFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ l0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ m0 getDefaultInstanceForType();

        @Override // com.google.protobuf.o0
        /* synthetic */ Descriptors.b getDescriptorForType();

        ByteString getEncoded();

        @Override // com.google.protobuf.o0
        /* synthetic */ Object getField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ String getInitializationErrorString();

        /* synthetic */ Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.g gVar);

        /* synthetic */ Object getRepeatedField(Descriptors.FieldDescriptor fieldDescriptor, int i);

        /* synthetic */ int getRepeatedFieldCount(Descriptors.FieldDescriptor fieldDescriptor);

        ByteString getSignature();

        @Override // com.google.protobuf.o0
        /* synthetic */ g1 getUnknownFields();

        @Override // com.google.protobuf.o0
        /* synthetic */ boolean hasField(Descriptors.FieldDescriptor fieldDescriptor);

        /* synthetic */ boolean hasOneof(Descriptors.g gVar);

        @Override // defpackage.d82
        /* synthetic */ boolean isInitialized();
    }

    static {
        Descriptors.b bVar = getDescriptor().o().get(0);
        internal_static_TW_Theta_Proto_SigningInput_descriptor = bVar;
        internal_static_TW_Theta_Proto_SigningInput_fieldAccessorTable = new GeneratedMessageV3.e(bVar, new String[]{"ChainId", "ToAddress", "ThetaAmount", "TfuelAmount", "Sequence", "Fee", "PrivateKey"});
        Descriptors.b bVar2 = getDescriptor().o().get(1);
        internal_static_TW_Theta_Proto_SigningOutput_descriptor = bVar2;
        internal_static_TW_Theta_Proto_SigningOutput_fieldAccessorTable = new GeneratedMessageV3.e(bVar2, new String[]{"Encoded", "Signature"});
    }

    private Theta() {
    }

    public static Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    public static void registerAllExtensions(q qVar) {
        registerAllExtensions((r) qVar);
    }

    public static void registerAllExtensions(r rVar) {
    }
}
