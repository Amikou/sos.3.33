package wallet.core.jni;

/* loaded from: classes3.dex */
public enum Purpose {
    BIP44(44),
    BIP49(49),
    BIP84(84),
    BIP1852(1852);
    
    private final int value;

    Purpose(int i) {
        this.value = i;
    }

    public static Purpose createFromValue(int i) {
        if (i != 44) {
            if (i != 49) {
                if (i != 84) {
                    if (i != 1852) {
                        return null;
                    }
                    return BIP1852;
                }
                return BIP84;
            }
            return BIP49;
        }
        return BIP44;
    }

    public int value() {
        return this.value;
    }
}
