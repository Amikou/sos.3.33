package wallet.core.jni;

/* loaded from: classes3.dex */
public enum AESPaddingMode {
    ZERO(0),
    PKCS7(1);
    
    private final int value;

    AESPaddingMode(int i) {
        this.value = i;
    }

    public static AESPaddingMode createFromValue(int i) {
        if (i != 0) {
            if (i != 1) {
                return null;
            }
            return PKCS7;
        }
        return ZERO;
    }

    public int value() {
        return this.value;
    }
}
