package wallet.core.jni;

import java.security.InvalidParameterException;

/* loaded from: classes3.dex */
public class HDWallet {
    private long nativeHandle;

    private HDWallet() {
        this.nativeHandle = 0L;
    }

    public static HDWallet createFromNative(long j) {
        HDWallet hDWallet = new HDWallet();
        hDWallet.nativeHandle = j;
        HDWalletPhantomReference.register(hDWallet, j);
        return hDWallet;
    }

    public static native PublicKey getPublicKeyFromExtended(String str, CoinType coinType, String str2);

    public static native boolean isValid(String str);

    public static native long nativeCreate(int i, String str);

    public static native long nativeCreateWithData(byte[] bArr, String str);

    public static native long nativeCreateWithMnemonic(String str, String str2);

    public static native void nativeDelete(long j);

    public native String getAddressForCoin(CoinType coinType);

    public native String getExtendedPrivateKey(Purpose purpose, CoinType coinType, HDVersion hDVersion);

    public native String getExtendedPublicKey(Purpose purpose, CoinType coinType, HDVersion hDVersion);

    public native PrivateKey getKey(CoinType coinType, String str);

    public native PrivateKey getKeyBIP44(CoinType coinType, int i, int i2, int i3);

    public native PrivateKey getKeyForCoin(CoinType coinType);

    public native PrivateKey getMasterKey(Curve curve);

    public native String mnemonic();

    public native byte[] seed();

    public HDWallet(int i, String str) {
        long nativeCreate = nativeCreate(i, str);
        this.nativeHandle = nativeCreate;
        if (nativeCreate != 0) {
            HDWalletPhantomReference.register(this, nativeCreate);
            return;
        }
        throw new InvalidParameterException();
    }

    public HDWallet(String str, String str2) {
        long nativeCreateWithMnemonic = nativeCreateWithMnemonic(str, str2);
        this.nativeHandle = nativeCreateWithMnemonic;
        if (nativeCreateWithMnemonic != 0) {
            HDWalletPhantomReference.register(this, nativeCreateWithMnemonic);
            return;
        }
        throw new InvalidParameterException();
    }

    public HDWallet(byte[] bArr, String str) {
        long nativeCreateWithData = nativeCreateWithData(bArr, str);
        this.nativeHandle = nativeCreateWithData;
        if (nativeCreateWithData != 0) {
            HDWalletPhantomReference.register(this, nativeCreateWithData);
            return;
        }
        throw new InvalidParameterException();
    }
}
