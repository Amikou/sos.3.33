package wallet.core.jni;

/* loaded from: classes3.dex */
public enum EthereumChainID {
    ETHEREUM(1),
    GO(60),
    POA(99),
    CALLISTO(820),
    ETHEREUMCLASSIC(61),
    VECHAIN(74),
    THUNDERTOKEN(108),
    TOMOCHAIN(88),
    BINANCESMARTCHAIN(56),
    MATIC(137);
    
    private final int value;

    EthereumChainID(int i) {
        this.value = i;
    }

    public static EthereumChainID createFromValue(int i) {
        if (i != 1) {
            if (i != 56) {
                if (i != 74) {
                    if (i != 88) {
                        if (i != 99) {
                            if (i != 108) {
                                if (i != 137) {
                                    if (i != 820) {
                                        if (i != 60) {
                                            if (i != 61) {
                                                return null;
                                            }
                                            return ETHEREUMCLASSIC;
                                        }
                                        return GO;
                                    }
                                    return CALLISTO;
                                }
                                return MATIC;
                            }
                            return THUNDERTOKEN;
                        }
                        return POA;
                    }
                    return TOMOCHAIN;
                }
                return VECHAIN;
            }
            return BINANCESMARTCHAIN;
        }
        return ETHEREUM;
    }

    public int value() {
        return this.value;
    }
}
