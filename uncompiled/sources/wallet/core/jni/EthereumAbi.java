package wallet.core.jni;

/* loaded from: classes3.dex */
public class EthereumAbi {
    private long nativeHandle = 0;

    private EthereumAbi() {
    }

    public static EthereumAbi createFromNative(long j) {
        EthereumAbi ethereumAbi = new EthereumAbi();
        ethereumAbi.nativeHandle = j;
        return ethereumAbi;
    }

    public static native String decodeCall(byte[] bArr, String str);

    public static native boolean decodeOutput(EthereumAbiFunction ethereumAbiFunction, byte[] bArr);

    public static native byte[] encode(EthereumAbiFunction ethereumAbiFunction);
}
