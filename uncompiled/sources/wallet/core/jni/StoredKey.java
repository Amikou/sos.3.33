package wallet.core.jni;

import java.security.InvalidParameterException;

/* loaded from: classes3.dex */
public class StoredKey {
    private long nativeHandle;

    private StoredKey() {
        this.nativeHandle = 0L;
    }

    public static StoredKey createFromNative(long j) {
        StoredKey storedKey = new StoredKey();
        storedKey.nativeHandle = j;
        StoredKeyPhantomReference.register(storedKey, j);
        return storedKey;
    }

    public static native StoredKey importHDWallet(String str, String str2, byte[] bArr, CoinType coinType);

    public static native StoredKey importJSON(byte[] bArr);

    public static native StoredKey importPrivateKey(byte[] bArr, String str, byte[] bArr2, CoinType coinType);

    public static native StoredKey load(String str);

    public static native long nativeCreate(String str, byte[] bArr);

    public static native void nativeDelete(long j);

    public native Account account(int i);

    public native int accountCount();

    public native Account accountForCoin(CoinType coinType, HDWallet hDWallet);

    public native void addAccount(String str, CoinType coinType, String str2, String str3);

    public native String decryptMnemonic(byte[] bArr);

    public native byte[] decryptPrivateKey(byte[] bArr);

    public native byte[] exportJSON();

    public native boolean fixAddresses(byte[] bArr);

    public native String identifier();

    public native boolean isMnemonic();

    public native String name();

    public native PrivateKey privateKey(CoinType coinType, byte[] bArr);

    public native void removeAccountForCoin(CoinType coinType);

    public native boolean store(String str);

    public native HDWallet wallet(byte[] bArr);

    public StoredKey(String str, byte[] bArr) {
        long nativeCreate = nativeCreate(str, bArr);
        this.nativeHandle = nativeCreate;
        if (nativeCreate != 0) {
            StoredKeyPhantomReference.register(this, nativeCreate);
            return;
        }
        throw new InvalidParameterException();
    }
}
