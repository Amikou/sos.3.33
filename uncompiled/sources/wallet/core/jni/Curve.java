package wallet.core.jni;

/* loaded from: classes3.dex */
public enum Curve {
    SECP256K1(0),
    ED25519(1),
    ED25519HD(2),
    ED25519BLAKE2BNANO(3),
    CURVE25519(4),
    NIST256P1(5),
    ED25519EXTENDED(6);
    
    private final int value;

    /* renamed from: wallet.core.jni.Curve$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$Curve;

        static {
            int[] iArr = new int[Curve.values().length];
            $SwitchMap$wallet$core$jni$Curve = iArr;
            try {
                iArr[Curve.SECP256K1.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$Curve[Curve.ED25519.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$Curve[Curve.ED25519HD.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$Curve[Curve.ED25519BLAKE2BNANO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$Curve[Curve.CURVE25519.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$Curve[Curve.NIST256P1.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$Curve[Curve.ED25519EXTENDED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    Curve(int i) {
        this.value = i;
    }

    public static Curve createFromValue(int i) {
        switch (i) {
            case 0:
                return SECP256K1;
            case 1:
                return ED25519;
            case 2:
                return ED25519HD;
            case 3:
                return ED25519BLAKE2BNANO;
            case 4:
                return CURVE25519;
            case 5:
                return NIST256P1;
            case 6:
                return ED25519EXTENDED;
            default:
                return null;
        }
    }

    @Override // java.lang.Enum
    public String toString() {
        switch (AnonymousClass1.$SwitchMap$wallet$core$jni$Curve[ordinal()]) {
            case 1:
                return "secp256k1";
            case 2:
                return "ed25519";
            case 3:
                return "ed25519-hd";
            case 4:
                return "ed25519-blake2b-nano";
            case 5:
                return "curve25519";
            case 6:
                return "nist256p1";
            case 7:
                return "ed25519-cardano-seed";
            default:
                return "";
        }
    }

    public int value() {
        return this.value;
    }
}
