package wallet.core.jni;

/* loaded from: classes3.dex */
public enum StellarMemoType {
    NONE(0),
    TEXT(1),
    ID(2),
    HASH(3),
    RETURN(4);
    
    private final int value;

    StellarMemoType(int i) {
        this.value = i;
    }

    public static StellarMemoType createFromValue(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            return null;
                        }
                        return RETURN;
                    }
                    return HASH;
                }
                return ID;
            }
            return TEXT;
        }
        return NONE;
    }

    public int value() {
        return this.value;
    }
}
