package wallet.core.jni;

import java.security.InvalidParameterException;

/* loaded from: classes3.dex */
public class AnyAddress {
    private long nativeHandle;

    private AnyAddress() {
        this.nativeHandle = 0L;
    }

    public static AnyAddress createFromNative(long j) {
        AnyAddress anyAddress = new AnyAddress();
        anyAddress.nativeHandle = j;
        AnyAddressPhantomReference.register(anyAddress, j);
        return anyAddress;
    }

    public static native boolean equals(AnyAddress anyAddress, AnyAddress anyAddress2);

    public static native boolean isValid(String str, CoinType coinType);

    public static native long nativeCreateWithPublicKey(PublicKey publicKey, CoinType coinType);

    public static native long nativeCreateWithString(String str, CoinType coinType);

    public static native void nativeDelete(long j);

    public native CoinType coin();

    public native byte[] data();

    public native String description();

    public AnyAddress(String str, CoinType coinType) {
        long nativeCreateWithString = nativeCreateWithString(str, coinType);
        this.nativeHandle = nativeCreateWithString;
        if (nativeCreateWithString != 0) {
            AnyAddressPhantomReference.register(this, nativeCreateWithString);
            return;
        }
        throw new InvalidParameterException();
    }

    public AnyAddress(PublicKey publicKey, CoinType coinType) {
        long nativeCreateWithPublicKey = nativeCreateWithPublicKey(publicKey, coinType);
        this.nativeHandle = nativeCreateWithPublicKey;
        if (nativeCreateWithPublicKey != 0) {
            AnyAddressPhantomReference.register(this, nativeCreateWithPublicKey);
            return;
        }
        throw new InvalidParameterException();
    }
}
