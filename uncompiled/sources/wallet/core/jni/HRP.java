package wallet.core.jni;

import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes3.dex */
public enum HRP {
    UNKNOWN(0),
    BITCOIN(1),
    LITECOIN(2),
    VIACOIN(3),
    GROESTLCOIN(4),
    DIGIBYTE(5),
    MONACOIN(6),
    COSMOS(7),
    BITCOINCASH(8),
    BITCOINGOLD(9),
    IOTEX(10),
    ZILLIQA(11),
    TERRA(12),
    KAVA(13),
    OASIS(14),
    BANDCHAIN(15),
    ELROND(16),
    BINANCE(17),
    HARMONY(18),
    CARDANO(19),
    QTUM(20);
    
    private final int value;

    /* renamed from: wallet.core.jni.HRP$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$HRP;

        static {
            int[] iArr = new int[HRP.values().length];
            $SwitchMap$wallet$core$jni$HRP = iArr;
            try {
                iArr[HRP.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.BITCOIN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.LITECOIN.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.VIACOIN.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.GROESTLCOIN.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.DIGIBYTE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.MONACOIN.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.COSMOS.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.BITCOINCASH.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.BITCOINGOLD.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.IOTEX.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.ZILLIQA.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.TERRA.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.KAVA.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.OASIS.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.BANDCHAIN.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.ELROND.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.BINANCE.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.HARMONY.ordinal()] = 19;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.CARDANO.ordinal()] = 20;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                $SwitchMap$wallet$core$jni$HRP[HRP.QTUM.ordinal()] = 21;
            } catch (NoSuchFieldError unused21) {
            }
        }
    }

    HRP(int i) {
        this.value = i;
    }

    public static HRP createFromValue(int i) {
        switch (i) {
            case 0:
                return UNKNOWN;
            case 1:
                return BITCOIN;
            case 2:
                return LITECOIN;
            case 3:
                return VIACOIN;
            case 4:
                return GROESTLCOIN;
            case 5:
                return DIGIBYTE;
            case 6:
                return MONACOIN;
            case 7:
                return COSMOS;
            case 8:
                return BITCOINCASH;
            case 9:
                return BITCOINGOLD;
            case 10:
                return IOTEX;
            case 11:
                return ZILLIQA;
            case 12:
                return TERRA;
            case 13:
                return KAVA;
            case 14:
                return OASIS;
            case 15:
                return BANDCHAIN;
            case 16:
                return ELROND;
            case 17:
                return BINANCE;
            case 18:
                return HARMONY;
            case 19:
                return CARDANO;
            case 20:
                return QTUM;
            default:
                return null;
        }
    }

    @Override // java.lang.Enum
    public String toString() {
        switch (AnonymousClass1.$SwitchMap$wallet$core$jni$HRP[ordinal()]) {
            case 2:
                return "bc";
            case 3:
                return "ltc";
            case 4:
                return "via";
            case 5:
                return "grs";
            case 6:
                return "dgb";
            case 7:
                return "mona";
            case 8:
                return "cosmos";
            case 9:
                return "bitcoincash";
            case 10:
                return "btg";
            case 11:
                return "io";
            case 12:
                return "zil";
            case 13:
                return "terra";
            case 14:
                return "kava";
            case 15:
                return "oasis";
            case 16:
                return "band";
            case 17:
                return "erd";
            case 18:
                return "bnb";
            case 19:
                return "one";
            case 20:
                return PublicResolver.FUNC_ADDR;
            case 21:
                return "qc";
            default:
                return "";
        }
    }

    public int value() {
        return this.value;
    }
}
