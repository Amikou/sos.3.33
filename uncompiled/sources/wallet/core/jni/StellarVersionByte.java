package wallet.core.jni;

/* loaded from: classes3.dex */
public enum StellarVersionByte {
    ACCOUNTID(48),
    SEED(192),
    PREAUTHTX(200),
    SHA256HASH(280);
    
    private final short value;

    StellarVersionByte(short s) {
        this.value = s;
    }

    public static StellarVersionByte createFromValue(short s) {
        if (s != 48) {
            if (s != 192) {
                if (s != 200) {
                    if (s != 280) {
                        return null;
                    }
                    return SHA256HASH;
                }
                return PREAUTHTX;
            }
            return SEED;
        }
        return ACCOUNTID;
    }

    public short value() {
        return this.value;
    }
}
