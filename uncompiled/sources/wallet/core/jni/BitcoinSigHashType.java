package wallet.core.jni;

/* loaded from: classes3.dex */
public enum BitcoinSigHashType {
    ALL(1),
    NONE(2),
    SINGLE(3),
    FORK(64),
    FORKBTG(20288);
    
    private final int value;

    BitcoinSigHashType(int i) {
        this.value = i;
    }

    public static BitcoinSigHashType createFromValue(int i) {
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 64) {
                        if (i != 20288) {
                            return null;
                        }
                        return FORKBTG;
                    }
                    return FORK;
                }
                return SINGLE;
            }
            return NONE;
        }
        return ALL;
    }

    public native boolean isNone();

    public native boolean isSingle();

    public int value() {
        return this.value;
    }
}
