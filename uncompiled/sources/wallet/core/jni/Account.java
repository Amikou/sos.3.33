package wallet.core.jni;

import java.security.InvalidParameterException;

/* loaded from: classes3.dex */
public class Account {
    private long nativeHandle;

    private Account() {
        this.nativeHandle = 0L;
    }

    public static Account createFromNative(long j) {
        Account account = new Account();
        account.nativeHandle = j;
        AccountPhantomReference.register(account, j);
        return account;
    }

    public static native long nativeCreate(String str, CoinType coinType, String str2, String str3);

    public static native void nativeDelete(long j);

    public native String address();

    public native CoinType coin();

    public native String derivationPath();

    public native String extendedPublicKey();

    public Account(String str, CoinType coinType, String str2, String str3) {
        long nativeCreate = nativeCreate(str, coinType, str2, str3);
        this.nativeHandle = nativeCreate;
        if (nativeCreate != 0) {
            AccountPhantomReference.register(this, nativeCreate);
            return;
        }
        throw new InvalidParameterException();
    }
}
