package wallet.core.jni;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.HashSet;
import java.util.Set;

/* compiled from: StoredKey.java */
/* loaded from: classes3.dex */
class StoredKeyPhantomReference extends PhantomReference<StoredKey> {
    private long nativeHandle;
    private static Set<StoredKeyPhantomReference> references = new HashSet();
    private static ReferenceQueue<StoredKey> queue = new ReferenceQueue<>();

    private StoredKeyPhantomReference(StoredKey storedKey, long j) {
        super(storedKey, queue);
        this.nativeHandle = j;
    }

    public static void doDeletes() {
        for (StoredKeyPhantomReference storedKeyPhantomReference = (StoredKeyPhantomReference) queue.poll(); storedKeyPhantomReference != null; storedKeyPhantomReference = (StoredKeyPhantomReference) queue.poll()) {
            StoredKey.nativeDelete(storedKeyPhantomReference.nativeHandle);
            references.remove(storedKeyPhantomReference);
        }
    }

    public static void register(StoredKey storedKey, long j) {
        references.add(new StoredKeyPhantomReference(storedKey, j));
    }
}
