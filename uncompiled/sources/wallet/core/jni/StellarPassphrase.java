package wallet.core.jni;

/* loaded from: classes3.dex */
public enum StellarPassphrase {
    STELLAR(0),
    KIN(1);
    
    private final int value;

    /* renamed from: wallet.core.jni.StellarPassphrase$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$wallet$core$jni$StellarPassphrase;

        static {
            int[] iArr = new int[StellarPassphrase.values().length];
            $SwitchMap$wallet$core$jni$StellarPassphrase = iArr;
            try {
                iArr[StellarPassphrase.STELLAR.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$wallet$core$jni$StellarPassphrase[StellarPassphrase.KIN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    StellarPassphrase(int i) {
        this.value = i;
    }

    public static StellarPassphrase createFromValue(int i) {
        if (i != 0) {
            if (i != 1) {
                return null;
            }
            return KIN;
        }
        return STELLAR;
    }

    @Override // java.lang.Enum
    public String toString() {
        int i = AnonymousClass1.$SwitchMap$wallet$core$jni$StellarPassphrase[ordinal()];
        return i != 1 ? i != 2 ? "" : "Kin Mainnet ; December 2018" : "Public Global Stellar Network ; September 2015";
    }

    public int value() {
        return this.value;
    }
}
