package com.jama.carouselview;

import android.content.Context;
import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.v;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes2.dex */
public class CarouselLinearLayoutManager extends LinearLayoutManager {
    public boolean M0;
    public boolean N0;
    public final v O0;

    public CarouselLinearLayoutManager(Context context, int i, boolean z, v vVar) {
        super(context, i, z);
        this.N0 = false;
        this.O0 = vVar;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        View T;
        int F1 = super.F1(i, tVar, xVar);
        if (this.N0) {
            View h = this.O0.h(this);
            for (int i2 = 0; i2 < U() && (T = T(i2)) != null && h != null; i2++) {
                float right = T.getRight() - T.getLeft();
                float left = T.getLeft() + (right / 2.0f);
                if (!this.M0) {
                    right = v0();
                }
                float f = right / 2.0f;
                float f2 = 0.75f * f;
                float min = (((-0.39999998f) * (Math.min(f2, Math.abs(f - left)) - Utils.FLOAT_EPSILON)) / (f2 - Utils.FLOAT_EPSILON)) + 1.0f;
                T.setScaleX(min);
                T.setScaleY(min);
                if (T.getX() == h.getX()) {
                    T.setAlpha(1.0f);
                } else {
                    T.setAlpha(min / 2.0f);
                }
            }
        }
        return F1;
    }

    public void U2(boolean z) {
        this.M0 = z;
    }

    public void V2(boolean z) {
        this.N0 = z;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void f1(RecyclerView.t tVar, RecyclerView.x xVar) {
        super.f1(tVar, xVar);
        F1(0, tVar, xVar);
    }
}
