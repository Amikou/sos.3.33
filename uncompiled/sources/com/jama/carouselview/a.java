package com.jama.carouselview;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.m;
import androidx.recyclerview.widget.n;
import androidx.recyclerview.widget.q;

/* compiled from: CarouselSnapHelper.java */
/* loaded from: classes2.dex */
public class a extends n {
    public Context f;
    public int g = 1000;
    public float h = 100.0f;
    public q i = null;
    public Scroller j = null;
    public int k = 0;

    /* compiled from: CarouselSnapHelper.java */
    /* renamed from: com.jama.carouselview.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0153a extends m {
        public final /* synthetic */ RecyclerView.LayoutManager a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0153a(Context context, RecyclerView.LayoutManager layoutManager) {
            super(context);
            this.a = layoutManager;
        }

        @Override // androidx.recyclerview.widget.m
        public float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return a.this.h / displayMetrics.densityDpi;
        }

        @Override // androidx.recyclerview.widget.m, androidx.recyclerview.widget.RecyclerView.w
        public void onTargetFound(View view, RecyclerView.x xVar, RecyclerView.w.a aVar) {
            int[] c = a.this.c(this.a, view);
            int i = c[0];
            aVar.d(i, c[1], Math.max(1, Math.min(a.this.g, calculateTimeForDeceleration(Math.abs(i)))), this.mDecelerateInterpolator);
        }
    }

    @Override // androidx.recyclerview.widget.v
    public void b(RecyclerView recyclerView) throws IllegalStateException {
        if (recyclerView != null) {
            this.f = recyclerView.getContext();
            this.j = new Scroller(this.f, new DecelerateInterpolator());
        } else {
            this.j = null;
            this.f = null;
        }
        super.b(recyclerView);
    }

    @Override // androidx.recyclerview.widget.n, androidx.recyclerview.widget.v
    public int[] c(RecyclerView.LayoutManager layoutManager, View view) {
        return new int[]{u(view, w(layoutManager))};
    }

    @Override // androidx.recyclerview.widget.v
    public int[] d(int i, int i2) {
        int[] iArr = new int[2];
        q qVar = this.i;
        if (qVar == null) {
            return iArr;
        }
        if (this.k == 0) {
            this.k = (qVar.i() - qVar.m()) / 2;
        }
        Scroller scroller = this.j;
        int i3 = this.k;
        scroller.fling(0, 0, i, i2, -i3, i3, 0, 0);
        iArr[0] = this.j.getFinalX();
        iArr[1] = this.j.getFinalY();
        return iArr;
    }

    @Override // androidx.recyclerview.widget.v
    public RecyclerView.w e(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof RecyclerView.w.b) {
            return super.e(layoutManager);
        }
        Context context = this.f;
        if (context == null) {
            return null;
        }
        return new C0153a(context, layoutManager);
    }

    @Override // androidx.recyclerview.widget.n, androidx.recyclerview.widget.v
    public View h(RecyclerView.LayoutManager layoutManager) {
        return v(layoutManager, w(layoutManager));
    }

    public final int u(View view, q qVar) {
        return qVar.g(view) - qVar.m();
    }

    public final View v(RecyclerView.LayoutManager layoutManager, q qVar) {
        int U;
        View view = null;
        if (layoutManager == null || (U = layoutManager.U()) == 0) {
            return null;
        }
        int i = Integer.MAX_VALUE;
        int m = qVar.m();
        for (int i2 = 0; i2 < U; i2++) {
            View T = layoutManager.T(i2);
            int abs = Math.abs(qVar.g(T) - m);
            if (abs < i) {
                view = T;
                i = abs;
            }
        }
        return view;
    }

    public final q w(RecyclerView.LayoutManager layoutManager) {
        if (this.i == null) {
            this.i = q.a(layoutManager);
        }
        return this.i;
    }
}
