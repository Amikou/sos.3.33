package com.jama.carouselview;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.n;
import androidx.recyclerview.widget.v;
import com.jama.carouselview.CarouselView;
import com.jama.carouselview.enums.IndicatorAnimationType;
import com.jama.carouselview.enums.OffsetType;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;

/* loaded from: classes2.dex */
public class CarouselView extends FrameLayout {
    public Context a;
    public PageIndicatorView f0;
    public RecyclerView g0;
    public CarouselLinearLayoutManager h0;
    public lw i0;
    public iw j0;
    public IndicatorAnimationType k0;
    public OffsetType l0;
    public v m0;
    public boolean n0;
    public boolean o0;
    public int p0;
    public Handler q0;
    public boolean r0;
    public int s0;
    public int t0;
    public int u0;
    public int v0;

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            if (CarouselView.this.getAutoPlay()) {
                if (CarouselView.this.getSize() - 1 == CarouselView.this.getCurrentItem()) {
                    CarouselView.this.setCurrentItem(0);
                } else {
                    CarouselView carouselView = CarouselView.this;
                    carouselView.setCurrentItem(carouselView.getCurrentItem() + 1);
                }
                CarouselView.this.q0.postDelayed(this, CarouselView.this.getAutoPlayDelay());
            }
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class b {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[IndicatorAnimationType.values().length];
            b = iArr;
            try {
                iArr[IndicatorAnimationType.DROP.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[IndicatorAnimationType.FILL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[IndicatorAnimationType.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[IndicatorAnimationType.SWAP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[IndicatorAnimationType.WORM.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[IndicatorAnimationType.COLOR.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                b[IndicatorAnimationType.SCALE.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                b[IndicatorAnimationType.SLIDE.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                b[IndicatorAnimationType.THIN_WORM.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                b[IndicatorAnimationType.SCALE_DOWN.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr2 = new int[OffsetType.values().length];
            a = iArr2;
            try {
                iArr2[OffsetType.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[OffsetType.START.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    public CarouselView(Context context) {
        super(context);
        this.a = context;
        j(null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void l() {
        int[] c;
        this.g0.B1();
        this.g0.stopNestedScroll();
        this.g0.o1(this.v0);
        View N = this.g0.getLayoutManager().N(this.v0);
        if (N == null || (c = this.m0.c(this.g0.getLayoutManager(), N)) == null) {
            return;
        }
        if (c[0] == 0 && c[1] == 0) {
            return;
        }
        this.g0.s1(c[0], c[1]);
    }

    public void d() {
        this.g0.post(new Runnable() { // from class: jw
            @Override // java.lang.Runnable
            public final void run() {
                CarouselView.this.l();
            }
        });
    }

    public final void e() {
        this.q0.postDelayed(new a(), getAutoPlayDelay());
    }

    public void f(boolean z) {
        this.n0 = z;
    }

    public final IndicatorAnimationType g(int i) {
        switch (i) {
            case 1:
                return IndicatorAnimationType.FILL;
            case 2:
                return IndicatorAnimationType.DROP;
            case 3:
                return IndicatorAnimationType.SWAP;
            case 4:
                return IndicatorAnimationType.WORM;
            case 5:
                return IndicatorAnimationType.COLOR;
            case 6:
                return IndicatorAnimationType.SCALE;
            case 7:
                return IndicatorAnimationType.SLIDE;
            case 8:
                return IndicatorAnimationType.THIN_WORM;
            case 9:
                return IndicatorAnimationType.SCALE_DOWN;
            default:
                return IndicatorAnimationType.NONE;
        }
    }

    public boolean getAutoPlay() {
        return this.o0;
    }

    public int getAutoPlayDelay() {
        return this.p0;
    }

    public OffsetType getCarouselOffset() {
        return this.l0;
    }

    public iw getCarouselScrollListener() {
        return this.j0;
    }

    public lw getCarouselViewListener() {
        return this.i0;
    }

    public int getCurrentItem() {
        return this.v0;
    }

    public IndicatorAnimationType getIndicatorAnimationType() {
        return this.k0;
    }

    public int getIndicatorPadding() {
        return this.f0.getPadding();
    }

    public int getIndicatorRadius() {
        return this.f0.getRadius();
    }

    public int getIndicatorSelectedColor() {
        return this.f0.getSelectedColor();
    }

    public int getIndicatorUnselectedColor() {
        return this.f0.getUnselectedColor();
    }

    public int getResource() {
        return this.s0;
    }

    public boolean getScaleOnScroll() {
        return this.r0;
    }

    public int getSize() {
        return this.t0;
    }

    public int getSpacing() {
        return this.u0;
    }

    public final OffsetType h(int i) {
        if (i != 1) {
            return OffsetType.START;
        }
        return OffsetType.CENTER;
    }

    public void i(boolean z) {
        if (z) {
            this.f0.setVisibility(8);
        } else {
            this.f0.setVisibility(0);
        }
    }

    public final void j(AttributeSet attributeSet) {
        View inflate = LayoutInflater.from(this.a).inflate(y03.view_carousel, this);
        this.g0 = (RecyclerView) inflate.findViewById(c03.carouselRecyclerView);
        this.f0 = (PageIndicatorView) inflate.findViewById(c03.pageIndicatorView);
        this.q0 = new Handler();
        this.g0.setHasFixedSize(true);
        k(attributeSet);
    }

    public final void k(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = this.a.getTheme().obtainStyledAttributes(attributeSet, p23.CarouselView, 0, 0);
            f(obtainStyledAttributes.getBoolean(p23.CarouselView_enableSnapping, true));
            setScaleOnScroll(obtainStyledAttributes.getBoolean(p23.CarouselView_scaleOnScroll, false));
            setAutoPlay(obtainStyledAttributes.getBoolean(p23.CarouselView_setAutoPlay, false));
            setAutoPlayDelay(obtainStyledAttributes.getInteger(p23.CarouselView_setAutoPlayDelay, 2500));
            setCarouselOffset(h(obtainStyledAttributes.getInteger(p23.CarouselView_carouselOffset, 0)));
            int resourceId = obtainStyledAttributes.getResourceId(p23.CarouselView_resource, 0);
            if (resourceId != 0) {
                setResource(resourceId);
            }
            int color = obtainStyledAttributes.getColor(p23.CarouselView_indicatorSelectedColor, 0);
            int color2 = obtainStyledAttributes.getColor(p23.CarouselView_indicatorUnselectedColor, 0);
            if (color != 0) {
                setIndicatorSelectedColor(color);
            }
            if (color2 != 0) {
                setIndicatorUnselectedColor(color2);
            }
            setIndicatorAnimationType(g(obtainStyledAttributes.getInteger(p23.CarouselView_indicatorAnimationType, 0)));
            setIndicatorRadius(obtainStyledAttributes.getInteger(p23.CarouselView_indicatorRadius, 5));
            setIndicatorPadding(obtainStyledAttributes.getInteger(p23.CarouselView_indicatorPadding, 5));
            setSize(obtainStyledAttributes.getInteger(p23.CarouselView_size, 0));
            setSpacing(obtainStyledAttributes.getInteger(p23.CarouselView_spacing, 0));
            obtainStyledAttributes.recycle();
        }
    }

    public void m() throws NullPointerException {
        RecyclerView recyclerView = this.g0;
        if (recyclerView != null && recyclerView.getAdapter() != null) {
            this.g0.getAdapter().notifyDataSetChanged();
            return;
        }
        throw new NullPointerException("Recycler view is not initialized");
    }

    public final void n() {
        d();
    }

    public final void o() {
        CarouselLinearLayoutManager carouselLinearLayoutManager = new CarouselLinearLayoutManager(this.a, 0, false, this.m0);
        this.h0 = carouselLinearLayoutManager;
        carouselLinearLayoutManager.U2(getCarouselOffset() == OffsetType.START);
        if (getScaleOnScroll()) {
            this.h0.V2(true);
        }
        this.g0.setLayoutManager(this.h0);
        this.g0.setAdapter(new kw(getCarouselViewListener(), getResource(), getSize(), this.g0, getSpacing(), getCarouselOffset() == OffsetType.CENTER));
        if (this.n0) {
            this.g0.setOnFlingListener(null);
            this.m0.b(this.g0);
        }
        e();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setAutoPlay(false);
    }

    public void p() {
        q();
        o();
        n();
    }

    public final void q() {
    }

    public void setAutoPlay(boolean z) {
        this.o0 = z;
    }

    public void setAutoPlayDelay(int i) {
        this.p0 = i;
    }

    public void setCarouselOffset(OffsetType offsetType) {
        this.l0 = offsetType;
        int i = b.a[offsetType.ordinal()];
        if (i == 1) {
            this.m0 = new n();
        } else if (i != 2) {
        } else {
            this.m0 = new com.jama.carouselview.a();
        }
    }

    public void setCarouselScrollListener(iw iwVar) {
        this.j0 = iwVar;
    }

    public void setCarouselViewListener(lw lwVar) {
        this.i0 = lwVar;
    }

    public void setCurrentItem(int i) {
        if (i < 0) {
            this.v0 = 0;
        } else if (i >= getSize()) {
            this.v0 = getSize() - 1;
        } else {
            this.v0 = i;
        }
    }

    public void setIndicatorAnimationType(IndicatorAnimationType indicatorAnimationType) {
        this.k0 = indicatorAnimationType;
        switch (b.b[indicatorAnimationType.ordinal()]) {
            case 1:
                this.f0.setAnimationType(AnimationType.DROP);
                return;
            case 2:
                this.f0.setAnimationType(AnimationType.FILL);
                return;
            case 3:
                this.f0.setAnimationType(AnimationType.NONE);
                return;
            case 4:
                this.f0.setAnimationType(AnimationType.SWAP);
                return;
            case 5:
                this.f0.setAnimationType(AnimationType.WORM);
                return;
            case 6:
                this.f0.setAnimationType(AnimationType.COLOR);
                return;
            case 7:
                this.f0.setAnimationType(AnimationType.SCALE);
                return;
            case 8:
                this.f0.setAnimationType(AnimationType.SLIDE);
                return;
            case 9:
                this.f0.setAnimationType(AnimationType.THIN_WORM);
                return;
            case 10:
                this.f0.setAnimationType(AnimationType.SCALE_DOWN);
                return;
            default:
                return;
        }
    }

    public void setIndicatorPadding(int i) {
        this.f0.setPadding(i);
    }

    public void setIndicatorRadius(int i) {
        this.f0.setRadius(i);
    }

    public void setIndicatorSelectedColor(int i) {
        this.f0.setSelectedColor(i);
    }

    public void setIndicatorUnselectedColor(int i) {
        this.f0.setUnselectedColor(i);
    }

    public void setResource(int i) {
        this.s0 = i;
    }

    public void setScaleOnScroll(boolean z) {
        this.r0 = z;
    }

    public void setSize(int i) {
        this.t0 = i;
        this.f0.setCount(i);
    }

    public void setSpacing(int i) {
        this.u0 = i;
    }

    public CarouselView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
        j(attributeSet);
    }
}
