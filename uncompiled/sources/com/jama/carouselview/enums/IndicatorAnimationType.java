package com.jama.carouselview.enums;

/* loaded from: classes2.dex */
public enum IndicatorAnimationType {
    NONE,
    COLOR,
    SCALE,
    WORM,
    SLIDE,
    FILL,
    THIN_WORM,
    DROP,
    SWAP,
    SCALE_DOWN
}
