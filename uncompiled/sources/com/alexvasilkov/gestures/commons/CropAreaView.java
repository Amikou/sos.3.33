package com.alexvasilkov.gestures.commons;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import com.alexvasilkov.gestures.Settings;
import com.alexvasilkov.gestures.views.GestureImageView;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class CropAreaView extends View {
    public static final int x0 = Color.argb(160, 0, 0, 0);
    public static final Rect y0 = new Rect();
    public static final RectF z0 = new RectF();
    public final RectF a;
    public float f0;
    public final RectF g0;
    public final RectF h0;
    public final RectF i0;
    public float j0;
    public float k0;
    public final Paint l0;
    public final Paint m0;
    public final e71 n0;
    public final he o0;
    public int p0;
    public int q0;
    public float r0;
    public int s0;
    public int t0;
    public float u0;
    public float v0;
    public GestureImageView w0;

    /* loaded from: classes.dex */
    public class a extends he {
        public a() {
            super(CropAreaView.this);
        }

        @Override // defpackage.he
        public boolean a() {
            if (CropAreaView.this.n0.e()) {
                return false;
            }
            CropAreaView.this.n0.a();
            float c = CropAreaView.this.n0.c();
            v42.d(CropAreaView.this.a, CropAreaView.this.h0, CropAreaView.this.i0, c);
            float b = v42.b(CropAreaView.this.j0, CropAreaView.this.k0, c);
            CropAreaView cropAreaView = CropAreaView.this;
            cropAreaView.l(cropAreaView.a, b);
            return true;
        }
    }

    public CropAreaView(Context context) {
        this(context, null);
    }

    public final void h(Canvas canvas) {
        this.l0.setStyle(Paint.Style.STROKE);
        this.l0.setColor(this.q0);
        Paint paint = this.l0;
        float f = this.u0;
        if (f == Utils.FLOAT_EPSILON) {
            f = this.r0 * 0.5f;
        }
        paint.setStrokeWidth(f);
        float width = this.f0 * 0.5f * this.a.width();
        float height = this.f0 * 0.5f * this.a.height();
        int i = 0;
        int i2 = 0;
        while (i2 < this.t0) {
            RectF rectF = this.a;
            i2++;
            float width2 = rectF.left + (i2 * (rectF.width() / (this.t0 + 1)));
            RectF rectF2 = this.a;
            float k = k(width2, width, height, rectF2.left, rectF2.right);
            RectF rectF3 = this.a;
            canvas.drawLine(width2, rectF3.top + k, width2, rectF3.bottom - k, this.l0);
        }
        while (i < this.s0) {
            RectF rectF4 = this.a;
            i++;
            float height2 = rectF4.top + (i * (rectF4.height() / (this.s0 + 1)));
            RectF rectF5 = this.a;
            float k2 = k(height2, height, width, rectF5.top, rectF5.bottom);
            RectF rectF6 = this.a;
            canvas.drawLine(rectF6.left + k2, height2, rectF6.right - k2, height2, this.l0);
        }
        this.l0.setStyle(Paint.Style.STROKE);
        this.l0.setColor(this.q0);
        this.l0.setStrokeWidth(this.r0);
        canvas.drawRoundRect(this.g0, width, height, this.l0);
    }

    public final void i(Canvas canvas) {
        this.l0.setStyle(Paint.Style.FILL);
        this.l0.setColor(this.p0);
        RectF rectF = z0;
        rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, canvas.getWidth(), this.a.top);
        canvas.drawRect(rectF, this.l0);
        rectF.set(Utils.FLOAT_EPSILON, this.a.bottom, canvas.getWidth(), canvas.getHeight());
        canvas.drawRect(rectF, this.l0);
        RectF rectF2 = this.a;
        rectF.set(Utils.FLOAT_EPSILON, rectF2.top, rectF2.left, rectF2.bottom);
        canvas.drawRect(rectF, this.l0);
        RectF rectF3 = this.a;
        rectF.set(rectF3.right, rectF3.top, canvas.getWidth(), this.a.bottom);
        canvas.drawRect(rectF, this.l0);
    }

    public final void j(Canvas canvas) {
        this.l0.setStyle(Paint.Style.FILL);
        this.l0.setColor(this.p0);
        if (Build.VERSION.SDK_INT >= 21) {
            canvas.saveLayer(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, canvas.getWidth(), canvas.getHeight(), null);
        } else {
            canvas.saveLayer(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, canvas.getWidth(), canvas.getHeight(), null, 0);
        }
        canvas.drawPaint(this.l0);
        canvas.drawRoundRect(this.a, this.f0 * 0.5f * this.a.width(), this.f0 * 0.5f * this.a.height(), this.m0);
        canvas.restore();
    }

    public final float k(float f, float f2, float f3, float f4, float f5) {
        float f6 = f - f4 < f2 ? (f4 + f2) - f : f5 - f < f2 ? (f - f5) + f2 : 0.0f;
        return f2 == Utils.FLOAT_EPSILON ? Utils.FLOAT_EPSILON : f3 * (1.0f - ((float) Math.sqrt(1.0f - (((f6 * f6) / f2) / f2))));
    }

    public final void l(RectF rectF, float f) {
        this.a.set(rectF);
        this.f0 = f;
        this.g0.set(rectF);
        float f2 = -(this.r0 * 0.5f);
        this.g0.inset(f2, f2);
        invalidate();
    }

    public void m(boolean z) {
        GestureImageView gestureImageView = this.w0;
        Settings n = gestureImageView == null ? null : gestureImageView.getController().n();
        if (n == null || getWidth() <= 0 || getHeight() <= 0) {
            return;
        }
        float f = this.v0;
        if (f > Utils.FLOAT_EPSILON || f == -1.0f) {
            int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
            int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
            float f2 = this.v0;
            if (f2 == -1.0f) {
                f2 = n.l() / n.k();
            }
            float f3 = width;
            float f4 = height;
            if (f2 > f3 / f4) {
                n.N(width, (int) (f3 / f2));
            } else {
                n.N((int) (f4 * f2), height);
            }
            if (z) {
                this.w0.getController().k();
            } else {
                this.w0.getController().V();
            }
        }
        this.h0.set(this.a);
        Rect rect = y0;
        si1.d(n, rect);
        this.i0.set(rect);
        this.n0.b();
        if (z) {
            this.n0.f(n.e());
            this.n0.g(Utils.FLOAT_EPSILON, 1.0f);
            this.o0.c();
            return;
        }
        l(this.i0, this.k0);
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (this.f0 != Utils.FLOAT_EPSILON && !isInEditMode()) {
            j(canvas);
        } else {
            i(canvas);
        }
        h(canvas);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        m(false);
        GestureImageView gestureImageView = this.w0;
        if (gestureImageView != null) {
            gestureImageView.getController().Q();
        }
        if (isInEditMode()) {
            float paddingLeft = (i - getPaddingLeft()) - getPaddingRight();
            float paddingTop = (i2 - getPaddingTop()) - getPaddingBottom();
            float f = this.v0;
            if (f <= Utils.FLOAT_EPSILON) {
                paddingLeft = i;
                paddingTop = i2;
            } else if (f > paddingLeft / paddingTop) {
                paddingTop = paddingLeft / f;
            } else {
                paddingLeft = paddingTop * f;
            }
            float f2 = i;
            float f3 = i2;
            this.a.set((f2 - paddingLeft) * 0.5f, (f3 - paddingTop) * 0.5f, (f2 + paddingLeft) * 0.5f, (f3 + paddingTop) * 0.5f);
            this.g0.set(this.a);
        }
    }

    public void setAspect(float f) {
        this.v0 = f;
    }

    public void setBackColor(int i) {
        this.p0 = i;
        invalidate();
    }

    public void setBorderColor(int i) {
        this.q0 = i;
        invalidate();
    }

    public void setBorderWidth(float f) {
        this.r0 = f;
        invalidate();
    }

    public void setImageView(GestureImageView gestureImageView) {
        this.w0 = gestureImageView;
        gestureImageView.getController().n().K(Settings.Fit.OUTSIDE).J(true).L(false);
        m(false);
    }

    public void setRounded(boolean z) {
        this.j0 = this.f0;
        this.k0 = z ? 1.0f : Utils.FLOAT_EPSILON;
    }

    public void setRulesCount(int i, int i2) {
        this.s0 = i;
        this.t0 = i2;
        invalidate();
    }

    public void setRulesWidth(float f) {
        this.u0 = f;
        invalidate();
    }

    public CropAreaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new RectF();
        this.g0 = new RectF();
        this.h0 = new RectF();
        this.i0 = new RectF();
        Paint paint = new Paint();
        this.l0 = paint;
        Paint paint2 = new Paint();
        this.m0 = paint2;
        this.n0 = new e71();
        this.o0 = new a();
        paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        paint2.setAntiAlias(true);
        paint.setAntiAlias(true);
        float b = ye4.b(getContext(), 1, 2.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h23.CropAreaView);
        this.p0 = obtainStyledAttributes.getColor(h23.CropAreaView_gest_backgroundColor, x0);
        this.q0 = obtainStyledAttributes.getColor(h23.CropAreaView_gest_borderColor, -1);
        this.r0 = obtainStyledAttributes.getDimension(h23.CropAreaView_gest_borderWidth, b);
        this.s0 = obtainStyledAttributes.getInt(h23.CropAreaView_gest_rulesHorizontal, 0);
        this.t0 = obtainStyledAttributes.getInt(h23.CropAreaView_gest_rulesVertical, 0);
        int i = h23.CropAreaView_gest_rulesWidth;
        float f = Utils.FLOAT_EPSILON;
        this.u0 = obtainStyledAttributes.getDimension(i, Utils.FLOAT_EPSILON);
        boolean z = obtainStyledAttributes.getBoolean(h23.CropAreaView_gest_rounded, false);
        this.v0 = obtainStyledAttributes.getFloat(h23.CropAreaView_gest_aspect, Utils.FLOAT_EPSILON);
        obtainStyledAttributes.recycle();
        f = z ? 1.0f : f;
        this.k0 = f;
        this.f0 = f;
    }
}
