package com.alexvasilkov.gestures.commons.circle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.alexvasilkov.gestures.views.GestureImageView;
import com.github.mikephil.charting.utils.Utils;
import defpackage.tj4;

/* loaded from: classes.dex */
public class CircleGestureImageView extends GestureImageView {
    public static final Matrix o0 = new Matrix();
    public final Paint j0;
    public final RectF k0;
    public float l0;
    public boolean m0;
    public float n0;

    /* loaded from: classes.dex */
    public class a implements tj4.e {
        public a() {
        }

        @Override // defpackage.tj4.e
        public void a(float f, boolean z) {
            float v = f / CircleGestureImageView.this.getPositionAnimator().v();
            CircleGestureImageView.this.n0 = v42.f(v, Utils.FLOAT_EPSILON, 1.0f);
        }
    }

    public CircleGestureImageView(Context context) {
        this(context, null, 0);
    }

    @Override // com.alexvasilkov.gestures.views.GestureImageView, defpackage.oz
    public void a(RectF rectF, float f) {
        if (rectF == null) {
            this.k0.setEmpty();
        } else {
            this.k0.set(rectF);
        }
        this.l0 = f;
        i();
        super.a(rectF, f);
    }

    @Override // com.alexvasilkov.gestures.views.GestureImageView, android.view.View
    public void draw(Canvas canvas) {
        if (this.n0 != 1.0f && !this.k0.isEmpty() && this.j0.getShader() != null) {
            float width = this.k0.width() * 0.5f * (1.0f - this.n0);
            float height = this.k0.height() * 0.5f * (1.0f - this.n0);
            canvas.rotate(this.l0, this.k0.centerX(), this.k0.centerY());
            canvas.drawRoundRect(this.k0, width, height, this.j0);
            canvas.rotate(-this.l0, this.k0.centerX(), this.k0.centerY());
            if (ef1.c()) {
                bf0.a(this, canvas);
                return;
            }
            return;
        }
        super.draw(canvas);
    }

    public Bitmap g(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        throw new RuntimeException("For better performance only BitmapDrawables are supported, but you can override getBitmapFromDrawable() and build bitmap on your own");
    }

    public final void h() {
        Bitmap g = this.m0 ? g(getDrawable()) : null;
        if (g != null) {
            Paint paint = this.j0;
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            paint.setShader(new BitmapShader(g, tileMode, tileMode));
            i();
        } else {
            this.j0.setShader(null);
        }
        invalidate();
    }

    public final void i() {
        if (this.k0.isEmpty() || this.j0.getShader() == null) {
            return;
        }
        us3 o = getController().o();
        Matrix matrix = o0;
        o.d(matrix);
        matrix.postTranslate(getPaddingLeft(), getPaddingTop());
        matrix.postRotate(-this.l0, this.k0.centerX(), this.k0.centerY());
        this.j0.getShader().setLocalMatrix(matrix);
    }

    public void setCircle(boolean z) {
        this.m0 = z;
        h();
    }

    @Override // com.alexvasilkov.gestures.views.GestureImageView, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        h();
    }

    @Override // android.widget.ImageView
    public void setImageMatrix(Matrix matrix) {
        super.setImageMatrix(matrix);
        i();
    }

    public CircleGestureImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CircleGestureImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.j0 = new Paint(3);
        this.k0 = new RectF();
        this.m0 = true;
        getPositionAnimator().m(new a());
    }
}
