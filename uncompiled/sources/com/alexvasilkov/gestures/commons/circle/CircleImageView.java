package com.alexvasilkov.gestures.commons.circle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

/* loaded from: classes.dex */
public class CircleImageView extends ImageView {
    public static final Matrix h0 = new Matrix();
    public Paint a;
    public RectF f0;
    public boolean g0;

    public CircleImageView(Context context) {
        this(context, null, 0);
    }

    public Bitmap a(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        throw new RuntimeException("For better performance only BitmapDrawables are supported, but you can override getBitmapFromDrawable() and build bitmap on your own");
    }

    public final void b() {
        if (this.a == null) {
            this.a = new Paint(3);
            this.f0 = new RectF();
            this.g0 = true;
        }
    }

    public final void c() {
        b();
        Bitmap a = this.g0 ? a(getDrawable()) : null;
        if (a != null) {
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            BitmapShader bitmapShader = new BitmapShader(a, tileMode, tileMode);
            Matrix matrix = h0;
            matrix.set(getImageMatrix());
            matrix.postTranslate(getPaddingLeft(), getPaddingTop());
            bitmapShader.setLocalMatrix(matrix);
            this.a.setShader(bitmapShader);
        } else {
            this.a.setShader(null);
        }
        invalidate();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        b();
        if (this.a.getShader() == null) {
            super.onDraw(canvas);
            return;
        }
        canvas.drawRoundRect(this.f0, this.f0.width() * 0.5f, this.f0.height() * 0.5f, this.a);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i);
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size, 1073741824));
    }

    public void setCircle(boolean z) {
        this.g0 = z;
        c();
    }

    @Override // android.widget.ImageView
    public boolean setFrame(int i, int i2, int i3, int i4) {
        boolean frame = super.setFrame(i, i2, i3, i4);
        this.f0.set(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
        c();
        return frame;
    }

    @Override // android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        c();
    }

    @Override // android.widget.ImageView
    public void setImageMatrix(Matrix matrix) {
        super.setImageMatrix(matrix);
        c();
    }

    @Override // android.widget.ImageView
    public void setImageResource(int i) {
        super.setImageResource(i);
        c();
    }

    public CircleImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CircleImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        b();
    }
}
