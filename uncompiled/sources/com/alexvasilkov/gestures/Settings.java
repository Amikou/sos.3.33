package com.alexvasilkov.gestures;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class Settings {
    public int a;
    public int b;
    public int c;
    public int d;
    public boolean e;
    public int f;
    public int g;
    public float l;
    public float m;
    public int y;
    public int z;
    public float h = Utils.FLOAT_EPSILON;
    public float i = 2.0f;
    public float j = -1.0f;
    public float k = 2.0f;
    public boolean n = false;
    public int o = 17;
    public Fit p = Fit.INSIDE;
    public Bounds q = Bounds.NORMAL;
    public boolean r = true;
    public boolean s = true;
    public boolean t = true;
    public boolean u = false;
    public boolean v = false;
    public boolean w = true;
    public ExitType x = ExitType.ALL;
    public long A = 200;

    /* loaded from: classes.dex */
    public enum Bounds {
        NORMAL,
        INSIDE,
        OUTSIDE,
        PIVOT,
        NONE
    }

    /* loaded from: classes.dex */
    public enum ExitType {
        ALL,
        SCROLL,
        ZOOM,
        NONE
    }

    /* loaded from: classes.dex */
    public enum Fit {
        HORIZONTAL,
        VERTICAL,
        INSIDE,
        OUTSIDE,
        NONE
    }

    public boolean A() {
        return h() != ExitType.NONE;
    }

    public boolean B() {
        return this.n;
    }

    public boolean C() {
        return D() && this.s;
    }

    public boolean D() {
        return this.y <= 0;
    }

    public boolean E() {
        return D() && this.r;
    }

    public boolean F() {
        return this.z <= 0;
    }

    public boolean G() {
        return this.v;
    }

    public boolean H() {
        return D() && this.u;
    }

    public boolean I() {
        return D() && this.t;
    }

    public Settings J(boolean z) {
        this.n = z;
        return this;
    }

    public Settings K(Fit fit) {
        this.p = fit;
        return this;
    }

    public Settings L(boolean z) {
        this.s = z;
        return this;
    }

    public Settings M(int i, int i2) {
        this.f = i;
        this.g = i2;
        return this;
    }

    public Settings N(int i, int i2) {
        this.e = true;
        this.c = i;
        this.d = i2;
        return this;
    }

    public Settings O(int i, int i2) {
        this.a = i;
        this.b = i2;
        return this;
    }

    public Settings a() {
        this.z++;
        return this;
    }

    public Settings b() {
        this.y++;
        return this;
    }

    public Settings c() {
        this.z--;
        return this;
    }

    public Settings d() {
        this.y--;
        return this;
    }

    public long e() {
        return this.A;
    }

    public Bounds f() {
        return this.q;
    }

    public float g() {
        return this.j;
    }

    public ExitType h() {
        return D() ? this.x : ExitType.NONE;
    }

    public Fit i() {
        return this.p;
    }

    public int j() {
        return this.o;
    }

    public int k() {
        return this.g;
    }

    public int l() {
        return this.f;
    }

    public float m() {
        return this.i;
    }

    public float n() {
        return this.h;
    }

    public int o() {
        return this.e ? this.d : this.b;
    }

    public int p() {
        return this.e ? this.c : this.a;
    }

    public float q() {
        return this.l;
    }

    public float r() {
        return this.m;
    }

    public float s() {
        return this.k;
    }

    public int t() {
        return this.b;
    }

    public int u() {
        return this.a;
    }

    public boolean v() {
        return (this.f == 0 || this.g == 0) ? false : true;
    }

    public boolean w() {
        return (this.a == 0 || this.b == 0) ? false : true;
    }

    public void x(Context context, AttributeSet attributeSet) {
        if (attributeSet == null) {
            return;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h23.GestureView);
        this.c = obtainStyledAttributes.getDimensionPixelSize(h23.GestureView_gest_movementAreaWidth, this.c);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(h23.GestureView_gest_movementAreaHeight, this.d);
        this.d = dimensionPixelSize;
        this.e = this.c > 0 && dimensionPixelSize > 0;
        this.h = obtainStyledAttributes.getFloat(h23.GestureView_gest_minZoom, this.h);
        this.i = obtainStyledAttributes.getFloat(h23.GestureView_gest_maxZoom, this.i);
        this.j = obtainStyledAttributes.getFloat(h23.GestureView_gest_doubleTapZoom, this.j);
        this.k = obtainStyledAttributes.getFloat(h23.GestureView_gest_overzoomFactor, this.k);
        this.l = obtainStyledAttributes.getDimension(h23.GestureView_gest_overscrollX, this.l);
        this.m = obtainStyledAttributes.getDimension(h23.GestureView_gest_overscrollY, this.m);
        this.n = obtainStyledAttributes.getBoolean(h23.GestureView_gest_fillViewport, this.n);
        this.o = obtainStyledAttributes.getInt(h23.GestureView_gest_gravity, this.o);
        this.p = Fit.values()[obtainStyledAttributes.getInteger(h23.GestureView_gest_fitMethod, this.p.ordinal())];
        this.q = Bounds.values()[obtainStyledAttributes.getInteger(h23.GestureView_gest_boundsType, this.q.ordinal())];
        this.r = obtainStyledAttributes.getBoolean(h23.GestureView_gest_panEnabled, this.r);
        this.s = obtainStyledAttributes.getBoolean(h23.GestureView_gest_flingEnabled, this.s);
        this.t = obtainStyledAttributes.getBoolean(h23.GestureView_gest_zoomEnabled, this.t);
        this.u = obtainStyledAttributes.getBoolean(h23.GestureView_gest_rotationEnabled, this.u);
        this.v = obtainStyledAttributes.getBoolean(h23.GestureView_gest_restrictRotation, this.v);
        this.w = obtainStyledAttributes.getBoolean(h23.GestureView_gest_doubleTapEnabled, this.w);
        this.x = obtainStyledAttributes.getBoolean(h23.GestureView_gest_exitEnabled, true) ? this.x : ExitType.NONE;
        this.A = obtainStyledAttributes.getInt(h23.GestureView_gest_animationDuration, (int) this.A);
        if (obtainStyledAttributes.getBoolean(h23.GestureView_gest_disableGestures, false)) {
            b();
        }
        if (obtainStyledAttributes.getBoolean(h23.GestureView_gest_disableBounds, false)) {
            a();
        }
        obtainStyledAttributes.recycle();
    }

    public boolean y() {
        return D() && this.w;
    }

    public boolean z() {
        return D() && (this.r || this.t || this.u || this.w);
    }
}
