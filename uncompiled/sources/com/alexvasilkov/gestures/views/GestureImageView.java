package com.alexvasilkov.gestures.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.alexvasilkov.gestures.GestureController;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class GestureImageView extends ImageView implements hf1, oz, mz, te {
    public df1 a;
    public final nz f0;
    public final nz g0;
    public final Matrix h0;
    public tj4 i0;

    /* loaded from: classes.dex */
    public class a implements GestureController.e {
        public a() {
        }

        @Override // com.alexvasilkov.gestures.GestureController.e
        public void a(us3 us3Var) {
            GestureImageView.this.c(us3Var);
        }

        @Override // com.alexvasilkov.gestures.GestureController.e
        public void b(us3 us3Var, us3 us3Var2) {
            GestureImageView.this.c(us3Var2);
        }
    }

    public GestureImageView(Context context) {
        this(context, null, 0);
    }

    public static Drawable e(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return context.getDrawable(i);
        }
        return context.getResources().getDrawable(i);
    }

    public void a(RectF rectF, float f) {
        this.f0.a(rectF, f);
    }

    @Override // defpackage.mz
    public void b(RectF rectF) {
        this.g0.a(rectF, Utils.FLOAT_EPSILON);
    }

    public void c(us3 us3Var) {
        us3Var.d(this.h0);
        setImageMatrix(this.h0);
    }

    public final void d() {
        if (this.a == null) {
            this.a = new df1(this);
        }
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        this.g0.c(canvas);
        this.f0.c(canvas);
        super.draw(canvas);
        this.f0.b(canvas);
        this.g0.b(canvas);
        if (ef1.c()) {
            bf0.a(this, canvas);
        }
    }

    @Override // defpackage.te
    public tj4 getPositionAnimator() {
        if (this.i0 == null) {
            this.i0 = new tj4(this);
        }
        return this.i0;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.a.n().O((i - getPaddingLeft()) - getPaddingRight(), (i2 - getPaddingTop()) - getPaddingBottom());
        this.a.Q();
    }

    @Override // android.view.View
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.a.onTouch(this, motionEvent);
    }

    @Override // android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        d();
        Settings n = this.a.n();
        float l = n.l();
        float k = n.k();
        if (drawable == null) {
            n.M(0, 0);
        } else if (drawable.getIntrinsicWidth() != -1 && drawable.getIntrinsicHeight() != -1) {
            n.M(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        } else {
            n.M(n.p(), n.o());
        }
        float l2 = n.l();
        float k2 = n.k();
        if (l2 > Utils.FLOAT_EPSILON && k2 > Utils.FLOAT_EPSILON && l > Utils.FLOAT_EPSILON && k > Utils.FLOAT_EPSILON) {
            this.a.p().k(Math.min(l / l2, k / k2));
            this.a.V();
            this.a.p().k(Utils.FLOAT_EPSILON);
            return;
        }
        this.a.Q();
    }

    @Override // android.widget.ImageView
    public void setImageResource(int i) {
        setImageDrawable(e(getContext(), i));
    }

    public GestureImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Override // defpackage.hf1
    public df1 getController() {
        return this.a;
    }

    public GestureImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f0 = new nz(this);
        this.g0 = new nz(this);
        this.h0 = new Matrix();
        d();
        this.a.n().x(context, attributeSet);
        this.a.j(new a());
        setScaleType(ImageView.ScaleType.MATRIX);
    }
}
