package com.alexvasilkov.gestures.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import com.alexvasilkov.gestures.GestureController;

/* loaded from: classes.dex */
public class GestureFrameLayout extends FrameLayout implements hf1, te {
    public final df1 a;
    public tj4 f0;
    public final Matrix g0;
    public final Matrix h0;
    public final RectF i0;
    public final float[] j0;
    public MotionEvent k0;

    /* loaded from: classes.dex */
    public class a implements GestureController.e {
        public a() {
        }

        @Override // com.alexvasilkov.gestures.GestureController.e
        public void a(us3 us3Var) {
            GestureFrameLayout.this.c(us3Var);
        }

        @Override // com.alexvasilkov.gestures.GestureController.e
        public void b(us3 us3Var, us3 us3Var2) {
            GestureFrameLayout.this.c(us3Var2);
        }
    }

    public GestureFrameLayout(Context context) {
        this(context, null, 0);
    }

    public static int d(int i, int i2, int i3) {
        if (i3 == -2) {
            return View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), 0);
        }
        return FrameLayout.getChildMeasureSpec(i, i2, i3);
    }

    public final MotionEvent a(MotionEvent motionEvent, Matrix matrix) {
        this.j0[0] = motionEvent.getX();
        this.j0[1] = motionEvent.getY();
        matrix.mapPoints(this.j0);
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        float[] fArr = this.j0;
        obtain.setLocation(fArr[0], fArr[1]);
        return obtain;
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() == 0) {
            super.addView(view, i, layoutParams);
            return;
        }
        throw new IllegalArgumentException("GestureFrameLayout can contain only one child");
    }

    public final void b(Rect rect, Matrix matrix) {
        this.i0.set(rect.left, rect.top, rect.right, rect.bottom);
        matrix.mapRect(this.i0);
        rect.set(Math.round(this.i0.left), Math.round(this.i0.top), Math.round(this.i0.right), Math.round(this.i0.bottom));
    }

    public void c(us3 us3Var) {
        us3Var.d(this.g0);
        this.g0.invert(this.h0);
        invalidate();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        canvas.save();
        canvas.concat(this.g0);
        super.dispatchDraw(canvas);
        canvas.restore();
        if (ef1.c()) {
            bf0.a(this, canvas);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        this.k0 = motionEvent;
        MotionEvent a2 = a(motionEvent, this.h0);
        try {
            return super.dispatchTouchEvent(a2);
        } finally {
            a2.recycle();
        }
    }

    @Override // defpackage.te
    public tj4 getPositionAnimator() {
        if (this.f0 == null) {
            this.f0 = new tj4(this);
        }
        return this.f0;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public ViewParent invalidateChildInParent(int[] iArr, Rect rect) {
        b(rect, this.g0);
        return super.invalidateChildInParent(iArr, rect);
    }

    @Override // android.view.ViewGroup
    public void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        view.measure(d(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width), d(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height));
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.a.C(this, this.k0);
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        View childAt = getChildCount() == 0 ? null : getChildAt(0);
        if (childAt != null) {
            this.a.n().M(childAt.getMeasuredWidth(), childAt.getMeasuredHeight());
            this.a.V();
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.a.n().O((i - getPaddingLeft()) - getPaddingRight(), (i2 - getPaddingTop()) - getPaddingBottom());
        this.a.V();
    }

    @Override // android.view.View
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.a.onTouch(this, this.k0);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z) {
            MotionEvent obtain = MotionEvent.obtain(this.k0);
            obtain.setAction(3);
            this.a.C(this, obtain);
            obtain.recycle();
        }
    }

    public GestureFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Override // defpackage.hf1
    public df1 getController() {
        return this.a;
    }

    public GestureFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.g0 = new Matrix();
        this.h0 = new Matrix();
        this.i0 = new RectF();
        this.j0 = new float[2];
        df1 df1Var = new df1(this);
        this.a = df1Var;
        df1Var.n().x(context, attributeSet);
        df1Var.j(new a());
    }
}
