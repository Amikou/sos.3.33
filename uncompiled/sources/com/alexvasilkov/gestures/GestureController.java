package com.alexvasilkov.gestures;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.widget.OverScroller;
import com.github.mikephil.charting.utils.Utils;
import defpackage.o93;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class GestureController implements View.OnTouchListener {
    public static final PointF N0 = new PointF();
    public static final Point O0 = new Point();
    public static final RectF P0 = new RectF();
    public static final float[] Q0 = new float[2];
    public boolean A0;
    public final OverScroller C0;
    public final e71 D0;
    public final ba2 E0;
    public final View H0;
    public final Settings I0;
    public final vs3 L0;
    public final bz0 M0;
    public final int a;
    public final int f0;
    public final int g0;
    public d h0;
    public f i0;
    public final he k0;
    public final GestureDetector l0;
    public final ScaleGestureDetector m0;
    public final o93 n0;
    public boolean o0;
    public boolean p0;
    public boolean q0;
    public boolean r0;
    public boolean s0;
    public boolean x0;
    public boolean y0;
    public boolean z0;
    public final List<e> j0 = new ArrayList();
    public float t0 = Float.NaN;
    public float u0 = Float.NaN;
    public float v0 = Float.NaN;
    public float w0 = Float.NaN;
    public StateSource B0 = StateSource.NONE;
    public final us3 F0 = new us3();
    public final us3 G0 = new us3();
    public final us3 J0 = new us3();
    public final us3 K0 = new us3();

    /* loaded from: classes.dex */
    public enum StateSource {
        NONE,
        USER,
        ANIMATION
    }

    /* loaded from: classes.dex */
    public class b implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener, o93.a {
        public b() {
        }

        @Override // defpackage.o93.a
        public boolean a(o93 o93Var) {
            return GestureController.this.F(o93Var);
        }

        @Override // defpackage.o93.a
        public void b(o93 o93Var) {
            GestureController.this.G(o93Var);
        }

        @Override // defpackage.o93.a
        public boolean c(o93 o93Var) {
            return GestureController.this.E(o93Var);
        }

        @Override // android.view.GestureDetector.OnDoubleTapListener
        public boolean onDoubleTap(MotionEvent motionEvent) {
            return false;
        }

        @Override // android.view.GestureDetector.OnDoubleTapListener
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return GestureController.this.x(motionEvent);
        }

        @Override // android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return GestureController.this.y(motionEvent);
        }

        @Override // android.view.GestureDetector.OnGestureListener
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return GestureController.this.z(motionEvent, motionEvent2, f, f2);
        }

        @Override // android.view.GestureDetector.OnGestureListener
        public void onLongPress(MotionEvent motionEvent) {
            GestureController.this.D(motionEvent);
        }

        @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            return GestureController.this.H(scaleGestureDetector);
        }

        @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            return GestureController.this.I(scaleGestureDetector);
        }

        @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
            GestureController.this.J(scaleGestureDetector);
        }

        @Override // android.view.GestureDetector.OnGestureListener
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return GestureController.this.K(motionEvent, motionEvent2, f, f2);
        }

        @Override // android.view.GestureDetector.OnGestureListener
        public void onShowPress(MotionEvent motionEvent) {
        }

        @Override // android.view.GestureDetector.OnDoubleTapListener
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return GestureController.this.L(motionEvent);
        }

        @Override // android.view.GestureDetector.OnGestureListener
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return GestureController.this.M(motionEvent);
        }
    }

    /* loaded from: classes.dex */
    public class c extends he {
        public c(View view) {
            super(view);
        }

        @Override // defpackage.he
        public boolean a() {
            boolean z;
            boolean z2 = true;
            if (GestureController.this.r()) {
                int currX = GestureController.this.C0.getCurrX();
                int currY = GestureController.this.C0.getCurrY();
                if (GestureController.this.C0.computeScrollOffset()) {
                    if (!GestureController.this.B(GestureController.this.C0.getCurrX() - currX, GestureController.this.C0.getCurrY() - currY)) {
                        GestureController.this.T();
                    }
                    z = true;
                } else {
                    z = false;
                }
                if (!GestureController.this.r()) {
                    GestureController.this.A(false);
                }
            } else {
                z = false;
            }
            if (GestureController.this.s()) {
                GestureController.this.D0.a();
                v42.c(GestureController.this.J0, GestureController.this.F0, GestureController.this.t0, GestureController.this.u0, GestureController.this.G0, GestureController.this.v0, GestureController.this.w0, GestureController.this.D0.c());
                if (!GestureController.this.s()) {
                    GestureController.this.N(false);
                }
            } else {
                z2 = z;
            }
            if (z2) {
                GestureController.this.w();
            }
            return z2;
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a(MotionEvent motionEvent);

        boolean onDoubleTap(MotionEvent motionEvent);

        void onDown(MotionEvent motionEvent);

        void onLongPress(MotionEvent motionEvent);

        boolean onSingleTapConfirmed(MotionEvent motionEvent);

        boolean onSingleTapUp(MotionEvent motionEvent);
    }

    /* loaded from: classes.dex */
    public interface e {
        void a(us3 us3Var);

        void b(us3 us3Var, us3 us3Var2);
    }

    /* loaded from: classes.dex */
    public interface f {
        void a(StateSource stateSource);
    }

    public GestureController(View view) {
        Context context = view.getContext();
        this.H0 = view;
        Settings settings = new Settings();
        this.I0 = settings;
        this.L0 = new vs3(settings);
        this.k0 = new c(view);
        b bVar = new b();
        this.l0 = new GestureDetector(context, bVar);
        this.m0 = new nc3(context, bVar);
        this.n0 = new o93(context, bVar);
        this.M0 = new bz0(view, this);
        this.C0 = new OverScroller(context);
        this.D0 = new e71();
        this.E0 = new ba2(settings);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.a = viewConfiguration.getScaledTouchSlop();
        this.f0 = viewConfiguration.getScaledMinimumFlingVelocity();
        this.g0 = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    public void A(boolean z) {
        if (!z) {
            k();
        }
        v();
    }

    public boolean B(int i, int i2) {
        float f2 = this.J0.f();
        float g = this.J0.g();
        float f3 = i + f2;
        float f4 = i2 + g;
        if (this.I0.F()) {
            ba2 ba2Var = this.E0;
            PointF pointF = N0;
            ba2Var.h(f3, f4, pointF);
            f3 = pointF.x;
            f4 = pointF.y;
        }
        this.J0.o(f3, f4);
        return (us3.c(f2, f3) && us3.c(g, f4)) ? false : true;
    }

    public boolean C(View view, MotionEvent motionEvent) {
        this.o0 = true;
        return O(view, motionEvent);
    }

    public void D(MotionEvent motionEvent) {
        if (this.I0.z()) {
            this.H0.performLongClick();
            d dVar = this.h0;
            if (dVar != null) {
                dVar.onLongPress(motionEvent);
            }
        }
    }

    public boolean E(o93 o93Var) {
        if (!this.I0.H() || s()) {
            return false;
        }
        if (this.M0.j()) {
            return true;
        }
        this.t0 = o93Var.c();
        this.u0 = o93Var.d();
        this.J0.j(o93Var.e(), this.t0, this.u0);
        this.x0 = true;
        return true;
    }

    public boolean F(o93 o93Var) {
        boolean H = this.I0.H();
        this.s0 = H;
        if (H) {
            this.M0.k();
        }
        return this.s0;
    }

    public void G(o93 o93Var) {
        if (this.s0) {
            this.M0.l();
        }
        this.s0 = false;
        this.z0 = true;
    }

    public boolean H(ScaleGestureDetector scaleGestureDetector) {
        if (this.I0.I() && !s()) {
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            if (!Float.isNaN(scaleFactor) && !Float.isNaN(scaleGestureDetector.getFocusX()) && !Float.isNaN(scaleGestureDetector.getFocusY())) {
                if (this.M0.m(scaleFactor)) {
                    return true;
                }
                this.t0 = scaleGestureDetector.getFocusX();
                float focusY = scaleGestureDetector.getFocusY();
                this.u0 = focusY;
                this.J0.q(scaleFactor, this.t0, focusY);
                this.x0 = true;
                return true;
            }
        }
        return false;
    }

    public boolean I(ScaleGestureDetector scaleGestureDetector) {
        boolean I = this.I0.I();
        this.r0 = I;
        if (I) {
            this.M0.n();
        }
        return this.r0;
    }

    public void J(ScaleGestureDetector scaleGestureDetector) {
        if (this.r0) {
            this.M0.o();
        }
        this.r0 = false;
        this.y0 = true;
    }

    public boolean K(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (!this.I0.E() || s() || Float.isNaN(f2) || Float.isNaN(f3)) {
            return false;
        }
        float f4 = -f2;
        float f5 = -f3;
        if (this.M0.p(f4, f5)) {
            return true;
        }
        if (!this.q0) {
            boolean z = Math.abs(motionEvent2.getX() - motionEvent.getX()) > ((float) this.a) || Math.abs(motionEvent2.getY() - motionEvent.getY()) > ((float) this.a);
            this.q0 = z;
            if (z) {
                return false;
            }
        }
        if (this.q0) {
            this.J0.n(f4, f5);
            this.x0 = true;
        }
        return this.q0;
    }

    public boolean L(MotionEvent motionEvent) {
        if (this.I0.y()) {
            this.H0.performClick();
        }
        d dVar = this.h0;
        return dVar != null && dVar.onSingleTapConfirmed(motionEvent);
    }

    public boolean M(MotionEvent motionEvent) {
        if (!this.I0.y()) {
            this.H0.performClick();
        }
        d dVar = this.h0;
        return dVar != null && dVar.onSingleTapUp(motionEvent);
    }

    public void N(boolean z) {
        this.A0 = false;
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        this.v0 = Float.NaN;
        this.w0 = Float.NaN;
        v();
    }

    public boolean O(View view, MotionEvent motionEvent) {
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.offsetLocation(-view.getPaddingLeft(), -view.getPaddingTop());
        this.l0.setIsLongpressEnabled(view.isLongClickable());
        boolean onTouchEvent = this.l0.onTouchEvent(obtain);
        this.m0.onTouchEvent(obtain);
        this.n0.f(obtain);
        boolean z = onTouchEvent || this.r0 || this.s0;
        v();
        if (this.M0.g() && !this.J0.equals(this.K0)) {
            w();
        }
        if (this.x0) {
            this.x0 = false;
            this.L0.i(this.J0, this.K0, this.t0, this.u0, true, true, false);
            if (!this.J0.equals(this.K0)) {
                w();
            }
        }
        if (this.y0 || this.z0) {
            this.y0 = false;
            this.z0 = false;
            if (!this.M0.g()) {
                m(this.L0.j(this.J0, this.K0, this.t0, this.u0, true, false, true), false);
            }
        }
        if (obtain.getActionMasked() == 1 || obtain.getActionMasked() == 3) {
            P(obtain);
            v();
        }
        if (!this.p0 && R(obtain)) {
            this.p0 = true;
            ViewParent parent = view.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
        obtain.recycle();
        return z;
    }

    public void P(MotionEvent motionEvent) {
        this.q0 = false;
        this.r0 = false;
        this.s0 = false;
        this.M0.q();
        if (!r() && !this.A0) {
            k();
        }
        d dVar = this.h0;
        if (dVar != null) {
            dVar.a(motionEvent);
        }
    }

    public void Q() {
        S();
        if (this.L0.h(this.J0)) {
            u();
        } else {
            w();
        }
    }

    public boolean R(MotionEvent motionEvent) {
        if (this.M0.g()) {
            return true;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0 || actionMasked == 2) {
            vs3 vs3Var = this.L0;
            us3 us3Var = this.J0;
            RectF rectF = P0;
            vs3Var.g(us3Var, rectF);
            boolean z = us3.a(rectF.width(), Utils.FLOAT_EPSILON) > 0 || us3.a(rectF.height(), Utils.FLOAT_EPSILON) > 0;
            if (this.I0.E() && (z || !this.I0.F())) {
                return true;
            }
        } else if (actionMasked == 5) {
            return this.I0.I() || this.I0.H();
        }
        return false;
    }

    public void S() {
        U();
        T();
    }

    public void T() {
        if (r()) {
            this.C0.forceFinished(true);
            A(true);
        }
    }

    public void U() {
        if (s()) {
            this.D0.b();
            N(true);
        }
    }

    public void V() {
        this.L0.c(this.J0);
        this.L0.c(this.K0);
        this.L0.c(this.F0);
        this.L0.c(this.G0);
        this.M0.a();
        if (this.L0.m(this.J0)) {
            u();
        } else {
            w();
        }
    }

    public void j(e eVar) {
        this.j0.add(eVar);
    }

    public boolean k() {
        return m(this.J0, true);
    }

    public boolean l(us3 us3Var) {
        return m(us3Var, true);
    }

    public final boolean m(us3 us3Var, boolean z) {
        if (us3Var == null) {
            return false;
        }
        S();
        if (Float.isNaN(this.t0) || Float.isNaN(this.u0)) {
            Settings settings = this.I0;
            Point point = O0;
            si1.a(settings, point);
            this.t0 = point.x;
            this.u0 = point.y;
        }
        us3 j = z ? this.L0.j(us3Var, this.K0, this.t0, this.u0, false, false, true) : null;
        if (j != null) {
            us3Var = j;
        }
        if (us3Var.equals(this.J0)) {
            return false;
        }
        this.A0 = z;
        this.F0.m(this.J0);
        this.G0.m(us3Var);
        float[] fArr = Q0;
        fArr[0] = this.t0;
        fArr[1] = this.u0;
        v42.a(fArr, this.F0, this.G0);
        this.v0 = fArr[0];
        this.w0 = fArr[1];
        this.D0.f(this.I0.e());
        this.D0.g(Utils.FLOAT_EPSILON, 1.0f);
        this.k0.c();
        v();
        return true;
    }

    public Settings n() {
        return this.I0;
    }

    public us3 o() {
        return this.J0;
    }

    @Override // android.view.View.OnTouchListener
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.o0) {
            O(view, motionEvent);
        }
        this.o0 = false;
        return this.I0.z();
    }

    public vs3 p() {
        return this.L0;
    }

    public boolean q() {
        return s() || r();
    }

    public boolean r() {
        return !this.C0.isFinished();
    }

    public boolean s() {
        return !this.D0.e();
    }

    public final int t(float f2) {
        if (Math.abs(f2) < this.f0) {
            return 0;
        }
        if (Math.abs(f2) >= this.g0) {
            return ((int) Math.signum(f2)) * this.g0;
        }
        return Math.round(f2);
    }

    public void u() {
        this.M0.s();
        for (e eVar : this.j0) {
            eVar.b(this.K0, this.J0);
        }
        w();
    }

    public final void v() {
        StateSource stateSource = StateSource.NONE;
        if (q()) {
            stateSource = StateSource.ANIMATION;
        } else if (this.q0 || this.r0 || this.s0) {
            stateSource = StateSource.USER;
        }
        if (this.B0 != stateSource) {
            this.B0 = stateSource;
            f fVar = this.i0;
            if (fVar != null) {
                fVar.a(stateSource);
            }
        }
    }

    public void w() {
        this.K0.m(this.J0);
        for (e eVar : this.j0) {
            eVar.a(this.J0);
        }
    }

    public boolean x(MotionEvent motionEvent) {
        if (this.I0.y() && motionEvent.getActionMasked() == 1 && !this.r0) {
            d dVar = this.h0;
            if (dVar == null || !dVar.onDoubleTap(motionEvent)) {
                l(this.L0.l(this.J0, motionEvent.getX(), motionEvent.getY()));
                return true;
            }
            return true;
        }
        return false;
    }

    public boolean y(MotionEvent motionEvent) {
        this.p0 = false;
        T();
        d dVar = this.h0;
        if (dVar != null) {
            dVar.onDown(motionEvent);
        }
        return false;
    }

    public boolean z(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (this.I0.E() && this.I0.C() && !s()) {
            if (this.M0.i()) {
                return true;
            }
            T();
            this.E0.i(this.J0).e(this.J0.f(), this.J0.g());
            this.C0.fling(Math.round(this.J0.f()), Math.round(this.J0.g()), t(f2 * 0.9f), t(f3 * 0.9f), Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
            this.k0.c();
            v();
            return true;
        }
        return false;
    }
}
