package com.zendesk.logger;

import android.os.Build;
import android.util.Log;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/* loaded from: classes2.dex */
public class Logger {
    public static final TimeZone a = TimeZone.getTimeZone("UTC");
    public static final List<c> b = new ArrayList();
    public static c c;
    public static boolean d;

    /* loaded from: classes2.dex */
    public enum Priority {
        VERBOSE(2),
        DEBUG(3),
        INFO(4),
        WARN(5),
        ERROR(6);
        
        private final int priority;

        Priority(int i) {
            this.priority = i;
        }
    }

    /* loaded from: classes2.dex */
    public static class a implements c {
        @Override // com.zendesk.logger.Logger.c
        public void a(Priority priority, String str, String str2, Throwable th) {
            Priority priority2;
            SimpleDateFormat simpleDateFormat;
            String a = y12.a(str);
            if (b(str) && (priority2 = Priority.ERROR) == priority) {
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).setTimeZone(Logger.a);
                Log.println(priority2.priority, a, "Time in UTC: " + simpleDateFormat.format(new Date()));
            }
            if (th != null) {
                str2 = str2 + ru3.b + Log.getStackTraceString(th);
            }
            for (String str3 : y12.c(str2, 4000)) {
                Log.println(priority == null ? Priority.INFO.priority : priority.priority, a, str3);
            }
        }

        public final boolean b(String str) {
            return ru3.b(str) && (str.endsWith("Provider") || str.endsWith("Service"));
        }
    }

    /* loaded from: classes2.dex */
    public static class b implements c {
        @Override // com.zendesk.logger.Logger.c
        public void a(Priority priority, String str, String str2, Throwable th) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("[");
            sb.append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).format(new Date()));
            sb.append("]");
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            sb.append(priority == null ? y12.b(Priority.INFO.priority) : y12.b(priority.priority));
            sb.append("/");
            if (!ru3.b(str)) {
                str = "UNKNOWN";
            }
            sb.append(str);
            sb.append(": ");
            sb.append(str2);
            System.out.println(sb.toString());
            if (th != null) {
                th.printStackTrace(System.out);
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface c {
        void a(Priority priority, String str, String str2, Throwable th);
    }

    static {
        b bVar;
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                c = new a();
            }
        } catch (ClassNotFoundException unused) {
            if (c == null) {
                bVar = new b();
            }
        } catch (Throwable th) {
            if (c == null) {
                c = new b();
            }
            throw th;
        }
        if (c == null) {
            bVar = new b();
            c = bVar;
        }
        d = false;
    }

    public static void b(String str, String str2, Object... objArr) {
        i(Priority.DEBUG, str, str2, null, objArr);
    }

    public static void c(String str, cw0 cw0Var) {
        StringBuilder sb = new StringBuilder();
        if (cw0Var != null) {
            sb.append("Network Error: ");
            sb.append(cw0Var.j());
            sb.append(", Status Code: ");
            sb.append(cw0Var.e());
            if (ru3.b(cw0Var.h())) {
                sb.append(", Reason: ");
                sb.append(cw0Var.h());
            }
        }
        String sb2 = sb.toString();
        Priority priority = Priority.ERROR;
        if (!ru3.b(sb2)) {
            sb2 = "Unknown error";
        }
        i(priority, str, sb2, null, new Object[0]);
    }

    public static void d(String str, String str2, Throwable th, Object... objArr) {
        i(Priority.ERROR, str, str2, th, objArr);
    }

    public static void e(String str, String str2, Object... objArr) {
        i(Priority.ERROR, str, str2, null, objArr);
    }

    public static void f(String str, String str2, Throwable th, Object... objArr) {
        i(Priority.INFO, str, str2, th, objArr);
    }

    public static void g(String str, String str2, Object... objArr) {
        i(Priority.INFO, str, str2, null, objArr);
    }

    public static boolean h() {
        return d;
    }

    public static void i(Priority priority, String str, String str2, Throwable th, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str2 = String.format(Locale.US, str2, objArr);
        }
        if (d) {
            c.a(priority, str, str2, th);
            for (c cVar : b) {
                cVar.a(priority, str, str2, th);
            }
        }
    }

    public static void j(String str, String str2, Throwable th, Object... objArr) {
        i(Priority.WARN, str, str2, th, objArr);
    }

    public static void k(String str, String str2, Object... objArr) {
        i(Priority.WARN, str, str2, null, objArr);
    }
}
