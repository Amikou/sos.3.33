package com.creageek.segmentedbutton;

/* compiled from: SegmentTypes.kt */
/* loaded from: classes.dex */
public enum SegmentType {
    first,
    center,
    last,
    only
}
