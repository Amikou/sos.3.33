package com.creageek.segmentedbutton;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import kotlin.NoWhenBranchMatchedException;

/* compiled from: SegmentedButton.kt */
/* loaded from: classes.dex */
public final class SegmentedButton extends RadioGroup implements View.OnClickListener {
    public tc1<? super RadioButton, te4> A0;
    public tc1<? super RadioButton, te4> B0;
    public Integer C0;
    public final SegmentSpreadType a;
    public final int f0;
    public final int g0;
    public final int h0;
    public final int i0;
    public Typeface j0;
    public Typeface k0;
    public final int l0;
    public final int m0;
    public final int n0;
    public final float o0;
    public final float p0;
    public final int q0;
    public final int r0;
    public final int s0;
    public final int t0;
    public final float u0;
    public final int[] v0;
    public final int[] w0;
    public Integer x0;
    public RadioButton y0;
    public tc1<? super RadioButton, te4> z0;

    /* compiled from: SegmentedButton.kt */
    /* loaded from: classes.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[SegmentSpreadType.values().length];
            iArr[SegmentSpreadType.evenly.ordinal()] = 1;
            iArr[SegmentSpreadType.wrap.ordinal()] = 2;
            a = iArr;
            int[] iArr2 = new int[SegmentType.values().length];
            iArr2[SegmentType.first.ordinal()] = 1;
            iArr2[SegmentType.center.ordinal()] = 2;
            iArr2[SegmentType.last.ordinal()] = 3;
            iArr2[SegmentType.only.ordinal()] = 4;
            b = iArr2;
        }
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public SegmentedButton(Context context) {
        this(context, null);
        fs1.f(context, "context");
    }

    public static /* synthetic */ StateListDrawable d(SegmentedButton segmentedButton, SegmentType segmentType, int i, int i2, int i3, int i4, int i5, Object obj) {
        return segmentedButton.c(segmentType, i, i2, i3, (i5 & 16) != 0 ? i2 : i4);
    }

    public final LayerDrawable a(SegmentType segmentType, int i, int i2) {
        LayerDrawable layerDrawable = new LayerDrawable(new ShapeDrawable[]{e(segmentType, i2), b(segmentType, i)});
        int i3 = a.b[segmentType.ordinal()];
        if (i3 == 1) {
            int i4 = this.m0;
            layerDrawable.setLayerInset(1, i4, i4, this.n0, i4);
        } else if (i3 == 2) {
            int i5 = this.n0;
            int i6 = this.m0;
            layerDrawable.setLayerInset(1, i5, i6, i5, i6);
        } else if (i3 == 3) {
            int i7 = this.n0;
            int i8 = this.m0;
            layerDrawable.setLayerInset(1, i7, i8, i8, i8);
        } else if (i3 == 4) {
            int i9 = this.m0;
            layerDrawable.setLayerInset(1, i9, i9, i9, i9);
        }
        return layerDrawable;
    }

    public final ShapeDrawable b(SegmentType segmentType, int i) {
        return f(segmentType, i, this.p0, this.u0);
    }

    public final StateListDrawable c(SegmentType segmentType, int i, int i2, int i3, int i4) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(this.v0, a(segmentType, i2, i4));
        stateListDrawable.addState(this.w0, a(segmentType, i, i3));
        return stateListDrawable;
    }

    public final ShapeDrawable e(SegmentType segmentType, int i) {
        return f(segmentType, i, this.o0, this.u0);
    }

    public final ShapeDrawable f(SegmentType segmentType, int i, float f, float f2) {
        float[] fArr;
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        int i2 = a.b[segmentType.ordinal()];
        if (i2 == 1) {
            fArr = new float[]{f, f, f2, f2, f2, f2, f, f};
        } else if (i2 == 2) {
            fArr = new float[]{f2, f2, f2, f2, f2, f2, f2, f2};
        } else if (i2 == 3) {
            fArr = new float[]{f2, f2, f, f, f, f, f2, f2};
        } else if (i2 != 4) {
            throw new NoWhenBranchMatchedException();
        } else {
            fArr = new float[]{f, f, f, f, f, f, f, f};
        }
        shapeDrawable.setShape(new RoundRectShape(fArr, null, null));
        Paint paint = shapeDrawable.getPaint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(i);
        return shapeDrawable;
    }

    public final ColorStateList g(int i, int i2) {
        return new ColorStateList(new int[][]{this.w0, this.v0}, new int[]{i, i2});
    }

    public final Integer getCheckedIndex() {
        return this.x0;
    }

    public final Integer getInitialCheckedIndex() {
        return this.C0;
    }

    public final RadioButton h(RadioButton radioButton, SegmentType segmentType, SegmentSpreadType segmentSpreadType, boolean z) {
        int i;
        radioButton.setTextSize(0, this.f0);
        radioButton.setTypeface(this.j0);
        Drawable d = d(this, segmentType, this.q0, this.r0, this.l0, 0, 16, null);
        RippleDrawable j = j(d, this.s0, this.t0);
        radioButton.setTextColor(g(this.h0, this.i0));
        if (z) {
            d = j;
        }
        radioButton.setBackground(d);
        radioButton.setButtonDrawable((Drawable) null);
        radioButton.setGravity(17);
        int i2 = a.a[segmentSpreadType.ordinal()];
        if (i2 == 1) {
            i = -1;
        } else if (i2 != 2) {
            throw new NoWhenBranchMatchedException();
        } else {
            i = -2;
        }
        radioButton.setLayoutParams(new RadioGroup.LayoutParams(i, this.g0, 1.0f));
        return radioButton;
    }

    public final void i(boolean z) {
        SegmentType segmentType;
        int childCount = getChildCount();
        if (childCount < 0) {
            return;
        }
        int i = 0;
        while (true) {
            int i2 = i + 1;
            View childAt = getChildAt(i);
            RadioButton radioButton = childAt instanceof RadioButton ? (RadioButton) childAt : null;
            if (radioButton != null) {
                radioButton.setOnClickListener(this);
                if (getChildCount() == 1) {
                    segmentType = SegmentType.only;
                } else if (i2 == 1) {
                    segmentType = SegmentType.first;
                } else {
                    segmentType = i2 == getChildCount() ? SegmentType.last : SegmentType.center;
                }
                h(radioButton, segmentType, this.a, z);
            }
            if (i == childCount) {
                return;
            }
            i = i2;
        }
    }

    public final RippleDrawable j(Drawable drawable, int i, int i2) {
        return new RippleDrawable(new ColorStateList(new int[][]{this.w0, this.v0}, new int[]{i, i2}), drawable, drawable);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        RadioButton radioButton = view instanceof RadioButton ? (RadioButton) view : null;
        if (radioButton == null) {
            return;
        }
        int indexOfChild = indexOfChild(radioButton);
        Integer checkedIndex = getCheckedIndex();
        if (checkedIndex != null && checkedIndex.intValue() == indexOfChild) {
            tc1<? super RadioButton, te4> tc1Var = this.A0;
            if (tc1Var == null) {
                return;
            }
            tc1Var.invoke(radioButton);
            return;
        }
        radioButton.setTypeface(this.k0);
        tc1<? super RadioButton, te4> tc1Var2 = this.z0;
        if (tc1Var2 != null) {
            tc1Var2.invoke(radioButton);
        }
        RadioButton radioButton2 = this.y0;
        if (radioButton2 != null) {
            radioButton2.setTypeface(this.j0);
            tc1<? super RadioButton, te4> tc1Var3 = this.B0;
            if (tc1Var3 != null) {
                tc1Var3.invoke(radioButton2);
            }
        }
        this.y0 = radioButton;
        this.x0 = Integer.valueOf(indexOfChild);
    }

    @Override // android.widget.RadioGroup, android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        i(true);
    }

    public final void setInitialCheckedIndex(Integer num) {
        this.C0 = num;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SegmentedButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
        this.u0 = n11.a(0.1f);
        this.v0 = new int[]{16842912};
        this.w0 = new int[]{-16842912};
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, j23.SegmentedButton, 0, 0);
        fs1.e(obtainStyledAttributes, "context.theme.obtainStyl…           0, 0\n        )");
        this.a = SegmentSpreadType.values()[obtainStyledAttributes.getInt(j23.SegmentedButton_spreadType, SegmentSpreadType.wrap.getValue())];
        this.f0 = obtainStyledAttributes.getDimensionPixelSize(j23.SegmentedButton_textSize, context.getResources().getDimensionPixelSize(iz2.default_segment_text_size));
        this.g0 = obtainStyledAttributes.getDimensionPixelSize(j23.SegmentedButton_segmentHeight, context.getResources().getDimensionPixelSize(iz2.default_segment_height));
        this.h0 = obtainStyledAttributes.getColor(j23.SegmentedButton_textColor, m70.d(context, ry2.default_text_color));
        this.i0 = obtainStyledAttributes.getColor(j23.SegmentedButton_textColorChecked, m70.d(context, ry2.default_text_color_checked));
        this.q0 = obtainStyledAttributes.getColor(j23.SegmentedButton_segmentColor, m70.d(context, ry2.default_segment_color));
        this.r0 = obtainStyledAttributes.getColor(j23.SegmentedButton_segmentColorChecked, m70.d(context, ry2.default_segment_color_checked));
        this.l0 = obtainStyledAttributes.getColor(j23.SegmentedButton_borderColor, m70.d(context, ry2.default_border_color));
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(j23.SegmentedButton_borderWidth, context.getResources().getDimensionPixelSize(iz2.default_border_width));
        this.m0 = dimensionPixelSize;
        float dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(j23.SegmentedButton_cornerRadius, context.getResources().getDimensionPixelSize(iz2.default_corner_radius));
        this.o0 = dimensionPixelSize2;
        this.s0 = obtainStyledAttributes.getColor(j23.SegmentedButton_rippleColor, m70.d(context, ry2.default_ripple_color));
        this.t0 = obtainStyledAttributes.getColor(j23.SegmentedButton_rippleColorChecked, m70.d(context, ry2.default_ripple_color_checked));
        int b = t42.b(dimensionPixelSize / 2.0f);
        this.n0 = b;
        this.p0 = dimensionPixelSize2 - b;
        int resourceId = obtainStyledAttributes.getResourceId(j23.SegmentedButton_segmentFont, -1);
        if (resourceId != -1) {
            this.j0 = g83.g(context, resourceId);
        }
        int resourceId2 = obtainStyledAttributes.getResourceId(j23.SegmentedButton_segmentFontChecked, -1);
        if (resourceId2 != -1) {
            this.k0 = g83.g(context, resourceId2);
        } else if (resourceId != -1) {
            this.k0 = this.j0;
        }
        obtainStyledAttributes.recycle();
    }
}
