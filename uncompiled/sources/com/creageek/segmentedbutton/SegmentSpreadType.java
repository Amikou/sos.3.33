package com.creageek.segmentedbutton;

/* compiled from: SegmentTypes.kt */
/* loaded from: classes.dex */
public enum SegmentSpreadType {
    evenly(0),
    wrap(1);
    
    private final int value;

    SegmentSpreadType(int i) {
        this.value = i;
    }

    public final int getValue() {
        return this.value;
    }
}
