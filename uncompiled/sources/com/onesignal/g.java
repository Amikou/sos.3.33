package com.onesignal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

/* compiled from: GooglePlayServicesUpgradePrompt.java */
/* loaded from: classes2.dex */
public class g {

    /* compiled from: GooglePlayServicesUpgradePrompt.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {

        /* compiled from: GooglePlayServicesUpgradePrompt.java */
        /* renamed from: com.onesignal.g$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class DialogInterface$OnClickListenerC0159a implements DialogInterface.OnClickListener {
            public DialogInterface$OnClickListenerC0159a(a aVar) {
            }

            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                b1.j(b1.a, "GT_DO_NOT_SHOW_MISSING_GPS", true);
            }
        }

        /* compiled from: GooglePlayServicesUpgradePrompt.java */
        /* loaded from: classes2.dex */
        public class b implements DialogInterface.OnClickListener {
            public final /* synthetic */ Activity a;

            public b(a aVar, Activity activity) {
                this.a = activity;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                g.a(this.a);
            }
        }

        @Override // java.lang.Runnable
        public void run() {
            Activity Q = OneSignal.Q();
            if (Q == null) {
                return;
            }
            String k = OSUtils.k(Q, "onesignal_gms_missing_alert_text", "To receive push notifications please press 'Update' to enable 'Google Play services'.");
            String k2 = OSUtils.k(Q, "onesignal_gms_missing_alert_button_update", "Update");
            String k3 = OSUtils.k(Q, "onesignal_gms_missing_alert_button_skip", "Skip");
            new AlertDialog.Builder(Q).setMessage(k).setPositiveButton(k2, new b(this, Q)).setNegativeButton(k3, new DialogInterface$OnClickListenerC0159a(this)).setNeutralButton(OSUtils.k(Q, "onesignal_gms_missing_alert_button_close", "Close"), (DialogInterface.OnClickListener) null).create().show();
        }
    }

    public static void a(Activity activity) {
        try {
            dh1 q = dh1.q();
            PendingIntent e = q.e(activity, q.i(OneSignal.e), 9000);
            if (e != null) {
                e.send();
            }
        } catch (PendingIntent.CanceledException e2) {
            e2.printStackTrace();
        }
    }

    public static boolean c() {
        try {
            PackageManager packageManager = OneSignal.e.getPackageManager();
            return !((String) packageManager.getPackageInfo("com.google.android.gms", 128).applicationInfo.loadLabel(packageManager)).equals("Market");
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static void d() {
        if (OSUtils.B() && c() && !OneSignal.X() && !b1.b(b1.a, "GT_DO_NOT_SHOW_MISSING_GPS", false)) {
            OSUtils.S(new a());
        }
    }
}
