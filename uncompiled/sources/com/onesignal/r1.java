package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.OneSignalStateSynchronizer;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UserStateSecondaryChannelSynchronizer.java */
/* loaded from: classes2.dex */
public abstract class r1 extends s1 {
    public r1(OneSignalStateSynchronizer.UserStateSynchronizerType userStateSynchronizerType) {
        super(userStateSynchronizerType);
    }

    @Override // com.onesignal.s1
    public OneSignal.LOG_LEVEL C() {
        return OneSignal.LOG_LEVEL.INFO;
    }

    @Override // com.onesignal.s1
    public void P(JSONObject jSONObject) {
        if (jSONObject.has("identifier")) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put(i0(), jSONObject.get("identifier"));
                if (jSONObject.has(h0())) {
                    jSONObject2.put(h0(), jSONObject.get(h0()));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            g0(jSONObject2);
        }
    }

    @Override // com.onesignal.s1
    public void U() {
        if ((B() == null && E() == null) || OneSignal.z0() == null) {
            return;
        }
        D(0).c();
    }

    public abstract void f0();

    public abstract void g0(JSONObject jSONObject);

    public abstract String h0();

    public abstract String i0();

    public abstract int j0();

    public void k0() {
        U();
    }

    @Override // com.onesignal.s1
    public void n(JSONObject jSONObject) {
        try {
            jSONObject.put("device_type", j0());
            jSONObject.putOpt("device_player_id", OneSignal.z0());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override // com.onesignal.s1
    public void w(JSONObject jSONObject) {
        if (jSONObject.has("identifier")) {
            f0();
        }
    }
}
