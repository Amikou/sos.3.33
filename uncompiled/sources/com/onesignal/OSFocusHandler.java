package com.onesignal;

import android.content.Context;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.NetworkType;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c;
import com.onesignal.OneSignal;
import defpackage.h60;
import java.util.concurrent.TimeUnit;

/* compiled from: OSFocusHandler.kt */
/* loaded from: classes2.dex */
public final class OSFocusHandler {
    public static boolean b;
    public static boolean c;
    public static boolean d;
    public static final a e = new a(null);
    public Runnable a;

    /* compiled from: OSFocusHandler.kt */
    /* loaded from: classes2.dex */
    public static final class OnLostFocusWorker extends Worker {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public OnLostFocusWorker(Context context, WorkerParameters workerParameters) {
            super(context, workerParameters);
            fs1.f(context, "context");
            fs1.f(workerParameters, "workerParams");
        }

        @Override // androidx.work.Worker
        public ListenableWorker.a r() {
            OSFocusHandler.e.a();
            ListenableWorker.a c = ListenableWorker.a.c();
            fs1.e(c, "Result.success()");
            return c;
        }
    }

    /* compiled from: OSFocusHandler.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public final void a() {
            com.onesignal.a b = k7.b();
            if (b == null || b.d() == null) {
                OneSignal.C1(false);
            }
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "OSFocusHandler running onAppLostFocus");
            OSFocusHandler.c = true;
            OneSignal.a1();
            OSFocusHandler.d = true;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: OSFocusHandler.kt */
    /* loaded from: classes2.dex */
    public static final class b implements Runnable {
        public static final b a = new b();

        @Override // java.lang.Runnable
        public final void run() {
            OSFocusHandler.b = true;
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "OSFocusHandler setting stop state: true");
        }
    }

    public final h60 d() {
        h60 a2 = new h60.a().b(NetworkType.CONNECTED).a();
        fs1.e(a2, "Constraints.Builder()\n  …TED)\n            .build()");
        return a2;
    }

    public final void e(String str, Context context) {
        fs1.f(str, "tag");
        fs1.f(context, "context");
        gq4.f(context).a(str);
    }

    public final boolean f() {
        return c;
    }

    public final boolean g() {
        return d;
    }

    public final void h() {
        i();
        c = false;
    }

    public final void i() {
        b = false;
        Runnable runnable = this.a;
        if (runnable != null) {
            x0.b().a(runnable);
        }
    }

    public final void j() {
        h();
        OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "OSFocusHandler running onAppFocus");
        OneSignal.Y0();
    }

    public final void k(String str, long j, Context context) {
        fs1.f(str, "tag");
        fs1.f(context, "context");
        androidx.work.c b2 = new c.a(OnLostFocusWorker.class).e(d()).f(j, TimeUnit.MILLISECONDS).a(str).b();
        fs1.e(b2, "OneTimeWorkRequest.Build…tag)\n            .build()");
        gq4.f(context).d(str, ExistingWorkPolicy.KEEP, b2);
    }

    public final void l() {
        if (b) {
            b = false;
            this.a = null;
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "OSFocusHandler running onAppStartFocusLogic");
            OneSignal.b1();
            return;
        }
        i();
    }

    public final void m() {
        b bVar = b.a;
        x0.b().c(1500L, bVar);
        te4 te4Var = te4.a;
        this.a = bVar;
    }
}
