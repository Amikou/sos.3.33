package com.onesignal;

import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.onesignal.OneSignal;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NotificationSummaryManager.java */
/* loaded from: classes2.dex */
public class q {
    public static void a(Context context, a1 a1Var, String str) {
        Integer b = b(a1Var, str);
        boolean equals = str.equals(cn2.g());
        NotificationManager i = cn2.i(context);
        Integer h = cn2.h(a1Var, str, equals);
        if (h != null) {
            if (!OneSignal.P()) {
                OneSignal.o1(h.intValue());
                return;
            }
            if (equals) {
                b = Integer.valueOf(cn2.f());
            }
            if (b != null) {
                i.cancel(b.intValue());
            }
        }
    }

    public static Integer b(bn2 bn2Var, String str) {
        Integer num;
        Cursor c;
        Cursor cursor = null;
        try {
            c = bn2Var.c("notification", new String[]{"android_notification_id"}, "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 1", new String[]{str}, null, null, null);
        } catch (Throwable th) {
            th = th;
            num = null;
        }
        try {
            if (!c.moveToFirst()) {
                c.close();
                if (!c.isClosed()) {
                    c.close();
                }
                return null;
            }
            Integer valueOf = Integer.valueOf(c.getInt(c.getColumnIndex("android_notification_id")));
            c.close();
            if (c.isClosed()) {
                return valueOf;
            }
            c.close();
            return valueOf;
        } catch (Throwable th2) {
            th = th2;
            cursor = c;
            num = null;
            try {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error getting android notification id for summary notification group: " + str, th);
                return num;
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
    }

    public static Cursor c(Context context, bn2 bn2Var, String str, boolean z) {
        Long valueOf;
        String string;
        Cursor c = bn2Var.c("notification", new String[]{"android_notification_id", "created_time", "full_data"}, "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 0", new String[]{str}, null, null, "_id DESC");
        int count = c.getCount();
        if (count == 0) {
            c.close();
            Integer b = b(bn2Var, str);
            if (b == null) {
                return c;
            }
            cn2.i(context).cancel(b.intValue());
            ContentValues contentValues = new ContentValues();
            contentValues.put(z ? "dismissed" : "opened", (Integer) 1);
            bn2Var.a("notification", contentValues, "android_notification_id = " + b, null);
            return c;
        } else if (count == 1) {
            c.close();
            if (b(bn2Var, str) == null) {
                return c;
            }
            d(context, str);
            return c;
        } else {
            try {
                c.moveToFirst();
                valueOf = Long.valueOf(c.getLong(c.getColumnIndex("created_time")));
                string = c.getString(c.getColumnIndex("full_data"));
                c.close();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (b(bn2Var, str) == null) {
                return c;
            }
            zj2 zj2Var = new zj2(context);
            zj2Var.y(true);
            zj2Var.z(valueOf);
            zj2Var.q(new JSONObject(string));
            e.M(zj2Var);
            return c;
        }
    }

    public static void d(Context context, String str) {
        String[] strArr = {str};
        Cursor cursor = null;
        try {
            cursor = a1.g(context).c("notification", OSNotificationRestoreWorkManager.a, "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 0", strArr, null, null, null);
            OSNotificationRestoreWorkManager.e(context, cursor, 0);
            if (cursor == null || cursor.isClosed()) {
            }
        } catch (Throwable th) {
            try {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error restoring notification records! ", th);
                if (cursor == null) {
                    return;
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
    }

    public static void e(Context context, bn2 bn2Var, int i) {
        Cursor c = bn2Var.c("notification", new String[]{"group_id"}, "android_notification_id = " + i, null, null, null, null);
        if (c.moveToFirst()) {
            String string = c.getString(c.getColumnIndex("group_id"));
            c.close();
            if (string != null) {
                f(context, bn2Var, string, true);
                return;
            }
            return;
        }
        c.close();
    }

    public static void f(Context context, bn2 bn2Var, String str, boolean z) {
        try {
            Cursor c = c(context, bn2Var, str, z);
            if (c == null || c.isClosed()) {
                return;
            }
            c.close();
        } finally {
        }
    }
}
