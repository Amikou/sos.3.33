package com.onesignal;

import android.content.Context;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.NetworkType;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.b;
import androidx.work.c;
import com.onesignal.OneSignal;
import com.onesignal.d1;
import defpackage.h60;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class OSReceiveReceiptController {
    public static OSReceiveReceiptController d;
    public int a = 0;
    public int b = 25;
    public final s0 c = OneSignal.k0();

    /* loaded from: classes2.dex */
    public static class ReceiveReceiptWorker extends Worker {

        /* loaded from: classes2.dex */
        public class a extends d1.g {
            public final /* synthetic */ String a;

            public a(ReceiveReceiptWorker receiveReceiptWorker, String str) {
                this.a = str;
            }

            @Override // com.onesignal.d1.g
            public void a(int i, String str, Throwable th) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.a(log_level, "Receive receipt failed with statusCode: " + i + " response: " + str);
            }

            @Override // com.onesignal.d1.g
            public void b(String str) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.a(log_level, "Receive receipt sent for notificationID: " + this.a);
            }
        }

        public ReceiveReceiptWorker(Context context, WorkerParameters workerParameters) {
            super(context, workerParameters);
        }

        @Override // androidx.work.Worker
        public ListenableWorker.a r() {
            s(g().l("os_notification_id"));
            return ListenableWorker.a.c();
        }

        public void s(String str) {
            String str2 = OneSignal.g;
            String o0 = (str2 == null || str2.isEmpty()) ? OneSignal.o0() : OneSignal.g;
            String z0 = OneSignal.z0();
            Integer num = null;
            r0 r0Var = new r0();
            try {
                num = Integer.valueOf(new OSUtils().e());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            Integer num2 = num;
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "ReceiveReceiptWorker: Device Type is: " + num2);
            r0Var.a(o0, z0, num2, str, new a(this, str));
        }
    }

    public static synchronized OSReceiveReceiptController c() {
        OSReceiveReceiptController oSReceiveReceiptController;
        synchronized (OSReceiveReceiptController.class) {
            if (d == null) {
                d = new OSReceiveReceiptController();
            }
            oSReceiveReceiptController = d;
        }
        return oSReceiveReceiptController;
    }

    public void a(Context context, String str) {
        if (!this.c.j()) {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "sendReceiveReceipt disabled");
            return;
        }
        int j = OSUtils.j(this.a, this.b);
        androidx.work.b a = new b.a().h("os_notification_id", str).a();
        h60 b = b();
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "OSReceiveReceiptController enqueueing send receive receipt work with notificationId: " + str + " and delay: " + j + " seconds");
        gq4 f = gq4.f(context);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_receive_receipt");
        f.d(sb.toString(), ExistingWorkPolicy.KEEP, new c.a(ReceiveReceiptWorker.class).e(b).f((long) j, TimeUnit.SECONDS).g(a).b());
    }

    public h60 b() {
        return new h60.a().b(NetworkType.CONNECTED).a();
    }
}
