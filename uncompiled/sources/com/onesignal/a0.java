package com.onesignal;

import com.onesignal.OneSignal;

/* compiled from: OSInAppMessagePrompt.java */
/* loaded from: classes2.dex */
public abstract class a0 {
    public boolean a = false;

    public abstract String a();

    public abstract void b(OneSignal.c0 c0Var);

    public boolean c() {
        return this.a;
    }

    public void d(boolean z) {
        this.a = z;
    }

    public String toString() {
        return "OSInAppMessagePrompt{key=" + a() + " prompted=" + this.a + '}';
    }
}
