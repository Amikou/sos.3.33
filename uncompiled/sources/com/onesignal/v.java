package com.onesignal;

import com.onesignal.FocusTimeController;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: OSFocusTimeProcessorFactory.java */
/* loaded from: classes2.dex */
public class v {
    public final HashMap<String, FocusTimeController.b> a;

    public v() {
        HashMap<String, FocusTimeController.b> hashMap = new HashMap<>();
        this.a = hashMap;
        hashMap.put(FocusTimeController.c.class.getName(), new FocusTimeController.c());
        hashMap.put(FocusTimeController.a.class.getName(), new FocusTimeController.a());
    }

    public final FocusTimeController.b a() {
        return this.a.get(FocusTimeController.a.class.getName());
    }

    public FocusTimeController.b b() {
        FocusTimeController.b a = a();
        for (com.onesignal.influence.domain.a aVar : a.j()) {
            if (aVar.d().isAttributed()) {
                return a;
            }
        }
        return d();
    }

    public FocusTimeController.b c(List<com.onesignal.influence.domain.a> list) {
        boolean z;
        Iterator<com.onesignal.influence.domain.a> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            } else if (it.next().d().isAttributed()) {
                z = true;
                break;
            }
        }
        if (z) {
            return a();
        }
        return d();
    }

    public final FocusTimeController.b d() {
        return this.a.get(FocusTimeController.c.class.getName());
    }
}
