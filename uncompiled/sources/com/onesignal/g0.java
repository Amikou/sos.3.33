package com.onesignal;

import com.onesignal.OneSignal;
import defpackage.dh2;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: OSNotification.java */
/* loaded from: classes2.dex */
public class g0 {
    public int A;
    public dh2.f a;
    public List<g0> b;
    public int c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public JSONObject i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;
    public int q;
    public String r;
    public String s;
    public List<a> t;
    public String u;
    public b v;
    public String w;
    public int x;
    public String y;
    public long z;

    /* compiled from: OSNotification.java */
    /* loaded from: classes2.dex */
    public static class a {
        public String a;
        public String b;
        public String c;
    }

    /* compiled from: OSNotification.java */
    /* loaded from: classes2.dex */
    public static class b {
        public String a;
        public String b;
        public String c;
    }

    /* compiled from: OSNotification.java */
    /* loaded from: classes2.dex */
    public static class c {
        public int A;
        public dh2.f a;
        public List<g0> b;
        public int c;
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
        public JSONObject i;
        public String j;
        public String k;
        public String l;
        public String m;
        public String n;
        public String o;
        public String p;
        public int q = 1;
        public String r;
        public String s;
        public List<a> t;
        public String u;
        public b v;
        public String w;
        public int x;
        public String y;
        public long z;

        public c A(String str) {
            this.e = str;
            return this;
        }

        public c B(String str) {
            this.g = str;
            return this;
        }

        public g0 a() {
            g0 g0Var = new g0();
            g0Var.F(this.a);
            g0Var.A(this.b);
            g0Var.r(this.c);
            g0Var.G(this.d);
            g0Var.O(this.e);
            g0Var.N(this.f);
            g0Var.P(this.g);
            g0Var.v(this.h);
            g0Var.q(this.i);
            g0Var.K(this.j);
            g0Var.B(this.k);
            g0Var.u(this.l);
            g0Var.L(this.m);
            g0Var.C(this.n);
            g0Var.M(this.o);
            g0Var.D(this.p);
            g0Var.E(this.q);
            g0Var.y(this.r);
            g0Var.z(this.s);
            g0Var.p(this.t);
            g0Var.x(this.u);
            g0Var.s(this.v);
            g0Var.w(this.w);
            g0Var.H(this.x);
            g0Var.I(this.y);
            g0Var.J(this.z);
            g0Var.Q(this.A);
            return g0Var;
        }

        public c b(List<a> list) {
            this.t = list;
            return this;
        }

        public c c(JSONObject jSONObject) {
            this.i = jSONObject;
            return this;
        }

        public c d(int i) {
            this.c = i;
            return this;
        }

        public c e(b bVar) {
            this.v = bVar;
            return this;
        }

        public c f(String str) {
            this.l = str;
            return this;
        }

        public c g(String str) {
            this.h = str;
            return this;
        }

        public c h(String str) {
            this.w = str;
            return this;
        }

        public c i(String str) {
            this.u = str;
            return this;
        }

        public c j(String str) {
            this.r = str;
            return this;
        }

        public c k(String str) {
            this.s = str;
            return this;
        }

        public c l(List<g0> list) {
            this.b = list;
            return this;
        }

        public c m(String str) {
            this.k = str;
            return this;
        }

        public c n(String str) {
            this.n = str;
            return this;
        }

        public c o(String str) {
            this.p = str;
            return this;
        }

        public c p(int i) {
            this.q = i;
            return this;
        }

        public c q(dh2.f fVar) {
            this.a = fVar;
            return this;
        }

        public c r(String str) {
            this.d = str;
            return this;
        }

        public c s(int i) {
            this.x = i;
            return this;
        }

        public c t(String str) {
            this.y = str;
            return this;
        }

        public c u(long j) {
            this.z = j;
            return this;
        }

        public c v(String str) {
            this.j = str;
            return this;
        }

        public c w(String str) {
            this.m = str;
            return this;
        }

        public c x(String str) {
            this.o = str;
            return this;
        }

        public c y(int i) {
            this.A = i;
            return this;
        }

        public c z(String str) {
            this.f = str;
            return this;
        }
    }

    public g0() {
        this.q = 1;
    }

    public void A(List<g0> list) {
        this.b = list;
    }

    public void B(String str) {
        this.k = str;
    }

    public void C(String str) {
        this.n = str;
    }

    public void D(String str) {
        this.p = str;
    }

    public void E(int i) {
        this.q = i;
    }

    public void F(dh2.f fVar) {
        this.a = fVar;
    }

    public void G(String str) {
        this.d = str;
    }

    public void H(int i) {
        this.x = i;
    }

    public void I(String str) {
        this.y = str;
    }

    public final void J(long j) {
        this.z = j;
    }

    public void K(String str) {
        this.j = str;
    }

    public void L(String str) {
        this.m = str;
    }

    public void M(String str) {
        this.o = str;
    }

    public void N(String str) {
        this.f = str;
    }

    public void O(String str) {
        this.e = str;
    }

    public void P(String str) {
        this.g = str;
    }

    public final void Q(int i) {
        this.A = i;
    }

    public g0 c() {
        return new c().q(this.a).l(this.b).d(this.c).r(this.d).A(this.e).z(this.f).B(this.g).g(this.h).c(this.i).v(this.j).m(this.k).f(this.l).w(this.m).n(this.n).x(this.o).o(this.p).p(this.q).j(this.r).k(this.s).b(this.t).i(this.u).e(this.v).h(this.w).s(this.x).t(this.y).u(this.z).y(this.A).a();
    }

    public int d() {
        return this.c;
    }

    public String e() {
        return this.h;
    }

    public dh2.f f() {
        return this.a;
    }

    public String g() {
        return this.d;
    }

    public long h() {
        return this.z;
    }

    public String i() {
        return this.f;
    }

    public String j() {
        return this.e;
    }

    public String k() {
        return this.g;
    }

    public int l() {
        return this.A;
    }

    public boolean m() {
        return this.c != 0;
    }

    public final void n(JSONObject jSONObject) {
        try {
            JSONObject b2 = k.b(jSONObject);
            long a2 = OneSignal.w0().a();
            if (jSONObject.has("google.ttl")) {
                this.z = jSONObject.optLong("google.sent_time", a2) / 1000;
                this.A = jSONObject.optInt("google.ttl", 259200);
            } else if (jSONObject.has("hms.ttl")) {
                this.z = jSONObject.optLong("hms.sent_time", a2) / 1000;
                this.A = jSONObject.optInt("hms.ttl", 259200);
            } else {
                this.z = a2 / 1000;
                this.A = 259200;
            }
            this.d = b2.optString("i");
            this.f = b2.optString("ti");
            this.e = b2.optString("tn");
            this.y = jSONObject.toString();
            this.i = b2.optJSONObject("a");
            this.n = b2.optString("u", null);
            this.h = jSONObject.optString("alert", null);
            this.g = jSONObject.optString("title", null);
            this.j = jSONObject.optString("sicon", null);
            this.l = jSONObject.optString("bicon", null);
            this.k = jSONObject.optString("licon", null);
            this.o = jSONObject.optString("sound", null);
            this.r = jSONObject.optString("grp", null);
            this.s = jSONObject.optString("grp_msg", null);
            this.m = jSONObject.optString("bgac", null);
            this.p = jSONObject.optString("ledc", null);
            String optString = jSONObject.optString("vis", null);
            if (optString != null) {
                this.q = Integer.parseInt(optString);
            }
            this.u = jSONObject.optString("from", null);
            this.x = jSONObject.optInt("pri", 0);
            String optString2 = jSONObject.optString("collapse_key", null);
            if (!"do_not_collapse".equals(optString2)) {
                this.w = optString2;
            }
            try {
                o();
            } catch (Throwable th) {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error assigning OSNotificationReceivedEvent.actionButtons values!", th);
            }
            try {
                t(jSONObject);
            } catch (Throwable th2) {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error assigning OSNotificationReceivedEvent.backgroundImageLayout values!", th2);
            }
        } catch (Throwable th3) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error assigning OSNotificationReceivedEvent payload values!", th3);
        }
    }

    public final void o() throws Throwable {
        JSONObject jSONObject = this.i;
        if (jSONObject == null || !jSONObject.has("actionButtons")) {
            return;
        }
        JSONArray jSONArray = this.i.getJSONArray("actionButtons");
        this.t = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            a aVar = new a();
            aVar.a = jSONObject2.optString("id", null);
            aVar.b = jSONObject2.optString(PublicResolver.FUNC_TEXT, null);
            aVar.c = jSONObject2.optString("icon", null);
            this.t.add(aVar);
        }
        this.i.remove("actionId");
        this.i.remove("actionButtons");
    }

    public void p(List<a> list) {
        this.t = list;
    }

    public void q(JSONObject jSONObject) {
        this.i = jSONObject;
    }

    public void r(int i) {
        this.c = i;
    }

    public void s(b bVar) {
        this.v = bVar;
    }

    public final void t(JSONObject jSONObject) throws Throwable {
        String optString = jSONObject.optString("bg_img", null);
        if (optString != null) {
            JSONObject jSONObject2 = new JSONObject(optString);
            b bVar = new b();
            this.v = bVar;
            bVar.a = jSONObject2.optString("img");
            this.v.b = jSONObject2.optString("tc");
            this.v.c = jSONObject2.optString("bc");
        }
    }

    public String toString() {
        return "OSNotification{notificationExtender=" + this.a + ", groupedNotifications=" + this.b + ", androidNotificationId=" + this.c + ", notificationId='" + this.d + "', templateName='" + this.e + "', templateId='" + this.f + "', title='" + this.g + "', body='" + this.h + "', additionalData=" + this.i + ", smallIcon='" + this.j + "', largeIcon='" + this.k + "', bigPicture='" + this.l + "', smallIconAccentColor='" + this.m + "', launchURL='" + this.n + "', sound='" + this.o + "', ledColor='" + this.p + "', lockScreenVisibility=" + this.q + ", groupKey='" + this.r + "', groupMessage='" + this.s + "', actionButtons=" + this.t + ", fromProjectNumber='" + this.u + "', backgroundImageLayout=" + this.v + ", collapseId='" + this.w + "', priority=" + this.x + ", rawPayload='" + this.y + "'}";
    }

    public void u(String str) {
        this.l = str;
    }

    public void v(String str) {
        this.h = str;
    }

    public void w(String str) {
        this.w = str;
    }

    public void x(String str) {
        this.u = str;
    }

    public void y(String str) {
        this.r = str;
    }

    public void z(String str) {
        this.s = str;
    }

    public g0(JSONObject jSONObject) {
        this(null, jSONObject, 0);
    }

    public g0(List<g0> list, JSONObject jSONObject, int i) {
        this.q = 1;
        n(jSONObject);
        this.b = list;
        this.c = i;
    }
}
