package com.onesignal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.onesignal.k;

/* loaded from: classes2.dex */
public class FCMIntentJobService extends JobIntentService {

    /* loaded from: classes2.dex */
    public class a implements k.e {
        public a(FCMIntentJobService fCMIntentJobService) {
        }

        @Override // com.onesignal.k.e
        public void a(k.f fVar) {
        }
    }

    public static void j(Context context, Intent intent) {
        JobIntentService.d(context, FCMIntentJobService.class, 123890, intent, false);
    }

    @Override // com.onesignal.JobIntentService
    public void g(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return;
        }
        OneSignal.L0(this);
        k.h(this, extras, new a(this));
    }
}
