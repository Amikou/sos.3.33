package com.onesignal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.amazon.device.messaging.ADMMessageHandlerJobBase;
import com.onesignal.OneSignal;
import com.onesignal.k;
import org.json.JSONObject;

/* compiled from: ADMMessageHandlerJob.kt */
/* loaded from: classes2.dex */
public final class ADMMessageHandlerJob extends ADMMessageHandlerJobBase {

    /* compiled from: ADMMessageHandlerJob.kt */
    /* loaded from: classes2.dex */
    public static final class a implements k.e {
        public final /* synthetic */ Bundle a;
        public final /* synthetic */ Context b;

        public a(Bundle bundle, Context context) {
            this.a = bundle;
            this.b = context;
        }

        @Override // com.onesignal.k.e
        public void a(k.f fVar) {
            if (fVar == null || !fVar.c()) {
                JSONObject a = k.a(this.a);
                fs1.e(a, "NotificationBundleProces…undleAsJSONObject(bundle)");
                g0 g0Var = new g0(a);
                zj2 zj2Var = new zj2(this.b);
                zj2Var.q(a);
                zj2Var.o(this.b);
                zj2Var.r(g0Var);
                k.k(zj2Var, true);
            }
        }
    }

    public void onMessage(Context context, Intent intent) {
        Bundle extras = intent != null ? intent.getExtras() : null;
        k.h(context, extras, new a(extras, context));
    }

    public void onRegistered(Context context, String str) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
        OneSignal.a(log_level, "ADM registration ID: " + str);
        g1.c(str);
    }

    public void onRegistrationError(Context context, String str) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
        OneSignal.a(log_level, "ADM:onRegistrationError: " + str);
        if (fs1.b("INVALID_SENDER", str)) {
            OneSignal.a(log_level, "Please double check that you have a matching package name (NOTE: Case Sensitive), api_key.txt, and the apk was signed with the same Keystore and Alias.");
        }
        g1.c(null);
    }

    public void onUnregistered(Context context, String str) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
        OneSignal.a(log_level, "ADM:onUnregistered: " + str);
    }
}
