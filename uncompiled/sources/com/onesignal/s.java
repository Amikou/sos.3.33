package com.onesignal;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.onesignal.OneSignal;

/* compiled from: OSBackgroundSync.java */
/* loaded from: classes2.dex */
public abstract class s {
    public static final Object c = new Object();
    public boolean a = false;
    public Thread b;

    public static boolean n() {
        return Build.VERSION.SDK_INT >= 21;
    }

    public void a(Context context) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, getClass().getSimpleName() + " cancel background sync");
        synchronized (c) {
            if (n()) {
                ((JobScheduler) context.getSystemService("jobscheduler")).cancel(e());
            } else {
                ((AlarmManager) context.getSystemService("alarm")).cancel(m(context));
            }
        }
    }

    public void b(Context context, Runnable runnable) {
        OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "OSBackground sync, calling initWithContext");
        OneSignal.L0(context);
        Thread thread = new Thread(runnable, f());
        this.b = thread;
        thread.start();
    }

    public abstract Class c();

    public abstract Class d();

    public abstract int e();

    public abstract String f();

    public final boolean g(Context context) {
        return ed.a(context, "android.permission.RECEIVE_BOOT_COMPLETED") == 0;
    }

    public final boolean h(Context context) {
        Thread thread;
        for (JobInfo jobInfo : ((JobScheduler) context.getSystemService("jobscheduler")).getAllPendingJobs()) {
            if (jobInfo.getId() == e() && (thread = this.b) != null && thread.isAlive()) {
                return true;
            }
        }
        return false;
    }

    public void i(Context context, long j) {
        synchronized (c) {
            if (n()) {
                k(context, j);
            } else {
                j(context, j);
            }
        }
    }

    public final void j(Context context, long j) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.VERBOSE;
        OneSignal.a(log_level, getClass().getSimpleName() + " scheduleServiceSyncTask:atTime: " + j);
        ((AlarmManager) context.getSystemService("alarm")).set(0, OneSignal.w0().a() + j, m(context));
    }

    public final void k(Context context, long j) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.VERBOSE;
        OneSignal.a(log_level, "OSBackgroundSync scheduleSyncServiceAsJob:atTime: " + j);
        if (h(context)) {
            OneSignal.a(log_level, "OSBackgroundSync scheduleSyncServiceAsJob Scheduler already running!");
            this.a = true;
            return;
        }
        JobInfo.Builder builder = new JobInfo.Builder(e(), new ComponentName(context, c()));
        builder.setMinimumLatency(j).setRequiredNetworkType(1);
        if (g(context)) {
            builder.setPersisted(true);
        }
        try {
            int schedule = ((JobScheduler) context.getSystemService("jobscheduler")).schedule(builder.build());
            OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.INFO;
            OneSignal.a(log_level2, "OSBackgroundSync scheduleSyncServiceAsJob:result: " + schedule);
        } catch (NullPointerException e) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "scheduleSyncServiceAsJob called JobScheduler.jobScheduler which triggered an internal null Android error. Skipping job.", e);
        }
    }

    public boolean l() {
        Thread thread = this.b;
        if (thread != null && thread.isAlive()) {
            this.b.interrupt();
            return true;
        }
        return false;
    }

    public final PendingIntent m(Context context) {
        return PendingIntent.getService(context, e(), new Intent(context, d()), 201326592);
    }
}
