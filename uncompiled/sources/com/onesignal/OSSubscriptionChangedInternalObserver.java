package com.onesignal;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class OSSubscriptionChangedInternalObserver {
    public static void a(OSSubscriptionState oSSubscriptionState) {
        if (OneSignal.u0().c(new xk2(OneSignal.g0, (OSSubscriptionState) oSSubscriptionState.clone()))) {
            OSSubscriptionState oSSubscriptionState2 = (OSSubscriptionState) oSSubscriptionState.clone();
            OneSignal.g0 = oSSubscriptionState2;
            oSSubscriptionState2.e();
        }
    }

    public void changed(OSSubscriptionState oSSubscriptionState) {
        a(oSSubscriptionState);
    }
}
