package com.onesignal;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.onesignal.OneSignal;
import com.onesignal.a;
import java.lang.ref.WeakReference;
import java.util.List;

/* compiled from: OSSystemConditionController.java */
/* loaded from: classes2.dex */
public class v0 {
    public static final String b = "com.onesignal.v0";
    public final c a;

    /* compiled from: OSSystemConditionController.java */
    /* loaded from: classes2.dex */
    public class a extends FragmentManager.l {
        public final /* synthetic */ FragmentManager a;

        public a(FragmentManager fragmentManager) {
            this.a = fragmentManager;
        }

        @Override // androidx.fragment.app.FragmentManager.l
        public void e(FragmentManager fragmentManager, Fragment fragment) {
            super.e(fragmentManager, fragment);
            if (fragment instanceof sn0) {
                this.a.v1(this);
                v0.this.a.c();
            }
        }
    }

    /* compiled from: OSSystemConditionController.java */
    /* loaded from: classes2.dex */
    public interface b {
        void a(String str, a.c cVar);
    }

    /* compiled from: OSSystemConditionController.java */
    /* loaded from: classes2.dex */
    public interface c {
        void c();
    }

    public v0(c cVar) {
        this.a = cVar;
    }

    public boolean b(Context context) throws NoClassDefFoundError {
        if (context instanceof AppCompatActivity) {
            FragmentManager supportFragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
            supportFragmentManager.f1(new a(supportFragmentManager), true);
            List<Fragment> u0 = supportFragmentManager.u0();
            int size = u0.size();
            if (size > 0) {
                Fragment fragment = u0.get(size - 1);
                return fragment.isVisible() && (fragment instanceof sn0);
            }
            return false;
        }
        return false;
    }

    public boolean c() {
        if (OneSignal.Q() == null) {
            OneSignal.d1(OneSignal.LOG_LEVEL.WARN, "OSSystemConditionObserver curActivity null");
            return false;
        }
        try {
            if (b(OneSignal.Q())) {
                OneSignal.d1(OneSignal.LOG_LEVEL.WARN, "OSSystemConditionObserver dialog fragment detected");
                return false;
            }
        } catch (NoClassDefFoundError e) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
            OneSignal.d1(log_level, "AppCompatActivity is not used in this app, skipping 'isDialogFragmentShowing' check: " + e);
        }
        com.onesignal.a b2 = k7.b();
        boolean l = z0.l(new WeakReference(OneSignal.Q()));
        if (l && b2 != null) {
            b2.c(b, this.a);
            OneSignal.d1(OneSignal.LOG_LEVEL.WARN, "OSSystemConditionObserver keyboard up detected");
        }
        return !l;
    }
}
