package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.d1;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSReceiveReceiptRepository.java */
/* loaded from: classes2.dex */
public class r0 {
    public void a(String str, String str2, Integer num, String str3, d1.g gVar) {
        try {
            JSONObject put = new JSONObject().put("app_id", str).put("player_id", str2);
            if (num != null) {
                put.put("device_type", num);
            }
            d1.l("notifications/" + str3 + "/report_received", put, gVar);
        } catch (JSONException e) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Generating direct receive receipt:JSON Failed.", e);
        }
    }
}
