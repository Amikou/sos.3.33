package com.onesignal;

import org.json.JSONObject;

/* compiled from: OSPermissionState.java */
/* loaded from: classes2.dex */
public class q0 implements Cloneable {
    public n0<Object, q0> a = new n0<>("changed", false);
    public boolean f0;

    public q0(boolean z) {
        if (z) {
            this.f0 = b1.b(b1.a, "ONESIGNAL_ACCEPTED_NOTIFICATION_LAST", false);
        } else {
            e();
        }
    }

    public boolean a() {
        return this.f0;
    }

    public n0<Object, q0> b() {
        return this.a;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (Throwable unused) {
            return null;
        }
    }

    public void d() {
        b1.j(b1.a, "ONESIGNAL_ACCEPTED_NOTIFICATION_LAST", this.f0);
    }

    public void e() {
        f(OSUtils.a(OneSignal.e));
    }

    public final void f(boolean z) {
        boolean z2 = this.f0 != z;
        this.f0 = z;
        if (z2) {
            this.a.c(this);
        }
    }

    public JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("areNotificationsEnabled", this.f0);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return g().toString();
    }
}
