package com.onesignal;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import androidx.recyclerview.widget.RecyclerView;
import com.onesignal.OneSignal;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: NotificationLimitManager.java */
/* loaded from: classes2.dex */
public class m {
    public static final String a = Integer.toString(49);

    public static void a(Context context, int i) {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                c(context, i);
            } else {
                b(context, i);
            }
        } catch (Throwable unused) {
            b(context, i);
        }
    }

    public static void b(Context context, int i) {
        boolean isClosed;
        int count;
        a1 g = a1.g(context);
        Cursor cursor = null;
        try {
            cursor = g.b("notification", new String[]{"android_notification_id"}, a1.m().toString(), null, null, null, "_id", e() + i);
            count = (cursor.getCount() - d()) + i;
        } catch (Throwable th) {
            try {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error clearing oldest notifications over limit! ", th);
                if (cursor == null) {
                    return;
                }
                if (isClosed) {
                    return;
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
        if (count < 1) {
            if (cursor.isClosed()) {
                return;
            }
            cursor.close();
            return;
        }
        while (cursor.moveToNext()) {
            OneSignal.o1(cursor.getInt(cursor.getColumnIndex("android_notification_id")));
            count--;
            if (count <= 0) {
                break;
            }
        }
        if (cursor.isClosed()) {
        }
    }

    public static void c(Context context, int i) throws Throwable {
        StatusBarNotification[] d = cn2.d(context);
        int length = (d.length - d()) + i;
        if (length < 1) {
            return;
        }
        TreeMap treeMap = new TreeMap();
        for (StatusBarNotification statusBarNotification : d) {
            if (!f(statusBarNotification)) {
                treeMap.put(Long.valueOf(statusBarNotification.getNotification().when), Integer.valueOf(statusBarNotification.getId()));
            }
        }
        for (Map.Entry entry : treeMap.entrySet()) {
            OneSignal.o1(((Integer) entry.getValue()).intValue());
            length--;
            if (length <= 0) {
                return;
            }
        }
    }

    public static int d() {
        return 49;
    }

    public static String e() {
        return a;
    }

    public static boolean f(StatusBarNotification statusBarNotification) {
        return (statusBarNotification.getNotification().flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0;
    }
}
