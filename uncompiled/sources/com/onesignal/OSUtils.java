package com.onesignal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import androidx.annotation.Keep;
import com.google.android.gms.location.LocationListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.huawei.agconnect.config.AGConnectServicesConfig;
import com.huawei.hms.aaid.HmsInstanceId;
import com.huawei.hms.api.HuaweiApiAvailability;
import com.huawei.hms.location.LocationCallback;
import com.onesignal.OneSignal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.abi.datatypes.Utf8String;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class OSUtils {
    public static int a = 3;
    public static final int[] b = {401, 402, 403, 404, 410};

    /* loaded from: classes2.dex */
    public enum SchemaType {
        DATA("data"),
        HTTPS("https"),
        HTTP("http");
        
        private final String text;

        SchemaType(String str) {
            this.text = str;
        }

        public static SchemaType fromString(String str) {
            SchemaType[] values;
            for (SchemaType schemaType : values()) {
                if (schemaType.text.equalsIgnoreCase(str)) {
                    return schemaType;
                }
            }
            return null;
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[SchemaType.values().length];
            a = iArr;
            try {
                iArr[SchemaType.DATA.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[SchemaType.HTTPS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[SchemaType.HTTP.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public static boolean B() {
        return new OSUtils().e() == 1;
    }

    public static boolean C() {
        return new OSUtils().e() == 2;
    }

    public static boolean D() {
        return P("com.google.android.gms");
    }

    public static boolean E() {
        return HuaweiApiAvailability.getInstance().isHuaweiMobileServicesAvailable(OneSignal.e) == 0;
    }

    public static boolean F() {
        return P("com.huawei.hwid");
    }

    public static boolean G() {
        return new OSUtils().e() == 13;
    }

    public static boolean H() {
        return Thread.currentThread().equals(Looper.getMainLooper().getThread());
    }

    public static boolean I(String str) {
        return !TextUtils.isEmpty(str);
    }

    public static boolean J(String str) {
        return (str == null || str.matches("^[0-9]")) ? false : true;
    }

    public static <T> Set<T> K() {
        return Collections.newSetFromMap(new ConcurrentHashMap());
    }

    public static Set<String> L(JSONArray jSONArray) throws JSONException {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < jSONArray.length(); i++) {
            hashSet.add(jSONArray.getString(i));
        }
        return hashSet;
    }

    public static void M(Uri uri) {
        OneSignal.e.startActivity(O(uri));
    }

    public static void N(String str) {
        M(Uri.parse(str.trim()));
    }

    public static Intent O(Uri uri) {
        Intent makeMainSelectorActivity;
        SchemaType fromString = uri.getScheme() != null ? SchemaType.fromString(uri.getScheme()) : null;
        if (fromString == null) {
            fromString = SchemaType.HTTP;
            if (!uri.toString().contains("://")) {
                uri = Uri.parse("http://" + uri.toString());
            }
        }
        if (a.a[fromString.ordinal()] != 1) {
            makeMainSelectorActivity = new Intent("android.intent.action.VIEW", uri);
        } else {
            makeMainSelectorActivity = Intent.makeMainSelectorActivity("android.intent.action.MAIN", "android.intent.category.APP_BROWSER");
            makeMainSelectorActivity.setData(uri);
        }
        makeMainSelectorActivity.addFlags(268435456);
        return makeMainSelectorActivity;
    }

    public static boolean P(String str) {
        try {
            return OneSignal.e.getPackageManager().getPackageInfo(str, 128).applicationInfo.enabled;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static long[] Q(JSONObject jSONObject) {
        JSONArray jSONArray;
        try {
            Object opt = jSONObject.opt("vib_pt");
            if (opt instanceof String) {
                jSONArray = new JSONArray((String) opt);
            } else {
                jSONArray = (JSONArray) opt;
            }
            long[] jArr = new long[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                jArr[i] = jSONArray.optLong(i);
            }
            return jArr;
        } catch (JSONException unused) {
            return null;
        }
    }

    public static void R(Runnable runnable, int i) {
        new Handler(Looper.getMainLooper()).postDelayed(runnable, i);
    }

    public static void S(Runnable runnable) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            runnable.run();
        } else {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    public static boolean T(String str) {
        if (str != null) {
            return false;
        }
        OneSignal.a(OneSignal.LOG_LEVEL.INFO, "OneSignal was not initialized, ensure to always initialize OneSignal from the onCreate of your Application class.");
        return true;
    }

    public static boolean U(int i) {
        for (int i2 : b) {
            if (i == i2) {
                return false;
            }
        }
        return true;
    }

    public static void V(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static boolean a(Context context) {
        try {
            return androidx.core.app.c.d(OneSignal.e).a();
        } catch (Throwable unused) {
            return true;
        }
    }

    public static String f(Context context, String str) {
        Bundle h = h(context);
        if (h != null) {
            return h.getString(str);
        }
        return null;
    }

    public static boolean g(Context context, String str) {
        Bundle h = h(context);
        if (h != null) {
            return h.getBoolean(str);
        }
        return false;
    }

    public static Bundle h(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Manifest application info not found", e);
            return null;
        }
    }

    public static int j(int i, int i2) {
        return new Random().nextInt((i2 + 1) - i) + i;
    }

    public static String k(Context context, String str, String str2) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier(str, Utf8String.TYPE_NAME, context.getPackageName());
        return identifier != 0 ? resources.getString(identifier) : str2;
    }

    public static String l(Throwable th) {
        return m(th).getMessage();
    }

    public static Throwable m(Throwable th) {
        while (th.getCause() != null && th.getCause() != th) {
            th = th.getCause();
        }
        return th;
    }

    public static Uri n(Context context, String str) {
        int identifier;
        Resources resources = context.getResources();
        String packageName = context.getPackageName();
        if (J(str) && (identifier = resources.getIdentifier(str, "raw", packageName)) != 0) {
            return Uri.parse("android.resource://" + packageName + "/" + identifier);
        }
        int identifier2 = resources.getIdentifier("onesignal_default_sound", "raw", packageName);
        if (identifier2 != 0) {
            return Uri.parse("android.resource://" + packageName + "/" + identifier2);
        }
        return null;
    }

    public static int o(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 15;
        }
    }

    @Keep
    private static boolean opaqueHasClass(Class<?> cls) {
        return true;
    }

    public static boolean p() {
        return t() && w();
    }

    public static boolean q(Activity activity, int i) {
        try {
            return (activity.getPackageManager().getActivityInfo(activity.getComponentName(), 0).configChanges & i) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean r() {
        try {
            return opaqueHasClass(FirebaseMessaging.class);
        } catch (NoClassDefFoundError unused) {
            return false;
        }
    }

    public static boolean s() {
        try {
            return opaqueHasClass(LocationListener.class);
        } catch (NoClassDefFoundError unused) {
            return false;
        }
    }

    public static boolean t() {
        try {
            return opaqueHasClass(AGConnectServicesConfig.class);
        } catch (NoClassDefFoundError unused) {
            return false;
        }
    }

    public static boolean u() {
        try {
            return opaqueHasClass(HuaweiApiAvailability.class);
        } catch (NoClassDefFoundError unused) {
            return false;
        }
    }

    public static boolean v() {
        try {
            return opaqueHasClass(LocationCallback.class);
        } catch (NoClassDefFoundError unused) {
            return false;
        }
    }

    public static boolean w() {
        try {
            return opaqueHasClass(HmsInstanceId.class);
        } catch (NoClassDefFoundError unused) {
            return false;
        }
    }

    public static boolean x() {
        return true;
    }

    public static boolean y() {
        return true;
    }

    public static boolean z() {
        return true;
    }

    public int A(Context context, String str) {
        Integer c;
        int e = e();
        try {
            UUID.fromString(str);
            if ("b2f7f966-d8cc-11e4-bed1-df8f05be55ba".equals(str) || "5eb5a37e-b458-11e3-ac11-000c2940e62c".equals(str)) {
                OneSignal.a(OneSignal.LOG_LEVEL.ERROR, "OneSignal Example AppID detected, please update to your app's id found on OneSignal.com");
            }
            int i = 1;
            if (e == 1 && (c = c()) != null) {
                i = c.intValue();
            }
            Integer b2 = b(context);
            return b2 != null ? b2.intValue() : i;
        } catch (Throwable th) {
            OneSignal.b(OneSignal.LOG_LEVEL.FATAL, "OneSignal AppId format is invalid.\nExample: 'b2f7f966-d8cc-11e4-bed1-df8f05be55ba'\n", th);
            return -999;
        }
    }

    public final boolean W() {
        try {
            Class.forName("com.amazon.device.messaging.ADM");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    public final boolean X() {
        if (r()) {
            return D();
        }
        return false;
    }

    public final boolean Y() {
        if (u() && p()) {
            return E();
        }
        return false;
    }

    public final Integer b(Context context) {
        boolean z = z();
        boolean y = y();
        if (!z && !y) {
            OneSignal.a(OneSignal.LOG_LEVEL.FATAL, "Could not find the Android Support Library. Please make sure it has been correctly added to your project.");
            return -3;
        } else if (z && y) {
            if (Build.VERSION.SDK_INT < 26 || o(context) < 26 || x()) {
                return null;
            }
            OneSignal.a(OneSignal.LOG_LEVEL.FATAL, "The included Android Support Library is to old or incomplete. Please update to the 26.0.0 revision or newer.");
            return -5;
        } else {
            OneSignal.a(OneSignal.LOG_LEVEL.FATAL, "The included Android Support Library is to old or incomplete. Please update to the 26.0.0 revision or newer.");
            return -5;
        }
    }

    public Integer c() {
        if (r()) {
            return null;
        }
        OneSignal.a(OneSignal.LOG_LEVEL.FATAL, "The Firebase FCM library is missing! Please make sure to include it in your project.");
        return -4;
    }

    public String d() {
        try {
            String networkOperatorName = ((TelephonyManager) OneSignal.e.getSystemService("phone")).getNetworkOperatorName();
            if ("".equals(networkOperatorName)) {
                return null;
            }
            return networkOperatorName;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public int e() {
        if (W()) {
            return 2;
        }
        if (X()) {
            return 1;
        }
        if (Y()) {
            return 13;
        }
        return (!D() && F()) ? 13 : 1;
    }

    public Integer i() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) OneSignal.e.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            int type = activeNetworkInfo.getType();
            if (type != 1 && type != 9) {
                return 1;
            }
            return 0;
        }
        return null;
    }
}
