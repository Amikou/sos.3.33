package com.onesignal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import com.onesignal.OneSignal;
import java.util.HashMap;
import java.util.Set;

/* compiled from: OneSignalPrefs.java */
/* loaded from: classes2.dex */
public class b1 {
    public static final String a = OneSignal.class.getSimpleName();
    public static HashMap<String, HashMap<String, Object>> b;
    public static a c;

    /* compiled from: OneSignalPrefs.java */
    /* loaded from: classes2.dex */
    public static class a extends HandlerThread {
        public Handler a;
        public long f0;
        public boolean g0;

        /* compiled from: OneSignalPrefs.java */
        /* renamed from: com.onesignal.b1$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class RunnableC0155a implements Runnable {
            public RunnableC0155a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                a.this.c();
            }
        }

        public a(String str) {
            super(str);
            this.f0 = 0L;
        }

        public final void c() {
            for (String str : b1.b.keySet()) {
                SharedPreferences.Editor edit = b1.e(str).edit();
                HashMap<String, Object> hashMap = b1.b.get(str);
                synchronized (hashMap) {
                    for (String str2 : hashMap.keySet()) {
                        Object obj = hashMap.get(str2);
                        if (obj instanceof String) {
                            edit.putString(str2, (String) obj);
                        } else if (obj instanceof Boolean) {
                            edit.putBoolean(str2, ((Boolean) obj).booleanValue());
                        } else if (obj instanceof Integer) {
                            edit.putInt(str2, ((Integer) obj).intValue());
                        } else if (obj instanceof Long) {
                            edit.putLong(str2, ((Long) obj).longValue());
                        } else if (obj instanceof Set) {
                            edit.putStringSet(str2, (Set) obj);
                        } else if (obj == null) {
                            edit.remove(str2);
                        }
                    }
                    hashMap.clear();
                }
                edit.apply();
            }
            this.f0 = OneSignal.w0().a();
        }

        public final synchronized void d() {
            Handler handler = this.a;
            if (handler == null) {
                return;
            }
            handler.removeCallbacksAndMessages(null);
            if (this.f0 == 0) {
                this.f0 = OneSignal.w0().a();
            }
            RunnableC0155a runnableC0155a = new RunnableC0155a();
            this.a.postDelayed(runnableC0155a, (this.f0 - OneSignal.w0().a()) + 200);
        }

        public final synchronized void e() {
            if (OneSignal.e == null) {
                return;
            }
            f();
            d();
        }

        public final void f() {
            if (this.g0) {
                return;
            }
            start();
            this.g0 = true;
        }

        @Override // android.os.HandlerThread
        public void onLooperPrepared() {
            super.onLooperPrepared();
            this.a = new Handler(getLooper());
            d();
        }
    }

    static {
        h();
    }

    public static Object a(String str, String str2, Class cls, Object obj) {
        HashMap<String, Object> hashMap = b.get(str);
        synchronized (hashMap) {
            if (cls.equals(Object.class) && hashMap.containsKey(str2)) {
                return Boolean.TRUE;
            }
            Object obj2 = hashMap.get(str2);
            if (obj2 == null && !hashMap.containsKey(str2)) {
                SharedPreferences e = e(str);
                if (e != null) {
                    if (cls.equals(String.class)) {
                        return e.getString(str2, (String) obj);
                    }
                    if (cls.equals(Boolean.class)) {
                        return Boolean.valueOf(e.getBoolean(str2, ((Boolean) obj).booleanValue()));
                    }
                    if (cls.equals(Integer.class)) {
                        return Integer.valueOf(e.getInt(str2, ((Integer) obj).intValue()));
                    }
                    if (cls.equals(Long.class)) {
                        return Long.valueOf(e.getLong(str2, ((Long) obj).longValue()));
                    }
                    if (cls.equals(Set.class)) {
                        return e.getStringSet(str2, (Set) obj);
                    }
                    if (cls.equals(Object.class)) {
                        return Boolean.valueOf(e.contains(str2));
                    }
                    return null;
                }
                return obj;
            }
            return obj2;
        }
    }

    public static boolean b(String str, String str2, boolean z) {
        return ((Boolean) a(str, str2, Boolean.class, Boolean.valueOf(z))).booleanValue();
    }

    public static int c(String str, String str2, int i) {
        return ((Integer) a(str, str2, Integer.class, Integer.valueOf(i))).intValue();
    }

    public static long d(String str, String str2, long j) {
        return ((Long) a(str, str2, Long.class, Long.valueOf(j))).longValue();
    }

    public static synchronized SharedPreferences e(String str) {
        synchronized (b1.class) {
            Context context = OneSignal.e;
            if (context == null) {
                OneSignal.b(OneSignal.LOG_LEVEL.WARN, "OneSignal.appContext null, could not read " + str + " from getSharedPreferences.", new Throwable());
                return null;
            }
            return context.getSharedPreferences(str, 0);
        }
    }

    public static String f(String str, String str2, String str3) {
        return (String) a(str, str2, String.class, str3);
    }

    public static Set<String> g(String str, String str2, Set<String> set) {
        return (Set) a(str, str2, Set.class, set);
    }

    public static void h() {
        HashMap<String, HashMap<String, Object>> hashMap = new HashMap<>();
        b = hashMap;
        hashMap.put(a, new HashMap<>());
        b.put("GTPlayerPurchases", new HashMap<>());
        b.put("OneSignalTriggers", new HashMap<>());
        c = new a("OSH_WritePrefs");
    }

    public static void i(String str, String str2, Object obj) {
        HashMap<String, Object> hashMap = b.get(str);
        synchronized (hashMap) {
            hashMap.put(str2, obj);
        }
        o();
    }

    public static void j(String str, String str2, boolean z) {
        i(str, str2, Boolean.valueOf(z));
    }

    public static void k(String str, String str2, int i) {
        i(str, str2, Integer.valueOf(i));
    }

    public static void l(String str, String str2, long j) {
        i(str, str2, Long.valueOf(j));
    }

    public static void m(String str, String str2, String str3) {
        i(str, str2, str3);
    }

    public static void n(String str, String str2, Set<String> set) {
        i(str, str2, set);
    }

    public static void o() {
        c.e();
    }
}
