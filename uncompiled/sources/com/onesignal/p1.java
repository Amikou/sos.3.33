package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.OneSignalStateSynchronizer;
import com.onesignal.d1;
import com.onesignal.s1;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UserStatePushSynchronizer.java */
/* loaded from: classes2.dex */
public class p1 extends s1 {
    public static boolean m;

    /* compiled from: UserStatePushSynchronizer.java */
    /* loaded from: classes2.dex */
    public class a extends d1.g {
        public a() {
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            boolean unused = p1.m = true;
            try {
                JSONObject jSONObject = new JSONObject((str == null || str.isEmpty()) ? "{}" : "{}");
                if (jSONObject.has("tags")) {
                    synchronized (p1.this.a) {
                        p1 p1Var = p1.this;
                        JSONObject y = p1Var.y(p1Var.A().l().e("tags"), p1.this.G().l().e("tags"), null, null);
                        p1.this.A().t("tags", jSONObject.optJSONObject("tags"));
                        p1.this.A().q();
                        p1.this.G().o(jSONObject, y);
                        p1.this.G().q();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public p1() {
        super(OneSignalStateSynchronizer.UserStateSynchronizerType.PUSH);
    }

    @Override // com.onesignal.s1
    public String B() {
        return OneSignal.z0();
    }

    @Override // com.onesignal.s1
    public OneSignal.LOG_LEVEL C() {
        return OneSignal.LOG_LEVEL.ERROR;
    }

    @Override // com.onesignal.s1
    public n1 O(String str, boolean z) {
        return new bg4(str, z);
    }

    @Override // com.onesignal.s1
    public void P(JSONObject jSONObject) {
    }

    @Override // com.onesignal.s1
    public void U() {
        D(0).c();
    }

    @Override // com.onesignal.s1
    public void d0(String str) {
        OneSignal.S1(str);
    }

    public String g0() {
        return G().i().g("language", null);
    }

    public s1.e h0(boolean z) {
        s1.e eVar;
        if (z) {
            String z0 = OneSignal.z0();
            String o0 = OneSignal.o0();
            d1.f("players/" + z0 + "?app_id=" + o0, new a(), "CACHE_KEY_GET_TAGS");
        }
        synchronized (this.a) {
            eVar = new s1.e(m, j.c(G().l(), "tags"));
        }
        return eVar;
    }

    public boolean i0() {
        return G().i().c("userSubscribePref", true);
    }

    public void j0(String str) {
        OneSignal.v1(str);
    }

    public void k0(boolean z) {
        try {
            H().s("androidPermission", Boolean.valueOf(z));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void l0(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.putOpt("identifier", jSONObject.optString("identifier", null));
            if (jSONObject.has("device_type")) {
                jSONObject2.put("device_type", jSONObject.optInt("device_type"));
            }
            jSONObject2.putOpt("parent_player_id", jSONObject.optString("parent_player_id", null));
            H().h(jSONObject2, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jSONObject3 = new JSONObject();
            if (jSONObject.has("subscribableStatus")) {
                jSONObject3.put("subscribableStatus", jSONObject.optInt("subscribableStatus"));
            }
            if (jSONObject.has("androidPermission")) {
                jSONObject3.put("androidPermission", jSONObject.optBoolean("androidPermission"));
            }
            H().g(jSONObject3, null);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    @Override // com.onesignal.s1
    public void n(JSONObject jSONObject) {
    }

    @Override // com.onesignal.s1
    public void w(JSONObject jSONObject) {
        if (jSONObject.has("email")) {
            OneSignal.H();
        }
        if (jSONObject.has("sms_number")) {
            OneSignal.L();
        }
    }
}
