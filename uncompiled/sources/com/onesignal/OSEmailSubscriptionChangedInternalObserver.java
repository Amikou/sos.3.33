package com.onesignal;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class OSEmailSubscriptionChangedInternalObserver {
    public static void a(nj2 nj2Var) {
        if (OneSignal.Z().c(new oj2(OneSignal.j0, (nj2) nj2Var.clone()))) {
            nj2 nj2Var2 = (nj2) nj2Var.clone();
            OneSignal.j0 = nj2Var2;
            nj2Var2.d();
        }
    }

    public void changed(nj2 nj2Var) {
        a(nj2Var);
    }
}
