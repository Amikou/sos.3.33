package com.onesignal;

import android.location.Location;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hms.location.FusedLocationProviderClient;
import com.huawei.hms.location.LocationCallback;
import com.huawei.hms.location.LocationRequest;
import com.huawei.hms.location.LocationServices;
import com.onesignal.OneSignal;

/* compiled from: HMSLocationController.java */
/* loaded from: classes2.dex */
public class h extends LocationController {
    public static FusedLocationProviderClient j;
    public static c k;

    /* compiled from: HMSLocationController.java */
    /* loaded from: classes2.dex */
    public class a implements OnFailureListener {
    }

    /* compiled from: HMSLocationController.java */
    /* loaded from: classes2.dex */
    public class b implements OnSuccessListener<Location> {
    }

    /* compiled from: HMSLocationController.java */
    /* loaded from: classes2.dex */
    public static class c extends LocationCallback {
        public FusedLocationProviderClient a;

        public c(FusedLocationProviderClient fusedLocationProviderClient) {
            this.a = fusedLocationProviderClient;
            a();
        }

        public final void a() {
            long j = OneSignal.O0() ? 270000L : 570000L;
            LocationRequest priority = LocationRequest.create().setFastestInterval(j).setInterval(j).setMaxWaitTime((long) (j * 1.5d)).setPriority(102);
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "HMSLocationController Huawei LocationServices requestLocationUpdates!");
            this.a.requestLocationUpdates(priority, this, LocationController.h().getLooper());
        }
    }

    public static void e() {
        synchronized (LocationController.d) {
            j = null;
        }
    }

    public static void l() {
        synchronized (LocationController.d) {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "HMSLocationController onFocusChange!");
            if (LocationController.k() && j == null) {
                return;
            }
            FusedLocationProviderClient fusedLocationProviderClient = j;
            if (fusedLocationProviderClient != null) {
                c cVar = k;
                if (cVar != null) {
                    fusedLocationProviderClient.removeLocationUpdates(cVar);
                }
                k = new c(j);
            }
        }
    }

    public static void p() {
        q();
    }

    public static void q() {
        synchronized (LocationController.d) {
            if (j == null) {
                try {
                    j = LocationServices.getFusedLocationProviderClient(LocationController.g);
                } catch (Exception e) {
                    OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                    OneSignal.a(log_level, "Huawei LocationServices getFusedLocationProviderClient failed! " + e);
                    e();
                    return;
                }
            }
            Location location = LocationController.h;
            if (location != null) {
                LocationController.d(location);
            } else {
                j.getLastLocation().addOnSuccessListener(new b()).addOnFailureListener(new a());
            }
        }
    }
}
