package com.onesignal;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.onesignal.a;
import java.util.HashMap;

/* loaded from: classes2.dex */
public class PermissionsActivity extends Activity {
    public static final String g0 = "com.onesignal.PermissionsActivity";
    public static boolean h0;
    public static boolean i0;
    public static boolean j0;
    public static a.b k0;
    public static final HashMap<String, c> l0 = new HashMap<>();
    public String a;
    public String f0;

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ int[] a;

        public a(int[] iArr) {
            this.a = iArr;
        }

        @Override // java.lang.Runnable
        public void run() {
            int[] iArr = this.a;
            boolean z = false;
            if (iArr.length > 0 && iArr[0] == 0) {
                z = true;
            }
            c cVar = (c) PermissionsActivity.l0.get(PermissionsActivity.this.a);
            if (cVar == null) {
                throw new RuntimeException("Missing handler for permissionRequestType: " + PermissionsActivity.this.a);
            } else if (!z) {
                cVar.b(PermissionsActivity.this.h());
            } else {
                cVar.a();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b extends a.b {
        public final /* synthetic */ String a;
        public final /* synthetic */ String b;
        public final /* synthetic */ Class c;

        public b(String str, String str2, Class cls) {
            this.a = str;
            this.b = str2;
            this.c = cls;
        }

        @Override // com.onesignal.a.b
        public void a(Activity activity) {
            if (activity.getClass().equals(PermissionsActivity.class)) {
                return;
            }
            Intent intent = new Intent(activity, PermissionsActivity.class);
            intent.setFlags(131072);
            intent.putExtra("INTENT_EXTRA_PERMISSION_TYPE", this.a).putExtra("INTENT_EXTRA_ANDROID_PERMISSION_STRING", this.b).putExtra("INTENT_EXTRA_CALLBACK_CLASS", this.c.getName());
            activity.startActivity(intent);
            activity.overridePendingTransition(rx2.onesignal_fade_in, rx2.onesignal_fade_out);
        }
    }

    /* loaded from: classes2.dex */
    public interface c {
        void a();

        void b(boolean z);
    }

    public static void e(String str, c cVar) {
        l0.put(str, cVar);
    }

    public static void i(boolean z, String str, String str2, Class<?> cls) {
        if (h0) {
            return;
        }
        i0 = z;
        k0 = new b(str, str2, cls);
        com.onesignal.a b2 = k7.b();
        if (b2 != null) {
            b2.b(g0, k0);
        }
    }

    public final void d(Bundle bundle) {
        if (Build.VERSION.SDK_INT < 23) {
            finish();
            overridePendingTransition(rx2.onesignal_fade_in, rx2.onesignal_fade_out);
            return;
        }
        g(bundle);
        this.a = bundle.getString("INTENT_EXTRA_PERMISSION_TYPE");
        String string = bundle.getString("INTENT_EXTRA_ANDROID_PERMISSION_STRING");
        this.f0 = string;
        f(string);
    }

    public final void f(String str) {
        if (h0) {
            return;
        }
        h0 = true;
        j0 = !cd.b(this, str);
        cd.a(this, new String[]{str}, 2);
    }

    public final void g(Bundle bundle) {
        String string = bundle.getString("INTENT_EXTRA_CALLBACK_CLASS");
        try {
            Class.forName(string);
        } catch (ClassNotFoundException unused) {
            throw new RuntimeException("Could not find callback class for PermissionActivity: " + string);
        }
    }

    public final boolean h() {
        return i0 && j0 && !cd.b(this, this.f0);
    }

    @Override // android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        OneSignal.L0(this);
        d(getIntent().getExtras());
    }

    @Override // android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        d(intent.getExtras());
    }

    @Override // android.app.Activity
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        h0 = false;
        if (i == 2) {
            new Handler().postDelayed(new a(iArr), 500L);
        }
        com.onesignal.a b2 = k7.b();
        if (b2 != null) {
            b2.q(g0);
        }
        finish();
        overridePendingTransition(rx2.onesignal_fade_in, rx2.onesignal_fade_out);
    }
}
