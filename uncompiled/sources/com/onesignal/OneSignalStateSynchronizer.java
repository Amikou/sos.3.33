package com.onesignal;

import com.onesignal.LocationController;
import com.onesignal.OneSignal;
import com.onesignal.d1;
import com.onesignal.s1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class OneSignalStateSynchronizer {
    public static final Object a = new Object();
    public static HashMap<UserStateSynchronizerType, s1> b = new HashMap<>();

    /* loaded from: classes2.dex */
    public enum UserStateSynchronizerType {
        PUSH,
        EMAIL,
        SMS;

        public boolean isEmail() {
            return equals(EMAIL);
        }

        public boolean isPush() {
            return equals(PUSH);
        }

        public boolean isSMS() {
            return equals(SMS);
        }
    }

    /* loaded from: classes2.dex */
    public interface a {
        void a(String str);

        void b(b bVar);
    }

    /* loaded from: classes2.dex */
    public static class b {
        public b(int i, String str) {
        }
    }

    public static void a() {
        d().o();
        b().o();
        f().o();
    }

    public static o1 b() {
        HashMap<UserStateSynchronizerType, s1> hashMap = b;
        UserStateSynchronizerType userStateSynchronizerType = UserStateSynchronizerType.EMAIL;
        if (!hashMap.containsKey(userStateSynchronizerType) || b.get(userStateSynchronizerType) == null) {
            synchronized (a) {
                if (b.get(userStateSynchronizerType) == null) {
                    b.put(userStateSynchronizerType, new o1());
                }
            }
        }
        return (o1) b.get(userStateSynchronizerType);
    }

    public static String c() {
        return d().g0();
    }

    public static p1 d() {
        HashMap<UserStateSynchronizerType, s1> hashMap = b;
        UserStateSynchronizerType userStateSynchronizerType = UserStateSynchronizerType.PUSH;
        if (!hashMap.containsKey(userStateSynchronizerType) || b.get(userStateSynchronizerType) == null) {
            synchronized (a) {
                if (b.get(userStateSynchronizerType) == null) {
                    b.put(userStateSynchronizerType, new p1());
                }
            }
        }
        return (p1) b.get(userStateSynchronizerType);
    }

    public static String e() {
        return d().E();
    }

    public static q1 f() {
        HashMap<UserStateSynchronizerType, s1> hashMap = b;
        UserStateSynchronizerType userStateSynchronizerType = UserStateSynchronizerType.SMS;
        if (!hashMap.containsKey(userStateSynchronizerType) || b.get(userStateSynchronizerType) == null) {
            synchronized (a) {
                if (b.get(userStateSynchronizerType) == null) {
                    b.put(userStateSynchronizerType, new q1());
                }
            }
        }
        return (q1) b.get(userStateSynchronizerType);
    }

    public static boolean g() {
        return d().F() || b().F() || f().F();
    }

    public static s1.e h(boolean z) {
        return d().h0(z);
    }

    public static List<s1> i() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(d());
        if (OneSignal.H0()) {
            arrayList.add(b());
        }
        if (OneSignal.I0()) {
            arrayList.add(f());
        }
        return arrayList;
    }

    public static boolean j() {
        return d().i0();
    }

    public static void k() {
        d().K();
        b().K();
        f().K();
    }

    public static boolean l() {
        boolean Q = d().Q();
        boolean Q2 = b().Q();
        boolean Q3 = f().Q();
        if (Q2) {
            Q2 = b().E() != null;
        }
        if (Q3) {
            Q3 = f().E() != null;
        }
        return Q || Q2 || Q3;
    }

    public static void m(boolean z) {
        d().R(z);
        b().R(z);
        f().R(z);
    }

    public static void n() {
        b().k0();
        f().k0();
    }

    public static void o() {
        d().S();
        b().S();
        f().S();
        d().j0(null);
        b().l0(null);
        f().l0(null);
        OneSignal.D1(-3660L);
    }

    public static void p(JSONObject jSONObject, d1.g gVar) {
        for (s1 s1Var : i()) {
            s1Var.V(jSONObject, gVar);
        }
    }

    public static void q(JSONObject jSONObject, OneSignal.s sVar) {
        try {
            JSONObject put = new JSONObject().put("tags", jSONObject);
            d().W(put, sVar);
            b().W(put, sVar);
            f().W(put, sVar);
        } catch (JSONException e) {
            if (sVar != null) {
                sVar.b(new OneSignal.i0(-1, "Encountered an error attempting to serialize your tags into JSON: " + e.getMessage() + "\n" + e.getStackTrace()));
            }
            e.printStackTrace();
        }
    }

    public static void r() {
        d().Z();
        b().Z();
        f().Z();
    }

    public static void s() {
        b().Z();
    }

    public static void t(boolean z) {
        d().k0(z);
    }

    public static void u(boolean z) {
        d().b0(z);
        b().b0(z);
        f().b0(z);
    }

    public static void v(JSONObject jSONObject, a aVar) {
        d().c0(jSONObject, aVar);
        b().c0(jSONObject, aVar);
        f().c0(jSONObject, aVar);
    }

    public static void w(LocationController.d dVar) {
        d().e0(dVar);
        b().e0(dVar);
        f().e0(dVar);
    }

    public static void x(JSONObject jSONObject) {
        d().l0(jSONObject);
    }
}
