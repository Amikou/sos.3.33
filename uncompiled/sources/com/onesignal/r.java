package com.onesignal;

/* compiled from: OSBackgroundManager.kt */
/* loaded from: classes2.dex */
public class r {
    public final void d(Runnable runnable, String str) {
        fs1.f(runnable, "runnable");
        fs1.f(str, "threadName");
        if (OSUtils.H()) {
            new Thread(runnable, str).start();
        } else {
            runnable.run();
        }
    }
}
