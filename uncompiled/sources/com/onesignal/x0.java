package com.onesignal;

import android.os.Handler;
import android.os.HandlerThread;
import com.onesignal.OneSignal;

/* compiled from: OSTimeoutHandler.java */
/* loaded from: classes2.dex */
public class x0 extends HandlerThread {
    public static final String f0 = x0.class.getCanonicalName();
    public static final Object g0 = new Object();
    public static x0 h0;
    public final Handler a;

    public x0() {
        super(f0);
        start();
        this.a = new Handler(getLooper());
    }

    public static x0 b() {
        if (h0 == null) {
            synchronized (g0) {
                if (h0 == null) {
                    h0 = new x0();
                }
            }
        }
        return h0;
    }

    public void a(Runnable runnable) {
        synchronized (g0) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "Running destroyTimeout with runnable: " + runnable.toString());
            this.a.removeCallbacks(runnable);
        }
    }

    public void c(long j, Runnable runnable) {
        synchronized (g0) {
            a(runnable);
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "Running startTimeout with timeout: " + j + " and runnable: " + runnable.toString());
            this.a.postDelayed(runnable, j);
        }
    }
}
