package com.onesignal.shortcutbadger;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import com.onesignal.shortcutbadger.impl.AdwHomeBadger;
import com.onesignal.shortcutbadger.impl.ApexHomeBadger;
import com.onesignal.shortcutbadger.impl.AsusHomeBadger;
import com.onesignal.shortcutbadger.impl.DefaultBadger;
import com.onesignal.shortcutbadger.impl.EverythingMeHomeBadger;
import com.onesignal.shortcutbadger.impl.HuaweiHomeBadger;
import com.onesignal.shortcutbadger.impl.NewHtcHomeBadger;
import com.onesignal.shortcutbadger.impl.NovaHomeBadger;
import com.onesignal.shortcutbadger.impl.OPPOHomeBader;
import com.onesignal.shortcutbadger.impl.SamsungHomeBadger;
import com.onesignal.shortcutbadger.impl.SonyHomeBadger;
import com.onesignal.shortcutbadger.impl.VivoHomeBadger;
import com.onesignal.shortcutbadger.impl.ZukHomeBadger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.web3j.ens.contracts.generated.ENS;

/* compiled from: ShortcutBadger.java */
/* loaded from: classes2.dex */
public final class b {
    public static final List<Class<? extends a>> a;
    public static a b;
    public static ComponentName c;

    static {
        LinkedList linkedList = new LinkedList();
        a = linkedList;
        linkedList.add(AdwHomeBadger.class);
        linkedList.add(ApexHomeBadger.class);
        linkedList.add(NewHtcHomeBadger.class);
        linkedList.add(NovaHomeBadger.class);
        linkedList.add(SonyHomeBadger.class);
        linkedList.add(AsusHomeBadger.class);
        linkedList.add(HuaweiHomeBadger.class);
        linkedList.add(OPPOHomeBader.class);
        linkedList.add(SamsungHomeBadger.class);
        linkedList.add(ZukHomeBadger.class);
        linkedList.add(VivoHomeBadger.class);
        linkedList.add(EverythingMeHomeBadger.class);
    }

    public static void a(Context context, int i) throws ShortcutBadgeException {
        if (b == null && !b(context)) {
            throw new ShortcutBadgeException("No default launcher available");
        }
        try {
            b.b(context, c, i);
        } catch (Exception e) {
            throw new ShortcutBadgeException("Unable to execute badge", e);
        }
    }

    public static boolean b(Context context) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if (launchIntentForPackage == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to find launch intent for package ");
            sb.append(context.getPackageName());
            return false;
        }
        c = launchIntentForPackage.getComponent();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 65536);
        if (resolveActivity == null || resolveActivity.activityInfo.name.toLowerCase().contains(ENS.FUNC_RESOLVER)) {
            return false;
        }
        String str = resolveActivity.activityInfo.packageName;
        Iterator<Class<? extends a>> it = a.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            a aVar = null;
            try {
                aVar = it.next().newInstance();
            } catch (Exception unused) {
            }
            if (aVar != null && aVar.a().contains(str)) {
                b = aVar;
                break;
            }
        }
        if (b == null) {
            String str2 = Build.MANUFACTURER;
            if (str2.equalsIgnoreCase("ZUK")) {
                b = new ZukHomeBadger();
                return true;
            } else if (str2.equalsIgnoreCase("OPPO")) {
                b = new OPPOHomeBader();
                return true;
            } else if (str2.equalsIgnoreCase("VIVO")) {
                b = new VivoHomeBadger();
                return true;
            } else {
                b = new DefaultBadger();
                return true;
            }
        }
        return true;
    }
}
