package com.onesignal.shortcutbadger.impl;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.onesignal.shortcutbadger.ShortcutBadgeException;
import com.onesignal.shortcutbadger.a;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class NewHtcHomeBadger implements a {
    @Override // com.onesignal.shortcutbadger.a
    public List<String> a() {
        return Arrays.asList("com.htc.launcher");
    }

    @Override // com.onesignal.shortcutbadger.a
    public void b(Context context, ComponentName componentName, int i) throws ShortcutBadgeException {
        Intent intent = new Intent("com.htc.launcher.action.SET_NOTIFICATION");
        intent.putExtra("com.htc.launcher.extra.COMPONENT", componentName.flattenToShortString());
        intent.putExtra("com.htc.launcher.extra.COUNT", i);
        Intent intent2 = new Intent("com.htc.launcher.action.UPDATE_SHORTCUT");
        intent2.putExtra("packagename", componentName.getPackageName());
        intent2.putExtra("count", i);
        if (!mr.a(context, intent) && !mr.a(context, intent2)) {
            throw new ShortcutBadgeException("unable to resolve intent: " + intent2.toString());
        }
        context.sendBroadcast(intent);
        context.sendBroadcast(intent2);
    }
}
