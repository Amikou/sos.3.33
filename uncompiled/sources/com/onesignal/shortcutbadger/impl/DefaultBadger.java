package com.onesignal.shortcutbadger.impl;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.onesignal.shortcutbadger.ShortcutBadgeException;
import com.onesignal.shortcutbadger.a;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class DefaultBadger implements a {
    @Override // com.onesignal.shortcutbadger.a
    public List<String> a() {
        return new ArrayList(0);
    }

    @Override // com.onesignal.shortcutbadger.a
    public void b(Context context, ComponentName componentName, int i) throws ShortcutBadgeException {
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", i);
        intent.putExtra("badge_count_package_name", componentName.getPackageName());
        intent.putExtra("badge_count_class_name", componentName.getClassName());
        if (mr.a(context, intent)) {
            context.sendBroadcast(intent);
            return;
        }
        throw new ShortcutBadgeException("unable to resolve intent: " + intent.toString());
    }

    public boolean c(Context context) {
        return mr.a(context, new Intent("android.intent.action.BADGE_COUNT_UPDATE"));
    }
}
