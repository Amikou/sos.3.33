package com.onesignal.shortcutbadger.impl;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.onesignal.shortcutbadger.ShortcutBadgeException;
import com.onesignal.shortcutbadger.a;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class HuaweiHomeBadger implements a {
    @Override // com.onesignal.shortcutbadger.a
    public List<String> a() {
        return Arrays.asList("com.huawei.android.launcher");
    }

    @Override // com.onesignal.shortcutbadger.a
    @SuppressLint({"NewApi"})
    public void b(Context context, ComponentName componentName, int i) throws ShortcutBadgeException {
        Bundle bundle = new Bundle();
        bundle.putString("package", context.getPackageName());
        bundle.putString("class", componentName.getClassName());
        bundle.putInt("badgenumber", i);
        context.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", (String) null, bundle);
    }
}
