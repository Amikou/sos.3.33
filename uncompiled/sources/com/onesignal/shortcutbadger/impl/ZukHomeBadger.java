package com.onesignal.shortcutbadger.impl;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.onesignal.shortcutbadger.ShortcutBadgeException;
import com.onesignal.shortcutbadger.a;
import java.util.Collections;
import java.util.List;

/* loaded from: classes2.dex */
public class ZukHomeBadger implements a {
    public final Uri a = Uri.parse("content://com.android.badge/badge");

    @Override // com.onesignal.shortcutbadger.a
    public List<String> a() {
        return Collections.singletonList("com.zui.launcher");
    }

    @Override // com.onesignal.shortcutbadger.a
    @TargetApi(11)
    public void b(Context context, ComponentName componentName, int i) throws ShortcutBadgeException {
        Bundle bundle = new Bundle();
        bundle.putInt("app_badge_count", i);
        context.getContentResolver().call(this.a, "setAppBadgeCount", (String) null, bundle);
    }
}
