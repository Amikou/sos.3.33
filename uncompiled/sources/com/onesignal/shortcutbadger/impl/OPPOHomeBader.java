package com.onesignal.shortcutbadger.impl;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.onesignal.shortcutbadger.ShortcutBadgeException;
import com.onesignal.shortcutbadger.a;
import java.util.Collections;
import java.util.List;

/* loaded from: classes2.dex */
public class OPPOHomeBader implements a {
    public int a = -1;

    @Override // com.onesignal.shortcutbadger.a
    public List<String> a() {
        return Collections.singletonList("com.oppo.launcher");
    }

    @Override // com.onesignal.shortcutbadger.a
    public void b(Context context, ComponentName componentName, int i) throws ShortcutBadgeException {
        if (this.a == i) {
            return;
        }
        this.a = i;
        if (Build.VERSION.SDK_INT >= 11) {
            d(context, i);
        } else {
            c(context, componentName, i);
        }
    }

    public final void c(Context context, ComponentName componentName, int i) throws ShortcutBadgeException {
        if (i == 0) {
            i = -1;
        }
        Intent intent = new Intent("com.oppo.unsettledevent");
        intent.putExtra("pakeageName", componentName.getPackageName());
        intent.putExtra("number", i);
        intent.putExtra("upgradeNumber", i);
        mr.c(context, intent);
    }

    @TargetApi(11)
    public final void d(Context context, int i) throws ShortcutBadgeException {
        Bundle bundle = new Bundle();
        bundle.putInt("app_badge_count", i);
        context.getContentResolver().call(Uri.parse("content://com.android.badge/badge"), "setAppBadgeCount", (String) null, bundle);
    }
}
