package com.onesignal.shortcutbadger.impl;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.onesignal.shortcutbadger.ShortcutBadgeException;
import com.onesignal.shortcutbadger.a;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class VivoHomeBadger implements a {
    @Override // com.onesignal.shortcutbadger.a
    public List<String> a() {
        return Arrays.asList("com.vivo.launcher");
    }

    @Override // com.onesignal.shortcutbadger.a
    public void b(Context context, ComponentName componentName, int i) throws ShortcutBadgeException {
        Intent intent = new Intent("launcher.action.CHANGE_APPLICATION_NOTIFICATION_NUM");
        intent.putExtra("packageName", context.getPackageName());
        intent.putExtra("className", componentName.getClassName());
        intent.putExtra("notificationNum", i);
        context.sendBroadcast(intent);
    }
}
