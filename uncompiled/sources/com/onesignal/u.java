package com.onesignal;

import com.onesignal.OneSignal;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: OSDynamicTriggerTimer.java */
/* loaded from: classes2.dex */
public class u {
    public static void a(TimerTask timerTask, String str, long j) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, "scheduleTrigger: " + str + " delay: " + j);
        StringBuilder sb = new StringBuilder();
        sb.append("trigger_timer:");
        sb.append(str);
        new Timer(sb.toString()).schedule(timerTask, j);
    }
}
