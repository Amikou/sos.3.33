package com.onesignal;

import com.onesignal.OneSignal;

/* compiled from: OSNotificationReceivedEvent.java */
/* loaded from: classes2.dex */
public class m0 {
    public final h0 a;
    public final x0 b;
    public final Runnable c;
    public final g0 d;
    public boolean e = false;

    /* compiled from: OSNotificationReceivedEvent.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "Running complete from OSNotificationReceivedEvent timeout runnable!");
            m0 m0Var = m0.this;
            m0Var.b(m0Var.c());
        }
    }

    /* compiled from: OSNotificationReceivedEvent.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ g0 a;

        public b(g0 g0Var) {
            this.a = g0Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            m0.this.e(this.a);
        }
    }

    public m0(h0 h0Var, g0 g0Var) {
        this.d = g0Var;
        this.a = h0Var;
        x0 b2 = x0.b();
        this.b = b2;
        a aVar = new a();
        this.c = aVar;
        b2.c(25000L, aVar);
    }

    public static boolean d() {
        return OSUtils.H();
    }

    public synchronized void b(g0 g0Var) {
        this.b.a(this.c);
        if (this.e) {
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "OSNotificationReceivedEvent already completed");
            return;
        }
        this.e = true;
        if (d()) {
            new Thread(new b(g0Var), "OS_COMPLETE_NOTIFICATION").start();
        } else {
            e(g0Var);
        }
    }

    public g0 c() {
        return this.d;
    }

    public final void e(g0 g0Var) {
        this.a.f(this.d.c(), g0Var != null ? g0Var.c() : null);
    }

    public String toString() {
        return "OSNotificationReceivedEvent{isComplete=" + this.e + ", notification=" + this.d + '}';
    }
}
