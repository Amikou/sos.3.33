package com.onesignal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Build;
import android.view.ViewTreeObserver;
import com.onesignal.OneSignal;
import com.onesignal.v0;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ActivityLifecycleHandler.java */
/* loaded from: classes2.dex */
public class a implements v0.b {
    public static final Map<String, b> d = new ConcurrentHashMap();
    public static final Map<String, v0.c> e = new ConcurrentHashMap();
    public static final Map<String, c> f = new ConcurrentHashMap();
    public final OSFocusHandler a;
    @SuppressLint({"StaticFieldLeak"})
    public Activity b = null;
    public boolean c = false;

    /* compiled from: ActivityLifecycleHandler.java */
    /* loaded from: classes2.dex */
    public static abstract class b {
        public void a(Activity activity) {
        }

        public void b(Activity activity) {
        }
    }

    /* compiled from: ActivityLifecycleHandler.java */
    /* loaded from: classes2.dex */
    public static class c implements ViewTreeObserver.OnGlobalLayoutListener {
        public final v0.c a;
        public final v0.b f0;
        public final String g0;

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            if (z0.l(new WeakReference(OneSignal.Q()))) {
                return;
            }
            this.f0.a(this.g0, this);
            this.a.c();
        }

        public c(v0.b bVar, v0.c cVar, String str) {
            this.f0 = bVar;
            this.a = cVar;
            this.g0 = str;
        }
    }

    public a(OSFocusHandler oSFocusHandler) {
        this.a = oSFocusHandler;
    }

    @Override // com.onesignal.v0.b
    public void a(String str, c cVar) {
        Activity activity = this.b;
        if (activity != null) {
            ViewTreeObserver viewTreeObserver = activity.getWindow().getDecorView().getViewTreeObserver();
            if (Build.VERSION.SDK_INT < 16) {
                viewTreeObserver.removeGlobalOnLayoutListener(cVar);
            } else {
                viewTreeObserver.removeOnGlobalLayoutListener(cVar);
            }
        }
        f.remove(str);
        e.remove(str);
    }

    public void b(String str, b bVar) {
        d.put(str, bVar);
        Activity activity = this.b;
        if (activity != null) {
            bVar.a(activity);
        }
    }

    public void c(String str, v0.c cVar) {
        Activity activity = this.b;
        if (activity != null) {
            ViewTreeObserver viewTreeObserver = activity.getWindow().getDecorView().getViewTreeObserver();
            c cVar2 = new c(this, cVar, str);
            viewTreeObserver.addOnGlobalLayoutListener(cVar2);
            f.put(str, cVar2);
        }
        e.put(str, cVar);
    }

    public Activity d() {
        return this.b;
    }

    public final void e() {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, "ActivityLifecycleHandler handleFocus, nextResumeIsFirstActivity: " + this.c);
        if (!this.a.f() && !this.c) {
            OneSignal.d1(log_level, "ActivityLifecycleHandler cancel background lost focus worker");
            this.a.e("FOCUS_LOST_WORKER_TAG", OneSignal.e);
            return;
        }
        OneSignal.d1(log_level, "ActivityLifecycleHandler reset background state, call app focus");
        this.c = false;
        this.a.j();
    }

    public final void f() {
        OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "ActivityLifecycleHandler Handling lost focus");
        OSFocusHandler oSFocusHandler = this.a;
        if (oSFocusHandler != null) {
            if (!oSFocusHandler.f() || this.a.g()) {
                OneSignal.b0().c();
                this.a.k("FOCUS_LOST_WORKER_TAG", 2000L, OneSignal.e);
            }
        }
    }

    public final void g() {
        String str;
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        StringBuilder sb = new StringBuilder();
        sb.append("curActivity is NOW: ");
        if (this.b != null) {
            str = "" + this.b.getClass().getName() + ":" + this.b;
        } else {
            str = "null";
        }
        sb.append(str);
        OneSignal.a(log_level, sb.toString());
    }

    public final void h(int i, Activity activity) {
        if (i == 2) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.d1(log_level, "Configuration Orientation Change: LANDSCAPE (" + i + ") on activity: " + activity);
        } else if (i == 1) {
            OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.d1(log_level2, "Configuration Orientation Change: PORTRAIT (" + i + ") on activity: " + activity);
        }
    }

    public void i(Activity activity) {
    }

    public void j(Activity activity) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "onActivityDestroyed: " + activity);
        f.clear();
        if (activity == this.b) {
            this.b = null;
            f();
        }
        g();
    }

    public void k(Activity activity) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "onActivityPaused: " + activity);
        if (activity == this.b) {
            this.b = null;
            f();
        }
        g();
    }

    public void l(Activity activity) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "onActivityResumed: " + activity);
        r(activity);
        g();
        e();
    }

    public void m(Activity activity) {
        this.a.l();
    }

    public void n(Activity activity) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "onActivityStopped: " + activity);
        if (activity == this.b) {
            this.b = null;
            f();
        }
        for (Map.Entry<String, b> entry : d.entrySet()) {
            entry.getValue().b(activity);
        }
        g();
        if (this.b == null) {
            this.a.m();
        }
    }

    public void o(Configuration configuration, Activity activity) {
        Activity activity2 = this.b;
        if (activity2 == null || !OSUtils.q(activity2, 128)) {
            return;
        }
        h(configuration.orientation, activity);
        p(activity);
    }

    public final void p(Activity activity) {
        f();
        for (Map.Entry<String, b> entry : d.entrySet()) {
            entry.getValue().b(activity);
        }
        for (Map.Entry<String, b> entry2 : d.entrySet()) {
            entry2.getValue().a(this.b);
        }
        ViewTreeObserver viewTreeObserver = this.b.getWindow().getDecorView().getViewTreeObserver();
        for (Map.Entry<String, v0.c> entry3 : e.entrySet()) {
            c cVar = new c(this, entry3.getValue(), entry3.getKey());
            viewTreeObserver.addOnGlobalLayoutListener(cVar);
            f.put(entry3.getKey(), cVar);
        }
        e();
    }

    public void q(String str) {
        d.remove(str);
    }

    public void r(Activity activity) {
        this.b = activity;
        for (Map.Entry<String, b> entry : d.entrySet()) {
            entry.getValue().a(this.b);
        }
        try {
            ViewTreeObserver viewTreeObserver = this.b.getWindow().getDecorView().getViewTreeObserver();
            for (Map.Entry<String, v0.c> entry2 : e.entrySet()) {
                c cVar = new c(this, entry2.getValue(), entry2.getKey());
                viewTreeObserver.addOnGlobalLayoutListener(cVar);
                f.put(entry2.getKey(), cVar);
            }
        } catch (RuntimeException e2) {
            e2.printStackTrace();
        }
    }

    public void s(boolean z) {
        this.c = z;
    }
}
