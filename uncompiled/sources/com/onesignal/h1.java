package com.onesignal;

import android.content.Context;
import com.onesignal.OneSignal;
import com.onesignal.f1;
import java.io.IOException;

/* compiled from: PushRegistratorAbstractGoogle.java */
/* loaded from: classes2.dex */
public abstract class h1 implements f1 {
    public static int d = 5;
    public static int e = 10000;
    public f1.a a;
    public Thread b;
    public boolean c;

    /* compiled from: PushRegistratorAbstractGoogle.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ String a;

        public a(String str) {
            this.a = str;
        }

        @Override // java.lang.Runnable
        public void run() {
            int i = 0;
            while (i < h1.d && !h1.this.e(this.a, i)) {
                i++;
                OSUtils.V(h1.e * i);
            }
        }
    }

    public static int j(Throwable th) {
        String l = OSUtils.l(th);
        if (th instanceof IOException) {
            if ("SERVICE_NOT_AVAILABLE".equals(l)) {
                return -9;
            }
            return "AUTHENTICATION_FAILED".equals(l) ? -29 : -11;
        }
        return -12;
    }

    @Override // com.onesignal.f1
    public void a(Context context, String str, f1.a aVar) {
        this.a = aVar;
        if (i(str, aVar)) {
            h(str);
        }
    }

    public final boolean e(String str, int i) {
        try {
            String g = g(str);
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
            OneSignal.a(log_level, "Device registered, push token = " + g);
            this.a.a(g, 1);
            return true;
        } catch (IOException e2) {
            int j = j(e2);
            String l = OSUtils.l(e2);
            if ("SERVICE_NOT_AVAILABLE".equals(l) || "AUTHENTICATION_FAILED".equals(l)) {
                Exception exc = new Exception(e2);
                if (i >= d - 1) {
                    OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.ERROR;
                    OneSignal.b(log_level2, "Retry count of " + d + " exceed! Could not get a " + f() + " Token.", exc);
                } else {
                    OneSignal.LOG_LEVEL log_level3 = OneSignal.LOG_LEVEL.INFO;
                    OneSignal.b(log_level3, "'Google Play services' returned " + l + " error. Current retry count: " + i, exc);
                    if (i == 2) {
                        this.a.a(null, j);
                        this.c = true;
                        return true;
                    }
                }
                return false;
            }
            Exception exc2 = new Exception(e2);
            OneSignal.LOG_LEVEL log_level4 = OneSignal.LOG_LEVEL.ERROR;
            OneSignal.b(log_level4, "Error Getting " + f() + " Token", exc2);
            if (!this.c) {
                this.a.a(null, j);
            }
            return true;
        } catch (Throwable th) {
            Exception exc3 = new Exception(th);
            int j2 = j(th);
            OneSignal.LOG_LEVEL log_level5 = OneSignal.LOG_LEVEL.ERROR;
            OneSignal.b(log_level5, "Unknown error getting " + f() + " Token", exc3);
            this.a.a(null, j2);
            return true;
        }
    }

    public abstract String f();

    public abstract String g(String str) throws Throwable;

    public final void h(String str) {
        try {
            if (OSUtils.D()) {
                k(str);
            } else {
                g.d();
                OneSignal.a(OneSignal.LOG_LEVEL.ERROR, "'Google Play services' app not installed or disabled on the device.");
                this.a.a(null, -7);
            }
        } catch (Throwable th) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
            OneSignal.b(log_level, "Could not register with " + f() + " due to an issue with your AndroidManifest.xml or with 'Google Play services'.", th);
            this.a.a(null, -8);
        }
    }

    public final boolean i(String str, f1.a aVar) {
        boolean z;
        try {
            Float.parseFloat(str);
            z = true;
        } catch (Throwable unused) {
            z = false;
        }
        if (z) {
            return true;
        }
        OneSignal.a(OneSignal.LOG_LEVEL.ERROR, "Missing Google Project number!\nPlease enter a Google Project number / Sender ID on under App Settings > Android > Configuration on the OneSignal dashboard.");
        aVar.a(null, -6);
        return false;
    }

    public final synchronized void k(String str) {
        Thread thread = this.b;
        if (thread == null || !thread.isAlive()) {
            Thread thread2 = new Thread(new a(str));
            this.b = thread2;
            thread2.start();
        }
    }
}
