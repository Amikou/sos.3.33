package com.onesignal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import androidx.media3.common.PlaybackException;
import com.onesignal.OneSignal;
import com.onesignal.d1;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import okhttp3.HttpUrl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: TrackGooglePurchase.java */
/* loaded from: classes2.dex */
public class m1 {
    public static int i = -99;
    public static Class<?> j;
    public ServiceConnection a;
    public Object b;
    public Method c;
    public Method d;
    public Context e;
    public boolean g;
    public boolean h = false;
    public ArrayList<String> f = new ArrayList<>();

    /* compiled from: TrackGooglePurchase.java */
    /* loaded from: classes2.dex */
    public class a implements ServiceConnection {
        public a() {
        }

        @Override // android.content.ServiceConnection
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                Method q = m1.q(Class.forName("com.android.vending.billing.IInAppBillingService$Stub"));
                q.setAccessible(true);
                m1.this.b = q.invoke(null, iBinder);
                m1.this.b();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }

        @Override // android.content.ServiceConnection
        public void onServiceDisconnected(ComponentName componentName) {
            int unused = m1.i = -99;
            m1.this.b = null;
        }
    }

    /* compiled from: TrackGooglePurchase.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            m1.this.h = true;
            try {
                if (m1.this.c == null) {
                    m1.this.c = m1.r(m1.j);
                    m1.this.c.setAccessible(true);
                }
                Bundle bundle = (Bundle) m1.this.c.invoke(m1.this.b, 3, m1.this.e.getPackageName(), "inapp", null);
                if (bundle.getInt("RESPONSE_CODE") == 0) {
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    ArrayList<String> stringArrayList = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                    ArrayList<String> stringArrayList2 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                    for (int i = 0; i < stringArrayList2.size(); i++) {
                        String str = stringArrayList.get(i);
                        String string = new JSONObject(stringArrayList2.get(i)).getString("purchaseToken");
                        if (!m1.this.f.contains(string) && !arrayList2.contains(string)) {
                            arrayList2.add(string);
                            arrayList.add(str);
                        }
                    }
                    if (arrayList.size() > 0) {
                        m1.this.t(arrayList, arrayList2);
                    } else if (stringArrayList2.size() == 0) {
                        m1.this.g = false;
                        b1.j("GTPlayerPurchases", "ExistingPurchases", false);
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
            m1.this.h = false;
        }
    }

    /* compiled from: TrackGooglePurchase.java */
    /* loaded from: classes2.dex */
    public class c extends d1.g {
        public final /* synthetic */ ArrayList a;

        public c(ArrayList arrayList) {
            this.a = arrayList;
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            m1.this.f.addAll(this.a);
            b1.m("GTPlayerPurchases", "purchaseTokens", m1.this.f.toString());
            b1.j("GTPlayerPurchases", "ExistingPurchases", true);
            m1.this.g = false;
            m1.this.h = false;
        }
    }

    public m1(Context context) {
        this.g = true;
        this.e = context;
        try {
            JSONArray jSONArray = new JSONArray(b1.f("GTPlayerPurchases", "purchaseTokens", HttpUrl.PATH_SEGMENT_ENCODE_SET_URI));
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                this.f.add(jSONArray.get(i2).toString());
            }
            boolean z = jSONArray.length() == 0;
            this.g = z;
            if (z) {
                this.g = b1.b("GTPlayerPurchases", "ExistingPurchases", true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        u();
    }

    public static boolean a(Context context) {
        if (i == -99) {
            i = context.checkCallingOrSelfPermission("com.android.vending.BILLING");
        }
        try {
            if (i == 0) {
                j = Class.forName("com.android.vending.billing.IInAppBillingService");
            }
            return i == 0;
        } catch (Throwable unused) {
            i = 0;
            return false;
        }
    }

    public static Method q(Class cls) {
        Method[] methods;
        for (Method method : cls.getMethods()) {
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length == 1 && parameterTypes[0] == IBinder.class) {
                return method;
            }
        }
        return null;
    }

    public static Method r(Class cls) {
        Method[] methods;
        for (Method method : cls.getMethods()) {
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length == 4 && parameterTypes[0] == Integer.TYPE && parameterTypes[1] == String.class && parameterTypes[2] == String.class && parameterTypes[3] == String.class) {
                return method;
            }
        }
        return null;
    }

    public static Method s(Class cls) {
        Method[] methods;
        for (Method method : cls.getMethods()) {
            Class<?>[] parameterTypes = method.getParameterTypes();
            Class<?> returnType = method.getReturnType();
            if (parameterTypes.length == 4 && parameterTypes[0] == Integer.TYPE && parameterTypes[1] == String.class && parameterTypes[2] == String.class && parameterTypes[3] == Bundle.class && returnType == Bundle.class) {
                return method;
            }
        }
        return null;
    }

    public final void b() {
        if (this.h) {
            return;
        }
        new Thread(new b()).start();
    }

    public final void t(ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        try {
            if (this.d == null) {
                Method s = s(j);
                this.d = s;
                s.setAccessible(true);
            }
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
            Bundle bundle2 = (Bundle) this.d.invoke(this.b, 3, this.e.getPackageName(), "inapp", bundle);
            if (bundle2.getInt("RESPONSE_CODE") == 0) {
                ArrayList<String> stringArrayList = bundle2.getStringArrayList("DETAILS_LIST");
                HashMap hashMap = new HashMap();
                Iterator<String> it = stringArrayList.iterator();
                while (it.hasNext()) {
                    JSONObject jSONObject = new JSONObject(it.next());
                    String string = jSONObject.getString("productId");
                    BigDecimal divide = new BigDecimal(jSONObject.getString("price_amount_micros")).divide(new BigDecimal((int) PlaybackException.CUSTOM_ERROR_CODE_BASE));
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("sku", string);
                    jSONObject2.put("iso", jSONObject.getString("price_currency_code"));
                    jSONObject2.put("amount", divide.toString());
                    hashMap.put(string, jSONObject2);
                }
                JSONArray jSONArray = new JSONArray();
                Iterator<String> it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    String next = it2.next();
                    if (hashMap.containsKey(next)) {
                        jSONArray.put(hashMap.get(next));
                    }
                }
                if (jSONArray.length() > 0) {
                    OneSignal.y1(jSONArray, this.g, new c(arrayList2));
                }
            }
        } catch (Throwable th) {
            OneSignal.b(OneSignal.LOG_LEVEL.WARN, "Failed to track IAP purchases", th);
        }
    }

    public void u() {
        if (this.a == null) {
            this.a = new a();
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            this.e.bindService(intent, this.a, 1);
        } else if (this.b != null) {
            b();
        }
    }
}
