package com.onesignal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.onesignal.OSInAppMessageAction;
import com.onesignal.OneSignal;
import com.onesignal.d0;
import com.onesignal.t;
import com.onesignal.v0;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class OSInAppMessageController extends r implements t.c, v0.c {
    public static final Object u = new Object();
    public static ArrayList<String> v = new ArrayList<String>() { // from class: com.onesignal.OSInAppMessageController.1
        {
            add("android");
            add("app");
            add("all");
        }
    };
    public final yj2 a;
    public final w0 b;
    public final cy1 c;
    public v0 d;
    public d0 e;
    public y0 f;
    public final Set<String> h;
    public final Set<String> i;
    public final Set<String> j;
    public final Set<String> k;
    public final ArrayList<x> l;
    public Date t;
    public List<x> m = null;
    public a0 n = null;
    public boolean o = true;
    public boolean p = false;
    public String q = null;
    public qj2 r = null;
    public boolean s = false;
    public ArrayList<x> g = new ArrayList<>();

    /* loaded from: classes2.dex */
    public class a implements d0.i {
        public final /* synthetic */ String a;
        public final /* synthetic */ x b;

        public a(String str, x xVar) {
            this.a = str;
            this.b = xVar;
        }

        @Override // com.onesignal.d0.i
        public void a(String str) {
        }

        @Override // com.onesignal.d0.i
        public void b(String str) {
            OSInAppMessageController.this.k.remove(this.a);
            this.b.m(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends dm {
        public final /* synthetic */ x a;

        public b(x xVar) {
            this.a = xVar;
        }

        @Override // defpackage.dm, java.lang.Runnable
        public void run() {
            super.run();
            OSInAppMessageController.this.e.A(this.a);
            OSInAppMessageController.this.e.B(OSInAppMessageController.this.t);
        }
    }

    /* loaded from: classes2.dex */
    public class c implements OneSignal.x {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ x b;

        public c(boolean z, x xVar) {
            this.a = z;
            this.b = xVar;
        }

        @Override // com.onesignal.OneSignal.x
        public void a(JSONObject jSONObject) {
            OSInAppMessageController.this.s = false;
            if (jSONObject != null) {
                OSInAppMessageController.this.q = jSONObject.toString();
            }
            if (OSInAppMessageController.this.r != null) {
                if (!this.a) {
                    OneSignal.s0().k(this.b.a);
                }
                qj2 qj2Var = OSInAppMessageController.this.r;
                OSInAppMessageController oSInAppMessageController = OSInAppMessageController.this;
                qj2Var.h(oSInAppMessageController.t0(oSInAppMessageController.r.a()));
                WebViewManager.I(this.b, OSInAppMessageController.this.r);
                OSInAppMessageController.this.r = null;
            }
        }
    }

    /* loaded from: classes2.dex */
    public class d implements d0.i {
        public final /* synthetic */ x a;

        public d(x xVar) {
            this.a = xVar;
        }

        @Override // com.onesignal.d0.i
        public void a(String str) {
            try {
                qj2 h0 = OSInAppMessageController.this.h0(new JSONObject(str), this.a);
                if (h0.a() == null) {
                    OSInAppMessageController.this.a.debug("displayMessage:OnSuccess: No HTML retrieved from loadMessageContent");
                } else if (OSInAppMessageController.this.s) {
                    OSInAppMessageController.this.r = h0;
                } else {
                    OneSignal.s0().k(this.a.a);
                    OSInAppMessageController.this.f0(this.a);
                    h0.h(OSInAppMessageController.this.t0(h0.a()));
                    WebViewManager.I(this.a, h0);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override // com.onesignal.d0.i
        public void b(String str) {
            OSInAppMessageController.this.p = false;
            try {
                if (new JSONObject(str).getBoolean("retry")) {
                    OSInAppMessageController.this.k0(this.a);
                } else {
                    OSInAppMessageController.this.Y(this.a, true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class e implements d0.i {
        public final /* synthetic */ x a;

        public e(x xVar) {
            this.a = xVar;
        }

        @Override // com.onesignal.d0.i
        public void a(String str) {
            try {
                qj2 h0 = OSInAppMessageController.this.h0(new JSONObject(str), this.a);
                if (h0.a() == null) {
                    OSInAppMessageController.this.a.debug("displayPreviewMessage:OnSuccess: No HTML retrieved from loadMessageContent");
                } else if (OSInAppMessageController.this.s) {
                    OSInAppMessageController.this.r = h0;
                } else {
                    OSInAppMessageController.this.f0(this.a);
                    h0.h(OSInAppMessageController.this.t0(h0.a()));
                    WebViewManager.I(this.a, h0);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override // com.onesignal.d0.i
        public void b(String str) {
            OSInAppMessageController.this.E(null);
        }
    }

    /* loaded from: classes2.dex */
    public class f extends dm {
        public f() {
        }

        @Override // defpackage.dm, java.lang.Runnable
        public void run() {
            super.run();
            OSInAppMessageController.this.e.h();
        }
    }

    /* loaded from: classes2.dex */
    public class g extends dm {
        public g() {
        }

        @Override // defpackage.dm, java.lang.Runnable
        public void run() {
            super.run();
            synchronized (OSInAppMessageController.u) {
                OSInAppMessageController oSInAppMessageController = OSInAppMessageController.this;
                oSInAppMessageController.m = oSInAppMessageController.e.k();
                yj2 yj2Var = OSInAppMessageController.this.a;
                yj2Var.debug("Retrieved IAMs from DB redisplayedInAppMessages: " + OSInAppMessageController.this.m.toString());
            }
        }
    }

    /* loaded from: classes2.dex */
    public class h implements Runnable {
        public final /* synthetic */ JSONArray a;

        public h(JSONArray jSONArray) {
            this.a = jSONArray;
        }

        @Override // java.lang.Runnable
        public void run() {
            OSInAppMessageController.this.m0();
            try {
                OSInAppMessageController.this.j0(this.a);
            } catch (JSONException e) {
                OSInAppMessageController.this.a.error("ERROR processing InAppMessageJson JSON Response.", e);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class i implements Runnable {
        public i() {
        }

        @Override // java.lang.Runnable
        public void run() {
            OSInAppMessageController.this.a.debug("Delaying evaluateInAppMessages due to redisplay data not retrieved yet");
            OSInAppMessageController.this.H();
        }
    }

    /* loaded from: classes2.dex */
    public class j implements d0.i {
        public final /* synthetic */ x a;

        public j(x xVar) {
            this.a = xVar;
        }

        @Override // com.onesignal.d0.i
        public void a(String str) {
        }

        @Override // com.onesignal.d0.i
        public void b(String str) {
            OSInAppMessageController.this.i.remove(this.a.a);
        }
    }

    /* loaded from: classes2.dex */
    public class k implements OneSignal.c0 {
        public final /* synthetic */ x a;
        public final /* synthetic */ List b;

        public k(x xVar, List list) {
            this.a = xVar;
            this.b = list;
        }

        @Override // com.onesignal.OneSignal.c0
        public void a(OneSignal.PromptActionResult promptActionResult) {
            OSInAppMessageController.this.n = null;
            yj2 yj2Var = OSInAppMessageController.this.a;
            yj2Var.debug("IAM prompt to handle finished with result: " + promptActionResult);
            x xVar = this.a;
            if (!xVar.k || promptActionResult != OneSignal.PromptActionResult.LOCATION_PERMISSIONS_MISSING_MANIFEST) {
                OSInAppMessageController.this.s0(xVar, this.b);
            } else {
                OSInAppMessageController.this.r0(xVar, this.b);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class l implements DialogInterface.OnClickListener {
        public final /* synthetic */ x a;
        public final /* synthetic */ List f0;

        public l(x xVar, List list) {
            this.a = xVar;
            this.f0 = list;
        }

        @Override // android.content.DialogInterface.OnClickListener
        public void onClick(DialogInterface dialogInterface, int i) {
            OSInAppMessageController.this.s0(this.a, this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class m implements Runnable {
        public final /* synthetic */ String a;
        public final /* synthetic */ OSInAppMessageAction f0;

        public m(OSInAppMessageController oSInAppMessageController, String str, OSInAppMessageAction oSInAppMessageAction) {
            this.a = str;
            this.f0 = oSInAppMessageAction;
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.s0().h(this.a);
            OneSignal.s.a(this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class n implements d0.i {
        public final /* synthetic */ String a;

        public n(String str) {
            this.a = str;
        }

        @Override // com.onesignal.d0.i
        public void a(String str) {
        }

        @Override // com.onesignal.d0.i
        public void b(String str) {
            OSInAppMessageController.this.j.remove(this.a);
        }
    }

    public OSInAppMessageController(a1 a1Var, w0 w0Var, yj2 yj2Var, vk2 vk2Var, cy1 cy1Var) {
        this.t = null;
        this.b = w0Var;
        Set<String> K = OSUtils.K();
        this.h = K;
        this.l = new ArrayList<>();
        Set<String> K2 = OSUtils.K();
        this.i = K2;
        Set<String> K3 = OSUtils.K();
        this.j = K3;
        Set<String> K4 = OSUtils.K();
        this.k = K4;
        this.f = new y0(this);
        this.d = new v0(this);
        this.c = cy1Var;
        this.a = yj2Var;
        d0 P = P(a1Var, yj2Var, vk2Var);
        this.e = P;
        Set<String> m2 = P.m();
        if (m2 != null) {
            K.addAll(m2);
        }
        Set<String> p = this.e.p();
        if (p != null) {
            K2.addAll(p);
        }
        Set<String> s = this.e.s();
        if (s != null) {
            K3.addAll(s);
        }
        Set<String> l2 = this.e.l();
        if (l2 != null) {
            K4.addAll(l2);
        }
        Date q = this.e.q();
        if (q != null) {
            this.t = q;
        }
        S();
    }

    public final void B() {
        synchronized (this.l) {
            if (!this.d.c()) {
                this.a.b("In app message not showing due to system condition not correct");
                return;
            }
            yj2 yj2Var = this.a;
            yj2Var.debug("displayFirstIAMOnQueue: " + this.l);
            if (this.l.size() > 0 && !U()) {
                this.a.debug("No IAM showing currently, showing first item in the queue!");
                F(this.l.get(0));
                return;
            }
            yj2 yj2Var2 = this.a;
            yj2Var2.debug("In app message is currently showing or there are no IAMs left in the queue! isInAppMessageShowing: " + U());
        }
    }

    public final void C(x xVar, List<a0> list) {
        if (list.size() > 0) {
            yj2 yj2Var = this.a;
            yj2Var.debug("IAM showing prompts from IAM: " + xVar.toString());
            WebViewManager.x();
            s0(xVar, list);
        }
    }

    public void D() {
        d(new f(), "OS_IAM_DB_ACCESS");
    }

    public final void E(x xVar) {
        OneSignal.s0().i();
        if (q0()) {
            this.a.debug("Stop evaluateMessageDisplayQueue because prompt is currently displayed");
            return;
        }
        this.p = false;
        synchronized (this.l) {
            if (xVar != null) {
                if (!xVar.k && this.l.size() > 0) {
                    if (!this.l.contains(xVar)) {
                        this.a.debug("Message already removed from the queue!");
                        return;
                    }
                    String str = this.l.remove(0).a;
                    yj2 yj2Var = this.a;
                    yj2Var.debug("In app message with id: " + str + ", dismissed (removed) from the queue!");
                }
            }
            if (this.l.size() > 0) {
                yj2 yj2Var2 = this.a;
                yj2Var2.debug("In app message on queue available: " + this.l.get(0).a);
                F(this.l.get(0));
            } else {
                this.a.debug("In app message dismissed evaluating messages");
                H();
            }
        }
    }

    public final void F(x xVar) {
        if (!this.o) {
            this.a.a("In app messaging is currently paused, in app messages will not be shown!");
            return;
        }
        this.p = true;
        Q(xVar, false);
        this.e.n(OneSignal.g, xVar.a, u0(xVar), new d(xVar));
    }

    public void G(String str) {
        this.p = true;
        x xVar = new x(true);
        Q(xVar, true);
        this.e.o(OneSignal.g, str, new e(xVar));
    }

    public final void H() {
        this.a.debug("Starting evaluateInAppMessages");
        if (p0()) {
            this.b.c(new i());
            return;
        }
        Iterator<x> it = this.g.iterator();
        while (it.hasNext()) {
            x next = it.next();
            if (this.f.b(next)) {
                o0(next);
                if (!this.h.contains(next.a) && !next.h()) {
                    k0(next);
                }
            }
        }
    }

    public void I(Runnable runnable) {
        synchronized (u) {
            if (p0()) {
                this.a.debug("Delaying task due to redisplay data not retrieved yet");
                this.b.c(runnable);
            } else {
                runnable.run();
            }
        }
    }

    public final void J(OSInAppMessageAction oSInAppMessageAction) {
        if (oSInAppMessageAction.b() == null || oSInAppMessageAction.b().isEmpty()) {
            return;
        }
        if (oSInAppMessageAction.f() == OSInAppMessageAction.OSInAppMessageActionUrlType.BROWSER) {
            OSUtils.N(oSInAppMessageAction.b());
        } else if (oSInAppMessageAction.f() == OSInAppMessageAction.OSInAppMessageActionUrlType.IN_APP_WEBVIEW) {
            an2.b(oSInAppMessageAction.b(), true);
        }
    }

    public final void K(String str, List<z> list) {
        OneSignal.s0().h(str);
        OneSignal.x1(list);
    }

    public final void L(String str, OSInAppMessageAction oSInAppMessageAction) {
        if (OneSignal.s == null) {
            return;
        }
        OSUtils.S(new m(this, str, oSInAppMessageAction));
    }

    public final void M(x xVar, OSInAppMessageAction oSInAppMessageAction) {
        String u0 = u0(xVar);
        if (u0 == null) {
            return;
        }
        String a2 = oSInAppMessageAction.a();
        if ((xVar.e().e() && xVar.f(a2)) || !this.k.contains(a2)) {
            this.k.add(a2);
            xVar.a(a2);
            this.e.D(OneSignal.g, OneSignal.z0(), u0, new OSUtils().e(), xVar.a, a2, oSInAppMessageAction.g(), this.k, new a(a2, xVar));
        }
    }

    public final void N(x xVar, sj2 sj2Var) {
        String u0 = u0(xVar);
        if (u0 == null) {
            return;
        }
        String a2 = sj2Var.a();
        String str = xVar.a + a2;
        if (this.j.contains(str)) {
            this.a.a("Already sent page impression for id: " + a2);
            return;
        }
        this.j.add(str);
        this.e.F(OneSignal.g, OneSignal.z0(), u0, new OSUtils().e(), xVar.a, a2, this.j, new n(str));
    }

    public final void O(OSInAppMessageAction oSInAppMessageAction) {
        if (oSInAppMessageAction.e() != null) {
            e0 e2 = oSInAppMessageAction.e();
            if (e2.a() != null) {
                OneSignal.z1(e2.a());
            }
            if (e2.b() != null) {
                OneSignal.E(e2.b(), null);
            }
        }
    }

    public d0 P(a1 a1Var, yj2 yj2Var, vk2 vk2Var) {
        if (this.e == null) {
            this.e = new d0(a1Var, yj2Var, vk2Var);
        }
        return this.e;
    }

    public final void Q(x xVar, boolean z) {
        this.s = false;
        if (z || xVar.d()) {
            this.s = true;
            OneSignal.v0(new c(z, xVar));
        }
    }

    public final boolean R(x xVar) {
        if (this.f.e(xVar)) {
            return !xVar.g();
        }
        return xVar.i() || (!xVar.g() && xVar.c.isEmpty());
    }

    public void S() {
        this.b.c(new g());
        this.b.f();
    }

    public void T() {
        if (!this.g.isEmpty()) {
            yj2 yj2Var = this.a;
            yj2Var.debug("initWithCachedInAppMessages with already in memory messages: " + this.g);
            return;
        }
        String r = this.e.r();
        yj2 yj2Var2 = this.a;
        yj2Var2.debug("initWithCachedInAppMessages: " + r);
        if (r == null || r.isEmpty()) {
            return;
        }
        synchronized (u) {
            try {
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            if (this.g.isEmpty()) {
                j0(new JSONArray(r));
            }
        }
    }

    public boolean U() {
        return this.p;
    }

    public final void V(OSInAppMessageAction oSInAppMessageAction) {
        if (oSInAppMessageAction.e() != null) {
            yj2 yj2Var = this.a;
            yj2Var.debug("Tags detected inside of the action click payload, ignoring because action came from IAM preview:: " + oSInAppMessageAction.e().toString());
        }
        if (oSInAppMessageAction.c().size() > 0) {
            yj2 yj2Var2 = this.a;
            yj2Var2.debug("Outcomes detected inside of the action click payload, ignoring because action came from IAM preview: " + oSInAppMessageAction.c().toString());
        }
    }

    public final void W(Collection<String> collection) {
        Iterator<x> it = this.g.iterator();
        while (it.hasNext()) {
            x next = it.next();
            if (!next.i() && this.m.contains(next) && this.f.d(next, collection)) {
                yj2 yj2Var = this.a;
                yj2Var.debug("Trigger changed for message: " + next.toString());
                next.p(true);
            }
        }
    }

    public void X(x xVar) {
        Y(xVar, false);
    }

    public void Y(x xVar, boolean z) {
        if (!xVar.k) {
            this.h.add(xVar.a);
            if (!z) {
                this.e.x(this.h);
                this.t = new Date();
                i0(xVar);
            }
            yj2 yj2Var = this.a;
            yj2Var.debug("OSInAppMessageController messageWasDismissed dismissedMessages: " + this.h.toString());
        }
        if (!q0()) {
            b0(xVar);
        }
        E(xVar);
    }

    public void Z(x xVar, JSONObject jSONObject) throws JSONException {
        OSInAppMessageAction oSInAppMessageAction = new OSInAppMessageAction(jSONObject);
        oSInAppMessageAction.j(xVar.q());
        L(xVar.a, oSInAppMessageAction);
        C(xVar, oSInAppMessageAction.d());
        J(oSInAppMessageAction);
        M(xVar, oSInAppMessageAction);
        O(oSInAppMessageAction);
        K(xVar.a, oSInAppMessageAction.c());
    }

    @Override // com.onesignal.t.c
    public void a() {
        this.a.debug("messageTriggerConditionChanged called");
        H();
    }

    public void a0(x xVar, JSONObject jSONObject) throws JSONException {
        OSInAppMessageAction oSInAppMessageAction = new OSInAppMessageAction(jSONObject);
        oSInAppMessageAction.j(xVar.q());
        L(xVar.a, oSInAppMessageAction);
        C(xVar, oSInAppMessageAction.d());
        J(oSInAppMessageAction);
        V(oSInAppMessageAction);
    }

    @Override // com.onesignal.t.c
    public void b(String str) {
        yj2 yj2Var = this.a;
        yj2Var.debug("messageDynamicTriggerCompleted called with triggerId: " + str);
        HashSet hashSet = new HashSet();
        hashSet.add(str);
        W(hashSet);
    }

    public void b0(x xVar) {
        this.a.a("OSInAppMessageController onMessageDidDismiss: inAppMessageLifecycleHandler is null");
    }

    @Override // com.onesignal.v0.c
    public void c() {
        B();
    }

    public void c0(x xVar) {
        this.a.a("OSInAppMessageController onMessageDidDisplay: inAppMessageLifecycleHandler is null");
    }

    public void d0(x xVar) {
        c0(xVar);
        if (xVar.k || this.i.contains(xVar.a)) {
            return;
        }
        this.i.add(xVar.a);
        String u0 = u0(xVar);
        if (u0 == null) {
            return;
        }
        this.e.E(OneSignal.g, OneSignal.z0(), u0, new OSUtils().e(), xVar.a, this.i, new j(xVar));
    }

    public void e0(x xVar) {
        this.a.a("OSInAppMessageController onMessageWillDismiss: inAppMessageLifecycleHandler is null");
    }

    public void f0(x xVar) {
        this.a.a("OSInAppMessageController onMessageWillDisplay: inAppMessageLifecycleHandler is null");
    }

    public void g0(x xVar, JSONObject jSONObject) {
        sj2 sj2Var = new sj2(jSONObject);
        if (xVar.k) {
            return;
        }
        N(xVar, sj2Var);
    }

    public final qj2 h0(JSONObject jSONObject, x xVar) {
        qj2 qj2Var = new qj2(jSONObject);
        xVar.n(qj2Var.b().doubleValue());
        return qj2Var;
    }

    public final void i0(x xVar) {
        xVar.e().h(OneSignal.w0().a() / 1000);
        xVar.e().c();
        xVar.p(false);
        xVar.o(true);
        d(new b(xVar), "OS_IAM_DB_ACCESS");
        int indexOf = this.m.indexOf(xVar);
        if (indexOf != -1) {
            this.m.set(indexOf, xVar);
        } else {
            this.m.add(xVar);
        }
        yj2 yj2Var = this.a;
        yj2Var.debug("persistInAppMessageForRedisplay: " + xVar.toString() + " with msg array data: " + this.m.toString());
    }

    public final void j0(JSONArray jSONArray) throws JSONException {
        synchronized (u) {
            ArrayList<x> arrayList = new ArrayList<>();
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                x xVar = new x(jSONArray.getJSONObject(i2));
                if (xVar.a != null) {
                    arrayList.add(xVar);
                }
            }
            this.g = arrayList;
        }
        H();
    }

    public final void k0(x xVar) {
        synchronized (this.l) {
            if (!this.l.contains(xVar)) {
                this.l.add(xVar);
                yj2 yj2Var = this.a;
                yj2Var.debug("In app message with id: " + xVar.a + ", added to the queue");
            }
            B();
        }
    }

    public void l0(JSONArray jSONArray) throws JSONException {
        this.e.y(jSONArray.toString());
        I(new h(jSONArray));
    }

    public final void m0() {
        for (x xVar : this.m) {
            xVar.o(false);
        }
    }

    public void n0() {
        t.e();
    }

    public final void o0(x xVar) {
        boolean contains = this.h.contains(xVar.a);
        int indexOf = this.m.indexOf(xVar);
        if (!contains || indexOf == -1) {
            return;
        }
        x xVar2 = this.m.get(indexOf);
        xVar.e().g(xVar2.e());
        xVar.o(xVar2.g());
        boolean R = R(xVar);
        yj2 yj2Var = this.a;
        yj2Var.debug("setDataForRedisplay: " + xVar.toString() + " triggerHasChanged: " + R);
        if (R && xVar.e().d() && xVar.e().i()) {
            yj2 yj2Var2 = this.a;
            yj2Var2.debug("setDataForRedisplay message available for redisplay: " + xVar.a);
            this.h.remove(xVar.a);
            this.i.remove(xVar.a);
            this.j.clear();
            this.e.C(this.j);
            xVar.b();
        }
    }

    public boolean p0() {
        boolean z;
        synchronized (u) {
            z = this.m == null && this.b.e();
        }
        return z;
    }

    public final boolean q0() {
        return this.n != null;
    }

    public final void r0(x xVar, List<a0> list) {
        String string = OneSignal.e.getString(s13.location_permission_missing_title);
        new AlertDialog.Builder(OneSignal.Q()).setTitle(string).setMessage(OneSignal.e.getString(s13.location_permission_missing_message)).setPositiveButton(17039370, new l(xVar, list)).show();
    }

    public final void s0(x xVar, List<a0> list) {
        Iterator<a0> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            a0 next = it.next();
            if (!next.c()) {
                this.n = next;
                break;
            }
        }
        if (this.n != null) {
            yj2 yj2Var = this.a;
            yj2Var.debug("IAM prompt to handle: " + this.n.toString());
            this.n.d(true);
            this.n.b(new k(xVar, list));
            return;
        }
        yj2 yj2Var2 = this.a;
        yj2Var2.debug("No IAM prompt to handle, dismiss message: " + xVar.a);
        X(xVar);
    }

    public String t0(String str) {
        String str2 = this.q;
        return str + String.format("\n\n<script>\n    setPlayerTags(%s);\n</script>", str2);
    }

    public final String u0(x xVar) {
        String b2 = this.c.b();
        Iterator<String> it = v.iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (xVar.b.containsKey(next)) {
                HashMap<String, String> hashMap = xVar.b.get(next);
                if (hashMap.containsKey(b2)) {
                    return hashMap.get(b2);
                }
                return hashMap.get("default");
            }
        }
        return null;
    }
}
