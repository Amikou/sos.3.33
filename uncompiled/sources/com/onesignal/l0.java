package com.onesignal;

import com.onesignal.OneSignal;

/* compiled from: OSNotificationOpenedResult.java */
/* loaded from: classes2.dex */
public class l0 implements OneSignal.v {
    public final x0 a;
    public final Runnable b;
    public g0 c;
    public OSNotificationAction d;
    public boolean e = false;

    /* compiled from: OSNotificationOpenedResult.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "Running complete from OSNotificationOpenedResult timeout runnable!");
            l0.this.c(false);
        }
    }

    public l0(g0 g0Var, OSNotificationAction oSNotificationAction) {
        this.c = g0Var;
        this.d = oSNotificationAction;
        x0 b = x0.b();
        this.a = b;
        a aVar = new a();
        this.b = aVar;
        b.c(5000L, aVar);
    }

    @Override // com.onesignal.OneSignal.v
    public void a(OneSignal.AppEntryAction appEntryAction) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, "OSNotificationOpenedResult onEntryStateChange called with appEntryState: " + appEntryAction);
        c(OneSignal.AppEntryAction.APP_CLOSE.equals(appEntryAction));
    }

    public final void c(boolean z) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, "OSNotificationOpenedResult complete called with opened: " + z);
        this.a.a(this.b);
        if (this.e) {
            OneSignal.d1(log_level, "OSNotificationOpenedResult already completed");
            return;
        }
        this.e = true;
        if (z) {
            OneSignal.z(this.c.g());
        }
        OneSignal.n1(this);
    }

    public g0 d() {
        return this.c;
    }

    public String toString() {
        return "OSNotificationOpenedResult{notification=" + this.c + ", action=" + this.d + ", isComplete=" + this.e + '}';
    }
}
