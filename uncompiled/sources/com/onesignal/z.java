package com.onesignal;

import com.github.mikephil.charting.utils.Utils;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: OSInAppMessageOutcome.java */
/* loaded from: classes2.dex */
public class z {
    public String a;
    public float b;
    public boolean c;

    public z(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.getString(PublicResolver.FUNC_NAME);
        this.b = jSONObject.has("weight") ? (float) jSONObject.getDouble("weight") : Utils.FLOAT_EPSILON;
        this.c = jSONObject.has("unique") && jSONObject.getBoolean("unique");
    }

    public String a() {
        return this.a;
    }

    public float b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }

    public String toString() {
        return "OSInAppMessageOutcome{name='" + this.a + "', weight=" + this.b + ", unique=" + this.c + '}';
    }
}
