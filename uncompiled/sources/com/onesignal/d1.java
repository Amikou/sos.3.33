package com.onesignal;

import java.io.IOException;
import java.lang.Thread;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

/* compiled from: OneSignalRestClient.java */
/* loaded from: classes2.dex */
public class d1 {

    /* compiled from: OneSignalRestClient.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ String a;
        public final /* synthetic */ JSONObject f0;
        public final /* synthetic */ g g0;

        public a(String str, JSONObject jSONObject, g gVar) {
            this.a = str;
            this.f0 = jSONObject;
            this.g0 = gVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            d1.h(this.a, "PUT", this.f0, this.g0, 120000, null);
        }
    }

    /* compiled from: OneSignalRestClient.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ String a;
        public final /* synthetic */ JSONObject f0;
        public final /* synthetic */ g g0;

        public b(String str, JSONObject jSONObject, g gVar) {
            this.a = str;
            this.f0 = jSONObject;
            this.g0 = gVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            d1.h(this.a, "POST", this.f0, this.g0, 120000, null);
        }
    }

    /* compiled from: OneSignalRestClient.java */
    /* loaded from: classes2.dex */
    public class c implements Runnable {
        public final /* synthetic */ String a;
        public final /* synthetic */ g f0;
        public final /* synthetic */ String g0;

        public c(String str, g gVar, String str2) {
            this.a = str;
            this.f0 = gVar;
            this.g0 = str2;
        }

        @Override // java.lang.Runnable
        public void run() {
            d1.h(this.a, null, null, this.f0, 60000, this.g0);
        }
    }

    /* compiled from: OneSignalRestClient.java */
    /* loaded from: classes2.dex */
    public class d implements Runnable {
        public final /* synthetic */ Thread[] a;
        public final /* synthetic */ String f0;
        public final /* synthetic */ String g0;
        public final /* synthetic */ JSONObject h0;
        public final /* synthetic */ g i0;
        public final /* synthetic */ int j0;
        public final /* synthetic */ String k0;

        public d(Thread[] threadArr, String str, String str2, JSONObject jSONObject, g gVar, int i, String str3) {
            this.a = threadArr;
            this.f0 = str;
            this.g0 = str2;
            this.h0 = jSONObject;
            this.i0 = gVar;
            this.j0 = i;
            this.k0 = str3;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a[0] = d1.n(this.f0, this.g0, this.h0, this.i0, this.j0, this.k0);
        }
    }

    /* compiled from: OneSignalRestClient.java */
    /* loaded from: classes2.dex */
    public class e implements Runnable {
        public final /* synthetic */ g a;
        public final /* synthetic */ String f0;

        public e(g gVar, String str) {
            this.a = gVar;
            this.f0 = str;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.b(this.f0);
        }
    }

    /* compiled from: OneSignalRestClient.java */
    /* loaded from: classes2.dex */
    public class f implements Runnable {
        public final /* synthetic */ g a;
        public final /* synthetic */ int f0;
        public final /* synthetic */ String g0;
        public final /* synthetic */ Throwable h0;

        public f(g gVar, int i, String str, Throwable th) {
            this.a = gVar;
            this.f0 = i;
            this.g0 = str;
            this.h0 = th;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.a(this.f0, this.g0, this.h0);
        }
    }

    /* compiled from: OneSignalRestClient.java */
    /* loaded from: classes2.dex */
    public static abstract class g {
        public void a(int i, String str, Throwable th) {
        }

        public void b(String str) {
        }
    }

    public static Thread c(g gVar, int i, String str, Throwable th) {
        if (gVar == null) {
            return null;
        }
        Thread thread = new Thread(new f(gVar, i, str, th), "OS_REST_FAILURE_CALLBACK");
        thread.start();
        return thread;
    }

    public static Thread d(g gVar, String str) {
        if (gVar == null) {
            return null;
        }
        Thread thread = new Thread(new e(gVar, str), "OS_REST_SUCCESS_CALLBACK");
        thread.start();
        return thread;
    }

    public static void e(String str, g gVar, String str2) {
        new Thread(new c(str, gVar, str2), "OS_REST_ASYNC_GET").start();
    }

    public static void f(String str, g gVar, String str2) {
        h(str, null, null, gVar, 60000, str2);
    }

    public static int g(int i) {
        return i + 5000;
    }

    public static void h(String str, String str2, JSONObject jSONObject, g gVar, int i, String str3) {
        if (!OSUtils.H()) {
            if (str2 == null || !OneSignal.L1(null)) {
                Thread[] threadArr = new Thread[1];
                Thread thread = new Thread(new d(threadArr, str, str2, jSONObject, gVar, i, str3), "OS_HTTPConnection");
                thread.start();
                try {
                    thread.join(g(i));
                    if (thread.getState() != Thread.State.TERMINATED) {
                        thread.interrupt();
                    }
                    if (threadArr[0] != null) {
                        threadArr[0].join();
                        return;
                    }
                    return;
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                    return;
                }
            }
            return;
        }
        throw new OSThrowable$OSMainThreadException("Method: " + str2 + " was called from the Main Thread!");
    }

    public static HttpURLConnection i(String str) throws IOException {
        return (HttpURLConnection) new URL("https://api.onesignal.com/" + str).openConnection();
    }

    public static void j(String str, JSONObject jSONObject, g gVar) {
        new Thread(new b(str, jSONObject, gVar), "OS_REST_ASYNC_POST").start();
    }

    public static void k(String str, JSONObject jSONObject, g gVar) {
        h(str, "POST", jSONObject, gVar, 120000, null);
    }

    public static void l(String str, JSONObject jSONObject, g gVar) {
        new Thread(new a(str, jSONObject, gVar), "OS_REST_ASYNC_PUT").start();
    }

    public static void m(String str, JSONObject jSONObject, g gVar) {
        h(str, "PUT", jSONObject, gVar, 120000, null);
    }

    /* JADX WARN: Code restructure failed: missing block: B:81:0x02c3, code lost:
        if (r10 != null) goto L40;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static java.lang.Thread n(java.lang.String r16, java.lang.String r17, org.json.JSONObject r18, com.onesignal.d1.g r19, int r20, java.lang.String r21) {
        /*
            Method dump skipped, instructions count: 718
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.d1.n(java.lang.String, java.lang.String, org.json.JSONObject, com.onesignal.d1$g, int, java.lang.String):java.lang.Thread");
    }
}
