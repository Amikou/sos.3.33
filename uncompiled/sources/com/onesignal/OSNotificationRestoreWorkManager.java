package com.onesignal;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c;
import com.onesignal.OneSignal;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class OSNotificationRestoreWorkManager {
    public static final String[] a = {"notification_id", "android_notification_id", "full_data", "created_time"};
    public static final String b = NotificationRestoreWorker.class.getCanonicalName();
    public static boolean c;

    /* loaded from: classes2.dex */
    public static class NotificationRestoreWorker extends Worker {
        public NotificationRestoreWorker(Context context, WorkerParameters workerParameters) {
            super(context, workerParameters);
        }

        @Override // androidx.work.Worker
        public ListenableWorker.a r() {
            Context a = a();
            if (OneSignal.e == null) {
                OneSignal.L0(a);
            }
            if (!OSUtils.a(a)) {
                return ListenableWorker.a.a();
            }
            if (OSNotificationRestoreWorkManager.c) {
                return ListenableWorker.a.a();
            }
            OSNotificationRestoreWorkManager.c = true;
            OneSignal.a(OneSignal.LOG_LEVEL.INFO, "Restoring notifications");
            a1 g = a1.g(a);
            StringBuilder m = a1.m();
            OSNotificationRestoreWorkManager.f(a, m);
            OSNotificationRestoreWorkManager.d(a, g, m);
            return ListenableWorker.a.c();
        }
    }

    public static void c(Context context, boolean z) {
        gq4.f(context).d(b, ExistingWorkPolicy.KEEP, new c.a(NotificationRestoreWorker.class).f(z ? 15 : 0, TimeUnit.SECONDS).b());
    }

    public static void d(Context context, a1 a1Var, StringBuilder sb) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
        OneSignal.a(log_level, "Querying DB for notifications to restore: " + sb.toString());
        Cursor cursor = null;
        try {
            cursor = a1Var.b("notification", a, sb.toString(), null, null, null, "_id DESC", m.a);
            e(context, cursor, 200);
            b.c(a1Var, context);
            if (cursor == null || cursor.isClosed()) {
            }
        } catch (Throwable th) {
            try {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error restoring notification records! ", th);
                if (cursor == null) {
                    return;
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
    }

    public static void e(Context context, Cursor cursor, int i) {
        if (cursor.moveToFirst()) {
            do {
                OSNotificationWorkManager.b(context, cursor.getString(cursor.getColumnIndex("notification_id")), cursor.getInt(cursor.getColumnIndex("android_notification_id")), cursor.getString(cursor.getColumnIndex("full_data")), cursor.getLong(cursor.getColumnIndex("created_time")), true, false);
                if (i > 0) {
                    OSUtils.V(i);
                }
            } while (cursor.moveToNext());
        }
    }

    public static void f(Context context, StringBuilder sb) {
        if (Build.VERSION.SDK_INT < 23) {
            return;
        }
        StatusBarNotification[] d = cn2.d(context);
        if (d.length == 0) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (StatusBarNotification statusBarNotification : d) {
            arrayList.add(Integer.valueOf(statusBarNotification.getId()));
        }
        sb.append(" AND android_notification_id NOT IN (");
        sb.append(TextUtils.join(",", arrayList));
        sb.append(")");
    }
}
