package com.onesignal;

import com.github.mikephil.charting.utils.Utils;
import com.onesignal.influence.domain.OSInfluenceType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSOutcomeEvent.java */
/* loaded from: classes2.dex */
public class o0 {
    public OSInfluenceType a;
    public JSONArray b;
    public String c;
    public long d;
    public Float e;

    public o0(OSInfluenceType oSInfluenceType, JSONArray jSONArray, String str, long j, float f) {
        this.a = oSInfluenceType;
        this.b = jSONArray;
        this.c = str;
        this.d = j;
        this.e = Float.valueOf(f);
    }

    public static o0 a(dk2 dk2Var) {
        JSONArray jSONArray;
        OSInfluenceType oSInfluenceType = OSInfluenceType.UNATTRIBUTED;
        if (dk2Var.b() != null) {
            pk2 b = dk2Var.b();
            if (b.a() != null && b.a().b() != null && b.a().b().length() > 0) {
                oSInfluenceType = OSInfluenceType.DIRECT;
                jSONArray = b.a().b();
            } else if (b.b() != null && b.b().b() != null && b.b().b().length() > 0) {
                oSInfluenceType = OSInfluenceType.INDIRECT;
                jSONArray = b.b().b();
            }
            return new o0(oSInfluenceType, jSONArray, dk2Var.a(), dk2Var.c(), dk2Var.d());
        }
        jSONArray = null;
        return new o0(oSInfluenceType, jSONArray, dk2Var.a(), dk2Var.c(), dk2Var.d());
    }

    public OSInfluenceType b() {
        return this.a;
    }

    public JSONObject c() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = this.b;
        if (jSONArray != null && jSONArray.length() > 0) {
            jSONObject.put("notification_ids", this.b);
        }
        jSONObject.put("id", this.c);
        if (this.e.floatValue() > Utils.FLOAT_EPSILON) {
            jSONObject.put("weight", this.e);
        }
        long j = this.d;
        if (j > 0) {
            jSONObject.put("timestamp", j);
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || o0.class != obj.getClass()) {
            return false;
        }
        o0 o0Var = (o0) obj;
        return this.a.equals(o0Var.a) && this.b.equals(o0Var.b) && this.c.equals(o0Var.c) && this.d == o0Var.d && this.e.equals(o0Var.e);
    }

    public int hashCode() {
        int i = 1;
        Object[] objArr = {this.a, this.b, this.c, Long.valueOf(this.d), this.e};
        for (int i2 = 0; i2 < 5; i2++) {
            Object obj = objArr[i2];
            i = (i * 31) + (obj == null ? 0 : obj.hashCode());
        }
        return i;
    }

    public String toString() {
        return "OutcomeEvent{session=" + this.a + ", notificationIds=" + this.b + ", name='" + this.c + "', timestamp=" + this.d + ", weight=" + this.e + '}';
    }
}
