package com.onesignal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import androidx.cardview.widget.CardView;
import com.github.mikephil.charting.utils.Utils;
import com.onesignal.OneSignal;
import com.onesignal.WebViewManager;
import com.onesignal.c;

/* compiled from: InAppMessageView.java */
/* loaded from: classes2.dex */
public class i {
    public static final int v = Color.parseColor("#00000000");
    public static final int w = Color.parseColor("#BB000000");
    public static final int x = z0.b(4);
    public PopupWindow a;
    public Activity b;
    public int e;
    public double j;
    public boolean k;
    public boolean n;
    public qj2 o;
    public WebViewManager.Position p;
    public WebView q;
    public RelativeLayout r;
    public com.onesignal.c s;
    public j t;
    public Runnable u;
    public final Handler c = new Handler();
    public int f = z0.b(24);
    public int g = z0.b(24);
    public int h = z0.b(24);
    public int i = z0.b(24);
    public boolean l = false;
    public boolean m = false;
    public int d = -1;

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ int a;

        public a(int i) {
            this.a = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (i.this.q != null) {
                ViewGroup.LayoutParams layoutParams = i.this.q.getLayoutParams();
                if (layoutParams == null) {
                    OneSignal.d1(OneSignal.LOG_LEVEL.WARN, "WebView height update skipped because of null layoutParams, new height will be used once it is displayed.");
                    return;
                }
                layoutParams.height = this.a;
                i.this.q.setLayoutParams(layoutParams);
                if (i.this.s != null) {
                    com.onesignal.c cVar = i.this.s;
                    i iVar = i.this;
                    cVar.i(iVar.F(this.a, iVar.p, i.this.n));
                    return;
                }
                return;
            }
            OneSignal.d1(OneSignal.LOG_LEVEL.WARN, "WebView height update skipped, new height will be used once it is displayed.");
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ RelativeLayout.LayoutParams a;
        public final /* synthetic */ RelativeLayout.LayoutParams f0;
        public final /* synthetic */ c.C0156c g0;
        public final /* synthetic */ WebViewManager.Position h0;

        public b(RelativeLayout.LayoutParams layoutParams, RelativeLayout.LayoutParams layoutParams2, c.C0156c c0156c, WebViewManager.Position position) {
            this.a = layoutParams;
            this.f0 = layoutParams2;
            this.g0 = c0156c;
            this.h0 = position;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (i.this.q == null) {
                return;
            }
            i.this.q.setLayoutParams(this.a);
            Context applicationContext = i.this.b.getApplicationContext();
            i.this.S(applicationContext, this.f0, this.g0);
            i.this.T(applicationContext);
            i iVar = i.this;
            iVar.H(iVar.r);
            if (i.this.t != null) {
                i iVar2 = i.this;
                iVar2.z(this.h0, iVar2.s, i.this.r);
            }
            i.this.Y();
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class c implements c.b {
        public c() {
        }

        @Override // com.onesignal.c.b
        public void a() {
            i.this.m = true;
        }

        @Override // com.onesignal.c.b
        public void b() {
            i.this.m = false;
        }

        @Override // com.onesignal.c.b
        public void onDismiss() {
            if (i.this.t != null) {
                i.this.t.c();
            }
            i.this.L(null);
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class d implements Runnable {
        public d() {
        }

        @Override // java.lang.Runnable
        public void run() {
            if (i.this.t != null) {
                i.this.t.c();
            }
            if (i.this.b == null) {
                i.this.l = true;
                return;
            }
            i.this.K(null);
            i.this.u = null;
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class e implements Runnable {
        public final /* synthetic */ Activity a;

        public e(Activity activity) {
            this.a = activity;
        }

        @Override // java.lang.Runnable
        public void run() {
            i.this.I(this.a);
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class f implements Runnable {
        public final /* synthetic */ WebViewManager.l a;

        public f(WebViewManager.l lVar) {
            this.a = lVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (!i.this.k || i.this.r == null) {
                i.this.C();
                WebViewManager.l lVar = this.a;
                if (lVar != null) {
                    lVar.a();
                    return;
                }
                return;
            }
            i iVar = i.this;
            iVar.v(iVar.r, this.a);
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class g implements Animation.AnimationListener {
        public final /* synthetic */ CardView a;

        public g(CardView cardView) {
            this.a = cardView;
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            if (Build.VERSION.SDK_INT == 23) {
                this.a.setCardElevation(z0.b(5));
            }
            if (i.this.t != null) {
                i.this.t.b();
            }
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationRepeat(Animation animation) {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationStart(Animation animation) {
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public class h extends AnimatorListenerAdapter {
        public final /* synthetic */ WebViewManager.l a;

        public h(WebViewManager.l lVar) {
            this.a = lVar;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            i.this.C();
            WebViewManager.l lVar = this.a;
            if (lVar != null) {
                lVar.a();
            }
        }
    }

    /* compiled from: InAppMessageView.java */
    /* renamed from: com.onesignal.i$i  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class C0160i {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WebViewManager.Position.values().length];
            a = iArr;
            try {
                iArr[WebViewManager.Position.TOP_BANNER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WebViewManager.Position.BOTTOM_BANNER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WebViewManager.Position.CENTER_MODAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[WebViewManager.Position.FULL_SCREEN.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* compiled from: InAppMessageView.java */
    /* loaded from: classes2.dex */
    public interface j {
        void a();

        void b();

        void c();
    }

    public i(WebView webView, qj2 qj2Var, boolean z) {
        this.n = false;
        this.q = webView;
        this.p = qj2Var.c();
        this.e = qj2Var.d();
        this.j = qj2Var.b() == null ? Utils.DOUBLE_EPSILON : qj2Var.b().doubleValue();
        this.k = !this.p.isBanner();
        this.n = z;
        this.o = qj2Var;
        Q(qj2Var);
    }

    public final void A(View view, int i, Animation.AnimationListener animationListener) {
        xm2.a(view, (-i) - this.h, Utils.FLOAT_EPSILON, 1000, new zm2(0.1d, 8.0d), animationListener).start();
    }

    public void B() {
        if (this.l) {
            this.l = false;
            L(null);
        }
    }

    public final void C() {
        P();
        j jVar = this.t;
        if (jVar != null) {
            jVar.a();
        }
    }

    public final Animation.AnimationListener D(CardView cardView) {
        return new g(cardView);
    }

    public final CardView E(Context context) {
        CardView cardView = new CardView(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, this.p == WebViewManager.Position.FULL_SCREEN ? -1 : -2);
        layoutParams.addRule(13);
        cardView.setLayoutParams(layoutParams);
        if (Build.VERSION.SDK_INT == 23) {
            cardView.setCardElevation(Utils.FLOAT_EPSILON);
        } else {
            cardView.setCardElevation(z0.b(5));
        }
        cardView.setRadius(z0.b(8));
        cardView.setClipChildren(false);
        cardView.setClipToPadding(false);
        cardView.setPreventCornerOverlap(false);
        cardView.setCardBackgroundColor(0);
        return cardView;
    }

    public final c.C0156c F(int i, WebViewManager.Position position, boolean z) {
        c.C0156c c0156c = new c.C0156c();
        c0156c.d = this.g;
        c0156c.b = this.h;
        c0156c.g = z;
        c0156c.e = i;
        N();
        int i2 = C0160i.a[position.ordinal()];
        if (i2 == 1) {
            c0156c.c = this.h - x;
        } else if (i2 != 2) {
            if (i2 != 3) {
                if (i2 == 4) {
                    i = N() - (this.i + this.h);
                    c0156c.e = i;
                }
            }
            int N = (N() / 2) - (i / 2);
            c0156c.c = x + N;
            c0156c.b = N;
            c0156c.a = N;
        } else {
            c0156c.a = N() - i;
            c0156c.c = this.i + x;
        }
        c0156c.f = position == WebViewManager.Position.TOP_BANNER ? 0 : 1;
        return c0156c;
    }

    public final RelativeLayout.LayoutParams G() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.d, -1);
        int i = C0160i.a[this.p.ordinal()];
        if (i == 1) {
            layoutParams.addRule(10);
            layoutParams.addRule(14);
        } else if (i == 2) {
            layoutParams.addRule(12);
            layoutParams.addRule(14);
        } else if (i == 3 || i == 4) {
            layoutParams.addRule(13);
        }
        return layoutParams;
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x003f, code lost:
        if (r5 != 4) goto L25;
     */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0051  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0054  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void H(android.widget.RelativeLayout r5) {
        /*
            r4 = this;
            android.widget.PopupWindow r0 = new android.widget.PopupWindow
            boolean r1 = r4.k
            r2 = -1
            if (r1 == 0) goto L9
            r3 = r2
            goto Lb
        L9:
            int r3 = r4.d
        Lb:
            if (r1 == 0) goto Le
            goto Lf
        Le:
            r2 = -2
        Lf:
            r1 = 1
            r0.<init>(r5, r3, r2, r1)
            r4.a = r0
            android.graphics.drawable.ColorDrawable r5 = new android.graphics.drawable.ColorDrawable
            r2 = 0
            r5.<init>(r2)
            r0.setBackgroundDrawable(r5)
            android.widget.PopupWindow r5 = r4.a
            r5.setTouchable(r1)
            android.widget.PopupWindow r5 = r4.a
            r5.setClippingEnabled(r2)
            boolean r5 = r4.k
            if (r5 != 0) goto L48
            int[] r5 = com.onesignal.i.C0160i.a
            com.onesignal.WebViewManager$Position r0 = r4.p
            int r0 = r0.ordinal()
            r5 = r5[r0]
            if (r5 == r1) goto L45
            r0 = 2
            if (r5 == r0) goto L42
            r0 = 3
            if (r5 == r0) goto L49
            r0 = 4
            if (r5 == r0) goto L49
            goto L48
        L42:
            r1 = 81
            goto L49
        L45:
            r1 = 49
            goto L49
        L48:
            r1 = r2
        L49:
            qj2 r5 = r4.o
            boolean r5 = r5.g()
            if (r5 == 0) goto L54
            r5 = 1000(0x3e8, float:1.401E-42)
            goto L56
        L54:
            r5 = 1003(0x3eb, float:1.406E-42)
        L56:
            android.widget.PopupWindow r0 = r4.a
            defpackage.nt2.b(r0, r5)
            android.widget.PopupWindow r5 = r4.a
            android.app.Activity r0 = r4.b
            android.view.Window r0 = r0.getWindow()
            android.view.View r0 = r0.getDecorView()
            android.view.View r0 = r0.getRootView()
            r5.showAtLocation(r0, r1, r2, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.i.H(android.widget.RelativeLayout):void");
    }

    public final void I(Activity activity) {
        if (z0.k(activity) && this.r == null) {
            W(activity);
        } else {
            new Handler().postDelayed(new e(activity), 200L);
        }
    }

    public final void J() {
        this.r = null;
        this.s = null;
        this.q = null;
    }

    public void K(WebViewManager.l lVar) {
        com.onesignal.c cVar = this.s;
        if (cVar == null) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "No host presenter to trigger dismiss animation, counting as dismissed already", new Throwable());
            J();
            if (lVar != null) {
                lVar.a();
                return;
            }
            return;
        }
        cVar.g();
        L(lVar);
    }

    public final void L(WebViewManager.l lVar) {
        OSUtils.R(new f(lVar), 600);
    }

    public WebViewManager.Position M() {
        return this.p;
    }

    public final int N() {
        return z0.f(this.b);
    }

    public boolean O() {
        return this.m;
    }

    public void P() {
        OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "InAppMessageView removing views");
        Runnable runnable = this.u;
        if (runnable != null) {
            this.c.removeCallbacks(runnable);
            this.u = null;
        }
        com.onesignal.c cVar = this.s;
        if (cVar != null) {
            cVar.removeAllViews();
        }
        PopupWindow popupWindow = this.a;
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
        J();
    }

    public final void Q(qj2 qj2Var) {
        this.h = qj2Var.e() ? z0.b(24) : 0;
        this.i = qj2Var.e() ? z0.b(24) : 0;
        this.f = qj2Var.f() ? z0.b(24) : 0;
        this.g = qj2Var.f() ? z0.b(24) : 0;
    }

    public void R(j jVar) {
        this.t = jVar;
    }

    public final void S(Context context, RelativeLayout.LayoutParams layoutParams, c.C0156c c0156c) {
        com.onesignal.c cVar = new com.onesignal.c(context);
        this.s = cVar;
        if (layoutParams != null) {
            cVar.setLayoutParams(layoutParams);
        }
        this.s.i(c0156c);
        this.s.h(new c());
        if (this.q.getParent() != null) {
            ((ViewGroup) this.q.getParent()).removeAllViews();
        }
        CardView E = E(context);
        E.setTag("IN_APP_MESSAGE_CARD_VIEW_TAG");
        E.addView(this.q);
        this.s.setPadding(this.f, this.h, this.g, this.i);
        this.s.setClipChildren(false);
        this.s.setClipToPadding(false);
        this.s.addView(E);
    }

    public final void T(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        this.r = relativeLayout;
        relativeLayout.setBackgroundDrawable(new ColorDrawable(0));
        this.r.setClipChildren(false);
        this.r.setClipToPadding(false);
        this.r.addView(this.s);
    }

    public void U(WebView webView) {
        this.q = webView;
        webView.setBackgroundColor(0);
    }

    public final void V(WebViewManager.Position position, RelativeLayout.LayoutParams layoutParams, RelativeLayout.LayoutParams layoutParams2, c.C0156c c0156c) {
        OSUtils.S(new b(layoutParams, layoutParams2, c0156c, position));
    }

    public void W(Activity activity) {
        this.b = activity;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, this.e);
        layoutParams.addRule(13);
        RelativeLayout.LayoutParams G = this.k ? G() : null;
        WebViewManager.Position position = this.p;
        V(position, layoutParams, G, F(this.e, position, this.n));
    }

    public void X(Activity activity) {
        I(activity);
    }

    public final void Y() {
        if (this.j > Utils.DOUBLE_EPSILON && this.u == null) {
            d dVar = new d();
            this.u = dVar;
            this.c.postDelayed(dVar, ((long) this.j) * 1000);
        }
    }

    public void Z(int i) {
        this.e = i;
        OSUtils.S(new a(i));
    }

    public String toString() {
        return "InAppMessageView{currentActivity=" + this.b + ", pageWidth=" + this.d + ", pageHeight=" + this.e + ", displayDuration=" + this.j + ", hasBackground=" + this.k + ", shouldDismissWhenActive=" + this.l + ", isDragging=" + this.m + ", disableDragDismiss=" + this.n + ", displayLocation=" + this.p + ", webView=" + this.q + '}';
    }

    public final void v(View view, WebViewManager.l lVar) {
        w(view, 400, w, v, new h(lVar)).start();
    }

    public final ValueAnimator w(View view, int i, int i2, int i3, Animator.AnimatorListener animatorListener) {
        return xm2.b(view, i, i2, i3, animatorListener);
    }

    public final void x(View view, int i, Animation.AnimationListener animationListener) {
        xm2.a(view, i + this.i, Utils.FLOAT_EPSILON, 1000, new zm2(0.1d, 8.0d), animationListener).start();
    }

    public final void y(View view, View view2, Animation.AnimationListener animationListener, Animator.AnimatorListener animatorListener) {
        Animation c2 = xm2.c(view, 1000, new zm2(0.1d, 8.0d), animationListener);
        ValueAnimator w2 = w(view2, 400, v, w, animatorListener);
        c2.start();
        w2.start();
    }

    public final void z(WebViewManager.Position position, View view, View view2) {
        CardView cardView = (CardView) view.findViewWithTag("IN_APP_MESSAGE_CARD_VIEW_TAG");
        Animation.AnimationListener D = D(cardView);
        int i = C0160i.a[position.ordinal()];
        if (i == 1) {
            A(cardView, this.q.getHeight(), D);
        } else if (i == 2) {
            x(cardView, this.q.getHeight(), D);
        } else if (i == 3 || i == 4) {
            y(view, view2, D, null);
        }
    }
}
