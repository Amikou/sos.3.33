package com.onesignal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import org.json.JSONObject;

/* compiled from: GenerateNotificationOpenIntentFromPushPayload.kt */
/* loaded from: classes2.dex */
public final class f {
    public static final f a = new f();

    public final se1 a(Context context, JSONObject jSONObject) {
        fs1.f(context, "context");
        fs1.f(jSONObject, "fcmPayload");
        bk2 bk2Var = new bk2(context, jSONObject);
        return new se1(context, b(bk2Var.b()), c(bk2Var.a(), jSONObject));
    }

    public final Intent b(Uri uri) {
        if (uri == null) {
            return null;
        }
        return OSUtils.O(uri);
    }

    public final boolean c(boolean z, JSONObject jSONObject) {
        return z | (tj2.a(jSONObject) != null);
    }
}
