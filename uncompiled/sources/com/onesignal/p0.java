package com.onesignal;

import com.github.mikephil.charting.utils.Utils;
import com.onesignal.OneSignal;
import com.onesignal.influence.domain.OSInfluenceChannel;
import com.onesignal.influence.domain.OSInfluenceType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: OSOutcomeEventsController.java */
/* loaded from: classes2.dex */
public class p0 {
    public Set<String> a;
    public final hk2 b;
    public final t0 c;

    /* compiled from: OSOutcomeEventsController.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            Thread.currentThread().setPriority(10);
            p0.this.b.b().f("notification", "notification_id");
        }
    }

    /* compiled from: OSOutcomeEventsController.java */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            Thread.currentThread().setPriority(10);
            for (dk2 dk2Var : p0.this.b.b().c()) {
                p0.this.p(dk2Var);
            }
        }
    }

    /* compiled from: OSOutcomeEventsController.java */
    /* loaded from: classes2.dex */
    public class c implements ym2 {
        public final /* synthetic */ dk2 a;

        public c(dk2 dk2Var) {
            this.a = dk2Var;
        }

        @Override // defpackage.ym2
        public void a(String str) {
            p0.this.b.b().e(this.a);
        }

        @Override // defpackage.ym2
        public void b(int i, String str, Throwable th) {
        }
    }

    /* compiled from: OSOutcomeEventsController.java */
    /* loaded from: classes2.dex */
    public class d implements ym2 {
        public final /* synthetic */ dk2 a;
        public final /* synthetic */ OneSignal.g0 b;
        public final /* synthetic */ long c;
        public final /* synthetic */ String d;

        /* compiled from: OSOutcomeEventsController.java */
        /* loaded from: classes2.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                Thread.currentThread().setPriority(10);
                d dVar = d.this;
                dVar.a.f(dVar.c);
                p0.this.b.b().b(d.this.a);
            }
        }

        public d(dk2 dk2Var, OneSignal.g0 g0Var, long j, String str) {
            this.a = dk2Var;
            this.b = g0Var;
            this.c = j;
            this.d = str;
        }

        @Override // defpackage.ym2
        public void a(String str) {
            p0.this.k(this.a);
            OneSignal.g0 g0Var = this.b;
            if (g0Var != null) {
                g0Var.a(o0.a(this.a));
            }
        }

        @Override // defpackage.ym2
        public void b(int i, String str, Throwable th) {
            new Thread(new a(), "OS_SAVE_OUTCOMES").start();
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.WARN;
            OneSignal.d1(log_level, "Sending outcome with name: " + this.d + " failed with status code: " + i + " and response: " + str + "\nOutcome event was cached and will be reattempted on app cold start");
            OneSignal.g0 g0Var = this.b;
            if (g0Var != null) {
                g0Var.a(null);
            }
        }
    }

    /* compiled from: OSOutcomeEventsController.java */
    /* loaded from: classes2.dex */
    public class e implements Runnable {
        public final /* synthetic */ dk2 a;

        public e(dk2 dk2Var) {
            this.a = dk2Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            Thread.currentThread().setPriority(10);
            p0.this.b.b().i(this.a);
        }
    }

    /* compiled from: OSOutcomeEventsController.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class f {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[OSInfluenceChannel.values().length];
            b = iArr;
            try {
                iArr[OSInfluenceChannel.IAM.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[OSInfluenceChannel.NOTIFICATION.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[OSInfluenceType.values().length];
            a = iArr2;
            try {
                iArr2[OSInfluenceType.DIRECT.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[OSInfluenceType.INDIRECT.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[OSInfluenceType.UNATTRIBUTED.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[OSInfluenceType.DISABLED.ordinal()] = 4;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    public p0(t0 t0Var, hk2 hk2Var) {
        this.c = t0Var;
        this.b = hk2Var;
        g();
    }

    public void d() {
        new Thread(new a(), "OS_DELETE_CACHED_UNIQUE_OUTCOMES_NOTIFICATIONS_THREAD").start();
    }

    public void e() {
        OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "OneSignal cleanOutcomes for session");
        this.a = OSUtils.K();
        j();
    }

    public final List<com.onesignal.influence.domain.a> f(String str, List<com.onesignal.influence.domain.a> list) {
        List<com.onesignal.influence.domain.a> a2 = this.b.b().a(str, list);
        if (a2.size() > 0) {
            return a2;
        }
        return null;
    }

    public final void g() {
        this.a = OSUtils.K();
        Set<String> h = this.b.b().h();
        if (h != null) {
            this.a = h;
        }
    }

    public final List<com.onesignal.influence.domain.a> h(List<com.onesignal.influence.domain.a> list) {
        ArrayList arrayList = new ArrayList(list);
        for (com.onesignal.influence.domain.a aVar : list) {
            if (aVar.d().isDisabled()) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.d1(log_level, "Outcomes disabled for channel: " + aVar.c().toString());
                arrayList.remove(aVar);
            }
        }
        return arrayList;
    }

    public final void i(dk2 dk2Var) {
        new Thread(new e(dk2Var), "OS_SAVE_UNIQUE_OUTCOME_NOTIFICATIONS").start();
    }

    public final void j() {
        this.b.b().d(this.a);
    }

    public final void k(dk2 dk2Var) {
        if (dk2Var.e()) {
            j();
        } else {
            i(dk2Var);
        }
    }

    public final void l(String str, float f2, List<com.onesignal.influence.domain.a> list, OneSignal.g0 g0Var) {
        long a2 = OneSignal.w0().a() / 1000;
        int e2 = new OSUtils().e();
        String str2 = OneSignal.g;
        boolean z = false;
        qk2 qk2Var = null;
        qk2 qk2Var2 = null;
        for (com.onesignal.influence.domain.a aVar : list) {
            int i = f.a[aVar.d().ordinal()];
            if (i == 1) {
                if (qk2Var == null) {
                    qk2Var = new qk2();
                }
                qk2Var = t(aVar, qk2Var);
            } else if (i == 2) {
                if (qk2Var2 == null) {
                    qk2Var2 = new qk2();
                }
                qk2Var2 = t(aVar, qk2Var2);
            } else if (i == 3) {
                z = true;
            } else if (i == 4) {
                OneSignal.a(OneSignal.LOG_LEVEL.VERBOSE, "Outcomes disabled for channel: " + aVar.c());
                if (g0Var != null) {
                    g0Var.a(null);
                    return;
                }
                return;
            }
        }
        if (qk2Var == null && qk2Var2 == null && !z) {
            OneSignal.a(OneSignal.LOG_LEVEL.VERBOSE, "Outcomes disabled for all channels");
            if (g0Var != null) {
                g0Var.a(null);
                return;
            }
            return;
        }
        dk2 dk2Var = new dk2(str, new pk2(qk2Var, qk2Var2), f2, 0L);
        this.b.b().g(str2, e2, dk2Var, new d(dk2Var, g0Var, a2, str));
    }

    public void m(List<z> list) {
        for (z zVar : list) {
            String a2 = zVar.a();
            if (zVar.c()) {
                r(a2, null);
            } else if (zVar.b() > Utils.FLOAT_EPSILON) {
                o(a2, zVar.b(), null);
            } else {
                n(a2, null);
            }
        }
    }

    public void n(String str, OneSignal.g0 g0Var) {
        l(str, Utils.FLOAT_EPSILON, this.c.e(), g0Var);
    }

    public void o(String str, float f2, OneSignal.g0 g0Var) {
        l(str, f2, this.c.e(), g0Var);
    }

    public final void p(dk2 dk2Var) {
        int e2 = new OSUtils().e();
        this.b.b().g(OneSignal.g, e2, dk2Var, new c(dk2Var));
    }

    public void q() {
        new Thread(new b(), "OS_SEND_SAVED_OUTCOMES").start();
    }

    public void r(String str, OneSignal.g0 g0Var) {
        s(str, this.c.e(), g0Var);
    }

    public final void s(String str, List<com.onesignal.influence.domain.a> list, OneSignal.g0 g0Var) {
        List<com.onesignal.influence.domain.a> h = h(list);
        if (h.isEmpty()) {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "Unique Outcome disabled for current session");
            return;
        }
        boolean z = false;
        Iterator<com.onesignal.influence.domain.a> it = h.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            } else if (it.next().d().isAttributed()) {
                z = true;
                break;
            }
        }
        if (z) {
            List<com.onesignal.influence.domain.a> f2 = f(str, h);
            if (f2 == null) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.a(log_level, "Measure endpoint will not send because unique outcome already sent for: \nSessionInfluences: " + h.toString() + "\nOutcome name: " + str);
                if (g0Var != null) {
                    g0Var.a(null);
                    return;
                }
                return;
            }
            l(str, Utils.FLOAT_EPSILON, f2, g0Var);
        } else if (this.a.contains(str)) {
            OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level2, "Measure endpoint will not send because unique outcome already sent for: \nSession: " + OSInfluenceType.UNATTRIBUTED + "\nOutcome name: " + str);
            if (g0Var != null) {
                g0Var.a(null);
            }
        } else {
            this.a.add(str);
            l(str, Utils.FLOAT_EPSILON, h, g0Var);
        }
    }

    public final qk2 t(com.onesignal.influence.domain.a aVar, qk2 qk2Var) {
        int i = f.b[aVar.c().ordinal()];
        if (i == 1) {
            qk2Var.c(aVar.b());
        } else if (i == 2) {
            qk2Var.d(aVar.b());
        }
        return qk2Var;
    }
}
