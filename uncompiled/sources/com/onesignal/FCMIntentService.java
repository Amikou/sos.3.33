package com.onesignal;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import androidx.legacy.content.WakefulBroadcastReceiver;
import com.onesignal.k;

/* loaded from: classes2.dex */
public class FCMIntentService extends IntentService {

    /* loaded from: classes2.dex */
    public class a implements k.e {
        public final /* synthetic */ Intent a;

        public a(FCMIntentService fCMIntentService, Intent intent) {
            this.a = intent;
        }

        @Override // com.onesignal.k.e
        public void a(k.f fVar) {
            WakefulBroadcastReceiver.b(this.a);
        }
    }

    public FCMIntentService() {
        super("FCMIntentService");
        setIntentRedelivery(true);
    }

    @Override // android.app.IntentService
    public void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return;
        }
        OneSignal.L0(this);
        k.h(this, extras, new a(this, intent));
    }
}
