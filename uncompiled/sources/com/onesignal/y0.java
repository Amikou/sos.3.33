package com.onesignal;

import com.onesignal.OSTrigger;
import com.onesignal.OneSignal;
import com.onesignal.t;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: OSTriggerController.java */
/* loaded from: classes2.dex */
public class y0 {
    public t a;
    public final ConcurrentHashMap<String, Object> b = new ConcurrentHashMap<>();

    /* compiled from: OSTriggerController.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[OSTrigger.OSTriggerOperator.values().length];
            a = iArr;
            try {
                iArr[OSTrigger.OSTriggerOperator.EQUAL_TO.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.NOT_EQUAL_TO.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.EXISTS.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.CONTAINS.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.NOT_EXISTS.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.LESS_THAN.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.GREATER_THAN.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.LESS_THAN_OR_EQUAL_TO.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[OSTrigger.OSTriggerOperator.GREATER_THAN_OR_EQUAL_TO.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
        }
    }

    public y0(t.c cVar) {
        this.a = new t(cVar);
    }

    public final boolean a(ArrayList<OSTrigger> arrayList) {
        Iterator<OSTrigger> it = arrayList.iterator();
        while (it.hasNext()) {
            if (!c(it.next())) {
                return false;
            }
        }
        return true;
    }

    public boolean b(x xVar) {
        if (xVar.c.size() == 0) {
            return true;
        }
        Iterator<ArrayList<OSTrigger>> it = xVar.c.iterator();
        while (it.hasNext()) {
            if (a(it.next())) {
                return true;
            }
        }
        return false;
    }

    public final boolean c(OSTrigger oSTrigger) {
        OSTrigger.OSTriggerKind oSTriggerKind = oSTrigger.b;
        if (oSTriggerKind == OSTrigger.OSTriggerKind.UNKNOWN) {
            return false;
        }
        if (oSTriggerKind != OSTrigger.OSTriggerKind.CUSTOM) {
            return this.a.c(oSTrigger);
        }
        OSTrigger.OSTriggerOperator oSTriggerOperator = oSTrigger.d;
        Object obj = this.b.get(oSTrigger.c);
        if (obj == null) {
            if (oSTriggerOperator == OSTrigger.OSTriggerOperator.NOT_EXISTS) {
                return true;
            }
            return oSTriggerOperator == OSTrigger.OSTriggerOperator.NOT_EQUAL_TO && oSTrigger.e != null;
        } else if (oSTriggerOperator == OSTrigger.OSTriggerOperator.EXISTS) {
            return true;
        } else {
            if (oSTriggerOperator == OSTrigger.OSTriggerOperator.NOT_EXISTS) {
                return false;
            }
            if (oSTriggerOperator == OSTrigger.OSTriggerOperator.CONTAINS) {
                return (obj instanceof Collection) && ((Collection) obj).contains(oSTrigger.e);
            }
            if (obj instanceof String) {
                Object obj2 = oSTrigger.e;
                if ((obj2 instanceof String) && i((String) obj2, (String) obj, oSTriggerOperator)) {
                    return true;
                }
            }
            Object obj3 = oSTrigger.e;
            return ((obj3 instanceof Number) && (obj instanceof Number) && g((Number) obj3, (Number) obj, oSTriggerOperator)) || f(oSTrigger.e, obj, oSTriggerOperator);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x0032  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean d(com.onesignal.x r7, java.util.Collection<java.lang.String> r8) {
        /*
            r6 = this;
            java.util.ArrayList<java.util.ArrayList<com.onesignal.OSTrigger>> r0 = r7.c
            r1 = 0
            if (r0 != 0) goto L6
            return r1
        L6:
            java.util.Iterator r8 = r8.iterator()
        La:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L4a
            java.lang.Object r0 = r8.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.ArrayList<java.util.ArrayList<com.onesignal.OSTrigger>> r2 = r7.c
            java.util.Iterator r2 = r2.iterator()
        L1c:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto La
            java.lang.Object r3 = r2.next()
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            java.util.Iterator r3 = r3.iterator()
        L2c:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L1c
            java.lang.Object r4 = r3.next()
            com.onesignal.OSTrigger r4 = (com.onesignal.OSTrigger) r4
            java.lang.String r5 = r4.c
            boolean r5 = r0.equals(r5)
            if (r5 != 0) goto L48
            java.lang.String r4 = r4.a
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L2c
        L48:
            r7 = 1
            return r7
        L4a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.y0.d(com.onesignal.x, java.util.Collection):boolean");
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x0028  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean e(com.onesignal.x r5) {
        /*
            r4 = this;
            java.util.ArrayList<java.util.ArrayList<com.onesignal.OSTrigger>> r0 = r5.c
            r1 = 0
            if (r0 == 0) goto L3b
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto Lc
            goto L3b
        Lc:
            java.util.ArrayList<java.util.ArrayList<com.onesignal.OSTrigger>> r5 = r5.c
            java.util.Iterator r5 = r5.iterator()
        L12:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L39
            java.lang.Object r0 = r5.next()
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            java.util.Iterator r0 = r0.iterator()
        L22:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L12
            java.lang.Object r2 = r0.next()
            com.onesignal.OSTrigger r2 = (com.onesignal.OSTrigger) r2
            com.onesignal.OSTrigger$OSTriggerKind r2 = r2.b
            com.onesignal.OSTrigger$OSTriggerKind r3 = com.onesignal.OSTrigger.OSTriggerKind.CUSTOM
            if (r2 == r3) goto L38
            com.onesignal.OSTrigger$OSTriggerKind r3 = com.onesignal.OSTrigger.OSTriggerKind.UNKNOWN
            if (r2 != r3) goto L22
        L38:
            return r1
        L39:
            r5 = 1
            return r5
        L3b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.y0.e(com.onesignal.x):boolean");
    }

    public final boolean f(Object obj, Object obj2, OSTrigger.OSTriggerOperator oSTriggerOperator) {
        if (obj == null) {
            return false;
        }
        if (oSTriggerOperator.checksEquality()) {
            String obj3 = obj.toString();
            String obj4 = obj2.toString();
            if (obj2 instanceof Number) {
                obj4 = new DecimalFormat("0.#").format(obj2);
            }
            return i(obj3, obj4, oSTriggerOperator);
        } else if ((obj2 instanceof String) && (obj instanceof Number)) {
            return h((Number) obj, (String) obj2, oSTriggerOperator);
        } else {
            return false;
        }
    }

    public final boolean g(Number number, Number number2, OSTrigger.OSTriggerOperator oSTriggerOperator) {
        double doubleValue = number.doubleValue();
        double doubleValue2 = number2.doubleValue();
        switch (a.a[oSTriggerOperator.ordinal()]) {
            case 1:
                return doubleValue2 == doubleValue;
            case 2:
                return doubleValue2 != doubleValue;
            case 3:
            case 4:
            case 5:
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.d1(log_level, "Attempted to use an invalid operator with a numeric value: " + oSTriggerOperator.toString());
                return false;
            case 6:
                return doubleValue2 < doubleValue;
            case 7:
                return doubleValue2 > doubleValue;
            case 8:
                return doubleValue2 < doubleValue || doubleValue2 == doubleValue;
            case 9:
                int i = (doubleValue2 > doubleValue ? 1 : (doubleValue2 == doubleValue ? 0 : -1));
                return i > 0 || i == 0;
            default:
                return false;
        }
    }

    public final boolean h(Number number, String str, OSTrigger.OSTriggerOperator oSTriggerOperator) {
        try {
            return g(Double.valueOf(number.doubleValue()), Double.valueOf(Double.parseDouble(str)), oSTriggerOperator);
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    public final boolean i(String str, String str2, OSTrigger.OSTriggerOperator oSTriggerOperator) {
        int i = a.a[oSTriggerOperator.ordinal()];
        if (i != 1) {
            if (i != 2) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.d1(log_level, "Attempted to use an invalid operator for a string trigger comparison: " + oSTriggerOperator.toString());
                return false;
            }
            return !str.equals(str2);
        }
        return str.equals(str2);
    }
}
