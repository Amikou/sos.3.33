package com.onesignal;

import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import com.onesignal.LocationController;
import com.onesignal.OneSignal;
import java.lang.ref.WeakReference;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/* compiled from: OSSyncService.java */
/* loaded from: classes2.dex */
public class u0 extends s {
    public static final Object e = new Object();
    public static u0 f;
    public Long d = 0L;

    /* compiled from: OSSyncService.java */
    /* loaded from: classes2.dex */
    public static class a extends c {
        public WeakReference<Service> a;

        public a(Service service) {
            this.a = new WeakReference<>(service);
        }

        @Override // com.onesignal.u0.c
        public void a() {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "LegacySyncRunnable:Stopped");
            if (this.a.get() != null) {
                this.a.get().stopSelf();
            }
        }
    }

    /* compiled from: OSSyncService.java */
    /* loaded from: classes2.dex */
    public static class b extends c {
        public WeakReference<JobService> a;
        public JobParameters f0;

        public b(JobService jobService, JobParameters jobParameters) {
            this.a = new WeakReference<>(jobService);
            this.f0 = jobParameters;
        }

        @Override // com.onesignal.u0.c
        public void a() {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "LollipopSyncRunnable:JobFinished needsJobReschedule: " + u0.q().a);
            boolean z = u0.q().a;
            u0.q().a = false;
            if (this.a.get() != null) {
                this.a.get().jobFinished(this.f0, z);
            }
        }
    }

    /* compiled from: OSSyncService.java */
    /* loaded from: classes2.dex */
    public static abstract class c implements Runnable {

        /* compiled from: OSSyncService.java */
        /* loaded from: classes2.dex */
        public class a implements LocationController.b {
            public final /* synthetic */ BlockingQueue a;

            public a(c cVar, BlockingQueue blockingQueue) {
                this.a = blockingQueue;
            }

            /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
                r2 = r2;
             */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.onesignal.LocationController.b
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public void a(com.onesignal.LocationController.d r2) {
                /*
                    r1 = this;
                    if (r2 == 0) goto L3
                    goto L8
                L3:
                    java.lang.Object r2 = new java.lang.Object
                    r2.<init>()
                L8:
                    java.util.concurrent.BlockingQueue r0 = r1.a
                    r0.offer(r2)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.onesignal.u0.c.a.a(com.onesignal.LocationController$d):void");
            }

            @Override // com.onesignal.LocationController.b
            public LocationController.PermissionType getType() {
                return LocationController.PermissionType.SYNC_SERVICE;
            }
        }

        public abstract void a();

        @Override // java.lang.Runnable
        public final void run() {
            synchronized (s.c) {
                u0.q().d = 0L;
            }
            if (OneSignal.z0() == null) {
                a();
                return;
            }
            OneSignal.g = OneSignal.o0();
            OneSignalStateSynchronizer.k();
            try {
                ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue(1);
                LocationController.g(OneSignal.e, false, false, new a(this, arrayBlockingQueue));
                Object take = arrayBlockingQueue.take();
                if (take instanceof LocationController.d) {
                    OneSignalStateSynchronizer.w((LocationController.d) take);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            OneSignalStateSynchronizer.u(true);
            OneSignal.b0().d();
            a();
        }
    }

    public static u0 q() {
        if (f == null) {
            synchronized (e) {
                if (f == null) {
                    f = new u0();
                }
            }
        }
        return f;
    }

    @Override // com.onesignal.s
    public Class c() {
        return SyncJobService.class;
    }

    @Override // com.onesignal.s
    public Class d() {
        return SyncService.class;
    }

    @Override // com.onesignal.s
    public int e() {
        return 2071862118;
    }

    @Override // com.onesignal.s
    public String f() {
        return "OS_SYNCSRV_BG_SYNC";
    }

    public void p(Context context) {
        synchronized (s.c) {
            this.d = 0L;
            if (LocationController.m(context)) {
                return;
            }
            a(context);
        }
    }

    public void r(Context context, long j) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.VERBOSE;
        OneSignal.a(log_level, "OSSyncService scheduleLocationUpdateTask:delayMs: " + j);
        t(context, j);
    }

    public void s(Context context) {
        OneSignal.a(OneSignal.LOG_LEVEL.VERBOSE, "OSSyncService scheduleSyncTask:SYNC_AFTER_BG_DELAY_MS: 30000");
        t(context, 30000L);
    }

    public void t(Context context, long j) {
        synchronized (s.c) {
            if (this.d.longValue() != 0 && OneSignal.w0().a() + j > this.d.longValue()) {
                OneSignal.a(OneSignal.LOG_LEVEL.VERBOSE, "OSSyncService scheduleSyncTask already update scheduled nextScheduledSyncTimeMs: " + this.d);
                return;
            }
            if (j < 5000) {
                j = 5000;
            }
            i(context, j);
            this.d = Long.valueOf(OneSignal.w0().a() + j);
        }
    }
}
