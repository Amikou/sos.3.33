package com.onesignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInAppMessageTag.java */
/* loaded from: classes2.dex */
public class e0 {
    public JSONObject a;
    public JSONArray b;

    public e0(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.has("adds") ? jSONObject.getJSONObject("adds") : null;
        this.b = jSONObject.has("removes") ? jSONObject.getJSONArray("removes") : null;
    }

    public JSONObject a() {
        return this.a;
    }

    public JSONArray b() {
        return this.b;
    }

    public String toString() {
        return "OSInAppMessageTag{adds=" + this.a + ", removes=" + this.b + '}';
    }
}
