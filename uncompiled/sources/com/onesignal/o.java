package com.onesignal;

import android.app.Activity;
import android.content.Intent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NotificationPayloadProcessorHMS.java */
/* loaded from: classes2.dex */
public class o {
    public static JSONObject a(Intent intent) {
        if (j0.e(intent)) {
            JSONObject a = k.a(intent.getExtras());
            d(a);
            return a;
        }
        return null;
    }

    public static void b(Activity activity, Intent intent) {
        JSONObject a;
        OneSignal.L0(activity.getApplicationContext());
        if (intent == null || (a = a(intent)) == null) {
            return;
        }
        c(activity, a);
    }

    public static void c(Activity activity, JSONObject jSONObject) {
        if (tj2.b(activity, jSONObject)) {
            return;
        }
        OneSignal.E0(activity, new JSONArray().put(jSONObject), j0.b(jSONObject));
    }

    public static void d(JSONObject jSONObject) {
        try {
            String str = (String) k.b(jSONObject).remove("actionId");
            if (str == null) {
                return;
            }
            jSONObject.put("actionId", str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
