package com.onesignal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import com.onesignal.OneSignal;
import com.onesignal.shortcutbadger.ShortcutBadgeException;

/* compiled from: BadgeCountUpdater.java */
/* loaded from: classes2.dex */
public class b {
    public static int a = -1;

    public static boolean a(Context context) {
        int i = a;
        if (i != -1) {
            return i == 1;
        }
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle != null) {
                a = "DISABLE".equals(bundle.getString("com.onesignal.BadgeCount")) ? 0 : 1;
            } else {
                a = 1;
            }
        } catch (PackageManager.NameNotFoundException e) {
            a = 0;
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error reading meta-data tag 'com.onesignal.BadgeCount'. Disabling badge setting.", e);
        }
        return a == 1;
    }

    public static boolean b(Context context) {
        return a(context) && OSUtils.a(context);
    }

    public static void c(bn2 bn2Var, Context context) {
        if (b(context)) {
            if (Build.VERSION.SDK_INT >= 23) {
                f(context);
            } else {
                e(bn2Var, context);
            }
        }
    }

    public static void d(int i, Context context) {
        if (a(context)) {
            try {
                com.onesignal.shortcutbadger.b.a(context, i);
            } catch (ShortcutBadgeException unused) {
            }
        }
    }

    public static void e(bn2 bn2Var, Context context) {
        Cursor b = bn2Var.b("notification", null, a1.m().toString(), null, null, null, null, m.a);
        int count = b.getCount();
        b.close();
        d(count, context);
    }

    public static void f(Context context) {
        int i = 0;
        for (StatusBarNotification statusBarNotification : cn2.d(context)) {
            if (!m.f(statusBarNotification)) {
                i++;
            }
        }
        d(i, context);
    }
}
