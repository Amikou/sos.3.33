package com.onesignal;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class OSPermissionChangedInternalObserver {
    public static void a(q0 q0Var) {
        if (OneSignal.h0().c(new sk2(OneSignal.d0, (q0) q0Var.clone()))) {
            q0 q0Var2 = (q0) q0Var.clone();
            OneSignal.d0 = q0Var2;
            q0Var2.d();
        }
    }

    public static void b(q0 q0Var) {
        if (!q0Var.a()) {
            b.d(0, OneSignal.e);
        }
        OneSignalStateSynchronizer.t(OneSignal.A());
    }

    public void changed(q0 q0Var) {
        b(q0Var);
        a(q0Var);
    }
}
