package com.onesignal;

import android.content.Context;
import com.amazon.device.messaging.ADM;
import com.onesignal.OneSignal;
import com.onesignal.f1;

/* compiled from: PushRegistratorADM.java */
/* loaded from: classes2.dex */
public class g1 implements f1 {
    public static f1.a a = null;
    public static boolean b = false;

    /* compiled from: PushRegistratorADM.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ Context a;
        public final /* synthetic */ f1.a f0;

        public a(g1 g1Var, Context context, f1.a aVar) {
            this.a = context;
            this.f0 = aVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            ADM adm = new ADM(this.a);
            String registrationId = adm.getRegistrationId();
            if (registrationId == null) {
                adm.startRegister();
            } else {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.a(log_level, "ADM Already registered with ID:" + registrationId);
                this.f0.a(registrationId, 1);
            }
            try {
                Thread.sleep(30000L);
            } catch (InterruptedException unused) {
            }
            if (g1.b) {
                return;
            }
            OneSignal.a(OneSignal.LOG_LEVEL.ERROR, "com.onesignal.ADMMessageHandler timed out, please check that your have the receiver, service, and your package name matches(NOTE: Case Sensitive) per the OneSignal instructions.");
            g1.c(null);
        }
    }

    public static void c(String str) {
        f1.a aVar = a;
        if (aVar == null) {
            return;
        }
        b = true;
        aVar.a(str, 1);
    }

    @Override // com.onesignal.f1
    public void a(Context context, String str, f1.a aVar) {
        a = aVar;
        new Thread(new a(this, context, aVar)).start();
    }
}
