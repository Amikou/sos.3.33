package com.onesignal;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import com.onesignal.OneSignal;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NotificationOpenedProcessor.java */
/* loaded from: classes2.dex */
public class n {
    public static void a(JSONArray jSONArray, String str, a1 a1Var) {
        Cursor c = a1Var.c("notification", new String[]{"full_data"}, "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 0", new String[]{str}, null, null, null);
        if (c.getCount() > 1) {
            c.moveToFirst();
            do {
                try {
                    jSONArray.put(new JSONObject(c.getString(c.getColumnIndex("full_data"))));
                } catch (JSONException unused) {
                    OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                    OneSignal.a(log_level, "Could not parse JSON of sub notification in group: " + str);
                }
            } while (c.moveToNext());
            c.close();
        }
        c.close();
    }

    public static void b(Context context, a1 a1Var, String str) {
        if (str != null) {
            q.a(context, a1Var, str);
        } else if (Build.VERSION.SDK_INT < 23 || cn2.e(context).intValue() >= 1) {
        } else {
            cn2.i(context).cancel(cn2.f());
        }
    }

    public static void c(Context context, Intent intent) {
        if (intent.getBooleanExtra("action_button", false)) {
            androidx.core.app.c.d(context).b(intent.getIntExtra("androidNotificationId", 0));
            if (Build.VERSION.SDK_INT < 31) {
                context.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
            }
        }
    }

    public static boolean d(Intent intent) {
        return intent.hasExtra("onesignalData") || intent.hasExtra("summary") || intent.hasExtra("androidNotificationId");
    }

    public static void e(Context context, Intent intent, a1 a1Var, boolean z) {
        String str;
        String stringExtra = intent.getStringExtra("summary");
        String[] strArr = null;
        if (stringExtra != null) {
            boolean equals = stringExtra.equals(cn2.g());
            if (equals) {
                str = "group_id IS NULL";
            } else {
                strArr = new String[]{stringExtra};
                str = "group_id = ?";
            }
            if (!z && !OneSignal.P()) {
                String valueOf = String.valueOf(cn2.h(a1Var, stringExtra, equals));
                str = str + " AND android_notification_id = ?";
                strArr = equals ? new String[]{valueOf} : new String[]{stringExtra, valueOf};
            }
        } else {
            str = "android_notification_id = " + intent.getIntExtra("androidNotificationId", 0);
        }
        b(context, a1Var, stringExtra);
        a1Var.a("notification", f(intent), str, strArr);
        b.c(a1Var, context);
    }

    public static ContentValues f(Intent intent) {
        ContentValues contentValues = new ContentValues();
        if (intent.getBooleanExtra("dismissed", false)) {
            contentValues.put("dismissed", (Integer) 1);
        } else {
            contentValues.put("opened", (Integer) 1);
        }
        return contentValues;
    }

    public static void g(Context context, Intent intent) {
        if (d(intent)) {
            if (context != null) {
                OneSignal.L0(context.getApplicationContext());
            }
            c(context, intent);
            h(context, intent);
        }
    }

    public static void h(Context context, Intent intent) {
        ak2 ak2Var;
        String stringExtra;
        a1 g = a1.g(context);
        String stringExtra2 = intent.getStringExtra("summary");
        boolean booleanExtra = intent.getBooleanExtra("dismissed", false);
        if (booleanExtra) {
            ak2Var = null;
        } else {
            ak2Var = i(context, intent, g, stringExtra2);
            if (ak2Var == null) {
                return;
            }
        }
        e(context, intent, g, booleanExtra);
        if (stringExtra2 == null && (stringExtra = intent.getStringExtra("grp")) != null) {
            q.f(context, g, stringExtra, booleanExtra);
        }
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, "processIntent from context: " + context + " and intent: " + intent);
        if (intent.getExtras() != null) {
            OneSignal.d1(log_level, "processIntent intent extras: " + intent.getExtras().toString());
        }
        if (booleanExtra) {
            return;
        }
        if (!(context instanceof Activity)) {
            OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.ERROR;
            OneSignal.d1(log_level2, "NotificationOpenedProcessor processIntent from an non Activity context: " + context);
            return;
        }
        OneSignal.E0((Activity) context, ak2Var.a(), j0.b(ak2Var.b()));
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0058  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.ak2 i(android.content.Context r7, android.content.Intent r8, com.onesignal.a1 r9, java.lang.String r10) {
        /*
            java.lang.String r0 = "androidNotificationId"
            java.lang.String r1 = "onesignalData"
            r2 = 0
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch: org.json.JSONException -> L51
            java.lang.String r4 = r8.getStringExtra(r1)     // Catch: org.json.JSONException -> L51
            r3.<init>(r4)     // Catch: org.json.JSONException -> L51
            boolean r4 = r7 instanceof android.app.Activity     // Catch: org.json.JSONException -> L4f
            if (r4 != 0) goto L29
            com.onesignal.OneSignal$LOG_LEVEL r4 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch: org.json.JSONException -> L4f
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch: org.json.JSONException -> L4f
            r5.<init>()     // Catch: org.json.JSONException -> L4f
            java.lang.String r6 = "NotificationOpenedProcessor processIntent from an non Activity context: "
            r5.append(r6)     // Catch: org.json.JSONException -> L4f
            r5.append(r7)     // Catch: org.json.JSONException -> L4f
            java.lang.String r7 = r5.toString()     // Catch: org.json.JSONException -> L4f
            com.onesignal.OneSignal.d1(r4, r7)     // Catch: org.json.JSONException -> L4f
            goto L32
        L29:
            android.app.Activity r7 = (android.app.Activity) r7     // Catch: org.json.JSONException -> L4f
            boolean r7 = defpackage.tj2.b(r7, r3)     // Catch: org.json.JSONException -> L4f
            if (r7 == 0) goto L32
            return r2
        L32:
            r7 = 0
            int r7 = r8.getIntExtra(r0, r7)     // Catch: org.json.JSONException -> L4f
            r3.put(r0, r7)     // Catch: org.json.JSONException -> L4f
            java.lang.String r7 = r3.toString()     // Catch: org.json.JSONException -> L4f
            r8.putExtra(r1, r7)     // Catch: org.json.JSONException -> L4f
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch: org.json.JSONException -> L4f
            java.lang.String r8 = r8.getStringExtra(r1)     // Catch: org.json.JSONException -> L4f
            r7.<init>(r8)     // Catch: org.json.JSONException -> L4f
            org.json.JSONArray r2 = com.onesignal.k.g(r7)     // Catch: org.json.JSONException -> L4f
            goto L56
        L4f:
            r7 = move-exception
            goto L53
        L51:
            r7 = move-exception
            r3 = r2
        L53:
            r7.printStackTrace()
        L56:
            if (r10 == 0) goto L5b
            a(r2, r10, r9)
        L5b:
            ak2 r7 = new ak2
            r7.<init>(r2, r3)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.n.i(android.content.Context, android.content.Intent, com.onesignal.a1, java.lang.String):ak2");
    }
}
