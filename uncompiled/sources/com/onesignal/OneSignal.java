package com.onesignal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.onesignal.LocationController;
import com.onesignal.OSNotificationAction;
import com.onesignal.c1;
import com.onesignal.d1;
import com.onesignal.f1;
import com.onesignal.i0;
import com.onesignal.i1;
import com.onesignal.s1;
import com.onesignal.t0;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class OneSignal {
    public static FocusTimeController A;
    public static vk2 I;
    public static cl2 J;
    public static t0 K;
    public static p0 L;
    public static hk2 M;
    public static com.onesignal.i0 N;
    public static final Object O;
    public static String P;
    public static String Q;
    public static OSUtils R;
    public static boolean S;
    public static boolean T;
    public static boolean U;
    public static boolean V;
    public static boolean W;
    public static LocationController.d X;
    public static Collection<JSONArray> Y;
    public static HashSet<String> Z;
    public static f0 a;
    public static final ArrayList<x> a0;
    public static u b;
    public static zl0 b0;
    public static u c;
    public static q0 c0;
    public static q0 d0;
    public static Context e;
    public static n0<Object, sk2> e0;
    public static WeakReference<Activity> f;
    public static OSSubscriptionState f0;
    public static String g;
    public static OSSubscriptionState g0;
    public static String h;
    public static n0<Object, xk2> h0;
    public static nj2 i0;
    public static nj2 j0;
    public static n0<Object, oj2> k0;
    public static tk2 l0;
    public static tk2 m0;
    public static n0<Object, uk2> n0;
    public static w o0;
    public static d0 p;
    public static f1 p0;
    public static b0 q;
    public static a0 r;
    public static y s;
    public static boolean t;
    public static boolean u;
    public static m1 w;
    public static l1 x;
    public static y74 y;
    public static List<v> d = new ArrayList();
    public static LOG_LEVEL i = LOG_LEVEL.NONE;
    public static LOG_LEVEL j = LOG_LEVEL.WARN;
    public static String k = null;
    public static String l = null;
    public static String m = null;
    public static int n = Integer.MAX_VALUE;
    public static cy1 o = null;
    public static AppEntryAction v = AppEntryAction.APP_CLOSE;
    public static yj2 z = new com.onesignal.f0();
    public static t0.b B = new c();
    public static rj2 C = new rj2();
    public static zk2 D = new al2();
    public static s0 E = new s0();
    public static w0 F = new w0(z);
    public static yk2 G = new yk2(E, z);
    public static wm2 H = new e1();

    /* loaded from: classes2.dex */
    public enum AppEntryAction {
        NOTIFICATION_CLICK,
        APP_OPEN,
        APP_CLOSE;

        public boolean isAppClose() {
            return equals(APP_CLOSE);
        }

        public boolean isAppOpen() {
            return equals(APP_OPEN);
        }

        public boolean isNotificationClick() {
            return equals(NOTIFICATION_CLICK);
        }
    }

    /* loaded from: classes2.dex */
    public enum EmailErrorType {
        VALIDATION,
        REQUIRES_EMAIL_AUTH,
        INVALID_OPERATION,
        NETWORK
    }

    /* loaded from: classes2.dex */
    public enum LOG_LEVEL {
        NONE,
        FATAL,
        ERROR,
        WARN,
        INFO,
        DEBUG,
        VERBOSE
    }

    /* loaded from: classes2.dex */
    public enum PromptActionResult {
        PERMISSION_GRANTED,
        PERMISSION_DENIED,
        LOCATION_PERMISSIONS_MISSING_MANIFEST,
        ERROR
    }

    /* loaded from: classes2.dex */
    public enum SMSErrorType {
        VALIDATION,
        REQUIRES_SMS_AUTH,
        INVALID_OPERATION,
        NETWORK
    }

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        @Override // java.lang.Runnable
        public void run() {
            try {
                OneSignal.m1();
            } catch (JSONException e) {
                OneSignal.b(LOG_LEVEL.FATAL, "FATAL Error registering device!", e);
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface a0 {
        void a(l0 l0Var);
    }

    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public final /* synthetic */ JSONObject a;
        public final /* synthetic */ s f0;

        public b(JSONObject jSONObject, s sVar) {
            this.a = jSONObject;
            this.f0 = sVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.z.debug("Running sendTags() operation from pending task queue.");
            OneSignal.A1(this.a, this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public interface b0 {
        void a(m0 m0Var);
    }

    /* loaded from: classes2.dex */
    public class c implements t0.b {
        @Override // com.onesignal.t0.b
        public void a(List<com.onesignal.influence.domain.a> list) {
            if (OneSignal.L == null) {
                OneSignal.a(LOG_LEVEL.WARN, "OneSignal onSessionEnding called before init!");
            }
            if (OneSignal.L != null) {
                OneSignal.L.e();
            }
            OneSignal.b0().g(list);
        }
    }

    /* loaded from: classes2.dex */
    public interface c0 {
        void a(PromptActionResult promptActionResult);
    }

    /* loaded from: classes2.dex */
    public class d implements Runnable {
        public final /* synthetic */ JSONObject a;
        public final /* synthetic */ s f0;

        public d(JSONObject jSONObject, s sVar) {
            this.a = jSONObject;
            this.f0 = sVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            Object opt;
            if (this.a == null) {
                OneSignal.z.error("Attempted to send null tags");
                s sVar = this.f0;
                if (sVar != null) {
                    sVar.b(new i0(-1, "Attempted to send null tags"));
                    return;
                }
                return;
            }
            JSONObject jSONObject = OneSignalStateSynchronizer.h(false).b;
            JSONObject jSONObject2 = new JSONObject();
            Iterator<String> keys = this.a.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                try {
                    opt = this.a.opt(next);
                } catch (Throwable unused) {
                }
                if (!(opt instanceof JSONArray) && !(opt instanceof JSONObject)) {
                    if (!this.a.isNull(next) && !"".equals(opt)) {
                        jSONObject2.put(next, opt.toString());
                    }
                    if (jSONObject != null && jSONObject.has(next)) {
                        jSONObject2.put(next, "");
                    }
                }
                LOG_LEVEL log_level = LOG_LEVEL.ERROR;
                OneSignal.a(log_level, "Omitting key '" + next + "'! sendTags DO NOT supported nested values!");
            }
            if (!jSONObject2.toString().equals("{}")) {
                yj2 yj2Var = OneSignal.z;
                yj2Var.debug("Available tags to send: " + jSONObject2.toString());
                OneSignalStateSynchronizer.q(jSONObject2, this.f0);
                return;
            }
            OneSignal.z.debug("Send tags ended successfully");
            s sVar2 = this.f0;
            if (sVar2 != null) {
                sVar2.a(jSONObject);
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface d0 {
        void a(Context context, m0 m0Var);
    }

    /* loaded from: classes2.dex */
    public class e implements Runnable {
        public final /* synthetic */ x a;

        public e(x xVar) {
            this.a = xVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.z.debug("Running getTags() operation from pending queue.");
            OneSignal.v0(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public static class e0 {
        public e0(SMSErrorType sMSErrorType, String str) {
        }
    }

    /* loaded from: classes2.dex */
    public class f implements Runnable {
        public final /* synthetic */ x a;

        public f(x xVar) {
            this.a = xVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (OneSignal.a0) {
                OneSignal.a0.add(this.a);
                if (OneSignal.a0.size() > 1) {
                    return;
                }
                OneSignal.q1();
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface f0 {
        void a(JSONObject jSONObject);

        void b(e0 e0Var);
    }

    /* loaded from: classes2.dex */
    public class g implements Runnable {
        @Override // java.lang.Runnable
        public void run() {
            JSONObject jSONObject;
            s1.e h = OneSignalStateSynchronizer.h(!OneSignal.U);
            if (h.a) {
                boolean unused = OneSignal.U = true;
            }
            synchronized (OneSignal.a0) {
                Iterator it = OneSignal.a0.iterator();
                while (it.hasNext()) {
                    x xVar = (x) it.next();
                    if (h.b != null && !h.toString().equals("{}")) {
                        jSONObject = h.b;
                        xVar.a(jSONObject);
                    }
                    jSONObject = null;
                    xVar.a(jSONObject);
                }
                OneSignal.a0.clear();
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface g0 {
        void a(o0 o0Var);
    }

    /* loaded from: classes2.dex */
    public class h implements Runnable {
        public final /* synthetic */ l0 a;

        public h(l0 l0Var) {
            this.a = l0Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.r.a(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public interface h0 {
        void a(boolean z);
    }

    /* loaded from: classes2.dex */
    public class i extends d1.g {
        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            OneSignal.U0("sending Notification Opened Failed", i, th, str);
        }
    }

    /* loaded from: classes2.dex */
    public static class i0 {
        public i0(int i, String str) {
        }
    }

    /* loaded from: classes2.dex */
    public class j implements Runnable {
        public final /* synthetic */ c0 a;
        public final /* synthetic */ boolean f0;

        public j(c0 c0Var, boolean z) {
            this.a = c0Var;
            this.f0 = z;
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.z.debug("Running promptLocation() operation from pending queue.");
            OneSignal.g1(this.a, this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class k {
    }

    /* loaded from: classes2.dex */
    public class l extends LocationController.e {
        public final /* synthetic */ c0 a;

        public l(c0 c0Var) {
            this.a = c0Var;
        }

        @Override // com.onesignal.LocationController.b
        public void a(LocationController.d dVar) {
            if (OneSignal.L1("promptLocation()") || dVar == null) {
                return;
            }
            OneSignalStateSynchronizer.w(dVar);
        }

        @Override // com.onesignal.LocationController.e
        public void b(PromptActionResult promptActionResult) {
            super.b(promptActionResult);
            c0 c0Var = this.a;
            if (c0Var != null) {
                c0Var.a(promptActionResult);
            }
        }

        @Override // com.onesignal.LocationController.b
        public LocationController.PermissionType getType() {
            return LocationController.PermissionType.PROMPT_LOCATION;
        }
    }

    /* loaded from: classes2.dex */
    public class m implements Runnable {
        public final /* synthetic */ int a;

        public m(int i) {
            this.a = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            OneSignal.z.debug("Running removeNotification() operation from pending queue.");
            OneSignal.o1(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class n implements LocationController.b {
        @Override // com.onesignal.LocationController.b
        public void a(LocationController.d dVar) {
            LocationController.d unused = OneSignal.X = dVar;
            boolean unused2 = OneSignal.T = true;
            OneSignal.l1();
        }

        @Override // com.onesignal.LocationController.b
        public LocationController.PermissionType getType() {
            return LocationController.PermissionType.STARTUP;
        }
    }

    /* loaded from: classes2.dex */
    public class o implements f1.a {
        @Override // com.onesignal.f1.a
        public void a(String str, int i) {
            yj2 yj2Var = OneSignal.z;
            yj2Var.debug("registerForPushToken completed with id: " + str + " status: " + i);
            if (i >= 1) {
                if (OneSignal.h1(OneSignal.n)) {
                    int unused = OneSignal.n = i;
                }
            } else if (OneSignalStateSynchronizer.e() == null && (OneSignal.n == 1 || OneSignal.h1(OneSignal.n))) {
                int unused2 = OneSignal.n = i;
            }
            String unused3 = OneSignal.Q = str;
            boolean unused4 = OneSignal.S = true;
            OneSignal.U(OneSignal.e).g(str);
            OneSignal.l1();
        }
    }

    /* loaded from: classes2.dex */
    public class p implements c1.c {
        public final /* synthetic */ boolean a;

        public p(boolean z) {
            this.a = z;
        }

        @Override // com.onesignal.c1.c
        public void a(c1.f fVar) {
            boolean unused = OneSignal.W = false;
            String str = fVar.a;
            if (str != null) {
                OneSignal.h = str;
            }
            OneSignal.E.q(fVar, OneSignal.J, OneSignal.I, OneSignal.z);
            OneSignal.c1();
            com.onesignal.l.f(OneSignal.e, fVar.b);
            if (this.a) {
                OneSignal.k1();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class q implements Runnable {
        public final /* synthetic */ LOG_LEVEL a;
        public final /* synthetic */ String f0;

        public q(LOG_LEVEL log_level, String str) {
            this.a = log_level;
            this.f0 = str;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (OneSignal.Q() != null) {
                new AlertDialog.Builder(OneSignal.Q()).setTitle(this.a.toString()).setMessage(this.f0).show();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class r implements Runnable {
        @Override // java.lang.Runnable
        public void run() {
            OneSignal.z.debug("Running onAppLostFocus() operation from a pending task queue.");
            OneSignal.C();
        }
    }

    /* loaded from: classes2.dex */
    public interface s {
        void a(JSONObject jSONObject);

        void b(i0 i0Var);
    }

    /* loaded from: classes2.dex */
    public static class t {
        public t(EmailErrorType emailErrorType, String str) {
        }
    }

    /* loaded from: classes2.dex */
    public interface u {
        void a(t tVar);

        void onSuccess();
    }

    /* loaded from: classes2.dex */
    public interface v {
        void a(AppEntryAction appEntryAction);
    }

    /* loaded from: classes2.dex */
    public static class w {
        public JSONArray a;
        public boolean b;
        public d1.g c;

        public w(JSONArray jSONArray) {
            this.a = jSONArray;
        }
    }

    /* loaded from: classes2.dex */
    public interface x {
        void a(JSONObject jSONObject);
    }

    /* loaded from: classes2.dex */
    public interface y {
        void a(OSInAppMessageAction oSInAppMessageAction);
    }

    /* loaded from: classes2.dex */
    public interface z {
        void a(String str, boolean z);
    }

    static {
        wk2 wk2Var = new wk2();
        I = wk2Var;
        cl2 cl2Var = new cl2(wk2Var, z, D);
        J = cl2Var;
        K = new t0(B, cl2Var, z);
        O = new k();
        P = "native";
        R = new OSUtils();
        Y = new ArrayList();
        Z = new HashSet<>();
        a0 = new ArrayList<>();
    }

    public static boolean A() {
        if (E.t()) {
            return OSUtils.a(e);
        }
        return true;
    }

    public static void A0(Context context) {
        com.onesignal.a b2 = k7.b();
        boolean z2 = context instanceof Activity;
        boolean z3 = Q() == null;
        C1(!z3 || z2);
        yj2 yj2Var = z;
        yj2Var.debug("OneSignal handleActivityLifecycleHandler inForeground: " + u);
        if (!u) {
            if (b2 != null) {
                b2.s(true);
                return;
            }
            return;
        }
        if (z3 && z2 && b2 != null) {
            b2.r((Activity) context);
            b2.s(true);
        }
        OSNotificationRestoreWorkManager.c(context, false);
        b0().b();
    }

    public static void A1(JSONObject jSONObject, s sVar) {
        if (G.g("sendTags()")) {
            z.error("Waiting for remote params. Moving sendTags() operation to a pending task queue.");
            G.c(new b(jSONObject, sVar));
        } else if (L1("sendTags()")) {
        } else {
            d dVar = new d(jSONObject, sVar);
            if (G.e()) {
                z.debug("Sending sendTags() operation to pending task queue.");
                G.c(dVar);
                return;
            }
            dVar.run();
        }
    }

    public static boolean B(LOG_LEVEL log_level) {
        return log_level.compareTo(i) < 1 || log_level.compareTo(j) < 1;
    }

    public static void B0() {
        try {
            Class.forName("com.amazon.device.iap.PurchasingListener");
            x = new l1(e);
        } catch (ClassNotFoundException unused) {
        }
    }

    public static void B1(String str) {
        if (str != null && !str.isEmpty()) {
            if (!str.equals(g)) {
                t = false;
                yj2 yj2Var = z;
                yj2Var.a("setAppId called with id: " + str + " changing id from: " + g);
            }
            g = str;
            if (e == null) {
                z.b("appId set, but please call initWithContext(appContext) with Application context to complete OneSignal init!");
                return;
            }
            WeakReference<Activity> weakReference = f;
            if (weakReference != null && weakReference.get() != null) {
                K0(f.get());
                return;
            } else {
                K0(e);
                return;
            }
        }
        yj2 yj2Var2 = z;
        yj2Var2.b("setAppId called with id: " + str + ", ignoring!");
    }

    public static void C() {
        if (u) {
            return;
        }
        l1 l1Var = x;
        if (l1Var != null) {
            l1Var.c();
        }
        b0().a();
        w1();
    }

    public static void C0() {
        String o02 = o0();
        if (o02 != null) {
            if (o02.equals(g)) {
                return;
            }
            LOG_LEVEL log_level = LOG_LEVEL.DEBUG;
            a(log_level, "App id has changed:\nFrom: " + o02 + "\n To: " + g + "\nClearing the user id, app state, and remoteParams as they are no longer valid");
            s1(g);
            OneSignalStateSynchronizer.o();
            E.a();
            return;
        }
        LOG_LEVEL log_level2 = LOG_LEVEL.DEBUG;
        a(log_level2, "App id set for first time:  " + g);
        com.onesignal.b.d(0, e);
        s1(g);
    }

    public static void C1(boolean z2) {
        u = z2;
    }

    public static void D(AppEntryAction appEntryAction) {
        for (v vVar : new ArrayList(d)) {
            vVar.a(appEntryAction);
        }
    }

    public static void D0() {
        u uVar = c;
        if (uVar != null) {
            uVar.a(new t(EmailErrorType.NETWORK, "Failed due to network failure. Will retry on next sync."));
            c = null;
        }
    }

    public static void D1(long j2) {
        yj2 yj2Var = z;
        yj2Var.debug("Last session time set to: " + j2);
        b1.l(b1.a, "OS_LAST_SESSION_TIME", j2);
    }

    public static void E(JSONArray jSONArray, s sVar) {
        if (L1("deleteTags()")) {
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                jSONObject.put(jSONArray.getString(i2), "");
            }
            A1(jSONObject, sVar);
        } catch (Throwable th) {
            b(LOG_LEVEL.ERROR, "Failed to generate JSON for deleteTags.", th);
        }
    }

    public static void E0(Activity activity, JSONArray jSONArray, String str) {
        if (L1(null)) {
            return;
        }
        X0(activity, jSONArray);
        if (y != null && a0()) {
            y.g(N(jSONArray));
        }
        if (K1(activity, jSONArray)) {
            z(str);
        }
        e1(activity, jSONArray);
        r1(jSONArray);
    }

    public static void E1(LOG_LEVEL log_level, LOG_LEVEL log_level2) {
        j = log_level;
        i = log_level2;
    }

    public static void F() {
        if (M1()) {
            yj2 yj2Var = z;
            yj2Var.debug("Starting new session with appEntryState: " + O());
            OneSignalStateSynchronizer.r();
            g0().e();
            K.m(O());
            c0().n0();
            D1(D.a());
        } else if (O0()) {
            yj2 yj2Var2 = z;
            yj2Var2.debug("Continue on same session with appEntryState: " + O());
            K.c(O());
        }
        c0().T();
        if (!u && J0()) {
            z.debug("doSessionInit on background with already registered user");
        }
        P1();
    }

    public static void F0(zj2 zj2Var) {
        try {
            JSONObject jSONObject = new JSONObject(zj2Var.e().toString());
            jSONObject.put("androidNotificationId", zj2Var.a());
            l0 N2 = N(com.onesignal.k.g(jSONObject));
            if (y == null || !a0()) {
                return;
            }
            y.h(N2);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public static void F1(d0 d0Var) {
        if (p == null) {
            p = d0Var;
        }
    }

    public static void G() {
        for (JSONArray jSONArray : Y) {
            r1(jSONArray);
        }
        Y.clear();
    }

    public static void G0() {
        u uVar = c;
        if (uVar != null) {
            uVar.onSuccess();
            c = null;
        }
    }

    public static void G1(boolean z2) {
        if (k0().f()) {
            z.b("setRequiresUserPrivacyConsent already called by remote params!, ignoring user set");
        } else if (p1() && !z2) {
            a(LOG_LEVEL.ERROR, "Cannot change requiresUserPrivacyConsent() from TRUE to FALSE");
        } else {
            k0().o(z2);
        }
    }

    public static void H() {
        u uVar = b;
        if (uVar != null) {
            uVar.a(new t(EmailErrorType.NETWORK, "Failed due to network failure. Will retry on next sync."));
            b = null;
        }
    }

    public static boolean H0() {
        return !TextUtils.isEmpty(l);
    }

    public static void H1(boolean z2) {
        k7.c((Application) e);
        if (z2) {
            o = new cy1(I);
            b1.o();
            a1 V2 = V();
            com.onesignal.i0 i0Var = new com.onesignal.i0(V2, z);
            N = i0Var;
            i0Var.h();
            c0().D();
            if (M == null) {
                M = new hk2(z, H, V2, I);
            }
            K.g();
            g0().d();
        }
    }

    public static void I() {
        u uVar = b;
        if (uVar != null) {
            uVar.onSuccess();
            b = null;
        }
    }

    public static boolean I0() {
        return !TextUtils.isEmpty(m);
    }

    public static void I1(Context context) {
        try {
            String string = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("com.onesignal.PrivacyConsent");
            if (string != null) {
                G1("ENABLE".equalsIgnoreCase(string));
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void J(com.onesignal.h0 h0Var) {
        d1(LOG_LEVEL.INFO, "Fire notificationWillShowInForegroundHandler");
        m0 c2 = h0Var.c();
        try {
            q.a(c2);
        } catch (Throwable th) {
            d1(LOG_LEVEL.ERROR, "Exception thrown while notification was being processed for display by notificationWillShowInForegroundHandler, showing notification in foreground!");
            c2.b(c2.c());
            throw th;
        }
    }

    public static boolean J0() {
        return z0() != null;
    }

    public static boolean J1(zj2 zj2Var) {
        if (!O0()) {
            d1(LOG_LEVEL.INFO, "App is in background, show notification");
            return false;
        } else if (q == null) {
            d1(LOG_LEVEL.INFO, "No NotificationWillShowInForegroundHandler setup, show notification");
            return false;
        } else if (zj2Var.n()) {
            d1(LOG_LEVEL.INFO, "Not firing notificationWillShowInForegroundHandler for restored notifications");
            return false;
        } else {
            return true;
        }
    }

    public static void K(l0 l0Var) {
        OSUtils.S(new h(l0Var));
    }

    public static synchronized void K0(Context context) {
        synchronized (OneSignal.class) {
            z.a("Starting OneSignal initialization!");
            com.onesignal.h0.h(e);
            if (!p1() && E.k()) {
                int i2 = n;
                if (i2 == Integer.MAX_VALUE) {
                    i2 = R.A(e, g);
                }
                n = i2;
                if (S0()) {
                    return;
                }
                if (t) {
                    if (r != null) {
                        G();
                    }
                    z.debug("OneSignal SDK initialization already completed.");
                    return;
                }
                A0(context);
                f = null;
                OneSignalStateSynchronizer.k();
                C0();
                B0();
                OSPermissionChangedInternalObserver.b(S(e));
                F();
                if (r != null) {
                    G();
                }
                if (m1.a(e)) {
                    w = new m1(e);
                }
                if (y74.a()) {
                    y = new y74(e);
                }
                t = true;
                a(LOG_LEVEL.VERBOSE, "OneSignal SDK initialization done.");
                g0().q();
                G.f();
                return;
            }
            if (!E.k()) {
                z.a("OneSignal SDK initialization delayed, waiting for remote params.");
            } else {
                z.a("OneSignal SDK initialization delayed, waiting for privacy consent to be set.");
            }
            b0 = new zl0(e, g);
            String str = g;
            g = null;
            if (str != null && context != null) {
                V0(str, z0(), false);
            }
        }
    }

    public static boolean K1(Activity activity, JSONArray jSONArray) {
        if (u) {
            return false;
        }
        try {
            return new bk2(activity, jSONArray.getJSONObject(0)).a();
        } catch (JSONException e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public static void L() {
        f0 f0Var = a;
        if (f0Var != null) {
            f0Var.b(new e0(SMSErrorType.NETWORK, "Failed due to network failure. Will retry on next sync."));
            a = null;
        }
    }

    public static void L0(Context context) {
        if (context == null) {
            z.b("initWithContext called with null context, ignoring!");
            return;
        }
        if (context instanceof Activity) {
            f = new WeakReference<>((Activity) context);
        }
        boolean z2 = e == null;
        e = context.getApplicationContext();
        H1(z2);
        I1(e);
        if (g == null) {
            String o02 = o0();
            if (o02 == null) {
                z.b("appContext set, but please call setAppId(appId) with a valid appId to complete OneSignal init!");
                return;
            }
            yj2 yj2Var = z;
            yj2Var.a("appContext set and cached app id found, calling setAppId with: " + o02);
            B1(o02);
            return;
        }
        yj2 yj2Var2 = z;
        yj2Var2.a("initWithContext called with: " + context);
        K0(context);
    }

    public static boolean L1(String str) {
        if (p1()) {
            if (str != null) {
                LOG_LEVEL log_level = LOG_LEVEL.WARN;
                a(log_level, "Method " + str + " was called before the user provided privacy consent. Your application is set to require the user's privacy consent before the OneSignal SDK can be initialized. Please ensure the user has provided consent before calling this method. You can check the latest OneSignal consent status by calling OneSignal.userProvidedPrivacyConsent()");
                return true;
            }
            return true;
        }
        return false;
    }

    public static void M(JSONObject jSONObject) {
        f0 f0Var = a;
        if (f0Var != null) {
            f0Var.a(jSONObject);
            a = null;
        }
    }

    public static void M0() {
        ArrayList<x> arrayList = a0;
        synchronized (arrayList) {
            if (arrayList.size() == 0) {
                return;
            }
            new Thread(new g(), "OS_GETTAGS_CALLBACK").start();
        }
    }

    public static boolean M1() {
        return O0() && R0();
    }

    public static l0 N(JSONArray jSONArray) {
        int length = jSONArray.length();
        int optInt = jSONArray.optJSONObject(0).optInt("androidNotificationId");
        ArrayList arrayList = new ArrayList();
        boolean z2 = true;
        String str = null;
        JSONObject jSONObject = null;
        for (int i2 = 0; i2 < length; i2++) {
            try {
                jSONObject = jSONArray.getJSONObject(i2);
                if (str == null && jSONObject.has("actionId")) {
                    str = jSONObject.optString("actionId", null);
                }
                if (z2) {
                    z2 = false;
                } else {
                    arrayList.add(new com.onesignal.g0(jSONObject));
                }
            } catch (Throwable th) {
                b(LOG_LEVEL.ERROR, "Error parsing JSON item " + i2 + "/" + length + " for callback.", th);
            }
        }
        return new l0(new com.onesignal.g0(arrayList, jSONObject, optInt), new OSNotificationAction(str != null ? OSNotificationAction.ActionType.ActionTaken : OSNotificationAction.ActionType.Opened, str));
    }

    public static boolean N0() {
        return t && O0();
    }

    public static void N1(boolean z2) {
        yj2 yj2Var = z;
        yj2Var.debug("OneSignal startLocationShared: " + z2);
        k0().n(z2);
        if (z2) {
            return;
        }
        z.debug("OneSignal is shareLocation set false, clearing last location!");
        OneSignalStateSynchronizer.a();
    }

    public static AppEntryAction O() {
        return v;
    }

    public static boolean O0() {
        return u;
    }

    public static void O1() {
        LocationController.g(e, false, false, new n());
    }

    public static boolean P() {
        return E.b();
    }

    public static boolean P0() {
        return t;
    }

    public static void P1() {
        if (V) {
            return;
        }
        V = true;
        if (u && OneSignalStateSynchronizer.g()) {
            T = false;
        }
        O1();
        S = false;
        if (l0() != null) {
            k1();
        } else {
            V0(g, z0(), true);
        }
    }

    public static Activity Q() {
        com.onesignal.a b2 = k7.b();
        if (b2 != null) {
            return b2.d();
        }
        return null;
    }

    public static boolean Q0() {
        return E.h();
    }

    public static void Q1(String str) {
        t1(str);
        R(e).e(str);
        try {
            OneSignalStateSynchronizer.x(new JSONObject().put("parent_player_id", str));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public static nj2 R(Context context) {
        if (context == null) {
            return null;
        }
        if (i0 == null) {
            nj2 nj2Var = new nj2(false);
            i0 = nj2Var;
            nj2Var.a().b(new OSEmailSubscriptionChangedInternalObserver());
        }
        return i0;
    }

    public static boolean R0() {
        long a2 = w0().a();
        long d02 = d0();
        long j2 = a2 - d02;
        yj2 yj2Var = z;
        yj2Var.debug("isPastOnSessionTime currentTimeMillis: " + a2 + " lastSessionTime: " + d02 + " difference: " + j2);
        return j2 >= 30000;
    }

    public static void R1(String str) {
        u1(str);
        T(e).e(str);
    }

    public static q0 S(Context context) {
        if (context == null) {
            return null;
        }
        if (c0 == null) {
            q0 q0Var = new q0(false);
            c0 = q0Var;
            q0Var.b().b(new OSPermissionChangedInternalObserver());
        }
        return c0;
    }

    public static boolean S0() {
        return n == -999;
    }

    public static void S1(String str) {
        v1(str);
        M0();
        U(e).h(str);
        w wVar = o0;
        if (wVar != null) {
            y1(wVar.a, wVar.b, wVar.c);
            o0 = null;
        }
        OneSignalStateSynchronizer.n();
    }

    public static tk2 T(Context context) {
        if (context == null) {
            return null;
        }
        if (l0 == null) {
            tk2 tk2Var = new tk2(false);
            l0 = tk2Var;
            tk2Var.a().b(new OSSMSSubscriptionChangedInternalObserver());
        }
        return l0;
    }

    public static boolean T0() {
        return E.i();
    }

    public static boolean T1() {
        return E.e();
    }

    public static OSSubscriptionState U(Context context) {
        if (context == null) {
            return null;
        }
        if (f0 == null) {
            f0 = new OSSubscriptionState(false, S(context).a());
            S(context).b().a(f0);
            f0.a().b(new OSSubscriptionChangedInternalObserver());
        }
        return f0;
    }

    public static void U0(String str, int i2, Throwable th, String str2) {
        String str3;
        if (str2 == null || !B(LOG_LEVEL.INFO)) {
            str3 = "";
        } else {
            str3 = "\n" + str2 + "\n";
        }
        b(LOG_LEVEL.WARN, "HTTP code: " + i2 + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + str + str3, th);
    }

    public static a1 V() {
        return a1.g(e);
    }

    public static void V0(String str, String str2, boolean z2) {
        if (l0() != null || W) {
            return;
        }
        W = true;
        c1.e(str, str2, new p(z2));
    }

    public static a1 W(Context context) {
        return a1.g(context);
    }

    public static void W0(Context context, JSONObject jSONObject, i0.d dVar) {
        if (N == null) {
            N = new com.onesignal.i0(W(context), z);
        }
        N.j(jSONObject, dVar);
    }

    public static boolean X() {
        return E.g();
    }

    public static void X0(Context context, JSONArray jSONArray) {
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            try {
                String optString = new JSONObject(jSONArray.getJSONObject(i2).optString("custom", null)).optString("i", null);
                if (!Z.contains(optString)) {
                    Z.add(optString);
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("app_id", p0(context));
                    jSONObject.put("player_id", q0(context));
                    jSONObject.put("opened", true);
                    jSONObject.put("device_type", R.e());
                    d1.l("notifications/" + optString, jSONObject, new i());
                }
            } catch (Throwable th) {
                b(LOG_LEVEL.ERROR, "Failed to generate JSON to send notification opened.", th);
            }
        }
    }

    public static String Y() {
        if (l == null && e != null) {
            l = b1.f(b1.a, "OS_EMAIL_ID", null);
        }
        if (TextUtils.isEmpty(l)) {
            return null;
        }
        return l;
    }

    public static void Y0() {
        LOG_LEVEL log_level = LOG_LEVEL.DEBUG;
        a(log_level, "Application on focus");
        C1(true);
        AppEntryAction appEntryAction = v;
        AppEntryAction appEntryAction2 = AppEntryAction.NOTIFICATION_CLICK;
        if (!appEntryAction.equals(appEntryAction2)) {
            D(v);
            if (!v.equals(appEntryAction2)) {
                v = AppEntryAction.APP_OPEN;
            }
        }
        LocationController.l();
        com.onesignal.p.d.g();
        if (OSUtils.T(g)) {
            return;
        }
        if (!E.k()) {
            a(log_level, "Delay onAppFocus logic due to missing remote params");
            V0(g, z0(), false);
            return;
        }
        Z0();
    }

    public static n0<Object, oj2> Z() {
        if (k0 == null) {
            k0 = new n0<>("onOSEmailSubscriptionChanged", true);
        }
        return k0;
    }

    public static void Z0() {
        if (L1("onAppFocus")) {
            return;
        }
        b0().b();
        F();
        m1 m1Var = w;
        if (m1Var != null) {
            m1Var.u();
        }
        OSNotificationRestoreWorkManager.c(e, false);
        j1();
        if (y != null && a0()) {
            y.f();
        }
        u0.q().p(e);
    }

    public static void a(LOG_LEVEL log_level, String str) {
        b(log_level, str, null);
    }

    public static boolean a0() {
        return E.c();
    }

    public static void a1() {
        LOG_LEVEL log_level = LOG_LEVEL.DEBUG;
        a(log_level, "Application lost focus initDone: " + t);
        C1(false);
        v = AppEntryAction.APP_CLOSE;
        D1(w0().a());
        LocationController.l();
        if (!t) {
            if (G.g("onAppLostFocus()")) {
                z.error("Waiting for remote params. Moving onAppLostFocus() operation to a pending task queue.");
                G.c(new r());
                return;
            }
            return;
        }
        C();
    }

    public static void b(LOG_LEVEL log_level, String str, Throwable th) {
        StringWriter stringWriter;
        if (log_level.compareTo(j) < 1 && log_level != LOG_LEVEL.VERBOSE && log_level != LOG_LEVEL.DEBUG && log_level != LOG_LEVEL.INFO && log_level != LOG_LEVEL.WARN && log_level != LOG_LEVEL.ERROR) {
            LOG_LEVEL log_level2 = LOG_LEVEL.FATAL;
        }
        if (log_level.compareTo(i) >= 1 || Q() == null) {
            return;
        }
        try {
            String str2 = str + "\n";
            if (th != null) {
                th.printStackTrace(new PrintWriter(new StringWriter()));
                str2 = (str2 + th.getMessage()) + stringWriter.toString();
            }
            OSUtils.S(new q(log_level, str2));
        } catch (Throwable unused) {
        }
    }

    public static FocusTimeController b0() {
        if (A == null) {
            A = new FocusTimeController(new com.onesignal.v(), z);
        }
        return A;
    }

    public static void b1() {
        j1();
    }

    public static OSInAppMessageController c0() {
        return C.a(V(), F, e0(), t0(), o);
    }

    public static void c1() {
        if (i1() || !u) {
            return;
        }
        Z0();
    }

    public static long d0() {
        return b1.d(b1.a, "OS_LAST_SESSION_TIME", -31000L);
    }

    public static void d1(LOG_LEVEL log_level, String str) {
        a(log_level, str);
    }

    public static yj2 e0() {
        return z;
    }

    public static void e1(Activity activity, JSONArray jSONArray) {
        try {
            Intent b2 = com.onesignal.f.a.a(activity, jSONArray.getJSONObject(0)).b();
            if (b2 != null) {
                yj2 yj2Var = z;
                yj2Var.info("SDK running startActivity with Intent: " + b2);
                activity.startActivity(b2);
            } else {
                z.info("SDK not showing an Activity automatically due to it's settings.");
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public static String f0(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            return null;
        }
        try {
            jSONObject2 = new JSONObject(jSONObject.getString("custom"));
        } catch (JSONException unused) {
            z.debug("Not a OneSignal formatted FCM message. No 'custom' field in the JSONObject.");
        }
        if (jSONObject2.has("i")) {
            return jSONObject2.optString("i", null);
        }
        z.debug("Not a OneSignal formatted FCM message. No 'i' field in custom.");
        return null;
    }

    public static void f1(boolean z2, h0 h0Var) {
        com.onesignal.p.d.h(z2, h0Var);
    }

    public static p0 g0() {
        if (L == null) {
            synchronized (O) {
                if (L == null) {
                    if (M == null) {
                        M = new hk2(z, H, V(), I);
                    }
                    L = new p0(K, M);
                }
            }
        }
        return L;
    }

    public static void g1(c0 c0Var, boolean z2) {
        if (G.g("promptLocation()")) {
            z.error("Waiting for remote params. Moving promptLocation() operation to a pending queue.");
            G.c(new j(c0Var, z2));
        } else if (L1("promptLocation()")) {
        } else {
            LocationController.g(e, true, z2, new l(c0Var));
        }
    }

    public static n0<Object, sk2> h0() {
        if (e0 == null) {
            e0 = new n0<>("onOSPermissionChanged", true);
        }
        return e0;
    }

    public static boolean h1(int i2) {
        return i2 < -6;
    }

    public static f1 i0() {
        f1 f1Var = p0;
        if (f1Var != null) {
            return f1Var;
        }
        if (OSUtils.C()) {
            p0 = new g1();
        } else if (OSUtils.B()) {
            if (OSUtils.r()) {
                p0 = j0();
            }
        } else {
            p0 = new j1();
        }
        return p0;
    }

    public static boolean i1() {
        String a2;
        Context b2;
        if (t) {
            return false;
        }
        zl0 zl0Var = b0;
        if (zl0Var == null) {
            a2 = o0();
            b2 = e;
            z.error("Trying to continue OneSignal with null delayed params");
        } else {
            a2 = zl0Var.a();
            b2 = b0.b();
        }
        yj2 yj2Var = z;
        yj2Var.debug("reassignDelayedInitParams with appContext: " + e);
        b0 = null;
        B1(a2);
        if (t) {
            return true;
        }
        if (b2 == null) {
            z.error("Trying to continue OneSignal with null delayed params context");
            return false;
        }
        L0(b2);
        return true;
    }

    public static i1 j0() {
        c1.d dVar = E.d().l;
        return new i1(e, dVar != null ? new i1.a(dVar.a, dVar.b, dVar.c) : null);
    }

    public static void j1() {
        S(e).e();
    }

    public static s0 k0() {
        return E;
    }

    public static void k1() {
        i0().a(e, h, new o());
    }

    public static c1.f l0() {
        return E.d();
    }

    public static void l1() {
        yj2 yj2Var = z;
        yj2Var.debug("registerUser:registerForPushFired:" + S + ", locationFired: " + T + ", remoteParams: " + l0() + ", appId: " + g);
        if (S && T && l0() != null && g != null) {
            new Thread(new a(), "OS_REG_USER").start();
        } else {
            z.debug("registerUser not possible");
        }
    }

    public static String m0() {
        if (m == null && e != null) {
            m = b1.f(b1.a, "PREFS_OS_SMS_ID", null);
        }
        if (TextUtils.isEmpty(m)) {
            return null;
        }
        return m;
    }

    public static void m1() throws JSONException {
        LocationController.d dVar;
        String packageName = e.getPackageName();
        PackageManager packageManager = e.getPackageManager();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("app_id", o0());
        jSONObject.put("device_os", Build.VERSION.RELEASE);
        jSONObject.put("timezone", y0());
        jSONObject.put("timezone_id", x0());
        jSONObject.put("language", o.b());
        jSONObject.put("sdk", "040803");
        jSONObject.put("sdk_type", P);
        jSONObject.put("android_package", packageName);
        jSONObject.put("device_model", Build.MODEL);
        try {
            jSONObject.put("game_version", packageManager.getPackageInfo(packageName, 0).versionCode);
        } catch (PackageManager.NameNotFoundException unused) {
        }
        jSONObject.put("net_type", R.i());
        jSONObject.put("carrier", R.d());
        jSONObject.put("rooted", m93.a());
        OneSignalStateSynchronizer.v(jSONObject, null);
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("identifier", Q);
        jSONObject2.put("subscribableStatus", n);
        jSONObject2.put("androidPermission", A());
        jSONObject2.put("device_type", R.e());
        OneSignalStateSynchronizer.x(jSONObject2);
        if (Q0() && (dVar = X) != null) {
            OneSignalStateSynchronizer.w(dVar);
        }
        z.debug("registerUserTask calling readyToUpdate");
        OneSignalStateSynchronizer.m(true);
        V = false;
    }

    public static n0<Object, uk2> n0() {
        if (n0 == null) {
            n0 = new n0<>("onSMSSubscriptionChanged", true);
        }
        return n0;
    }

    public static void n1(v vVar) {
        d.remove(vVar);
    }

    public static String o0() {
        return p0(e);
    }

    public static void o1(int i2) {
        if (!G.g("removeNotification()") && N != null) {
            if (L1("removeNotification()")) {
                return;
            }
            N.k(i2, new WeakReference<>(e));
            return;
        }
        z.error("Waiting for remote params. Moving removeNotification() operation to a pending queue.");
        G.c(new m(i2));
    }

    public static String p0(Context context) {
        if (context == null) {
            return null;
        }
        return b1.f(b1.a, "GT_APP_ID", null);
    }

    public static boolean p1() {
        return e == null || (T0() && !T1());
    }

    public static String q0(Context context) {
        if (context == null) {
            return null;
        }
        return b1.f(b1.a, "GT_PLAYER_ID", null);
    }

    public static void q1() {
        if (z0() == null) {
            z.b("getTags called under a null user!");
        } else {
            M0();
        }
    }

    public static String r0() {
        return "040803";
    }

    public static void r1(JSONArray jSONArray) {
        if (r == null) {
            Y.add(jSONArray);
            return;
        }
        l0 N2 = N(jSONArray);
        x(N2, v);
        K(N2);
    }

    public static t0 s0() {
        return K;
    }

    public static void s1(String str) {
        if (e == null) {
            return;
        }
        b1.m(b1.a, "GT_APP_ID", str);
    }

    public static vk2 t0() {
        return I;
    }

    public static void t1(String str) {
        l = str;
        if (e == null) {
            return;
        }
        b1.m(b1.a, "OS_EMAIL_ID", "".equals(l) ? null : l);
    }

    public static n0<Object, xk2> u0() {
        if (h0 == null) {
            h0 = new n0<>("onOSSubscriptionChanged", true);
        }
        return h0;
    }

    public static void u1(String str) {
        m = str;
        if (e == null) {
            return;
        }
        b1.m(b1.a, "PREFS_OS_SMS_ID", "".equals(m) ? null : m);
    }

    public static void v0(x xVar) {
        if (G.g("getTags()")) {
            z.error("Waiting for remote params. Moving getTags() operation to a pending queue.");
            G.c(new e(xVar));
        } else if (L1("getTags()")) {
        } else {
            if (xVar == null) {
                z.error("getTags called with null GetTagsHandler!");
            } else {
                new Thread(new f(xVar), "OS_GETTAGS").start();
            }
        }
    }

    public static void v1(String str) {
        k = str;
        if (e == null) {
            return;
        }
        b1.m(b1.a, "GT_PLAYER_ID", k);
    }

    public static zk2 w0() {
        return D;
    }

    public static boolean w1() {
        boolean l2 = OneSignalStateSynchronizer.l();
        yj2 yj2Var = z;
        yj2Var.debug("OneSignal scheduleSyncService unsyncedChanges: " + l2);
        if (l2) {
            u0.q().s(e);
        }
        boolean m2 = LocationController.m(e);
        yj2 yj2Var2 = z;
        yj2Var2.debug("OneSignal scheduleSyncService locationScheduled: " + m2);
        return m2 || l2;
    }

    public static void x(v vVar, AppEntryAction appEntryAction) {
        if (appEntryAction.equals(AppEntryAction.NOTIFICATION_CLICK)) {
            return;
        }
        d.add(vVar);
    }

    public static String x0() {
        if (Build.VERSION.SDK_INT >= 26) {
            return ZoneId.systemDefault().getId();
        }
        return TimeZone.getDefault().getID();
    }

    public static void x1(List<com.onesignal.z> list) {
        p0 p0Var = L;
        if (p0Var != null && g != null) {
            p0Var.m(list);
        } else {
            a(LOG_LEVEL.ERROR, "Make sure OneSignal.init is called first");
        }
    }

    public static void y(JSONObject jSONObject) {
        try {
            jSONObject.put("net_type", R.i());
        } catch (Throwable unused) {
        }
    }

    public static int y0() {
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        int rawOffset = timeZone.getRawOffset();
        if (timeZone.inDaylightTime(new Date())) {
            rawOffset += timeZone.getDSTSavings();
        }
        return rawOffset / 1000;
    }

    public static void y1(JSONArray jSONArray, boolean z2, d1.g gVar) {
        if (L1("sendPurchases()")) {
            return;
        }
        if (z0() == null) {
            w wVar = new w(jSONArray);
            o0 = wVar;
            wVar.b = z2;
            wVar.c = gVar;
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("app_id", o0());
            if (z2) {
                jSONObject.put("existing", true);
            }
            jSONObject.put("purchases", jSONArray);
            OneSignalStateSynchronizer.p(jSONObject, gVar);
        } catch (Throwable th) {
            b(LOG_LEVEL.ERROR, "Failed to generate JSON for sendPurchases.", th);
        }
    }

    public static void z(String str) {
        AppEntryAction appEntryAction = AppEntryAction.NOTIFICATION_CLICK;
        v = appEntryAction;
        K.j(appEntryAction, str);
    }

    public static String z0() {
        Context context;
        if (k == null && (context = e) != null) {
            k = q0(context);
        }
        return k;
    }

    public static void z1(JSONObject jSONObject) {
        A1(jSONObject, null);
    }
}
