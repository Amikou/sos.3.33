package com.onesignal;

import android.content.Context;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.b;
import androidx.work.c;
import com.onesignal.OneSignal;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class OSNotificationWorkManager {
    public static Set<String> a = OSUtils.K();

    /* loaded from: classes2.dex */
    public static class NotificationWorker extends Worker {
        public NotificationWorker(Context context, WorkerParameters workerParameters) {
            super(context, workerParameters);
        }

        @Override // androidx.work.Worker
        public ListenableWorker.a r() {
            androidx.work.b g = g();
            try {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.d1(log_level, "NotificationWorker running doWork with data: " + g);
                int i = g.i("android_notif_id", 0);
                JSONObject jSONObject = new JSONObject(g.l("json_payload"));
                long k = g.k("timestamp", System.currentTimeMillis() / 1000);
                s(a(), i, jSONObject, g.h("is_restoring", false), Long.valueOf(k));
                return ListenableWorker.a.c();
            } catch (JSONException e) {
                OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.d1(log_level2, "Error occurred doing work for job with id: " + e().toString());
                e.printStackTrace();
                return ListenableWorker.a.a();
            }
        }

        public final void s(Context context, int i, JSONObject jSONObject, boolean z, Long l) {
            g0 g0Var = new g0(null, jSONObject, i);
            m0 m0Var = new m0(new h0(context, g0Var, jSONObject, z, true, l), g0Var);
            OneSignal.d0 d0Var = OneSignal.p;
            if (d0Var != null) {
                try {
                    d0Var.a(context, m0Var);
                    return;
                } catch (Throwable th) {
                    OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "remoteNotificationReceived throw an exception. Displaying normal OneSignal notification.", th);
                    m0Var.b(g0Var);
                    throw th;
                }
            }
            OneSignal.a(OneSignal.LOG_LEVEL.WARN, "remoteNotificationReceivedHandler not setup, displaying normal OneSignal notification");
            m0Var.b(g0Var);
        }
    }

    public static boolean a(String str) {
        if (OSUtils.I(str)) {
            if (a.contains(str)) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.a(log_level, "OSNotificationWorkManager notification with notificationId: " + str + " already queued");
                return false;
            }
            a.add(str);
            return true;
        }
        return true;
    }

    public static void b(Context context, String str, int i, String str2, long j, boolean z, boolean z2) {
        androidx.work.b a2 = new b.a().f("android_notif_id", i).h("json_payload", str2).g("timestamp", j).e("is_restoring", z).a();
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "OSNotificationWorkManager enqueueing notification work with notificationId: " + str + " and jsonPayload: " + str2);
        gq4.f(context).d(str, ExistingWorkPolicy.KEEP, new c.a(NotificationWorker.class).g(a2).b());
    }

    public static void c(String str) {
        if (OSUtils.I(str)) {
            a.remove(str);
        }
    }
}
