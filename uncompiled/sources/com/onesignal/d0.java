package com.onesignal;

import android.content.ContentValues;
import com.onesignal.OneSignal;
import com.onesignal.d1;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInAppMessageRepository.java */
/* loaded from: classes2.dex */
public class d0 {
    public final a1 a;
    public final yj2 b;
    public final vk2 c;
    public int d = 0;

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class a extends JSONObject {
        public final /* synthetic */ String a;
        public final /* synthetic */ int b;
        public final /* synthetic */ String c;
        public final /* synthetic */ String d;
        public final /* synthetic */ String e;
        public final /* synthetic */ boolean f;

        public a(d0 d0Var, String str, int i, String str2, String str3, String str4, boolean z) throws JSONException {
            this.a = str;
            this.b = i;
            this.c = str2;
            this.d = str3;
            this.e = str4;
            this.f = z;
            put("app_id", str);
            put("device_type", i);
            put("player_id", str2);
            put("click_id", str3);
            put("variant_id", str4);
            if (z) {
                put("first_click", true);
            }
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class b extends d1.g {
        public final /* synthetic */ Set a;
        public final /* synthetic */ i b;

        public b(Set set, i iVar) {
            this.a = set;
            this.b = iVar;
        }

        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            d0.this.u("engagement", i, str);
            this.b.b(str);
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            d0.this.v("engagement", str);
            d0.this.w(this.a);
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class c extends JSONObject {
        public final /* synthetic */ String a;
        public final /* synthetic */ String b;
        public final /* synthetic */ String c;
        public final /* synthetic */ int d;
        public final /* synthetic */ String e;

        public c(d0 d0Var, String str, String str2, String str3, int i, String str4) throws JSONException {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = i;
            this.e = str4;
            put("app_id", str);
            put("player_id", str2);
            put("variant_id", str3);
            put("device_type", i);
            put("page_id", str4);
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class d extends d1.g {
        public final /* synthetic */ Set a;
        public final /* synthetic */ i b;

        public d(Set set, i iVar) {
            this.a = set;
            this.b = iVar;
        }

        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            d0.this.u("page impression", i, str);
            this.b.b(str);
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            d0.this.v("page impression", str);
            d0.this.C(this.a);
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class e extends JSONObject {
        public final /* synthetic */ String a;
        public final /* synthetic */ String b;
        public final /* synthetic */ String c;
        public final /* synthetic */ int d;

        public e(d0 d0Var, String str, String str2, String str3, int i) throws JSONException {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = i;
            put("app_id", str);
            put("player_id", str2);
            put("variant_id", str3);
            put("device_type", i);
            put("first_impression", true);
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class f extends d1.g {
        public final /* synthetic */ Set a;
        public final /* synthetic */ i b;

        public f(Set set, i iVar) {
            this.a = set;
            this.b = iVar;
        }

        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            d0.this.u("impression", i, str);
            this.b.b(str);
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            d0.this.v("impression", str);
            d0.this.z(this.a);
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class g extends d1.g {
        public final /* synthetic */ i a;

        public g(i iVar) {
            this.a = iVar;
        }

        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            d0.this.u("html", i, str);
            this.a.b(str);
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            this.a.a(str);
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public class h extends d1.g {
        public final /* synthetic */ i a;

        public h(i iVar) {
            this.a = iVar;
        }

        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            d0.this.u("html", i, str);
            JSONObject jSONObject = new JSONObject();
            if (!OSUtils.U(i) || d0.this.d >= OSUtils.a) {
                d0.this.d = 0;
                try {
                    jSONObject.put("retry", false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                d0.g(d0.this);
                try {
                    jSONObject.put("retry", true);
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
            this.a.b(jSONObject.toString());
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            d0.this.d = 0;
            this.a.a(str);
        }
    }

    /* compiled from: OSInAppMessageRepository.java */
    /* loaded from: classes2.dex */
    public interface i {
        void a(String str);

        void b(String str);
    }

    public d0(a1 a1Var, yj2 yj2Var, vk2 vk2Var) {
        this.a = a1Var;
        this.b = yj2Var;
        this.c = vk2Var;
    }

    public static /* synthetic */ int g(d0 d0Var) {
        int i2 = d0Var.d;
        d0Var.d = i2 + 1;
        return i2;
    }

    public synchronized void A(x xVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("message_id", xVar.a);
        contentValues.put("display_quantity", Integer.valueOf(xVar.e().a()));
        contentValues.put("last_display", Long.valueOf(xVar.e().b()));
        contentValues.put("click_ids", xVar.c().toString());
        contentValues.put("displayed_in_session", Boolean.valueOf(xVar.g()));
        if (this.a.a("in_app_message", contentValues, "message_id = ?", new String[]{xVar.a}) == 0) {
            this.a.e("in_app_message", null, contentValues);
        }
    }

    public void B(Date date) {
        this.c.i(b1.a, "PREFS_OS_LAST_TIME_IAM_DISMISSED", date != null ? date.toString() : null);
    }

    public void C(Set<String> set) {
        this.c.g(b1.a, "PREFS_OS_PAGE_IMPRESSIONED_IAMS", set);
    }

    public void D(String str, String str2, String str3, int i2, String str4, String str5, boolean z, Set<String> set, i iVar) {
        try {
            a aVar = new a(this, str, i2, str2, str5, str3, z);
            d1.j("in_app_messages/" + str4 + "/click", aVar, new b(set, iVar));
        } catch (JSONException e2) {
            e2.printStackTrace();
            this.b.error("Unable to execute in-app message action HTTP request due to invalid JSON");
        }
    }

    public void E(String str, String str2, String str3, int i2, String str4, Set<String> set, i iVar) {
        try {
            e eVar = new e(this, str, str2, str3, i2);
            d1.j("in_app_messages/" + str4 + "/impression", eVar, new f(set, iVar));
        } catch (JSONException e2) {
            e2.printStackTrace();
            this.b.error("Unable to execute in-app message impression HTTP request due to invalid JSON");
        }
    }

    public void F(String str, String str2, String str3, int i2, String str4, String str5, Set<String> set, i iVar) {
        try {
            c cVar = new c(this, str, str2, str3, i2, str5);
            d1.j("in_app_messages/" + str4 + "/pageImpression", cVar, new d(set, iVar));
        } catch (JSONException e2) {
            e2.printStackTrace();
            this.b.error("Unable to execute in-app message impression HTTP request due to invalid JSON");
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x0072, code lost:
        if (r12.isClosed() != false) goto L19;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public synchronized void h() {
        /*
            r13 = this;
            monitor-enter(r13)
            java.lang.String r0 = "message_id"
            java.lang.String r1 = "click_ids"
            java.lang.String[] r4 = new java.lang.String[]{r0, r1}     // Catch: java.lang.Throwable -> Lb6
            java.lang.String r0 = "last_display < ?"
            long r1 = java.lang.System.currentTimeMillis()     // Catch: java.lang.Throwable -> Lb6
            r5 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r5
            r5 = 15552000(0xed4e00, double:7.683709E-317)
            long r1 = r1 - r5
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch: java.lang.Throwable -> Lb6
            r2 = 1
            java.lang.String[] r10 = new java.lang.String[r2]     // Catch: java.lang.Throwable -> Lb6
            r2 = 0
            r10[r2] = r1     // Catch: java.lang.Throwable -> Lb6
            java.util.Set r1 = com.onesignal.OSUtils.K()     // Catch: java.lang.Throwable -> Lb6
            java.util.Set r11 = com.onesignal.OSUtils.K()     // Catch: java.lang.Throwable -> Lb6
            r12 = 0
            com.onesignal.a1 r2 = r13.a     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            java.lang.String r3 = "in_app_message"
            r7 = 0
            r8 = 0
            r9 = 0
            r5 = r0
            r6 = r10
            android.database.Cursor r12 = r2.c(r3, r4, r5, r6, r7, r8, r9)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            if (r12 == 0) goto L78
            int r2 = r12.getCount()     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            if (r2 != 0) goto L3f
            goto L78
        L3f:
            boolean r2 = r12.moveToFirst()     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            if (r2 == 0) goto L6e
        L45:
            java.lang.String r2 = "message_id"
            int r2 = r12.getColumnIndex(r2)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            java.lang.String r2 = r12.getString(r2)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            java.lang.String r3 = "click_ids"
            int r3 = r12.getColumnIndex(r3)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            java.lang.String r3 = r12.getString(r3)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            r1.add(r2)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            r2.<init>(r3)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            java.util.Set r2 = com.onesignal.OSUtils.L(r2)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            r11.addAll(r2)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            boolean r2 = r12.moveToNext()     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            if (r2 != 0) goto L45
        L6e:
            boolean r2 = r12.isClosed()     // Catch: java.lang.Throwable -> Lb6
            if (r2 != 0) goto L9b
        L74:
            r12.close()     // Catch: java.lang.Throwable -> Lb6
            goto L9b
        L78:
            com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.DEBUG     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            java.lang.String r3 = "Attempted to clean 6 month old IAM data, but none exists!"
            com.onesignal.OneSignal.d1(r2, r3)     // Catch: java.lang.Throwable -> L8c org.json.JSONException -> L8e
            if (r12 == 0) goto L8a
            boolean r0 = r12.isClosed()     // Catch: java.lang.Throwable -> Lb6
            if (r0 != 0) goto L8a
            r12.close()     // Catch: java.lang.Throwable -> Lb6
        L8a:
            monitor-exit(r13)
            return
        L8c:
            r0 = move-exception
            goto Laa
        L8e:
            r2 = move-exception
            r2.printStackTrace()     // Catch: java.lang.Throwable -> L8c
            if (r12 == 0) goto L9b
            boolean r2 = r12.isClosed()     // Catch: java.lang.Throwable -> Lb6
            if (r2 != 0) goto L9b
            goto L74
        L9b:
            com.onesignal.a1 r2 = r13.a     // Catch: java.lang.Throwable -> Lb6
            java.lang.String r3 = "in_app_message"
            r2.d(r3, r0, r10)     // Catch: java.lang.Throwable -> Lb6
            r13.j(r1)     // Catch: java.lang.Throwable -> Lb6
            r13.i(r11)     // Catch: java.lang.Throwable -> Lb6
            monitor-exit(r13)
            return
        Laa:
            if (r12 == 0) goto Lb5
            boolean r1 = r12.isClosed()     // Catch: java.lang.Throwable -> Lb6
            if (r1 != 0) goto Lb5
            r12.close()     // Catch: java.lang.Throwable -> Lb6
        Lb5:
            throw r0     // Catch: java.lang.Throwable -> Lb6
        Lb6:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.d0.h():void");
    }

    public final void i(Set<String> set) {
        String str;
        Set<String> g2;
        if (set == null || set.size() <= 0 || (g2 = b1.g((str = b1.a), "PREFS_OS_CLICKED_CLICK_IDS_IAMS", null)) == null || g2.size() <= 0) {
            return;
        }
        g2.removeAll(set);
        b1.n(str, "PREFS_OS_CLICKED_CLICK_IDS_IAMS", g2);
    }

    public final void j(Set<String> set) {
        if (set == null || set.size() <= 0) {
            return;
        }
        String str = b1.a;
        Set<String> g2 = b1.g(str, "PREFS_OS_DISPLAYED_IAMS", null);
        Set<String> g3 = b1.g(str, "PREFS_OS_IMPRESSIONED_IAMS", null);
        if (g2 != null && g2.size() > 0) {
            g2.removeAll(set);
            b1.n(str, "PREFS_OS_DISPLAYED_IAMS", g2);
        }
        if (g3 == null || g3.size() <= 0) {
            return;
        }
        g3.removeAll(set);
        b1.n(str, "PREFS_OS_IMPRESSIONED_IAMS", g3);
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x0072, code lost:
        if (r1.isClosed() != false) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0074, code lost:
        r1.close();
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0088, code lost:
        if (r1.isClosed() == false) goto L18;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public synchronized java.util.List<com.onesignal.x> k() {
        /*
            r10 = this;
            monitor-enter(r10)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch: java.lang.Throwable -> L99
            r0.<init>()     // Catch: java.lang.Throwable -> L99
            r1 = 0
            com.onesignal.a1 r2 = r10.a     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.lang.String r3 = "in_app_message"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r1 = r2.c(r3, r4, r5, r6, r7, r8, r9)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            boolean r2 = r1.moveToFirst()     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            if (r2 == 0) goto L6e
        L1b:
            java.lang.String r2 = "message_id"
            int r2 = r1.getColumnIndex(r2)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.lang.String r2 = r1.getString(r2)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.lang.String r3 = "click_ids"
            int r3 = r1.getColumnIndex(r3)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.lang.String r3 = r1.getString(r3)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.lang.String r4 = "display_quantity"
            int r4 = r1.getColumnIndex(r4)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            int r4 = r1.getInt(r4)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.lang.String r5 = "last_display"
            int r5 = r1.getColumnIndex(r5)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            long r5 = r1.getLong(r5)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.lang.String r7 = "displayed_in_session"
            int r7 = r1.getColumnIndex(r7)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            int r7 = r1.getInt(r7)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            r8 = 1
            if (r7 != r8) goto L51
            goto L52
        L51:
            r8 = 0
        L52:
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            r7.<init>(r3)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            java.util.Set r3 = com.onesignal.OSUtils.L(r7)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            com.onesignal.x r7 = new com.onesignal.x     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            com.onesignal.c0 r9 = new com.onesignal.c0     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            r9.<init>(r4, r5)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            r7.<init>(r2, r3, r8, r9)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            r0.add(r7)     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            boolean r2 = r1.moveToNext()     // Catch: java.lang.Throwable -> L78 org.json.JSONException -> L7a
            if (r2 != 0) goto L1b
        L6e:
            boolean r2 = r1.isClosed()     // Catch: java.lang.Throwable -> L99
            if (r2 != 0) goto L8b
        L74:
            r1.close()     // Catch: java.lang.Throwable -> L99
            goto L8b
        L78:
            r0 = move-exception
            goto L8d
        L7a:
            r2 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r3 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch: java.lang.Throwable -> L78
            java.lang.String r4 = "Generating JSONArray from iam click ids:JSON Failed."
            com.onesignal.OneSignal.b(r3, r4, r2)     // Catch: java.lang.Throwable -> L78
            if (r1 == 0) goto L8b
            boolean r2 = r1.isClosed()     // Catch: java.lang.Throwable -> L99
            if (r2 != 0) goto L8b
            goto L74
        L8b:
            monitor-exit(r10)
            return r0
        L8d:
            if (r1 == 0) goto L98
            boolean r2 = r1.isClosed()     // Catch: java.lang.Throwable -> L99
            if (r2 != 0) goto L98
            r1.close()     // Catch: java.lang.Throwable -> L99
        L98:
            throw r0     // Catch: java.lang.Throwable -> L99
        L99:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.d0.k():java.util.List");
    }

    public Set<String> l() {
        return this.c.c(b1.a, "PREFS_OS_CLICKED_CLICK_IDS_IAMS", null);
    }

    public Set<String> m() {
        return this.c.c(b1.a, "PREFS_OS_DISPLAYED_IAMS", null);
    }

    public void n(String str, String str2, String str3, i iVar) {
        d1.e(t(str2, str3, str), new h(iVar), null);
    }

    public void o(String str, String str2, i iVar) {
        d1.e("in_app_messages/device_preview?preview_id=" + str2 + "&app_id=" + str, new g(iVar), null);
    }

    public Set<String> p() {
        return this.c.c(b1.a, "PREFS_OS_IMPRESSIONED_IAMS", null);
    }

    public Date q() {
        String e2 = this.c.e(b1.a, "PREFS_OS_LAST_TIME_IAM_DISMISSED", null);
        if (e2 == null) {
            return null;
        }
        try {
            return new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH).parse(e2);
        } catch (ParseException e3) {
            OneSignal.d1(OneSignal.LOG_LEVEL.ERROR, e3.getLocalizedMessage());
            return null;
        }
    }

    public String r() {
        return this.c.e(b1.a, "PREFS_OS_CACHED_IAMS", null);
    }

    public Set<String> s() {
        return this.c.c(b1.a, "PREFS_OS_PAGE_IMPRESSIONED_IAMS", null);
    }

    public final String t(String str, String str2, String str3) {
        if (str2 == null) {
            yj2 yj2Var = this.b;
            yj2Var.error("Unable to find a variant for in-app message " + str);
            return null;
        }
        return "in_app_messages/" + str + "/variants/" + str2 + "/html?app_id=" + str3;
    }

    public final void u(String str, int i2, String str2) {
        yj2 yj2Var = this.b;
        yj2Var.error("Encountered a " + i2 + " error while attempting in-app message " + str + " request: " + str2);
    }

    public final void v(String str, String str2) {
        yj2 yj2Var = this.b;
        yj2Var.debug("Successful post for in-app message " + str + " request: " + str2);
    }

    public final void w(Set<String> set) {
        this.c.g(b1.a, "PREFS_OS_CLICKED_CLICK_IDS_IAMS", set);
    }

    public void x(Set<String> set) {
        this.c.g(b1.a, "PREFS_OS_DISPLAYED_IAMS", set);
    }

    public void y(String str) {
        this.c.i(b1.a, "PREFS_OS_CACHED_IAMS", str);
    }

    public final void z(Set<String> set) {
        this.c.g(b1.a, "PREFS_OS_IMPRESSIONED_IAMS", set);
    }
}
