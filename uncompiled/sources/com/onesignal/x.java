package com.onesignal;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInAppMessageInternal.java */
/* loaded from: classes2.dex */
public class x extends pj2 {
    public HashMap<String, HashMap<String, String>> b;
    public ArrayList<ArrayList<OSTrigger>> c;
    public Set<String> d;
    public c0 e;
    public double f;
    public boolean g;
    public boolean h;
    public boolean i;
    public Date j;
    public boolean k;
    public boolean l;

    public x(boolean z) {
        super("");
        this.e = new c0();
        this.g = false;
        this.h = false;
        this.k = z;
    }

    public void a(String str) {
        this.d.add(str);
    }

    public void b() {
        this.d.clear();
    }

    public Set<String> c() {
        return this.d;
    }

    public boolean d() {
        return this.l;
    }

    public c0 e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || x.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((x) obj).a);
    }

    public boolean f(String str) {
        return !this.d.contains(str);
    }

    public boolean g() {
        return this.g;
    }

    public boolean h() {
        if (this.j == null) {
            return false;
        }
        return this.j.before(new Date());
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public boolean i() {
        return this.h;
    }

    public final Date j(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("end_time");
            if (string.equals("null")) {
                return null;
            }
            try {
                return dn2.a().parse(string);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        } catch (JSONException unused) {
        }
    }

    public ArrayList<ArrayList<OSTrigger>> k(JSONArray jSONArray) throws JSONException {
        ArrayList<ArrayList<OSTrigger>> arrayList = new ArrayList<>();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONArray jSONArray2 = jSONArray.getJSONArray(i);
            ArrayList<OSTrigger> arrayList2 = new ArrayList<>();
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                arrayList2.add(new OSTrigger(jSONArray2.getJSONObject(i2)));
            }
            arrayList.add(arrayList2);
        }
        return arrayList;
    }

    public final HashMap<String, HashMap<String, String>> l(JSONObject jSONObject) throws JSONException {
        HashMap<String, HashMap<String, String>> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            JSONObject jSONObject2 = jSONObject.getJSONObject(next);
            HashMap<String, String> hashMap2 = new HashMap<>();
            Iterator<String> keys2 = jSONObject2.keys();
            while (keys2.hasNext()) {
                String next2 = keys2.next();
                hashMap2.put(next2, jSONObject2.getString(next2));
            }
            hashMap.put(next, hashMap2);
        }
        return hashMap;
    }

    public void m(String str) {
        this.d.remove(str);
    }

    public void n(double d) {
        this.f = d;
    }

    public void o(boolean z) {
        this.g = z;
    }

    public void p(boolean z) {
        this.h = z;
    }

    public boolean q() {
        if (this.i) {
            return false;
        }
        this.i = true;
        return true;
    }

    public String toString() {
        return "OSInAppMessage{messageId='" + this.a + "', variants=" + this.b + ", triggers=" + this.c + ", clickedClickIds=" + this.d + ", redisplayStats=" + this.e + ", displayDuration=" + this.f + ", displayedInSession=" + this.g + ", triggerChanged=" + this.h + ", actionTaken=" + this.i + ", isPreview=" + this.k + ", endTime=" + this.j + ", hasLiquid=" + this.l + '}';
    }

    public x(String str, Set<String> set, boolean z, c0 c0Var) {
        super(str);
        this.e = new c0();
        this.g = false;
        this.h = false;
        this.d = set;
        this.g = z;
        this.e = c0Var;
    }

    public x(JSONObject jSONObject) throws JSONException {
        super(jSONObject.getString("id"));
        this.e = new c0();
        this.g = false;
        this.h = false;
        this.b = l(jSONObject.getJSONObject("variants"));
        this.c = k(jSONObject.getJSONArray("triggers"));
        this.d = new HashSet();
        this.j = j(jSONObject);
        if (jSONObject.has("has_liquid")) {
            this.l = jSONObject.getBoolean("has_liquid");
        }
        if (jSONObject.has("redisplay")) {
            this.e = new c0(jSONObject.getJSONObject("redisplay"));
        }
    }
}
