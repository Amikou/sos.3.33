package com.onesignal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

/* compiled from: OSNotificationDataController.java */
/* loaded from: classes2.dex */
public class i0 extends r {
    public final a1 a;
    public final yj2 b;

    /* compiled from: OSNotificationDataController.java */
    /* loaded from: classes2.dex */
    public class a extends dm {
        public a() {
        }

        @Override // defpackage.dm, java.lang.Runnable
        public void run() {
            super.run();
            i0.this.a.d("notification", "created_time < ?", new String[]{String.valueOf((OneSignal.w0().a() / 1000) - 604800)});
        }
    }

    /* compiled from: OSNotificationDataController.java */
    /* loaded from: classes2.dex */
    public class b extends dm {
        public final /* synthetic */ WeakReference a;
        public final /* synthetic */ int f0;

        public b(WeakReference weakReference, int i) {
            this.a = weakReference;
            this.f0 = i;
        }

        @Override // defpackage.dm, java.lang.Runnable
        public void run() {
            super.run();
            Context context = (Context) this.a.get();
            if (context == null) {
                return;
            }
            String str = "android_notification_id = " + this.f0 + " AND opened = 0 AND dismissed = 0";
            ContentValues contentValues = new ContentValues();
            contentValues.put("dismissed", (Integer) 1);
            if (i0.this.a.a("notification", contentValues, str, null) > 0) {
                q.e(context, i0.this.a, this.f0);
            }
            com.onesignal.b.c(i0.this.a, context);
            cn2.i(context).cancel(this.f0);
        }
    }

    /* compiled from: OSNotificationDataController.java */
    /* loaded from: classes2.dex */
    public class c extends dm {
        public final /* synthetic */ String a;
        public final /* synthetic */ d f0;

        public c(String str, d dVar) {
            this.a = str;
            this.f0 = dVar;
        }

        @Override // defpackage.dm, java.lang.Runnable
        public void run() {
            super.run();
            boolean z = true;
            Cursor c = i0.this.a.c("notification", new String[]{"notification_id"}, "notification_id = ?", new String[]{this.a}, null, null, null);
            boolean moveToFirst = c.moveToFirst();
            c.close();
            if (moveToFirst) {
                i0.this.b.debug("Notification notValidOrDuplicated with id duplicated, duplicate FCM message received, skip processing of " + this.a);
            } else {
                z = false;
            }
            this.f0.a(z);
        }
    }

    /* compiled from: OSNotificationDataController.java */
    /* loaded from: classes2.dex */
    public interface d {
        void a(boolean z);
    }

    public i0(a1 a1Var, yj2 yj2Var) {
        this.a = a1Var;
        this.b = yj2Var;
    }

    public final void g() {
        d(new a(), "OS_NOTIFICATIONS_THREAD");
    }

    public void h() {
        g();
    }

    public final void i(String str, d dVar) {
        if (str != null && !"".equals(str)) {
            if (!OSNotificationWorkManager.a(str)) {
                this.b.debug("Notification notValidOrDuplicated with id duplicated");
                dVar.a(true);
                return;
            }
            d(new c(str, dVar), "OS_NOTIFICATIONS_THREAD");
            return;
        }
        dVar.a(false);
    }

    public void j(JSONObject jSONObject, d dVar) {
        String b2 = j0.b(jSONObject);
        if (b2 == null) {
            this.b.debug("Notification notValidOrDuplicated with id null");
            dVar.a(true);
            return;
        }
        i(b2, dVar);
    }

    public void k(int i, WeakReference<Context> weakReference) {
        d(new b(weakReference, i), "OS_NOTIFICATIONS_THREAD");
    }
}
