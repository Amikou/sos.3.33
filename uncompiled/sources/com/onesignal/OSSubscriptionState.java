package com.onesignal;

import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class OSSubscriptionState implements Cloneable {
    public n0<Object, OSSubscriptionState> a = new n0<>("changed", false);
    public String f0;
    public String g0;
    public boolean h0;
    public boolean i0;

    public OSSubscriptionState(boolean z, boolean z2) {
        if (z) {
            String str = b1.a;
            this.i0 = b1.b(str, "ONESIGNAL_SUBSCRIPTION_LAST", true);
            this.f0 = b1.f(str, "ONESIGNAL_PLAYER_ID_LAST", null);
            this.g0 = b1.f(str, "ONESIGNAL_PUSH_TOKEN_LAST", null);
            this.h0 = b1.b(str, "ONESIGNAL_PERMISSION_ACCEPTED_LAST", false);
            return;
        }
        this.i0 = !OneSignalStateSynchronizer.j();
        this.f0 = OneSignal.z0();
        this.g0 = OneSignalStateSynchronizer.e();
        this.h0 = z2;
    }

    public n0<Object, OSSubscriptionState> a() {
        return this.a;
    }

    public boolean b() {
        return this.i0;
    }

    public void changed(q0 q0Var) {
        f(q0Var.a());
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }

    public boolean d() {
        return (this.f0 == null || this.g0 == null || this.i0 || !this.h0) ? false : true;
    }

    public void e() {
        String str = b1.a;
        b1.j(str, "ONESIGNAL_SUBSCRIPTION_LAST", this.i0);
        b1.m(str, "ONESIGNAL_PLAYER_ID_LAST", this.f0);
        b1.m(str, "ONESIGNAL_PUSH_TOKEN_LAST", this.g0);
        b1.j(str, "ONESIGNAL_PERMISSION_ACCEPTED_LAST", this.h0);
    }

    public final void f(boolean z) {
        boolean d = d();
        this.h0 = z;
        if (d != d()) {
            this.a.c(this);
        }
    }

    public void g(String str) {
        if (str == null) {
            return;
        }
        boolean z = !str.equals(this.g0);
        this.g0 = str;
        if (z) {
            this.a.c(this);
        }
    }

    public void h(String str) {
        boolean z = true;
        if (str != null ? str.equals(this.f0) : this.f0 == null) {
            z = false;
        }
        this.f0 = str;
        if (z) {
            this.a.c(this);
        }
    }

    public JSONObject j() {
        JSONObject jSONObject = new JSONObject();
        try {
            String str = this.f0;
            if (str != null) {
                jSONObject.put("userId", str);
            } else {
                jSONObject.put("userId", JSONObject.NULL);
            }
            String str2 = this.g0;
            if (str2 != null) {
                jSONObject.put("pushToken", str2);
            } else {
                jSONObject.put("pushToken", JSONObject.NULL);
            }
            jSONObject.put("isPushDisabled", b());
            jSONObject.put("isSubscribed", d());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return j().toString();
    }
}
