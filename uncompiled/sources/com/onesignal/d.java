package com.onesignal;

import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.onesignal.OneSignal;

/* compiled from: GMSLocationController.java */
/* loaded from: classes2.dex */
public class d extends LocationController {
    public static fh1 j;
    public static C0158d k;

    /* compiled from: GMSLocationController.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        @Override // java.lang.Runnable
        public void run() {
            try {
                Thread.sleep(d.s());
                OneSignal.a(OneSignal.LOG_LEVEL.WARN, "Location permission exists but GoogleApiClient timed out. Maybe related to mismatch google-play aar versions.");
                LocationController.e();
                LocationController.m(LocationController.g);
            } catch (InterruptedException unused) {
            }
        }
    }

    /* compiled from: GMSLocationController.java */
    /* loaded from: classes2.dex */
    public static class b {
        public static Location a(GoogleApiClient googleApiClient) {
            synchronized (LocationController.d) {
                if (googleApiClient.h()) {
                    return LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                }
                return null;
            }
        }

        public static void b(GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationListener locationListener) {
            try {
                synchronized (LocationController.d) {
                    if (Looper.myLooper() == null) {
                        Looper.prepare();
                    }
                    if (googleApiClient.h()) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
                    }
                }
            } catch (Throwable th) {
                OneSignal.b(OneSignal.LOG_LEVEL.WARN, "FusedLocationApi.requestLocationUpdates failed!", th);
            }
        }
    }

    /* compiled from: GMSLocationController.java */
    /* loaded from: classes2.dex */
    public static class c implements GoogleApiClient.b, GoogleApiClient.c {
        public c() {
        }

        @Override // defpackage.u50
        public void onConnected(Bundle bundle) {
            synchronized (LocationController.d) {
                if (d.j != null && d.j.c() != null) {
                    OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                    OneSignal.a(log_level, "GMSLocationController GoogleApiClientListener onConnected lastLocation: " + LocationController.h);
                    if (LocationController.h == null) {
                        LocationController.h = b.a(d.j.c());
                        OneSignal.a(log_level, "GMSLocationController GoogleApiClientListener lastLocation: " + LocationController.h);
                        Location location = LocationController.h;
                        if (location != null) {
                            LocationController.d(location);
                        }
                    }
                    d.k = new C0158d(d.j.c());
                    return;
                }
                OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "GMSLocationController GoogleApiClientListener onConnected googleApiClient not available, returning");
            }
        }

        @Override // defpackage.jm2
        public void onConnectionFailed(ConnectionResult connectionResult) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "GMSLocationController GoogleApiClientListener onConnectionSuspended connectionResult: " + connectionResult);
            d.e();
        }

        @Override // defpackage.u50
        public void onConnectionSuspended(int i) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "GMSLocationController GoogleApiClientListener onConnectionSuspended i: " + i);
            d.e();
        }

        public /* synthetic */ c(a aVar) {
            this();
        }
    }

    /* compiled from: GMSLocationController.java */
    /* renamed from: com.onesignal.d$d  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static class C0158d implements LocationListener {
        public GoogleApiClient a;

        public C0158d(GoogleApiClient googleApiClient) {
            this.a = googleApiClient;
            a();
        }

        public final void a() {
            long j = OneSignal.O0() ? 270000L : 570000L;
            if (this.a != null) {
                LocationRequest priority = LocationRequest.create().setFastestInterval(j).setInterval(j).setMaxWaitTime((long) (j * 1.5d)).setPriority(102);
                OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "GMSLocationController GoogleApiClient requestLocationUpdates!");
                b.b(this.a, priority, this);
            }
        }
    }

    public static void e() {
        synchronized (LocationController.d) {
            fh1 fh1Var = j;
            if (fh1Var != null) {
                fh1Var.b();
            }
            j = null;
        }
    }

    public static void l() {
        synchronized (LocationController.d) {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "GMSLocationController onFocusChange!");
            fh1 fh1Var = j;
            if (fh1Var != null && fh1Var.c().h()) {
                fh1 fh1Var2 = j;
                if (fh1Var2 != null) {
                    GoogleApiClient c2 = fh1Var2.c();
                    if (k != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(c2, k);
                    }
                    k = new C0158d(c2);
                }
            }
        }
    }

    public static void p() {
        t();
    }

    public static int s() {
        return 30000;
    }

    public static void t() {
        Location location;
        if (LocationController.f != null) {
            return;
        }
        synchronized (LocationController.d) {
            u();
            if (j != null && (location = LocationController.h) != null) {
                LocationController.d(location);
            }
            c cVar = new c(null);
            fh1 fh1Var = new fh1(new GoogleApiClient.a(LocationController.g).a(LocationServices.API).b(cVar).c(cVar).f(LocationController.h().a).d());
            j = fh1Var;
            fh1Var.a();
        }
    }

    public static void u() {
        Thread thread = new Thread(new a(), "OS_GMS_LOCATION_FALLBACK");
        LocationController.f = thread;
        thread.start();
    }
}
