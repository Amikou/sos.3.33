package com.onesignal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.DisplayCutout;
import android.view.View;
import android.view.Window;
import android.view.WindowInsets;
import com.github.mikephil.charting.utils.Utils;
import com.onesignal.a;
import java.lang.ref.WeakReference;

/* compiled from: OSViewUtils.java */
/* loaded from: classes2.dex */
public class z0 {
    public static final int a = b(24);

    /* compiled from: OSViewUtils.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ String a;
        public final /* synthetic */ Runnable f0;

        /* compiled from: OSViewUtils.java */
        /* renamed from: com.onesignal.z0$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0161a extends a.b {
            public final /* synthetic */ com.onesignal.a a;

            public C0161a(com.onesignal.a aVar) {
                this.a = aVar;
            }

            @Override // com.onesignal.a.b
            public void a(Activity activity) {
                this.a.q(a.this.a);
                if (z0.k(activity)) {
                    a.this.f0.run();
                } else {
                    z0.a(activity, a.this.f0);
                }
            }
        }

        public a(String str, Runnable runnable) {
            this.a = str;
            this.f0 = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            com.onesignal.a b = k7.b();
            if (b != null) {
                b.b(this.a, new C0161a(b));
            }
        }
    }

    public static void a(Activity activity, Runnable runnable) {
        activity.getWindow().getDecorView().post(new a("decorViewReady:" + runnable, runnable));
    }

    public static int b(int i) {
        return (int) (i * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int[] c(Activity activity) {
        float f;
        DisplayCutout cutout;
        Rect i = i(activity);
        View findViewById = activity.getWindow().findViewById(16908290);
        float top = (i.top - findViewById.getTop()) / Resources.getSystem().getDisplayMetrics().density;
        float bottom = (findViewById.getBottom() - i.bottom) / Resources.getSystem().getDisplayMetrics().density;
        int i2 = Build.VERSION.SDK_INT;
        float f2 = Utils.FLOAT_EPSILON;
        if (i2 != 29 || (cutout = activity.getWindowManager().getDefaultDisplay().getCutout()) == null) {
            f = 0.0f;
        } else {
            f2 = cutout.getSafeInsetRight() / Resources.getSystem().getDisplayMetrics().density;
            f = cutout.getSafeInsetLeft() / Resources.getSystem().getDisplayMetrics().density;
        }
        return new int[]{Math.round(top), Math.round(bottom), Math.round(f2), Math.round(f)};
    }

    public static int d(Activity activity) {
        Point point = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(point);
        return point.y;
    }

    public static int e(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            return activity.getWindow().getDecorView().getWidth();
        }
        return j(activity);
    }

    public static int f(Activity activity) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            return g(activity);
        }
        if (i >= 21) {
            return h(activity);
        }
        return d(activity);
    }

    @TargetApi(23)
    public static int g(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        WindowInsets rootWindowInsets = decorView.getRootWindowInsets();
        if (rootWindowInsets == null) {
            return decorView.getHeight();
        }
        return (decorView.getHeight() - rootWindowInsets.getStableInsetBottom()) - rootWindowInsets.getStableInsetTop();
    }

    public static int h(Activity activity) {
        if (activity.getResources().getConfiguration().orientation == 2) {
            return i(activity).height();
        }
        return d(activity);
    }

    public static Rect i(Activity activity) {
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect;
    }

    public static int j(Activity activity) {
        return i(activity).width();
    }

    public static boolean k(Activity activity) {
        boolean z = activity.getWindow().getDecorView().getApplicationWindowToken() != null;
        if (Build.VERSION.SDK_INT < 23) {
            return z;
        }
        return z && (activity.getWindow().getDecorView().getRootWindowInsets() != null);
    }

    public static boolean l(WeakReference<Activity> weakReference) {
        View view;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Rect rect = new Rect();
        if (weakReference.get() != null) {
            Window window = weakReference.get().getWindow();
            view = window.getDecorView();
            view.getWindowVisibleDisplayFrame(rect);
            window.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        } else {
            view = null;
        }
        return view != null && displayMetrics.heightPixels - rect.bottom > a;
    }
}
