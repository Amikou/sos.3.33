package com.onesignal;

import android.R;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.widget.RemoteViews;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.onesignal.OneSignal;
import defpackage.dh2;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: GenerateNotification.java */
/* loaded from: classes2.dex */
public class e {
    public static Class<?> a = NotificationDismissReceiver.class;
    public static Resources b = null;
    public static Context c = null;
    public static String d = null;
    public static Integer e = null;

    /* compiled from: GenerateNotification.java */
    /* loaded from: classes2.dex */
    public static class b {
        public dh2.e a;
        public boolean b;

        public b() {
        }
    }

    public static int A(JSONObject jSONObject) {
        int z = z(jSONObject.optString("sicon", null));
        return z != 0 ? z : u();
    }

    public static CharSequence B(JSONObject jSONObject) {
        String optString = jSONObject.optString("title", null);
        return optString != null ? optString : c.getPackageManager().getApplicationLabel(c.getApplicationInfo());
    }

    public static void C() {
        if (Build.VERSION.SDK_INT >= 24) {
            e = 2;
        } else {
            e = 1;
        }
    }

    public static void D() {
        if (OSUtils.H()) {
            throw new OSThrowable$OSMainThreadException("Process for showing a notification should never been done on Main Thread!");
        }
    }

    public static boolean E(JSONObject jSONObject) {
        String optString = jSONObject.optString("sound", null);
        return ("null".equals(optString) || "nil".equals(optString)) ? false : true;
    }

    public static void F(dh2.e eVar) {
        eVar.x(true).n(0).B(null).E(null).D(null);
    }

    public static Bitmap G(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        try {
            int dimension = (int) b.getDimension(17104902);
            int dimension2 = (int) b.getDimension(17104901);
            int height = bitmap.getHeight();
            int width = bitmap.getWidth();
            if (width > dimension2 || height > dimension) {
                if (height > width) {
                    dimension2 = (int) (dimension * (width / height));
                } else if (width > height) {
                    dimension = (int) (dimension2 * (height / width));
                }
                return Bitmap.createScaledBitmap(bitmap, dimension2, dimension, true);
            }
            return bitmap;
        } catch (Throwable unused) {
            return bitmap;
        }
    }

    public static Integer H(JSONObject jSONObject, String str) {
        if (jSONObject != null) {
            try {
                if (jSONObject.has(str)) {
                    return Integer.valueOf(new BigInteger(jSONObject.optString(str), 16).intValue());
                }
                return null;
            } catch (Throwable unused) {
                return null;
            }
        }
        return null;
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0063  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void I(org.json.JSONObject r6, defpackage.dh2.e r7) {
        /*
            java.lang.String r0 = "pri"
            r1 = 6
            int r0 = r6.optInt(r0, r1)
            int r0 = e(r0)
            r7.y(r0)
            r1 = 0
            r2 = 1
            if (r0 >= 0) goto L14
            r0 = r2
            goto L15
        L14:
            r0 = r1
        L15:
            if (r0 == 0) goto L18
            return
        L18:
            java.lang.String r0 = "ledc"
            boolean r3 = r6.has(r0)
            r4 = 4
            if (r3 == 0) goto L40
            java.lang.String r3 = "led"
            int r3 = r6.optInt(r3, r2)
            if (r3 != r2) goto L40
            java.math.BigInteger r3 = new java.math.BigInteger     // Catch: java.lang.Throwable -> L40
            java.lang.String r0 = r6.optString(r0)     // Catch: java.lang.Throwable -> L40
            r5 = 16
            r3.<init>(r0, r5)     // Catch: java.lang.Throwable -> L40
            int r0 = r3.intValue()     // Catch: java.lang.Throwable -> L40
            r3 = 2000(0x7d0, float:2.803E-42)
            r5 = 5000(0x1388, float:7.006E-42)
            r7.u(r0, r3, r5)     // Catch: java.lang.Throwable -> L40
            goto L41
        L40:
            r1 = r4
        L41:
            java.lang.String r0 = "vib"
            int r0 = r6.optInt(r0, r2)
            if (r0 != r2) goto L5d
            java.lang.String r0 = "vib_pt"
            boolean r0 = r6.has(r0)
            if (r0 == 0) goto L5b
            long[] r0 = com.onesignal.OSUtils.Q(r6)
            if (r0 == 0) goto L5d
            r7.E(r0)
            goto L5d
        L5b:
            r1 = r1 | 2
        L5d:
            boolean r0 = E(r6)
            if (r0 == 0) goto L78
            android.content.Context r0 = com.onesignal.e.c
            r2 = 0
            java.lang.String r3 = "sound"
            java.lang.String r6 = r6.optString(r3, r2)
            android.net.Uri r6 = com.onesignal.OSUtils.n(r0, r6)
            if (r6 == 0) goto L76
            r7.B(r6)
            goto L78
        L76:
            r1 = r1 | 1
        L78:
            r7.n(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.e.I(org.json.JSONObject, dh2$e):void");
    }

    public static void J(Context context) {
        c = context;
        d = context.getPackageName();
        b = c.getResources();
    }

    public static void K(RemoteViews remoteViews, JSONObject jSONObject, int i, String str, String str2) {
        Integer H = H(jSONObject, str);
        if (H != null) {
            remoteViews.setTextColor(i, H.intValue());
            return;
        }
        int identifier = b.getIdentifier(str2, "color", d);
        if (identifier != 0) {
            remoteViews.setTextColor(i, ed.b(c, identifier));
        }
    }

    public static boolean L(zj2 zj2Var) {
        Notification h;
        int intValue = zj2Var.a().intValue();
        JSONObject e2 = zj2Var.e();
        String optString = e2.optString("grp", null);
        zr1 zr1Var = new zr1(c);
        ArrayList<StatusBarNotification> arrayList = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 24) {
            arrayList = cn2.c(c);
            if (optString == null && arrayList.size() >= 3) {
                optString = cn2.g();
                cn2.b(c, arrayList);
            }
        }
        b p = p(zj2Var);
        dh2.e eVar = p.a;
        b(e2, zr1Var, eVar, intValue, null);
        try {
            a(e2, eVar);
        } catch (Throwable th) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Could not set background notification image!", th);
        }
        d(zj2Var, eVar);
        if (zj2Var.n()) {
            F(eVar);
        }
        m.a(c, optString != null ? 2 : 1);
        if (optString != null) {
            g(eVar, zr1Var, e2, optString, intValue);
            h = j(zj2Var, eVar);
            if (Build.VERSION.SDK_INT >= 24 && optString.equals(cn2.g())) {
                i(zj2Var, zr1Var, arrayList.size() + 1);
            } else {
                l(zj2Var, p);
            }
        } else {
            h = h(eVar, zr1Var, e2, intValue);
        }
        if (optString == null || Build.VERSION.SDK_INT > 17) {
            c(p, h);
            androidx.core.app.c.d(c).f(intValue, h);
        }
        if (Build.VERSION.SDK_INT >= 26) {
            return cn2.a(c, h.getChannelId());
        }
        return true;
    }

    public static void M(zj2 zj2Var) {
        J(zj2Var.d());
        l(zj2Var, null);
    }

    public static void a(JSONObject jSONObject, dh2.e eVar) throws Throwable {
        Bitmap bitmap;
        JSONObject jSONObject2;
        String string;
        int i = Build.VERSION.SDK_INT;
        if (i >= 16 && i < 31) {
            String optString = jSONObject.optString("bg_img", null);
            if (optString != null) {
                jSONObject2 = new JSONObject(optString);
                bitmap = q(jSONObject2.optString("img", null));
            } else {
                bitmap = null;
                jSONObject2 = null;
            }
            if (bitmap == null) {
                bitmap = r("onesignal_bgimage_default_image");
            }
            if (bitmap != null) {
                RemoteViews remoteViews = new RemoteViews(c.getPackageName(), z03.onesignal_bgimage_notif_layout);
                int i2 = d03.os_bgimage_notif_title;
                remoteViews.setTextViewText(i2, B(jSONObject));
                int i3 = d03.os_bgimage_notif_body;
                remoteViews.setTextViewText(i3, jSONObject.optString("alert"));
                K(remoteViews, jSONObject2, i2, "tc", "onesignal_bgimage_notif_title_color");
                K(remoteViews, jSONObject2, i3, "bc", "onesignal_bgimage_notif_body_color");
                if (jSONObject2 != null && jSONObject2.has("img_align")) {
                    string = jSONObject2.getString("img_align");
                } else {
                    int identifier = b.getIdentifier("onesignal_bgimage_notif_image_align", Utf8String.TYPE_NAME, d);
                    string = identifier != 0 ? b.getString(identifier) : null;
                }
                if ("right".equals(string)) {
                    remoteViews.setViewPadding(d03.os_bgimage_notif_bgimage_align_layout, -5000, 0, 0, 0);
                    int i4 = d03.os_bgimage_notif_bgimage_right_aligned;
                    remoteViews.setImageViewBitmap(i4, bitmap);
                    remoteViews.setViewVisibility(i4, 0);
                    remoteViews.setViewVisibility(d03.os_bgimage_notif_bgimage, 2);
                } else {
                    remoteViews.setImageViewBitmap(d03.os_bgimage_notif_bgimage, bitmap);
                }
                eVar.j(remoteViews);
                eVar.C(null);
                return;
            }
            return;
        }
        OneSignal.a(OneSignal.LOG_LEVEL.VERBOSE, "Cannot use background images in notifications for device on version: " + i);
    }

    public static void b(JSONObject jSONObject, zr1 zr1Var, dh2.e eVar, int i, String str) {
        try {
            JSONObject jSONObject2 = new JSONObject(jSONObject.optString("custom"));
            if (jSONObject2.has("a")) {
                JSONObject jSONObject3 = jSONObject2.getJSONObject("a");
                if (jSONObject3.has("actionButtons")) {
                    JSONArray jSONArray = jSONObject3.getJSONArray("actionButtons");
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        JSONObject optJSONObject = jSONArray.optJSONObject(i2);
                        JSONObject jSONObject4 = new JSONObject(jSONObject.toString());
                        Intent b2 = zr1Var.b(i);
                        b2.setAction("" + i2);
                        b2.putExtra("action_button", true);
                        jSONObject4.put("actionId", optJSONObject.optString("id"));
                        b2.putExtra("onesignalData", jSONObject4.toString());
                        if (str != null) {
                            b2.putExtra("summary", str);
                        } else if (jSONObject.has("grp")) {
                            b2.putExtra("grp", jSONObject.optString("grp"));
                        }
                        eVar.a(optJSONObject.has("icon") ? z(optJSONObject.optString("icon")) : 0, optJSONObject.optString(PublicResolver.FUNC_TEXT), zr1Var.a(i, b2));
                    }
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void c(b bVar, Notification notification) {
        if (bVar.b) {
            try {
                Object newInstance = Class.forName("android.app.MiuiNotification").newInstance();
                Field declaredField = newInstance.getClass().getDeclaredField("customizedIcon");
                declaredField.setAccessible(true);
                declaredField.set(newInstance, Boolean.TRUE);
                Field field = notification.getClass().getField("extraNotification");
                field.setAccessible(true);
                field.set(notification, newInstance);
            } catch (Throwable unused) {
            }
        }
    }

    public static void d(zj2 zj2Var, dh2.e eVar) {
        if (zj2Var.l()) {
            try {
                Field declaredField = dh2.e.class.getDeclaredField("S");
                declaredField.setAccessible(true);
                Notification notification = (Notification) declaredField.get(eVar);
                zj2Var.s(Integer.valueOf(notification.flags));
                zj2Var.t(notification.sound);
                eVar.c(zj2Var.f().f());
                Notification notification2 = (Notification) declaredField.get(eVar);
                Field declaredField2 = dh2.e.class.getDeclaredField("f");
                declaredField2.setAccessible(true);
                Field declaredField3 = dh2.e.class.getDeclaredField("e");
                declaredField3.setAccessible(true);
                zj2Var.u((CharSequence) declaredField2.get(eVar));
                zj2Var.x((CharSequence) declaredField3.get(eVar));
                if (zj2Var.n()) {
                    return;
                }
                zj2Var.v(Integer.valueOf(notification2.flags));
                zj2Var.w(notification2.sound);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static int e(int i) {
        if (i > 9) {
            return 2;
        }
        if (i > 7) {
            return 1;
        }
        if (i > 4) {
            return 0;
        }
        return i > 2 ? -1 : -2;
    }

    public static Intent f(int i, zr1 zr1Var, JSONObject jSONObject, String str) {
        return zr1Var.b(i).putExtra("onesignalData", jSONObject.toString()).putExtra("summary", str);
    }

    public static void g(dh2.e eVar, zr1 zr1Var, JSONObject jSONObject, String str, int i) {
        SecureRandom secureRandom = new SecureRandom();
        eVar.k(zr1Var.a(secureRandom.nextInt(), zr1Var.b(i).putExtra("onesignalData", jSONObject.toString()).putExtra("grp", str)));
        eVar.o(y(secureRandom.nextInt(), x(i).putExtra("grp", str)));
        eVar.q(str);
        try {
            eVar.r(e.intValue());
        } catch (Throwable unused) {
        }
    }

    public static Notification h(dh2.e eVar, zr1 zr1Var, JSONObject jSONObject, int i) {
        SecureRandom secureRandom = new SecureRandom();
        eVar.k(zr1Var.a(secureRandom.nextInt(), zr1Var.b(i).putExtra("onesignalData", jSONObject.toString())));
        eVar.o(y(secureRandom.nextInt(), x(i)));
        return eVar.b();
    }

    public static void i(zj2 zj2Var, zr1 zr1Var, int i) {
        JSONObject e2 = zj2Var.e();
        SecureRandom secureRandom = new SecureRandom();
        String g = cn2.g();
        String str = i + " new messages";
        int f = cn2.f();
        k(a1.g(c), g, f);
        PendingIntent a2 = zr1Var.a(secureRandom.nextInt(), f(f, zr1Var, e2, g));
        PendingIntent y = y(secureRandom.nextInt(), x(0).putExtra("summary", g));
        dh2.e eVar = p(zj2Var).a;
        if (zj2Var.i() != null) {
            eVar.B(zj2Var.i());
        }
        if (zj2Var.h() != null) {
            eVar.n(zj2Var.h().intValue());
        }
        eVar.k(a2).o(y).m(c.getPackageManager().getApplicationLabel(c.getApplicationInfo())).l(str).w(i).A(u()).t(t()).x(true).g(false).q(g).s(true);
        try {
            eVar.r(e.intValue());
        } catch (Throwable unused) {
        }
        dh2.g gVar = new dh2.g();
        gVar.i(str);
        eVar.C(gVar);
        androidx.core.app.c.d(c).f(f, eVar.b());
    }

    public static Notification j(zj2 zj2Var, dh2.e eVar) {
        int i = Build.VERSION.SDK_INT;
        boolean z = i > 17 && i < 24 && !zj2Var.n();
        if (z && zj2Var.i() != null && !zj2Var.i().equals(zj2Var.g())) {
            eVar.B(null);
        }
        Notification b2 = eVar.b();
        if (z) {
            eVar.B(zj2Var.i());
        }
        return b2;
    }

    public static void k(a1 a1Var, String str, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("android_notification_id", Integer.valueOf(i));
        contentValues.put("group_id", str);
        contentValues.put("is_summary", (Integer) 1);
        a1Var.j("notification", null, contentValues);
    }

    public static void l(zj2 zj2Var, b bVar) {
        String str;
        Cursor c2;
        JSONObject jSONObject;
        ArrayList<SpannableString> arrayList;
        Integer num;
        Notification b2;
        String str2;
        String str3;
        String str4;
        String str5 = ThrowableDeserializer.PROP_NAME_MESSAGE;
        String str6 = "title";
        String str7 = "is_summary";
        boolean n = zj2Var.n();
        JSONObject e2 = zj2Var.e();
        zr1 zr1Var = new zr1(c);
        Cursor cursor = null;
        String optString = e2.optString("grp", null);
        SecureRandom secureRandom = new SecureRandom();
        PendingIntent y = y(secureRandom.nextInt(), x(0).putExtra("summary", optString));
        a1 g = a1.g(c);
        try {
            String[] strArr = {"android_notification_id", "full_data", "is_summary", "title", ThrowableDeserializer.PROP_NAME_MESSAGE};
            try {
                String[] strArr2 = {optString};
                if (n) {
                    str = "group_id = ? AND dismissed = 0 AND opened = 0";
                } else {
                    try {
                        str = "group_id = ? AND dismissed = 0 AND opened = 0 AND android_notification_id <> " + zj2Var.a();
                    } catch (Throwable th) {
                        th = th;
                        cursor = null;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                c2 = g.c("notification", strArr, str, strArr2, null, null, "_id DESC");
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
            }
            try {
                if (c2.moveToFirst()) {
                    arrayList = new ArrayList();
                    String str8 = null;
                    num = null;
                    while (true) {
                        String str9 = str7;
                        if (c2.getInt(c2.getColumnIndex(str7)) == 1) {
                            num = Integer.valueOf(c2.getInt(c2.getColumnIndex("android_notification_id")));
                            str2 = str5;
                            str3 = str6;
                        } else {
                            String string = c2.getString(c2.getColumnIndex(str6));
                            String str10 = string == null ? "" : string + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
                            str2 = str5;
                            str3 = str6;
                            SpannableString spannableString = new SpannableString(str10 + c2.getString(c2.getColumnIndex(str5)));
                            if (str10.length() > 0) {
                                spannableString.setSpan(new StyleSpan(1), 0, str10.length(), 0);
                            }
                            arrayList.add(spannableString);
                            if (str8 == null) {
                                str8 = c2.getString(c2.getColumnIndex("full_data"));
                            }
                        }
                        str4 = str8;
                        if (!c2.moveToNext()) {
                            break;
                        }
                        str8 = str4;
                        str7 = str9;
                        str5 = str2;
                        str6 = str3;
                    }
                    if (n != 0 && str4 != null) {
                        try {
                            jSONObject = new JSONObject(str4);
                        } catch (JSONException e3) {
                            e3.printStackTrace();
                        }
                    }
                    jSONObject = e2;
                } else {
                    jSONObject = e2;
                    arrayList = null;
                    num = null;
                }
                if (!c2.isClosed()) {
                    c2.close();
                }
                if (num == null) {
                    num = Integer.valueOf(secureRandom.nextInt());
                    k(g, optString, num.intValue());
                }
                PendingIntent a2 = zr1Var.a(secureRandom.nextInt(), f(num.intValue(), zr1Var, jSONObject, optString));
                if (arrayList != null && ((n && arrayList.size() > 1) || (!n && arrayList.size() > 0))) {
                    int size = arrayList.size() + (!n);
                    String optString2 = jSONObject.optString("grp_msg", null);
                    CharSequence replace = optString2 == null ? size + " new messages" : optString2.replace("$[notif_count]", "" + size);
                    dh2.e eVar = p(zj2Var).a;
                    if (n != 0) {
                        F(eVar);
                    } else {
                        if (zj2Var.i() != null) {
                            eVar.B(zj2Var.i());
                        }
                        if (zj2Var.h() != null) {
                            eVar.n(zj2Var.h().intValue());
                        }
                    }
                    eVar.k(a2).o(y).m(c.getPackageManager().getApplicationLabel(c.getApplicationInfo())).l(replace).w(size).A(u()).t(t()).x(n).g(false).q(optString).s(true);
                    try {
                        eVar.r(e.intValue());
                    } catch (Throwable unused) {
                    }
                    if (n == 0) {
                        eVar.D(replace);
                    }
                    dh2.g gVar = new dh2.g();
                    if (n == 0) {
                        String charSequence = zj2Var.k() != null ? zj2Var.k().toString() : null;
                        String str11 = charSequence != null ? charSequence + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR : "";
                        SpannableString spannableString2 = new SpannableString(str11 + zj2Var.c().toString());
                        if (str11.length() > 0) {
                            spannableString2.setSpan(new StyleSpan(1), 0, str11.length(), 0);
                        }
                        gVar.h(spannableString2);
                    }
                    for (SpannableString spannableString3 : arrayList) {
                        gVar.h(spannableString3);
                    }
                    gVar.i(replace);
                    eVar.C(gVar);
                    b2 = eVar.b();
                } else {
                    dh2.e eVar2 = bVar.a;
                    eVar2.b.clear();
                    b(jSONObject, zr1Var, eVar2, num.intValue(), optString);
                    eVar2.k(a2).o(y).x(n).g(false).q(optString).s(true);
                    try {
                        eVar2.r(e.intValue());
                    } catch (Throwable unused2) {
                    }
                    b2 = eVar2.b();
                    c(bVar, b2);
                }
                androidx.core.app.c.d(c).f(num.intValue(), b2);
            } catch (Throwable th3) {
                th = th3;
                cursor = c2;
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
        }
    }

    public static boolean m(zj2 zj2Var) {
        J(zj2Var.d());
        return L(zj2Var);
    }

    public static boolean n(zj2 zj2Var) {
        J(zj2Var.d());
        D();
        C();
        return L(zj2Var);
    }

    public static BigInteger o(JSONObject jSONObject) {
        try {
            if (jSONObject.has("bgac")) {
                return new BigInteger(jSONObject.optString("bgac", null), 16);
            }
        } catch (Throwable unused) {
        }
        try {
            String k = OSUtils.k(OneSignal.e, "onesignal_notification_accent_color", null);
            if (k != null) {
                return new BigInteger(k, 16);
            }
        } catch (Throwable unused2) {
        }
        try {
            String f = OSUtils.f(OneSignal.e, "com.onesignal.NotificationAccentColor.DEFAULT");
            if (f != null) {
                return new BigInteger(f, 16);
            }
        } catch (Throwable unused3) {
        }
        return null;
    }

    public static b p(zj2 zj2Var) {
        dh2.e eVar;
        JSONObject e2 = zj2Var.e();
        b bVar = new b();
        try {
            eVar = new dh2.e(c, l.c(zj2Var));
        } catch (Throwable unused) {
            eVar = new dh2.e(c);
        }
        String optString = e2.optString("alert", null);
        eVar.g(true).A(A(e2)).C(new dh2.c().h(optString)).l(optString).D(optString);
        if (Build.VERSION.SDK_INT < 24 || !e2.optString("title").equals("")) {
            eVar.m(B(e2));
        }
        try {
            BigInteger o = o(e2);
            if (o != null) {
                eVar.i(o.intValue());
            }
        } catch (Throwable unused2) {
        }
        try {
            eVar.F(e2.has("vis") ? Integer.parseInt(e2.optString("vis")) : 1);
        } catch (Throwable unused3) {
        }
        Bitmap w = w(e2);
        if (w != null) {
            bVar.b = true;
            eVar.t(w);
        }
        Bitmap q = q(e2.optString("bicon", null));
        if (q != null) {
            eVar.C(new dh2.b().i(q).j(optString));
        }
        if (zj2Var.j() != null) {
            try {
                eVar.G(zj2Var.j().longValue() * 1000);
            } catch (Throwable unused4) {
            }
        }
        I(e2, eVar);
        bVar.a = eVar;
        return bVar;
    }

    public static Bitmap q(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (!trim.startsWith("http://") && !trim.startsWith("https://")) {
            return r(str);
        }
        return s(trim);
    }

    public static Bitmap r(String str) {
        Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeStream(c.getAssets().open(str));
        } catch (Throwable unused) {
            bitmap = null;
        }
        if (bitmap != null) {
            return bitmap;
        }
        try {
            for (String str2 : Arrays.asList(".png", ".webp", ".jpg", ".gif", ".bmp")) {
                try {
                    bitmap = BitmapFactory.decodeStream(c.getAssets().open(str + str2));
                    continue;
                } catch (Throwable unused2) {
                }
                if (bitmap != null) {
                    return bitmap;
                }
            }
            int z = z(str);
            if (z != 0) {
                return BitmapFactory.decodeResource(b, z);
            }
        } catch (Throwable unused3) {
        }
        return null;
    }

    public static Bitmap s(String str) {
        try {
            return BitmapFactory.decodeStream(new URL(str).openConnection().getInputStream());
        } catch (Throwable th) {
            OneSignal.b(OneSignal.LOG_LEVEL.WARN, "Could not download image!", th);
            return null;
        }
    }

    public static Bitmap t() {
        return G(r("ic_onesignal_large_icon_default"));
    }

    public static int u() {
        int v = v("ic_stat_onesignal_default");
        if (v != 0) {
            return v;
        }
        int v2 = v("corona_statusbar_icon_default");
        if (v2 != 0) {
            return v2;
        }
        int v3 = v("ic_os_notification_fallback_white_24dp");
        if (v3 != 0) {
            return v3;
        }
        return 17301598;
    }

    public static int v(String str) {
        return b.getIdentifier(str, "drawable", d);
    }

    public static Bitmap w(JSONObject jSONObject) {
        Bitmap q = q(jSONObject.optString("licon"));
        if (q == null) {
            q = r("ic_onesignal_large_icon_default");
        }
        if (q == null) {
            return null;
        }
        return G(q);
    }

    public static Intent x(int i) {
        return new Intent(c, a).putExtra("androidNotificationId", i).putExtra("dismissed", true);
    }

    public static PendingIntent y(int i, Intent intent) {
        return PendingIntent.getBroadcast(c, i, intent, 201326592);
    }

    public static int z(String str) {
        if (str == null) {
            return 0;
        }
        String trim = str.trim();
        if (OSUtils.J(trim)) {
            int v = v(trim);
            if (v != 0) {
                return v;
            }
            try {
                return R.drawable.class.getField(str).getInt(null);
            } catch (Throwable unused) {
                return 0;
            }
        }
        return 0;
    }
}
