package com.onesignal;

import com.onesignal.d1;
import org.json.JSONObject;

/* compiled from: OneSignalRestClientWrapper.java */
/* loaded from: classes2.dex */
public class e1 implements wm2 {

    /* compiled from: OneSignalRestClientWrapper.java */
    /* loaded from: classes2.dex */
    public class a extends d1.g {
        public final /* synthetic */ ym2 a;

        public a(e1 e1Var, ym2 ym2Var) {
            this.a = ym2Var;
        }

        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            this.a.b(i, str, th);
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            this.a.a(str);
        }
    }

    @Override // defpackage.wm2
    public void a(String str, JSONObject jSONObject, ym2 ym2Var) {
        d1.j(str, jSONObject, new a(this, ym2Var));
    }
}
