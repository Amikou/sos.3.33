package com.onesignal;

import com.onesignal.c1;

/* compiled from: OSRemoteParamController.java */
/* loaded from: classes2.dex */
public class s0 {
    public c1.f a = null;

    public void a() {
        this.a = null;
    }

    public boolean b() {
        return b1.b(b1.a, "OS_CLEAR_GROUP_SUMMARY_CLICK", true);
    }

    public boolean c() {
        return b1.b(b1.a, "GT_FIREBASE_TRACKING_ENABLED", false);
    }

    public c1.f d() {
        return this.a;
    }

    public boolean e() {
        return b1.b(b1.a, "ONESIGNAL_USER_PROVIDED_CONSENT", false);
    }

    public boolean f() {
        c1.f fVar = this.a;
        return (fVar == null || fVar.j == null) ? false : true;
    }

    public boolean g() {
        return b1.b(b1.a, "PREFS_OS_DISABLE_GMS_MISSING_PROMPT", false);
    }

    public boolean h() {
        return b1.b(b1.a, "PREFS_OS_LOCATION_SHARED", true);
    }

    public boolean i() {
        return b1.b(b1.a, "PREFS_OS_REQUIRES_USER_PRIVACY_CONSENT", false);
    }

    public boolean j() {
        return b1.b(b1.a, "PREFS_OS_RECEIVE_RECEIPTS_ENABLED", false);
    }

    public boolean k() {
        return this.a != null;
    }

    public boolean l() {
        return b1.b(b1.a, "OS_RESTORE_TTL_FILTER", true);
    }

    public void m(boolean z) {
        b1.j(b1.a, "PREFS_OS_DISABLE_GMS_MISSING_PROMPT", z);
    }

    public void n(boolean z) {
        b1.j(b1.a, "PREFS_OS_LOCATION_SHARED", z);
    }

    public void o(boolean z) {
        b1.j(b1.a, "PREFS_OS_REQUIRES_USER_PRIVACY_CONSENT", z);
    }

    public final void p(boolean z) {
        b1.j(b1.a, "PREFS_OS_RECEIVE_RECEIPTS_ENABLED", z);
    }

    public void q(c1.f fVar, cl2 cl2Var, vk2 vk2Var, yj2 yj2Var) {
        this.a = fVar;
        String str = b1.a;
        b1.j(str, "GT_FIREBASE_TRACKING_ENABLED", fVar.c);
        r(fVar.d);
        b1.j(str, "OS_CLEAR_GROUP_SUMMARY_CLICK", fVar.e);
        b1.j(str, vk2Var.h(), fVar.k.h);
        p(fVar.f);
        yj2Var.debug("OneSignal saveInfluenceParams: " + fVar.k.toString());
        cl2Var.j(fVar.k);
        Boolean bool = fVar.g;
        if (bool != null) {
            m(bool.booleanValue());
        }
        Boolean bool2 = fVar.h;
        if (bool2 != null) {
            s(bool2.booleanValue());
        }
        Boolean bool3 = fVar.i;
        if (bool3 != null) {
            OneSignal.N1(bool3.booleanValue());
        }
        Boolean bool4 = fVar.j;
        if (bool4 != null) {
            o(bool4.booleanValue());
        }
    }

    public final void r(boolean z) {
        b1.j(b1.a, "OS_RESTORE_TTL_FILTER", this.a.d);
    }

    public void s(boolean z) {
        b1.j(b1.a, "PREFS_OS_UNSUBSCRIBE_WHEN_NOTIFICATIONS_DISABLED", z);
    }

    public boolean t() {
        return b1.b(b1.a, "PREFS_OS_UNSUBSCRIBE_WHEN_NOTIFICATIONS_DISABLED", true);
    }
}
