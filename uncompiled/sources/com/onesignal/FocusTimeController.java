package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.d1;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class FocusTimeController {
    public Long a;
    public v b;
    public yj2 c;

    /* loaded from: classes2.dex */
    public enum FocusEventType {
        BACKGROUND,
        END_SESSION
    }

    /* loaded from: classes2.dex */
    public static class a extends b {
        public a() {
            this.a = 1L;
            this.b = "OS_UNSENT_ATTRIBUTED_ACTIVE_TIME";
        }

        @Override // com.onesignal.FocusTimeController.b
        public void h(JSONObject jSONObject) {
            OneSignal.s0().b(jSONObject, j());
        }

        @Override // com.onesignal.FocusTimeController.b
        public List<com.onesignal.influence.domain.a> j() {
            ArrayList arrayList = new ArrayList();
            for (String str : b1.g(b1.a, "PREFS_OS_ATTRIBUTED_INFLUENCES", new HashSet())) {
                try {
                    arrayList.add(new com.onesignal.influence.domain.a(str));
                } catch (JSONException e) {
                    OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                    OneSignal.a(log_level, a.class.getSimpleName() + ": error generation OSInfluence from json object: " + e);
                }
            }
            return arrayList;
        }

        @Override // com.onesignal.FocusTimeController.b
        public void m(List<com.onesignal.influence.domain.a> list) {
            HashSet hashSet = new HashSet();
            for (com.onesignal.influence.domain.a aVar : list) {
                try {
                    hashSet.add(aVar.g());
                } catch (JSONException e) {
                    OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                    OneSignal.a(log_level, a.class.getSimpleName() + ": error generation json object OSInfluence: " + e);
                }
            }
            b1.n(b1.a, "PREFS_OS_ATTRIBUTED_INFLUENCES", hashSet);
        }

        @Override // com.onesignal.FocusTimeController.b
        public void r(FocusEventType focusEventType) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.d1(log_level, a.class.getSimpleName() + " sendTime with: " + focusEventType);
            if (focusEventType.equals(FocusEventType.END_SESSION)) {
                u();
            } else {
                u0.q().s(OneSignal.e);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class b {
        public long a;
        public String b;
        public Long c = null;
        public final AtomicBoolean d = new AtomicBoolean();

        /* loaded from: classes2.dex */
        public class a extends d1.g {
            public a() {
            }

            @Override // com.onesignal.d1.g
            public void a(int i, String str, Throwable th) {
                OneSignal.U0("sending on_focus Failed", i, th, str);
            }

            @Override // com.onesignal.d1.g
            public void b(String str) {
                b.this.o(0L);
            }
        }

        public final void g(long j, List<com.onesignal.influence.domain.a> list, FocusEventType focusEventType) {
            n(j, list);
            t(focusEventType);
        }

        public void h(JSONObject jSONObject) {
        }

        public final JSONObject i(long j) throws JSONException {
            JSONObject put = new JSONObject().put("app_id", OneSignal.o0()).put("type", 1).put("state", "ping").put("active_time", j).put("device_type", new OSUtils().e());
            OneSignal.y(put);
            return put;
        }

        public abstract List<com.onesignal.influence.domain.a> j();

        public final long k() {
            if (this.c == null) {
                this.c = Long.valueOf(b1.d(b1.a, this.b, 0L));
            }
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, getClass().getSimpleName() + ":getUnsentActiveTime: " + this.c);
            return this.c.longValue();
        }

        public final boolean l() {
            return k() >= this.a;
        }

        public abstract void m(List<com.onesignal.influence.domain.a> list);

        public final void n(long j, List<com.onesignal.influence.domain.a> list) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, getClass().getSimpleName() + ":saveUnsentActiveData with lastFocusTimeInfluences: " + list.toString());
            m(list);
            o(k() + j);
        }

        public final void o(long j) {
            this.c = Long.valueOf(j);
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, getClass().getSimpleName() + ":saveUnsentActiveTime: " + this.c);
            b1.l(b1.a, this.b, j);
        }

        public final void p(long j) {
            try {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.a(log_level, getClass().getSimpleName() + ":sendOnFocus with totalTimeActive: " + j);
                JSONObject i = i(j);
                h(i);
                q(OneSignal.z0(), i);
                if (OneSignal.H0()) {
                    q(OneSignal.Y(), i(j));
                }
                if (OneSignal.I0()) {
                    q(OneSignal.m0(), i(j));
                }
                m(new ArrayList());
            } catch (JSONException e) {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Generating on_focus:JSON Failed.", e);
            }
        }

        public final void q(String str, JSONObject jSONObject) {
            a aVar = new a();
            d1.k("players/" + str + "/on_focus", jSONObject, aVar);
        }

        public abstract void r(FocusEventType focusEventType);

        public final void s() {
            List<com.onesignal.influence.domain.a> j = j();
            long k = k();
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, getClass().getSimpleName() + ":sendUnsentTimeNow with time: " + k + " and influences: " + j.toString());
            t(FocusEventType.BACKGROUND);
        }

        public final void t(FocusEventType focusEventType) {
            if (!OneSignal.J0()) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.WARN;
                OneSignal.a(log_level, getClass().getSimpleName() + ":sendUnsentTimeNow not possible due to user id null");
                return;
            }
            r(focusEventType);
        }

        public void u() {
            if (this.d.get()) {
                return;
            }
            synchronized (this.d) {
                this.d.set(true);
                if (l()) {
                    p(k());
                }
                this.d.set(false);
            }
        }

        public final void v() {
            if (l()) {
                u();
            }
        }

        public void w() {
            if (l()) {
                u0.q().s(OneSignal.e);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static class c extends b {
        public c() {
            this.a = 60L;
            this.b = "GT_UNSENT_ACTIVE_TIME";
        }

        @Override // com.onesignal.FocusTimeController.b
        public List<com.onesignal.influence.domain.a> j() {
            return new ArrayList();
        }

        @Override // com.onesignal.FocusTimeController.b
        public void m(List<com.onesignal.influence.domain.a> list) {
        }

        @Override // com.onesignal.FocusTimeController.b
        public void r(FocusEventType focusEventType) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.d1(log_level, c.class.getSimpleName() + " sendTime with: " + focusEventType);
            if (focusEventType.equals(FocusEventType.END_SESSION)) {
                return;
            }
            w();
        }
    }

    public FocusTimeController(v vVar, yj2 yj2Var) {
        this.b = vVar;
        this.c = yj2Var;
    }

    public void a() {
        yj2 yj2Var = this.c;
        yj2Var.debug("Application backgrounded focus time: " + this.a);
        this.b.b().s();
        this.a = null;
    }

    public void b() {
        this.a = Long.valueOf(OneSignal.w0().b());
        yj2 yj2Var = this.c;
        yj2Var.debug("Application foregrounded focus time: " + this.a);
    }

    public void c() {
        Long e = e();
        yj2 yj2Var = this.c;
        yj2Var.debug("Application stopped focus time: " + this.a + " timeElapsed: " + e);
        if (e == null) {
            return;
        }
        List<com.onesignal.influence.domain.a> f = OneSignal.s0().f();
        this.b.c(f).n(e.longValue(), f);
    }

    public void d() {
        if (OneSignal.O0()) {
            return;
        }
        this.b.b().v();
    }

    public final Long e() {
        if (this.a == null) {
            return null;
        }
        long b2 = (long) (((OneSignal.w0().b() - this.a.longValue()) / 1000.0d) + 0.5d);
        if (b2 < 1 || b2 > 86400) {
            return null;
        }
        return Long.valueOf(b2);
    }

    public final boolean f(List<com.onesignal.influence.domain.a> list, FocusEventType focusEventType) {
        Long e = e();
        if (e == null) {
            return false;
        }
        this.b.c(list).g(e.longValue(), list, focusEventType);
        return true;
    }

    public void g(List<com.onesignal.influence.domain.a> list) {
        FocusEventType focusEventType = FocusEventType.END_SESSION;
        if (f(list, focusEventType)) {
            return;
        }
        this.b.c(list).t(focusEventType);
    }
}
