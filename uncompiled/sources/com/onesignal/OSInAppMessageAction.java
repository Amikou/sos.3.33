package com.onesignal;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public class OSInAppMessageAction {
    public String a;
    public OSInAppMessageActionUrlType b;
    public String c;
    public List<z> d = new ArrayList();
    public List<a0> e = new ArrayList();
    public e0 f;
    public boolean g;

    /* loaded from: classes2.dex */
    public enum OSInAppMessageActionUrlType {
        IN_APP_WEBVIEW("webview"),
        BROWSER("browser"),
        REPLACE_CONTENT("replacement");
        
        private String text;

        OSInAppMessageActionUrlType(String str) {
            this.text = str;
        }

        public static OSInAppMessageActionUrlType fromString(String str) {
            OSInAppMessageActionUrlType[] values;
            for (OSInAppMessageActionUrlType oSInAppMessageActionUrlType : values()) {
                if (oSInAppMessageActionUrlType.text.equalsIgnoreCase(str)) {
                    return oSInAppMessageActionUrlType;
                }
            }
            return null;
        }

        public JSONObject toJSONObject() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("url_type", this.text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jSONObject;
        }

        @Override // java.lang.Enum
        public String toString() {
            return this.text;
        }
    }

    public OSInAppMessageAction(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.optString("id", null);
        jSONObject.optString(PublicResolver.FUNC_NAME, null);
        this.c = jSONObject.optString("url", null);
        jSONObject.optString("pageId", null);
        OSInAppMessageActionUrlType fromString = OSInAppMessageActionUrlType.fromString(jSONObject.optString("url_target", null));
        this.b = fromString;
        if (fromString == null) {
            this.b = OSInAppMessageActionUrlType.IN_APP_WEBVIEW;
        }
        jSONObject.optBoolean("close", true);
        if (jSONObject.has("outcomes")) {
            h(jSONObject);
        }
        if (jSONObject.has("tags")) {
            this.f = new e0(jSONObject.getJSONObject("tags"));
        }
        if (jSONObject.has("prompts")) {
            i(jSONObject);
        }
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.c;
    }

    public List<z> c() {
        return this.d;
    }

    public List<a0> d() {
        return this.e;
    }

    public e0 e() {
        return this.f;
    }

    public OSInAppMessageActionUrlType f() {
        return this.b;
    }

    public boolean g() {
        return this.g;
    }

    public final void h(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = jSONObject.getJSONArray("outcomes");
        for (int i = 0; i < jSONArray.length(); i++) {
            this.d.add(new z((JSONObject) jSONArray.get(i)));
        }
    }

    public final void i(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = jSONObject.getJSONArray("prompts");
        for (int i = 0; i < jSONArray.length(); i++) {
            String string = jSONArray.getString(i);
            string.hashCode();
            if (string.equals("push")) {
                this.e.add(new b0());
            } else if (string.equals("location")) {
                this.e.add(new y());
            }
        }
    }

    public void j(boolean z) {
        this.g = z;
    }
}
