package com.onesignal;

import android.content.Context;
import android.content.res.Resources;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import defpackage.ji4;

/* compiled from: DraggableRelativeLayout.java */
/* loaded from: classes2.dex */
public class c extends RelativeLayout {
    public static final int i0 = z0.b(28);
    public static final int j0 = z0.b(64);
    public b a;
    public ji4 f0;
    public boolean g0;
    public C0156c h0;

    /* compiled from: DraggableRelativeLayout.java */
    /* loaded from: classes2.dex */
    public class a extends ji4.c {
        public int a;

        public a() {
        }

        @Override // defpackage.ji4.c
        public int a(View view, int i, int i2) {
            return c.this.h0.d;
        }

        @Override // defpackage.ji4.c
        public int b(View view, int i, int i2) {
            if (c.this.h0.g) {
                return c.this.h0.b;
            }
            this.a = i;
            if (c.this.h0.f == 1) {
                if (i >= c.this.h0.c && c.this.a != null) {
                    c.this.a.a();
                }
                if (i < c.this.h0.b) {
                    return c.this.h0.b;
                }
            } else {
                if (i <= c.this.h0.c && c.this.a != null) {
                    c.this.a.a();
                }
                if (i > c.this.h0.b) {
                    return c.this.h0.b;
                }
            }
            return i;
        }

        @Override // defpackage.ji4.c
        public void l(View view, float f, float f2) {
            int i = c.this.h0.b;
            if (!c.this.g0) {
                if (c.this.h0.f == 1) {
                    if (this.a > c.this.h0.j || f2 > c.this.h0.h) {
                        i = c.this.h0.i;
                        c.this.g0 = true;
                        if (c.this.a != null) {
                            c.this.a.onDismiss();
                        }
                    }
                } else if (this.a < c.this.h0.j || f2 < c.this.h0.h) {
                    i = c.this.h0.i;
                    c.this.g0 = true;
                    if (c.this.a != null) {
                        c.this.a.onDismiss();
                    }
                }
            }
            if (c.this.f0.P(c.this.h0.d, i)) {
                ei4.j0(c.this);
            }
        }

        @Override // defpackage.ji4.c
        public boolean m(View view, int i) {
            return true;
        }
    }

    /* compiled from: DraggableRelativeLayout.java */
    /* loaded from: classes2.dex */
    public interface b {
        void a();

        void b();

        void onDismiss();
    }

    /* compiled from: DraggableRelativeLayout.java */
    /* renamed from: com.onesignal.c$c  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static class C0156c {
        public int a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public boolean g;
        public int h;
        public int i;
        public int j;
    }

    public c(Context context) {
        super(context);
        setClipChildren(false);
        f();
    }

    @Override // android.view.View
    public void computeScroll() {
        super.computeScroll();
        if (this.f0.n(true)) {
            ei4.j0(this);
        }
    }

    public final void f() {
        this.f0 = ji4.o(this, 1.0f, new a());
    }

    public void g() {
        this.g0 = true;
        this.f0.R(this, getLeft(), this.h0.i);
        ei4.j0(this);
    }

    public void h(b bVar) {
        this.a = bVar;
    }

    public void i(C0156c c0156c) {
        this.h0 = c0156c;
        c0156c.i = c0156c.e + c0156c.a + ((Resources.getSystem().getDisplayMetrics().heightPixels - c0156c.e) - c0156c.a) + j0;
        c0156c.h = z0.b(3000);
        if (c0156c.f == 0) {
            c0156c.i = (-c0156c.e) - i0;
            c0156c.h = -c0156c.h;
            c0156c.j = c0156c.i / 3;
            return;
        }
        c0156c.j = (c0156c.e / 3) + (c0156c.b * 2);
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        b bVar;
        if (this.g0) {
            return true;
        }
        int action = motionEvent.getAction();
        if ((action == 0 || action == 5) && (bVar = this.a) != null) {
            bVar.b();
        }
        this.f0.G(motionEvent);
        return false;
    }
}
