package com.onesignal;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: OSObservable.java */
/* loaded from: classes2.dex */
public class n0<ObserverType, StateType> {
    public String a;
    public List<Object> b = new ArrayList();
    public boolean c;

    /* compiled from: OSObservable.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ Method a;
        public final /* synthetic */ Object f0;
        public final /* synthetic */ Object g0;

        public a(n0 n0Var, Method method, Object obj, Object obj2) {
            this.a = method;
            this.f0 = obj;
            this.g0 = obj2;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                this.a.invoke(this.f0, this.g0);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e2) {
                e2.printStackTrace();
            }
        }
    }

    public n0(String str, boolean z) {
        this.a = str;
        this.c = z;
    }

    public void a(ObserverType observertype) {
        this.b.add(new WeakReference(observertype));
    }

    public void b(ObserverType observertype) {
        this.b.add(observertype);
    }

    public boolean c(StateType statetype) {
        Iterator<Object> it = this.b.iterator();
        boolean z = false;
        while (it.hasNext()) {
            Object next = it.next();
            if (next instanceof WeakReference) {
                next = ((WeakReference) next).get();
            }
            if (next != null) {
                try {
                    Method declaredMethod = next.getClass().getDeclaredMethod(this.a, statetype.getClass());
                    declaredMethod.setAccessible(true);
                    if (this.c) {
                        OSUtils.S(new a(this, declaredMethod, next, statetype));
                    } else {
                        try {
                            declaredMethod.invoke(next, statetype);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e2) {
                            e2.printStackTrace();
                        }
                    }
                    z = true;
                } catch (NoSuchMethodException e3) {
                    e3.printStackTrace();
                }
            }
        }
        return z;
    }
}
