package com.onesignal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.onesignal.OneSignal;
import java.util.ArrayList;

/* compiled from: OneSignalDbHelper.java */
/* loaded from: classes2.dex */
public class a1 extends SQLiteOpenHelper implements bn2 {
    public static a1 h0;
    public static final Object a = new Object();
    public static final String[] f0 = {"CREATE INDEX notification_notification_id_idx ON notification(notification_id); ", "CREATE INDEX notification_android_notification_id_idx ON notification(android_notification_id); ", "CREATE INDEX notification_group_id_idx ON notification(group_id); ", "CREATE INDEX notification_collapse_id_idx ON notification(collapse_id); ", "CREATE INDEX notification_created_time_idx ON notification(created_time); ", "CREATE INDEX notification_expire_time_idx ON notification(expire_time); "};
    public static yj2 g0 = new f0();
    public static rk2 i0 = new rk2();

    public a1(Context context) {
        super(context, "OneSignal.db", (SQLiteDatabase.CursorFactory) null, f());
    }

    public static int f() {
        return 8;
    }

    public static a1 g(Context context) {
        if (h0 == null) {
            synchronized (a) {
                if (h0 == null) {
                    h0 = new a1(context.getApplicationContext());
                }
            }
        }
        return h0;
    }

    public static StringBuilder m() {
        long a2 = OneSignal.w0().a() / 1000;
        StringBuilder sb = new StringBuilder("created_time > " + (a2 - 604800) + " AND dismissed = 0 AND opened = 0 AND is_summary = 0");
        if (OneSignal.k0().l()) {
            sb.append(" AND expire_time > " + a2);
        }
        return sb;
    }

    public static void n(SQLiteDatabase sQLiteDatabase, String str) {
        try {
            sQLiteDatabase.execSQL(str);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public static void q(SQLiteDatabase sQLiteDatabase) {
        i0.b(sQLiteDatabase);
    }

    public static void r(SQLiteDatabase sQLiteDatabase) {
        n(sQLiteDatabase, "ALTER TABLE notification ADD COLUMN collapse_id TEXT;");
        n(sQLiteDatabase, "CREATE INDEX notification_group_id_idx ON notification(group_id); ");
    }

    public static void u(SQLiteDatabase sQLiteDatabase) {
        n(sQLiteDatabase, "ALTER TABLE notification ADD COLUMN expire_time TIMESTAMP;");
        n(sQLiteDatabase, "UPDATE notification SET expire_time = created_time + 259200;");
        n(sQLiteDatabase, "CREATE INDEX notification_expire_time_idx ON notification(expire_time); ");
    }

    public static void v(SQLiteDatabase sQLiteDatabase) {
        n(sQLiteDatabase, "CREATE TABLE outcome (_id INTEGER PRIMARY KEY,notification_ids TEXT,name TEXT,session TEXT,params TEXT,timestamp TIMESTAMP);");
    }

    public static void w(SQLiteDatabase sQLiteDatabase) {
        n(sQLiteDatabase, "CREATE TABLE cached_unique_outcome_notification (_id INTEGER PRIMARY KEY,notification_id TEXT,name TEXT);");
        q(sQLiteDatabase);
    }

    public static void x(SQLiteDatabase sQLiteDatabase) {
        n(sQLiteDatabase, "CREATE TABLE in_app_message (_id INTEGER PRIMARY KEY,display_quantity INTEGER,last_display INTEGER,message_id TEXT,displayed_in_session INTEGER,click_ids TEXT);");
    }

    @Override // defpackage.bn2
    public int a(String str, ContentValues contentValues, String str2, String[] strArr) {
        yj2 yj2Var;
        String str3;
        yj2 yj2Var2;
        String str4;
        int i = 0;
        if (contentValues == null || contentValues.toString().isEmpty()) {
            return 0;
        }
        synchronized (a) {
            SQLiteDatabase i2 = i();
            try {
                i2.beginTransaction();
                i = i2.update(str, contentValues, str2, strArr);
                i2.setTransactionSuccessful();
            } catch (SQLiteException e) {
                yj2 yj2Var3 = g0;
                yj2Var3.error("Error updating on table: " + str + " with whereClause: " + str2 + " and whereArgs: " + strArr, e);
                if (i2 != null) {
                    try {
                        try {
                            i2.endTransaction();
                        } catch (SQLiteException e2) {
                            e = e2;
                            yj2Var2 = g0;
                            str4 = "Error closing transaction! ";
                            yj2Var2.error(str4, e);
                            return i;
                        }
                    } catch (IllegalStateException e3) {
                        e = e3;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                        return i;
                    }
                }
            } catch (IllegalStateException e4) {
                yj2 yj2Var4 = g0;
                yj2Var4.error("Error under update transaction under table: " + str + " with whereClause: " + str2 + " and whereArgs: " + strArr, e4);
                if (i2 != null) {
                    try {
                        i2.endTransaction();
                    } catch (SQLiteException e5) {
                        e = e5;
                        yj2Var2 = g0;
                        str4 = "Error closing transaction! ";
                        yj2Var2.error(str4, e);
                        return i;
                    } catch (IllegalStateException e6) {
                        e = e6;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                        return i;
                    }
                }
            }
            try {
                i2.endTransaction();
            } catch (SQLiteException e7) {
                e = e7;
                yj2Var2 = g0;
                str4 = "Error closing transaction! ";
                yj2Var2.error(str4, e);
                return i;
            } catch (IllegalStateException e8) {
                e = e8;
                yj2Var = g0;
                str3 = "Error closing transaction! ";
                yj2Var.error(str3, e);
                return i;
            }
        }
        return i;
    }

    @Override // defpackage.bn2
    public Cursor b(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        Cursor query;
        synchronized (a) {
            query = i().query(str, strArr, str2, strArr2, str3, str4, str5, str6);
        }
        return query;
    }

    @Override // defpackage.bn2
    public Cursor c(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5) {
        Cursor query;
        synchronized (a) {
            query = i().query(str, strArr, str2, strArr2, str3, str4, str5);
        }
        return query;
    }

    @Override // defpackage.bn2
    public void d(String str, String str2, String[] strArr) {
        yj2 yj2Var;
        String str3;
        yj2 yj2Var2;
        String str4;
        synchronized (a) {
            SQLiteDatabase i = i();
            try {
                i.beginTransaction();
                i.delete(str, str2, strArr);
                i.setTransactionSuccessful();
            } catch (SQLiteException e) {
                yj2 yj2Var3 = g0;
                yj2Var3.error("Error deleting on table: " + str + " with whereClause: " + str2 + " and whereArgs: " + strArr, e);
                if (i != null) {
                    try {
                        try {
                            i.endTransaction();
                        } catch (SQLiteException e2) {
                            e = e2;
                            yj2Var2 = g0;
                            str4 = "Error closing transaction! ";
                            yj2Var2.error(str4, e);
                        }
                    } catch (IllegalStateException e3) {
                        e = e3;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                    }
                }
            } catch (IllegalStateException e4) {
                yj2 yj2Var4 = g0;
                yj2Var4.error("Error under delete transaction under table: " + str + " with whereClause: " + str2 + " and whereArgs: " + strArr, e4);
                if (i != null) {
                    try {
                        i.endTransaction();
                    } catch (SQLiteException e5) {
                        e = e5;
                        yj2Var2 = g0;
                        str4 = "Error closing transaction! ";
                        yj2Var2.error(str4, e);
                    } catch (IllegalStateException e6) {
                        e = e6;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                    }
                }
            }
            try {
                i.endTransaction();
            } catch (SQLiteException e7) {
                e = e7;
                yj2Var2 = g0;
                str4 = "Error closing transaction! ";
                yj2Var2.error(str4, e);
            } catch (IllegalStateException e8) {
                e = e8;
                yj2Var = g0;
                str3 = "Error closing transaction! ";
                yj2Var.error(str3, e);
            }
        }
    }

    @Override // defpackage.bn2
    public void e(String str, String str2, ContentValues contentValues) {
        yj2 yj2Var;
        String str3;
        yj2 yj2Var2;
        String str4;
        synchronized (a) {
            SQLiteDatabase i = i();
            try {
                i.beginTransaction();
                i.insert(str, str2, contentValues);
                i.setTransactionSuccessful();
            } catch (SQLiteException e) {
                yj2 yj2Var3 = g0;
                yj2Var3.error("Error inserting on table: " + str + " with nullColumnHack: " + str2 + " and values: " + contentValues, e);
                if (i != null) {
                    try {
                        try {
                            i.endTransaction();
                        } catch (SQLiteException e2) {
                            e = e2;
                            yj2Var2 = g0;
                            str4 = "Error closing transaction! ";
                            yj2Var2.error(str4, e);
                        }
                    } catch (IllegalStateException e3) {
                        e = e3;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                    }
                }
            } catch (IllegalStateException e4) {
                yj2 yj2Var4 = g0;
                yj2Var4.error("Error under inserting transaction under table: " + str + " with nullColumnHack: " + str2 + " and values: " + contentValues, e4);
                if (i != null) {
                    try {
                        i.endTransaction();
                    } catch (SQLiteException e5) {
                        e = e5;
                        yj2Var2 = g0;
                        str4 = "Error closing transaction! ";
                        yj2Var2.error(str4, e);
                    } catch (IllegalStateException e6) {
                        e = e6;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                    }
                }
            }
            try {
                i.endTransaction();
            } catch (SQLiteException e7) {
                e = e7;
                yj2Var2 = g0;
                str4 = "Error closing transaction! ";
                yj2Var2.error(str4, e);
            } catch (IllegalStateException e8) {
                e = e8;
                yj2Var = g0;
                str3 = "Error closing transaction! ";
                yj2Var.error(str3, e);
            }
        }
    }

    public final SQLiteDatabase h() {
        SQLiteDatabase writableDatabase;
        synchronized (a) {
            try {
                try {
                    writableDatabase = getWritableDatabase();
                } catch (SQLiteCantOpenDatabaseException e) {
                    throw e;
                } catch (SQLiteDatabaseLockedException e2) {
                    throw e2;
                }
            } finally {
            }
        }
        return writableDatabase;
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x0012  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0018 A[Catch: all -> 0x000b, LOOP:0: B:21:0x0005->B:17:0x0018, LOOP_END, TryCatch #2 {all -> 0x000b, blocks: (B:5:0x0005, B:6:0x0009, B:15:0x0013, B:17:0x0018, B:18:0x001f), top: B:21:0x0005 }] */
    /* JADX WARN: Removed duplicated region for block: B:24:0x001f A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.database.sqlite.SQLiteDatabase i() {
        /*
            r5 = this;
            java.lang.Object r0 = com.onesignal.a1.a
            monitor-enter(r0)
            r1 = 0
            r2 = 0
        L5:
            android.database.sqlite.SQLiteDatabase r1 = r5.h()     // Catch: java.lang.Throwable -> Lb android.database.sqlite.SQLiteDatabaseLockedException -> Ld android.database.sqlite.SQLiteCantOpenDatabaseException -> Lf
            monitor-exit(r0)     // Catch: java.lang.Throwable -> Lb
            return r1
        Lb:
            r1 = move-exception
            goto L20
        Ld:
            r3 = move-exception
            goto L10
        Lf:
            r3 = move-exception
        L10:
            if (r1 != 0) goto L13
            r1 = r3
        L13:
            int r2 = r2 + 1
            r3 = 5
            if (r2 >= r3) goto L1f
            int r3 = r2 * 400
            long r3 = (long) r3     // Catch: java.lang.Throwable -> Lb
            android.os.SystemClock.sleep(r3)     // Catch: java.lang.Throwable -> Lb
            goto L5
        L1f:
            throw r1     // Catch: java.lang.Throwable -> Lb
        L20:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> Lb
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.a1.i():android.database.sqlite.SQLiteDatabase");
    }

    public void j(String str, String str2, ContentValues contentValues) throws SQLException {
        yj2 yj2Var;
        String str3;
        yj2 yj2Var2;
        String str4;
        synchronized (a) {
            SQLiteDatabase i = i();
            try {
                i.beginTransaction();
                i.insertOrThrow(str, str2, contentValues);
                i.setTransactionSuccessful();
            } catch (SQLiteException e) {
                yj2 yj2Var3 = g0;
                yj2Var3.error("Error inserting or throw on table: " + str + " with nullColumnHack: " + str2 + " and values: " + contentValues, e);
                if (i != null) {
                    try {
                        try {
                            i.endTransaction();
                        } catch (SQLiteException e2) {
                            e = e2;
                            yj2Var2 = g0;
                            str4 = "Error closing transaction! ";
                            yj2Var2.error(str4, e);
                        }
                    } catch (IllegalStateException e3) {
                        e = e3;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                    }
                }
            } catch (IllegalStateException e4) {
                yj2 yj2Var4 = g0;
                yj2Var4.error("Error under inserting or throw transaction under table: " + str + " with nullColumnHack: " + str2 + " and values: " + contentValues, e4);
                if (i != null) {
                    try {
                        i.endTransaction();
                    } catch (SQLiteException e5) {
                        e = e5;
                        yj2Var2 = g0;
                        str4 = "Error closing transaction! ";
                        yj2Var2.error(str4, e);
                    } catch (IllegalStateException e6) {
                        e = e6;
                        yj2Var = g0;
                        str3 = "Error closing transaction! ";
                        yj2Var.error(str3, e);
                    }
                }
            }
            try {
                i.endTransaction();
            } catch (SQLiteException e7) {
                e = e7;
                yj2Var2 = g0;
                str4 = "Error closing transaction! ";
                yj2Var2.error(str4, e);
            } catch (IllegalStateException e8) {
                e = e8;
                yj2Var = g0;
                str3 = "Error closing transaction! ";
                yj2Var.error(str3, e);
            }
        }
    }

    public final void l(SQLiteDatabase sQLiteDatabase, int i) {
        if (i < 2) {
            r(sQLiteDatabase);
        }
        if (i < 3) {
            u(sQLiteDatabase);
        }
        if (i < 4) {
            v(sQLiteDatabase);
        }
        if (i < 5) {
            w(sQLiteDatabase);
        }
        if (i == 5) {
            q(sQLiteDatabase);
        }
        if (i < 7) {
            x(sQLiteDatabase);
        }
        if (i < 8) {
            z(sQLiteDatabase);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        synchronized (a) {
            sQLiteDatabase.execSQL("CREATE TABLE notification (_id INTEGER PRIMARY KEY,notification_id TEXT,android_notification_id INTEGER,group_id TEXT,collapse_id TEXT,is_summary INTEGER DEFAULT 0,opened INTEGER DEFAULT 0,dismissed INTEGER DEFAULT 0,title TEXT,message TEXT,full_data TEXT,created_time TIMESTAMP DEFAULT (strftime('%s', 'now')),expire_time TIMESTAMP);");
            sQLiteDatabase.execSQL("CREATE TABLE outcome (_id INTEGER PRIMARY KEY,notification_influence_type TEXT,iam_influence_type TEXT,notification_ids TEXT,iam_ids TEXT,name TEXT,timestamp TIMESTAMP,weight FLOAT);");
            sQLiteDatabase.execSQL("CREATE TABLE cached_unique_outcome (_id INTEGER PRIMARY KEY,channel_influence_id TEXT,channel_type TEXT,name TEXT);");
            sQLiteDatabase.execSQL("CREATE TABLE in_app_message (_id INTEGER PRIMARY KEY,display_quantity INTEGER,last_display INTEGER,message_id TEXT,displayed_in_session INTEGER,click_ids TEXT);");
            for (String str : f0) {
                sQLiteDatabase.execSQL(str);
            }
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        OneSignal.a(OneSignal.LOG_LEVEL.WARN, "SDK version rolled back! Clearing OneSignal.db as it could be in an unexpected state.");
        synchronized (a) {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
            ArrayList<String> arrayList = new ArrayList(rawQuery.getCount());
            while (rawQuery.moveToNext()) {
                arrayList.add(rawQuery.getString(0));
            }
            for (String str : arrayList) {
                if (!str.startsWith("sqlite_")) {
                    sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + str);
                }
            }
            rawQuery.close();
            onCreate(sQLiteDatabase);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "OneSignal Database onUpgrade from: " + i + " to: " + i2);
        synchronized (a) {
            try {
                l(sQLiteDatabase, i);
            } catch (SQLiteException e) {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error in upgrade, migration may have already run! Skipping!", e);
            }
        }
    }

    public final void z(SQLiteDatabase sQLiteDatabase) {
        i0.c(sQLiteDatabase);
        i0.a(sQLiteDatabase);
    }
}
