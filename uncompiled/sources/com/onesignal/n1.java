package com.onesignal;

import com.onesignal.LocationController;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UserState.java */
/* loaded from: classes2.dex */
public abstract class n1 {
    public static final Object d = new Object();
    public static final String[] e;
    public static final Set<String> f;
    public String a;
    public JSONObject b;
    public JSONObject c;

    static {
        String[] strArr = {"lat", "long", "loc_acc", "loc_type", "loc_bg", "loc_time_stamp"};
        e = strArr;
        f = new HashSet(Arrays.asList(strArr));
    }

    public n1(String str, boolean z) {
        this.a = str;
        if (z) {
            n();
            return;
        }
        this.b = new JSONObject();
        this.c = new JSONObject();
    }

    public static JSONObject e(JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, Set<String> set) {
        JSONObject b;
        synchronized (d) {
            b = j.b(jSONObject, jSONObject2, jSONObject3, set);
        }
        return b;
    }

    public abstract void a();

    public void b() {
        try {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("lat", null);
            hashMap.put("long", null);
            hashMap.put("loc_acc", null);
            hashMap.put("loc_type", null);
            hashMap.put("loc_bg", null);
            hashMap.put("loc_time_stamp", null);
            u(this.c, hashMap);
            HashMap<String, Object> hashMap2 = new HashMap<>();
            hashMap2.put("loc_bg", null);
            hashMap2.put("loc_time_stamp", null);
            u(this.b, hashMap2);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public n1 c(String str) {
        n1 p = p(str);
        try {
            p.b = j();
            p.c = m();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return p;
    }

    public JSONObject d(n1 n1Var, boolean z) {
        a();
        n1Var.a();
        JSONObject e2 = e(this.c, n1Var.c, null, k(n1Var));
        if (z || !e2.toString().equals("{}")) {
            try {
                if (!e2.has("app_id")) {
                    e2.put("app_id", this.c.optString("app_id"));
                }
                if (this.c.has("email_auth_hash")) {
                    e2.put("email_auth_hash", this.c.optString("email_auth_hash"));
                }
                if (this.c.has("sms_auth_hash")) {
                    e2.put("sms_auth_hash", this.c.optString("sms_auth_hash"));
                }
                if (this.c.has("external_user_id_auth_hash") && !e2.has("external_user_id_auth_hash")) {
                    e2.put("external_user_id_auth_hash", this.c.optString("external_user_id_auth_hash"));
                }
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
            return e2;
        }
        return null;
    }

    public JSONObject f(n1 n1Var, Set<String> set) {
        JSONObject b;
        synchronized (d) {
            b = j.b(this.b, n1Var.b, null, set);
        }
        return b;
    }

    public JSONObject g(JSONObject jSONObject, Set<String> set) {
        JSONObject b;
        synchronized (d) {
            JSONObject jSONObject2 = this.b;
            b = j.b(jSONObject2, jSONObject, jSONObject2, set);
        }
        return b;
    }

    public JSONObject h(JSONObject jSONObject, Set<String> set) {
        JSONObject b;
        synchronized (d) {
            JSONObject jSONObject2 = this.c;
            b = j.b(jSONObject2, jSONObject, jSONObject2, set);
        }
        return b;
    }

    public hp1 i() {
        try {
            return new hp1(j());
        } catch (JSONException e2) {
            e2.printStackTrace();
            return new hp1();
        }
    }

    public JSONObject j() throws JSONException {
        JSONObject jSONObject;
        synchronized (d) {
            jSONObject = new JSONObject(this.b.toString());
        }
        return jSONObject;
    }

    public final Set<String> k(n1 n1Var) {
        try {
            if (this.b.optLong("loc_time_stamp") != n1Var.b.getLong("loc_time_stamp")) {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("loc_bg", n1Var.b.opt("loc_bg"));
                hashMap.put("loc_time_stamp", n1Var.b.opt("loc_time_stamp"));
                u(n1Var.c, hashMap);
                return f;
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    public hp1 l() {
        try {
            return new hp1(m());
        } catch (JSONException e2) {
            e2.printStackTrace();
            return new hp1();
        }
    }

    public JSONObject m() throws JSONException {
        JSONObject jSONObject;
        synchronized (d) {
            jSONObject = new JSONObject(this.c.toString());
        }
        return jSONObject;
    }

    public final void n() {
        int c;
        boolean z;
        String str = b1.a;
        String f2 = b1.f(str, "ONESIGNAL_USERSTATE_DEPENDVALYES_" + this.a, null);
        if (f2 == null) {
            x(new JSONObject());
            try {
                int i = 1;
                if (this.a.equals("CURRENT_STATE")) {
                    c = b1.c(str, "ONESIGNAL_SUBSCRIPTION", 1);
                } else {
                    c = b1.c(str, "ONESIGNAL_SYNCED_SUBSCRIPTION", 1);
                }
                if (c == -2) {
                    z = false;
                } else {
                    i = c;
                    z = true;
                }
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("subscribableStatus", Integer.valueOf(i));
                hashMap.put("userSubscribePref", Boolean.valueOf(z));
                u(this.b, hashMap);
            } catch (JSONException unused) {
            }
        } else {
            try {
                x(new JSONObject(f2));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        String str2 = b1.a;
        String f3 = b1.f(str2, "ONESIGNAL_USERSTATE_SYNCVALYES_" + this.a, null);
        JSONObject jSONObject = new JSONObject();
        try {
            if (f3 == null) {
                jSONObject.put("identifier", b1.f(str2, "GT_REGISTRATION_ID", null));
            } else {
                jSONObject = new JSONObject(f3);
            }
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        z(jSONObject);
    }

    public void o(JSONObject jSONObject, JSONObject jSONObject2) {
        JSONObject jSONObject3;
        if (jSONObject.has("tags")) {
            try {
                JSONObject m = m();
                if (m.has("tags")) {
                    try {
                        jSONObject3 = new JSONObject(m.optString("tags"));
                    } catch (JSONException unused) {
                        jSONObject3 = new JSONObject();
                    }
                } else {
                    jSONObject3 = new JSONObject();
                }
                JSONObject optJSONObject = jSONObject.optJSONObject("tags");
                Iterator<String> keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    if ("".equals(optJSONObject.optString(next))) {
                        jSONObject3.remove(next);
                    } else if (jSONObject2 == null || !jSONObject2.has(next)) {
                        jSONObject3.put(next, optJSONObject.optString(next));
                    }
                }
                synchronized (d) {
                    if (jSONObject3.toString().equals("{}")) {
                        this.c.remove("tags");
                    } else {
                        this.c.put("tags", jSONObject3);
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    public abstract n1 p(String str);

    public void q() {
        synchronized (d) {
            try {
                if (this.c.has("external_user_id_auth_hash") && ((this.c.has("external_user_id") && this.c.get("external_user_id").toString() == "") || !this.c.has("external_user_id"))) {
                    this.c.remove("external_user_id_auth_hash");
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            String str = b1.a;
            b1.m(str, "ONESIGNAL_USERSTATE_SYNCVALYES_" + this.a, this.c.toString());
            b1.m(str, "ONESIGNAL_USERSTATE_DEPENDVALYES_" + this.a, this.b.toString());
        }
    }

    public void r(JSONObject jSONObject, JSONObject jSONObject2) {
        if (jSONObject != null) {
            JSONObject jSONObject3 = this.b;
            e(jSONObject3, jSONObject, jSONObject3, null);
        }
        if (jSONObject2 != null) {
            JSONObject jSONObject4 = this.c;
            e(jSONObject4, jSONObject2, jSONObject4, null);
            o(jSONObject2, null);
        }
        if (jSONObject == null && jSONObject2 == null) {
            return;
        }
        q();
    }

    public void s(String str, Object obj) throws JSONException {
        synchronized (d) {
            this.b.put(str, obj);
        }
    }

    public void t(String str, Object obj) throws JSONException {
        synchronized (d) {
            this.c.put(str, obj);
        }
    }

    public String toString() {
        return "UserState{persistKey='" + this.a + "', dependValues=" + this.b + ", syncValues=" + this.c + '}';
    }

    public final void u(JSONObject jSONObject, HashMap<String, Object> hashMap) throws JSONException {
        synchronized (d) {
            for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                jSONObject.put(entry.getKey(), entry.getValue());
            }
        }
    }

    public void v(String str) {
        synchronized (d) {
            this.b.remove(str);
        }
    }

    public void w(String str) {
        synchronized (d) {
            this.c.remove(str);
        }
    }

    public void x(JSONObject jSONObject) {
        synchronized (d) {
            this.b = jSONObject;
        }
    }

    public void y(LocationController.d dVar) {
        try {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("lat", dVar.a);
            hashMap.put("long", dVar.b);
            hashMap.put("loc_acc", dVar.c);
            hashMap.put("loc_type", dVar.d);
            u(this.c, hashMap);
            HashMap<String, Object> hashMap2 = new HashMap<>();
            hashMap2.put("loc_bg", dVar.e);
            hashMap2.put("loc_time_stamp", dVar.f);
            u(this.b, hashMap2);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void z(JSONObject jSONObject) {
        synchronized (d) {
            this.c = jSONObject;
        }
    }
}
