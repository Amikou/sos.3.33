package com.onesignal;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.legacy.content.WakefulBroadcastReceiver;
import com.onesignal.OneSignal;
import com.onesignal.k;

/* loaded from: classes2.dex */
public class FCMBroadcastReceiver extends WakefulBroadcastReceiver {

    /* loaded from: classes2.dex */
    public class a implements k.e {
        public a() {
        }

        @Override // com.onesignal.k.e
        public void a(k.f fVar) {
            if (fVar == null) {
                FCMBroadcastReceiver.this.j();
            } else if (fVar.a() || fVar.b()) {
                FCMBroadcastReceiver.this.h();
            } else {
                FCMBroadcastReceiver.this.j();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b implements k.e {
        public final /* synthetic */ k.e a;
        public final /* synthetic */ Context b;
        public final /* synthetic */ Bundle c;

        public b(k.e eVar, Context context, Bundle bundle) {
            this.a = eVar;
            this.b = context;
            this.c = bundle;
        }

        @Override // com.onesignal.k.e
        public void a(k.f fVar) {
            if (fVar != null && fVar.c()) {
                this.a.a(fVar);
                return;
            }
            FCMBroadcastReceiver.k(this.b, this.c);
            this.a.a(fVar);
        }
    }

    public static boolean f(Intent intent) {
        if ("com.google.android.c2dm.intent.RECEIVE".equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("message_type");
            return stringExtra == null || "gcm".equals(stringExtra);
        }
        return false;
    }

    public static void g(Context context, Intent intent, Bundle bundle, k.e eVar) {
        if (!f(intent)) {
            eVar.a(null);
        }
        k.h(context, bundle, new b(eVar, context, bundle));
    }

    public static cs i(Bundle bundle, cs csVar) {
        csVar.putString("json_payload", k.a(bundle).toString());
        csVar.a("timestamp", Long.valueOf(OneSignal.w0().a() / 1000));
        return csVar;
    }

    public static void k(Context context, Bundle bundle) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "startFCMService from: " + context + " and bundle: " + bundle);
        if (!k.c(bundle)) {
            OneSignal.a(log_level, "startFCMService with no remote resources, no need for services");
            k.j(context, i(bundle, es.a()));
            return;
        }
        if (!(Integer.parseInt(bundle.getString("pri", "0")) > 9) && Build.VERSION.SDK_INT >= 26) {
            l(context, bundle);
            return;
        }
        try {
            m(context, bundle);
        } catch (IllegalStateException e) {
            if (Build.VERSION.SDK_INT >= 21) {
                l(context, bundle);
                return;
            }
            throw e;
        }
    }

    @TargetApi(21)
    public static void l(Context context, Bundle bundle) {
        cs i = i(bundle, es.a());
        Intent intent = new Intent(context, FCMIntentJobService.class);
        intent.putExtra("Bundle:Parcelable:Extras", (Parcelable) i.c());
        FCMIntentJobService.j(context, intent);
    }

    public static void m(Context context, Bundle bundle) {
        WakefulBroadcastReceiver.c(context, new Intent().replaceExtras((Bundle) i(bundle, new ds()).c()).setComponent(new ComponentName(context.getPackageName(), FCMIntentService.class.getName())));
    }

    public final void h() {
        if (isOrderedBroadcast()) {
            abortBroadcast();
            setResultCode(-1);
        }
    }

    public final void j() {
        if (isOrderedBroadcast()) {
            setResultCode(-1);
        }
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null || "google.com/iid".equals(extras.getString("from"))) {
            return;
        }
        OneSignal.L0(context);
        g(context, intent, extras, new a());
    }
}
