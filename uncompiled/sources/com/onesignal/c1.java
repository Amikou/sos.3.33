package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.d1;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OneSignalRemoteParams.java */
/* loaded from: classes2.dex */
public class c1 {
    public static int a;

    /* compiled from: OneSignalRemoteParams.java */
    /* loaded from: classes2.dex */
    public class a extends d1.g {
        public final /* synthetic */ String a;
        public final /* synthetic */ String b;
        public final /* synthetic */ c c;

        /* compiled from: OneSignalRemoteParams.java */
        /* renamed from: com.onesignal.c1$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class RunnableC0157a implements Runnable {
            public RunnableC0157a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                int i = (c1.a * 10000) + 30000;
                if (i > 90000) {
                    i = 90000;
                }
                OneSignal.a(OneSignal.LOG_LEVEL.INFO, "Failed to get Android parameters, trying again in " + (i / 1000) + " seconds.");
                try {
                    Thread.sleep(i);
                    c1.b();
                    a aVar = a.this;
                    c1.e(aVar.a, aVar.b, aVar.c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public a(String str, String str2, c cVar) {
            this.a = str;
            this.b = str2;
            this.c = cVar;
        }

        @Override // com.onesignal.d1.g
        public void a(int i, String str, Throwable th) {
            if (i == 403) {
                OneSignal.a(OneSignal.LOG_LEVEL.FATAL, "403 error getting OneSignal params, omitting further retries!");
            } else {
                new Thread(new RunnableC0157a(), "OS_PARAMS_REQUEST").start();
            }
        }

        @Override // com.onesignal.d1.g
        public void b(String str) {
            c1.f(str, this.c);
        }
    }

    /* compiled from: OneSignalRemoteParams.java */
    /* loaded from: classes2.dex */
    public class b extends f {
        public final /* synthetic */ JSONObject m;

        public b(JSONObject jSONObject) {
            this.m = jSONObject;
            jSONObject.optBoolean("enterp", false);
            jSONObject.optBoolean("require_email_auth", false);
            jSONObject.optBoolean("require_user_id_auth", false);
            this.b = jSONObject.optJSONArray("chnl_lst");
            this.c = jSONObject.optBoolean("fba", false);
            this.d = jSONObject.optBoolean("restore_ttl_filter", true);
            this.a = jSONObject.optString("android_sender_id", null);
            this.e = jSONObject.optBoolean("clear_group_on_summary_click", true);
            this.f = jSONObject.optBoolean("receive_receipts_enable", false);
            this.g = !jSONObject.has("disable_gms_missing_prompt") ? null : Boolean.valueOf(jSONObject.optBoolean("disable_gms_missing_prompt", false));
            this.h = !jSONObject.has("unsubscribe_on_notifications_disabled") ? null : Boolean.valueOf(jSONObject.optBoolean("unsubscribe_on_notifications_disabled", true));
            this.i = !jSONObject.has("location_shared") ? null : Boolean.valueOf(jSONObject.optBoolean("location_shared", true));
            this.j = !jSONObject.has("requires_user_privacy_consent") ? null : Boolean.valueOf(jSONObject.optBoolean("requires_user_privacy_consent", false));
            this.k = new e();
            if (jSONObject.has("outcomes")) {
                c1.g(jSONObject.optJSONObject("outcomes"), this.k);
            }
            this.l = new d();
            if (jSONObject.has("fcm")) {
                JSONObject optJSONObject = jSONObject.optJSONObject("fcm");
                this.l.c = optJSONObject.optString("api_key", null);
                this.l.b = optJSONObject.optString("app_id", null);
                this.l.a = optJSONObject.optString("project_id", null);
            }
        }
    }

    /* compiled from: OneSignalRemoteParams.java */
    /* loaded from: classes2.dex */
    public interface c {
        void a(f fVar);
    }

    /* compiled from: OneSignalRemoteParams.java */
    /* loaded from: classes2.dex */
    public static class d {
        public String a;
        public String b;
        public String c;
    }

    /* compiled from: OneSignalRemoteParams.java */
    /* loaded from: classes2.dex */
    public static class e {
        public int a = 1440;
        public int b = 10;
        public int c = 1440;
        public int d = 10;
        public boolean e = false;
        public boolean f = false;
        public boolean g = false;
        public boolean h = false;

        public int a() {
            return this.d;
        }

        public int b() {
            return this.c;
        }

        public int c() {
            return this.a;
        }

        public int d() {
            return this.b;
        }

        public boolean e() {
            return this.e;
        }

        public boolean f() {
            return this.f;
        }

        public boolean g() {
            return this.g;
        }

        public String toString() {
            return "InfluenceParams{indirectNotificationAttributionWindow=" + this.a + ", notificationLimit=" + this.b + ", indirectIAMAttributionWindow=" + this.c + ", iamLimit=" + this.d + ", directEnabled=" + this.e + ", indirectEnabled=" + this.f + ", unattributedEnabled=" + this.g + '}';
        }
    }

    /* compiled from: OneSignalRemoteParams.java */
    /* loaded from: classes2.dex */
    public static class f {
        public String a;
        public JSONArray b;
        public boolean c;
        public boolean d;
        public boolean e;
        public boolean f;
        public Boolean g;
        public Boolean h;
        public Boolean i;
        public Boolean j;
        public e k;
        public d l;
    }

    public static /* synthetic */ int b() {
        int i = a;
        a = i + 1;
        return i;
    }

    public static void e(String str, String str2, c cVar) {
        a aVar = new a(str, str2, cVar);
        String str3 = "apps/" + str + "/android_params.js";
        if (str2 != null) {
            str3 = str3 + "?player_id=" + str2;
        }
        OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "Starting request to get Android parameters.");
        d1.e(str3, aVar, "CACHE_KEY_REMOTE_PARAMS");
    }

    public static void f(String str, c cVar) {
        try {
            cVar.a(new b(new JSONObject(str)));
        } catch (NullPointerException | JSONException e2) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.FATAL;
            OneSignal.b(log_level, "Error parsing android_params!: ", e2);
            OneSignal.a(log_level, "Response that errored from android_params!: " + str);
        }
    }

    public static void g(JSONObject jSONObject, e eVar) {
        if (jSONObject.has("v2_enabled")) {
            eVar.h = jSONObject.optBoolean("v2_enabled");
        }
        if (jSONObject.has("direct")) {
            eVar.e = jSONObject.optJSONObject("direct").optBoolean("enabled");
        }
        if (jSONObject.has("indirect")) {
            JSONObject optJSONObject = jSONObject.optJSONObject("indirect");
            eVar.f = optJSONObject.optBoolean("enabled");
            if (optJSONObject.has("notification_attribution")) {
                JSONObject optJSONObject2 = optJSONObject.optJSONObject("notification_attribution");
                eVar.a = optJSONObject2.optInt("minutes_since_displayed", 1440);
                eVar.b = optJSONObject2.optInt("limit", 10);
            }
            if (optJSONObject.has("in_app_message_attribution")) {
                JSONObject optJSONObject3 = optJSONObject.optJSONObject("in_app_message_attribution");
                eVar.c = optJSONObject3.optInt("minutes_since_displayed", 1440);
                eVar.d = optJSONObject3.optInt("limit", 10);
            }
        }
        if (jSONObject.has("unattributed")) {
            eVar.g = jSONObject.optJSONObject("unattributed").optBoolean("enabled");
        }
    }
}
