package com.onesignal;

import com.onesignal.OneSignal;

/* compiled from: OSLogWrapper.java */
/* loaded from: classes2.dex */
public class f0 implements yj2 {
    @Override // defpackage.yj2
    public void a(String str) {
        OneSignal.a(OneSignal.LOG_LEVEL.VERBOSE, str);
    }

    @Override // defpackage.yj2
    public void b(String str) {
        OneSignal.a(OneSignal.LOG_LEVEL.WARN, str);
    }

    @Override // defpackage.yj2
    public void debug(String str) {
        OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, str);
    }

    @Override // defpackage.yj2
    public void error(String str) {
        OneSignal.a(OneSignal.LOG_LEVEL.ERROR, str);
    }

    @Override // defpackage.yj2
    public void info(String str) {
        OneSignal.a(OneSignal.LOG_LEVEL.INFO, str);
    }

    @Override // defpackage.yj2
    public void error(String str, Throwable th) {
        OneSignal.b(OneSignal.LOG_LEVEL.ERROR, str, th);
    }
}
