package com.onesignal;

import com.onesignal.OneSignal;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInAppMessageRedisplayStats.java */
/* loaded from: classes2.dex */
public class c0 {
    public long a;
    public int b;
    public int c;
    public long d;
    public boolean e;

    public c0() {
        this.a = -1L;
        this.b = 0;
        this.c = 1;
        this.d = 0L;
        this.e = false;
    }

    public int a() {
        return this.b;
    }

    public long b() {
        return this.a;
    }

    public void c() {
        this.b++;
    }

    public boolean d() {
        if (this.a < 0) {
            return true;
        }
        long a = OneSignal.w0().a() / 1000;
        long j = a - this.a;
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "OSInAppMessage lastDisplayTime: " + this.a + " currentTimeInSeconds: " + a + " diffInSeconds: " + j + " displayDelay: " + this.d);
        return j >= this.d;
    }

    public boolean e() {
        return this.e;
    }

    public void f(int i) {
        this.b = i;
    }

    public void g(c0 c0Var) {
        h(c0Var.b());
        f(c0Var.a());
    }

    public void h(long j) {
        this.a = j;
    }

    public boolean i() {
        boolean z = this.b < this.c;
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "OSInAppMessage shouldDisplayAgain: " + z);
        return z;
    }

    public String toString() {
        return "OSInAppMessageDisplayStats{lastDisplayTime=" + this.a + ", displayQuantity=" + this.b + ", displayLimit=" + this.c + ", displayDelay=" + this.d + '}';
    }

    public c0(int i, long j) {
        this.a = -1L;
        this.b = 0;
        this.c = 1;
        this.d = 0L;
        this.e = false;
        this.b = i;
        this.a = j;
    }

    public c0(JSONObject jSONObject) throws JSONException {
        this.a = -1L;
        this.b = 0;
        this.c = 1;
        this.d = 0L;
        this.e = false;
        this.e = true;
        Object obj = jSONObject.get("limit");
        Object obj2 = jSONObject.get("delay");
        if (obj instanceof Integer) {
            this.c = ((Integer) obj).intValue();
        }
        if (obj2 instanceof Long) {
            this.d = ((Long) obj2).longValue();
        } else if (obj2 instanceof Integer) {
            this.d = ((Integer) obj2).intValue();
        }
    }
}
