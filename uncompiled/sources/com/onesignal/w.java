package com.onesignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInAppMessageDummyController.java */
/* loaded from: classes2.dex */
public class w extends OSInAppMessageController {
    public w(a1 a1Var, w0 w0Var, yj2 yj2Var, vk2 vk2Var, cy1 cy1Var) {
        super(a1Var, w0Var, yj2Var, vk2Var, cy1Var);
    }

    @Override // com.onesignal.OSInAppMessageController
    public void D() {
    }

    @Override // com.onesignal.OSInAppMessageController
    public void G(String str) {
    }

    @Override // com.onesignal.OSInAppMessageController
    public void S() {
    }

    @Override // com.onesignal.OSInAppMessageController
    public void T() {
    }

    @Override // com.onesignal.OSInAppMessageController
    public boolean U() {
        return false;
    }

    @Override // com.onesignal.OSInAppMessageController
    public void X(x xVar) {
    }

    @Override // com.onesignal.OSInAppMessageController
    public void Z(x xVar, JSONObject jSONObject) {
    }

    @Override // com.onesignal.OSInAppMessageController, com.onesignal.t.c
    public void a() {
    }

    @Override // com.onesignal.OSInAppMessageController
    public void a0(x xVar, JSONObject jSONObject) {
    }

    @Override // com.onesignal.OSInAppMessageController
    public void l0(JSONArray jSONArray) throws JSONException {
    }
}
