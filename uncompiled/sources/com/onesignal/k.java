package com.onesignal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.onesignal.OneSignal;
import com.onesignal.i0;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: NotificationBundleProcessor.java */
/* loaded from: classes2.dex */
public class k {

    /* compiled from: NotificationBundleProcessor.java */
    /* loaded from: classes2.dex */
    public class a implements i0.d {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ JSONObject b;
        public final /* synthetic */ Context c;
        public final /* synthetic */ int d;
        public final /* synthetic */ String e;
        public final /* synthetic */ long f;

        public a(boolean z, JSONObject jSONObject, Context context, int i, String str, long j) {
            this.a = z;
            this.b = jSONObject;
            this.c = context;
            this.d = i;
            this.e = str;
            this.f = j;
        }

        @Override // com.onesignal.i0.d
        public void a(boolean z) {
            if (this.a || !z) {
                OSNotificationWorkManager.b(this.c, j0.b(this.b), this.d, this.e, this.f, this.a, false);
                if (this.a) {
                    OSUtils.V(100);
                }
            }
        }
    }

    /* compiled from: NotificationBundleProcessor.java */
    /* loaded from: classes2.dex */
    public class b implements d {
        public final /* synthetic */ f a;
        public final /* synthetic */ e b;

        public b(f fVar, e eVar) {
            this.a = fVar;
            this.b = eVar;
        }

        @Override // com.onesignal.k.d
        public void a(boolean z) {
            if (!z) {
                this.a.d(true);
            }
            this.b.a(this.a);
        }
    }

    /* compiled from: NotificationBundleProcessor.java */
    /* loaded from: classes2.dex */
    public class c implements i0.d {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ Context b;
        public final /* synthetic */ Bundle c;
        public final /* synthetic */ d d;
        public final /* synthetic */ JSONObject e;
        public final /* synthetic */ long f;
        public final /* synthetic */ boolean g;
        public final /* synthetic */ f h;

        public c(boolean z, Context context, Bundle bundle, d dVar, JSONObject jSONObject, long j, boolean z2, f fVar) {
            this.a = z;
            this.b = context;
            this.c = bundle;
            this.d = dVar;
            this.e = jSONObject;
            this.f = j;
            this.g = z2;
            this.h = fVar;
        }

        @Override // com.onesignal.i0.d
        public void a(boolean z) {
            if (!this.a && z) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.a(log_level, "startNotificationProcessing returning, with context: " + this.b + " and bundle: " + this.c);
                this.d.a(false);
                return;
            }
            OSNotificationWorkManager.b(this.b, j0.b(this.e), this.c.containsKey("android_notif_id") ? this.c.getInt("android_notif_id") : 0, this.e.toString(), this.f, this.a, this.g);
            this.h.g(true);
            this.d.a(true);
        }
    }

    /* compiled from: NotificationBundleProcessor.java */
    /* loaded from: classes2.dex */
    public interface d {
        void a(boolean z);
    }

    /* compiled from: NotificationBundleProcessor.java */
    /* loaded from: classes2.dex */
    public interface e {
        void a(f fVar);
    }

    /* compiled from: NotificationBundleProcessor.java */
    /* loaded from: classes2.dex */
    public static class f {
        public boolean a;
        public boolean b;
        public boolean c;
        public boolean d;

        public boolean a() {
            return this.b;
        }

        public boolean b() {
            return this.d;
        }

        public boolean c() {
            return !this.a || this.b || this.c || this.d;
        }

        public void d(boolean z) {
            this.b = z;
        }

        public void e(boolean z) {
            this.c = z;
        }

        public void f(boolean z) {
            this.a = z;
        }

        public void g(boolean z) {
            this.d = z;
        }
    }

    public static JSONObject a(Bundle bundle) {
        JSONObject jSONObject = new JSONObject();
        for (String str : bundle.keySet()) {
            try {
                jSONObject.put(str, bundle.get(str));
            } catch (JSONException e2) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.b(log_level, "bundleAsJSONObject error for key: " + str, e2);
            }
        }
        return jSONObject;
    }

    public static JSONObject b(JSONObject jSONObject) throws JSONException {
        return new JSONObject(jSONObject.optString("custom"));
    }

    public static boolean c(Bundle bundle) {
        return d(bundle, "licon") || d(bundle, "bicon") || bundle.getString("bg_img", null) != null;
    }

    public static boolean d(Bundle bundle, String str) {
        String trim = bundle.getString(str, "").trim();
        return trim.startsWith("http://") || trim.startsWith("https://");
    }

    public static void e(zj2 zj2Var) {
        if (zj2Var.m()) {
            OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "Marking restored or disabled notifications as dismissed: " + zj2Var.toString());
            a1 g = a1.g(zj2Var.d());
            ContentValues contentValues = new ContentValues();
            contentValues.put("dismissed", (Integer) 1);
            g.a("notification", contentValues, "android_notification_id = " + zj2Var.a(), null);
            com.onesignal.b.c(g, zj2Var.d());
        }
    }

    public static void f(Bundle bundle) {
        JSONObject jSONObject;
        String str;
        if (bundle.containsKey("o")) {
            try {
                JSONObject jSONObject2 = new JSONObject(bundle.getString("custom"));
                if (jSONObject2.has("a")) {
                    jSONObject = jSONObject2.getJSONObject("a");
                } else {
                    jSONObject = new JSONObject();
                }
                JSONArray jSONArray = new JSONArray(bundle.getString("o"));
                bundle.remove("o");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                    String string = jSONObject3.getString("n");
                    jSONObject3.remove("n");
                    if (jSONObject3.has("i")) {
                        str = jSONObject3.getString("i");
                        jSONObject3.remove("i");
                    } else {
                        str = string;
                    }
                    jSONObject3.put("id", str);
                    jSONObject3.put(PublicResolver.FUNC_TEXT, string);
                    if (jSONObject3.has("p")) {
                        jSONObject3.put("icon", jSONObject3.getString("p"));
                        jSONObject3.remove("p");
                    }
                }
                jSONObject.put("actionButtons", jSONArray);
                jSONObject.put("actionId", "__DEFAULT__");
                if (!jSONObject2.has("a")) {
                    jSONObject2.put("a", jSONObject);
                }
                bundle.putString("custom", jSONObject2.toString());
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static JSONArray g(JSONObject jSONObject) {
        return new JSONArray().put(jSONObject);
    }

    public static void h(Context context, Bundle bundle, e eVar) {
        f fVar = new f();
        if (!j0.d(bundle)) {
            eVar.a(fVar);
            return;
        }
        fVar.f(true);
        f(bundle);
        if (tj2.c(context, bundle)) {
            fVar.e(true);
            eVar.a(fVar);
            return;
        }
        q(context, bundle, fVar, new b(fVar, eVar));
    }

    public static void i(zj2 zj2Var) {
        if (zj2Var.n() || !zj2Var.e().has("collapse_key") || "do_not_collapse".equals(zj2Var.e().optString("collapse_key"))) {
            return;
        }
        Cursor c2 = a1.g(zj2Var.d()).c("notification", new String[]{"android_notification_id"}, "collapse_id = ? AND dismissed = 0 AND opened = 0 ", new String[]{zj2Var.e().optString("collapse_key")}, null, null, null);
        if (c2.moveToFirst()) {
            zj2Var.f().r(c2.getInt(c2.getColumnIndex("android_notification_id")));
        }
        c2.close();
    }

    public static void j(Context context, cs csVar) {
        OneSignal.L0(context);
        try {
            String e2 = csVar.e("json_payload");
            if (e2 == null) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.a(log_level, "json_payload key is nonexistent from mBundle passed to ProcessFromFCMIntentService: " + csVar);
                return;
            }
            JSONObject jSONObject = new JSONObject(e2);
            OneSignal.W0(context, jSONObject, new a(csVar.getBoolean("is_restoring", false), jSONObject, context, csVar.f("android_notif_id") ? csVar.d("android_notif_id").intValue() : 0, e2, csVar.b("timestamp").longValue()));
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
    }

    public static int k(zj2 zj2Var, boolean z) {
        return m(new h0(zj2Var, zj2Var.n(), true), false, z);
    }

    public static int l(h0 h0Var, boolean z) {
        return m(h0Var, false, z);
    }

    public static int m(h0 h0Var, boolean z, boolean z2) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "Starting processJobForDisplay opened: " + z + " fromBackgroundLogic: " + z2);
        zj2 b2 = h0Var.b();
        i(b2);
        int intValue = b2.a().intValue();
        boolean z3 = false;
        if (p(b2)) {
            b2.p(true);
            if (z2 && OneSignal.J1(b2)) {
                h0Var.g(false);
                OneSignal.J(h0Var);
                return intValue;
            }
            z3 = com.onesignal.e.n(b2);
        }
        if (!b2.n()) {
            n(b2, z, z3);
            OSNotificationWorkManager.c(j0.b(h0Var.b().e()));
            OneSignal.F0(b2);
        }
        return intValue;
    }

    public static void n(zj2 zj2Var, boolean z, boolean z2) {
        o(zj2Var, z);
        if (!z2) {
            e(zj2Var);
            return;
        }
        String b2 = zj2Var.b();
        OSReceiveReceiptController.c().a(zj2Var.d(), b2);
        OneSignal.s0().l(b2);
    }

    public static void o(zj2 zj2Var, boolean z) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "Saving Notification job: " + zj2Var.toString());
        Context d2 = zj2Var.d();
        JSONObject e2 = zj2Var.e();
        try {
            JSONObject b2 = b(zj2Var.e());
            a1 g = a1.g(zj2Var.d());
            int i = 1;
            if (zj2Var.m()) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("dismissed", (Integer) 1);
                g.a("notification", contentValues, "android_notification_id = " + zj2Var.a(), null);
                com.onesignal.b.c(g, d2);
            }
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("notification_id", b2.optString("i"));
            if (e2.has("grp")) {
                contentValues2.put("group_id", e2.optString("grp"));
            }
            if (e2.has("collapse_key") && !"do_not_collapse".equals(e2.optString("collapse_key"))) {
                contentValues2.put("collapse_id", e2.optString("collapse_key"));
            }
            if (!z) {
                i = 0;
            }
            contentValues2.put("opened", Integer.valueOf(i));
            if (!z) {
                contentValues2.put("android_notification_id", zj2Var.a());
            }
            if (zj2Var.k() != null) {
                contentValues2.put("title", zj2Var.k().toString());
            }
            if (zj2Var.c() != null) {
                contentValues2.put(ThrowableDeserializer.PROP_NAME_MESSAGE, zj2Var.c().toString());
            }
            contentValues2.put("expire_time", Long.valueOf((e2.optLong("google.sent_time", OneSignal.w0().a()) / 1000) + e2.optInt("google.ttl", 259200)));
            contentValues2.put("full_data", e2.toString());
            g.j("notification", null, contentValues2);
            OneSignal.a(log_level, "Notification saved values: " + contentValues2.toString());
            if (z) {
                return;
            }
            com.onesignal.b.c(g, d2);
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
    }

    public static boolean p(zj2 zj2Var) {
        return zj2Var.l() || OSUtils.I(zj2Var.e().optString("alert"));
    }

    public static void q(Context context, Bundle bundle, f fVar, d dVar) {
        JSONObject a2 = a(bundle);
        OneSignal.W0(context, a2, new c(bundle.getBoolean("is_restoring", false), context, bundle, dVar, a2, OneSignal.w0().a() / 1000, Integer.parseInt(bundle.getString("pri", "0")) > 9, fVar));
    }
}
