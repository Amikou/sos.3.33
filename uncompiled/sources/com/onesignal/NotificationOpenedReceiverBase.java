package com.onesignal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/* compiled from: NotificationOpenedReceiverBase.kt */
/* loaded from: classes2.dex */
public abstract class NotificationOpenedReceiverBase extends Activity {
    @Override // android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        n.g(this, getIntent());
        finish();
    }

    @Override // android.app.Activity
    public void onNewIntent(Intent intent) {
        fs1.f(intent, "intent");
        super.onNewIntent(intent);
        n.g(this, getIntent());
        finish();
    }
}
