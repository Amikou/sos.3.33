package com.onesignal;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class OSSMSSubscriptionChangedInternalObserver {
    public static void a(tk2 tk2Var) {
        if (OneSignal.n0().c(new uk2(OneSignal.m0, (tk2) tk2Var.clone()))) {
            tk2 tk2Var2 = (tk2) tk2Var.clone();
            OneSignal.m0 = tk2Var2;
            tk2Var2.d();
        }
    }

    public void changed(tk2 tk2Var) {
        a(tk2Var);
    }
}
