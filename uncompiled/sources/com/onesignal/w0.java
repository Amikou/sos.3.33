package com.onesignal;

import com.onesignal.OneSignal;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: OSTaskController.java */
/* loaded from: classes2.dex */
public class w0 {
    public final ConcurrentLinkedQueue<Runnable> a = new ConcurrentLinkedQueue<>();
    public final AtomicLong b = new AtomicLong();
    public ExecutorService c;
    public final yj2 d;

    /* compiled from: OSTaskController.java */
    /* loaded from: classes2.dex */
    public class a implements ThreadFactory {
        public a(w0 w0Var) {
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setName("OS_PENDING_EXECUTOR_" + thread.getId());
            return thread;
        }
    }

    /* compiled from: OSTaskController.java */
    /* loaded from: classes2.dex */
    public static class b implements Runnable {
        public w0 a;
        public Runnable f0;
        public long g0;

        public b(w0 w0Var, Runnable runnable) {
            this.a = w0Var;
            this.f0 = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.f0.run();
            this.a.d(this.g0);
        }

        public String toString() {
            return "PendingTaskRunnable{innerTask=" + this.f0 + ", taskId=" + this.g0 + '}';
        }
    }

    public w0(yj2 yj2Var) {
        this.d = yj2Var;
    }

    public final void b(b bVar) {
        bVar.g0 = this.b.incrementAndGet();
        ExecutorService executorService = this.c;
        if (executorService == null) {
            yj2 yj2Var = this.d;
            yj2Var.debug("Adding a task to the pending queue with ID: " + bVar.g0);
            this.a.add(bVar);
        } else if (executorService.isShutdown()) {
        } else {
            yj2 yj2Var2 = this.d;
            yj2Var2.debug("Executor is still running, add to the executor with ID: " + bVar.g0);
            try {
                this.c.submit(bVar);
            } catch (RejectedExecutionException e) {
                yj2 yj2Var3 = this.d;
                yj2Var3.info("Executor is shutdown, running task manually with ID: " + bVar.g0);
                bVar.run();
                e.printStackTrace();
            }
        }
    }

    public void c(Runnable runnable) {
        b(new b(this, runnable));
    }

    public final void d(long j) {
        if (this.b.get() == j) {
            OneSignal.a(OneSignal.LOG_LEVEL.INFO, "Last Pending Task has ran, shutting down");
            this.c.shutdown();
        }
    }

    public boolean e() {
        if (Thread.currentThread().getName().contains("OS_PENDING_EXECUTOR_")) {
            return false;
        }
        if (OneSignal.P0() && this.c == null) {
            return false;
        }
        if (OneSignal.P0() || this.c != null) {
            return !this.c.isShutdown();
        }
        return true;
    }

    public void f() {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "startPendingTasks with task queue quantity: " + this.a.size());
        if (this.a.isEmpty()) {
            return;
        }
        this.c = Executors.newSingleThreadExecutor(new a(this));
        while (!this.a.isEmpty()) {
            this.c.submit(this.a.poll());
        }
    }
}
