package com.onesignal;

import android.content.Context;

/* compiled from: OSNotificationOpenAppSettings.kt */
/* loaded from: classes2.dex */
public final class k0 {
    public static final k0 a = new k0();

    public final boolean a(Context context) {
        fs1.f(context, "context");
        return !fs1.b("DISABLE", OSUtils.f(context, "com.onesignal.NotificationOpened.DEFAULT"));
    }

    public final boolean b(Context context) {
        fs1.f(context, "context");
        return OSUtils.g(context, "com.onesignal.suppressLaunchURLs");
    }
}
