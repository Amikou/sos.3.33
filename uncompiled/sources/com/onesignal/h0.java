package com.onesignal;

import android.content.Context;
import com.onesignal.OneSignal;
import org.json.JSONObject;

/* compiled from: OSNotificationController.java */
/* loaded from: classes2.dex */
public class h0 {
    public final zj2 a;
    public boolean b;
    public boolean c;

    public h0(zj2 zj2Var, boolean z, boolean z2) {
        this.b = z;
        this.c = z2;
        this.a = zj2Var;
    }

    public static void h(Context context) {
        String f = OSUtils.f(context, "com.onesignal.NotificationServiceExtension");
        if (f == null) {
            OneSignal.d1(OneSignal.LOG_LEVEL.VERBOSE, "No class found, not setting up OSRemoteNotificationReceivedHandler");
            return;
        }
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.VERBOSE;
        OneSignal.d1(log_level, "Found class: " + f + ", attempting to call constructor");
        try {
            Object newInstance = Class.forName(f).newInstance();
            if ((newInstance instanceof OneSignal.d0) && OneSignal.p == null) {
                OneSignal.F1((OneSignal.d0) newInstance);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InstantiationException e3) {
            e3.printStackTrace();
        }
    }

    public final zj2 a(Context context, g0 g0Var, JSONObject jSONObject, Long l) {
        zj2 zj2Var = new zj2(context);
        zj2Var.q(jSONObject);
        zj2Var.z(l);
        zj2Var.y(this.b);
        zj2Var.r(g0Var);
        return zj2Var;
    }

    public zj2 b() {
        return this.a;
    }

    public m0 c() {
        return new m0(this, this.a.f());
    }

    public boolean d() {
        if (OneSignal.k0().l()) {
            return this.a.f().h() + ((long) this.a.f().l()) > OneSignal.w0().a() / 1000;
        }
        return true;
    }

    public final void e(g0 g0Var) {
        this.a.r(g0Var);
        if (this.b) {
            k.e(this.a);
            return;
        }
        this.a.p(false);
        k.n(this.a, true, false);
        OneSignal.F0(this.a);
    }

    public void f(g0 g0Var, g0 g0Var2) {
        if (g0Var2 != null) {
            boolean I = OSUtils.I(g0Var2.e());
            boolean d = d();
            if (I && d) {
                this.a.r(g0Var2);
                k.l(this, this.c);
            } else {
                e(g0Var);
            }
            if (this.b) {
                OSUtils.V(100);
                return;
            }
            return;
        }
        e(g0Var);
    }

    public void g(boolean z) {
        this.c = z;
    }

    public String toString() {
        return "OSNotificationController{notificationJob=" + this.a + ", isRestoring=" + this.b + ", isBackgroundLogic=" + this.c + '}';
    }

    public h0(Context context, g0 g0Var, JSONObject jSONObject, boolean z, boolean z2, Long l) {
        this.b = z;
        this.c = z2;
        this.a = a(context, g0Var, jSONObject, l);
    }
}
