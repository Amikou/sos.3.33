package com.onesignal;

import android.content.Context;
import com.amazon.device.iap.PurchasingListener;
import com.amazon.device.iap.PurchasingService;
import com.onesignal.OneSignal;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/* compiled from: TrackAmazonPurchase.java */
/* loaded from: classes2.dex */
public class l1 {
    public Context a;
    public boolean b;
    public b c;
    public boolean d;
    public Object e;
    public Field f;

    /* compiled from: TrackAmazonPurchase.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            PurchasingService.registerListener(l1.this.a, l1.this.c);
        }
    }

    /* compiled from: TrackAmazonPurchase.java */
    /* loaded from: classes2.dex */
    public class b implements PurchasingListener {
        public PurchasingListener a;

        public b(l1 l1Var) {
        }

        public /* synthetic */ b(l1 l1Var, a aVar) {
            this(l1Var);
        }
    }

    public l1(Context context) {
        this.b = false;
        this.d = false;
        this.a = context;
        try {
            Class<?> cls = Class.forName("com.amazon.device.iap.internal.d");
            try {
                this.e = cls.getMethod("d", new Class[0]).invoke(null, new Object[0]);
            } catch (NullPointerException unused) {
                this.e = cls.getMethod("e", new Class[0]).invoke(null, new Object[0]);
                this.d = true;
            }
            Field declaredField = cls.getDeclaredField("f");
            this.f = declaredField;
            declaredField.setAccessible(true);
            this.c = new b(this, null);
            PurchasingListener purchasingListener = (PurchasingListener) this.f.get(this.e);
            this.b = true;
            e();
        } catch (ClassCastException e) {
            d(e);
        } catch (ClassNotFoundException e2) {
            d(e2);
        } catch (IllegalAccessException e3) {
            d(e3);
        } catch (NoSuchFieldException e4) {
            d(e4);
        } catch (NoSuchMethodException e5) {
            d(e5);
        } catch (InvocationTargetException e6) {
            d(e6);
        }
    }

    public static void d(Exception exc) {
        OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error adding Amazon IAP listener.", exc);
        exc.printStackTrace();
    }

    public void c() {
        if (this.b) {
            try {
                PurchasingListener purchasingListener = (PurchasingListener) this.f.get(this.e);
                b bVar = this.c;
                if (purchasingListener != bVar) {
                    bVar.a = purchasingListener;
                    e();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public final void e() {
        if (this.d) {
            OSUtils.S(new a());
        } else {
            PurchasingService.registerListener(this.a, this.c);
        }
    }
}
