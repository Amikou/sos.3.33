package com.onesignal;

import android.content.Context;
import android.util.Base64;
import com.google.firebase.messaging.FirebaseMessaging;
import com.onesignal.OneSignal;
import defpackage.y51;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutionException;

/* compiled from: PushRegistratorFCM.java */
/* loaded from: classes2.dex */
public class i1 extends h1 {
    public c51 f;
    public final Context g;
    public final a h;

    /* compiled from: PushRegistratorFCM.java */
    /* loaded from: classes2.dex */
    public static class a {
        public final String a;
        public final String b;
        public final String c;

        public a() {
            this(null, null, null);
        }

        public a(String str, String str2, String str3) {
            this.a = str == null ? "onesignal-shared-public" : str;
            this.b = str2 == null ? "1:754795614042:android:c682b8144a8dd52bc1ad63" : str2;
            this.c = str3 == null ? new String(Base64.decode("QUl6YVN5QW5UTG41LV80TWMyYTJQLWRLVWVFLWFCdGd5Q3JqbFlV", 0)) : str3;
        }
    }

    public i1(Context context, a aVar) {
        this.g = context;
        if (aVar == null) {
            this.h = new a();
        } else {
            this.h = aVar;
        }
    }

    @Override // com.onesignal.h1
    public String f() {
        return "FCM";
    }

    @Override // com.onesignal.h1
    public String g(String str) throws Exception {
        n(str);
        try {
            return m();
        } catch (NoClassDefFoundError | NoSuchMethodError unused) {
            OneSignal.a(OneSignal.LOG_LEVEL.INFO, "FirebaseMessaging.getToken not found, attempting to use FirebaseInstanceId.getToken");
            return l(str);
        }
    }

    @Deprecated
    public final String l(String str) throws IOException {
        try {
            Object invoke = Class.forName("com.google.firebase.iid.FirebaseInstanceId").getMethod("getInstance", c51.class).invoke(null, this.f);
            return (String) invoke.getClass().getMethod("getToken", String.class, String.class).invoke(invoke, str, "FCM");
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new Error("Reflection error on FirebaseInstanceId.getInstance(firebaseApp).getToken(senderId, FirebaseMessaging.INSTANCE_ID_SCOPE)", e);
        }
    }

    public final String m() throws Exception {
        com.google.android.gms.tasks.c<String> i = ((FirebaseMessaging) this.f.g(FirebaseMessaging.class)).i();
        try {
            return (String) com.google.android.gms.tasks.d.a(i);
        } catch (ExecutionException unused) {
            throw i.k();
        }
    }

    public final void n(String str) {
        if (this.f != null) {
            return;
        }
        this.f = c51.p(this.g, new y51.b().d(str).c(this.h.b).b(this.h.c).e(this.h.a).a(), "ONESIGNAL_SDK_FCM_APP_NAME");
    }
}
