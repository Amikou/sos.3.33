package com.onesignal.influence.domain;

/* compiled from: OSInfluenceType.kt */
/* loaded from: classes2.dex */
public enum OSInfluenceType {
    DIRECT,
    INDIRECT,
    UNATTRIBUTED,
    DISABLED;
    
    public static final a Companion = new a(null);

    /* compiled from: OSInfluenceType.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public final OSInfluenceType a(String str) {
            OSInfluenceType oSInfluenceType;
            if (str != null) {
                OSInfluenceType[] values = OSInfluenceType.values();
                int length = values.length;
                while (true) {
                    length--;
                    if (length < 0) {
                        oSInfluenceType = null;
                        break;
                    }
                    oSInfluenceType = values[length];
                    if (dv3.t(oSInfluenceType.name(), str, true)) {
                        break;
                    }
                }
                if (oSInfluenceType != null) {
                    return oSInfluenceType;
                }
            }
            return OSInfluenceType.UNATTRIBUTED;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public static final OSInfluenceType fromString(String str) {
        return Companion.a(str);
    }

    public final boolean isAttributed() {
        return isDirect() || isIndirect();
    }

    public final boolean isDirect() {
        return this == DIRECT;
    }

    public final boolean isDisabled() {
        return this == DISABLED;
    }

    public final boolean isIndirect() {
        return this == INDIRECT;
    }

    public final boolean isUnattributed() {
        return this == UNATTRIBUTED;
    }
}
