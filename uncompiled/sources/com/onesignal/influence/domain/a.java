package com.onesignal.influence.domain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInfluence.kt */
/* loaded from: classes2.dex */
public final class a {
    public OSInfluenceType a;
    public OSInfluenceChannel b;
    public JSONArray c;

    public a(String str) throws JSONException {
        fs1.f(str, "jsonString");
        JSONObject jSONObject = new JSONObject(str);
        String string = jSONObject.getString("influence_channel");
        String string2 = jSONObject.getString("influence_type");
        String string3 = jSONObject.getString("influence_ids");
        this.b = OSInfluenceChannel.Companion.a(string);
        this.a = OSInfluenceType.Companion.a(string2);
        fs1.e(string3, "ids");
        this.c = string3.length() == 0 ? null : new JSONArray(string3);
    }

    public final a a() {
        return new a(this.b, this.a, this.c);
    }

    public final JSONArray b() {
        return this.c;
    }

    public final OSInfluenceChannel c() {
        return this.b;
    }

    public final OSInfluenceType d() {
        return this.a;
    }

    public final void e(JSONArray jSONArray) {
        this.c = jSONArray;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || (!fs1.b(a.class, obj.getClass()))) {
            return false;
        }
        a aVar = (a) obj;
        return this.b == aVar.b && this.a == aVar.a;
    }

    public final void f(OSInfluenceType oSInfluenceType) {
        fs1.f(oSInfluenceType, "<set-?>");
        this.a = oSInfluenceType;
    }

    public final String g() throws JSONException {
        JSONObject put = new JSONObject().put("influence_channel", this.b.toString()).put("influence_type", this.a.toString());
        JSONArray jSONArray = this.c;
        String jSONObject = put.put("influence_ids", jSONArray != null ? String.valueOf(jSONArray) : "").toString();
        fs1.e(jSONObject, "JSONObject()\n        .pu…e \"\")\n        .toString()");
        return jSONObject;
    }

    public int hashCode() {
        return (this.b.hashCode() * 31) + this.a.hashCode();
    }

    public String toString() {
        return "SessionInfluence{influenceChannel=" + this.b + ", influenceType=" + this.a + ", ids=" + this.c + '}';
    }

    public a(OSInfluenceChannel oSInfluenceChannel, OSInfluenceType oSInfluenceType, JSONArray jSONArray) {
        fs1.f(oSInfluenceChannel, "influenceChannel");
        fs1.f(oSInfluenceType, "influenceType");
        this.b = oSInfluenceChannel;
        this.a = oSInfluenceType;
        this.c = jSONArray;
    }
}
