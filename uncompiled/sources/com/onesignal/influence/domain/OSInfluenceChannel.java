package com.onesignal.influence.domain;

/* compiled from: OSInfluenceChannel.kt */
/* loaded from: classes2.dex */
public enum OSInfluenceChannel {
    IAM("iam"),
    NOTIFICATION("notification");
    
    public static final a Companion = new a(null);
    private final String nameValue;

    /* compiled from: OSInfluenceChannel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public final OSInfluenceChannel a(String str) {
            OSInfluenceChannel oSInfluenceChannel;
            if (str != null) {
                OSInfluenceChannel[] values = OSInfluenceChannel.values();
                int length = values.length;
                while (true) {
                    length--;
                    if (length < 0) {
                        oSInfluenceChannel = null;
                        break;
                    }
                    oSInfluenceChannel = values[length];
                    if (oSInfluenceChannel.equalsName(str)) {
                        break;
                    }
                }
                if (oSInfluenceChannel != null) {
                    return oSInfluenceChannel;
                }
            }
            return OSInfluenceChannel.NOTIFICATION;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    OSInfluenceChannel(String str) {
        this.nameValue = str;
    }

    public static final OSInfluenceChannel fromString(String str) {
        return Companion.a(str);
    }

    public final boolean equalsName(String str) {
        fs1.f(str, "otherName");
        return fs1.b(this.nameValue, str);
    }

    @Override // java.lang.Enum
    public String toString() {
        return this.nameValue;
    }
}
