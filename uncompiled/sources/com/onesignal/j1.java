package com.onesignal;

import android.content.Context;
import android.text.TextUtils;
import com.huawei.agconnect.config.AGConnectServicesConfig;
import com.huawei.hms.aaid.HmsInstanceId;
import com.huawei.hms.common.ApiException;
import com.onesignal.OneSignal;
import com.onesignal.f1;

/* compiled from: PushRegistratorHMS.java */
/* loaded from: classes2.dex */
public class j1 implements f1 {
    public static boolean a;

    /* compiled from: PushRegistratorHMS.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ Context a;
        public final /* synthetic */ f1.a f0;

        public a(Context context, f1.a aVar) {
            this.a = context;
            this.f0 = aVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                j1.this.d(this.a, this.f0);
            } catch (ApiException e) {
                OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "HMS ApiException getting Huawei push token!", e);
                this.f0.a(null, e.getStatusCode() == 907135000 ? -26 : -27);
            }
        }
    }

    public static void c() {
        try {
            Thread.sleep(30000L);
        } catch (InterruptedException unused) {
        }
    }

    @Override // com.onesignal.f1
    public void a(Context context, String str, f1.a aVar) {
        new Thread(new a(context, aVar), "OS_HMS_GET_TOKEN").start();
    }

    public final synchronized void d(Context context, f1.a aVar) throws ApiException {
        if (!OSUtils.p()) {
            aVar.a(null, -28);
            return;
        }
        String token = HmsInstanceId.getInstance(context).getToken(AGConnectServicesConfig.fromContext(context).getString("client/app_id"), "HCM");
        if (!TextUtils.isEmpty(token)) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
            OneSignal.a(log_level, "Device registered for HMS, push token = " + token);
            aVar.a(token, 1);
        } else {
            e(aVar);
        }
    }

    public final void e(f1.a aVar) {
        c();
        if (a) {
            return;
        }
        OneSignal.a(OneSignal.LOG_LEVEL.ERROR, "HmsMessageServiceOneSignal.onNewToken timed out.");
        aVar.a(null, -25);
    }
}
