package com.onesignal;

import com.onesignal.OSTrigger;
import com.onesignal.OneSignal;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

/* compiled from: OSDynamicTriggerController.java */
/* loaded from: classes2.dex */
public class t {
    public static Date c = new Date();
    public final c a;
    public final ArrayList<String> b = new ArrayList<>();

    /* compiled from: OSDynamicTriggerController.java */
    /* loaded from: classes2.dex */
    public class a extends TimerTask {
        public final /* synthetic */ String a;

        public a(String str) {
            this.a = str;
        }

        @Override // java.util.TimerTask, java.lang.Runnable
        public void run() {
            t.this.b.remove(this.a);
            t.this.a.a();
        }
    }

    /* compiled from: OSDynamicTriggerController.java */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class b {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[OSTrigger.OSTriggerOperator.values().length];
            b = iArr;
            try {
                iArr[OSTrigger.OSTriggerOperator.LESS_THAN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[OSTrigger.OSTriggerOperator.LESS_THAN_OR_EQUAL_TO.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[OSTrigger.OSTriggerOperator.GREATER_THAN.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[OSTrigger.OSTriggerOperator.GREATER_THAN_OR_EQUAL_TO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[OSTrigger.OSTriggerOperator.EQUAL_TO.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[OSTrigger.OSTriggerOperator.NOT_EQUAL_TO.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[OSTrigger.OSTriggerKind.values().length];
            a = iArr2;
            try {
                iArr2[OSTrigger.OSTriggerKind.SESSION_TIME.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[OSTrigger.OSTriggerKind.TIME_SINCE_LAST_IN_APP.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    /* compiled from: OSDynamicTriggerController.java */
    /* loaded from: classes2.dex */
    public interface c {
        void a();

        void b(String str);
    }

    public t(c cVar) {
        this.a = cVar;
    }

    public static boolean d(double d, double d2, OSTrigger.OSTriggerOperator oSTriggerOperator) {
        switch (b.b[oSTriggerOperator.ordinal()]) {
            case 1:
                return d2 < d;
            case 2:
                return d2 <= d || f(d, d2);
            case 3:
                return d2 >= d;
            case 4:
                return d2 >= d || f(d, d2);
            case 5:
                return f(d, d2);
            case 6:
                return !f(d, d2);
            default:
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.d1(log_level, "Attempted to apply an invalid operator on a time-based in-app-message trigger: " + oSTriggerOperator.toString());
                return false;
        }
    }

    public static void e() {
        c = new Date();
    }

    public static boolean f(double d, double d2) {
        return Math.abs(d - d2) < 0.3d;
    }

    /* JADX WARN: Removed duplicated region for block: B:28:0x0076 A[Catch: all -> 0x009d, TryCatch #0 {, blocks: (B:7:0x0009, B:9:0x000f, B:11:0x0011, B:26:0x005b, B:28:0x0076, B:29:0x007b, B:31:0x007d, B:33:0x0082, B:35:0x0084, B:37:0x008c, B:39:0x008e, B:40:0x009b, B:16:0x0025, B:18:0x002f, B:20:0x0031, B:23:0x003d, B:25:0x005a, B:24:0x004b), top: B:45:0x0009 }] */
    /* JADX WARN: Removed duplicated region for block: B:31:0x007d A[Catch: all -> 0x009d, TryCatch #0 {, blocks: (B:7:0x0009, B:9:0x000f, B:11:0x0011, B:26:0x005b, B:28:0x0076, B:29:0x007b, B:31:0x007d, B:33:0x0082, B:35:0x0084, B:37:0x008c, B:39:0x008e, B:40:0x009b, B:16:0x0025, B:18:0x002f, B:20:0x0031, B:23:0x003d, B:25:0x005a, B:24:0x004b), top: B:45:0x0009 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean c(com.onesignal.OSTrigger r15) {
        /*
            r14 = this;
            java.lang.Object r0 = r15.e
            r1 = 0
            if (r0 != 0) goto L6
            return r1
        L6:
            java.util.ArrayList<java.lang.String> r0 = r14.b
            monitor-enter(r0)
            java.lang.Object r2 = r15.e     // Catch: java.lang.Throwable -> L9d
            boolean r2 = r2 instanceof java.lang.Number     // Catch: java.lang.Throwable -> L9d
            if (r2 != 0) goto L11
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9d
            return r1
        L11:
            int[] r2 = com.onesignal.t.b.a     // Catch: java.lang.Throwable -> L9d
            com.onesignal.OSTrigger$OSTriggerKind r3 = r15.b     // Catch: java.lang.Throwable -> L9d
            int r3 = r3.ordinal()     // Catch: java.lang.Throwable -> L9d
            r2 = r2[r3]     // Catch: java.lang.Throwable -> L9d
            r3 = 1
            r4 = 0
            if (r2 == r3) goto L4b
            r6 = 2
            if (r2 == r6) goto L25
            r6 = r4
            goto L5b
        L25:
            com.onesignal.OSInAppMessageController r2 = com.onesignal.OneSignal.c0()     // Catch: java.lang.Throwable -> L9d
            boolean r2 = r2.U()     // Catch: java.lang.Throwable -> L9d
            if (r2 == 0) goto L31
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9d
            return r1
        L31:
            com.onesignal.OSInAppMessageController r2 = com.onesignal.OneSignal.c0()     // Catch: java.lang.Throwable -> L9d
            java.util.Date r2 = r2.t     // Catch: java.lang.Throwable -> L9d
            if (r2 != 0) goto L3d
            r6 = 999999(0xf423f, double:4.94065E-318)
            goto L5b
        L3d:
            java.util.Date r6 = new java.util.Date     // Catch: java.lang.Throwable -> L9d
            r6.<init>()     // Catch: java.lang.Throwable -> L9d
            long r6 = r6.getTime()     // Catch: java.lang.Throwable -> L9d
            long r8 = r2.getTime()     // Catch: java.lang.Throwable -> L9d
            goto L5a
        L4b:
            java.util.Date r2 = new java.util.Date     // Catch: java.lang.Throwable -> L9d
            r2.<init>()     // Catch: java.lang.Throwable -> L9d
            long r6 = r2.getTime()     // Catch: java.lang.Throwable -> L9d
            java.util.Date r2 = com.onesignal.t.c     // Catch: java.lang.Throwable -> L9d
            long r8 = r2.getTime()     // Catch: java.lang.Throwable -> L9d
        L5a:
            long r6 = r6 - r8
        L5b:
            java.lang.String r2 = r15.a     // Catch: java.lang.Throwable -> L9d
            java.lang.Object r8 = r15.e     // Catch: java.lang.Throwable -> L9d
            java.lang.Number r8 = (java.lang.Number) r8     // Catch: java.lang.Throwable -> L9d
            double r8 = r8.doubleValue()     // Catch: java.lang.Throwable -> L9d
            r10 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r8 = r8 * r10
            long r8 = (long) r8     // Catch: java.lang.Throwable -> L9d
            double r10 = (double) r8     // Catch: java.lang.Throwable -> L9d
            double r12 = (double) r6     // Catch: java.lang.Throwable -> L9d
            com.onesignal.OSTrigger$OSTriggerOperator r15 = r15.d     // Catch: java.lang.Throwable -> L9d
            boolean r15 = d(r10, r12, r15)     // Catch: java.lang.Throwable -> L9d
            if (r15 == 0) goto L7d
            com.onesignal.t$c r15 = r14.a     // Catch: java.lang.Throwable -> L9d
            r15.b(r2)     // Catch: java.lang.Throwable -> L9d
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9d
            return r3
        L7d:
            long r8 = r8 - r6
            int r15 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r15 > 0) goto L84
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9d
            return r1
        L84:
            java.util.ArrayList<java.lang.String> r15 = r14.b     // Catch: java.lang.Throwable -> L9d
            boolean r15 = r15.contains(r2)     // Catch: java.lang.Throwable -> L9d
            if (r15 == 0) goto L8e
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9d
            return r1
        L8e:
            com.onesignal.t$a r15 = new com.onesignal.t$a     // Catch: java.lang.Throwable -> L9d
            r15.<init>(r2)     // Catch: java.lang.Throwable -> L9d
            com.onesignal.u.a(r15, r2, r8)     // Catch: java.lang.Throwable -> L9d
            java.util.ArrayList<java.lang.String> r15 = r14.b     // Catch: java.lang.Throwable -> L9d
            r15.add(r2)     // Catch: java.lang.Throwable -> L9d
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9d
            return r1
        L9d:
            r15 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9d
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.t.c(com.onesignal.OSTrigger):boolean");
    }
}
