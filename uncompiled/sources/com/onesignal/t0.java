package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.influence.domain.OSInfluenceType;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: OSSessionManager.java */
/* loaded from: classes2.dex */
public class t0 {
    public cl2 a;
    public b b;
    public yj2 c;

    /* compiled from: OSSessionManager.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ List a;

        public a(List list) {
            this.a = list;
        }

        @Override // java.lang.Runnable
        public void run() {
            Thread.currentThread().setPriority(10);
            t0.this.b.a(this.a);
        }
    }

    /* compiled from: OSSessionManager.java */
    /* loaded from: classes2.dex */
    public interface b {
        void a(List<com.onesignal.influence.domain.a> list);
    }

    public t0(b bVar, cl2 cl2Var, yj2 yj2Var) {
        this.b = bVar;
        this.a = cl2Var;
        this.c = yj2Var;
    }

    public void b(JSONObject jSONObject, List<com.onesignal.influence.domain.a> list) {
        yj2 yj2Var = this.c;
        yj2Var.debug("OneSignal SessionManager addSessionData with influences: " + list.toString());
        this.a.a(jSONObject, list);
        yj2 yj2Var2 = this.c;
        yj2Var2.debug("OneSignal SessionManager addSessionIds on jsonObject: " + jSONObject);
    }

    public void c(OneSignal.AppEntryAction appEntryAction) {
        d(appEntryAction, null);
    }

    public final void d(OneSignal.AppEntryAction appEntryAction, String str) {
        boolean z;
        com.onesignal.influence.domain.a aVar;
        this.c.debug("OneSignal SessionManager attemptSessionUpgrade with entryAction: " + appEntryAction);
        mj2 b2 = this.a.b(appEntryAction);
        List<mj2> d = this.a.d(appEntryAction);
        ArrayList arrayList = new ArrayList();
        if (b2 != null) {
            aVar = b2.e();
            OSInfluenceType oSInfluenceType = OSInfluenceType.DIRECT;
            if (str == null) {
                str = b2.g();
            }
            z = o(b2, oSInfluenceType, str, null);
        } else {
            z = false;
            aVar = null;
        }
        if (z) {
            this.c.debug("OneSignal SessionManager attemptSessionUpgrade channel updated, search for ending direct influences on channels: " + d);
            arrayList.add(aVar);
            for (mj2 mj2Var : d) {
                if (mj2Var.k().isDirect()) {
                    arrayList.add(mj2Var.e());
                    mj2Var.t();
                }
            }
        }
        this.c.debug("OneSignal SessionManager attemptSessionUpgrade try UNATTRIBUTED to INDIRECT upgrade");
        for (mj2 mj2Var2 : d) {
            if (mj2Var2.k().isUnattributed()) {
                JSONArray n = mj2Var2.n();
                if (n.length() > 0 && !appEntryAction.isAppClose()) {
                    com.onesignal.influence.domain.a e = mj2Var2.e();
                    if (o(mj2Var2, OSInfluenceType.INDIRECT, null, n)) {
                        arrayList.add(e);
                    }
                }
            }
        }
        OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "Trackers after update attempt: " + this.a.c().toString());
        n(arrayList);
    }

    public List<com.onesignal.influence.domain.a> e() {
        return this.a.f();
    }

    public List<com.onesignal.influence.domain.a> f() {
        return this.a.h();
    }

    public void g() {
        this.a.i();
    }

    public void h(String str) {
        yj2 yj2Var = this.c;
        yj2Var.debug("OneSignal SessionManager onDirectInfluenceFromIAMClick messageId: " + str);
        o(this.a.e(), OSInfluenceType.DIRECT, str, null);
    }

    public void i() {
        this.c.debug("OneSignal SessionManager onDirectInfluenceFromIAMClickFinished");
        this.a.e().t();
    }

    public void j(OneSignal.AppEntryAction appEntryAction, String str) {
        yj2 yj2Var = this.c;
        yj2Var.debug("OneSignal SessionManager onDirectInfluenceFromNotificationOpen notificationId: " + str);
        if (str == null || str.isEmpty()) {
            return;
        }
        d(appEntryAction, str);
    }

    public void k(String str) {
        yj2 yj2Var = this.c;
        yj2Var.debug("OneSignal SessionManager onInAppMessageReceived messageId: " + str);
        mj2 e = this.a.e();
        e.v(str);
        e.t();
    }

    public void l(String str) {
        yj2 yj2Var = this.c;
        yj2Var.debug("OneSignal SessionManager onNotificationReceived notificationId: " + str);
        if (str == null || str.isEmpty()) {
            return;
        }
        this.a.g().v(str);
    }

    public void m(OneSignal.AppEntryAction appEntryAction) {
        boolean o;
        List<mj2> d = this.a.d(appEntryAction);
        ArrayList arrayList = new ArrayList();
        yj2 yj2Var = this.c;
        yj2Var.debug("OneSignal SessionManager restartSessionIfNeeded with entryAction: " + appEntryAction + "\n channelTrackers: " + d.toString());
        for (mj2 mj2Var : d) {
            JSONArray n = mj2Var.n();
            yj2 yj2Var2 = this.c;
            yj2Var2.debug("OneSignal SessionManager restartSessionIfNeeded lastIds: " + n);
            com.onesignal.influence.domain.a e = mj2Var.e();
            if (n.length() > 0) {
                o = o(mj2Var, OSInfluenceType.INDIRECT, null, n);
            } else {
                o = o(mj2Var, OSInfluenceType.UNATTRIBUTED, null, null);
            }
            if (o) {
                arrayList.add(e);
            }
        }
        n(arrayList);
    }

    public final void n(List<com.onesignal.influence.domain.a> list) {
        yj2 yj2Var = this.c;
        yj2Var.debug("OneSignal SessionManager sendSessionEndingWithInfluences with influences: " + list);
        if (list.size() > 0) {
            new Thread(new a(list), "OS_END_CURRENT_SESSION").start();
        }
    }

    public final boolean o(mj2 mj2Var, OSInfluenceType oSInfluenceType, String str, JSONArray jSONArray) {
        if (p(mj2Var, oSInfluenceType, str, jSONArray)) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "OSChannelTracker changed: " + mj2Var.h() + "\nfrom:\ninfluenceType: " + mj2Var.k() + ", directNotificationId: " + mj2Var.g() + ", indirectNotificationIds: " + mj2Var.j() + "\nto:\ninfluenceType: " + oSInfluenceType + ", directNotificationId: " + str + ", indirectNotificationIds: " + jSONArray);
            mj2Var.y(oSInfluenceType);
            mj2Var.w(str);
            mj2Var.x(jSONArray);
            mj2Var.b();
            StringBuilder sb = new StringBuilder();
            sb.append("Trackers changed to: ");
            sb.append(this.a.c().toString());
            OneSignal.a(log_level, sb.toString());
            return true;
        }
        return false;
    }

    public final boolean p(mj2 mj2Var, OSInfluenceType oSInfluenceType, String str, JSONArray jSONArray) {
        if (oSInfluenceType.equals(mj2Var.k())) {
            OSInfluenceType k = mj2Var.k();
            if (!k.isDirect() || mj2Var.g() == null || mj2Var.g().equals(str)) {
                return k.isIndirect() && mj2Var.j() != null && mj2Var.j().length() > 0 && !j.a(mj2Var.j(), jSONArray);
            }
            return true;
        }
        return true;
    }
}
