package com.onesignal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import com.onesignal.OneSignal;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes2.dex */
public class LocationController {
    public static boolean c;
    public static c e;
    public static Thread f;
    public static Context g;
    public static Location h;
    public static String i;
    public static final List<e> a = new ArrayList();
    public static ConcurrentHashMap<PermissionType, b> b = new ConcurrentHashMap<>();
    public static final Object d = new a();

    /* loaded from: classes2.dex */
    public enum PermissionType {
        STARTUP,
        PROMPT_LOCATION,
        SYNC_SERVICE
    }

    /* loaded from: classes2.dex */
    public class a {
    }

    /* loaded from: classes2.dex */
    public interface b {
        void a(d dVar);

        PermissionType getType();
    }

    /* loaded from: classes2.dex */
    public static class c extends HandlerThread {
        public Handler a;

        public c() {
            super("OSH_LocationHandlerThread");
            start();
            this.a = new Handler(getLooper());
        }
    }

    /* loaded from: classes2.dex */
    public static class d {
        public Double a;
        public Double b;
        public Float c;
        public Integer d;
        public Boolean e;
        public Long f;

        public String toString() {
            return "LocationPoint{lat=" + this.a + ", log=" + this.b + ", accuracy=" + this.c + ", type=" + this.d + ", bg=" + this.e + ", timeStamp=" + this.f + '}';
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class e implements b {
        public void b(OneSignal.PromptActionResult promptActionResult) {
        }
    }

    public static void a(b bVar) {
        if (bVar instanceof e) {
            List<e> list = a;
            synchronized (list) {
                list.add((e) bVar);
            }
        }
    }

    public static void b(Context context, boolean z, boolean z2) {
        try {
            if (Arrays.asList(context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions).contains("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                i = "android.permission.ACCESS_BACKGROUND_LOCATION";
            }
            if (i != null && z) {
                i12.a.d(z2, i);
                return;
            }
            n(z, OneSignal.PromptActionResult.PERMISSION_GRANTED);
            p();
        } catch (PackageManager.NameNotFoundException e2) {
            n(z, OneSignal.PromptActionResult.ERROR);
            e2.printStackTrace();
        }
    }

    public static void c(d dVar) {
        Thread thread;
        HashMap hashMap = new HashMap();
        synchronized (LocationController.class) {
            hashMap.putAll(b);
            b.clear();
            thread = f;
        }
        for (PermissionType permissionType : hashMap.keySet()) {
            ((b) hashMap.get(permissionType)).a(dVar);
        }
        if (thread != null && !Thread.currentThread().equals(thread)) {
            thread.interrupt();
        }
        if (thread == f) {
            synchronized (LocationController.class) {
                if (thread == f) {
                    f = null;
                }
            }
        }
        o(OneSignal.w0().a());
    }

    public static void d(Location location) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "LocationController fireCompleteForLocation with location: " + location);
        d dVar = new d();
        dVar.c = Float.valueOf(location.getAccuracy());
        dVar.e = Boolean.valueOf(OneSignal.O0() ^ true);
        dVar.d = Integer.valueOf(!c ? 1 : 0);
        dVar.f = Long.valueOf(location.getTime());
        if (c) {
            dVar.a = Double.valueOf(new BigDecimal(location.getLatitude()).setScale(7, RoundingMode.HALF_UP).doubleValue());
            dVar.b = Double.valueOf(new BigDecimal(location.getLongitude()).setScale(7, RoundingMode.HALF_UP).doubleValue());
        } else {
            dVar.a = Double.valueOf(location.getLatitude());
            dVar.b = Double.valueOf(location.getLongitude());
        }
        c(dVar);
        m(g);
    }

    public static void e() {
        synchronized (d) {
            if (j()) {
                com.onesignal.d.e();
            } else if (k()) {
                h.e();
            }
        }
        c(null);
    }

    public static long f() {
        return b1.d(b1.a, "OS_LAST_LOCATION_TIME", -600000L);
    }

    public static void g(Context context, boolean z, boolean z2, b bVar) {
        int i2;
        a(bVar);
        g = context;
        b.put(bVar.getType(), bVar);
        if (!OneSignal.Q0()) {
            n(z, OneSignal.PromptActionResult.ERROR);
            e();
            return;
        }
        int a2 = ed.a(context, "android.permission.ACCESS_FINE_LOCATION");
        if (a2 == -1) {
            i2 = ed.a(context, "android.permission.ACCESS_COARSE_LOCATION");
            c = true;
        } else {
            i2 = -1;
        }
        int i3 = Build.VERSION.SDK_INT;
        int a3 = i3 >= 29 ? ed.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") : -1;
        if (i3 < 23) {
            if (a2 != 0 && i2 != 0) {
                n(z, OneSignal.PromptActionResult.LOCATION_PERMISSIONS_MISSING_MANIFEST);
                bVar.a(null);
                return;
            }
            n(z, OneSignal.PromptActionResult.PERMISSION_GRANTED);
            p();
        } else if (a2 == 0) {
            if (i3 >= 29 && a3 != 0) {
                b(context, z, z2);
                return;
            }
            n(z, OneSignal.PromptActionResult.PERMISSION_GRANTED);
            p();
        } else {
            try {
                List asList = Arrays.asList(context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions);
                OneSignal.PromptActionResult promptActionResult = OneSignal.PromptActionResult.PERMISSION_DENIED;
                if (asList.contains("android.permission.ACCESS_FINE_LOCATION")) {
                    i = "android.permission.ACCESS_FINE_LOCATION";
                } else if (!asList.contains("android.permission.ACCESS_COARSE_LOCATION")) {
                    OneSignal.d1(OneSignal.LOG_LEVEL.INFO, "Location permissions not added on AndroidManifest file");
                    promptActionResult = OneSignal.PromptActionResult.LOCATION_PERMISSIONS_MISSING_MANIFEST;
                } else if (i2 != 0) {
                    i = "android.permission.ACCESS_COARSE_LOCATION";
                } else if (i3 >= 29 && asList.contains("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    i = "android.permission.ACCESS_BACKGROUND_LOCATION";
                }
                if (i != null && z) {
                    i12.a.d(z2, i);
                } else if (i2 == 0) {
                    n(z, OneSignal.PromptActionResult.PERMISSION_GRANTED);
                    p();
                } else {
                    n(z, promptActionResult);
                    e();
                }
            } catch (PackageManager.NameNotFoundException e2) {
                n(z, OneSignal.PromptActionResult.ERROR);
                e2.printStackTrace();
            }
        }
    }

    public static c h() {
        if (e == null) {
            synchronized (d) {
                if (e == null) {
                    e = new c();
                }
            }
        }
        return e;
    }

    public static boolean i(Context context) {
        return ed.a(context, "android.permission.ACCESS_FINE_LOCATION") == 0 || ed.a(context, "android.permission.ACCESS_COARSE_LOCATION") == 0;
    }

    public static boolean j() {
        return OSUtils.B() && OSUtils.s();
    }

    public static boolean k() {
        return OSUtils.G() && OSUtils.v();
    }

    public static void l() {
        synchronized (d) {
            if (j()) {
                com.onesignal.d.l();
                return;
            }
            if (k()) {
                h.l();
            }
        }
    }

    public static boolean m(Context context) {
        if (!i(context)) {
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "LocationController scheduleUpdate not possible, location permission not enabled");
            return false;
        } else if (!OneSignal.Q0()) {
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "LocationController scheduleUpdate not possible, location shared not enabled");
            return false;
        } else {
            long a2 = OneSignal.w0().a() - f();
            long j = (OneSignal.O0() ? 300L : 600L) * 1000;
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.d1(log_level, "LocationController scheduleUpdate lastTime: " + a2 + " minTime: " + j);
            u0.q().r(context, j - a2);
            return true;
        }
    }

    public static void n(boolean z, OneSignal.PromptActionResult promptActionResult) {
        if (!z) {
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "LocationController sendAndClearPromptHandlers from non prompt flow");
            return;
        }
        List<e> list = a;
        synchronized (list) {
            OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "LocationController calling prompt handlers");
            for (e eVar : list) {
                eVar.b(promptActionResult);
            }
            a.clear();
        }
    }

    public static void o(long j) {
        b1.l(b1.a, "OS_LAST_LOCATION_TIME", j);
    }

    public static void p() {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "LocationController startGetLocation with lastLocation: " + h);
        try {
            if (j()) {
                com.onesignal.d.p();
            } else if (k()) {
                h.p();
            } else {
                OneSignal.a(OneSignal.LOG_LEVEL.WARN, "LocationController startGetLocation not possible, no location dependency found");
                e();
            }
        } catch (Throwable th) {
            OneSignal.b(OneSignal.LOG_LEVEL.WARN, "Location permission exists but there was an error initializing: ", th);
            e();
        }
    }
}
