package com.onesignal;

import com.onesignal.OneSignalStateSynchronizer;
import org.json.JSONObject;

/* compiled from: UserStateEmailSynchronizer.java */
/* loaded from: classes2.dex */
public class o1 extends r1 {
    public o1() {
        super(OneSignalStateSynchronizer.UserStateSynchronizerType.EMAIL);
    }

    @Override // com.onesignal.s1
    public String B() {
        return OneSignal.Y();
    }

    @Override // com.onesignal.s1
    public n1 O(String str, boolean z) {
        return new ag4(str, z);
    }

    @Override // com.onesignal.s1
    public void d0(String str) {
        OneSignal.Q1(str);
    }

    @Override // com.onesignal.r1
    public void f0() {
        OneSignal.H();
    }

    @Override // com.onesignal.r1
    public void g0(JSONObject jSONObject) {
        OneSignal.I();
    }

    @Override // com.onesignal.r1
    public String h0() {
        return "email_auth_hash";
    }

    @Override // com.onesignal.r1
    public String i0() {
        return "email";
    }

    @Override // com.onesignal.r1
    public int j0() {
        return 11;
    }

    public void l0(String str) {
        OneSignal.t1(str);
    }
}
