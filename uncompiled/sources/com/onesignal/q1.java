package com.onesignal;

import com.onesignal.OneSignalStateSynchronizer;
import org.json.JSONObject;

/* compiled from: UserStateSMSSynchronizer.java */
/* loaded from: classes2.dex */
public class q1 extends r1 {
    public q1() {
        super(OneSignalStateSynchronizer.UserStateSynchronizerType.SMS);
    }

    @Override // com.onesignal.s1
    public String B() {
        return OneSignal.m0();
    }

    @Override // com.onesignal.s1
    public n1 O(String str, boolean z) {
        return new cg4(str, z);
    }

    @Override // com.onesignal.s1
    public void d0(String str) {
        OneSignal.R1(str);
    }

    @Override // com.onesignal.r1
    public void f0() {
        OneSignal.L();
    }

    @Override // com.onesignal.r1
    public void g0(JSONObject jSONObject) {
        OneSignal.M(jSONObject);
    }

    @Override // com.onesignal.r1
    public String h0() {
        return "sms_auth_hash";
    }

    @Override // com.onesignal.r1
    public String i0() {
        return "sms_number";
    }

    @Override // com.onesignal.r1
    public int j0() {
        return 14;
    }

    public void l0(String str) {
        OneSignal.u1(str);
    }
}
