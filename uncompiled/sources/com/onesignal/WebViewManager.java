package com.onesignal;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import com.onesignal.OneSignal;
import com.onesignal.a;
import com.onesignal.i;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

@TargetApi(19)
/* loaded from: classes2.dex */
public class WebViewManager extends a.b {
    public static final String k = "com.onesignal.WebViewManager";
    public static final int l = z0.b(24);
    public static WebViewManager m = null;
    public OSWebView b;
    public com.onesignal.i c;
    public Activity d;
    public x e;
    public qj2 f;
    public final Object a = new b(this);
    public String g = null;
    public Integer h = null;
    public boolean i = false;
    public boolean j = false;

    /* loaded from: classes2.dex */
    public enum Position {
        TOP_BANNER,
        BOTTOM_BANNER,
        CENTER_MODAL,
        FULL_SCREEN;

        public boolean isBanner() {
            int i = a.a[ordinal()];
            return i == 1 || i == 2;
        }
    }

    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Position.values().length];
            a = iArr;
            try {
                iArr[Position.TOP_BANNER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Position.BOTTOM_BANNER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b {
        public b(WebViewManager webViewManager) {
        }
    }

    /* loaded from: classes2.dex */
    public class c implements l {
        public final /* synthetic */ Activity a;
        public final /* synthetic */ x b;
        public final /* synthetic */ qj2 c;

        public c(Activity activity, x xVar, qj2 qj2Var) {
            this.a = activity;
            this.b = xVar;
            this.c = qj2Var;
        }

        @Override // com.onesignal.WebViewManager.l
        public void a() {
            WebViewManager.m = null;
            WebViewManager.B(this.a, this.b, this.c);
        }
    }

    /* loaded from: classes2.dex */
    public class d implements Runnable {
        public final /* synthetic */ x a;
        public final /* synthetic */ qj2 f0;

        public d(x xVar, qj2 qj2Var) {
            this.a = xVar;
            this.f0 = qj2Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            WebViewManager.I(this.a, this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class e implements Runnable {
        public final /* synthetic */ Activity f0;
        public final /* synthetic */ String g0;
        public final /* synthetic */ qj2 h0;

        public e(Activity activity, String str, qj2 qj2Var) {
            this.f0 = activity;
            this.g0 = str;
            this.h0 = qj2Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                WebViewManager.this.H(this.f0, this.g0, this.h0.g());
            } catch (Exception e) {
                if (e.getMessage() != null && e.getMessage().contains("No WebView installed")) {
                    OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Error setting up WebView: ", e);
                    return;
                }
                throw e;
            }
        }
    }

    /* loaded from: classes2.dex */
    public class f implements Runnable {
        public f() {
        }

        @Override // java.lang.Runnable
        public void run() {
            int[] c = z0.c(WebViewManager.this.d);
            WebViewManager.this.b.evaluateJavascript(String.format("setSafeAreaInsets(%s)", String.format("{\n   top: %d,\n   bottom: %d,\n   right: %d,\n   left: %d,\n}", Integer.valueOf(c[0]), Integer.valueOf(c[1]), Integer.valueOf(c[2]), Integer.valueOf(c[3]))), null);
        }
    }

    /* loaded from: classes2.dex */
    public class g implements Runnable {

        /* loaded from: classes2.dex */
        public class a implements ValueCallback<String> {
            public a() {
            }

            @Override // android.webkit.ValueCallback
            /* renamed from: a */
            public void onReceiveValue(String str) {
                try {
                    WebViewManager webViewManager = WebViewManager.this;
                    WebViewManager.this.J(Integer.valueOf(webViewManager.C(webViewManager.d, new JSONObject(str))));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        public g() {
        }

        @Override // java.lang.Runnable
        public void run() {
            WebViewManager webViewManager = WebViewManager.this;
            webViewManager.G(webViewManager.d);
            if (WebViewManager.this.f.g()) {
                WebViewManager.this.K();
            }
            WebViewManager.this.b.evaluateJavascript("getPageMetaData()", new a());
        }
    }

    /* loaded from: classes2.dex */
    public class h implements Runnable {
        public final /* synthetic */ Activity a;
        public final /* synthetic */ String f0;

        public h(Activity activity, String str) {
            this.a = activity;
            this.f0 = str;
        }

        @Override // java.lang.Runnable
        public void run() {
            WebViewManager.this.G(this.a);
            WebViewManager.this.b.loadData(this.f0, "text/html; charset=utf-8", "base64");
        }
    }

    /* loaded from: classes2.dex */
    public class i implements i.j {
        public i() {
        }

        @Override // com.onesignal.i.j
        public void a() {
            OneSignal.c0().X(WebViewManager.this.e);
            WebViewManager.this.D();
        }

        @Override // com.onesignal.i.j
        public void b() {
            OneSignal.c0().d0(WebViewManager.this.e);
        }

        @Override // com.onesignal.i.j
        public void c() {
            OneSignal.c0().e0(WebViewManager.this.e);
        }
    }

    /* loaded from: classes2.dex */
    public class j implements l {
        public final /* synthetic */ l a;

        public j(l lVar) {
            this.a = lVar;
        }

        @Override // com.onesignal.WebViewManager.l
        public void a() {
            WebViewManager.this.i = false;
            WebViewManager.this.F(null);
            l lVar = this.a;
            if (lVar != null) {
                lVar.a();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class k {
        public k() {
        }

        public final Position a(JSONObject jSONObject) {
            Position position = Position.FULL_SCREEN;
            try {
                return (!jSONObject.has("displayLocation") || jSONObject.get("displayLocation").equals("")) ? position : Position.valueOf(jSONObject.optString("displayLocation", "FULL_SCREEN").toUpperCase());
            } catch (JSONException e) {
                e.printStackTrace();
                return position;
            }
        }

        public final boolean b(JSONObject jSONObject) {
            try {
                return jSONObject.getBoolean("dragToDismissDisabled");
            } catch (JSONException unused) {
                return false;
            }
        }

        public final int c(JSONObject jSONObject) {
            try {
                WebViewManager webViewManager = WebViewManager.this;
                return webViewManager.C(webViewManager.d, jSONObject.getJSONObject("pageMetaData"));
            } catch (JSONException unused) {
                return -1;
            }
        }

        public final void d(JSONObject jSONObject) throws JSONException {
            JSONObject jSONObject2 = jSONObject.getJSONObject("body");
            String optString = jSONObject2.optString("id", null);
            WebViewManager.this.j = jSONObject2.getBoolean("close");
            if (WebViewManager.this.e.k) {
                OneSignal.c0().a0(WebViewManager.this.e, jSONObject2);
            } else if (optString != null) {
                OneSignal.c0().Z(WebViewManager.this.e, jSONObject2);
            }
            if (WebViewManager.this.j) {
                WebViewManager.this.w(null);
            }
        }

        public final void e(JSONObject jSONObject) throws JSONException {
            OneSignal.c0().g0(WebViewManager.this.e, jSONObject);
        }

        public final void f(JSONObject jSONObject) {
            Position a = a(jSONObject);
            int c = a == Position.FULL_SCREEN ? -1 : c(jSONObject);
            boolean b = b(jSONObject);
            WebViewManager.this.f.i(a);
            WebViewManager.this.f.j(c);
            WebViewManager.this.v(b);
        }

        @JavascriptInterface
        public void postMessage(String str) {
            try {
                OneSignal.d1(OneSignal.LOG_LEVEL.DEBUG, "OSJavaScriptInterface:postMessage: " + str);
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString("type");
                char c = 65535;
                switch (string.hashCode()) {
                    case -1484226720:
                        if (string.equals("page_change")) {
                            c = 3;
                            break;
                        }
                        break;
                    case -934437708:
                        if (string.equals("resize")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 42998156:
                        if (string.equals("rendering_complete")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 1851145598:
                        if (string.equals("action_taken")) {
                            c = 1;
                            break;
                        }
                        break;
                }
                if (c == 0) {
                    f(jSONObject);
                } else if (c != 1) {
                    if (c != 3) {
                        return;
                    }
                    e(jSONObject);
                } else if (WebViewManager.this.c.O()) {
                } else {
                    d(jSONObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface l {
        void a();
    }

    public WebViewManager(x xVar, Activity activity, qj2 qj2Var) {
        this.e = xVar;
        this.d = activity;
        this.f = qj2Var;
    }

    public static void B(Activity activity, x xVar, qj2 qj2Var) {
        if (qj2Var.g()) {
            E(qj2Var, activity);
        }
        try {
            String encodeToString = Base64.encodeToString(qj2Var.a().getBytes("UTF-8"), 2);
            WebViewManager webViewManager = new WebViewManager(xVar, activity, qj2Var);
            m = webViewManager;
            OSUtils.S(new e(activity, encodeToString, qj2Var));
        } catch (UnsupportedEncodingException e2) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "Catch on initInAppMessage: ", e2);
            e2.printStackTrace();
        }
    }

    public static void E(qj2 qj2Var, Activity activity) {
        String a2 = qj2Var.a();
        int[] c2 = z0.c(activity);
        String format = String.format("\n\n<script>\n    setSafeAreaInsets(%s);\n</script>", String.format("{\n   top: %d,\n   bottom: %d,\n   right: %d,\n   left: %d,\n}", Integer.valueOf(c2[0]), Integer.valueOf(c2[1]), Integer.valueOf(c2[2]), Integer.valueOf(c2[3])));
        qj2Var.h(a2 + format);
    }

    public static void I(x xVar, qj2 qj2Var) {
        Activity Q = OneSignal.Q();
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, "in app message showMessageContent on currentActivity: " + Q);
        if (Q != null) {
            WebViewManager webViewManager = m;
            if (webViewManager != null && xVar.k) {
                webViewManager.w(new c(Q, xVar, qj2Var));
                return;
            } else {
                B(Q, xVar, qj2Var);
                return;
            }
        }
        Looper.prepare();
        new Handler().postDelayed(new d(xVar, qj2Var), 200L);
    }

    public static void x() {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.d1(log_level, "WebViewManager IAM dismissAndAwaitNextMessage lastInstance: " + m);
        WebViewManager webViewManager = m;
        if (webViewManager != null) {
            webViewManager.w(null);
        }
    }

    public static void y() {
        if (Build.VERSION.SDK_INT < 19 || !OneSignal.B(OneSignal.LOG_LEVEL.DEBUG)) {
            return;
        }
        WebView.setWebContentsDebuggingEnabled(true);
    }

    public final int A(Activity activity) {
        return z0.f(activity) - (this.f.g() ? 0 : l * 2);
    }

    public final int C(Activity activity, JSONObject jSONObject) {
        try {
            int b2 = z0.b(jSONObject.getJSONObject("rect").getInt("height"));
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.d1(log_level, "getPageHeightData:pxHeight: " + b2);
            int A = A(activity);
            if (b2 > A) {
                OneSignal.a(log_level, "getPageHeightData:pxHeight is over screen max: " + A);
                return A;
            }
            return b2;
        } catch (JSONException e2) {
            OneSignal.b(OneSignal.LOG_LEVEL.ERROR, "pageRectToViewHeight could not get page height", e2);
            return -1;
        }
    }

    public final void D() {
        com.onesignal.a b2 = k7.b();
        if (b2 != null) {
            b2.q(k + this.e.a);
        }
    }

    public final void F(com.onesignal.i iVar) {
        synchronized (this.a) {
            this.c = iVar;
        }
    }

    public final void G(Activity activity) {
        this.b.layout(0, 0, z(activity), A(activity));
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public final void H(Activity activity, String str, boolean z) {
        y();
        OSWebView oSWebView = new OSWebView(activity);
        this.b = oSWebView;
        oSWebView.setOverScrollMode(2);
        this.b.setVerticalScrollBarEnabled(false);
        this.b.setHorizontalScrollBarEnabled(false);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.addJavascriptInterface(new k(), "OSAndroid");
        if (z) {
            this.b.setSystemUiVisibility(3074);
            if (Build.VERSION.SDK_INT >= 30) {
                this.b.setFitsSystemWindows(false);
            }
        }
        t(this.b);
        z0.a(activity, new h(activity, str));
    }

    public final void J(Integer num) {
        synchronized (this.a) {
            if (this.c == null) {
                OneSignal.a(OneSignal.LOG_LEVEL.WARN, "No messageView found to update a with a new height.");
                return;
            }
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.a(log_level, "In app message, showing first one with height: " + num);
            this.c.U(this.b);
            if (num != null) {
                this.h = num;
                this.c.Z(num.intValue());
            }
            this.c.X(this.d);
            this.c.B();
        }
    }

    public final void K() {
        OSUtils.S(new f());
    }

    @Override // com.onesignal.a.b
    public void a(Activity activity) {
        String str = this.g;
        this.d = activity;
        this.g = activity.getLocalClassName();
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "In app message activity available currentActivityName: " + this.g + " lastActivityName: " + str);
        if (str == null) {
            J(null);
        } else if (!str.equals(this.g)) {
            if (this.j) {
                return;
            }
            com.onesignal.i iVar = this.c;
            if (iVar != null) {
                iVar.P();
            }
            J(this.h);
        } else {
            u();
        }
    }

    @Override // com.onesignal.a.b
    public void b(Activity activity) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.a(log_level, "In app message activity stopped, cleaning views, currentActivityName: " + this.g + "\nactivity: " + this.d + "\nmessageView: " + this.c);
        if (this.c == null || !activity.getLocalClassName().equals(this.g)) {
            return;
        }
        this.c.P();
    }

    public final void t(WebView webView) {
        if (Build.VERSION.SDK_INT == 19) {
            webView.setLayerType(1, null);
        }
    }

    public final void u() {
        com.onesignal.i iVar = this.c;
        if (iVar == null) {
            return;
        }
        if (iVar.M() == Position.FULL_SCREEN && !this.f.g()) {
            J(null);
            return;
        }
        OneSignal.a(OneSignal.LOG_LEVEL.DEBUG, "In app message new activity, calculate height and show ");
        z0.a(this.d, new g());
    }

    public final void v(boolean z) {
        this.h = Integer.valueOf(this.f.d());
        F(new com.onesignal.i(this.b, this.f, z));
        this.c.R(new i());
        com.onesignal.a b2 = k7.b();
        if (b2 != null) {
            b2.b(k + this.e.a, this);
        }
    }

    public void w(l lVar) {
        com.onesignal.i iVar = this.c;
        if (iVar == null || this.i) {
            if (lVar != null) {
                lVar.a();
                return;
            }
            return;
        }
        if (this.e != null && iVar != null) {
            OneSignal.c0().e0(this.e);
        }
        this.c.K(new j(lVar));
        this.i = true;
    }

    public final int z(Activity activity) {
        if (this.f.g()) {
            return z0.e(activity);
        }
        return z0.j(activity) - (l * 2);
    }
}
