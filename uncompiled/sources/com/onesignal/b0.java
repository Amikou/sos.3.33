package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.b0;

/* compiled from: OSInAppMessagePushPrompt.java */
/* loaded from: classes2.dex */
public class b0 extends a0 {
    public static /* synthetic */ void f(OneSignal.c0 c0Var, boolean z) {
        OneSignal.PromptActionResult promptActionResult;
        if (z) {
            promptActionResult = OneSignal.PromptActionResult.PERMISSION_GRANTED;
        } else {
            promptActionResult = OneSignal.PromptActionResult.PERMISSION_DENIED;
        }
        c0Var.a(promptActionResult);
    }

    @Override // com.onesignal.a0
    public String a() {
        return "push";
    }

    @Override // com.onesignal.a0
    public void b(final OneSignal.c0 c0Var) {
        OneSignal.f1(true, new OneSignal.h0() { // from class: uj2
            @Override // com.onesignal.OneSignal.h0
            public final void a(boolean z) {
                b0.f(OneSignal.c0.this, z);
            }
        });
    }
}
