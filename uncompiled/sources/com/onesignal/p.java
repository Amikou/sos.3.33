package com.onesignal;

import android.app.Activity;
import android.os.Build;
import com.onesignal.OneSignal;
import com.onesignal.PermissionsActivity;
import defpackage.ua;
import java.util.HashSet;
import java.util.Set;

/* compiled from: NotificationPermissionController.kt */
/* loaded from: classes2.dex */
public final class p implements PermissionsActivity.c {
    public static final Set<OneSignal.h0> a;
    public static boolean b;
    public static final boolean c;
    public static final p d;

    /* compiled from: NotificationPermissionController.kt */
    /* loaded from: classes2.dex */
    public static final class a implements ua.a {
        public final /* synthetic */ Activity a;

        public a(Activity activity) {
            this.a = activity;
        }

        @Override // defpackage.ua.a
        public void a() {
            ge2.a.a(this.a);
            p pVar = p.d;
            p.b = true;
        }

        @Override // defpackage.ua.a
        public void b() {
            p.d.e(false);
        }
    }

    static {
        p pVar = new p();
        d = pVar;
        a = new HashSet();
        PermissionsActivity.e("NOTIFICATION", pVar);
        c = Build.VERSION.SDK_INT > 32 && OSUtils.o(OneSignal.e) > 32;
    }

    @Override // com.onesignal.PermissionsActivity.c
    public void a() {
        OneSignal.j1();
        e(true);
    }

    @Override // com.onesignal.PermissionsActivity.c
    public void b(boolean z) {
        if (z ? i() : false) {
            return;
        }
        e(false);
    }

    public final void e(boolean z) {
        for (OneSignal.h0 h0Var : a) {
            h0Var.a(z);
        }
        a.clear();
    }

    public final boolean f() {
        return OSUtils.a(OneSignal.e);
    }

    public final void g() {
        if (b) {
            b = false;
            e(f());
        }
    }

    public final void h(boolean z, OneSignal.h0 h0Var) {
        if (h0Var != null) {
            a.add(h0Var);
        }
        if (f()) {
            e(true);
        } else if (c) {
            PermissionsActivity.i(z, "NOTIFICATION", "android.permission.POST_NOTIFICATIONS", p.class);
        } else if (z) {
            i();
        } else {
            e(false);
        }
    }

    public final boolean i() {
        Activity Q = OneSignal.Q();
        if (Q != null) {
            fs1.e(Q, "OneSignal.getCurrentActivity() ?: return false");
            ua uaVar = ua.a;
            String string = Q.getString(s13.notification_permission_name_for_title);
            fs1.e(string, "activity.getString(R.str…ermission_name_for_title)");
            String string2 = Q.getString(s13.notification_permission_settings_message);
            fs1.e(string2, "activity.getString(R.str…mission_settings_message)");
            uaVar.a(Q, string, string2, new a(Q));
            return true;
        }
        return false;
    }
}
