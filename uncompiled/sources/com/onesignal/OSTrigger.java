package com.onesignal;

import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class OSTrigger {
    public String a;
    public OSTriggerKind b;
    public String c;
    public OSTriggerOperator d;
    public Object e;

    /* loaded from: classes2.dex */
    public enum OSTriggerKind {
        TIME_SINCE_LAST_IN_APP("min_time_since"),
        SESSION_TIME("session_time"),
        CUSTOM("custom"),
        UNKNOWN("unknown");
        
        private String value;

        OSTriggerKind(String str) {
            this.value = str;
        }

        public static OSTriggerKind fromString(String str) {
            OSTriggerKind[] values;
            for (OSTriggerKind oSTriggerKind : values()) {
                if (oSTriggerKind.value.equalsIgnoreCase(str)) {
                    return oSTriggerKind;
                }
            }
            return UNKNOWN;
        }

        @Override // java.lang.Enum
        public String toString() {
            return this.value;
        }
    }

    /* loaded from: classes2.dex */
    public enum OSTriggerOperator {
        GREATER_THAN("greater"),
        LESS_THAN("less"),
        EQUAL_TO("equal"),
        NOT_EQUAL_TO("not_equal"),
        LESS_THAN_OR_EQUAL_TO("less_or_equal"),
        GREATER_THAN_OR_EQUAL_TO("greater_or_equal"),
        EXISTS("exists"),
        NOT_EXISTS("not_exists"),
        CONTAINS("in");
        
        private String text;

        OSTriggerOperator(String str) {
            this.text = str;
        }

        public static OSTriggerOperator fromString(String str) {
            OSTriggerOperator[] values;
            for (OSTriggerOperator oSTriggerOperator : values()) {
                if (oSTriggerOperator.text.equalsIgnoreCase(str)) {
                    return oSTriggerOperator;
                }
            }
            return EQUAL_TO;
        }

        public boolean checksEquality() {
            return this == EQUAL_TO || this == NOT_EQUAL_TO;
        }

        @Override // java.lang.Enum
        public String toString() {
            return this.text;
        }
    }

    public OSTrigger(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.getString("id");
        this.b = OSTriggerKind.fromString(jSONObject.getString("kind"));
        this.c = jSONObject.optString("property", null);
        this.d = OSTriggerOperator.fromString(jSONObject.getString("operator"));
        this.e = jSONObject.opt("value");
    }

    public String toString() {
        return "OSTrigger{triggerId='" + this.a + "', kind=" + this.b + ", property='" + this.c + "', operatorType=" + this.d + ", value=" + this.e + '}';
    }
}
