package com.onesignal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.amazon.device.messaging.ADMMessageHandlerBase;
import com.onesignal.OneSignal;
import com.onesignal.k;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class ADMMessageHandler extends ADMMessageHandlerBase {
    private static final int JOB_ID = 123891;

    /* loaded from: classes2.dex */
    public class a implements k.e {
        public final /* synthetic */ Bundle a;
        public final /* synthetic */ Context b;

        public a(ADMMessageHandler aDMMessageHandler, Bundle bundle, Context context) {
            this.a = bundle;
            this.b = context;
        }

        @Override // com.onesignal.k.e
        public void a(k.f fVar) {
            if (fVar.c()) {
                return;
            }
            JSONObject a = k.a(this.a);
            g0 g0Var = new g0(a);
            zj2 zj2Var = new zj2(this.b);
            zj2Var.q(a);
            zj2Var.o(this.b);
            zj2Var.r(g0Var);
            k.k(zj2Var, true);
        }
    }

    public ADMMessageHandler() {
        super("ADMMessageHandler");
    }

    public void onMessage(Intent intent) {
        Context applicationContext = getApplicationContext();
        Bundle extras = intent.getExtras();
        k.h(applicationContext, extras, new a(this, extras, applicationContext));
    }

    public void onRegistered(String str) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
        OneSignal.a(log_level, "ADM registration ID: " + str);
        g1.c(str);
    }

    public void onRegistrationError(String str) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
        OneSignal.a(log_level, "ADM:onRegistrationError: " + str);
        if ("INVALID_SENDER".equals(str)) {
            OneSignal.a(log_level, "Please double check that you have a matching package name (NOTE: Case Sensitive), api_key.txt, and the apk was signed with the same Keystore and Alias.");
        }
        g1.c(null);
    }

    public void onUnregistered(String str) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
        OneSignal.a(log_level, "ADM:onUnregistered: " + str);
    }
}
