package com.fasterxml.jackson.core;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.format.MatchStrength;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.core.io.InputDecorator;
import com.fasterxml.jackson.core.io.OutputDecorator;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.core.io.g;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import java.io.CharArrayReader;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.Writer;
import java.lang.ref.SoftReference;
import java.net.URL;

/* loaded from: classes.dex */
public class JsonFactory implements Serializable {
    public static final String FORMAT_NAME_JSON = "JSON";
    private static final long serialVersionUID = 1;
    public final transient vs _byteSymbolCanonicalizer;
    public CharacterEscapes _characterEscapes;
    public int _factoryFeatures;
    public int _generatorFeatures;
    public InputDecorator _inputDecorator;
    public c _objectCodec;
    public OutputDecorator _outputDecorator;
    public int _parserFeatures;
    public final transient zx _rootCharSymbols;
    public yl3 _rootValueSeparator;
    public static final int DEFAULT_FACTORY_FEATURE_FLAGS = Feature.collectDefaults();
    public static final int DEFAULT_PARSER_FEATURE_FLAGS = JsonParser.Feature.collectDefaults();
    public static final int DEFAULT_GENERATOR_FEATURE_FLAGS = JsonGenerator.Feature.collectDefaults();
    public static final yl3 a = DefaultPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
    public static final ThreadLocal<SoftReference<wr>> _recyclerRef = new ThreadLocal<>();

    /* loaded from: classes.dex */
    public enum Feature {
        INTERN_FIELD_NAMES(true),
        CANONICALIZE_FIELD_NAMES(true),
        FAIL_ON_SYMBOL_HASH_OVERFLOW(true),
        USE_THREAD_LOCAL_FOR_BUFFER_RECYCLING(true);
        
        private final boolean _defaultState;

        Feature(boolean z) {
            this._defaultState = z;
        }

        public static int collectDefaults() {
            Feature[] values;
            int i = 0;
            for (Feature feature : values()) {
                if (feature.enabledByDefault()) {
                    i |= feature.getMask();
                }
            }
            return i;
        }

        public boolean enabledByDefault() {
            return this._defaultState;
        }

        public boolean enabledIn(int i) {
            return (i & getMask()) != 0;
        }

        public int getMask() {
            return 1 << ordinal();
        }
    }

    public JsonFactory() {
        this(null);
    }

    public void _checkInvalidCopy(Class<?> cls) {
        if (getClass() == cls) {
            return;
        }
        throw new IllegalStateException("Failed copy(): " + getClass().getName() + " (version: " + version() + ") does not override copy(); it has to");
    }

    public jm1 _createContext(Object obj, boolean z) {
        return new jm1(_getBufferRecycler(), obj, z);
    }

    public OutputStream _createDataOutputWrapper(DataOutput dataOutput) {
        return new com.fasterxml.jackson.core.io.b(dataOutput);
    }

    public JsonGenerator _createGenerator(Writer writer, jm1 jm1Var) throws IOException {
        lr4 lr4Var = new lr4(jm1Var, this._generatorFeatures, this._objectCodec, writer);
        CharacterEscapes characterEscapes = this._characterEscapes;
        if (characterEscapes != null) {
            lr4Var.r(characterEscapes);
        }
        yl3 yl3Var = this._rootValueSeparator;
        if (yl3Var != a) {
            lr4Var.z(yl3Var);
        }
        return lr4Var;
    }

    public JsonParser _createParser(InputStream inputStream, jm1 jm1Var) throws IOException {
        return new ws(jm1Var, inputStream).c(this._parserFeatures, this._objectCodec, this._byteSymbolCanonicalizer, this._rootCharSymbols, this._factoryFeatures);
    }

    public JsonGenerator _createUTF8Generator(OutputStream outputStream, jm1 jm1Var) throws IOException {
        je4 je4Var = new je4(jm1Var, this._generatorFeatures, this._objectCodec, outputStream);
        CharacterEscapes characterEscapes = this._characterEscapes;
        if (characterEscapes != null) {
            je4Var.r(characterEscapes);
        }
        yl3 yl3Var = this._rootValueSeparator;
        if (yl3Var != a) {
            je4Var.z(yl3Var);
        }
        return je4Var;
    }

    public Writer _createWriter(OutputStream outputStream, JsonEncoding jsonEncoding, jm1 jm1Var) throws IOException {
        if (jsonEncoding == JsonEncoding.UTF8) {
            return new g(jm1Var, outputStream);
        }
        return new OutputStreamWriter(outputStream, jsonEncoding.getJavaName());
    }

    public final InputStream _decorate(InputStream inputStream, jm1 jm1Var) throws IOException {
        InputStream decorate;
        InputDecorator inputDecorator = this._inputDecorator;
        return (inputDecorator == null || (decorate = inputDecorator.decorate(jm1Var, inputStream)) == null) ? inputStream : decorate;
    }

    public wr _getBufferRecycler() {
        if (isEnabled(Feature.USE_THREAD_LOCAL_FOR_BUFFER_RECYCLING)) {
            ThreadLocal<SoftReference<wr>> threadLocal = _recyclerRef;
            SoftReference<wr> softReference = threadLocal.get();
            wr wrVar = softReference == null ? null : softReference.get();
            if (wrVar == null) {
                wr wrVar2 = new wr();
                threadLocal.set(new SoftReference<>(wrVar2));
                return wrVar2;
            }
            return wrVar;
        }
        return new wr();
    }

    public InputStream _optimizedStreamFromURL(URL url) throws IOException {
        String host;
        if ("file".equals(url.getProtocol()) && (((host = url.getHost()) == null || host.length() == 0) && url.getPath().indexOf(37) < 0)) {
            return new FileInputStream(url.getPath());
        }
        return url.openStream();
    }

    public boolean canHandleBinaryNatively() {
        return false;
    }

    public boolean canUseCharArrays() {
        return true;
    }

    public boolean canUseSchema(z81 z81Var) {
        String formatName;
        return (z81Var == null || (formatName = getFormatName()) == null || !formatName.equals(z81Var.a())) ? false : true;
    }

    public final JsonFactory configure(Feature feature, boolean z) {
        return z ? enable(feature) : disable(feature);
    }

    public JsonFactory copy() {
        _checkInvalidCopy(JsonFactory.class);
        return new JsonFactory(this, null);
    }

    public JsonGenerator createGenerator(OutputStream outputStream, JsonEncoding jsonEncoding) throws IOException {
        jm1 _createContext = _createContext(outputStream, false);
        _createContext.u(jsonEncoding);
        if (jsonEncoding == JsonEncoding.UTF8) {
            return _createUTF8Generator(_decorate(outputStream, _createContext), _createContext);
        }
        return _createGenerator(_decorate(_createWriter(outputStream, jsonEncoding, _createContext), _createContext), _createContext);
    }

    @Deprecated
    public JsonGenerator createJsonGenerator(OutputStream outputStream, JsonEncoding jsonEncoding) throws IOException {
        return createGenerator(outputStream, jsonEncoding);
    }

    @Deprecated
    public JsonParser createJsonParser(File file) throws IOException, JsonParseException {
        return createParser(file);
    }

    public JsonParser createParser(File file) throws IOException, JsonParseException {
        jm1 _createContext = _createContext(file, true);
        return _createParser(_decorate(new FileInputStream(file), _createContext), _createContext);
    }

    public JsonFactory disable(Feature feature) {
        this._factoryFeatures = (~feature.getMask()) & this._factoryFeatures;
        return this;
    }

    public JsonFactory enable(Feature feature) {
        this._factoryFeatures = feature.getMask() | this._factoryFeatures;
        return this;
    }

    public CharacterEscapes getCharacterEscapes() {
        return this._characterEscapes;
    }

    public c getCodec() {
        return this._objectCodec;
    }

    public String getFormatName() {
        if (getClass() == JsonFactory.class) {
            return FORMAT_NAME_JSON;
        }
        return null;
    }

    public Class<? extends x81> getFormatReadFeatureType() {
        return null;
    }

    public Class<? extends x81> getFormatWriteFeatureType() {
        return null;
    }

    public InputDecorator getInputDecorator() {
        return this._inputDecorator;
    }

    public OutputDecorator getOutputDecorator() {
        return this._outputDecorator;
    }

    public String getRootValueSeparator() {
        yl3 yl3Var = this._rootValueSeparator;
        if (yl3Var == null) {
            return null;
        }
        return yl3Var.getValue();
    }

    public MatchStrength hasFormat(com.fasterxml.jackson.core.format.a aVar) throws IOException {
        if (getClass() == JsonFactory.class) {
            return hasJSONFormat(aVar);
        }
        return null;
    }

    public MatchStrength hasJSONFormat(com.fasterxml.jackson.core.format.a aVar) throws IOException {
        return ws.h(aVar);
    }

    public final boolean isEnabled(Feature feature) {
        return (feature.getMask() & this._factoryFeatures) != 0;
    }

    public Object readResolve() {
        return new JsonFactory(this, this._objectCodec);
    }

    public boolean requiresCustomCodec() {
        return false;
    }

    public boolean requiresPropertyOrdering() {
        return false;
    }

    public JsonFactory setCharacterEscapes(CharacterEscapes characterEscapes) {
        this._characterEscapes = characterEscapes;
        return this;
    }

    public JsonFactory setCodec(c cVar) {
        this._objectCodec = cVar;
        return this;
    }

    public JsonFactory setInputDecorator(InputDecorator inputDecorator) {
        this._inputDecorator = inputDecorator;
        return this;
    }

    public JsonFactory setOutputDecorator(OutputDecorator outputDecorator) {
        this._outputDecorator = outputDecorator;
        return this;
    }

    public JsonFactory setRootValueSeparator(String str) {
        this._rootValueSeparator = str == null ? null : new SerializedString(str);
        return this;
    }

    public Version version() {
        return xo2.a;
    }

    public JsonFactory(c cVar) {
        this._rootCharSymbols = zx.m();
        this._byteSymbolCanonicalizer = vs.A();
        this._factoryFeatures = DEFAULT_FACTORY_FEATURE_FLAGS;
        this._parserFeatures = DEFAULT_PARSER_FEATURE_FLAGS;
        this._generatorFeatures = DEFAULT_GENERATOR_FEATURE_FLAGS;
        this._rootValueSeparator = a;
        this._objectCodec = cVar;
    }

    public JsonParser _createParser(Reader reader, jm1 jm1Var) throws IOException {
        return new b43(jm1Var, this._parserFeatures, reader, this._objectCodec, this._rootCharSymbols.q(this._factoryFeatures));
    }

    public final JsonFactory configure(JsonParser.Feature feature, boolean z) {
        return z ? enable(feature) : disable(feature);
    }

    @Deprecated
    public JsonGenerator createJsonGenerator(Writer writer) throws IOException {
        return createGenerator(writer);
    }

    @Deprecated
    public JsonParser createJsonParser(URL url) throws IOException, JsonParseException {
        return createParser(url);
    }

    public JsonFactory disable(JsonParser.Feature feature) {
        this._parserFeatures = (~feature.getMask()) & this._parserFeatures;
        return this;
    }

    public JsonFactory enable(JsonParser.Feature feature) {
        this._parserFeatures = feature.getMask() | this._parserFeatures;
        return this;
    }

    public final boolean isEnabled(JsonParser.Feature feature) {
        return (feature.getMask() & this._parserFeatures) != 0;
    }

    public JsonParser _createParser(char[] cArr, int i, int i2, jm1 jm1Var, boolean z) throws IOException {
        return new b43(jm1Var, this._parserFeatures, null, this._objectCodec, this._rootCharSymbols.q(this._factoryFeatures), cArr, i, i + i2, z);
    }

    public final Reader _decorate(Reader reader, jm1 jm1Var) throws IOException {
        Reader decorate;
        InputDecorator inputDecorator = this._inputDecorator;
        return (inputDecorator == null || (decorate = inputDecorator.decorate(jm1Var, reader)) == null) ? reader : decorate;
    }

    public final JsonFactory configure(JsonGenerator.Feature feature, boolean z) {
        return z ? enable(feature) : disable(feature);
    }

    @Deprecated
    public JsonGenerator createJsonGenerator(OutputStream outputStream) throws IOException {
        return createGenerator(outputStream, JsonEncoding.UTF8);
    }

    @Deprecated
    public JsonParser createJsonParser(InputStream inputStream) throws IOException, JsonParseException {
        return createParser(inputStream);
    }

    public JsonFactory disable(JsonGenerator.Feature feature) {
        this._generatorFeatures = (~feature.getMask()) & this._generatorFeatures;
        return this;
    }

    public JsonFactory enable(JsonGenerator.Feature feature) {
        this._generatorFeatures = feature.getMask() | this._generatorFeatures;
        return this;
    }

    public final boolean isEnabled(JsonGenerator.Feature feature) {
        return (feature.getMask() & this._generatorFeatures) != 0;
    }

    public JsonParser _createParser(byte[] bArr, int i, int i2, jm1 jm1Var) throws IOException {
        return new ws(jm1Var, bArr, i, i2).c(this._parserFeatures, this._objectCodec, this._byteSymbolCanonicalizer, this._rootCharSymbols, this._factoryFeatures);
    }

    @Deprecated
    public JsonParser createJsonParser(Reader reader) throws IOException, JsonParseException {
        return createParser(reader);
    }

    public JsonParser createParser(URL url) throws IOException, JsonParseException {
        jm1 _createContext = _createContext(url, true);
        return _createParser(_decorate(_optimizedStreamFromURL(url), _createContext), _createContext);
    }

    public JsonParser _createParser(DataInput dataInput, jm1 jm1Var) throws IOException {
        String formatName = getFormatName();
        if (formatName == FORMAT_NAME_JSON) {
            return new ie4(jm1Var, this._parserFeatures, dataInput, this._objectCodec, this._byteSymbolCanonicalizer.G(this._factoryFeatures), ws.l(dataInput));
        }
        throw new UnsupportedOperationException(String.format("InputData source not (yet?) support for this format (%s)", formatName));
    }

    public final DataInput _decorate(DataInput dataInput, jm1 jm1Var) throws IOException {
        DataInput decorate;
        InputDecorator inputDecorator = this._inputDecorator;
        return (inputDecorator == null || (decorate = inputDecorator.decorate(jm1Var, dataInput)) == null) ? dataInput : decorate;
    }

    @Deprecated
    public JsonParser createJsonParser(byte[] bArr) throws IOException, JsonParseException {
        return createParser(bArr);
    }

    @Deprecated
    public JsonParser createJsonParser(byte[] bArr, int i, int i2) throws IOException, JsonParseException {
        return createParser(bArr, i, i2);
    }

    public final OutputStream _decorate(OutputStream outputStream, jm1 jm1Var) throws IOException {
        OutputStream decorate;
        OutputDecorator outputDecorator = this._outputDecorator;
        return (outputDecorator == null || (decorate = outputDecorator.decorate(jm1Var, outputStream)) == null) ? outputStream : decorate;
    }

    public JsonGenerator createGenerator(OutputStream outputStream) throws IOException {
        return createGenerator(outputStream, JsonEncoding.UTF8);
    }

    @Deprecated
    public JsonParser createJsonParser(String str) throws IOException, JsonParseException {
        return createParser(str);
    }

    public JsonParser createParser(InputStream inputStream) throws IOException, JsonParseException {
        jm1 _createContext = _createContext(inputStream, false);
        return _createParser(_decorate(inputStream, _createContext), _createContext);
    }

    public JsonGenerator createGenerator(Writer writer) throws IOException {
        jm1 _createContext = _createContext(writer, false);
        return _createGenerator(_decorate(writer, _createContext), _createContext);
    }

    public final Writer _decorate(Writer writer, jm1 jm1Var) throws IOException {
        Writer decorate;
        OutputDecorator outputDecorator = this._outputDecorator;
        return (outputDecorator == null || (decorate = outputDecorator.decorate(jm1Var, writer)) == null) ? writer : decorate;
    }

    public JsonParser createParser(Reader reader) throws IOException, JsonParseException {
        jm1 _createContext = _createContext(reader, false);
        return _createParser(_decorate(reader, _createContext), _createContext);
    }

    public JsonFactory(JsonFactory jsonFactory, c cVar) {
        this._rootCharSymbols = zx.m();
        this._byteSymbolCanonicalizer = vs.A();
        this._factoryFeatures = DEFAULT_FACTORY_FEATURE_FLAGS;
        this._parserFeatures = DEFAULT_PARSER_FEATURE_FLAGS;
        this._generatorFeatures = DEFAULT_GENERATOR_FEATURE_FLAGS;
        this._rootValueSeparator = a;
        this._objectCodec = null;
        this._factoryFeatures = jsonFactory._factoryFeatures;
        this._parserFeatures = jsonFactory._parserFeatures;
        this._generatorFeatures = jsonFactory._generatorFeatures;
        this._characterEscapes = jsonFactory._characterEscapes;
        this._inputDecorator = jsonFactory._inputDecorator;
        this._outputDecorator = jsonFactory._outputDecorator;
        this._rootValueSeparator = jsonFactory._rootValueSeparator;
    }

    public JsonGenerator createGenerator(File file, JsonEncoding jsonEncoding) throws IOException {
        OutputStream fileOutputStream = new FileOutputStream(file);
        jm1 _createContext = _createContext(fileOutputStream, true);
        _createContext.u(jsonEncoding);
        if (jsonEncoding == JsonEncoding.UTF8) {
            return _createUTF8Generator(_decorate(fileOutputStream, _createContext), _createContext);
        }
        return _createGenerator(_decorate(_createWriter(fileOutputStream, jsonEncoding, _createContext), _createContext), _createContext);
    }

    public JsonParser createParser(byte[] bArr) throws IOException, JsonParseException {
        InputStream decorate;
        jm1 _createContext = _createContext(bArr, true);
        InputDecorator inputDecorator = this._inputDecorator;
        if (inputDecorator != null && (decorate = inputDecorator.decorate(_createContext, bArr, 0, bArr.length)) != null) {
            return _createParser(decorate, _createContext);
        }
        return _createParser(bArr, 0, bArr.length, _createContext);
    }

    public JsonParser createParser(byte[] bArr, int i, int i2) throws IOException, JsonParseException {
        InputStream decorate;
        jm1 _createContext = _createContext(bArr, true);
        InputDecorator inputDecorator = this._inputDecorator;
        if (inputDecorator != null && (decorate = inputDecorator.decorate(_createContext, bArr, i, i2)) != null) {
            return _createParser(decorate, _createContext);
        }
        return _createParser(bArr, i, i2, _createContext);
    }

    public JsonGenerator createGenerator(DataOutput dataOutput, JsonEncoding jsonEncoding) throws IOException {
        return createGenerator(_createDataOutputWrapper(dataOutput), jsonEncoding);
    }

    public JsonGenerator createGenerator(DataOutput dataOutput) throws IOException {
        return createGenerator(_createDataOutputWrapper(dataOutput), JsonEncoding.UTF8);
    }

    public JsonParser createParser(String str) throws IOException, JsonParseException {
        int length = str.length();
        if (this._inputDecorator == null && length <= 32768 && canUseCharArrays()) {
            jm1 _createContext = _createContext(str, true);
            char[] i = _createContext.i(length);
            str.getChars(0, length, i, 0);
            return _createParser(i, 0, length, _createContext, true);
        }
        return createParser(new StringReader(str));
    }

    public JsonParser createParser(char[] cArr) throws IOException {
        return createParser(cArr, 0, cArr.length);
    }

    public JsonParser createParser(char[] cArr, int i, int i2) throws IOException {
        if (this._inputDecorator != null) {
            return createParser(new CharArrayReader(cArr, i, i2));
        }
        return _createParser(cArr, i, i2, _createContext(cArr, true), false);
    }

    public JsonParser createParser(DataInput dataInput) throws IOException {
        jm1 _createContext = _createContext(dataInput, false);
        return _createParser(_decorate(dataInput, _createContext), _createContext);
    }
}
