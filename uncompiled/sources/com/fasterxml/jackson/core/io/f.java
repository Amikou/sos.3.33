package com.fasterxml.jackson.core.io;

import java.io.CharConversionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

/* compiled from: UTF32Reader.java */
/* loaded from: classes.dex */
public class f extends Reader {
    public final jm1 a;
    public InputStream f0;
    public byte[] g0;
    public int h0;
    public int i0;
    public final boolean j0;
    public char k0 = 0;
    public int l0;
    public int m0;
    public final boolean n0;
    public char[] o0;

    public f(jm1 jm1Var, InputStream inputStream, byte[] bArr, int i, int i2, boolean z) {
        this.a = jm1Var;
        this.f0 = inputStream;
        this.g0 = bArr;
        this.h0 = i;
        this.i0 = i2;
        this.j0 = z;
        this.n0 = inputStream != null;
    }

    public final void a() {
        byte[] bArr = this.g0;
        if (bArr != null) {
            this.g0 = null;
            this.a.r(bArr);
        }
    }

    public final boolean b(int i) throws IOException {
        int read;
        this.m0 += this.i0 - i;
        if (i > 0) {
            int i2 = this.h0;
            if (i2 > 0) {
                byte[] bArr = this.g0;
                System.arraycopy(bArr, i2, bArr, 0, i);
                this.h0 = 0;
            }
            this.i0 = i;
        } else {
            this.h0 = 0;
            InputStream inputStream = this.f0;
            int read2 = inputStream == null ? -1 : inputStream.read(this.g0);
            if (read2 < 1) {
                this.i0 = 0;
                if (read2 < 0) {
                    if (this.n0) {
                        a();
                    }
                    return false;
                }
                e();
            }
            this.i0 = read2;
        }
        while (true) {
            int i3 = this.i0;
            if (i3 >= 4) {
                return true;
            }
            InputStream inputStream2 = this.f0;
            if (inputStream2 == null) {
                read = -1;
            } else {
                byte[] bArr2 = this.g0;
                read = inputStream2.read(bArr2, i3, bArr2.length - i3);
            }
            if (read < 1) {
                if (read < 0) {
                    if (this.n0) {
                        a();
                    }
                    f(this.i0, 4);
                }
                e();
            }
            this.i0 += read;
        }
    }

    public final void c(char[] cArr, int i, int i2) throws IOException {
        throw new ArrayIndexOutOfBoundsException("read(buf," + i + "," + i2 + "), cbuf[" + cArr.length + "]");
    }

    @Override // java.io.Reader, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        InputStream inputStream = this.f0;
        if (inputStream != null) {
            this.f0 = null;
            a();
            inputStream.close();
        }
    }

    public final void d(int i, int i2, String str) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("Invalid UTF-32 character 0x");
        sb.append(Integer.toHexString(i));
        sb.append(str);
        sb.append(" at char #");
        sb.append(this.l0 + i2);
        sb.append(", byte #");
        sb.append((this.m0 + this.h0) - 1);
        sb.append(")");
        throw new CharConversionException(sb.toString());
    }

    public final void e() throws IOException {
        throw new IOException("Strange I/O stream, returned 0 bytes on read");
    }

    public final void f(int i, int i2) throws IOException {
        int i3 = this.l0;
        throw new CharConversionException("Unexpected EOF in the middle of a 4-byte UTF-32 char: got " + i + ", needed " + i2 + ", at char #" + i3 + ", byte #" + (this.m0 + i) + ")");
    }

    @Override // java.io.Reader
    public int read() throws IOException {
        if (this.o0 == null) {
            this.o0 = new char[1];
        }
        if (read(this.o0, 0, 1) < 1) {
            return -1;
        }
        return this.o0[0];
    }

    @Override // java.io.Reader
    public int read(char[] cArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        int i5;
        int i6;
        if (this.g0 == null) {
            return -1;
        }
        if (i2 < 1) {
            return i2;
        }
        if (i < 0 || i + i2 > cArr.length) {
            c(cArr, i, i2);
        }
        int i7 = i2 + i;
        char c = this.k0;
        if (c != 0) {
            i3 = i + 1;
            cArr[i] = c;
            this.k0 = (char) 0;
        } else {
            int i8 = this.i0 - this.h0;
            if (i8 < 4 && !b(i8)) {
                if (i8 == 0) {
                    return -1;
                }
                f(this.i0 - this.h0, 4);
            }
            i3 = i;
        }
        int i9 = this.i0 - 3;
        while (i3 < i7) {
            int i10 = this.h0;
            if (this.j0) {
                byte[] bArr = this.g0;
                i4 = (bArr[i10] << 8) | (bArr[i10 + 1] & 255);
                i5 = (bArr[i10 + 3] & 255) | ((bArr[i10 + 2] & 255) << 8);
            } else {
                byte[] bArr2 = this.g0;
                int i11 = (bArr2[i10] & 255) | ((bArr2[i10 + 1] & 255) << 8);
                i4 = (bArr2[i10 + 3] << 8) | (bArr2[i10 + 2] & 255);
                i5 = i11;
            }
            this.h0 = i10 + 4;
            if (i4 != 0) {
                int i12 = 65535 & i4;
                int i13 = i5 | ((i12 - 1) << 16);
                if (i12 > 16) {
                    d(i13, i3 - i, String.format(" (above 0x%08x)", 1114111));
                }
                i6 = i3 + 1;
                cArr[i3] = (char) ((i13 >> 10) + 55296);
                int i14 = 56320 | (i13 & 1023);
                if (i6 >= i7) {
                    this.k0 = (char) i13;
                    i3 = i6;
                    break;
                }
                i5 = i14;
                i3 = i6;
            }
            i6 = i3 + 1;
            cArr[i3] = (char) i5;
            if (this.h0 > i9) {
                i3 = i6;
                break;
            }
            i3 = i6;
        }
        int i15 = i3 - i;
        this.l0 += i15;
        return i15;
    }
}
