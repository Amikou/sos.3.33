package com.fasterxml.jackson.core.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.Writer;

/* loaded from: classes.dex */
public abstract class OutputDecorator implements Serializable {
    public abstract OutputStream decorate(jm1 jm1Var, OutputStream outputStream) throws IOException;

    public abstract Writer decorate(jm1 jm1Var, Writer writer) throws IOException;
}
