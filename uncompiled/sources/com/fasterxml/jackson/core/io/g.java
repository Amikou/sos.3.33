package com.fasterxml.jackson.core.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

/* compiled from: UTF8Writer.java */
/* loaded from: classes.dex */
public final class g extends Writer {
    public final jm1 a;
    public OutputStream f0;
    public byte[] g0;
    public final int h0;
    public int i0;
    public int j0;

    public g(jm1 jm1Var, OutputStream outputStream) {
        this.a = jm1Var;
        this.f0 = outputStream;
        byte[] j = jm1Var.j();
        this.g0 = j;
        this.h0 = j.length - 4;
        this.i0 = 0;
    }

    public static void b(int i) throws IOException {
        throw new IOException(c(i));
    }

    public static String c(int i) {
        if (i > 1114111) {
            return "Illegal character point (0x" + Integer.toHexString(i) + ") to output; max is 0x10FFFF as per RFC 4627";
        } else if (i < 55296) {
            return "Illegal character point (0x" + Integer.toHexString(i) + ") to output";
        } else if (i <= 56319) {
            return "Unmatched first part of surrogate pair (0x" + Integer.toHexString(i) + ")";
        } else {
            return "Unmatched second part of surrogate pair (0x" + Integer.toHexString(i) + ")";
        }
    }

    public int a(int i) throws IOException {
        int i2 = this.j0;
        this.j0 = 0;
        if (i < 56320 || i > 57343) {
            throw new IOException("Broken surrogate pair: first char 0x" + Integer.toHexString(i2) + ", second 0x" + Integer.toHexString(i) + "; illegal combination");
        }
        return ((i2 - 55296) << 10) + 65536 + (i - 56320);
    }

    @Override // java.io.Writer, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        OutputStream outputStream = this.f0;
        if (outputStream != null) {
            int i = this.i0;
            if (i > 0) {
                outputStream.write(this.g0, 0, i);
                this.i0 = 0;
            }
            OutputStream outputStream2 = this.f0;
            this.f0 = null;
            byte[] bArr = this.g0;
            if (bArr != null) {
                this.g0 = null;
                this.a.t(bArr);
            }
            outputStream2.close();
            int i2 = this.j0;
            this.j0 = 0;
            if (i2 > 0) {
                b(i2);
            }
        }
    }

    @Override // java.io.Writer, java.io.Flushable
    public void flush() throws IOException {
        OutputStream outputStream = this.f0;
        if (outputStream != null) {
            int i = this.i0;
            if (i > 0) {
                outputStream.write(this.g0, 0, i);
                this.i0 = 0;
            }
            this.f0.flush();
        }
    }

    @Override // java.io.Writer
    public void write(char[] cArr) throws IOException {
        write(cArr, 0, cArr.length);
    }

    @Override // java.io.Writer, java.lang.Appendable
    public Writer append(char c) throws IOException {
        write(c);
        return this;
    }

    /* JADX WARN: Code restructure failed: missing block: B:60:0x0025, code lost:
        continue;
     */
    @Override // java.io.Writer
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void write(char[] r8, int r9, int r10) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 228
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.io.g.write(char[], int, int):void");
    }

    @Override // java.io.Writer
    public void write(int i) throws IOException {
        int i2;
        if (this.j0 > 0) {
            i = a(i);
        } else if (i >= 55296 && i <= 57343) {
            if (i > 56319) {
                b(i);
            }
            this.j0 = i;
            return;
        }
        int i3 = this.i0;
        if (i3 >= this.h0) {
            this.f0.write(this.g0, 0, i3);
            this.i0 = 0;
        }
        if (i < 128) {
            byte[] bArr = this.g0;
            int i4 = this.i0;
            this.i0 = i4 + 1;
            bArr[i4] = (byte) i;
            return;
        }
        int i5 = this.i0;
        if (i < 2048) {
            byte[] bArr2 = this.g0;
            int i6 = i5 + 1;
            bArr2[i5] = (byte) ((i >> 6) | 192);
            i2 = i6 + 1;
            bArr2[i6] = (byte) ((i & 63) | 128);
        } else if (i <= 65535) {
            byte[] bArr3 = this.g0;
            int i7 = i5 + 1;
            bArr3[i5] = (byte) ((i >> 12) | 224);
            int i8 = i7 + 1;
            bArr3[i7] = (byte) (((i >> 6) & 63) | 128);
            bArr3[i8] = (byte) ((i & 63) | 128);
            i2 = i8 + 1;
        } else {
            if (i > 1114111) {
                b(i);
            }
            byte[] bArr4 = this.g0;
            int i9 = i5 + 1;
            bArr4[i5] = (byte) ((i >> 18) | 240);
            int i10 = i9 + 1;
            bArr4[i9] = (byte) (((i >> 12) & 63) | 128);
            int i11 = i10 + 1;
            bArr4[i10] = (byte) (((i >> 6) & 63) | 128);
            i2 = i11 + 1;
            bArr4[i11] = (byte) ((i & 63) | 128);
        }
        this.i0 = i2;
    }

    @Override // java.io.Writer
    public void write(String str) throws IOException {
        write(str, 0, str.length());
    }

    /* JADX WARN: Code restructure failed: missing block: B:60:0x0029, code lost:
        continue;
     */
    @Override // java.io.Writer
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void write(java.lang.String r8, int r9, int r10) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 238
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.io.g.write(java.lang.String, int, int):void");
    }
}
