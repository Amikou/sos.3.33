package com.fasterxml.jackson.core.io;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: MergedStream.java */
/* loaded from: classes.dex */
public final class c extends InputStream {
    public final jm1 a;
    public final InputStream f0;
    public byte[] g0;
    public int h0;
    public final int i0;

    public c(jm1 jm1Var, InputStream inputStream, byte[] bArr, int i, int i2) {
        this.a = jm1Var;
        this.f0 = inputStream;
        this.g0 = bArr;
        this.h0 = i;
        this.i0 = i2;
    }

    public final void a() {
        byte[] bArr = this.g0;
        if (bArr != null) {
            this.g0 = null;
            jm1 jm1Var = this.a;
            if (jm1Var != null) {
                jm1Var.r(bArr);
            }
        }
    }

    @Override // java.io.InputStream
    public int available() throws IOException {
        if (this.g0 != null) {
            return this.i0 - this.h0;
        }
        return this.f0.available();
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        a();
        this.f0.close();
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        if (this.g0 == null) {
            this.f0.mark(i);
        }
    }

    @Override // java.io.InputStream
    public boolean markSupported() {
        return this.g0 == null && this.f0.markSupported();
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        byte[] bArr = this.g0;
        if (bArr != null) {
            int i = this.h0;
            int i2 = i + 1;
            this.h0 = i2;
            int i3 = bArr[i] & 255;
            if (i2 >= this.i0) {
                a();
            }
            return i3;
        }
        return this.f0.read();
    }

    @Override // java.io.InputStream
    public void reset() throws IOException {
        if (this.g0 == null) {
            this.f0.reset();
        }
    }

    @Override // java.io.InputStream
    public long skip(long j) throws IOException {
        long j2;
        if (this.g0 != null) {
            int i = this.i0;
            int i2 = this.h0;
            long j3 = i - i2;
            if (j3 > j) {
                this.h0 = i2 + ((int) j);
                return j;
            }
            a();
            j2 = j3 + 0;
            j -= j3;
        } else {
            j2 = 0;
        }
        return j > 0 ? j2 + this.f0.skip(j) : j2;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        byte[] bArr2 = this.g0;
        if (bArr2 != null) {
            int i3 = this.i0;
            int i4 = this.h0;
            int i5 = i3 - i4;
            if (i2 > i5) {
                i2 = i5;
            }
            System.arraycopy(bArr2, i4, bArr, i, i2);
            int i6 = this.h0 + i2;
            this.h0 = i6;
            if (i6 >= this.i0) {
                a();
            }
            return i2;
        }
        return this.f0.read(bArr, i, i2);
    }
}
