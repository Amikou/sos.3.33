package com.fasterxml.jackson.core.io;

import java.math.BigDecimal;

/* compiled from: NumberInput.java */
/* loaded from: classes.dex */
public final class d {
    public static final String a = String.valueOf(Long.MIN_VALUE).substring(1);
    public static final String b = String.valueOf(Long.MAX_VALUE);

    public static NumberFormatException a(String str) {
        return new NumberFormatException("Value \"" + str + "\" can not be represented as BigDecimal");
    }

    public static boolean b(String str, boolean z) {
        String str2 = z ? a : b;
        int length = str2.length();
        int length2 = str.length();
        if (length2 < length) {
            return true;
        }
        if (length2 > length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            int charAt = str.charAt(i) - str2.charAt(i);
            if (charAt != 0) {
                return charAt < 0;
            }
        }
        return true;
    }

    public static boolean c(char[] cArr, int i, int i2, boolean z) {
        String str = z ? a : b;
        int length = str.length();
        if (i2 < length) {
            return true;
        }
        if (i2 > length) {
            return false;
        }
        for (int i3 = 0; i3 < length; i3++) {
            int charAt = cArr[i + i3] - str.charAt(i3);
            if (charAt != 0) {
                return charAt < 0;
            }
        }
        return true;
    }

    public static int d(String str, int i) {
        String trim;
        int length;
        if (str == null || (length = (trim = str.trim()).length()) == 0) {
            return i;
        }
        int i2 = 0;
        if (length > 0) {
            char charAt = trim.charAt(0);
            if (charAt == '+') {
                trim = trim.substring(1);
                length = trim.length();
            } else if (charAt == '-') {
                i2 = 1;
            }
        }
        while (i2 < length) {
            char charAt2 = trim.charAt(i2);
            if (charAt2 > '9' || charAt2 < '0') {
                try {
                    return (int) i(trim);
                } catch (NumberFormatException unused) {
                    return i;
                }
            }
            i2++;
        }
        try {
            return Integer.parseInt(trim);
        } catch (NumberFormatException unused2) {
            return i;
        }
    }

    public static long e(String str, long j) {
        String trim;
        int length;
        if (str == null || (length = (trim = str.trim()).length()) == 0) {
            return j;
        }
        int i = 0;
        if (length > 0) {
            char charAt = trim.charAt(0);
            if (charAt == '+') {
                trim = trim.substring(1);
                length = trim.length();
            } else if (charAt == '-') {
                i = 1;
            }
        }
        while (i < length) {
            char charAt2 = trim.charAt(i);
            if (charAt2 > '9' || charAt2 < '0') {
                try {
                    return (long) i(trim);
                } catch (NumberFormatException unused) {
                    return j;
                }
            }
            i++;
        }
        try {
            return Long.parseLong(trim);
        } catch (NumberFormatException unused2) {
            return j;
        }
    }

    public static BigDecimal f(String str) throws NumberFormatException {
        try {
            return new BigDecimal(str);
        } catch (NumberFormatException unused) {
            throw a(str);
        }
    }

    public static BigDecimal g(char[] cArr) throws NumberFormatException {
        return h(cArr, 0, cArr.length);
    }

    public static BigDecimal h(char[] cArr, int i, int i2) throws NumberFormatException {
        try {
            return new BigDecimal(cArr, i, i2);
        } catch (NumberFormatException unused) {
            throw a(new String(cArr, i, i2));
        }
    }

    public static double i(String str) throws NumberFormatException {
        if ("2.2250738585072012e-308".equals(str)) {
            return Double.MIN_VALUE;
        }
        return Double.parseDouble(str);
    }

    /* JADX WARN: Code restructure failed: missing block: B:45:0x0075, code lost:
        return java.lang.Integer.parseInt(r9);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int j(java.lang.String r9) {
        /*
            r0 = 0
            char r1 = r9.charAt(r0)
            int r2 = r9.length()
            r3 = 1
            r4 = 45
            if (r1 != r4) goto Lf
            r0 = r3
        Lf:
            r4 = 10
            if (r0 == 0) goto L26
            if (r2 == r3) goto L21
            if (r2 <= r4) goto L18
            goto L21
        L18:
            r1 = 2
            char r3 = r9.charAt(r3)
            r8 = r3
            r3 = r1
            r1 = r8
            goto L2f
        L21:
            int r9 = java.lang.Integer.parseInt(r9)
            return r9
        L26:
            r5 = 9
            if (r2 <= r5) goto L2f
            int r9 = java.lang.Integer.parseInt(r9)
            return r9
        L2f:
            r5 = 57
            if (r1 > r5) goto L84
            r6 = 48
            if (r1 >= r6) goto L38
            goto L84
        L38:
            int r1 = r1 - r6
            if (r3 >= r2) goto L80
            int r7 = r3 + 1
            char r3 = r9.charAt(r3)
            if (r3 > r5) goto L7b
            if (r3 >= r6) goto L46
            goto L7b
        L46:
            int r1 = r1 * 10
            int r3 = r3 - r6
            int r1 = r1 + r3
            if (r7 >= r2) goto L80
            int r3 = r7 + 1
            char r7 = r9.charAt(r7)
            if (r7 > r5) goto L76
            if (r7 >= r6) goto L57
            goto L76
        L57:
            int r1 = r1 * 10
            int r7 = r7 - r6
            int r1 = r1 + r7
            if (r3 >= r2) goto L80
        L5d:
            int r7 = r3 + 1
            char r3 = r9.charAt(r3)
            if (r3 > r5) goto L71
            if (r3 >= r6) goto L68
            goto L71
        L68:
            int r1 = r1 * r4
            int r3 = r3 + (-48)
            int r1 = r1 + r3
            if (r7 < r2) goto L6f
            goto L80
        L6f:
            r3 = r7
            goto L5d
        L71:
            int r9 = java.lang.Integer.parseInt(r9)
            return r9
        L76:
            int r9 = java.lang.Integer.parseInt(r9)
            return r9
        L7b:
            int r9 = java.lang.Integer.parseInt(r9)
            return r9
        L80:
            if (r0 == 0) goto L83
            int r1 = -r1
        L83:
            return r1
        L84:
            int r9 = java.lang.Integer.parseInt(r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.io.d.j(java.lang.String):int");
    }

    public static int k(char[] cArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8 = cArr[i] - '0';
        if (i2 > 4) {
            i = i + 1 + 1 + 1 + 1;
            i8 = (((((((i8 * 10) + (cArr[i3] - '0')) * 10) + (cArr[i4] - '0')) * 10) + (cArr[i5] - '0')) * 10) + (cArr[i] - '0');
            i2 -= 4;
            if (i2 > 4) {
                int i9 = i + 1 + 1 + 1;
                return (((((((i8 * 10) + (cArr[i6] - '0')) * 10) + (cArr[i7] - '0')) * 10) + (cArr[i9] - '0')) * 10) + (cArr[i9 + 1] - '0');
            }
        }
        if (i2 > 1) {
            int i10 = i + 1;
            int i11 = (i8 * 10) + (cArr[i10] - '0');
            if (i2 > 2) {
                int i12 = i10 + 1;
                int i13 = (i11 * 10) + (cArr[i12] - '0');
                return i2 > 3 ? (i13 * 10) + (cArr[i12 + 1] - '0') : i13;
            }
            return i11;
        }
        return i8;
    }

    public static long l(String str) {
        if (str.length() <= 9) {
            return j(str);
        }
        return Long.parseLong(str);
    }

    public static long m(char[] cArr, int i, int i2) {
        int i3 = i2 - 9;
        return (k(cArr, i, i3) * 1000000000) + k(cArr, i + i3, 9);
    }
}
