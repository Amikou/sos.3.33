package com.fasterxml.jackson.core.io;

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;

/* loaded from: classes.dex */
public abstract class InputDecorator implements Serializable {
    private static final long serialVersionUID = 1;

    public DataInput decorate(jm1 jm1Var, DataInput dataInput) throws IOException {
        throw new UnsupportedOperationException();
    }

    public abstract InputStream decorate(jm1 jm1Var, InputStream inputStream) throws IOException;

    public abstract InputStream decorate(jm1 jm1Var, byte[] bArr, int i, int i2) throws IOException;

    public abstract Reader decorate(jm1 jm1Var, Reader reader) throws IOException;
}
