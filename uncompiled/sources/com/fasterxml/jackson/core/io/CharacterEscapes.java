package com.fasterxml.jackson.core.io;

import java.io.Serializable;
import java.util.Arrays;

/* loaded from: classes.dex */
public abstract class CharacterEscapes implements Serializable {
    public static final int ESCAPE_CUSTOM = -2;
    public static final int ESCAPE_NONE = 0;
    public static final int ESCAPE_STANDARD = -1;

    public static int[] standardAsciiEscapesForJSON() {
        int[] e = a.e();
        return Arrays.copyOf(e, e.length);
    }

    public abstract int[] getEscapeCodesForAscii();

    public abstract yl3 getEscapeSequence(int i);
}
