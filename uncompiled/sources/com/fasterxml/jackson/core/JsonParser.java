package com.fasterxml.jackson.core;

import com.fasterxml.jackson.core.util.RequestPayload;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/* loaded from: classes.dex */
public abstract class JsonParser implements Closeable {
    public int a;
    public transient RequestPayload f0;

    /* loaded from: classes.dex */
    public enum Feature {
        AUTO_CLOSE_SOURCE(true),
        ALLOW_COMMENTS(false),
        ALLOW_YAML_COMMENTS(false),
        ALLOW_UNQUOTED_FIELD_NAMES(false),
        ALLOW_SINGLE_QUOTES(false),
        ALLOW_UNQUOTED_CONTROL_CHARS(false),
        ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER(false),
        ALLOW_NUMERIC_LEADING_ZEROS(false),
        ALLOW_NON_NUMERIC_NUMBERS(false),
        STRICT_DUPLICATE_DETECTION(false),
        IGNORE_UNDEFINED(false),
        ALLOW_MISSING_VALUES(false);
        
        private final boolean _defaultState;
        private final int _mask = 1 << ordinal();

        Feature(boolean z) {
            this._defaultState = z;
        }

        public static int collectDefaults() {
            Feature[] values;
            int i = 0;
            for (Feature feature : values()) {
                if (feature.enabledByDefault()) {
                    i |= feature.getMask();
                }
            }
            return i;
        }

        public boolean enabledByDefault() {
            return this._defaultState;
        }

        public boolean enabledIn(int i) {
            return (i & this._mask) != 0;
        }

        public int getMask() {
            return this._mask;
        }
    }

    /* loaded from: classes.dex */
    public enum NumberType {
        INT,
        LONG,
        BIG_INTEGER,
        FLOAT,
        DOUBLE,
        BIG_DECIMAL
    }

    public JsonParser() {
    }

    public abstract float A() throws IOException;

    public abstract boolean B0();

    public Object C() {
        return null;
    }

    public abstract int F() throws IOException;

    public abstract boolean F0(JsonToken jsonToken);

    public abstract boolean H0(int i);

    public boolean J0(Feature feature) {
        return feature.enabledIn(this.a);
    }

    public boolean K0() {
        return f() == JsonToken.START_ARRAY;
    }

    public boolean L0() {
        return f() == JsonToken.START_OBJECT;
    }

    public abstract long M() throws IOException;

    public abstract NumberType N() throws IOException;

    public String O0() throws IOException {
        if (T0() == JsonToken.FIELD_NAME) {
            return r();
        }
        return null;
    }

    public abstract Number Q() throws IOException;

    public Object R() throws IOException {
        return null;
    }

    public abstract cw1 S();

    public String S0() throws IOException {
        if (T0() == JsonToken.VALUE_STRING) {
            return X();
        }
        return null;
    }

    public abstract JsonToken T0() throws IOException;

    public abstract JsonToken U0() throws IOException;

    public short W() throws IOException {
        int F = F();
        if (F < -32768 || F > 32767) {
            throw a("Numeric value (" + X() + ") out of range of Java short");
        }
        return (short) F;
    }

    public JsonParser W0(int i, int i2) {
        throw new IllegalArgumentException("No FormatFeatures defined for parser of type " + getClass().getName());
    }

    public abstract String X() throws IOException;

    public JsonParser X0(int i, int i2) {
        return f1((i & i2) | (this.a & (~i2)));
    }

    public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException {
        b();
        return 0;
    }

    public JsonParseException a(String str) {
        return new JsonParseException(this, str).withRequestPayload(this.f0);
    }

    public abstract char[] a0() throws IOException;

    public void b() {
        throw new UnsupportedOperationException("Operation not supported by parser of type " + getClass().getName());
    }

    public abstract int b0() throws IOException;

    public boolean c() {
        return false;
    }

    public boolean c1() {
        return false;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public abstract void close() throws IOException;

    public boolean d() {
        return false;
    }

    public abstract void e();

    public abstract int e0() throws IOException;

    public void e1(Object obj) {
        cw1 S = S();
        if (S != null) {
            S.h(obj);
        }
    }

    public JsonToken f() {
        return u();
    }

    public abstract JsonLocation f0();

    @Deprecated
    public JsonParser f1(int i) {
        this.a = i;
        return this;
    }

    public JsonParser g(Feature feature) {
        this.a = feature.getMask() | this.a;
        return this;
    }

    public Object g0() throws IOException {
        return null;
    }

    public abstract BigInteger h() throws IOException;

    public byte[] i() throws IOException {
        return j(a.a());
    }

    public int i0() throws IOException {
        return m0(0);
    }

    public void i1(z81 z81Var) {
        throw new UnsupportedOperationException("Parser of type " + getClass().getName() + " does not support schema of type '" + z81Var.a() + "'");
    }

    public abstract byte[] j(Base64Variant base64Variant) throws IOException;

    public abstract JsonParser k1() throws IOException;

    public boolean l() throws IOException {
        JsonToken f = f();
        if (f == JsonToken.VALUE_TRUE) {
            return true;
        }
        if (f == JsonToken.VALUE_FALSE) {
            return false;
        }
        throw new JsonParseException(this, String.format("Current token (%s) not of boolean type", f)).withRequestPayload(this.f0);
    }

    public byte m() throws IOException {
        int F = F();
        if (F < -128 || F > 255) {
            throw a("Numeric value (" + X() + ") out of range of Java byte");
        }
        return (byte) F;
    }

    public int m0(int i) throws IOException {
        return i;
    }

    public abstract c n();

    public abstract JsonLocation q();

    public abstract String r() throws IOException;

    public long r0() throws IOException {
        return w0(0L);
    }

    public abstract JsonToken u();

    public abstract int v();

    public abstract BigDecimal w() throws IOException;

    public long w0(long j) throws IOException {
        return j;
    }

    public abstract double x() throws IOException;

    public String x0() throws IOException {
        return y0(null);
    }

    public abstract String y0(String str) throws IOException;

    public Object z() throws IOException {
        return null;
    }

    public abstract boolean z0();

    public JsonParser(int i) {
        this.a = i;
    }
}
