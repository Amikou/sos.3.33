package com.fasterxml.jackson.core;

import java.io.IOException;
import java.util.Iterator;

/* compiled from: ObjectCodec.java */
/* loaded from: classes.dex */
public abstract class c extends e {
    @Override // com.fasterxml.jackson.core.e
    public abstract f createArrayNode();

    @Override // com.fasterxml.jackson.core.e
    public abstract f createObjectNode();

    public JsonFactory getFactory() {
        return getJsonFactory();
    }

    @Deprecated
    public JsonFactory getJsonFactory() {
        return getFactory();
    }

    @Override // com.fasterxml.jackson.core.e
    public abstract <T extends f> T readTree(JsonParser jsonParser) throws IOException;

    public abstract <T> T readValue(JsonParser jsonParser, Class<T> cls) throws IOException;

    public abstract <T> T readValue(JsonParser jsonParser, r73 r73Var) throws IOException;

    public abstract <T> T readValue(JsonParser jsonParser, ud4<?> ud4Var) throws IOException;

    public abstract <T> Iterator<T> readValues(JsonParser jsonParser, Class<T> cls) throws IOException;

    public abstract <T> Iterator<T> readValues(JsonParser jsonParser, r73 r73Var) throws IOException;

    public abstract <T> Iterator<T> readValues(JsonParser jsonParser, ud4<?> ud4Var) throws IOException;

    @Override // com.fasterxml.jackson.core.e
    public abstract JsonParser treeAsTokens(f fVar);

    public abstract <T> T treeToValue(f fVar, Class<T> cls) throws JsonProcessingException;

    public abstract Version version();

    @Override // com.fasterxml.jackson.core.e
    public abstract void writeTree(JsonGenerator jsonGenerator, f fVar) throws IOException;

    public abstract void writeValue(JsonGenerator jsonGenerator, Object obj) throws IOException;
}
