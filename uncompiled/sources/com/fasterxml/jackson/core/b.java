package com.fasterxml.jackson.core;

/* compiled from: JsonPointer.java */
/* loaded from: classes.dex */
public class b {
    public static final b e = new b();
    public final b a;
    public final String b;
    public final String c;
    public final int d;

    public b() {
        this.a = null;
        this.c = "";
        this.d = -1;
        this.b = "";
    }

    public static void a(StringBuilder sb, char c) {
        if (c == '0') {
            c = '~';
        } else if (c == '1') {
            c = '/';
        } else {
            sb.append('~');
        }
        sb.append(c);
    }

    public static final int b(String str) {
        int length = str.length();
        if (length == 0 || length > 10) {
            return -1;
        }
        char charAt = str.charAt(0);
        if (charAt <= '0') {
            return (length == 1 && charAt == '0') ? 0 : -1;
        } else if (charAt > '9') {
            return -1;
        } else {
            for (int i = 1; i < length; i++) {
                char charAt2 = str.charAt(i);
                if (charAt2 > '9' || charAt2 < '0') {
                    return -1;
                }
            }
            if (length != 10 || com.fasterxml.jackson.core.io.d.l(str) <= 2147483647L) {
                return com.fasterxml.jackson.core.io.d.j(str);
            }
            return -1;
        }
    }

    public static b c(String str, int i) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(Math.max(16, length));
        if (i > 2) {
            sb.append((CharSequence) str, 1, i - 1);
        }
        int i2 = i + 1;
        a(sb, str.charAt(i));
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (charAt == '/') {
                return new b(str, sb.toString(), d(str.substring(i2)));
            }
            i2++;
            if (charAt == '~' && i2 < length) {
                a(sb, str.charAt(i2));
                i2++;
            } else {
                sb.append(charAt);
            }
        }
        return new b(str, sb.toString(), e);
    }

    public static b d(String str) {
        int length = str.length();
        int i = 1;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt == '/') {
                return new b(str, str.substring(1, i), d(str.substring(i)));
            }
            i++;
            if (charAt == '~' && i < length) {
                return c(str, i);
            }
        }
        return new b(str, str.substring(1), e);
    }

    public static b e(String str) throws IllegalArgumentException {
        if (str != null && str.length() != 0) {
            if (str.charAt(0) == '/') {
                return d(str);
            }
            throw new IllegalArgumentException("Invalid input: JSON Pointer expression must start with '/': \"" + str + "\"");
        }
        return e;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof b)) {
            return this.b.equals(((b) obj).b);
        }
        return false;
    }

    public b f(int i) {
        if (i != this.d || i < 0) {
            return null;
        }
        return this.a;
    }

    public b g(String str) {
        if (this.a == null || !this.c.equals(str)) {
            return null;
        }
        return this.a;
    }

    public boolean h() {
        return this.a == null;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        return this.b;
    }

    public b(String str, String str2, b bVar) {
        this.b = str;
        this.a = bVar;
        this.c = str2;
        this.d = b(str2);
    }
}
