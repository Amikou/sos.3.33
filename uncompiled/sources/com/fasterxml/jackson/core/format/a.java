package com.fasterxml.jackson.core.format;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: InputAccessor.java */
/* loaded from: classes.dex */
public interface a {
    byte e() throws IOException;

    boolean f() throws IOException;

    /* compiled from: InputAccessor.java */
    /* renamed from: com.fasterxml.jackson.core.format.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0090a implements a {
        public final InputStream a;
        public final byte[] b;
        public final int c;
        public int d;
        public int e;

        public C0090a(InputStream inputStream, byte[] bArr) {
            this.a = inputStream;
            this.b = bArr;
            this.c = 0;
            this.e = 0;
            this.d = 0;
        }

        public void a() {
            this.e = this.c;
        }

        @Override // com.fasterxml.jackson.core.format.a
        public byte e() throws IOException {
            if (this.e >= this.d && !f()) {
                throw new EOFException("Failed auto-detect: could not read more than " + this.e + " bytes (max buffer size: " + this.b.length + ")");
            }
            byte[] bArr = this.b;
            int i = this.e;
            this.e = i + 1;
            return bArr[i];
        }

        @Override // com.fasterxml.jackson.core.format.a
        public boolean f() throws IOException {
            int read;
            int i = this.e;
            if (i < this.d) {
                return true;
            }
            InputStream inputStream = this.a;
            if (inputStream == null) {
                return false;
            }
            byte[] bArr = this.b;
            int length = bArr.length - i;
            if (length >= 1 && (read = inputStream.read(bArr, i, length)) > 0) {
                this.d += read;
                return true;
            }
            return false;
        }

        public C0090a(byte[] bArr, int i, int i2) {
            this.a = null;
            this.b = bArr;
            this.e = i;
            this.c = i;
            this.d = i + i2;
        }
    }
}
