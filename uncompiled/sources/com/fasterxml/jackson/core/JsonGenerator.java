package com.fasterxml.jackson.core;

import com.fasterxml.jackson.core.io.CharacterEscapes;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* loaded from: classes.dex */
public abstract class JsonGenerator implements Closeable, Flushable {
    public d a;

    /* loaded from: classes.dex */
    public enum Feature {
        AUTO_CLOSE_TARGET(true),
        AUTO_CLOSE_JSON_CONTENT(true),
        FLUSH_PASSED_TO_STREAM(true),
        QUOTE_FIELD_NAMES(true),
        QUOTE_NON_NUMERIC_NUMBERS(true),
        WRITE_NUMBERS_AS_STRINGS(false),
        WRITE_BIGDECIMAL_AS_PLAIN(false),
        ESCAPE_NON_ASCII(false),
        STRICT_DUPLICATE_DETECTION(false),
        IGNORE_UNKNOWN(false);
        
        private final boolean _defaultState;
        private final int _mask = 1 << ordinal();

        Feature(boolean z) {
            this._defaultState = z;
        }

        public static int collectDefaults() {
            Feature[] values;
            int i = 0;
            for (Feature feature : values()) {
                if (feature.enabledByDefault()) {
                    i |= feature.getMask();
                }
            }
            return i;
        }

        public boolean enabledByDefault() {
            return this._defaultState;
        }

        public boolean enabledIn(int i) {
            return (i & this._mask) != 0;
        }

        public int getMask() {
            return this._mask;
        }
    }

    public void A(z81 z81Var) {
        throw new UnsupportedOperationException("Generator of type " + getClass().getName() + " does not support schema of type '" + z81Var.a() + "'");
    }

    public abstract void B0(BigDecimal bigDecimal) throws IOException;

    public void C(double[] dArr, int i, int i2) throws IOException {
        if (dArr != null) {
            c(dArr.length, i, i2);
            e1();
            int i3 = i2 + i;
            while (i < i3) {
                r0(dArr[i]);
                i++;
            }
            e0();
            return;
        }
        throw new IllegalArgumentException("null array");
    }

    public void F(int[] iArr, int i, int i2) throws IOException {
        if (iArr != null) {
            c(iArr.length, i, i2);
            e1();
            int i3 = i2 + i;
            while (i < i3) {
                x0(iArr[i]);
                i++;
            }
            e0();
            return;
        }
        throw new IllegalArgumentException("null array");
    }

    public abstract void F0(BigInteger bigInteger) throws IOException;

    public void H0(short s) throws IOException {
        x0(s);
    }

    public abstract void J0(Object obj) throws IOException;

    public final void K0(String str) throws IOException {
        i0(str);
        i1();
    }

    public void L0(Object obj) throws IOException {
        throw new JsonGenerationException("No native support for writing Object Ids", this);
    }

    public void M(long[] jArr, int i, int i2) throws IOException {
        if (jArr != null) {
            c(jArr.length, i, i2);
            e1();
            int i3 = i2 + i;
            while (i < i3) {
                y0(jArr[i]);
                i++;
            }
            e0();
            return;
        }
        throw new IllegalArgumentException("null array");
    }

    public final void N(String str) throws IOException {
        i0(str);
        e1();
    }

    public void O0(Object obj) throws IOException {
        throw new JsonGenerationException("No native support for writing Object Ids", this);
    }

    public abstract int Q(Base64Variant base64Variant, InputStream inputStream, int i) throws IOException;

    public int R(InputStream inputStream, int i) throws IOException {
        return Q(a.a(), inputStream, i);
    }

    public abstract void S(Base64Variant base64Variant, byte[] bArr, int i, int i2) throws IOException;

    public void S0(String str) throws IOException {
    }

    public abstract void T0(char c) throws IOException;

    public void U0(yl3 yl3Var) throws IOException {
        W0(yl3Var.getValue());
    }

    public void W(byte[] bArr) throws IOException {
        S(a.a(), bArr, 0, bArr.length);
    }

    public abstract void W0(String str) throws IOException;

    public void X(byte[] bArr, int i, int i2) throws IOException {
        S(a.a(), bArr, i, i2);
    }

    public abstract void X0(char[] cArr, int i, int i2) throws IOException;

    public void Z0(yl3 yl3Var) throws IOException {
        c1(yl3Var.getValue());
    }

    public void a(String str) throws JsonGenerationException {
        throw new JsonGenerationException(str, this);
    }

    public abstract void a0(boolean z) throws IOException;

    public final void b() {
        gh4.c();
    }

    public void b0(Object obj) throws IOException {
        if (obj == null) {
            m0();
        } else if (obj instanceof byte[]) {
            W((byte[]) obj);
        } else {
            throw new JsonGenerationException("No native support for writing embedded objects of type " + obj.getClass().getName(), this);
        }
    }

    public final void c(int i, int i2, int i3) {
        if (i2 < 0 || i2 + i3 > i) {
            throw new IllegalArgumentException(String.format("invalid argument(s) (offset=%d, length=%d) for input array of %d element", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i)));
        }
    }

    public abstract void c1(String str) throws IOException;

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public abstract void close() throws IOException;

    public void d(Object obj) throws IOException {
        if (obj == null) {
            m0();
        } else if (obj instanceof String) {
            o1((String) obj);
        } else {
            if (obj instanceof Number) {
                Number number = (Number) obj;
                if (number instanceof Integer) {
                    x0(number.intValue());
                    return;
                } else if (number instanceof Long) {
                    y0(number.longValue());
                    return;
                } else if (number instanceof Double) {
                    r0(number.doubleValue());
                    return;
                } else if (number instanceof Float) {
                    w0(number.floatValue());
                    return;
                } else if (number instanceof Short) {
                    H0(number.shortValue());
                    return;
                } else if (number instanceof Byte) {
                    H0(number.byteValue());
                    return;
                } else if (number instanceof BigInteger) {
                    F0((BigInteger) number);
                    return;
                } else if (number instanceof BigDecimal) {
                    B0((BigDecimal) number);
                    return;
                } else if (number instanceof AtomicInteger) {
                    x0(((AtomicInteger) number).get());
                    return;
                } else if (number instanceof AtomicLong) {
                    y0(((AtomicLong) number).get());
                    return;
                }
            } else if (obj instanceof byte[]) {
                W((byte[]) obj);
                return;
            } else if (obj instanceof Boolean) {
                a0(((Boolean) obj).booleanValue());
                return;
            } else if (obj instanceof AtomicBoolean) {
                a0(((AtomicBoolean) obj).get());
                return;
            }
            throw new IllegalStateException("No ObjectCodec defined for the generator, can only serialize simple wrapper types (type passed " + obj.getClass().getName() + ")");
        }
    }

    public boolean e() {
        return true;
    }

    public abstract void e0() throws IOException;

    public abstract void e1() throws IOException;

    public boolean f() {
        return false;
    }

    public abstract void f0() throws IOException;

    public void f1(int i) throws IOException {
        e1();
    }

    @Override // java.io.Flushable
    public abstract void flush() throws IOException;

    public boolean g() {
        return false;
    }

    public abstract void g0(yl3 yl3Var) throws IOException;

    public boolean h() {
        return false;
    }

    public abstract JsonGenerator i(Feature feature);

    public abstract void i0(String str) throws IOException;

    public abstract void i1() throws IOException;

    public abstract int j();

    public void k1(Object obj) throws IOException {
        i1();
        u(obj);
    }

    public abstract cw1 l();

    public d m() {
        return this.a;
    }

    public abstract void m0() throws IOException;

    public abstract void m1(yl3 yl3Var) throws IOException;

    public JsonGenerator n(int i, int i2) {
        throw new IllegalArgumentException("No FormatFeatures defined for generator of type " + getClass().getName());
    }

    public abstract void o1(String str) throws IOException;

    public abstract void p1(char[] cArr, int i, int i2) throws IOException;

    public JsonGenerator q(int i, int i2) {
        return v((i & i2) | (j() & (~i2)));
    }

    public JsonGenerator r(CharacterEscapes characterEscapes) {
        return this;
    }

    public abstract void r0(double d) throws IOException;

    public void r1(String str, String str2) throws IOException {
        i0(str);
        o1(str2);
    }

    public void s1(Object obj) throws IOException {
        throw new JsonGenerationException("No native support for writing Type Ids", this);
    }

    public void u(Object obj) {
        cw1 l = l();
        if (l != null) {
            l.h(obj);
        }
    }

    @Deprecated
    public abstract JsonGenerator v(int i);

    public abstract JsonGenerator w(int i);

    public abstract void w0(float f) throws IOException;

    public JsonGenerator x(d dVar) {
        this.a = dVar;
        return this;
    }

    public abstract void x0(int i) throws IOException;

    public abstract void y0(long j) throws IOException;

    public JsonGenerator z(yl3 yl3Var) {
        throw new UnsupportedOperationException();
    }

    public abstract void z0(String str) throws IOException;
}
