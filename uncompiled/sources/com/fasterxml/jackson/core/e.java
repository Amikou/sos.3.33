package com.fasterxml.jackson.core;

import java.io.IOException;

/* compiled from: TreeCodec.java */
/* loaded from: classes.dex */
public abstract class e {
    public abstract f createArrayNode();

    public abstract f createObjectNode();

    public abstract <T extends f> T readTree(JsonParser jsonParser) throws IOException, JsonProcessingException;

    public abstract JsonParser treeAsTokens(f fVar);

    public abstract void writeTree(JsonGenerator jsonGenerator, f fVar) throws IOException, JsonProcessingException;
}
