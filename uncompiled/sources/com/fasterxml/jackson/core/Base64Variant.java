package com.fasterxml.jackson.core;

import java.io.Serializable;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class Base64Variant implements Serializable {
    public static final int BASE64_VALUE_INVALID = -1;
    public static final int BASE64_VALUE_PADDING = -2;
    public static final char PADDING_CHAR_NONE = 0;
    private static final long serialVersionUID = 1;
    public final String _name;
    public final transient int[] a;
    public final transient char[] f0;
    public final transient byte[] g0;
    public final transient boolean h0;
    public final transient char i0;
    public final transient int j0;

    public Base64Variant(String str, String str2, boolean z, char c, int i) {
        int[] iArr = new int[128];
        this.a = iArr;
        char[] cArr = new char[64];
        this.f0 = cArr;
        this.g0 = new byte[64];
        this._name = str;
        this.h0 = z;
        this.i0 = c;
        this.j0 = i;
        int length = str2.length();
        if (length == 64) {
            str2.getChars(0, length, cArr, 0);
            Arrays.fill(iArr, -1);
            for (int i2 = 0; i2 < length; i2++) {
                char c2 = this.f0[i2];
                this.g0[i2] = (byte) c2;
                this.a[c2] = i2;
            }
            if (z) {
                this.a[c] = -2;
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Base64Alphabet length must be exactly 64 (was " + length + ")");
    }

    public void _reportBase64EOF() throws IllegalArgumentException {
        throw new IllegalArgumentException("Unexpected end-of-String in base64 content");
    }

    public void _reportInvalidBase64(char c, int i, String str) throws IllegalArgumentException {
        String str2;
        if (c <= ' ') {
            str2 = "Illegal white space character (code 0x" + Integer.toHexString(c) + ") as character #" + (i + 1) + " of 4-char base64 unit: can only used between units";
        } else if (usesPaddingChar(c)) {
            str2 = "Unexpected padding character ('" + getPaddingChar() + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (Character.isDefined(c) && !Character.isISOControl(c)) {
            str2 = "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
        } else {
            str2 = "Illegal character (code 0x" + Integer.toHexString(c) + ") in base64 content";
        }
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        throw new IllegalArgumentException(str2);
    }

    public byte[] decode(String str) throws IllegalArgumentException {
        ms msVar = new ms();
        decode(str, msVar);
        return msVar.n();
    }

    public int decodeBase64Byte(byte b) {
        if (b < 0) {
            return -1;
        }
        return this.a[b];
    }

    public int decodeBase64Char(char c) {
        if (c <= 127) {
            return this.a[c];
        }
        return -1;
    }

    public String encode(byte[] bArr) {
        return encode(bArr, false);
    }

    public byte encodeBase64BitsAsByte(int i) {
        return this.g0[i];
    }

    public char encodeBase64BitsAsChar(int i) {
        return this.f0[i];
    }

    public int encodeBase64Chunk(int i, char[] cArr, int i2) {
        int i3 = i2 + 1;
        char[] cArr2 = this.f0;
        cArr[i2] = cArr2[(i >> 18) & 63];
        int i4 = i3 + 1;
        cArr[i3] = cArr2[(i >> 12) & 63];
        int i5 = i4 + 1;
        cArr[i4] = cArr2[(i >> 6) & 63];
        int i6 = i5 + 1;
        cArr[i5] = cArr2[i & 63];
        return i6;
    }

    public int encodeBase64Partial(int i, int i2, char[] cArr, int i3) {
        int i4 = i3 + 1;
        char[] cArr2 = this.f0;
        cArr[i3] = cArr2[(i >> 18) & 63];
        int i5 = i4 + 1;
        cArr[i4] = cArr2[(i >> 12) & 63];
        if (this.h0) {
            int i6 = i5 + 1;
            cArr[i5] = i2 == 2 ? cArr2[(i >> 6) & 63] : this.i0;
            int i7 = i6 + 1;
            cArr[i6] = this.i0;
            return i7;
        } else if (i2 == 2) {
            int i8 = i5 + 1;
            cArr[i5] = cArr2[(i >> 6) & 63];
            return i8;
        } else {
            return i5;
        }
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public int getMaxLineLength() {
        return this.j0;
    }

    public String getName() {
        return this._name;
    }

    public byte getPaddingByte() {
        return (byte) this.i0;
    }

    public char getPaddingChar() {
        return this.i0;
    }

    public int hashCode() {
        return this._name.hashCode();
    }

    public Object readResolve() {
        return a.b(this._name);
    }

    public String toString() {
        return this._name;
    }

    public boolean usesPadding() {
        return this.h0;
    }

    public boolean usesPaddingChar(char c) {
        return c == this.i0;
    }

    public int decodeBase64Char(int i) {
        if (i <= 127) {
            return this.a[i];
        }
        return -1;
    }

    public String encode(byte[] bArr, boolean z) {
        int length = bArr.length;
        StringBuilder sb = new StringBuilder((length >> 2) + length + (length >> 3));
        if (z) {
            sb.append('\"');
        }
        int maxLineLength = getMaxLineLength() >> 2;
        int i = 0;
        int i2 = length - 3;
        while (i <= i2) {
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = i4 + 1;
            encodeBase64Chunk(sb, (((bArr[i] << 8) | (bArr[i3] & 255)) << 8) | (bArr[i4] & 255));
            maxLineLength--;
            if (maxLineLength <= 0) {
                sb.append('\\');
                sb.append('n');
                maxLineLength = getMaxLineLength() >> 2;
            }
            i = i5;
        }
        int i6 = length - i;
        if (i6 > 0) {
            int i7 = i + 1;
            int i8 = bArr[i] << 16;
            if (i6 == 2) {
                i8 |= (bArr[i7] & 255) << 8;
            }
            encodeBase64Partial(sb, i8, i6);
        }
        if (z) {
            sb.append('\"');
        }
        return sb.toString();
    }

    public boolean usesPaddingChar(int i) {
        return i == this.i0;
    }

    public void decode(String str, ms msVar) throws IllegalArgumentException {
        int i;
        char charAt;
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            while (true) {
                i = i2 + 1;
                charAt = str.charAt(i2);
                if (i >= length || charAt > ' ') {
                    break;
                }
                i2 = i;
            }
            int decodeBase64Char = decodeBase64Char(charAt);
            if (decodeBase64Char < 0) {
                _reportInvalidBase64(charAt, 0, null);
            }
            if (i >= length) {
                _reportBase64EOF();
            }
            int i3 = i + 1;
            char charAt2 = str.charAt(i);
            int decodeBase64Char2 = decodeBase64Char(charAt2);
            if (decodeBase64Char2 < 0) {
                _reportInvalidBase64(charAt2, 1, null);
            }
            int i4 = (decodeBase64Char << 6) | decodeBase64Char2;
            if (i3 >= length) {
                if (!usesPadding()) {
                    msVar.b(i4 >> 4);
                    return;
                }
                _reportBase64EOF();
            }
            int i5 = i3 + 1;
            char charAt3 = str.charAt(i3);
            int decodeBase64Char3 = decodeBase64Char(charAt3);
            if (decodeBase64Char3 < 0) {
                if (decodeBase64Char3 != -2) {
                    _reportInvalidBase64(charAt3, 2, null);
                }
                if (i5 >= length) {
                    _reportBase64EOF();
                }
                i2 = i5 + 1;
                char charAt4 = str.charAt(i5);
                if (!usesPaddingChar(charAt4)) {
                    _reportInvalidBase64(charAt4, 3, "expected padding character '" + getPaddingChar() + "'");
                }
                msVar.b(i4 >> 4);
            } else {
                int i6 = (i4 << 6) | decodeBase64Char3;
                if (i5 >= length) {
                    if (!usesPadding()) {
                        msVar.d(i6 >> 2);
                        return;
                    }
                    _reportBase64EOF();
                }
                int i7 = i5 + 1;
                char charAt5 = str.charAt(i5);
                int decodeBase64Char4 = decodeBase64Char(charAt5);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        _reportInvalidBase64(charAt5, 3, null);
                    }
                    msVar.d(i6 >> 2);
                } else {
                    msVar.c((i6 << 6) | decodeBase64Char4);
                }
                i2 = i7;
            }
        }
    }

    public void encodeBase64Chunk(StringBuilder sb, int i) {
        sb.append(this.f0[(i >> 18) & 63]);
        sb.append(this.f0[(i >> 12) & 63]);
        sb.append(this.f0[(i >> 6) & 63]);
        sb.append(this.f0[i & 63]);
    }

    public void encodeBase64Partial(StringBuilder sb, int i, int i2) {
        sb.append(this.f0[(i >> 18) & 63]);
        sb.append(this.f0[(i >> 12) & 63]);
        if (this.h0) {
            sb.append(i2 == 2 ? this.f0[(i >> 6) & 63] : this.i0);
            sb.append(this.i0);
        } else if (i2 == 2) {
            sb.append(this.f0[(i >> 6) & 63]);
        }
    }

    public int encodeBase64Chunk(int i, byte[] bArr, int i2) {
        int i3 = i2 + 1;
        byte[] bArr2 = this.g0;
        bArr[i2] = bArr2[(i >> 18) & 63];
        int i4 = i3 + 1;
        bArr[i3] = bArr2[(i >> 12) & 63];
        int i5 = i4 + 1;
        bArr[i4] = bArr2[(i >> 6) & 63];
        int i6 = i5 + 1;
        bArr[i5] = bArr2[i & 63];
        return i6;
    }

    public int encodeBase64Partial(int i, int i2, byte[] bArr, int i3) {
        int i4 = i3 + 1;
        byte[] bArr2 = this.g0;
        bArr[i3] = bArr2[(i >> 18) & 63];
        int i5 = i4 + 1;
        bArr[i4] = bArr2[(i >> 12) & 63];
        if (!this.h0) {
            if (i2 == 2) {
                int i6 = i5 + 1;
                bArr[i5] = bArr2[(i >> 6) & 63];
                return i6;
            }
            return i5;
        }
        byte b = (byte) this.i0;
        int i7 = i5 + 1;
        bArr[i5] = i2 == 2 ? bArr2[(i >> 6) & 63] : b;
        int i8 = i7 + 1;
        bArr[i7] = b;
        return i8;
    }

    public Base64Variant(Base64Variant base64Variant, String str, int i) {
        this(base64Variant, str, base64Variant.h0, base64Variant.i0, i);
    }

    public Base64Variant(Base64Variant base64Variant, String str, boolean z, char c, int i) {
        int[] iArr = new int[128];
        this.a = iArr;
        char[] cArr = new char[64];
        this.f0 = cArr;
        byte[] bArr = new byte[64];
        this.g0 = bArr;
        this._name = str;
        byte[] bArr2 = base64Variant.g0;
        System.arraycopy(bArr2, 0, bArr, 0, bArr2.length);
        char[] cArr2 = base64Variant.f0;
        System.arraycopy(cArr2, 0, cArr, 0, cArr2.length);
        int[] iArr2 = base64Variant.a;
        System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
        this.h0 = z;
        this.i0 = c;
        this.j0 = i;
    }
}
