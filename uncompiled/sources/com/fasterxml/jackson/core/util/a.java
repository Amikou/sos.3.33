package com.fasterxml.jackson.core.util;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: JsonParserDelegate.java */
/* loaded from: classes.dex */
public class a extends JsonParser {
    public JsonParser g0;

    public a(JsonParser jsonParser) {
        this.g0 = jsonParser;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public float A() throws IOException {
        return this.g0.A();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean B0() {
        return this.g0.B0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object C() {
        return this.g0.C();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int F() throws IOException {
        return this.g0.F();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean F0(JsonToken jsonToken) {
        return this.g0.F0(jsonToken);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean H0(int i) {
        return this.g0.H0(i);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean K0() {
        return this.g0.K0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean L0() {
        return this.g0.L0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public long M() throws IOException {
        return this.g0.M();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser.NumberType N() throws IOException {
        return this.g0.N();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Number Q() throws IOException {
        return this.g0.Q();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object R() throws IOException {
        return this.g0.R();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public cw1 S() {
        return this.g0.S();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken U0() throws IOException {
        return this.g0.U0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public short W() throws IOException {
        return this.g0.W();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser W0(int i, int i2) {
        this.g0.W0(i, i2);
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String X() throws IOException {
        return this.g0.X();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser X0(int i, int i2) {
        this.g0.X0(i, i2);
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException {
        return this.g0.Z0(base64Variant, outputStream);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public char[] a0() throws IOException {
        return this.g0.a0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int b0() throws IOException {
        return this.g0.b0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean c() {
        return this.g0.c();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean c1() {
        return this.g0.c1();
    }

    @Override // com.fasterxml.jackson.core.JsonParser, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.g0.close();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean d() {
        return this.g0.d();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public void e() {
        this.g0.e();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int e0() throws IOException {
        return this.g0.e0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public void e1(Object obj) {
        this.g0.e1(obj);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken f() {
        return this.g0.f();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation f0() {
        return this.g0.f0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    @Deprecated
    public JsonParser f1(int i) {
        this.g0.f1(i);
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser g(JsonParser.Feature feature) {
        this.g0.g(feature);
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object g0() throws IOException {
        return this.g0.g0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public BigInteger h() throws IOException {
        return this.g0.h();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int i0() throws IOException {
        return this.g0.i0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public void i1(z81 z81Var) {
        this.g0.i1(z81Var);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public byte[] j(Base64Variant base64Variant) throws IOException {
        return this.g0.j(base64Variant);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser k1() throws IOException {
        this.g0.k1();
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean l() throws IOException {
        return this.g0.l();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public byte m() throws IOException {
        return this.g0.m();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int m0(int i) throws IOException {
        return this.g0.m0(i);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public com.fasterxml.jackson.core.c n() {
        return this.g0.n();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation q() {
        return this.g0.q();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String r() throws IOException {
        return this.g0.r();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public long r0() throws IOException {
        return this.g0.r0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken u() {
        return this.g0.u();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int v() {
        return this.g0.v();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public BigDecimal w() throws IOException {
        return this.g0.w();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public long w0(long j) throws IOException {
        return this.g0.w0(j);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public double x() throws IOException {
        return this.g0.x();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String x0() throws IOException {
        return this.g0.x0();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String y0(String str) throws IOException {
        return this.g0.y0(str);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object z() throws IOException {
        return this.g0.z();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean z0() {
        return this.g0.z0();
    }
}
