package com.fasterxml.jackson.core.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: JsonParserSequence.java */
/* loaded from: classes.dex */
public class b extends a {
    public final JsonParser[] h0;
    public final boolean i0;
    public int j0;
    public boolean k0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b(boolean z, JsonParser[] jsonParserArr) {
        super(jsonParserArr[0]);
        boolean z2 = false;
        this.i0 = z;
        if (z && this.g0.z0()) {
            z2 = true;
        }
        this.k0 = z2;
        this.h0 = jsonParserArr;
        this.j0 = 1;
    }

    public static b o1(boolean z, JsonParser jsonParser, JsonParser jsonParser2) {
        boolean z2 = jsonParser instanceof b;
        if (!z2 && !(jsonParser2 instanceof b)) {
            return new b(z, new JsonParser[]{jsonParser, jsonParser2});
        }
        ArrayList arrayList = new ArrayList();
        if (z2) {
            ((b) jsonParser).m1(arrayList);
        } else {
            arrayList.add(jsonParser);
        }
        if (jsonParser2 instanceof b) {
            ((b) jsonParser2).m1(arrayList);
        } else {
            arrayList.add(jsonParser2);
        }
        return new b(z, (JsonParser[]) arrayList.toArray(new JsonParser[arrayList.size()]));
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken T0() throws IOException {
        JsonParser jsonParser = this.g0;
        if (jsonParser == null) {
            return null;
        }
        if (this.k0) {
            this.k0 = false;
            return jsonParser.f();
        }
        JsonToken T0 = jsonParser.T0();
        return T0 == null ? p1() : T0;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        do {
            this.g0.close();
        } while (r1());
    }

    public void m1(List<JsonParser> list) {
        int length = this.h0.length;
        for (int i = this.j0 - 1; i < length; i++) {
            JsonParser jsonParser = this.h0[i];
            if (jsonParser instanceof b) {
                ((b) jsonParser).m1(list);
            } else {
                list.add(jsonParser);
            }
        }
    }

    public JsonToken p1() throws IOException {
        JsonToken T0;
        do {
            int i = this.j0;
            JsonParser[] jsonParserArr = this.h0;
            if (i >= jsonParserArr.length) {
                return null;
            }
            this.j0 = i + 1;
            JsonParser jsonParser = jsonParserArr[i];
            this.g0 = jsonParser;
            if (this.i0 && jsonParser.z0()) {
                return this.g0.u();
            }
            T0 = this.g0.T0();
        } while (T0 == null);
        return T0;
    }

    public boolean r1() {
        int i = this.j0;
        JsonParser[] jsonParserArr = this.h0;
        if (i < jsonParserArr.length) {
            this.j0 = i + 1;
            this.g0 = jsonParserArr[i];
            return true;
        }
        return false;
    }
}
