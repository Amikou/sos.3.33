package com.fasterxml.jackson.core.util;

import com.fasterxml.jackson.core.io.d;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

/* compiled from: TextBuffer.java */
/* loaded from: classes.dex */
public final class c {
    public static final char[] l = new char[0];
    public final wr a;
    public char[] b;
    public int c;
    public int d;
    public ArrayList<char[]> e;
    public boolean f;
    public int g;
    public char[] h;
    public int i;
    public String j;
    public char[] k;

    public c(wr wrVar) {
        this.a = wrVar;
    }

    public int A() {
        if (this.c >= 0) {
            return this.d;
        }
        char[] cArr = this.k;
        if (cArr != null) {
            return cArr.length;
        }
        String str = this.j;
        if (str != null) {
            return str.length();
        }
        return this.g + this.i;
    }

    public final void B(int i) {
        int i2 = this.d;
        this.d = 0;
        char[] cArr = this.b;
        this.b = null;
        int i3 = this.c;
        this.c = -1;
        int i4 = i + i2;
        char[] cArr2 = this.h;
        if (cArr2 == null || i4 > cArr2.length) {
            this.h = d(i4);
        }
        if (i2 > 0) {
            System.arraycopy(cArr, i3, this.h, 0, i2);
        }
        this.g = 0;
        this.i = i2;
    }

    public void a(char c) {
        if (this.c >= 0) {
            B(16);
        }
        this.j = null;
        this.k = null;
        char[] cArr = this.h;
        if (this.i >= cArr.length) {
            l(1);
            cArr = this.h;
        }
        int i = this.i;
        this.i = i + 1;
        cArr[i] = c;
    }

    public void b(String str, int i, int i2) {
        if (this.c >= 0) {
            B(i2);
        }
        this.j = null;
        this.k = null;
        char[] cArr = this.h;
        int length = cArr.length;
        int i3 = this.i;
        int i4 = length - i3;
        if (i4 >= i2) {
            str.getChars(i, i + i2, cArr, i3);
            this.i += i2;
            return;
        }
        if (i4 > 0) {
            int i5 = i + i4;
            str.getChars(i, i5, cArr, i3);
            i2 -= i4;
            i = i5;
        }
        while (true) {
            l(i2);
            int min = Math.min(this.h.length, i2);
            int i6 = i + min;
            str.getChars(i, i6, this.h, 0);
            this.i += min;
            i2 -= min;
            if (i2 <= 0) {
                return;
            }
            i = i6;
        }
    }

    public void c(char[] cArr, int i, int i2) {
        if (this.c >= 0) {
            B(i2);
        }
        this.j = null;
        this.k = null;
        char[] cArr2 = this.h;
        int length = cArr2.length;
        int i3 = this.i;
        int i4 = length - i3;
        if (i4 >= i2) {
            System.arraycopy(cArr, i, cArr2, i3, i2);
            this.i += i2;
            return;
        }
        if (i4 > 0) {
            System.arraycopy(cArr, i, cArr2, i3, i4);
            i += i4;
            i2 -= i4;
        }
        do {
            l(i2);
            int min = Math.min(this.h.length, i2);
            System.arraycopy(cArr, i, this.h, 0, min);
            this.i += min;
            i += min;
            i2 -= min;
        } while (i2 > 0);
    }

    public final char[] d(int i) {
        wr wrVar = this.a;
        if (wrVar != null) {
            return wrVar.d(2, i);
        }
        return new char[Math.max(i, 1000)];
    }

    public final char[] e(int i) {
        return new char[i];
    }

    public final void f() {
        this.f = false;
        this.e.clear();
        this.g = 0;
        this.i = 0;
    }

    public char[] g() {
        char[] cArr = this.k;
        if (cArr == null) {
            char[] x = x();
            this.k = x;
            return x;
        }
        return cArr;
    }

    public BigDecimal h() throws NumberFormatException {
        char[] cArr;
        char[] cArr2;
        char[] cArr3 = this.k;
        if (cArr3 != null) {
            return d.g(cArr3);
        }
        int i = this.c;
        if (i >= 0 && (cArr2 = this.b) != null) {
            return d.h(cArr2, i, this.d);
        }
        if (this.g == 0 && (cArr = this.h) != null) {
            return d.h(cArr, 0, this.i);
        }
        return d.g(g());
    }

    public double i() throws NumberFormatException {
        return d.i(j());
    }

    public String j() {
        if (this.j == null) {
            char[] cArr = this.k;
            if (cArr != null) {
                this.j = new String(cArr);
            } else {
                int i = this.c;
                if (i >= 0) {
                    int i2 = this.d;
                    if (i2 < 1) {
                        this.j = "";
                        return "";
                    }
                    this.j = new String(this.b, i, i2);
                } else {
                    int i3 = this.g;
                    int i4 = this.i;
                    if (i3 == 0) {
                        this.j = i4 != 0 ? new String(this.h, 0, i4) : "";
                    } else {
                        StringBuilder sb = new StringBuilder(i3 + i4);
                        ArrayList<char[]> arrayList = this.e;
                        if (arrayList != null) {
                            int size = arrayList.size();
                            for (int i5 = 0; i5 < size; i5++) {
                                char[] cArr2 = this.e.get(i5);
                                sb.append(cArr2, 0, cArr2.length);
                            }
                        }
                        sb.append(this.h, 0, this.i);
                        this.j = sb.toString();
                    }
                }
            }
        }
        return this.j;
    }

    public char[] k() {
        this.c = -1;
        this.i = 0;
        this.d = 0;
        this.b = null;
        this.j = null;
        this.k = null;
        if (this.f) {
            f();
        }
        char[] cArr = this.h;
        if (cArr == null) {
            char[] d = d(0);
            this.h = d;
            return d;
        }
        return cArr;
    }

    public final void l(int i) {
        if (this.e == null) {
            this.e = new ArrayList<>();
        }
        char[] cArr = this.h;
        this.f = true;
        this.e.add(cArr);
        this.g += cArr.length;
        this.i = 0;
        int length = cArr.length;
        int i2 = length + (length >> 1);
        if (i2 < 1000) {
            i2 = 1000;
        } else if (i2 > 262144) {
            i2 = 262144;
        }
        this.h = e(i2);
    }

    public char[] m() {
        char[] cArr = this.h;
        int length = cArr.length;
        int i = (length >> 1) + length;
        if (i > 262144) {
            i = (length >> 2) + length;
        }
        char[] copyOf = Arrays.copyOf(cArr, i);
        this.h = copyOf;
        return copyOf;
    }

    public char[] n() {
        if (this.e == null) {
            this.e = new ArrayList<>();
        }
        this.f = true;
        this.e.add(this.h);
        int length = this.h.length;
        this.g += length;
        this.i = 0;
        int i = length + (length >> 1);
        if (i < 1000) {
            i = 1000;
        } else if (i > 262144) {
            i = 262144;
        }
        char[] e = e(i);
        this.h = e;
        return e;
    }

    public char[] o() {
        if (this.c >= 0) {
            B(1);
        } else {
            char[] cArr = this.h;
            if (cArr == null) {
                this.h = d(0);
            } else if (this.i >= cArr.length) {
                l(1);
            }
        }
        return this.h;
    }

    public int p() {
        return this.i;
    }

    public char[] q() {
        if (this.c >= 0) {
            return this.b;
        }
        char[] cArr = this.k;
        if (cArr != null) {
            return cArr;
        }
        String str = this.j;
        if (str != null) {
            char[] charArray = str.toCharArray();
            this.k = charArray;
            return charArray;
        } else if (!this.f) {
            char[] cArr2 = this.h;
            return cArr2 == null ? l : cArr2;
        } else {
            return g();
        }
    }

    public int r() {
        int i = this.c;
        if (i >= 0) {
            return i;
        }
        return 0;
    }

    public void s() {
        if (this.a == null) {
            u();
        } else if (this.h != null) {
            u();
            char[] cArr = this.h;
            this.h = null;
            this.a.j(2, cArr);
        }
    }

    public void t(char[] cArr, int i, int i2) {
        this.b = null;
        this.c = -1;
        this.d = 0;
        this.j = null;
        this.k = null;
        if (this.f) {
            f();
        } else if (this.h == null) {
            this.h = d(i2);
        }
        this.g = 0;
        this.i = 0;
        c(cArr, i, i2);
    }

    public String toString() {
        return j();
    }

    public void u() {
        this.c = -1;
        this.i = 0;
        this.d = 0;
        this.b = null;
        this.j = null;
        this.k = null;
        if (this.f) {
            f();
        }
    }

    public void v(char[] cArr, int i, int i2) {
        this.j = null;
        this.k = null;
        this.b = cArr;
        this.c = i;
        this.d = i2;
        if (this.f) {
            f();
        }
    }

    public void w(String str) {
        this.b = null;
        this.c = -1;
        this.d = 0;
        this.j = str;
        this.k = null;
        if (this.f) {
            f();
        }
        this.i = 0;
    }

    public final char[] x() {
        int i;
        String str = this.j;
        if (str != null) {
            return str.toCharArray();
        }
        int i2 = this.c;
        if (i2 >= 0) {
            int i3 = this.d;
            if (i3 < 1) {
                return l;
            }
            if (i2 == 0) {
                return Arrays.copyOf(this.b, i3);
            }
            return Arrays.copyOfRange(this.b, i2, i3 + i2);
        }
        int A = A();
        if (A < 1) {
            return l;
        }
        char[] e = e(A);
        ArrayList<char[]> arrayList = this.e;
        if (arrayList != null) {
            int size = arrayList.size();
            i = 0;
            for (int i4 = 0; i4 < size; i4++) {
                char[] cArr = this.e.get(i4);
                int length = cArr.length;
                System.arraycopy(cArr, 0, e, i, length);
                i += length;
            }
        } else {
            i = 0;
        }
        System.arraycopy(this.h, 0, e, i, this.i);
        return e;
    }

    public String y(int i) {
        this.i = i;
        if (this.g > 0) {
            return j();
        }
        String str = i == 0 ? "" : new String(this.h, 0, i);
        this.j = str;
        return str;
    }

    public void z(int i) {
        this.i = i;
    }
}
