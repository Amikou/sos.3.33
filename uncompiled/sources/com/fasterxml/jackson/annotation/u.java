package com.fasterxml.jackson.annotation;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import java.util.HashMap;
import java.util.Map;

/* compiled from: SimpleObjectIdResolver.java */
/* loaded from: classes.dex */
public class u implements t {
    public Map<ObjectIdGenerator.IdKey, Object> a;

    @Override // com.fasterxml.jackson.annotation.t
    public void a(ObjectIdGenerator.IdKey idKey, Object obj) {
        Map<ObjectIdGenerator.IdKey, Object> map = this.a;
        if (map == null) {
            this.a = new HashMap();
        } else if (map.containsKey(idKey)) {
            throw new IllegalStateException("Already had POJO for id (" + idKey.key.getClass().getName() + ") [" + idKey + "]");
        }
        this.a.put(idKey, obj);
    }

    @Override // com.fasterxml.jackson.annotation.t
    public t b(Object obj) {
        return new u();
    }

    @Override // com.fasterxml.jackson.annotation.t
    public boolean c(t tVar) {
        return tVar.getClass() == u.class;
    }

    @Override // com.fasterxml.jackson.annotation.t
    public Object d(ObjectIdGenerator.IdKey idKey) {
        Map<ObjectIdGenerator.IdKey, Object> map = this.a;
        if (map == null) {
            return null;
        }
        return map.get(idKey);
    }
}
