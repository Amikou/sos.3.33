package com.fasterxml.jackson.annotation;

/* loaded from: classes.dex */
public abstract class ObjectIdGenerators$PropertyGenerator extends ObjectIdGenerators$Base<Object> {
    private static final long serialVersionUID = 1;

    public ObjectIdGenerators$PropertyGenerator(Class<?> cls) {
        super(cls);
    }

    @Override // com.fasterxml.jackson.annotation.ObjectIdGenerators$Base, com.fasterxml.jackson.annotation.ObjectIdGenerator
    public /* bridge */ /* synthetic */ boolean canUseFor(ObjectIdGenerator objectIdGenerator) {
        return super.canUseFor(objectIdGenerator);
    }
}
