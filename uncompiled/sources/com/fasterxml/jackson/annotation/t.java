package com.fasterxml.jackson.annotation;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;

/* compiled from: ObjectIdResolver.java */
/* loaded from: classes.dex */
public interface t {
    void a(ObjectIdGenerator.IdKey idKey, Object obj);

    t b(Object obj);

    boolean c(t tVar);

    Object d(ObjectIdGenerator.IdKey idKey);
}
