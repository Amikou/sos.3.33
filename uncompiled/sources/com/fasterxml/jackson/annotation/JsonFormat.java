package com.fasterxml.jackson.annotation;

import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Locale;
import java.util.TimeZone;

@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
/* loaded from: classes.dex */
public @interface JsonFormat {

    /* loaded from: classes.dex */
    public enum Feature {
        ACCEPT_SINGLE_VALUE_AS_ARRAY,
        ACCEPT_CASE_INSENSITIVE_PROPERTIES,
        WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS,
        WRITE_DATES_WITH_ZONE_ID,
        WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED,
        WRITE_SORTED_MAP_ENTRIES,
        ADJUST_DATES_TO_CONTEXT_TIME_ZONE
    }

    /* loaded from: classes.dex */
    public enum Shape {
        ANY,
        NATURAL,
        SCALAR,
        ARRAY,
        OBJECT,
        NUMBER,
        NUMBER_FLOAT,
        NUMBER_INT,
        STRING,
        BOOLEAN;

        public boolean isNumeric() {
            return this == NUMBER || this == NUMBER_INT || this == NUMBER_FLOAT;
        }

        public boolean isStructured() {
            return this == OBJECT || this == ARRAY;
        }
    }

    /* loaded from: classes.dex */
    public static class Value implements Serializable {
        public static final Value f0 = new Value();
        private static final long serialVersionUID = 1;
        private final a _features;
        private final Locale _locale;
        private final String _pattern;
        private final Shape _shape;
        private final String _timezoneStr;
        public transient TimeZone a;

        public Value() {
            this("", Shape.ANY, "", "", a.c());
        }

        public static <T> boolean a(T t, T t2) {
            if (t == null) {
                return t2 == null;
            } else if (t2 == null) {
                return false;
            } else {
                return t.equals(t2);
            }
        }

        public static final Value empty() {
            return f0;
        }

        public static Value forPattern(String str) {
            return new Value(str, null, null, null, null, a.c());
        }

        public static Value forShape(Shape shape) {
            return new Value(null, shape, null, null, null, a.c());
        }

        public static final Value from(JsonFormat jsonFormat) {
            if (jsonFormat == null) {
                return null;
            }
            return new Value(jsonFormat);
        }

        public static Value merge(Value value, Value value2) {
            return value == null ? value2 : value.withOverrides(value2);
        }

        public static Value mergeAll(Value... valueArr) {
            Value value = null;
            for (Value value2 : valueArr) {
                if (value2 != null) {
                    if (value != null) {
                        value2 = value.withOverrides(value2);
                    }
                    value = value2;
                }
            }
            return value;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj != null && obj.getClass() == getClass()) {
                Value value = (Value) obj;
                if (this._shape == value._shape && this._features.equals(value._features)) {
                    return a(this._timezoneStr, value._timezoneStr) && a(this._pattern, value._pattern) && a(this.a, value.a) && a(this._locale, value._locale);
                }
                return false;
            }
            return false;
        }

        public Boolean getFeature(Feature feature) {
            return this._features.d(feature);
        }

        public a getFeatures() {
            return this._features;
        }

        public Locale getLocale() {
            return this._locale;
        }

        public String getPattern() {
            return this._pattern;
        }

        public Shape getShape() {
            return this._shape;
        }

        public TimeZone getTimeZone() {
            TimeZone timeZone = this.a;
            if (timeZone == null) {
                String str = this._timezoneStr;
                if (str == null) {
                    return null;
                }
                TimeZone timeZone2 = TimeZone.getTimeZone(str);
                this.a = timeZone2;
                return timeZone2;
            }
            return timeZone;
        }

        public boolean hasLocale() {
            return this._locale != null;
        }

        public boolean hasPattern() {
            String str = this._pattern;
            return str != null && str.length() > 0;
        }

        public boolean hasShape() {
            return this._shape != Shape.ANY;
        }

        public boolean hasTimeZone() {
            String str;
            return (this.a == null && ((str = this._timezoneStr) == null || str.isEmpty())) ? false : true;
        }

        public int hashCode() {
            String str = this._timezoneStr;
            int hashCode = str == null ? 1 : str.hashCode();
            String str2 = this._pattern;
            if (str2 != null) {
                hashCode ^= str2.hashCode();
            }
            int hashCode2 = hashCode + this._shape.hashCode();
            Locale locale = this._locale;
            if (locale != null) {
                hashCode2 ^= locale.hashCode();
            }
            return hashCode2 + this._features.hashCode();
        }

        public String timeZoneAsString() {
            TimeZone timeZone = this.a;
            if (timeZone != null) {
                return timeZone.getID();
            }
            return this._timezoneStr;
        }

        public String toString() {
            return String.format("[pattern=%s,shape=%s,locale=%s,timezone=%s]", this._pattern, this._shape, this._locale, this._timezoneStr);
        }

        public Class<JsonFormat> valueFor() {
            return JsonFormat.class;
        }

        public Value withFeature(Feature feature) {
            a e = this._features.e(feature);
            return e == this._features ? this : new Value(this._pattern, this._shape, this._locale, this._timezoneStr, this.a, e);
        }

        public Value withLocale(Locale locale) {
            return new Value(this._pattern, this._shape, locale, this._timezoneStr, this.a, this._features);
        }

        public final Value withOverrides(Value value) {
            Value value2;
            a f;
            String str;
            TimeZone timeZone;
            if (value == null || value == (value2 = f0)) {
                return this;
            }
            if (this == value2) {
                return value;
            }
            String str2 = value._pattern;
            if (str2 == null || str2.isEmpty()) {
                str2 = this._pattern;
            }
            String str3 = str2;
            Shape shape = value._shape;
            if (shape == Shape.ANY) {
                shape = this._shape;
            }
            Shape shape2 = shape;
            Locale locale = value._locale;
            if (locale == null) {
                locale = this._locale;
            }
            Locale locale2 = locale;
            a aVar = this._features;
            if (aVar == null) {
                f = value._features;
            } else {
                f = aVar.f(value._features);
            }
            a aVar2 = f;
            String str4 = value._timezoneStr;
            if (str4 != null && !str4.isEmpty()) {
                timeZone = value.a;
                str = str4;
            } else {
                str = this._timezoneStr;
                timeZone = this.a;
            }
            return new Value(str3, shape2, locale2, str, timeZone, aVar2);
        }

        public Value withPattern(String str) {
            return new Value(str, this._shape, this._locale, this._timezoneStr, this.a, this._features);
        }

        public Value withShape(Shape shape) {
            return new Value(this._pattern, shape, this._locale, this._timezoneStr, this.a, this._features);
        }

        public Value withTimeZone(TimeZone timeZone) {
            return new Value(this._pattern, this._shape, this._locale, null, timeZone, this._features);
        }

        public Value withoutFeature(Feature feature) {
            a g = this._features.g(feature);
            return g == this._features ? this : new Value(this._pattern, this._shape, this._locale, this._timezoneStr, this.a, g);
        }

        public Value(JsonFormat jsonFormat) {
            this(jsonFormat.pattern(), jsonFormat.shape(), jsonFormat.locale(), jsonFormat.timezone(), a.a(jsonFormat));
        }

        public Value(String str, Shape shape, String str2, String str3, a aVar) {
            this(str, shape, (str2 == null || str2.length() == 0 || "##default".equals(str2)) ? null : new Locale(str2), (str3 == null || str3.length() == 0 || "##default".equals(str3)) ? null : str3, null, aVar);
        }

        public Value(String str, Shape shape, Locale locale, TimeZone timeZone, a aVar) {
            this._pattern = str;
            this._shape = shape == null ? Shape.ANY : shape;
            this._locale = locale;
            this.a = timeZone;
            this._timezoneStr = null;
            this._features = aVar == null ? a.c() : aVar;
        }

        public Value(String str, Shape shape, Locale locale, String str2, TimeZone timeZone, a aVar) {
            this._pattern = str;
            this._shape = shape == null ? Shape.ANY : shape;
            this._locale = locale;
            this.a = timeZone;
            this._timezoneStr = str2;
            this._features = aVar == null ? a.c() : aVar;
        }

        @Deprecated
        public Value(String str, Shape shape, Locale locale, TimeZone timeZone) {
            this(str, shape, locale, timeZone, a.c());
        }

        @Deprecated
        public Value(String str, Shape shape, String str2, String str3) {
            this(str, shape, str2, str3, a.c());
        }

        @Deprecated
        public Value(String str, Shape shape, Locale locale, String str2, TimeZone timeZone) {
            this(str, shape, locale, str2, timeZone, a.c());
        }
    }

    /* loaded from: classes.dex */
    public static class a {
        public static final a c = new a(0, 0);
        public final int a;
        public final int b;

        public a(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public static a a(JsonFormat jsonFormat) {
            return b(jsonFormat.with(), jsonFormat.without());
        }

        public static a b(Feature[] featureArr, Feature[] featureArr2) {
            int i = 0;
            for (Feature feature : featureArr) {
                i |= 1 << feature.ordinal();
            }
            int i2 = 0;
            for (Feature feature2 : featureArr2) {
                i2 |= 1 << feature2.ordinal();
            }
            return new a(i, i2);
        }

        public static a c() {
            return c;
        }

        public Boolean d(Feature feature) {
            int ordinal = 1 << feature.ordinal();
            if ((this.b & ordinal) != 0) {
                return Boolean.FALSE;
            }
            if ((ordinal & this.a) != 0) {
                return Boolean.TRUE;
            }
            return null;
        }

        public a e(Feature... featureArr) {
            int i = this.a;
            for (Feature feature : featureArr) {
                i |= 1 << feature.ordinal();
            }
            return i == this.a ? this : new a(i, this.b);
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj != null && obj.getClass() == a.class) {
                a aVar = (a) obj;
                return aVar.a == this.a && aVar.b == this.b;
            }
            return false;
        }

        public a f(a aVar) {
            if (aVar == null) {
                return this;
            }
            int i = aVar.b;
            int i2 = aVar.a;
            if (i == 0 && i2 == 0) {
                return this;
            }
            int i3 = this.a;
            if (i3 == 0 && this.b == 0) {
                return aVar;
            }
            int i4 = ((~i) & i3) | i2;
            int i5 = this.b;
            int i6 = i | ((~i2) & i5);
            return (i4 == i3 && i6 == i5) ? this : new a(i4, i6);
        }

        public a g(Feature... featureArr) {
            int i = this.b;
            for (Feature feature : featureArr) {
                i |= 1 << feature.ordinal();
            }
            return i == this.b ? this : new a(this.a, i);
        }

        public int hashCode() {
            return this.b + this.a;
        }
    }

    String locale() default "##default";

    String pattern() default "";

    Shape shape() default Shape.ANY;

    String timezone() default "##default";

    Feature[] with() default {};

    Feature[] without() default {};
}
