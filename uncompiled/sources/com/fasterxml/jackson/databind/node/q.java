package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.node.k;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: TreeTraversingParser.java */
/* loaded from: classes.dex */
public class q extends rp2 {
    public com.fasterxml.jackson.core.c h0;
    public k i0;
    public JsonToken j0;
    public boolean k0;
    public boolean l0;

    /* compiled from: TreeTraversingParser.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonToken.values().length];
            a = iArr;
            try {
                iArr[JsonToken.FIELD_NAME.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JsonToken.VALUE_STRING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_INT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    public q(com.fasterxml.jackson.databind.d dVar, com.fasterxml.jackson.core.c cVar) {
        super(0);
        this.h0 = cVar;
        if (dVar.B()) {
            this.j0 = JsonToken.START_ARRAY;
            this.i0 = new k.a(dVar, null);
        } else if (dVar.F()) {
            this.j0 = JsonToken.START_OBJECT;
            this.i0 = new k.b(dVar, null);
        } else {
            this.i0 = new k.c(dVar, null);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public float A() throws IOException, JsonParseException {
        return (float) O1().q();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean B0() {
        return false;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int F() throws IOException, JsonParseException {
        return O1().z();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public long M() throws IOException, JsonParseException {
        return O1().H();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser.NumberType N() throws IOException, JsonParseException {
        com.fasterxml.jackson.databind.d O1 = O1();
        if (O1 == null) {
            return null;
        }
        return O1.e();
    }

    public com.fasterxml.jackson.databind.d N1() {
        k kVar;
        if (this.l0 || (kVar = this.i0) == null) {
            return null;
        }
        return kVar.k();
    }

    public com.fasterxml.jackson.databind.d O1() throws JsonParseException {
        com.fasterxml.jackson.databind.d N1 = N1();
        if (N1 == null || !N1.E()) {
            JsonToken i = N1 == null ? null : N1.i();
            throw a("Current token (" + i + ") not numeric, can not use numeric value accessors");
        }
        return N1;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Number Q() throws IOException, JsonParseException {
        return O1().I();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public cw1 S() {
        return this.i0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken T0() throws IOException, JsonParseException {
        JsonToken jsonToken = this.j0;
        if (jsonToken != null) {
            this.g0 = jsonToken;
            this.j0 = null;
            return jsonToken;
        } else if (this.k0) {
            this.k0 = false;
            if (!this.i0.j()) {
                JsonToken jsonToken2 = this.g0 == JsonToken.START_OBJECT ? JsonToken.END_OBJECT : JsonToken.END_ARRAY;
                this.g0 = jsonToken2;
                return jsonToken2;
            }
            k n = this.i0.n();
            this.i0 = n;
            JsonToken o = n.o();
            this.g0 = o;
            if (o == JsonToken.START_OBJECT || o == JsonToken.START_ARRAY) {
                this.k0 = true;
            }
            return o;
        } else {
            k kVar = this.i0;
            if (kVar == null) {
                this.l0 = true;
                return null;
            }
            JsonToken o2 = kVar.o();
            this.g0 = o2;
            if (o2 != null) {
                if (o2 == JsonToken.START_OBJECT || o2 == JsonToken.START_ARRAY) {
                    this.k0 = true;
                }
                return o2;
            }
            this.g0 = this.i0.l();
            this.i0 = this.i0.m();
            return this.g0;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String X() {
        com.fasterxml.jackson.databind.d N1;
        if (this.l0) {
            return null;
        }
        int i = a.a[this.g0.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3 && i != 4) {
                    if (i == 5 && (N1 = N1()) != null && N1.D()) {
                        return N1.m();
                    }
                    JsonToken jsonToken = this.g0;
                    if (jsonToken == null) {
                        return null;
                    }
                    return jsonToken.asString();
                }
                return String.valueOf(N1().I());
            }
            return N1().J();
        }
        return this.i0.b();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException, JsonParseException {
        byte[] j = j(base64Variant);
        if (j != null) {
            outputStream.write(j, 0, j.length);
            return j.length;
        }
        return 0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public char[] a0() throws IOException, JsonParseException {
        return X().toCharArray();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int b0() throws IOException, JsonParseException {
        return X().length();
    }

    @Override // com.fasterxml.jackson.core.JsonParser, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.l0) {
            return;
        }
        this.l0 = true;
        this.i0 = null;
        this.g0 = null;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int e0() throws IOException, JsonParseException {
        return 0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation f0() {
        return JsonLocation.NA;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public BigInteger h() throws IOException, JsonParseException {
        return O1().n();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public byte[] j(Base64Variant base64Variant) throws IOException, JsonParseException {
        com.fasterxml.jackson.databind.d N1 = N1();
        if (N1 != null) {
            byte[] o = N1.o();
            if (o != null) {
                return o;
            }
            if (N1.G()) {
                Object L = ((n) N1).L();
                if (L instanceof byte[]) {
                    return (byte[]) L;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public JsonParser k1() throws IOException, JsonParseException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.START_OBJECT) {
            this.k0 = false;
            this.g0 = JsonToken.END_OBJECT;
        } else if (jsonToken == JsonToken.START_ARRAY) {
            this.k0 = false;
            this.g0 = JsonToken.END_ARRAY;
        }
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public com.fasterxml.jackson.core.c n() {
        return this.h0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation q() {
        return JsonLocation.NA;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String r() {
        k kVar = this.i0;
        if (kVar == null) {
            return null;
        }
        return kVar.b();
    }

    @Override // defpackage.rp2
    public void r1() throws JsonParseException {
        J1();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public BigDecimal w() throws IOException, JsonParseException {
        return O1().p();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public double x() throws IOException, JsonParseException {
        return O1().q();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object z() {
        com.fasterxml.jackson.databind.d N1;
        if (this.l0 || (N1 = N1()) == null) {
            return null;
        }
        if (N1.G()) {
            return ((n) N1).L();
        }
        if (N1.D()) {
            return ((d) N1).o();
        }
        return null;
    }
}
