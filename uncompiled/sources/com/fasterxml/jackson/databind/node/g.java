package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: DoubleNode.java */
/* loaded from: classes.dex */
public class g extends fj2 {
    public final double a;

    public g(double d) {
        this.a = d;
    }

    public static g K(double d) {
        return new g(d);
    }

    @Override // com.fasterxml.jackson.databind.d
    public long H() {
        return (long) this.a;
    }

    @Override // com.fasterxml.jackson.databind.d
    public Number I() {
        return Double.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.core.f
    public JsonParser.NumberType e() {
        return JsonParser.NumberType.DOUBLE;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof g)) {
            return Double.compare(this.a, ((g) obj).a) == 0;
        }
        return false;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.a);
        return ((int) (doubleToLongBits >> 32)) ^ ((int) doubleToLongBits);
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_NUMBER_FLOAT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return aj2.s(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigInteger n() {
        return p().toBigInteger();
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigDecimal p() {
        return BigDecimal.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public double q() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException, JsonProcessingException {
        jsonGenerator.r0(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public int z() {
        return (int) this.a;
    }
}
