package com.fasterxml.jackson.databind.node;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/* loaded from: classes.dex */
public class JsonNodeFactory implements Serializable {
    public static final JsonNodeFactory a;
    public static final JsonNodeFactory f0;
    public static final JsonNodeFactory instance;
    private static final long serialVersionUID = 1;
    private final boolean _cfgBigDecimalExact;

    static {
        JsonNodeFactory jsonNodeFactory = new JsonNodeFactory(false);
        a = jsonNodeFactory;
        f0 = new JsonNodeFactory(true);
        instance = jsonNodeFactory;
    }

    public JsonNodeFactory(boolean z) {
        this._cfgBigDecimalExact = z;
    }

    public static JsonNodeFactory withExactBigDecimals(boolean z) {
        return z ? f0 : a;
    }

    public boolean _inIntRange(long j) {
        return ((long) ((int) j)) == j;
    }

    public a arrayNode() {
        return new a(this);
    }

    public m objectNode() {
        return new m(this);
    }

    public r pojoNode(Object obj) {
        return new n(obj);
    }

    public r rawValueNode(com.fasterxml.jackson.databind.util.d dVar) {
        return new n(dVar);
    }

    public a arrayNode(int i) {
        return new a(this, i);
    }

    /* renamed from: booleanNode */
    public e m3booleanNode(boolean z) {
        return z ? e.L() : e.K();
    }

    /* renamed from: nullNode */
    public l m4nullNode() {
        return l.K();
    }

    /* renamed from: textNode */
    public p m13textNode(String str) {
        return p.R(str);
    }

    public JsonNodeFactory() {
        this(false);
    }

    /* renamed from: binaryNode */
    public d m1binaryNode(byte[] bArr) {
        return d.K(bArr);
    }

    /* renamed from: binaryNode */
    public d m2binaryNode(byte[] bArr, int i, int i2) {
        return d.L(bArr, i, i2);
    }

    public fj2 numberNode(byte b) {
        return i.K(b);
    }

    public r numberNode(Byte b) {
        return b == null ? m4nullNode() : i.K(b.intValue());
    }

    public fj2 numberNode(short s) {
        return o.K(s);
    }

    public r numberNode(Short sh) {
        return sh == null ? m4nullNode() : o.K(sh.shortValue());
    }

    public fj2 numberNode(int i) {
        return i.K(i);
    }

    public r numberNode(Integer num) {
        return num == null ? m4nullNode() : i.K(num.intValue());
    }

    public fj2 numberNode(long j) {
        return j.K(j);
    }

    public r numberNode(Long l) {
        if (l == null) {
            return m4nullNode();
        }
        return j.K(l.longValue());
    }

    public fj2 numberNode(BigInteger bigInteger) {
        return c.K(bigInteger);
    }

    public fj2 numberNode(float f) {
        return h.K(f);
    }

    public r numberNode(Float f) {
        return f == null ? m4nullNode() : h.K(f.floatValue());
    }

    public fj2 numberNode(double d) {
        return g.K(d);
    }

    public r numberNode(Double d) {
        return d == null ? m4nullNode() : g.K(d.doubleValue());
    }

    public fj2 numberNode(BigDecimal bigDecimal) {
        if (this._cfgBigDecimalExact) {
            return f.K(bigDecimal);
        }
        return bigDecimal.compareTo(BigDecimal.ZERO) == 0 ? f.f0 : f.K(bigDecimal.stripTrailingZeros());
    }
}
