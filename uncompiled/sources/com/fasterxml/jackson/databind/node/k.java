package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonToken;
import java.util.Iterator;
import java.util.Map;

/* compiled from: NodeCursor.java */
/* loaded from: classes.dex */
public abstract class k extends cw1 {
    public final k c;
    public String d;
    public Object e;

    /* compiled from: NodeCursor.java */
    /* loaded from: classes.dex */
    public static final class a extends k {
        public Iterator<com.fasterxml.jackson.databind.d> f;
        public com.fasterxml.jackson.databind.d g;

        public a(com.fasterxml.jackson.databind.d dVar, k kVar) {
            super(1, kVar);
            this.f = dVar.s();
        }

        @Override // defpackage.cw1
        public /* bridge */ /* synthetic */ cw1 d() {
            return super.m();
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public boolean j() {
            return ((e70) k()).size() > 0;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public com.fasterxml.jackson.databind.d k() {
            return this.g;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public JsonToken l() {
            return JsonToken.END_ARRAY;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public JsonToken o() {
            if (!this.f.hasNext()) {
                this.g = null;
                return null;
            }
            com.fasterxml.jackson.databind.d next = this.f.next();
            this.g = next;
            return next.i();
        }
    }

    /* compiled from: NodeCursor.java */
    /* loaded from: classes.dex */
    public static final class b extends k {
        public Iterator<Map.Entry<String, com.fasterxml.jackson.databind.d>> f;
        public Map.Entry<String, com.fasterxml.jackson.databind.d> g;
        public boolean h;

        public b(com.fasterxml.jackson.databind.d dVar, k kVar) {
            super(2, kVar);
            this.f = ((m) dVar).t();
            this.h = true;
        }

        @Override // defpackage.cw1
        public /* bridge */ /* synthetic */ cw1 d() {
            return super.m();
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public boolean j() {
            return ((e70) k()).size() > 0;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public com.fasterxml.jackson.databind.d k() {
            Map.Entry<String, com.fasterxml.jackson.databind.d> entry = this.g;
            if (entry == null) {
                return null;
            }
            return entry.getValue();
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public JsonToken l() {
            return JsonToken.END_OBJECT;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public JsonToken o() {
            if (this.h) {
                if (!this.f.hasNext()) {
                    this.d = null;
                    this.g = null;
                    return null;
                }
                this.h = false;
                Map.Entry<String, com.fasterxml.jackson.databind.d> next = this.f.next();
                this.g = next;
                this.d = next != null ? next.getKey() : null;
                return JsonToken.FIELD_NAME;
            }
            this.h = true;
            return this.g.getValue().i();
        }
    }

    /* compiled from: NodeCursor.java */
    /* loaded from: classes.dex */
    public static final class c extends k {
        public com.fasterxml.jackson.databind.d f;
        public boolean g;

        public c(com.fasterxml.jackson.databind.d dVar, k kVar) {
            super(0, kVar);
            this.g = false;
            this.f = dVar;
        }

        @Override // defpackage.cw1
        public /* bridge */ /* synthetic */ cw1 d() {
            return super.m();
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public boolean j() {
            return false;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public com.fasterxml.jackson.databind.d k() {
            return this.f;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public JsonToken l() {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.node.k
        public JsonToken o() {
            if (!this.g) {
                this.g = true;
                return this.f.i();
            }
            this.f = null;
            return null;
        }
    }

    public k(int i, k kVar) {
        this.a = i;
        this.b = -1;
        this.c = kVar;
    }

    @Override // defpackage.cw1
    public final String b() {
        return this.d;
    }

    @Override // defpackage.cw1
    public void h(Object obj) {
        this.e = obj;
    }

    public abstract boolean j();

    public abstract com.fasterxml.jackson.databind.d k();

    public abstract JsonToken l();

    public final k m() {
        return this.c;
    }

    public final k n() {
        com.fasterxml.jackson.databind.d k = k();
        if (k != null) {
            if (k.B()) {
                return new a(k, this);
            }
            if (k.F()) {
                return new b(k, this);
            }
            throw new IllegalStateException("Current node of type " + k.getClass().getName());
        }
        throw new IllegalStateException("No current node");
    }

    public abstract JsonToken o();
}
