package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: BigIntegerNode.java */
/* loaded from: classes.dex */
public class c extends fj2 {
    public final BigInteger a;

    static {
        BigInteger.valueOf(-2147483648L);
        BigInteger.valueOf(2147483647L);
        BigInteger.valueOf(Long.MIN_VALUE);
        BigInteger.valueOf(Long.MAX_VALUE);
    }

    public c(BigInteger bigInteger) {
        this.a = bigInteger;
    }

    public static c K(BigInteger bigInteger) {
        return new c(bigInteger);
    }

    @Override // com.fasterxml.jackson.databind.d
    public long H() {
        return this.a.longValue();
    }

    @Override // com.fasterxml.jackson.databind.d
    public Number I() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.core.f
    public JsonParser.NumberType e() {
        return JsonParser.NumberType.BIG_INTEGER;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof c)) {
            return ((c) obj).a.equals(this.a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_NUMBER_INT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return this.a.toString();
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigInteger n() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigDecimal p() {
        return new BigDecimal(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public double q() {
        return this.a.doubleValue();
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException, JsonProcessingException {
        jsonGenerator.F0(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public int z() {
        return this.a.intValue();
    }
}
