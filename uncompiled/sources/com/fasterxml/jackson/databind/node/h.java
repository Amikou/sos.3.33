package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: FloatNode.java */
/* loaded from: classes.dex */
public class h extends fj2 {
    public final float a;

    public h(float f) {
        this.a = f;
    }

    public static h K(float f) {
        return new h(f);
    }

    @Override // com.fasterxml.jackson.databind.d
    public long H() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.d
    public Number I() {
        return Float.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.core.f
    public JsonParser.NumberType e() {
        return JsonParser.NumberType.FLOAT;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof h)) {
            return Float.compare(this.a, ((h) obj).a) == 0;
        }
        return false;
    }

    public int hashCode() {
        return Float.floatToIntBits(this.a);
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_NUMBER_FLOAT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return Float.toString(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigInteger n() {
        return p().toBigInteger();
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigDecimal p() {
        return BigDecimal.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public double q() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException {
        jsonGenerator.w0(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public int z() {
        return (int) this.a;
    }
}
