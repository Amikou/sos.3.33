package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;

/* compiled from: POJONode.java */
/* loaded from: classes.dex */
public class n extends r {
    public final Object a;

    public n(Object obj) {
        this.a = obj;
    }

    public boolean K(n nVar) {
        Object obj = this.a;
        if (obj == null) {
            return nVar.a == null;
        }
        return obj.equals(nVar.a);
    }

    public Object L() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof n)) {
            return K((n) obj);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_EMBEDDED_OBJECT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        Object obj = this.a;
        return obj == null ? "null" : obj.toString();
    }

    @Override // com.fasterxml.jackson.databind.d
    public byte[] o() throws IOException {
        Object obj = this.a;
        if (obj instanceof byte[]) {
            return (byte[]) obj;
        }
        return super.o();
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException {
        Object obj = this.a;
        if (obj == null) {
            kVar.defaultSerializeNull(jsonGenerator);
        } else if (obj instanceof com.fasterxml.jackson.databind.e) {
            ((com.fasterxml.jackson.databind.e) obj).serialize(jsonGenerator, kVar);
        } else {
            jsonGenerator.J0(obj);
        }
    }

    @Override // com.fasterxml.jackson.databind.node.r, com.fasterxml.jackson.databind.d
    public String toString() {
        Object obj = this.a;
        return obj instanceof byte[] ? String.format("(binary value of %d bytes)", Integer.valueOf(((byte[]) obj).length)) : obj instanceof com.fasterxml.jackson.databind.util.d ? String.format("(raw value '%s')", ((com.fasterxml.jackson.databind.util.d) obj).toString()) : String.valueOf(obj);
    }

    @Override // com.fasterxml.jackson.databind.d
    public JsonNodeType y() {
        return JsonNodeType.POJO;
    }
}
