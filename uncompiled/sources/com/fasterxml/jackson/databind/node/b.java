package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;

/* compiled from: BaseJsonNode.java */
/* loaded from: classes.dex */
public abstract class b extends com.fasterxml.jackson.databind.d {
    @Override // com.fasterxml.jackson.core.f
    public JsonParser.NumberType e() {
        return null;
    }

    @Override // com.fasterxml.jackson.databind.e
    public abstract void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException, JsonProcessingException;
}
