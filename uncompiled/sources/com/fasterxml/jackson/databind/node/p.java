package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;

/* compiled from: TextNode.java */
/* loaded from: classes.dex */
public class p extends r {
    public static final p f0 = new p("");
    public final String a;

    public p(String str) {
        this.a = str;
    }

    public static void O(StringBuilder sb, String str) {
        sb.append('\"');
        com.fasterxml.jackson.core.io.a.a(sb, str);
        sb.append('\"');
    }

    public static p R(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return f0;
        }
        return new p(str);
    }

    @Override // com.fasterxml.jackson.databind.d
    public String J() {
        return this.a;
    }

    public void K() throws JsonParseException {
        throw new JsonParseException((JsonParser) null, "Unexpected end-of-String when base64 content");
    }

    public void L(Base64Variant base64Variant, char c, int i) throws JsonParseException {
        N(base64Variant, c, i, null);
    }

    public void N(Base64Variant base64Variant, char c, int i, String str) throws JsonParseException {
        String str2;
        if (c <= ' ') {
            str2 = "Illegal white space character (code 0x" + Integer.toHexString(c) + ") as character #" + (i + 1) + " of 4-char base64 unit: can only used between units";
        } else if (base64Variant.usesPaddingChar(c)) {
            str2 = "Unexpected padding character ('" + base64Variant.getPaddingChar() + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (Character.isDefined(c) && !Character.isISOControl(c)) {
            str2 = "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
        } else {
            str2 = "Illegal character (code 0x" + Integer.toHexString(c) + ") in base64 content";
        }
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        throw new JsonParseException((JsonParser) null, str2, JsonLocation.NA);
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r6 >= 0) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x0025, code lost:
        L(r12, r4, 0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r5 < r2) goto L14;
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x002a, code lost:
        K();
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x002d, code lost:
        r4 = r5 + 1;
        r5 = r1.charAt(r5);
        r7 = r12.decodeBase64Char(r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0037, code lost:
        if (r7 >= 0) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x0039, code lost:
        L(r12, r5, 1);
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x003d, code lost:
        r5 = (r6 << 6) | r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0040, code lost:
        if (r4 < r2) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0046, code lost:
        if (r12.usesPadding() != false) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0048, code lost:
        r0.b(r5 >> 4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x004f, code lost:
        K();
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0052, code lost:
        r6 = r4 + 1;
        r4 = r1.charAt(r4);
        r7 = r12.decodeBase64Char(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x005f, code lost:
        if (r7 >= 0) goto L29;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0061, code lost:
        if (r7 == (-2)) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x0063, code lost:
        L(r12, r4, 2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0066, code lost:
        if (r6 < r2) goto L51;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0068, code lost:
        K();
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x006b, code lost:
        r4 = r6 + 1;
        r6 = r1.charAt(r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x0075, code lost:
        if (r12.usesPaddingChar(r6) != false) goto L54;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x0077, code lost:
        N(r12, r6, 3, "expected padding character '" + r12.getPaddingChar() + "'");
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x0094, code lost:
        r0.b(r5 >> 4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x009b, code lost:
        r4 = (r5 << 6) | r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x009e, code lost:
        if (r6 < r2) goto L37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x00a4, code lost:
        if (r12.usesPadding() != false) goto L34;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00a6, code lost:
        r0.d(r4 >> 2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00ac, code lost:
        K();
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x00af, code lost:
        r5 = r6 + 1;
        r6 = r1.charAt(r6);
        r7 = r12.decodeBase64Char(r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x00b9, code lost:
        if (r7 >= 0) goto L45;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x00bb, code lost:
        if (r7 == (-2)) goto L41;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x00bd, code lost:
        L(r12, r6, 3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00c0, code lost:
        r0.d(r4 >> 2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x00c6, code lost:
        r0.c((r4 << 6) | r7);
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00cc, code lost:
        r4 = r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x001f, code lost:
        r6 = r12.decodeBase64Char(r4);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public byte[] P(com.fasterxml.jackson.core.Base64Variant r12) throws java.io.IOException {
        /*
            r11 = this;
            ms r0 = new ms
            r1 = 100
            r0.<init>(r1)
            java.lang.String r1 = r11.a
            int r2 = r1.length()
            r3 = 0
            r4 = r3
        Lf:
            if (r4 >= r2) goto Ld2
        L11:
            int r5 = r4 + 1
            char r4 = r1.charAt(r4)
            if (r5 < r2) goto L1b
            goto Ld2
        L1b:
            r6 = 32
            if (r4 <= r6) goto Lcf
            int r6 = r12.decodeBase64Char(r4)
            if (r6 >= 0) goto L28
            r11.L(r12, r4, r3)
        L28:
            if (r5 < r2) goto L2d
            r11.K()
        L2d:
            int r4 = r5 + 1
            char r5 = r1.charAt(r5)
            int r7 = r12.decodeBase64Char(r5)
            if (r7 >= 0) goto L3d
            r8 = 1
            r11.L(r12, r5, r8)
        L3d:
            int r5 = r6 << 6
            r5 = r5 | r7
            if (r4 < r2) goto L52
            boolean r6 = r12.usesPadding()
            if (r6 != 0) goto L4f
            int r12 = r5 >> 4
            r0.b(r12)
            goto Ld2
        L4f:
            r11.K()
        L52:
            int r6 = r4 + 1
            char r4 = r1.charAt(r4)
            int r7 = r12.decodeBase64Char(r4)
            r8 = 3
            r9 = -2
            r10 = 2
            if (r7 >= 0) goto L9b
            if (r7 == r9) goto L66
            r11.L(r12, r4, r10)
        L66:
            if (r6 < r2) goto L6b
            r11.K()
        L6b:
            int r4 = r6 + 1
            char r6 = r1.charAt(r6)
            boolean r7 = r12.usesPaddingChar(r6)
            if (r7 != 0) goto L94
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r9 = "expected padding character '"
            r7.append(r9)
            char r9 = r12.getPaddingChar()
            r7.append(r9)
            java.lang.String r9 = "'"
            r7.append(r9)
            java.lang.String r7 = r7.toString()
            r11.N(r12, r6, r8, r7)
        L94:
            int r5 = r5 >> 4
            r0.b(r5)
            goto Lf
        L9b:
            int r4 = r5 << 6
            r4 = r4 | r7
            if (r6 < r2) goto Laf
            boolean r5 = r12.usesPadding()
            if (r5 != 0) goto Lac
            int r12 = r4 >> 2
            r0.d(r12)
            goto Ld2
        Lac:
            r11.K()
        Laf:
            int r5 = r6 + 1
            char r6 = r1.charAt(r6)
            int r7 = r12.decodeBase64Char(r6)
            if (r7 >= 0) goto Lc6
            if (r7 == r9) goto Lc0
            r11.L(r12, r6, r8)
        Lc0:
            int r4 = r4 >> 2
            r0.d(r4)
            goto Lcc
        Lc6:
            int r4 = r4 << 6
            r4 = r4 | r7
            r0.c(r4)
        Lcc:
            r4 = r5
            goto Lf
        Lcf:
            r4 = r5
            goto L11
        Ld2:
            byte[] r12 = r0.n()
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.node.p.P(com.fasterxml.jackson.core.Base64Variant):byte[]");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof p)) {
            return ((p) obj).a.equals(this.a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_STRING;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.d
    public byte[] o() throws IOException {
        return P(com.fasterxml.jackson.core.a.a());
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException {
        String str = this.a;
        if (str == null) {
            jsonGenerator.m0();
        } else {
            jsonGenerator.o1(str);
        }
    }

    @Override // com.fasterxml.jackson.databind.node.r, com.fasterxml.jackson.databind.d
    public String toString() {
        int length = this.a.length();
        StringBuilder sb = new StringBuilder(length + 2 + (length >> 4));
        O(sb, this.a);
        return sb.toString();
    }

    @Override // com.fasterxml.jackson.databind.d
    public JsonNodeType y() {
        return JsonNodeType.STRING;
    }
}
