package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;

/* compiled from: NullNode.java */
/* loaded from: classes.dex */
public final class l extends r {
    public static final l a = new l();

    public static l K() {
        return a;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public int hashCode() {
        return JsonNodeType.NULL.ordinal();
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_NULL;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return "null";
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException {
        kVar.defaultSerializeNull(jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.d
    public JsonNodeType y() {
        return JsonNodeType.NULL;
    }
}
