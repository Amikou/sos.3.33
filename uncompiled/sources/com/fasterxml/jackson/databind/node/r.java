package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;

/* compiled from: ValueNode.java */
/* loaded from: classes.dex */
public abstract class r extends b {
    @Override // com.fasterxml.jackson.databind.e
    public void serializeWithType(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException, JsonProcessingException {
        cVar.j(this, jsonGenerator);
        serialize(jsonGenerator, kVar);
        cVar.n(this, jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.d
    public String toString() {
        return m();
    }

    @Override // com.fasterxml.jackson.databind.d
    public final com.fasterxml.jackson.databind.d w(String str) {
        return null;
    }
}
