package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: IntNode.java */
/* loaded from: classes.dex */
public class i extends fj2 {
    public static final i[] f0 = new i[12];
    public final int a;

    static {
        for (int i = 0; i < 12; i++) {
            f0[i] = new i(i - 1);
        }
    }

    public i(int i) {
        this.a = i;
    }

    public static i K(int i) {
        if (i <= 10 && i >= -1) {
            return f0[i - (-1)];
        }
        return new i(i);
    }

    @Override // com.fasterxml.jackson.databind.d
    public long H() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.d
    public Number I() {
        return Integer.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.core.f
    public JsonParser.NumberType e() {
        return JsonParser.NumberType.INT;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return obj != null && (obj instanceof i) && ((i) obj).a == this.a;
    }

    public int hashCode() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_NUMBER_INT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return aj2.t(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigInteger n() {
        return BigInteger.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigDecimal p() {
        return BigDecimal.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public double q() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException, JsonProcessingException {
        jsonGenerator.x0(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public int z() {
        return this.a;
    }
}
