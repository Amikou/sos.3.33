package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: DecimalNode.java */
/* loaded from: classes.dex */
public class f extends fj2 {
    public static final f f0 = new f(BigDecimal.ZERO);
    public final BigDecimal a;

    static {
        BigDecimal.valueOf(-2147483648L);
        BigDecimal.valueOf(2147483647L);
        BigDecimal.valueOf(Long.MIN_VALUE);
        BigDecimal.valueOf(Long.MAX_VALUE);
    }

    public f(BigDecimal bigDecimal) {
        this.a = bigDecimal;
    }

    public static f K(BigDecimal bigDecimal) {
        return new f(bigDecimal);
    }

    @Override // com.fasterxml.jackson.databind.d
    public long H() {
        return this.a.longValue();
    }

    @Override // com.fasterxml.jackson.databind.d
    public Number I() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.core.f
    public JsonParser.NumberType e() {
        return JsonParser.NumberType.BIG_DECIMAL;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return obj != null && (obj instanceof f) && ((f) obj).a.compareTo(this.a) == 0;
    }

    public int hashCode() {
        return Double.valueOf(q()).hashCode();
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_NUMBER_FLOAT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return this.a.toString();
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigInteger n() {
        return this.a.toBigInteger();
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigDecimal p() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.d
    public double q() {
        return this.a.doubleValue();
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException, JsonProcessingException {
        jsonGenerator.B0(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public int z() {
        return this.a.intValue();
    }
}
