package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: BinaryNode.java */
/* loaded from: classes.dex */
public class d extends r {
    public static final d f0 = new d(new byte[0]);
    public final byte[] a;

    public d(byte[] bArr) {
        this.a = bArr;
    }

    public static d K(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        if (bArr.length == 0) {
            return f0;
        }
        return new d(bArr);
    }

    public static d L(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return null;
        }
        if (i2 == 0) {
            return f0;
        }
        return new d(bArr, i, i2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof d)) {
            return Arrays.equals(((d) obj).a, this.a);
        }
        return false;
    }

    public int hashCode() {
        byte[] bArr = this.a;
        if (bArr == null) {
            return -1;
        }
        return bArr.length;
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_EMBEDDED_OBJECT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return com.fasterxml.jackson.core.a.a().encode(this.a, false);
    }

    @Override // com.fasterxml.jackson.databind.d
    public byte[] o() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException, JsonProcessingException {
        Base64Variant base64Variant = kVar.getConfig().getBase64Variant();
        byte[] bArr = this.a;
        jsonGenerator.S(base64Variant, bArr, 0, bArr.length);
    }

    @Override // com.fasterxml.jackson.databind.node.r, com.fasterxml.jackson.databind.d
    public String toString() {
        return com.fasterxml.jackson.core.a.a().encode(this.a, true);
    }

    @Override // com.fasterxml.jackson.databind.d
    public JsonNodeType y() {
        return JsonNodeType.BINARY;
    }

    public d(byte[] bArr, int i, int i2) {
        if (i == 0 && i2 == bArr.length) {
            this.a = bArr;
            return;
        }
        byte[] bArr2 = new byte[i2];
        this.a = bArr2;
        System.arraycopy(bArr, i, bArr2, 0, i2);
    }
}
