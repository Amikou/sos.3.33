package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ObjectNode.java */
/* loaded from: classes.dex */
public class m extends e70<m> {
    public final Map<String, com.fasterxml.jackson.databind.d> f0;

    public m(JsonNodeFactory jsonNodeFactory) {
        super(jsonNodeFactory);
        this.f0 = new LinkedHashMap();
    }

    public boolean R(m mVar) {
        return this.f0.equals(mVar.f0);
    }

    public m S(String str, com.fasterxml.jackson.databind.d dVar) {
        this.f0.put(str, dVar);
        return this;
    }

    public m T(String str, String str2) {
        com.fasterxml.jackson.databind.d P;
        if (str2 == null) {
            P = N();
        } else {
            P = P(str2);
        }
        return S(str, P);
    }

    public m U(String str, boolean z) {
        return S(str, L(z));
    }

    public a V(String str) {
        a K = K();
        S(str, K);
        return K;
    }

    public com.fasterxml.jackson.databind.d Y(String str, com.fasterxml.jackson.databind.d dVar) {
        if (dVar == null) {
            dVar = N();
        }
        return this.f0.put(str, dVar);
    }

    public com.fasterxml.jackson.databind.d Z(String str, com.fasterxml.jackson.databind.d dVar) {
        if (dVar == null) {
            dVar = N();
        }
        this.f0.put(str, dVar);
        return this;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof m)) {
            return R((m) obj);
        }
        return false;
    }

    public int hashCode() {
        return this.f0.hashCode();
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.START_OBJECT;
    }

    @Override // com.fasterxml.jackson.databind.e.a
    public boolean k(com.fasterxml.jackson.databind.k kVar) {
        return this.f0.isEmpty();
    }

    @Override // com.fasterxml.jackson.databind.d
    public Iterator<com.fasterxml.jackson.databind.d> s() {
        return this.f0.values().iterator();
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException {
        boolean z = (kVar == null || kVar.isEnabled(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS)) ? false : true;
        jsonGenerator.k1(this);
        for (Map.Entry<String, com.fasterxml.jackson.databind.d> entry : this.f0.entrySet()) {
            b bVar = (b) entry.getValue();
            if (!z || !bVar.B() || !bVar.k(kVar)) {
                jsonGenerator.i0(entry.getKey());
                bVar.serialize(jsonGenerator, kVar);
            }
        }
        jsonGenerator.f0();
    }

    @Override // com.fasterxml.jackson.databind.e
    public void serializeWithType(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        boolean z = (kVar == null || kVar.isEnabled(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS)) ? false : true;
        cVar.i(this, jsonGenerator);
        for (Map.Entry<String, com.fasterxml.jackson.databind.d> entry : this.f0.entrySet()) {
            b bVar = (b) entry.getValue();
            if (!z || !bVar.B() || !bVar.k(kVar)) {
                jsonGenerator.i0(entry.getKey());
                bVar.serialize(jsonGenerator, kVar);
            }
        }
        cVar.m(this, jsonGenerator);
    }

    @Override // defpackage.e70
    public int size() {
        return this.f0.size();
    }

    @Override // com.fasterxml.jackson.databind.d
    public Iterator<Map.Entry<String, com.fasterxml.jackson.databind.d>> t() {
        return this.f0.entrySet().iterator();
    }

    @Override // com.fasterxml.jackson.databind.d
    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 32);
        sb.append("{");
        int i = 0;
        for (Map.Entry<String, com.fasterxml.jackson.databind.d> entry : this.f0.entrySet()) {
            if (i > 0) {
                sb.append(",");
            }
            i++;
            p.O(sb, entry.getKey());
            sb.append(':');
            sb.append(entry.getValue().toString());
        }
        sb.append("}");
        return sb.toString();
    }

    @Override // com.fasterxml.jackson.databind.d
    public com.fasterxml.jackson.databind.d w(String str) {
        return this.f0.get(str);
    }

    @Override // com.fasterxml.jackson.databind.d
    public JsonNodeType y() {
        return JsonNodeType.OBJECT;
    }
}
