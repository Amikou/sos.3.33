package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;

/* compiled from: BooleanNode.java */
/* loaded from: classes.dex */
public class e extends r {
    public static final e f0 = new e(true);
    public static final e g0 = new e(false);
    public final boolean a;

    public e(boolean z) {
        this.a = z;
    }

    public static e K() {
        return g0;
    }

    public static e L() {
        return f0;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return obj != null && (obj instanceof e) && this.a == ((e) obj).a;
    }

    public int hashCode() {
        return this.a ? 3 : 1;
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return this.a ? JsonToken.VALUE_TRUE : JsonToken.VALUE_FALSE;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return this.a ? "true" : "false";
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException {
        jsonGenerator.a0(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public JsonNodeType y() {
        return JsonNodeType.BOOLEAN;
    }
}
