package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: ShortNode.java */
/* loaded from: classes.dex */
public class o extends fj2 {
    public final short a;

    public o(short s) {
        this.a = s;
    }

    public static o K(short s) {
        return new o(s);
    }

    @Override // com.fasterxml.jackson.databind.d
    public long H() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.d
    public Number I() {
        return Short.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.core.f
    public JsonParser.NumberType e() {
        return JsonParser.NumberType.INT;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return obj != null && (obj instanceof o) && ((o) obj).a == this.a;
    }

    public int hashCode() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.VALUE_NUMBER_INT;
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return aj2.t(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigInteger n() {
        return BigInteger.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public BigDecimal p() {
        return BigDecimal.valueOf(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public double q() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public final void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException, JsonProcessingException {
        jsonGenerator.H0(this.a);
    }

    @Override // com.fasterxml.jackson.databind.d
    public int z() {
        return this.a;
    }
}
