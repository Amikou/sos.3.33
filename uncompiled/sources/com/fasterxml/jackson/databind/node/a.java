package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ArrayNode.java */
/* loaded from: classes.dex */
public class a extends e70<a> {
    public final List<com.fasterxml.jackson.databind.d> f0;

    public a(JsonNodeFactory jsonNodeFactory) {
        super(jsonNodeFactory);
        this.f0 = new ArrayList();
    }

    public a R(com.fasterxml.jackson.databind.d dVar) {
        this.f0.add(dVar);
        return this;
    }

    public a S(com.fasterxml.jackson.databind.d dVar) {
        if (dVar == null) {
            dVar = N();
        }
        R(dVar);
        return this;
    }

    public a T(String str) {
        if (str == null) {
            return U();
        }
        return R(P(str));
    }

    public a U() {
        R(N());
        return this;
    }

    public com.fasterxml.jackson.databind.d V(int i) {
        if (i < 0 || i >= this.f0.size()) {
            return null;
        }
        return this.f0.get(i);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof a)) {
            return this.f0.equals(((a) obj).f0);
        }
        return false;
    }

    public int hashCode() {
        return this.f0.hashCode();
    }

    @Override // com.fasterxml.jackson.core.f
    public JsonToken i() {
        return JsonToken.START_ARRAY;
    }

    @Override // com.fasterxml.jackson.databind.e.a
    public boolean k(com.fasterxml.jackson.databind.k kVar) {
        return this.f0.isEmpty();
    }

    @Override // com.fasterxml.jackson.databind.d
    public Iterator<com.fasterxml.jackson.databind.d> s() {
        return this.f0.iterator();
    }

    @Override // com.fasterxml.jackson.databind.node.b, com.fasterxml.jackson.databind.e
    public void serialize(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar) throws IOException {
        List<com.fasterxml.jackson.databind.d> list = this.f0;
        int size = list.size();
        jsonGenerator.f1(size);
        for (int i = 0; i < size; i++) {
            com.fasterxml.jackson.databind.d dVar = list.get(i);
            if (dVar instanceof b) {
                ((b) dVar).serialize(jsonGenerator, kVar);
            } else {
                dVar.serialize(jsonGenerator, kVar);
            }
        }
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.e
    public void serializeWithType(JsonGenerator jsonGenerator, com.fasterxml.jackson.databind.k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        cVar.h(this, jsonGenerator);
        Iterator<com.fasterxml.jackson.databind.d> it = this.f0.iterator();
        while (it.hasNext()) {
            ((b) it.next()).serialize(jsonGenerator, kVar);
        }
        cVar.l(this, jsonGenerator);
    }

    @Override // defpackage.e70
    public int size() {
        return this.f0.size();
    }

    @Override // com.fasterxml.jackson.databind.d
    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 16);
        sb.append('[');
        int size = this.f0.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append(',');
            }
            sb.append(this.f0.get(i).toString());
        }
        sb.append(']');
        return sb.toString();
    }

    @Override // com.fasterxml.jackson.databind.d
    public com.fasterxml.jackson.databind.d w(String str) {
        return null;
    }

    @Override // com.fasterxml.jackson.databind.d
    public JsonNodeType y() {
        return JsonNodeType.ARRAY;
    }

    public a(JsonNodeFactory jsonNodeFactory, int i) {
        super(jsonNodeFactory);
        this.f0 = new ArrayList(i);
    }
}
