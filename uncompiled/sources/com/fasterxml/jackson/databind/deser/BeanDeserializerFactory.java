package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators$PropertyGenerator;
import com.fasterxml.jackson.annotation.t;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.cfg.DeserializerFactoryConfig;
import com.fasterxml.jackson.databind.deser.impl.FieldProperty;
import com.fasterxml.jackson.databind.deser.impl.MethodProperty;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdReader;
import com.fasterxml.jackson.databind.deser.impl.PropertyBasedObjectIdGenerator;
import com.fasterxml.jackson.databind.deser.impl.SetterlessProperty;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes.dex */
public class BeanDeserializerFactory extends BasicDeserializerFactory {
    private static final long serialVersionUID = 1;
    public static final Class<?>[] j0 = {Throwable.class};
    public static final Class<?>[] k0 = new Class[0];
    public static final BeanDeserializerFactory instance = new BeanDeserializerFactory(new DeserializerFactoryConfig());

    public BeanDeserializerFactory(DeserializerFactoryConfig deserializerFactoryConfig) {
        super(deserializerFactoryConfig);
    }

    public void _validateSubType(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.jsontype.impl.h.a().b(deserializationContext, javaType);
    }

    public void addBeanProps(DeserializationContext deserializationContext, so soVar, to toVar) throws JsonMappingException {
        Set<String> emptySet;
        AnnotatedMember c;
        SettableBeanProperty settableBeanProperty;
        boolean z;
        CreatorProperty creatorProperty;
        Set<String> w;
        boolean z2 = true;
        SettableBeanProperty[] fromObjectArguments = soVar.y().isAbstract() ^ true ? toVar.p().getFromObjectArguments(deserializationContext.getConfig()) : null;
        boolean z3 = fromObjectArguments != null;
        JsonIgnoreProperties.Value defaultPropertyIgnorals = deserializationContext.getConfig().getDefaultPropertyIgnorals(soVar.r(), soVar.t());
        if (defaultPropertyIgnorals != null) {
            toVar.r(defaultPropertyIgnorals.getIgnoreUnknown());
            emptySet = defaultPropertyIgnorals.findIgnoredForDeserialization();
            for (String str : emptySet) {
                toVar.d(str);
            }
        } else {
            emptySet = Collections.emptySet();
        }
        Set<String> set = emptySet;
        AnnotatedMethod b = soVar.b();
        if (b != null) {
            toVar.q(constructAnySetter(deserializationContext, soVar, b));
            c = null;
        } else {
            c = soVar.c();
            if (c != null) {
                toVar.q(constructAnySetter(deserializationContext, soVar, c));
            }
        }
        if (b == null && c == null && (w = soVar.w()) != null) {
            for (String str2 : w) {
                toVar.d(str2);
            }
        }
        boolean z4 = deserializationContext.isEnabled(MapperFeature.USE_GETTERS_AS_SETTERS) && deserializationContext.isEnabled(MapperFeature.AUTO_DETECT_GETTERS);
        List<vo> filterBeanProps = filterBeanProps(deserializationContext, soVar, toVar, soVar.n(), set);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                filterBeanProps = uoVar.updateProperties(deserializationContext.getConfig(), soVar, filterBeanProps);
            }
        }
        for (vo voVar : filterBeanProps) {
            if (voVar.E()) {
                settableBeanProperty = constructSettableProperty(deserializationContext, soVar, voVar, voVar.x().getParameterType(0));
            } else if (voVar.B()) {
                settableBeanProperty = constructSettableProperty(deserializationContext, soVar, voVar, voVar.l().getType());
            } else {
                if (z4 && voVar.C()) {
                    Class<?> rawType = voVar.p().getRawType();
                    if (Collection.class.isAssignableFrom(rawType) || Map.class.isAssignableFrom(rawType)) {
                        settableBeanProperty = constructSetterlessProperty(deserializationContext, soVar, voVar);
                    }
                }
                settableBeanProperty = null;
            }
            if (z3 && voVar.A()) {
                String t = voVar.t();
                if (fromObjectArguments != null) {
                    for (SettableBeanProperty settableBeanProperty2 : fromObjectArguments) {
                        if (t.equals(settableBeanProperty2.getName()) && (settableBeanProperty2 instanceof CreatorProperty)) {
                            creatorProperty = (CreatorProperty) settableBeanProperty2;
                            break;
                        }
                    }
                }
                creatorProperty = null;
                if (creatorProperty == null) {
                    ArrayList arrayList = new ArrayList();
                    for (SettableBeanProperty settableBeanProperty3 : fromObjectArguments) {
                        arrayList.add(settableBeanProperty3.getName());
                    }
                    z = true;
                    deserializationContext.reportBadPropertyDefinition(soVar, voVar, "Could not find creator property with name '%s' (known Creator properties: %s)", t, arrayList);
                } else {
                    z = true;
                    if (settableBeanProperty != null) {
                        creatorProperty.setFallbackSetter(settableBeanProperty);
                    }
                    toVar.c(creatorProperty);
                }
            } else {
                z = z2;
                if (settableBeanProperty != null) {
                    Class<?>[] h = voVar.h();
                    if (h == null && !deserializationContext.isEnabled(MapperFeature.DEFAULT_VIEW_INCLUSION)) {
                        h = k0;
                    }
                    settableBeanProperty.setViews(h);
                    toVar.g(settableBeanProperty);
                }
            }
            z2 = z;
        }
    }

    public void addInjectables(DeserializationContext deserializationContext, so soVar, to toVar) throws JsonMappingException {
        Map<Object, AnnotatedMember> i = soVar.i();
        if (i != null) {
            for (Map.Entry<Object, AnnotatedMember> entry : i.entrySet()) {
                AnnotatedMember value = entry.getValue();
                toVar.e(PropertyName.construct(value.getName()), value.getType(), soVar.s(), value, entry.getKey());
            }
        }
    }

    public void addObjectIdReader(DeserializationContext deserializationContext, so soVar, to toVar) throws JsonMappingException {
        SettableBeanProperty settableBeanProperty;
        PropertyBasedObjectIdGenerator objectIdGeneratorInstance;
        JavaType javaType;
        jl2 x = soVar.x();
        if (x == null) {
            return;
        }
        Class<? extends ObjectIdGenerator<?>> c = x.c();
        t objectIdResolverInstance = deserializationContext.objectIdResolverInstance(soVar.t(), x);
        if (c == ObjectIdGenerators$PropertyGenerator.class) {
            PropertyName d = x.d();
            settableBeanProperty = toVar.k(d);
            if (settableBeanProperty != null) {
                JavaType type = settableBeanProperty.getType();
                javaType = type;
                objectIdGeneratorInstance = new PropertyBasedObjectIdGenerator(x.f());
            } else {
                throw new IllegalArgumentException("Invalid Object Id definition for " + soVar.r().getName() + ": can not find property with name '" + d + "'");
            }
        } else {
            JavaType javaType2 = deserializationContext.getTypeFactory().findTypeParameters(deserializationContext.constructType((Class<?>) c), ObjectIdGenerator.class)[0];
            settableBeanProperty = null;
            objectIdGeneratorInstance = deserializationContext.objectIdGeneratorInstance(soVar.t(), x);
            javaType = javaType2;
        }
        com.fasterxml.jackson.databind.c<Object> findRootValueDeserializer = deserializationContext.findRootValueDeserializer(javaType);
        toVar.s(ObjectIdReader.construct(javaType, x.d(), objectIdGeneratorInstance, findRootValueDeserializer, settableBeanProperty, objectIdResolverInstance));
    }

    public void addReferenceProperties(DeserializationContext deserializationContext, so soVar, to toVar) throws JsonMappingException {
        JavaType type;
        Map<String, AnnotatedMember> d = soVar.d();
        if (d != null) {
            for (Map.Entry<String, AnnotatedMember> entry : d.entrySet()) {
                String key = entry.getKey();
                AnnotatedMember value = entry.getValue();
                if (value instanceof AnnotatedMethod) {
                    type = ((AnnotatedMethod) value).getParameterType(0);
                } else {
                    type = value.getType();
                    if (value instanceof AnnotatedParameter) {
                        deserializationContext.reportBadTypeDefinition(soVar, "Can not bind back references as Creator parameters: type %s (reference '%s', parameter index #%d)", soVar.r().getName(), key, Integer.valueOf(((AnnotatedParameter) value).getIndex()));
                    }
                }
                toVar.b(key, constructSettableProperty(deserializationContext, soVar, wo3.J(deserializationContext.getConfig(), value, PropertyName.construct(key)), type));
            }
        }
    }

    public com.fasterxml.jackson.databind.c<Object> buildBeanDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.c<?> h;
        try {
            i findValueInstantiator = findValueInstantiator(deserializationContext, soVar);
            to constructBeanDeserializerBuilder = constructBeanDeserializerBuilder(deserializationContext, soVar);
            constructBeanDeserializerBuilder.u(findValueInstantiator);
            addBeanProps(deserializationContext, soVar, constructBeanDeserializerBuilder);
            addObjectIdReader(deserializationContext, soVar, constructBeanDeserializerBuilder);
            addReferenceProperties(deserializationContext, soVar, constructBeanDeserializerBuilder);
            addInjectables(deserializationContext, soVar, constructBeanDeserializerBuilder);
            DeserializationConfig config = deserializationContext.getConfig();
            if (this._factoryConfig.hasDeserializerModifiers()) {
                for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                    constructBeanDeserializerBuilder = uoVar.updateBuilder(config, soVar, constructBeanDeserializerBuilder);
                }
            }
            if (javaType.isAbstract() && !findValueInstantiator.canInstantiate()) {
                h = constructBeanDeserializerBuilder.i();
            } else {
                h = constructBeanDeserializerBuilder.h();
            }
            if (this._factoryConfig.hasDeserializerModifiers()) {
                for (uo uoVar2 : this._factoryConfig.deserializerModifiers()) {
                    h = uoVar2.modifyDeserializer(config, soVar, h);
                }
            }
            return h;
        } catch (NoClassDefFoundError e) {
            return new com.fasterxml.jackson.databind.deser.impl.a(e);
        }
    }

    public com.fasterxml.jackson.databind.c<Object> buildBuilderBasedDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        i findValueInstantiator = findValueInstantiator(deserializationContext, soVar);
        DeserializationConfig config = deserializationContext.getConfig();
        to constructBeanDeserializerBuilder = constructBeanDeserializerBuilder(deserializationContext, soVar);
        constructBeanDeserializerBuilder.u(findValueInstantiator);
        addBeanProps(deserializationContext, soVar, constructBeanDeserializerBuilder);
        addObjectIdReader(deserializationContext, soVar, constructBeanDeserializerBuilder);
        addReferenceProperties(deserializationContext, soVar, constructBeanDeserializerBuilder);
        addInjectables(deserializationContext, soVar, constructBeanDeserializerBuilder);
        d.a m = soVar.m();
        String str = m == null ? "build" : m.a;
        AnnotatedMethod k = soVar.k(str, null);
        if (k != null && config.canOverrideAccessModifiers()) {
            com.fasterxml.jackson.databind.util.c.f(k.getMember(), config.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
        }
        constructBeanDeserializerBuilder.t(k, m);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                constructBeanDeserializerBuilder = uoVar.updateBuilder(config, soVar, constructBeanDeserializerBuilder);
            }
        }
        com.fasterxml.jackson.databind.c<?> j = constructBeanDeserializerBuilder.j(javaType, str);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar2 : this._factoryConfig.deserializerModifiers()) {
                j = uoVar2.modifyDeserializer(config, soVar, j);
            }
        }
        return j;
    }

    public com.fasterxml.jackson.databind.c<Object> buildThrowableDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        SettableBeanProperty constructSettableProperty;
        DeserializationConfig config = deserializationContext.getConfig();
        to constructBeanDeserializerBuilder = constructBeanDeserializerBuilder(deserializationContext, soVar);
        constructBeanDeserializerBuilder.u(findValueInstantiator(deserializationContext, soVar));
        addBeanProps(deserializationContext, soVar, constructBeanDeserializerBuilder);
        AnnotatedMethod k = soVar.k("initCause", j0);
        if (k != null && (constructSettableProperty = constructSettableProperty(deserializationContext, soVar, wo3.J(deserializationContext.getConfig(), k, new PropertyName("cause")), k.getParameterType(0))) != null) {
            constructBeanDeserializerBuilder.f(constructSettableProperty, true);
        }
        constructBeanDeserializerBuilder.d("localizedMessage");
        constructBeanDeserializerBuilder.d("suppressed");
        constructBeanDeserializerBuilder.d(ThrowableDeserializer.PROP_NAME_MESSAGE);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                constructBeanDeserializerBuilder = uoVar.updateBuilder(config, soVar, constructBeanDeserializerBuilder);
            }
        }
        com.fasterxml.jackson.databind.c<?> h = constructBeanDeserializerBuilder.h();
        if (h instanceof BeanDeserializer) {
            h = new ThrowableDeserializer((BeanDeserializer) h);
        }
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar2 : this._factoryConfig.deserializerModifiers()) {
                h = uoVar2.modifyDeserializer(config, soVar, h);
            }
        }
        return h;
    }

    public SettableAnyProperty constructAnySetter(DeserializationContext deserializationContext, so soVar, AnnotatedMember annotatedMember) throws JsonMappingException {
        JavaType contentType;
        if (annotatedMember instanceof AnnotatedMethod) {
            contentType = ((AnnotatedMethod) annotatedMember).getParameterType(1);
        } else {
            contentType = annotatedMember instanceof AnnotatedField ? ((AnnotatedField) annotatedMember).getType().getContentType() : null;
        }
        JavaType resolveMemberAndTypeAnnotations = resolveMemberAndTypeAnnotations(deserializationContext, annotatedMember, contentType);
        a.C0091a c0091a = new a.C0091a(PropertyName.construct(annotatedMember.getName()), resolveMemberAndTypeAnnotations, null, soVar.s(), annotatedMember, PropertyMetadata.STD_OPTIONAL);
        com.fasterxml.jackson.databind.c<?> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationContext, annotatedMember);
        if (findDeserializerFromAnnotation == null) {
            findDeserializerFromAnnotation = (com.fasterxml.jackson.databind.c) resolveMemberAndTypeAnnotations.getValueHandler();
        }
        return new SettableAnyProperty(c0091a, annotatedMember, resolveMemberAndTypeAnnotations, findDeserializerFromAnnotation != null ? deserializationContext.handlePrimaryContextualization(findDeserializerFromAnnotation, c0091a, resolveMemberAndTypeAnnotations) : findDeserializerFromAnnotation, (com.fasterxml.jackson.databind.jsontype.a) resolveMemberAndTypeAnnotations.getTypeHandler());
    }

    public to constructBeanDeserializerBuilder(DeserializationContext deserializationContext, so soVar) {
        return new to(soVar, deserializationContext.getConfig());
    }

    public SettableBeanProperty constructSettableProperty(DeserializationContext deserializationContext, so soVar, vo voVar, JavaType javaType) throws JsonMappingException {
        SettableBeanProperty fieldProperty;
        AnnotatedMember u = voVar.u();
        if (u == null) {
            deserializationContext.reportBadPropertyDefinition(soVar, voVar, "No non-constructor mutator available", new Object[0]);
        }
        JavaType resolveMemberAndTypeAnnotations = resolveMemberAndTypeAnnotations(deserializationContext, u, javaType);
        com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) resolveMemberAndTypeAnnotations.getTypeHandler();
        if (u instanceof AnnotatedMethod) {
            fieldProperty = new MethodProperty(voVar, resolveMemberAndTypeAnnotations, aVar, soVar.s(), (AnnotatedMethod) u);
        } else {
            fieldProperty = new FieldProperty(voVar, resolveMemberAndTypeAnnotations, aVar, soVar.s(), (AnnotatedField) u);
        }
        com.fasterxml.jackson.databind.c<?> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationContext, u);
        if (findDeserializerFromAnnotation == null) {
            findDeserializerFromAnnotation = (com.fasterxml.jackson.databind.c) resolveMemberAndTypeAnnotations.getValueHandler();
        }
        if (findDeserializerFromAnnotation != null) {
            fieldProperty = fieldProperty.withValueDeserializer(deserializationContext.handlePrimaryContextualization(findDeserializerFromAnnotation, fieldProperty, resolveMemberAndTypeAnnotations));
        }
        AnnotationIntrospector.ReferenceProperty g = voVar.g();
        if (g != null && g.d()) {
            fieldProperty.setManagedReferenceName(g.b());
        }
        jl2 f = voVar.f();
        if (f != null) {
            fieldProperty.setObjectIdInfo(f);
        }
        return fieldProperty;
    }

    public SettableBeanProperty constructSetterlessProperty(DeserializationContext deserializationContext, so soVar, vo voVar) throws JsonMappingException {
        AnnotatedMethod p = voVar.p();
        JavaType resolveMemberAndTypeAnnotations = resolveMemberAndTypeAnnotations(deserializationContext, p, p.getType());
        SetterlessProperty setterlessProperty = new SetterlessProperty(voVar, resolveMemberAndTypeAnnotations, (com.fasterxml.jackson.databind.jsontype.a) resolveMemberAndTypeAnnotations.getTypeHandler(), soVar.s(), p);
        com.fasterxml.jackson.databind.c<?> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationContext, p);
        if (findDeserializerFromAnnotation == null) {
            findDeserializerFromAnnotation = (com.fasterxml.jackson.databind.c) resolveMemberAndTypeAnnotations.getValueHandler();
        }
        return findDeserializerFromAnnotation != null ? setterlessProperty.withValueDeserializer(deserializationContext.handlePrimaryContextualization(findDeserializerFromAnnotation, setterlessProperty, resolveMemberAndTypeAnnotations)) : setterlessProperty;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<Object> createBeanDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        JavaType materializeAbstractType;
        DeserializationConfig config = deserializationContext.getConfig();
        com.fasterxml.jackson.databind.c<Object> _findCustomBeanDeserializer = _findCustomBeanDeserializer(javaType, config, soVar);
        if (_findCustomBeanDeserializer != null) {
            return _findCustomBeanDeserializer;
        }
        if (javaType.isThrowable()) {
            return buildThrowableDeserializer(deserializationContext, javaType, soVar);
        }
        if (javaType.isAbstract() && !javaType.isPrimitive() && !javaType.isEnumType() && (materializeAbstractType = materializeAbstractType(deserializationContext, javaType, soVar)) != null) {
            return buildBeanDeserializer(deserializationContext, materializeAbstractType, config.introspect(materializeAbstractType));
        }
        com.fasterxml.jackson.databind.c<?> findStdDeserializer = findStdDeserializer(deserializationContext, javaType, soVar);
        if (findStdDeserializer != null) {
            return findStdDeserializer;
        }
        if (isPotentialBeanType(javaType.getRawClass())) {
            _validateSubType(deserializationContext, javaType, soVar);
            return buildBeanDeserializer(deserializationContext, javaType, soVar);
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<Object> createBuilderBasedDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar, Class<?> cls) throws JsonMappingException {
        return buildBuilderBasedDeserializer(deserializationContext, javaType, deserializationContext.getConfig().introspectForBuilder(deserializationContext.constructType(cls)));
    }

    public List<vo> filterBeanProps(DeserializationContext deserializationContext, so soVar, to toVar, List<vo> list, Set<String> set) throws JsonMappingException {
        ArrayList arrayList = new ArrayList(Math.max(4, list.size()));
        HashMap hashMap = new HashMap();
        for (vo voVar : list) {
            String t = voVar.t();
            if (!set.contains(t)) {
                if (!voVar.A()) {
                    Class<?> cls = null;
                    if (voVar.E()) {
                        cls = voVar.x().getRawParameterType(0);
                    } else if (voVar.B()) {
                        cls = voVar.l().getRawType();
                    }
                    if (cls != null && isIgnorableType(deserializationContext.getConfig(), soVar, cls, hashMap)) {
                        toVar.d(t);
                    }
                }
                arrayList.add(voVar);
            }
        }
        return arrayList;
    }

    public com.fasterxml.jackson.databind.c<?> findStdDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.c<?> findDefaultDeserializer = findDefaultDeserializer(deserializationContext, javaType, soVar);
        if (findDefaultDeserializer != null && this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                findDefaultDeserializer = uoVar.modifyDeserializer(deserializationContext.getConfig(), soVar, findDefaultDeserializer);
            }
        }
        return findDefaultDeserializer;
    }

    public boolean isIgnorableType(DeserializationConfig deserializationConfig, so soVar, Class<?> cls, Map<Class<?>, Boolean> map) {
        Boolean bool = map.get(cls);
        if (bool != null) {
            return bool.booleanValue();
        }
        w40 findConfigOverride = deserializationConfig.findConfigOverride(cls);
        if (findConfigOverride != null) {
            bool = findConfigOverride.getIsIgnoredType();
        }
        if (bool == null) {
            bool = deserializationConfig.getAnnotationIntrospector().isIgnorableType(deserializationConfig.introspectClassAnnotations(cls).t());
            if (bool == null) {
                bool = Boolean.FALSE;
            }
        }
        map.put(cls, bool);
        return bool.booleanValue();
    }

    public boolean isPotentialBeanType(Class<?> cls) {
        String d = com.fasterxml.jackson.databind.util.c.d(cls);
        if (d == null) {
            if (!com.fasterxml.jackson.databind.util.c.N(cls)) {
                String K = com.fasterxml.jackson.databind.util.c.K(cls, true);
                if (K == null) {
                    return true;
                }
                throw new IllegalArgumentException("Can not deserialize Class " + cls.getName() + " (of type " + K + ") as a Bean");
            }
            throw new IllegalArgumentException("Can not deserialize Proxy class " + cls.getName() + " as a Bean");
        }
        throw new IllegalArgumentException("Can not deserialize Class " + cls.getName() + " (of type " + d + ") as a Bean");
    }

    public JavaType materializeAbstractType(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        for (o5 o5Var : this._factoryConfig.abstractTypeResolvers()) {
            JavaType resolveAbstractType = o5Var.resolveAbstractType(deserializationContext.getConfig(), soVar);
            if (resolveAbstractType != null) {
                return resolveAbstractType;
            }
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.deser.BasicDeserializerFactory
    public e withConfig(DeserializerFactoryConfig deserializerFactoryConfig) {
        if (this._factoryConfig == deserializerFactoryConfig) {
            return this;
        }
        if (getClass() == BeanDeserializerFactory.class) {
            return new BeanDeserializerFactory(deserializerFactoryConfig);
        }
        throw new IllegalStateException("Subtype of BeanDeserializerFactory (" + getClass().getName() + ") has not properly overridden method 'withAdditionalDeserializers': can not instantiate subtype with additional deserializer definitions");
    }
}
