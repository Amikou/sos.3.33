package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.deser.impl.e;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/* loaded from: classes.dex */
public class SettableAnyProperty implements Serializable {
    private static final long serialVersionUID = 1;
    public final com.fasterxml.jackson.databind.a _property;
    public final AnnotatedMember _setter;
    public final boolean _setterIsField;
    public final JavaType _type;
    public com.fasterxml.jackson.databind.c<Object> _valueDeserializer;
    public final com.fasterxml.jackson.databind.jsontype.a _valueTypeDeserializer;

    /* loaded from: classes.dex */
    public static class a extends e.a {
        public final SettableAnyProperty c;
        public final Object d;
        public final String e;

        public a(SettableAnyProperty settableAnyProperty, UnresolvedForwardReference unresolvedForwardReference, Class<?> cls, Object obj, String str) {
            super(unresolvedForwardReference, cls);
            this.c = settableAnyProperty;
            this.d = obj;
            this.e = str;
        }

        @Override // com.fasterxml.jackson.databind.deser.impl.e.a
        public void c(Object obj, Object obj2) throws IOException {
            if (d(obj)) {
                this.c.set(this.d, this.e, obj2);
                return;
            }
            throw new IllegalArgumentException("Trying to resolve a forward reference with id [" + obj.toString() + "] that wasn't previously registered.");
        }
    }

    public SettableAnyProperty(com.fasterxml.jackson.databind.a aVar, AnnotatedMember annotatedMember, JavaType javaType, com.fasterxml.jackson.databind.c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar2) {
        this._property = aVar;
        this._setter = annotatedMember;
        this._type = javaType;
        this._valueDeserializer = cVar;
        this._valueTypeDeserializer = aVar2;
        this._setterIsField = annotatedMember instanceof AnnotatedField;
    }

    public void _throwAsIOE(Exception exc, String str, Object obj) throws IOException {
        if (exc instanceof IllegalArgumentException) {
            String name = obj == null ? "[NULL]" : obj.getClass().getName();
            StringBuilder sb = new StringBuilder("Problem deserializing \"any\" property '");
            sb.append(str);
            sb.append("' of class " + a() + " (expected type: ");
            sb.append(this._type);
            sb.append("; actual type: ");
            sb.append(name);
            sb.append(")");
            String message = exc.getMessage();
            if (message != null) {
                sb.append(", problem: ");
                sb.append(message);
            } else {
                sb.append(" (no error message provided)");
            }
            throw new JsonMappingException((Closeable) null, sb.toString(), exc);
        } else if (!(exc instanceof IOException)) {
            boolean z = exc instanceof RuntimeException;
            Exception exc2 = exc;
            if (!z) {
                while (exc2.getCause() != null) {
                    exc2 = exc2.getCause();
                }
                throw new JsonMappingException((Closeable) null, exc2.getMessage(), exc2);
            }
            throw ((RuntimeException) exc);
        } else {
            throw ((IOException) exc);
        }
    }

    public final String a() {
        return this._setter.getDeclaringClass().getName();
    }

    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.u() == JsonToken.VALUE_NULL) {
            return this._valueDeserializer.getNullValue(deserializationContext);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        if (aVar != null) {
            return this._valueDeserializer.deserializeWithType(jsonParser, deserializationContext, aVar);
        }
        return this._valueDeserializer.deserialize(jsonParser, deserializationContext);
    }

    public final void deserializeAndSet(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj, String str) throws IOException {
        try {
            set(obj, str, deserialize(jsonParser, deserializationContext));
        } catch (UnresolvedForwardReference e) {
            if (this._valueDeserializer.getObjectIdReader() != null) {
                e.getRoid().a(new a(this, e, this._type.getRawClass(), obj, str));
                return;
            }
            throw JsonMappingException.from(jsonParser, "Unresolved forward reference but no identity info.", e);
        }
    }

    public void fixAccess(DeserializationConfig deserializationConfig) {
        this._setter.fixAccess(deserializationConfig.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
    }

    public com.fasterxml.jackson.databind.a getProperty() {
        return this._property;
    }

    public JavaType getType() {
        return this._type;
    }

    public boolean hasValueDeserializer() {
        return this._valueDeserializer != null;
    }

    public Object readResolve() {
        AnnotatedMember annotatedMember = this._setter;
        if (annotatedMember == null || annotatedMember.getAnnotated() == null) {
            throw new IllegalArgumentException("Missing method (broken JDK (de)serialization?)");
        }
        return this;
    }

    public void set(Object obj, String str, Object obj2) throws IOException {
        try {
            if (this._setterIsField) {
                Map map = (Map) ((AnnotatedField) this._setter).getValue(obj);
                if (map != null) {
                    map.put(str, obj2);
                }
            } else {
                ((AnnotatedMethod) this._setter).callOnWith(obj, str, obj2);
            }
        } catch (Exception e) {
            _throwAsIOE(e, str, obj2);
        }
    }

    public String toString() {
        return "[any property on class " + a() + "]";
    }

    public SettableAnyProperty withValueDeserializer(com.fasterxml.jackson.databind.c<Object> cVar) {
        return new SettableAnyProperty(this._property, this._setter, this._type, cVar, this._valueTypeDeserializer);
    }

    public SettableAnyProperty(SettableAnyProperty settableAnyProperty) {
        this._property = settableAnyProperty._property;
        this._setter = settableAnyProperty._setter;
        this._type = settableAnyProperty._type;
        this._valueDeserializer = settableAnyProperty._valueDeserializer;
        this._valueTypeDeserializer = settableAnyProperty._valueTypeDeserializer;
        this._setterIsField = settableAnyProperty._setterIsField;
    }
}
