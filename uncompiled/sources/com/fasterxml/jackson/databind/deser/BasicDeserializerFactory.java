package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.cfg.DeserializerFactoryConfig;
import com.fasterxml.jackson.databind.deser.impl.CreatorCollector;
import com.fasterxml.jackson.databind.deser.std.ArrayBlockingQueueDeserializer;
import com.fasterxml.jackson.databind.deser.std.AtomicReferenceDeserializer;
import com.fasterxml.jackson.databind.deser.std.CollectionDeserializer;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.fasterxml.jackson.databind.deser.std.EnumDeserializer;
import com.fasterxml.jackson.databind.deser.std.EnumSetDeserializer;
import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.fasterxml.jackson.databind.deser.std.MapEntryDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;
import com.fasterxml.jackson.databind.deser.std.ObjectArrayDeserializer;
import com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers;
import com.fasterxml.jackson.databind.deser.std.StdKeyDeserializers;
import com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringCollectionDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.deser.std.TokenBufferDeserializer;
import com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer;
import com.fasterxml.jackson.databind.ext.OptionalHandlerFactory;
import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.AnnotatedWithParams;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.ReferenceType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.EnumResolver;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public abstract class BasicDeserializerFactory extends e implements Serializable {
    public static final HashMap<String, Class<? extends Collection>> _collectionFallbacks;
    public static final HashMap<String, Class<? extends Map>> _mapFallbacks;
    public final DeserializerFactoryConfig _factoryConfig;
    public static final Class<?> a = Object.class;
    public static final Class<?> f0 = String.class;
    public static final Class<?> g0 = CharSequence.class;
    public static final Class<?> h0 = Iterable.class;
    public static final Class<?> i0 = Map.Entry.class;
    public static final PropertyName UNWRAPPED_CREATOR_PARAM_NAME = new PropertyName("@JsonUnwrapped");

    static {
        HashMap<String, Class<? extends Map>> hashMap = new HashMap<>();
        _mapFallbacks = hashMap;
        hashMap.put(Map.class.getName(), LinkedHashMap.class);
        hashMap.put(ConcurrentMap.class.getName(), ConcurrentHashMap.class);
        hashMap.put(SortedMap.class.getName(), TreeMap.class);
        hashMap.put(NavigableMap.class.getName(), TreeMap.class);
        hashMap.put(ConcurrentNavigableMap.class.getName(), ConcurrentSkipListMap.class);
        HashMap<String, Class<? extends Collection>> hashMap2 = new HashMap<>();
        _collectionFallbacks = hashMap2;
        hashMap2.put(Collection.class.getName(), ArrayList.class);
        hashMap2.put(List.class.getName(), ArrayList.class);
        hashMap2.put(Set.class.getName(), HashSet.class);
        hashMap2.put(SortedSet.class.getName(), TreeSet.class);
        hashMap2.put(Queue.class.getName(), LinkedList.class);
        hashMap2.put("java.util.Deque", LinkedList.class);
        hashMap2.put("java.util.NavigableSet", TreeSet.class);
    }

    public BasicDeserializerFactory(DeserializerFactoryConfig deserializerFactoryConfig) {
        this._factoryConfig = deserializerFactoryConfig;
    }

    public void _addDeserializerConstructors(DeserializationContext deserializationContext, so soVar, VisibilityChecker<?> visibilityChecker, AnnotationIntrospector annotationIntrospector, CreatorCollector creatorCollector, Map<AnnotatedWithParams, BeanPropertyDefinition[]> map) throws JsonMappingException {
        Iterator<AnnotatedConstructor> it;
        int i;
        SettableBeanProperty[] settableBeanPropertyArr;
        int i2;
        Iterator<AnnotatedConstructor> it2;
        AnnotatedParameter annotatedParameter;
        AnnotatedWithParams e = soVar.e();
        if (e != null && (!creatorCollector.l() || annotationIntrospector.hasCreatorAnnotation(e))) {
            creatorCollector.o(e);
        }
        if (soVar.B()) {
            return;
        }
        Iterator<AnnotatedConstructor> it3 = soVar.u().iterator();
        List<AnnotatedConstructor> list = null;
        while (it3.hasNext()) {
            AnnotatedConstructor next = it3.next();
            boolean hasCreatorAnnotation = annotationIntrospector.hasCreatorAnnotation(next);
            vo[] voVarArr = map.get(next);
            int parameterCount = next.getParameterCount();
            if (parameterCount == 1) {
                vo voVar = voVarArr == null ? null : voVarArr[0];
                if (_checkIfCreatorPropertyBased(annotationIntrospector, next, voVar)) {
                    SettableBeanProperty[] settableBeanPropertyArr2 = new SettableBeanProperty[1];
                    PropertyName o = voVar == null ? null : voVar.o();
                    AnnotatedParameter parameter = next.getParameter(0);
                    settableBeanPropertyArr2[0] = constructCreatorProperty(deserializationContext, soVar, o, 0, parameter, annotationIntrospector.findInjectableValueId(parameter));
                    creatorCollector.i(next, hasCreatorAnnotation, settableBeanPropertyArr2);
                } else {
                    vo voVar2 = voVar;
                    _handleSingleArgumentConstructor(deserializationContext, soVar, visibilityChecker, annotationIntrospector, creatorCollector, next, hasCreatorAnnotation, visibilityChecker.isCreatorVisible(next));
                    if (voVar2 != null) {
                        ((no2) voVar2).t0();
                    }
                }
                it = it3;
            } else {
                int i3 = 0;
                SettableBeanProperty[] settableBeanPropertyArr3 = new SettableBeanProperty[parameterCount];
                int i4 = 0;
                int i5 = 0;
                int i6 = 0;
                AnnotatedParameter annotatedParameter2 = null;
                while (i4 < parameterCount) {
                    AnnotatedParameter parameter2 = next.getParameter(i4);
                    vo voVar3 = voVarArr == null ? null : voVarArr[i4];
                    Object findInjectableValueId = annotationIntrospector.findInjectableValueId(parameter2);
                    PropertyName o2 = voVar3 == null ? null : voVar3.o();
                    if (voVar3 == null || !voVar3.H()) {
                        i = i4;
                        settableBeanPropertyArr = settableBeanPropertyArr3;
                        i2 = parameterCount;
                        it2 = it3;
                        annotatedParameter = annotatedParameter2;
                        if (findInjectableValueId != null) {
                            i6++;
                            settableBeanPropertyArr[i] = constructCreatorProperty(deserializationContext, soVar, o2, i, parameter2, findInjectableValueId);
                        } else if (annotationIntrospector.findUnwrappingNameTransformer(parameter2) != null) {
                            settableBeanPropertyArr[i] = constructCreatorProperty(deserializationContext, soVar, UNWRAPPED_CREATOR_PARAM_NAME, i, parameter2, null);
                            i3++;
                        } else if (hasCreatorAnnotation && o2 != null && !o2.isEmpty()) {
                            i5++;
                            settableBeanPropertyArr[i] = constructCreatorProperty(deserializationContext, soVar, o2, i, parameter2, findInjectableValueId);
                        } else if (annotatedParameter == null) {
                            annotatedParameter2 = parameter2;
                            i4 = i + 1;
                            settableBeanPropertyArr3 = settableBeanPropertyArr;
                            parameterCount = i2;
                            it3 = it2;
                        }
                    } else {
                        i3++;
                        it2 = it3;
                        annotatedParameter = annotatedParameter2;
                        i = i4;
                        settableBeanPropertyArr = settableBeanPropertyArr3;
                        i2 = parameterCount;
                        settableBeanPropertyArr[i] = constructCreatorProperty(deserializationContext, soVar, o2, i4, parameter2, findInjectableValueId);
                    }
                    annotatedParameter2 = annotatedParameter;
                    i4 = i + 1;
                    settableBeanPropertyArr3 = settableBeanPropertyArr;
                    parameterCount = i2;
                    it3 = it2;
                }
                SettableBeanProperty[] settableBeanPropertyArr4 = settableBeanPropertyArr3;
                int i7 = parameterCount;
                it = it3;
                AnnotatedParameter annotatedParameter3 = annotatedParameter2;
                int i8 = i3 + i5;
                if (hasCreatorAnnotation || i3 > 0 || i6 > 0) {
                    if (i8 + i6 == i7) {
                        creatorCollector.i(next, hasCreatorAnnotation, settableBeanPropertyArr4);
                    } else if (i3 == 0 && i6 + 1 == i7) {
                        creatorCollector.e(next, hasCreatorAnnotation, settableBeanPropertyArr4);
                    } else {
                        PropertyName _findImplicitParamName = _findImplicitParamName(annotatedParameter3, annotationIntrospector);
                        if (_findImplicitParamName == null || _findImplicitParamName.isEmpty()) {
                            throw new IllegalArgumentException("Argument #" + annotatedParameter3.getIndex() + " of constructor " + next + " has no property name annotation; must have name when multiple-parameter constructor annotated as Creator");
                        }
                    }
                }
                if (!creatorCollector.l()) {
                    if (list == null) {
                        list = new LinkedList<>();
                    }
                    list.add(next);
                }
            }
            it3 = it;
        }
        if (list == null || creatorCollector.m() || creatorCollector.n()) {
            return;
        }
        _checkImplicitlyNamedConstructors(deserializationContext, soVar, visibilityChecker, annotationIntrospector, creatorCollector, list);
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0070  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x010e A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:77:0x0109 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void _addDeserializerFactoryMethods(com.fasterxml.jackson.databind.DeserializationContext r24, defpackage.so r25, com.fasterxml.jackson.databind.introspect.VisibilityChecker<?> r26, com.fasterxml.jackson.databind.AnnotationIntrospector r27, com.fasterxml.jackson.databind.deser.impl.CreatorCollector r28, java.util.Map<com.fasterxml.jackson.databind.introspect.AnnotatedWithParams, com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition[]> r29) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            Method dump skipped, instructions count: 322
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.BasicDeserializerFactory._addDeserializerFactoryMethods(com.fasterxml.jackson.databind.DeserializationContext, so, com.fasterxml.jackson.databind.introspect.VisibilityChecker, com.fasterxml.jackson.databind.AnnotationIntrospector, com.fasterxml.jackson.databind.deser.impl.CreatorCollector, java.util.Map):void");
    }

    public boolean _checkIfCreatorPropertyBased(AnnotationIntrospector annotationIntrospector, AnnotatedWithParams annotatedWithParams, vo voVar) {
        String t;
        JsonCreator.Mode findCreatorBinding = annotationIntrospector.findCreatorBinding(annotatedWithParams);
        if (findCreatorBinding == JsonCreator.Mode.PROPERTIES) {
            return true;
        }
        if (findCreatorBinding == JsonCreator.Mode.DELEGATING) {
            return false;
        }
        if ((voVar == null || !voVar.H()) && annotationIntrospector.findInjectableValueId(annotatedWithParams.getParameter(0)) == null) {
            return (voVar == null || (t = voVar.t()) == null || t.isEmpty() || !voVar.d()) ? false : true;
        }
        return true;
    }

    public void _checkImplicitlyNamedConstructors(DeserializationContext deserializationContext, so soVar, VisibilityChecker<?> visibilityChecker, AnnotationIntrospector annotationIntrospector, CreatorCollector creatorCollector, List<AnnotatedConstructor> list) throws JsonMappingException {
        int i;
        Iterator<AnnotatedConstructor> it = list.iterator();
        AnnotatedConstructor annotatedConstructor = null;
        AnnotatedConstructor annotatedConstructor2 = null;
        SettableBeanProperty[] settableBeanPropertyArr = null;
        while (true) {
            if (!it.hasNext()) {
                annotatedConstructor = annotatedConstructor2;
                break;
            }
            AnnotatedConstructor next = it.next();
            if (visibilityChecker.isCreatorVisible(next)) {
                int parameterCount = next.getParameterCount();
                SettableBeanProperty[] settableBeanPropertyArr2 = new SettableBeanProperty[parameterCount];
                int i2 = 0;
                while (true) {
                    if (i2 < parameterCount) {
                        AnnotatedParameter parameter = next.getParameter(i2);
                        PropertyName _findParamName = _findParamName(parameter, annotationIntrospector);
                        if (_findParamName != null && !_findParamName.isEmpty()) {
                            settableBeanPropertyArr2[i2] = constructCreatorProperty(deserializationContext, soVar, _findParamName, parameter.getIndex(), parameter, null);
                            i2++;
                        }
                    } else if (annotatedConstructor2 != null) {
                        break;
                    } else {
                        annotatedConstructor2 = next;
                        settableBeanPropertyArr = settableBeanPropertyArr2;
                    }
                }
            }
        }
        if (annotatedConstructor != null) {
            creatorCollector.i(annotatedConstructor, false, settableBeanPropertyArr);
            com.fasterxml.jackson.databind.introspect.b bVar = (com.fasterxml.jackson.databind.introspect.b) soVar;
            for (SettableBeanProperty settableBeanProperty : settableBeanPropertyArr) {
                PropertyName fullName = settableBeanProperty.getFullName();
                if (!bVar.J(fullName)) {
                    bVar.E(wo3.J(deserializationContext.getConfig(), settableBeanProperty.getMember(), fullName));
                }
            }
        }
    }

    public i _constructDefaultValueInstantiator(DeserializationContext deserializationContext, so soVar) throws JsonMappingException {
        CreatorCollector creatorCollector = new CreatorCollector(soVar, deserializationContext.getConfig());
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        DeserializationConfig config = deserializationContext.getConfig();
        VisibilityChecker<?> findAutoDetectVisibility = annotationIntrospector.findAutoDetectVisibility(soVar.t(), config.getDefaultVisibilityChecker());
        Map<AnnotatedWithParams, BeanPropertyDefinition[]> _findCreatorsFromProperties = _findCreatorsFromProperties(deserializationContext, soVar);
        _addDeserializerFactoryMethods(deserializationContext, soVar, findAutoDetectVisibility, annotationIntrospector, creatorCollector, _findCreatorsFromProperties);
        if (soVar.y().isConcrete()) {
            _addDeserializerConstructors(deserializationContext, soVar, findAutoDetectVisibility, annotationIntrospector, creatorCollector, _findCreatorsFromProperties);
        }
        return creatorCollector.k(config);
    }

    public Map<AnnotatedWithParams, BeanPropertyDefinition[]> _findCreatorsFromProperties(DeserializationContext deserializationContext, so soVar) throws JsonMappingException {
        Map<AnnotatedWithParams, BeanPropertyDefinition[]> emptyMap = Collections.emptyMap();
        for (vo voVar : soVar.n()) {
            Iterator<AnnotatedParameter> k = voVar.k();
            while (k.hasNext()) {
                AnnotatedParameter next = k.next();
                AnnotatedWithParams owner = next.getOwner();
                vo[] voVarArr = emptyMap.get(owner);
                int index = next.getIndex();
                if (voVarArr == null) {
                    if (emptyMap.isEmpty()) {
                        emptyMap = new LinkedHashMap<>();
                    }
                    voVarArr = new vo[owner.getParameterCount()];
                    emptyMap.put(owner, voVarArr);
                } else if (voVarArr[index] != null) {
                    throw new IllegalStateException("Conflict: parameter #" + index + " of " + owner + " bound to more than one property; " + voVarArr[index] + " vs " + voVar);
                }
                voVarArr[index] = voVar;
            }
        }
        return emptyMap;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomArrayDeserializer(ArrayType arrayType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findArrayDeserializer = fVar.findArrayDeserializer(arrayType, deserializationConfig, soVar, aVar, cVar);
            if (findArrayDeserializer != null) {
                return findArrayDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<Object> _findCustomBeanDeserializer(JavaType javaType, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findBeanDeserializer = fVar.findBeanDeserializer(javaType, deserializationConfig, soVar);
            if (findBeanDeserializer != null) {
                return findBeanDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomCollectionDeserializer(CollectionType collectionType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findCollectionDeserializer = fVar.findCollectionDeserializer(collectionType, deserializationConfig, soVar, aVar, cVar);
            if (findCollectionDeserializer != null) {
                return findCollectionDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomCollectionLikeDeserializer(CollectionLikeType collectionLikeType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findCollectionLikeDeserializer = fVar.findCollectionLikeDeserializer(collectionLikeType, deserializationConfig, soVar, aVar, cVar);
            if (findCollectionLikeDeserializer != null) {
                return findCollectionLikeDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomEnumDeserializer(Class<?> cls, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findEnumDeserializer = fVar.findEnumDeserializer(cls, deserializationConfig, soVar);
            if (findEnumDeserializer != null) {
                return findEnumDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomMapDeserializer(MapType mapType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.g gVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findMapDeserializer = fVar.findMapDeserializer(mapType, deserializationConfig, soVar, gVar, aVar, cVar);
            if (findMapDeserializer != null) {
                return findMapDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomMapLikeDeserializer(MapLikeType mapLikeType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.g gVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findMapLikeDeserializer = fVar.findMapLikeDeserializer(mapLikeType, deserializationConfig, soVar, gVar, aVar, cVar);
            if (findMapLikeDeserializer != null) {
                return findMapLikeDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomReferenceDeserializer(ReferenceType referenceType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findReferenceDeserializer = fVar.findReferenceDeserializer(referenceType, deserializationConfig, soVar, aVar, cVar);
            if (findReferenceDeserializer != null) {
                return findReferenceDeserializer;
            }
        }
        return null;
    }

    public com.fasterxml.jackson.databind.c<?> _findCustomTreeNodeDeserializer(Class<? extends com.fasterxml.jackson.databind.d> cls, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        for (f fVar : this._factoryConfig.deserializers()) {
            com.fasterxml.jackson.databind.c<?> findTreeNodeDeserializer = fVar.findTreeNodeDeserializer(cls, deserializationConfig, soVar);
            if (findTreeNodeDeserializer != null) {
                return findTreeNodeDeserializer;
            }
        }
        return null;
    }

    @Deprecated
    public PropertyName _findExplicitParamName(AnnotatedParameter annotatedParameter, AnnotationIntrospector annotationIntrospector) {
        if (annotatedParameter == null || annotationIntrospector == null) {
            return null;
        }
        return annotationIntrospector.findNameForDeserialization(annotatedParameter);
    }

    public PropertyName _findImplicitParamName(AnnotatedParameter annotatedParameter, AnnotationIntrospector annotationIntrospector) {
        String findImplicitPropertyName = annotationIntrospector.findImplicitPropertyName(annotatedParameter);
        if (findImplicitPropertyName == null || findImplicitPropertyName.isEmpty()) {
            return null;
        }
        return PropertyName.construct(findImplicitPropertyName);
    }

    @Deprecated
    public AnnotatedMethod _findJsonValueFor(DeserializationConfig deserializationConfig, JavaType javaType) {
        if (javaType == null) {
            return null;
        }
        return deserializationConfig.introspect(javaType).j();
    }

    public PropertyName _findParamName(AnnotatedParameter annotatedParameter, AnnotationIntrospector annotationIntrospector) {
        if (annotatedParameter == null || annotationIntrospector == null) {
            return null;
        }
        PropertyName findNameForDeserialization = annotationIntrospector.findNameForDeserialization(annotatedParameter);
        if (findNameForDeserialization != null) {
            return findNameForDeserialization;
        }
        String findImplicitPropertyName = annotationIntrospector.findImplicitPropertyName(annotatedParameter);
        if (findImplicitPropertyName == null || findImplicitPropertyName.isEmpty()) {
            return null;
        }
        return PropertyName.construct(findImplicitPropertyName);
    }

    public JavaType _findRemappedType(DeserializationConfig deserializationConfig, Class<?> cls) throws JsonMappingException {
        JavaType mapAbstractType = mapAbstractType(deserializationConfig, deserializationConfig.constructType(cls));
        if (mapAbstractType == null || mapAbstractType.hasRawClass(cls)) {
            return null;
        }
        return mapAbstractType;
    }

    public boolean _handleSingleArgumentConstructor(DeserializationContext deserializationContext, so soVar, VisibilityChecker<?> visibilityChecker, AnnotationIntrospector annotationIntrospector, CreatorCollector creatorCollector, AnnotatedConstructor annotatedConstructor, boolean z, boolean z2) throws JsonMappingException {
        Class<?> rawParameterType = annotatedConstructor.getRawParameterType(0);
        if (rawParameterType == String.class || rawParameterType == CharSequence.class) {
            if (z || z2) {
                creatorCollector.j(annotatedConstructor, z);
            }
            return true;
        } else if (rawParameterType == Integer.TYPE || rawParameterType == Integer.class) {
            if (z || z2) {
                creatorCollector.g(annotatedConstructor, z);
            }
            return true;
        } else if (rawParameterType == Long.TYPE || rawParameterType == Long.class) {
            if (z || z2) {
                creatorCollector.h(annotatedConstructor, z);
            }
            return true;
        } else if (rawParameterType == Double.TYPE || rawParameterType == Double.class) {
            if (z || z2) {
                creatorCollector.f(annotatedConstructor, z);
            }
            return true;
        } else if (rawParameterType == Boolean.TYPE || rawParameterType == Boolean.class) {
            if (z || z2) {
                creatorCollector.d(annotatedConstructor, z);
            }
            return true;
        } else if (z) {
            creatorCollector.e(annotatedConstructor, z, null);
            return true;
        } else {
            return false;
        }
    }

    public boolean _handleSingleArgumentFactory(DeserializationConfig deserializationConfig, so soVar, VisibilityChecker<?> visibilityChecker, AnnotationIntrospector annotationIntrospector, CreatorCollector creatorCollector, AnnotatedMethod annotatedMethod, boolean z) throws JsonMappingException {
        Class<?> rawParameterType = annotatedMethod.getRawParameterType(0);
        if (rawParameterType != String.class && rawParameterType != CharSequence.class) {
            if (rawParameterType != Integer.TYPE && rawParameterType != Integer.class) {
                if (rawParameterType != Long.TYPE && rawParameterType != Long.class) {
                    if (rawParameterType != Double.TYPE && rawParameterType != Double.class) {
                        if (rawParameterType == Boolean.TYPE || rawParameterType == Boolean.class) {
                            if (z || visibilityChecker.isCreatorVisible(annotatedMethod)) {
                                creatorCollector.d(annotatedMethod, z);
                            }
                            return true;
                        } else if (z) {
                            creatorCollector.e(annotatedMethod, z, null);
                            return true;
                        } else {
                            return false;
                        }
                    }
                    if (z || visibilityChecker.isCreatorVisible(annotatedMethod)) {
                        creatorCollector.f(annotatedMethod, z);
                    }
                    return true;
                }
                if (z || visibilityChecker.isCreatorVisible(annotatedMethod)) {
                    creatorCollector.h(annotatedMethod, z);
                }
                return true;
            }
            if (z || visibilityChecker.isCreatorVisible(annotatedMethod)) {
                creatorCollector.g(annotatedMethod, z);
            }
            return true;
        }
        if (z || visibilityChecker.isCreatorVisible(annotatedMethod)) {
            creatorCollector.j(annotatedMethod, z);
        }
        return true;
    }

    @Deprecated
    public boolean _hasExplicitParamName(AnnotatedParameter annotatedParameter, AnnotationIntrospector annotationIntrospector) {
        PropertyName findNameForDeserialization;
        return (annotatedParameter == null || annotationIntrospector == null || (findNameForDeserialization = annotationIntrospector.findNameForDeserialization(annotatedParameter)) == null || !findNameForDeserialization.hasSimpleName()) ? false : true;
    }

    public CollectionType _mapAbstractCollectionType(JavaType javaType, DeserializationConfig deserializationConfig) {
        Class<? extends Collection> cls = _collectionFallbacks.get(javaType.getRawClass().getName());
        if (cls == null) {
            return null;
        }
        return (CollectionType) deserializationConfig.constructSpecializedType(javaType, cls);
    }

    public i _valueInstantiatorInstance(DeserializationConfig deserializationConfig, ue ueVar, Object obj) throws JsonMappingException {
        if (obj == null) {
            return null;
        }
        if (obj instanceof i) {
            return (i) obj;
        }
        if (obj instanceof Class) {
            Class cls = (Class) obj;
            if (com.fasterxml.jackson.databind.util.c.G(cls)) {
                return null;
            }
            if (i.class.isAssignableFrom(cls)) {
                deserializationConfig.getHandlerInstantiator();
                return (i) com.fasterxml.jackson.databind.util.c.i(cls, deserializationConfig.canOverrideAccessModifiers());
            }
            throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<ValueInstantiator>");
        }
        throw new IllegalStateException("AnnotationIntrospector returned key deserializer definition of type " + obj.getClass().getName() + "; expected type KeyDeserializer or Class<KeyDeserializer> instead");
    }

    public final com.fasterxml.jackson.databind.g a(DeserializationContext deserializationContext, JavaType javaType) throws JsonMappingException {
        DeserializationConfig config = deserializationContext.getConfig();
        Class<?> rawClass = javaType.getRawClass();
        so introspect = config.introspect(javaType);
        com.fasterxml.jackson.databind.g findKeyDeserializerFromAnnotation = findKeyDeserializerFromAnnotation(deserializationContext, introspect.t());
        if (findKeyDeserializerFromAnnotation != null) {
            return findKeyDeserializerFromAnnotation;
        }
        com.fasterxml.jackson.databind.c<?> _findCustomEnumDeserializer = _findCustomEnumDeserializer(rawClass, config, introspect);
        if (_findCustomEnumDeserializer != null) {
            return StdKeyDeserializers.constructDelegatingKeyDeserializer(config, javaType, _findCustomEnumDeserializer);
        }
        com.fasterxml.jackson.databind.c<Object> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationContext, introspect.t());
        if (findDeserializerFromAnnotation != null) {
            return StdKeyDeserializers.constructDelegatingKeyDeserializer(config, javaType, findDeserializerFromAnnotation);
        }
        EnumResolver constructEnumResolver = constructEnumResolver(rawClass, config, introspect.j());
        AnnotationIntrospector annotationIntrospector = config.getAnnotationIntrospector();
        for (AnnotatedMethod annotatedMethod : introspect.v()) {
            if (annotationIntrospector.hasCreatorAnnotation(annotatedMethod)) {
                if (annotatedMethod.getParameterCount() == 1 && annotatedMethod.getRawReturnType().isAssignableFrom(rawClass)) {
                    if (annotatedMethod.getRawParameterType(0) == String.class) {
                        if (config.canOverrideAccessModifiers()) {
                            com.fasterxml.jackson.databind.util.c.f(annotatedMethod.getMember(), deserializationContext.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
                        }
                        return StdKeyDeserializers.constructEnumKeyDeserializer(constructEnumResolver, annotatedMethod);
                    }
                    throw new IllegalArgumentException("Parameter #0 type for factory method (" + annotatedMethod + ") not suitable, must be java.lang.String");
                }
                throw new IllegalArgumentException("Unsuitable method (" + annotatedMethod + ") decorated with @JsonCreator (for Enum type " + rawClass.getName() + ")");
            }
        }
        return StdKeyDeserializers.constructEnumKeyDeserializer(constructEnumResolver);
    }

    public final i b(DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        if (soVar.r() == JsonLocation.class) {
            return new dv1();
        }
        return null;
    }

    public final JavaType c(DeserializationConfig deserializationConfig, JavaType javaType) throws JsonMappingException {
        Class<?> rawClass = javaType.getRawClass();
        if (this._factoryConfig.hasAbstractTypeResolvers()) {
            for (o5 o5Var : this._factoryConfig.abstractTypeResolvers()) {
                JavaType findTypeMapping = o5Var.findTypeMapping(deserializationConfig, javaType);
                if (findTypeMapping != null && findTypeMapping.getRawClass() != rawClass) {
                    return findTypeMapping;
                }
            }
            return null;
        }
        return null;
    }

    public SettableBeanProperty constructCreatorProperty(DeserializationContext deserializationContext, so soVar, PropertyName propertyName, int i, AnnotatedParameter annotatedParameter, Object obj) throws JsonMappingException {
        PropertyMetadata construct;
        DeserializationConfig config = deserializationContext.getConfig();
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        if (annotationIntrospector == null) {
            construct = PropertyMetadata.STD_REQUIRED_OR_OPTIONAL;
        } else {
            construct = PropertyMetadata.construct(annotationIntrospector.hasRequiredMarker(annotatedParameter), annotationIntrospector.findPropertyDescription(annotatedParameter), annotationIntrospector.findPropertyIndex(annotatedParameter), annotationIntrospector.findPropertyDefaultValue(annotatedParameter));
        }
        PropertyMetadata propertyMetadata = construct;
        JavaType resolveMemberAndTypeAnnotations = resolveMemberAndTypeAnnotations(deserializationContext, annotatedParameter, annotatedParameter.getType());
        a.C0091a c0091a = new a.C0091a(propertyName, resolveMemberAndTypeAnnotations, annotationIntrospector.findWrapperName(annotatedParameter), soVar.s(), annotatedParameter, propertyMetadata);
        com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) resolveMemberAndTypeAnnotations.getTypeHandler();
        if (aVar == null) {
            aVar = findTypeDeserializer(config, resolveMemberAndTypeAnnotations);
        }
        CreatorProperty creatorProperty = new CreatorProperty(propertyName, resolveMemberAndTypeAnnotations, c0091a.getWrapperName(), aVar, soVar.s(), annotatedParameter, i, obj, propertyMetadata);
        com.fasterxml.jackson.databind.c<?> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationContext, annotatedParameter);
        if (findDeserializerFromAnnotation == null) {
            findDeserializerFromAnnotation = (com.fasterxml.jackson.databind.c) resolveMemberAndTypeAnnotations.getValueHandler();
        }
        return findDeserializerFromAnnotation != null ? creatorProperty.withValueDeserializer(deserializationContext.handlePrimaryContextualization(findDeserializerFromAnnotation, creatorProperty, resolveMemberAndTypeAnnotations)) : creatorProperty;
    }

    public EnumResolver constructEnumResolver(Class<?> cls, DeserializationConfig deserializationConfig, AnnotatedMethod annotatedMethod) {
        if (annotatedMethod != null) {
            Method annotated = annotatedMethod.getAnnotated();
            if (deserializationConfig.canOverrideAccessModifiers()) {
                com.fasterxml.jackson.databind.util.c.f(annotated, deserializationConfig.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
            }
            return EnumResolver.constructUnsafeUsingMethod(cls, annotated, deserializationConfig.getAnnotationIntrospector());
        }
        return EnumResolver.constructUnsafe(cls, deserializationConfig.getAnnotationIntrospector());
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<?> createArrayDeserializer(DeserializationContext deserializationContext, ArrayType arrayType, so soVar) throws JsonMappingException {
        DeserializationConfig config = deserializationContext.getConfig();
        JavaType contentType = arrayType.getContentType();
        com.fasterxml.jackson.databind.c<?> cVar = (com.fasterxml.jackson.databind.c) contentType.getValueHandler();
        com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) contentType.getTypeHandler();
        if (aVar == null) {
            aVar = findTypeDeserializer(config, contentType);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = aVar;
        com.fasterxml.jackson.databind.c<?> _findCustomArrayDeserializer = _findCustomArrayDeserializer(arrayType, config, soVar, aVar2, cVar);
        if (_findCustomArrayDeserializer == null) {
            if (cVar == null) {
                Class<?> rawClass = contentType.getRawClass();
                if (contentType.isPrimitive()) {
                    return PrimitiveArrayDeserializers.forType(rawClass);
                }
                if (rawClass == String.class) {
                    return StringArrayDeserializer.instance;
                }
            }
            _findCustomArrayDeserializer = new ObjectArrayDeserializer(arrayType, cVar, aVar2);
        }
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                _findCustomArrayDeserializer = uoVar.modifyArrayDeserializer(config, arrayType, soVar, _findCustomArrayDeserializer);
            }
        }
        return _findCustomArrayDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<?> createCollectionDeserializer(DeserializationContext deserializationContext, CollectionType collectionType, so soVar) throws JsonMappingException {
        JavaType contentType = collectionType.getContentType();
        com.fasterxml.jackson.databind.c<?> cVar = (com.fasterxml.jackson.databind.c) contentType.getValueHandler();
        DeserializationConfig config = deserializationContext.getConfig();
        com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) contentType.getTypeHandler();
        if (aVar == null) {
            aVar = findTypeDeserializer(config, contentType);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = aVar;
        com.fasterxml.jackson.databind.c<?> _findCustomCollectionDeserializer = _findCustomCollectionDeserializer(collectionType, config, soVar, aVar2, cVar);
        if (_findCustomCollectionDeserializer == null) {
            Class<?> rawClass = collectionType.getRawClass();
            if (cVar == null && EnumSet.class.isAssignableFrom(rawClass)) {
                _findCustomCollectionDeserializer = new EnumSetDeserializer(contentType, null);
            }
        }
        if (_findCustomCollectionDeserializer == null) {
            if (collectionType.isInterface() || collectionType.isAbstract()) {
                CollectionType _mapAbstractCollectionType = _mapAbstractCollectionType(collectionType, config);
                if (_mapAbstractCollectionType == null) {
                    if (collectionType.getTypeHandler() != null) {
                        _findCustomCollectionDeserializer = AbstractDeserializer.constructForNonPOJO(soVar);
                    } else {
                        throw new IllegalArgumentException("Can not find a deserializer for non-concrete Collection type " + collectionType);
                    }
                } else {
                    soVar = config.introspectForCreation(_mapAbstractCollectionType);
                    collectionType = _mapAbstractCollectionType;
                }
            }
            if (_findCustomCollectionDeserializer == null) {
                i findValueInstantiator = findValueInstantiator(deserializationContext, soVar);
                if (!findValueInstantiator.canCreateUsingDefault() && collectionType.getRawClass() == ArrayBlockingQueue.class) {
                    return new ArrayBlockingQueueDeserializer(collectionType, cVar, aVar2, findValueInstantiator);
                }
                if (contentType.getRawClass() == String.class) {
                    _findCustomCollectionDeserializer = new StringCollectionDeserializer(collectionType, cVar, findValueInstantiator);
                } else {
                    _findCustomCollectionDeserializer = new CollectionDeserializer(collectionType, cVar, aVar2, findValueInstantiator);
                }
            }
        }
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                _findCustomCollectionDeserializer = uoVar.modifyCollectionDeserializer(config, collectionType, soVar, _findCustomCollectionDeserializer);
            }
        }
        return _findCustomCollectionDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<?> createCollectionLikeDeserializer(DeserializationContext deserializationContext, CollectionLikeType collectionLikeType, so soVar) throws JsonMappingException {
        JavaType contentType = collectionLikeType.getContentType();
        com.fasterxml.jackson.databind.c<?> cVar = (com.fasterxml.jackson.databind.c) contentType.getValueHandler();
        DeserializationConfig config = deserializationContext.getConfig();
        com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) contentType.getTypeHandler();
        com.fasterxml.jackson.databind.c<?> _findCustomCollectionLikeDeserializer = _findCustomCollectionLikeDeserializer(collectionLikeType, config, soVar, aVar == null ? findTypeDeserializer(config, contentType) : aVar, cVar);
        if (_findCustomCollectionLikeDeserializer != null && this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                _findCustomCollectionLikeDeserializer = uoVar.modifyCollectionLikeDeserializer(config, collectionLikeType, soVar, _findCustomCollectionLikeDeserializer);
            }
        }
        return _findCustomCollectionLikeDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<?> createEnumDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        DeserializationConfig config = deserializationContext.getConfig();
        Class<?> rawClass = javaType.getRawClass();
        com.fasterxml.jackson.databind.c<?> _findCustomEnumDeserializer = _findCustomEnumDeserializer(rawClass, config, soVar);
        if (_findCustomEnumDeserializer == null) {
            i _constructDefaultValueInstantiator = _constructDefaultValueInstantiator(deserializationContext, soVar);
            SettableBeanProperty[] fromObjectArguments = _constructDefaultValueInstantiator == null ? null : _constructDefaultValueInstantiator.getFromObjectArguments(deserializationContext.getConfig());
            Iterator<AnnotatedMethod> it = soVar.v().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                AnnotatedMethod next = it.next();
                if (deserializationContext.getAnnotationIntrospector().hasCreatorAnnotation(next)) {
                    if (next.getParameterCount() == 0) {
                        _findCustomEnumDeserializer = EnumDeserializer.deserializerForNoArgsCreator(config, rawClass, next);
                        break;
                    } else if (next.getRawReturnType().isAssignableFrom(rawClass)) {
                        _findCustomEnumDeserializer = EnumDeserializer.deserializerForCreator(config, rawClass, next, _constructDefaultValueInstantiator, fromObjectArguments);
                        break;
                    }
                }
            }
            if (_findCustomEnumDeserializer == null) {
                _findCustomEnumDeserializer = new EnumDeserializer(constructEnumResolver(rawClass, config, soVar.j()));
            }
        }
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                _findCustomEnumDeserializer = uoVar.modifyEnumDeserializer(config, javaType, soVar, _findCustomEnumDeserializer);
            }
        }
        return _findCustomEnumDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.g createKeyDeserializer(DeserializationContext deserializationContext, JavaType javaType) throws JsonMappingException {
        DeserializationConfig config = deserializationContext.getConfig();
        com.fasterxml.jackson.databind.g gVar = null;
        if (this._factoryConfig.hasKeyDeserializers()) {
            so introspectClassAnnotations = config.introspectClassAnnotations(javaType.getRawClass());
            Iterator<g> it = this._factoryConfig.keyDeserializers().iterator();
            while (it.hasNext() && (gVar = it.next().findKeyDeserializer(javaType, config, introspectClassAnnotations)) == null) {
            }
        }
        if (gVar == null) {
            if (javaType.isEnumType()) {
                gVar = a(deserializationContext, javaType);
            } else {
                gVar = StdKeyDeserializers.findStringBasedKeyDeserializer(config, javaType);
            }
        }
        if (gVar != null && this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                gVar = uoVar.modifyKeyDeserializer(config, javaType, gVar);
            }
        }
        return gVar;
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x009d  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00e5  */
    @Override // com.fasterxml.jackson.databind.deser.e
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.c<?> createMapDeserializer(com.fasterxml.jackson.databind.DeserializationContext r18, com.fasterxml.jackson.databind.type.MapType r19, defpackage.so r20) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            Method dump skipped, instructions count: 257
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.BasicDeserializerFactory.createMapDeserializer(com.fasterxml.jackson.databind.DeserializationContext, com.fasterxml.jackson.databind.type.MapType, so):com.fasterxml.jackson.databind.c");
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<?> createMapLikeDeserializer(DeserializationContext deserializationContext, MapLikeType mapLikeType, so soVar) throws JsonMappingException {
        JavaType keyType = mapLikeType.getKeyType();
        JavaType contentType = mapLikeType.getContentType();
        DeserializationConfig config = deserializationContext.getConfig();
        com.fasterxml.jackson.databind.c<?> cVar = (com.fasterxml.jackson.databind.c) contentType.getValueHandler();
        com.fasterxml.jackson.databind.g gVar = (com.fasterxml.jackson.databind.g) keyType.getValueHandler();
        com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) contentType.getTypeHandler();
        if (aVar == null) {
            aVar = findTypeDeserializer(config, contentType);
        }
        com.fasterxml.jackson.databind.c<?> _findCustomMapLikeDeserializer = _findCustomMapLikeDeserializer(mapLikeType, config, soVar, gVar, aVar, cVar);
        if (_findCustomMapLikeDeserializer != null && this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                _findCustomMapLikeDeserializer = uoVar.modifyMapLikeDeserializer(config, mapLikeType, soVar, _findCustomMapLikeDeserializer);
            }
        }
        return _findCustomMapLikeDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<?> createReferenceDeserializer(DeserializationContext deserializationContext, ReferenceType referenceType, so soVar) throws JsonMappingException {
        JavaType contentType = referenceType.getContentType();
        com.fasterxml.jackson.databind.c<?> cVar = (com.fasterxml.jackson.databind.c) contentType.getValueHandler();
        DeserializationConfig config = deserializationContext.getConfig();
        com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) contentType.getTypeHandler();
        if (aVar == null) {
            aVar = findTypeDeserializer(config, contentType);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = aVar;
        com.fasterxml.jackson.databind.c<?> _findCustomReferenceDeserializer = _findCustomReferenceDeserializer(referenceType, config, soVar, aVar2, cVar);
        if (_findCustomReferenceDeserializer == null && AtomicReference.class.isAssignableFrom(referenceType.getRawClass())) {
            return new AtomicReferenceDeserializer(referenceType, aVar2, cVar);
        }
        if (_findCustomReferenceDeserializer != null && this._factoryConfig.hasDeserializerModifiers()) {
            for (uo uoVar : this._factoryConfig.deserializerModifiers()) {
                _findCustomReferenceDeserializer = uoVar.modifyReferenceDeserializer(config, referenceType, soVar, _findCustomReferenceDeserializer);
            }
        }
        return _findCustomReferenceDeserializer;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.c<?> createTreeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, so soVar) throws JsonMappingException {
        Class<?> rawClass = javaType.getRawClass();
        com.fasterxml.jackson.databind.c<?> _findCustomTreeNodeDeserializer = _findCustomTreeNodeDeserializer(rawClass, deserializationConfig, soVar);
        return _findCustomTreeNodeDeserializer != null ? _findCustomTreeNodeDeserializer : JsonNodeDeserializer.getDeserializer(rawClass);
    }

    public com.fasterxml.jackson.databind.c<?> findDefaultDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        JavaType javaType2;
        JavaType javaType3;
        Class<?> rawClass = javaType.getRawClass();
        if (rawClass == a) {
            DeserializationConfig config = deserializationContext.getConfig();
            if (this._factoryConfig.hasAbstractTypeResolvers()) {
                javaType2 = _findRemappedType(config, List.class);
                javaType3 = _findRemappedType(config, Map.class);
            } else {
                javaType2 = null;
                javaType3 = null;
            }
            return new UntypedObjectDeserializer(javaType2, javaType3);
        } else if (rawClass != f0 && rawClass != g0) {
            Class<?> cls = h0;
            if (rawClass == cls) {
                TypeFactory typeFactory = deserializationContext.getTypeFactory();
                JavaType[] findTypeParameters = typeFactory.findTypeParameters(javaType, cls);
                return createCollectionDeserializer(deserializationContext, typeFactory.constructCollectionType(Collection.class, (findTypeParameters == null || findTypeParameters.length != 1) ? TypeFactory.unknownType() : findTypeParameters[0]), soVar);
            } else if (rawClass == i0) {
                JavaType containedType = javaType.containedType(0);
                if (containedType == null) {
                    containedType = TypeFactory.unknownType();
                }
                JavaType containedType2 = javaType.containedType(1);
                if (containedType2 == null) {
                    containedType2 = TypeFactory.unknownType();
                }
                com.fasterxml.jackson.databind.jsontype.a aVar = (com.fasterxml.jackson.databind.jsontype.a) containedType2.getTypeHandler();
                if (aVar == null) {
                    aVar = findTypeDeserializer(deserializationContext.getConfig(), containedType2);
                }
                return new MapEntryDeserializer(javaType, (com.fasterxml.jackson.databind.g) containedType.getValueHandler(), (com.fasterxml.jackson.databind.c) containedType2.getValueHandler(), aVar);
            } else {
                String name = rawClass.getName();
                if (rawClass.isPrimitive() || name.startsWith("java.")) {
                    com.fasterxml.jackson.databind.c<?> a2 = NumberDeserializers.a(rawClass, name);
                    if (a2 == null) {
                        a2 = DateDeserializers.a(rawClass, name);
                    }
                    if (a2 != null) {
                        return a2;
                    }
                }
                if (rawClass == com.fasterxml.jackson.databind.util.e.class) {
                    return new TokenBufferDeserializer();
                }
                com.fasterxml.jackson.databind.c<?> findOptionalStdDeserializer = findOptionalStdDeserializer(deserializationContext, javaType, soVar);
                return findOptionalStdDeserializer != null ? findOptionalStdDeserializer : com.fasterxml.jackson.databind.deser.std.a.a(rawClass, name);
            }
        } else {
            return StringDeserializer.instance;
        }
    }

    public com.fasterxml.jackson.databind.c<Object> findDeserializerFromAnnotation(DeserializationContext deserializationContext, ue ueVar) throws JsonMappingException {
        Object findDeserializer;
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        if (annotationIntrospector == null || (findDeserializer = annotationIntrospector.findDeserializer(ueVar)) == null) {
            return null;
        }
        return deserializationContext.deserializerInstance(ueVar, findDeserializer);
    }

    public com.fasterxml.jackson.databind.g findKeyDeserializerFromAnnotation(DeserializationContext deserializationContext, ue ueVar) throws JsonMappingException {
        Object findKeyDeserializer;
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        if (annotationIntrospector == null || (findKeyDeserializer = annotationIntrospector.findKeyDeserializer(ueVar)) == null) {
            return null;
        }
        return deserializationContext.keyDeserializerInstance(ueVar, findKeyDeserializer);
    }

    public com.fasterxml.jackson.databind.c<?> findOptionalStdDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException {
        return OptionalHandlerFactory.instance.findDeserializer(javaType, deserializationContext.getConfig(), soVar);
    }

    public com.fasterxml.jackson.databind.jsontype.a findPropertyContentTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, AnnotatedMember annotatedMember) throws JsonMappingException {
        vd4<?> findPropertyContentTypeResolver = deserializationConfig.getAnnotationIntrospector().findPropertyContentTypeResolver(deserializationConfig, annotatedMember, javaType);
        JavaType contentType = javaType.getContentType();
        if (findPropertyContentTypeResolver == null) {
            return findTypeDeserializer(deserializationConfig, contentType);
        }
        return findPropertyContentTypeResolver.buildTypeDeserializer(deserializationConfig, contentType, deserializationConfig.getSubtypeResolver().collectAndResolveSubtypesByTypeId(deserializationConfig, annotatedMember, contentType));
    }

    public com.fasterxml.jackson.databind.jsontype.a findPropertyTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, AnnotatedMember annotatedMember) throws JsonMappingException {
        vd4<?> findPropertyTypeResolver = deserializationConfig.getAnnotationIntrospector().findPropertyTypeResolver(deserializationConfig, annotatedMember, javaType);
        if (findPropertyTypeResolver == null) {
            return findTypeDeserializer(deserializationConfig, javaType);
        }
        return findPropertyTypeResolver.buildTypeDeserializer(deserializationConfig, javaType, deserializationConfig.getSubtypeResolver().collectAndResolveSubtypesByTypeId(deserializationConfig, annotatedMember, javaType));
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fasterxml.jackson.databind.deser.e
    public com.fasterxml.jackson.databind.jsontype.a findTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.introspect.a t = deserializationConfig.introspectClassAnnotations(javaType.getRawClass()).t();
        vd4<?> findTypeResolver = deserializationConfig.getAnnotationIntrospector().findTypeResolver(deserializationConfig, t, javaType);
        Collection<NamedType> collection = null;
        if (findTypeResolver == null) {
            findTypeResolver = deserializationConfig.getDefaultTyper(javaType);
            if (findTypeResolver == null) {
                return null;
            }
        } else {
            collection = deserializationConfig.getSubtypeResolver().collectAndResolveSubtypesByTypeId(deserializationConfig, t);
        }
        Class<?> defaultImpl = findTypeResolver.getDefaultImpl();
        vd4<?> vd4Var = findTypeResolver;
        if (defaultImpl == null) {
            vd4Var = findTypeResolver;
            if (javaType.isAbstract()) {
                JavaType mapAbstractType = mapAbstractType(deserializationConfig, javaType);
                vd4Var = findTypeResolver;
                if (mapAbstractType != null) {
                    vd4Var = findTypeResolver;
                    if (mapAbstractType.getRawClass() != javaType.getRawClass()) {
                        vd4Var = findTypeResolver.defaultImpl(mapAbstractType.getRawClass());
                    }
                }
            }
        }
        return vd4Var.buildTypeDeserializer(deserializationConfig, javaType, collection);
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public i findValueInstantiator(DeserializationContext deserializationContext, so soVar) throws JsonMappingException {
        DeserializationConfig config = deserializationContext.getConfig();
        com.fasterxml.jackson.databind.introspect.a t = soVar.t();
        Object findValueInstantiator = deserializationContext.getAnnotationIntrospector().findValueInstantiator(t);
        i _valueInstantiatorInstance = findValueInstantiator != null ? _valueInstantiatorInstance(config, t, findValueInstantiator) : null;
        if (_valueInstantiatorInstance == null && (_valueInstantiatorInstance = b(config, soVar)) == null) {
            _valueInstantiatorInstance = _constructDefaultValueInstantiator(deserializationContext, soVar);
        }
        if (this._factoryConfig.hasValueInstantiators()) {
            for (ah4 ah4Var : this._factoryConfig.valueInstantiators()) {
                _valueInstantiatorInstance = ah4Var.findValueInstantiator(config, soVar, _valueInstantiatorInstance);
                if (_valueInstantiatorInstance == null) {
                    deserializationContext.reportMappingException("Broken registered ValueInstantiators (of type %s): returned null ValueInstantiator", ah4Var.getClass().getName());
                }
            }
        }
        if (_valueInstantiatorInstance.getIncompleteParameter() == null) {
            return _valueInstantiatorInstance;
        }
        AnnotatedParameter incompleteParameter = _valueInstantiatorInstance.getIncompleteParameter();
        AnnotatedWithParams owner = incompleteParameter.getOwner();
        throw new IllegalArgumentException("Argument #" + incompleteParameter.getIndex() + " of constructor " + owner + " has no property name annotation; must have name when multiple-parameter constructor annotated as Creator");
    }

    public DeserializerFactoryConfig getFactoryConfig() {
        return this._factoryConfig;
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public JavaType mapAbstractType(DeserializationConfig deserializationConfig, JavaType javaType) throws JsonMappingException {
        JavaType c;
        while (true) {
            c = c(deserializationConfig, javaType);
            if (c == null) {
                return javaType;
            }
            Class<?> rawClass = javaType.getRawClass();
            Class<?> rawClass2 = c.getRawClass();
            if (rawClass == rawClass2 || !rawClass.isAssignableFrom(rawClass2)) {
                break;
            }
            javaType = c;
        }
        throw new IllegalArgumentException("Invalid abstract type resolution from " + javaType + " to " + c + ": latter is not a subtype of former");
    }

    @Deprecated
    public JavaType modifyTypeByAnnotation(DeserializationContext deserializationContext, ue ueVar, JavaType javaType) throws JsonMappingException {
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        return annotationIntrospector == null ? javaType : annotationIntrospector.refineDeserializationType(deserializationContext.getConfig(), ueVar, javaType);
    }

    public JavaType resolveMemberAndTypeAnnotations(DeserializationContext deserializationContext, AnnotatedMember annotatedMember, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.g keyDeserializerInstance;
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        if (annotationIntrospector == null) {
            return javaType;
        }
        if (javaType.isMapLikeType() && javaType.getKeyType() != null && (keyDeserializerInstance = deserializationContext.keyDeserializerInstance(annotatedMember, annotationIntrospector.findKeyDeserializer(annotatedMember))) != null) {
            javaType = ((MapLikeType) javaType).withKeyValueHandler(keyDeserializerInstance);
            javaType.getKeyType();
        }
        if (javaType.hasContentType()) {
            com.fasterxml.jackson.databind.c<Object> deserializerInstance = deserializationContext.deserializerInstance(annotatedMember, annotationIntrospector.findContentDeserializer(annotatedMember));
            if (deserializerInstance != null) {
                javaType = javaType.withContentValueHandler(deserializerInstance);
            }
            com.fasterxml.jackson.databind.jsontype.a findPropertyContentTypeDeserializer = findPropertyContentTypeDeserializer(deserializationContext.getConfig(), javaType, annotatedMember);
            if (findPropertyContentTypeDeserializer != null) {
                javaType = javaType.withContentTypeHandler(findPropertyContentTypeDeserializer);
            }
        }
        com.fasterxml.jackson.databind.jsontype.a findPropertyTypeDeserializer = findPropertyTypeDeserializer(deserializationContext.getConfig(), javaType, annotatedMember);
        if (findPropertyTypeDeserializer != null) {
            javaType = javaType.withTypeHandler(findPropertyTypeDeserializer);
        }
        return annotationIntrospector.refineDeserializationType(deserializationContext.getConfig(), annotatedMember, javaType);
    }

    @Deprecated
    public JavaType resolveType(DeserializationContext deserializationContext, so soVar, JavaType javaType, AnnotatedMember annotatedMember) throws JsonMappingException {
        return resolveMemberAndTypeAnnotations(deserializationContext, annotatedMember, javaType);
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public final e withAbstractTypeResolver(o5 o5Var) {
        return withConfig(this._factoryConfig.withAbstractTypeResolver(o5Var));
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public final e withAdditionalDeserializers(f fVar) {
        return withConfig(this._factoryConfig.withAdditionalDeserializers(fVar));
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public final e withAdditionalKeyDeserializers(g gVar) {
        return withConfig(this._factoryConfig.withAdditionalKeyDeserializers(gVar));
    }

    public abstract e withConfig(DeserializerFactoryConfig deserializerFactoryConfig);

    @Override // com.fasterxml.jackson.databind.deser.e
    public final e withDeserializerModifier(uo uoVar) {
        return withConfig(this._factoryConfig.withDeserializerModifier(uoVar));
    }

    @Override // com.fasterxml.jackson.databind.deser.e
    public final e withValueInstantiators(ah4 ah4Var) {
        return withConfig(this._factoryConfig.withValueInstantiators(ah4Var));
    }
}
