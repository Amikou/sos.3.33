package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.ReferenceType;

/* compiled from: Deserializers.java */
/* loaded from: classes.dex */
public interface f {
    com.fasterxml.jackson.databind.c<?> findArrayDeserializer(ArrayType arrayType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findBeanDeserializer(JavaType javaType, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findCollectionDeserializer(CollectionType collectionType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findCollectionLikeDeserializer(CollectionLikeType collectionLikeType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findEnumDeserializer(Class<?> cls, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findMapDeserializer(MapType mapType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.g gVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findMapLikeDeserializer(MapLikeType mapLikeType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.g gVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findReferenceDeserializer(ReferenceType referenceType, DeserializationConfig deserializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) throws JsonMappingException;

    com.fasterxml.jackson.databind.c<?> findTreeNodeDeserializer(Class<? extends com.fasterxml.jackson.databind.d> cls, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException;
}
