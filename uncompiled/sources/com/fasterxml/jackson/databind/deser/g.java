package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;

/* compiled from: KeyDeserializers.java */
/* loaded from: classes.dex */
public interface g {
    com.fasterxml.jackson.databind.g findKeyDeserializer(JavaType javaType, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException;
}
