package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;

/* compiled from: ResolvableDeserializer.java */
/* loaded from: classes.dex */
public interface h {
    void resolve(DeserializationContext deserializationContext) throws JsonMappingException;
}
