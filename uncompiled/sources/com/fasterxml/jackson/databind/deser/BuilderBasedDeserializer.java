package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.impl.BeanAsArrayBuilderDeserializer;
import com.fasterxml.jackson.databind.deser.impl.BeanPropertyMap;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdReader;
import com.fasterxml.jackson.databind.deser.impl.PropertyBasedCreator;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/* loaded from: classes.dex */
public class BuilderBasedDeserializer extends BeanDeserializerBase {
    private static final long serialVersionUID = 1;
    public final AnnotatedMethod _buildMethod;

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonToken.values().length];
            a = iArr;
            try {
                iArr[JsonToken.VALUE_STRING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_INT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[JsonToken.VALUE_TRUE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[JsonToken.VALUE_FALSE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[JsonToken.START_ARRAY.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[JsonToken.FIELD_NAME.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[JsonToken.END_OBJECT.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
        }
    }

    public BuilderBasedDeserializer(to toVar, so soVar, BeanPropertyMap beanPropertyMap, Map<String, SettableBeanProperty> map, Set<String> set, boolean z, boolean z2) {
        super(toVar, soVar, beanPropertyMap, map, set, z, z2);
        this._buildMethod = toVar.m();
        if (this._objectIdReader == null) {
            return;
        }
        throw new IllegalArgumentException("Can not use Object Id with Builder-based deserialization (type " + soVar.y() + ")");
    }

    public final Object _deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException, JsonProcessingException {
        Class<?> activeView;
        if (this._injectables != null) {
            injectValues(deserializationContext, obj);
        }
        if (this._unwrappedPropertyHandler != null) {
            return deserializeWithUnwrapped(jsonParser, deserializationContext, obj);
        }
        if (this._externalTypeIdHandler != null) {
            return deserializeWithExternalTypeId(jsonParser, deserializationContext, obj);
        }
        if (this._needViewProcesing && (activeView = deserializationContext.getActiveView()) != null) {
            return deserializeWithView(jsonParser, deserializationContext, obj, activeView);
        }
        JsonToken u = jsonParser.u();
        if (u == JsonToken.START_OBJECT) {
            u = jsonParser.T0();
        }
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                try {
                    obj = find.deserializeSetAndReturn(jsonParser, deserializationContext, obj);
                } catch (Exception e) {
                    wrapAndThrow(e, obj, r, deserializationContext);
                }
            } else {
                handleUnknownVanilla(jsonParser, deserializationContext, handledType(), r);
            }
            u = jsonParser.T0();
        }
        return obj;
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public final Object _deserializeUsingPropertyBased(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Object wrapInstantiationProblem;
        PropertyBasedCreator propertyBasedCreator = this._propertyBasedCreator;
        com.fasterxml.jackson.databind.deser.impl.d f = propertyBasedCreator.f(jsonParser, deserializationContext, this._objectIdReader);
        JsonToken u = jsonParser.u();
        com.fasterxml.jackson.databind.util.e eVar = null;
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty d = propertyBasedCreator.d(r);
            if (d != null) {
                if (f.b(d, d.deserialize(jsonParser, deserializationContext))) {
                    jsonParser.T0();
                    try {
                        Object a2 = propertyBasedCreator.a(deserializationContext, f);
                        if (a2.getClass() != this._beanType.getRawClass()) {
                            return handlePolymorphic(jsonParser, deserializationContext, a2, eVar);
                        }
                        if (eVar != null) {
                            a2 = handleUnknownProperties(deserializationContext, a2, eVar);
                        }
                        return _deserialize(jsonParser, deserializationContext, a2);
                    } catch (Exception e) {
                        wrapAndThrow(e, this._beanType.getRawClass(), r, deserializationContext);
                    }
                } else {
                    continue;
                }
            } else if (!f.i(r)) {
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    f.e(find, find.deserialize(jsonParser, deserializationContext));
                } else {
                    Set<String> set = this._ignorableProps;
                    if (set != null && set.contains(r)) {
                        handleIgnoredProperty(jsonParser, deserializationContext, handledType(), r);
                    } else {
                        SettableAnyProperty settableAnyProperty = this._anySetter;
                        if (settableAnyProperty != null) {
                            f.c(settableAnyProperty, r, settableAnyProperty.deserialize(jsonParser, deserializationContext));
                        } else {
                            if (eVar == null) {
                                eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
                            }
                            eVar.i0(r);
                            eVar.O1(jsonParser);
                        }
                    }
                }
            }
            u = jsonParser.T0();
        }
        try {
            wrapInstantiationProblem = propertyBasedCreator.a(deserializationContext, f);
        } catch (Exception e2) {
            wrapInstantiationProblem = wrapInstantiationProblem(e2, deserializationContext);
        }
        if (eVar != null) {
            if (wrapInstantiationProblem.getClass() != this._beanType.getRawClass()) {
                return handlePolymorphic(null, deserializationContext, wrapInstantiationProblem, eVar);
            }
            return handleUnknownProperties(deserializationContext, wrapInstantiationProblem, eVar);
        }
        return wrapInstantiationProblem;
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializerBase asArrayDeserializer() {
        return new BeanAsArrayBuilderDeserializer(this, this._beanProperties.getPropertiesInInsertionOrder(), this._buildMethod);
    }

    public final Object d(JsonParser jsonParser, DeserializationContext deserializationContext, JsonToken jsonToken) throws IOException, JsonProcessingException {
        Object createUsingDefault = this._valueInstantiator.createUsingDefault(deserializationContext);
        while (jsonParser.u() != JsonToken.END_OBJECT) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                try {
                    createUsingDefault = find.deserializeSetAndReturn(jsonParser, deserializationContext, createUsingDefault);
                } catch (Exception e) {
                    wrapAndThrow(e, createUsingDefault, r, deserializationContext);
                }
            } else {
                handleUnknownVanilla(jsonParser, deserializationContext, createUsingDefault, r);
            }
            jsonParser.T0();
        }
        return createUsingDefault;
    }

    @Override // com.fasterxml.jackson.databind.c
    public final Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.START_OBJECT) {
            JsonToken T0 = jsonParser.T0();
            if (this._vanillaProcessing) {
                return finishBuild(deserializationContext, d(jsonParser, deserializationContext, T0));
            }
            return finishBuild(deserializationContext, deserializeFromObject(jsonParser, deserializationContext));
        }
        if (u != null) {
            switch (a.a[u.ordinal()]) {
                case 1:
                    return finishBuild(deserializationContext, deserializeFromString(jsonParser, deserializationContext));
                case 2:
                    return finishBuild(deserializationContext, deserializeFromNumber(jsonParser, deserializationContext));
                case 3:
                    return finishBuild(deserializationContext, deserializeFromDouble(jsonParser, deserializationContext));
                case 4:
                    return jsonParser.z();
                case 5:
                case 6:
                    return finishBuild(deserializationContext, deserializeFromBoolean(jsonParser, deserializationContext));
                case 7:
                    return finishBuild(deserializationContext, deserializeFromArray(jsonParser, deserializationContext));
                case 8:
                case 9:
                    return finishBuild(deserializationContext, deserializeFromObject(jsonParser, deserializationContext));
            }
        }
        return deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public Object deserializeFromObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Class<?> activeView;
        if (this._nonStandardCreation) {
            if (this._unwrappedPropertyHandler != null) {
                return deserializeWithUnwrapped(jsonParser, deserializationContext);
            }
            if (this._externalTypeIdHandler != null) {
                return deserializeWithExternalTypeId(jsonParser, deserializationContext);
            }
            return deserializeFromObjectUsingNonDefault(jsonParser, deserializationContext);
        }
        Object createUsingDefault = this._valueInstantiator.createUsingDefault(deserializationContext);
        if (this._injectables != null) {
            injectValues(deserializationContext, createUsingDefault);
        }
        if (this._needViewProcesing && (activeView = deserializationContext.getActiveView()) != null) {
            return deserializeWithView(jsonParser, deserializationContext, createUsingDefault, activeView);
        }
        while (jsonParser.u() != JsonToken.END_OBJECT) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                try {
                    createUsingDefault = find.deserializeSetAndReturn(jsonParser, deserializationContext, createUsingDefault);
                } catch (Exception e) {
                    wrapAndThrow(e, createUsingDefault, r, deserializationContext);
                }
            } else {
                handleUnknownVanilla(jsonParser, deserializationContext, createUsingDefault, r);
            }
            jsonParser.T0();
        }
        return createUsingDefault;
    }

    public Object deserializeUsingPropertyBasedWithExternalTypeId(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        throw new IllegalStateException("Deserialization with Builder, External type id, @JsonCreator not yet implemented");
    }

    public Object deserializeUsingPropertyBasedWithUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        PropertyBasedCreator propertyBasedCreator = this._propertyBasedCreator;
        com.fasterxml.jackson.databind.deser.impl.d f = propertyBasedCreator.f(jsonParser, deserializationContext, this._objectIdReader);
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.i1();
        JsonToken u = jsonParser.u();
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty d = propertyBasedCreator.d(r);
            if (d != null) {
                f.b(d, d.deserialize(jsonParser, deserializationContext));
            } else if (!f.i(r)) {
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    f.e(find, find.deserialize(jsonParser, deserializationContext));
                } else {
                    Set<String> set = this._ignorableProps;
                    if (set != null && set.contains(r)) {
                        handleIgnoredProperty(jsonParser, deserializationContext, handledType(), r);
                    } else {
                        eVar.i0(r);
                        eVar.O1(jsonParser);
                        SettableAnyProperty settableAnyProperty = this._anySetter;
                        if (settableAnyProperty != null) {
                            f.c(settableAnyProperty, r, settableAnyProperty.deserialize(jsonParser, deserializationContext));
                        }
                    }
                }
            }
            u = jsonParser.T0();
        }
        try {
            return this._unwrappedPropertyHandler.b(jsonParser, deserializationContext, propertyBasedCreator.a(deserializationContext, f), eVar);
        } catch (Exception e) {
            return wrapInstantiationProblem(e, deserializationContext);
        }
    }

    public Object deserializeWithExternalTypeId(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        if (this._propertyBasedCreator != null) {
            return deserializeUsingPropertyBasedWithExternalTypeId(jsonParser, deserializationContext);
        }
        return deserializeWithExternalTypeId(jsonParser, deserializationContext, this._valueInstantiator.createUsingDefault(deserializationContext));
    }

    public Object deserializeWithUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        com.fasterxml.jackson.databind.c<Object> cVar = this._delegateDeserializer;
        if (cVar != null) {
            return this._valueInstantiator.createUsingDelegate(deserializationContext, cVar.deserialize(jsonParser, deserializationContext));
        }
        if (this._propertyBasedCreator != null) {
            return deserializeUsingPropertyBasedWithUnwrapped(jsonParser, deserializationContext);
        }
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.i1();
        Object createUsingDefault = this._valueInstantiator.createUsingDefault(deserializationContext);
        if (this._injectables != null) {
            injectValues(deserializationContext, createUsingDefault);
        }
        Class<?> activeView = this._needViewProcesing ? deserializationContext.getActiveView() : null;
        while (jsonParser.u() != JsonToken.END_OBJECT) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                if (activeView != null && !find.visibleInView(activeView)) {
                    jsonParser.k1();
                } else {
                    try {
                        createUsingDefault = find.deserializeSetAndReturn(jsonParser, deserializationContext, createUsingDefault);
                    } catch (Exception e) {
                        wrapAndThrow(e, createUsingDefault, r, deserializationContext);
                    }
                }
            } else {
                Set<String> set = this._ignorableProps;
                if (set != null && set.contains(r)) {
                    handleIgnoredProperty(jsonParser, deserializationContext, createUsingDefault, r);
                } else {
                    eVar.i0(r);
                    eVar.O1(jsonParser);
                    SettableAnyProperty settableAnyProperty = this._anySetter;
                    if (settableAnyProperty != null) {
                        try {
                            settableAnyProperty.deserializeAndSet(jsonParser, deserializationContext, createUsingDefault, r);
                        } catch (Exception e2) {
                            wrapAndThrow(e2, createUsingDefault, r, deserializationContext);
                        }
                    }
                }
            }
            jsonParser.T0();
        }
        eVar.f0();
        this._unwrappedPropertyHandler.b(jsonParser, deserializationContext, createUsingDefault, eVar);
        return createUsingDefault;
    }

    public final Object deserializeWithView(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj, Class<?> cls) throws IOException, JsonProcessingException {
        JsonToken u = jsonParser.u();
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                if (!find.visibleInView(cls)) {
                    jsonParser.k1();
                } else {
                    try {
                        obj = find.deserializeSetAndReturn(jsonParser, deserializationContext, obj);
                    } catch (Exception e) {
                        wrapAndThrow(e, obj, r, deserializationContext);
                    }
                }
            } else {
                handleUnknownVanilla(jsonParser, deserializationContext, obj, r);
            }
            u = jsonParser.T0();
        }
        return obj;
    }

    public final Object finishBuild(DeserializationContext deserializationContext, Object obj) throws IOException {
        AnnotatedMethod annotatedMethod = this._buildMethod;
        if (annotatedMethod == null) {
            return obj;
        }
        try {
            return annotatedMethod.getMember().invoke(obj, new Object[0]);
        } catch (Exception e) {
            return wrapInstantiationProblem(e, deserializationContext);
        }
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase, com.fasterxml.jackson.databind.c
    public com.fasterxml.jackson.databind.c<Object> unwrappingDeserializer(NameTransformer nameTransformer) {
        return new BuilderBasedDeserializer(this, nameTransformer);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializerBase withBeanProperties(BeanPropertyMap beanPropertyMap) {
        return new BuilderBasedDeserializer(this, beanPropertyMap);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializerBase withIgnorableProperties(Set<String> set) {
        return new BuilderBasedDeserializer(this, set);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializerBase withObjectIdReader(ObjectIdReader objectIdReader) {
        return new BuilderBasedDeserializer(this, objectIdReader);
    }

    public Object deserializeWithExternalTypeId(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException, JsonProcessingException {
        Class<?> activeView = this._needViewProcesing ? deserializationContext.getActiveView() : null;
        com.fasterxml.jackson.databind.deser.impl.b g = this._externalTypeIdHandler.g();
        JsonToken u = jsonParser.u();
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            JsonToken T0 = jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                if (T0.isScalarValue()) {
                    g.f(jsonParser, deserializationContext, r, obj);
                }
                if (activeView != null && !find.visibleInView(activeView)) {
                    jsonParser.k1();
                } else {
                    try {
                        obj = find.deserializeSetAndReturn(jsonParser, deserializationContext, obj);
                    } catch (Exception e) {
                        wrapAndThrow(e, obj, r, deserializationContext);
                    }
                }
            } else {
                Set<String> set = this._ignorableProps;
                if (set != null && set.contains(r)) {
                    handleIgnoredProperty(jsonParser, deserializationContext, obj, r);
                } else if (!g.e(jsonParser, deserializationContext, r, obj)) {
                    SettableAnyProperty settableAnyProperty = this._anySetter;
                    if (settableAnyProperty != null) {
                        try {
                            settableAnyProperty.deserializeAndSet(jsonParser, deserializationContext, obj, r);
                        } catch (Exception e2) {
                            wrapAndThrow(e2, obj, r, deserializationContext);
                        }
                    } else {
                        handleUnknownProperty(jsonParser, deserializationContext, obj, r);
                    }
                }
            }
            u = jsonParser.T0();
        }
        return g.d(jsonParser, deserializationContext, obj);
    }

    public BuilderBasedDeserializer(BuilderBasedDeserializer builderBasedDeserializer) {
        this(builderBasedDeserializer, builderBasedDeserializer._ignoreAllUnknown);
    }

    public BuilderBasedDeserializer(BuilderBasedDeserializer builderBasedDeserializer, boolean z) {
        super(builderBasedDeserializer, z);
        this._buildMethod = builderBasedDeserializer._buildMethod;
    }

    public BuilderBasedDeserializer(BuilderBasedDeserializer builderBasedDeserializer, NameTransformer nameTransformer) {
        super(builderBasedDeserializer, nameTransformer);
        this._buildMethod = builderBasedDeserializer._buildMethod;
    }

    public BuilderBasedDeserializer(BuilderBasedDeserializer builderBasedDeserializer, ObjectIdReader objectIdReader) {
        super(builderBasedDeserializer, objectIdReader);
        this._buildMethod = builderBasedDeserializer._buildMethod;
    }

    public BuilderBasedDeserializer(BuilderBasedDeserializer builderBasedDeserializer, Set<String> set) {
        super(builderBasedDeserializer, set);
        this._buildMethod = builderBasedDeserializer._buildMethod;
    }

    public BuilderBasedDeserializer(BuilderBasedDeserializer builderBasedDeserializer, BeanPropertyMap beanPropertyMap) {
        super(builderBasedDeserializer, beanPropertyMap);
        this._buildMethod = builderBasedDeserializer._buildMethod;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        return finishBuild(deserializationContext, _deserialize(jsonParser, deserializationContext, obj));
    }

    public Object deserializeWithUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException, JsonProcessingException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.START_OBJECT) {
            u = jsonParser.T0();
        }
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.i1();
        Class<?> activeView = this._needViewProcesing ? deserializationContext.getActiveView() : null;
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            SettableBeanProperty find = this._beanProperties.find(r);
            jsonParser.T0();
            if (find != null) {
                if (activeView != null && !find.visibleInView(activeView)) {
                    jsonParser.k1();
                } else {
                    try {
                        obj = find.deserializeSetAndReturn(jsonParser, deserializationContext, obj);
                    } catch (Exception e) {
                        wrapAndThrow(e, obj, r, deserializationContext);
                    }
                }
            } else {
                Set<String> set = this._ignorableProps;
                if (set != null && set.contains(r)) {
                    handleIgnoredProperty(jsonParser, deserializationContext, obj, r);
                } else {
                    eVar.i0(r);
                    eVar.O1(jsonParser);
                    SettableAnyProperty settableAnyProperty = this._anySetter;
                    if (settableAnyProperty != null) {
                        settableAnyProperty.deserializeAndSet(jsonParser, deserializationContext, obj, r);
                    }
                }
            }
            u = jsonParser.T0();
        }
        eVar.f0();
        this._unwrappedPropertyHandler.b(jsonParser, deserializationContext, obj, eVar);
        return obj;
    }
}
