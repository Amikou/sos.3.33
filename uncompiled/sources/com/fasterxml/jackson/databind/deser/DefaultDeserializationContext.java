package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.t;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.impl.e;
import com.fasterxml.jackson.databind.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public abstract class DefaultDeserializationContext extends DeserializationContext {
    private static final long serialVersionUID = 1;
    private List<t> _objectIdResolvers;
    public transient LinkedHashMap<ObjectIdGenerator.IdKey, com.fasterxml.jackson.databind.deser.impl.e> _objectIds;

    /* loaded from: classes.dex */
    public static final class Impl extends DefaultDeserializationContext {
        private static final long serialVersionUID = 1;

        public Impl(e eVar) {
            super(eVar, (DeserializerCache) null);
        }

        @Override // com.fasterxml.jackson.databind.deser.DefaultDeserializationContext
        public DefaultDeserializationContext copy() {
            if (Impl.class != Impl.class) {
                return super.copy();
            }
            return new Impl(this);
        }

        @Override // com.fasterxml.jackson.databind.deser.DefaultDeserializationContext
        public DefaultDeserializationContext createInstance(DeserializationConfig deserializationConfig, JsonParser jsonParser, sq1 sq1Var) {
            return new Impl(this, deserializationConfig, jsonParser, sq1Var);
        }

        @Override // com.fasterxml.jackson.databind.deser.DefaultDeserializationContext
        public DefaultDeserializationContext with(e eVar) {
            return new Impl(this, eVar);
        }

        public Impl(Impl impl, DeserializationConfig deserializationConfig, JsonParser jsonParser, sq1 sq1Var) {
            super(impl, deserializationConfig, jsonParser, sq1Var);
        }

        public Impl(Impl impl) {
            super(impl);
        }

        public Impl(Impl impl, e eVar) {
            super(impl, eVar);
        }
    }

    public DefaultDeserializationContext(e eVar, DeserializerCache deserializerCache) {
        super(eVar, deserializerCache);
    }

    @Override // com.fasterxml.jackson.databind.DeserializationContext
    public void checkUnresolvedObjectId() throws UnresolvedForwardReference {
        if (this._objectIds != null && isEnabled(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS)) {
            UnresolvedForwardReference unresolvedForwardReference = null;
            for (Map.Entry<ObjectIdGenerator.IdKey, com.fasterxml.jackson.databind.deser.impl.e> entry : this._objectIds.entrySet()) {
                com.fasterxml.jackson.databind.deser.impl.e value = entry.getValue();
                if (value.d() && !tryToResolveUnresolvedObjectId(value)) {
                    if (unresolvedForwardReference == null) {
                        unresolvedForwardReference = new UnresolvedForwardReference(getParser(), "Unresolved forward references for: ");
                    }
                    Object obj = value.c().key;
                    Iterator<e.a> e = value.e();
                    while (e.hasNext()) {
                        e.a next = e.next();
                        unresolvedForwardReference.addUnresolvedId(obj, next.a(), next.b());
                    }
                }
            }
            if (unresolvedForwardReference != null) {
                throw unresolvedForwardReference;
            }
        }
    }

    public DefaultDeserializationContext copy() {
        throw new IllegalStateException("DefaultDeserializationContext sub-class not overriding copy()");
    }

    public abstract DefaultDeserializationContext createInstance(DeserializationConfig deserializationConfig, JsonParser jsonParser, sq1 sq1Var);

    public com.fasterxml.jackson.databind.deser.impl.e createReadableObjectId(ObjectIdGenerator.IdKey idKey) {
        return new com.fasterxml.jackson.databind.deser.impl.e(idKey);
    }

    @Override // com.fasterxml.jackson.databind.DeserializationContext
    public com.fasterxml.jackson.databind.c<Object> deserializerInstance(ue ueVar, Object obj) throws JsonMappingException {
        com.fasterxml.jackson.databind.c<Object> cVar;
        if (obj == null) {
            return null;
        }
        if (obj instanceof com.fasterxml.jackson.databind.c) {
            cVar = (com.fasterxml.jackson.databind.c) obj;
        } else if (obj instanceof Class) {
            Class cls = (Class) obj;
            if (cls == c.a.class || com.fasterxml.jackson.databind.util.c.G(cls)) {
                return null;
            }
            if (com.fasterxml.jackson.databind.c.class.isAssignableFrom(cls)) {
                this._config.getHandlerInstantiator();
                cVar = (com.fasterxml.jackson.databind.c) com.fasterxml.jackson.databind.util.c.i(cls, this._config.canOverrideAccessModifiers());
            } else {
                throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonDeserializer>");
            }
        } else {
            throw new IllegalStateException("AnnotationIntrospector returned deserializer definition of type " + obj.getClass().getName() + "; expected type JsonDeserializer or Class<JsonDeserializer> instead");
        }
        if (cVar instanceof h) {
            ((h) cVar).resolve(this);
        }
        return cVar;
    }

    @Override // com.fasterxml.jackson.databind.DeserializationContext
    public com.fasterxml.jackson.databind.deser.impl.e findObjectId(Object obj, ObjectIdGenerator<?> objectIdGenerator, t tVar) {
        t tVar2 = null;
        if (obj == null) {
            return null;
        }
        ObjectIdGenerator.IdKey key = objectIdGenerator.key(obj);
        LinkedHashMap<ObjectIdGenerator.IdKey, com.fasterxml.jackson.databind.deser.impl.e> linkedHashMap = this._objectIds;
        if (linkedHashMap == null) {
            this._objectIds = new LinkedHashMap<>();
        } else {
            com.fasterxml.jackson.databind.deser.impl.e eVar = linkedHashMap.get(key);
            if (eVar != null) {
                return eVar;
            }
        }
        List<t> list = this._objectIdResolvers;
        if (list == null) {
            this._objectIdResolvers = new ArrayList(8);
        } else {
            Iterator<t> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                t next = it.next();
                if (next.c(tVar)) {
                    tVar2 = next;
                    break;
                }
            }
        }
        if (tVar2 == null) {
            tVar2 = tVar.b(this);
            this._objectIdResolvers.add(tVar2);
        }
        com.fasterxml.jackson.databind.deser.impl.e createReadableObjectId = createReadableObjectId(key);
        createReadableObjectId.g(tVar2);
        this._objectIds.put(key, createReadableObjectId);
        return createReadableObjectId;
    }

    @Override // com.fasterxml.jackson.databind.DeserializationContext
    public final com.fasterxml.jackson.databind.g keyDeserializerInstance(ue ueVar, Object obj) throws JsonMappingException {
        com.fasterxml.jackson.databind.g gVar;
        if (obj == null) {
            return null;
        }
        if (obj instanceof com.fasterxml.jackson.databind.g) {
            gVar = (com.fasterxml.jackson.databind.g) obj;
        } else if (obj instanceof Class) {
            Class cls = (Class) obj;
            if (cls == g.a.class || com.fasterxml.jackson.databind.util.c.G(cls)) {
                return null;
            }
            if (com.fasterxml.jackson.databind.g.class.isAssignableFrom(cls)) {
                this._config.getHandlerInstantiator();
                gVar = (com.fasterxml.jackson.databind.g) com.fasterxml.jackson.databind.util.c.i(cls, this._config.canOverrideAccessModifiers());
            } else {
                throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<KeyDeserializer>");
            }
        } else {
            throw new IllegalStateException("AnnotationIntrospector returned key deserializer definition of type " + obj.getClass().getName() + "; expected type KeyDeserializer or Class<KeyDeserializer> instead");
        }
        if (gVar instanceof h) {
            ((h) gVar).resolve(this);
        }
        return gVar;
    }

    public boolean tryToResolveUnresolvedObjectId(com.fasterxml.jackson.databind.deser.impl.e eVar) {
        return eVar.h(this);
    }

    public abstract DefaultDeserializationContext with(e eVar);

    public DefaultDeserializationContext(DefaultDeserializationContext defaultDeserializationContext, DeserializationConfig deserializationConfig, JsonParser jsonParser, sq1 sq1Var) {
        super(defaultDeserializationContext, deserializationConfig, jsonParser, sq1Var);
    }

    public DefaultDeserializationContext(DefaultDeserializationContext defaultDeserializationContext, e eVar) {
        super(defaultDeserializationContext, eVar);
    }

    public DefaultDeserializationContext(DefaultDeserializationContext defaultDeserializationContext) {
        super(defaultDeserializationContext);
    }
}
