package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import java.io.IOException;

/* compiled from: DeserializationProblemHandler.java */
/* loaded from: classes.dex */
public abstract class d {
    public static final Object a = new Object();

    public Object a(DeserializationContext deserializationContext, Class<?> cls, Object obj, Throwable th) throws IOException {
        return a;
    }

    public Object b(DeserializationContext deserializationContext, Class<?> cls, JsonParser jsonParser, String str) throws IOException {
        return a;
    }

    public Object c(DeserializationContext deserializationContext, Class<?> cls, JsonToken jsonToken, JsonParser jsonParser, String str) throws IOException {
        return a;
    }

    public boolean d(DeserializationContext deserializationContext, JsonParser jsonParser, com.fasterxml.jackson.databind.c<?> cVar, Object obj, String str) throws IOException {
        return false;
    }

    public JavaType e(DeserializationContext deserializationContext, JavaType javaType, String str, com.fasterxml.jackson.databind.jsontype.b bVar, String str2) throws IOException {
        return null;
    }

    public Object f(DeserializationContext deserializationContext, Class<?> cls, String str, String str2) throws IOException {
        return a;
    }

    public Object g(DeserializationContext deserializationContext, Class<?> cls, Number number, String str) throws IOException {
        return a;
    }

    public Object h(DeserializationContext deserializationContext, Class<?> cls, String str, String str2) throws IOException {
        return a;
    }
}
