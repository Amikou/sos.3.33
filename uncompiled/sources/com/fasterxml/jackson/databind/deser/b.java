package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;

/* compiled from: ContextualKeyDeserializer.java */
/* loaded from: classes.dex */
public interface b {
    com.fasterxml.jackson.databind.g createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException;
}
