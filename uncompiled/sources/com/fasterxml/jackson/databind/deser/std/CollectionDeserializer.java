package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.UnresolvedForwardReference;
import com.fasterxml.jackson.databind.deser.i;
import com.fasterxml.jackson.databind.deser.impl.e;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@pt1
/* loaded from: classes.dex */
public class CollectionDeserializer extends ContainerDeserializerBase<Collection<Object>> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = -1;
    public final JavaType _collectionType;
    public final c<Object> _delegateDeserializer;
    public final Boolean _unwrapSingle;
    public final c<Object> _valueDeserializer;
    public final i _valueInstantiator;
    public final com.fasterxml.jackson.databind.jsontype.a _valueTypeDeserializer;

    /* loaded from: classes.dex */
    public static final class a extends e.a {
        public final b c;
        public final List<Object> d;

        public a(b bVar, UnresolvedForwardReference unresolvedForwardReference, Class<?> cls) {
            super(unresolvedForwardReference, cls);
            this.d = new ArrayList();
            this.c = bVar;
        }

        @Override // com.fasterxml.jackson.databind.deser.impl.e.a
        public void c(Object obj, Object obj2) throws IOException {
            this.c.c(obj, obj2);
        }
    }

    /* loaded from: classes.dex */
    public static final class b {
        public final Class<?> a;
        public final Collection<Object> b;
        public List<a> c = new ArrayList();

        public b(Class<?> cls, Collection<Object> collection) {
            this.a = cls;
            this.b = collection;
        }

        public void a(Object obj) {
            if (this.c.isEmpty()) {
                this.b.add(obj);
                return;
            }
            List<a> list = this.c;
            list.get(list.size() - 1).d.add(obj);
        }

        public e.a b(UnresolvedForwardReference unresolvedForwardReference) {
            a aVar = new a(this, unresolvedForwardReference, this.a);
            this.c.add(aVar);
            return aVar;
        }

        public void c(Object obj, Object obj2) throws IOException {
            Iterator<a> it = this.c.iterator();
            Collection collection = this.b;
            while (it.hasNext()) {
                a next = it.next();
                if (next.d(obj)) {
                    it.remove();
                    collection.add(obj2);
                    collection.addAll(next.d);
                    return;
                }
                collection = next.d;
            }
            throw new IllegalArgumentException("Trying to resolve a forward reference with id [" + obj + "] that wasn't previously seen as unresolved.");
        }
    }

    public CollectionDeserializer(JavaType javaType, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar, i iVar) {
        this(javaType, cVar, aVar, iVar, null, null);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return aVar.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public c<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public JavaType getContentType() {
        return this._collectionType.getContentType();
    }

    public final Collection<Object> handleNonArray(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<Object> collection) throws IOException {
        Object deserializeWithType;
        Boolean bool = this._unwrapSingle;
        if (!(bool == Boolean.TRUE || (bool == null && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)))) {
            return (Collection) deserializationContext.handleUnexpectedToken(this._collectionType.getRawClass(), jsonParser);
        }
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        try {
            if (jsonParser.u() == JsonToken.VALUE_NULL) {
                deserializeWithType = cVar.getNullValue(deserializationContext);
            } else if (aVar == null) {
                deserializeWithType = cVar.deserialize(jsonParser, deserializationContext);
            } else {
                deserializeWithType = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
            }
            collection.add(deserializeWithType);
            return collection;
        } catch (Exception e) {
            throw JsonMappingException.wrapWithPath(e, Object.class, collection.size());
        }
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return this._valueDeserializer == null && this._valueTypeDeserializer == null && this._delegateDeserializer == null;
    }

    public CollectionDeserializer withResolved(c<?> cVar, c<?> cVar2, com.fasterxml.jackson.databind.jsontype.a aVar, Boolean bool) {
        return (cVar == this._delegateDeserializer && cVar2 == this._valueDeserializer && aVar == this._valueTypeDeserializer && this._unwrapSingle == bool) ? this : new CollectionDeserializer(this._collectionType, cVar2, aVar, this._valueInstantiator, cVar, bool);
    }

    public CollectionDeserializer(JavaType javaType, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar, i iVar, c<Object> cVar2, Boolean bool) {
        super(javaType);
        this._collectionType = javaType;
        this._valueDeserializer = cVar;
        this._valueTypeDeserializer = aVar;
        this._valueInstantiator = iVar;
        this._delegateDeserializer = cVar2;
        this._unwrapSingle = bool;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x00a9  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00ae  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00b6  */
    @Override // com.fasterxml.jackson.databind.deser.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.deser.std.CollectionDeserializer createContextual(com.fasterxml.jackson.databind.DeserializationContext r5, com.fasterxml.jackson.databind.a r6) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r4 = this;
            com.fasterxml.jackson.databind.deser.i r0 = r4._valueInstantiator
            if (r0 == 0) goto L92
            boolean r0 = r0.canCreateUsingDelegate()
            java.lang.String r1 = ": value instantiator ("
            if (r0 == 0) goto L4b
            com.fasterxml.jackson.databind.deser.i r0 = r4._valueInstantiator
            com.fasterxml.jackson.databind.DeserializationConfig r2 = r5.getConfig()
            com.fasterxml.jackson.databind.JavaType r0 = r0.getDelegateType(r2)
            if (r0 == 0) goto L1d
            com.fasterxml.jackson.databind.c r0 = r4.findDeserializer(r5, r0, r6)
            goto L93
        L1d:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r0 = "Invalid delegate-creator definition for "
            r6.append(r0)
            com.fasterxml.jackson.databind.JavaType r0 = r4._collectionType
            r6.append(r0)
            r6.append(r1)
            com.fasterxml.jackson.databind.deser.i r0 = r4._valueInstantiator
            java.lang.Class r0 = r0.getClass()
            java.lang.String r0 = r0.getName()
            r6.append(r0)
            java.lang.String r0 = ") returned true for 'canCreateUsingDelegate()', but null for 'getDelegateType()'"
            r6.append(r0)
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        L4b:
            com.fasterxml.jackson.databind.deser.i r0 = r4._valueInstantiator
            boolean r0 = r0.canCreateUsingArrayDelegate()
            if (r0 == 0) goto L92
            com.fasterxml.jackson.databind.deser.i r0 = r4._valueInstantiator
            com.fasterxml.jackson.databind.DeserializationConfig r2 = r5.getConfig()
            com.fasterxml.jackson.databind.JavaType r0 = r0.getArrayDelegateType(r2)
            if (r0 == 0) goto L64
            com.fasterxml.jackson.databind.c r0 = r4.findDeserializer(r5, r0, r6)
            goto L93
        L64:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r0 = "Invalid array-delegate-creator definition for "
            r6.append(r0)
            com.fasterxml.jackson.databind.JavaType r0 = r4._collectionType
            r6.append(r0)
            r6.append(r1)
            com.fasterxml.jackson.databind.deser.i r0 = r4._valueInstantiator
            java.lang.Class r0 = r0.getClass()
            java.lang.String r0 = r0.getName()
            r6.append(r0)
            java.lang.String r0 = ") returned true for 'canCreateUsingArrayDelegate()', but null for 'getArrayDelegateType()'"
            r6.append(r0)
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        L92:
            r0 = 0
        L93:
            java.lang.Class<java.util.Collection> r1 = java.util.Collection.class
            com.fasterxml.jackson.annotation.JsonFormat$Feature r2 = com.fasterxml.jackson.annotation.JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY
            java.lang.Boolean r1 = r4.findFormatFeature(r5, r6, r1, r2)
            com.fasterxml.jackson.databind.c<java.lang.Object> r2 = r4._valueDeserializer
            com.fasterxml.jackson.databind.c r2 = r4.findConvertingContentDeserializer(r5, r6, r2)
            com.fasterxml.jackson.databind.JavaType r3 = r4._collectionType
            com.fasterxml.jackson.databind.JavaType r3 = r3.getContentType()
            if (r2 != 0) goto Lae
            com.fasterxml.jackson.databind.c r5 = r5.findContextualValueDeserializer(r3, r6)
            goto Lb2
        Lae:
            com.fasterxml.jackson.databind.c r5 = r5.handleSecondaryContextualization(r2, r6, r3)
        Lb2:
            com.fasterxml.jackson.databind.jsontype.a r2 = r4._valueTypeDeserializer
            if (r2 == 0) goto Lba
            com.fasterxml.jackson.databind.jsontype.a r2 = r2.forProperty(r6)
        Lba:
            com.fasterxml.jackson.databind.deser.std.CollectionDeserializer r5 = r4.withResolved(r0, r5, r2, r1)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.std.CollectionDeserializer.createContextual(com.fasterxml.jackson.databind.DeserializationContext, com.fasterxml.jackson.databind.a):com.fasterxml.jackson.databind.deser.std.CollectionDeserializer");
    }

    @Override // com.fasterxml.jackson.databind.c
    public Collection<Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        c<Object> cVar = this._delegateDeserializer;
        if (cVar != null) {
            return (Collection) this._valueInstantiator.createUsingDelegate(deserializationContext, cVar.deserialize(jsonParser, deserializationContext));
        }
        if (jsonParser.F0(JsonToken.VALUE_STRING)) {
            String X = jsonParser.X();
            if (X.length() == 0) {
                return (Collection) this._valueInstantiator.createFromString(deserializationContext, X);
            }
        }
        return deserialize(jsonParser, deserializationContext, (Collection) this._valueInstantiator.createUsingDefault(deserializationContext));
    }

    @Deprecated
    public CollectionDeserializer withResolved(c<?> cVar, c<?> cVar2, com.fasterxml.jackson.databind.jsontype.a aVar) {
        return withResolved(cVar, cVar2, aVar, this._unwrapSingle);
    }

    public CollectionDeserializer(CollectionDeserializer collectionDeserializer) {
        super(collectionDeserializer._collectionType);
        this._collectionType = collectionDeserializer._collectionType;
        this._valueDeserializer = collectionDeserializer._valueDeserializer;
        this._valueTypeDeserializer = collectionDeserializer._valueTypeDeserializer;
        this._valueInstantiator = collectionDeserializer._valueInstantiator;
        this._delegateDeserializer = collectionDeserializer._delegateDeserializer;
        this._unwrapSingle = collectionDeserializer._unwrapSingle;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Collection<Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<Object> collection) throws IOException {
        Object deserializeWithType;
        if (!jsonParser.K0()) {
            return handleNonArray(jsonParser, deserializationContext, collection);
        }
        jsonParser.e1(collection);
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        b bVar = cVar.getObjectIdReader() == null ? null : new b(this._collectionType.getContentType().getRawClass(), collection);
        while (true) {
            JsonToken T0 = jsonParser.T0();
            if (T0 == JsonToken.END_ARRAY) {
                return collection;
            }
            try {
                if (T0 == JsonToken.VALUE_NULL) {
                    deserializeWithType = cVar.getNullValue(deserializationContext);
                } else if (aVar == null) {
                    deserializeWithType = cVar.deserialize(jsonParser, deserializationContext);
                } else {
                    deserializeWithType = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
                }
                if (bVar != null) {
                    bVar.a(deserializeWithType);
                } else {
                    collection.add(deserializeWithType);
                }
            } catch (UnresolvedForwardReference e) {
                if (bVar != null) {
                    e.getRoid().a(bVar.b(e));
                } else {
                    throw JsonMappingException.from(jsonParser, "Unresolved forward reference but no identity info", e);
                }
            } catch (Exception e2) {
                if (!(deserializationContext == null || deserializationContext.isEnabled(DeserializationFeature.WRAP_EXCEPTIONS)) && (e2 instanceof RuntimeException)) {
                    throw ((RuntimeException) e2);
                }
                throw JsonMappingException.wrapWithPath(e2, collection, collection.size());
            }
        }
    }
}
