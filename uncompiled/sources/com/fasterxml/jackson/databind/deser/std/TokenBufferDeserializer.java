package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.util.e;
import java.io.IOException;

@pt1
/* loaded from: classes.dex */
public class TokenBufferDeserializer extends StdScalarDeserializer<e> {
    private static final long serialVersionUID = 1;

    public TokenBufferDeserializer() {
        super(e.class);
    }

    public e createBufferInstance(JsonParser jsonParser) {
        return new e(jsonParser);
    }

    @Override // com.fasterxml.jackson.databind.c
    public e deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return createBufferInstance(jsonParser).P1(jsonParser, deserializationContext);
    }
}
