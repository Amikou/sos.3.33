package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.h;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@pt1
/* loaded from: classes.dex */
public class UntypedObjectDeserializer extends StdDeserializer<Object> implements h, com.fasterxml.jackson.databind.deser.a {
    public static final Object[] NO_OBJECTS = new Object[0];
    @Deprecated
    public static final UntypedObjectDeserializer instance = new UntypedObjectDeserializer(null, null);
    private static final long serialVersionUID = 1;
    public c<Object> _listDeserializer;
    public JavaType _listType;
    public c<Object> _mapDeserializer;
    public JavaType _mapType;
    public c<Object> _numberDeserializer;
    public c<Object> _stringDeserializer;

    @pt1
    /* loaded from: classes.dex */
    public static class Vanilla extends StdDeserializer<Object> {
        private static final long serialVersionUID = 1;
        public static final Vanilla std = new Vanilla();

        public Vanilla() {
            super(Object.class);
        }

        /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
        @Override // com.fasterxml.jackson.databind.c
        public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            switch (jsonParser.v()) {
                case 1:
                    if (jsonParser.T0() == JsonToken.END_OBJECT) {
                        return new LinkedHashMap(2);
                    }
                    break;
                case 2:
                    return new LinkedHashMap(2);
                case 3:
                    if (jsonParser.T0() == JsonToken.END_ARRAY) {
                        if (deserializationContext.isEnabled(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY)) {
                            return UntypedObjectDeserializer.NO_OBJECTS;
                        }
                        return new ArrayList(2);
                    } else if (deserializationContext.isEnabled(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY)) {
                        return mapArrayToArray(jsonParser, deserializationContext);
                    } else {
                        return mapArray(jsonParser, deserializationContext);
                    }
                case 4:
                default:
                    return deserializationContext.handleUnexpectedToken(Object.class, jsonParser);
                case 5:
                    break;
                case 6:
                    return jsonParser.X();
                case 7:
                    if (deserializationContext.hasSomeOfFeatures(StdDeserializer.F_MASK_INT_COERCIONS)) {
                        return _coerceIntegral(jsonParser, deserializationContext);
                    }
                    return jsonParser.Q();
                case 8:
                    if (deserializationContext.isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
                        return jsonParser.w();
                    }
                    return jsonParser.Q();
                case 9:
                    return Boolean.TRUE;
                case 10:
                    return Boolean.FALSE;
                case 11:
                    return null;
                case 12:
                    return jsonParser.z();
            }
            return mapObject(jsonParser, deserializationContext);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
        public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
            int v = jsonParser.v();
            if (v != 1 && v != 3) {
                switch (v) {
                    case 5:
                        break;
                    case 6:
                        return jsonParser.X();
                    case 7:
                        if (deserializationContext.isEnabled(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS)) {
                            return jsonParser.h();
                        }
                        return jsonParser.Q();
                    case 8:
                        if (deserializationContext.isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
                            return jsonParser.w();
                        }
                        return jsonParser.Q();
                    case 9:
                        return Boolean.TRUE;
                    case 10:
                        return Boolean.FALSE;
                    case 11:
                        return null;
                    case 12:
                        return jsonParser.z();
                    default:
                        return deserializationContext.handleUnexpectedToken(Object.class, jsonParser);
                }
            }
            return aVar.deserializeTypedFromAny(jsonParser, deserializationContext);
        }

        public Object mapArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            Object deserialize = deserialize(jsonParser, deserializationContext);
            JsonToken T0 = jsonParser.T0();
            JsonToken jsonToken = JsonToken.END_ARRAY;
            int i = 2;
            if (T0 == jsonToken) {
                ArrayList arrayList = new ArrayList(2);
                arrayList.add(deserialize);
                return arrayList;
            }
            Object deserialize2 = deserialize(jsonParser, deserializationContext);
            if (jsonParser.T0() == jsonToken) {
                ArrayList arrayList2 = new ArrayList(2);
                arrayList2.add(deserialize);
                arrayList2.add(deserialize2);
                return arrayList2;
            }
            gl2 leaseObjectBuffer = deserializationContext.leaseObjectBuffer();
            Object[] i2 = leaseObjectBuffer.i();
            i2[0] = deserialize;
            i2[1] = deserialize2;
            int i3 = 2;
            while (true) {
                Object deserialize3 = deserialize(jsonParser, deserializationContext);
                i++;
                if (i3 >= i2.length) {
                    i2 = leaseObjectBuffer.c(i2);
                    i3 = 0;
                }
                int i4 = i3 + 1;
                i2[i3] = deserialize3;
                if (jsonParser.T0() == JsonToken.END_ARRAY) {
                    ArrayList arrayList3 = new ArrayList(i);
                    leaseObjectBuffer.e(i2, i4, arrayList3);
                    return arrayList3;
                }
                i3 = i4;
            }
        }

        public Object[] mapArrayToArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            gl2 leaseObjectBuffer = deserializationContext.leaseObjectBuffer();
            Object[] i = leaseObjectBuffer.i();
            int i2 = 0;
            while (true) {
                Object deserialize = deserialize(jsonParser, deserializationContext);
                if (i2 >= i.length) {
                    i = leaseObjectBuffer.c(i);
                    i2 = 0;
                }
                int i3 = i2 + 1;
                i[i2] = deserialize;
                if (jsonParser.T0() == JsonToken.END_ARRAY) {
                    return leaseObjectBuffer.f(i, i3);
                }
                i2 = i3;
            }
        }

        public Object mapObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            String X = jsonParser.X();
            jsonParser.T0();
            Object deserialize = deserialize(jsonParser, deserializationContext);
            String O0 = jsonParser.O0();
            if (O0 == null) {
                LinkedHashMap linkedHashMap = new LinkedHashMap(2);
                linkedHashMap.put(X, deserialize);
                return linkedHashMap;
            }
            jsonParser.T0();
            Object deserialize2 = deserialize(jsonParser, deserializationContext);
            String O02 = jsonParser.O0();
            if (O02 == null) {
                LinkedHashMap linkedHashMap2 = new LinkedHashMap(4);
                linkedHashMap2.put(X, deserialize);
                linkedHashMap2.put(O0, deserialize2);
                return linkedHashMap2;
            }
            LinkedHashMap linkedHashMap3 = new LinkedHashMap();
            linkedHashMap3.put(X, deserialize);
            linkedHashMap3.put(O0, deserialize2);
            do {
                jsonParser.T0();
                linkedHashMap3.put(O02, deserialize(jsonParser, deserializationContext));
                O02 = jsonParser.O0();
            } while (O02 != null);
            return linkedHashMap3;
        }
    }

    @Deprecated
    public UntypedObjectDeserializer() {
        this(null, null);
    }

    public c<Object> _clearIfStdImpl(c<Object> cVar) {
        if (com.fasterxml.jackson.databind.util.c.J(cVar)) {
            return null;
        }
        return cVar;
    }

    public c<Object> _findCustomDeser(DeserializationContext deserializationContext, JavaType javaType) throws JsonMappingException {
        return deserializationContext.findNonContextualValueDeserializer(javaType);
    }

    public c<?> _withResolved(c<?> cVar, c<?> cVar2, c<?> cVar3, c<?> cVar4) {
        return new UntypedObjectDeserializer(this, cVar, cVar2, cVar3, cVar4);
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        return (this._stringDeserializer == null && this._numberDeserializer == null && this._mapDeserializer == null && this._listDeserializer == null && getClass() == UntypedObjectDeserializer.class) ? Vanilla.std : this;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        switch (jsonParser.v()) {
            case 1:
            case 2:
            case 5:
                c<Object> cVar = this._mapDeserializer;
                if (cVar != null) {
                    return cVar.deserialize(jsonParser, deserializationContext);
                }
                return mapObject(jsonParser, deserializationContext);
            case 3:
                if (deserializationContext.isEnabled(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY)) {
                    return mapArrayToArray(jsonParser, deserializationContext);
                }
                c<Object> cVar2 = this._listDeserializer;
                if (cVar2 != null) {
                    return cVar2.deserialize(jsonParser, deserializationContext);
                }
                return mapArray(jsonParser, deserializationContext);
            case 4:
            default:
                return deserializationContext.handleUnexpectedToken(Object.class, jsonParser);
            case 6:
                c<Object> cVar3 = this._stringDeserializer;
                if (cVar3 != null) {
                    return cVar3.deserialize(jsonParser, deserializationContext);
                }
                return jsonParser.X();
            case 7:
                c<Object> cVar4 = this._numberDeserializer;
                if (cVar4 != null) {
                    return cVar4.deserialize(jsonParser, deserializationContext);
                }
                if (deserializationContext.hasSomeOfFeatures(StdDeserializer.F_MASK_INT_COERCIONS)) {
                    return _coerceIntegral(jsonParser, deserializationContext);
                }
                return jsonParser.Q();
            case 8:
                c<Object> cVar5 = this._numberDeserializer;
                if (cVar5 != null) {
                    return cVar5.deserialize(jsonParser, deserializationContext);
                }
                if (deserializationContext.isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
                    return jsonParser.w();
                }
                return jsonParser.Q();
            case 9:
                return Boolean.TRUE;
            case 10:
                return Boolean.FALSE;
            case 11:
                return null;
            case 12:
                return jsonParser.z();
        }
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        int v = jsonParser.v();
        if (v != 1 && v != 3) {
            switch (v) {
                case 5:
                    break;
                case 6:
                    c<Object> cVar = this._stringDeserializer;
                    if (cVar != null) {
                        return cVar.deserialize(jsonParser, deserializationContext);
                    }
                    return jsonParser.X();
                case 7:
                    c<Object> cVar2 = this._numberDeserializer;
                    if (cVar2 != null) {
                        return cVar2.deserialize(jsonParser, deserializationContext);
                    }
                    if (deserializationContext.hasSomeOfFeatures(StdDeserializer.F_MASK_INT_COERCIONS)) {
                        return _coerceIntegral(jsonParser, deserializationContext);
                    }
                    return jsonParser.Q();
                case 8:
                    c<Object> cVar3 = this._numberDeserializer;
                    if (cVar3 != null) {
                        return cVar3.deserialize(jsonParser, deserializationContext);
                    }
                    if (deserializationContext.isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
                        return jsonParser.w();
                    }
                    return jsonParser.Q();
                case 9:
                    return Boolean.TRUE;
                case 10:
                    return Boolean.FALSE;
                case 11:
                    return null;
                case 12:
                    return jsonParser.z();
                default:
                    return deserializationContext.handleUnexpectedToken(Object.class, jsonParser);
            }
        }
        return aVar.deserializeTypedFromAny(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return true;
    }

    public Object mapArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken T0 = jsonParser.T0();
        JsonToken jsonToken = JsonToken.END_ARRAY;
        int i = 2;
        if (T0 == jsonToken) {
            return new ArrayList(2);
        }
        Object deserialize = deserialize(jsonParser, deserializationContext);
        if (jsonParser.T0() == jsonToken) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(deserialize);
            return arrayList;
        }
        Object deserialize2 = deserialize(jsonParser, deserializationContext);
        if (jsonParser.T0() == jsonToken) {
            ArrayList arrayList2 = new ArrayList(2);
            arrayList2.add(deserialize);
            arrayList2.add(deserialize2);
            return arrayList2;
        }
        gl2 leaseObjectBuffer = deserializationContext.leaseObjectBuffer();
        Object[] i2 = leaseObjectBuffer.i();
        i2[0] = deserialize;
        i2[1] = deserialize2;
        int i3 = 2;
        while (true) {
            Object deserialize3 = deserialize(jsonParser, deserializationContext);
            i++;
            if (i3 >= i2.length) {
                i2 = leaseObjectBuffer.c(i2);
                i3 = 0;
            }
            int i4 = i3 + 1;
            i2[i3] = deserialize3;
            if (jsonParser.T0() == JsonToken.END_ARRAY) {
                ArrayList arrayList3 = new ArrayList(i);
                leaseObjectBuffer.e(i2, i4, arrayList3);
                return arrayList3;
            }
            i3 = i4;
        }
    }

    public Object[] mapArrayToArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.T0() == JsonToken.END_ARRAY) {
            return NO_OBJECTS;
        }
        gl2 leaseObjectBuffer = deserializationContext.leaseObjectBuffer();
        Object[] i = leaseObjectBuffer.i();
        int i2 = 0;
        while (true) {
            Object deserialize = deserialize(jsonParser, deserializationContext);
            if (i2 >= i.length) {
                i = leaseObjectBuffer.c(i);
                i2 = 0;
            }
            int i3 = i2 + 1;
            i[i2] = deserialize;
            if (jsonParser.T0() == JsonToken.END_ARRAY) {
                return leaseObjectBuffer.f(i, i3);
            }
            i2 = i3;
        }
    }

    public Object mapObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String str;
        JsonToken u = jsonParser.u();
        if (u == JsonToken.START_OBJECT) {
            str = jsonParser.O0();
        } else if (u == JsonToken.FIELD_NAME) {
            str = jsonParser.r();
        } else if (u != JsonToken.END_OBJECT) {
            return deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
        } else {
            str = null;
        }
        if (str == null) {
            return new LinkedHashMap(2);
        }
        jsonParser.T0();
        Object deserialize = deserialize(jsonParser, deserializationContext);
        String O0 = jsonParser.O0();
        if (O0 == null) {
            LinkedHashMap linkedHashMap = new LinkedHashMap(2);
            linkedHashMap.put(str, deserialize);
            return linkedHashMap;
        }
        jsonParser.T0();
        Object deserialize2 = deserialize(jsonParser, deserializationContext);
        String O02 = jsonParser.O0();
        if (O02 == null) {
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(4);
            linkedHashMap2.put(str, deserialize);
            linkedHashMap2.put(O0, deserialize2);
            return linkedHashMap2;
        }
        LinkedHashMap linkedHashMap3 = new LinkedHashMap();
        linkedHashMap3.put(str, deserialize);
        linkedHashMap3.put(O0, deserialize2);
        do {
            jsonParser.T0();
            linkedHashMap3.put(O02, deserialize(jsonParser, deserializationContext));
            O02 = jsonParser.O0();
        } while (O02 != null);
        return linkedHashMap3;
    }

    @Override // com.fasterxml.jackson.databind.deser.h
    public void resolve(DeserializationContext deserializationContext) throws JsonMappingException {
        JavaType constructType = deserializationContext.constructType(Object.class);
        JavaType constructType2 = deserializationContext.constructType(String.class);
        TypeFactory typeFactory = deserializationContext.getTypeFactory();
        JavaType javaType = this._listType;
        if (javaType == null) {
            this._listDeserializer = _clearIfStdImpl(_findCustomDeser(deserializationContext, typeFactory.constructCollectionType(List.class, constructType)));
        } else {
            this._listDeserializer = _findCustomDeser(deserializationContext, javaType);
        }
        JavaType javaType2 = this._mapType;
        if (javaType2 == null) {
            this._mapDeserializer = _clearIfStdImpl(_findCustomDeser(deserializationContext, typeFactory.constructMapType(Map.class, constructType2, constructType)));
        } else {
            this._mapDeserializer = _findCustomDeser(deserializationContext, javaType2);
        }
        this._stringDeserializer = _clearIfStdImpl(_findCustomDeser(deserializationContext, constructType2));
        this._numberDeserializer = _clearIfStdImpl(_findCustomDeser(deserializationContext, typeFactory.constructType(Number.class)));
        JavaType unknownType = TypeFactory.unknownType();
        this._mapDeserializer = deserializationContext.handleSecondaryContextualization(this._mapDeserializer, null, unknownType);
        this._listDeserializer = deserializationContext.handleSecondaryContextualization(this._listDeserializer, null, unknownType);
        this._stringDeserializer = deserializationContext.handleSecondaryContextualization(this._stringDeserializer, null, unknownType);
        this._numberDeserializer = deserializationContext.handleSecondaryContextualization(this._numberDeserializer, null, unknownType);
    }

    public UntypedObjectDeserializer(JavaType javaType, JavaType javaType2) {
        super(Object.class);
        this._listType = javaType;
        this._mapType = javaType2;
    }

    public UntypedObjectDeserializer(UntypedObjectDeserializer untypedObjectDeserializer, c<?> cVar, c<?> cVar2, c<?> cVar3, c<?> cVar4) {
        super(Object.class);
        this._mapDeserializer = cVar;
        this._listDeserializer = cVar2;
        this._stringDeserializer = cVar3;
        this._numberDeserializer = cVar4;
        this._listType = untypedObjectDeserializer._listType;
        this._mapType = untypedObjectDeserializer._mapType;
    }
}
