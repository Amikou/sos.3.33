package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: JdkDeserializers.java */
/* loaded from: classes.dex */
public class a {
    public static final HashSet<String> a = new HashSet<>();

    static {
        Class[] clsArr = {UUID.class, AtomicBoolean.class, StackTraceElement.class, ByteBuffer.class};
        for (int i = 0; i < 4; i++) {
            a.add(clsArr[i].getName());
        }
        for (Class<?> cls : FromStringDeserializer.types()) {
            a.add(cls.getName());
        }
    }

    public static c<?> a(Class<?> cls, String str) {
        if (a.contains(str)) {
            FromStringDeserializer.Std findDeserializer = FromStringDeserializer.findDeserializer(cls);
            if (findDeserializer != null) {
                return findDeserializer;
            }
            if (cls == UUID.class) {
                return new UUIDDeserializer();
            }
            if (cls == StackTraceElement.class) {
                return new StackTraceElementDeserializer();
            }
            if (cls == AtomicBoolean.class) {
                return new AtomicBooleanDeserializer();
            }
            if (cls == ByteBuffer.class) {
                return new ByteBufferDeserializer();
            }
            return null;
        }
        return null;
    }
}
