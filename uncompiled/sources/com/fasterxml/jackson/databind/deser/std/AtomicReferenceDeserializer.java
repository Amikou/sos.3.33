package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.c;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public class AtomicReferenceDeserializer extends ReferenceTypeDeserializer<AtomicReference<Object>> {
    private static final long serialVersionUID = 1;

    @Deprecated
    public AtomicReferenceDeserializer(JavaType javaType) {
        this(javaType, null, null);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ReferenceTypeDeserializer
    /* renamed from: withResolved  reason: avoid collision after fix types in other method */
    public /* bridge */ /* synthetic */ ReferenceTypeDeserializer<AtomicReference<Object>> withResolved2(com.fasterxml.jackson.databind.jsontype.a aVar, c cVar) {
        return withResolved(aVar, (c<?>) cVar);
    }

    public AtomicReferenceDeserializer(JavaType javaType, com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar) {
        super(javaType, aVar, cVar);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ReferenceTypeDeserializer, com.fasterxml.jackson.databind.c
    public AtomicReference<Object> getNullValue(DeserializationContext deserializationContext) {
        return new AtomicReference<>();
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ReferenceTypeDeserializer
    public AtomicReference<Object> referenceValue(Object obj) {
        return new AtomicReference<>(obj);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ReferenceTypeDeserializer
    public ReferenceTypeDeserializer<AtomicReference<Object>> withResolved(com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar) {
        return new AtomicReferenceDeserializer(this._fullType, aVar, cVar);
    }
}
