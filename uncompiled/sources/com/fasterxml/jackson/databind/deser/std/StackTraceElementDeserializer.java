package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import java.io.IOException;

/* loaded from: classes.dex */
public class StackTraceElementDeserializer extends StdScalarDeserializer<StackTraceElement> {
    private static final long serialVersionUID = 1;

    public StackTraceElementDeserializer() {
        super(StackTraceElement.class);
    }

    public StackTraceElement constructValue(DeserializationContext deserializationContext, String str, String str2, String str3, int i, String str4, String str5) {
        return new StackTraceElement(str, str2, str3, i);
    }

    @Override // com.fasterxml.jackson.databind.c
    public StackTraceElement deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        int _parseIntPrimitive;
        JsonToken u = jsonParser.u();
        if (u != JsonToken.START_OBJECT) {
            if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                jsonParser.T0();
                StackTraceElement deserialize = deserialize(jsonParser, deserializationContext);
                if (jsonParser.T0() != JsonToken.END_ARRAY) {
                    handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                }
                return deserialize;
            }
            return (StackTraceElement) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
        }
        int i = -1;
        String str = null;
        String str2 = null;
        String str3 = "";
        String str4 = str3;
        String str5 = str4;
        while (true) {
            JsonToken U0 = jsonParser.U0();
            if (U0 != JsonToken.END_OBJECT) {
                String r = jsonParser.r();
                if ("className".equals(r)) {
                    str3 = jsonParser.X();
                } else if ("fileName".equals(r)) {
                    str5 = jsonParser.X();
                } else if ("lineNumber".equals(r)) {
                    if (U0.isNumeric()) {
                        _parseIntPrimitive = jsonParser.F();
                    } else {
                        _parseIntPrimitive = _parseIntPrimitive(jsonParser, deserializationContext);
                    }
                    i = _parseIntPrimitive;
                } else if ("methodName".equals(r)) {
                    str4 = jsonParser.X();
                } else if (!"nativeMethod".equals(r)) {
                    if ("moduleName".equals(r)) {
                        str = jsonParser.X();
                    } else if ("moduleVersion".equals(r)) {
                        str2 = jsonParser.X();
                    } else {
                        handleUnknownProperty(jsonParser, deserializationContext, this._valueClass, r);
                    }
                }
            } else {
                return constructValue(deserializationContext, str3, str4, str5, i, str, str2);
            }
        }
    }
}
