package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.g;
import com.fasterxml.jackson.databind.deser.std.StdKeyDeserializer;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.util.EnumResolver;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/* loaded from: classes.dex */
public class StdKeyDeserializers implements g, Serializable {
    private static final long serialVersionUID = 1;

    public static com.fasterxml.jackson.databind.g constructDelegatingKeyDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, c<?> cVar) {
        return new StdKeyDeserializer.DelegatingKD(javaType.getRawClass(), cVar);
    }

    public static com.fasterxml.jackson.databind.g constructEnumKeyDeserializer(EnumResolver enumResolver) {
        return new StdKeyDeserializer.EnumKD(enumResolver, null);
    }

    public static com.fasterxml.jackson.databind.g findStringBasedKeyDeserializer(DeserializationConfig deserializationConfig, JavaType javaType) {
        so introspect = deserializationConfig.introspect(javaType);
        Constructor<?> q = introspect.q(String.class);
        if (q != null) {
            if (deserializationConfig.canOverrideAccessModifiers()) {
                com.fasterxml.jackson.databind.util.c.f(q, deserializationConfig.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
            }
            return new StdKeyDeserializer.StringCtorKeyDeserializer(q);
        }
        Method h = introspect.h(String.class);
        if (h != null) {
            if (deserializationConfig.canOverrideAccessModifiers()) {
                com.fasterxml.jackson.databind.util.c.f(h, deserializationConfig.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
            }
            return new StdKeyDeserializer.StringFactoryKeyDeserializer(h);
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.deser.g
    public com.fasterxml.jackson.databind.g findKeyDeserializer(JavaType javaType, DeserializationConfig deserializationConfig, so soVar) throws JsonMappingException {
        Class<?> rawClass = javaType.getRawClass();
        if (rawClass.isPrimitive()) {
            rawClass = com.fasterxml.jackson.databind.util.c.U(rawClass);
        }
        return StdKeyDeserializer.forType(rawClass);
    }

    public static com.fasterxml.jackson.databind.g constructEnumKeyDeserializer(EnumResolver enumResolver, AnnotatedMethod annotatedMethod) {
        return new StdKeyDeserializer.EnumKD(enumResolver, annotatedMethod);
    }
}
