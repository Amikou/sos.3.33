package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import java.io.IOException;

@pt1
/* loaded from: classes.dex */
public final class StringArrayDeserializer extends StdDeserializer<String[]> implements com.fasterxml.jackson.databind.deser.a {
    public static final StringArrayDeserializer instance = new StringArrayDeserializer();
    private static final long serialVersionUID = 2;
    public c<String> _elementDeserializer;
    public final Boolean _unwrapSingle;

    public StringArrayDeserializer() {
        this(null, null);
    }

    public final String[] _deserializeCustom(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String deserialize;
        gl2 leaseObjectBuffer = deserializationContext.leaseObjectBuffer();
        Object[] i = leaseObjectBuffer.i();
        c<String> cVar = this._elementDeserializer;
        int i2 = 0;
        while (true) {
            try {
                if (jsonParser.S0() == null) {
                    JsonToken u = jsonParser.u();
                    if (u == JsonToken.END_ARRAY) {
                        String[] strArr = (String[]) leaseObjectBuffer.g(i, i2, String.class);
                        deserializationContext.returnObjectBuffer(leaseObjectBuffer);
                        return strArr;
                    }
                    deserialize = u == JsonToken.VALUE_NULL ? cVar.getNullValue(deserializationContext) : cVar.deserialize(jsonParser, deserializationContext);
                } else {
                    deserialize = cVar.deserialize(jsonParser, deserializationContext);
                }
                if (i2 >= i.length) {
                    i = leaseObjectBuffer.c(i);
                    i2 = 0;
                }
                int i3 = i2 + 1;
                try {
                    i[i2] = deserialize;
                    i2 = i3;
                } catch (Exception e) {
                    e = e;
                    i2 = i3;
                    throw JsonMappingException.wrapWithPath(e, String.class, i2);
                }
            } catch (Exception e2) {
                e = e2;
            }
        }
    }

    public final String[] a(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Boolean bool = this._unwrapSingle;
        if (bool == Boolean.TRUE || (bool == null && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY))) {
            String[] strArr = new String[1];
            strArr[0] = jsonParser.F0(JsonToken.VALUE_NULL) ? null : _parseString(jsonParser, deserializationContext);
            return strArr;
        } else if (jsonParser.F0(JsonToken.VALUE_STRING) && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && jsonParser.X().length() == 0) {
            return null;
        } else {
            return (String[]) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
        }
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        c<?> findConvertingContentDeserializer = findConvertingContentDeserializer(deserializationContext, aVar, this._elementDeserializer);
        JavaType constructType = deserializationContext.constructType(String.class);
        if (findConvertingContentDeserializer == null) {
            handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(constructType, aVar);
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(findConvertingContentDeserializer, aVar, constructType);
        }
        Boolean findFormatFeature = findFormatFeature(deserializationContext, aVar, String[].class, JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        if (handleSecondaryContextualization != null && isDefaultDeserializer(handleSecondaryContextualization)) {
            handleSecondaryContextualization = null;
        }
        return (this._elementDeserializer == handleSecondaryContextualization && this._unwrapSingle == findFormatFeature) ? this : new StringArrayDeserializer(handleSecondaryContextualization, findFormatFeature);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return aVar.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StringArrayDeserializer(c<?> cVar, Boolean bool) {
        super(String[].class);
        this._elementDeserializer = cVar;
        this._unwrapSingle = bool;
    }

    @Override // com.fasterxml.jackson.databind.c
    public String[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String S0;
        int i;
        if (!jsonParser.K0()) {
            return a(jsonParser, deserializationContext);
        }
        if (this._elementDeserializer != null) {
            return _deserializeCustom(jsonParser, deserializationContext);
        }
        gl2 leaseObjectBuffer = deserializationContext.leaseObjectBuffer();
        Object[] i2 = leaseObjectBuffer.i();
        int i3 = 0;
        while (true) {
            try {
                S0 = jsonParser.S0();
                if (S0 == null) {
                    JsonToken u = jsonParser.u();
                    if (u == JsonToken.END_ARRAY) {
                        String[] strArr = (String[]) leaseObjectBuffer.g(i2, i3, String.class);
                        deserializationContext.returnObjectBuffer(leaseObjectBuffer);
                        return strArr;
                    } else if (u != JsonToken.VALUE_NULL) {
                        S0 = _parseString(jsonParser, deserializationContext);
                    }
                }
                if (i3 >= i2.length) {
                    i2 = leaseObjectBuffer.c(i2);
                    i3 = 0;
                }
                i = i3 + 1;
            } catch (Exception e) {
                e = e;
            }
            try {
                i2[i3] = S0;
                i3 = i;
            } catch (Exception e2) {
                e = e2;
                i3 = i;
                throw JsonMappingException.wrapWithPath(e, i2, leaseObjectBuffer.d() + i3);
            }
        }
    }
}
