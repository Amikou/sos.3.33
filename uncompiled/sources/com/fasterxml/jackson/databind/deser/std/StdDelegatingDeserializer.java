package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.h;
import java.io.IOException;

/* loaded from: classes.dex */
public class StdDelegatingDeserializer<T> extends StdDeserializer<T> implements com.fasterxml.jackson.databind.deser.a, h {
    private static final long serialVersionUID = 1;
    public final o80<Object, T> _converter;
    public final c<Object> _delegateDeserializer;
    public final JavaType _delegateType;

    public StdDelegatingDeserializer(o80<?, T> o80Var) {
        super(Object.class);
        this._converter = o80Var;
        this._delegateType = null;
        this._delegateDeserializer = null;
    }

    public Object _handleIncompatibleUpdateValue(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        throw new UnsupportedOperationException(String.format("Can not update object of type %s (using deserializer for type %s)" + obj.getClass().getName(), this._delegateType));
    }

    public T convertValue(Object obj) {
        return this._converter.a(obj);
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> cVar = this._delegateDeserializer;
        if (cVar != null) {
            c<?> handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(cVar, aVar, this._delegateType);
            return handleSecondaryContextualization != this._delegateDeserializer ? withDelegate(this._converter, this._delegateType, handleSecondaryContextualization) : this;
        }
        JavaType b = this._converter.b(deserializationContext.getTypeFactory());
        return withDelegate(this._converter, b, deserializationContext.findContextualValueDeserializer(b, aVar));
    }

    @Override // com.fasterxml.jackson.databind.c
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object deserialize = this._delegateDeserializer.deserialize(jsonParser, deserializationContext);
        if (deserialize == null) {
            return null;
        }
        return convertValue(deserialize);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        Object deserialize = this._delegateDeserializer.deserialize(jsonParser, deserializationContext);
        if (deserialize == null) {
            return null;
        }
        return convertValue(deserialize);
    }

    @Override // com.fasterxml.jackson.databind.c
    public c<?> getDelegatee() {
        return this._delegateDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Class<?> handledType() {
        return this._delegateDeserializer.handledType();
    }

    @Override // com.fasterxml.jackson.databind.deser.h
    public void resolve(DeserializationContext deserializationContext) throws JsonMappingException {
        c<Object> cVar = this._delegateDeserializer;
        if (cVar == null || !(cVar instanceof h)) {
            return;
        }
        ((h) cVar).resolve(deserializationContext);
    }

    public StdDelegatingDeserializer<T> withDelegate(o80<Object, T> o80Var, JavaType javaType, c<?> cVar) {
        if (getClass() == StdDelegatingDeserializer.class) {
            return new StdDelegatingDeserializer<>(o80Var, javaType, cVar);
        }
        throw new IllegalStateException("Sub-class " + getClass().getName() + " must override 'withDelegate'");
    }

    @Override // com.fasterxml.jackson.databind.c
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        if (this._delegateType.getRawClass().isAssignableFrom(obj.getClass())) {
            return (T) this._delegateDeserializer.deserialize(jsonParser, deserializationContext, obj);
        }
        return (T) _handleIncompatibleUpdateValue(jsonParser, deserializationContext, obj);
    }

    public StdDelegatingDeserializer(o80<Object, T> o80Var, JavaType javaType, c<?> cVar) {
        super(javaType);
        this._converter = o80Var;
        this._delegateType = javaType;
        this._delegateDeserializer = cVar;
    }

    public StdDelegatingDeserializer(StdDelegatingDeserializer<T> stdDelegatingDeserializer) {
        super(stdDelegatingDeserializer);
        this._converter = stdDelegatingDeserializer._converter;
        this._delegateType = stdDelegatingDeserializer._delegateType;
        this._delegateDeserializer = stdDelegatingDeserializer._delegateDeserializer;
    }
}
