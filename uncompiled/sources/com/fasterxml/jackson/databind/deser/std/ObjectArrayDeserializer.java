package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.type.ArrayType;
import java.io.IOException;
import java.lang.reflect.Array;

@pt1
/* loaded from: classes.dex */
public class ObjectArrayDeserializer extends ContainerDeserializerBase<Object[]> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = 1;
    public final ArrayType _arrayType;
    public final Class<?> _elementClass;
    public c<Object> _elementDeserializer;
    public final com.fasterxml.jackson.databind.jsontype.a _elementTypeDeserializer;
    public final boolean _untyped;
    public final Boolean _unwrapSingle;

    public ObjectArrayDeserializer(ArrayType arrayType, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar) {
        super(arrayType);
        this._arrayType = arrayType;
        Class<?> rawClass = arrayType.getContentType().getRawClass();
        this._elementClass = rawClass;
        this._untyped = rawClass == Object.class;
        this._elementDeserializer = cVar;
        this._elementTypeDeserializer = aVar;
        this._unwrapSingle = null;
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        c<?> cVar = this._elementDeserializer;
        Boolean findFormatFeature = findFormatFeature(deserializationContext, aVar, this._arrayType.getRawClass(), JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        c<?> findConvertingContentDeserializer = findConvertingContentDeserializer(deserializationContext, aVar, cVar);
        JavaType contentType = this._arrayType.getContentType();
        if (findConvertingContentDeserializer == null) {
            handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(contentType, aVar);
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(findConvertingContentDeserializer, aVar, contentType);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = this._elementTypeDeserializer;
        if (aVar2 != null) {
            aVar2 = aVar2.forProperty(aVar);
        }
        return withResolved(aVar2, handleSecondaryContextualization, findFormatFeature);
    }

    public Byte[] deserializeFromBase64(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        byte[] j = jsonParser.j(deserializationContext.getBase64Variant());
        Byte[] bArr = new Byte[j.length];
        int length = j.length;
        for (int i = 0; i < length; i++) {
            bArr[i] = Byte.valueOf(j[i]);
        }
        return bArr;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public c<Object> getContentDeserializer() {
        return this._elementDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public JavaType getContentType() {
        return this._arrayType.getContentType();
    }

    public Object[] handleNonArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object deserializeWithType;
        JsonToken jsonToken = JsonToken.VALUE_STRING;
        if (jsonParser.F0(jsonToken) && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && jsonParser.X().length() == 0) {
            return null;
        }
        Boolean bool = this._unwrapSingle;
        if (!(bool == Boolean.TRUE || (bool == null && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)))) {
            if (jsonParser.u() == jsonToken && this._elementClass == Byte.class) {
                return deserializeFromBase64(jsonParser, deserializationContext);
            }
            return (Object[]) deserializationContext.handleUnexpectedToken(this._arrayType.getRawClass(), jsonParser);
        }
        if (jsonParser.u() == JsonToken.VALUE_NULL) {
            deserializeWithType = this._elementDeserializer.getNullValue(deserializationContext);
        } else {
            com.fasterxml.jackson.databind.jsontype.a aVar = this._elementTypeDeserializer;
            if (aVar == null) {
                deserializeWithType = this._elementDeserializer.deserialize(jsonParser, deserializationContext);
            } else {
                deserializeWithType = this._elementDeserializer.deserializeWithType(jsonParser, deserializationContext, aVar);
            }
        }
        Object[] objArr = this._untyped ? new Object[1] : (Object[]) Array.newInstance(this._elementClass, 1);
        objArr[0] = deserializeWithType;
        return objArr;
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return this._elementDeserializer == null && this._elementTypeDeserializer == null;
    }

    public ObjectArrayDeserializer withDeserializer(com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar) {
        return withResolved(aVar, cVar, this._unwrapSingle);
    }

    public ObjectArrayDeserializer withResolved(com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar, Boolean bool) {
        return (bool == this._unwrapSingle && cVar == this._elementDeserializer && aVar == this._elementTypeDeserializer) ? this : new ObjectArrayDeserializer(this, cVar, aVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object[] g;
        Object deserializeWithType;
        if (!jsonParser.K0()) {
            return handleNonArray(jsonParser, deserializationContext);
        }
        gl2 leaseObjectBuffer = deserializationContext.leaseObjectBuffer();
        Object[] i = leaseObjectBuffer.i();
        com.fasterxml.jackson.databind.jsontype.a aVar = this._elementTypeDeserializer;
        int i2 = 0;
        while (true) {
            try {
                JsonToken T0 = jsonParser.T0();
                if (T0 == JsonToken.END_ARRAY) {
                    break;
                }
                if (T0 == JsonToken.VALUE_NULL) {
                    deserializeWithType = this._elementDeserializer.getNullValue(deserializationContext);
                } else if (aVar == null) {
                    deserializeWithType = this._elementDeserializer.deserialize(jsonParser, deserializationContext);
                } else {
                    deserializeWithType = this._elementDeserializer.deserializeWithType(jsonParser, deserializationContext, aVar);
                }
                if (i2 >= i.length) {
                    i = leaseObjectBuffer.c(i);
                    i2 = 0;
                }
                int i3 = i2 + 1;
                try {
                    i[i2] = deserializeWithType;
                    i2 = i3;
                } catch (Exception e) {
                    e = e;
                    i2 = i3;
                    throw JsonMappingException.wrapWithPath(e, i, leaseObjectBuffer.d() + i2);
                }
            } catch (Exception e2) {
                e = e2;
            }
        }
        if (this._untyped) {
            g = leaseObjectBuffer.f(i, i2);
        } else {
            g = leaseObjectBuffer.g(i, i2, this._elementClass);
        }
        deserializationContext.returnObjectBuffer(leaseObjectBuffer);
        return g;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object[] deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return (Object[]) aVar.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    public ObjectArrayDeserializer(ObjectArrayDeserializer objectArrayDeserializer, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar, Boolean bool) {
        super(objectArrayDeserializer._arrayType);
        this._arrayType = objectArrayDeserializer._arrayType;
        this._elementClass = objectArrayDeserializer._elementClass;
        this._untyped = objectArrayDeserializer._untyped;
        this._elementDeserializer = cVar;
        this._elementTypeDeserializer = aVar;
        this._unwrapSingle = bool;
    }
}
