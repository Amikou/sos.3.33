package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.m;
import java.io.IOException;

/* compiled from: JsonNodeDeserializer.java */
/* loaded from: classes.dex */
public abstract class BaseNodeDeserializer<T extends d> extends StdDeserializer<T> {
    public BaseNodeDeserializer(Class<T> cls) {
        super((Class<?>) cls);
    }

    public final d _fromEmbedded(JsonParser jsonParser, DeserializationContext deserializationContext, JsonNodeFactory jsonNodeFactory) throws IOException {
        Object z = jsonParser.z();
        if (z == null) {
            return jsonNodeFactory.m4nullNode();
        }
        if (z.getClass() == byte[].class) {
            return jsonNodeFactory.m1binaryNode((byte[]) z);
        }
        if (z instanceof com.fasterxml.jackson.databind.util.d) {
            return jsonNodeFactory.rawValueNode((com.fasterxml.jackson.databind.util.d) z);
        }
        if (z instanceof d) {
            return (d) z;
        }
        return jsonNodeFactory.pojoNode(z);
    }

    public final d _fromFloat(JsonParser jsonParser, DeserializationContext deserializationContext, JsonNodeFactory jsonNodeFactory) throws IOException {
        JsonParser.NumberType N = jsonParser.N();
        if (N == JsonParser.NumberType.BIG_DECIMAL) {
            return jsonNodeFactory.numberNode(jsonParser.w());
        }
        if (deserializationContext.isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
            double x = jsonParser.x();
            if (!Double.isInfinite(x) && !Double.isNaN(x)) {
                return jsonNodeFactory.numberNode(jsonParser.w());
            }
            return jsonNodeFactory.numberNode(x);
        } else if (N == JsonParser.NumberType.FLOAT) {
            return jsonNodeFactory.numberNode(jsonParser.A());
        } else {
            return jsonNodeFactory.numberNode(jsonParser.x());
        }
    }

    public final d _fromInt(JsonParser jsonParser, DeserializationContext deserializationContext, JsonNodeFactory jsonNodeFactory) throws IOException {
        JsonParser.NumberType N;
        int deserializationFeatures = deserializationContext.getDeserializationFeatures();
        if ((StdDeserializer.F_MASK_INT_COERCIONS & deserializationFeatures) != 0) {
            if (DeserializationFeature.USE_BIG_INTEGER_FOR_INTS.enabledIn(deserializationFeatures)) {
                N = JsonParser.NumberType.BIG_INTEGER;
            } else if (DeserializationFeature.USE_LONG_FOR_INTS.enabledIn(deserializationFeatures)) {
                N = JsonParser.NumberType.LONG;
            } else {
                N = jsonParser.N();
            }
        } else {
            N = jsonParser.N();
        }
        if (N == JsonParser.NumberType.INT) {
            return jsonNodeFactory.numberNode(jsonParser.F());
        }
        if (N == JsonParser.NumberType.LONG) {
            return jsonNodeFactory.numberNode(jsonParser.M());
        }
        return jsonNodeFactory.numberNode(jsonParser.h());
    }

    public void _handleDuplicateField(JsonParser jsonParser, DeserializationContext deserializationContext, JsonNodeFactory jsonNodeFactory, String str, m mVar, d dVar, d dVar2) throws JsonProcessingException {
        if (deserializationContext.isEnabled(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY)) {
            deserializationContext.reportMappingException("Duplicate field '%s' for ObjectNode: not allowed when FAIL_ON_READING_DUP_TREE_KEY enabled", str);
        }
    }

    @Deprecated
    public void _reportProblem(JsonParser jsonParser, String str) throws JsonMappingException {
        throw JsonMappingException.from(jsonParser, str);
    }

    public final d deserializeAny(JsonParser jsonParser, DeserializationContext deserializationContext, JsonNodeFactory jsonNodeFactory) throws IOException {
        switch (jsonParser.v()) {
            case 1:
            case 2:
            case 5:
                return deserializeObject(jsonParser, deserializationContext, jsonNodeFactory);
            case 3:
                return deserializeArray(jsonParser, deserializationContext, jsonNodeFactory);
            case 4:
            default:
                return (d) deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
            case 6:
                return jsonNodeFactory.m13textNode(jsonParser.X());
            case 7:
                return _fromInt(jsonParser, deserializationContext, jsonNodeFactory);
            case 8:
                return _fromFloat(jsonParser, deserializationContext, jsonNodeFactory);
            case 9:
                return jsonNodeFactory.m3booleanNode(true);
            case 10:
                return jsonNodeFactory.m3booleanNode(false);
            case 11:
                return jsonNodeFactory.m4nullNode();
            case 12:
                return _fromEmbedded(jsonParser, deserializationContext, jsonNodeFactory);
        }
    }

    public final com.fasterxml.jackson.databind.node.a deserializeArray(JsonParser jsonParser, DeserializationContext deserializationContext, JsonNodeFactory jsonNodeFactory) throws IOException {
        com.fasterxml.jackson.databind.node.a arrayNode = jsonNodeFactory.arrayNode();
        while (true) {
            switch (jsonParser.T0().id()) {
                case 1:
                    arrayNode.S(deserializeObject(jsonParser, deserializationContext, jsonNodeFactory));
                    break;
                case 2:
                case 5:
                case 8:
                default:
                    arrayNode.S(deserializeAny(jsonParser, deserializationContext, jsonNodeFactory));
                    break;
                case 3:
                    arrayNode.S(deserializeArray(jsonParser, deserializationContext, jsonNodeFactory));
                    break;
                case 4:
                    return arrayNode;
                case 6:
                    arrayNode.S(jsonNodeFactory.m13textNode(jsonParser.X()));
                    break;
                case 7:
                    arrayNode.S(_fromInt(jsonParser, deserializationContext, jsonNodeFactory));
                    break;
                case 9:
                    arrayNode.S(jsonNodeFactory.m3booleanNode(true));
                    break;
                case 10:
                    arrayNode.S(jsonNodeFactory.m3booleanNode(false));
                    break;
                case 11:
                    arrayNode.S(jsonNodeFactory.m4nullNode());
                    break;
                case 12:
                    arrayNode.S(_fromEmbedded(jsonParser, deserializationContext, jsonNodeFactory));
                    break;
            }
        }
    }

    public final m deserializeObject(JsonParser jsonParser, DeserializationContext deserializationContext, JsonNodeFactory jsonNodeFactory) throws IOException {
        String r;
        d deserializeObject;
        m objectNode = jsonNodeFactory.objectNode();
        if (jsonParser.L0()) {
            r = jsonParser.O0();
        } else {
            JsonToken u = jsonParser.u();
            if (u == JsonToken.END_OBJECT) {
                return objectNode;
            }
            if (u != JsonToken.FIELD_NAME) {
                return (m) deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
            }
            r = jsonParser.r();
        }
        String str = r;
        while (str != null) {
            JsonToken T0 = jsonParser.T0();
            if (T0 != null) {
                int id = T0.id();
                if (id == 1) {
                    deserializeObject = deserializeObject(jsonParser, deserializationContext, jsonNodeFactory);
                } else if (id == 3) {
                    deserializeObject = deserializeArray(jsonParser, deserializationContext, jsonNodeFactory);
                } else if (id == 6) {
                    deserializeObject = jsonNodeFactory.m13textNode(jsonParser.X());
                } else if (id != 7) {
                    switch (id) {
                        case 9:
                            deserializeObject = jsonNodeFactory.m3booleanNode(true);
                            break;
                        case 10:
                            deserializeObject = jsonNodeFactory.m3booleanNode(false);
                            break;
                        case 11:
                            deserializeObject = jsonNodeFactory.m4nullNode();
                            break;
                        case 12:
                            deserializeObject = _fromEmbedded(jsonParser, deserializationContext, jsonNodeFactory);
                            break;
                        default:
                            deserializeObject = deserializeAny(jsonParser, deserializationContext, jsonNodeFactory);
                            break;
                    }
                } else {
                    deserializeObject = _fromInt(jsonParser, deserializationContext, jsonNodeFactory);
                }
                d dVar = deserializeObject;
                d Y = objectNode.Y(str, dVar);
                if (Y != null) {
                    _handleDuplicateField(jsonParser, deserializationContext, jsonNodeFactory, str, objectNode, Y, dVar);
                }
                str = jsonParser.O0();
            } else {
                throw deserializationContext.mappingException("Unexpected end-of-input when binding data into ObjectNode");
            }
        }
        return objectNode;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return aVar.deserializeTypedFromAny(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return true;
    }
}
