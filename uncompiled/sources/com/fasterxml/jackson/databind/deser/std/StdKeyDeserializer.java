package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.d;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.g;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.util.EnumResolver;
import com.fasterxml.jackson.databind.util.e;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

@pt1
/* loaded from: classes.dex */
public class StdKeyDeserializer extends g implements Serializable {
    public static final int TYPE_BOOLEAN = 1;
    public static final int TYPE_BYTE = 2;
    public static final int TYPE_CALENDAR = 11;
    public static final int TYPE_CHAR = 4;
    public static final int TYPE_CLASS = 15;
    public static final int TYPE_CURRENCY = 16;
    public static final int TYPE_DATE = 10;
    public static final int TYPE_DOUBLE = 8;
    public static final int TYPE_FLOAT = 7;
    public static final int TYPE_INT = 5;
    public static final int TYPE_LOCALE = 9;
    public static final int TYPE_LONG = 6;
    public static final int TYPE_SHORT = 3;
    public static final int TYPE_URI = 13;
    public static final int TYPE_URL = 14;
    public static final int TYPE_UUID = 12;
    private static final long serialVersionUID = 1;
    public final FromStringDeserializer<?> _deser;
    public final Class<?> _keyClass;
    public final int _kind;

    /* loaded from: classes.dex */
    public static final class DelegatingKD extends g implements Serializable {
        private static final long serialVersionUID = 1;
        public final c<?> _delegate;
        public final Class<?> _keyClass;

        public DelegatingKD(Class<?> cls, c<?> cVar) {
            this._keyClass = cls;
            this._delegate = cVar;
        }

        @Override // com.fasterxml.jackson.databind.g
        public final Object deserializeKey(String str, DeserializationContext deserializationContext) throws IOException {
            if (str == null) {
                return null;
            }
            e eVar = new e(deserializationContext.getParser(), deserializationContext);
            eVar.o1(str);
            try {
                JsonParser K1 = eVar.K1();
                K1.T0();
                Object deserialize = this._delegate.deserialize(K1, deserializationContext);
                return deserialize != null ? deserialize : deserializationContext.handleWeirdKey(this._keyClass, str, "not a valid representation", new Object[0]);
            } catch (Exception e) {
                return deserializationContext.handleWeirdKey(this._keyClass, str, "not a valid representation: %s", e.getMessage());
            }
        }

        public Class<?> getKeyClass() {
            return this._keyClass;
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class EnumKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;
        public final EnumResolver _byNameResolver;
        public EnumResolver _byToStringResolver;
        public final AnnotatedMethod _factory;

        public EnumKD(EnumResolver enumResolver, AnnotatedMethod annotatedMethod) {
            super(-1, enumResolver.getEnumClass());
            this._byNameResolver = enumResolver;
            this._factory = annotatedMethod;
        }

        @Override // com.fasterxml.jackson.databind.deser.std.StdKeyDeserializer
        public Object _parse(String str, DeserializationContext deserializationContext) throws IOException {
            AnnotatedMethod annotatedMethod = this._factory;
            if (annotatedMethod != null) {
                try {
                    return annotatedMethod.call1(str);
                } catch (Exception e) {
                    com.fasterxml.jackson.databind.util.c.S(e);
                }
            }
            EnumResolver a = deserializationContext.isEnabled(DeserializationFeature.READ_ENUMS_USING_TO_STRING) ? a(deserializationContext) : this._byNameResolver;
            Enum<?> findEnum = a.findEnum(str);
            return (findEnum != null || deserializationContext.getConfig().isEnabled(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) ? findEnum : deserializationContext.handleWeirdKey(this._keyClass, str, "not one of values excepted for Enum class: %s", a.getEnumIds());
        }

        public final EnumResolver a(DeserializationContext deserializationContext) {
            EnumResolver enumResolver = this._byToStringResolver;
            if (enumResolver == null) {
                synchronized (this) {
                    enumResolver = EnumResolver.constructUnsafeUsingToString(this._byNameResolver.getEnumClass(), deserializationContext.getAnnotationIntrospector());
                }
            }
            return enumResolver;
        }
    }

    /* loaded from: classes.dex */
    public static final class StringCtorKeyDeserializer extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;
        public final Constructor<?> _ctor;

        public StringCtorKeyDeserializer(Constructor<?> constructor) {
            super(-1, constructor.getDeclaringClass());
            this._ctor = constructor;
        }

        @Override // com.fasterxml.jackson.databind.deser.std.StdKeyDeserializer
        public Object _parse(String str, DeserializationContext deserializationContext) throws Exception {
            return this._ctor.newInstance(str);
        }
    }

    /* loaded from: classes.dex */
    public static final class StringFactoryKeyDeserializer extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;
        public final Method _factoryMethod;

        public StringFactoryKeyDeserializer(Method method) {
            super(-1, method.getDeclaringClass());
            this._factoryMethod = method;
        }

        @Override // com.fasterxml.jackson.databind.deser.std.StdKeyDeserializer
        public Object _parse(String str, DeserializationContext deserializationContext) throws Exception {
            return this._factoryMethod.invoke(null, str);
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class StringKD extends StdKeyDeserializer {
        public static final StringKD a = new StringKD(String.class);
        public static final StringKD f0 = new StringKD(Object.class);
        private static final long serialVersionUID = 1;

        public StringKD(Class<?> cls) {
            super(-1, cls);
        }

        public static StringKD forType(Class<?> cls) {
            if (cls == String.class) {
                return a;
            }
            if (cls == Object.class) {
                return f0;
            }
            return new StringKD(cls);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.StdKeyDeserializer, com.fasterxml.jackson.databind.g
        public Object deserializeKey(String str, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            return str;
        }
    }

    public StdKeyDeserializer(int i, Class<?> cls) {
        this(i, cls, null);
    }

    public static StdKeyDeserializer forType(Class<?> cls) {
        int i;
        if (cls != String.class && cls != Object.class && cls != CharSequence.class) {
            if (cls == UUID.class) {
                i = 12;
            } else if (cls == Integer.class) {
                i = 5;
            } else if (cls == Long.class) {
                i = 6;
            } else if (cls == Date.class) {
                i = 10;
            } else if (cls == Calendar.class) {
                i = 11;
            } else if (cls == Boolean.class) {
                i = 1;
            } else if (cls == Byte.class) {
                i = 2;
            } else if (cls == Character.class) {
                i = 4;
            } else if (cls == Short.class) {
                i = 3;
            } else if (cls == Float.class) {
                i = 7;
            } else if (cls == Double.class) {
                i = 8;
            } else if (cls == URI.class) {
                i = 13;
            } else if (cls == URL.class) {
                i = 14;
            } else if (cls != Class.class) {
                if (cls == Locale.class) {
                    return new StdKeyDeserializer(9, cls, FromStringDeserializer.findDeserializer(Locale.class));
                }
                if (cls == Currency.class) {
                    return new StdKeyDeserializer(16, cls, FromStringDeserializer.findDeserializer(Currency.class));
                }
                return null;
            } else {
                i = 15;
            }
            return new StdKeyDeserializer(i, cls);
        }
        return StringKD.forType(cls);
    }

    public Object _parse(String str, DeserializationContext deserializationContext) throws Exception {
        switch (this._kind) {
            case 1:
                if ("true".equals(str)) {
                    return Boolean.TRUE;
                }
                if ("false".equals(str)) {
                    return Boolean.FALSE;
                }
                return deserializationContext.handleWeirdKey(this._keyClass, str, "value not 'true' or 'false'", new Object[0]);
            case 2:
                int _parseInt = _parseInt(str);
                if (_parseInt >= -128 && _parseInt <= 255) {
                    return Byte.valueOf((byte) _parseInt);
                }
                return deserializationContext.handleWeirdKey(this._keyClass, str, "overflow, value can not be represented as 8-bit value", new Object[0]);
            case 3:
                int _parseInt2 = _parseInt(str);
                if (_parseInt2 >= -32768 && _parseInt2 <= 32767) {
                    return Short.valueOf((short) _parseInt2);
                }
                return deserializationContext.handleWeirdKey(this._keyClass, str, "overflow, value can not be represented as 16-bit value", new Object[0]);
            case 4:
                if (str.length() == 1) {
                    return Character.valueOf(str.charAt(0));
                }
                return deserializationContext.handleWeirdKey(this._keyClass, str, "can only convert 1-character Strings", new Object[0]);
            case 5:
                return Integer.valueOf(_parseInt(str));
            case 6:
                return Long.valueOf(_parseLong(str));
            case 7:
                return Float.valueOf((float) _parseDouble(str));
            case 8:
                return Double.valueOf(_parseDouble(str));
            case 9:
                try {
                    return this._deser._deserialize(str, deserializationContext);
                } catch (IOException unused) {
                    return deserializationContext.handleWeirdKey(this._keyClass, str, "unable to parse key as locale", new Object[0]);
                }
            case 10:
                return deserializationContext.parseDate(str);
            case 11:
                Date parseDate = deserializationContext.parseDate(str);
                if (parseDate == null) {
                    return null;
                }
                return deserializationContext.constructCalendar(parseDate);
            case 12:
                try {
                    return UUID.fromString(str);
                } catch (Exception e) {
                    return deserializationContext.handleWeirdKey(this._keyClass, str, "problem: %s", e.getMessage());
                }
            case 13:
                try {
                    return URI.create(str);
                } catch (Exception e2) {
                    return deserializationContext.handleWeirdKey(this._keyClass, str, "problem: %s", e2.getMessage());
                }
            case 14:
                try {
                    return new URL(str);
                } catch (MalformedURLException e3) {
                    return deserializationContext.handleWeirdKey(this._keyClass, str, "problem: %s", e3.getMessage());
                }
            case 15:
                try {
                    return deserializationContext.findClass(str);
                } catch (Exception unused2) {
                    return deserializationContext.handleWeirdKey(this._keyClass, str, "unable to parse key as Class", new Object[0]);
                }
            case 16:
                try {
                    return this._deser._deserialize(str, deserializationContext);
                } catch (IOException unused3) {
                    return deserializationContext.handleWeirdKey(this._keyClass, str, "unable to parse key as currency", new Object[0]);
                }
            default:
                throw new IllegalStateException("Internal error: unknown key type " + this._keyClass);
        }
    }

    public double _parseDouble(String str) throws IllegalArgumentException {
        return d.i(str);
    }

    public int _parseInt(String str) throws IllegalArgumentException {
        return Integer.parseInt(str);
    }

    public long _parseLong(String str) throws IllegalArgumentException {
        return Long.parseLong(str);
    }

    @Override // com.fasterxml.jackson.databind.g
    public Object deserializeKey(String str, DeserializationContext deserializationContext) throws IOException {
        if (str == null) {
            return null;
        }
        try {
            Object _parse = _parse(str, deserializationContext);
            if (_parse != null) {
                return _parse;
            }
            if (this._keyClass.isEnum() && deserializationContext.getConfig().isEnabled(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                return null;
            }
            return deserializationContext.handleWeirdKey(this._keyClass, str, "not a valid representation", new Object[0]);
        } catch (Exception e) {
            return deserializationContext.handleWeirdKey(this._keyClass, str, "not a valid representation, problem: (%s) %s", e.getClass().getName(), e.getMessage());
        }
    }

    public Class<?> getKeyClass() {
        return this._keyClass;
    }

    public StdKeyDeserializer(int i, Class<?> cls, FromStringDeserializer<?> fromStringDeserializer) {
        this._kind = i;
        this._keyClass = cls;
        this._deser = fromStringDeserializer;
    }
}
