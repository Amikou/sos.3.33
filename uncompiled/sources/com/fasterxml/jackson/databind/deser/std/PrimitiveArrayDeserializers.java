package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import defpackage.lh;
import java.io.IOException;

/* loaded from: classes.dex */
public abstract class PrimitiveArrayDeserializers<T> extends StdDeserializer<T> implements com.fasterxml.jackson.databind.deser.a {
    public final Boolean _unwrapSingle;

    @pt1
    /* loaded from: classes.dex */
    public static final class BooleanDeser extends PrimitiveArrayDeserializers<boolean[]> {
        private static final long serialVersionUID = 1;

        public BooleanDeser() {
            super(boolean[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return new BooleanDeser(this, bool);
        }

        public BooleanDeser(BooleanDeser booleanDeser, Boolean bool) {
            super(booleanDeser, bool);
        }

        @Override // com.fasterxml.jackson.databind.c
        public boolean[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            boolean _parseBooleanPrimitive;
            int i;
            if (!jsonParser.K0()) {
                return handleNonArray(jsonParser, deserializationContext);
            }
            lh.b c = deserializationContext.getArrayBuilders().c();
            boolean[] f = c.f();
            int i2 = 0;
            while (jsonParser.T0() != JsonToken.END_ARRAY) {
                try {
                    _parseBooleanPrimitive = _parseBooleanPrimitive(jsonParser, deserializationContext);
                    if (i2 >= f.length) {
                        boolean[] c2 = c.c(f, i2);
                        i2 = 0;
                        f = c2;
                    }
                    i = i2 + 1;
                } catch (Exception e) {
                    e = e;
                }
                try {
                    f[i2] = _parseBooleanPrimitive;
                    i2 = i;
                } catch (Exception e2) {
                    e = e2;
                    i2 = i;
                    throw JsonMappingException.wrapWithPath(e, f, c.d() + i2);
                }
            }
            return c.e(f, i2);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public boolean[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return new boolean[]{_parseBooleanPrimitive(jsonParser, deserializationContext)};
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class ByteDeser extends PrimitiveArrayDeserializers<byte[]> {
        private static final long serialVersionUID = 1;

        public ByteDeser() {
            super(byte[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return new ByteDeser(this, bool);
        }

        public ByteDeser(ByteDeser byteDeser, Boolean bool) {
            super(byteDeser, bool);
        }

        /* JADX WARN: Removed duplicated region for block: B:35:0x0070 A[Catch: Exception -> 0x0088, TRY_LEAVE, TryCatch #1 {Exception -> 0x0088, blocks: (B:21:0x0041, B:23:0x0049, B:25:0x004d, B:28:0x0052, B:33:0x006d, B:35:0x0070, B:31:0x0058, B:32:0x0069), top: B:49:0x0041 }] */
        @Override // com.fasterxml.jackson.databind.c
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public byte[] deserialize(com.fasterxml.jackson.core.JsonParser r7, com.fasterxml.jackson.databind.DeserializationContext r8) throws java.io.IOException {
            /*
                r6 = this;
                com.fasterxml.jackson.core.JsonToken r0 = r7.u()
                com.fasterxml.jackson.core.JsonToken r1 = com.fasterxml.jackson.core.JsonToken.VALUE_STRING
                if (r0 != r1) goto L11
                com.fasterxml.jackson.core.Base64Variant r8 = r8.getBase64Variant()
                byte[] r7 = r7.j(r8)
                return r7
            L11:
                com.fasterxml.jackson.core.JsonToken r1 = com.fasterxml.jackson.core.JsonToken.VALUE_EMBEDDED_OBJECT
                if (r0 != r1) goto L24
                java.lang.Object r0 = r7.z()
                if (r0 != 0) goto L1d
                r7 = 0
                return r7
            L1d:
                boolean r1 = r0 instanceof byte[]
                if (r1 == 0) goto L24
                byte[] r0 = (byte[]) r0
                return r0
            L24:
                boolean r0 = r7.K0()
                if (r0 != 0) goto L31
                java.lang.Object r7 = r6.handleNonArray(r7, r8)
                byte[] r7 = (byte[]) r7
                return r7
            L31:
                lh r0 = r8.getArrayBuilders()
                lh$c r0 = r0.d()
                java.lang.Object r1 = r0.f()
                byte[] r1 = (byte[]) r1
                r2 = 0
                r3 = r2
            L41:
                com.fasterxml.jackson.core.JsonToken r4 = r7.T0()     // Catch: java.lang.Exception -> L88
                com.fasterxml.jackson.core.JsonToken r5 = com.fasterxml.jackson.core.JsonToken.END_ARRAY     // Catch: java.lang.Exception -> L88
                if (r4 == r5) goto L81
                com.fasterxml.jackson.core.JsonToken r5 = com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_INT     // Catch: java.lang.Exception -> L88
                if (r4 == r5) goto L69
                com.fasterxml.jackson.core.JsonToken r5 = com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_FLOAT     // Catch: java.lang.Exception -> L88
                if (r4 != r5) goto L52
                goto L69
            L52:
                com.fasterxml.jackson.core.JsonToken r5 = com.fasterxml.jackson.core.JsonToken.VALUE_NULL     // Catch: java.lang.Exception -> L88
                if (r4 != r5) goto L58
                r4 = r2
                goto L6d
            L58:
                java.lang.Class<?> r4 = r6._valueClass     // Catch: java.lang.Exception -> L88
                java.lang.Class r4 = r4.getComponentType()     // Catch: java.lang.Exception -> L88
                java.lang.Object r4 = r8.handleUnexpectedToken(r4, r7)     // Catch: java.lang.Exception -> L88
                java.lang.Number r4 = (java.lang.Number) r4     // Catch: java.lang.Exception -> L88
                byte r4 = r4.byteValue()     // Catch: java.lang.Exception -> L88
                goto L6d
            L69:
                byte r4 = r7.m()     // Catch: java.lang.Exception -> L88
            L6d:
                int r5 = r1.length     // Catch: java.lang.Exception -> L88
                if (r3 < r5) goto L78
                java.lang.Object r5 = r0.c(r1, r3)     // Catch: java.lang.Exception -> L88
                byte[] r5 = (byte[]) r5     // Catch: java.lang.Exception -> L88
                r3 = r2
                r1 = r5
            L78:
                int r5 = r3 + 1
                r1[r3] = r4     // Catch: java.lang.Exception -> L7e
                r3 = r5
                goto L41
            L7e:
                r7 = move-exception
                r3 = r5
                goto L89
            L81:
                java.lang.Object r7 = r0.e(r1, r3)
                byte[] r7 = (byte[]) r7
                return r7
            L88:
                r7 = move-exception
            L89:
                int r8 = r0.d()
                int r8 = r8 + r3
                com.fasterxml.jackson.databind.JsonMappingException r7 = com.fasterxml.jackson.databind.JsonMappingException.wrapWithPath(r7, r1, r8)
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers.ByteDeser.deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext):byte[]");
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public byte[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            byte m;
            JsonToken u = jsonParser.u();
            if (u != JsonToken.VALUE_NUMBER_INT && u != JsonToken.VALUE_NUMBER_FLOAT) {
                if (u == JsonToken.VALUE_NULL) {
                    return null;
                }
                m = ((Number) deserializationContext.handleUnexpectedToken(this._valueClass.getComponentType(), jsonParser)).byteValue();
            } else {
                m = jsonParser.m();
            }
            return new byte[]{m};
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class CharDeser extends PrimitiveArrayDeserializers<char[]> {
        private static final long serialVersionUID = 1;

        public CharDeser() {
            super(char[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return this;
        }

        public CharDeser(CharDeser charDeser, Boolean bool) {
            super(charDeser, bool);
        }

        @Override // com.fasterxml.jackson.databind.c
        public char[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            String charSequence;
            JsonToken u = jsonParser.u();
            if (u == JsonToken.VALUE_STRING) {
                char[] a0 = jsonParser.a0();
                int e0 = jsonParser.e0();
                int b0 = jsonParser.b0();
                char[] cArr = new char[b0];
                System.arraycopy(a0, e0, cArr, 0, b0);
                return cArr;
            } else if (jsonParser.K0()) {
                StringBuilder sb = new StringBuilder(64);
                while (true) {
                    JsonToken T0 = jsonParser.T0();
                    if (T0 != JsonToken.END_ARRAY) {
                        if (T0 == JsonToken.VALUE_STRING) {
                            charSequence = jsonParser.X();
                        } else {
                            charSequence = ((CharSequence) deserializationContext.handleUnexpectedToken(Character.TYPE, jsonParser)).toString();
                        }
                        if (charSequence.length() != 1) {
                            deserializationContext.reportMappingException("Can not convert a JSON String of length %d into a char element of char array", Integer.valueOf(charSequence.length()));
                        }
                        sb.append(charSequence.charAt(0));
                    } else {
                        return sb.toString().toCharArray();
                    }
                }
            } else {
                if (u == JsonToken.VALUE_EMBEDDED_OBJECT) {
                    Object z = jsonParser.z();
                    if (z == null) {
                        return null;
                    }
                    if (z instanceof char[]) {
                        return (char[]) z;
                    }
                    if (z instanceof String) {
                        return ((String) z).toCharArray();
                    }
                    if (z instanceof byte[]) {
                        return com.fasterxml.jackson.core.a.a().encode((byte[]) z, false).toCharArray();
                    }
                }
                return (char[]) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
            }
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public char[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return (char[]) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class DoubleDeser extends PrimitiveArrayDeserializers<double[]> {
        private static final long serialVersionUID = 1;

        public DoubleDeser() {
            super(double[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return new DoubleDeser(this, bool);
        }

        public DoubleDeser(DoubleDeser doubleDeser, Boolean bool) {
            super(doubleDeser, bool);
        }

        @Override // com.fasterxml.jackson.databind.c
        public double[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            double _parseDoublePrimitive;
            int i;
            if (!jsonParser.K0()) {
                return handleNonArray(jsonParser, deserializationContext);
            }
            lh.d e = deserializationContext.getArrayBuilders().e();
            double[] f = e.f();
            int i2 = 0;
            while (jsonParser.T0() != JsonToken.END_ARRAY) {
                try {
                    _parseDoublePrimitive = _parseDoublePrimitive(jsonParser, deserializationContext);
                    if (i2 >= f.length) {
                        double[] c = e.c(f, i2);
                        i2 = 0;
                        f = c;
                    }
                    i = i2 + 1;
                } catch (Exception e2) {
                    e = e2;
                }
                try {
                    f[i2] = _parseDoublePrimitive;
                    i2 = i;
                } catch (Exception e3) {
                    e = e3;
                    i2 = i;
                    throw JsonMappingException.wrapWithPath(e, f, e.d() + i2);
                }
            }
            return e.e(f, i2);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public double[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return new double[]{_parseDoublePrimitive(jsonParser, deserializationContext)};
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class FloatDeser extends PrimitiveArrayDeserializers<float[]> {
        private static final long serialVersionUID = 1;

        public FloatDeser() {
            super(float[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return new FloatDeser(this, bool);
        }

        public FloatDeser(FloatDeser floatDeser, Boolean bool) {
            super(floatDeser, bool);
        }

        @Override // com.fasterxml.jackson.databind.c
        public float[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            float _parseFloatPrimitive;
            int i;
            if (!jsonParser.K0()) {
                return handleNonArray(jsonParser, deserializationContext);
            }
            lh.e f = deserializationContext.getArrayBuilders().f();
            float[] f2 = f.f();
            int i2 = 0;
            while (jsonParser.T0() != JsonToken.END_ARRAY) {
                try {
                    _parseFloatPrimitive = _parseFloatPrimitive(jsonParser, deserializationContext);
                    if (i2 >= f2.length) {
                        float[] c = f.c(f2, i2);
                        i2 = 0;
                        f2 = c;
                    }
                    i = i2 + 1;
                } catch (Exception e) {
                    e = e;
                }
                try {
                    f2[i2] = _parseFloatPrimitive;
                    i2 = i;
                } catch (Exception e2) {
                    e = e2;
                    i2 = i;
                    throw JsonMappingException.wrapWithPath(e, f2, f.d() + i2);
                }
            }
            return f.e(f2, i2);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public float[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return new float[]{_parseFloatPrimitive(jsonParser, deserializationContext)};
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class IntDeser extends PrimitiveArrayDeserializers<int[]> {
        public static final IntDeser instance = new IntDeser();
        private static final long serialVersionUID = 1;

        public IntDeser() {
            super(int[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return new IntDeser(this, bool);
        }

        public IntDeser(IntDeser intDeser, Boolean bool) {
            super(intDeser, bool);
        }

        @Override // com.fasterxml.jackson.databind.c
        public int[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            int _parseIntPrimitive;
            int i;
            if (!jsonParser.K0()) {
                return handleNonArray(jsonParser, deserializationContext);
            }
            lh.f g = deserializationContext.getArrayBuilders().g();
            int[] f = g.f();
            int i2 = 0;
            while (jsonParser.T0() != JsonToken.END_ARRAY) {
                try {
                    _parseIntPrimitive = _parseIntPrimitive(jsonParser, deserializationContext);
                    if (i2 >= f.length) {
                        int[] c = g.c(f, i2);
                        i2 = 0;
                        f = c;
                    }
                    i = i2 + 1;
                } catch (Exception e) {
                    e = e;
                }
                try {
                    f[i2] = _parseIntPrimitive;
                    i2 = i;
                } catch (Exception e2) {
                    e = e2;
                    i2 = i;
                    throw JsonMappingException.wrapWithPath(e, f, g.d() + i2);
                }
            }
            return g.e(f, i2);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public int[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return new int[]{_parseIntPrimitive(jsonParser, deserializationContext)};
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class LongDeser extends PrimitiveArrayDeserializers<long[]> {
        public static final LongDeser instance = new LongDeser();
        private static final long serialVersionUID = 1;

        public LongDeser() {
            super(long[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return new LongDeser(this, bool);
        }

        public LongDeser(LongDeser longDeser, Boolean bool) {
            super(longDeser, bool);
        }

        @Override // com.fasterxml.jackson.databind.c
        public long[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            long _parseLongPrimitive;
            int i;
            if (!jsonParser.K0()) {
                return handleNonArray(jsonParser, deserializationContext);
            }
            lh.g h = deserializationContext.getArrayBuilders().h();
            long[] f = h.f();
            int i2 = 0;
            while (jsonParser.T0() != JsonToken.END_ARRAY) {
                try {
                    _parseLongPrimitive = _parseLongPrimitive(jsonParser, deserializationContext);
                    if (i2 >= f.length) {
                        long[] c = h.c(f, i2);
                        i2 = 0;
                        f = c;
                    }
                    i = i2 + 1;
                } catch (Exception e) {
                    e = e;
                }
                try {
                    f[i2] = _parseLongPrimitive;
                    i2 = i;
                } catch (Exception e2) {
                    e = e2;
                    i2 = i;
                    throw JsonMappingException.wrapWithPath(e, f, h.d() + i2);
                }
            }
            return h.e(f, i2);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public long[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return new long[]{_parseLongPrimitive(jsonParser, deserializationContext)};
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class ShortDeser extends PrimitiveArrayDeserializers<short[]> {
        private static final long serialVersionUID = 1;

        public ShortDeser() {
            super(short[].class);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public PrimitiveArrayDeserializers<?> withResolved(Boolean bool) {
            return new ShortDeser(this, bool);
        }

        public ShortDeser(ShortDeser shortDeser, Boolean bool) {
            super(shortDeser, bool);
        }

        @Override // com.fasterxml.jackson.databind.c
        public short[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            short _parseShortPrimitive;
            int i;
            if (!jsonParser.K0()) {
                return handleNonArray(jsonParser, deserializationContext);
            }
            lh.h i2 = deserializationContext.getArrayBuilders().i();
            short[] f = i2.f();
            int i3 = 0;
            while (jsonParser.T0() != JsonToken.END_ARRAY) {
                try {
                    _parseShortPrimitive = _parseShortPrimitive(jsonParser, deserializationContext);
                    if (i3 >= f.length) {
                        short[] c = i2.c(f, i3);
                        i3 = 0;
                        f = c;
                    }
                    i = i3 + 1;
                } catch (Exception e) {
                    e = e;
                }
                try {
                    f[i3] = _parseShortPrimitive;
                    i3 = i;
                } catch (Exception e2) {
                    e = e2;
                    i3 = i;
                    throw JsonMappingException.wrapWithPath(e, f, i2.d() + i3);
                }
            }
            return i2.e(f, i3);
        }

        @Override // com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers
        public short[] handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return new short[]{_parseShortPrimitive(jsonParser, deserializationContext)};
        }
    }

    public PrimitiveArrayDeserializers(Class<T> cls) {
        super((Class<?>) cls);
        this._unwrapSingle = null;
    }

    public static c<?> forType(Class<?> cls) {
        if (cls == Integer.TYPE) {
            return IntDeser.instance;
        }
        if (cls == Long.TYPE) {
            return LongDeser.instance;
        }
        if (cls == Byte.TYPE) {
            return new ByteDeser();
        }
        if (cls == Short.TYPE) {
            return new ShortDeser();
        }
        if (cls == Float.TYPE) {
            return new FloatDeser();
        }
        if (cls == Double.TYPE) {
            return new DoubleDeser();
        }
        if (cls == Boolean.TYPE) {
            return new BooleanDeser();
        }
        if (cls == Character.TYPE) {
            return new CharDeser();
        }
        throw new IllegalStateException();
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        Boolean findFormatFeature = findFormatFeature(deserializationContext, aVar, this._valueClass, JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        return findFormatFeature == this._unwrapSingle ? this : withResolved(findFormatFeature);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return aVar.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    public T handleNonArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.F0(JsonToken.VALUE_STRING) && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && jsonParser.X().length() == 0) {
            return null;
        }
        Boolean bool = this._unwrapSingle;
        if (bool == Boolean.TRUE || (bool == null && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY))) {
            return handleSingleElementUnwrapped(jsonParser, deserializationContext);
        }
        return (T) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
    }

    public abstract T handleSingleElementUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException;

    public abstract PrimitiveArrayDeserializers<?> withResolved(Boolean bool);

    public PrimitiveArrayDeserializers(PrimitiveArrayDeserializers<?> primitiveArrayDeserializers, Boolean bool) {
        super(primitiveArrayDeserializers._valueClass);
        this._unwrapSingle = bool;
    }
}
