package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.UnresolvedForwardReference;
import com.fasterxml.jackson.databind.deser.h;
import com.fasterxml.jackson.databind.deser.i;
import com.fasterxml.jackson.databind.deser.impl.PropertyBasedCreator;
import com.fasterxml.jackson.databind.deser.impl.d;
import com.fasterxml.jackson.databind.deser.impl.e;
import com.fasterxml.jackson.databind.g;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@pt1
/* loaded from: classes.dex */
public class MapDeserializer extends ContainerDeserializerBase<Map<Object, Object>> implements com.fasterxml.jackson.databind.deser.a, h {
    private static final long serialVersionUID = 1;
    public c<Object> _delegateDeserializer;
    public final boolean _hasDefaultCreator;
    public Set<String> _ignorableProperties;
    public final g _keyDeserializer;
    public final JavaType _mapType;
    public PropertyBasedCreator _propertyBasedCreator;
    public boolean _standardStringKey;
    public final c<Object> _valueDeserializer;
    public final i _valueInstantiator;
    public final com.fasterxml.jackson.databind.jsontype.a _valueTypeDeserializer;

    /* loaded from: classes.dex */
    public static class a extends e.a {
        public final b c;
        public final Map<Object, Object> d;
        public final Object e;

        public a(b bVar, UnresolvedForwardReference unresolvedForwardReference, Class<?> cls, Object obj) {
            super(unresolvedForwardReference, cls);
            this.d = new LinkedHashMap();
            this.c = bVar;
            this.e = obj;
        }

        @Override // com.fasterxml.jackson.databind.deser.impl.e.a
        public void c(Object obj, Object obj2) throws IOException {
            this.c.c(obj, obj2);
        }
    }

    /* loaded from: classes.dex */
    public static final class b {
        public final Class<?> a;
        public Map<Object, Object> b;
        public List<a> c = new ArrayList();

        public b(Class<?> cls, Map<Object, Object> map) {
            this.a = cls;
            this.b = map;
        }

        public e.a a(UnresolvedForwardReference unresolvedForwardReference, Object obj) {
            a aVar = new a(this, unresolvedForwardReference, this.a, obj);
            this.c.add(aVar);
            return aVar;
        }

        public void b(Object obj, Object obj2) {
            if (this.c.isEmpty()) {
                this.b.put(obj, obj2);
                return;
            }
            List<a> list = this.c;
            list.get(list.size() - 1).d.put(obj, obj2);
        }

        public void c(Object obj, Object obj2) throws IOException {
            Iterator<a> it = this.c.iterator();
            Map<Object, Object> map = this.b;
            while (it.hasNext()) {
                a next = it.next();
                if (next.d(obj)) {
                    it.remove();
                    map.put(next.e, obj2);
                    map.putAll(next.d);
                    return;
                }
                map = next.d;
            }
            throw new IllegalArgumentException("Trying to resolve a forward reference with id [" + obj + "] that wasn't previously seen as unresolved.");
        }
    }

    public MapDeserializer(JavaType javaType, i iVar, g gVar, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar) {
        super(javaType);
        this._mapType = javaType;
        this._keyDeserializer = gVar;
        this._valueDeserializer = cVar;
        this._valueTypeDeserializer = aVar;
        this._valueInstantiator = iVar;
        this._hasDefaultCreator = iVar.canCreateUsingDefault();
        this._delegateDeserializer = null;
        this._propertyBasedCreator = null;
        this._standardStringKey = _isStdKeyDeser(javaType, gVar);
    }

    public Map<Object, Object> _deserializeUsingCreator(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String r;
        Object deserializeWithType;
        PropertyBasedCreator propertyBasedCreator = this._propertyBasedCreator;
        d f = propertyBasedCreator.f(jsonParser, deserializationContext, null);
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        if (jsonParser.L0()) {
            r = jsonParser.O0();
        } else {
            r = jsonParser.F0(JsonToken.FIELD_NAME) ? jsonParser.r() : null;
        }
        while (r != null) {
            JsonToken T0 = jsonParser.T0();
            Set<String> set = this._ignorableProperties;
            if (set != null && set.contains(r)) {
                jsonParser.k1();
            } else {
                SettableBeanProperty d = propertyBasedCreator.d(r);
                if (d != null) {
                    if (f.b(d, d.deserialize(jsonParser, deserializationContext))) {
                        jsonParser.T0();
                        try {
                            Map<Object, Object> map = (Map) propertyBasedCreator.a(deserializationContext, f);
                            _readAndBind(jsonParser, deserializationContext, map);
                            return map;
                        } catch (Exception e) {
                            wrapAndThrow(e, this._mapType.getRawClass(), r);
                            return null;
                        }
                    }
                } else {
                    Object deserializeKey = this._keyDeserializer.deserializeKey(r, deserializationContext);
                    try {
                        if (T0 == JsonToken.VALUE_NULL) {
                            deserializeWithType = cVar.getNullValue(deserializationContext);
                        } else if (aVar == null) {
                            deserializeWithType = cVar.deserialize(jsonParser, deserializationContext);
                        } else {
                            deserializeWithType = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
                        }
                        f.d(deserializeKey, deserializeWithType);
                    } catch (Exception e2) {
                        wrapAndThrow(e2, this._mapType.getRawClass(), r);
                        return null;
                    }
                }
            }
            r = jsonParser.O0();
        }
        try {
            return (Map) propertyBasedCreator.a(deserializationContext, f);
        } catch (Exception e3) {
            wrapAndThrow(e3, this._mapType.getRawClass(), r);
            return null;
        }
    }

    public final boolean _isStdKeyDeser(JavaType javaType, g gVar) {
        JavaType keyType;
        if (gVar == null || (keyType = javaType.getKeyType()) == null) {
            return true;
        }
        Class<?> rawClass = keyType.getRawClass();
        return (rawClass == String.class || rawClass == Object.class) && isDefaultKeyDeserializer(gVar);
    }

    public final void _readAndBind(JsonParser jsonParser, DeserializationContext deserializationContext, Map<Object, Object> map) throws IOException {
        String r;
        Object deserializeWithType;
        g gVar = this._keyDeserializer;
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        boolean z = cVar.getObjectIdReader() != null;
        b bVar = z ? new b(this._mapType.getContentType().getRawClass(), map) : null;
        if (jsonParser.L0()) {
            r = jsonParser.O0();
        } else {
            JsonToken u = jsonParser.u();
            if (u == JsonToken.END_OBJECT) {
                return;
            }
            JsonToken jsonToken = JsonToken.FIELD_NAME;
            if (u != jsonToken) {
                deserializationContext.reportWrongTokenException(jsonParser, jsonToken, null, new Object[0]);
            }
            r = jsonParser.r();
        }
        while (r != null) {
            Object deserializeKey = gVar.deserializeKey(r, deserializationContext);
            JsonToken T0 = jsonParser.T0();
            Set<String> set = this._ignorableProperties;
            if (set != null && set.contains(r)) {
                jsonParser.k1();
            } else {
                try {
                    if (T0 == JsonToken.VALUE_NULL) {
                        deserializeWithType = cVar.getNullValue(deserializationContext);
                    } else if (aVar == null) {
                        deserializeWithType = cVar.deserialize(jsonParser, deserializationContext);
                    } else {
                        deserializeWithType = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
                    }
                    if (z) {
                        bVar.b(deserializeKey, deserializeWithType);
                    } else {
                        map.put(deserializeKey, deserializeWithType);
                    }
                } catch (UnresolvedForwardReference e) {
                    a(jsonParser, bVar, deserializeKey, e);
                } catch (Exception e2) {
                    wrapAndThrow(e2, map, r);
                }
            }
            r = jsonParser.O0();
        }
    }

    public final void _readAndBindStringKeyMap(JsonParser jsonParser, DeserializationContext deserializationContext, Map<Object, Object> map) throws IOException {
        String r;
        Object deserializeWithType;
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        boolean z = cVar.getObjectIdReader() != null;
        b bVar = z ? new b(this._mapType.getContentType().getRawClass(), map) : null;
        if (jsonParser.L0()) {
            r = jsonParser.O0();
        } else {
            JsonToken u = jsonParser.u();
            if (u == JsonToken.END_OBJECT) {
                return;
            }
            JsonToken jsonToken = JsonToken.FIELD_NAME;
            if (u != jsonToken) {
                deserializationContext.reportWrongTokenException(jsonParser, jsonToken, null, new Object[0]);
            }
            r = jsonParser.r();
        }
        while (r != null) {
            JsonToken T0 = jsonParser.T0();
            Set<String> set = this._ignorableProperties;
            if (set != null && set.contains(r)) {
                jsonParser.k1();
            } else {
                try {
                    if (T0 == JsonToken.VALUE_NULL) {
                        deserializeWithType = cVar.getNullValue(deserializationContext);
                    } else if (aVar == null) {
                        deserializeWithType = cVar.deserialize(jsonParser, deserializationContext);
                    } else {
                        deserializeWithType = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
                    }
                    if (z) {
                        bVar.b(r, deserializeWithType);
                    } else {
                        map.put(r, deserializeWithType);
                    }
                } catch (UnresolvedForwardReference e) {
                    a(jsonParser, bVar, r, e);
                } catch (Exception e2) {
                    wrapAndThrow(e2, map, r);
                }
            }
            r = jsonParser.O0();
        }
    }

    public final void a(JsonParser jsonParser, b bVar, Object obj, UnresolvedForwardReference unresolvedForwardReference) throws JsonMappingException {
        if (bVar != null) {
            unresolvedForwardReference.getRoid().a(bVar.a(unresolvedForwardReference, obj));
            return;
        }
        throw JsonMappingException.from(jsonParser, "Unresolved forward reference but no identity info.", unresolvedForwardReference);
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        AnnotatedMember member;
        JsonIgnoreProperties.Value findPropertyIgnorals;
        g gVar = this._keyDeserializer;
        if (gVar == null) {
            gVar = deserializationContext.findKeyDeserializer(this._mapType.getKeyType(), aVar);
        } else if (gVar instanceof com.fasterxml.jackson.databind.deser.b) {
            gVar = ((com.fasterxml.jackson.databind.deser.b) gVar).createContextual(deserializationContext, aVar);
        }
        c<?> cVar = this._valueDeserializer;
        if (aVar != null) {
            cVar = findConvertingContentDeserializer(deserializationContext, aVar, cVar);
        }
        JavaType contentType = this._mapType.getContentType();
        if (cVar == null) {
            handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(contentType, aVar);
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(cVar, aVar, contentType);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = this._valueTypeDeserializer;
        if (aVar2 != null) {
            aVar2 = aVar2.forProperty(aVar);
        }
        Set<String> set = this._ignorableProperties;
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        if (annotationIntrospector != null && aVar != null && (member = aVar.getMember()) != null && (findPropertyIgnorals = annotationIntrospector.findPropertyIgnorals(member)) != null) {
            Set<String> findIgnoredForDeserialization = findPropertyIgnorals.findIgnoredForDeserialization();
            if (!findIgnoredForDeserialization.isEmpty()) {
                set = set == null ? new HashSet() : new HashSet(set);
                for (String str : findIgnoredForDeserialization) {
                    set.add(str);
                }
            }
        }
        return withResolved(gVar, aVar2, handleSecondaryContextualization, set);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException, JsonProcessingException {
        return aVar.deserializeTypedFromObject(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public c<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public JavaType getContentType() {
        return this._mapType.getContentType();
    }

    public final Class<?> getMapClass() {
        return this._mapType.getRawClass();
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer
    public JavaType getValueType() {
        return this._mapType;
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return this._valueDeserializer == null && this._keyDeserializer == null && this._valueTypeDeserializer == null && this._ignorableProperties == null;
    }

    @Override // com.fasterxml.jackson.databind.deser.h
    public void resolve(DeserializationContext deserializationContext) throws JsonMappingException {
        i iVar = this._valueInstantiator;
        if (iVar != null) {
            if (iVar.canCreateUsingDelegate()) {
                JavaType delegateType = this._valueInstantiator.getDelegateType(deserializationContext.getConfig());
                if (delegateType != null) {
                    this._delegateDeserializer = findDeserializer(deserializationContext, delegateType, null);
                } else {
                    throw new IllegalArgumentException("Invalid delegate-creator definition for " + this._mapType + ": value instantiator (" + this._valueInstantiator.getClass().getName() + ") returned true for 'canCreateUsingDelegate()', but null for 'getDelegateType()'");
                }
            } else if (this._valueInstantiator.canCreateUsingArrayDelegate()) {
                JavaType arrayDelegateType = this._valueInstantiator.getArrayDelegateType(deserializationContext.getConfig());
                if (arrayDelegateType != null) {
                    this._delegateDeserializer = findDeserializer(deserializationContext, arrayDelegateType, null);
                } else {
                    throw new IllegalArgumentException("Invalid delegate-creator definition for " + this._mapType + ": value instantiator (" + this._valueInstantiator.getClass().getName() + ") returned true for 'canCreateUsingDelegate()', but null for 'getArrayDelegateType()'");
                }
            }
        }
        if (this._valueInstantiator.canCreateFromObjectWith()) {
            this._propertyBasedCreator = PropertyBasedCreator.b(deserializationContext, this._valueInstantiator, this._valueInstantiator.getFromObjectArguments(deserializationContext.getConfig()));
        }
        this._standardStringKey = _isStdKeyDeser(this._mapType, this._keyDeserializer);
    }

    public void setIgnorableProperties(String[] strArr) {
        this._ignorableProperties = (strArr == null || strArr.length == 0) ? null : lh.a(strArr);
    }

    public MapDeserializer withResolved(g gVar, com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar, Set<String> set) {
        return (this._keyDeserializer == gVar && this._valueDeserializer == cVar && this._valueTypeDeserializer == aVar && this._ignorableProperties == set) ? this : new MapDeserializer(this, gVar, cVar, aVar, set);
    }

    @Deprecated
    public void wrapAndThrow(Throwable th, Object obj) throws IOException {
        wrapAndThrow(th, obj, null);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Map<Object, Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (this._propertyBasedCreator != null) {
            return _deserializeUsingCreator(jsonParser, deserializationContext);
        }
        c<Object> cVar = this._delegateDeserializer;
        if (cVar != null) {
            return (Map) this._valueInstantiator.createUsingDelegate(deserializationContext, cVar.deserialize(jsonParser, deserializationContext));
        }
        if (!this._hasDefaultCreator) {
            return (Map) deserializationContext.handleMissingInstantiator(getMapClass(), jsonParser, "no default constructor found", new Object[0]);
        }
        JsonToken u = jsonParser.u();
        if (u != JsonToken.START_OBJECT && u != JsonToken.FIELD_NAME && u != JsonToken.END_OBJECT) {
            if (u == JsonToken.VALUE_STRING) {
                return (Map) this._valueInstantiator.createFromString(deserializationContext, jsonParser.X());
            }
            return _deserializeFromEmpty(jsonParser, deserializationContext);
        }
        Map<Object, Object> map = (Map) this._valueInstantiator.createUsingDefault(deserializationContext);
        if (this._standardStringKey) {
            _readAndBindStringKeyMap(jsonParser, deserializationContext, map);
            return map;
        }
        _readAndBind(jsonParser, deserializationContext, map);
        return map;
    }

    public void setIgnorableProperties(Set<String> set) {
        this._ignorableProperties = (set == null || set.size() == 0) ? null : null;
    }

    public MapDeserializer(MapDeserializer mapDeserializer) {
        super(mapDeserializer._mapType);
        this._mapType = mapDeserializer._mapType;
        this._keyDeserializer = mapDeserializer._keyDeserializer;
        this._valueDeserializer = mapDeserializer._valueDeserializer;
        this._valueTypeDeserializer = mapDeserializer._valueTypeDeserializer;
        this._valueInstantiator = mapDeserializer._valueInstantiator;
        this._propertyBasedCreator = mapDeserializer._propertyBasedCreator;
        this._delegateDeserializer = mapDeserializer._delegateDeserializer;
        this._hasDefaultCreator = mapDeserializer._hasDefaultCreator;
        this._ignorableProperties = mapDeserializer._ignorableProperties;
        this._standardStringKey = mapDeserializer._standardStringKey;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Map<Object, Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Map<Object, Object> map) throws IOException {
        jsonParser.e1(map);
        JsonToken u = jsonParser.u();
        if (u != JsonToken.START_OBJECT && u != JsonToken.FIELD_NAME) {
            return (Map) deserializationContext.handleUnexpectedToken(getMapClass(), jsonParser);
        }
        if (this._standardStringKey) {
            _readAndBindStringKeyMap(jsonParser, deserializationContext, map);
            return map;
        }
        _readAndBind(jsonParser, deserializationContext, map);
        return map;
    }

    public MapDeserializer(MapDeserializer mapDeserializer, g gVar, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar, Set<String> set) {
        super(mapDeserializer._mapType);
        JavaType javaType = mapDeserializer._mapType;
        this._mapType = javaType;
        this._keyDeserializer = gVar;
        this._valueDeserializer = cVar;
        this._valueTypeDeserializer = aVar;
        this._valueInstantiator = mapDeserializer._valueInstantiator;
        this._propertyBasedCreator = mapDeserializer._propertyBasedCreator;
        this._delegateDeserializer = mapDeserializer._delegateDeserializer;
        this._hasDefaultCreator = mapDeserializer._hasDefaultCreator;
        this._ignorableProperties = set;
        this._standardStringKey = _isStdKeyDeser(javaType, gVar);
    }
}
