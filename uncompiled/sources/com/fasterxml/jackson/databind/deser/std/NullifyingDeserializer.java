package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import java.io.IOException;

/* loaded from: classes.dex */
public class NullifyingDeserializer extends StdDeserializer<Object> {
    public static final NullifyingDeserializer instance = new NullifyingDeserializer();
    private static final long serialVersionUID = 1;

    public NullifyingDeserializer() {
        super(Object.class);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (!jsonParser.F0(JsonToken.FIELD_NAME)) {
            jsonParser.k1();
            return null;
        }
        while (true) {
            JsonToken T0 = jsonParser.T0();
            if (T0 == null || T0 == JsonToken.END_OBJECT) {
                return null;
            }
            jsonParser.k1();
        }
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        int v = jsonParser.v();
        if (v == 1 || v == 3 || v == 5) {
            return aVar.deserializeTypedFromAny(jsonParser, deserializationContext);
        }
        return null;
    }
}
