package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.i;
import com.fasterxml.jackson.databind.deser.impl.PropertyBasedCreator;
import com.fasterxml.jackson.databind.deser.impl.d;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/* loaded from: classes.dex */
public class FactoryBasedEnumDeserializer extends StdDeserializer<Object> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = 1;
    public final SettableBeanProperty[] _creatorProps;
    public final c<?> _deser;
    public final AnnotatedMethod _factory;
    public final boolean _hasArgs;
    public final JavaType _inputType;
    public final i _valueInstantiator;
    public transient PropertyBasedCreator a;

    public FactoryBasedEnumDeserializer(Class<?> cls, AnnotatedMethod annotatedMethod, JavaType javaType, i iVar, SettableBeanProperty[] settableBeanPropertyArr) {
        super(cls);
        this._factory = annotatedMethod;
        this._hasArgs = true;
        this._inputType = javaType.hasRawClass(String.class) ? null : javaType;
        this._deser = null;
        this._valueInstantiator = iVar;
        this._creatorProps = settableBeanPropertyArr;
    }

    public final Object _deserializeWithErrorWrapping(JsonParser jsonParser, DeserializationContext deserializationContext, SettableBeanProperty settableBeanProperty) throws IOException {
        try {
            return settableBeanProperty.deserialize(jsonParser, deserializationContext);
        } catch (Exception e) {
            wrapAndThrow(e, this._valueClass.getClass(), settableBeanProperty.getName(), deserializationContext);
            return null;
        }
    }

    public final Throwable a(Throwable th, DeserializationContext deserializationContext) throws IOException {
        while ((th instanceof InvocationTargetException) && th.getCause() != null) {
            th = th.getCause();
        }
        if (!(th instanceof Error)) {
            boolean z = deserializationContext == null || deserializationContext.isEnabled(DeserializationFeature.WRAP_EXCEPTIONS);
            if (th instanceof IOException) {
                if (!z || !(th instanceof JsonProcessingException)) {
                    throw ((IOException) th);
                }
            } else if (!z && (th instanceof RuntimeException)) {
                throw ((RuntimeException) th);
            }
            return th;
        }
        throw ((Error) th);
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        JavaType javaType;
        return (this._deser == null && (javaType = this._inputType) != null && this._creatorProps == null) ? new FactoryBasedEnumDeserializer(this, deserializationContext.findContextualValueDeserializer(javaType, aVar)) : this;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object X;
        c<?> cVar = this._deser;
        if (cVar != null) {
            X = cVar.deserialize(jsonParser, deserializationContext);
        } else if (this._hasArgs) {
            JsonToken u = jsonParser.u();
            if (u != JsonToken.VALUE_STRING && u != JsonToken.FIELD_NAME) {
                if (this._creatorProps != null && jsonParser.L0()) {
                    if (this.a == null) {
                        this.a = PropertyBasedCreator.b(deserializationContext, this._valueInstantiator, this._creatorProps);
                    }
                    jsonParser.T0();
                    return deserializeEnumUsingPropertyBased(jsonParser, deserializationContext, this.a);
                }
                X = jsonParser.x0();
            } else {
                X = jsonParser.X();
            }
        } else {
            jsonParser.k1();
            try {
                return this._factory.call();
            } catch (Exception e) {
                return deserializationContext.handleInstantiationProblem(this._valueClass, null, com.fasterxml.jackson.databind.util.c.R(e));
            }
        }
        try {
            return this._factory.callOnWith(this._valueClass, X);
        } catch (Exception e2) {
            Throwable R = com.fasterxml.jackson.databind.util.c.R(e2);
            if (deserializationContext.isEnabled(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL) && (R instanceof IllegalArgumentException)) {
                return null;
            }
            return deserializationContext.handleInstantiationProblem(this._valueClass, X, R);
        }
    }

    public Object deserializeEnumUsingPropertyBased(JsonParser jsonParser, DeserializationContext deserializationContext, PropertyBasedCreator propertyBasedCreator) throws IOException {
        d f = propertyBasedCreator.f(jsonParser, deserializationContext, null);
        JsonToken u = jsonParser.u();
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty d = propertyBasedCreator.d(r);
            if (d != null) {
                f.b(d, _deserializeWithErrorWrapping(jsonParser, deserializationContext, d));
            } else {
                f.i(r);
            }
            u = jsonParser.T0();
        }
        return propertyBasedCreator.a(deserializationContext, f);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        if (this._deser == null) {
            return deserialize(jsonParser, deserializationContext);
        }
        return aVar.deserializeTypedFromAny(jsonParser, deserializationContext);
    }

    public void wrapAndThrow(Throwable th, Object obj, String str, DeserializationContext deserializationContext) throws IOException {
        throw JsonMappingException.wrapWithPath(a(th, deserializationContext), obj, str);
    }

    public FactoryBasedEnumDeserializer(Class<?> cls, AnnotatedMethod annotatedMethod) {
        super(cls);
        this._factory = annotatedMethod;
        this._hasArgs = false;
        this._inputType = null;
        this._deser = null;
        this._valueInstantiator = null;
        this._creatorProps = null;
    }

    public FactoryBasedEnumDeserializer(FactoryBasedEnumDeserializer factoryBasedEnumDeserializer, c<?> cVar) {
        super(factoryBasedEnumDeserializer._valueClass);
        this._inputType = factoryBasedEnumDeserializer._inputType;
        this._factory = factoryBasedEnumDeserializer._factory;
        this._hasArgs = factoryBasedEnumDeserializer._hasArgs;
        this._valueInstantiator = factoryBasedEnumDeserializer._valueInstantiator;
        this._creatorProps = factoryBasedEnumDeserializer._creatorProps;
        this._deser = cVar;
    }
}
