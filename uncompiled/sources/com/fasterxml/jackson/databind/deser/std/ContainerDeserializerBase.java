package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import org.web3j.utils.b;

/* loaded from: classes.dex */
public abstract class ContainerDeserializerBase<T> extends StdDeserializer<T> {
    public ContainerDeserializerBase(JavaType javaType) {
        super(javaType);
    }

    @Override // com.fasterxml.jackson.databind.c
    public SettableBeanProperty findBackReference(String str) {
        c<Object> contentDeserializer = getContentDeserializer();
        if (contentDeserializer != null) {
            return contentDeserializer.findBackReference(str);
        }
        throw new IllegalArgumentException("Can not handle managed/back reference '" + str + "': type: container deserializer of type " + getClass().getName() + " returned null for 'getContentDeserializer()'");
    }

    public abstract c<Object> getContentDeserializer();

    public abstract JavaType getContentType();

    public void wrapAndThrow(Throwable th, Object obj, String str) throws IOException {
        while ((th instanceof InvocationTargetException) && th.getCause() != null) {
            th = th.getCause();
        }
        if (!(th instanceof Error)) {
            if ((th instanceof IOException) && !(th instanceof JsonMappingException)) {
                throw ((IOException) th);
            }
            if (str == null) {
                str = b.MISSING_REASON;
            }
            throw JsonMappingException.wrapWithPath(th, obj, str);
        }
        throw ((Error) th);
    }
}
