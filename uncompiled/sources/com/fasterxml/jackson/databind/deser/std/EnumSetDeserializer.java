package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import java.io.IOException;
import java.util.EnumSet;

/* loaded from: classes.dex */
public class EnumSetDeserializer extends StdDeserializer<EnumSet<?>> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = 1;
    public final Class<Enum> _enumClass;
    public c<Enum<?>> _enumDeserializer;
    public final JavaType _enumType;
    public final Boolean _unwrapSingle;

    /* JADX WARN: Multi-variable type inference failed */
    public EnumSetDeserializer(JavaType javaType, c<?> cVar) {
        super(EnumSet.class);
        this._enumType = javaType;
        Class rawClass = javaType.getRawClass();
        this._enumClass = rawClass;
        if (rawClass.isEnum()) {
            this._enumDeserializer = cVar;
            this._unwrapSingle = null;
            return;
        }
        throw new IllegalArgumentException("Type " + javaType + " not Java Enum type");
    }

    public final EnumSet a() {
        return EnumSet.noneOf(this._enumClass);
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        Boolean findFormatFeature = findFormatFeature(deserializationContext, aVar, EnumSet.class, JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        c<Enum<?>> cVar = this._enumDeserializer;
        if (cVar == null) {
            handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(this._enumType, aVar);
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(cVar, aVar, this._enumType);
        }
        return withResolved(handleSecondaryContextualization, findFormatFeature);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException, JsonProcessingException {
        return aVar.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    public EnumSet<?> handleNonArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Boolean bool = this._unwrapSingle;
        if (!(bool == Boolean.TRUE || (bool == null && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)))) {
            return (EnumSet) deserializationContext.handleUnexpectedToken(EnumSet.class, jsonParser);
        }
        EnumSet<?> a = a();
        if (jsonParser.F0(JsonToken.VALUE_NULL)) {
            return (EnumSet) deserializationContext.handleUnexpectedToken(this._enumClass, jsonParser);
        }
        try {
            Enum<?> deserialize = this._enumDeserializer.deserialize(jsonParser, deserializationContext);
            if (deserialize != null) {
                a.add(deserialize);
            }
            return a;
        } catch (Exception e) {
            throw JsonMappingException.wrapWithPath(e, a, a.size());
        }
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return this._enumType.getValueHandler() == null;
    }

    public EnumSetDeserializer withDeserializer(c<?> cVar) {
        return this._enumDeserializer == cVar ? this : new EnumSetDeserializer(this, cVar, this._unwrapSingle);
    }

    public EnumSetDeserializer withResolved(c<?> cVar, Boolean bool) {
        return (this._unwrapSingle == bool && this._enumDeserializer == cVar) ? this : new EnumSetDeserializer(this, cVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.c
    public EnumSet<?> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (!jsonParser.K0()) {
            return handleNonArray(jsonParser, deserializationContext);
        }
        EnumSet<?> a = a();
        while (true) {
            try {
                JsonToken T0 = jsonParser.T0();
                if (T0 == JsonToken.END_ARRAY) {
                    return a;
                }
                if (T0 == JsonToken.VALUE_NULL) {
                    return (EnumSet) deserializationContext.handleUnexpectedToken(this._enumClass, jsonParser);
                }
                Enum<?> deserialize = this._enumDeserializer.deserialize(jsonParser, deserializationContext);
                if (deserialize != null) {
                    a.add(deserialize);
                }
            } catch (Exception e) {
                throw JsonMappingException.wrapWithPath(e, a, a.size());
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public EnumSetDeserializer(EnumSetDeserializer enumSetDeserializer, c<?> cVar, Boolean bool) {
        super(EnumSet.class);
        this._enumType = enumSetDeserializer._enumType;
        this._enumClass = enumSetDeserializer._enumClass;
        this._enumDeserializer = cVar;
        this._unwrapSingle = bool;
    }
}
