package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.node.l;
import com.fasterxml.jackson.databind.node.m;
import java.io.IOException;

/* loaded from: classes.dex */
public class JsonNodeDeserializer extends BaseNodeDeserializer<d> {
    public static final JsonNodeDeserializer a = new JsonNodeDeserializer();

    /* loaded from: classes.dex */
    public static final class ArrayDeserializer extends BaseNodeDeserializer<com.fasterxml.jackson.databind.node.a> {
        public static final ArrayDeserializer _instance = new ArrayDeserializer();
        private static final long serialVersionUID = 1;

        public ArrayDeserializer() {
            super(com.fasterxml.jackson.databind.node.a.class);
        }

        public static ArrayDeserializer getInstance() {
            return _instance;
        }

        @Override // com.fasterxml.jackson.databind.c
        public com.fasterxml.jackson.databind.node.a deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            if (jsonParser.K0()) {
                return deserializeArray(jsonParser, deserializationContext, deserializationContext.getNodeFactory());
            }
            return (com.fasterxml.jackson.databind.node.a) deserializationContext.handleUnexpectedToken(com.fasterxml.jackson.databind.node.a.class, jsonParser);
        }
    }

    /* loaded from: classes.dex */
    public static final class ObjectDeserializer extends BaseNodeDeserializer<m> {
        public static final ObjectDeserializer _instance = new ObjectDeserializer();
        private static final long serialVersionUID = 1;

        public ObjectDeserializer() {
            super(m.class);
        }

        public static ObjectDeserializer getInstance() {
            return _instance;
        }

        @Override // com.fasterxml.jackson.databind.c
        public m deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            if (!jsonParser.L0() && !jsonParser.F0(JsonToken.FIELD_NAME)) {
                if (jsonParser.F0(JsonToken.END_OBJECT)) {
                    return deserializationContext.getNodeFactory().objectNode();
                }
                return (m) deserializationContext.handleUnexpectedToken(m.class, jsonParser);
            }
            return deserializeObject(jsonParser, deserializationContext, deserializationContext.getNodeFactory());
        }
    }

    public JsonNodeDeserializer() {
        super(d.class);
    }

    public static c<? extends d> getDeserializer(Class<?> cls) {
        if (cls == m.class) {
            return ObjectDeserializer.getInstance();
        }
        if (cls == com.fasterxml.jackson.databind.node.a.class) {
            return ArrayDeserializer.getInstance();
        }
        return a;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.BaseNodeDeserializer, com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public /* bridge */ /* synthetic */ Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return super.deserializeWithType(jsonParser, deserializationContext, aVar);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.BaseNodeDeserializer, com.fasterxml.jackson.databind.c
    public /* bridge */ /* synthetic */ boolean isCachable() {
        return super.isCachable();
    }

    @Override // com.fasterxml.jackson.databind.c
    public d deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        int v = jsonParser.v();
        if (v != 1) {
            if (v != 3) {
                return deserializeAny(jsonParser, deserializationContext, deserializationContext.getNodeFactory());
            }
            return deserializeArray(jsonParser, deserializationContext, deserializationContext.getNodeFactory());
        }
        return deserializeObject(jsonParser, deserializationContext, deserializationContext.getNodeFactory());
    }

    @Override // com.fasterxml.jackson.databind.c
    public d getNullValue(DeserializationContext deserializationContext) {
        return l.K();
    }

    @Override // com.fasterxml.jackson.databind.c
    @Deprecated
    public d getNullValue() {
        return l.K();
    }
}
