package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.i;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;

/* loaded from: classes.dex */
public class ArrayBlockingQueueDeserializer extends CollectionDeserializer {
    private static final long serialVersionUID = 1;

    public ArrayBlockingQueueDeserializer(JavaType javaType, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar, i iVar) {
        super(javaType, cVar, aVar, iVar);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.CollectionDeserializer, com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return aVar.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.CollectionDeserializer
    public /* bridge */ /* synthetic */ CollectionDeserializer withResolved(c cVar, c cVar2, com.fasterxml.jackson.databind.jsontype.a aVar, Boolean bool) {
        return withResolved((c<?>) cVar, (c<?>) cVar2, aVar, bool);
    }

    public ArrayBlockingQueueDeserializer(JavaType javaType, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar, i iVar, c<Object> cVar2, Boolean bool) {
        super(javaType, cVar, aVar, iVar, cVar2, bool);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.CollectionDeserializer
    public ArrayBlockingQueueDeserializer withResolved(c<?> cVar, c<?> cVar2, com.fasterxml.jackson.databind.jsontype.a aVar, Boolean bool) {
        return (cVar == this._delegateDeserializer && cVar2 == this._valueDeserializer && aVar == this._valueTypeDeserializer && this._unwrapSingle == bool) ? this : new ArrayBlockingQueueDeserializer(this._collectionType, cVar2, aVar, this._valueInstantiator, cVar, bool);
    }

    public ArrayBlockingQueueDeserializer(ArrayBlockingQueueDeserializer arrayBlockingQueueDeserializer) {
        super(arrayBlockingQueueDeserializer);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.CollectionDeserializer, com.fasterxml.jackson.databind.c
    public Collection<Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        c<Object> cVar = this._delegateDeserializer;
        if (cVar != null) {
            return (Collection) this._valueInstantiator.createUsingDelegate(deserializationContext, cVar.deserialize(jsonParser, deserializationContext));
        }
        if (jsonParser.u() == JsonToken.VALUE_STRING) {
            String X = jsonParser.X();
            if (X.length() == 0) {
                return (Collection) this._valueInstantiator.createFromString(deserializationContext, X);
            }
        }
        return deserialize(jsonParser, deserializationContext, (Collection<Object>) null);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.CollectionDeserializer, com.fasterxml.jackson.databind.c
    public Collection<Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<Object> collection) throws IOException {
        Object deserializeWithType;
        if (!jsonParser.K0()) {
            return handleNonArray(jsonParser, deserializationContext, new ArrayBlockingQueue(1));
        }
        ArrayList arrayList = new ArrayList();
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        while (true) {
            try {
                JsonToken T0 = jsonParser.T0();
                if (T0 == JsonToken.END_ARRAY) {
                    break;
                }
                if (T0 == JsonToken.VALUE_NULL) {
                    deserializeWithType = cVar.getNullValue(deserializationContext);
                } else if (aVar == null) {
                    deserializeWithType = cVar.deserialize(jsonParser, deserializationContext);
                } else {
                    deserializeWithType = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
                }
                arrayList.add(deserializeWithType);
            } catch (Exception e) {
                throw JsonMappingException.wrapWithPath(e, arrayList, arrayList.size());
            }
        }
        if (collection != null) {
            collection.addAll(arrayList);
            return collection;
        }
        return new ArrayBlockingQueue(arrayList.size(), false, arrayList);
    }
}
