package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.i;
import java.io.IOException;
import java.util.Collection;

@pt1
/* loaded from: classes.dex */
public final class StringCollectionDeserializer extends ContainerDeserializerBase<Collection<String>> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = 1;
    public final JavaType _collectionType;
    public final c<Object> _delegateDeserializer;
    public final Boolean _unwrapSingle;
    public final c<String> _valueDeserializer;
    public final i _valueInstantiator;

    public StringCollectionDeserializer(JavaType javaType, c<?> cVar, i iVar) {
        this(javaType, iVar, null, cVar, null);
    }

    public final Collection<String> a(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<String> collection, c<String> cVar) throws IOException {
        String deserialize;
        while (true) {
            if (jsonParser.S0() == null) {
                JsonToken u = jsonParser.u();
                if (u == JsonToken.END_ARRAY) {
                    return collection;
                }
                deserialize = u == JsonToken.VALUE_NULL ? cVar.getNullValue(deserializationContext) : cVar.deserialize(jsonParser, deserializationContext);
            } else {
                deserialize = cVar.deserialize(jsonParser, deserializationContext);
            }
            collection.add(deserialize);
        }
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        i iVar = this._valueInstantiator;
        c<?> findDeserializer = (iVar == null || iVar.getDelegateCreator() == null) ? null : findDeserializer(deserializationContext, this._valueInstantiator.getDelegateType(deserializationContext.getConfig()), aVar);
        c<String> cVar = this._valueDeserializer;
        JavaType contentType = this._collectionType.getContentType();
        if (cVar == null) {
            handleSecondaryContextualization = findConvertingContentDeserializer(deserializationContext, aVar, cVar);
            if (handleSecondaryContextualization == null) {
                handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(contentType, aVar);
            }
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(cVar, aVar, contentType);
        }
        return withResolved(findDeserializer, isDefaultDeserializer(handleSecondaryContextualization) ? null : handleSecondaryContextualization, findFormatFeature(deserializationContext, aVar, Collection.class, JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY));
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return aVar.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public c<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public JavaType getContentType() {
        return this._collectionType.getContentType();
    }

    public final Collection<String> handleNonArray(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<String> collection) throws IOException {
        String _parseString;
        Boolean bool = this._unwrapSingle;
        if (!(bool == Boolean.TRUE || (bool == null && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)))) {
            return (Collection) deserializationContext.handleUnexpectedToken(this._collectionType.getRawClass(), jsonParser);
        }
        c<String> cVar = this._valueDeserializer;
        if (jsonParser.u() == JsonToken.VALUE_NULL) {
            _parseString = cVar == null ? null : cVar.getNullValue(deserializationContext);
        } else {
            _parseString = cVar == null ? _parseString(jsonParser, deserializationContext) : cVar.deserialize(jsonParser, deserializationContext);
        }
        collection.add(_parseString);
        return collection;
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return this._valueDeserializer == null && this._delegateDeserializer == null;
    }

    public StringCollectionDeserializer withResolved(c<?> cVar, c<?> cVar2, Boolean bool) {
        return (this._unwrapSingle == bool && this._valueDeserializer == cVar2 && this._delegateDeserializer == cVar) ? this : new StringCollectionDeserializer(this._collectionType, this._valueInstantiator, cVar, cVar2, bool);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StringCollectionDeserializer(JavaType javaType, i iVar, c<?> cVar, c<?> cVar2, Boolean bool) {
        super(javaType);
        this._collectionType = javaType;
        this._valueDeserializer = cVar2;
        this._valueInstantiator = iVar;
        this._delegateDeserializer = cVar;
        this._unwrapSingle = bool;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Collection<String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        c<Object> cVar = this._delegateDeserializer;
        if (cVar != null) {
            return (Collection) this._valueInstantiator.createUsingDelegate(deserializationContext, cVar.deserialize(jsonParser, deserializationContext));
        }
        return deserialize(jsonParser, deserializationContext, (Collection) this._valueInstantiator.createUsingDefault(deserializationContext));
    }

    @Override // com.fasterxml.jackson.databind.c
    public Collection<String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<String> collection) throws IOException {
        if (!jsonParser.K0()) {
            return handleNonArray(jsonParser, deserializationContext, collection);
        }
        c<String> cVar = this._valueDeserializer;
        if (cVar != null) {
            return a(jsonParser, deserializationContext, collection, cVar);
        }
        while (true) {
            try {
                String S0 = jsonParser.S0();
                if (S0 != null) {
                    collection.add(S0);
                } else {
                    JsonToken u = jsonParser.u();
                    if (u == JsonToken.END_ARRAY) {
                        return collection;
                    }
                    if (u != JsonToken.VALUE_NULL) {
                        S0 = _parseString(jsonParser, deserializationContext);
                    }
                    collection.add(S0);
                }
            } catch (Exception e) {
                throw JsonMappingException.wrapWithPath(e, collection, collection.size());
            }
        }
    }
}
