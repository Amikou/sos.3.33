package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import java.io.IOException;

@pt1
/* loaded from: classes.dex */
public final class StringDeserializer extends StdScalarDeserializer<String> {
    public static final int FEATURES_ACCEPT_ARRAYS = DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS.getMask() | DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT.getMask();
    public static final StringDeserializer instance = new StringDeserializer();
    private static final long serialVersionUID = 1;

    public StringDeserializer() {
        super(String.class);
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return true;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer
    public String _deserializeFromArray(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u;
        if (deserializationContext.hasSomeOfFeatures(FEATURES_ACCEPT_ARRAYS)) {
            u = jsonParser.T0();
            JsonToken jsonToken = JsonToken.END_ARRAY;
            if (u == jsonToken && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)) {
                return getNullValue(deserializationContext);
            }
            if (deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                String _parseString = _parseString(jsonParser, deserializationContext);
                if (jsonParser.T0() != jsonToken) {
                    handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                }
                return _parseString;
            }
        } else {
            u = jsonParser.u();
        }
        return (String) deserializationContext.handleUnexpectedToken(this._valueClass, u, jsonParser, null, new Object[0]);
    }

    @Override // com.fasterxml.jackson.databind.c
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.F0(JsonToken.VALUE_STRING)) {
            return jsonParser.X();
        }
        JsonToken u = jsonParser.u();
        if (u == JsonToken.START_ARRAY) {
            return _deserializeFromArray(jsonParser, deserializationContext);
        }
        if (u == JsonToken.VALUE_EMBEDDED_OBJECT) {
            Object z = jsonParser.z();
            if (z == null) {
                return null;
            }
            if (z instanceof byte[]) {
                return deserializationContext.getBase64Variant().encode((byte[]) z, false);
            }
            return z.toString();
        }
        String x0 = jsonParser.x0();
        return x0 != null ? x0 : (String) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer, com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public String deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return deserialize(jsonParser, deserializationContext);
    }
}
