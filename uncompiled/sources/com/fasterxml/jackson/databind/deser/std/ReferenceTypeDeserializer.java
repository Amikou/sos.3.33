package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.i;
import java.io.IOException;

/* loaded from: classes.dex */
public abstract class ReferenceTypeDeserializer<T> extends StdDeserializer<T> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = 1;
    public final JavaType _fullType;
    public final c<?> _valueDeserializer;
    public final com.fasterxml.jackson.databind.jsontype.a _valueTypeDeserializer;

    public ReferenceTypeDeserializer(JavaType javaType, com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar) {
        super(javaType);
        this._fullType = javaType;
        this._valueDeserializer = cVar;
        this._valueTypeDeserializer = aVar;
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        c<?> cVar = this._valueDeserializer;
        if (cVar == null) {
            handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(this._fullType.getReferencedType(), aVar);
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(cVar, aVar, this._fullType.getReferencedType());
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = this._valueTypeDeserializer;
        if (aVar2 != null) {
            aVar2 = aVar2.forProperty(aVar);
        }
        return (handleSecondaryContextualization == this._valueDeserializer && aVar2 == this._valueTypeDeserializer) ? this : withResolved(aVar2, handleSecondaryContextualization);
    }

    @Override // com.fasterxml.jackson.databind.c
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object deserializeWithType;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        if (aVar == null) {
            deserializeWithType = this._valueDeserializer.deserialize(jsonParser, deserializationContext);
        } else {
            deserializeWithType = this._valueDeserializer.deserializeWithType(jsonParser, deserializationContext, aVar);
        }
        return referenceValue(deserializeWithType);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        if (jsonParser.u() == JsonToken.VALUE_NULL) {
            return getNullValue(deserializationContext);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = this._valueTypeDeserializer;
        if (aVar2 == null) {
            return deserialize(jsonParser, deserializationContext);
        }
        return referenceValue(aVar2.deserializeTypedFromAny(jsonParser, deserializationContext));
    }

    @Override // com.fasterxml.jackson.databind.c
    public abstract T getNullValue(DeserializationContext deserializationContext);

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer
    public JavaType getValueType() {
        return this._fullType;
    }

    public abstract T referenceValue(Object obj);

    public abstract ReferenceTypeDeserializer<T> withResolved(com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar);

    public ReferenceTypeDeserializer(JavaType javaType, i iVar, com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar) {
        this(javaType, aVar, cVar);
    }
}
