package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.b;
import com.fasterxml.jackson.databind.g;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;

@pt1
/* loaded from: classes.dex */
public class MapEntryDeserializer extends ContainerDeserializerBase<Map.Entry<Object, Object>> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = 1;
    public final g _keyDeserializer;
    public final JavaType _type;
    public final c<Object> _valueDeserializer;
    public final com.fasterxml.jackson.databind.jsontype.a _valueTypeDeserializer;

    public MapEntryDeserializer(JavaType javaType, g gVar, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar) {
        super(javaType);
        if (javaType.containedTypeCount() == 2) {
            this._type = javaType;
            this._keyDeserializer = gVar;
            this._valueDeserializer = cVar;
            this._valueTypeDeserializer = aVar;
            return;
        }
        throw new IllegalArgumentException("Missing generic type information for " + javaType);
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        g gVar = this._keyDeserializer;
        if (gVar == null) {
            gVar = deserializationContext.findKeyDeserializer(this._type.containedType(0), aVar);
        } else if (gVar instanceof b) {
            gVar = ((b) gVar).createContextual(deserializationContext, aVar);
        }
        c<?> findConvertingContentDeserializer = findConvertingContentDeserializer(deserializationContext, aVar, this._valueDeserializer);
        JavaType containedType = this._type.containedType(1);
        if (findConvertingContentDeserializer == null) {
            handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(containedType, aVar);
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(findConvertingContentDeserializer, aVar, containedType);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = this._valueTypeDeserializer;
        if (aVar2 != null) {
            aVar2 = aVar2.forProperty(aVar);
        }
        return withResolved(gVar, aVar2, handleSecondaryContextualization);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException, JsonProcessingException {
        return aVar.deserializeTypedFromObject(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public c<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public JavaType getContentType() {
        return this._type.containedType(1);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer
    public JavaType getValueType() {
        return this._type;
    }

    public MapEntryDeserializer withResolved(g gVar, com.fasterxml.jackson.databind.jsontype.a aVar, c<?> cVar) {
        return (this._keyDeserializer == gVar && this._valueDeserializer == cVar && this._valueTypeDeserializer == aVar) ? this : new MapEntryDeserializer(this, gVar, cVar, aVar);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Map.Entry<Object, Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object obj;
        JsonToken u = jsonParser.u();
        JsonToken jsonToken = JsonToken.START_OBJECT;
        if (u != jsonToken && u != JsonToken.FIELD_NAME && u != JsonToken.END_OBJECT) {
            return _deserializeFromEmpty(jsonParser, deserializationContext);
        }
        if (u == jsonToken) {
            u = jsonParser.T0();
        }
        if (u != JsonToken.FIELD_NAME) {
            if (u == JsonToken.END_OBJECT) {
                deserializationContext.reportMappingException("Can not deserialize a Map.Entry out of empty JSON Object", new Object[0]);
                return null;
            }
            return (Map.Entry) deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
        }
        g gVar = this._keyDeserializer;
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        String r = jsonParser.r();
        Object deserializeKey = gVar.deserializeKey(r, deserializationContext);
        try {
            if (jsonParser.T0() == JsonToken.VALUE_NULL) {
                obj = cVar.getNullValue(deserializationContext);
            } else if (aVar == null) {
                obj = cVar.deserialize(jsonParser, deserializationContext);
            } else {
                obj = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
            }
        } catch (Exception e) {
            wrapAndThrow(e, Map.Entry.class, r);
            obj = null;
        }
        JsonToken T0 = jsonParser.T0();
        if (T0 != JsonToken.END_OBJECT) {
            if (T0 == JsonToken.FIELD_NAME) {
                deserializationContext.reportMappingException("Problem binding JSON into Map.Entry: more than one entry in JSON (second field: '" + jsonParser.r() + "')", new Object[0]);
            } else {
                deserializationContext.reportMappingException("Problem binding JSON into Map.Entry: unexpected content after JSON Object entry: " + T0, new Object[0]);
            }
            return null;
        }
        return new AbstractMap.SimpleEntry(deserializeKey, obj);
    }

    public MapEntryDeserializer(MapEntryDeserializer mapEntryDeserializer) {
        super(mapEntryDeserializer._type);
        this._type = mapEntryDeserializer._type;
        this._keyDeserializer = mapEntryDeserializer._keyDeserializer;
        this._valueDeserializer = mapEntryDeserializer._valueDeserializer;
        this._valueTypeDeserializer = mapEntryDeserializer._valueTypeDeserializer;
    }

    public MapEntryDeserializer(MapEntryDeserializer mapEntryDeserializer, g gVar, c<Object> cVar, com.fasterxml.jackson.databind.jsontype.a aVar) {
        super(mapEntryDeserializer._type);
        this._type = mapEntryDeserializer._type;
        this._keyDeserializer = gVar;
        this._valueDeserializer = cVar;
        this._valueTypeDeserializer = aVar;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Map.Entry<Object, Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Map.Entry<Object, Object> entry) throws IOException {
        throw new IllegalStateException("Can not update Map.Entry values");
    }
}
