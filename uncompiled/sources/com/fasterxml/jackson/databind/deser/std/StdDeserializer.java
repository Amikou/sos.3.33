package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.io.d;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.g;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import org.web3j.abi.datatypes.Int;

/* loaded from: classes.dex */
public abstract class StdDeserializer<T> extends c<T> implements Serializable {
    public static final int F_MASK_INT_COERCIONS = DeserializationFeature.USE_BIG_INTEGER_FOR_INTS.getMask() | DeserializationFeature.USE_LONG_FOR_INTS.getMask();
    private static final long serialVersionUID = 1;
    public final Class<?> _valueClass;

    public StdDeserializer(Class<?> cls) {
        this._valueClass = cls;
    }

    public static final double parseDouble(String str) throws NumberFormatException {
        if ("2.2250738585072012e-308".equals(str)) {
            return Double.MIN_NORMAL;
        }
        return Double.parseDouble(str);
    }

    public Object _coerceIntegral(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        int deserializationFeatures = deserializationContext.getDeserializationFeatures();
        if (DeserializationFeature.USE_BIG_INTEGER_FOR_INTS.enabledIn(deserializationFeatures)) {
            return jsonParser.h();
        }
        if (DeserializationFeature.USE_LONG_FOR_INTS.enabledIn(deserializationFeatures)) {
            return Long.valueOf(jsonParser.M());
        }
        return jsonParser.h();
    }

    public T _deserializeFromEmpty(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.START_ARRAY) {
            if (deserializationContext.isEnabled(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)) {
                if (jsonParser.T0() == JsonToken.END_ARRAY) {
                    return null;
                }
                return (T) deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
            }
        } else if (u == JsonToken.VALUE_STRING && deserializationContext.isEnabled(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && jsonParser.X().trim().isEmpty()) {
            return null;
        }
        return (T) deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
    }

    public void _failDoubleToIntCoercion(JsonParser jsonParser, DeserializationContext deserializationContext, String str) throws IOException {
        deserializationContext.reportMappingException("Can not coerce a floating-point value ('%s') into %s; enable `DeserializationFeature.ACCEPT_FLOAT_AS_INT` to allow", jsonParser.x0(), str);
    }

    public boolean _hasTextualNull(String str) {
        return "null".equals(str);
    }

    public final boolean _isIntNumber(String str) {
        int length = str.length();
        if (length > 0) {
            char charAt = str.charAt(0);
            for (int i = (charAt == '-' || charAt == '+') ? 1 : 0; i < length; i++) {
                char charAt2 = str.charAt(i);
                if (charAt2 > '9' || charAt2 < '0') {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public final boolean _isNaN(String str) {
        return "NaN".equals(str);
    }

    public final boolean _isNegInf(String str) {
        return "-Infinity".equals(str) || "-INF".equals(str);
    }

    public final boolean _isPosInf(String str) {
        return "Infinity".equals(str) || "INF".equals(str);
    }

    public final Boolean _parseBoolean(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.VALUE_TRUE) {
            return Boolean.TRUE;
        }
        if (u == JsonToken.VALUE_FALSE) {
            return Boolean.FALSE;
        }
        if (u == JsonToken.VALUE_NUMBER_INT) {
            return Boolean.valueOf(_parseBooleanFromInt(jsonParser, deserializationContext));
        }
        if (u == JsonToken.VALUE_NULL) {
            return (Boolean) getNullValue(deserializationContext);
        }
        if (u == JsonToken.VALUE_STRING) {
            String trim = jsonParser.X().trim();
            if (!"true".equals(trim) && !"True".equals(trim)) {
                if (!"false".equals(trim) && !"False".equals(trim)) {
                    if (trim.length() == 0) {
                        return (Boolean) getEmptyValue(deserializationContext);
                    }
                    if (_hasTextualNull(trim)) {
                        return (Boolean) getNullValue(deserializationContext);
                    }
                    return (Boolean) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "only \"true\" or \"false\" recognized", new Object[0]);
                }
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        } else if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
            jsonParser.T0();
            Boolean _parseBoolean = _parseBoolean(jsonParser, deserializationContext);
            if (jsonParser.T0() != JsonToken.END_ARRAY) {
                handleMissingEndArrayForSingle(jsonParser, deserializationContext);
            }
            return _parseBoolean;
        } else {
            return (Boolean) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
        }
    }

    public boolean _parseBooleanFromInt(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return !"0".equals(jsonParser.X());
    }

    @Deprecated
    public boolean _parseBooleanFromOther(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return _parseBooleanFromInt(jsonParser, deserializationContext);
    }

    public final boolean _parseBooleanPrimitive(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Boolean bool;
        JsonToken u = jsonParser.u();
        if (u == JsonToken.VALUE_TRUE) {
            return true;
        }
        if (u == JsonToken.VALUE_FALSE || u == JsonToken.VALUE_NULL) {
            return false;
        }
        if (u == JsonToken.VALUE_NUMBER_INT) {
            return _parseBooleanFromInt(jsonParser, deserializationContext);
        }
        if (u == JsonToken.VALUE_STRING) {
            String trim = jsonParser.X().trim();
            if ("true".equals(trim) || "True".equals(trim)) {
                return true;
            }
            if ("false".equals(trim) || "False".equals(trim) || trim.length() == 0 || _hasTextualNull(trim) || (bool = (Boolean) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "only \"true\" or \"false\" recognized", new Object[0])) == null) {
                return false;
            }
            return bool.booleanValue();
        } else if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
            jsonParser.T0();
            boolean _parseBooleanPrimitive = _parseBooleanPrimitive(jsonParser, deserializationContext);
            if (jsonParser.T0() != JsonToken.END_ARRAY) {
                handleMissingEndArrayForSingle(jsonParser, deserializationContext);
            }
            return _parseBooleanPrimitive;
        } else {
            return ((Boolean) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser)).booleanValue();
        }
    }

    public Byte _parseByte(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.VALUE_NUMBER_INT) {
            return Byte.valueOf(jsonParser.m());
        }
        if (u == JsonToken.VALUE_STRING) {
            String trim = jsonParser.X().trim();
            if (_hasTextualNull(trim)) {
                return (Byte) getNullValue(deserializationContext);
            }
            try {
                if (trim.length() == 0) {
                    return (Byte) getEmptyValue(deserializationContext);
                }
                int j = d.j(trim);
                if (j >= -128 && j <= 255) {
                    return Byte.valueOf((byte) j);
                }
                return (Byte) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "overflow, value can not be represented as 8-bit value", new Object[0]);
            } catch (IllegalArgumentException unused) {
                return (Byte) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid Byte value", new Object[0]);
            }
        } else if (u == JsonToken.VALUE_NUMBER_FLOAT) {
            if (!deserializationContext.isEnabled(DeserializationFeature.ACCEPT_FLOAT_AS_INT)) {
                _failDoubleToIntCoercion(jsonParser, deserializationContext, "Byte");
            }
            return Byte.valueOf(jsonParser.m());
        } else if (u == JsonToken.VALUE_NULL) {
            return (Byte) getNullValue(deserializationContext);
        } else {
            if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                jsonParser.T0();
                Byte _parseByte = _parseByte(jsonParser, deserializationContext);
                if (jsonParser.T0() != JsonToken.END_ARRAY) {
                    handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                }
                return _parseByte;
            }
            return (Byte) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
        }
    }

    public Date _parseDate(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.VALUE_NUMBER_INT) {
            return new Date(jsonParser.M());
        }
        if (u == JsonToken.VALUE_NULL) {
            return (Date) getNullValue(deserializationContext);
        }
        if (u == JsonToken.VALUE_STRING) {
            return _parseDate(jsonParser.X().trim(), deserializationContext);
        }
        if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
            jsonParser.T0();
            Date _parseDate = _parseDate(jsonParser, deserializationContext);
            if (jsonParser.T0() != JsonToken.END_ARRAY) {
                handleMissingEndArrayForSingle(jsonParser, deserializationContext);
            }
            return _parseDate;
        }
        return (Date) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
    }

    public final Double _parseDouble(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u != JsonToken.VALUE_NUMBER_INT && u != JsonToken.VALUE_NUMBER_FLOAT) {
            if (u == JsonToken.VALUE_STRING) {
                String trim = jsonParser.X().trim();
                if (trim.length() == 0) {
                    return (Double) getEmptyValue(deserializationContext);
                }
                if (_hasTextualNull(trim)) {
                    return (Double) getNullValue(deserializationContext);
                }
                char charAt = trim.charAt(0);
                if (charAt != '-') {
                    if (charAt != 'I') {
                        if (charAt == 'N' && _isNaN(trim)) {
                            return Double.valueOf(Double.NaN);
                        }
                    } else if (_isPosInf(trim)) {
                        return Double.valueOf(Double.POSITIVE_INFINITY);
                    }
                } else if (_isNegInf(trim)) {
                    return Double.valueOf(Double.NEGATIVE_INFINITY);
                }
                try {
                    return Double.valueOf(parseDouble(trim));
                } catch (IllegalArgumentException unused) {
                    return (Double) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid Double value", new Object[0]);
                }
            } else if (u == JsonToken.VALUE_NULL) {
                return (Double) getNullValue(deserializationContext);
            } else {
                if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                    jsonParser.T0();
                    Double _parseDouble = _parseDouble(jsonParser, deserializationContext);
                    if (jsonParser.T0() != JsonToken.END_ARRAY) {
                        handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                    }
                    return _parseDouble;
                }
                return (Double) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
            }
        }
        return Double.valueOf(jsonParser.x());
    }

    public final double _parseDoublePrimitive(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u != JsonToken.VALUE_NUMBER_INT && u != JsonToken.VALUE_NUMBER_FLOAT) {
            if (u == JsonToken.VALUE_STRING) {
                String trim = jsonParser.X().trim();
                if (trim.length() == 0 || _hasTextualNull(trim)) {
                    return Utils.DOUBLE_EPSILON;
                }
                char charAt = trim.charAt(0);
                if (charAt != '-') {
                    if (charAt != 'I') {
                        if (charAt == 'N' && _isNaN(trim)) {
                            return Double.NaN;
                        }
                    } else if (_isPosInf(trim)) {
                        return Double.POSITIVE_INFINITY;
                    }
                } else if (_isNegInf(trim)) {
                    return Double.NEGATIVE_INFINITY;
                }
                try {
                    return parseDouble(trim);
                } catch (IllegalArgumentException unused) {
                    Number number = (Number) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid double value", new Object[0]);
                    return number == null ? Utils.DOUBLE_EPSILON : number.doubleValue();
                }
            } else if (u == JsonToken.VALUE_NULL) {
                return Utils.DOUBLE_EPSILON;
            } else {
                if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                    jsonParser.T0();
                    double _parseDoublePrimitive = _parseDoublePrimitive(jsonParser, deserializationContext);
                    if (jsonParser.T0() != JsonToken.END_ARRAY) {
                        handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                    }
                    return _parseDoublePrimitive;
                }
                return ((Number) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser)).doubleValue();
            }
        }
        return jsonParser.x();
    }

    public final Float _parseFloat(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u != JsonToken.VALUE_NUMBER_INT && u != JsonToken.VALUE_NUMBER_FLOAT) {
            if (u == JsonToken.VALUE_STRING) {
                String trim = jsonParser.X().trim();
                if (trim.length() == 0) {
                    return (Float) getEmptyValue(deserializationContext);
                }
                if (_hasTextualNull(trim)) {
                    return (Float) getNullValue(deserializationContext);
                }
                char charAt = trim.charAt(0);
                if (charAt != '-') {
                    if (charAt != 'I') {
                        if (charAt == 'N' && _isNaN(trim)) {
                            return Float.valueOf(Float.NaN);
                        }
                    } else if (_isPosInf(trim)) {
                        return Float.valueOf(Float.POSITIVE_INFINITY);
                    }
                } else if (_isNegInf(trim)) {
                    return Float.valueOf(Float.NEGATIVE_INFINITY);
                }
                try {
                    return Float.valueOf(Float.parseFloat(trim));
                } catch (IllegalArgumentException unused) {
                    return (Float) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid Float value", new Object[0]);
                }
            } else if (u == JsonToken.VALUE_NULL) {
                return (Float) getNullValue(deserializationContext);
            } else {
                if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                    jsonParser.T0();
                    Float _parseFloat = _parseFloat(jsonParser, deserializationContext);
                    if (jsonParser.T0() != JsonToken.END_ARRAY) {
                        handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                    }
                    return _parseFloat;
                }
                return (Float) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
            }
        }
        return Float.valueOf(jsonParser.A());
    }

    public final float _parseFloatPrimitive(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u != JsonToken.VALUE_NUMBER_INT && u != JsonToken.VALUE_NUMBER_FLOAT) {
            if (u == JsonToken.VALUE_STRING) {
                String trim = jsonParser.X().trim();
                if (trim.length() == 0 || _hasTextualNull(trim)) {
                    return Utils.FLOAT_EPSILON;
                }
                char charAt = trim.charAt(0);
                if (charAt != '-') {
                    if (charAt != 'I') {
                        if (charAt == 'N' && _isNaN(trim)) {
                            return Float.NaN;
                        }
                    } else if (_isPosInf(trim)) {
                        return Float.POSITIVE_INFINITY;
                    }
                } else if (_isNegInf(trim)) {
                    return Float.NEGATIVE_INFINITY;
                }
                try {
                    return Float.parseFloat(trim);
                } catch (IllegalArgumentException unused) {
                    Number number = (Number) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid float value", new Object[0]);
                    return number == null ? Utils.FLOAT_EPSILON : number.floatValue();
                }
            } else if (u == JsonToken.VALUE_NULL) {
                return Utils.FLOAT_EPSILON;
            } else {
                if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                    jsonParser.T0();
                    float _parseFloatPrimitive = _parseFloatPrimitive(jsonParser, deserializationContext);
                    if (jsonParser.T0() != JsonToken.END_ARRAY) {
                        handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                    }
                    return _parseFloatPrimitive;
                }
                return ((Number) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser)).floatValue();
            }
        }
        return jsonParser.A();
    }

    public final int _parseIntPrimitive(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.F0(JsonToken.VALUE_NUMBER_INT)) {
            return jsonParser.F();
        }
        JsonToken u = jsonParser.u();
        if (u == JsonToken.VALUE_STRING) {
            String trim = jsonParser.X().trim();
            if (_hasTextualNull(trim)) {
                return 0;
            }
            try {
                int length = trim.length();
                if (length <= 9) {
                    if (length == 0) {
                        return 0;
                    }
                    return d.j(trim);
                }
                long parseLong = Long.parseLong(trim);
                if (parseLong >= -2147483648L && parseLong <= 2147483647L) {
                    return (int) parseLong;
                }
                Number number = (Number) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "Overflow: numeric value (%s) out of range of int (%d -%d)", trim, Integer.MIN_VALUE, Integer.MAX_VALUE);
                if (number == null) {
                    return 0;
                }
                return number.intValue();
            } catch (IllegalArgumentException unused) {
                Number number2 = (Number) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid int value", new Object[0]);
                if (number2 == null) {
                    return 0;
                }
                return number2.intValue();
            }
        } else if (u == JsonToken.VALUE_NUMBER_FLOAT) {
            if (!deserializationContext.isEnabled(DeserializationFeature.ACCEPT_FLOAT_AS_INT)) {
                _failDoubleToIntCoercion(jsonParser, deserializationContext, Int.TYPE_NAME);
            }
            return jsonParser.i0();
        } else if (u == JsonToken.VALUE_NULL) {
            return 0;
        } else {
            if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                jsonParser.T0();
                int _parseIntPrimitive = _parseIntPrimitive(jsonParser, deserializationContext);
                if (jsonParser.T0() != JsonToken.END_ARRAY) {
                    handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                }
                return _parseIntPrimitive;
            }
            return ((Number) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser)).intValue();
        }
    }

    public final Integer _parseInteger(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        int v = jsonParser.v();
        if (v != 3) {
            if (v == 11) {
                return (Integer) getNullValue(deserializationContext);
            }
            if (v == 6) {
                String trim = jsonParser.X().trim();
                try {
                    int length = trim.length();
                    if (_hasTextualNull(trim)) {
                        return (Integer) getNullValue(deserializationContext);
                    }
                    if (length <= 9) {
                        if (length == 0) {
                            return (Integer) getEmptyValue(deserializationContext);
                        }
                        return Integer.valueOf(d.j(trim));
                    }
                    long parseLong = Long.parseLong(trim);
                    if (parseLong >= -2147483648L && parseLong <= 2147483647L) {
                        return Integer.valueOf((int) parseLong);
                    }
                    Class<?> cls = this._valueClass;
                    return (Integer) deserializationContext.handleWeirdStringValue(cls, trim, "Overflow: numeric value (" + trim + ") out of range of Integer (-2147483648 - 2147483647)", new Object[0]);
                } catch (IllegalArgumentException unused) {
                    return (Integer) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid Integer value", new Object[0]);
                }
            } else if (v == 7) {
                return Integer.valueOf(jsonParser.F());
            } else {
                if (v == 8) {
                    if (!deserializationContext.isEnabled(DeserializationFeature.ACCEPT_FLOAT_AS_INT)) {
                        _failDoubleToIntCoercion(jsonParser, deserializationContext, "Integer");
                    }
                    return Integer.valueOf(jsonParser.i0());
                }
            }
        } else if (deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
            jsonParser.T0();
            Integer _parseInteger = _parseInteger(jsonParser, deserializationContext);
            if (jsonParser.T0() != JsonToken.END_ARRAY) {
                handleMissingEndArrayForSingle(jsonParser, deserializationContext);
            }
            return _parseInteger;
        }
        return (Integer) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
    }

    public final Long _parseLong(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        int v = jsonParser.v();
        if (v != 3) {
            if (v == 11) {
                return (Long) getNullValue(deserializationContext);
            }
            if (v == 6) {
                String trim = jsonParser.X().trim();
                if (trim.length() == 0) {
                    return (Long) getEmptyValue(deserializationContext);
                }
                if (_hasTextualNull(trim)) {
                    return (Long) getNullValue(deserializationContext);
                }
                try {
                    return Long.valueOf(d.l(trim));
                } catch (IllegalArgumentException unused) {
                    return (Long) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid Long value", new Object[0]);
                }
            } else if (v == 7) {
                return Long.valueOf(jsonParser.M());
            } else {
                if (v == 8) {
                    if (!deserializationContext.isEnabled(DeserializationFeature.ACCEPT_FLOAT_AS_INT)) {
                        _failDoubleToIntCoercion(jsonParser, deserializationContext, "Long");
                    }
                    return Long.valueOf(jsonParser.r0());
                }
            }
        } else if (deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
            jsonParser.T0();
            Long _parseLong = _parseLong(jsonParser, deserializationContext);
            if (jsonParser.T0() != JsonToken.END_ARRAY) {
                handleMissingEndArrayForSingle(jsonParser, deserializationContext);
            }
            return _parseLong;
        }
        return (Long) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
    }

    public final long _parseLongPrimitive(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        int v = jsonParser.v();
        if (v != 3) {
            if (v == 11) {
                return 0L;
            }
            if (v == 6) {
                String trim = jsonParser.X().trim();
                if (trim.length() == 0 || _hasTextualNull(trim)) {
                    return 0L;
                }
                try {
                    return d.l(trim);
                } catch (IllegalArgumentException unused) {
                    Number number = (Number) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid long value", new Object[0]);
                    if (number == null) {
                        return 0L;
                    }
                    return number.longValue();
                }
            } else if (v == 7) {
                return jsonParser.M();
            } else {
                if (v == 8) {
                    if (!deserializationContext.isEnabled(DeserializationFeature.ACCEPT_FLOAT_AS_INT)) {
                        _failDoubleToIntCoercion(jsonParser, deserializationContext, "long");
                    }
                    return jsonParser.r0();
                }
            }
        } else if (deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
            jsonParser.T0();
            long _parseLongPrimitive = _parseLongPrimitive(jsonParser, deserializationContext);
            if (jsonParser.T0() != JsonToken.END_ARRAY) {
                handleMissingEndArrayForSingle(jsonParser, deserializationContext);
            }
            return _parseLongPrimitive;
        }
        return ((Number) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser)).longValue();
    }

    public Short _parseShort(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.VALUE_NUMBER_INT) {
            return Short.valueOf(jsonParser.W());
        }
        if (u == JsonToken.VALUE_STRING) {
            String trim = jsonParser.X().trim();
            try {
                if (trim.length() == 0) {
                    return (Short) getEmptyValue(deserializationContext);
                }
                if (_hasTextualNull(trim)) {
                    return (Short) getNullValue(deserializationContext);
                }
                int j = d.j(trim);
                if (j >= -32768 && j <= 32767) {
                    return Short.valueOf((short) j);
                }
                return (Short) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "overflow, value can not be represented as 16-bit value", new Object[0]);
            } catch (IllegalArgumentException unused) {
                return (Short) deserializationContext.handleWeirdStringValue(this._valueClass, trim, "not a valid Short value", new Object[0]);
            }
        } else if (u == JsonToken.VALUE_NUMBER_FLOAT) {
            if (!deserializationContext.isEnabled(DeserializationFeature.ACCEPT_FLOAT_AS_INT)) {
                _failDoubleToIntCoercion(jsonParser, deserializationContext, "Short");
            }
            return Short.valueOf(jsonParser.W());
        } else if (u == JsonToken.VALUE_NULL) {
            return (Short) getNullValue(deserializationContext);
        } else {
            if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
                jsonParser.T0();
                Short _parseShort = _parseShort(jsonParser, deserializationContext);
                if (jsonParser.T0() != JsonToken.END_ARRAY) {
                    handleMissingEndArrayForSingle(jsonParser, deserializationContext);
                }
                return _parseShort;
            }
            return (Short) deserializationContext.handleUnexpectedToken(this._valueClass, jsonParser);
        }
    }

    public final short _parseShortPrimitive(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        int _parseIntPrimitive = _parseIntPrimitive(jsonParser, deserializationContext);
        if (_parseIntPrimitive < -32768 || _parseIntPrimitive > 32767) {
            Number number = (Number) deserializationContext.handleWeirdStringValue(this._valueClass, String.valueOf(_parseIntPrimitive), "overflow, value can not be represented as 16-bit value", new Object[0]);
            if (number == null) {
                return (short) 0;
            }
            return number.shortValue();
        }
        return (short) _parseIntPrimitive;
    }

    public final String _parseString(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.VALUE_STRING) {
            return jsonParser.X();
        }
        if (u == JsonToken.START_ARRAY && deserializationContext.isEnabled(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS)) {
            jsonParser.T0();
            String _parseString = _parseString(jsonParser, deserializationContext);
            if (jsonParser.T0() != JsonToken.END_ARRAY) {
                handleMissingEndArrayForSingle(jsonParser, deserializationContext);
            }
            return _parseString;
        }
        String x0 = jsonParser.x0();
        return x0 != null ? x0 : (String) deserializationContext.handleUnexpectedToken(String.class, jsonParser);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        return aVar.deserializeTypedFromAny(jsonParser, deserializationContext);
    }

    public c<?> findConvertingContentDeserializer(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar, c<?> cVar) throws JsonMappingException {
        AnnotatedMember member;
        Object findDeserializationContentConverter;
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        if (annotationIntrospector == null || aVar == null || (member = aVar.getMember()) == null || (findDeserializationContentConverter = annotationIntrospector.findDeserializationContentConverter(member)) == null) {
            return cVar;
        }
        o80<Object, Object> converterInstance = deserializationContext.converterInstance(aVar.getMember(), findDeserializationContentConverter);
        JavaType b = converterInstance.b(deserializationContext.getTypeFactory());
        if (cVar == null) {
            cVar = deserializationContext.findContextualValueDeserializer(b, aVar);
        }
        return new StdDelegatingDeserializer(converterInstance, b, cVar);
    }

    public c<Object> findDeserializer(DeserializationContext deserializationContext, JavaType javaType, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        return deserializationContext.findContextualValueDeserializer(javaType, aVar);
    }

    public Boolean findFormatFeature(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar, Class<?> cls, JsonFormat.Feature feature) {
        JsonFormat.Value findFormatOverrides = findFormatOverrides(deserializationContext, aVar, cls);
        if (findFormatOverrides != null) {
            return findFormatOverrides.getFeature(feature);
        }
        return null;
    }

    public JsonFormat.Value findFormatOverrides(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar, Class<?> cls) {
        if (aVar != null) {
            return aVar.findPropertyFormat(deserializationContext.getConfig(), cls);
        }
        return deserializationContext.getDefaultPropertyFormat(cls);
    }

    @Deprecated
    public final Class<?> getValueClass() {
        return this._valueClass;
    }

    public JavaType getValueType() {
        return null;
    }

    public void handleMissingEndArrayForSingle(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        deserializationContext.reportWrongTokenException(jsonParser, JsonToken.END_ARRAY, "Attempted to unwrap single value array for single '%s' value but there was more than a single value in the array", handledType().getName());
    }

    public void handleUnknownProperty(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj, String str) throws IOException {
        if (obj == null) {
            obj = handledType();
        }
        if (deserializationContext.handleUnknownProperty(jsonParser, this, obj, str)) {
            return;
        }
        jsonParser.k1();
    }

    @Override // com.fasterxml.jackson.databind.c
    public Class<?> handledType() {
        return this._valueClass;
    }

    public boolean isDefaultDeserializer(c<?> cVar) {
        return com.fasterxml.jackson.databind.util.c.J(cVar);
    }

    public boolean isDefaultKeyDeserializer(g gVar) {
        return com.fasterxml.jackson.databind.util.c.J(gVar);
    }

    public StdDeserializer(JavaType javaType) {
        this._valueClass = javaType == null ? null : javaType.getRawClass();
    }

    public StdDeserializer(StdDeserializer<?> stdDeserializer) {
        this._valueClass = stdDeserializer._valueClass;
    }

    public Date _parseDate(String str, DeserializationContext deserializationContext) throws IOException {
        try {
            if (str.length() == 0) {
                return (Date) getEmptyValue(deserializationContext);
            }
            if (_hasTextualNull(str)) {
                return (Date) getNullValue(deserializationContext);
            }
            return deserializationContext.parseDate(str);
        } catch (IllegalArgumentException e) {
            return (Date) deserializationContext.handleWeirdStringValue(this._valueClass, str, "not a valid representation (error: %s)", e.getMessage());
        }
    }
}
