package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.g;
import java.io.IOException;
import java.util.EnumMap;

/* loaded from: classes.dex */
public class EnumMapDeserializer extends ContainerDeserializerBase<EnumMap<?, ?>> implements com.fasterxml.jackson.databind.deser.a {
    private static final long serialVersionUID = 1;
    public final Class<?> _enumClass;
    public g _keyDeserializer;
    public final JavaType _mapType;
    public c<Object> _valueDeserializer;
    public final com.fasterxml.jackson.databind.jsontype.a _valueTypeDeserializer;

    public EnumMapDeserializer(JavaType javaType, g gVar, c<?> cVar, com.fasterxml.jackson.databind.jsontype.a aVar) {
        super(javaType);
        this._mapType = javaType;
        this._enumClass = javaType.getKeyType().getRawClass();
        this._keyDeserializer = gVar;
        this._valueDeserializer = cVar;
        this._valueTypeDeserializer = aVar;
    }

    public EnumMap<?, ?> constructMap() {
        return new EnumMap<>(this._enumClass);
    }

    @Override // com.fasterxml.jackson.databind.deser.a
    public c<?> createContextual(DeserializationContext deserializationContext, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        c<?> handleSecondaryContextualization;
        g gVar = this._keyDeserializer;
        if (gVar == null) {
            gVar = deserializationContext.findKeyDeserializer(this._mapType.getKeyType(), aVar);
        }
        c<?> cVar = this._valueDeserializer;
        JavaType contentType = this._mapType.getContentType();
        if (cVar == null) {
            handleSecondaryContextualization = deserializationContext.findContextualValueDeserializer(contentType, aVar);
        } else {
            handleSecondaryContextualization = deserializationContext.handleSecondaryContextualization(cVar, aVar, contentType);
        }
        com.fasterxml.jackson.databind.jsontype.a aVar2 = this._valueTypeDeserializer;
        if (aVar2 != null) {
            aVar2 = aVar2.forProperty(aVar);
        }
        return withResolved(gVar, handleSecondaryContextualization, aVar2);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.StdDeserializer, com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException, JsonProcessingException {
        return aVar.deserializeTypedFromObject(jsonParser, deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public c<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    @Override // com.fasterxml.jackson.databind.deser.std.ContainerDeserializerBase
    public JavaType getContentType() {
        return this._mapType.getContentType();
    }

    @Override // com.fasterxml.jackson.databind.c
    public boolean isCachable() {
        return this._valueDeserializer == null && this._keyDeserializer == null && this._valueTypeDeserializer == null;
    }

    public EnumMapDeserializer withResolved(g gVar, c<?> cVar, com.fasterxml.jackson.databind.jsontype.a aVar) {
        return (gVar == this._keyDeserializer && cVar == this._valueDeserializer && aVar == this._valueTypeDeserializer) ? this : new EnumMapDeserializer(this._mapType, gVar, cVar, this._valueTypeDeserializer);
    }

    @Override // com.fasterxml.jackson.databind.c
    public EnumMap<?, ?> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object deserializeWithType;
        if (jsonParser.u() != JsonToken.START_OBJECT) {
            return (EnumMap) _deserializeFromEmpty(jsonParser, deserializationContext);
        }
        EnumMap<?, ?> constructMap = constructMap();
        c<Object> cVar = this._valueDeserializer;
        com.fasterxml.jackson.databind.jsontype.a aVar = this._valueTypeDeserializer;
        while (jsonParser.T0() == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            Enum r4 = (Enum) this._keyDeserializer.deserializeKey(r, deserializationContext);
            if (r4 == null) {
                if (!deserializationContext.isEnabled(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                    return (EnumMap) deserializationContext.handleWeirdStringValue(this._enumClass, r, "value not one of declared Enum instance names for %s", this._mapType.getKeyType());
                }
                jsonParser.T0();
                jsonParser.k1();
            } else {
                try {
                    if (jsonParser.T0() == JsonToken.VALUE_NULL) {
                        deserializeWithType = cVar.getNullValue(deserializationContext);
                    } else if (aVar == null) {
                        deserializeWithType = cVar.deserialize(jsonParser, deserializationContext);
                    } else {
                        deserializeWithType = cVar.deserializeWithType(jsonParser, deserializationContext, aVar);
                    }
                    constructMap.put((EnumMap<?, ?>) r4, (Enum) deserializeWithType);
                } catch (Exception e) {
                    wrapAndThrow(e, constructMap, r);
                    return null;
                }
            }
        }
        return constructMap;
    }
}
