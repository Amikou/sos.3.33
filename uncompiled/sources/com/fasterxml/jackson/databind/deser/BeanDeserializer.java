package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.impl.BeanAsArrayDeserializer;
import com.fasterxml.jackson.databind.deser.impl.BeanPropertyMap;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdReader;
import com.fasterxml.jackson.databind.deser.impl.PropertyBasedCreator;
import com.fasterxml.jackson.databind.deser.impl.e;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* loaded from: classes.dex */
public class BeanDeserializer extends BeanDeserializerBase {
    private static final long serialVersionUID = 1;
    public transient Exception _nullFromCreator;

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonToken.values().length];
            a = iArr;
            try {
                iArr[JsonToken.VALUE_STRING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_INT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[JsonToken.VALUE_TRUE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[JsonToken.VALUE_FALSE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[JsonToken.VALUE_NULL.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[JsonToken.START_ARRAY.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[JsonToken.FIELD_NAME.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[JsonToken.END_OBJECT.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    /* loaded from: classes.dex */
    public static class b extends e.a {
        public final DeserializationContext c;
        public final SettableBeanProperty d;
        public Object e;

        public b(DeserializationContext deserializationContext, UnresolvedForwardReference unresolvedForwardReference, JavaType javaType, com.fasterxml.jackson.databind.deser.impl.d dVar, SettableBeanProperty settableBeanProperty) {
            super(unresolvedForwardReference, javaType);
            this.c = deserializationContext;
            this.d = settableBeanProperty;
        }

        @Override // com.fasterxml.jackson.databind.deser.impl.e.a
        public void c(Object obj, Object obj2) throws IOException {
            if (this.e == null) {
                this.c.reportMappingException("Can not resolve ObjectId forward reference using property '%s' (of type %s): Bean not yet resolved", this.d.getName(), this.d.getDeclaringClass().getName());
            }
            this.d.set(this.e, obj2);
        }

        public void e(Object obj) {
            this.e = obj;
        }
    }

    public BeanDeserializer(to toVar, so soVar, BeanPropertyMap beanPropertyMap, Map<String, SettableBeanProperty> map, HashSet<String> hashSet, boolean z, boolean z2) {
        super(toVar, soVar, beanPropertyMap, map, hashSet, z, z2);
    }

    public Exception _creatorReturnedNullException() {
        if (this._nullFromCreator == null) {
            this._nullFromCreator = new NullPointerException("JSON Creator returned null");
        }
        return this._nullFromCreator;
    }

    public final Object _deserializeOther(JsonParser jsonParser, DeserializationContext deserializationContext, JsonToken jsonToken) throws IOException {
        switch (a.a[jsonToken.ordinal()]) {
            case 1:
                return deserializeFromString(jsonParser, deserializationContext);
            case 2:
                return deserializeFromNumber(jsonParser, deserializationContext);
            case 3:
                return deserializeFromDouble(jsonParser, deserializationContext);
            case 4:
                return deserializeFromEmbedded(jsonParser, deserializationContext);
            case 5:
            case 6:
                return deserializeFromBoolean(jsonParser, deserializationContext);
            case 7:
                return deserializeFromNull(jsonParser, deserializationContext);
            case 8:
                return deserializeFromArray(jsonParser, deserializationContext);
            case 9:
            case 10:
                if (this._vanillaProcessing) {
                    return e(jsonParser, deserializationContext, jsonToken);
                }
                if (this._objectIdReader != null) {
                    return deserializeWithObjectId(jsonParser, deserializationContext);
                }
                return deserializeFromObject(jsonParser, deserializationContext);
            default:
                return deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
        }
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public Object _deserializeUsingPropertyBased(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object obj;
        Object wrapInstantiationProblem;
        PropertyBasedCreator propertyBasedCreator = this._propertyBasedCreator;
        com.fasterxml.jackson.databind.deser.impl.d f = propertyBasedCreator.f(jsonParser, deserializationContext, this._objectIdReader);
        JsonToken u = jsonParser.u();
        ArrayList<b> arrayList = null;
        com.fasterxml.jackson.databind.util.e eVar = null;
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            if (!f.i(r)) {
                SettableBeanProperty d = propertyBasedCreator.d(r);
                if (d != null) {
                    if (f.b(d, _deserializeWithErrorWrapping(jsonParser, deserializationContext, d))) {
                        jsonParser.T0();
                        try {
                            wrapInstantiationProblem = propertyBasedCreator.a(deserializationContext, f);
                        } catch (Exception e) {
                            wrapInstantiationProblem = wrapInstantiationProblem(e, deserializationContext);
                        }
                        if (wrapInstantiationProblem == null) {
                            return deserializationContext.handleInstantiationProblem(handledType(), null, _creatorReturnedNullException());
                        }
                        jsonParser.e1(wrapInstantiationProblem);
                        if (wrapInstantiationProblem.getClass() != this._beanType.getRawClass()) {
                            return handlePolymorphic(jsonParser, deserializationContext, wrapInstantiationProblem, eVar);
                        }
                        if (eVar != null) {
                            wrapInstantiationProblem = handleUnknownProperties(deserializationContext, wrapInstantiationProblem, eVar);
                        }
                        return deserialize(jsonParser, deserializationContext, wrapInstantiationProblem);
                    }
                } else {
                    SettableBeanProperty find = this._beanProperties.find(r);
                    if (find != null) {
                        try {
                            f.e(find, _deserializeWithErrorWrapping(jsonParser, deserializationContext, find));
                        } catch (UnresolvedForwardReference e2) {
                            b d2 = d(deserializationContext, find, f, e2);
                            if (arrayList == null) {
                                arrayList = new ArrayList();
                            }
                            arrayList.add(d2);
                        }
                    } else {
                        Set<String> set = this._ignorableProps;
                        if (set != null && set.contains(r)) {
                            handleIgnoredProperty(jsonParser, deserializationContext, handledType(), r);
                        } else {
                            SettableAnyProperty settableAnyProperty = this._anySetter;
                            if (settableAnyProperty != null) {
                                try {
                                    f.c(settableAnyProperty, r, settableAnyProperty.deserialize(jsonParser, deserializationContext));
                                } catch (Exception e3) {
                                    wrapAndThrow(e3, this._beanType.getRawClass(), r, deserializationContext);
                                }
                            } else {
                                if (eVar == null) {
                                    eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
                                }
                                eVar.i0(r);
                                eVar.O1(jsonParser);
                            }
                        }
                    }
                }
            }
            u = jsonParser.T0();
        }
        try {
            obj = propertyBasedCreator.a(deserializationContext, f);
        } catch (Exception e4) {
            wrapInstantiationProblem(e4, deserializationContext);
            obj = null;
        }
        if (arrayList != null) {
            for (b bVar : arrayList) {
                bVar.e(obj);
            }
        }
        if (eVar != null) {
            if (obj.getClass() != this._beanType.getRawClass()) {
                return handlePolymorphic(null, deserializationContext, obj, eVar);
            }
            return handleUnknownProperties(deserializationContext, obj, eVar);
        }
        return obj;
    }

    public final Object _deserializeWithErrorWrapping(JsonParser jsonParser, DeserializationContext deserializationContext, SettableBeanProperty settableBeanProperty) throws IOException {
        try {
            return settableBeanProperty.deserialize(jsonParser, deserializationContext);
        } catch (Exception e) {
            wrapAndThrow(e, this._beanType.getRawClass(), settableBeanProperty.getName(), deserializationContext);
            return null;
        }
    }

    @Deprecated
    public Object _missingToken(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        throw deserializationContext.endOfInputException(handledType());
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializerBase asArrayDeserializer() {
        return new BeanAsArrayDeserializer(this, this._beanProperties.getPropertiesInInsertionOrder());
    }

    public final b d(DeserializationContext deserializationContext, SettableBeanProperty settableBeanProperty, com.fasterxml.jackson.databind.deser.impl.d dVar, UnresolvedForwardReference unresolvedForwardReference) throws JsonMappingException {
        b bVar = new b(deserializationContext, unresolvedForwardReference, settableBeanProperty.getType(), dVar, settableBeanProperty);
        unresolvedForwardReference.getRoid().a(bVar);
        return bVar;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.L0()) {
            if (this._vanillaProcessing) {
                return e(jsonParser, deserializationContext, jsonParser.T0());
            }
            jsonParser.T0();
            if (this._objectIdReader != null) {
                return deserializeWithObjectId(jsonParser, deserializationContext);
            }
            return deserializeFromObject(jsonParser, deserializationContext);
        }
        return _deserializeOther(jsonParser, deserializationContext, jsonParser.u());
    }

    public Object deserializeFromNull(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.c1()) {
            com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
            eVar.f0();
            JsonParser L1 = eVar.L1(jsonParser);
            L1.T0();
            Object e = this._vanillaProcessing ? e(L1, deserializationContext, JsonToken.END_OBJECT) : deserializeFromObject(L1, deserializationContext);
            L1.close();
            return e;
        }
        return deserializationContext.handleUnexpectedToken(handledType(), jsonParser);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public Object deserializeFromObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Class<?> activeView;
        Object R;
        ObjectIdReader objectIdReader = this._objectIdReader;
        if (objectIdReader != null && objectIdReader.maySerializeAsObject() && jsonParser.H0(5) && this._objectIdReader.isValidReferencePropertyName(jsonParser.r(), jsonParser)) {
            return deserializeFromObjectId(jsonParser, deserializationContext);
        }
        if (this._nonStandardCreation) {
            if (this._unwrappedPropertyHandler != null) {
                return deserializeWithUnwrapped(jsonParser, deserializationContext);
            }
            if (this._externalTypeIdHandler != null) {
                return deserializeWithExternalTypeId(jsonParser, deserializationContext);
            }
            Object deserializeFromObjectUsingNonDefault = deserializeFromObjectUsingNonDefault(jsonParser, deserializationContext);
            if (this._injectables != null) {
                injectValues(deserializationContext, deserializeFromObjectUsingNonDefault);
            }
            return deserializeFromObjectUsingNonDefault;
        }
        Object createUsingDefault = this._valueInstantiator.createUsingDefault(deserializationContext);
        jsonParser.e1(createUsingDefault);
        if (jsonParser.c() && (R = jsonParser.R()) != null) {
            _handleTypedObjectId(jsonParser, deserializationContext, createUsingDefault, R);
        }
        if (this._injectables != null) {
            injectValues(deserializationContext, createUsingDefault);
        }
        if (this._needViewProcesing && (activeView = deserializationContext.getActiveView()) != null) {
            return deserializeWithView(jsonParser, deserializationContext, createUsingDefault, activeView);
        }
        if (jsonParser.H0(5)) {
            String r = jsonParser.r();
            do {
                jsonParser.T0();
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    try {
                        find.deserializeAndSet(jsonParser, deserializationContext, createUsingDefault);
                    } catch (Exception e) {
                        wrapAndThrow(e, createUsingDefault, r, deserializationContext);
                    }
                } else {
                    handleUnknownVanilla(jsonParser, deserializationContext, createUsingDefault, r);
                }
                r = jsonParser.O0();
            } while (r != null);
            return createUsingDefault;
        }
        return createUsingDefault;
    }

    public Object deserializeUsingPropertyBasedWithExternalTypeId(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        com.fasterxml.jackson.databind.deser.impl.b g = this._externalTypeIdHandler.g();
        PropertyBasedCreator propertyBasedCreator = this._propertyBasedCreator;
        com.fasterxml.jackson.databind.deser.impl.d f = propertyBasedCreator.f(jsonParser, deserializationContext, this._objectIdReader);
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.i1();
        JsonToken u = jsonParser.u();
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty d = propertyBasedCreator.d(r);
            if (d != null) {
                if (!g.e(jsonParser, deserializationContext, r, null) && f.b(d, _deserializeWithErrorWrapping(jsonParser, deserializationContext, d))) {
                    JsonToken T0 = jsonParser.T0();
                    try {
                        Object a2 = propertyBasedCreator.a(deserializationContext, f);
                        while (T0 == JsonToken.FIELD_NAME) {
                            jsonParser.T0();
                            eVar.O1(jsonParser);
                            T0 = jsonParser.T0();
                        }
                        if (a2.getClass() != this._beanType.getRawClass()) {
                            deserializationContext.reportMappingException("Can not create polymorphic instances with external type ids", new Object[0]);
                            return null;
                        }
                        return g.d(jsonParser, deserializationContext, a2);
                    } catch (Exception e) {
                        wrapAndThrow(e, this._beanType.getRawClass(), r, deserializationContext);
                    }
                }
            } else if (!f.i(r)) {
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    f.e(find, find.deserialize(jsonParser, deserializationContext));
                } else if (!g.e(jsonParser, deserializationContext, r, null)) {
                    Set<String> set = this._ignorableProps;
                    if (set != null && set.contains(r)) {
                        handleIgnoredProperty(jsonParser, deserializationContext, handledType(), r);
                    } else {
                        SettableAnyProperty settableAnyProperty = this._anySetter;
                        if (settableAnyProperty != null) {
                            f.c(settableAnyProperty, r, settableAnyProperty.deserialize(jsonParser, deserializationContext));
                        }
                    }
                }
            }
            u = jsonParser.T0();
        }
        try {
            return g.c(jsonParser, deserializationContext, f, propertyBasedCreator);
        } catch (Exception e2) {
            return wrapInstantiationProblem(e2, deserializationContext);
        }
    }

    public Object deserializeUsingPropertyBasedWithUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Object wrapInstantiationProblem;
        PropertyBasedCreator propertyBasedCreator = this._propertyBasedCreator;
        com.fasterxml.jackson.databind.deser.impl.d f = propertyBasedCreator.f(jsonParser, deserializationContext, this._objectIdReader);
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.i1();
        JsonToken u = jsonParser.u();
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            jsonParser.T0();
            SettableBeanProperty d = propertyBasedCreator.d(r);
            if (d != null) {
                if (f.b(d, _deserializeWithErrorWrapping(jsonParser, deserializationContext, d))) {
                    JsonToken T0 = jsonParser.T0();
                    try {
                        wrapInstantiationProblem = propertyBasedCreator.a(deserializationContext, f);
                    } catch (Exception e) {
                        wrapInstantiationProblem = wrapInstantiationProblem(e, deserializationContext);
                    }
                    jsonParser.e1(wrapInstantiationProblem);
                    while (T0 == JsonToken.FIELD_NAME) {
                        jsonParser.T0();
                        eVar.O1(jsonParser);
                        T0 = jsonParser.T0();
                    }
                    eVar.f0();
                    if (wrapInstantiationProblem.getClass() != this._beanType.getRawClass()) {
                        eVar.close();
                        deserializationContext.reportMappingException("Can not create polymorphic instances with unwrapped values", new Object[0]);
                        return null;
                    }
                    return this._unwrappedPropertyHandler.b(jsonParser, deserializationContext, wrapInstantiationProblem, eVar);
                }
            } else if (!f.i(r)) {
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    f.e(find, _deserializeWithErrorWrapping(jsonParser, deserializationContext, find));
                } else {
                    Set<String> set = this._ignorableProps;
                    if (set != null && set.contains(r)) {
                        handleIgnoredProperty(jsonParser, deserializationContext, handledType(), r);
                    } else if (this._anySetter == null) {
                        eVar.i0(r);
                        eVar.O1(jsonParser);
                    } else {
                        com.fasterxml.jackson.databind.util.e eVar2 = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
                        eVar2.O1(jsonParser);
                        eVar.i0(r);
                        eVar.J1(eVar2);
                        try {
                            JsonParser L1 = eVar2.L1(jsonParser);
                            L1.T0();
                            SettableAnyProperty settableAnyProperty = this._anySetter;
                            f.c(settableAnyProperty, r, settableAnyProperty.deserialize(L1, deserializationContext));
                        } catch (Exception e2) {
                            wrapAndThrow(e2, this._beanType.getRawClass(), r, deserializationContext);
                        }
                    }
                }
            }
            u = jsonParser.T0();
        }
        try {
            return this._unwrappedPropertyHandler.b(jsonParser, deserializationContext, propertyBasedCreator.a(deserializationContext, f), eVar);
        } catch (Exception e3) {
            wrapInstantiationProblem(e3, deserializationContext);
            return null;
        }
    }

    public Object deserializeWithExternalTypeId(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (this._propertyBasedCreator != null) {
            return deserializeUsingPropertyBasedWithExternalTypeId(jsonParser, deserializationContext);
        }
        com.fasterxml.jackson.databind.c<Object> cVar = this._delegateDeserializer;
        if (cVar != null) {
            return this._valueInstantiator.createUsingDelegate(deserializationContext, cVar.deserialize(jsonParser, deserializationContext));
        }
        return deserializeWithExternalTypeId(jsonParser, deserializationContext, this._valueInstantiator.createUsingDefault(deserializationContext));
    }

    public Object deserializeWithUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        com.fasterxml.jackson.databind.c<Object> cVar = this._delegateDeserializer;
        if (cVar != null) {
            return this._valueInstantiator.createUsingDelegate(deserializationContext, cVar.deserialize(jsonParser, deserializationContext));
        }
        if (this._propertyBasedCreator != null) {
            return deserializeUsingPropertyBasedWithUnwrapped(jsonParser, deserializationContext);
        }
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.i1();
        Object createUsingDefault = this._valueInstantiator.createUsingDefault(deserializationContext);
        jsonParser.e1(createUsingDefault);
        if (this._injectables != null) {
            injectValues(deserializationContext, createUsingDefault);
        }
        Class<?> activeView = this._needViewProcesing ? deserializationContext.getActiveView() : null;
        String r = jsonParser.H0(5) ? jsonParser.r() : null;
        while (r != null) {
            jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                if (activeView != null && !find.visibleInView(activeView)) {
                    jsonParser.k1();
                } else {
                    try {
                        find.deserializeAndSet(jsonParser, deserializationContext, createUsingDefault);
                    } catch (Exception e) {
                        wrapAndThrow(e, createUsingDefault, r, deserializationContext);
                    }
                }
            } else {
                Set<String> set = this._ignorableProps;
                if (set != null && set.contains(r)) {
                    handleIgnoredProperty(jsonParser, deserializationContext, createUsingDefault, r);
                } else if (this._anySetter == null) {
                    eVar.i0(r);
                    eVar.O1(jsonParser);
                } else {
                    com.fasterxml.jackson.databind.util.e eVar2 = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
                    eVar2.O1(jsonParser);
                    eVar.i0(r);
                    eVar.J1(eVar2);
                    try {
                        JsonParser L1 = eVar2.L1(jsonParser);
                        L1.T0();
                        this._anySetter.deserializeAndSet(L1, deserializationContext, createUsingDefault, r);
                    } catch (Exception e2) {
                        wrapAndThrow(e2, createUsingDefault, r, deserializationContext);
                    }
                }
            }
            r = jsonParser.O0();
        }
        eVar.f0();
        this._unwrappedPropertyHandler.b(jsonParser, deserializationContext, createUsingDefault, eVar);
        return createUsingDefault;
    }

    public final Object deserializeWithView(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj, Class<?> cls) throws IOException {
        if (jsonParser.H0(5)) {
            String r = jsonParser.r();
            do {
                jsonParser.T0();
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    if (!find.visibleInView(cls)) {
                        jsonParser.k1();
                    } else {
                        try {
                            find.deserializeAndSet(jsonParser, deserializationContext, obj);
                        } catch (Exception e) {
                            wrapAndThrow(e, obj, r, deserializationContext);
                        }
                    }
                } else {
                    handleUnknownVanilla(jsonParser, deserializationContext, obj, r);
                }
                r = jsonParser.O0();
            } while (r != null);
            return obj;
        }
        return obj;
    }

    public final Object e(JsonParser jsonParser, DeserializationContext deserializationContext, JsonToken jsonToken) throws IOException {
        Object createUsingDefault = this._valueInstantiator.createUsingDefault(deserializationContext);
        jsonParser.e1(createUsingDefault);
        if (jsonParser.H0(5)) {
            String r = jsonParser.r();
            do {
                jsonParser.T0();
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    try {
                        find.deserializeAndSet(jsonParser, deserializationContext, createUsingDefault);
                    } catch (Exception e) {
                        wrapAndThrow(e, createUsingDefault, r, deserializationContext);
                    }
                } else {
                    handleUnknownVanilla(jsonParser, deserializationContext, createUsingDefault, r);
                }
                r = jsonParser.O0();
            } while (r != null);
            return createUsingDefault;
        }
        return createUsingDefault;
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase, com.fasterxml.jackson.databind.c
    public com.fasterxml.jackson.databind.c<Object> unwrappingDeserializer(NameTransformer nameTransformer) {
        return getClass() != BeanDeserializer.class ? this : new BeanDeserializer(this, nameTransformer);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializerBase withBeanProperties(BeanPropertyMap beanPropertyMap) {
        return new BeanDeserializer(this, beanPropertyMap);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public /* bridge */ /* synthetic */ BeanDeserializerBase withIgnorableProperties(Set set) {
        return withIgnorableProperties((Set<String>) set);
    }

    public BeanDeserializer(BeanDeserializerBase beanDeserializerBase) {
        super(beanDeserializerBase, beanDeserializerBase._ignoreAllUnknown);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializer withIgnorableProperties(Set<String> set) {
        return new BeanDeserializer(this, set);
    }

    @Override // com.fasterxml.jackson.databind.deser.BeanDeserializerBase
    public BeanDeserializer withObjectIdReader(ObjectIdReader objectIdReader) {
        return new BeanDeserializer(this, objectIdReader);
    }

    public BeanDeserializer(BeanDeserializerBase beanDeserializerBase, boolean z) {
        super(beanDeserializerBase, z);
    }

    public BeanDeserializer(BeanDeserializerBase beanDeserializerBase, NameTransformer nameTransformer) {
        super(beanDeserializerBase, nameTransformer);
    }

    public BeanDeserializer(BeanDeserializerBase beanDeserializerBase, ObjectIdReader objectIdReader) {
        super(beanDeserializerBase, objectIdReader);
    }

    public BeanDeserializer(BeanDeserializerBase beanDeserializerBase, Set<String> set) {
        super(beanDeserializerBase, set);
    }

    public BeanDeserializer(BeanDeserializerBase beanDeserializerBase, BeanPropertyMap beanPropertyMap) {
        super(beanDeserializerBase, beanPropertyMap);
    }

    public Object deserializeWithExternalTypeId(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        Class<?> activeView = this._needViewProcesing ? deserializationContext.getActiveView() : null;
        com.fasterxml.jackson.databind.deser.impl.b g = this._externalTypeIdHandler.g();
        JsonToken u = jsonParser.u();
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            JsonToken T0 = jsonParser.T0();
            SettableBeanProperty find = this._beanProperties.find(r);
            if (find != null) {
                if (T0.isScalarValue()) {
                    g.f(jsonParser, deserializationContext, r, obj);
                }
                if (activeView != null && !find.visibleInView(activeView)) {
                    jsonParser.k1();
                } else {
                    try {
                        find.deserializeAndSet(jsonParser, deserializationContext, obj);
                    } catch (Exception e) {
                        wrapAndThrow(e, obj, r, deserializationContext);
                    }
                }
            } else {
                Set<String> set = this._ignorableProps;
                if (set != null && set.contains(r)) {
                    handleIgnoredProperty(jsonParser, deserializationContext, obj, r);
                } else if (!g.e(jsonParser, deserializationContext, r, obj)) {
                    SettableAnyProperty settableAnyProperty = this._anySetter;
                    if (settableAnyProperty != null) {
                        try {
                            settableAnyProperty.deserializeAndSet(jsonParser, deserializationContext, obj, r);
                        } catch (Exception e2) {
                            wrapAndThrow(e2, obj, r, deserializationContext);
                        }
                    } else {
                        handleUnknownProperty(jsonParser, deserializationContext, obj, r);
                    }
                }
            }
            u = jsonParser.T0();
        }
        return g.d(jsonParser, deserializationContext, obj);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        String r;
        Class<?> activeView;
        jsonParser.e1(obj);
        if (this._injectables != null) {
            injectValues(deserializationContext, obj);
        }
        if (this._unwrappedPropertyHandler != null) {
            return deserializeWithUnwrapped(jsonParser, deserializationContext, obj);
        }
        if (this._externalTypeIdHandler != null) {
            return deserializeWithExternalTypeId(jsonParser, deserializationContext, obj);
        }
        if (jsonParser.L0()) {
            r = jsonParser.O0();
            if (r == null) {
                return obj;
            }
        } else if (!jsonParser.H0(5)) {
            return obj;
        } else {
            r = jsonParser.r();
        }
        if (!this._needViewProcesing || (activeView = deserializationContext.getActiveView()) == null) {
            do {
                jsonParser.T0();
                SettableBeanProperty find = this._beanProperties.find(r);
                if (find != null) {
                    try {
                        find.deserializeAndSet(jsonParser, deserializationContext, obj);
                    } catch (Exception e) {
                        wrapAndThrow(e, obj, r, deserializationContext);
                    }
                } else {
                    handleUnknownVanilla(jsonParser, deserializationContext, obj, r);
                }
                r = jsonParser.O0();
            } while (r != null);
            return obj;
        }
        return deserializeWithView(jsonParser, deserializationContext, obj, activeView);
    }

    public Object deserializeWithUnwrapped(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        JsonToken u = jsonParser.u();
        if (u == JsonToken.START_OBJECT) {
            u = jsonParser.T0();
        }
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.i1();
        Class<?> activeView = this._needViewProcesing ? deserializationContext.getActiveView() : null;
        while (u == JsonToken.FIELD_NAME) {
            String r = jsonParser.r();
            SettableBeanProperty find = this._beanProperties.find(r);
            jsonParser.T0();
            if (find != null) {
                if (activeView != null && !find.visibleInView(activeView)) {
                    jsonParser.k1();
                } else {
                    try {
                        find.deserializeAndSet(jsonParser, deserializationContext, obj);
                    } catch (Exception e) {
                        wrapAndThrow(e, obj, r, deserializationContext);
                    }
                }
            } else {
                Set<String> set = this._ignorableProps;
                if (set != null && set.contains(r)) {
                    handleIgnoredProperty(jsonParser, deserializationContext, obj, r);
                } else if (this._anySetter == null) {
                    eVar.i0(r);
                    eVar.O1(jsonParser);
                } else {
                    com.fasterxml.jackson.databind.util.e eVar2 = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
                    eVar2.O1(jsonParser);
                    eVar.i0(r);
                    eVar.J1(eVar2);
                    try {
                        JsonParser L1 = eVar2.L1(jsonParser);
                        L1.T0();
                        this._anySetter.deserializeAndSet(L1, deserializationContext, obj, r);
                    } catch (Exception e2) {
                        wrapAndThrow(e2, obj, r, deserializationContext);
                    }
                }
            }
            u = jsonParser.T0();
        }
        eVar.f0();
        this._unwrappedPropertyHandler.b(jsonParser, deserializationContext, obj, eVar);
        return obj;
    }
}
