package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.t;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.deser.UnresolvedForwardReference;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: ReadableObjectId.java */
/* loaded from: classes.dex */
public class e {
    public Object a;
    public final ObjectIdGenerator.IdKey b;
    public LinkedList<a> c;
    public t d;

    public e(ObjectIdGenerator.IdKey idKey) {
        this.b = idKey;
    }

    public void a(a aVar) {
        if (this.c == null) {
            this.c = new LinkedList<>();
        }
        this.c.add(aVar);
    }

    public void b(Object obj) throws IOException {
        this.d.a(this.b, obj);
        this.a = obj;
        Object obj2 = this.b.key;
        LinkedList<a> linkedList = this.c;
        if (linkedList != null) {
            Iterator<a> it = linkedList.iterator();
            this.c = null;
            while (it.hasNext()) {
                it.next().c(obj2, obj);
            }
        }
    }

    public ObjectIdGenerator.IdKey c() {
        return this.b;
    }

    public boolean d() {
        LinkedList<a> linkedList = this.c;
        return (linkedList == null || linkedList.isEmpty()) ? false : true;
    }

    public Iterator<a> e() {
        LinkedList<a> linkedList = this.c;
        if (linkedList == null) {
            return Collections.emptyList().iterator();
        }
        return linkedList.iterator();
    }

    public Object f() {
        Object d = this.d.d(this.b);
        this.a = d;
        return d;
    }

    public void g(t tVar) {
        this.d = tVar;
    }

    public boolean h(DeserializationContext deserializationContext) {
        return false;
    }

    public String toString() {
        return String.valueOf(this.b);
    }

    /* compiled from: ReadableObjectId.java */
    /* loaded from: classes.dex */
    public static abstract class a {
        public final UnresolvedForwardReference a;
        public final Class<?> b;

        public a(UnresolvedForwardReference unresolvedForwardReference, Class<?> cls) {
            this.a = unresolvedForwardReference;
            this.b = cls;
        }

        public Class<?> a() {
            return this.b;
        }

        public JsonLocation b() {
            return this.a.getLocation();
        }

        public abstract void c(Object obj, Object obj2) throws IOException;

        public boolean d(Object obj) {
            return obj.equals(this.a.getUnresolvedId());
        }

        public a(UnresolvedForwardReference unresolvedForwardReference, JavaType javaType) {
            this.a = unresolvedForwardReference;
            this.b = javaType.getRawClass();
        }
    }
}
