package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.t;
import com.fasterxml.jackson.annotation.u;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import java.io.IOException;
import java.io.Serializable;

/* loaded from: classes.dex */
public class ObjectIdReader implements Serializable {
    private static final long serialVersionUID = 1;
    public final com.fasterxml.jackson.databind.c<Object> _deserializer;
    public final JavaType _idType;
    public final ObjectIdGenerator<?> generator;
    public final SettableBeanProperty idProperty;
    public final PropertyName propertyName;
    public final t resolver;

    public ObjectIdReader(JavaType javaType, PropertyName propertyName, ObjectIdGenerator<?> objectIdGenerator, com.fasterxml.jackson.databind.c<?> cVar, SettableBeanProperty settableBeanProperty, t tVar) {
        this._idType = javaType;
        this.propertyName = propertyName;
        this.generator = objectIdGenerator;
        this.resolver = tVar;
        this._deserializer = cVar;
        this.idProperty = settableBeanProperty;
    }

    public static ObjectIdReader construct(JavaType javaType, PropertyName propertyName, ObjectIdGenerator<?> objectIdGenerator, com.fasterxml.jackson.databind.c<?> cVar, SettableBeanProperty settableBeanProperty, t tVar) {
        return new ObjectIdReader(javaType, propertyName, objectIdGenerator, cVar, settableBeanProperty, tVar);
    }

    public com.fasterxml.jackson.databind.c<Object> getDeserializer() {
        return this._deserializer;
    }

    public JavaType getIdType() {
        return this._idType;
    }

    public boolean isValidReferencePropertyName(String str, JsonParser jsonParser) {
        return this.generator.isValidReferencePropertyName(str, jsonParser);
    }

    public boolean maySerializeAsObject() {
        return this.generator.maySerializeAsObject();
    }

    public Object readObjectReference(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return this._deserializer.deserialize(jsonParser, deserializationContext);
    }

    @Deprecated
    public static ObjectIdReader construct(JavaType javaType, PropertyName propertyName, ObjectIdGenerator<?> objectIdGenerator, com.fasterxml.jackson.databind.c<?> cVar, SettableBeanProperty settableBeanProperty) {
        return construct(javaType, propertyName, objectIdGenerator, cVar, settableBeanProperty, new u());
    }

    @Deprecated
    public ObjectIdReader(JavaType javaType, PropertyName propertyName, ObjectIdGenerator<?> objectIdGenerator, com.fasterxml.jackson.databind.c<?> cVar, SettableBeanProperty settableBeanProperty) {
        this(javaType, propertyName, objectIdGenerator, cVar, settableBeanProperty, new u());
    }
}
