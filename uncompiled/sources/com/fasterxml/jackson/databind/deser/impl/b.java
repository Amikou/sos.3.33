package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ExternalTypeHandler.java */
/* loaded from: classes.dex */
public class b {
    public final C0093b[] a;
    public final HashMap<String, Integer> b;
    public final String[] c;
    public final com.fasterxml.jackson.databind.util.e[] d;

    /* compiled from: ExternalTypeHandler.java */
    /* loaded from: classes.dex */
    public static class a {
        public final ArrayList<C0093b> a = new ArrayList<>();
        public final HashMap<String, Integer> b = new HashMap<>();

        public void a(SettableBeanProperty settableBeanProperty, com.fasterxml.jackson.databind.jsontype.a aVar) {
            Integer valueOf = Integer.valueOf(this.a.size());
            this.a.add(new C0093b(settableBeanProperty, aVar));
            this.b.put(settableBeanProperty.getName(), valueOf);
            this.b.put(aVar.getPropertyName(), valueOf);
        }

        public b b(BeanPropertyMap beanPropertyMap) {
            int size = this.a.size();
            C0093b[] c0093bArr = new C0093b[size];
            for (int i = 0; i < size; i++) {
                C0093b c0093b = this.a.get(i);
                SettableBeanProperty find = beanPropertyMap.find(c0093b.d());
                if (find != null) {
                    c0093b.g(find);
                }
                c0093bArr[i] = c0093b;
            }
            return new b(c0093bArr, this.b, null, null);
        }
    }

    /* compiled from: ExternalTypeHandler.java */
    /* renamed from: com.fasterxml.jackson.databind.deser.impl.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0093b {
        public final SettableBeanProperty a;
        public final com.fasterxml.jackson.databind.jsontype.a b;
        public final String c;
        public SettableBeanProperty d;

        public C0093b(SettableBeanProperty settableBeanProperty, com.fasterxml.jackson.databind.jsontype.a aVar) {
            this.a = settableBeanProperty;
            this.b = aVar;
            this.c = aVar.getPropertyName();
        }

        public String a() {
            Class<?> defaultImpl = this.b.getDefaultImpl();
            if (defaultImpl == null) {
                return null;
            }
            return this.b.getTypeIdResolver().e(null, defaultImpl);
        }

        public SettableBeanProperty b() {
            return this.a;
        }

        public SettableBeanProperty c() {
            return this.d;
        }

        public String d() {
            return this.c;
        }

        public boolean e() {
            return this.b.getDefaultImpl() != null;
        }

        public boolean f(String str) {
            return str.equals(this.c);
        }

        public void g(SettableBeanProperty settableBeanProperty) {
            this.d = settableBeanProperty;
        }
    }

    public b(C0093b[] c0093bArr, HashMap<String, Integer> hashMap, String[] strArr, com.fasterxml.jackson.databind.util.e[] eVarArr) {
        this.a = c0093bArr;
        this.b = hashMap;
        this.c = strArr;
        this.d = eVarArr;
    }

    public final Object a(JsonParser jsonParser, DeserializationContext deserializationContext, int i, String str) throws IOException {
        JsonParser L1 = this.d[i].L1(jsonParser);
        if (L1.T0() == JsonToken.VALUE_NULL) {
            return null;
        }
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.e1();
        eVar.o1(str);
        eVar.O1(L1);
        eVar.e0();
        JsonParser L12 = eVar.L1(jsonParser);
        L12.T0();
        return this.a[i].b().deserialize(L12, deserializationContext);
    }

    public final void b(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj, int i, String str) throws IOException {
        JsonParser L1 = this.d[i].L1(jsonParser);
        if (L1.T0() == JsonToken.VALUE_NULL) {
            this.a[i].b().set(obj, null);
            return;
        }
        com.fasterxml.jackson.databind.util.e eVar = new com.fasterxml.jackson.databind.util.e(jsonParser, deserializationContext);
        eVar.e1();
        eVar.o1(str);
        eVar.O1(L1);
        eVar.e0();
        JsonParser L12 = eVar.L1(jsonParser);
        L12.T0();
        this.a[i].b().deserializeAndSet(L12, deserializationContext, obj);
    }

    public Object c(JsonParser jsonParser, DeserializationContext deserializationContext, d dVar, PropertyBasedCreator propertyBasedCreator) throws IOException {
        int length = this.a.length;
        Object[] objArr = new Object[length];
        for (int i = 0; i < length; i++) {
            String str = this.c[i];
            C0093b c0093b = this.a[i];
            if (str == null) {
                if (this.d[i] != null) {
                    if (!c0093b.e()) {
                        deserializationContext.reportMappingException("Missing external type id property '%s'", c0093b.d());
                    } else {
                        str = c0093b.a();
                    }
                }
            } else if (this.d[i] == null) {
                deserializationContext.reportMappingException("Missing property '%s' for external type id '%s'", c0093b.b().getName(), this.a[i].d());
            }
            objArr[i] = a(jsonParser, deserializationContext, i, str);
            SettableBeanProperty b = c0093b.b();
            if (b.getCreatorIndex() >= 0) {
                dVar.b(b, objArr[i]);
                SettableBeanProperty c = c0093b.c();
                if (c != null && c.getCreatorIndex() >= 0) {
                    dVar.b(c, str);
                }
            }
        }
        Object a2 = propertyBasedCreator.a(deserializationContext, dVar);
        for (int i2 = 0; i2 < length; i2++) {
            SettableBeanProperty b2 = this.a[i2].b();
            if (b2.getCreatorIndex() < 0) {
                b2.set(a2, objArr[i2]);
            }
        }
        return a2;
    }

    public Object d(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        int length = this.a.length;
        for (int i = 0; i < length; i++) {
            String str = this.c[i];
            if (str == null) {
                com.fasterxml.jackson.databind.util.e eVar = this.d[i];
                if (eVar != null) {
                    JsonToken Q1 = eVar.Q1();
                    if (Q1 != null && Q1.isScalarValue()) {
                        JsonParser L1 = eVar.L1(jsonParser);
                        L1.T0();
                        SettableBeanProperty b = this.a[i].b();
                        Object deserializeIfNatural = com.fasterxml.jackson.databind.jsontype.a.deserializeIfNatural(L1, deserializationContext, b.getType());
                        if (deserializeIfNatural != null) {
                            b.set(obj, deserializeIfNatural);
                        } else if (!this.a[i].e()) {
                            deserializationContext.reportMappingException("Missing external type id property '%s'", this.a[i].d());
                        } else {
                            str = this.a[i].a();
                        }
                    }
                }
            } else if (this.d[i] == null) {
                SettableBeanProperty b2 = this.a[i].b();
                if (b2.isRequired() || deserializationContext.isEnabled(DeserializationFeature.FAIL_ON_MISSING_EXTERNAL_TYPE_ID_PROPERTY)) {
                    deserializationContext.reportMappingException("Missing property '%s' for external type id '%s'", b2.getName(), this.a[i].d());
                }
                return obj;
            }
            b(jsonParser, deserializationContext, obj, i, str);
        }
        return obj;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x002c, code lost:
        if (r9.d[r0] != null) goto L11;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x002e, code lost:
        r1 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0042, code lost:
        if (r9.c[r0] != null) goto L11;
     */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0047  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean e(com.fasterxml.jackson.core.JsonParser r10, com.fasterxml.jackson.databind.DeserializationContext r11, java.lang.String r12, java.lang.Object r13) throws java.io.IOException {
        /*
            r9 = this;
            java.util.HashMap<java.lang.String, java.lang.Integer> r0 = r9.b
            java.lang.Object r0 = r0.get(r12)
            java.lang.Integer r0 = (java.lang.Integer) r0
            r1 = 0
            if (r0 != 0) goto Lc
            return r1
        Lc:
            int r0 = r0.intValue()
            com.fasterxml.jackson.databind.deser.impl.b$b[] r2 = r9.a
            r2 = r2[r0]
            boolean r12 = r2.f(r12)
            r8 = 1
            if (r12 == 0) goto L30
            java.lang.String[] r12 = r9.c
            java.lang.String r2 = r10.X()
            r12[r0] = r2
            r10.k1()
            if (r13 == 0) goto L45
            com.fasterxml.jackson.databind.util.e[] r12 = r9.d
            r12 = r12[r0]
            if (r12 == 0) goto L45
        L2e:
            r1 = r8
            goto L45
        L30:
            com.fasterxml.jackson.databind.util.e r12 = new com.fasterxml.jackson.databind.util.e
            r12.<init>(r10, r11)
            r12.O1(r10)
            com.fasterxml.jackson.databind.util.e[] r2 = r9.d
            r2[r0] = r12
            if (r13 == 0) goto L45
            java.lang.String[] r12 = r9.c
            r12 = r12[r0]
            if (r12 == 0) goto L45
            goto L2e
        L45:
            if (r1 == 0) goto L5a
            java.lang.String[] r12 = r9.c
            r7 = r12[r0]
            r1 = 0
            r12[r0] = r1
            r2 = r9
            r3 = r10
            r4 = r11
            r5 = r13
            r6 = r0
            r2.b(r3, r4, r5, r6, r7)
            com.fasterxml.jackson.databind.util.e[] r10 = r9.d
            r10[r0] = r1
        L5a:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.impl.b.e(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext, java.lang.String, java.lang.Object):boolean");
    }

    public boolean f(JsonParser jsonParser, DeserializationContext deserializationContext, String str, Object obj) throws IOException {
        Integer num = this.b.get(str);
        boolean z = false;
        if (num == null) {
            return false;
        }
        int intValue = num.intValue();
        if (this.a[intValue].f(str)) {
            String X = jsonParser.X();
            if (obj != null && this.d[intValue] != null) {
                z = true;
            }
            if (z) {
                b(jsonParser, deserializationContext, obj, intValue, X);
                this.d[intValue] = null;
            } else {
                this.c[intValue] = X;
            }
            return true;
        }
        return false;
    }

    public b g() {
        return new b(this);
    }

    public b(b bVar) {
        C0093b[] c0093bArr = bVar.a;
        this.a = c0093bArr;
        this.b = bVar.b;
        int length = c0093bArr.length;
        this.c = new String[length];
        this.d = new com.fasterxml.jackson.databind.util.e[length];
    }
}
