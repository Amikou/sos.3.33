package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import java.io.IOException;

/* compiled from: ValueInjector.java */
/* loaded from: classes.dex */
public class g extends a.C0091a {
    public final Object k0;

    public g(PropertyName propertyName, JavaType javaType, xe xeVar, AnnotatedMember annotatedMember, Object obj) {
        super(propertyName, javaType, null, xeVar, annotatedMember, PropertyMetadata.STD_OPTIONAL);
        this.k0 = obj;
    }

    public Object a(DeserializationContext deserializationContext, Object obj) {
        return deserializationContext.findInjectableValue(this.k0, this, obj);
    }

    public void b(DeserializationContext deserializationContext, Object obj) throws IOException {
        this.i0.setValue(obj, a(deserializationContext, obj));
    }
}
