package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.SettableAnyProperty;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.impl.c;
import java.io.IOException;
import java.util.BitSet;

/* compiled from: PropertyValueBuffer.java */
/* loaded from: classes.dex */
public class d {
    public final JsonParser a;
    public final DeserializationContext b;
    public final ObjectIdReader c;
    public final Object[] d;
    public int e;
    public int f;
    public final BitSet g;
    public c h;
    public Object i;

    public d(JsonParser jsonParser, DeserializationContext deserializationContext, int i, ObjectIdReader objectIdReader) {
        this.a = jsonParser;
        this.b = deserializationContext;
        this.e = i;
        this.c = objectIdReader;
        this.d = new Object[i];
        if (i < 32) {
            this.g = null;
        } else {
            this.g = new BitSet();
        }
    }

    public Object a(SettableBeanProperty settableBeanProperty) throws JsonMappingException {
        if (settableBeanProperty.getInjectableValueId() != null) {
            return this.b.findInjectableValue(settableBeanProperty.getInjectableValueId(), settableBeanProperty, null);
        }
        if (settableBeanProperty.isRequired()) {
            this.b.reportMappingException("Missing required creator property '%s' (index %d)", settableBeanProperty.getName(), Integer.valueOf(settableBeanProperty.getCreatorIndex()));
        }
        if (this.b.isEnabled(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES)) {
            this.b.reportMappingException("Missing creator property '%s' (index %d); DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES enabled", settableBeanProperty.getName(), Integer.valueOf(settableBeanProperty.getCreatorIndex()));
        }
        return settableBeanProperty.getValueDeserializer().getNullValue(this.b);
    }

    public boolean b(SettableBeanProperty settableBeanProperty, Object obj) {
        int creatorIndex = settableBeanProperty.getCreatorIndex();
        this.d[creatorIndex] = obj;
        BitSet bitSet = this.g;
        if (bitSet == null) {
            int i = this.f;
            int i2 = (1 << creatorIndex) | i;
            if (i != i2) {
                this.f = i2;
                int i3 = this.e - 1;
                this.e = i3;
                if (i3 <= 0) {
                    return this.c == null || this.i != null;
                }
            }
        } else if (!bitSet.get(creatorIndex)) {
            this.g.set(creatorIndex);
            this.e--;
        }
        return false;
    }

    public void c(SettableAnyProperty settableAnyProperty, String str, Object obj) {
        this.h = new c.a(this.h, obj, settableAnyProperty, str);
    }

    public void d(Object obj, Object obj2) {
        this.h = new c.b(this.h, obj2, obj);
    }

    public void e(SettableBeanProperty settableBeanProperty, Object obj) {
        this.h = new c.C0094c(this.h, obj, settableBeanProperty);
    }

    public c f() {
        return this.h;
    }

    public Object[] g(SettableBeanProperty[] settableBeanPropertyArr) throws JsonMappingException {
        if (this.e > 0) {
            if (this.g == null) {
                int i = this.f;
                int length = this.d.length;
                int i2 = 0;
                while (i2 < length) {
                    if ((i & 1) == 0) {
                        this.d[i2] = a(settableBeanPropertyArr[i2]);
                    }
                    i2++;
                    i >>= 1;
                }
            } else {
                int length2 = this.d.length;
                int i3 = 0;
                while (true) {
                    int nextClearBit = this.g.nextClearBit(i3);
                    if (nextClearBit >= length2) {
                        break;
                    }
                    this.d[nextClearBit] = a(settableBeanPropertyArr[nextClearBit]);
                    i3 = nextClearBit + 1;
                }
            }
        }
        if (this.b.isEnabled(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES)) {
            for (int i4 = 0; i4 < settableBeanPropertyArr.length; i4++) {
                if (this.d[i4] == null) {
                    this.b.reportMappingException("Null value for creator property '%s'; DeserializationFeature.FAIL_ON_NULL_FOR_CREATOR_PARAMETERS enabled", settableBeanPropertyArr[i4].getName(), Integer.valueOf(settableBeanPropertyArr[i4].getCreatorIndex()));
                }
            }
        }
        return this.d;
    }

    public Object h(DeserializationContext deserializationContext, Object obj) throws IOException {
        ObjectIdReader objectIdReader = this.c;
        if (objectIdReader != null) {
            Object obj2 = this.i;
            if (obj2 != null) {
                deserializationContext.findObjectId(obj2, objectIdReader.generator, objectIdReader.resolver).b(obj);
                SettableBeanProperty settableBeanProperty = this.c.idProperty;
                if (settableBeanProperty != null) {
                    return settableBeanProperty.setAndReturn(obj, this.i);
                }
            } else {
                deserializationContext.reportUnresolvedObjectId(objectIdReader, obj);
            }
        }
        return obj;
    }

    public boolean i(String str) throws IOException {
        ObjectIdReader objectIdReader = this.c;
        if (objectIdReader == null || !str.equals(objectIdReader.propertyName.getSimpleName())) {
            return false;
        }
        this.i = this.c.readObjectReference(this.a, this.b);
        return true;
    }
}
