package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: UnwrappedPropertyHandler.java */
/* loaded from: classes.dex */
public class f {
    public final List<SettableBeanProperty> a;

    public f() {
        this.a = new ArrayList();
    }

    public void a(SettableBeanProperty settableBeanProperty) {
        this.a.add(settableBeanProperty);
    }

    public Object b(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj, com.fasterxml.jackson.databind.util.e eVar) throws IOException {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            JsonParser K1 = eVar.K1();
            K1.T0();
            this.a.get(i).deserializeAndSet(K1, deserializationContext, obj);
        }
        return obj;
    }

    public f c(NameTransformer nameTransformer) {
        com.fasterxml.jackson.databind.c<Object> unwrappingDeserializer;
        ArrayList arrayList = new ArrayList(this.a.size());
        for (SettableBeanProperty settableBeanProperty : this.a) {
            SettableBeanProperty withSimpleName = settableBeanProperty.withSimpleName(nameTransformer.transform(settableBeanProperty.getName()));
            com.fasterxml.jackson.databind.c<Object> valueDeserializer = withSimpleName.getValueDeserializer();
            if (valueDeserializer != null && (unwrappingDeserializer = valueDeserializer.unwrappingDeserializer(nameTransformer)) != valueDeserializer) {
                withSimpleName = withSimpleName.withValueDeserializer(unwrappingDeserializer);
            }
            arrayList.add(withSimpleName);
        }
        return new f(arrayList);
    }

    public f(List<SettableBeanProperty> list) {
        this.a = list;
    }
}
