package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;

/* loaded from: classes.dex */
public final class TypeWrappedDeserializer extends com.fasterxml.jackson.databind.c<Object> implements Serializable {
    private static final long serialVersionUID = 1;
    public final com.fasterxml.jackson.databind.c<Object> _deserializer;
    public final com.fasterxml.jackson.databind.jsontype.a _typeDeserializer;

    public TypeWrappedDeserializer(com.fasterxml.jackson.databind.jsontype.a aVar, com.fasterxml.jackson.databind.c<?> cVar) {
        this._typeDeserializer = aVar;
        this._deserializer = cVar;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return this._deserializer.deserializeWithType(jsonParser, deserializationContext, this._typeDeserializer);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, com.fasterxml.jackson.databind.jsontype.a aVar) throws IOException {
        throw new IllegalStateException("Type-wrapped deserializer's deserializeWithType should never get called");
    }

    @Override // com.fasterxml.jackson.databind.c
    public com.fasterxml.jackson.databind.c<?> getDelegatee() {
        return this._deserializer.getDelegatee();
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object getEmptyValue(DeserializationContext deserializationContext) throws JsonMappingException {
        return this._deserializer.getEmptyValue(deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Collection<Object> getKnownPropertyNames() {
        return this._deserializer.getKnownPropertyNames();
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object getNullValue(DeserializationContext deserializationContext) throws JsonMappingException {
        return this._deserializer.getNullValue(deserializationContext);
    }

    @Override // com.fasterxml.jackson.databind.c
    public Class<?> handledType() {
        return this._deserializer.handledType();
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException {
        return this._deserializer.deserialize(jsonParser, deserializationContext, obj);
    }
}
