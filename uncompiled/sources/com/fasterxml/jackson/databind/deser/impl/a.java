package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import java.io.IOException;

/* compiled from: ErrorThrowingDeserializer.java */
/* loaded from: classes.dex */
public class a extends com.fasterxml.jackson.databind.c<Object> {
    public final Error a;

    public a(NoClassDefFoundError noClassDefFoundError) {
        this.a = noClassDefFoundError;
    }

    @Override // com.fasterxml.jackson.databind.c
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        throw this.a;
    }
}
