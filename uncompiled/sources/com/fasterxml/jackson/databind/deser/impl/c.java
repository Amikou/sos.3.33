package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.deser.SettableAnyProperty;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import java.io.IOException;
import java.util.Map;

/* compiled from: PropertyValue.java */
/* loaded from: classes.dex */
public abstract class c {
    public final c a;
    public final Object b;

    /* compiled from: PropertyValue.java */
    /* loaded from: classes.dex */
    public static final class a extends c {
        public final SettableAnyProperty c;
        public final String d;

        public a(c cVar, Object obj, SettableAnyProperty settableAnyProperty, String str) {
            super(cVar, obj);
            this.c = settableAnyProperty;
            this.d = str;
        }

        @Override // com.fasterxml.jackson.databind.deser.impl.c
        public void a(Object obj) throws IOException, JsonProcessingException {
            this.c.set(obj, this.d, this.b);
        }
    }

    /* compiled from: PropertyValue.java */
    /* loaded from: classes.dex */
    public static final class b extends c {
        public final Object c;

        public b(c cVar, Object obj, Object obj2) {
            super(cVar, obj);
            this.c = obj2;
        }

        @Override // com.fasterxml.jackson.databind.deser.impl.c
        public void a(Object obj) throws IOException, JsonProcessingException {
            ((Map) obj).put(this.c, this.b);
        }
    }

    /* compiled from: PropertyValue.java */
    /* renamed from: com.fasterxml.jackson.databind.deser.impl.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0094c extends c {
        public final SettableBeanProperty c;

        public C0094c(c cVar, Object obj, SettableBeanProperty settableBeanProperty) {
            super(cVar, obj);
            this.c = settableBeanProperty;
        }

        @Override // com.fasterxml.jackson.databind.deser.impl.c
        public void a(Object obj) throws IOException, JsonProcessingException {
            this.c.set(obj, this.b);
        }
    }

    public c(c cVar, Object obj) {
        this.a = cVar;
        this.b = obj;
    }

    public abstract void a(Object obj) throws IOException, JsonProcessingException;
}
