package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.i;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

/* loaded from: classes.dex */
public final class PropertyBasedCreator {
    public final int a;
    public final i b;
    public final HashMap<String, SettableBeanProperty> c;
    public final SettableBeanProperty[] d;

    /* loaded from: classes.dex */
    public static class CaseInsensitiveMap extends HashMap<String, SettableBeanProperty> {
        private static final long serialVersionUID = 1;

        @Override // java.util.HashMap, java.util.AbstractMap, java.util.Map
        public SettableBeanProperty get(Object obj) {
            return (SettableBeanProperty) super.get((Object) ((String) obj).toLowerCase());
        }

        @Override // java.util.HashMap, java.util.AbstractMap, java.util.Map
        public SettableBeanProperty put(String str, SettableBeanProperty settableBeanProperty) {
            return (SettableBeanProperty) super.put((CaseInsensitiveMap) str.toLowerCase(), (String) settableBeanProperty);
        }
    }

    public PropertyBasedCreator(i iVar, SettableBeanProperty[] settableBeanPropertyArr, boolean z) {
        this.b = iVar;
        if (z) {
            this.c = new CaseInsensitiveMap();
        } else {
            this.c = new HashMap<>();
        }
        int length = settableBeanPropertyArr.length;
        this.a = length;
        this.d = new SettableBeanProperty[length];
        for (int i = 0; i < length; i++) {
            SettableBeanProperty settableBeanProperty = settableBeanPropertyArr[i];
            this.d[i] = settableBeanProperty;
            this.c.put(settableBeanProperty.getName(), settableBeanProperty);
        }
    }

    public static PropertyBasedCreator b(DeserializationContext deserializationContext, i iVar, SettableBeanProperty[] settableBeanPropertyArr) throws JsonMappingException {
        int length = settableBeanPropertyArr.length;
        SettableBeanProperty[] settableBeanPropertyArr2 = new SettableBeanProperty[length];
        for (int i = 0; i < length; i++) {
            SettableBeanProperty settableBeanProperty = settableBeanPropertyArr[i];
            if (!settableBeanProperty.hasValueDeserializer()) {
                settableBeanProperty = settableBeanProperty.withValueDeserializer(deserializationContext.findContextualValueDeserializer(settableBeanProperty.getType(), settableBeanProperty));
            }
            settableBeanPropertyArr2[i] = settableBeanProperty;
        }
        return new PropertyBasedCreator(iVar, settableBeanPropertyArr2, deserializationContext.isEnabled(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES));
    }

    public Object a(DeserializationContext deserializationContext, d dVar) throws IOException {
        Object createFromObjectWith = this.b.createFromObjectWith(deserializationContext, this.d, dVar);
        if (createFromObjectWith != null) {
            createFromObjectWith = dVar.h(deserializationContext, createFromObjectWith);
            for (c f = dVar.f(); f != null; f = f.a) {
                f.a(createFromObjectWith);
            }
        }
        return createFromObjectWith;
    }

    public SettableBeanProperty c(int i) {
        for (SettableBeanProperty settableBeanProperty : this.c.values()) {
            if (settableBeanProperty.getPropertyIndex() == i) {
                return settableBeanProperty;
            }
        }
        return null;
    }

    public SettableBeanProperty d(String str) {
        return this.c.get(str);
    }

    public Collection<SettableBeanProperty> e() {
        return this.c.values();
    }

    public d f(JsonParser jsonParser, DeserializationContext deserializationContext, ObjectIdReader objectIdReader) {
        return new d(jsonParser, deserializationContext, this.a, objectIdReader);
    }
}
