package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.std.StdDelegatingDeserializer;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.ReferenceType;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes.dex */
public final class DeserializerCache implements Serializable {
    private static final long serialVersionUID = 1;
    public final ConcurrentHashMap<JavaType, com.fasterxml.jackson.databind.c<Object>> _cachedDeserializers = new ConcurrentHashMap<>(64, 0.75f, 4);
    public final HashMap<JavaType, com.fasterxml.jackson.databind.c<Object>> _incompleteDeserializers = new HashMap<>(8);

    public com.fasterxml.jackson.databind.c<Object> _createAndCache2(DeserializationContext deserializationContext, e eVar, JavaType javaType) throws JsonMappingException {
        try {
            com.fasterxml.jackson.databind.c<Object> _createDeserializer = _createDeserializer(deserializationContext, eVar, javaType);
            if (_createDeserializer == null) {
                return null;
            }
            boolean z = !a(javaType) && _createDeserializer.isCachable();
            if (_createDeserializer instanceof h) {
                this._incompleteDeserializers.put(javaType, _createDeserializer);
                ((h) _createDeserializer).resolve(deserializationContext);
                this._incompleteDeserializers.remove(javaType);
            }
            if (z) {
                this._cachedDeserializers.put(javaType, _createDeserializer);
            }
            return _createDeserializer;
        } catch (IllegalArgumentException e) {
            throw JsonMappingException.from(deserializationContext, e.getMessage(), e);
        }
    }

    public com.fasterxml.jackson.databind.c<Object> _createAndCacheValueDeserializer(DeserializationContext deserializationContext, e eVar, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.c<Object> cVar;
        synchronized (this._incompleteDeserializers) {
            com.fasterxml.jackson.databind.c<Object> _findCachedDeserializer = _findCachedDeserializer(javaType);
            if (_findCachedDeserializer != null) {
                return _findCachedDeserializer;
            }
            int size = this._incompleteDeserializers.size();
            if (size <= 0 || (cVar = this._incompleteDeserializers.get(javaType)) == null) {
                com.fasterxml.jackson.databind.c<Object> _createAndCache2 = _createAndCache2(deserializationContext, eVar, javaType);
                if (size == 0 && this._incompleteDeserializers.size() > 0) {
                    this._incompleteDeserializers.clear();
                }
                return _createAndCache2;
            }
            return cVar;
        }
    }

    public com.fasterxml.jackson.databind.c<Object> _createDeserializer(DeserializationContext deserializationContext, e eVar, JavaType javaType) throws JsonMappingException {
        DeserializationConfig config = deserializationContext.getConfig();
        if (javaType.isAbstract() || javaType.isMapLikeType() || javaType.isCollectionLikeType()) {
            javaType = eVar.mapAbstractType(config, javaType);
        }
        so introspect = config.introspect(javaType);
        com.fasterxml.jackson.databind.c<Object> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationContext, introspect.t());
        if (findDeserializerFromAnnotation != null) {
            return findDeserializerFromAnnotation;
        }
        JavaType c = c(deserializationContext, introspect.t(), javaType);
        if (c != javaType) {
            introspect = config.introspect(c);
            javaType = c;
        }
        Class<?> l = introspect.l();
        if (l != null) {
            return eVar.createBuilderBasedDeserializer(deserializationContext, javaType, introspect, l);
        }
        o80<Object, Object> f = introspect.f();
        if (f == null) {
            return _createDeserializer2(deserializationContext, eVar, javaType, introspect);
        }
        JavaType b = f.b(deserializationContext.getTypeFactory());
        if (!b.hasRawClass(javaType.getRawClass())) {
            introspect = config.introspect(b);
        }
        return new StdDelegatingDeserializer(f, b, _createDeserializer2(deserializationContext, eVar, b, introspect));
    }

    public com.fasterxml.jackson.databind.c<?> _createDeserializer2(DeserializationContext deserializationContext, e eVar, JavaType javaType, so soVar) throws JsonMappingException {
        JsonFormat.Value g;
        DeserializationConfig config = deserializationContext.getConfig();
        if (javaType.isEnumType()) {
            return eVar.createEnumDeserializer(deserializationContext, javaType, soVar);
        }
        if (javaType.isContainerType()) {
            if (javaType.isArrayType()) {
                return eVar.createArrayDeserializer(deserializationContext, (ArrayType) javaType, soVar);
            }
            if (javaType.isMapLikeType()) {
                MapLikeType mapLikeType = (MapLikeType) javaType;
                if (mapLikeType.isTrueMapType()) {
                    return eVar.createMapDeserializer(deserializationContext, (MapType) mapLikeType, soVar);
                }
                return eVar.createMapLikeDeserializer(deserializationContext, mapLikeType, soVar);
            } else if (javaType.isCollectionLikeType() && ((g = soVar.g(null)) == null || g.getShape() != JsonFormat.Shape.OBJECT)) {
                CollectionLikeType collectionLikeType = (CollectionLikeType) javaType;
                if (collectionLikeType.isTrueCollectionType()) {
                    return eVar.createCollectionDeserializer(deserializationContext, (CollectionType) collectionLikeType, soVar);
                }
                return eVar.createCollectionLikeDeserializer(deserializationContext, collectionLikeType, soVar);
            }
        }
        if (javaType.isReferenceType()) {
            return eVar.createReferenceDeserializer(deserializationContext, (ReferenceType) javaType, soVar);
        }
        if (com.fasterxml.jackson.databind.d.class.isAssignableFrom(javaType.getRawClass())) {
            return eVar.createTreeDeserializer(config, javaType, soVar);
        }
        return eVar.createBeanDeserializer(deserializationContext, javaType, soVar);
    }

    public com.fasterxml.jackson.databind.c<Object> _findCachedDeserializer(JavaType javaType) {
        if (javaType != null) {
            if (a(javaType)) {
                return null;
            }
            return this._cachedDeserializers.get(javaType);
        }
        throw new IllegalArgumentException("Null JavaType passed");
    }

    public com.fasterxml.jackson.databind.g _handleUnknownKeyDeserializer(DeserializationContext deserializationContext, JavaType javaType) throws JsonMappingException {
        deserializationContext.reportMappingException("Can not find a (Map) Key deserializer for type %s", javaType);
        return null;
    }

    public com.fasterxml.jackson.databind.c<Object> _handleUnknownValueDeserializer(DeserializationContext deserializationContext, JavaType javaType) throws JsonMappingException {
        if (!com.fasterxml.jackson.databind.util.c.H(javaType.getRawClass())) {
            deserializationContext.reportMappingException("Can not find a Value deserializer for abstract type %s", javaType);
        }
        deserializationContext.reportMappingException("Can not find a Value deserializer for type %s", javaType);
        return null;
    }

    public final boolean a(JavaType javaType) {
        if (javaType.isContainerType()) {
            JavaType contentType = javaType.getContentType();
            if (contentType == null || (contentType.getValueHandler() == null && contentType.getTypeHandler() == null)) {
                return javaType.isMapLikeType() && javaType.getKeyType().getValueHandler() != null;
            }
            return true;
        }
        return false;
    }

    public final Class<?> b(Object obj, String str, Class<?> cls) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Class) {
            Class<?> cls2 = (Class) obj;
            if (cls2 == cls || com.fasterxml.jackson.databind.util.c.G(cls2)) {
                return null;
            }
            return cls2;
        }
        throw new IllegalStateException("AnnotationIntrospector." + str + "() returned value of type " + obj.getClass().getName() + ": expected type JsonSerializer or Class<JsonSerializer> instead");
    }

    public final JavaType c(DeserializationContext deserializationContext, ue ueVar, JavaType javaType) throws JsonMappingException {
        Object findContentDeserializer;
        JavaType keyType;
        Object findKeyDeserializer;
        com.fasterxml.jackson.databind.g keyDeserializerInstance;
        AnnotationIntrospector annotationIntrospector = deserializationContext.getAnnotationIntrospector();
        if (annotationIntrospector == null) {
            return javaType;
        }
        if (javaType.isMapLikeType() && (keyType = javaType.getKeyType()) != null && keyType.getValueHandler() == null && (findKeyDeserializer = annotationIntrospector.findKeyDeserializer(ueVar)) != null && (keyDeserializerInstance = deserializationContext.keyDeserializerInstance(ueVar, findKeyDeserializer)) != null) {
            javaType = ((MapLikeType) javaType).withKeyValueHandler(keyDeserializerInstance);
            javaType.getKeyType();
        }
        JavaType contentType = javaType.getContentType();
        if (contentType != null && contentType.getValueHandler() == null && (findContentDeserializer = annotationIntrospector.findContentDeserializer(ueVar)) != null) {
            com.fasterxml.jackson.databind.c<Object> cVar = null;
            if (findContentDeserializer instanceof com.fasterxml.jackson.databind.c) {
                com.fasterxml.jackson.databind.c cVar2 = (com.fasterxml.jackson.databind.c) findContentDeserializer;
            } else {
                Class<?> b = b(findContentDeserializer, "findContentDeserializer", c.a.class);
                if (b != null) {
                    cVar = deserializationContext.deserializerInstance(ueVar, b);
                }
            }
            if (cVar != null) {
                javaType = javaType.withContentValueHandler(cVar);
            }
        }
        return annotationIntrospector.refineDeserializationType(deserializationContext.getConfig(), ueVar, javaType);
    }

    public int cachedDeserializersCount() {
        return this._cachedDeserializers.size();
    }

    public o80<Object, Object> findConverter(DeserializationContext deserializationContext, ue ueVar) throws JsonMappingException {
        Object findDeserializationConverter = deserializationContext.getAnnotationIntrospector().findDeserializationConverter(ueVar);
        if (findDeserializationConverter == null) {
            return null;
        }
        return deserializationContext.converterInstance(ueVar, findDeserializationConverter);
    }

    public com.fasterxml.jackson.databind.c<Object> findConvertingDeserializer(DeserializationContext deserializationContext, ue ueVar, com.fasterxml.jackson.databind.c<Object> cVar) throws JsonMappingException {
        o80<Object, Object> findConverter = findConverter(deserializationContext, ueVar);
        return findConverter == null ? cVar : new StdDelegatingDeserializer(findConverter, findConverter.b(deserializationContext.getTypeFactory()), cVar);
    }

    public com.fasterxml.jackson.databind.c<Object> findDeserializerFromAnnotation(DeserializationContext deserializationContext, ue ueVar) throws JsonMappingException {
        Object findDeserializer = deserializationContext.getAnnotationIntrospector().findDeserializer(ueVar);
        if (findDeserializer == null) {
            return null;
        }
        return findConvertingDeserializer(deserializationContext, ueVar, deserializationContext.deserializerInstance(ueVar, findDeserializer));
    }

    public com.fasterxml.jackson.databind.g findKeyDeserializer(DeserializationContext deserializationContext, e eVar, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.g createKeyDeserializer = eVar.createKeyDeserializer(deserializationContext, javaType);
        if (createKeyDeserializer == null) {
            return _handleUnknownKeyDeserializer(deserializationContext, javaType);
        }
        if (createKeyDeserializer instanceof h) {
            ((h) createKeyDeserializer).resolve(deserializationContext);
        }
        return createKeyDeserializer;
    }

    public com.fasterxml.jackson.databind.c<Object> findValueDeserializer(DeserializationContext deserializationContext, e eVar, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.c<Object> _findCachedDeserializer = _findCachedDeserializer(javaType);
        if (_findCachedDeserializer == null) {
            com.fasterxml.jackson.databind.c<Object> _createAndCacheValueDeserializer = _createAndCacheValueDeserializer(deserializationContext, eVar, javaType);
            return _createAndCacheValueDeserializer == null ? _handleUnknownValueDeserializer(deserializationContext, javaType) : _createAndCacheValueDeserializer;
        }
        return _findCachedDeserializer;
    }

    public void flushCachedDeserializers() {
        this._cachedDeserializers.clear();
    }

    public boolean hasValueDeserializerFor(DeserializationContext deserializationContext, e eVar, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.c<Object> _findCachedDeserializer = _findCachedDeserializer(javaType);
        if (_findCachedDeserializer == null) {
            _findCachedDeserializer = _createAndCacheValueDeserializer(deserializationContext, eVar, javaType);
        }
        return _findCachedDeserializer != null;
    }

    public Object writeReplace() {
        this._incompleteDeserializers.clear();
        return this;
    }
}
