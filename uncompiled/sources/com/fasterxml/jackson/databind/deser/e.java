package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.ReferenceType;

/* compiled from: DeserializerFactory.java */
/* loaded from: classes.dex */
public abstract class e {
    public static final f[] NO_DESERIALIZERS = new f[0];

    public abstract com.fasterxml.jackson.databind.c<?> createArrayDeserializer(DeserializationContext deserializationContext, ArrayType arrayType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<Object> createBeanDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<Object> createBuilderBasedDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar, Class<?> cls) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<?> createCollectionDeserializer(DeserializationContext deserializationContext, CollectionType collectionType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<?> createCollectionLikeDeserializer(DeserializationContext deserializationContext, CollectionLikeType collectionLikeType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<?> createEnumDeserializer(DeserializationContext deserializationContext, JavaType javaType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.g createKeyDeserializer(DeserializationContext deserializationContext, JavaType javaType) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<?> createMapDeserializer(DeserializationContext deserializationContext, MapType mapType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<?> createMapLikeDeserializer(DeserializationContext deserializationContext, MapLikeType mapLikeType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<?> createReferenceDeserializer(DeserializationContext deserializationContext, ReferenceType referenceType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.c<?> createTreeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, so soVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.jsontype.a findTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType) throws JsonMappingException;

    public abstract i findValueInstantiator(DeserializationContext deserializationContext, so soVar) throws JsonMappingException;

    public abstract JavaType mapAbstractType(DeserializationConfig deserializationConfig, JavaType javaType) throws JsonMappingException;

    public abstract e withAbstractTypeResolver(o5 o5Var);

    public abstract e withAdditionalDeserializers(f fVar);

    public abstract e withAdditionalKeyDeserializers(g gVar);

    public abstract e withDeserializerModifier(uo uoVar);

    public abstract e withValueInstantiators(ah4 ah4Var);
}
