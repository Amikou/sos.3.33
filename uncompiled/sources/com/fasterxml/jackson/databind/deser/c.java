package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.format.MatchStrength;
import com.fasterxml.jackson.core.format.a;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: DataFormatReaders.java */
/* loaded from: classes.dex */
public class c {
    public final ObjectReader[] a;
    public final MatchStrength b;
    public final MatchStrength c;
    public final int d;

    /* compiled from: DataFormatReaders.java */
    /* loaded from: classes.dex */
    public class a extends a.C0090a {
        public a(c cVar, InputStream inputStream, byte[] bArr) {
            super(inputStream, bArr);
        }

        public b b(ObjectReader objectReader, MatchStrength matchStrength) {
            InputStream inputStream = this.a;
            byte[] bArr = this.b;
            int i = this.c;
            return new b(inputStream, bArr, i, this.d - i, objectReader, matchStrength);
        }

        public a(c cVar, byte[] bArr, int i, int i2) {
            super(bArr, i, i2);
        }
    }

    /* compiled from: DataFormatReaders.java */
    /* loaded from: classes.dex */
    public static class b {
        public final InputStream a;
        public final byte[] b;
        public final int c;
        public final int d;
        public final ObjectReader e;

        public b(InputStream inputStream, byte[] bArr, int i, int i2, ObjectReader objectReader, MatchStrength matchStrength) {
            this.a = inputStream;
            this.b = bArr;
            this.c = i;
            this.d = i2;
            this.e = objectReader;
        }

        public JsonParser a() throws IOException {
            ObjectReader objectReader = this.e;
            if (objectReader == null) {
                return null;
            }
            JsonFactory factory = objectReader.getFactory();
            if (this.a == null) {
                return factory.createParser(this.b, this.c, this.d);
            }
            return factory.createParser(b());
        }

        public InputStream b() {
            if (this.a == null) {
                return new ByteArrayInputStream(this.b, this.c, this.d);
            }
            return new com.fasterxml.jackson.core.io.c(null, this.a, this.b, this.c, this.d);
        }

        public ObjectReader c() {
            return this.e;
        }

        public boolean d() {
            return this.e != null;
        }
    }

    public c(ObjectReader... objectReaderArr) {
        this(objectReaderArr, MatchStrength.SOLID_MATCH, MatchStrength.WEAK_MATCH, 64);
    }

    public final b a(a aVar) throws IOException {
        ObjectReader[] objectReaderArr = this.a;
        int length = objectReaderArr.length;
        ObjectReader objectReader = null;
        int i = 0;
        MatchStrength matchStrength = null;
        while (true) {
            if (i >= length) {
                break;
            }
            ObjectReader objectReader2 = objectReaderArr[i];
            aVar.a();
            MatchStrength hasFormat = objectReader2.getFactory().hasFormat(aVar);
            if (hasFormat != null && hasFormat.ordinal() >= this.c.ordinal() && (objectReader == null || matchStrength.ordinal() < hasFormat.ordinal())) {
                if (hasFormat.ordinal() >= this.b.ordinal()) {
                    objectReader = objectReader2;
                    matchStrength = hasFormat;
                    break;
                }
                objectReader = objectReader2;
                matchStrength = hasFormat;
            }
            i++;
        }
        return aVar.b(objectReader, matchStrength);
    }

    public b b(InputStream inputStream) throws IOException {
        return a(new a(this, inputStream, new byte[this.d]));
    }

    public b c(byte[] bArr, int i, int i2) throws IOException {
        return a(new a(this, bArr, i, i2));
    }

    public c d(DeserializationConfig deserializationConfig) {
        int length = this.a.length;
        ObjectReader[] objectReaderArr = new ObjectReader[length];
        for (int i = 0; i < length; i++) {
            objectReaderArr[i] = this.a[i].with(deserializationConfig);
        }
        return new c(objectReaderArr, this.b, this.c, this.d);
    }

    public c e(JavaType javaType) {
        int length = this.a.length;
        ObjectReader[] objectReaderArr = new ObjectReader[length];
        for (int i = 0; i < length; i++) {
            objectReaderArr[i] = this.a[i].forType(javaType);
        }
        return new c(objectReaderArr, this.b, this.c, this.d);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        ObjectReader[] objectReaderArr = this.a;
        int length = objectReaderArr.length;
        if (length > 0) {
            sb.append(objectReaderArr[0].getFactory().getFormatName());
            for (int i = 1; i < length; i++) {
                sb.append(", ");
                sb.append(this.a[i].getFactory().getFormatName());
            }
        }
        sb.append(']');
        return sb.toString();
    }

    public c(ObjectReader[] objectReaderArr, MatchStrength matchStrength, MatchStrength matchStrength2, int i) {
        this.a = objectReaderArr;
        this.b = matchStrength;
        this.c = matchStrength2;
        this.d = i;
    }
}
