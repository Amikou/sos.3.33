package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.cfg.ContextAttributes;
import com.fasterxml.jackson.databind.ser.impl.FailingSerializer;
import com.fasterxml.jackson.databind.ser.impl.UnknownSerializer;
import com.fasterxml.jackson.databind.ser.std.NullSerializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

/* compiled from: SerializerProvider.java */
/* loaded from: classes.dex */
public abstract class k extends b {
    public static final boolean CACHE_UNKNOWN_MAPPINGS = false;
    public static final f<Object> DEFAULT_NULL_KEY_SERIALIZER = new FailingSerializer("Null key for a Map not allowed in JSON (use a converting NullKeySerializer?)");
    public static final f<Object> DEFAULT_UNKNOWN_SERIALIZER = new UnknownSerializer();
    public transient ContextAttributes _attributes;
    public final SerializationConfig _config;
    public DateFormat _dateFormat;
    public f<Object> _keySerializer;
    public final a43 _knownSerializers;
    public f<Object> _nullKeySerializer;
    public f<Object> _nullValueSerializer;
    public final Class<?> _serializationView;
    public final com.fasterxml.jackson.databind.ser.f _serializerCache;
    public final com.fasterxml.jackson.databind.ser.g _serializerFactory;
    public final boolean _stdNullValueSerializer;
    public f<Object> _unknownTypeSerializer;

    public k() {
        this._unknownTypeSerializer = DEFAULT_UNKNOWN_SERIALIZER;
        this._nullValueSerializer = NullSerializer.instance;
        this._nullKeySerializer = DEFAULT_NULL_KEY_SERIALIZER;
        this._config = null;
        this._serializerFactory = null;
        this._serializerCache = new com.fasterxml.jackson.databind.ser.f();
        this._knownSerializers = null;
        this._serializationView = null;
        this._attributes = null;
        this._stdNullValueSerializer = true;
    }

    public f<Object> _createAndCacheUntypedSerializer(Class<?> cls) throws JsonMappingException {
        JavaType constructType = this._config.constructType(cls);
        try {
            f<Object> _createUntypedSerializer = _createUntypedSerializer(constructType);
            if (_createUntypedSerializer != null) {
                this._serializerCache.c(cls, constructType, _createUntypedSerializer, this);
            }
            return _createUntypedSerializer;
        } catch (IllegalArgumentException e) {
            reportMappingProblem(e, e.getMessage(), new Object[0]);
            return null;
        }
    }

    public f<Object> _createUntypedSerializer(JavaType javaType) throws JsonMappingException {
        f<Object> createSerializer;
        synchronized (this._serializerCache) {
            createSerializer = this._serializerFactory.createSerializer(this, javaType);
        }
        return createSerializer;
    }

    public final DateFormat _dateFormat() {
        DateFormat dateFormat = this._dateFormat;
        if (dateFormat != null) {
            return dateFormat;
        }
        DateFormat dateFormat2 = (DateFormat) this._config.getDateFormat().clone();
        this._dateFormat = dateFormat2;
        return dateFormat2;
    }

    public String _desc(Object obj) {
        if (obj == null) {
            return org.web3j.utils.b.MISSING_REASON;
        }
        return "'" + obj + "'";
    }

    public f<Object> _findExplicitUntypedSerializer(Class<?> cls) throws JsonMappingException {
        f<Object> f = this._knownSerializers.f(cls);
        if (f == null && (f = this._serializerCache.l(cls)) == null) {
            f = _createAndCacheUntypedSerializer(cls);
        }
        if (isUnknownTypeSerializer(f)) {
            return null;
        }
        return f;
    }

    public f<Object> _handleContextualResolvable(f<?> fVar, a aVar) throws JsonMappingException {
        if (fVar instanceof com.fasterxml.jackson.databind.ser.e) {
            ((com.fasterxml.jackson.databind.ser.e) fVar).resolve(this);
        }
        return handleSecondaryContextualization(fVar, aVar);
    }

    public f<Object> _handleResolvable(f<?> fVar) throws JsonMappingException {
        if (fVar instanceof com.fasterxml.jackson.databind.ser.e) {
            ((com.fasterxml.jackson.databind.ser.e) fVar).resolve(this);
        }
        return fVar;
    }

    public String _quotedString(Object obj) {
        return obj == null ? org.web3j.utils.b.MISSING_REASON : String.valueOf(obj);
    }

    public void _reportIncompatibleRootType(Object obj, JavaType javaType) throws IOException {
        if (javaType.isPrimitive() && com.fasterxml.jackson.databind.util.c.U(javaType.getRawClass()).isAssignableFrom(obj.getClass())) {
            return;
        }
        reportMappingProblem("Incompatible types: declared root type (%s) vs %s", javaType, obj.getClass().getName());
    }

    @Override // com.fasterxml.jackson.databind.b
    public final boolean canOverrideAccessModifiers() {
        return this._config.canOverrideAccessModifiers();
    }

    public void defaultSerializeDateKey(long j, JsonGenerator jsonGenerator) throws IOException {
        if (isEnabled(SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS)) {
            jsonGenerator.i0(String.valueOf(j));
        } else {
            jsonGenerator.i0(_dateFormat().format(new Date(j)));
        }
    }

    public final void defaultSerializeDateValue(long j, JsonGenerator jsonGenerator) throws IOException {
        if (isEnabled(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)) {
            jsonGenerator.y0(j);
        } else {
            jsonGenerator.o1(_dateFormat().format(new Date(j)));
        }
    }

    public final void defaultSerializeField(String str, Object obj, JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.i0(str);
        if (obj == null) {
            if (this._stdNullValueSerializer) {
                jsonGenerator.m0();
                return;
            } else {
                this._nullValueSerializer.serialize(null, jsonGenerator, this);
                return;
            }
        }
        findTypedValueSerializer(obj.getClass(), true, (a) null).serialize(obj, jsonGenerator, this);
    }

    public final void defaultSerializeNull(JsonGenerator jsonGenerator) throws IOException {
        if (this._stdNullValueSerializer) {
            jsonGenerator.m0();
        } else {
            this._nullValueSerializer.serialize(null, jsonGenerator, this);
        }
    }

    public final void defaultSerializeValue(Object obj, JsonGenerator jsonGenerator) throws IOException {
        if (obj == null) {
            if (this._stdNullValueSerializer) {
                jsonGenerator.m0();
                return;
            } else {
                this._nullValueSerializer.serialize(null, jsonGenerator, this);
                return;
            }
        }
        findTypedValueSerializer(obj.getClass(), true, (a) null).serialize(obj, jsonGenerator, this);
    }

    public f<Object> findKeySerializer(JavaType javaType, a aVar) throws JsonMappingException {
        return _handleContextualResolvable(this._serializerFactory.createKeySerializer(this._config, javaType, this._keySerializer), aVar);
    }

    public f<Object> findNullKeySerializer(JavaType javaType, a aVar) throws JsonMappingException {
        return this._nullKeySerializer;
    }

    public f<Object> findNullValueSerializer(a aVar) throws JsonMappingException {
        return this._nullValueSerializer;
    }

    public abstract com.fasterxml.jackson.databind.ser.impl.c findObjectId(Object obj, ObjectIdGenerator<?> objectIdGenerator);

    public f<Object> findPrimaryPropertySerializer(JavaType javaType, a aVar) throws JsonMappingException {
        f<Object> e = this._knownSerializers.e(javaType);
        if (e == null && (e = this._serializerCache.k(javaType)) == null && (e = _createAndCacheUntypedSerializer(javaType)) == null) {
            return getUnknownTypeSerializer(javaType.getRawClass());
        }
        return handlePrimaryContextualization(e, aVar);
    }

    public com.fasterxml.jackson.databind.jsontype.c findTypeSerializer(JavaType javaType) throws JsonMappingException {
        return this._serializerFactory.createTypeSerializer(this._config, javaType);
    }

    public f<Object> findTypedValueSerializer(Class<?> cls, boolean z, a aVar) throws JsonMappingException {
        f<Object> d = this._knownSerializers.d(cls);
        if (d != null) {
            return d;
        }
        f<Object> j = this._serializerCache.j(cls);
        if (j != null) {
            return j;
        }
        f<Object> findValueSerializer = findValueSerializer(cls, aVar);
        com.fasterxml.jackson.databind.ser.g gVar = this._serializerFactory;
        SerializationConfig serializationConfig = this._config;
        com.fasterxml.jackson.databind.jsontype.c createTypeSerializer = gVar.createTypeSerializer(serializationConfig, serializationConfig.constructType(cls));
        if (createTypeSerializer != null) {
            findValueSerializer = new com.fasterxml.jackson.databind.ser.impl.b(createTypeSerializer.a(aVar), findValueSerializer);
        }
        if (z) {
            this._serializerCache.e(cls, findValueSerializer);
        }
        return findValueSerializer;
    }

    public f<Object> findValueSerializer(Class<?> cls, a aVar) throws JsonMappingException {
        f<Object> f = this._knownSerializers.f(cls);
        if (f == null && (f = this._serializerCache.l(cls)) == null && (f = this._serializerCache.k(this._config.constructType(cls))) == null && (f = _createAndCacheUntypedSerializer(cls)) == null) {
            return getUnknownTypeSerializer(cls);
        }
        return handleSecondaryContextualization(f, aVar);
    }

    @Override // com.fasterxml.jackson.databind.b
    public final Class<?> getActiveView() {
        return this._serializationView;
    }

    @Override // com.fasterxml.jackson.databind.b
    public final AnnotationIntrospector getAnnotationIntrospector() {
        return this._config.getAnnotationIntrospector();
    }

    @Override // com.fasterxml.jackson.databind.b
    public Object getAttribute(Object obj) {
        return this._attributes.getAttribute(obj);
    }

    public f<Object> getDefaultNullKeySerializer() {
        return this._nullKeySerializer;
    }

    public f<Object> getDefaultNullValueSerializer() {
        return this._nullValueSerializer;
    }

    @Override // com.fasterxml.jackson.databind.b
    public final JsonFormat.Value getDefaultPropertyFormat(Class<?> cls) {
        return this._config.getDefaultPropertyFormat(cls);
    }

    public final JsonInclude.Value getDefaultPropertyInclusion(Class<?> cls) {
        return this._config.getDefaultPropertyInclusion();
    }

    public final j41 getFilterProvider() {
        return this._config.getFilterProvider();
    }

    public JsonGenerator getGenerator() {
        return null;
    }

    @Override // com.fasterxml.jackson.databind.b
    public Locale getLocale() {
        return this._config.getLocale();
    }

    @Deprecated
    public final Class<?> getSerializationView() {
        return this._serializationView;
    }

    @Override // com.fasterxml.jackson.databind.b
    public TimeZone getTimeZone() {
        return this._config.getTimeZone();
    }

    @Override // com.fasterxml.jackson.databind.b
    public final TypeFactory getTypeFactory() {
        return this._config.getTypeFactory();
    }

    public f<Object> getUnknownTypeSerializer(Class<?> cls) {
        if (cls == Object.class) {
            return this._unknownTypeSerializer;
        }
        return new UnknownSerializer(cls);
    }

    public f<?> handlePrimaryContextualization(f<?> fVar, a aVar) throws JsonMappingException {
        return (fVar == null || !(fVar instanceof com.fasterxml.jackson.databind.ser.b)) ? fVar : ((com.fasterxml.jackson.databind.ser.b) fVar).createContextual(this, aVar);
    }

    public f<?> handleSecondaryContextualization(f<?> fVar, a aVar) throws JsonMappingException {
        return (fVar == null || !(fVar instanceof com.fasterxml.jackson.databind.ser.b)) ? fVar : ((com.fasterxml.jackson.databind.ser.b) fVar).createContextual(this, aVar);
    }

    public final boolean hasSerializationFeatures(int i) {
        return this._config.hasSerializationFeatures(i);
    }

    @Override // com.fasterxml.jackson.databind.b
    public final boolean isEnabled(MapperFeature mapperFeature) {
        return this._config.isEnabled(mapperFeature);
    }

    public boolean isUnknownTypeSerializer(f<?> fVar) {
        if (fVar == this._unknownTypeSerializer || fVar == null) {
            return true;
        }
        return isEnabled(SerializationFeature.FAIL_ON_EMPTY_BEANS) && fVar.getClass() == UnknownSerializer.class;
    }

    public JsonMappingException mappingException(String str, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        return JsonMappingException.from(getGenerator(), str);
    }

    public <T> T reportBadPropertyDefinition(so soVar, vo voVar, String str, Object... objArr) throws JsonMappingException {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        String str2 = org.web3j.utils.b.MISSING_REASON;
        String _quotedString = voVar == null ? org.web3j.utils.b.MISSING_REASON : _quotedString(voVar.t());
        if (soVar != null) {
            str2 = _desc(soVar.y().getGenericSignature());
        }
        throw mappingException("Invalid definition for property %s (of type %s): %s", _quotedString, str2, str);
    }

    public <T> T reportBadTypeDefinition(so soVar, String str, Object... objArr) throws JsonMappingException {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        throw mappingException("Invalid type definition for type %s: %s", soVar == null ? org.web3j.utils.b.MISSING_REASON : _desc(soVar.y().getGenericSignature()), str);
    }

    public void reportMappingProblem(String str, Object... objArr) throws JsonMappingException {
        throw mappingException(str, objArr);
    }

    public abstract f<Object> serializerInstance(ue ueVar, Object obj) throws JsonMappingException;

    public void setDefaultKeySerializer(f<Object> fVar) {
        if (fVar != null) {
            this._keySerializer = fVar;
            return;
        }
        throw new IllegalArgumentException("Can not pass null JsonSerializer");
    }

    public void setNullKeySerializer(f<Object> fVar) {
        if (fVar != null) {
            this._nullKeySerializer = fVar;
            return;
        }
        throw new IllegalArgumentException("Can not pass null JsonSerializer");
    }

    public void setNullValueSerializer(f<Object> fVar) {
        if (fVar != null) {
            this._nullValueSerializer = fVar;
            return;
        }
        throw new IllegalArgumentException("Can not pass null JsonSerializer");
    }

    @Override // com.fasterxml.jackson.databind.b
    public final SerializationConfig getConfig() {
        return this._config;
    }

    public final boolean isEnabled(SerializationFeature serializationFeature) {
        return this._config.isEnabled(serializationFeature);
    }

    public void reportMappingProblem(Throwable th, String str, Object... objArr) throws JsonMappingException {
        throw mappingException(th, str, objArr);
    }

    @Override // com.fasterxml.jackson.databind.b
    public k setAttribute(Object obj, Object obj2) {
        this._attributes = this._attributes.withPerCallAttribute(obj, obj2);
        return this;
    }

    public f<Object> findKeySerializer(Class<?> cls, a aVar) throws JsonMappingException {
        return findKeySerializer(this._config.constructType(cls), aVar);
    }

    public void defaultSerializeDateKey(Date date, JsonGenerator jsonGenerator) throws IOException {
        if (isEnabled(SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS)) {
            jsonGenerator.i0(String.valueOf(date.getTime()));
        } else {
            jsonGenerator.i0(_dateFormat().format(date));
        }
    }

    public final void defaultSerializeDateValue(Date date, JsonGenerator jsonGenerator) throws IOException {
        if (isEnabled(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)) {
            jsonGenerator.y0(date.getTime());
        } else {
            jsonGenerator.o1(_dateFormat().format(date));
        }
    }

    public JsonMappingException mappingException(Throwable th, String str, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        return JsonMappingException.from(getGenerator(), str, th);
    }

    public f<Object> _createAndCacheUntypedSerializer(JavaType javaType) throws JsonMappingException {
        try {
            f<Object> _createUntypedSerializer = _createUntypedSerializer(javaType);
            if (_createUntypedSerializer != null) {
                this._serializerCache.b(javaType, _createUntypedSerializer, this);
            }
            return _createUntypedSerializer;
        } catch (IllegalArgumentException e) {
            reportMappingProblem(e, e.getMessage(), new Object[0]);
            return null;
        }
    }

    public f<Object> findPrimaryPropertySerializer(Class<?> cls, a aVar) throws JsonMappingException {
        f<Object> f = this._knownSerializers.f(cls);
        if (f == null && (f = this._serializerCache.l(cls)) == null && (f = this._serializerCache.k(this._config.constructType(cls))) == null && (f = _createAndCacheUntypedSerializer(cls)) == null) {
            return getUnknownTypeSerializer(cls);
        }
        return handlePrimaryContextualization(f, aVar);
    }

    public f<Object> findValueSerializer(JavaType javaType, a aVar) throws JsonMappingException {
        f<Object> e = this._knownSerializers.e(javaType);
        if (e == null && (e = this._serializerCache.k(javaType)) == null && (e = _createAndCacheUntypedSerializer(javaType)) == null) {
            return getUnknownTypeSerializer(javaType.getRawClass());
        }
        return handleSecondaryContextualization(e, aVar);
    }

    public f<Object> findTypedValueSerializer(JavaType javaType, boolean z, a aVar) throws JsonMappingException {
        f<Object> c = this._knownSerializers.c(javaType);
        if (c != null) {
            return c;
        }
        f<Object> i = this._serializerCache.i(javaType);
        if (i != null) {
            return i;
        }
        f<Object> findValueSerializer = findValueSerializer(javaType, aVar);
        com.fasterxml.jackson.databind.jsontype.c createTypeSerializer = this._serializerFactory.createTypeSerializer(this._config, javaType);
        if (createTypeSerializer != null) {
            findValueSerializer = new com.fasterxml.jackson.databind.ser.impl.b(createTypeSerializer.a(aVar), findValueSerializer);
        }
        if (z) {
            this._serializerCache.d(javaType, findValueSerializer);
        }
        return findValueSerializer;
    }

    public k(k kVar, SerializationConfig serializationConfig, com.fasterxml.jackson.databind.ser.g gVar) {
        this._unknownTypeSerializer = DEFAULT_UNKNOWN_SERIALIZER;
        this._nullValueSerializer = NullSerializer.instance;
        f<Object> fVar = DEFAULT_NULL_KEY_SERIALIZER;
        this._nullKeySerializer = fVar;
        Objects.requireNonNull(serializationConfig);
        this._serializerFactory = gVar;
        this._config = serializationConfig;
        com.fasterxml.jackson.databind.ser.f fVar2 = kVar._serializerCache;
        this._serializerCache = fVar2;
        this._unknownTypeSerializer = kVar._unknownTypeSerializer;
        this._keySerializer = kVar._keySerializer;
        f<Object> fVar3 = kVar._nullValueSerializer;
        this._nullValueSerializer = fVar3;
        this._nullKeySerializer = kVar._nullKeySerializer;
        this._stdNullValueSerializer = fVar3 == fVar;
        this._serializationView = serializationConfig.getActiveView();
        this._attributes = serializationConfig.getAttributes();
        this._knownSerializers = fVar2.g();
    }

    public f<Object> findValueSerializer(Class<?> cls) throws JsonMappingException {
        f<Object> f = this._knownSerializers.f(cls);
        if (f == null) {
            f<Object> l = this._serializerCache.l(cls);
            if (l == null) {
                f<Object> k = this._serializerCache.k(this._config.constructType(cls));
                if (k == null) {
                    f<Object> _createAndCacheUntypedSerializer = _createAndCacheUntypedSerializer(cls);
                    return _createAndCacheUntypedSerializer == null ? getUnknownTypeSerializer(cls) : _createAndCacheUntypedSerializer;
                }
                return k;
            }
            return l;
        }
        return f;
    }

    public f<Object> findValueSerializer(JavaType javaType) throws JsonMappingException {
        f<Object> e = this._knownSerializers.e(javaType);
        if (e == null) {
            f<Object> k = this._serializerCache.k(javaType);
            if (k == null) {
                f<Object> _createAndCacheUntypedSerializer = _createAndCacheUntypedSerializer(javaType);
                return _createAndCacheUntypedSerializer == null ? getUnknownTypeSerializer(javaType.getRawClass()) : _createAndCacheUntypedSerializer;
            }
            return k;
        }
        return e;
    }

    public k(k kVar) {
        this._unknownTypeSerializer = DEFAULT_UNKNOWN_SERIALIZER;
        this._nullValueSerializer = NullSerializer.instance;
        this._nullKeySerializer = DEFAULT_NULL_KEY_SERIALIZER;
        this._config = null;
        this._serializationView = null;
        this._serializerFactory = null;
        this._knownSerializers = null;
        this._serializerCache = new com.fasterxml.jackson.databind.ser.f();
        this._unknownTypeSerializer = kVar._unknownTypeSerializer;
        this._keySerializer = kVar._keySerializer;
        this._nullValueSerializer = kVar._nullValueSerializer;
        this._nullKeySerializer = kVar._nullKeySerializer;
        this._stdNullValueSerializer = kVar._stdNullValueSerializer;
    }
}
