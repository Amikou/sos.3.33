package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.databind.e;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.Map;

/* compiled from: JsonNode.java */
/* loaded from: classes.dex */
public abstract class d extends e.a implements com.fasterxml.jackson.core.f, Iterable<d> {
    public final boolean B() {
        return y() == JsonNodeType.ARRAY;
    }

    public final boolean D() {
        return y() == JsonNodeType.BINARY;
    }

    public final boolean E() {
        return y() == JsonNodeType.NUMBER;
    }

    public final boolean F() {
        return y() == JsonNodeType.OBJECT;
    }

    public final boolean G() {
        return y() == JsonNodeType.POJO;
    }

    public long H() {
        return 0L;
    }

    public Number I() {
        return null;
    }

    public String J() {
        return null;
    }

    @Override // java.lang.Iterable
    public final Iterator<d> iterator() {
        return s();
    }

    public abstract String m();

    public BigInteger n() {
        return BigInteger.ZERO;
    }

    public byte[] o() throws IOException {
        return null;
    }

    public BigDecimal p() {
        return BigDecimal.ZERO;
    }

    public double q() {
        return Utils.DOUBLE_EPSILON;
    }

    public Iterator<d> s() {
        return com.fasterxml.jackson.databind.util.c.k();
    }

    public Iterator<Map.Entry<String, d>> t() {
        return com.fasterxml.jackson.databind.util.c.k();
    }

    public abstract String toString();

    public d w(String str) {
        return null;
    }

    public abstract JsonNodeType y();

    public int z() {
        return 0;
    }
}
