package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.impl.BeanAsArraySerializer;
import com.fasterxml.jackson.databind.ser.impl.UnwrappingBeanSerializer;
import com.fasterxml.jackson.databind.ser.std.BeanSerializerBase;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.IOException;
import java.util.Set;

/* loaded from: classes.dex */
public class BeanSerializer extends BeanSerializerBase {
    private static final long serialVersionUID = -3618164443537292758L;

    public BeanSerializer(JavaType javaType, wo woVar, BeanPropertyWriter[] beanPropertyWriterArr, BeanPropertyWriter[] beanPropertyWriterArr2) {
        super(javaType, woVar, beanPropertyWriterArr, beanPropertyWriterArr2);
    }

    public static BeanSerializer createDummy(JavaType javaType) {
        return new BeanSerializer(javaType, null, BeanSerializerBase.NO_PROPS, null);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.BeanSerializerBase
    public BeanSerializerBase asArraySerializer() {
        return (this._objectIdWriter == null && this._anyGetterWriter == null && this._propertyFilterId == null) ? new BeanAsArraySerializer(this) : this;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.BeanSerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (this._objectIdWriter != null) {
            jsonGenerator.u(obj);
            _serializeWithObjectId(obj, jsonGenerator, kVar, true);
            return;
        }
        jsonGenerator.k1(obj);
        if (this._propertyFilterId != null) {
            serializeFieldsFiltered(obj, jsonGenerator, kVar);
        } else {
            serializeFields(obj, jsonGenerator, kVar);
        }
        jsonGenerator.f0();
    }

    public String toString() {
        return "BeanSerializer for " + handledType().getName();
    }

    @Override // com.fasterxml.jackson.databind.f
    public com.fasterxml.jackson.databind.f<Object> unwrappingSerializer(NameTransformer nameTransformer) {
        return new UnwrappingBeanSerializer(this, nameTransformer);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.BeanSerializerBase
    public BeanSerializerBase withIgnorals(Set<String> set) {
        return new BeanSerializer(this, set);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.BeanSerializerBase
    public BeanSerializerBase withObjectIdWriter(kl2 kl2Var) {
        return new BeanSerializer(this, kl2Var, this._propertyFilterId);
    }

    public BeanSerializer(BeanSerializerBase beanSerializerBase) {
        super(beanSerializerBase);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.BeanSerializerBase, com.fasterxml.jackson.databind.f
    public BeanSerializerBase withFilterId(Object obj) {
        return new BeanSerializer(this, this._objectIdWriter, obj);
    }

    public BeanSerializer(BeanSerializerBase beanSerializerBase, kl2 kl2Var) {
        super(beanSerializerBase, kl2Var);
    }

    public BeanSerializer(BeanSerializerBase beanSerializerBase, kl2 kl2Var, Object obj) {
        super(beanSerializerBase, kl2Var, obj);
    }

    public BeanSerializer(BeanSerializerBase beanSerializerBase, Set<String> set) {
        super(beanSerializerBase, set);
    }
}
