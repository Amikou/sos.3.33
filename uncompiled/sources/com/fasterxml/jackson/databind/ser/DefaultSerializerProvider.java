package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public abstract class DefaultSerializerProvider extends k implements Serializable {
    private static final long serialVersionUID = 1;
    public transient JsonGenerator _generator;
    public transient ArrayList<ObjectIdGenerator<?>> _objectIdGenerators;
    public transient Map<Object, com.fasterxml.jackson.databind.ser.impl.c> _seenObjectIds;

    /* loaded from: classes.dex */
    public static final class Impl extends DefaultSerializerProvider {
        private static final long serialVersionUID = 1;

        public Impl() {
        }

        @Override // com.fasterxml.jackson.databind.ser.DefaultSerializerProvider
        public DefaultSerializerProvider copy() {
            if (Impl.class != Impl.class) {
                return super.copy();
            }
            return new Impl(this);
        }

        public Impl(Impl impl) {
            super(impl);
        }

        @Override // com.fasterxml.jackson.databind.ser.DefaultSerializerProvider
        public Impl createInstance(SerializationConfig serializationConfig, g gVar) {
            return new Impl(this, serializationConfig, gVar);
        }

        public Impl(k kVar, SerializationConfig serializationConfig, g gVar) {
            super(kVar, serializationConfig, gVar);
        }
    }

    public DefaultSerializerProvider() {
    }

    public Map<Object, com.fasterxml.jackson.databind.ser.impl.c> _createObjectIdMap() {
        if (isEnabled(SerializationFeature.USE_EQUALITY_FOR_OBJECT_ID)) {
            return new HashMap();
        }
        return new IdentityHashMap();
    }

    public void _serializeNull(JsonGenerator jsonGenerator) throws IOException {
        try {
            getDefaultNullValueSerializer().serialize(null, jsonGenerator, this);
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            String message = e2.getMessage();
            if (message == null) {
                message = "[no message for " + e2.getClass().getName() + "]";
            }
            reportMappingProblem(e2, message, new Object[0]);
        }
    }

    public void acceptJsonFormatVisitor(JavaType javaType, com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar) throws JsonMappingException {
        if (javaType != null) {
            bVar.h(this);
            findValueSerializer(javaType, (com.fasterxml.jackson.databind.a) null).acceptJsonFormatVisitor(bVar, javaType);
            return;
        }
        throw new IllegalArgumentException("A class must be provided");
    }

    public int cachedSerializersCount() {
        return this._serializerCache.h();
    }

    public DefaultSerializerProvider copy() {
        throw new IllegalStateException("DefaultSerializerProvider sub-class not overriding copy()");
    }

    public abstract DefaultSerializerProvider createInstance(SerializationConfig serializationConfig, g gVar);

    @Override // com.fasterxml.jackson.databind.k
    public com.fasterxml.jackson.databind.ser.impl.c findObjectId(Object obj, ObjectIdGenerator<?> objectIdGenerator) {
        Map<Object, com.fasterxml.jackson.databind.ser.impl.c> map = this._seenObjectIds;
        if (map == null) {
            this._seenObjectIds = _createObjectIdMap();
        } else {
            com.fasterxml.jackson.databind.ser.impl.c cVar = map.get(obj);
            if (cVar != null) {
                return cVar;
            }
        }
        ObjectIdGenerator<?> objectIdGenerator2 = null;
        ArrayList<ObjectIdGenerator<?>> arrayList = this._objectIdGenerators;
        if (arrayList == null) {
            this._objectIdGenerators = new ArrayList<>(8);
        } else {
            int i = 0;
            int size = arrayList.size();
            while (true) {
                if (i >= size) {
                    break;
                }
                ObjectIdGenerator<?> objectIdGenerator3 = this._objectIdGenerators.get(i);
                if (objectIdGenerator3.canUseFor(objectIdGenerator)) {
                    objectIdGenerator2 = objectIdGenerator3;
                    break;
                }
                i++;
            }
        }
        if (objectIdGenerator2 == null) {
            objectIdGenerator2 = objectIdGenerator.newForSerialization(this);
            this._objectIdGenerators.add(objectIdGenerator2);
        }
        com.fasterxml.jackson.databind.ser.impl.c cVar2 = new com.fasterxml.jackson.databind.ser.impl.c(objectIdGenerator2);
        this._seenObjectIds.put(obj, cVar2);
        return cVar2;
    }

    public void flushCachedSerializers() {
        this._serializerCache.f();
    }

    @Deprecated
    public aw1 generateJsonSchema(Class<?> cls) throws JsonMappingException {
        if (cls != null) {
            com.fasterxml.jackson.databind.f<Object> findValueSerializer = findValueSerializer(cls, (com.fasterxml.jackson.databind.a) null);
            com.fasterxml.jackson.databind.d schema = findValueSerializer instanceof ld3 ? ((ld3) findValueSerializer).getSchema(this, null) : aw1.a();
            if (schema instanceof m) {
                return new aw1((m) schema);
            }
            throw new IllegalArgumentException("Class " + cls.getName() + " would not be serialized as a JSON object and therefore has no schema");
        }
        throw new IllegalArgumentException("A class must be provided");
    }

    @Override // com.fasterxml.jackson.databind.k
    public JsonGenerator getGenerator() {
        return this._generator;
    }

    public boolean hasSerializerFor(Class<?> cls, AtomicReference<Throwable> atomicReference) {
        if (cls != Object.class || this._config.isEnabled(SerializationFeature.FAIL_ON_EMPTY_BEANS)) {
            try {
                return _findExplicitUntypedSerializer(cls) != null;
            } catch (JsonMappingException e) {
                if (atomicReference != null) {
                    atomicReference.set(e);
                }
                return false;
            } catch (RuntimeException e2) {
                if (atomicReference != null) {
                    atomicReference.set(e2);
                    return false;
                }
                throw e2;
            }
        }
        return true;
    }

    public void serializePolymorphic(JsonGenerator jsonGenerator, Object obj, JavaType javaType, com.fasterxml.jackson.databind.f<Object> fVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        boolean z;
        this._generator = jsonGenerator;
        if (obj == null) {
            _serializeNull(jsonGenerator);
            return;
        }
        if (javaType != null && !javaType.getRawClass().isAssignableFrom(obj.getClass())) {
            _reportIncompatibleRootType(obj, javaType);
        }
        if (fVar == null) {
            if (javaType != null && javaType.isContainerType()) {
                fVar = findValueSerializer(javaType, (com.fasterxml.jackson.databind.a) null);
            } else {
                fVar = findValueSerializer(obj.getClass(), (com.fasterxml.jackson.databind.a) null);
            }
        }
        PropertyName fullRootName = this._config.getFullRootName();
        if (fullRootName == null) {
            z = this._config.isEnabled(SerializationFeature.WRAP_ROOT_VALUE);
            if (z) {
                jsonGenerator.i1();
                jsonGenerator.g0(this._config.findRootName(obj.getClass()).simpleAsEncoded(this._config));
            }
        } else if (fullRootName.isEmpty()) {
            z = false;
        } else {
            jsonGenerator.i1();
            jsonGenerator.i0(fullRootName.getSimpleName());
            z = true;
        }
        try {
            fVar.serializeWithType(obj, jsonGenerator, this, cVar);
            if (z) {
                jsonGenerator.f0();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            String message = e2.getMessage();
            if (message == null) {
                message = "[no message for " + e2.getClass().getName() + "]";
            }
            reportMappingProblem(e2, message, new Object[0]);
        }
    }

    public void serializeValue(JsonGenerator jsonGenerator, Object obj) throws IOException {
        this._generator = jsonGenerator;
        if (obj == null) {
            _serializeNull(jsonGenerator);
            return;
        }
        boolean z = true;
        com.fasterxml.jackson.databind.f<Object> findTypedValueSerializer = findTypedValueSerializer(obj.getClass(), true, (com.fasterxml.jackson.databind.a) null);
        PropertyName fullRootName = this._config.getFullRootName();
        if (fullRootName == null) {
            z = this._config.isEnabled(SerializationFeature.WRAP_ROOT_VALUE);
            if (z) {
                jsonGenerator.i1();
                jsonGenerator.g0(this._config.findRootName(obj.getClass()).simpleAsEncoded(this._config));
            }
        } else if (fullRootName.isEmpty()) {
            z = false;
        } else {
            jsonGenerator.i1();
            jsonGenerator.i0(fullRootName.getSimpleName());
        }
        try {
            findTypedValueSerializer.serialize(obj, jsonGenerator, this);
            if (z) {
                jsonGenerator.f0();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            String message = e2.getMessage();
            if (message == null) {
                message = "[no message for " + e2.getClass().getName() + "]";
            }
            throw new JsonMappingException(jsonGenerator, message, e2);
        }
    }

    @Override // com.fasterxml.jackson.databind.k
    public com.fasterxml.jackson.databind.f<Object> serializerInstance(ue ueVar, Object obj) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<?> fVar;
        if (obj == null) {
            return null;
        }
        if (obj instanceof com.fasterxml.jackson.databind.f) {
            fVar = (com.fasterxml.jackson.databind.f) obj;
        } else if (obj instanceof Class) {
            Class cls = (Class) obj;
            if (cls == f.a.class || com.fasterxml.jackson.databind.util.c.G(cls)) {
                return null;
            }
            if (com.fasterxml.jackson.databind.f.class.isAssignableFrom(cls)) {
                this._config.getHandlerInstantiator();
                fVar = (com.fasterxml.jackson.databind.f) com.fasterxml.jackson.databind.util.c.i(cls, this._config.canOverrideAccessModifiers());
            } else {
                throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonSerializer>");
            }
        } else {
            throw new IllegalStateException("AnnotationIntrospector returned serializer definition of type " + obj.getClass().getName() + "; expected type JsonSerializer or Class<JsonSerializer> instead");
        }
        return _handleResolvable(fVar);
    }

    public DefaultSerializerProvider(k kVar, SerializationConfig serializationConfig, g gVar) {
        super(kVar, serializationConfig, gVar);
    }

    public DefaultSerializerProvider(DefaultSerializerProvider defaultSerializerProvider) {
        super(defaultSerializerProvider);
    }

    public void serializeValue(JsonGenerator jsonGenerator, Object obj, JavaType javaType) throws IOException {
        this._generator = jsonGenerator;
        if (obj == null) {
            _serializeNull(jsonGenerator);
            return;
        }
        if (!javaType.getRawClass().isAssignableFrom(obj.getClass())) {
            _reportIncompatibleRootType(obj, javaType);
        }
        boolean z = true;
        com.fasterxml.jackson.databind.f<Object> findTypedValueSerializer = findTypedValueSerializer(javaType, true, (com.fasterxml.jackson.databind.a) null);
        PropertyName fullRootName = this._config.getFullRootName();
        if (fullRootName == null) {
            z = this._config.isEnabled(SerializationFeature.WRAP_ROOT_VALUE);
            if (z) {
                jsonGenerator.i1();
                jsonGenerator.g0(this._config.findRootName(obj.getClass()).simpleAsEncoded(this._config));
            }
        } else if (fullRootName.isEmpty()) {
            z = false;
        } else {
            jsonGenerator.i1();
            jsonGenerator.i0(fullRootName.getSimpleName());
        }
        try {
            findTypedValueSerializer.serialize(obj, jsonGenerator, this);
            if (z) {
                jsonGenerator.f0();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            String message = e2.getMessage();
            if (message == null) {
                message = "[no message for " + e2.getClass().getName() + "]";
            }
            reportMappingProblem(e2, message, new Object[0]);
        }
    }

    @Deprecated
    public void serializePolymorphic(JsonGenerator jsonGenerator, Object obj, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        serializePolymorphic(jsonGenerator, obj, obj == null ? null : this._config.constructType(obj.getClass()), null, cVar);
    }

    public void serializeValue(JsonGenerator jsonGenerator, Object obj, JavaType javaType, com.fasterxml.jackson.databind.f<Object> fVar) throws IOException {
        PropertyName findRootName;
        this._generator = jsonGenerator;
        if (obj == null) {
            _serializeNull(jsonGenerator);
            return;
        }
        if (javaType != null && !javaType.getRawClass().isAssignableFrom(obj.getClass())) {
            _reportIncompatibleRootType(obj, javaType);
        }
        boolean z = true;
        if (fVar == null) {
            fVar = findTypedValueSerializer(javaType, true, (com.fasterxml.jackson.databind.a) null);
        }
        PropertyName fullRootName = this._config.getFullRootName();
        if (fullRootName == null) {
            z = this._config.isEnabled(SerializationFeature.WRAP_ROOT_VALUE);
            if (z) {
                jsonGenerator.i1();
                if (javaType == null) {
                    findRootName = this._config.findRootName(obj.getClass());
                } else {
                    findRootName = this._config.findRootName(javaType);
                }
                jsonGenerator.g0(findRootName.simpleAsEncoded(this._config));
            }
        } else if (fullRootName.isEmpty()) {
            z = false;
        } else {
            jsonGenerator.i1();
            jsonGenerator.i0(fullRootName.getSimpleName());
        }
        try {
            fVar.serialize(obj, jsonGenerator, this);
            if (z) {
                jsonGenerator.f0();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            String message = e2.getMessage();
            if (message == null) {
                message = "[no message for " + e2.getClass().getName() + "]";
            }
            reportMappingProblem(e2, message, new Object[0]);
        }
    }
}
