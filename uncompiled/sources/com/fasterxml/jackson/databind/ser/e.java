package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.k;

/* compiled from: ResolvableSerializer.java */
/* loaded from: classes.dex */
public interface e {
    void resolve(k kVar) throws JsonMappingException;
}
