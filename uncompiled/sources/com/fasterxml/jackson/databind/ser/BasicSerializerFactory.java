package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.cfg.SerializerFactoryConfig;
import com.fasterxml.jackson.databind.ext.OptionalHandlerFactory;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.impl.IndexedListSerializer;
import com.fasterxml.jackson.databind.ser.impl.IndexedStringListSerializer;
import com.fasterxml.jackson.databind.ser.impl.IteratorSerializer;
import com.fasterxml.jackson.databind.ser.impl.MapEntrySerializer;
import com.fasterxml.jackson.databind.ser.impl.StringArraySerializer;
import com.fasterxml.jackson.databind.ser.impl.StringCollectionSerializer;
import com.fasterxml.jackson.databind.ser.std.BooleanSerializer;
import com.fasterxml.jackson.databind.ser.std.ByteBufferSerializer;
import com.fasterxml.jackson.databind.ser.std.CalendarSerializer;
import com.fasterxml.jackson.databind.ser.std.CollectionSerializer;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.databind.ser.std.EnumSerializer;
import com.fasterxml.jackson.databind.ser.std.EnumSetSerializer;
import com.fasterxml.jackson.databind.ser.std.InetAddressSerializer;
import com.fasterxml.jackson.databind.ser.std.InetSocketAddressSerializer;
import com.fasterxml.jackson.databind.ser.std.IterableSerializer;
import com.fasterxml.jackson.databind.ser.std.JsonValueSerializer;
import com.fasterxml.jackson.databind.ser.std.MapSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer;
import com.fasterxml.jackson.databind.ser.std.SerializableSerializer;
import com.fasterxml.jackson.databind.ser.std.StdArraySerializers;
import com.fasterxml.jackson.databind.ser.std.StdDelegatingSerializer;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import com.fasterxml.jackson.databind.ser.std.TimeZoneSerializer;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.databind.ser.std.TokenBufferSerializer;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.RandomAccess;
import java.util.TimeZone;

/* loaded from: classes.dex */
public abstract class BasicSerializerFactory extends g implements Serializable {
    public static final HashMap<String, com.fasterxml.jackson.databind.f<?>> _concrete;
    public static final HashMap<String, Class<? extends com.fasterxml.jackson.databind.f<?>>> _concreteLazy;
    public final SerializerFactoryConfig _factoryConfig;

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[JsonInclude.Include.values().length];
            b = iArr;
            try {
                iArr[JsonInclude.Include.USE_DEFAULTS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[JsonInclude.Include.NON_DEFAULT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[JsonFormat.Shape.values().length];
            a = iArr2;
            try {
                iArr2[JsonFormat.Shape.STRING.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JsonFormat.Shape.OBJECT.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[JsonFormat.Shape.ARRAY.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    static {
        HashMap<String, Class<? extends com.fasterxml.jackson.databind.f<?>>> hashMap = new HashMap<>();
        HashMap<String, com.fasterxml.jackson.databind.f<?>> hashMap2 = new HashMap<>();
        hashMap2.put(String.class.getName(), new StringSerializer());
        ToStringSerializer toStringSerializer = ToStringSerializer.instance;
        hashMap2.put(StringBuffer.class.getName(), toStringSerializer);
        hashMap2.put(StringBuilder.class.getName(), toStringSerializer);
        hashMap2.put(Character.class.getName(), toStringSerializer);
        hashMap2.put(Character.TYPE.getName(), toStringSerializer);
        NumberSerializers.a(hashMap2);
        hashMap2.put(Boolean.TYPE.getName(), new BooleanSerializer(true));
        hashMap2.put(Boolean.class.getName(), new BooleanSerializer(false));
        hashMap2.put(BigInteger.class.getName(), new NumberSerializer(BigInteger.class));
        hashMap2.put(BigDecimal.class.getName(), new NumberSerializer(BigDecimal.class));
        hashMap2.put(Calendar.class.getName(), CalendarSerializer.instance);
        hashMap2.put(Date.class.getName(), DateSerializer.instance);
        for (Map.Entry<Class<?>, Object> entry : StdJdkSerializers.a()) {
            Object value = entry.getValue();
            if (value instanceof com.fasterxml.jackson.databind.f) {
                hashMap2.put(entry.getKey().getName(), (com.fasterxml.jackson.databind.f) value);
            } else if (value instanceof Class) {
                hashMap.put(entry.getKey().getName(), (Class) value);
            } else {
                throw new IllegalStateException("Internal error: unrecognized value of type " + entry.getClass().getName());
            }
        }
        hashMap.put(com.fasterxml.jackson.databind.util.e.class.getName(), TokenBufferSerializer.class);
        _concrete = hashMap2;
        _concreteLazy = hashMap;
    }

    public BasicSerializerFactory(SerializerFactoryConfig serializerFactoryConfig) {
        this._factoryConfig = serializerFactoryConfig == null ? new SerializerFactoryConfig() : serializerFactoryConfig;
    }

    public com.fasterxml.jackson.databind.f<Object> _findContentSerializer(k kVar, ue ueVar) throws JsonMappingException {
        Object findContentSerializer = kVar.getAnnotationIntrospector().findContentSerializer(ueVar);
        if (findContentSerializer != null) {
            return kVar.serializerInstance(ueVar, findContentSerializer);
        }
        return null;
    }

    public com.fasterxml.jackson.databind.f<Object> _findKeySerializer(k kVar, ue ueVar) throws JsonMappingException {
        Object findKeySerializer = kVar.getAnnotationIntrospector().findKeySerializer(ueVar);
        if (findKeySerializer != null) {
            return kVar.serializerInstance(ueVar, findKeySerializer);
        }
        return null;
    }

    public Class<?> _verifyAsClass(Object obj, String str, Class<?> cls) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Class) {
            Class<?> cls2 = (Class) obj;
            if (cls2 == cls || com.fasterxml.jackson.databind.util.c.G(cls2)) {
                return null;
            }
            return cls2;
        }
        throw new IllegalStateException("AnnotationIntrospector." + str + "() returned value of type " + obj.getClass().getName() + ": expected type JsonSerializer or Class<JsonSerializer> instead");
    }

    public com.fasterxml.jackson.databind.f<?> buildArraySerializer(k kVar, ArrayType arrayType, so soVar, boolean z, com.fasterxml.jackson.databind.jsontype.c cVar, com.fasterxml.jackson.databind.f<Object> fVar) throws JsonMappingException {
        SerializationConfig config = kVar.getConfig();
        Iterator<am3> it = customSerializers().iterator();
        com.fasterxml.jackson.databind.f<?> fVar2 = null;
        while (it.hasNext() && (fVar2 = it.next().findArraySerializer(config, arrayType, soVar, cVar, fVar)) == null) {
        }
        if (fVar2 == null) {
            Class<?> rawClass = arrayType.getRawClass();
            if (fVar == null || com.fasterxml.jackson.databind.util.c.J(fVar)) {
                if (String[].class == rawClass) {
                    fVar2 = StringArraySerializer.instance;
                } else {
                    fVar2 = StdArraySerializers.a(rawClass);
                }
            }
            if (fVar2 == null) {
                fVar2 = new ObjectArraySerializer(arrayType.getContentType(), z, cVar, fVar);
            }
        }
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (xo xoVar : this._factoryConfig.serializerModifiers()) {
                fVar2 = xoVar.b(config, arrayType, soVar, fVar2);
            }
        }
        return fVar2;
    }

    public com.fasterxml.jackson.databind.f<?> buildCollectionSerializer(k kVar, CollectionType collectionType, so soVar, boolean z, com.fasterxml.jackson.databind.jsontype.c cVar, com.fasterxml.jackson.databind.f<Object> fVar) throws JsonMappingException {
        SerializationConfig config = kVar.getConfig();
        Iterator<am3> it = customSerializers().iterator();
        com.fasterxml.jackson.databind.f<?> fVar2 = null;
        while (it.hasNext() && (fVar2 = it.next().findCollectionSerializer(config, collectionType, soVar, cVar, fVar)) == null) {
        }
        if (fVar2 == null && (fVar2 = findSerializerByAnnotations(kVar, collectionType, soVar)) == null) {
            JsonFormat.Value g = soVar.g(null);
            if (g != null && g.getShape() == JsonFormat.Shape.OBJECT) {
                return null;
            }
            Class<?> rawClass = collectionType.getRawClass();
            if (EnumSet.class.isAssignableFrom(rawClass)) {
                JavaType contentType = collectionType.getContentType();
                fVar2 = buildEnumSetSerializer(contentType.isEnumType() ? contentType : null);
            } else {
                Class<?> rawClass2 = collectionType.getContentType().getRawClass();
                if (isIndexedList(rawClass)) {
                    if (rawClass2 == String.class) {
                        if (fVar == null || com.fasterxml.jackson.databind.util.c.J(fVar)) {
                            fVar2 = IndexedStringListSerializer.instance;
                        }
                    } else {
                        fVar2 = buildIndexedListSerializer(collectionType.getContentType(), z, cVar, fVar);
                    }
                } else if (rawClass2 == String.class && (fVar == null || com.fasterxml.jackson.databind.util.c.J(fVar))) {
                    fVar2 = StringCollectionSerializer.instance;
                }
                if (fVar2 == null) {
                    fVar2 = buildCollectionSerializer(collectionType.getContentType(), z, cVar, fVar);
                }
            }
        }
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (xo xoVar : this._factoryConfig.serializerModifiers()) {
                fVar2 = xoVar.d(config, collectionType, soVar, fVar2);
            }
        }
        return fVar2;
    }

    public com.fasterxml.jackson.databind.f<?> buildContainerSerializer(k kVar, JavaType javaType, so soVar, boolean z) throws JsonMappingException {
        so soVar2;
        so soVar3 = soVar;
        SerializationConfig config = kVar.getConfig();
        boolean z2 = (z || !javaType.useStaticType() || (javaType.isContainerType() && javaType.getContentType().getRawClass() == Object.class)) ? z : true;
        com.fasterxml.jackson.databind.jsontype.c createTypeSerializer = createTypeSerializer(config, javaType.getContentType());
        if (createTypeSerializer != null) {
            z2 = false;
        }
        boolean z3 = z2;
        com.fasterxml.jackson.databind.f<Object> _findContentSerializer = _findContentSerializer(kVar, soVar.t());
        com.fasterxml.jackson.databind.f<?> fVar = null;
        if (javaType.isMapLikeType()) {
            MapLikeType mapLikeType = (MapLikeType) javaType;
            com.fasterxml.jackson.databind.f<Object> _findKeySerializer = _findKeySerializer(kVar, soVar.t());
            if (mapLikeType.isTrueMapType()) {
                return buildMapSerializer(kVar, (MapType) mapLikeType, soVar, z3, _findKeySerializer, createTypeSerializer, _findContentSerializer);
            }
            Iterator<am3> it = customSerializers().iterator();
            while (it.hasNext() && (fVar = it.next().findMapLikeSerializer(config, mapLikeType, soVar, _findKeySerializer, createTypeSerializer, _findContentSerializer)) == null) {
            }
            if (fVar == null) {
                fVar = findSerializerByAnnotations(kVar, javaType, soVar);
            }
            if (fVar != null && this._factoryConfig.hasSerializerModifiers()) {
                for (xo xoVar : this._factoryConfig.serializerModifiers()) {
                    fVar = xoVar.g(config, mapLikeType, soVar3, fVar);
                }
            }
            return fVar;
        } else if (javaType.isCollectionLikeType()) {
            CollectionLikeType collectionLikeType = (CollectionLikeType) javaType;
            if (collectionLikeType.isTrueCollectionType()) {
                return buildCollectionSerializer(kVar, (CollectionType) collectionLikeType, soVar, z3, createTypeSerializer, _findContentSerializer);
            }
            Iterator<am3> it2 = customSerializers().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    soVar2 = soVar3;
                    break;
                }
                soVar2 = soVar3;
                fVar = it2.next().findCollectionLikeSerializer(config, collectionLikeType, soVar, createTypeSerializer, _findContentSerializer);
                if (fVar != null) {
                    break;
                }
                soVar3 = soVar2;
            }
            if (fVar == null) {
                fVar = findSerializerByAnnotations(kVar, javaType, soVar);
            }
            if (fVar != null && this._factoryConfig.hasSerializerModifiers()) {
                for (xo xoVar2 : this._factoryConfig.serializerModifiers()) {
                    fVar = xoVar2.c(config, collectionLikeType, soVar2, fVar);
                }
            }
            return fVar;
        } else if (javaType.isArrayType()) {
            return buildArraySerializer(kVar, (ArrayType) javaType, soVar, z3, createTypeSerializer, _findContentSerializer);
        } else {
            return null;
        }
    }

    public com.fasterxml.jackson.databind.f<?> buildEnumSerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar) throws JsonMappingException {
        JsonFormat.Value g = soVar.g(null);
        if (g != null && g.getShape() == JsonFormat.Shape.OBJECT) {
            ((com.fasterxml.jackson.databind.introspect.b) soVar).L("declaringClass");
            return null;
        }
        com.fasterxml.jackson.databind.f<?> construct = EnumSerializer.construct(javaType.getRawClass(), serializationConfig, soVar, g);
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (xo xoVar : this._factoryConfig.serializerModifiers()) {
                construct = xoVar.e(serializationConfig, javaType, soVar, construct);
            }
        }
        return construct;
    }

    public com.fasterxml.jackson.databind.f<?> buildEnumSetSerializer(JavaType javaType) {
        return new EnumSetSerializer(javaType);
    }

    public ContainerSerializer<?> buildIndexedListSerializer(JavaType javaType, boolean z, com.fasterxml.jackson.databind.jsontype.c cVar, com.fasterxml.jackson.databind.f<Object> fVar) {
        return new IndexedListSerializer(javaType, z, cVar, fVar);
    }

    public com.fasterxml.jackson.databind.f<?> buildIterableSerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar, boolean z, JavaType javaType2) throws JsonMappingException {
        return new IterableSerializer(javaType2, z, createTypeSerializer(serializationConfig, javaType2));
    }

    public com.fasterxml.jackson.databind.f<?> buildIteratorSerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar, boolean z, JavaType javaType2) throws JsonMappingException {
        return new IteratorSerializer(javaType2, z, createTypeSerializer(serializationConfig, javaType2));
    }

    public com.fasterxml.jackson.databind.f<?> buildMapEntrySerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar, boolean z, JavaType javaType2, JavaType javaType3) throws JsonMappingException {
        return new MapEntrySerializer(javaType3, javaType2, javaType3, z, createTypeSerializer(serializationConfig, javaType3), null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1 */
    /* JADX WARN: Type inference failed for: r1v11, types: [com.fasterxml.jackson.databind.ser.std.MapSerializer] */
    /* JADX WARN: Type inference failed for: r1v12, types: [com.fasterxml.jackson.databind.ser.std.MapSerializer] */
    /* JADX WARN: Type inference failed for: r1v15, types: [com.fasterxml.jackson.databind.f] */
    /* JADX WARN: Type inference failed for: r1v16 */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5, types: [com.fasterxml.jackson.databind.f<?>] */
    /* JADX WARN: Type inference failed for: r1v6, types: [com.fasterxml.jackson.databind.f] */
    /* JADX WARN: Type inference failed for: r1v8, types: [com.fasterxml.jackson.databind.f] */
    /* JADX WARN: Type inference failed for: r3v2, types: [xo] */
    public com.fasterxml.jackson.databind.f<?> buildMapSerializer(k kVar, MapType mapType, so soVar, boolean z, com.fasterxml.jackson.databind.f<Object> fVar, com.fasterxml.jackson.databind.jsontype.c cVar, com.fasterxml.jackson.databind.f<Object> fVar2) throws JsonMappingException {
        SerializationConfig config = kVar.getConfig();
        com.fasterxml.jackson.databind.f<?> fVar3 = 0;
        for (am3 am3Var : customSerializers()) {
            fVar3 = am3Var.findMapSerializer(config, mapType, soVar, fVar, cVar, fVar2);
            fVar3 = fVar3;
            if (fVar3 != 0) {
                break;
            }
        }
        if (fVar3 == 0 && (fVar3 = findSerializerByAnnotations(kVar, mapType, soVar)) == 0) {
            Object findFilterId = findFilterId(config, soVar);
            JsonIgnoreProperties.Value defaultPropertyIgnorals = config.getDefaultPropertyIgnorals(Map.class, soVar.t());
            fVar3 = MapSerializer.construct(defaultPropertyIgnorals != null ? defaultPropertyIgnorals.findIgnoredForSerialization() : null, mapType, z, cVar, fVar, fVar2, findFilterId);
            Object findSuppressableContentValue = findSuppressableContentValue(config, mapType.getContentType(), soVar);
            if (findSuppressableContentValue != null) {
                fVar3 = fVar3.withContentInclusion(findSuppressableContentValue);
            }
        }
        if (this._factoryConfig.hasSerializerModifiers()) {
            fVar3 = fVar3;
            for (xo xoVar : this._factoryConfig.serializerModifiers()) {
                fVar3 = xoVar.h(config, mapType, soVar, fVar3);
            }
        }
        return fVar3;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:23:0x007a  */
    @Override // com.fasterxml.jackson.databind.ser.g
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.f<java.lang.Object> createKeySerializer(com.fasterxml.jackson.databind.SerializationConfig r5, com.fasterxml.jackson.databind.JavaType r6, com.fasterxml.jackson.databind.f<java.lang.Object> r7) {
        /*
            r4 = this;
            java.lang.Class r0 = r6.getRawClass()
            so r0 = r5.introspectClassAnnotations(r0)
            com.fasterxml.jackson.databind.cfg.SerializerFactoryConfig r1 = r4._factoryConfig
            boolean r1 = r1.hasKeySerializers()
            r2 = 0
            if (r1 == 0) goto L2d
            com.fasterxml.jackson.databind.cfg.SerializerFactoryConfig r1 = r4._factoryConfig
            java.lang.Iterable r1 = r1.keySerializers()
            java.util.Iterator r1 = r1.iterator()
        L1b:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L2d
            java.lang.Object r2 = r1.next()
            am3 r2 = (defpackage.am3) r2
            com.fasterxml.jackson.databind.f r2 = r2.findSerializer(r5, r6, r0)
            if (r2 == 0) goto L1b
        L2d:
            if (r2 != 0) goto L71
            if (r7 != 0) goto L72
            java.lang.Class r7 = r6.getRawClass()
            r1 = 0
            com.fasterxml.jackson.databind.f r7 = com.fasterxml.jackson.databind.ser.std.StdKeySerializers.b(r5, r7, r1)
            if (r7 != 0) goto L72
            so r0 = r5.introspect(r6)
            com.fasterxml.jackson.databind.introspect.AnnotatedMethod r7 = r0.j()
            if (r7 == 0) goto L68
            java.lang.Class r1 = r7.getRawReturnType()
            r2 = 1
            com.fasterxml.jackson.databind.f r1 = com.fasterxml.jackson.databind.ser.std.StdKeySerializers.b(r5, r1, r2)
            java.lang.reflect.Method r2 = r7.getAnnotated()
            boolean r3 = r5.canOverrideAccessModifiers()
            if (r3 == 0) goto L62
            com.fasterxml.jackson.databind.MapperFeature r3 = com.fasterxml.jackson.databind.MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS
            boolean r3 = r5.isEnabled(r3)
            com.fasterxml.jackson.databind.util.c.f(r2, r3)
        L62:
            com.fasterxml.jackson.databind.ser.std.JsonValueSerializer r2 = new com.fasterxml.jackson.databind.ser.std.JsonValueSerializer
            r2.<init>(r7, r1)
            goto L71
        L68:
            java.lang.Class r7 = r6.getRawClass()
            com.fasterxml.jackson.databind.f r7 = com.fasterxml.jackson.databind.ser.std.StdKeySerializers.a(r5, r7)
            goto L72
        L71:
            r7 = r2
        L72:
            com.fasterxml.jackson.databind.cfg.SerializerFactoryConfig r1 = r4._factoryConfig
            boolean r1 = r1.hasSerializerModifiers()
            if (r1 == 0) goto L95
            com.fasterxml.jackson.databind.cfg.SerializerFactoryConfig r1 = r4._factoryConfig
            java.lang.Iterable r1 = r1.serializerModifiers()
            java.util.Iterator r1 = r1.iterator()
        L84:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L95
            java.lang.Object r2 = r1.next()
            xo r2 = (defpackage.xo) r2
            com.fasterxml.jackson.databind.f r7 = r2.f(r5, r6, r0, r7)
            goto L84
        L95:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.BasicSerializerFactory.createKeySerializer(com.fasterxml.jackson.databind.SerializationConfig, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.f):com.fasterxml.jackson.databind.f");
    }

    @Override // com.fasterxml.jackson.databind.ser.g
    public abstract com.fasterxml.jackson.databind.f<Object> createSerializer(k kVar, JavaType javaType) throws JsonMappingException;

    @Override // com.fasterxml.jackson.databind.ser.g
    public com.fasterxml.jackson.databind.jsontype.c createTypeSerializer(SerializationConfig serializationConfig, JavaType javaType) {
        Collection<NamedType> collectAndResolveSubtypesByClass;
        com.fasterxml.jackson.databind.introspect.a t = serializationConfig.introspectClassAnnotations(javaType.getRawClass()).t();
        vd4<?> findTypeResolver = serializationConfig.getAnnotationIntrospector().findTypeResolver(serializationConfig, t, javaType);
        if (findTypeResolver == null) {
            findTypeResolver = serializationConfig.getDefaultTyper(javaType);
            collectAndResolveSubtypesByClass = null;
        } else {
            collectAndResolveSubtypesByClass = serializationConfig.getSubtypeResolver().collectAndResolveSubtypesByClass(serializationConfig, t);
        }
        if (findTypeResolver == null) {
            return null;
        }
        return findTypeResolver.buildTypeSerializer(serializationConfig, javaType, collectAndResolveSubtypesByClass);
    }

    public abstract Iterable<am3> customSerializers();

    public o80<Object, Object> findConverter(k kVar, ue ueVar) throws JsonMappingException {
        Object findSerializationConverter = kVar.getAnnotationIntrospector().findSerializationConverter(ueVar);
        if (findSerializationConverter == null) {
            return null;
        }
        return kVar.converterInstance(ueVar, findSerializationConverter);
    }

    public com.fasterxml.jackson.databind.f<?> findConvertingSerializer(k kVar, ue ueVar, com.fasterxml.jackson.databind.f<?> fVar) throws JsonMappingException {
        o80<Object, Object> findConverter = findConverter(kVar, ueVar);
        return findConverter == null ? fVar : new StdDelegatingSerializer(findConverter, findConverter.c(kVar.getTypeFactory()), fVar);
    }

    public Object findFilterId(SerializationConfig serializationConfig, so soVar) {
        return serializationConfig.getAnnotationIntrospector().findFilterId(soVar.t());
    }

    public com.fasterxml.jackson.databind.f<?> findOptionalStdSerializer(k kVar, JavaType javaType, so soVar, boolean z) throws JsonMappingException {
        return OptionalHandlerFactory.instance.findSerializer(kVar.getConfig(), javaType, soVar);
    }

    public final com.fasterxml.jackson.databind.f<?> findSerializerByAddonType(SerializationConfig serializationConfig, JavaType javaType, so soVar, boolean z) throws JsonMappingException {
        Class<?> rawClass = javaType.getRawClass();
        if (Iterator.class.isAssignableFrom(rawClass)) {
            JavaType[] findTypeParameters = serializationConfig.getTypeFactory().findTypeParameters(javaType, Iterator.class);
            return buildIteratorSerializer(serializationConfig, javaType, soVar, z, (findTypeParameters == null || findTypeParameters.length != 1) ? TypeFactory.unknownType() : findTypeParameters[0]);
        } else if (Iterable.class.isAssignableFrom(rawClass)) {
            JavaType[] findTypeParameters2 = serializationConfig.getTypeFactory().findTypeParameters(javaType, Iterable.class);
            return buildIterableSerializer(serializationConfig, javaType, soVar, z, (findTypeParameters2 == null || findTypeParameters2.length != 1) ? TypeFactory.unknownType() : findTypeParameters2[0]);
        } else if (CharSequence.class.isAssignableFrom(rawClass)) {
            return ToStringSerializer.instance;
        } else {
            return null;
        }
    }

    public final com.fasterxml.jackson.databind.f<?> findSerializerByAnnotations(k kVar, JavaType javaType, so soVar) throws JsonMappingException {
        if (com.fasterxml.jackson.databind.e.class.isAssignableFrom(javaType.getRawClass())) {
            return SerializableSerializer.instance;
        }
        AnnotatedMethod j = soVar.j();
        if (j != null) {
            Method annotated = j.getAnnotated();
            if (kVar.canOverrideAccessModifiers()) {
                com.fasterxml.jackson.databind.util.c.f(annotated, kVar.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
            }
            return new JsonValueSerializer(j, findSerializerFromAnnotation(kVar, j));
        }
        return null;
    }

    public final com.fasterxml.jackson.databind.f<?> findSerializerByLookup(JavaType javaType, SerializationConfig serializationConfig, so soVar, boolean z) {
        Class<? extends com.fasterxml.jackson.databind.f<?>> cls;
        String name = javaType.getRawClass().getName();
        com.fasterxml.jackson.databind.f<?> fVar = _concrete.get(name);
        if (fVar != null || (cls = _concreteLazy.get(name)) == null) {
            return fVar;
        }
        try {
            return cls.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Failed to instantiate standard serializer (of type " + cls.getName() + "): " + e.getMessage(), e);
        }
    }

    public final com.fasterxml.jackson.databind.f<?> findSerializerByPrimaryType(k kVar, JavaType javaType, so soVar, boolean z) throws JsonMappingException {
        Class<?> rawClass = javaType.getRawClass();
        com.fasterxml.jackson.databind.f<?> findOptionalStdSerializer = findOptionalStdSerializer(kVar, javaType, soVar, z);
        if (findOptionalStdSerializer != null) {
            return findOptionalStdSerializer;
        }
        if (Calendar.class.isAssignableFrom(rawClass)) {
            return CalendarSerializer.instance;
        }
        if (Date.class.isAssignableFrom(rawClass)) {
            return DateSerializer.instance;
        }
        if (Map.Entry.class.isAssignableFrom(rawClass)) {
            JavaType findSuperType = javaType.findSuperType(Map.Entry.class);
            return buildMapEntrySerializer(kVar.getConfig(), javaType, soVar, z, findSuperType.containedTypeOrUnknown(0), findSuperType.containedTypeOrUnknown(1));
        } else if (ByteBuffer.class.isAssignableFrom(rawClass)) {
            return new ByteBufferSerializer();
        } else {
            if (InetAddress.class.isAssignableFrom(rawClass)) {
                return new InetAddressSerializer();
            }
            if (InetSocketAddress.class.isAssignableFrom(rawClass)) {
                return new InetSocketAddressSerializer();
            }
            if (TimeZone.class.isAssignableFrom(rawClass)) {
                return new TimeZoneSerializer();
            }
            if (Charset.class.isAssignableFrom(rawClass)) {
                return ToStringSerializer.instance;
            }
            if (Number.class.isAssignableFrom(rawClass)) {
                JsonFormat.Value g = soVar.g(null);
                if (g != null) {
                    int i = a.a[g.getShape().ordinal()];
                    if (i == 1) {
                        return ToStringSerializer.instance;
                    }
                    if (i == 2 || i == 3) {
                        return null;
                    }
                }
                return NumberSerializer.instance;
            } else if (Enum.class.isAssignableFrom(rawClass)) {
                return buildEnumSerializer(kVar.getConfig(), javaType, soVar);
            } else {
                return null;
            }
        }
    }

    public com.fasterxml.jackson.databind.f<Object> findSerializerFromAnnotation(k kVar, ue ueVar) throws JsonMappingException {
        Object findSerializer = kVar.getAnnotationIntrospector().findSerializer(ueVar);
        if (findSerializer == null) {
            return null;
        }
        return findConvertingSerializer(kVar, ueVar, kVar.serializerInstance(ueVar, findSerializer));
    }

    public Object findSuppressableContentValue(SerializationConfig serializationConfig, JavaType javaType, so soVar) throws JsonMappingException {
        JsonInclude.Value o = soVar.o(serializationConfig.getDefaultPropertyInclusion());
        if (o == null) {
            return null;
        }
        JsonInclude.Include contentInclusion = o.getContentInclusion();
        if (a.b[contentInclusion.ordinal()] != 1) {
            return contentInclusion;
        }
        return null;
    }

    public SerializerFactoryConfig getFactoryConfig() {
        return this._factoryConfig;
    }

    public boolean isIndexedList(Class<?> cls) {
        return RandomAccess.class.isAssignableFrom(cls);
    }

    public boolean usesStaticTyping(SerializationConfig serializationConfig, so soVar, com.fasterxml.jackson.databind.jsontype.c cVar) {
        if (cVar != null) {
            return false;
        }
        JsonSerialize.Typing findSerializationTyping = serializationConfig.getAnnotationIntrospector().findSerializationTyping(soVar.t());
        if (findSerializationTyping == null || findSerializationTyping == JsonSerialize.Typing.DEFAULT_TYPING) {
            return serializationConfig.isEnabled(MapperFeature.USE_STATIC_TYPING);
        }
        return findSerializationTyping == JsonSerialize.Typing.STATIC;
    }

    @Override // com.fasterxml.jackson.databind.ser.g
    public final g withAdditionalKeySerializers(am3 am3Var) {
        return withConfig(this._factoryConfig.withAdditionalKeySerializers(am3Var));
    }

    @Override // com.fasterxml.jackson.databind.ser.g
    public final g withAdditionalSerializers(am3 am3Var) {
        return withConfig(this._factoryConfig.withAdditionalSerializers(am3Var));
    }

    public abstract g withConfig(SerializerFactoryConfig serializerFactoryConfig);

    @Override // com.fasterxml.jackson.databind.ser.g
    public final g withSerializerModifier(xo xoVar) {
        return withConfig(this._factoryConfig.withSerializerModifier(xoVar));
    }

    public ContainerSerializer<?> buildCollectionSerializer(JavaType javaType, boolean z, com.fasterxml.jackson.databind.jsontype.c cVar, com.fasterxml.jackson.databind.f<Object> fVar) {
        return new CollectionSerializer(javaType, z, cVar, fVar);
    }
}
