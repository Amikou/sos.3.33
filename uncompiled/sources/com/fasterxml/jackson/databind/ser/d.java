package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;

/* compiled from: PropertyFilter.java */
/* loaded from: classes.dex */
public interface d {
    @Deprecated
    void a(PropertyWriter propertyWriter, m mVar, k kVar) throws JsonMappingException;

    void b(Object obj, JsonGenerator jsonGenerator, k kVar, PropertyWriter propertyWriter) throws Exception;

    void c(PropertyWriter propertyWriter, com.fasterxml.jackson.databind.jsonFormatVisitors.d dVar, k kVar) throws JsonMappingException;
}
