package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.util.NameTransformer;

/* compiled from: PropertyBuilder.java */
/* loaded from: classes.dex */
public class c {
    public static final Object g = Boolean.FALSE;
    public final SerializationConfig a;
    public final so b;
    public final AnnotationIntrospector c;
    public Object d;
    public final JsonInclude.Value e;
    public final boolean f;

    /* compiled from: PropertyBuilder.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonInclude.Include.values().length];
            a = iArr;
            try {
                iArr[JsonInclude.Include.NON_DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JsonInclude.Include.NON_ABSENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JsonInclude.Include.NON_EMPTY.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JsonInclude.Include.NON_NULL.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[JsonInclude.Include.ALWAYS.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    public c(SerializationConfig serializationConfig, so soVar) {
        this.a = serializationConfig;
        this.b = soVar;
        JsonInclude.Value merge = JsonInclude.Value.merge(soVar.o(JsonInclude.Value.empty()), serializationConfig.getDefaultPropertyInclusion(soVar.r(), JsonInclude.Value.empty()));
        this.e = JsonInclude.Value.merge(serializationConfig.getDefaultPropertyInclusion(), merge);
        this.f = merge.getValueInclusion() == JsonInclude.Include.NON_DEFAULT;
        this.c = serializationConfig.getAnnotationIntrospector();
    }

    /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
        r3 = r3;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object a(java.lang.Exception r3, java.lang.String r4, java.lang.Object r5) {
        /*
            r2 = this;
        L0:
            java.lang.Throwable r0 = r3.getCause()
            if (r0 == 0) goto Lb
            java.lang.Throwable r3 = r3.getCause()
            goto L0
        Lb:
            boolean r0 = r3 instanceof java.lang.Error
            if (r0 != 0) goto L42
            boolean r0 = r3 instanceof java.lang.RuntimeException
            if (r0 == 0) goto L16
            java.lang.RuntimeException r3 = (java.lang.RuntimeException) r3
            throw r3
        L16:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Failed to get property '"
            r0.append(r1)
            r0.append(r4)
            java.lang.String r4 = "' of default "
            r0.append(r4)
            java.lang.Class r4 = r5.getClass()
            java.lang.String r4 = r4.getName()
            r0.append(r4)
            java.lang.String r4 = " instance"
            r0.append(r4)
            java.lang.String r4 = r0.toString()
            r3.<init>(r4)
            throw r3
        L42:
            java.lang.Error r3 = (java.lang.Error) r3
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.c.a(java.lang.Exception, java.lang.String, java.lang.Object):java.lang.Object");
    }

    public BeanPropertyWriter b(k kVar, vo voVar, JavaType javaType, com.fasterxml.jackson.databind.f<?> fVar, com.fasterxml.jackson.databind.jsontype.c cVar, com.fasterxml.jackson.databind.jsontype.c cVar2, AnnotatedMember annotatedMember, boolean z) throws JsonMappingException {
        JavaType javaType2;
        Object b;
        Object e;
        Object obj;
        boolean z2;
        Object obj2;
        try {
            JavaType c = c(annotatedMember, z, javaType);
            if (cVar2 != null) {
                if (c == null) {
                    c = javaType;
                }
                if (c.getContentType() == null) {
                    kVar.reportBadPropertyDefinition(this.b, voVar, "serialization type " + c + " has no content", new Object[0]);
                }
                JavaType withContentTypeHandler = c.withContentTypeHandler(cVar2);
                withContentTypeHandler.getContentType();
                javaType2 = withContentTypeHandler;
            } else {
                javaType2 = c;
            }
            Object obj3 = null;
            JavaType javaType3 = javaType2 == null ? javaType : javaType2;
            JsonInclude.Include valueInclusion = this.a.getDefaultPropertyInclusion(javaType3.getRawClass(), this.e).withOverrides(voVar.e()).getValueInclusion();
            if (valueInclusion == JsonInclude.Include.USE_DEFAULTS) {
                valueInclusion = JsonInclude.Include.ALWAYS;
            }
            int i = a.a[valueInclusion.ordinal()];
            if (i != 1) {
                if (i == 2) {
                    if (javaType3.isReferenceType()) {
                        obj2 = BeanPropertyWriter.MARKER_FOR_EMPTY;
                    }
                    obj = obj3;
                    z2 = true;
                } else if (i != 3) {
                    r3 = i == 4;
                    if (javaType3.isContainerType() && !this.a.isEnabled(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS)) {
                        b = BeanPropertyWriter.MARKER_FOR_EMPTY;
                        obj = b;
                        z2 = r3;
                    }
                    z2 = r3;
                    obj = obj3;
                } else {
                    obj2 = BeanPropertyWriter.MARKER_FOR_EMPTY;
                }
                obj = obj2;
                z2 = true;
            } else {
                if (this.f && (e = e()) != null) {
                    if (kVar.isEnabled(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
                        annotatedMember.fixAccess(this.a.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
                    }
                    try {
                        obj3 = annotatedMember.getValue(e);
                    } catch (Exception e2) {
                        a(e2, voVar.t(), e);
                    }
                } else {
                    obj3 = f(javaType3);
                    r3 = true;
                }
                if (obj3 != null) {
                    if (obj3.getClass().isArray()) {
                        b = lh.b(obj3);
                        obj = b;
                        z2 = r3;
                    }
                    z2 = r3;
                    obj = obj3;
                }
                obj = obj3;
                z2 = true;
            }
            BeanPropertyWriter beanPropertyWriter = new BeanPropertyWriter(voVar, annotatedMember, this.b.s(), javaType, fVar, cVar, javaType2, z2, obj);
            Object findNullSerializer = this.c.findNullSerializer(annotatedMember);
            if (findNullSerializer != null) {
                beanPropertyWriter.assignNullSerializer(kVar.serializerInstance(annotatedMember, findNullSerializer));
            }
            NameTransformer findUnwrappingNameTransformer = this.c.findUnwrappingNameTransformer(annotatedMember);
            return findUnwrappingNameTransformer != null ? beanPropertyWriter.unwrappingWriter(findUnwrappingNameTransformer) : beanPropertyWriter;
        } catch (JsonMappingException e3) {
            return (BeanPropertyWriter) kVar.reportBadPropertyDefinition(this.b, voVar, e3.getMessage(), new Object[0]);
        }
    }

    public JavaType c(ue ueVar, boolean z, JavaType javaType) throws JsonMappingException {
        JavaType refineSerializationType = this.c.refineSerializationType(this.a, ueVar, javaType);
        if (refineSerializationType != javaType) {
            Class<?> rawClass = refineSerializationType.getRawClass();
            Class<?> rawClass2 = javaType.getRawClass();
            if (!rawClass.isAssignableFrom(rawClass2) && !rawClass2.isAssignableFrom(rawClass)) {
                throw new IllegalArgumentException("Illegal concrete-type annotation for method '" + ueVar.getName() + "': class " + rawClass.getName() + " not a super-type of (declared) class " + rawClass2.getName());
            }
            javaType = refineSerializationType;
            z = true;
        }
        JsonSerialize.Typing findSerializationTyping = this.c.findSerializationTyping(ueVar);
        if (findSerializationTyping != null && findSerializationTyping != JsonSerialize.Typing.DEFAULT_TYPING) {
            z = findSerializationTyping == JsonSerialize.Typing.STATIC;
        }
        if (z) {
            return javaType.withStaticTyping();
        }
        return null;
    }

    public xe d() {
        return this.b.s();
    }

    public Object e() {
        Object obj = this.d;
        if (obj == null) {
            obj = this.b.A(this.a.canOverrideAccessModifiers());
            if (obj == null) {
                obj = g;
            }
            this.d = obj;
        }
        if (obj == g) {
            return null;
        }
        return this.d;
    }

    public Object f(JavaType javaType) {
        Class<?> rawClass = javaType.getRawClass();
        Class<?> O = com.fasterxml.jackson.databind.util.c.O(rawClass);
        if (O != null) {
            return com.fasterxml.jackson.databind.util.c.j(O);
        }
        if (!javaType.isContainerType() && !javaType.isReferenceType()) {
            if (rawClass == String.class) {
                return "";
            }
            return null;
        }
        return JsonInclude.Include.NON_EMPTY;
    }
}
