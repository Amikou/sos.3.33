package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.k;

/* compiled from: SerializerFactory.java */
/* loaded from: classes.dex */
public abstract class g {
    public abstract com.fasterxml.jackson.databind.f<Object> createKeySerializer(SerializationConfig serializationConfig, JavaType javaType, com.fasterxml.jackson.databind.f<Object> fVar) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.f<Object> createSerializer(k kVar, JavaType javaType) throws JsonMappingException;

    public abstract com.fasterxml.jackson.databind.jsontype.c createTypeSerializer(SerializationConfig serializationConfig, JavaType javaType) throws JsonMappingException;

    public abstract g withAdditionalKeySerializers(am3 am3Var);

    public abstract g withAdditionalSerializers(am3 am3Var);

    public abstract g withSerializerModifier(xo xoVar);
}
