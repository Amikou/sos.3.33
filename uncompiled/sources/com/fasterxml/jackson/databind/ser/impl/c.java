package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;

/* compiled from: WritableObjectId.java */
/* loaded from: classes.dex */
public final class c {
    public final ObjectIdGenerator<?> a;
    public Object b;
    public boolean c = false;

    public c(ObjectIdGenerator<?> objectIdGenerator) {
        this.a = objectIdGenerator;
    }

    public Object a(Object obj) {
        if (this.b == null) {
            this.b = this.a.generateId(obj);
        }
        return this.b;
    }

    public void b(JsonGenerator jsonGenerator, k kVar, kl2 kl2Var) throws IOException {
        this.c = true;
        if (jsonGenerator.g()) {
            jsonGenerator.L0(String.valueOf(this.b));
            return;
        }
        yl3 yl3Var = kl2Var.b;
        if (yl3Var != null) {
            jsonGenerator.g0(yl3Var);
            kl2Var.d.serialize(this.b, jsonGenerator, kVar);
        }
    }

    public boolean c(JsonGenerator jsonGenerator, k kVar, kl2 kl2Var) throws IOException {
        if (this.b != null) {
            if (this.c || kl2Var.e) {
                if (jsonGenerator.g()) {
                    jsonGenerator.O0(String.valueOf(this.b));
                    return true;
                }
                kl2Var.d.serialize(this.b, jsonGenerator, kVar);
                return true;
            }
            return false;
        }
        return false;
    }
}
