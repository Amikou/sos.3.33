package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.std.ArraySerializerBase;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.lang.reflect.Type;
import org.web3j.abi.datatypes.Utf8String;

@pt1
/* loaded from: classes.dex */
public class StringArraySerializer extends ArraySerializerBase<String[]> {
    public static final JavaType f0 = TypeFactory.defaultInstance().uncheckedSimpleType(String.class);
    public static final StringArraySerializer instance = new StringArraySerializer();
    public final f<Object> _elementSerializer;

    public StringArraySerializer() {
        super(String[].class);
        this._elementSerializer = null;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
    public f<?> _withResolved(com.fasterxml.jackson.databind.a aVar, Boolean bool) {
        return new StringArraySerializer(this, aVar, this._elementSerializer, bool);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public ContainerSerializer<?> _withValueTypeSerializer(com.fasterxml.jackson.databind.jsontype.c cVar) {
        return this;
    }

    public final void a(String[] strArr, JsonGenerator jsonGenerator, k kVar, f<Object> fVar) throws IOException {
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (strArr[i] == null) {
                kVar.defaultSerializeNull(jsonGenerator);
            } else {
                fVar.serialize(strArr[i], jsonGenerator, kVar);
            }
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        visitArrayFormat(bVar, javaType, JsonFormatTypes.STRING);
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x002b  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x003d  */
    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.f<?> createContextual(com.fasterxml.jackson.databind.k r5, com.fasterxml.jackson.databind.a r6) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r4 = this;
            r0 = 0
            if (r6 == 0) goto L18
            com.fasterxml.jackson.databind.AnnotationIntrospector r1 = r5.getAnnotationIntrospector()
            com.fasterxml.jackson.databind.introspect.AnnotatedMember r2 = r6.getMember()
            if (r2 == 0) goto L18
            java.lang.Object r1 = r1.findContentSerializer(r2)
            if (r1 == 0) goto L18
            com.fasterxml.jackson.databind.f r1 = r5.serializerInstance(r2, r1)
            goto L19
        L18:
            r1 = r0
        L19:
            java.lang.Class<java.lang.String[]> r2 = java.lang.String[].class
            com.fasterxml.jackson.annotation.JsonFormat$Feature r3 = com.fasterxml.jackson.annotation.JsonFormat.Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            java.lang.Boolean r2 = r4.findFormatFeature(r5, r6, r2, r3)
            if (r1 != 0) goto L25
            com.fasterxml.jackson.databind.f<java.lang.Object> r1 = r4._elementSerializer
        L25:
            com.fasterxml.jackson.databind.f r1 = r4.findConvertingContentSerializer(r5, r6, r1)
            if (r1 != 0) goto L32
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            com.fasterxml.jackson.databind.f r5 = r5.findValueSerializer(r1, r6)
            goto L36
        L32:
            com.fasterxml.jackson.databind.f r5 = r5.handleSecondaryContextualization(r1, r6)
        L36:
            boolean r1 = r4.isDefaultSerializer(r5)
            if (r1 == 0) goto L3d
            goto L3e
        L3d:
            r0 = r5
        L3e:
            com.fasterxml.jackson.databind.f<java.lang.Object> r5 = r4._elementSerializer
            if (r0 != r5) goto L47
            java.lang.Boolean r5 = r4._unwrapSingle
            if (r2 != r5) goto L47
            return r4
        L47:
            com.fasterxml.jackson.databind.ser.impl.StringArraySerializer r5 = new com.fasterxml.jackson.databind.ser.impl.StringArraySerializer
            r5.<init>(r4, r6, r0, r2)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.impl.StringArraySerializer.createContextual(com.fasterxml.jackson.databind.k, com.fasterxml.jackson.databind.a):com.fasterxml.jackson.databind.f");
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public f<?> getContentSerializer() {
        return this._elementSerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public JavaType getContentType() {
        return f0;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode("array", true).Z("items", createSchemaNode(Utf8String.TYPE_NAME));
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(String[] strArr) {
        return strArr.length == 1;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, String[] strArr) {
        return strArr == null || strArr.length == 0;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(String[] strArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int length = strArr.length;
        if (length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
            serializeContents(strArr, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.f1(length);
        serializeContents(strArr, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
    public void serializeContents(String[] strArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int length = strArr.length;
        if (length == 0) {
            return;
        }
        f<Object> fVar = this._elementSerializer;
        if (fVar != null) {
            a(strArr, jsonGenerator, kVar, fVar);
            return;
        }
        for (int i = 0; i < length; i++) {
            if (strArr[i] == null) {
                jsonGenerator.m0();
            } else {
                jsonGenerator.o1(strArr[i]);
            }
        }
    }

    public StringArraySerializer(StringArraySerializer stringArraySerializer, com.fasterxml.jackson.databind.a aVar, f<?> fVar, Boolean bool) {
        super(stringArraySerializer, aVar, bool);
        this._elementSerializer = fVar;
    }
}
