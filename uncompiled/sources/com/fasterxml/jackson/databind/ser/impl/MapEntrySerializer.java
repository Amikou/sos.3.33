package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.impl.a;
import java.io.IOException;
import java.util.Map;

@pt1
/* loaded from: classes.dex */
public class MapEntrySerializer extends ContainerSerializer<Map.Entry<?, ?>> implements com.fasterxml.jackson.databind.ser.b {
    public a _dynamicValueSerializers;
    public final JavaType _entryType;
    public f<Object> _keySerializer;
    public final JavaType _keyType;
    public final com.fasterxml.jackson.databind.a _property;
    public f<Object> _valueSerializer;
    public final JavaType _valueType;
    public final boolean _valueTypeIsStatic;
    public final com.fasterxml.jackson.databind.jsontype.c _valueTypeSerializer;

    public MapEntrySerializer(JavaType javaType, JavaType javaType2, JavaType javaType3, boolean z, com.fasterxml.jackson.databind.jsontype.c cVar, com.fasterxml.jackson.databind.a aVar) {
        super(javaType);
        this._entryType = javaType;
        this._keyType = javaType2;
        this._valueType = javaType3;
        this._valueTypeIsStatic = z;
        this._valueTypeSerializer = cVar;
        this._property = aVar;
        this._dynamicValueSerializers = a.a();
    }

    public final f<Object> _findAndAddDynamic(a aVar, Class<?> cls, k kVar) throws JsonMappingException {
        a.d g = aVar.g(cls, kVar, this._property);
        a aVar2 = g.b;
        if (aVar != aVar2) {
            this._dynamicValueSerializers = aVar2;
        }
        return g.a;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public ContainerSerializer<?> _withValueTypeSerializer(com.fasterxml.jackson.databind.jsontype.c cVar) {
        return new MapEntrySerializer(this, this._property, cVar, this._keySerializer, this._valueSerializer);
    }

    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        f<?> fVar;
        f<?> handleSecondaryContextualization;
        AnnotationIntrospector annotationIntrospector = kVar.getAnnotationIntrospector();
        f<Object> fVar2 = null;
        AnnotatedMember member = aVar == null ? null : aVar.getMember();
        if (member == null || annotationIntrospector == null) {
            fVar = null;
        } else {
            Object findKeySerializer = annotationIntrospector.findKeySerializer(member);
            fVar = findKeySerializer != null ? kVar.serializerInstance(member, findKeySerializer) : null;
            Object findContentSerializer = annotationIntrospector.findContentSerializer(member);
            if (findContentSerializer != null) {
                fVar2 = kVar.serializerInstance(member, findContentSerializer);
            }
        }
        if (fVar2 == null) {
            fVar2 = this._valueSerializer;
        }
        f<?> findConvertingContentSerializer = findConvertingContentSerializer(kVar, aVar, fVar2);
        if (findConvertingContentSerializer == null) {
            if (this._valueTypeIsStatic && !this._valueType.isJavaLangObject()) {
                findConvertingContentSerializer = kVar.findValueSerializer(this._valueType, aVar);
            }
        } else {
            findConvertingContentSerializer = kVar.handleSecondaryContextualization(findConvertingContentSerializer, aVar);
        }
        if (fVar == null) {
            fVar = this._keySerializer;
        }
        if (fVar == null) {
            handleSecondaryContextualization = kVar.findKeySerializer(this._keyType, aVar);
        } else {
            handleSecondaryContextualization = kVar.handleSecondaryContextualization(fVar, aVar);
        }
        return withResolved(aVar, handleSecondaryContextualization, findConvertingContentSerializer);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public f<?> getContentSerializer() {
        return this._valueSerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public JavaType getContentType() {
        return this._valueType;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(Map.Entry<?, ?> entry) {
        return true;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Map.Entry<?, ?> entry) {
        return entry == null;
    }

    public void serializeDynamic(Map.Entry<?, ?> entry, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> _findAndAddDynamic;
        f<Object> fVar = this._keySerializer;
        boolean z = !kVar.isEnabled(SerializationFeature.WRITE_NULL_MAP_VALUES);
        com.fasterxml.jackson.databind.jsontype.c cVar = this._valueTypeSerializer;
        a aVar = this._dynamicValueSerializers;
        Object value = entry.getValue();
        Object key = entry.getKey();
        if (key == null) {
            kVar.findNullKeySerializer(this._keyType, this._property).serialize(null, jsonGenerator, kVar);
        } else if (z && value == null) {
            return;
        } else {
            fVar.serialize(key, jsonGenerator, kVar);
        }
        if (value == null) {
            kVar.defaultSerializeNull(jsonGenerator);
            return;
        }
        Class<?> cls = value.getClass();
        f<Object> i = aVar.i(cls);
        if (i == null) {
            if (this._valueType.hasGenericTypes()) {
                _findAndAddDynamic = _findAndAddDynamic(aVar, kVar.constructSpecializedType(this._valueType, cls), kVar);
            } else {
                _findAndAddDynamic = _findAndAddDynamic(aVar, cls, kVar);
            }
            i = _findAndAddDynamic;
        }
        try {
            if (cVar == null) {
                i.serialize(value, jsonGenerator, kVar);
            } else {
                i.serializeWithType(value, jsonGenerator, kVar, cVar);
            }
        } catch (Exception e) {
            wrapAndThrow(kVar, e, entry, "" + key);
        }
    }

    public void serializeUsing(Map.Entry<?, ?> entry, JsonGenerator jsonGenerator, k kVar, f<Object> fVar) throws IOException, JsonGenerationException {
        f<Object> fVar2 = this._keySerializer;
        com.fasterxml.jackson.databind.jsontype.c cVar = this._valueTypeSerializer;
        boolean z = !kVar.isEnabled(SerializationFeature.WRITE_NULL_MAP_VALUES);
        Object value = entry.getValue();
        Object key = entry.getKey();
        if (key == null) {
            kVar.findNullKeySerializer(this._keyType, this._property).serialize(null, jsonGenerator, kVar);
        } else if (z && value == null) {
            return;
        } else {
            fVar2.serialize(key, jsonGenerator, kVar);
        }
        if (value == null) {
            kVar.defaultSerializeNull(jsonGenerator);
            return;
        }
        try {
            if (cVar == null) {
                fVar.serialize(value, jsonGenerator, kVar);
            } else {
                fVar.serializeWithType(value, jsonGenerator, kVar, cVar);
            }
        } catch (Exception e) {
            wrapAndThrow(kVar, e, entry, "" + key);
        }
    }

    public MapEntrySerializer withResolved(com.fasterxml.jackson.databind.a aVar, f<?> fVar, f<?> fVar2) {
        return new MapEntrySerializer(this, aVar, this._valueTypeSerializer, fVar, fVar2);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Map.Entry<?, ?> entry, JsonGenerator jsonGenerator, k kVar) throws IOException {
        jsonGenerator.k1(entry);
        f<Object> fVar = this._valueSerializer;
        if (fVar != null) {
            serializeUsing(entry, jsonGenerator, kVar, fVar);
        } else {
            serializeDynamic(entry, jsonGenerator, kVar);
        }
        jsonGenerator.f0();
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(Map.Entry<?, ?> entry, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        cVar.i(entry, jsonGenerator);
        jsonGenerator.u(entry);
        f<Object> fVar = this._valueSerializer;
        if (fVar != null) {
            serializeUsing(entry, jsonGenerator, kVar, fVar);
        } else {
            serializeDynamic(entry, jsonGenerator, kVar);
        }
        cVar.m(entry, jsonGenerator);
    }

    public final f<Object> _findAndAddDynamic(a aVar, JavaType javaType, k kVar) throws JsonMappingException {
        a.d f = aVar.f(javaType, kVar, this._property);
        a aVar2 = f.b;
        if (aVar != aVar2) {
            this._dynamicValueSerializers = aVar2;
        }
        return f.a;
    }

    public MapEntrySerializer(MapEntrySerializer mapEntrySerializer, com.fasterxml.jackson.databind.a aVar, com.fasterxml.jackson.databind.jsontype.c cVar, f<?> fVar, f<?> fVar2) {
        super(Map.class, false);
        this._entryType = mapEntrySerializer._entryType;
        this._keyType = mapEntrySerializer._keyType;
        this._valueType = mapEntrySerializer._valueType;
        this._valueTypeIsStatic = mapEntrySerializer._valueTypeIsStatic;
        this._valueTypeSerializer = mapEntrySerializer._valueTypeSerializer;
        this._keySerializer = fVar;
        this._valueSerializer = fVar2;
        this._dynamicValueSerializers = mapEntrySerializer._dynamicValueSerializers;
        this._property = mapEntrySerializer._property;
    }
}
