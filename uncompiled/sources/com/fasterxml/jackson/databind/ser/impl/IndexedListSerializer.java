package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase;
import java.io.IOException;
import java.util.List;

@pt1
/* loaded from: classes.dex */
public final class IndexedListSerializer extends AsArraySerializerBase<List<?>> {
    private static final long serialVersionUID = 1;

    public IndexedListSerializer(JavaType javaType, boolean z, com.fasterxml.jackson.databind.jsontype.c cVar, f<Object> fVar) {
        super(List.class, javaType, z, cVar, fVar);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public ContainerSerializer<?> _withValueTypeSerializer(com.fasterxml.jackson.databind.jsontype.c cVar) {
        return new IndexedListSerializer(this, this._property, cVar, this._elementSerializer, this._unwrapSingle);
    }

    public void serializeContentsUsing(List<?> list, JsonGenerator jsonGenerator, k kVar, f<Object> fVar) throws IOException {
        int size = list.size();
        if (size == 0) {
            return;
        }
        com.fasterxml.jackson.databind.jsontype.c cVar = this._valueTypeSerializer;
        for (int i = 0; i < size; i++) {
            Object obj = list.get(i);
            if (obj == null) {
                try {
                    kVar.defaultSerializeNull(jsonGenerator);
                } catch (Exception e) {
                    wrapAndThrow(kVar, e, list, i);
                }
            } else if (cVar == null) {
                fVar.serialize(obj, jsonGenerator, kVar);
            } else {
                fVar.serializeWithType(obj, jsonGenerator, kVar, cVar);
            }
        }
    }

    public void serializeTypedContents(List<?> list, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> _findAndAddDynamic;
        int size = list.size();
        if (size == 0) {
            return;
        }
        int i = 0;
        try {
            com.fasterxml.jackson.databind.jsontype.c cVar = this._valueTypeSerializer;
            a aVar = this._dynamicSerializers;
            while (i < size) {
                Object obj = list.get(i);
                if (obj == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else {
                    Class<?> cls = obj.getClass();
                    f<Object> i2 = aVar.i(cls);
                    if (i2 == null) {
                        if (this._elementType.hasGenericTypes()) {
                            _findAndAddDynamic = _findAndAddDynamic(aVar, kVar.constructSpecializedType(this._elementType, cls), kVar);
                        } else {
                            _findAndAddDynamic = _findAndAddDynamic(aVar, cls, kVar);
                        }
                        i2 = _findAndAddDynamic;
                        aVar = this._dynamicSerializers;
                    }
                    i2.serializeWithType(obj, jsonGenerator, kVar, cVar);
                }
                i++;
            }
        } catch (Exception e) {
            wrapAndThrow(kVar, e, list, i);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    /* renamed from: withResolved  reason: avoid collision after fix types in other method */
    public /* bridge */ /* synthetic */ AsArraySerializerBase<List<?>> withResolved2(com.fasterxml.jackson.databind.a aVar, com.fasterxml.jackson.databind.jsontype.c cVar, f fVar, Boolean bool) {
        return withResolved(aVar, cVar, (f<?>) fVar, bool);
    }

    public IndexedListSerializer(IndexedListSerializer indexedListSerializer, com.fasterxml.jackson.databind.a aVar, com.fasterxml.jackson.databind.jsontype.c cVar, f<?> fVar, Boolean bool) {
        super(indexedListSerializer, aVar, cVar, fVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(List<?> list) {
        return list.size() == 1;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, List<?> list) {
        return list == null || list.isEmpty();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(List<?> list, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int size = list.size();
        if (size == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
            serializeContents(list, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.f1(size);
        serializeContents(list, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public void serializeContents(List<?> list, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> _findAndAddDynamic;
        f<Object> fVar = this._elementSerializer;
        if (fVar != null) {
            serializeContentsUsing(list, jsonGenerator, kVar, fVar);
        } else if (this._valueTypeSerializer != null) {
            serializeTypedContents(list, jsonGenerator, kVar);
        } else {
            int size = list.size();
            if (size == 0) {
                return;
            }
            int i = 0;
            try {
                a aVar = this._dynamicSerializers;
                while (i < size) {
                    Object obj = list.get(i);
                    if (obj == null) {
                        kVar.defaultSerializeNull(jsonGenerator);
                    } else {
                        Class<?> cls = obj.getClass();
                        f<Object> i2 = aVar.i(cls);
                        if (i2 == null) {
                            if (this._elementType.hasGenericTypes()) {
                                _findAndAddDynamic = _findAndAddDynamic(aVar, kVar.constructSpecializedType(this._elementType, cls), kVar);
                            } else {
                                _findAndAddDynamic = _findAndAddDynamic(aVar, cls, kVar);
                            }
                            i2 = _findAndAddDynamic;
                            aVar = this._dynamicSerializers;
                        }
                        i2.serialize(obj, jsonGenerator, kVar);
                    }
                    i++;
                }
            } catch (Exception e) {
                wrapAndThrow(kVar, e, list, i);
            }
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public AsArraySerializerBase<List<?>> withResolved(com.fasterxml.jackson.databind.a aVar, com.fasterxml.jackson.databind.jsontype.c cVar, f<?> fVar, Boolean bool) {
        return new IndexedListSerializer(this, aVar, cVar, fVar, bool);
    }
}
