package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.d;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.util.NameTransformer;

/* loaded from: classes.dex */
public abstract class FilteredBeanPropertyWriter {

    /* loaded from: classes.dex */
    public static final class MultiView extends BeanPropertyWriter {
        private static final long serialVersionUID = 1;
        public final BeanPropertyWriter _delegate;
        public final Class<?>[] _views;

        public MultiView(BeanPropertyWriter beanPropertyWriter, Class<?>[] clsArr) {
            super(beanPropertyWriter);
            this._delegate = beanPropertyWriter;
            this._views = clsArr;
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
        public void assignNullSerializer(f<Object> fVar) {
            this._delegate.assignNullSerializer(fVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
        public void assignSerializer(f<Object> fVar) {
            this._delegate.assignSerializer(fVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
        public void depositSchemaProperty(d dVar, k kVar) throws JsonMappingException {
            Class<?> activeView = kVar.getActiveView();
            if (activeView != null) {
                int i = 0;
                int length = this._views.length;
                while (i < length && !this._views[i].isAssignableFrom(activeView)) {
                    i++;
                }
                if (i == length) {
                    return;
                }
            }
            super.depositSchemaProperty(dVar, kVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter
        public void serializeAsElement(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
            Class<?> activeView = kVar.getActiveView();
            if (activeView != null) {
                int i = 0;
                int length = this._views.length;
                while (i < length && !this._views[i].isAssignableFrom(activeView)) {
                    i++;
                }
                if (i == length) {
                    this._delegate.serializeAsPlaceholder(obj, jsonGenerator, kVar);
                    return;
                }
            }
            this._delegate.serializeAsElement(obj, jsonGenerator, kVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter
        public void serializeAsField(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
            Class<?> activeView = kVar.getActiveView();
            if (activeView != null) {
                int i = 0;
                int length = this._views.length;
                while (i < length && !this._views[i].isAssignableFrom(activeView)) {
                    i++;
                }
                if (i == length) {
                    this._delegate.serializeAsOmittedField(obj, jsonGenerator, kVar);
                    return;
                }
            }
            this._delegate.serializeAsField(obj, jsonGenerator, kVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
        public MultiView rename(NameTransformer nameTransformer) {
            return new MultiView(this._delegate.rename(nameTransformer), this._views);
        }
    }

    /* loaded from: classes.dex */
    public static final class SingleView extends BeanPropertyWriter {
        private static final long serialVersionUID = 1;
        public final BeanPropertyWriter _delegate;
        public final Class<?> _view;

        public SingleView(BeanPropertyWriter beanPropertyWriter, Class<?> cls) {
            super(beanPropertyWriter);
            this._delegate = beanPropertyWriter;
            this._view = cls;
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
        public void assignNullSerializer(f<Object> fVar) {
            this._delegate.assignNullSerializer(fVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
        public void assignSerializer(f<Object> fVar) {
            this._delegate.assignSerializer(fVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
        public void depositSchemaProperty(d dVar, k kVar) throws JsonMappingException {
            Class<?> activeView = kVar.getActiveView();
            if (activeView == null || this._view.isAssignableFrom(activeView)) {
                super.depositSchemaProperty(dVar, kVar);
            }
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter
        public void serializeAsElement(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
            Class<?> activeView = kVar.getActiveView();
            if (activeView != null && !this._view.isAssignableFrom(activeView)) {
                this._delegate.serializeAsPlaceholder(obj, jsonGenerator, kVar);
            } else {
                this._delegate.serializeAsElement(obj, jsonGenerator, kVar);
            }
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter
        public void serializeAsField(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
            Class<?> activeView = kVar.getActiveView();
            if (activeView != null && !this._view.isAssignableFrom(activeView)) {
                this._delegate.serializeAsOmittedField(obj, jsonGenerator, kVar);
            } else {
                this._delegate.serializeAsField(obj, jsonGenerator, kVar);
            }
        }

        @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
        public SingleView rename(NameTransformer nameTransformer) {
            return new SingleView(this._delegate.rename(nameTransformer), this._view);
        }
    }

    public static BeanPropertyWriter a(BeanPropertyWriter beanPropertyWriter, Class<?>[] clsArr) {
        if (clsArr.length == 1) {
            return new SingleView(beanPropertyWriter, clsArr[0]);
        }
        return new MultiView(beanPropertyWriter, clsArr);
    }
}
