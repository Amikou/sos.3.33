package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase;
import java.io.IOException;
import java.util.Iterator;

@pt1
/* loaded from: classes.dex */
public class IteratorSerializer extends AsArraySerializerBase<Iterator<?>> {
    public IteratorSerializer(JavaType javaType, boolean z, com.fasterxml.jackson.databind.jsontype.c cVar) {
        super(Iterator.class, javaType, z, cVar, (f<Object>) null);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public ContainerSerializer<?> _withValueTypeSerializer(com.fasterxml.jackson.databind.jsontype.c cVar) {
        return new IteratorSerializer(this, this._property, cVar, this._elementSerializer, this._unwrapSingle);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(Iterator<?> it) {
        return false;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    /* renamed from: withResolved  reason: avoid collision after fix types in other method */
    public /* bridge */ /* synthetic */ AsArraySerializerBase<Iterator<?>> withResolved2(com.fasterxml.jackson.databind.a aVar, com.fasterxml.jackson.databind.jsontype.c cVar, f fVar, Boolean bool) {
        return withResolved(aVar, cVar, (f<?>) fVar, bool);
    }

    public IteratorSerializer(IteratorSerializer iteratorSerializer, com.fasterxml.jackson.databind.a aVar, com.fasterxml.jackson.databind.jsontype.c cVar, f<?> fVar, Boolean bool) {
        super(iteratorSerializer, aVar, cVar, fVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Iterator<?> it) {
        return it == null || !it.hasNext();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(Iterator<?> it, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE) && hasSingleElement(it)) {
            serializeContents(it, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.e1();
        serializeContents(it, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public void serializeContents(Iterator<?> it, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> fVar;
        if (it.hasNext()) {
            com.fasterxml.jackson.databind.jsontype.c cVar = this._valueTypeSerializer;
            Class<?> cls = null;
            f<Object> fVar2 = null;
            do {
                Object next = it.next();
                if (next == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else {
                    f<Object> fVar3 = this._elementSerializer;
                    if (fVar3 == null) {
                        Class<?> cls2 = next.getClass();
                        if (cls2 != cls) {
                            fVar2 = kVar.findValueSerializer(cls2, this._property);
                            cls = cls2;
                        }
                        fVar = fVar2;
                    } else {
                        fVar = fVar2;
                        fVar2 = fVar3;
                    }
                    if (cVar == null) {
                        fVar2.serialize(next, jsonGenerator, kVar);
                    } else {
                        fVar2.serializeWithType(next, jsonGenerator, kVar, cVar);
                    }
                    fVar2 = fVar;
                }
            } while (it.hasNext());
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public AsArraySerializerBase<Iterator<?>> withResolved(com.fasterxml.jackson.databind.a aVar, com.fasterxml.jackson.databind.jsontype.c cVar, f<?> fVar, Boolean bool) {
        return new IteratorSerializer(this, aVar, cVar, fVar, bool);
    }
}
