package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;

/* compiled from: TypeWrappedSerializer.java */
/* loaded from: classes.dex */
public final class b extends f<Object> {
    public final com.fasterxml.jackson.databind.jsontype.c a;
    public final f<Object> f0;

    public b(com.fasterxml.jackson.databind.jsontype.c cVar, f<?> fVar) {
        this.a = cVar;
        this.f0 = fVar;
    }

    public com.fasterxml.jackson.databind.jsontype.c a() {
        return this.a;
    }

    @Override // com.fasterxml.jackson.databind.f
    public Class<Object> handledType() {
        return Object.class;
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        this.f0.serializeWithType(obj, jsonGenerator, kVar, this.a);
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(Object obj, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        this.f0.serializeWithType(obj, jsonGenerator, kVar, cVar);
    }
}
