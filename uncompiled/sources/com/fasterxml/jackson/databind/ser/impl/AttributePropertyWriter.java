package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;

/* loaded from: classes.dex */
public class AttributePropertyWriter extends VirtualBeanPropertyWriter {
    private static final long serialVersionUID = 1;
    public final String _attrName;

    public AttributePropertyWriter(String str, vo voVar, xe xeVar, JavaType javaType) {
        this(str, voVar, xeVar, javaType, voVar.e());
    }

    public static AttributePropertyWriter construct(String str, vo voVar, xe xeVar, JavaType javaType) {
        return new AttributePropertyWriter(str, voVar, xeVar, javaType);
    }

    @Override // com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter
    public Object value(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        return kVar.getAttribute(this._attrName);
    }

    @Override // com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> mapperConfig, com.fasterxml.jackson.databind.introspect.a aVar, vo voVar, JavaType javaType) {
        throw new IllegalStateException("Should not be called on this type");
    }

    public AttributePropertyWriter(String str, vo voVar, xe xeVar, JavaType javaType, JsonInclude.Value value) {
        super(voVar, xeVar, javaType, null, null, null, value);
        this._attrName = str;
    }

    public AttributePropertyWriter(AttributePropertyWriter attributePropertyWriter) {
        super(attributePropertyWriter);
        this._attrName = attributePropertyWriter._attrName;
    }
}
