package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.k;
import java.util.Arrays;

/* compiled from: PropertySerializerMap.java */
/* loaded from: classes.dex */
public abstract class a {
    public final boolean a;

    /* compiled from: PropertySerializerMap.java */
    /* renamed from: com.fasterxml.jackson.databind.ser.impl.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0096a extends a {
        public final Class<?> b;
        public final Class<?> c;
        public final com.fasterxml.jackson.databind.f<Object> d;
        public final com.fasterxml.jackson.databind.f<Object> e;

        public C0096a(a aVar, Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar, Class<?> cls2, com.fasterxml.jackson.databind.f<Object> fVar2) {
            super(aVar);
            this.b = cls;
            this.d = fVar;
            this.c = cls2;
            this.e = fVar2;
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public a h(Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar) {
            return new c(this, new f[]{new f(this.b, this.d), new f(this.c, this.e), new f(cls, fVar)});
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public com.fasterxml.jackson.databind.f<Object> i(Class<?> cls) {
            if (cls == this.b) {
                return this.d;
            }
            if (cls == this.c) {
                return this.e;
            }
            return null;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes.dex */
    public static final class b extends a {
        public static final b b = new b(false);
        public static final b c = new b(true);

        public b(boolean z) {
            super(z);
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public a h(Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar) {
            return new e(this, cls, fVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public com.fasterxml.jackson.databind.f<Object> i(Class<?> cls) {
            return null;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes.dex */
    public static final class c extends a {
        public final f[] b;

        public c(a aVar, f[] fVarArr) {
            super(aVar);
            this.b = fVarArr;
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public a h(Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar) {
            f[] fVarArr = this.b;
            int length = fVarArr.length;
            if (length == 8) {
                return this.a ? new e(this, cls, fVar) : this;
            }
            f[] fVarArr2 = (f[]) Arrays.copyOf(fVarArr, length + 1);
            fVarArr2[length] = new f(cls, fVar);
            return new c(this, fVarArr2);
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public com.fasterxml.jackson.databind.f<Object> i(Class<?> cls) {
            int length = this.b.length;
            for (int i = 0; i < length; i++) {
                f fVar = this.b[i];
                if (fVar.a == cls) {
                    return fVar.b;
                }
            }
            return null;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes.dex */
    public static final class d {
        public final com.fasterxml.jackson.databind.f<Object> a;
        public final a b;

        public d(com.fasterxml.jackson.databind.f<Object> fVar, a aVar) {
            this.a = fVar;
            this.b = aVar;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes.dex */
    public static final class e extends a {
        public final Class<?> b;
        public final com.fasterxml.jackson.databind.f<Object> c;

        public e(a aVar, Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar) {
            super(aVar);
            this.b = cls;
            this.c = fVar;
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public a h(Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar) {
            return new C0096a(this, this.b, this.c, cls, fVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.impl.a
        public com.fasterxml.jackson.databind.f<Object> i(Class<?> cls) {
            if (cls == this.b) {
                return this.c;
            }
            return null;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes.dex */
    public static final class f {
        public final Class<?> a;
        public final com.fasterxml.jackson.databind.f<Object> b;

        public f(Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar) {
            this.a = cls;
            this.b = fVar;
        }
    }

    public a(boolean z) {
        this.a = z;
    }

    public static a a() {
        return b.b;
    }

    public static a b() {
        return b.c;
    }

    public final d c(Class<?> cls, k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<Object> findKeySerializer = kVar.findKeySerializer(cls, aVar);
        return new d(findKeySerializer, h(cls, findKeySerializer));
    }

    public final d d(JavaType javaType, k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<Object> findPrimaryPropertySerializer = kVar.findPrimaryPropertySerializer(javaType, aVar);
        return new d(findPrimaryPropertySerializer, h(javaType.getRawClass(), findPrimaryPropertySerializer));
    }

    public final d e(Class<?> cls, k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<Object> findPrimaryPropertySerializer = kVar.findPrimaryPropertySerializer(cls, aVar);
        return new d(findPrimaryPropertySerializer, h(cls, findPrimaryPropertySerializer));
    }

    public final d f(JavaType javaType, k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<Object> findValueSerializer = kVar.findValueSerializer(javaType, aVar);
        return new d(findValueSerializer, h(javaType.getRawClass(), findValueSerializer));
    }

    public final d g(Class<?> cls, k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        com.fasterxml.jackson.databind.f<Object> findValueSerializer = kVar.findValueSerializer(cls, aVar);
        return new d(findValueSerializer, h(cls, findValueSerializer));
    }

    public abstract a h(Class<?> cls, com.fasterxml.jackson.databind.f<Object> fVar);

    public abstract com.fasterxml.jackson.databind.f<Object> i(Class<?> cls);

    public a(a aVar) {
        this.a = aVar.a;
    }
}
