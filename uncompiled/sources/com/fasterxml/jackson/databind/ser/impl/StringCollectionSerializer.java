package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase;
import java.io.IOException;
import java.util.Collection;
import org.web3j.abi.datatypes.Utf8String;

@pt1
/* loaded from: classes.dex */
public class StringCollectionSerializer extends StaticListSerializerBase<Collection<String>> {
    public static final StringCollectionSerializer instance = new StringCollectionSerializer();

    public StringCollectionSerializer() {
        super(Collection.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase
    public f<?> _withResolved(com.fasterxml.jackson.databind.a aVar, f<?> fVar, Boolean bool) {
        return new StringCollectionSerializer(this, fVar, bool);
    }

    public final void a(Collection<String> collection, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (this._serializer == null) {
            serializeContents(collection, jsonGenerator, kVar);
        } else {
            b(collection, jsonGenerator, kVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase
    public void acceptContentVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.a aVar) throws JsonMappingException {
        aVar.i(JsonFormatTypes.STRING);
    }

    public final void b(Collection<String> collection, JsonGenerator jsonGenerator, k kVar) throws IOException, JsonGenerationException {
        f<String> fVar = this._serializer;
        for (String str : collection) {
            if (str == null) {
                try {
                    kVar.defaultSerializeNull(jsonGenerator);
                } catch (Exception e) {
                    wrapAndThrow(kVar, e, collection, 0);
                }
            } else {
                fVar.serialize(str, jsonGenerator, kVar);
            }
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase
    public d contentSchema() {
        return createSchemaNode(Utf8String.TYPE_NAME, true);
    }

    public final void serializeContents(Collection<String> collection, JsonGenerator jsonGenerator, k kVar) throws IOException, JsonGenerationException {
        if (this._serializer != null) {
            b(collection, jsonGenerator, kVar);
            return;
        }
        int i = 0;
        for (String str : collection) {
            if (str == null) {
                try {
                    kVar.defaultSerializeNull(jsonGenerator);
                } catch (Exception e) {
                    wrapAndThrow(kVar, e, collection, i);
                }
            } else {
                jsonGenerator.o1(str);
            }
            i++;
        }
    }

    public StringCollectionSerializer(StringCollectionSerializer stringCollectionSerializer, f<?> fVar, Boolean bool) {
        super(stringCollectionSerializer, fVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Collection<String> collection, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int size = collection.size();
        if (size == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
            a(collection, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.f1(size);
        if (this._serializer == null) {
            serializeContents(collection, jsonGenerator, kVar);
        } else {
            b(collection, jsonGenerator, kVar);
        }
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(Collection<String> collection, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException, JsonGenerationException {
        cVar.h(collection, jsonGenerator);
        if (this._serializer == null) {
            serializeContents(collection, jsonGenerator, kVar);
        } else {
            b(collection, jsonGenerator, kVar);
        }
        cVar.l(collection, jsonGenerator);
    }
}
