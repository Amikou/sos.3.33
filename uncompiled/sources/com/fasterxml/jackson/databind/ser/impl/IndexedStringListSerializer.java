package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase;
import java.io.IOException;
import java.util.List;
import org.web3j.abi.datatypes.Utf8String;

@pt1
/* loaded from: classes.dex */
public final class IndexedStringListSerializer extends StaticListSerializerBase<List<String>> {
    public static final IndexedStringListSerializer instance = new IndexedStringListSerializer();
    private static final long serialVersionUID = 1;

    public IndexedStringListSerializer() {
        super(List.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase
    public f<?> _withResolved(com.fasterxml.jackson.databind.a aVar, f<?> fVar, Boolean bool) {
        return new IndexedStringListSerializer(this, fVar, bool);
    }

    public final void a(List<String> list, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (this._serializer == null) {
            b(list, jsonGenerator, kVar, 1);
        } else {
            c(list, jsonGenerator, kVar, 1);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase
    public void acceptContentVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.a aVar) throws JsonMappingException {
        aVar.i(JsonFormatTypes.STRING);
    }

    public final void b(List<String> list, JsonGenerator jsonGenerator, k kVar, int i) throws IOException {
        for (int i2 = 0; i2 < i; i2++) {
            try {
                String str = list.get(i2);
                if (str == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else {
                    jsonGenerator.o1(str);
                }
            } catch (Exception e) {
                wrapAndThrow(kVar, e, list, i2);
                return;
            }
        }
    }

    public final void c(List<String> list, JsonGenerator jsonGenerator, k kVar, int i) throws IOException {
        int i2 = 0;
        try {
            f<String> fVar = this._serializer;
            while (i2 < i) {
                String str = list.get(i2);
                if (str == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else {
                    fVar.serialize(str, jsonGenerator, kVar);
                }
                i2++;
            }
        } catch (Exception e) {
            wrapAndThrow(kVar, e, list, i2);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase
    public d contentSchema() {
        return createSchemaNode(Utf8String.TYPE_NAME, true);
    }

    public IndexedStringListSerializer(IndexedStringListSerializer indexedStringListSerializer, f<?> fVar, Boolean bool) {
        super(indexedStringListSerializer, fVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(List<String> list, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int size = list.size();
        if (size == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
            a(list, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.f1(size);
        if (this._serializer == null) {
            b(list, jsonGenerator, kVar, size);
        } else {
            c(list, jsonGenerator, kVar, size);
        }
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(List<String> list, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        int size = list.size();
        cVar.h(list, jsonGenerator);
        if (this._serializer == null) {
            b(list, jsonGenerator, kVar, size);
        } else {
            c(list, jsonGenerator, kVar, size);
        }
        cVar.l(list, jsonGenerator);
    }
}
