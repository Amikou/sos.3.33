package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.jsonFormatVisitors.d;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes.dex */
public class UnwrappingBeanPropertyWriter extends BeanPropertyWriter {
    private static final long serialVersionUID = 1;
    public final NameTransformer _nameTransformer;

    /* loaded from: classes.dex */
    public class a extends b.a {
        public final /* synthetic */ d b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(UnwrappingBeanPropertyWriter unwrappingBeanPropertyWriter, k kVar, d dVar) {
            super(kVar);
            this.b = dVar;
        }

        @Override // com.fasterxml.jackson.databind.jsonFormatVisitors.b
        public d l(JavaType javaType) throws JsonMappingException {
            return this.b;
        }
    }

    public UnwrappingBeanPropertyWriter(BeanPropertyWriter beanPropertyWriter, NameTransformer nameTransformer) {
        super(beanPropertyWriter);
        this._nameTransformer = nameTransformer;
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
    public void _depositSchemaProperty(m mVar, com.fasterxml.jackson.databind.d dVar) {
        com.fasterxml.jackson.databind.d w = dVar.w("properties");
        if (w != null) {
            Iterator<Map.Entry<String, com.fasterxml.jackson.databind.d>> t = w.t();
            while (t.hasNext()) {
                Map.Entry<String, com.fasterxml.jackson.databind.d> next = t.next();
                String key = next.getKey();
                NameTransformer nameTransformer = this._nameTransformer;
                if (nameTransformer != null) {
                    key = nameTransformer.transform(key);
                }
                mVar.Z(key, next.getValue());
            }
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
    public f<Object> _findAndAddDynamic(com.fasterxml.jackson.databind.ser.impl.a aVar, Class<?> cls, k kVar) throws JsonMappingException {
        f<Object> findValueSerializer;
        JavaType javaType = this._nonTrivialBaseType;
        if (javaType != null) {
            findValueSerializer = kVar.findValueSerializer(kVar.constructSpecializedType(javaType, cls), this);
        } else {
            findValueSerializer = kVar.findValueSerializer(cls, this);
        }
        NameTransformer nameTransformer = this._nameTransformer;
        if (findValueSerializer.isUnwrappingSerializer()) {
            nameTransformer = NameTransformer.chainedTransformer(nameTransformer, ((UnwrappingBeanSerializer) findValueSerializer)._nameTransformer);
        }
        f<Object> unwrappingSerializer = findValueSerializer.unwrappingSerializer(nameTransformer);
        this._dynamicSerializers = this._dynamicSerializers.h(cls, unwrappingSerializer);
        return unwrappingSerializer;
    }

    public UnwrappingBeanPropertyWriter _new(NameTransformer nameTransformer, SerializedString serializedString) {
        return new UnwrappingBeanPropertyWriter(this, nameTransformer, serializedString);
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
    public void assignSerializer(f<Object> fVar) {
        super.assignSerializer(fVar);
        f<Object> fVar2 = this._serializer;
        if (fVar2 != null) {
            NameTransformer nameTransformer = this._nameTransformer;
            if (fVar2.isUnwrappingSerializer()) {
                nameTransformer = NameTransformer.chainedTransformer(nameTransformer, ((UnwrappingBeanSerializer) this._serializer)._nameTransformer);
            }
            this._serializer = this._serializer.unwrappingSerializer(nameTransformer);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public void depositSchemaProperty(d dVar, k kVar) throws JsonMappingException {
        f<Object> unwrappingSerializer = kVar.findValueSerializer(getType(), this).unwrappingSerializer(this._nameTransformer);
        if (unwrappingSerializer.isUnwrappingSerializer()) {
            unwrappingSerializer.acceptJsonFormatVisitor(new a(this, kVar, dVar), getType());
        } else {
            super.depositSchemaProperty(dVar, kVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
    public boolean isUnwrapping() {
        return true;
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter, com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsField(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        Object obj2 = get(obj);
        if (obj2 == null) {
            return;
        }
        f<?> fVar = this._serializer;
        if (fVar == null) {
            Class<?> cls = obj2.getClass();
            com.fasterxml.jackson.databind.ser.impl.a aVar = this._dynamicSerializers;
            f<?> i = aVar.i(cls);
            fVar = i == null ? _findAndAddDynamic(aVar, cls, kVar) : i;
        }
        Object obj3 = this._suppressableValue;
        if (obj3 != null) {
            if (BeanPropertyWriter.MARKER_FOR_EMPTY == obj3) {
                if (fVar.isEmpty(kVar, obj2)) {
                    return;
                }
            } else if (obj3.equals(obj2)) {
                return;
            }
        }
        if (obj2 == obj && _handleSelfReference(obj, jsonGenerator, kVar, fVar)) {
            return;
        }
        if (!fVar.isUnwrappingSerializer()) {
            jsonGenerator.g0(this._name);
        }
        com.fasterxml.jackson.databind.jsontype.c cVar = this._typeSerializer;
        if (cVar == null) {
            fVar.serialize(obj2, jsonGenerator, kVar);
        } else {
            fVar.serializeWithType(obj2, jsonGenerator, kVar, cVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.BeanPropertyWriter
    public UnwrappingBeanPropertyWriter rename(NameTransformer nameTransformer) {
        return _new(NameTransformer.chainedTransformer(nameTransformer, this._nameTransformer), new SerializedString(nameTransformer.transform(this._name.getValue())));
    }

    public UnwrappingBeanPropertyWriter(UnwrappingBeanPropertyWriter unwrappingBeanPropertyWriter, NameTransformer nameTransformer, SerializedString serializedString) {
        super(unwrappingBeanPropertyWriter, serializedString);
        this._nameTransformer = nameTransformer;
    }
}
