package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.lang.reflect.Type;

/* loaded from: classes.dex */
public class UnknownSerializer extends StdSerializer<Object> {
    public UnknownSerializer() {
        super(Object.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        bVar.e(javaType);
    }

    public void failForEmpty(k kVar, Object obj) throws JsonMappingException {
        kVar.reportMappingProblem("No serializer found for class %s and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS)", obj.getClass().getName());
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) throws JsonMappingException {
        return null;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Object obj) {
        return true;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (kVar.isEnabled(SerializationFeature.FAIL_ON_EMPTY_BEANS)) {
            failForEmpty(kVar, obj);
        }
        jsonGenerator.i1();
        jsonGenerator.f0();
    }

    @Override // com.fasterxml.jackson.databind.f
    public final void serializeWithType(Object obj, JsonGenerator jsonGenerator, k kVar, com.fasterxml.jackson.databind.jsontype.c cVar) throws IOException {
        if (kVar.isEnabled(SerializationFeature.FAIL_ON_EMPTY_BEANS)) {
            failForEmpty(kVar, obj);
        }
        cVar.i(obj, jsonGenerator);
        cVar.m(obj, jsonGenerator);
    }

    public UnknownSerializer(Class<?> cls) {
        super(cls, false);
    }
}
