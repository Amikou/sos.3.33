package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/* loaded from: classes.dex */
public abstract class ContainerSerializer<T> extends StdSerializer<T> {
    public ContainerSerializer(Class<T> cls) {
        super(cls);
    }

    public abstract ContainerSerializer<?> _withValueTypeSerializer(com.fasterxml.jackson.databind.jsontype.c cVar);

    public abstract com.fasterxml.jackson.databind.f<?> getContentSerializer();

    public abstract JavaType getContentType();

    @Deprecated
    public boolean hasContentTypeAnnotation(k kVar, com.fasterxml.jackson.databind.a aVar) {
        return false;
    }

    public abstract boolean hasSingleElement(T t);

    @Override // com.fasterxml.jackson.databind.f
    @Deprecated
    public boolean isEmpty(T t) {
        return isEmpty(null, t);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ContainerSerializer<?> withValueTypeSerializer(com.fasterxml.jackson.databind.jsontype.c cVar) {
        return cVar == null ? this : _withValueTypeSerializer(cVar);
    }

    public ContainerSerializer(JavaType javaType) {
        super(javaType);
    }

    public ContainerSerializer(Class<?> cls, boolean z) {
        super(cls, z);
    }

    public ContainerSerializer(ContainerSerializer<?> containerSerializer) {
        super(containerSerializer._handledType, false);
    }
}
