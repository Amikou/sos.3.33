package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;

@pt1
/* loaded from: classes.dex */
public class NumberSerializer extends StdScalarSerializer<Number> {
    public static final NumberSerializer instance = new NumberSerializer(Number.class);
    public final boolean _isInt;

    public NumberSerializer(Class<? extends Number> cls) {
        super(cls, false);
        this._isInt = cls == BigInteger.class;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        if (this._isInt) {
            visitIntFormat(bVar, javaType, JsonParser.NumberType.BIG_INTEGER);
        } else if (handledType() == BigDecimal.class) {
            visitFloatFormat(bVar, javaType, JsonParser.NumberType.BIG_DECIMAL);
        } else {
            bVar.f(javaType);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode(this._isInt ? "integer" : "number", true);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Number number, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (number instanceof BigDecimal) {
            jsonGenerator.B0((BigDecimal) number);
        } else if (number instanceof BigInteger) {
            jsonGenerator.F0((BigInteger) number);
        } else if (number instanceof Long) {
            jsonGenerator.y0(number.longValue());
        } else if (number instanceof Double) {
            jsonGenerator.r0(number.doubleValue());
        } else if (number instanceof Float) {
            jsonGenerator.w0(number.floatValue());
        } else if (!(number instanceof Integer) && !(number instanceof Byte) && !(number instanceof Short)) {
            jsonGenerator.z0(number.toString());
        } else {
            jsonGenerator.x0(number.intValue());
        }
    }
}
