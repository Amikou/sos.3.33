package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.b;
import java.io.IOException;

/* loaded from: classes.dex */
public abstract class ArraySerializerBase<T> extends ContainerSerializer<T> implements b {
    public final a _property;
    public final Boolean _unwrapSingle;

    public ArraySerializerBase(Class<T> cls) {
        super(cls);
        this._property = null;
        this._unwrapSingle = null;
    }

    public abstract f<?> _withResolved(a aVar, Boolean bool);

    /* JADX WARN: Multi-variable type inference failed */
    public f<?> createContextual(k kVar, a aVar) throws JsonMappingException {
        JsonFormat.Value findFormatOverrides;
        Boolean feature;
        return (aVar == null || (findFormatOverrides = findFormatOverrides(kVar, aVar, handledType())) == null || (feature = findFormatOverrides.getFeature(JsonFormat.Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) == this._unwrapSingle) ? this : _withResolved(aVar, feature);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(T t, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE) && hasSingleElement(t)) {
            serializeContents(t, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.u(t);
        jsonGenerator.e1();
        serializeContents(t, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    public abstract void serializeContents(T t, JsonGenerator jsonGenerator, k kVar) throws IOException;

    @Override // com.fasterxml.jackson.databind.f
    public final void serializeWithType(T t, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        cVar.h(t, jsonGenerator);
        jsonGenerator.u(t);
        serializeContents(t, jsonGenerator, kVar);
        cVar.l(t, jsonGenerator);
    }

    @Deprecated
    public ArraySerializerBase(Class<T> cls, a aVar) {
        super(cls);
        this._property = aVar;
        this._unwrapSingle = null;
    }

    public ArraySerializerBase(ArraySerializerBase<?> arraySerializerBase) {
        super(arraySerializerBase._handledType, false);
        this._property = arraySerializerBase._property;
        this._unwrapSingle = arraySerializerBase._unwrapSingle;
    }

    public ArraySerializerBase(ArraySerializerBase<?> arraySerializerBase, a aVar, Boolean bool) {
        super(arraySerializerBase._handledType, false);
        this._property = aVar;
        this._unwrapSingle = bool;
    }

    @Deprecated
    public ArraySerializerBase(ArraySerializerBase<?> arraySerializerBase, a aVar) {
        super(arraySerializerBase._handledType, false);
        this._property = aVar;
        this._unwrapSingle = arraySerializerBase._unwrapSingle;
    }
}
