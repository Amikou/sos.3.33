package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.impl.a;
import com.fasterxml.jackson.databind.type.ArrayType;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;

@pt1
/* loaded from: classes.dex */
public class ObjectArraySerializer extends ArraySerializerBase<Object[]> {
    public a _dynamicSerializers;
    public f<Object> _elementSerializer;
    public final JavaType _elementType;
    public final boolean _staticTyping;
    public final c _valueTypeSerializer;

    public ObjectArraySerializer(JavaType javaType, boolean z, c cVar, f<Object> fVar) {
        super(Object[].class);
        this._elementType = javaType;
        this._staticTyping = z;
        this._valueTypeSerializer = cVar;
        this._dynamicSerializers = a.a();
        this._elementSerializer = fVar;
    }

    public final f<Object> _findAndAddDynamic(a aVar, Class<?> cls, k kVar) throws JsonMappingException {
        a.d g = aVar.g(cls, kVar, this._property);
        a aVar2 = g.b;
        if (aVar != aVar2) {
            this._dynamicSerializers = aVar2;
        }
        return g.a;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
    public f<?> _withResolved(com.fasterxml.jackson.databind.a aVar, Boolean bool) {
        return new ObjectArraySerializer(this, aVar, this._valueTypeSerializer, this._elementSerializer, bool);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
        return new ObjectArraySerializer(this._elementType, this._staticTyping, cVar, this._elementSerializer);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        com.fasterxml.jackson.databind.jsonFormatVisitors.a j = bVar.j(javaType);
        if (j != null) {
            JavaType moreSpecificType = bVar.b().getTypeFactory().moreSpecificType(this._elementType, javaType.getContentType());
            if (moreSpecificType != null) {
                f<Object> fVar = this._elementSerializer;
                if (fVar == null) {
                    fVar = bVar.b().findValueSerializer(moreSpecificType, this._property);
                }
                j.n(fVar, moreSpecificType);
                return;
            }
            throw JsonMappingException.from(bVar.b(), "Could not resolve type");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:15:0x002b  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0033  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x003b  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0050  */
    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.f<?> createContextual(com.fasterxml.jackson.databind.k r6, com.fasterxml.jackson.databind.a r7) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r5 = this;
            com.fasterxml.jackson.databind.jsontype.c r0 = r5._valueTypeSerializer
            if (r0 == 0) goto L8
            com.fasterxml.jackson.databind.jsontype.c r0 = r0.a(r7)
        L8:
            r1 = 0
            if (r7 == 0) goto L20
            com.fasterxml.jackson.databind.introspect.AnnotatedMember r2 = r7.getMember()
            com.fasterxml.jackson.databind.AnnotationIntrospector r3 = r6.getAnnotationIntrospector()
            if (r2 == 0) goto L20
            java.lang.Object r3 = r3.findContentSerializer(r2)
            if (r3 == 0) goto L20
            com.fasterxml.jackson.databind.f r2 = r6.serializerInstance(r2, r3)
            goto L21
        L20:
            r2 = r1
        L21:
            java.lang.Class r3 = r5.handledType()
            com.fasterxml.jackson.annotation.JsonFormat$Value r3 = r5.findFormatOverrides(r6, r7, r3)
            if (r3 == 0) goto L31
            com.fasterxml.jackson.annotation.JsonFormat$Feature r1 = com.fasterxml.jackson.annotation.JsonFormat.Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            java.lang.Boolean r1 = r3.getFeature(r1)
        L31:
            if (r2 != 0) goto L35
            com.fasterxml.jackson.databind.f<java.lang.Object> r2 = r5._elementSerializer
        L35:
            com.fasterxml.jackson.databind.f r2 = r5.findConvertingContentSerializer(r6, r7, r2)
            if (r2 != 0) goto L50
            com.fasterxml.jackson.databind.JavaType r3 = r5._elementType
            if (r3 == 0) goto L54
            boolean r4 = r5._staticTyping
            if (r4 == 0) goto L54
            boolean r3 = r3.isJavaLangObject()
            if (r3 != 0) goto L54
            com.fasterxml.jackson.databind.JavaType r2 = r5._elementType
            com.fasterxml.jackson.databind.f r2 = r6.findValueSerializer(r2, r7)
            goto L54
        L50:
            com.fasterxml.jackson.databind.f r2 = r6.handleSecondaryContextualization(r2, r7)
        L54:
            com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer r6 = r5.withResolved(r7, r0, r2, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer.createContextual(com.fasterxml.jackson.databind.k, com.fasterxml.jackson.databind.a):com.fasterxml.jackson.databind.f");
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public f<?> getContentSerializer() {
        return this._elementSerializer;
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public JavaType getContentType() {
        return this._elementType;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) throws JsonMappingException {
        d a;
        m createSchemaNode = createSchemaNode("array", true);
        if (type != null) {
            JavaType constructType = kVar.constructType(type);
            if (constructType.isArrayType()) {
                Class<?> rawClass = ((ArrayType) constructType).getContentType().getRawClass();
                if (rawClass == Object.class) {
                    createSchemaNode.Z("items", aw1.a());
                } else {
                    f<Object> findValueSerializer = kVar.findValueSerializer(rawClass, this._property);
                    if (findValueSerializer instanceof ld3) {
                        a = ((ld3) findValueSerializer).getSchema(kVar, null);
                    } else {
                        a = aw1.a();
                    }
                    createSchemaNode.Z("items", a);
                }
            }
        }
        return createSchemaNode;
    }

    public void serializeContentsUsing(Object[] objArr, JsonGenerator jsonGenerator, k kVar, f<Object> fVar) throws IOException {
        int length = objArr.length;
        c cVar = this._valueTypeSerializer;
        Object obj = null;
        for (int i = 0; i < length; i++) {
            try {
                obj = objArr[i];
                if (obj == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else if (cVar == null) {
                    fVar.serialize(obj, jsonGenerator, kVar);
                } else {
                    fVar.serializeWithType(obj, jsonGenerator, kVar, cVar);
                }
            } catch (IOException e) {
                throw e;
            } catch (Exception e2) {
                e = e2;
                while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                    e = e.getCause();
                }
                if (e instanceof Error) {
                    throw ((Error) e);
                }
                throw JsonMappingException.wrapWithPath(e, obj, i);
            }
        }
    }

    public void serializeTypedContents(Object[] objArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int length = objArr.length;
        c cVar = this._valueTypeSerializer;
        int i = 0;
        Object obj = null;
        try {
            a aVar = this._dynamicSerializers;
            while (i < length) {
                obj = objArr[i];
                if (obj == null) {
                    kVar.defaultSerializeNull(jsonGenerator);
                } else {
                    Class<?> cls = obj.getClass();
                    f<Object> i2 = aVar.i(cls);
                    if (i2 == null) {
                        i2 = _findAndAddDynamic(aVar, cls, kVar);
                    }
                    i2.serializeWithType(obj, jsonGenerator, kVar, cVar);
                }
                i++;
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            e = e2;
            while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                e = e.getCause();
            }
            if (e instanceof Error) {
                throw ((Error) e);
            }
            throw JsonMappingException.wrapWithPath(e, obj, i);
        }
    }

    public ObjectArraySerializer withResolved(com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar, Boolean bool) {
        return (this._property == aVar && fVar == this._elementSerializer && this._valueTypeSerializer == cVar && this._unwrapSingle == bool) ? this : new ObjectArraySerializer(this, aVar, cVar, fVar, bool);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(Object[] objArr) {
        return objArr.length == 1;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, Object[] objArr) {
        return objArr == null || objArr.length == 0;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(Object[] objArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int length = objArr.length;
        if (length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
            serializeContents(objArr, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.f1(length);
        serializeContents(objArr, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
    public void serializeContents(Object[] objArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int length = objArr.length;
        if (length == 0) {
            return;
        }
        f<Object> fVar = this._elementSerializer;
        if (fVar != null) {
            serializeContentsUsing(objArr, jsonGenerator, kVar, fVar);
        } else if (this._valueTypeSerializer != null) {
            serializeTypedContents(objArr, jsonGenerator, kVar);
        } else {
            int i = 0;
            Object obj = null;
            try {
                a aVar = this._dynamicSerializers;
                while (i < length) {
                    obj = objArr[i];
                    if (obj == null) {
                        kVar.defaultSerializeNull(jsonGenerator);
                    } else {
                        Class<?> cls = obj.getClass();
                        f<Object> i2 = aVar.i(cls);
                        if (i2 == null) {
                            if (this._elementType.hasGenericTypes()) {
                                i2 = _findAndAddDynamic(aVar, kVar.constructSpecializedType(this._elementType, cls), kVar);
                            } else {
                                i2 = _findAndAddDynamic(aVar, cls, kVar);
                            }
                        }
                        i2.serialize(obj, jsonGenerator, kVar);
                    }
                    i++;
                }
            } catch (IOException e) {
                throw e;
            } catch (Exception e2) {
                e = e2;
                while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                    e = e.getCause();
                }
                if (e instanceof Error) {
                    throw ((Error) e);
                }
                throw JsonMappingException.wrapWithPath(e, obj, i);
            }
        }
    }

    public final f<Object> _findAndAddDynamic(a aVar, JavaType javaType, k kVar) throws JsonMappingException {
        a.d f = aVar.f(javaType, kVar, this._property);
        a aVar2 = f.b;
        if (aVar != aVar2) {
            this._dynamicSerializers = aVar2;
        }
        return f.a;
    }

    public ObjectArraySerializer(ObjectArraySerializer objectArraySerializer, c cVar) {
        super(objectArraySerializer);
        this._elementType = objectArraySerializer._elementType;
        this._valueTypeSerializer = cVar;
        this._staticTyping = objectArraySerializer._staticTyping;
        this._dynamicSerializers = objectArraySerializer._dynamicSerializers;
        this._elementSerializer = objectArraySerializer._elementSerializer;
    }

    public ObjectArraySerializer(ObjectArraySerializer objectArraySerializer, com.fasterxml.jackson.databind.a aVar, c cVar, f<?> fVar, Boolean bool) {
        super(objectArraySerializer, aVar, bool);
        this._elementType = objectArraySerializer._elementType;
        this._valueTypeSerializer = cVar;
        this._staticTyping = objectArraySerializer._staticTyping;
        this._dynamicSerializers = objectArraySerializer._dynamicSerializers;
        this._elementSerializer = fVar;
    }
}
