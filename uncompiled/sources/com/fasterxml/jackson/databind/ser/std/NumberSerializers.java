package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.b;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

/* loaded from: classes.dex */
public class NumberSerializers {

    /* loaded from: classes.dex */
    public static abstract class Base<T> extends StdScalarSerializer<T> implements b {
        public final boolean _isInt;
        public final JsonParser.NumberType _numberType;
        public final String _schemaType;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Base(Class<?> cls, JsonParser.NumberType numberType, String str) {
            super(cls, false);
            boolean z = false;
            this._numberType = numberType;
            this._schemaType = str;
            this._isInt = (numberType == JsonParser.NumberType.INT || numberType == JsonParser.NumberType.LONG || numberType == JsonParser.NumberType.BIG_INTEGER) ? true : true;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
            if (this._isInt) {
                visitIntFormat(bVar, javaType, this._numberType);
            } else {
                visitFloatFormat(bVar, javaType, this._numberType);
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fasterxml.jackson.databind.ser.b
        public f<?> createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
            JsonFormat.Value findFormatOverrides = findFormatOverrides(kVar, aVar, handledType());
            return (findFormatOverrides == null || a.a[findFormatOverrides.getShape().ordinal()] != 1) ? this : ToStringSerializer.instance;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            return createSchemaNode(this._schemaType, true);
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class DoubleSerializer extends Base<Object> {
        public DoubleSerializer(Class<?> cls) {
            super(cls, JsonParser.NumberType.DOUBLE, "number");
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public /* bridge */ /* synthetic */ void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
            super.acceptJsonFormatVisitor(bVar, javaType);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.b
        public /* bridge */ /* synthetic */ f createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
            return super.createContextual(kVar, aVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public /* bridge */ /* synthetic */ d getSchema(k kVar, Type type) {
            return super.getSchema(kVar, type);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            jsonGenerator.r0(((Double) obj).doubleValue());
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.f
        public void serializeWithType(Object obj, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
            serialize(obj, jsonGenerator, kVar);
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class FloatSerializer extends Base<Object> {
        public static final FloatSerializer instance = new FloatSerializer();

        public FloatSerializer() {
            super(Float.class, JsonParser.NumberType.FLOAT, "number");
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public /* bridge */ /* synthetic */ void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
            super.acceptJsonFormatVisitor(bVar, javaType);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.b
        public /* bridge */ /* synthetic */ f createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
            return super.createContextual(kVar, aVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public /* bridge */ /* synthetic */ d getSchema(k kVar, Type type) {
            return super.getSchema(kVar, type);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            jsonGenerator.w0(((Float) obj).floatValue());
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class IntLikeSerializer extends Base<Object> {
        public static final IntLikeSerializer instance = new IntLikeSerializer();

        public IntLikeSerializer() {
            super(Number.class, JsonParser.NumberType.INT, "integer");
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public /* bridge */ /* synthetic */ void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
            super.acceptJsonFormatVisitor(bVar, javaType);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.b
        public /* bridge */ /* synthetic */ f createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
            return super.createContextual(kVar, aVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public /* bridge */ /* synthetic */ d getSchema(k kVar, Type type) {
            return super.getSchema(kVar, type);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            jsonGenerator.x0(((Number) obj).intValue());
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class IntegerSerializer extends Base<Object> {
        public IntegerSerializer(Class<?> cls) {
            super(cls, JsonParser.NumberType.INT, "integer");
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public /* bridge */ /* synthetic */ void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
            super.acceptJsonFormatVisitor(bVar, javaType);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.b
        public /* bridge */ /* synthetic */ f createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
            return super.createContextual(kVar, aVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public /* bridge */ /* synthetic */ d getSchema(k kVar, Type type) {
            return super.getSchema(kVar, type);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            jsonGenerator.x0(((Integer) obj).intValue());
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.f
        public void serializeWithType(Object obj, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
            serialize(obj, jsonGenerator, kVar);
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class LongSerializer extends Base<Object> {
        public LongSerializer(Class<?> cls) {
            super(cls, JsonParser.NumberType.LONG, "number");
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public /* bridge */ /* synthetic */ void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
            super.acceptJsonFormatVisitor(bVar, javaType);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.b
        public /* bridge */ /* synthetic */ f createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
            return super.createContextual(kVar, aVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public /* bridge */ /* synthetic */ d getSchema(k kVar, Type type) {
            return super.getSchema(kVar, type);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            jsonGenerator.y0(((Long) obj).longValue());
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static final class ShortSerializer extends Base<Object> {
        public static final ShortSerializer instance = new ShortSerializer();

        public ShortSerializer() {
            super(Short.class, JsonParser.NumberType.INT, "number");
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public /* bridge */ /* synthetic */ void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
            super.acceptJsonFormatVisitor(bVar, javaType);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.b
        public /* bridge */ /* synthetic */ f createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
            return super.createContextual(kVar, aVar);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.NumberSerializers.Base, com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public /* bridge */ /* synthetic */ d getSchema(k kVar, Type type) {
            return super.getSchema(kVar, type);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
            jsonGenerator.H0(((Short) obj).shortValue());
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonFormat.Shape.values().length];
            a = iArr;
            try {
                iArr[JsonFormat.Shape.STRING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public static void a(Map<String, f<?>> map) {
        map.put(Integer.class.getName(), new IntegerSerializer(Integer.class));
        Class cls = Integer.TYPE;
        map.put(cls.getName(), new IntegerSerializer(cls));
        map.put(Long.class.getName(), new LongSerializer(Long.class));
        Class cls2 = Long.TYPE;
        map.put(cls2.getName(), new LongSerializer(cls2));
        String name = Byte.class.getName();
        IntLikeSerializer intLikeSerializer = IntLikeSerializer.instance;
        map.put(name, intLikeSerializer);
        map.put(Byte.TYPE.getName(), intLikeSerializer);
        String name2 = Short.class.getName();
        ShortSerializer shortSerializer = ShortSerializer.instance;
        map.put(name2, shortSerializer);
        map.put(Short.TYPE.getName(), shortSerializer);
        map.put(Double.class.getName(), new DoubleSerializer(Double.class));
        map.put(Double.TYPE.getName(), new DoubleSerializer(Double.TYPE));
        String name3 = Float.class.getName();
        FloatSerializer floatSerializer = FloatSerializer.instance;
        map.put(name3, floatSerializer);
        map.put(Float.TYPE.getName(), floatSerializer);
    }
}
