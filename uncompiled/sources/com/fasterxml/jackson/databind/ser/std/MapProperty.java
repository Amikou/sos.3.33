package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.jsonFormatVisitors.d;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.lang.annotation.Annotation;

/* loaded from: classes.dex */
public class MapProperty extends PropertyWriter {
    private static final long serialVersionUID = 1;
    public Object _key;
    public f<Object> _keySerializer;
    public final a _property;
    public final c _typeSerializer;
    public f<Object> _valueSerializer;

    public MapProperty(c cVar, a aVar) {
        super(aVar == null ? PropertyMetadata.STD_REQUIRED_OR_OPTIONAL : aVar.getMetadata());
        this._typeSerializer = cVar;
        this._property = aVar;
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public void depositSchemaProperty(d dVar, k kVar) throws JsonMappingException {
        a aVar = this._property;
        if (aVar != null) {
            aVar.depositSchemaProperty(dVar, kVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    @Deprecated
    public void depositSchemaProperty(m mVar, k kVar) throws JsonMappingException {
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public <A extends Annotation> A getAnnotation(Class<A> cls) {
        a aVar = this._property;
        if (aVar == null) {
            return null;
        }
        return (A) aVar.getAnnotation(cls);
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public <A extends Annotation> A getContextAnnotation(Class<A> cls) {
        a aVar = this._property;
        if (aVar == null) {
            return null;
        }
        return (A) aVar.getContextAnnotation(cls);
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase
    public PropertyName getFullName() {
        return new PropertyName(getName());
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public AnnotatedMember getMember() {
        a aVar = this._property;
        if (aVar == null) {
            return null;
        }
        return aVar.getMember();
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public String getName() {
        Object obj = this._key;
        if (obj instanceof String) {
            return (String) obj;
        }
        return String.valueOf(obj);
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public JavaType getType() {
        a aVar = this._property;
        return aVar == null ? TypeFactory.unknownType() : aVar.getType();
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter, com.fasterxml.jackson.databind.introspect.ConcreteBeanPropertyBase, com.fasterxml.jackson.databind.a
    public PropertyName getWrapperName() {
        a aVar = this._property;
        if (aVar == null) {
            return null;
        }
        return aVar.getWrapperName();
    }

    public void reset(Object obj, f<Object> fVar, f<Object> fVar2) {
        this._key = obj;
        this._keySerializer = fVar;
        this._valueSerializer = fVar2;
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsElement(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        c cVar = this._typeSerializer;
        if (cVar == null) {
            this._valueSerializer.serialize(obj, jsonGenerator, kVar);
        } else {
            this._valueSerializer.serializeWithType(obj, jsonGenerator, kVar, cVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsField(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        this._keySerializer.serialize(this._key, jsonGenerator, kVar);
        c cVar = this._typeSerializer;
        if (cVar == null) {
            this._valueSerializer.serialize(obj, jsonGenerator, kVar);
        } else {
            this._valueSerializer.serializeWithType(obj, jsonGenerator, kVar, cVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsOmittedField(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        if (jsonGenerator.e()) {
            return;
        }
        jsonGenerator.S0(getName());
    }

    @Override // com.fasterxml.jackson.databind.ser.PropertyWriter
    public void serializeAsPlaceholder(Object obj, JsonGenerator jsonGenerator, k kVar) throws Exception {
        jsonGenerator.m0();
    }
}
