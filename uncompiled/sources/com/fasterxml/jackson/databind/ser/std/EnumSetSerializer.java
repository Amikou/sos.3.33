package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Iterator;

/* loaded from: classes.dex */
public class EnumSetSerializer extends AsArraySerializerBase<EnumSet<? extends Enum<?>>> {
    public EnumSetSerializer(JavaType javaType) {
        super((Class<?>) EnumSet.class, javaType, true, (c) null, (f<Object>) null);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public EnumSetSerializer _withValueTypeSerializer(c cVar) {
        return this;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public /* bridge */ /* synthetic */ AsArraySerializerBase<EnumSet<? extends Enum<?>>> withResolved(a aVar, c cVar, f fVar, Boolean bool) {
        return withResolved2(aVar, cVar, (f<?>) fVar, bool);
    }

    @Deprecated
    public EnumSetSerializer(JavaType javaType, a aVar) {
        this(javaType);
    }

    @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
    public boolean hasSingleElement(EnumSet<? extends Enum<?>> enumSet) {
        return enumSet.size() == 1;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, EnumSet<? extends Enum<?>> enumSet) {
        return enumSet == null || enumSet.isEmpty();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public final void serialize(EnumSet<? extends Enum<?>> enumSet, JsonGenerator jsonGenerator, k kVar) throws IOException {
        int size = enumSet.size();
        if (size == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
            serializeContents(enumSet, jsonGenerator, kVar);
            return;
        }
        jsonGenerator.f1(size);
        serializeContents(enumSet, jsonGenerator, kVar);
        jsonGenerator.e0();
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    public void serializeContents(EnumSet<? extends Enum<?>> enumSet, JsonGenerator jsonGenerator, k kVar) throws IOException {
        f<Object> fVar = this._elementSerializer;
        Iterator it = enumSet.iterator();
        while (it.hasNext()) {
            Enum r1 = (Enum) it.next();
            if (fVar == null) {
                fVar = kVar.findValueSerializer(r1.getDeclaringClass(), this._property);
            }
            fVar.serialize(r1, jsonGenerator, kVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
    /* renamed from: withResolved  reason: avoid collision after fix types in other method */
    public AsArraySerializerBase<EnumSet<? extends Enum<?>>> withResolved2(a aVar, c cVar, f<?> fVar, Boolean bool) {
        return new EnumSetSerializer(this, aVar, cVar, fVar, bool);
    }

    public EnumSetSerializer(EnumSetSerializer enumSetSerializer, a aVar, c cVar, f<?> fVar, Boolean bool) {
        super(enumSetSerializer, aVar, cVar, fVar, bool);
    }
}
