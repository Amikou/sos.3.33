package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.b;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.LinkedHashSet;

@pt1
/* loaded from: classes.dex */
public class JsonValueSerializer extends StdSerializer<Object> implements b {
    public final AnnotatedMethod _accessorMethod;
    public final boolean _forceTypeInformation;
    public final com.fasterxml.jackson.databind.a _property;
    public final f<Object> _valueSerializer;

    /* loaded from: classes.dex */
    public static class a extends c {
        public final c a;
        public final Object b;

        public a(c cVar, Object obj) {
            this.a = cVar;
            this.b = obj;
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public c a(com.fasterxml.jackson.databind.a aVar) {
            throw new UnsupportedOperationException();
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public String b() {
            return this.a.b();
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public JsonTypeInfo.As c() {
            return this.a.c();
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void d(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
            this.a.d(this.b, jsonGenerator, str);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void e(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
            this.a.e(this.b, jsonGenerator, str);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void f(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
            this.a.f(this.b, jsonGenerator, str);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void g(Object obj, JsonGenerator jsonGenerator, String str) throws IOException {
            this.a.g(this.b, jsonGenerator, str);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void h(Object obj, JsonGenerator jsonGenerator) throws IOException {
            this.a.h(this.b, jsonGenerator);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void i(Object obj, JsonGenerator jsonGenerator) throws IOException {
            this.a.i(this.b, jsonGenerator);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void j(Object obj, JsonGenerator jsonGenerator) throws IOException {
            this.a.j(this.b, jsonGenerator);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void k(Object obj, JsonGenerator jsonGenerator, Class<?> cls) throws IOException {
            this.a.k(this.b, jsonGenerator, cls);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void l(Object obj, JsonGenerator jsonGenerator) throws IOException {
            this.a.l(this.b, jsonGenerator);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void m(Object obj, JsonGenerator jsonGenerator) throws IOException {
            this.a.m(this.b, jsonGenerator);
        }

        @Override // com.fasterxml.jackson.databind.jsontype.c
        public void n(Object obj, JsonGenerator jsonGenerator) throws IOException {
            this.a.n(this.b, jsonGenerator);
        }
    }

    public JsonValueSerializer(AnnotatedMethod annotatedMethod, f<?> fVar) {
        super(annotatedMethod.getType());
        this._accessorMethod = annotatedMethod;
        this._valueSerializer = fVar;
        this._property = null;
        this._forceTypeInformation = true;
    }

    public static final Class<Object> a(Class<?> cls) {
        return cls == null ? Object.class : cls;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public boolean _acceptJsonFormatVisitorForEnum(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType, Class<?> cls) throws JsonMappingException {
        Object[] enumConstants;
        ew1 d = bVar.d(javaType);
        if (d != null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (Object obj : cls.getEnumConstants()) {
                try {
                    linkedHashSet.add(String.valueOf(this._accessorMethod.callOn(obj)));
                } catch (Exception e) {
                    e = e;
                    while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                        e = e.getCause();
                    }
                    if (e instanceof Error) {
                        throw ((Error) e);
                    }
                    throw JsonMappingException.wrapWithPath(e, obj, this._accessorMethod.getName() + "()");
                }
            }
            d.b(linkedHashSet);
            return true;
        }
        return true;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        JavaType type = this._accessorMethod.getType();
        Class<?> declaringClass = this._accessorMethod.getDeclaringClass();
        if (declaringClass != null && declaringClass.isEnum() && _acceptJsonFormatVisitorForEnum(bVar, javaType, declaringClass)) {
            return;
        }
        f<Object> fVar = this._valueSerializer;
        if (fVar == null && (fVar = bVar.b().findTypedValueSerializer(type, false, this._property)) == null) {
            bVar.e(javaType);
        } else {
            fVar.acceptJsonFormatVisitor(bVar, null);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        f<?> fVar = this._valueSerializer;
        if (fVar == null) {
            JavaType type = this._accessorMethod.getType();
            if (kVar.isEnabled(MapperFeature.USE_STATIC_TYPING) || type.isFinal()) {
                f<Object> findPrimaryPropertySerializer = kVar.findPrimaryPropertySerializer(type, aVar);
                return withResolved(aVar, findPrimaryPropertySerializer, isNaturalTypeWithStdHandling(type.getRawClass(), findPrimaryPropertySerializer));
            }
            return this;
        }
        return withResolved(aVar, kVar.handlePrimaryContextualization(fVar, aVar), this._forceTypeInformation);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) throws JsonMappingException {
        f<Object> fVar = this._valueSerializer;
        if (fVar instanceof ld3) {
            return ((ld3) fVar).getSchema(kVar, null);
        }
        return aw1.a();
    }

    public boolean isNaturalTypeWithStdHandling(Class<?> cls, f<?> fVar) {
        if (cls.isPrimitive()) {
            if (cls != Integer.TYPE && cls != Boolean.TYPE && cls != Double.TYPE) {
                return false;
            }
        } else if (cls != String.class && cls != Integer.class && cls != Boolean.class && cls != Double.class) {
            return false;
        }
        return isDefaultSerializer(fVar);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        try {
            Object value = this._accessorMethod.getValue(obj);
            if (value == null) {
                kVar.defaultSerializeNull(jsonGenerator);
                return;
            }
            f<Object> fVar = this._valueSerializer;
            if (fVar == null) {
                fVar = kVar.findTypedValueSerializer(value.getClass(), true, this._property);
            }
            fVar.serialize(value, jsonGenerator, kVar);
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            e = e2;
            while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                e = e.getCause();
            }
            if (e instanceof Error) {
                throw ((Error) e);
            }
            throw JsonMappingException.wrapWithPath(e, obj, this._accessorMethod.getName() + "()");
        }
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(Object obj, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        try {
            Object value = this._accessorMethod.getValue(obj);
            if (value == null) {
                kVar.defaultSerializeNull(jsonGenerator);
                return;
            }
            f<Object> fVar = this._valueSerializer;
            if (fVar == null) {
                fVar = kVar.findValueSerializer(value.getClass(), this._property);
            } else if (this._forceTypeInformation) {
                cVar.j(obj, jsonGenerator);
                fVar.serialize(value, jsonGenerator, kVar);
                cVar.n(obj, jsonGenerator);
                return;
            }
            fVar.serializeWithType(value, jsonGenerator, kVar, new a(cVar, obj));
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            e = e2;
            while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                e = e.getCause();
            }
            if (e instanceof Error) {
                throw ((Error) e);
            }
            throw JsonMappingException.wrapWithPath(e, obj, this._accessorMethod.getName() + "()");
        }
    }

    public String toString() {
        return "(@JsonValue serializer for method " + this._accessorMethod.getDeclaringClass() + "#" + this._accessorMethod.getName() + ")";
    }

    public JsonValueSerializer withResolved(com.fasterxml.jackson.databind.a aVar, f<?> fVar, boolean z) {
        return (this._property == aVar && this._valueSerializer == fVar && z == this._forceTypeInformation) ? this : new JsonValueSerializer(this, aVar, fVar, z);
    }

    public JsonValueSerializer(JsonValueSerializer jsonValueSerializer, com.fasterxml.jackson.databind.a aVar, f<?> fVar, boolean z) {
        super(a(jsonValueSerializer.handledType()));
        this._accessorMethod = jsonValueSerializer._accessorMethod;
        this._valueSerializer = fVar;
        this._property = aVar;
        this._forceTypeInformation = z;
    }
}
