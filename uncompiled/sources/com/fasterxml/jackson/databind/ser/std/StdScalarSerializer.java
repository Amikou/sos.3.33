package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.lang.reflect.Type;
import org.web3j.abi.datatypes.Utf8String;

/* loaded from: classes.dex */
public abstract class StdScalarSerializer<T> extends StdSerializer<T> {
    public StdScalarSerializer(Class<T> cls) {
        super(cls);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        visitStringFormat(bVar, javaType);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) throws JsonMappingException {
        return createSchemaNode(Utf8String.TYPE_NAME, true);
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(T t, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        cVar.j(t, jsonGenerator);
        serialize(t, jsonGenerator, kVar);
        cVar.n(t, jsonGenerator);
    }

    public StdScalarSerializer(Class<?> cls, boolean z) {
        super(cls);
    }
}
