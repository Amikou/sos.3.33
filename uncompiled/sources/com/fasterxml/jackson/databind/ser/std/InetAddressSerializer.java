package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.net.InetAddress;

/* loaded from: classes.dex */
public class InetAddressSerializer extends StdScalarSerializer<InetAddress> {
    public InetAddressSerializer() {
        super(InetAddress.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(InetAddress inetAddress, JsonGenerator jsonGenerator, k kVar) throws IOException {
        String trim = inetAddress.toString().trim();
        int indexOf = trim.indexOf(47);
        if (indexOf >= 0) {
            if (indexOf == 0) {
                trim = trim.substring(1);
            } else {
                trim = trim.substring(0, indexOf);
            }
        }
        jsonGenerator.o1(trim);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.f
    public void serializeWithType(InetAddress inetAddress, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException, JsonGenerationException {
        cVar.k(inetAddress, jsonGenerator, InetAddress.class);
        serialize(inetAddress, jsonGenerator, kVar);
        cVar.n(inetAddress, jsonGenerator);
    }
}
