package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators$PropertyGenerator;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.jsonFormatVisitors.d;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.b;
import com.fasterxml.jackson.databind.ser.e;
import com.fasterxml.jackson.databind.ser.impl.PropertyBasedObjectIdGenerator;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

/* loaded from: classes.dex */
public abstract class BeanSerializerBase extends StdSerializer<Object> implements b, e {
    public static final PropertyName NAME_FOR_OBJECT_REF = new PropertyName("#object-ref");
    public static final BeanPropertyWriter[] NO_PROPS = new BeanPropertyWriter[0];
    public final com.fasterxml.jackson.databind.ser.a _anyGetterWriter;
    public final BeanPropertyWriter[] _filteredProps;
    public final kl2 _objectIdWriter;
    public final Object _propertyFilterId;
    public final BeanPropertyWriter[] _props;
    public final JsonFormat.Shape _serializationShape;
    public final AnnotatedMember _typeId;

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonFormat.Shape.values().length];
            a = iArr;
            try {
                iArr[JsonFormat.Shape.STRING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JsonFormat.Shape.NUMBER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JsonFormat.Shape.NUMBER_INT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public BeanSerializerBase(JavaType javaType, wo woVar, BeanPropertyWriter[] beanPropertyWriterArr, BeanPropertyWriter[] beanPropertyWriterArr2) {
        super(javaType);
        this._props = beanPropertyWriterArr;
        this._filteredProps = beanPropertyWriterArr2;
        if (woVar == null) {
            this._typeId = null;
            this._anyGetterWriter = null;
            this._propertyFilterId = null;
            this._objectIdWriter = null;
            this._serializationShape = null;
            return;
        }
        this._typeId = woVar.h();
        this._anyGetterWriter = woVar.c();
        this._propertyFilterId = woVar.e();
        this._objectIdWriter = woVar.f();
        JsonFormat.Value g = woVar.d().g(null);
        this._serializationShape = g != null ? g.getShape() : null;
    }

    public static final BeanPropertyWriter[] a(BeanPropertyWriter[] beanPropertyWriterArr, NameTransformer nameTransformer) {
        if (beanPropertyWriterArr == null || beanPropertyWriterArr.length == 0 || nameTransformer == null || nameTransformer == NameTransformer.NOP) {
            return beanPropertyWriterArr;
        }
        int length = beanPropertyWriterArr.length;
        BeanPropertyWriter[] beanPropertyWriterArr2 = new BeanPropertyWriter[length];
        for (int i = 0; i < length; i++) {
            BeanPropertyWriter beanPropertyWriter = beanPropertyWriterArr[i];
            if (beanPropertyWriter != null) {
                beanPropertyWriterArr2[i] = beanPropertyWriter.rename(nameTransformer);
            }
        }
        return beanPropertyWriterArr2;
    }

    public final String _customTypeId(Object obj) {
        Object value = this._typeId.getValue(obj);
        return value == null ? "" : value instanceof String ? (String) value : value.toString();
    }

    public void _serializeObjectId(Object obj, JsonGenerator jsonGenerator, k kVar, c cVar, com.fasterxml.jackson.databind.ser.impl.c cVar2) throws IOException {
        kl2 kl2Var = this._objectIdWriter;
        String _customTypeId = this._typeId == null ? null : _customTypeId(obj);
        if (_customTypeId == null) {
            cVar.i(obj, jsonGenerator);
        } else {
            cVar.e(obj, jsonGenerator, _customTypeId);
        }
        cVar2.b(jsonGenerator, kVar, kl2Var);
        if (this._propertyFilterId != null) {
            serializeFieldsFiltered(obj, jsonGenerator, kVar);
        } else {
            serializeFields(obj, jsonGenerator, kVar);
        }
        if (_customTypeId == null) {
            cVar.m(obj, jsonGenerator);
        } else {
            cVar.g(obj, jsonGenerator, _customTypeId);
        }
    }

    public final void _serializeWithObjectId(Object obj, JsonGenerator jsonGenerator, k kVar, boolean z) throws IOException {
        kl2 kl2Var = this._objectIdWriter;
        com.fasterxml.jackson.databind.ser.impl.c findObjectId = kVar.findObjectId(obj, kl2Var.c);
        if (findObjectId.c(jsonGenerator, kVar, kl2Var)) {
            return;
        }
        Object a2 = findObjectId.a(obj);
        if (kl2Var.e) {
            kl2Var.d.serialize(a2, jsonGenerator, kVar);
            return;
        }
        if (z) {
            jsonGenerator.k1(obj);
        }
        findObjectId.b(jsonGenerator, kVar, kl2Var);
        if (this._propertyFilterId != null) {
            serializeFieldsFiltered(obj, jsonGenerator, kVar);
        } else {
            serializeFields(obj, jsonGenerator, kVar);
        }
        if (z) {
            jsonGenerator.f0();
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        d l;
        BeanPropertyWriter[] beanPropertyWriterArr;
        if (bVar == null || (l = bVar.l(javaType)) == null) {
            return;
        }
        k b = bVar.b();
        int i = 0;
        Class<?> cls = null;
        if (this._propertyFilterId != null) {
            com.fasterxml.jackson.databind.ser.d findPropertyFilter = findPropertyFilter(bVar.b(), this._propertyFilterId, null);
            int length = this._props.length;
            while (i < length) {
                findPropertyFilter.c(this._props[i], l, b);
                i++;
            }
            return;
        }
        if (this._filteredProps != null && b != null) {
            cls = b.getActiveView();
        }
        if (cls != null) {
            beanPropertyWriterArr = this._filteredProps;
        } else {
            beanPropertyWriterArr = this._props;
        }
        int length2 = beanPropertyWriterArr.length;
        while (i < length2) {
            BeanPropertyWriter beanPropertyWriter = beanPropertyWriterArr[i];
            if (beanPropertyWriter != null) {
                beanPropertyWriter.depositSchemaProperty(l, b);
            }
            i++;
        }
    }

    public abstract BeanSerializerBase asArraySerializer();

    @Override // com.fasterxml.jackson.databind.ser.b
    public f<?> createContextual(k kVar, com.fasterxml.jackson.databind.a aVar) throws JsonMappingException {
        JsonFormat.Shape shape;
        Object obj;
        kl2 d;
        Object obj2;
        jl2 findObjectReferenceInfo;
        int i;
        AnnotationIntrospector annotationIntrospector = kVar.getAnnotationIntrospector();
        Set<String> set = null;
        AnnotatedMember member = (aVar == null || annotationIntrospector == null) ? null : aVar.getMember();
        SerializationConfig config = kVar.getConfig();
        JsonFormat.Value findFormatOverrides = findFormatOverrides(kVar, aVar, handledType());
        if (findFormatOverrides == null || !findFormatOverrides.hasShape()) {
            shape = null;
        } else {
            shape = findFormatOverrides.getShape();
            if (shape != JsonFormat.Shape.ANY && shape != this._serializationShape && this._handledType.isEnum() && ((i = a.a[shape.ordinal()]) == 1 || i == 2 || i == 3)) {
                return kVar.handlePrimaryContextualization(EnumSerializer.construct(this._handledType, kVar.getConfig(), config.introspectClassAnnotations((Class<?>) this._handledType), findFormatOverrides), aVar);
            }
        }
        kl2 kl2Var = this._objectIdWriter;
        if (member != null) {
            JsonIgnoreProperties.Value findPropertyIgnorals = annotationIntrospector.findPropertyIgnorals(member);
            Set<String> findIgnoredForSerialization = findPropertyIgnorals != null ? findPropertyIgnorals.findIgnoredForSerialization() : null;
            jl2 findObjectIdInfo = annotationIntrospector.findObjectIdInfo(member);
            if (findObjectIdInfo != null) {
                jl2 findObjectReferenceInfo2 = annotationIntrospector.findObjectReferenceInfo(member, findObjectIdInfo);
                Class<? extends ObjectIdGenerator<?>> c = findObjectReferenceInfo2.c();
                JavaType javaType = kVar.getTypeFactory().findTypeParameters(kVar.constructType(c), ObjectIdGenerator.class)[0];
                if (c == ObjectIdGenerators$PropertyGenerator.class) {
                    String simpleName = findObjectReferenceInfo2.d().getSimpleName();
                    int length = this._props.length;
                    for (int i2 = 0; i2 != length; i2++) {
                        BeanPropertyWriter beanPropertyWriter = this._props[i2];
                        if (simpleName.equals(beanPropertyWriter.getName())) {
                            if (i2 > 0) {
                                BeanPropertyWriter[] beanPropertyWriterArr = this._props;
                                System.arraycopy(beanPropertyWriterArr, 0, beanPropertyWriterArr, 1, i2);
                                this._props[0] = beanPropertyWriter;
                                BeanPropertyWriter[] beanPropertyWriterArr2 = this._filteredProps;
                                if (beanPropertyWriterArr2 != null) {
                                    BeanPropertyWriter beanPropertyWriter2 = beanPropertyWriterArr2[i2];
                                    System.arraycopy(beanPropertyWriterArr2, 0, beanPropertyWriterArr2, 1, i2);
                                    this._filteredProps[0] = beanPropertyWriter2;
                                }
                            }
                            kl2Var = kl2.a(beanPropertyWriter.getType(), null, new PropertyBasedObjectIdGenerator(findObjectReferenceInfo2, beanPropertyWriter), findObjectReferenceInfo2.b());
                        }
                    }
                    throw new IllegalArgumentException("Invalid Object Id definition for " + this._handledType.getName() + ": can not find property with name '" + simpleName + "'");
                }
                kl2Var = kl2.a(javaType, findObjectReferenceInfo2.d(), kVar.objectIdGeneratorInstance(member, findObjectReferenceInfo2), findObjectReferenceInfo2.b());
            } else if (kl2Var != null && (findObjectReferenceInfo = annotationIntrospector.findObjectReferenceInfo(member, null)) != null) {
                kl2Var = this._objectIdWriter.c(findObjectReferenceInfo.b());
            }
            obj = annotationIntrospector.findFilterId(member);
            if (obj == null || ((obj2 = this._propertyFilterId) != null && obj.equals(obj2))) {
                obj = null;
            }
            set = findIgnoredForSerialization;
        } else {
            obj = null;
        }
        BeanSerializerBase withObjectIdWriter = (kl2Var == null || (d = kl2Var.d(kVar.findValueSerializer(kl2Var.a, aVar))) == this._objectIdWriter) ? this : withObjectIdWriter(d);
        if (set != null && !set.isEmpty()) {
            withObjectIdWriter = withObjectIdWriter.withIgnorals(set);
        }
        if (obj != null) {
            withObjectIdWriter = withObjectIdWriter.withFilterId(obj);
        }
        if (shape == null) {
            shape = this._serializationShape;
        }
        return shape == JsonFormat.Shape.ARRAY ? withObjectIdWriter.asArraySerializer() : withObjectIdWriter;
    }

    public f<Object> findConvertingSerializer(k kVar, BeanPropertyWriter beanPropertyWriter) throws JsonMappingException {
        AnnotatedMember member;
        Object findSerializationConverter;
        AnnotationIntrospector annotationIntrospector = kVar.getAnnotationIntrospector();
        if (annotationIntrospector == null || (member = beanPropertyWriter.getMember()) == null || (findSerializationConverter = annotationIntrospector.findSerializationConverter(member)) == null) {
            return null;
        }
        o80<Object, Object> converterInstance = kVar.converterInstance(beanPropertyWriter.getMember(), findSerializationConverter);
        JavaType c = converterInstance.c(kVar.getTypeFactory());
        return new StdDelegatingSerializer(converterInstance, c, c.isJavaLangObject() ? null : kVar.findValueSerializer(c, beanPropertyWriter));
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    @Deprecated
    public com.fasterxml.jackson.databind.d getSchema(k kVar, Type type) throws JsonMappingException {
        String id;
        m createSchemaNode = createSchemaNode("object", true);
        bw1 bw1Var = (bw1) this._handledType.getAnnotation(bw1.class);
        if (bw1Var != null && (id = bw1Var.id()) != null && id.length() > 0) {
            createSchemaNode.T("id", id);
        }
        m O = createSchemaNode.O();
        Object obj = this._propertyFilterId;
        com.fasterxml.jackson.databind.ser.d findPropertyFilter = obj != null ? findPropertyFilter(kVar, obj, null) : null;
        int i = 0;
        while (true) {
            BeanPropertyWriter[] beanPropertyWriterArr = this._props;
            if (i < beanPropertyWriterArr.length) {
                BeanPropertyWriter beanPropertyWriter = beanPropertyWriterArr[i];
                if (findPropertyFilter == null) {
                    beanPropertyWriter.depositSchemaProperty(O, kVar);
                } else {
                    findPropertyFilter.a(beanPropertyWriter, O, kVar);
                }
                i++;
            } else {
                createSchemaNode.Z("properties", O);
                return createSchemaNode;
            }
        }
    }

    @Override // com.fasterxml.jackson.databind.f
    public Iterator<PropertyWriter> properties() {
        return Arrays.asList(this._props).iterator();
    }

    @Override // com.fasterxml.jackson.databind.ser.e
    public void resolve(k kVar) throws JsonMappingException {
        BeanPropertyWriter beanPropertyWriter;
        c cVar;
        f<Object> findNullValueSerializer;
        BeanPropertyWriter beanPropertyWriter2;
        BeanPropertyWriter[] beanPropertyWriterArr = this._filteredProps;
        int length = beanPropertyWriterArr == null ? 0 : beanPropertyWriterArr.length;
        int length2 = this._props.length;
        for (int i = 0; i < length2; i++) {
            BeanPropertyWriter beanPropertyWriter3 = this._props[i];
            if (!beanPropertyWriter3.willSuppressNulls() && !beanPropertyWriter3.hasNullSerializer() && (findNullValueSerializer = kVar.findNullValueSerializer(beanPropertyWriter3)) != null) {
                beanPropertyWriter3.assignNullSerializer(findNullValueSerializer);
                if (i < length && (beanPropertyWriter2 = this._filteredProps[i]) != null) {
                    beanPropertyWriter2.assignNullSerializer(findNullValueSerializer);
                }
            }
            if (!beanPropertyWriter3.hasSerializer()) {
                f<Object> findConvertingSerializer = findConvertingSerializer(kVar, beanPropertyWriter3);
                if (findConvertingSerializer == null) {
                    JavaType serializationType = beanPropertyWriter3.getSerializationType();
                    if (serializationType == null) {
                        serializationType = beanPropertyWriter3.getType();
                        if (!serializationType.isFinal()) {
                            if (serializationType.isContainerType() || serializationType.containedTypeCount() > 0) {
                                beanPropertyWriter3.setNonTrivialBaseType(serializationType);
                            }
                        }
                    }
                    f<Object> findValueSerializer = kVar.findValueSerializer(serializationType, beanPropertyWriter3);
                    findConvertingSerializer = (serializationType.isContainerType() && (cVar = (c) serializationType.getContentType().getTypeHandler()) != null && (findValueSerializer instanceof ContainerSerializer)) ? ((ContainerSerializer) findValueSerializer).withValueTypeSerializer(cVar) : findValueSerializer;
                }
                beanPropertyWriter3.assignSerializer(findConvertingSerializer);
                if (i < length && (beanPropertyWriter = this._filteredProps[i]) != null) {
                    beanPropertyWriter.assignSerializer(findConvertingSerializer);
                }
            }
        }
        com.fasterxml.jackson.databind.ser.a aVar = this._anyGetterWriter;
        if (aVar != null) {
            aVar.d(kVar);
        }
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public abstract void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException;

    public void serializeFields(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        BeanPropertyWriter[] beanPropertyWriterArr;
        if (this._filteredProps != null && kVar.getActiveView() != null) {
            beanPropertyWriterArr = this._filteredProps;
        } else {
            beanPropertyWriterArr = this._props;
        }
        int i = 0;
        try {
            int length = beanPropertyWriterArr.length;
            while (i < length) {
                BeanPropertyWriter beanPropertyWriter = beanPropertyWriterArr[i];
                if (beanPropertyWriter != null) {
                    beanPropertyWriter.serializeAsField(obj, jsonGenerator, kVar);
                }
                i++;
            }
            com.fasterxml.jackson.databind.ser.a aVar = this._anyGetterWriter;
            if (aVar != null) {
                aVar.c(obj, jsonGenerator, kVar);
            }
        } catch (Exception e) {
            wrapAndThrow(kVar, e, obj, i != beanPropertyWriterArr.length ? beanPropertyWriterArr[i].getName() : "[anySetter]");
        } catch (StackOverflowError e2) {
            JsonMappingException jsonMappingException = new JsonMappingException(jsonGenerator, "Infinite recursion (StackOverflowError)", e2);
            jsonMappingException.prependPath(new JsonMappingException.Reference(obj, i != beanPropertyWriterArr.length ? beanPropertyWriterArr[i].getName() : "[anySetter]"));
            throw jsonMappingException;
        }
    }

    public void serializeFieldsFiltered(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException, JsonGenerationException {
        BeanPropertyWriter[] beanPropertyWriterArr;
        if (this._filteredProps != null && kVar.getActiveView() != null) {
            beanPropertyWriterArr = this._filteredProps;
        } else {
            beanPropertyWriterArr = this._props;
        }
        com.fasterxml.jackson.databind.ser.d findPropertyFilter = findPropertyFilter(kVar, this._propertyFilterId, obj);
        if (findPropertyFilter == null) {
            serializeFields(obj, jsonGenerator, kVar);
            return;
        }
        int i = 0;
        try {
            int length = beanPropertyWriterArr.length;
            while (i < length) {
                BeanPropertyWriter beanPropertyWriter = beanPropertyWriterArr[i];
                if (beanPropertyWriter != null) {
                    findPropertyFilter.b(obj, jsonGenerator, kVar, beanPropertyWriter);
                }
                i++;
            }
            com.fasterxml.jackson.databind.ser.a aVar = this._anyGetterWriter;
            if (aVar != null) {
                aVar.b(obj, jsonGenerator, kVar, findPropertyFilter);
            }
        } catch (Exception e) {
            wrapAndThrow(kVar, e, obj, i != beanPropertyWriterArr.length ? beanPropertyWriterArr[i].getName() : "[anySetter]");
        } catch (StackOverflowError e2) {
            JsonMappingException jsonMappingException = new JsonMappingException(jsonGenerator, "Infinite recursion (StackOverflowError)", e2);
            jsonMappingException.prependPath(new JsonMappingException.Reference(obj, i != beanPropertyWriterArr.length ? beanPropertyWriterArr[i].getName() : "[anySetter]"));
            throw jsonMappingException;
        }
    }

    @Override // com.fasterxml.jackson.databind.f
    public void serializeWithType(Object obj, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        if (this._objectIdWriter != null) {
            jsonGenerator.u(obj);
            _serializeWithObjectId(obj, jsonGenerator, kVar, cVar);
            return;
        }
        String _customTypeId = this._typeId == null ? null : _customTypeId(obj);
        if (_customTypeId == null) {
            cVar.i(obj, jsonGenerator);
        } else {
            cVar.e(obj, jsonGenerator, _customTypeId);
        }
        jsonGenerator.u(obj);
        if (this._propertyFilterId != null) {
            serializeFieldsFiltered(obj, jsonGenerator, kVar);
        } else {
            serializeFields(obj, jsonGenerator, kVar);
        }
        if (_customTypeId == null) {
            cVar.m(obj, jsonGenerator);
        } else {
            cVar.g(obj, jsonGenerator, _customTypeId);
        }
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean usesObjectId() {
        return this._objectIdWriter != null;
    }

    @Override // com.fasterxml.jackson.databind.f
    public abstract BeanSerializerBase withFilterId(Object obj);

    public abstract BeanSerializerBase withIgnorals(Set<String> set);

    @Deprecated
    public BeanSerializerBase withIgnorals(String[] strArr) {
        return withIgnorals(lh.a(strArr));
    }

    public abstract BeanSerializerBase withObjectIdWriter(kl2 kl2Var);

    public final void _serializeWithObjectId(Object obj, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        kl2 kl2Var = this._objectIdWriter;
        com.fasterxml.jackson.databind.ser.impl.c findObjectId = kVar.findObjectId(obj, kl2Var.c);
        if (findObjectId.c(jsonGenerator, kVar, kl2Var)) {
            return;
        }
        Object a2 = findObjectId.a(obj);
        if (kl2Var.e) {
            kl2Var.d.serialize(a2, jsonGenerator, kVar);
        } else {
            _serializeObjectId(obj, jsonGenerator, kVar, cVar, findObjectId);
        }
    }

    public BeanSerializerBase(BeanSerializerBase beanSerializerBase, BeanPropertyWriter[] beanPropertyWriterArr, BeanPropertyWriter[] beanPropertyWriterArr2) {
        super(beanSerializerBase._handledType);
        this._props = beanPropertyWriterArr;
        this._filteredProps = beanPropertyWriterArr2;
        this._typeId = beanSerializerBase._typeId;
        this._anyGetterWriter = beanSerializerBase._anyGetterWriter;
        this._objectIdWriter = beanSerializerBase._objectIdWriter;
        this._propertyFilterId = beanSerializerBase._propertyFilterId;
        this._serializationShape = beanSerializerBase._serializationShape;
    }

    public BeanSerializerBase(BeanSerializerBase beanSerializerBase, kl2 kl2Var) {
        this(beanSerializerBase, kl2Var, beanSerializerBase._propertyFilterId);
    }

    public BeanSerializerBase(BeanSerializerBase beanSerializerBase, kl2 kl2Var, Object obj) {
        super(beanSerializerBase._handledType);
        this._props = beanSerializerBase._props;
        this._filteredProps = beanSerializerBase._filteredProps;
        this._typeId = beanSerializerBase._typeId;
        this._anyGetterWriter = beanSerializerBase._anyGetterWriter;
        this._objectIdWriter = kl2Var;
        this._propertyFilterId = obj;
        this._serializationShape = beanSerializerBase._serializationShape;
    }

    @Deprecated
    public BeanSerializerBase(BeanSerializerBase beanSerializerBase, String[] strArr) {
        this(beanSerializerBase, lh.a(strArr));
    }

    public BeanSerializerBase(BeanSerializerBase beanSerializerBase, Set<String> set) {
        super(beanSerializerBase._handledType);
        BeanPropertyWriter[] beanPropertyWriterArr = beanSerializerBase._props;
        BeanPropertyWriter[] beanPropertyWriterArr2 = beanSerializerBase._filteredProps;
        int length = beanPropertyWriterArr.length;
        ArrayList arrayList = new ArrayList(length);
        ArrayList arrayList2 = beanPropertyWriterArr2 == null ? null : new ArrayList(length);
        for (int i = 0; i < length; i++) {
            BeanPropertyWriter beanPropertyWriter = beanPropertyWriterArr[i];
            if (set == null || !set.contains(beanPropertyWriter.getName())) {
                arrayList.add(beanPropertyWriter);
                if (beanPropertyWriterArr2 != null) {
                    arrayList2.add(beanPropertyWriterArr2[i]);
                }
            }
        }
        this._props = (BeanPropertyWriter[]) arrayList.toArray(new BeanPropertyWriter[arrayList.size()]);
        this._filteredProps = arrayList2 != null ? (BeanPropertyWriter[]) arrayList2.toArray(new BeanPropertyWriter[arrayList2.size()]) : null;
        this._typeId = beanSerializerBase._typeId;
        this._anyGetterWriter = beanSerializerBase._anyGetterWriter;
        this._objectIdWriter = beanSerializerBase._objectIdWriter;
        this._propertyFilterId = beanSerializerBase._propertyFilterId;
        this._serializationShape = beanSerializerBase._serializationShape;
    }

    public BeanSerializerBase(BeanSerializerBase beanSerializerBase) {
        this(beanSerializerBase, beanSerializerBase._props, beanSerializerBase._filteredProps);
    }

    public BeanSerializerBase(BeanSerializerBase beanSerializerBase, NameTransformer nameTransformer) {
        this(beanSerializerBase, a(beanSerializerBase._props, nameTransformer), a(beanSerializerBase._filteredProps, nameTransformer));
    }
}
