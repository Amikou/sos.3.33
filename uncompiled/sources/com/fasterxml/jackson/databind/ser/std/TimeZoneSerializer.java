package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.util.TimeZone;

/* loaded from: classes.dex */
public class TimeZoneSerializer extends StdScalarSerializer<TimeZone> {
    public TimeZoneSerializer() {
        super(TimeZone.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(TimeZone timeZone, JsonGenerator jsonGenerator, k kVar) throws IOException {
        jsonGenerator.o1(timeZone.getID());
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdScalarSerializer, com.fasterxml.jackson.databind.f
    public void serializeWithType(TimeZone timeZone, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        cVar.k(timeZone, jsonGenerator, TimeZone.class);
        serialize(timeZone, jsonGenerator, kVar);
        cVar.n(timeZone, jsonGenerator);
    }
}
