package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.util.e;
import java.io.IOException;
import java.lang.reflect.Type;

@pt1
/* loaded from: classes.dex */
public class TokenBufferSerializer extends StdSerializer<e> {
    public TokenBufferSerializer() {
        super(e.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        bVar.e(javaType);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode("any", true);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(e eVar, JsonGenerator jsonGenerator, k kVar) throws IOException {
        eVar.T1(jsonGenerator);
    }

    @Override // com.fasterxml.jackson.databind.f
    public final void serializeWithType(e eVar, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException {
        cVar.j(eVar, jsonGenerator);
        serialize(eVar, jsonGenerator, kVar);
        cVar.n(eVar, jsonGenerator);
    }
}
