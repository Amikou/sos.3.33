package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.util.e;
import java.io.IOException;
import java.util.UUID;

/* loaded from: classes.dex */
public class UUIDSerializer extends StdScalarSerializer<UUID> {
    public static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();

    public UUIDSerializer() {
        super(UUID.class);
    }

    public static final void a(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >> 24);
        int i3 = i2 + 1;
        bArr[i3] = (byte) (i >> 16);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i >> 8);
        bArr[i4 + 1] = (byte) i;
    }

    public static void b(int i, char[] cArr, int i2) {
        c(i >> 16, cArr, i2);
        c(i, cArr, i2 + 4);
    }

    public static void c(int i, char[] cArr, int i2) {
        char[] cArr2 = HEX_CHARS;
        cArr[i2] = cArr2[(i >> 12) & 15];
        int i3 = i2 + 1;
        cArr[i3] = cArr2[(i >> 8) & 15];
        int i4 = i3 + 1;
        cArr[i4] = cArr2[(i >> 4) & 15];
        cArr[i4 + 1] = cArr2[i & 15];
    }

    public static final byte[] d(UUID uuid) {
        byte[] bArr = new byte[16];
        long mostSignificantBits = uuid.getMostSignificantBits();
        long leastSignificantBits = uuid.getLeastSignificantBits();
        a((int) (mostSignificantBits >> 32), bArr, 0);
        a((int) mostSignificantBits, bArr, 4);
        a((int) (leastSignificantBits >> 32), bArr, 8);
        a((int) leastSignificantBits, bArr, 12);
        return bArr;
    }

    @Override // com.fasterxml.jackson.databind.f
    public boolean isEmpty(k kVar, UUID uuid) {
        if (uuid == null) {
            return true;
        }
        return uuid.getLeastSignificantBits() == 0 && uuid.getMostSignificantBits() == 0;
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(UUID uuid, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (jsonGenerator.f() && !(jsonGenerator instanceof e)) {
            jsonGenerator.W(d(uuid));
            return;
        }
        char[] cArr = new char[36];
        long mostSignificantBits = uuid.getMostSignificantBits();
        b((int) (mostSignificantBits >> 32), cArr, 0);
        cArr[8] = '-';
        int i = (int) mostSignificantBits;
        c(i >>> 16, cArr, 9);
        cArr[13] = '-';
        c(i, cArr, 14);
        cArr[18] = '-';
        long leastSignificantBits = uuid.getLeastSignificantBits();
        c((int) (leastSignificantBits >>> 48), cArr, 19);
        cArr[23] = '-';
        c((int) (leastSignificantBits >>> 32), cArr, 24);
        b((int) leastSignificantBits, cArr, 28);
        jsonGenerator.p1(cArr, 0, 36);
    }
}
