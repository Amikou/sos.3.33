package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import org.web3j.abi.datatypes.Utf8String;

/* loaded from: classes.dex */
public class StdArraySerializers {
    public static final HashMap<String, f<?>> a;

    @pt1
    /* loaded from: classes.dex */
    public static class BooleanArraySerializer extends ArraySerializerBase<boolean[]> {
        public static final JavaType f0 = TypeFactory.defaultInstance().uncheckedSimpleType(Boolean.class);

        public BooleanArraySerializer() {
            super(boolean[].class);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public f<?> _withResolved(a aVar, Boolean bool) {
            return new BooleanArraySerializer(this, aVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
            return this;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitArrayFormat(bVar, javaType, JsonFormatTypes.BOOLEAN);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public f<?> getContentSerializer() {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public JavaType getContentType() {
            return f0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            m createSchemaNode = createSchemaNode("array", true);
            createSchemaNode.Z("items", createSchemaNode("boolean"));
            return createSchemaNode;
        }

        public BooleanArraySerializer(BooleanArraySerializer booleanArraySerializer, a aVar, Boolean bool) {
            super(booleanArraySerializer, aVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public boolean hasSingleElement(boolean[] zArr) {
            return zArr.length == 1;
        }

        @Override // com.fasterxml.jackson.databind.f
        public boolean isEmpty(k kVar, boolean[] zArr) {
            return zArr == null || zArr.length == 0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public final void serialize(boolean[] zArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            int length = zArr.length;
            if (length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
                serializeContents(zArr, jsonGenerator, kVar);
                return;
            }
            jsonGenerator.f1(length);
            serializeContents(zArr, jsonGenerator, kVar);
            jsonGenerator.e0();
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public void serializeContents(boolean[] zArr, JsonGenerator jsonGenerator, k kVar) throws IOException, JsonGenerationException {
            for (boolean z : zArr) {
                jsonGenerator.a0(z);
            }
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static class CharArraySerializer extends StdSerializer<char[]> {
        public CharArraySerializer() {
            super(char[].class);
        }

        public final void a(JsonGenerator jsonGenerator, char[] cArr) throws IOException, JsonGenerationException {
            int length = cArr.length;
            for (int i = 0; i < length; i++) {
                jsonGenerator.p1(cArr, i, 1);
            }
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitArrayFormat(bVar, javaType, JsonFormatTypes.STRING);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            m createSchemaNode = createSchemaNode("array", true);
            m createSchemaNode2 = createSchemaNode(Utf8String.TYPE_NAME);
            createSchemaNode2.T("type", Utf8String.TYPE_NAME);
            return createSchemaNode.Z("items", createSchemaNode2);
        }

        @Override // com.fasterxml.jackson.databind.f
        public boolean isEmpty(k kVar, char[] cArr) {
            return cArr == null || cArr.length == 0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void serialize(char[] cArr, JsonGenerator jsonGenerator, k kVar) throws IOException, JsonGenerationException {
            if (kVar.isEnabled(SerializationFeature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS)) {
                jsonGenerator.f1(cArr.length);
                a(jsonGenerator, cArr);
                jsonGenerator.e0();
                return;
            }
            jsonGenerator.p1(cArr, 0, cArr.length);
        }

        @Override // com.fasterxml.jackson.databind.f
        public void serializeWithType(char[] cArr, JsonGenerator jsonGenerator, k kVar, c cVar) throws IOException, JsonGenerationException {
            if (kVar.isEnabled(SerializationFeature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS)) {
                cVar.h(cArr, jsonGenerator);
                a(jsonGenerator, cArr);
                cVar.l(cArr, jsonGenerator);
                return;
            }
            cVar.j(cArr, jsonGenerator);
            jsonGenerator.p1(cArr, 0, cArr.length);
            cVar.n(cArr, jsonGenerator);
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static class DoubleArraySerializer extends ArraySerializerBase<double[]> {
        public static final JavaType f0 = TypeFactory.defaultInstance().uncheckedSimpleType(Double.TYPE);

        public DoubleArraySerializer() {
            super(double[].class);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public f<?> _withResolved(a aVar, Boolean bool) {
            return new DoubleArraySerializer(this, aVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
            return this;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitArrayFormat(bVar, javaType, JsonFormatTypes.NUMBER);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public f<?> getContentSerializer() {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public JavaType getContentType() {
            return f0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            return createSchemaNode("array", true).Z("items", createSchemaNode("number"));
        }

        public DoubleArraySerializer(DoubleArraySerializer doubleArraySerializer, a aVar, Boolean bool) {
            super(doubleArraySerializer, aVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public boolean hasSingleElement(double[] dArr) {
            return dArr.length == 1;
        }

        @Override // com.fasterxml.jackson.databind.f
        public boolean isEmpty(k kVar, double[] dArr) {
            return dArr == null || dArr.length == 0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public final void serialize(double[] dArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            if (dArr.length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
                serializeContents(dArr, jsonGenerator, kVar);
                return;
            }
            jsonGenerator.u(dArr);
            jsonGenerator.C(dArr, 0, dArr.length);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public void serializeContents(double[] dArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            for (double d : dArr) {
                jsonGenerator.r0(d);
            }
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static class FloatArraySerializer extends TypedPrimitiveArraySerializer<float[]> {
        public static final JavaType f0 = TypeFactory.defaultInstance().uncheckedSimpleType(Float.TYPE);

        public FloatArraySerializer() {
            super(float[].class);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public f<?> _withResolved(a aVar, Boolean bool) {
            return new FloatArraySerializer(this, aVar, this._valueTypeSerializer, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
            return new FloatArraySerializer(this, this._property, cVar, this._unwrapSingle);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitArrayFormat(bVar, javaType, JsonFormatTypes.NUMBER);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public f<?> getContentSerializer() {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public JavaType getContentType() {
            return f0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            return createSchemaNode("array", true).Z("items", createSchemaNode("number"));
        }

        public FloatArraySerializer(FloatArraySerializer floatArraySerializer, a aVar, c cVar, Boolean bool) {
            super(floatArraySerializer, aVar, cVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public boolean hasSingleElement(float[] fArr) {
            return fArr.length == 1;
        }

        @Override // com.fasterxml.jackson.databind.f
        public boolean isEmpty(k kVar, float[] fArr) {
            return fArr == null || fArr.length == 0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public final void serialize(float[] fArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            int length = fArr.length;
            if (length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
                serializeContents(fArr, jsonGenerator, kVar);
                return;
            }
            jsonGenerator.f1(length);
            serializeContents(fArr, jsonGenerator, kVar);
            jsonGenerator.e0();
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public void serializeContents(float[] fArr, JsonGenerator jsonGenerator, k kVar) throws IOException, JsonGenerationException {
            int i = 0;
            if (this._valueTypeSerializer != null) {
                int length = fArr.length;
                while (i < length) {
                    this._valueTypeSerializer.k(null, jsonGenerator, Float.TYPE);
                    jsonGenerator.w0(fArr[i]);
                    this._valueTypeSerializer.n(null, jsonGenerator);
                    i++;
                }
                return;
            }
            int length2 = fArr.length;
            while (i < length2) {
                jsonGenerator.w0(fArr[i]);
                i++;
            }
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static class IntArraySerializer extends ArraySerializerBase<int[]> {
        public static final JavaType f0 = TypeFactory.defaultInstance().uncheckedSimpleType(Integer.TYPE);

        public IntArraySerializer() {
            super(int[].class);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public f<?> _withResolved(a aVar, Boolean bool) {
            return new IntArraySerializer(this, aVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
            return this;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitArrayFormat(bVar, javaType, JsonFormatTypes.INTEGER);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public f<?> getContentSerializer() {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public JavaType getContentType() {
            return f0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            return createSchemaNode("array", true).Z("items", createSchemaNode("integer"));
        }

        public IntArraySerializer(IntArraySerializer intArraySerializer, a aVar, Boolean bool) {
            super(intArraySerializer, aVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public boolean hasSingleElement(int[] iArr) {
            return iArr.length == 1;
        }

        @Override // com.fasterxml.jackson.databind.f
        public boolean isEmpty(k kVar, int[] iArr) {
            return iArr == null || iArr.length == 0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public final void serialize(int[] iArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            if (iArr.length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
                serializeContents(iArr, jsonGenerator, kVar);
                return;
            }
            jsonGenerator.u(iArr);
            jsonGenerator.F(iArr, 0, iArr.length);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public void serializeContents(int[] iArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            for (int i : iArr) {
                jsonGenerator.x0(i);
            }
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static class LongArraySerializer extends TypedPrimitiveArraySerializer<long[]> {
        public static final JavaType f0 = TypeFactory.defaultInstance().uncheckedSimpleType(Long.TYPE);

        public LongArraySerializer() {
            super(long[].class);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public f<?> _withResolved(a aVar, Boolean bool) {
            return new LongArraySerializer(this, aVar, this._valueTypeSerializer, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
            return new LongArraySerializer(this, this._property, cVar, this._unwrapSingle);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitArrayFormat(bVar, javaType, JsonFormatTypes.NUMBER);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public f<?> getContentSerializer() {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public JavaType getContentType() {
            return f0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            return createSchemaNode("array", true).Z("items", createSchemaNode("number", true));
        }

        public LongArraySerializer(LongArraySerializer longArraySerializer, a aVar, c cVar, Boolean bool) {
            super(longArraySerializer, aVar, cVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public boolean hasSingleElement(long[] jArr) {
            return jArr.length == 1;
        }

        @Override // com.fasterxml.jackson.databind.f
        public boolean isEmpty(k kVar, long[] jArr) {
            return jArr == null || jArr.length == 0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public final void serialize(long[] jArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            if (jArr.length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
                serializeContents(jArr, jsonGenerator, kVar);
                return;
            }
            jsonGenerator.u(jArr);
            jsonGenerator.M(jArr, 0, jArr.length);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public void serializeContents(long[] jArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            int i = 0;
            if (this._valueTypeSerializer != null) {
                int length = jArr.length;
                while (i < length) {
                    this._valueTypeSerializer.k(null, jsonGenerator, Long.TYPE);
                    jsonGenerator.y0(jArr[i]);
                    this._valueTypeSerializer.n(null, jsonGenerator);
                    i++;
                }
                return;
            }
            int length2 = jArr.length;
            while (i < length2) {
                jsonGenerator.y0(jArr[i]);
                i++;
            }
        }
    }

    @pt1
    /* loaded from: classes.dex */
    public static class ShortArraySerializer extends TypedPrimitiveArraySerializer<short[]> {
        public static final JavaType f0 = TypeFactory.defaultInstance().uncheckedSimpleType(Short.TYPE);

        public ShortArraySerializer() {
            super(short[].class);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public f<?> _withResolved(a aVar, Boolean bool) {
            return new ShortArraySerializer(this, aVar, this._valueTypeSerializer, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public ContainerSerializer<?> _withValueTypeSerializer(c cVar) {
            return new ShortArraySerializer(this, this._property, cVar, this._unwrapSingle);
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
            visitArrayFormat(bVar, javaType, JsonFormatTypes.INTEGER);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public f<?> getContentSerializer() {
            return null;
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public JavaType getContentType() {
            return f0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
        public d getSchema(k kVar, Type type) {
            return createSchemaNode("array", true).Z("items", createSchemaNode("integer"));
        }

        public ShortArraySerializer(ShortArraySerializer shortArraySerializer, a aVar, c cVar, Boolean bool) {
            super(shortArraySerializer, aVar, cVar, bool);
        }

        @Override // com.fasterxml.jackson.databind.ser.ContainerSerializer
        public boolean hasSingleElement(short[] sArr) {
            return sArr.length == 1;
        }

        @Override // com.fasterxml.jackson.databind.f
        public boolean isEmpty(k kVar, short[] sArr) {
            return sArr == null || sArr.length == 0;
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase, com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
        public final void serialize(short[] sArr, JsonGenerator jsonGenerator, k kVar) throws IOException {
            int length = sArr.length;
            if (length == 1 && ((this._unwrapSingle == null && kVar.isEnabled(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) || this._unwrapSingle == Boolean.TRUE)) {
                serializeContents(sArr, jsonGenerator, kVar);
                return;
            }
            jsonGenerator.f1(length);
            serializeContents(sArr, jsonGenerator, kVar);
            jsonGenerator.e0();
        }

        @Override // com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
        public void serializeContents(short[] sArr, JsonGenerator jsonGenerator, k kVar) throws IOException, JsonGenerationException {
            int i = 0;
            if (this._valueTypeSerializer != null) {
                int length = sArr.length;
                while (i < length) {
                    this._valueTypeSerializer.k(null, jsonGenerator, Short.TYPE);
                    jsonGenerator.H0(sArr[i]);
                    this._valueTypeSerializer.n(null, jsonGenerator);
                    i++;
                }
                return;
            }
            int length2 = sArr.length;
            while (i < length2) {
                jsonGenerator.x0(sArr[i]);
                i++;
            }
        }
    }

    static {
        HashMap<String, f<?>> hashMap = new HashMap<>();
        a = hashMap;
        hashMap.put(boolean[].class.getName(), new BooleanArraySerializer());
        hashMap.put(byte[].class.getName(), new ByteArraySerializer());
        hashMap.put(char[].class.getName(), new CharArraySerializer());
        hashMap.put(short[].class.getName(), new ShortArraySerializer());
        hashMap.put(int[].class.getName(), new IntArraySerializer());
        hashMap.put(long[].class.getName(), new LongArraySerializer());
        hashMap.put(float[].class.getName(), new FloatArraySerializer());
        hashMap.put(double[].class.getName(), new DoubleArraySerializer());
    }

    public static f<?> a(Class<?> cls) {
        return a.get(cls.getName());
    }

    /* loaded from: classes.dex */
    public static abstract class TypedPrimitiveArraySerializer<T> extends ArraySerializerBase<T> {
        public final c _valueTypeSerializer;

        public TypedPrimitiveArraySerializer(Class<T> cls) {
            super(cls);
            this._valueTypeSerializer = null;
        }

        public TypedPrimitiveArraySerializer(TypedPrimitiveArraySerializer<T> typedPrimitiveArraySerializer, a aVar, c cVar, Boolean bool) {
            super(typedPrimitiveArraySerializer, aVar, bool);
            this._valueTypeSerializer = cVar;
        }
    }
}
