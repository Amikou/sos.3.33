package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.util.a;
import java.io.IOException;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public class ByteBufferSerializer extends StdScalarSerializer<ByteBuffer> {
    public ByteBufferSerializer() {
        super(ByteBuffer.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(ByteBuffer byteBuffer, JsonGenerator jsonGenerator, k kVar) throws IOException {
        if (byteBuffer.hasArray()) {
            jsonGenerator.X(byteBuffer.array(), 0, byteBuffer.limit());
            return;
        }
        ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
        if (asReadOnlyBuffer.position() > 0) {
            asReadOnlyBuffer.rewind();
        }
        a aVar = new a(asReadOnlyBuffer);
        jsonGenerator.R(aVar, asReadOnlyBuffer.remaining());
        aVar.close();
    }
}
