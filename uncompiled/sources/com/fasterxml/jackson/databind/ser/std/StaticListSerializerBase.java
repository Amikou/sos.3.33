package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.k;
import com.fasterxml.jackson.databind.ser.b;
import java.lang.reflect.Type;
import java.util.Collection;

/* loaded from: classes.dex */
public abstract class StaticListSerializerBase<T extends Collection<?>> extends StdSerializer<T> implements b {
    public final f<String> _serializer;
    public final Boolean _unwrapSingle;

    public StaticListSerializerBase(Class<?> cls) {
        super(cls, false);
        this._serializer = null;
        this._unwrapSingle = null;
    }

    public abstract f<?> _withResolved(a aVar, f<?> fVar, Boolean bool);

    public abstract void acceptContentVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.a aVar) throws JsonMappingException;

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(com.fasterxml.jackson.databind.jsonFormatVisitors.b bVar, JavaType javaType) throws JsonMappingException {
        acceptContentVisitor(bVar.j(javaType));
    }

    public abstract d contentSchema();

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:12:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0035  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x003c  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0047  */
    @Override // com.fasterxml.jackson.databind.ser.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.databind.f<?> createContextual(com.fasterxml.jackson.databind.k r5, com.fasterxml.jackson.databind.a r6) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r4 = this;
            r0 = 0
            if (r6 == 0) goto L18
            com.fasterxml.jackson.databind.AnnotationIntrospector r1 = r5.getAnnotationIntrospector()
            com.fasterxml.jackson.databind.introspect.AnnotatedMember r2 = r6.getMember()
            if (r2 == 0) goto L18
            java.lang.Object r1 = r1.findContentSerializer(r2)
            if (r1 == 0) goto L18
            com.fasterxml.jackson.databind.f r1 = r5.serializerInstance(r2, r1)
            goto L19
        L18:
            r1 = r0
        L19:
            java.lang.Class r2 = r4.handledType()
            com.fasterxml.jackson.annotation.JsonFormat$Value r2 = r4.findFormatOverrides(r5, r6, r2)
            if (r2 == 0) goto L2a
            com.fasterxml.jackson.annotation.JsonFormat$Feature r3 = com.fasterxml.jackson.annotation.JsonFormat.Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            java.lang.Boolean r2 = r2.getFeature(r3)
            goto L2b
        L2a:
            r2 = r0
        L2b:
            if (r1 != 0) goto L2f
            com.fasterxml.jackson.databind.f<java.lang.String> r1 = r4._serializer
        L2f:
            com.fasterxml.jackson.databind.f r1 = r4.findConvertingContentSerializer(r5, r6, r1)
            if (r1 != 0) goto L3c
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            com.fasterxml.jackson.databind.f r5 = r5.findValueSerializer(r1, r6)
            goto L40
        L3c:
            com.fasterxml.jackson.databind.f r5 = r5.handleSecondaryContextualization(r1, r6)
        L40:
            boolean r1 = r4.isDefaultSerializer(r5)
            if (r1 == 0) goto L47
            goto L48
        L47:
            r0 = r5
        L48:
            com.fasterxml.jackson.databind.f<java.lang.String> r5 = r4._serializer
            if (r0 != r5) goto L51
            java.lang.Boolean r5 = r4._unwrapSingle
            if (r2 != r5) goto L51
            return r4
        L51:
            com.fasterxml.jackson.databind.f r5 = r4._withResolved(r6, r0, r2)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase.createContextual(com.fasterxml.jackson.databind.k, com.fasterxml.jackson.databind.a):com.fasterxml.jackson.databind.f");
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) {
        return createSchemaNode("array", true).Z("items", contentSchema());
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fasterxml.jackson.databind.f
    public /* bridge */ /* synthetic */ boolean isEmpty(k kVar, Object obj) {
        return isEmpty(kVar, (k) ((Collection) obj));
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fasterxml.jackson.databind.f
    @Deprecated
    public /* bridge */ /* synthetic */ boolean isEmpty(Object obj) {
        return isEmpty((StaticListSerializerBase<T>) ((Collection) obj));
    }

    @Deprecated
    public boolean isEmpty(T t) {
        return isEmpty((k) null, (k) t);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StaticListSerializerBase(StaticListSerializerBase<?> staticListSerializerBase, f<?> fVar, Boolean bool) {
        super(staticListSerializerBase);
        this._serializer = fVar;
        this._unwrapSingle = bool;
    }

    public boolean isEmpty(k kVar, T t) {
        return t == null || t.size() == 0;
    }
}
