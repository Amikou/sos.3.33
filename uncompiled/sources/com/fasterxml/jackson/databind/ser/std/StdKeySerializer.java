package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.jsonFormatVisitors.b;
import com.fasterxml.jackson.databind.k;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import org.web3j.abi.datatypes.Utf8String;

@Deprecated
/* loaded from: classes.dex */
public class StdKeySerializer extends StdSerializer<Object> {
    public StdKeySerializer() {
        super(Object.class);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void acceptJsonFormatVisitor(b bVar, JavaType javaType) throws JsonMappingException {
        visitStringFormat(bVar, javaType);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, defpackage.ld3
    public d getSchema(k kVar, Type type) throws JsonMappingException {
        return createSchemaNode(Utf8String.TYPE_NAME);
    }

    @Override // com.fasterxml.jackson.databind.ser.std.StdSerializer, com.fasterxml.jackson.databind.f
    public void serialize(Object obj, JsonGenerator jsonGenerator, k kVar) throws IOException {
        String obj2;
        Class<?> cls = obj.getClass();
        if (cls == String.class) {
            obj2 = (String) obj;
        } else if (cls.isEnum()) {
            SerializationFeature serializationFeature = SerializationFeature.WRITE_ENUMS_USING_TO_STRING;
            if (kVar.isEnabled(serializationFeature)) {
                obj2 = obj.toString();
            } else {
                Enum r3 = (Enum) obj;
                if (kVar.isEnabled(serializationFeature)) {
                    obj2 = String.valueOf(r3.ordinal());
                } else {
                    obj2 = r3.name();
                }
            }
        } else if (obj instanceof Date) {
            kVar.defaultSerializeDateKey((Date) obj, jsonGenerator);
            return;
        } else if (cls == Class.class) {
            obj2 = ((Class) obj).getName();
        } else {
            obj2 = obj.toString();
        }
        jsonGenerator.i0(obj2);
    }
}
